BEGIN;

-- CREATE SEQUENCE "seq_riwayat_persetujuan" -------------------
CREATE SEQUENCE "ekontrak"."seq_riwayat_persetujuan"
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 2560
CACHE 1;
-- -------------------------------------------------------------

-- CHANGE "CURRENT VALUE" OF "SEQUENCE "seq_riwayat_persetujuan" 
ALTER SEQUENCE "ekontrak"."seq_riwayat_persetujuan" RESTART 2586;
-- -------------------------------------------------------------

COMMIT;
