-------------------fixing pk table riwayat_persetujuan -----------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'riwayat_persetujuan' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
         FOR rec IN SELECT rwt_pst_id FROM riwayat_persetujuan GROUP BY rwt_pst_id HAVING COUNT(rwt_pst_id) > 1 ORDER BY rwt_pst_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM riwayat_persetujuan WHERE rwt_pst_id = rec.rwt_pst_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM riwayat_persetujuan WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
         END LOOP;
        ALTER TABLE riwayat_persetujuan ADD PRIMARY KEY (rwt_pst_id);
    END IF;
END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table ekontak riwayat_persetujuan -----------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'riwayat_persetujuan' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
        FOR rec IN SELECT rwt_pst_id FROM ekontrak.riwayat_persetujuan GROUP BY rwt_pst_id HAVING COUNT(rwt_pst_id) > 1 ORDER BY rwt_pst_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM ekontrak.riwayat_persetujuan WHERE rwt_pst_id = rec.rwt_pst_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM ekontrak.riwayat_persetujuan WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
        END LOOP;
        ALTER TABLE ekontrak.riwayat_persetujuan ADD PRIMARY KEY (rwt_pst_id);
    END IF;
END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table paket_satker -----------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'paket_satker' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
        FOR rec IN SELECT pks_id FROM paket_satker GROUP BY pks_id  HAVING COUNT(pks_id) > 1 ORDER BY pks_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM paket_satker WHERE pks_id = rec.pks_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM paket_satker WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
        END LOOP;
        ALTER TABLE paket_satker ADD PRIMARY KEY (pks_id);
    END IF;
END;
$$;
-----------------------------------end ---------------------------------------

-------------------fixing pk table blob_log -----------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'blob_log' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
         FOR rec IN SELECT blb_log_id FROM blob_log GROUP BY blb_log_id HAVING COUNT(blb_log_id) > 1 ORDER BY blb_log_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM blob_log WHERE blb_log_id = rec.blb_log_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM blob_log WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
         END LOOP;
        ALTER TABLE blob_log ADD PRIMARY KEY (blb_log_id);
    END IF;
END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table wf_workflow, scheme ekontrak -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'wf_workflow' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
            FOR rec IN SELECT workflow_id FROM ekontrak.wf_workflow GROUP BY workflow_id HAVING COUNT(workflow_id) > 1 ORDER BY workflow_id LOOP
                  counter := 0;
                  FOR rec2 IN SELECT ctid FROM ekontrak.wf_workflow WHERE workflow_id = rec.workflow_id LOOP
                       IF (counter > 0) THEN
                          DELETE FROM ekontrak.wf_workflow WHERE ctid::text = rec2.ctid::text;
                       END IF;
                       counter := counter + 1 ;
                  END LOOP;
             END LOOP;
            ALTER TABLE ekontrak.wf_workflow ADD CONSTRAINT pk_wf_workflow PRIMARY KEY (workflow_id);
        END IF;
    END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table wf_process_definition, scheme ekontrak -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'wf_process_definition' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
            FOR rec IN SELECT process_definition_id FROM ekontrak.wf_process_definition GROUP BY process_definition_id HAVING COUNT(process_definition_id) > 1 ORDER BY process_definition_id LOOP
                        counter := 0;
                        FOR rec2 IN SELECT ctid FROM ekontrak.wf_process_definition WHERE process_definition_id = rec.process_definition_id LOOP
                                IF (counter > 0) THEN
                                    DELETE FROM ekontrak.wf_process_definition WHERE ctid::text = rec2.ctid::text;
                                END IF;
                                counter := counter + 1 ;
                        END LOOP;
            END LOOP;
            ALTER TABLE ekontrak.wf_process_definition ADD CONSTRAINT pk_wf_process_definition PRIMARY KEY (process_definition_id);
        END IF;
    END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table wf_process_instance, scheme ekontrak -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'wf_process_instance' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
            FOR rec IN SELECT process_instance_id FROM ekontrak.wf_process_instance GROUP BY process_instance_id HAVING COUNT(process_instance_id) > 1 ORDER BY process_instance_id LOOP
                  counter := 0;
                  FOR rec2 IN SELECT ctid FROM ekontrak.wf_process_instance WHERE process_instance_id = rec.process_instance_id LOOP
                                    IF (counter > 0) THEN
                                        DELETE FROM ekontrak.wf_process_instance WHERE ctid::text = rec2.ctid::text;
                                    END IF;
                                    counter := counter + 1 ;
                  END LOOP;
            END LOOP;
            ALTER TABLE ekontrak.wf_process_instance ADD CONSTRAINT pk_wf_process_instance PRIMARY KEY (process_instance_id);
        END IF;
    END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table wf_state, scheme ekontrak -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'wf_state' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
             FOR rec IN SELECT state_id FROM ekontrak.wf_state GROUP BY state_id HAVING COUNT(state_id) > 1 ORDER BY state_id LOOP
                       counter := 0;
                       FOR rec2 IN SELECT ctid FROM ekontrak.wf_state WHERE state_id = rec.state_id LOOP
                                IF (counter > 0) THEN
                                    DELETE FROM ekontrak.wf_state WHERE ctid::text = rec2.ctid::text;
                                END IF;
                                counter := counter + 1 ;
                       END LOOP;
             END LOOP;
            ALTER TABLE ekontrak.wf_state ADD CONSTRAINT pk_wf_state PRIMARY KEY (state_id);
        END IF;
    END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table wf_state, scheme ekontrak -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'wf_state_instance' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
             FOR rec IN SELECT state_instance_id FROM ekontrak.wf_state_instance GROUP BY state_instance_id HAVING COUNT(state_instance_id) > 1 ORDER BY state_instance_id LOOP
                       counter := 0;
                       FOR rec2 IN SELECT ctid FROM ekontrak.wf_state_instance WHERE state_instance_id = rec.state_instance_id LOOP
                              IF (counter > 0) THEN
                                   DELETE FROM ekontrak.wf_state_instance WHERE ctid::text = rec2.ctid::text;
                              END IF;
                              counter := counter + 1 ;
                       END LOOP;
             END LOOP;
            ALTER TABLE ekontrak.wf_state_instance ADD CONSTRAINT pk_wf_state_instance PRIMARY KEY (state_instance_id);
        END IF;
    END;
$$;
------------------ end ---------------------------------------------------

-------------------fixing pk table wf_state_mapper, scheme ekontrak -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'wf_state_mapper' and constraint_type = 'PRIMARY KEY' AND constraint_schema = 'ekontrak') then
             FOR rec IN SELECT state_mapper_id FROM ekontrak.wf_state_mapper GROUP BY state_mapper_id HAVING COUNT(state_mapper_id) > 1 ORDER BY state_mapper_id LOOP
                            counter := 0;
                            FOR rec2 IN SELECT ctid FROM ekontrak.wf_state_mapper WHERE state_mapper_id = rec.state_mapper_id LOOP
                                  IF (counter > 0) THEN
                                        DELETE FROM ekontrak.wf_state_mapper WHERE ctid::text = rec2.ctid::text;
                                  END IF;
                                  counter := counter + 1 ;
                           END LOOP;
             END LOOP;
            ALTER TABLE ekontrak.wf_state_mapper ADD CONSTRAINT pk_wf_state_mapper PRIMARY KEY (state_mapper_id);
        END IF;
    END;
$$;
------------------ end ---------------------------------------------------

------------------fixing pk table auditor_skauditor -----------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'auditor_skauditor' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
            FOR rec IN SELECT auditorid, skid, count(*) FROM auditor_skauditor GROUP BY auditorid, skid HAVING count(*) > 1 LOOP
                  counter := 0;
                  FOR rec2 IN SELECT ctid FROM auditor_skauditor WHERE auditorid = rec.auditorid AND skid=rec.skid LOOP
                                    IF (counter > 0) THEN
                                        DELETE FROM auditor_skauditor WHERE ctid::text = rec2.ctid::text;
                                    END IF;
                                    counter := counter + 1;
                  END LOOP;
             END LOOP;
            ALTER TABLE auditor_skauditor ADD PRIMARY KEY (auditorid, skid);
        END IF;
    END;
$$;

-----------------fixing pk table anggaran -------------------------------------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'anggaran' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
            FOR rec IN SELECT ang_id, count(*) FROM anggaran GROUP BY ang_id HAVING count(*) > 1 LOOP
                  counter := 0;
                  FOR rec2 IN SELECT ctid FROM anggaran WHERE ang_id = rec.ang_id LOOP
                                    IF (counter > 0) THEN
                                        DELETE FROM anggaran WHERE ctid::text = rec2.ctid::text;
                                    END IF;
                                    counter := counter + 1;
                  END LOOP;
             END LOOP;
            ALTER TABLE anggaran ADD PRIMARY KEY (ang_id);
        END IF;
    END;
$$;

-----------------fixing pk table blob_table -------------------------------------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
        count INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'blob_table' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
            FOR rec IN SELECT blb_id_content, blb_versi, count(*) FROM blob_table GROUP BY blb_id_content, blb_versi HAVING count(*) > 1 LOOP
                  counter := 0;
                  count := 0;
                  SELECT MAX(blb_versi) INTO count FROM blob_table WHERE blb_id_content = rec.blb_id_content;
                  FOR rec2 IN SELECT ctid FROM blob_table WHERE blb_id_content = rec.blb_id_content AND blb_versi = rec.blb_versi LOOP
                     IF (counter > 0) THEN
                         count := count + 1;
                         UPDATE blob_table SET blb_versi = count WHERE ctid::text = rec2.ctid::text;
                     END IF;
                 END LOOP;
            END LOOP;
        END IF;
    END;
$$;

-----------------fixing pk table peserta_harga -------------------------------------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'peserta_harga' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
            FOR rec IN SELECT psr_id, count(*) FROM peserta_harga GROUP BY psr_id HAVING count(*) > 1 LOOP
                  counter := 0;
                  FOR rec2 IN SELECT ctid FROM peserta_harga WHERE psr_id = rec.psr_id LOOP
                      IF (counter > 0) THEN
                         DELETE FROM peserta_harga WHERE ctid::text = rec2.ctid::text;
                      END IF;
                      counter := counter + 1;
                  END LOOP;
             END LOOP;
            ALTER TABLE peserta_harga ADD PRIMARY KEY (psr_id);
        END IF;
    END;
$$;

-----------------fixing pk table blacklist_checker_history -------------------------------------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'blacklist_checker_history' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
         FOR rec IN SELECT bch_id FROM blacklist_checker_history GROUP BY bch_id HAVING COUNT(bch_id) > 1 ORDER BY bch_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM blacklist_checker_history WHERE bch_id = rec.bch_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM blacklist_checker_history WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
         END LOOP;
        ALTER TABLE blacklist_checker_history ADD PRIMARY KEY (bch_id);
    END IF;
END;
$$;

----------------fixing pk table aktivasi-------------------------------------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'aktivasi' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
         FOR rec IN SELECT rkn_id FROM aktivasi GROUP BY rkn_id HAVING COUNT(rkn_id) > 1 ORDER BY rkn_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM aktivasi WHERE rkn_id = rec.rkn_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM aktivasi WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
         END LOOP;
        ALTER TABLE aktivasi ADD PRIMARY KEY (rkn_id);
    END IF;
END;
$$;

----------------fixing pk table berita_acara-------------------------------------------------
DO LANGUAGE plpgsql $$
DECLARE
	rec RECORD;
	rec2 RECORD;
    counter INTEGER := 0;
BEGIN
    IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'berita_acara' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
         FOR rec IN SELECT brc_id FROM berita_acara GROUP BY brc_id HAVING COUNT(brc_id) > 1 ORDER BY brc_id LOOP
                counter := 0;
                FOR rec2 IN SELECT ctid FROM berita_acara WHERE brc_id = rec.brc_id LOOP
                    IF (counter > 0) THEN
                        DELETE FROM berita_acara WHERE ctid::text = rec2.ctid::text;
                    END IF;
                    counter := counter + 1 ;
                END LOOP;
         END LOOP;
        ALTER TABLE berita_acara ADD PRIMARY KEY (brc_id);
    END IF;
END;
$$;
----------------fixing pk table auditor-------------------------------------------------
DO LANGUAGE plpgsql $$
    DECLARE
        rec RECORD;
        rec2 RECORD;
        counter INTEGER := 0;
    BEGIN
        IF NOT EXISTS (select constraint_name from information_schema.table_constraints where table_name = 'auditor' and constraint_type = 'PRIMARY KEY' and constraint_schema ='public') then
            FOR rec IN SELECT auditorid, count(*) FROM auditor GROUP BY auditorid HAVING count(*) > 1 LOOP
                  counter := 0;
                  FOR rec2 IN SELECT ctid FROM auditor WHERE auditorid = rec.auditorid LOOP
                                    IF (counter > 0) THEN
                                        DELETE FROM auditor WHERE ctid::text = rec2.ctid::text;
                                    END IF;
                                    counter := counter + 1;
                  END LOOP;
             END LOOP;
            ALTER TABLE auditor ADD PRIMARY KEY (auditorid);
        END IF;
    END;
$$;