DROP TABLE IF EXISTS "public"."pokja_";
CREATE TABLE "public"."pokja" (
"pokja_id" numeric(19) NOT NULL,
"agc_id" numeric(19) NOT NULL,
"nama" text COLLATE "default" NOT NULL,
"audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
"audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
"auditupdate" timestamp(6) DEFAULT now() NOT NULL
)
WITH (OIDS=FALSE)

;

CREATE SEQUENCE seq_ukpbj
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_ukpbj OWNER TO epns;