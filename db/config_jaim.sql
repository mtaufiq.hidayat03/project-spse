-- CREATE FIELD "sp_id" ----------------------------------------
ALTER TABLE "public"."update_schedule" ADD COLUMN "sp_id" Integer;
-- -------------------------------------------------------------

INSERT INTO "public"."configuration"(
    "cfg_category",
    "cfg_sub_category",
    "audittype",
    "audituser",
    "auditupdate",
    "cfg_value",
    "cfg_comment"
) VALUES (
    'CONFIG',
    'ppe.versi',
    'C',
    'ADMIN',
    '2018-04-16 11:39:35.639327',
    'spse-4.2',
    'Versi SPSE yang digunakan. Ini harus disesuaikan dengan JAIM agar notifikasi update dapat langsung muncul'
 );