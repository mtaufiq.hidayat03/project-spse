BEGIN;

-- CREATE SEQUENCE "seq_persetujuan" ---------------------------
CREATE SEQUENCE "ekontrak"."seq_persetujuan"
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 13640
CACHE 1;
-- -------------------------------------------------------------

-- CHANGE "CURRENT VALUE" OF "SEQUENCE "seq_persetujuan" -------
ALTER SEQUENCE "ekontrak"."seq_persetujuan" RESTART 13718;
-- -------------------------------------------------------------

COMMIT;
