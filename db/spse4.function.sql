CREATE OR REPLACE FUNCTION tahap_now (id numeric, date timestamp without time zone) RETURNS character varying LANGUAGE 'plpgsql' AS
$body$
	DECLARE
	tahap character varying;
	awal timestamp without time zone;
	akhir timestamp without time zone;
BEGIN
   SELECT array_to_string(array_agg(a.akt_jenis), ',') INTO tahap FROM jadwal j, aktivitas a WHERE j.akt_id=a.akt_id AND j.lls_id= $1 AND dtj_tglawal <= $2 AND dtj_tglakhir >= $2;
   IF tahap IS NULL THEN
	SELECT dtj_tglawal INTO awal FROM jadwal WHERE lls_id=$1 ORDER BY dtj_tglawal ASC LIMIT 1;
	SELECT dtj_tglakhir INTO akhir FROM jadwal WHERE lls_id=$1 ORDER BY dtj_tglakhir DESC LIMIT 1;
	IF awal IS NOT NULL AND awal > $2 THEN
		tahap := 'BELUM_DILAKSANAKAN';
	ELSIF akhir IS NOT NULL AND akhir < $2 THEN
		tahap := 'SUDAH_SELESAI';
	ELSE
		tahap := 'TIDAK_ADA_JADWAL';
	END IF;
   END IF;
   RETURN tahap;
END;
$body$;

ALTER FUNCTION tahap_now(numeric, timestamp without time zone) OWNER TO epns;

CREATE OR REPLACE FUNCTION ekontrak.tahap_now(id numeric,  date timestamp without time zone) RETURNS character varying LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
	tahap character varying;
	awal timestamp without time zone;
	akhir timestamp without time zone;
BEGIN
   SELECT array_to_string(array_agg(a.akt_jenis), ',') INTO tahap FROM ekontrak.jadwal j, ekontrak.aktivitas_pl a WHERE j.akt_id=a.akt_id AND j.lls_id= $1 AND dtj_tglawal <= $2 AND dtj_tglakhir >= $2;
   IF tahap IS NULL THEN
	SELECT dtj_tglawal INTO awal FROM ekontrak.jadwal WHERE lls_id=$1 ORDER BY dtj_tglawal ASC LIMIT 1;
	SELECT dtj_tglakhir INTO akhir FROM ekontrak.jadwal WHERE lls_id=$1 ORDER BY dtj_tglakhir DESC LIMIT 1;
	IF awal IS NOT NULL AND awal > $2 THEN
		tahap := 'PAKET_BELUM_DILAKSANAKAN';
	ELSIF akhir IS NOT NULL AND akhir < $2 THEN
		tahap := 'PAKET_SUDAH_SELESAI';
	ELSE
		tahap := 'TIDAK_ADA_JADWAL';
	END IF;
   END IF;
   RETURN tahap;
END;
$BODY$;
ALTER FUNCTION ekontrak.tahap_now(numeric, timestamp without time zone)  OWNER TO epns;

CREATE OR REPLACE FUNCTION lastversioncertificate(cer_id numeric) RETURNS numeric LANGUAGE 'sql' AS
$BODY$
 SELECT cer_versi FROM certificate  WHERE cer_id = $1 AND cer_versi = (SELECT max(cer_versi) FROM certificate WHERE cer_id=$1)
$BODY$;
ALTER FUNCTION lastversioncertificate(numeric) OWNER TO epns;
COMMENT ON FUNCTION lastversioncertificate(numeric) IS 'this is function to get last version of certificate';

CREATE OR REPLACE FUNCTION paket_instansi(paketid numeric) RETURNS text LANGUAGE 'plpgsql' AS
$body$
DECLARE
	namaInstansi text;
BEGIN
   SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') into namaInstansi FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja s, paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=$1);
   IF namaInstansi IS NULL THEN
		SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct agc_nama), ', ') into namaInstansi FROM agency WHERE agc_id in (SELECT agc_id FROM satuan_kerja s, paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=$1);
   END IF;

   RETURN namaInstansi;
END;
$body$;
ALTER FUNCTION paket_instansi(numeric) OWNER TO epns;

CREATE OR REPLACE FUNCTION paket_satker(paketid numeric) RETURNS text LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
	namaSatker text;
BEGIN
   SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') into namaSatker  FROM satuan_kerja s, paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=$1;
   RETURN namaSatker;
END;
$BODY$;
ALTER FUNCTION paket_satker(numeric) OWNER TO epns;

CREATE OR REPLACE FUNCTION ekontrak.paket_instansi(paketid numeric) RETURNS text LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
	namaInstansi text;
BEGIN
   SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') into namaInstansi FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=$1);
   IF namaInstansi IS NULL THEN
		SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct agc_nama), ', ') into namaInstansi FROM agency WHERE agc_id in (SELECT agc_id FROM satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=$1);
   END IF;
   RETURN namaInstansi;
END;
$BODY$;
ALTER FUNCTION ekontrak.paket_instansi(numeric) OWNER TO epns;

CREATE OR REPLACE FUNCTION ekontrak.paket_satker(paketid numeric)  RETURNS text LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
	namaSatker text;
BEGIN
   SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') into namaSatker  FROM satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=$1;
   RETURN namaSatker;
END;
$BODY$;
ALTER FUNCTION ekontrak.paket_satker(numeric) OWNER TO epns;

CREATE OR REPLACE FUNCTION update_lelang_query() RETURNS TRIGGER AS $$
    DECLARE
        status numeric;
    BEGIN
        SELECT lls_status INTO status FROM lelang_seleksi WHERE lls_id = NEW.lls_id;
        IF (status = 1) THEN
			 DELETE FROM lelang_query WHERE lls_id = NEW.lls_id AND lls_status = 1;
             INSERT INTO lelang_query SELECT l.lls_id,  p.pkt_id, pkt_nama, pkt_flag, pkt_hps, pkt_hps_enable, mtd_id, mtd_pemilihan, mtd_evaluasi, kgr_id, paket_instansi(p.pkt_id) as nama_instansi,lls_penawaran_ulang, lls_evaluasi_ulang,
			 (SELECT kontrak_id FROM kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_id,(SELECT kontrak_nilai FROM kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_nilai, p.is_pkt_konsolidasi,
			 (SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, lls_versi_lelang, (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) as eva_versi,
			 l.lls_status,  (SELECT dtj_tglawal FROM jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglawal ASC LIMIT 1) as jadwal_awal_pengumuman, (SELECT dtj_tglakhir FROM jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglakhir DESC LIMIT 1) as jadwal_akhir,
			 (SELECT dtj_tglakhir FROM jadwal j, aktivitas a WHERE lls_id=l.lls_id AND j.akt_id=a.akt_id AND akt_urut=2 LIMIT 1) as tgl_akhir_daftar,
			 setweight(to_tsvector('english',coalesce(pkt_nama,'')), 'A') || setweight(to_tsvector('english',coalesce(paket_instansi(p.pkt_id),'')), 'B') FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id WHERE lls_status=1 AND lls_id=NEW.lls_id;
        ELSE
            DELETE FROM lelang_query WHERE lls_id=NEW.lls_id;
        END IF;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION public.update_lelang_query() OWNER TO epns;
DROP TRIGGER IF EXISTS update_lelang_query ON public.lelang_seleksi;
CREATE TRIGGER update_lelang_query AFTER INSERT OR UPDATE ON lelang_seleksi FOR EACH ROW EXECUTE PROCEDURE update_lelang_query();
DROP TRIGGER IF EXISTS update_lelang_query_evaluasi ON public.evaluasi;
CREATE TRIGGER update_lelang_query_evaluasi AFTER INSERT OR UPDATE ON evaluasi FOR EACH ROW EXECUTE PROCEDURE update_lelang_query();
DROP TRIGGER IF EXISTS update_lelang_query_kontrak ON public.kontrak;
CREATE TRIGGER update_lelang_query_kontrak AFTER INSERT OR UPDATE ON kontrak FOR EACH ROW EXECUTE PROCEDURE update_lelang_query();
DROP TRIGGER IF EXISTS update_lelang_query_jadwal ON public.jadwal;
CREATE TRIGGER update_lelang_query_jadwal AFTER INSERT OR UPDATE ON jadwal FOR EACH ROW EXECUTE PROCEDURE update_lelang_query();

CREATE OR REPLACE FUNCTION ekontrak.update_lelang_query() RETURNS TRIGGER AS $$
    DECLARE
        status numeric;
    BEGIN
        SELECT lls_status INTO status FROM ekontrak.nonlelang_seleksi WHERE lls_id = NEW.lls_id;
        IF (status = 1) THEN
			 DELETE FROM ekontrak.lelang_query WHERE lls_id = NEW.lls_id;
             INSERT INTO ekontrak.lelang_query SELECT l.lls_id,  p.pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_pemilihan, mtd_evaluasi, kgr_id, ekontrak.paket_instansi(p.pkt_id) as nama_instansi,
            (SELECT kontrak_id FROM ekontrak.kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_id,
            (SELECT kontrak_nilai FROM ekontrak.kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_nilai, p.is_pkt_konsolidasi,
            (SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, ekontrak.Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, lls_versi_lelang,
            (SELECT MAX(eva_versi) FROM ekontrak.evaluasi WHERE lls_id=l.lls_id) as eva_versi, l.lls_status, (SELECT dtj_tglawal FROM ekontrak.jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglawal ASC LIMIT 1) as jadwal_awal_pengumuman,
            (SELECT dtj_tglakhir FROM ekontrak.jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglakhir DESC LIMIT 1) as jadwal_akhir, (SELECT dtj_tglakhir FROM ekontrak.jadwal WHERE lls_id=l.lls_id AND thp_id =18833  LIMIT 1) as tgl_akhir_daftar,
            setweight(to_tsvector('english',coalesce(pkt_nama,'')), 'A') || setweight(to_tsvector('english',coalesce(ekontrak.paket_instansi(p.pkt_id),'')), 'B')
            FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id WHERE lls_status=1 AND lls_id=NEW.lls_id;
        ELSE
            DELETE FROM ekontrak.lelang_query WHERE lls_id=NEW.lls_id;
        END IF;
        RETURN NULL;
    END;
$$ LANGUAGE plpgsql;

ALTER FUNCTION ekontrak.update_lelang_query() OWNER TO epns;
DROP TRIGGER IF EXISTS update_lelang_query ON ekontrak.nonlelang_seleksi;
CREATE TRIGGER update_lelang_query AFTER INSERT OR UPDATE ON ekontrak.nonlelang_seleksi FOR EACH ROW EXECUTE PROCEDURE ekontrak.update_lelang_query();
DROP TRIGGER IF EXISTS update_lelang_query_evaluasi ON ekontrak.evaluasi;
CREATE TRIGGER update_lelang_query_evaluasi AFTER INSERT OR UPDATE ON ekontrak.evaluasi FOR EACH ROW EXECUTE PROCEDURE ekontrak.update_lelang_query();
DROP TRIGGER IF EXISTS update_lelang_query_kontrak ON ekontrak.kontrak;
CREATE TRIGGER update_lelang_query_kontrak AFTER INSERT OR UPDATE ON ekontrak.kontrak FOR EACH ROW EXECUTE PROCEDURE ekontrak.update_lelang_query();
DROP TRIGGER IF EXISTS update_lelang_query_jadwal ON ekontrak.jadwal;
CREATE TRIGGER update_lelang_query_jadwal AFTER INSERT OR UPDATE ON ekontrak.jadwal FOR EACH ROW EXECUTE PROCEDURE ekontrak.update_lelang_query();

CREATE OR REPLACE FUNCTION ekontrak.paket_instansi_swakelola(paketid numeric) RETURNS text LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
	namaInstansi text;
BEGIN
   SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') into namaInstansi FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=$1);
   IF namaInstansi IS NULL THEN
		SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct agc_nama), ', ') into namaInstansi FROM agency WHERE agc_id in (SELECT agc_id FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=$1);
   END IF;
   RETURN namaInstansi;
END;
$BODY$;
ALTER FUNCTION ekontrak.paket_instansi_swakelola(numeric) OWNER TO epns;

CREATE OR REPLACE FUNCTION ekontrak.paket_satker_swakelola(paketid numeric)  RETURNS text LANGUAGE 'plpgsql' AS
$BODY$
DECLARE
	namaSatker text;
BEGIN
   SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') into namaSatker  FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=$1;
   RETURN namaSatker;
END;
$BODY$;
ALTER FUNCTION ekontrak.paket_satker_swakelola(numeric) OWNER TO epns;