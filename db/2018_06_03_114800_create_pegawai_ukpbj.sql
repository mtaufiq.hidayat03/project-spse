DROP TABLE IF EXISTS "public"."pegawai_ukpbj";
CREATE TABLE "public"."pegawai_ukpbj"
(
  ukpbj_id numeric(19,0) NOT NULL,
  audittype character(1) NOT NULL DEFAULT 'C'::bpchar,
  audituser character varying(100) NOT NULL DEFAULT 'ADMIN'::character varying,
  auditupdate timestamp(6) without time zone NOT NULL DEFAULT now(),
  peg_id numeric(19,0) NOT NULL,
  CONSTRAINT pegawai_ukpbj_pkey PRIMARY KEY (ukpbj_id, peg_id),
  CONSTRAINT pegawai_ukpbj_peg_id_fkey FOREIGN KEY (peg_id)
      REFERENCES pegawai (peg_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);

ALTER TABLE "public"."pegawai_ukpbj" OWNER TO epns;