DROP TABLE IF EXISTS "public"."pokja_anggota";
CREATE TABLE "public"."pokja_anggota" (
"pokja_id" numeric(19) NOT NULL,
"peg_id" numeric(19) NOT NULL,
"audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
"audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
"auditupdate" timestamp(6) DEFAULT now() NOT NULL,
"asign_date" timestamp(6) DEFAULT now() NOT NULL,
"designate" numeric(19) NOT NULL,
"is_active" numeric(19) NOT NULL
)
WITH (OIDS=FALSE)

;