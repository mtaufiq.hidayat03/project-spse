BEGIN;

-- CREATE TABLE "riwayat_persetujuan" --------------------------
CREATE TABLE "ekontrak"."riwayat_persetujuan" ( 
	"rwt_pst_id" Bigint NOT NULL,
	"audittype" Character Varying( 1 ),
	"auditupdate" Timestamp( 6 ) Without Time Zone,
	"audituser" Character Varying( 100 ),
	"pst_alasan" Text,
	"pst_jenis" Integer,
	"pst_status" Integer,
	"pst_tgl_setuju" Timestamp( 6 ) Without Time Zone,
	"lls_id" Bigint,
	"peg_id" Bigint );
 ;
-- -------------------------------------------------------------

COMMIT;
