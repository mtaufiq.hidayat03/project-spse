alter table ekontrak.paket_anggaran add column ppk_jabatan numeric(1,0) DEFAULT NULL::numeric;
alter table ekontrak.paket_anggaran add column rup_id numeric(64,0) DEFAULT NULL::numeric;

update ekontrak.paket_anggaran set ppk_jabatan=0;
update ekontrak.paket_anggaran pa set rup_id=(SELECT distinct rup.id FROM rup_paket AS rup inner JOIN ekontrak.paket_satker AS ps ON rup.id=ps.rup_id WHERE ps.pkt_id=pa.pkt_id limit 1);
