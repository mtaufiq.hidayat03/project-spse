CREATE TABLE "public"."update_schedule" ( 
	"id" Integer NOT NULL,
	"update_date" Timestamp Without Time Zone,
	"executed" Boolean NOT NULL,
	PRIMARY KEY ( "id" ),
	CONSTRAINT "unique_id" UNIQUE( "id" ) );