-- TAMBAH KOLOM sudah_kirim_pengumuman DI public.sppbj
ALTER TABLE "public"."sppbj" ADD sudah_kirim_pengumuman Boolean;

-- TAMBAH KOLOM sudah_kirim_pengumuman DI ekontrak.sppbj
ALTER TABLE "ekontrak"."sppbj" ADD sudah_kirim_pengumuman Boolean;
