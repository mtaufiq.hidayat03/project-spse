BEGIN;

-- CREATE TABLE "persetujuan" ----------------------------------
CREATE TABLE "ekontrak"."persetujuan" ( 
	"pst_id" Bigint NOT NULL,
	"audittype" Character Varying( 1 ),
	"auditupdate" Timestamp( 6 ) Without Time Zone,
	"audituser" Character Varying( 100 ),
	"pst_alasan" Text,
	"pst_jenis" Integer,
	"pst_status" Integer,
	"pst_tgl_setuju" Timestamp( 6 ) Without Time Zone,
	"lls_id" Bigint,
	"peg_id" Bigint,
	PRIMARY KEY ( "pst_id" ) );
 ;
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_status" -----------------------
CREATE INDEX "idx_persetujuan_status" ON "ekontrak"."persetujuan" USING btree( "pst_status" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_pegawai" ----------------------
CREATE INDEX "idx_persetujuan_pegawai" ON "ekontrak"."persetujuan" USING btree( "peg_id" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_lelang" -----------------------
CREATE INDEX "idx_persetujuan_lelang" ON "ekontrak"."persetujuan" USING btree( "lls_id" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_jenis" ------------------------
CREATE INDEX "idx_persetujuan_jenis" ON "ekontrak"."persetujuan" USING btree( "pst_jenis" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_auditupdate" ------------------
CREATE INDEX "idx_persetujuan_auditupdate" ON "ekontrak"."persetujuan" USING btree( "auditupdate" Asc NULLS Last );
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "persetujuan_lls_id_fkey" -----------------------
ALTER TABLE "ekontrak"."persetujuan"
	ADD CONSTRAINT "persetujuan_lls_id_fkey" FOREIGN KEY ( "lls_id" )
	REFERENCES "ekontrak"."nonlelang_seleksi" ( "lls_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE LINK "persetujuan_peg_id_fkey" -----------------------
ALTER TABLE "ekontrak"."persetujuan"
	ADD CONSTRAINT "persetujuan_peg_id_fkey" FOREIGN KEY ( "peg_id" )
	REFERENCES "public"."pegawai" ( "peg_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;
-- -------------------------------------------------------------

COMMIT;
