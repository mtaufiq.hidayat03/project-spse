CREATE TABLE "public"."ukpbj" (
"ukpbj_id" numeric(19) NOT NULL,
"agc_id" numeric(19) NOT NULL,
"kpl_unit_pemilihan_id" numeric(19) NOT NULL,
"audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
"audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
"auditupdate" timestamp(6) DEFAULT now() NOT NULL,
"nama" text COLLATE "default" NOT NULL,
"alamat" text COLLATE "default" NOT NULL,
"telepon" text COLLATE "default" NOT NULL,
"fax" text COLLATE "default",
"tgl_daftar" timestamp(6) NOT NULL,
"is_lpse" int4 DEFAULT 0 NOT NULL
);
alter table "public"."ukpbj" owner to epns;
CREATE SEQUENCE seq_ukpbj START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;
ALTER TABLE seq_ukpbj OWNER TO epns;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Indexes structure for table ukpbj
-- ----------------------------
CREATE INDEX "idx_ukpbj_auditupdate" ON "public"."ukpbj" USING btree ("auditupdate");
CREATE INDEX "ind_ukpbj_nama" ON "public"."ukpbj" USING btree ("nama");

-- ----------------------------
-- Primary Key structure for table ukpbj
-- ----------------------------
ALTER TABLE "public"."ukpbj" ADD PRIMARY KEY ("ukpbj_id");

-- ----------------------------
-- Foreign Key structure for table "public"."ukpbj"
-- ----------------------------
ALTER TABLE "public"."ukpbj" ADD FOREIGN KEY ("ukpbj_id") REFERENCES "public"."ukpbj" ("ukpbj_id") ON DELETE RESTRICT ON UPDATE CASCADE;

--- tambah ams_id di pegawai dan rekanan ------
ALTER TABLE "public"."pegawai" ADD ams_id character varying;
ALTER TABLE "public"."rekanan" ADD ams_id character varying;

-- tambah ukpbj_id di Pegawai, Panitia, Paket
ALTER TABLE "public"."paket" ADD ukpbj_id numeric(18,2) DEFAULT NULL::numeric;
ALTER TABLE "public"."panitia" ADD ukpbj_id numeric(18,2) DEFAULT NULL::numeric;
ALTER TABLE "public"."pegawai" ADD ukpbj_id numeric(18,2) DEFAULT NULL::numeric;

--- tahap lelang pasca 2 file Kualitas (konsultan perorangan) Perpres 16/2018  ----
DELETE FROM aktivitas WHERE mtd_id='53';
INSERT INTO aktivitas VALUES (275, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PENGUMUMAN_LELANG', '1', '1');
INSERT INTO aktivitas VALUES (276, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'AMBIL_DOKUMEN', '2', '1');
INSERT INTO aktivitas VALUES (277, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PENJELASAN', '3', '1');
INSERT INTO aktivitas VALUES (278, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PEMASUKAN_PENAWARAN', '4', '1');
INSERT INTO aktivitas VALUES (279, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', '5', '1');
INSERT INTO aktivitas VALUES (280, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'EVALUASI_DOK_PRA', '6', '1');
INSERT INTO aktivitas VALUES (281, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'VERIFIKASI_KUALIFIKASI', '7', '1');
INSERT INTO aktivitas VALUES (282, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PENGUMUMAN_PEMENANG_ADM_TEKNIS', '8', '1');
INSERT INTO aktivitas VALUES (283, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'SANGGAH', '9', '1');
INSERT INTO aktivitas VALUES (284, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA', '10', '1');
INSERT INTO aktivitas VALUES (285, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'KLARIFIKASI_NEGOSIASI', '11', '1');
INSERT INTO aktivitas VALUES (286, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'UPLOAD_BA_HASIL_LELANG', '12', '1');
INSERT INTO aktivitas VALUES (287, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PENETAPAN_PEMENANG_AKHIR', '13', '1');
INSERT INTO aktivitas VALUES (288, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PENGUMUMAN_PEMENANG_AKHIR', '14', '1');
INSERT INTO aktivitas VALUES (289, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'PENUNJUKAN_PEMENANG', '15', '1');
INSERT INTO aktivitas VALUES (290, '53', 'C', 'ADMIN', '2018-05-31 09:00:00', 'TANDATANGAN_KONTRAK', '16', '1');

-- Tabel baru pegawai_ukpbj
CREATE TABLE "public"."pegawai_ukpbj"
(
  ukpbj_id numeric(19,0) NOT NULL,
  audittype character(1) NOT NULL DEFAULT 'C'::bpchar,
  audituser character varying(100) NOT NULL DEFAULT 'ADMIN'::character varying,
  auditupdate timestamp(6) without time zone NOT NULL DEFAULT now(),
  peg_id numeric(19,0) NOT NULL,
  CONSTRAINT pegawai_ukpbj_pkey PRIMARY KEY (ukpbj_id, peg_id),
  CONSTRAINT pegawai_ukpbj_peg_id_fkey FOREIGN KEY (peg_id)
      REFERENCES pegawai (peg_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
);

ALTER TABLE "public"."pegawai_ukpbj" OWNER TO epns;

-- Kolom changelogs di update_schedule
ALTER TABLE "public"."update_schedule" ADD COLUMN "changelogs" text;
ALTER TABLE "public"."update_schedule" ADD COLUMN "spse_version" Character( 255 );

-- Tabel dok_persiapan
CREATE TABLE "public"."dok_persiapan" (
  "dp_id" numeric(19) NOT NULL,
  "pkt_id" numeric(19) NOT NULL,
  "audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
  "audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
  "auditupdate" timestamp(6) DEFAULT now() NOT NULL,
  "dp_versi" smallint DEFAULT 1 NOT NULL,
  "dp_dkh" text,
  "dp_spek" numeric( 19, 0 ),
  "dp_sskk" text,
  "dp_modified" Boolean DEFAULT false,
  PRIMARY KEY ( "dp_id", "dp_versi" )
);
ALTER TABLE dok_persiapan OWNER TO epns;

CREATE SEQUENCE seq_dok_persiapan
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE seq_dok_persiapan OWNER TO epns;

CREATE INDEX "idx_dok_persiapan_auditupdate" ON "public"."dok_persiapan" USING btree ("auditupdate");

ALTER TABLE "public"."dok_persiapan" ADD FOREIGN KEY ("pkt_id") REFERENCES "public"."paket" ("pkt_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- Create table ekontrak.paket_ppk
CREATE TABLE ekontrak.paket_ppk
(
    pkt_id numeric NOT NULL,
    ppk_id numeric NOT NULL,
    audittype character(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 'C'::bpchar,
    audituser character varying(100) COLLATE pg_catalog."default" NOT NULL DEFAULT 'ADMIN'::character varying,
    auditupdate timestamp without time zone NOT NULL DEFAULT now(),
    ppk_jabatan numeric,
    CONSTRAINT pk_paket_ppk PRIMARY KEY (pkt_id, ppk_id),
    CONSTRAINT fk_paket_ppk_ppk FOREIGN KEY (ppk_id)
        REFERENCES public.ppk (ppk_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

ALTER TABLE ekontrak.paket_ppk OWNER to epns;

-- Index: idx_paket_ppk_auditupdate

-- DROP INDEX ekontrak.idx_paket_ppk_auditupdate;

CREATE INDEX idx_paket_ppk_auditupdate ON ekontrak.paket_ppk USING btree(auditupdate);

ALTER TABLE ekontrak.paket ADD COLUMN pkt_hps_enable boolean DEFAULT true;

ALTER TABLE ekontrak.paket ADD COLUMN ukpbj_id numeric(18, 2) DEFAULT NULL::numeric;

--Alter Table history_paket_ppk
ALTER TABLE history_paket_ppk ADD COLUMN "alasan" text;

-- aktifin tahap sanggah prakualifikasi
UPDATE aktivitas SET akt_status = 1 WHERE akt_jenis = 'SANGGAH_PRA';

-- table paket sirup 2.3
CREATE TABLE public.paket_sirup
(
    id numeric NOT NULL,
    nama text NOT NULL,
    paket_lokasi jsonb,
    volume text,
    keterangan text,
    spesifikasi text,
    is_tkdn boolean,
    is_pradipa boolean,
    paket_anggaran jsonb,
    pagu numeric NOT NULL,
    paket_jenis jsonb,
    metode_pengadaan integer NOT NULL,
    tanggal_kebutuhan date,
    tanggal_awal_pengadaan date,
    tanggal_akhir_pengadaan date,
    tanggal_awal_pekerjaan date,
    tanggal_akhir_pekerjaan date,
    CONSTRAINT pk_paket_sirup PRIMARY KEY (id)
);

ALTER TABLE public.paket_sirup OWNER to epns;

-- table dok_persiapan di ekontrak
CREATE TABLE "ekontrak"."dok_persiapan" (
  "dp_id" numeric(19) NOT NULL,
  "pkt_id" numeric(19) NOT NULL,
  "audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
  "audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
  "auditupdate" timestamp(6) DEFAULT now() NOT NULL,
  "dp_versi" smallint DEFAULT 1 NOT NULL,
  "dp_dkh" text,
  "dp_spek" numeric( 19, 0 ),
  "dp_sskk" text,
  "dp_modified" Boolean DEFAULT false,
  PRIMARY KEY ( "dp_id", "dp_versi" )
);
ALTER TABLE "ekontrak"."dok_persiapan" OWNER TO epns;

CREATE SEQUENCE "ekontrak"."seq_dok_persiapan"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE "ekontrak"."seq_dok_persiapan" OWNER TO epns;

CREATE INDEX "idx_dok_persiapan_auditupdate" ON "ekontrak"."dok_persiapan" USING btree ("auditupdate");

ALTER TABLE "ekontrak"."dok_persiapan" ADD FOREIGN KEY ("pkt_id") REFERENCES "ekontrak"."paket" ("pkt_id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- Kolom baru di peserta untuk fitur upload dokumen kualifikasi dan evaluasi kualifikasi tambahan
ALTER TABLE "public"."peserta" ADD COLUMN is_dikirim_pesan boolean DEFAULT false;
ALTER TABLE "public"."peserta" ADD COLUMN sudah_evaluasi_kualifikasi boolean DEFAULT false;

-- Kolom baru di lelang_seleksi untuk fitur upload dokumen kualifikasi dan evaluasi kualifikasi tambahan
ALTER TABLE "public"."lelang_seleksi" ADD COLUMN is_kualifikasi_tambahan boolean DEFAULT false;

--Kolom baru di draf_peserta_nonlelang untuk fitur repo lpse.
ALTER TABLE ekontrak.DRAFT_PESERTA_NONLELANG ADD COLUMN repo_id numeric;

-- kolom baru rup_stk_id , tahun (tahun anggaran) ------
ALTER TABLE public.paket_sirup ADD COLUMN rup_stk_id text;
ALTER TABLE public.paket_sirup ADD COLUMN tahun numeric;

-- kolom baru untuk fitur sskk upload ------
ALTER TABLE public.dok_persiapan ADD COLUMN dp_sskk_attachment numeric(19,0);
ALTER TABLE public.dok_lelang_content ADD COLUMN dll_sskk_attachment numeric(19,0);

-- kolom baru untuk fitur sskk upload non tender------
ALTER TABLE ekontrak.dok_persiapan ADD COLUMN dp_sskk_attachment numeric(19,0);
ALTER TABLE ekontrak.dok_nonlelang_content ADD COLUMN dll_sskk_attachment numeric(19,0);

--- tambah master workflow PASCA_DUA_FILE_KUALITAS -----
INSERT INTO wf_workflow (workflow_id, audittype, auditupdate, audituser, createdate, name) VALUES
('PASCA_DUA_FILE_KUALITAS', 'C', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', 'Pascakualifikasi 2 sampull kualitas');

INSERT INTO wf_process_definition (process_definition_id, audittype, auditupdate, audituser, createdate, status, version, workflow_id, jsonvariable) VALUES
(13, 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', 1, 1, 'PASCA_DUA_FILE_KUALITAS', '{"EVALUASI_ULANG":{"name":"EVALUASI_ULANG","required":false},"NOW":{"name":"NOW","required":false},"PEMASUKAN_PENAWARAN_ULANG":{"name":"PEMASUKAN_PENAWARAN_ULANG","required":false},"LELANG_GAGAL":{"name":"LELANG_GAGAL","required":false},"LELANG_ULANG":{"name":"LELANG_ULANG","required":false},"PESERTA":{"name":"PESERTA","required":false},"LELANG_SELESAI":{"name":"LELANG_SELESAI","required":false}}');

INSERT INTO wf_state (state_id, audittype, auditupdate, audituser, createdate, description, fork, name, process_definition_id) VALUES
('13.START', 'C', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'START', 13),
('13.PENGUMUMAN_LELANG', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'PENGUMUMAN_LELANG', 13),
('13.AMBIL_DOKUMEN', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'AMBIL_DOKUMEN', 13),
('13.PENJELASAN', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PENJELASAN', 13),
('13.PEMBUKAAN_PENAWARAN', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'PEMBUKAAN_PENAWARAN', 13),
('13.EVALUASI_DOK_PRA', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'EVALUASI_DOK_PRA', 13),
('13.VERIFIKASI_KUALIFIKASI', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'VERIFIKASI_KUALIFIKASI', 13),
('13.PENGUMUMAN_PEMENANG_ADM_TEKNIS', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PENGUMUMAN_PEMENANG_ADM_TEKNIS', 13),
('13.PENETAPAN_PEMENANG_AKHIR', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PENETAPAN_PEMENANG_AKHIR', 13),
('13.PENGUMUMAN_PEMENANG_AKHIR', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PENGUMUMAN_PEMENANG_AKHIR', 13),
('13.SANGGAH', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'SANGGAH', 13),
('13.KLARIFIKASI_NEGOSIASI', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'KLARIFIKASI_NEGOSIASI', 13),
('13.UPLOAD_BA_HASIL_LELANG', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'UPLOAD_BA_HASIL_LELANG', 13),
('13.PENUNJUKAN_PEMENANG', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PENUNJUKAN_PEMENANG', 13),
('13.TANDATANGAN_KONTRAK', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'TANDATANGAN_KONTRAK', 13),
('13.GAGAL', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, true, 'GAGAL', 13),
('13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 13),
('13.PEMASUKAN_PENAWARAN', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PEMASUKAN_PENAWARAN', 13),
('13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA', 13),
('13.END', 'U', '2018-08-28 17:53:29.496', 'N/A', '2018-08-28 17:53:29.496', NULL, false, 'END', 13);

INSERT INTO wf_state_mapper (state_mapper_id, audittype, auditupdate, audituser, createdate, condition, postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(469, 'C', '2015-01-21 10:50:01.212', 'N/A', '2015-01-21 10:50:01.208', 'NOW', NULL, '13.START', '13.PENGUMUMAN_LELANG', 13),
(471, 'C', '2015-01-21 10:50:01.228', 'N/A', '2015-01-21 10:50:01.223', 'NOW', NULL, '13.PENGUMUMAN_LELANG', '13.AMBIL_DOKUMEN', 13),
(472, 'C', '2015-01-21 10:50:01.242', 'N/A', '2015-01-21 10:50:01.238', 'NOW', NULL, '13.PENGUMUMAN_LELANG', '13.PENJELASAN', 13),
(473, 'C', '2015-01-21 10:50:01.258', 'N/A', '2015-01-21 10:50:01.254', 'NOW', NULL, '13.PENGUMUMAN_LELANG', '13.PEMASUKAN_PENAWARAN', 13),
(474, 'C', '2015-01-21 10:50:01.271', 'N/A', '2015-01-21 10:50:01.268', 'NOW', NULL, '13.AMBIL_DOKUMEN', '13.PENJELASAN', 13),
(475, 'C', '2015-01-21 10:50:01.285', 'N/A', '2015-01-21 10:50:01.281', 'NOW', NULL, '13.AMBIL_DOKUMEN', '13.PEMASUKAN_PENAWARAN', 13),
(476, 'C', '2015-01-21 10:50:01.303', 'N/A', '2015-01-21 10:50:01.298', 'NOW', NULL, '13.PENJELASAN', '13.PEMASUKAN_PENAWARAN', 13),
(477, 'C', '2015-01-21 10:50:01.332', 'N/A', '2015-01-21 10:50:01.327', 'NOW && (PESERTA >= 3)', NULL, '13.PEMASUKAN_PENAWARAN', '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 13),
(478, 'C', '2015-01-21 10:50:01.347', 'N/A', '2015-01-21 10:50:01.343', 'NOW', NULL, '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 13),
(479, 'C', '2015-01-21 10:50:01.362', 'N/A', '2015-01-21 10:50:01.359', 'LELANG_GAGAL', NULL, '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', '13.GAGAL', 13),
(480, 'C', '2015-01-21 10:50:01.374', 'N/A', '2015-01-21 10:50:01.371', 'PEMASUKAN_PENAWARAN_ULANG', NULL, '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', '13.PEMASUKAN_PENAWARAN', 13),
(481, 'C', '2015-01-21 10:50:01.386', 'N/A', '2015-01-21 10:50:01.382', 'NOW', NULL, '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', '13.EVALUASI_DOK_PRA', 13),
(482, 'C', '2015-01-21 10:50:01.399', 'N/A', '2015-01-21 10:50:01.395', 'NOW', NULL, '13.EVALUASI_DOK_PRA', '13.VERIFIKASI_KUALIFIKASI', 13),
(483, 'C', '2015-01-21 10:50:01.411', 'N/A', '2015-01-21 10:50:01.409', 'LELANG_GAGAL', NULL, '13.EVALUASI_DOK_PRA', '13.GAGAL', 13),
(484, 'C', '2015-01-21 10:50:01.425', 'N/A', '2015-01-21 10:50:01.423', 'LELANG_GAGAL', NULL, '13.VERIFIKASI_KUALIFIKASI', '13.GAGAL', 13),
(485, 'C', '2015-01-21 10:50:01.44', 'N/A', '2015-01-21 10:50:01.436', 'NOW', NULL, '13.VERIFIKASI_KUALIFIKASI', '13.PENGUMUMAN_PEMENANG_ADM_TEKNIS', 13),
(486, 'C', '2015-01-21 10:50:01.459', 'N/A', '2015-01-21 10:50:01.455', 'NOW', NULL, '13.PENGUMUMAN_PEMENANG_ADM_TEKNIS', '13.SANGGAH', 13),
(487, 'C', '2015-01-21 10:50:01.473', 'N/A', '2015-01-21 10:50:01.47', 'NOW', NULL, '13.PENETAPAN_PEMENANG_AKHIR', '13.PENGUMUMAN_PEMENANG_AKHIR', 13),
(488, 'C', '2015-01-21 10:50:01.496', 'N/A', '2015-01-21 10:50:01.491', 'NOW', NULL, '13.PENGUMUMAN_PEMENANG_AKHIR', '13.PENUNJUKAN_PEMENANG', 13),
(489, 'C', '2015-01-21 10:50:01.511', 'N/A', '2015-01-21 10:50:01.508', 'LELANG_GAGAL', NULL, '13.SANGGAH', '13.GAGAL', 13),
(490, 'C', '2015-01-21 10:50:01.525', 'N/A', '2015-01-21 10:50:01.521', 'NOW', NULL, '13.SANGGAH', '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA', 13),
(491, 'C', '2015-01-21 10:50:01.539', 'N/A', '2015-01-21 10:50:01.535', 'NOW', NULL, '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA', '13.KLARIFIKASI_NEGOSIASI', 13),
(492, 'C', '2015-01-21 10:50:01.557', 'N/A', '2015-01-21 10:50:01.552', 'NOW', NULL, '13.UPLOAD_BA_HASIL_LELANG', '13.PENETAPAN_PEMENANG_AKHIR', 13),
(493, 'C', '2015-01-21 10:50:01.569', 'N/A', '2015-01-21 10:50:01.566', 'LELANG_GAGAL', NULL, '13.PENUNJUKAN_PEMENANG', '13.GAGAL', 13),
(494, 'C', '2015-01-21 10:50:01.583', 'N/A', '2015-01-21 10:50:01.578', 'NOW', NULL, '13.PENUNJUKAN_PEMENANG', '13.TANDATANGAN_KONTRAK', 13),
(495, 'C', '2015-01-21 10:50:01.595', 'N/A', '2015-01-21 10:50:01.592', 'LELANG_GAGAL', NULL, '13.TANDATANGAN_KONTRAK', '13.GAGAL', 13),
(496, 'C', '2015-01-21 10:50:01.611', 'N/A', '2015-01-21 10:50:01.607', 'LELANG_SELESAI', NULL, '13.TANDATANGAN_KONTRAK', '13.END', 13),
(497, 'C', '2015-01-21 10:50:01.616', 'N/A', '2015-01-21 10:50:01.616', NULL, NULL, '13.END', NULL, 13),
(498, 'C', '2015-01-21 10:50:01.632', 'N/A', '2015-01-21 10:50:01.63', 'EVALUASI_ULANG', NULL, '13.GAGAL', '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 13),
(499, 'C', '2015-01-21 10:50:01.651', 'N/A', '2015-01-21 10:50:01.647', 'PEMASUKAN_PENAWARAN_ULANG', NULL, '13.GAGAL', '13.PEMASUKAN_PENAWARAN', 13),
(500, 'C', '2015-01-21 10:50:01.664', 'N/A', '2015-01-21 10:50:01.661', 'LELANG_SELESAI', NULL, '13.GAGAL', '13.END', 13);


 -- kolom baru untuk penggabungan paket pencatatan
ALTER TABLE ekontrak.nonlelang_seleksi DROP COLUMN lls_metode;

-- tambah kolom reverse_auction
ALTER TABLE public.reverse_auction ADD COLUMN ra_alasan text DEFAULT 0;
ALTER TABLE public.reverse_auction ADD COLUMN ra_status numeric DEFAULT 0;
ALTER TABLE public.reverse_auction DROP COLUMN ra_batas_bawah;
ALTER TABLE public.reverse_auction ADD COLUMN ra_versi numeric DEFAULT 0;

-- CREATE FIELD "is_active" -----------------------------------
ALTER TABLE "public"."panitia" ADD COLUMN "is_active" Integer DEFAULT -1 NOT NULL;
-- -------------------------------------------------------------

-- Tabel history_paket_pokja untuk fitur ganti pokja/panitia
-- ################################################################

-- CREATE TABLE "history_paket_pokja" ----------------------------
CREATE TABLE "public"."history_paket_pokja" (
	"pnt_id" Numeric( 19, 0 ) NOT NULL,
	"pkt_id" Numeric( 19, 0 ) NOT NULL,
	"peg_id" Numeric( 19, 0 ) NOT NULL,
	"tgl_perubahan" Timestamp Without Time Zone NOT NULL,
	"audittype" Character( 1 ) DEFAULT 'C'::bpchar NOT NULL,
	"audituser" Character Varying( 100 ) DEFAULT 'ADMIN'::character varying NOT NULL,
	"auditupdate" Timestamp( 6 ) Without Time Zone DEFAULT now() NOT NULL,
	"alasan" Text,
	PRIMARY KEY ( "pnt_id", "pkt_id" ),
	CONSTRAINT "pp_audittype_check" CHECK((audittype = 'D'::bpchar) OR (audittype = 'C'::bpchar) OR (audittype = 'U'::bpchar)) );

-- CREATE LINK "fk_history_paket_pokja_pegawai" ------------------
ALTER TABLE "public"."history_paket_pokja"
	ADD CONSTRAINT "fk_history_paket_pokja_pegawai" FOREIGN KEY ( "peg_id" )
	REFERENCES "public"."pegawai" ( "peg_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;

-- CREATE LINK "fk_history_paket_pokja_paket" --------------------
ALTER TABLE "public"."history_paket_pokja"
	ADD CONSTRAINT "fk_history_paket_pokja_paket" FOREIGN KEY ( "pkt_id" )
	REFERENCES "public"."paket" ( "pkt_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;

-- CREATE LINK "fk_history_paket_pokja_panitia" ----------------------
ALTER TABLE "public"."history_paket_pokja"
	ADD CONSTRAINT "fk_history_paket_pokja_panitia" FOREIGN KEY ( "pnt_id" )
	REFERENCES "public"."panitia" ( "pnt_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;

-- ################################################################

UPDATE configuration set cfg_value='spse-4.3' where cfg_sub_category='ppe.versi' AND (cfg_value='spse-4.2' OR cfg_value IS NULL);

-- update aktivitas ----
UPDATE aktivitas SET akt_jenis='PENETAPAN_PEMENANG_AKHIR' WHERE akt_id='287';

-- Tabel ekontrak.history_paket_pokja untuk fitur ganti pokja/panitia di non tender
-- ################################################################
-- CREATE TABLE "history_paket_pokja" ----------------------------
CREATE TABLE ekontrak.history_paket_pokja (
	pnt_id Numeric( 19, 0 ) NOT NULL,
	pkt_id Numeric( 19, 0 ) NOT NULL,
	peg_id Numeric( 19, 0 ) NOT NULL,
	tgl_perubahan Timestamp Without Time Zone NOT NULL,
	audittype Character( 1 ) DEFAULT 'C'::bpchar NOT NULL,
	audituser Character Varying( 100 ) DEFAULT 'ADMIN'::character varying NOT NULL,
	auditupdate Timestamp( 6 ) Without Time Zone DEFAULT now() NOT NULL,
	alasan Text,
	PRIMARY KEY ( pnt_id, pkt_id ),
	CONSTRAINT pp_audittype_check CHECK((audittype = 'D'::bpchar) OR (audittype = 'C'::bpchar) OR (audittype = 'U'::bpchar)) );

-- CREATE LINK "fk_history_paket_pokja_pegawai" ------------------
ALTER TABLE ekontrak.history_paket_pokja
	ADD CONSTRAINT fk_history_paket_pokja_pegawai FOREIGN KEY ( peg_id )
	REFERENCES public.pegawai ( peg_id ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;

-- CREATE LINK "fk_history_paket_pokja_paket" --------------------
ALTER TABLE ekontrak.history_paket_pokja
	ADD CONSTRAINT fk_history_paket_pokja_paket FOREIGN KEY ( pkt_id )
	REFERENCES ekontrak.paket ( pkt_id ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;

-- CREATE LINK "fk_history_paket_pokja_panitia" ----------------------
ALTER TABLE ekontrak.history_paket_pokja
	ADD CONSTRAINT fk_history_paket_pokja_panitia FOREIGN KEY ( pnt_id )
	REFERENCES public.panitia ( pnt_id ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;
-- ################################################################

-- Tabel ekontrak.persetujuan untuk fitur persetujuan kolegial di non tender
-- ################################################################
-- CREATE TABLE "persetujuan" ----------------------------------
CREATE TABLE "ekontrak"."persetujuan" (
	"pst_id" Bigint NOT NULL,
	"audittype" Character Varying( 1 ),
	"auditupdate" Timestamp( 6 ) Without Time Zone,
	"audituser" Character Varying( 100 ),
	"pst_alasan" Text,
	"pst_jenis" Integer,
	"pst_status" Integer,
	"pst_tgl_setuju" Timestamp( 6 ) Without Time Zone,
	"lls_id" Bigint,
	"peg_id" Bigint,
	PRIMARY KEY ( "pst_id" ) );

	ALTER TABLE "ekontrak"."persetujuan" OWNER TO epns;
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_status" -----------------------
CREATE INDEX "idx_persetujuan_status" ON "ekontrak"."persetujuan" USING btree( "pst_status" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_pegawai" ----------------------
CREATE INDEX "idx_persetujuan_pegawai" ON "ekontrak"."persetujuan" USING btree( "peg_id" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_lelang" -----------------------
CREATE INDEX "idx_persetujuan_lelang" ON "ekontrak"."persetujuan" USING btree( "lls_id" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_jenis" ------------------------
CREATE INDEX "idx_persetujuan_jenis" ON "ekontrak"."persetujuan" USING btree( "pst_jenis" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE INDEX "idx_persetujuan_auditupdate" ------------------
CREATE INDEX "idx_persetujuan_auditupdate" ON "ekontrak"."persetujuan" USING btree( "auditupdate" Asc NULLS Last );
-- -------------------------------------------------------------

-- CREATE LINK "persetujuan_lls_id_fkey" -----------------------
ALTER TABLE "ekontrak"."persetujuan"
	ADD CONSTRAINT "persetujuan_lls_id_fkey" FOREIGN KEY ( "lls_id" )
	REFERENCES "ekontrak"."nonlelang_seleksi" ( "lls_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;
-- -------------------------------------------------------------

-- CREATE LINK "persetujuan_peg_id_fkey" -----------------------
ALTER TABLE "ekontrak"."persetujuan"
	ADD CONSTRAINT "persetujuan_peg_id_fkey" FOREIGN KEY ( "peg_id" )
	REFERENCES "public"."pegawai" ( "peg_id" ) MATCH SIMPLE
	ON DELETE Restrict
	ON UPDATE Cascade;
-- -------------------------------------------------------------

-- CREATE TABLE "riwayat_persetujuan" --------------------------
CREATE TABLE "ekontrak"."riwayat_persetujuan" (
	"rwt_pst_id" Bigint NOT NULL,
	"audittype" Character Varying( 1 ),
	"auditupdate" Timestamp( 6 ) Without Time Zone,
	"audituser" Character Varying( 100 ),
	"pst_alasan" Text,
	"pst_jenis" Integer,
	"pst_status" Integer,
	"pst_tgl_setuju" Timestamp( 6 ) Without Time Zone,
	"lls_id" Bigint,
	"peg_id" Bigint,
	 CONSTRAINT pk_riwayat_persetujuan PRIMARY KEY (rwt_pst_id));
 ;

 ALTER TABLE "ekontrak"."riwayat_persetujuan" OWNER TO epns;
-- -------------------------------------------------------------

-- CREATE SEQUENCE "seq_persetujuan" ---------------------------
CREATE SEQUENCE "ekontrak"."seq_persetujuan"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
-- -------------------------------------------------------------

ALTER TABLE "ekontrak"."seq_persetujuan" OWNER TO epns;

-- CREATE SEQUENCE "seq_riwayat_persetujuan" -------------------
CREATE SEQUENCE "ekontrak"."seq_riwayat_persetujuan"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
-- -------------------------------------------------------------
ALTER TABLE "ekontrak"."seq_riwayat_persetujuan" OWNER TO epns;
-- ################################################################

-- CHANGE "NULLABLE" OF "FIELD "nev_lulus" ---------------------
ALTER TABLE "public"."nilai_evaluasi" ALTER COLUMN "nev_lulus" DROP NOT NULL;
-- -------------------------------------------------------------

-- CREATE FIELD "table_header" ---------------------------------
ALTER TABLE "public"."checklist_master" ADD COLUMN "table_header" JSON;
-- -------------------------------------------------------------
-- TENAGA AHLI --
update checklist_master set table_header='{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}' where ckm_id=4;
-- TENAGA TEKNIS --
update checklist_master set table_header='{"key":["jenis_kemampuan_tk", "kemampuan_teknis_tk", "pengalaman_tk", "kemampuan_managerial_tk"], "label":["Jenis Kemampuan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}' where ckm_id=11;
-- PERALATAN --
update checklist_master set table_header='{"key":["nama_peralatan", "spesifikasi_peralatan"], "label":["Nama", "Spesifikasi"]}' where ckm_id=12;

-- tahapan pasca 1 file , sesuai perlem 9 lkpp thn 2018 ------
INSERT INTO public.aktivitas(akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES
(291, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENGUMUMAN_LELANG', 1, 1),
(292, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'AMBIL_DOKUMEN', 2, 1),
(293, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENJELASAN', 3, 1),
(294, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PEMASUKAN_PENAWARAN', 4, 1),
(295, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PEMBUKAAN_PENAWARAN', 5, 1),
(296, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'EVALUASI_PENAWARAN_KUALIFIKASI', 6, 1),
(297, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'VERIFIKASI_KUALIFIKASI', 7, 1),
(298, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENETAPAN_PEMENANG_AKHIR', 8, 1),
(299, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENGUMUMAN_PEMENANG_AKHIR', 9, 1),
(300, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'SANGGAH', 10, 1),
(301, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENUNJUKAN_PEMENANG', 11, 1),
(302, 54, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'TANDATANGAN_KONTRAK', 12, 1);

-- tahapan pasca 2 file , sesuai perlem 9 lkpp thn 2018 ------
INSERT INTO public.aktivitas(akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES
(303, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENGUMUMAN_LELANG', 1, 1),
(304, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'AMBIL_DOKUMEN', 2, 1),
(305, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENJELASAN', 3, 1),
(306, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PEMASUKAN_PENAWARAN', 4, 1),
(307, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI', 5, 1),
(308, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENGUMUMAN_PEMENANG_ADM_TEKNIS', 6, 1),
(309, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA', 7, 1),
(310, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'VERIFIKASI_KUALIFIKASI', 8, 1),
(311, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENETAPAN_PEMENANG_AKHIR', 9, 1),
(312, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENGUMUMAN_PEMENANG_AKHIR', 10, 1),
(313, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'SANGGAH', 11, 1),
(314, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'PENUNJUKAN_PEMENANG', 12, 1),
(315, 55, 'C', 'ADMIN', '2018-10-10 05:07:32.458', 'TANDATANGAN_KONTRAK', 13, 1);

ALTER TABLE ekontrak.SWAKELOLA_SELEKSI ADD COLUMN tipe_swakelola integer;

ALTER TABLE rekanan ADD COLUMN edited_data character varying(4096) NULL;

INSERT INTO "public"."wf_state"(state_id, audittype, auditupdate, audituser, createdate, description, fork, name, process_definition_id) VALUES
('1.EVALUASI_PENAWARAN_KUALIFIKASI', 'U', '2015-01-21 10:41:16.072', 'N/A', '2015-01-21 10:41:16.072', NULL, 'f', 'EVALUASI_PENAWARAN_KUALIFIKASI', 1),
('2.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI', 'U', '2015-01-21 10:41:16.072', 'N/A', '2015-01-21 10:41:16.072', NULL, 'f', 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI', 2);

---- ADD column sghb_isi ,  sghb_tanggal at table sanggah_banding -------
ALTER TABLE sanggah_banding ADD COLUMN sghb_isi text NULL;
ALTER TABLE sanggah_banding ADD COLUMN sghb_tanggal timestamp(6) without time zone;

-- CHANGE "NULLABLE" OF "FIELD ra_waktu_mulai,ra_waktu_selesai "" ---------------------
ALTER TABLE "public"."reverse_auction" ALTER COLUMN "ra_waktu_mulai" DROP NOT NULL;
ALTER TABLE "public"."reverse_auction" ALTER COLUMN "ra_waktu_selesai" DROP NOT NULL;

-- create index on table kontrak---------------------
CREATE INDEX idx_kontrak_paket_only ON public.kontrak USING btree (lls_id) TABLESPACE pg_default;

-- CREATE FIELD "kontrak_namarekening" -------------------------
ALTER TABLE "public"."kontrak" ADD COLUMN "kontrak_namarekening" Text;

-- CREATE FIELD "jaminan_penawaran" ----------------------------
ALTER TABLE "public"."sppbj" ADD COLUMN "jaminan_pelaksanaan" Numeric( 18, 2 ) DEFAULT 0 NOT NULL;

-- CHANGE "TYPE" OF "FIELD "lls_kontrak_pembayaran" ------------
ALTER TABLE "public"."lelang_seleksi" ALTER COLUMN "lls_kontrak_pembayaran" TYPE SmallInt;

ALTER TABLE "public"."update_schedule" ADD COLUMN "do_backup" bool DEFAULT true NOT NULL;

-- CHANGE "NULLABLE" OF "FIELD mtd_evaluasi --------
ALTER TABLE ekontrak.nonlelang_seleksi ALTER COLUMN mtd_evaluasi DROP NOT NULL;

INSERT INTO public.aktivitas(akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) 
VALUES (316, 46, 'C', 'ADMIN', '2018-12-03 02:32:23.846406', 'PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 20, 1);

UPDATE public.aktivitas SET  akt_urut = 21 WHERE akt_id = 170;
UPDATE public.aktivitas SET  akt_urut = 22 WHERE akt_id = 171;

INSERT INTO "public"."wf_state"(state_id, audittype, auditupdate, audituser, createdate, description, fork, name, process_definition_id) 
VALUES ('7.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 'U', '2018-12-04 06:33:42.342125', 'N/A', '2018-12-04 06:33:42.342125', NULL, 'f', 'PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 7);

-- perbaikan tahapan pasca 2 file konsultan perorangan, penyesuain perpres 16/18 ----
UPDATE aktivitas SET akt_jenis='PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI' WHERE akt_id=279;
DELETE FROM history_jadwal WHERE dtj_id IN (SELECT dtj_id FROM jadwal WHERE akt_id IN (280,286));
DELETE FROM jadwal WHERE akt_id in (280, 286);
DELETE FROM aktivitas WHERE akt_id in (280, 286);
UPDATE aktivitas SET akt_urut ='6' WHERE akt_id=281;
UPDATE aktivitas SET akt_urut ='7' WHERE akt_id=282;
UPDATE aktivitas SET akt_urut ='8' WHERE akt_id=283;
UPDATE aktivitas SET akt_urut ='9' WHERE akt_id=284;
UPDATE aktivitas SET akt_urut ='10' WHERE akt_id=285;
UPDATE aktivitas SET akt_urut ='11' WHERE akt_id=287;
UPDATE aktivitas SET akt_urut ='12' WHERE akt_id=288;
UPDATE aktivitas SET akt_urut ='13' WHERE akt_id=289;
UPDATE aktivitas SET akt_urut ='14' WHERE akt_id=290;

ALTER TABLE public.paket ADD COLUMN is_pkt_konsolidasi bool, ADD COLUMN pkt_id_konsolidasi numeric(19),
ADD CONSTRAINT fk_pkt_id_konsolidasi_to_pkt_id FOREIGN KEY (pkt_id_konsolidasi) REFERENCES public.paket (pkt_id) ON DELETE RESTRICT ON UPDATE CASCADE ;

CREATE TABLE public.temp_sppbj_hps (
	ppk_id numeric (19,0) NOT NULL,
	audittype Character Varying( 1 ),
	auditupdate Timestamp Without Time Zone,
	audituser Character Varying( 100 ),
	dkh Text,
	lls_id numeric (19,0) NOT NULL,
	psr_id numeric (19,0)
);
ALTER TABLE public.temp_sppbj_hps OWNER TO epns;

ALTER TABLE public.wf_state_mapper DROP CONSTRAINT fk_wf_state_mapper_state;
ALTER TABLE public.wf_state_mapper ADD CONSTRAINT fk_wf_state_mapper_state FOREIGN KEY (state_id) REFERENCES public.wf_state (state_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE public.wf_state_instance DROP CONSTRAINT fk_wf_state_instance_wf_state;
ALTER TABLE public.wf_state_instance ADD CONSTRAINT fk_wf_state_instance_wf_state FOREIGN KEY (state_id) REFERENCES public.wf_state (state_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE public.wf_state_mapper DROP CONSTRAINT fk_wf_state_mapper_next_state;
ALTER TABLE public.wf_state_mapper ADD CONSTRAINT fk_wf_state_mapper_next_state FOREIGN KEY (next_state_id) REFERENCES public.wf_state (state_id) MATCH SIMPLE ON UPDATE CASCADE    ON DELETE RESTRICT;
UPDATE public.wf_state SET name = 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI',  state_id = '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI' WHERE state_id = '13.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS';
DELETE FROM public.wf_state_mapper WHERE state_id = '13.UPLOAD_BA_HASIL_LELANG';
DELETE FROM public.wf_state_instance WHERE state_id = '13.UPLOAD_BA_HASIL_LELANG';
DELETE FROM public.wf_state WHERE state_id = '13.UPLOAD_BA_HASIL_LELANG';

-- Penyeragaman Tipe Data ekontrak.ijin_usaha_peserta dengan public.ijin_usaha_peserta --
ALTER TABLE ekontrak.ijin_usaha_peserta ALTER COLUMN ius_no TYPE text;

-- Penyeragaman Tipe Data ekontrak.pengalaman_peserta dengan public.pengalaman_peserta --
ALTER TABLE ekontrak.pengalaman_peserta ALTER COLUMN pgl_kegiatan TYPE text;
ALTER TABLE ekontrak.pengalaman_peserta ALTER COLUMN pgl_lokasi TYPE text;
ALTER TABLE ekontrak.pengalaman_peserta ALTER COLUMN pgl_pembtgs TYPE text;
ALTER TABLE ekontrak.pengalaman_peserta ALTER COLUMN pgl_almtpembtgs TYPE text;
ALTER TABLE ekontrak.pengalaman_peserta ALTER COLUMN pgl_telppembtgs TYPE text;
ALTER TABLE ekontrak.pengalaman_peserta ALTER COLUMN pgl_nokontrak TYPE text;


CREATE TABLE public.history_persetujuan
(
    pst_id bigint NOT NULL,
    audittype character varying(1) COLLATE pg_catalog."default",
    auditupdate timestamp(6) without time zone,
    audituser character varying(100) COLLATE pg_catalog."default",
    pst_alasan text COLLATE pg_catalog."default",
    pst_jenis integer,
    pst_status integer,
    pst_tgl_setuju timestamp(6) without time zone,
    lls_id bigint,
    peg_id bigint,
    CONSTRAINT pk_history_persetujuan PRIMARY KEY (pst_id),
    CONSTRAINT fk_history_persetujuan_lelang FOREIGN KEY (lls_id)
        REFERENCES public.lelang_seleksi (lls_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT,
    CONSTRAINT fk_persetujuan_pegawai FOREIGN KEY (peg_id) REFERENCES public.pegawai (peg_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE RESTRICT
);

ALTER TABLE public.history_persetujuan OWNER to epns;

-- Kolom baru di peserta untuk fitur upload dokumen kualifikasi dan evaluasi kualifikasi tambahan
ALTER TABLE ekontrak.peserta_nonlelang ADD COLUMN is_dikirim_pesan boolean DEFAULT false;

-- Kolom baru di lelang_seleksi untuk fitur upload dokumen kualifikasi dan evaluasi kualifikasi tambahan
ALTER TABLE ekontrak.nonlelang_seleksi ADD COLUMN is_kualifikasi_tambahan boolean DEFAULT false;

ALTER TABLE public.dok_persiapan ADD COLUMN dp_lainnya numeric(19,0);

ALTER TABLE ekontrak.bukti_pajak ALTER COLUMN pjk_no TYPE text;
ALTER TABLE ekontrak.bukti_pajak ALTER COLUMN pjk_npwp TYPE text;
ALTER TABLE ekontrak.ijin_usaha_peserta ALTER COLUMN jni_nama TYPE text;
ALTER TABLE ekontrak.ijin_usaha_peserta ALTER COLUMN ius_instansi TYPE text;


---- UPDATE dan INSERT "Master Checklist" Non Tender-----
update checklist_master set ckm_nama = 'Spesifikasi Teknis dan Identitas' where ckm_id = 32;
update checklist_master set ckm_nama = 'Jadwal Penyerahan atau Pengiriman Barang' where ckm_id = 33;
update checklist_master set ckm_nama = 'Bagian Pekerjaan yang Disubkontrakkan dari Isian LDK' where ckm_id = 34;
update checklist_master set ckm_nama = 'Brosur atau Gambar-Gambar' where ckm_id = 35;
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(42, 4, 'Jaminan Purnajual', 'ADMIN','C', 'now()', null, 1, 0,0);
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(43, 4, 'Asuransi', 'ADMIN','C', 'now()', null, 1, 0,0);
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(44, 4, 'Tenaga Teknis', 'ADMIN','C', 'now()', null, 1, 0,0);
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(45, 4, 'Rekapitulasi Perhitungan TKDN', 'ADMIN','C', 'now()', null, 1, 0,0);
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(46, 4, 'Metodologi dan Pendekatan', 'ADMIN','C', 'now()', null, 1, 0,0);
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(47, 4, 'Pengalaman Perusahaan', 'ADMIN','C', 'now()', null, 1, 0,0);
INSERT INTO checklist_master (ckm_id, ckm_jenis, ckm_nama, audituser, audittype , auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan)
VALUES(48, 4, 'Kualifikasi Tenaga Ahli', 'ADMIN','C', 'now()', null, 1, 0,0);

--- tambah kolom pkt_tgl_build --------------------------
ALTER TABLE public.paket ADD COLUMN pkt_tgl_build character varying(100);
---tambah kolom dp_lainnya schema ekontrak---------------
ALTER TABLE ekontrak.dok_persiapan ADD COLUMN dp_lainnya numeric(19,0);

--- update metode penawaran di lelang_seleksi jika terjadi auction ----------------
UPDATE lelang_seleksi SET lls_metode_penawaran = 1 WHERE lls_id IN (SELECT distinct lls_id FROM reverse_auction);

ALTER TABLE ekontrak.paket_swakelola RENAME COLUMN pkt_kegiatan to pkt_nama;
ALTER TABLE ekontrak.RUP_PAKET_SWAKELOLA RENAME COLUMN kegiatan to nama;

ALTER TABLE ekontrak.PAKET_ANGGARAN_SWAKELOLA ADD COLUMN ppk_id numeric DEFAULT NULL;
ALTER TABLE ekontrak.PAKET_ANGGARAN_SWAKELOLA ADD COLUMN rup_id numeric DEFAULT NULL;

-- table paket sirup 2.3
CREATE TABLE ekontrak.paket_swakelola_sirup
(
    id numeric NOT NULL,
    nama text NOT NULL,
    paket_lokasi jsonb,
    volume text,
    keterangan text,
    spesifikasi text,
    is_pradipa boolean,
    paket_anggaran jsonb,
    jumlah_pagu numeric NOT NULL,
    tipe_swakelola integer,
    tanggal_kebutuhan date,
    tanggal_awal_pengadaan date,
    tanggal_akhir_pengadaan date,
    tanggal_awal_pekerjaan date,
    tanggal_akhir_pekerjaan date,
    rup_stk_id text,
    tahun numeric,
    CONSTRAINT pk_paket_swakelola_sirup PRIMARY KEY (id)
);
ALTER TABLE ekontrak.paket_swakelola_sirup OWNER to epns;

-----ekontrak anggaran swakelola------
CREATE TABLE ekontrak.anggaran_swakelola
(
    stk_id numeric NOT NULL,
    sbd_id text NOT NULL,
    audittype character(1) NOT NULL DEFAULT 'C'::bpchar,
    audituser character varying(100) NOT NULL DEFAULT 'ADMIN'::character varying,
    auditupdate timestamp without time zone NOT NULL DEFAULT now(),
    ang_koderekening text NOT NULL,
    ang_nilai numeric(18, 2) NOT NULL,
    ang_uraian text NOT NULL,
    ang_tahun numeric NOT NULL,
    ang_id numeric NOT NULL,
    CONSTRAINT pk_anggaran_swakelola PRIMARY KEY (ang_id),
    CONSTRAINT fk_anggaran_swakelola_satuan_kerja FOREIGN KEY (stk_id)
        REFERENCES public.satuan_kerja (stk_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);
ALTER TABLE ekontrak.anggaran_swakelola OWNER to epns;


CREATE TABLE ekontrak.paket_ppk_swakelola
(
    swk_id numeric NOT NULL,
    ppk_id numeric NOT NULL,
    audittype character(1) NOT NULL DEFAULT 'C'::bpchar,
    audituser character varying(100) NOT NULL DEFAULT 'ADMIN'::character varying,
    auditupdate timestamp without time zone NOT NULL DEFAULT now(),
    CONSTRAINT pk_paket_ppk_swakelola PRIMARY KEY (swk_id, ppk_id),
    CONSTRAINT fk_paket_ppk_swakelola_ppk FOREIGN KEY (ppk_id)
        REFERENCES public.ppk (ppk_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

ALTER TABLE ekontrak.paket_ppk_swakelola OWNER to epns;

ALTER TABLE ekontrak.paket ADD COLUMN is_pkt_konsolidasi bool, ADD COLUMN pkt_id_konsolidasi numeric(19),
ADD CONSTRAINT fk_pkt_id_konsolidasi_to_pkt_id FOREIGN KEY (pkt_id_konsolidasi) REFERENCES ekontrak.paket (pkt_id) ON DELETE RESTRICT ON UPDATE CASCADE ;

-- CREATE FIELD "jaminan_penawaran" ----------------------------
ALTER TABLE ekontrak.sppbj ADD COLUMN jaminan_pelaksanaan Numeric( 18, 2 ) DEFAULT 0 NOT NULL;

CREATE TABLE ekontrak.temp_sppbj_hps
(
    ppk_id bigint NOT NULL,
    audittype character varying(1),
    auditupdate timestamp(6) without time zone,
    audituser character varying(100),
    dkh text,
    lls_id bigint NOT NULL,
    psr_id bigint
);

ALTER TABLE ekontrak.temp_sppbj_hps OWNER to epns;

UPDATE bentuk_usaha SET btu_nama='Konsultan Perorangan' WHERE btu_id='07';

ALTER TABLE ekontrak.peserta_nonlelang ADD COLUMN sudah_verifikasi_sikap numeric DEFAULT 0;

ALTER TABLE peserta ADD COLUMN is_pemenang_klarifikasi numeric DEFAULT 0;

UPDATE lelang_seleksi SET lls_metode_penawaran = 1 WHERE lls_id IN (SELECT lls_id FROM reverse_auction);

ALTER TABLE ekontrak.spk ADD COLUMN spk_attachment2 numeric;

-- tambahkan kontrak pembayaran di dok content - rprimar
ALTER TABLE dok_lelang_content ADD COLUMN dll_kontrak_pembayaran integer;
ALTER TABLE ekontrak.dok_nonlelang_content ADD COLUMN dll_kontrak_pembayaran integer;
-- alter column type text--
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_npwp TYPE text;
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_kewarganearaan TYPE text;
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_nama TYPE text;
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_jabatan TYPE text;
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_keahlian TYPE text;
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_pendidikan TYPE text;
ALTER TABLE ekontrak.staf_ahli_psr ALTER COLUMN sta_email TYPE text;

ALTER TABLE paket_anggaran ADD pkt_ang_versi INT DEFAULT 1;
ALTER TABLE paket_lokasi ADD pkt_lokasi_versi INT DEFAULT 1;

ALTER TABLE ekontrak.paket_anggaran ADD pkt_ang_versi INT DEFAULT 1;
ALTER TABLE ekontrak.paket_lokasi ADD pkt_lokasi_versi INT DEFAULT 1;
--tambah survey harga--
ALTER TABLE ekontrak.survey_harga_pl ADD COLUMN pkt_id NUMERIC;
---tambah versi lokasi swakelola---
ALTER TABLE ekontrak.paket_swakelola_lokasi ADD COLUMN pkt_lokasi_versi INTEGER DEFAULT 1;
--paket swakelola ----
ALTER TABLE ekontrak.paket_swakelola DROP COLUMN pkt_kegiatan;

ALTER TABLE SANGGAH_BANDING ADD sghb_id_datadukung_attachment NUMERIC;
ALTER TABLE ekontrak.paket_lokasi ALTER COLUMN pkt_lokasi TYPE text;

ALTER TABLE ekontrak.jenis_realisasi_non_spk ADD COLUMN rsk_bukti_pembayaran numeric NOT NULL DEFAULT 0;

INSERT INTO propinsi(prp_id, audittype, audituser, auditupdate, prp_nama) VALUES (0, 'C', 'ADMIN', NOW(), 'LAINNYA');
INSERT INTO kabupaten VALUES (0, 0, 'U', 'ADMIN', NOW(), 'Lainnya', 1), (99999, 0, 'U', 'ADMIN', NOW(), 'Luar Indonesia', 1);
ALTER TABLE public.paket_satker ADD CONSTRAINT pk_paket_satker PRIMARY KEY (pks_id);

ALTER TABLE ekontrak.rup_paket_swakelola ADD kegiatan text DEFAULT NULL;
ALTER TABLE ekontrak.paket_swakelola ADD pkt_kegiatan text DEFAULT NULL;

ALTER TABLE public.temp_sppbj_hps ADD CONSTRAINT pk_temp_sppbj_hps PRIMARY KEY (lls_id, ppk_id);
ALTER TABLE ekontrak.temp_sppbj_hps ADD CONSTRAINT pk_temp_sppbj_hps PRIMARY KEY (lls_id, ppk_id);
ALTER TABLE public.syarat_paket ADD CONSTRAINT pk_syarat_paket PRIMARY KEY (id_syarat_paket);
ALTER TABLE public.rhs_permission ADD CONSTRAINT pk_rhs_permission PRIMARY KEY (rpn_id);
ALTER TABLE public.lelang_nasional ADD CONSTRAINT pk_lelang_nasional PRIMARY KEY (lls_id);
UPDATE blob_log SET blb_log_id = nextsequence('seq_blob_log') WHERE blb_log_id IS NULL;
ALTER TABLE public.blob_log ADD CONSTRAINT pk_blob_log PRIMARY KEY (blb_log_id);
ALTER TABLE riwayat_persetujuan ADD PRIMARY KEY (rwt_pst_id);
ALTER TABLE ekontrak.riwayat_persetujuan ADD PRIMARY KEY (rwt_pst_id);

INSERT INTO public.wf_state(state_id, audittype, auditupdate, audituser, createdate, description, fork, name, process_definition_id) VALUES ('7.PENETAPAN_PEMENANG_ADM_TEKNIS', 'U', '2015-01-21 10:52:34.53', 'N/A', '2015-01-21 10:52:34.529', NULL, 'f', 'PENETAPAN_PEMENANG_ADM_TEKNIS', 7);
INSERT INTO public.wf_state(state_id, audittype, auditupdate, audituser, createdate, description, fork, name, process_definition_id) VALUES ('7.KLARIFIKASI_TEKNIS_BIAYA', 'U', '2015-01-21 10:52:34.53', 'N/A', '2015-01-21 10:52:34.529', NULL, 'f', 'KLARIFIKASI_TEKNIS_BIAYA', 7);
UPDATE aktivitas SET akt_status = 0 WHERE akt_jenis = 'UPLOAD_BA_HASIL_LELANG';

ALTER TABLE ekontrak.dok_persiapan ADD COLUMN dp_survey numeric(19,0);

---- Perbaikan tahapan Penjelasan dokumen prakualifikasi ---------------------
UPDATE aktivitas SET akt_status = 1 WHERE mtd_id in (46,47,48) AND akt_jenis = 'PENJELASAN_PRA';
INSERT INTO public.aktivitas(akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) 
  VALUES (317, 42, 'C', 'ADMIN', NOW(), 'PENJELASAN_PRA', 3, 1), (318, 44, 'C', 'ADMIN', NOW(), 'PENJELASAN_PRA', 3, 1);

UPDATE public.aktivitas SET akt_urut = 4 WHERE akt_id = 133 AND mtd_id = 42 AND akt_jenis = 'PEMASUKAN_DOK_PRA';
UPDATE public.aktivitas SET akt_urut = 5 WHERE akt_id = 134 AND mtd_id = 42 AND akt_jenis = 'EVALUASI_DOK_PRA';
UPDATE public.aktivitas SET akt_urut = 6 WHERE akt_id = 135 AND mtd_id = 42 AND akt_jenis = 'VERIFIKASI_KUALIFIKASI';
UPDATE public.aktivitas SET akt_urut = 7 WHERE akt_id = 136 AND mtd_id = 42 AND akt_jenis = 'PENETAPAN_HASIL_PRA';
UPDATE public.aktivitas SET akt_urut = 8 WHERE akt_id = 137 AND mtd_id = 42 AND akt_jenis = 'PENGUMUMAN_HASIL_PRA';
UPDATE public.aktivitas SET akt_urut = 9 WHERE akt_id = 138 AND mtd_id = 42 AND akt_jenis = 'SANGGAH_PRA';
UPDATE public.aktivitas SET akt_urut = 10 WHERE akt_id = 139 AND mtd_id = 42 AND akt_jenis = 'AMBIL_DOKUMEN_PEMILIHAN';
UPDATE public.aktivitas SET akt_urut = 11 WHERE akt_id = 140 AND mtd_id = 42 AND akt_jenis = 'PENJELASAN';
UPDATE public.aktivitas SET akt_urut = 12 WHERE akt_id = 141 AND mtd_id = 42 AND akt_jenis = 'PEMASUKAN_PENAWARAN';
UPDATE public.aktivitas SET akt_urut = 13 WHERE akt_id = 142 AND mtd_id = 42 AND akt_jenis = 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 14 WHERE akt_id = 143 AND mtd_id = 42 AND akt_jenis = 'PENGUMUMAN_PEMENANG_ADM_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 15 WHERE akt_id = 144 AND mtd_id = 42 AND akt_jenis = 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA';
UPDATE public.aktivitas SET akt_urut = 16 WHERE akt_id = 145 AND mtd_id = 42 AND akt_jenis = 'UPLOAD_BA_HASIL_LELANG' ;
UPDATE public.aktivitas SET akt_urut = 17 WHERE akt_id = 146 AND mtd_id = 42 AND akt_jenis = 'PENETAPAN_PEMENANG_AKHIR';
UPDATE public.aktivitas SET akt_urut = 18 WHERE akt_id = 147 AND mtd_id = 42 AND akt_jenis = 'PENGUMUMAN_PEMENANG_AKHIR';
UPDATE public.aktivitas SET akt_urut = 19 WHERE akt_id = 148 AND mtd_id = 42 AND akt_jenis = 'SANGGAH';
UPDATE public.aktivitas SET akt_urut = 20 WHERE akt_id = 149 AND mtd_id = 42 AND akt_jenis = 'PENUNJUKAN_PEMENANG';
UPDATE public.aktivitas SET akt_urut = 21 WHERE akt_id = 150 AND mtd_id = 42 AND akt_jenis = 'TANDATANGAN_KONTRAK';  

UPDATE public.aktivitas SET akt_urut = 4 WHERE akt_id = 197 AND mtd_id = 44 AND akt_jenis = 'PEMASUKAN_DOK_PRA';
UPDATE public.aktivitas SET akt_urut = 5 WHERE akt_id = 198 AND mtd_id = 44 AND akt_jenis = 'EVALUASI_DOK_PRA';
UPDATE public.aktivitas SET akt_urut = 6 WHERE akt_id = 199 AND mtd_id = 44 AND akt_jenis = 'VERIFIKASI_KUALIFIKASI';
UPDATE public.aktivitas SET akt_urut = 7 WHERE akt_id = 200 AND mtd_id = 44 AND akt_jenis = 'PENETAPAN_HASIL_PRA';
UPDATE public.aktivitas SET akt_urut = 8 WHERE akt_id = 201 AND mtd_id = 44 AND akt_jenis = 'PENGUMUMAN_HASIL_PRA';
UPDATE public.aktivitas SET akt_urut = 9 WHERE akt_id = 202 AND mtd_id = 44 AND akt_jenis = 'SANGGAH_PRA';
UPDATE public.aktivitas SET akt_urut = 10 WHERE akt_id = 203 AND mtd_id = 44 AND akt_jenis = 'AMBIL_DOKUMEN_PEMILIHAN';
UPDATE public.aktivitas SET akt_urut = 11 WHERE akt_id = 204 AND mtd_id = 44 AND akt_jenis = 'PENJELASAN';
UPDATE public.aktivitas SET akt_urut = 12 WHERE akt_id = 205 AND mtd_id = 44 AND akt_jenis = 'PEMASUKAN_PENAWARAN_ADM_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 13 WHERE akt_id = 206 AND mtd_id = 44 AND akt_jenis = 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 14 WHERE akt_id = 207 AND mtd_id = 44 AND akt_jenis = 'PENYETARAAN_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 15 WHERE akt_id = 208 AND mtd_id = 44 AND akt_jenis = 'PENETAPAN_PEMENANG_ADM_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 16 WHERE akt_id = 209 AND mtd_id = 44 AND akt_jenis = 'PENGUMUMAN_PEMENANG_ADM_TEKNIS';
UPDATE public.aktivitas SET akt_urut = 17 WHERE akt_id = 210 AND mtd_id = 44 AND akt_jenis = 'PEMASUKAN_PENAWARAN_BIAYA';
UPDATE public.aktivitas SET akt_urut = 18 WHERE akt_id = 211 AND mtd_id = 44 AND akt_jenis = 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA';
UPDATE public.aktivitas SET akt_urut = 19 WHERE akt_id = 212 AND mtd_id = 44 AND akt_jenis = 'UPLOAD_BA_HASIL_LELANG';
UPDATE public.aktivitas SET akt_urut = 20 WHERE akt_id = 213 AND mtd_id = 44 AND akt_jenis = 'PENETAPAN_PEMENANG_AKHIR';
UPDATE public.aktivitas SET akt_urut = 21 WHERE akt_id = 214 AND mtd_id = 44 AND akt_jenis = 'PENGUMUMAN_PEMENANG_AKHIR';
UPDATE public.aktivitas SET akt_urut = 22 WHERE akt_id = 215 AND mtd_id = 44 AND akt_jenis = 'SANGGAH';
UPDATE public.aktivitas SET akt_urut = 23 WHERE akt_id = 216 AND mtd_id = 44 AND akt_jenis = 'PENUNJUKAN_PEMENANG';
UPDATE public.aktivitas SET akt_urut = 24 WHERE akt_id = 217 AND mtd_id = 44 AND akt_jenis = 'TANDATANGAN_KONTRAK';

UPDATE public.aktivitas SET akt_status = 0 WHERE akt_jenis = 'PENETAPAN_PEMENANG_ADM_TEKNIS';

INSERT INTO public.wf_state(state_id, audittype, auditupdate, audituser, createdate, description, fork, name, process_definition_id)
VALUES ('12.PENJELASAN_PRA', 'U', NOW(), 'N/A', NOW(), NULL, 't', 'PENJELASAN_PRA', 12), ('9.PENJELASAN_PRA', 'U', NOW(), 'N/A', NOW(), NULL, 't', 'PENJELASAN_PRA', 9);

ALTER TABLE blob_table ADD PRIMARY KEY (blb_id_content, blb_versi);

INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(50, 5, 'Izin Usaha', 'C', 'ADMIN', '2019-06-27 17:30:29.496', NULL, 1, 1, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(51, 5, 'Memiliki TDP atau NIB', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(52, 5, 'Memiliki NPWP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(53, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(54, 5, 'Mempunyai atau menguasai tempat usaha/kantor dengan alamat yang benar, tetap dan jelas berupa milik sendiri atau sewa', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(55, 5, 'Secara hukum mempunyai kapasitas untuk mengikatkan diri pada Kontrak yang dibuktikan dengan:
a) Akta Pendirian Perusahaan dan/atau perubahannya; (akta perubahan bisa berlaku seluruhnya)
b) Surat Kuasa (apabila dikuasakan);
c) Bukti bahwa yang diberikan kuasa merupakan pegawai tetap (apabila dikuasakan); dan
d) KTP.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 6, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(56, 5, 'Surat Pernyataan:
a) Yang bersangkutan dan manajemennya tidak dalam pengawasan pengadilan, tidak pailit, dan kegiatan usahanya tidak sedang dihentikan;
b) Yang bersangkutan berikut Pengurus Badan Usaha tidak sedang dikenakan sanksi Daftar Hitam;
c) Yang bertindak untuk dan atas nama Badan Usaha tidak sedang dalam menjalani sanksi pidana;
d) pimpinan dan pengurus Badan Usaha bukan sebagai pegawai K/L/PD atau pimpinan dan pengurus Badan Usaha sebagai pegawai K/L/PD yang sedang mengambil cuti diluar tanggungan Negara;
e) Pernyataan lain yang menjadi syarat kualifikasi yang tercantum dalam Dokumen Kualifikasi; dan
f) Pernyataan bahwa data kualifikasi yang diisikan dan dokumen penawaran yang disampaikan  benar, dan jika dikemudian hari ditemukan bahwa data/dokumen yang disampaikan tidak benar dan ada pemalsuan maka Direktur Utama/Pimpinan Perusahaan/Pimpinan Koperasi, atau Kepala Cabang, dari seluruh anggota Kemitraan bersedia dikenakan sanksi administratif, sanksi pencantuman dalam Daftar Hitam, gugatan secara perdata, dan/atau pelaporan secara pidana kepada pihak berwenang sesuai dengan ketentuan peraturan perundang undangan.
', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 7, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(57, 5, 'Tidak masuk dalam Daftar Hitam', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 8, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(58, 5, 'Dalam hal Peserta akan melakukan konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain harus mempunyai perjanjian konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 9, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(995, 5, 'Syarat Kualifikasi Administrasi Lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', NULL, 1, 0, 99, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(60, 6, 'Memiliki Pengalaman Pekerjaan:
a) Penyediaan jasa pada divisi yang sama paling kurang number pekerjaan dalam kurun waktu 1 (satu)  tahun terakhir baik di lingkungan pemerintah maupun swasta, termasuk pengalaman subkontrak;
b) Penyediaan jasa sekurang-kurangnya dalam kelompok/grup yang sama paling kurang number pekerjaan dalam kurun waktu 3 (tiga) tahun terakhir baik di lingkungan pemerintah maupun swasta, termasuk pengalaman subkontrak; dan
c) Nilai pekerjaan sejenis tertinggi dalam kurun waktu 10 (sepuluh) tahun terakhir untuk usaha non kecil paling kurang sama dengan 50% (lima puluh persen) nilai total HPS/Pagu Anggaran.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 1, 1, '{"key":["number1", "number2"], "label":["Jumlah A", "Jumlah B"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(61, 6, 'Memiliki SDM Tenaga Ahli', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 0, 2, '{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(62, 6, 'Memiliki SDM Tenaga Teknis', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 0, 3, '{"key":["jenis_kemampuan_tk", "kemampuan_teknis_tk", "pengalaman_tk", "kemampuan_managerial_tk"], "label":["Jenis Kemampuan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(63, 6, 'Memiliki kemampuan untuk menyediakan peralatan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 0, 4, '{"key":["nama_peralatan", "spesifikasi_peralatan"], "label":["Nama", "Spesifikasi"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(64, 6, 'Layanan purnajual', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 0, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(996, 6, 'Syarat Kualifikasi Teknis Lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', NULL, 1, 0, 99, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(66, 7, 'Menyampaikan laporan keuangan tahun terakhir yang mencerminkan Total Ekuitas yang dilihat dari neraca keuangan.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 0, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(67, 7, 'Memiliki Sisa Kemampuan Nyata (SKN) paling kecil 50% (lima puluh persen) dari nilai Total HPS.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 3, 1, 0, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(997, 7, 'Syarat Kualifikasi Kemampuan Keuangan Lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', NULL, 1, 0, 99, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(69, 5, 'Memiliki TDP atau NIB', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(70, 5, 'Memiliki NPWP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(71, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(72, 5, 'Mempunyai atau menguasai tempat usaha/kantor dengan alamat yang benar, tetap dan jelas berupa milik sendiri atau sewa', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(73, 5, 'Secara hukum mempunyai kapasitas untuk mengikatkan diri pada Kontrak yang dibuktikan dengan:
a) Akta Pendirian Perusahaan dan/atau perubahannya; (akta perubahan bisa berlaku seluruhnya)
b) Surat Kuasa (apabila dikuasakan);
c) Bukti bahwa yang diberikan kuasa merupakan pegawai tetap (apabila dikuasakan); dan
d) KTP.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 6, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(74, 5, 'Surat Pernyataan:
a) Yang bersangkutan dan manajemennya tidak dalam pengawasan pengadilan, tidak pailit, dan kegiatan usahanya tidak sedang dihentikan;
b) Yang bersangkutan berikut Pengurus Badan Usaha tidak sedang dikenakan sanksi Daftar Hitam;
c) Yang bertindak untuk dan atas nama Badan Usaha tidak sedang dalam menjalani sanksi pidana;
d) pimpinan dan pengurus Badan Usaha bukan sebagai pegawai K/L/PD atau pimpinan dan pengurus Badan Usaha sebagai pegawai K/L/PD yang sedang mengambil cuti diluar tanggungan Negara;
e) Pernyataan lain yang menjadi syarat kualifikasi yang tercantum dalam Dokumen Kualifikasi; dan
f) Pernyataan bahwa data kualifikasi yang diisikan dan dokumen penawaran yang disampaikan  benar, dan jika dikemudian hari ditemukan bahwa data/dokumen yang disampaikan tidak benar dan ada pemalsuan maka Direktur Utama/Pimpinan Perusahaan/Pimpinan Koperasi, atau Kepala Cabang, dari seluruh anggota Kemitraan bersedia dikenakan sanksi administratif, sanksi pencantuman dalam Daftar Hitam, gugatan secara perdata, dan/atau pelaporan secara pidana kepada pihak berwenang sesuai dengan ketentuan peraturan perundang undangan.
', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 7, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(75, 5, 'Tidak masuk dalam Daftar Hitam', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 8, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(76, 5, 'Dalam hal Peserta akan melakukan konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain harus mempunyai perjanjian konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 9, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(77, 6, 'Memiliki Pengalaman Pekerjaan:
a) Pekerjaan di bidang Jasa Konsultansi paling kurang number pekerjaan dalam kurun waktu 1 (satu)  tahun terakhir baik di lingkungan pemerintah maupun swasta, termasuk pengalaman subkontrak;
b) Pekerjaan yang sejenis berdasarkan jenis pekerjaan, kompleksitas pekerjaan, metodologi, teknologi, atau karakteristik lainnya yang bisa menggambarkan kesamaan, paling kurang number pekerjaan dalam kurun waktu 3 (tiga) tahun terakhir baik di lingkungan pemerintah maupun swasta, termasuk pengalaman subkontrak; dan
c) Nilai pekerjaan sejenis tertinggi dalam kurun waktu 10 (sepuluh) tahun terakhir paling kurang sama dengan 50% (lima puluh persen) nilai total HPS/Pagu Anggaran.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 1, '{"key":["number1", "number2"], "label":["Jumlah A", "Jumlah B"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(78, 6, 'Memiliki SDM Manajerial', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 1, 2, '{"key":["jenis_keahlian_tm", "keahlian_tm", "pengalaman_tm", "kemampuan_tm"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(79, 6, 'Memiliki SDM Tenaga Ahli', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 0, 3, '{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(80, 6, 'Memiliki SDM Tenaga Teknis', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 0, 4, '{"key":["jenis_kemampuan_tk", "kemampuan_teknis_tk", "pengalaman_tk", "kemampuan_managerial_tk"], "label":["Jenis Kemampuan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(81, 6, 'Memiliki kemampuan untuk menyediakan peralatan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 0, 5, '{"key":["nama_peralatan", "spesifikasi_peralatan"], "label":["Nama", "Spesifikasi"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(84, 7, 'Menyampaikan laporan keuangan tahun terakhir yang mencerminkan Total Ekuitas yang dilihat dari neraca keuangan.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 0, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(85, 7, 'Memiliki Sisa Kemampuan Nyata (SKN) paling kecil 50% (lima puluh persen) dari nilai Total HPS.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 1, 1, 0, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(86, 5, 'Memiliki TDP atau NIB', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(87, 5, 'Memiliki NPWP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(88, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(89, 5, 'Mempunyai atau menguasai tempat usaha/kantor dengan alamat yang benar, tetap dan jelas berupa milik sendiri atau sewa', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(90, 5, 'Secara hukum mempunyai kapasitas untuk mengikatkan diri pada Kontrak yang dibuktikan dengan:
a) Akta Pendirian Perusahaan dan/atau perubahannya; (akta perubahan bisa berlaku seluruhnya)
b) Surat Kuasa (apabila dikuasakan);
c) Bukti bahwa yang diberikan kuasa merupakan pegawai tetap (apabila dikuasakan); dan
d) KTP.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 6, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(91, 5, 'Surat Pernyataan:
a) Yang bersangkutan dan manajemennya tidak dalam pengawasan pengadilan, tidak pailit, dan kegiatan usahanya tidak sedang dihentikan;
b) Yang bersangkutan berikut Pengurus Badan Usaha tidak sedang dikenakan sanksi Daftar Hitam;
c) Yang bertindak untuk dan atas nama Badan Usaha tidak sedang dalam menjalani sanksi pidana;
d) pimpinan dan pengurus Badan Usaha bukan sebagai pegawai K/L/PD atau pimpinan dan pengurus Badan Usaha sebagai pegawai K/L/PD yang sedang mengambil cuti diluar tanggungan Negara;
e) Pernyataan lain yang menjadi syarat kualifikasi yang tercantum dalam Dokumen Kualifikasi; dan
f) Pernyataan bahwa data kualifikasi yang diisikan dan dokumen penawaran yang disampaikan  benar, dan jika dikemudian hari ditemukan bahwa data/dokumen yang disampaikan tidak benar dan ada pemalsuan maka Direktur Utama/Pimpinan Perusahaan/Pimpinan Koperasi, atau Kepala Cabang, dari seluruh anggota Kemitraan bersedia dikenakan sanksi administratif, sanksi pencantuman dalam Daftar Hitam, gugatan secara perdata, dan/atau pelaporan secara pidana kepada pihak berwenang sesuai dengan ketentuan peraturan perundang undangan.
', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 7, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(92, 5, 'Tidak masuk dalam Daftar Hitam', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 8, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(93, 5, 'Dalam hal Peserta akan melakukan konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain harus mempunyai perjanjian konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 9, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(94, 6, 'Memiliki Pengalaman Pekerjaan:
a) Penyediaan barang pada divisi yang sama paling kurang number pekerjaan dalam kurun waktu 1 (satu) tahun terakhir baik di lingkungan pemerintah maupun swasta, termasuk pengalaman subkontrak; dan
b) Penyediaan barang sekurang-kurangnya dalam kelompok/grup yang sama paling kurang number pekerjaan dalam kurun waktu 3 (tiga) tahun terakhir baik di lingkungan pemerintah maupun swasta, termasuk pengalaman subkontrak.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 1, 1, '{"key":["number1", "number2"], "label":["Jumlah A", "Jumlah B"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(95, 6, 'Memiliki SDM Tenaga Ahli', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 0, 2, '{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(96, 6, 'Memiliki SDM Tenaga Teknis', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 0, 3, '{"key":["jenis_kemampuan_tk", "kemampuan_teknis_tk", "pengalaman_tk", "kemampuan_managerial_tk"], "label":["Jenis Kemampuan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(97, 6, 'Memiliki kemampuan untuk menyediakan peralatan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 0, 4, '{"key":["nama_peralatan", "spesifikasi_peralatan"], "label":["Nama", "Spesifikasi"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(98, 6, 'Layanan purnajual', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 0, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(99, 7, 'Menyampaikan laporan keuangan tahun terakhir yang mencerminkan Total Ekuitas yang dilihat dari neraca keuangan.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 0, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(100, 7, 'Memiliki Sisa Kemampuan Nyata (SKN) paling kecil 50% (lima puluh persen) dari nilai Total HPS.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 0, 1, 0, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(101, 5, 'Memiliki KTP/Paspor/Surat Keterangan Domisili Tinggal', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 1, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(102, 5, 'Memiliki NPWP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 1, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(103, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 1, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(104, 5, 'Surat Pernyataan:
a) Tidak dikenakan sanksi Daftar Hitam;
b) Keikutsertaannya tidak menimbulkan pertentangan kepentingan pihak yang terkait;
c) Tidak dalam pengawasan pengadilan dan/atau sedang menjalani sanksi pidana; dan
d) Tidak berstatus sebagai ASN, kecuali yang bersangkutan mengambil cuti diluar tanggungan Negara.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 1, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(105, 6, 'Memiliki Pengalaman Pekerjaan:
a) Pekerjaan sejenis (jenis pekerjaan, kompleksitas pekerjaan, metodologi, teknologi, atau karakteristik lainnya yang bisa menggambarkan kesamaan); dan
b) Nilai pekerjaan sejenis tertinggi dalam kurun waktu 10 (sepuluh) tahun terakhir paling kurang sama dengan 50% (lima puluh persen) nilai total HPS/Pagu Anggaran.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 1, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(106, 6, 'Jenjang Pendidikan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 0, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(107, 6, 'Memiliki sertifikat keahlian/teknis', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 0, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(108, 6, 'Pernah mengikuti pelatihan/kursus', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 0, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(109, 6, 'Memiliki kompetensi sesuai bidangnya', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 0, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(110, 7, 'Laporan Keuangan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 0, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(111, 7, 'SKN/SKP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 4, 1, 0, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(112, 5, 'Memiliki TDP atau NIB', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(113, 5, 'Memiliki NPWP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(114, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(115, 5, 'Mempunyai atau menguasai tempat usaha/kantor dengan alamat yang benar, tetap dan jelas berupa milik sendiri atau sewa', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(116, 5, 'Secara hukum mempunyai kapasitas untuk mengikatkan diri pada Kontrak yang dibuktikan dengan:
a) Akta Pendirian Perusahaan dan/atau perubahannya; (akta perubahan bisa berlaku seluruhnya)
b) Surat Kuasa (apabila dikuasakan);
c) Bukti bahwa yang diberikan kuasa merupakan pegawai tetap (apabila dikuasakan); dan
d) KTP.', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 6, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(117, 5, 'Surat Pernyataan:
a) Yang bersangkutan dan manajemennya tidak dalam pengawasan pengadilan, tidak pailit, dan kegiatan usahanya tidak sedang dihentikan;
b) Yang bersangkutan berikut Pengurus Badan Usaha tidak sedang dikenakan sanksi Daftar Hitam;
c) Yang bertindak untuk dan atas nama Badan Usaha tidak sedang dalam menjalani sanksi pidana;
d) pimpinan dan pengurus Badan Usaha bukan sebagai pegawai K/L/PD atau pimpinan dan pengurus Badan Usaha sebagai pegawai K/L/PD yang sedang mengambil cuti diluar tanggungan Negara;
e) Pernyataan lain yang menjadi syarat kualifikasi yang tercantum dalam Dokumen Kualifikasi; dan
f) Pernyataan bahwa data kualifikasi yang diisikan dan dokumen penawaran yang disampaikan  benar, dan jika dikemudian hari ditemukan bahwa data/dokumen yang disampaikan tidak benar dan ada pemalsuan maka Direktur Utama/Pimpinan Perusahaan/Pimpinan Koperasi, atau Kepala Cabang, dari seluruh anggota Kemitraan bersedia dikenakan sanksi administratif, sanksi pencantuman dalam Daftar Hitam, gugatan secara perdata, dan/atau pelaporan secara pidana kepada pihak berwenang sesuai dengan ketentuan peraturan perundang undangan.
', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 7, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(118, 5, 'Tidak masuk dalam Daftar Hitam', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 8, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(119, 5, 'Dalam hal Peserta akan melakukan konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain harus mempunyai perjanjian konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 9, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(120, 6, 'Memiliki Pengalaman Pekerjaan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 1, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(121, 6, 'Memiliki SDM Tenaga Ahli', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 0, 2, '{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(122, 6, 'Memiliki SDM Tenaga Teknis', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 0, 3, '{"key":["jenis_kemampuan_tk", "kemampuan_teknis_tk", "pengalaman_tk", "kemampuan_managerial_tk"], "label":["Jenis Kemampuan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(123, 6, 'Memiliki kemampuan untuk menyediakan peralatan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 0, 4, '{"key":["nama_peralatan", "spesifikasi_peralatan"], "label":["Nama", "Spesifikasi"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(124, 6, 'Layanan purnajual', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 0, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(125, 7, 'Laporan Keuangan', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 0, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(126, 7, 'SKN/SKP', 'C', 'ADMIN', '2019-06-27 17:30:29.496', 2, 1, 0, 2, NULL);

UPDATE public.checklist_master SET ckm_nama = 'Surat Penawaran' WHERE ckm_id = 18;

UPDATE public.checklist_master SET ckm_nama= REPLACE(ckm_nama, 'terkahir', 'terakhir') WHERE ckm_nama LIKE '%terkahir%' ;

-- Tabel ekontrak.history_paket_pp untuk fitur ganti pejabat pengadaan di non tender
-- ################################################################
-- CREATE TABLE "history_paket_pp" ----------------------------
CREATE TABLE ekontrak.history_paket_pp (
	pp_id Numeric( 19, 0 ) NOT NULL,
	pkt_id Numeric( 19, 0 ) NOT NULL,
	peg_id Numeric( 19, 0 ) NOT NULL,
	tgl_perubahan Timestamp Without Time Zone NOT NULL,
	audittype Character( 1 ) DEFAULT 'C'::bpchar NOT NULL,
	audituser Character Varying( 100 ) DEFAULT 'ADMIN'::character varying NOT NULL,
	auditupdate Timestamp( 6 ) Without Time Zone DEFAULT now() NOT NULL,
	alasan Text,
	PRIMARY KEY ( pp_id, pkt_id ),
	CONSTRAINT pp_audittype_check CHECK((audittype = 'D'::bpchar) OR (audittype = 'C'::bpchar) OR (audittype = 'U'::bpchar)) );

ALTER TABLE ekontrak.history_paket_pp OWNER TO epns;

-- CREATE LINK "fk_history_paket_pp_pegawai" ------------------
ALTER TABLE ekontrak.history_paket_pp ADD CONSTRAINT fk_history_paket_pp_pegawai FOREIGN KEY ( peg_id ) REFERENCES public.pegawai ( peg_id ) MATCH SIMPLE ON DELETE Restrict ON UPDATE Cascade;
-- CREATE LINK "fk_history_paket_pp_paket" --------------------
ALTER TABLE ekontrak.history_paket_pp ADD CONSTRAINT fk_history_paket_pp_paket FOREIGN KEY ( pkt_id )	REFERENCES ekontrak.paket ( pkt_id ) MATCH SIMPLE	ON DELETE Restrict	ON UPDATE Cascade;

ALTER TABLE public.lelang_seleksi ADD COLUMN kirim_pengumuman_pemenang boolean NOT NULL DEFAULT false;
ALTER TABLE public.lelang_seleksi ADD COLUMN kirim_pengumuman_pemenang_pra boolean NOT NULL DEFAULT false;
UPDATE public.lelang_seleksi SET kirim_pengumuman_pemenang = 'true' FROM public.mail_queue WHERE public.lelang_seleksi.lls_id = public.mail_queue.lls_id AND jenis = 6;
UPDATE public.lelang_seleksi SET kirim_pengumuman_pemenang_pra = 'true' FROM public.mail_queue WHERE public.lelang_seleksi.lls_id = public.mail_queue.lls_id AND jenis = 3;


ALTER TABLE ekontrak.jenis_realisasi_swakelola ALTER COLUMN rsk_keterangan TYPE text;
ALTER TABLE ekontrak.jenis_realisasi_non_spk ALTER COLUMN rsk_keterangan TYPE text;


CREATE TABLE public.history_dok_penawaran
(
    dok_id numeric(19,0) NOT NULL,
    psr_id numeric(19,0) NOT NULL,
    audittype character(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 'C'::bpchar,
    audituser character varying(100) COLLATE pg_catalog."default" NOT NULL DEFAULT 'ADMIN'::character varying,
    auditupdate timestamp(6) without time zone NOT NULL DEFAULT now(),
    dok_judul text COLLATE pg_catalog."default",
    dok_tgljam timestamp(6) without time zone,
    dok_waktu_buka timestamp(6) without time zone,
    dok_hash character varying(40) COLLATE pg_catalog."default",
    dok_signature text COLLATE pg_catalog."default",
    dok_id_attachment numeric(19,0) NOT NULL,
    dok_jenis numeric(1,0) NOT NULL,
    dok_disclaim numeric(1,0) DEFAULT 0,
    dok_audit_key numeric(19,0),
    dok_waktu integer,
    dok_surat text COLLATE pg_catalog."default",
    dok_struk_nama text COLLATE pg_catalog."default",
    dok_struk_hash character varying(40) COLLATE pg_catalog."default",
    dok_struk_id numeric(19,0),
    CONSTRAINT pk_history_dok_penawaran PRIMARY KEY (dok_id),
    CONSTRAINT fk_history_dok_penawaran_peserta FOREIGN KEY (psr_id) REFERENCES public.peserta (psr_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE public.history_dok_penawaran OWNER to epns;
CREATE INDEX idx_history_dok_penawaran_auditupdate ON public.history_dok_penawaran USING btree (auditupdate);
CREATE INDEX idx_history_dok_penawaran_peserta ON public.history_dok_penawaran USING btree (psr_id, dok_jenis);
CREATE INDEX ind_history_dok_penawaran_blob_table ON public.history_dok_penawaran USING btree(dok_id_attachment);
CREATE INDEX ind_history_dok_penawaran_disclaime ON public.history_dok_penawaran USING btree(dok_disclaim);
CREATE INDEX ind_history_dok_penawaran_hash  ON public.history_dok_penawaran USING btree (dok_hash);
CREATE INDEX ind_history_dok_penawaran_jenis ON public.history_dok_penawaran USING btree(dok_jenis);
CREATE INDEX ind_history_dok_penawaran_peserta ON public.history_dok_penawaran USING btree(psr_id);


CREATE TABLE public.history_penawaran
(
    psr_id numeric(19,0) NOT NULL,
    versi integer NOT NULL DEFAULT 1,
    audittype character(1) COLLATE pg_catalog."default" NOT NULL DEFAULT 'C'::bpchar,
    audituser character varying(100) COLLATE pg_catalog."default" NOT NULL DEFAULT 'ADMIN'::character varying,
    auditupdate timestamp(6) without time zone NOT NULL DEFAULT now(),
    is_pemenang numeric(1,0) NOT NULL DEFAULT 0,
    psr_harga numeric(18,2),
    psr_harga_terkoreksi numeric(18,2),
    psr_uraian text COLLATE pg_catalog."default",
    psr_identitas text COLLATE pg_catalog."default",
    psr_pemilik text COLLATE pg_catalog."default",
    psr_pengurus text COLLATE pg_catalog."default",
    psr_dkh text COLLATE pg_catalog."default",
    is_pemenang_verif numeric(1,0),
    psr_alasan_menang text COLLATE pg_catalog."default",
    sudah_verifikasi_sikap numeric(1,0) DEFAULT 0,
    is_dikirim_pesan boolean DEFAULT false,
    sudah_evaluasi_kualifikasi boolean DEFAULT false,
    is_pemenang_klarifikasi numeric DEFAULT 0,
    CONSTRAINT pk_history_penawaran PRIMARY KEY (psr_id, versi)
);

ALTER TABLE public.history_penawaran OWNER to epns;
CREATE INDEX idx_history_penawaran_auditupdate ON public.history_penawaran USING btree (auditupdate);

ALTER TABLE ekontrak.workflow ADD CONSTRAINT pk_workflow PRIMARY KEY (wf_id);
ALTER TABLE ekontrak.wf_process_instance ADD CONSTRAINT pk_wf_process_instance PRIMARY KEY (process_instance_id);
ALTER TABLE ekontrak.wf_process_instance_log ADD CONSTRAINT pk_wf_process_instance_log PRIMARY KEY (logid);
ALTER TABLE ekontrak.wf_state_instance ADD CONSTRAINT pk_wf_state_instance PRIMARY KEY (state_instance_id);
ALTER TABLE ekontrak.wf_state_mapper_instance ADD CONSTRAINT pk_wf_state_mapper_instance PRIMARY KEY (state_mapper_instance_id);

UPDATE public.checklist_master SET ckm_checked = 1 WHERE ckm_id IN (50,51,52,53,54,55,56,57,58,60, 69,70,71,72,73,74,75,76,77,78,86,87,88,89,90,91,92,93,94,101,102,103,104,105,112,113,114,115,116,117,118,119,120);

ALTER TABLE kontrak DROP CONSTRAINT "fk_kontrak_ppk";
ALTER TABLE kontrak ADD CONSTRAINT "fk_kontrak_ppk" FOREIGN KEY (ppk_id) REFERENCES public.ppk (ppk_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE TABLE "public".checklist_tmp (
	chk_id numeric(19) NOT NULL,
	ckm_id numeric(19) NULL,
	audittype bpchar(1) NOT NULL DEFAULT 'C'::bpchar,
	audituser varchar(100) NOT NULL DEFAULT 'ADMIN'::character varying,
	auditupdate timestamp NOT NULL DEFAULT now(),
	chk_nama text NOT NULL,
	dll_id numeric(19) NULL,
	chk_klasifikasi text NULL,
	PRIMARY KEY (chk_id),
	FOREIGN KEY (ckm_id) REFERENCES checklist_master(ckm_id),
	FOREIGN KEY (dll_id) REFERENCES dok_lelang(dll_id)
) ;

CREATE INDEX idx_checklist_tmp_auditupdate ON public.checklist_tmp USING btree (auditupdate) ;
CREATE INDEX idx_checklist_tmp_jenis_dok_lelang ON public.checklist_tmp USING btree (dll_id, ckm_id) ;
CREATE INDEX idx_checklist_tmp_dok_lelang ON public.checklist_tmp USING btree (dll_id) ;
CREATE INDEX idx_checklist_tmp_master ON public.checklist_tmp USING btree (ckm_id) ;
CREATE SEQUENCE seq_checklist_tmp START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

ALTER TABLE public.checklist ADD COLUMN chk_versi numeric(18,0) NOT NULL DEFAULT 1;
INSERT INTO public.checklist_master(ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES (127, 3, 'Rincian Komponen Remunerasi Personel', 'C', 'ADMIN', NOW(), NULL, 1, 0, NULL, NULL);

alter table checklist_tmp owner to epns;
alter table seq_checklist_tmp owner to epns;
alter table seq_persetujuan_pemenang_express owner to epns;

--- alter column blob_log ---------
ALTER TABLE blob_log ALTER COLUMN userid TYPE character varying(80);
--- add colom sgh_id on sanggah_banding -------
ALTER TABLE sanggah_banding ADD COLUMN sgh_id numeric(19,0);
UPDATE sanggah_banding sb SET sgh_id = s.sgh_id FROM sanggahan s WHERE sb.psr_id = s.psr_id;
--- alter lelang_seleksi add column lls_evaluasi_ulang
ALTER TABLE public.lelang_seleksi ADD COLUMN lls_evaluasi_ulang numeric(1,0) DEFAULT 0;
UPDATE lelang_seleksi SET lls_evaluasi_ulang  = 1 WHERE (lls_penawaran_ulang is null OR lls_penawaran_ulang = 0) AND lls_id IN (SELECT lls_id FROM evaluasi WHERE eva_versi > 1 GROUP BY lls_id);
--- create table lelang_query ---------
DROP TABLE lelang_query;
CREATE TABLE lelang_query
(
  lls_id numeric(19,0),
  pkt_id numeric(19,0),
  pkt_nama text,
  pkt_flag numeric(1,0),
  pkt_hps numeric(18,2),
  pkt_hps_enable boolean,
  mtd_id numeric(19,0),
  mtd_pemilihan numeric(2,0),
  mtd_evaluasi numeric(1,0),
  kgr_id numeric(19,0),
  nama_instansi text,
  lls_penawaran_ulang numeric(1,0),
  lls_evaluasi_ulang numeric(1,0),
  kontrak_id numeric,
  kontrak_nilai numeric,
  is_pkt_konsolidasi boolean,
  anggaran text,
  lls_versi_lelang numeric(2,0),
  eva_versi numeric,
  lls_status numeric(1,0),
  jadwal_awal_pengumuman timestamp without time zone,
  jadwal_akhir timestamp without time zone,
  tgl_akhir_daftar timestamp without time zone,
  tsv tsvector,
  CONSTRAINT pk_lelang_query PRIMARY KEY (lls_id)
);
ALTER TABLE lelang_query OWNER TO epns;
INSERT INTO lelang_query SELECT l.lls_id,  p.pkt_id, pkt_nama, pkt_flag, pkt_hps, pkt_hps_enable, mtd_id, mtd_pemilihan, mtd_evaluasi, kgr_id, paket_instansi(p.pkt_id) as nama_instansi,lls_penawaran_ulang, lls_evaluasi_ulang,
(SELECT kontrak_id FROM kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_id,
(SELECT kontrak_nilai FROM kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_nilai, p.is_pkt_konsolidasi,
(SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, lls_versi_lelang,
(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) as eva_versi, l.lls_status, (SELECT dtj_tglawal FROM jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglawal ASC LIMIT 1) as jadwal_awal_pengumuman ,
(SELECT dtj_tglakhir FROM jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglakhir DESC LIMIT 1) as jadwal_akhir, (SELECT dtj_tglakhir FROM jadwal j, aktivitas a WHERE lls_id=l.lls_id AND j.akt_id=a.akt_id AND akt_urut=2 LIMIT 1) as tgl_akhir_daftar,
setweight(to_tsvector('english',coalesce(pkt_nama,'')), 'A') || setweight(to_tsvector('english',coalesce(paket_instansi(p.pkt_id),'')), 'B')
FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id WHERE lls_status=1;
CREATE INDEX lelang_query_lls_status_mtd_pemilihan_kgr_idx  ON lelang_query USING btree (lls_status, kgr_id);
CREATE INDEX idx_lelang_query_tsv ON lelang_query USING GIST (tsv);
CREATE INDEX lelang_query_jadwal_awal_pengumuman_idx ON lelang_query  USING btree(jadwal_awal_pengumuman);
CREATE INDEX lelang_query_jadwal_akhir_idx ON lelang_query  USING btree(jadwal_akhir);

DROP TABLE ekontrak.lelang_query;
CREATE TABLE ekontrak.lelang_query
(
  lls_id numeric(19,0),
  pkt_id numeric(19,0),
  pkt_nama text,
  pkt_flag numeric(1,0),
  pkt_hps numeric(18,2),
  mtd_pemilihan numeric(2,0),
  mtd_evaluasi numeric(1,0),
  kgr_id numeric(19,0),
  nama_instansi text,
  paket_satker text,
  kontrak_id numeric,
  kontrak_nilai numeric,
  is_pkt_konsolidasi boolean,
  anggaran text,
  lls_versi_lelang numeric(2,0),
  eva_versi numeric,
  lls_status numeric(1,0),
  jadwal_awal_pengumuman timestamp without time zone,
  jadwal_akhir timestamp without time zone,
  tgl_akhir_daftar timestamp without time zone,
  tsv tsvector,
  CONSTRAINT lelang_query_pkey PRIMARY KEY (lls_id)
);
ALTER TABLE ekontrak.lelang_query OWNER TO epns;
INSERT INTO ekontrak.lelang_query SELECT l.lls_id,  p.pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_pemilihan, mtd_evaluasi, kgr_id, ekontrak.paket_instansi(p.pkt_id) as nama_instansi,ekontrak.paket_satker(p.pkt_id) as nama_instansi,
(SELECT kontrak_id FROM ekontrak.kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_id,
(SELECT kontrak_nilai FROM ekontrak.kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_nilai, p.is_pkt_konsolidasi,
(SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, ekontrak.Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, lls_versi_lelang,
(SELECT MAX(eva_versi) FROM ekontrak.evaluasi WHERE lls_id=l.lls_id) as eva_versi, l.lls_status, (SELECT dtj_tglawal FROM ekontrak.jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglawal ASC LIMIT 1) as jadwal_awal_pengumuman ,
(SELECT dtj_tglakhir FROM ekontrak.jadwal WHERE lls_id=l.lls_id ORDER BY dtj_tglakhir DESC LIMIT 1) as jadwal_akhir,
(SELECT dtj_tglakhir FROM ekontrak.jadwal WHERE lls_id=l.lls_id AND thp_id =18833  LIMIT 1) as tgl_akhir_daftar,
setweight(to_tsvector('english',coalesce(pkt_nama,'')), 'A') || setweight(to_tsvector('english',coalesce(ekontrak.paket_instansi(p.pkt_id),'')), 'B')
FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id WHERE lls_status=1;
CREATE INDEX lelang_query_lls_status_mtd_pemilihan_kgr_idx  ON ekontrak.lelang_query USING btree (lls_status, kgr_id);
CREATE INDEX lelang_query_jadwal_awal_pengumuman_idx ON ekontrak.lelang_query  USING btree(jadwal_awal_pengumuman);
CREATE INDEX lelang_query_jadwal_akhir_idx ON ekontrak.lelang_query  USING btree(jadwal_akhir);
CREATE INDEX idx_lelang_query_tsv ON ekontrak.lelang_query USING GIST (tsv);

---DROP INDEX public.ind_checklist_master_id_jenis;
CREATE INDEX ind_checklist_master_id_jenis ON public.checklist_master USING btree (ckm_id ASC NULLS LAST, ckm_jenis ASC NULLS LAST);
CREATE INDEX ind_jadwal_akhir ON public.jadwal USING btree (dtj_tglakhir ASC NULLS LAST);
--- rename field groupid table usergroup : idgroup, agar v3 tidak bisa login------------------
ALTER TABLE public.usergroup RENAME COLUMN groupid TO idgroup;
ALTER TABLE usergroup ADD PRIMARY KEY (systemid, idgroup, userid);

ALTER TABLE ekontrak.nonlelang_seleksi ADD COLUMN "lls_flow_new" integer;

CREATE TABLE ekontrak.diskusi_lelang_pl (
	dsl_id_topik numeric(19) NOT NULL,
	psr_id numeric(19) NULL,
	lls_id numeric(19) NOT NULL,
	pnt_id numeric(19) NULL,
	audittype bpchar(1) NOT NULL DEFAULT 'C'::bpchar,
	audituser varchar(100) NOT NULL DEFAULT 'ADMIN'::character varying,
	auditupdate timestamp NOT NULL DEFAULT now(),
	dsl_dokumen text NULL,
	dsl_bab text NULL,
	dsl_uraian text NULL,
	dsl_tanggal timestamp NULL,
	dsl_id_attachment numeric(19) NULL,
	dsl_nama_file text NULL,
	ppk_id numeric(19) NULL,
	thp_id numeric(19) NOT NULL,
	dsl_pertanyaan_id numeric(19) NULL,
	dsl_jenis int2 NULL,
	CONSTRAINT diskusi_lelang_pkey PRIMARY KEY (dsl_id_topik)
);

INSERT INTO ekontrak.aktivitas_pl (akt_id, audittype, audituser, auditUPDATE, akt_jenis, akt_urut, akt_status)
VALUES (7, 'C', 'ADMIN', '2019-07-23 10:10:00', 'UMUM_PRAKUALIFIKASI', 1, 1),
(8, 'C', 'ADMIN', '2019-07-23 10:10:00', 'PEMASUKAN_DOK_PRA', 2, 1),
(9, 'C', 'ADMIN', '2019-07-23 10:10:00', 'EVALUASI_DOK_PRA', 3, 1),
(10, 'C', 'ADMIN', '2019-07-23 10:10:00', 'VERIFIKASI_KUALIFIKASI', 4, 1),
(11, 'C', 'ADMIN', '2019-07-23 10:10:00', 'PENETAPAN_HASIL_PRA', 5, 1),
(12, 'C', 'ADMIN', '2019-07-23 10:10:00', 'PENJELASAN', 6, 1),
(13, 'C', 'ADMIN', '2019-07-23 10:10:00', 'PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 11, 1);

INSERT INTO ekontrak.wf_process_definition (process_definition_id, audittype, auditUPDATE, audituser, createdate, status, "version", workflow_id, jsonvariable) 
VALUES (5, 'U', '2019-07-23 10:02:00', 'N/A', '2019-07-23 10:03:00', 1, 2, 'PENUNJUKAN_LANGSUNG', '{"NOW":{"name":"NOW","required":false},"LELANG_GAGAL":{"name":"LELANG_GAGAL","required":false},"LELANG_ULANG":{"name":"LELANG_ULANG","required":false},"PESERTA":{"name":"PESERTA","required":false},"LELANG_SELESAI":{"name":"LELANG_SELESAI","required":false}}');

UPDATE ekontrak.aktivitas_pl SET akt_urut = 7 WHERE akt_id = 1;
UPDATE ekontrak.aktivitas_pl SET akt_urut = 8 WHERE akt_id = 2;
UPDATE ekontrak.aktivitas_pl SET akt_urut = 9 WHERE akt_id = 3;
UPDATE ekontrak.aktivitas_pl SET akt_urut = 10 WHERE akt_id = 4;
UPDATE ekontrak.aktivitas_pl SET akt_urut = 12 WHERE akt_id = 5;
UPDATE ekontrak.aktivitas_pl SET akt_urut = 13 WHERE akt_id = 6;

INSERT INTO ekontrak.wf_state (state_id, audittype, auditUPDATE, audituser, createdate, description, fork, "name", process_definition_id)
VALUES ('5.START', 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, false, 'START', 5),
('5.UMUM_PRAKUALIFIKASI', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'UMUM_PRAKUALIFIKASI', 5),
('5.PEMASUKAN_DOK_PRA', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'PEMASUKAN_DOK_PRA', 5),
('5.EVALUASI_DOK_PRA', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'EVALUASI_DOK_PRA', 5),
('5.VERIFIKASI_KUALIFIKASI', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'VERIFIKASI_KUALIFIKASI', 5),
('5.PENETAPAN_HASIL_PRA', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'PENETAPAN_HASIL_PRA', 5),
('5.PENJELASAN', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'PENJELASAN', 5),
('5.PEMASUKAN_PENAWARAN', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'PEMASUKAN_PENAWARAN', 5),
('5.PEMBUKAAN_PENAWARAN', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'PEMBUKAAN_PENAWARAN', 5),
('5.EVALUASI_PENAWARAN', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'EVALUASI_PENAWARAN', 5),
('5.KLARIFIKASI_NEGOSIASI', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'KLARIFIKASI_NEGOSIASI', 5),
('5.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 5),
('5.TANDATANGAN_SPK', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'TANDATANGAN_SPK', 5),
('5.TANDATANGAN_SMPK', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'TANDATANGAN_SMPK', 5),
('5.GAGAL', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, true, 'GAGAL', 5),
('5.END', 'U', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', null, false, 'END', 5);

INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id)
VALUES (61 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.START', '5.UMUM_PRAKUALIFIKASI', 5),
(62 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.UMUM_PRAKUALIFIKASI', '5.PEMASUKAN_DOK_PRA', 5),
(63 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.PEMASUKAN_DOK_PRA', '5.EVALUASI_DOK_PRA', 5),
(64 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.EVALUASI_DOK_PRA', '5.VERIFIKASI_KUALIFIKASI', 5),
(65 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.VERIFIKASI_KUALIFIKASI', '5.PENETAPAN_HASIL_PRA', 5),
(66 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.PENETAPAN_HASIL_PRA', '5.PENJELASAN', 5),
(67 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.PENJELASAN', '5.PEMBUKAAN_PENAWARAN', 5),
(68 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.PEMBUKAAN_PENAWARAN', '5.EVALUASI_PENAWARAN', 5),
(69 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.EVALUASI_PENAWARAN', '5.KLARIFIKASI_NEGOSIASI', 5),
(70 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.KLARIFIKASI_NEGOSIASI', '5.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', 5),
(71 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', '5.TANDATANGAN_SPK', 5),
(72 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'NOW' , null, '5.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', '5.TANDATANGAN_SMPK', 5),
(73 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_SELESAI' , null, '5.TANDATANGAN_SPK', '5.END', 5),
(74 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_SELESAI' , null, '5.TANDATANGAN_SMPK', '5.END', 5),
(75 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_SELESAI' , null, '5.GAGAL', '5.END', 5),
(76 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.UMUM_PRAKUALIFIKASI', '5.GAGAL', 5),
(77 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.PEMASUKAN_DOK_PRA', '5.GAGAL', 5),
(78 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.EVALUASI_DOK_PRA', '5.GAGAL', 5),
(79 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.VERIFIKASI_KUALIFIKASI', '5.GAGAL', 5),
(80 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.PENETAPAN_HASIL_PRA', '5.GAGAL', 5),
(81 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.PENJELASAN', '5.GAGAL', 5),
(82 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.PEMBUKAAN_PENAWARAN', '5.GAGAL', 5),
(83 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.EVALUASI_PENAWARAN', '5.GAGAL', 5),
(84 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.KLARIFIKASI_NEGOSIASI', '5.GAGAL', 5),
(85 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR', '5.GAGAL', 5),
(86 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.TANDATANGAN_SPK', '5.GAGAL', 5),
(87 , 'C', '2019-07-23 10:38:00', 'N/A', '2019-07-23 10:38:00', 'LELANG_GAGAL' , null, '5.TANDATANGAN_SMPK', '5.GAGAL', 5);

-- Kolom baru di lelang_seleksi untuk fitur apakah akan menggunakan reverse auction
ALTER TABLE "public"."lelang_seleksi" ADD COLUMN use_reverse_auction boolean DEFAULT true;
ALTER TABLE "public"."lelang_seleksi" DROP COLUMN use_reverse_auction;

ALTER TABLE public.checklist_master ADD COLUMN ckm_versi numeric(18,0) NOT NULL DEFAULT 1;

ALTER TABLE ekontrak.nonlelang_seleksi ADD COLUMN mtd_kualifikasi integer;

CREATE TABLE public.jadwal_sanggah_banding (
    lls_id numeric(19) NOT NULL,
    auditupdate timestamp NOT NULL DEFAULT now(),
    sgh_waktu_mulai timestamp NULL,
    audittype bpchar(1) NOT NULL DEFAULT 'C'::bpchar,
    sgh_waktu_selesai timestamp NULL,
    audituser varchar(100) NOT NULL DEFAULT 'ADMIN'::character varying,
    jdwl_sgh_id numeric(19) NOT NULL,
    sgh_status numeric NULL DEFAULT 0,
    sgh_versi numeric NULL DEFAULT 1,
    sgh_alasan varchar(100) NULL,
    CONSTRAINT jadwal_sanggah_banding_pkey PRIMARY KEY (jdwl_sgh_id),
    CONSTRAINT jadwal_sanggah_banding_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES lelang_seleksi(lls_id)
);
ALTER TABLE public.jadwal_sanggah_banding OWNER TO epns;
CREATE SEQUENCE public.seq_jdwl_sgh START 1;


INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(128, 5, 'Memiliki NPWP', 'C', 'ADMIN', now(), 5, 1, 1, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(129, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', now(), 5, 1, 1, 3, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(130, 5, 'Secara hukum mempunyai kapasitas untuk mengikatkan diri pada Kontrak yang dibuktikan dengan:
a) Akta Pendirian Perusahaan dan/atau perubahannya; (akta perubahan bisa berlaku seluruhnya)
b) Surat Kuasa (apabila dikuasakan);
c) Bukti bahwa yang diberikan kuasa merupakan pegawai tetap (apabila dikuasakan); dan
d) KTP.', 'C', 'ADMIN', now(), 5, 1, 1, 4, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(131, 5, 'Surat Pernyataan:
a) Yang bersangkutan dan manajemennya tidak dalam pengawasan pengadilan, tidak pailit, dan kegiatan usahanya tidak sedang dihentikan;
b) Yang bersangkutan berikut Pengurus Badan Usaha tidak sedang dikenakan sanksi Daftar Hitam;
c) Yang bertindak untuk dan atas nama Badan Usaha tidak sedang dalam menjalani sanksi pidana;
d) pimpinan dan pengurus Badan Usaha bukan sebagai pegawai K/L/PD atau pimpinan dan pengurus Badan Usaha sebagai pegawai K/L/PD yang sedang mengambil cuti diluar tanggungan Negara;
e) Pernyataan lain yang menjadi syarat kualifikasi yang tercantum dalam Dokumen Kualifikasi; dan
f) Pernyataan bahwa data kualifikasi yang diisikan dan dokumen penawaran yang disampaikan  benar, dan jika dikemudian hari ditemukan bahwa data/dokumen yang disampaikan tidak benar dan ada pemalsuan maka Direktur Utama/Pimpinan Perusahaan/Pimpinan Koperasi, atau Kepala Cabang, dari seluruh anggota Kemitraan bersedia dikenakan sanksi administratif, sanksi pencantuman dalam Daftar Hitam, gugatan secara perdata, dan/atau pelaporan secara pidana kepada pihak berwenang sesuai dengan ketentuan peraturan perundang undangan.
', 'C', 'ADMIN', now(), 5, 1, 1, 5, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(132, 5, 'Tidak masuk dalam Daftar Hitam', 'C', 'ADMIN', now(), 5, 1, 1, 6, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(133, 5, 'Dalam hal Peserta akan melakukan konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain harus mempunyai perjanjian konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain', 'C', 'ADMIN', now(), 5, 1, 1, 7, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(134, 6, 'Memiliki pengalaman paling kurang 1 pekerjaan dalam kurun waktu 3 tahun terakhir, baik di lingkungan pemerintah maupun swasta termasuk pengalaman subkontrak, kecuali bagi pelaku usaha yang baru berdiri kurang dari 3 tahun', 'C', 'ADMIN', now(), 5, 1, 1, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(135, 6, 'Memiliki pengalaman mengerjakan pekerjaan sejenis berdasarkan klasifikasi text dan berdasarkan subklasifikasi text atau berdasarkan kesamaan jenis pekerjaan text;', 'C', 'ADMIN', now(), 5, 1, 1, 2, '{"key":["text1", "text2", "text3"], "label":["Text A", "Text B", "Text C"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(136, 6, 'Memiliki SDM Tenaga Tetap bersertifikat', 'C', 'ADMIN', now(), 5, 1, 0, 3, '{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}');
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(137, 6, 'Memiliki Sertifikat', 'C', 'ADMIN', now(), 5, 1, 0, 9, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(138, 7, 'Memiliki Sisa Kemampuan Paket (SKP)', 'C', 'ADMIN', now(), 5, 0, 0, 1, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header) VALUES
(139, 7, 'Memiliki Sisa Kemampuan Nyata (SKN)', 'C', 'ADMIN', now(), 5, 1, 0, 2, NULL);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(140, 5, 'Memiliki NPWP', 'C', 'ADMIN', NOW(), 2, 1, 1, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(141, 5, 'Telah Memenuhi kewajiban perpajakan tahun pajak terkahir (SPT Tahunan)', 'C', 'ADMIN', NOW(), 2, 1, 1, 3, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(142, 5, 'Secara hukum mempunyai kapasitas untuk mengikatkan diri pada Kontrak yang dibuktikan dengan:
a) Akta Pendirian Perusahaan dan/atau perubahannya; (akta perubahan bisa berlaku seluruhnya)
b) Surat Kuasa (apabila dikuasakan);
c) Bukti bahwa yang diberikan kuasa merupakan pegawai tetap (apabila dikuasakan); dan
d) KTP.', 'C', 'ADMIN', NOW(), 2, 1, 1, 4, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(143, 5, 'Surat Pernyataan:
a) Yang bersangkutan dan manajemennya tidak dalam pengawasan pengadilan, tidak pailit, dan kegiatan usahanya tidak sedang dihentikan;
b) Yang bersangkutan berikut Pengurus Badan Usaha tidak sedang dikenakan sanksi Daftar Hitam;
c) Yang bertindak untuk dan atas nama Badan Usaha tidak sedang dalam menjalani sanksi pidana;
d) pimpinan dan pengurus Badan Usaha bukan sebagai pegawai K/L/PD atau pimpinan dan pengurus Badan Usaha sebagai pegawai K/L/PD yang sedang mengambil cuti diluar tanggungan Negara;
e) Pernyataan lain yang menjadi syarat kualifikasi yang tercantum dalam Dokumen Kualifikasi; dan
f) Pernyataan bahwa data kualifikasi yang diisikan dan dokumen penawaran yang disampaikan  benar, dan jika dikemudian hari ditemukan bahwa data/dokumen yang disampaikan tidak benar dan ada pemalsuan maka Direktur Utama/Pimpinan Perusahaan/Pimpinan Koperasi, atau Kepala Cabang, dari seluruh anggota Kemitraan bersedia dikenakan sanksi administratif, sanksi pencantuman dalam Daftar Hitam, gugatan secara perdata, dan/atau pelaporan secara pidana kepada pihak berwenang sesuai dengan ketentuan peraturan perundang undangan.
', 'C', 'ADMIN', NOW(), 2, 1, 1, 5, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(144, 5, 'Tidak masuk dalam Daftar Hitam', 'C', 'ADMIN', NOW(), 2, 1, 1, 6, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(145, 5, 'Dalam hal Peserta akan melakukan konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain harus mempunyai perjanjian konsorsium/kerja sama operasi/kemitraan/bentuk kerjasama lain', 'C', 'ADMIN', NOW(), 2, 1, 1, 7, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(146, 6, 'Memiliki pengalaman paling kurang 1 pekerjaan dalam kurun waktu 3 tahun terakhir, baik di lingkungan pemerintah maupun swasta termasuk pengalaman subkontrak, kecuali bagi pelaku usaha yang baru berdiri kurang dari 3 tahun', 'C', 'ADMIN', now(), 2, 1, 1, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(147, 6, 'Memiliki Kemampuan Dasar (Diperuntukkan bagi Kualifikasi Usaha Menengah dan Besar)', 'C', 'ADMIN', NOW(), 2, 1, 1, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(148, 6, 'Memiliki SDM Tenaga Tetap bersertifikat', 'C', 'ADMIN', now(), 2, 1, 0, 3, '{"key":["jenis_keahlian_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Jenis Keahlian", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}', 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(149, 6, 'Memiliki SDM Tenaga Terampil Tetap bersertifikat', 'C', 'ADMIN', now(), 2, 1, 0, 4, '{"key":["jenis_keterampilan_tt", "keterampilan_tt", "pengalaman_tt", "kemampuan_tt"], "label":["Jenis Keterampilan", "Keterampilan/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}', 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(150, 6, 'Memiliki Sertifikat Manajemen Mutu, Sertifikat Manajemen Lingkungan, serta Sertifikat Keselamatan dan Kesehatan Kerja (hanya disyaratkan untuk Pekerjaan Konstruksi yang bersifat Kompleks/Berisiko Tinggi dan/atau diperuntukkan bagi Kualifikasi Usaha Besar)', 'C', 'ADMIN', NOW(), 2, 1, 0, 5, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(151, 7, 'Memiliki Sisa Kemampuan Paket (SKP)', 'C', 'ADMIN', now(), 2, 1, 0, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(152, 7, 'Memiliki Sisa Kemampuan Nyata (SKN)', 'C', 'ADMIN', now(), 2, 1, 0, 2, NULL, 2);

ALTER TABLE paket_sirup ADD COLUMN tanggal_pengumuman date;
ALTER TABLE ekontrak.paket_swakelola_sirup ADD COLUMN tanggal_pengumuman date;

ALTER TABLE ekontrak.checklist ADD COLUMN chk_versi numeric(18,0) NOT NULL DEFAULT 1;

INSERT INTO ekontrak.aktivitas_pl (akt_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES
(14, 'C', 'ADMIN', now(), 'PENETAPAN_PEMENANG_AKHIR', 14, 1);
INSERT INTO ekontrak.aktivitas_pl (akt_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES
(15, 'C', 'ADMIN', now(), 'PENGUMUMAN_PEMENANG_AKHIR', 15, 1);

DELETE FROM ekontrak.wf_state WHERE state_id = '5.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR';
DELETE FROM ekontrak.wf_state WHERE state_id = '5.TANDATANGAN_SPK';
DELETE FROM ekontrak.wf_state WHERE state_id = '5.TANDATANGAN_SMPK';

INSERT INTO ekontrak.wf_state (state_id, audittype, auditUPDATE, audituser, createdate, description, fork, "name", process_definition_id) VALUES 
('5.PENETAPAN_PEMENANG_AKHIR', 'C', NOW(), 'N/A', NOW(), null, true, 'PENETAPAN_PEMENANG_AKHIR', 5);
INSERT INTO ekontrak.wf_state (state_id, audittype, auditUPDATE, audituser, createdate, description, fork, "name", process_definition_id) VALUES
('5.PENGUMUMAN_PEMENANG_AKHIR', 'C', NOW(), 'N/A', NOW(), null, true, 'PENGUMUMAN_PEMENANG_AKHIR', 5);

DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 67;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 68;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 69;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 70;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 71;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 72;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 73;
DELETE FROM ekontrak.wf_state_mapper WHERE state_mapper_id = 74;

UPDATE ekontrak.wf_state_mapper SET state_id = '5.PEMASUKAN_PENAWARAN' WHERE state_mapper_id = 85;
UPDATE ekontrak.wf_state_mapper SET state_id = '5.PENETAPAN_PEMENANG_AKHIR' WHERE state_mapper_id = 86;
UPDATE ekontrak.wf_state_mapper SET state_id = '5.PENGUMUMAN_PEMENANG_AKHIR' WHERE state_mapper_id = 87;

INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(67 , 'C', now(), 'N/A', now(), 'NOW' , null, '5.PENJELASAN', '5.PEMASUKAN_PENAWARAN', 5);
INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(68 , 'C', now(), 'N/A', now(), 'NOW' , null, '5.PEMASUKAN_PENAWARAN', '5.PEMBUKAAN_PENAWARAN', 5);
INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(69 , 'C', now(), 'N/A', now(), 'NOW' , null, '5.PEMBUKAAN_PENAWARAN', '5.EVALUASI_PENAWARAN', 5);
INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(70 , 'C', now(), 'N/A', now(), 'NOW' , null, '5.EVALUASI_PENAWARAN', '5.KLARIFIKASI_NEGOSIASI', 5);
INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(71 , 'C', now(), 'N/A', now(), 'NOW' , null, '5.KLARIFIKASI_NEGOSIASI', '5.PENETAPAN_PEMENANG_AKHIR', 5);
INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(72 , 'C', now(), 'N/A', now(), 'NOW' , null, '5.PENETAPAN_PEMENANG_AKHIR', '5.PENGUMUMAN_PEMENANG_AKHIR', 5);
INSERT INTO ekontrak.wf_state_mapper (state_mapper_id, audittype, auditUPDATE, audituser, createdate, "condition", postvisitscript, state_id, next_state_id, process_definition_id) VALUES
(73 , 'C', now(), 'N/A', now(), 'LELANG_SELESAI' , null, '5.PENETAPAN_PEMENANG_AKHIR', '5.END', 5);

INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(153, 1, 'Masa Berlaku Penawaran', 'C', 'ADMIN', now(), 2, 1, 1, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(154, 1, 'Surat Penawaran', 'C', 'ADMIN', now(), 2, 1, 1, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(155, 1, 'Jaminan Penawaran Asli', 'C', 'ADMIN', now(), 2, 1, 0, 3, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(156, 1, 'Surat Perjanjian Kerja Sama Operasi', 'C', 'ADMIN', now(), 2, 1, 0, 4, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(157, 2, 'Metode Pelaksanaan Pekerjaan', 'C', 'ADMIN', now(), 2, 1, 0, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(158, 2, 'Jangka Waktu Pelaksanaan Pekerjaan', 'C', 'ADMIN', now(), 2, 1, 0, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(159, 2, 'Daftar Peralatan Utama dan Bukti Kepemilikan/Bukti Sewa Beli/Surat Perjanjian Sewa', 'C', 'ADMIN', now(), 2, 1, 0, 3, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(160, 2, 'Daftar Personel Manajerial beserta daftar riwayat pengalaman kerja atau referensi kerja dari pemberi tugas dan Surat pernyataan kepemilikan sertifikat kompetensi kerja', 'C', 'ADMIN', now(), 2, 1, 1, 4, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(161, 2, 'Formulir Rencana Keselamatan Konstruksi (RKK) dan Pakta Komitmen Keselamatan Konstruksi', 'C', 'ADMIN', now(), 2, 1, 0, 5, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(162, 2, 'Daftar Bagian Pekerjaan yang Disubkontrakkan', 'C', 'ADMIN', now(), 2, 1, 0, 6, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(163, 3, 'Daftar Kuantitas Harga (untuk jenis kontrak Harga Satuan)', 'C', 'ADMIN', now(), 2, 1, 0, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(165, 3, 'Daftar Keluaran Harga (untuk jenis kontrak Lumsum)', 'C', 'ADMIN', now(), 2, 1, 0, 3, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(166, 3, 'Formulir rekapitulasi perhitungan TKDN', 'C', 'ADMIN', now(), 2, 1, 0, 4, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(167, 3, 'Daftar barang yang diimpor', 'C', 'ADMIN', now(), 2, 1, 0, 5, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(168, 1, 'Masa Berlaku Penawaran', 'C', 'ADMIN', now(),5 , 1, 1, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(169, 1, 'Surat Penawaran', 'C', 'ADMIN', now(), 5, 1, 1, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(170, 2, 'Pengalaman Perusahaan', 'C', 'ADMIN', now(), 5, 1, 0, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(171, 2, 'Proposal Teknis', 'C', 'ADMIN', now(), 5, 1, 0, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(172, 2, 'Kualifikasi Tenaga Ahli', 'C', 'ADMIN', now(), 5, 1, 0, 3, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(173, 3, 'Daftar Kuantitas Harga (untuk jenis kontrak Harga Satuan)', 'C', 'ADMIN', now(), 5, 1, 0, 1, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(174, 3, 'Daftar Keluaran Harga (untuk jenis kontrak Lumsum)', 'C', 'ADMIN', now(), 5, 1, 0, 2, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(175, 3, 'Surat pernyataan telah mematuhi peraturan perundang-undangan terkait standar remunerasi tenaga ahli', 'C', 'ADMIN', now(), 5, 1, 0, 3, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(176, 3, 'Rincian Komponen Remunerasi Personel', 'C', 'ADMIN', now(), 5, 1, 0, 4, NULL, 2);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(195, 2, 'Daftar Personel Manajerial beserta daftar riwayat pengalaman kerja atau referensi kerja dari pemberi tugas dan Surat pernyataan kepemilikan sertifikat kompetensi kerja', 'C', 'ADMIN', now(), 5, 1, 1, 4, NULL, 2);

UPDATE public.checklist_master SET ckm_versi = 0 WHERE ckm_id in (29, 995, 996, 997);

--KETERANGAN HARI KERJA 

ALTER TABLE public.aktivitas ADD keterangan varchar(500) NULL;

--55 PASCA_DUA_FILE
UPDATE public.aktivitas SET keterangan='paling kurang 5 (lima) hari kerja'WHERE mtd_id = 55 and akt_urut = 1;
UPDATE public.aktivitas SET keterangan='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 55 and akt_urut = 2;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal pengumuman Tender' WHERE mtd_id = 55 and akt_urut = 3;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan dan paling kurang 3 (tiga) hari kerja setelah Berita Acara Hasil Pemberian Penjelasan' WHERE mtd_id = 55 and akt_urut = 4;
UPDATE public.aktivitas SET keterangan='setelah masa  penyampaian Dokumen Penawaran berakhir ' WHERE mtd_id = 55 and akt_urut = 5;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi penawaran' WHERE mtd_id = 55 and akt_urut = 6;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah pengumuman peserta yang lulus evaluasi administrasi dan teknis' WHERE mtd_id = 55 and akt_urut = 7;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan ' WHERE mtd_id = 55 and akt_urut = 8;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah klarifikasi kualifikasi' WHERE mtd_id = 55 and akt_urut = 9;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah klarifikasi kualifikasi' WHERE mtd_id = 55 and akt_urut = 10;
UPDATE public.aktivitas SET keterangan='selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sanggah' WHERE mtd_id = 55 and akt_urut = 11;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 55 and akt_urut = 12;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 55 and akt_urut = 13;
--54 PASCA_SATU_FILE
UPDATE public.aktivitas SET keterangan='paling kurang 5 (lima) hari kerja' WHERE mtd_id = 54 and akt_urut = 1;
UPDATE public.aktivitas SET keterangan='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 54 and akt_urut = 2;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal pengumuman Tender' WHERE mtd_id = 54 and akt_urut = 3;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan dan paling kurang 3 (tiga) hari kerja setelah Berita Acara Hasil Pemberian Penjelasan' WHERE mtd_id = 54 and akt_urut = 4;
UPDATE public.aktivitas SET keterangan='Setelah masa penyampaian Dokumen Penawaran berakhir' WHERE mtd_id = 54 and akt_urut = 5;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 54 and akt_urut = 6;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 54 and akt_urut = 7;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah klarifikasi kualifikasi' WHERE mtd_id = 54 and akt_urut = 8;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah klarifikasi kualifikasi' WHERE mtd_id = 54 and akt_urut = 9;
UPDATE public.aktivitas SET keterangan='selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sanggah' WHERE mtd_id = 54 and akt_urut = 10;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 54 and akt_urut = 11;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 54 and akt_urut = 12;
--44 PRA_DUA_TAHAP
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah selesai masa sanggah kualifikasi jika tidak ada sanggah atau 1 (satu) hari kerja setelah semua sanggah dijawab' WHERE mtd_id=44 and akt_urut = 1;;
UPDATE public.aktivitas SET keterangan='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran ' WHERE mtd_id=44 and akt_urut = 2;;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal undangan Tender  ' WHERE mtd_id=44 and akt_urut = 3;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 4;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 5;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 6;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 7;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 8;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 9;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 10;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 11;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 12;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 13;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 14;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 15;;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi penawaran' WHERE mtd_id=44 and akt_urut = 16;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 17;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 18;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id=44 and akt_urut = 19;;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi ' WHERE mtd_id=44 and akt_urut = 20;;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi ' WHERE mtd_id=44 and akt_urut = 21;;
UPDATE public.aktivitas SET keterangan='Selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sanggah' WHERE mtd_id=44 and akt_urut = 22;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan ' WHERE mtd_id=44 and akt_urut = 23;;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan ' WHERE mtd_id=44 and akt_urut = 24;;
--42 PRA_DUA_FILE
UPDATE public.aktivitas SET keterangan='paling singkat 7 (tujuh) hari kerja' WHERE mtd_id = 42 and akt_urut = 1;
UPDATE public.aktivitas SET keterangan='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 42 and akt_urut = 2;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal pengumuman prakualifikasi' WHERE mtd_id = 42 and akt_urut = 3;
UPDATE public.aktivitas SET keterangan='paling singkat 3 (tiga) hari kerja setelah berakhirnya penayangan pengumuman prakualifikasi' WHERE mtd_id = 42 and akt_urut = 4;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 42 and akt_urut = 5;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 42 and akt_urut = 6;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah pembuktian kualifikasi' WHERE mtd_id = 42 and akt_urut = 7;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah pembuktian kualifikasi' WHERE mtd_id = 42 and akt_urut = 8;
UPDATE public.aktivitas SET keterangan='5 (lima) hari kerja setelah pengumuman hasil kualifikasi dan jawab sanggah disampaikan paling lambat 3 (tiga) hari setelah akhir masa sanggah' WHERE mtd_id = 42 and akt_urut = 9;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja sebelum batas akhir penyampaian dokumen penawaran' WHERE mtd_id = 42 and akt_urut = 10;
UPDATE public.aktivitas SET keterangan='paling singkat 3 (tiga) hari kerja sejak tanggal undangan Tender' WHERE mtd_id = 42 and akt_urut = 11;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 42 and akt_urut = 12;
UPDATE public.aktivitas SET keterangan='setelah masa  penyampaian Dokumen Penawaran berakhir' WHERE mtd_id = 42 and akt_urut = 13;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi penawaran' WHERE mtd_id = 42 and akt_urut = 14;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah  pengumuman peserta yang lulus evaluasi  administrasi dan teknis' WHERE mtd_id = 42 and akt_urut = 15;
UPDATE public.aktivitas SET keterangan='' WHERE mtd_id = 42 and akt_urut = 16;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 42 and akt_urut = 17;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 42 and akt_urut = 18;
UPDATE public.aktivitas SET keterangan='selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sanggah' WHERE mtd_id = 42 and akt_urut = 19;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 42 and akt_urut = 20;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 42 and akt_urut = 21;
--46 PRA_DUA_FILE_KUALITAS
UPDATE public.aktivitas SET keterangan='paling singkat 7 (tujuh) hari kerja' WHERE mtd_id = 46 and akt_urut = 1;
UPDATE public.aktivitas SET keterangan='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 46 and akt_urut = 2;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal pengumuman prakualifikasi ' WHERE mtd_id = 46 and akt_urut = 3;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 4;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 5;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 6;
UPDATE public.aktivitas SET keterangan='1 (satu) hari setelah pembuktian kualifikasi' WHERE mtd_id = 46 and akt_urut = 7;
UPDATE public.aktivitas SET keterangan='1 (satu) hari setelah pembuktian kualifikasi' WHERE mtd_id = 46 and akt_urut = 8;
UPDATE public.aktivitas SET keterangan='5 (lima) hari kerja setelah pengumuman hasil kualifikasi' WHERE mtd_id = 46 and akt_urut = 9;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja sebelum batas akhir penyampaian dokumen penawaran' WHERE mtd_id = 46 and akt_urut = 10;
UPDATE public.aktivitas SET keterangan='paling singkat 3 (tiga) hari kerja sejak tanggal undangan Seleksi' WHERE mtd_id = 46 and akt_urut = 11;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 12;
UPDATE public.aktivitas SET keterangan='setelah masa  penyampaian Dokumen Penawaran berakhir' WHERE mtd_id = 46 and akt_urut = 13;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 14;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi penawaran' WHERE mtd_id = 46 and akt_urut = 15;
UPDATE public.aktivitas SET keterangan='5 (lima) hari kerja setelah pengumuman pemenang dan jawaban sanggah disampaikan paling lambat 3 (tiga) hari kerja setelah masa sanggah berakhir' WHERE mtd_id = 46 and akt_urut = 16;
UPDATE public.aktivitas SET keterangan='setelah masa sanggah berakhir atau sanggah telah dijawab' WHERE mtd_id = 46 and akt_urut = 17;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 18;
UPDATE public.aktivitas SET keterangan='selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sangga' WHERE mtd_id = 46 and akt_urut = 19;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 46 and akt_urut = 20;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 21;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 22;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 46 and akt_urut = 23;
--47 PRA_DUA_FILE_( KUALITAS BIAYA/PAGU ANGGARAN/BIAYA TERENDAH)
UPDATE public.aktivitas SET keterangan='paling kurang 7 (tujuh) hari kerja' WHERE mtd_id = 47 and akt_urut =1;
UPDATE public.aktivitas SET keterangan='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 47 and akt_urut =2;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal undangan Tender ' WHERE mtd_id = 47 and akt_urut =3;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =4;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =5;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =6;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =7;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =8;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =9;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 47 and akt_urut =10;
UPDATE public.aktivitas SET keterangan='paling cepat 3 (tiga) hari kerja sejak tanggal undangan Seleksi' WHERE mtd_id = 47 and akt_urut =11;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =12;
UPDATE public.aktivitas SET keterangan='setelah masa  penyampaian Dokumen Penawaran berakhir' WHERE mtd_id = 47 and akt_urut =13;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =14;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi penawaran' WHERE mtd_id = 47 and akt_urut =15;
UPDATE public.aktivitas SET keterangan='1 (satu) hari setelah pengumuman hasil evaluasi administrasi dan teknis' WHERE mtd_id = 47 and akt_urut =16;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 47 and akt_urut =17;
UPDATE public.aktivitas SET keterangan='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 47 and akt_urut =18;
UPDATE public.aktivitas SET keterangan='selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sanggah' WHERE mtd_id = 47 and akt_urut =19;
UPDATE public.aktivitas SET keterangan='setelah masa sanggah berakhir' WHERE mtd_id = 47 and akt_urut =20;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =21;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =22;
UPDATE public.aktivitas SET keterangan='disesuaikan dengan kebutuhan' WHERE mtd_id = 47 and akt_urut =23;
--53 JASA_KONSULTANSI_PERORANGAN/PASCA_DUA_FILE_KUALITAS
UPDATE public.aktivitas SET keterangan ='paling kurang 5 (lima) hari kerja' WHERE mtd_id = 53 and akt_urut = 1;
UPDATE public.aktivitas SET keterangan ='sampai dengan 1 (satu) hari kerja sebelum batas akhir Penyampaian Dokumen Penawaran' WHERE mtd_id = 53 and akt_urut = 2;
UPDATE public.aktivitas SET keterangan ='paling cepat 3 (tiga) hari kerja sejak tanggal pengumuman Tender' WHERE mtd_id = 53 and akt_urut = 3;
UPDATE public.aktivitas SET keterangan ='disesuaikan dengan kebutuhan' WHERE mtd_id = 53 and akt_urut = 4;
UPDATE public.aktivitas SET keterangan ='Setelah masa penyampaian Dokumen Penawaran berakhir' WHERE mtd_id = 53 and akt_urut = 5;
UPDATE public.aktivitas SET keterangan ='disesuaikan dengan kebutuhan' WHERE mtd_id = 53 and akt_urut = 6;
UPDATE public.aktivitas SET keterangan ='1 (satu) hari kerja setelah evaluasi Penawaran' WHERE mtd_id = 53 and akt_urut = 7;
UPDATE public.aktivitas SET keterangan ='Selama 5 (lima) hari kerja setelah pengumuman Pemenang dan jawaban sanggah paling lambat 3 (tiga) hari kerja  setelah akhir masa sanggah' WHERE mtd_id = 53 and akt_urut = 8;
UPDATE public.aktivitas SET keterangan ='disesuaikan dengan kebutuhan' WHERE mtd_id = 53 and akt_urut = 9;
UPDATE public.aktivitas SET keterangan ='disesuaikan dengan kebutuhan' WHERE mtd_id = 53 and akt_urut = 10;
UPDATE public.aktivitas SET keterangan ='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 53 and akt_urut = 11;
UPDATE public.aktivitas SET keterangan ='1 (satu) hari kerja setelah evaluasi' WHERE mtd_id = 53 and akt_urut = 12;
UPDATE public.aktivitas SET keterangan ='disesuaikan dengan kebutuhan' WHERE mtd_id = 53 and akt_urut = 13;
UPDATE public.aktivitas SET keterangan ='disesuaikan dengan kebutuhan' WHERE mtd_id = 53 and akt_urut = 14;


ALTER TABLE public.jadwal ADD CONSTRAINT jadwal_fk_unique UNIQUE (lls_id, akt_id);
ALTER TABLE public.peserta ADD CONSTRAINT peserta_fk_unique UNIQUE (lls_id, rkn_id);

UPDATE ekontrak.nonlelang_seleksi SET mtd_kualifikasi = 1 WHERE mtd_kualifikasi IS NULL;

INSERT INTO ekontrak.wf_state (state_id, audittype, auditUPDATE, audituser, createdate, description, fork, "name", process_definition_id) VALUES 
('5.TANDATANGAN_SPK', 'C', NOW(), 'N/A', NOW(), null, true, 'TANDATANGAN_SPK', 5);

CREATE TABLE ekontrak.checklist_tmp (
	chk_id numeric(19) NOT NULL,
	ckm_id numeric(19) NULL,
	audittype bpchar(1) NOT NULL DEFAULT 'C'::bpchar,
	audituser varchar(100) NOT NULL DEFAULT 'ADMIN'::character varying,
	auditupdate timestamp NOT NULL DEFAULT now(),
	chk_nama text NOT NULL,
	dll_id numeric(19) NULL,
	chk_klasifikasi text NULL,
	CONSTRAINT checklist_tmp_pkey PRIMARY KEY (chk_id),
	CONSTRAINT checklist_tmp_ckm_id_fkey FOREIGN KEY (ckm_id) REFERENCES checklist_master(ckm_id),
	CONSTRAINT checklist_tmp_dll_id_fkey FOREIGN KEY (dll_id) REFERENCES ekontrak.dok_nonlelang(dll_id)
);

CREATE INDEX idx_checklist_tmp_auditupdate ON ekontrak.checklist_tmp USING btree (auditupdate);
CREATE INDEX idx_checklist_tmp_dok_pl ON ekontrak.checklist_tmp USING btree (dll_id);
CREATE INDEX idx_checklist_tmp_jenis_dok_pl ON ekontrak.checklist_tmp USING btree (dll_id, ckm_id);
CREATE INDEX idx_checklist_tmp_master ON ekontrak.checklist_tmp USING btree (ckm_id);
CREATE SEQUENCE ekontrak.seq_checklistpl_tmp START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

alter table ekontrak.checklist_tmp owner to epns;
alter table ekontrak.seq_checklistpl_tmp owner to epns;
alter table seq_persetujuan_pemenang_express owner to epns;

CREATE TABLE "public".checklist_sanggah (
	cks_id numeric(19) NOT NULL,
	ckm_id numeric(19) NULL,
	audittype bpchar(1) NOT NULL DEFAULT 'C'::bpchar,
	audituser varchar(100) NOT NULL DEFAULT 'ADMIN'::character varying,
	auditupdate timestamp NOT NULL DEFAULT now(),
	sgh_id numeric(19) NULL,
	cks_versi numeric(18) NOT NULL DEFAULT 1,
	PRIMARY KEY (cks_id),
	FOREIGN KEY (ckm_id) REFERENCES checklist_master(ckm_id),
	FOREIGN KEY (sgh_id) REFERENCES sanggahan(sgh_id)
)
WITH (
	OIDS=FALSE
) ;
CREATE INDEX idx_checklist_sanggah_auditupdate ON public.checklist_sanggah USING btree (auditupdate) ;
CREATE INDEX idx_checklist_sanggah_with_master ON public.checklist_sanggah USING btree (sgh_id, ckm_id) ;
CREATE INDEX ind_checklist_sanggah ON public.checklist_sanggah USING btree (sgh_id) ;
CREATE INDEX ind_checklist_sanggah_master ON public.checklist_sanggah USING btree (ckm_id) ;
CREATE SEQUENCE seq_checklist_sanggah START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

alter table checklist_sanggah owner to epns;
alter table seq_checklist_sanggah owner to epns;

alter table checklist_master alter column ckm_jenis type numeric(2,0);

INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(177, 8, 'Kesalahan dalam melaksanakan evaluasi', 'C', 'ADMIN', now(), NULL, 1, 0, 1, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(178, 8, 'Penyimpangan terhadap ketentuan dan prosedur yang diatur dalam Peraturan Presiden Nomor 16 Tahun 2018 tentang Pengadaan Barang/Jasa Pemerintah dan ketentuan yang telah ditetapkan dalam Dokumen Pemilihan', 'C', 'ADMIN', now(), NULL, 1, 0, 2, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(179, 8, 'Rekayasa/persekongkolan sehingga menghalangi terjadinya persaingan usaha yang sehat', 'C', 'ADMIN', now(), NULL, 1, 0, 3, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(180, 8, 'Penyalahgunaan wewenang oleh Pokja Pemilihan, pimpinan UKPBJ, PPK, PA/KPA, dan/atau Kepala Daerah', 'C', 'ADMIN', now(), NULL, 1, 0, 4, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(181, 9, 'Indikasi terjadi persekongkolan', 'C', 'ADMIN', now(), NULL, 1, 0, 1, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(182, 9, 'Adanya persyaratan kualifikasi yang diskriminatif', 'C', 'ADMIN', now(), NULL, 1, 0, 2, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(183, 9, 'Kecurangan dalam pengumuman', 'C', 'ADMIN', now(), NULL, 1, 0, 3, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(184, 9, 'Tidak ada peserta yang menyampaikan Dokumen Kualifikasi', 'C', 'ADMIN', now(), NULL, 1, 0, 4, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(185, 9, 'Jumlah peserta yang lulus prakualifikasi kurang dari 3 peserta', 'C', 'ADMIN', now(), NULL, 1, 0, 5, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(186, 10, 'Terdapat kesalahan dalam proses evaluasi', 'C', 'ADMIN', now(), NULL, 1, 0, 1, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(187, 10, 'Tidak ada peserta yang menyampaikan dokumen penawaran setelah ada pemberian waktu perpanjangan', 'C', 'ADMIN', now(), NULL, 1, 0, 2, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(188, 10, 'Tidak ada peserta yang lulus evaluasi penawaran', 'C', 'ADMIN', now(), NULL, 1, 0, 3, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(189, 10, 'Ditemukan kesalahan dalam Dokumen Pemilihan atau Dokumen Pemilihan tidak sesuai dengan ketentuan dalam Peraturan Presiden Nomor 16 Tahun 2018 tentang Pengadaan Barang/Jasa Pemerintah beserta petunjuk ', 'C', 'ADMIN', now(), NULL, 1, 0, 4, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(190, 10, 'Seluruh peserta terlibat Korupsi, Kolusi, dan Nepotisme (KKN)', 'C', 'ADMIN', now(), NULL, 1, 0, 5, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(191, 10, 'Seluruh peserta terlibat persaingan usaha tidak sehat', 'C', 'ADMIN', now(), NULL, 1, 0, 6, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(192, 10, 'Seluruh penawaran harga pada Tender Barang/Pekerjaan Konstruksi/Jasa Lainnya di atas HPS', 'C', 'ADMIN', now(), NULL, 1, 0, 7, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(193, 10, 'Negosiasi biaya pada Seleksi tidak tercapai', 'C', 'ADMIN', now(), NULL, 1, 0, 8, NULL, 1);
INSERT INTO public.checklist_master (ckm_id, ckm_jenis, ckm_nama, audittype, audituser, auditupdate, kgr_id, ckm_status, ckm_checked, ckm_urutan, table_header, ckm_versi) VALUES
(194, 10, 'KKN melibatkan Pokja Pemilihan/PPK', 'C', 'ADMIN', now(), NULL, 1, 0, 9, NULL, 1);

CREATE TABLE public.checklist_gagal (
	cks_id numeric(19) NOT NULL,
	ckm_id numeric(19) NULL,
	audittype bpchar(1) NOT NULL DEFAULT 'C'::bpchar,
	audituser varchar(100) NOT NULL DEFAULT 'ADMIN'::character varying,
	auditupdate timestamp NOT NULL DEFAULT now(),
	lls_id numeric(19) NULL,
	cks_versi numeric(18) NOT NULL DEFAULT 1,
	CONSTRAINT checklist_gagal_pkey PRIMARY KEY (cks_id),
	CONSTRAINT checklist_gagal_ckm_id_fkey FOREIGN KEY (ckm_id) REFERENCES checklist_master(ckm_id),
	CONSTRAINT checklist_gagal_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES lelang_seleksi(lls_id)
);
CREATE INDEX idx_checklist_gagal_auditupdate ON public.checklist_gagal USING btree (auditupdate);
CREATE INDEX idx_checklist_gagal_with_master ON public.checklist_gagal USING btree (lls_id, ckm_id);
CREATE INDEX idx_checklist_gagal ON public.checklist_gagal USING btree (lls_id);
CREATE INDEX ind_checklist_gagal_master ON public.checklist_gagal USING btree (ckm_id);

CREATE SEQUENCE seq_checklist_gagal_pra START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;

alter table checklist_gagal owner to epns;
alter table seq_checklist_gagal_pra owner to epns;

-- Penyeragaman Length peg_namauser & rkn_namauser --
ALTER TABLE public.usergroup ALTER COLUMN userid TYPE varchar(80);

-- Penambahan Bentuk Usaha Perorangan: PerLKPP 9/2018, 3.4.1, poin b --
INSERT INTO public.bentuk_usaha (btu_id, audittype, audituser, auditupdate, btu_nama) VALUES ('09', 'C', 'ADMIN', '2020-01-21 14:00:00', 'Perorangan');
--history paket ppk di non tender---
CREATE TABLE ekontrak.history_paket_ppk
(
    ppk_id numeric NOT NULL,
    pkt_id numeric NOT NULL,
    peg_id numeric NOT NULL,
    tgl_perubahan timestamp without time zone NOT NULL,
    audittype character(1) NOT NULL DEFAULT 'C'::bpchar,
    audituser character varying(100) NOT NULL DEFAULT 'ADMIN'::character varying,
    auditupdate timestamp(6) without time zone NOT NULL DEFAULT now(),
    alasan text,
    CONSTRAINT pk_history_paket_ppk PRIMARY KEY (ppk_id, pkt_id),
    CONSTRAINT fk_history_paket_ppk_paket FOREIGN KEY (pkt_id)
        REFERENCES ekontrak.paket (pkt_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_history_paket_ppk_pegawai FOREIGN KEY (peg_id)
        REFERENCES public.pegawai (peg_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_history_paket_ppk_ppk FOREIGN KEY (ppk_id)
        REFERENCES public.ppk (ppk_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

ALTER TABLE ekontrak.history_paket_ppk OWNER to epns;
ALTER TABLE public.kontrak ADD kontrak_lingkup_pekerjaan text NULL;
ALTER TABLE public.sppbj ADD alamat_satker text null;

ALTER TABLE public.kontrak ADD kontrak_namapemilikrekening text NULL;
ALTER TABLE public.kontrak ADD alasanubah_kontrak_nilai text NULL;
ALTER TABLE public.sppbj ADD catatan_hargakontrak text null;

ALTER TABLE public.kontrak ADD no_surat_perjanjian text NULL;
ALTER TABLE public.kontrak ADD kota_surat_perjanjian text NULL;
ALTER TABLE public.kontrak ADD tanggal_surat_perjanjian Timestamp(6) Without Time Zone;
-- tiga kolom diatas sudah tidak digunakan kembali per 22 maret 2020
ALTER TABLE public.kontrak drop no_surat_perjanjian;
ALTER TABLE public.kontrak drop kota_surat_perjanjian;
ALTER TABLE public.kontrak drop tanggal_surat_perjanjian;

ALTER TABLE public.ba_pembayaran ADD jabatan_penandatangan_sk text NULL;
ALTER TABLE public.ba_pembayaran ADD kontrak_wakil_penyedia text NULL;
ALTER TABLE public.ba_pembayaran ADD kontrak_jabatan_wakil text NULL;
ALTER TABLE public.ba_pembayaran ALTER COLUMN besar_pembayaran TYPE numeric (19,2) NULL;

-- penambahan fields penyesuaian kontrak untuk kontrak PL
ALTER TABLE ekontrak.kontrak ADD kontrak_lingkup_pekerjaan text NULL;
ALTER TABLE ekontrak.kontrak ADD no_surat_perjanjian text NULL;
ALTER TABLE ekontrak.kontrak ADD kota_surat_perjanjian text NULL;
-- tiga kolom diatas sudah tidak digunakan kembali per 22 maret 2020
ALTER TABLE ekontrak.kontrak drop no_surat_perjanjian;
ALTER TABLE ekontrak.kontrak drop kota_surat_perjanjian;
ALTER TABLE ekontrak.kontrak drop tanggal_surat_perjanjian;

ALTER TABLE ekontrak.kontrak ADD tanggal_surat_perjanjian Timestamp(6) Without Time Zone;
ALTER TABLE ekontrak.kontrak ADD alasanubah_kontrak_nilai text NULL;
ALTER TABLE ekontrak.kontrak ADD kontrak_namapemilikrekening text NULL;
ALTER TABLE ekontrak.sppbj ADD alamat_satker text null;

ALTER TABLE ekontrak.ba_pembayaran ADD jabatan_penandatangan_sk text NULL;
ALTER TABLE ekontrak.ba_pembayaran ADD kontrak_wakil_penyedia text NULL;
ALTER TABLE ekontrak.ba_pembayaran ADD kontrak_jabatan_wakil text NULL;
ALTER TABLE ekontrak.ba_pembayaran ALTER COLUMN besar_pembayaran TYPE numeric (19,2) NULL;

-- 23 maret 2020
-- kebutuhan input jabatan ppk manual, dan ppk dari sppbj
ALTER TABLE ekontrak.kontrak ADD jabatan_ppk_kontrak text NULL;
ALTER TABLE public.kontrak ADD jabatan_ppk_kontrak text NULL;
ALTER TABLE public.kontrak ADD anggota_kso text NULL;
ALTER TABLE ekontrak.kontrak ADD anggota_kso text NULL;
ALTER TABLE public.sppbj ADD jabatan_ppk_sppbj text NULL;
ALTER TABLE ekontrak.sppbj ADD jabatan_ppk_sppbj text NULL;

-- attachment ra --
ALTER TABLE public.reverse_auction_detail ADD rad_id_attachment numeric NULL;

-- 26 maret 2020
-- tambahkan jumlah penawaran ke penawaranPL
ALTER TABLE ekontrak.PESERTA_NONLELANG ADD lama_pekerjaan numeric NULL;

-- 6 april 2020
-- persyaratan kualifikasi teknis perorangan
UPDATE public.checklist_master
SET ckm_nama='Memiliki Pengalaman Pekerjaan: 
a) Pekerjaan sejenis text ; dan 
b) Memiliki nilai pekerjaan sejenis tertinggi dalam kurun waktu 10 (sepuluh) tahun terakhir paling kurang sama dengan 50% (lima puluh persen) nilai total HPS/Pagu Anggaran.'
, table_header='{"key":["text1"], "label":["Text A"]}'
WHERE ckm_id=105;

UPDATE public.checklist_master
SET table_header='{"key":["tingkat_pendidikan_ta", "keahlian_ta", "pengalaman_ta", "kemampuan_ta"], "label":["Tingkat Pendidikan", "Keahlian/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}'
WHERE ckm_id=106;

UPDATE public.checklist_master
SET table_header='{"key":["jenis_kemampuan_tk", "kemampuan_teknis_tk", "pengalaman_tk", "kemampuan_managerial_tk"], "label":["Jenis Kemampuan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}'
WHERE ckm_id=107;

UPDATE public.checklist_master
SET table_header='{"key":["jenis_pelatihan_jp", "kemampuan_teknis_jp", "pengalaman_jp", "kemampuan_managerial_jp"], "label":["Jenis Pelatihan", "Kemampuan Teknis", "Pengalaman", "Kemampuan Manajerial"]}'
WHERE ckm_id=108;

UPDATE public.checklist_master
SET table_header='{"key":["jenis_keterampilan_tt", "keterampilan_tt", "pengalaman_tt", "kemampuan_tt"], "label":["Jenis Keterampilan", "Keterampilan/Spesifikasi", "Pengalaman", "Kemampuan Manajerial"]}'
WHERE ckm_id=109;

-- tgl 7 april penambahan filed hanya untuk konstruksi dan jk konstruksi
ALTER TABLE public.kontrak ADD no_skpemenang text NULL;
ALTER TABLE public.kontrak ADD tgl_skpemenang Timestamp(6) Without Time Zone;
ALTER TABLE ekontrak.kontrak ADD no_skpemenang text NULL;
ALTER TABLE ekontrak.kontrak ADD tgl_skpemenang Timestamp(6) Without Time Zone;

ALTER TABLE ekontrak.kontrak ADD kode_akun_kegiatan text NULL;
ALTER TABLE public.kontrak ADD kode_akun_kegiatan text NULL;
ALTER TABLE ekontrak.kontrak ADD lama_durasi_penyerahan1 numeric(19) NULL;
ALTER TABLE public.kontrak ADD lama_durasi_penyerahan1 numeric(19) NULL;
ALTER TABLE ekontrak.kontrak ADD lama_durasi_pemeliharaan numeric(19) NULL;
ALTER TABLE public.kontrak ADD lama_durasi_pemeliharaan numeric(19) NULL;

--30 April penambahan dokumen penawaran Konsultansi Perorangan
INSERT INTO public.checklist_master
(ckm_id, ckm_jenis, ckm_nama, kgr_id, ckm_status, ckm_versi, ckm_checked)
VALUES(196, 2, 'Proposal Teknis',4,1,1,0);
INSERT INTO public.checklist_master
(ckm_id, ckm_jenis, ckm_nama, kgr_id, ckm_status, ckm_versi, ckm_checked)
VALUES(197, 2, 'Kualifikasi Tenaga Ahli',4,1,1,0);