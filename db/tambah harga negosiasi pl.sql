-- Tambah kolom nev_harga_negosiasi di tabel nilai_evaluasi
ALTER TABLE "ekontrak"."nilai_evaluasi" ADD nev_harga_negosiasi numeric(18,2) DEFAULT NULL::numeric;
