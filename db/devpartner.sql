CREATE SCHEMA devpartner;

CREATE SEQUENCE devpartner.seq_panel
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE TABLE devpartner.panel
(
  pnl_id numeric NOT NULL,
  nama_panel character varying(255),
  keterangan text,
  member_details_id NUMERIC(19),
  contract_award_id NUMERIC(19),
  pnl_owner_id NUMERIC(19),
  pnl_size NUMERIC(18) DEFAULT 0,
  audittype Character Varying( 1 ),
  auditupdate Timestamp( 6 ) Without Time Zone,
  audituser Character Varying( 100 ),
  CONSTRAINT pk_panel PRIMARY KEY (pnl_id),
  CONSTRAINT fk_panel_owner FOREIGN KEY (pnl_owner_id)
      REFERENCES pegawai (peg_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);
ALTER TABLE devpartner.panel
  OWNER TO epns;
  
CREATE TABLE devpartner.panel_rekanan
(
  pnl_id numeric NOT NULL,
  rkn_id numeric NOT NULL,
  CONSTRAINT pk_panel_rekanan PRIMARY KEY (pnl_id, rkn_id),
  CONSTRAINT fk_panel_rekanan_panel FOREIGN KEY (pnl_id)
      REFERENCES devpartner.panel (pnl_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_panel_rekanan_rekanan FOREIGN KEY (rkn_id)
      REFERENCES rekanan (rkn_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);
ALTER TABLE devpartner.panel_rekanan
  OWNER TO epns;
  

CREATE OR REPLACE FUNCTION devpartner.tahap_now(id numeric, date timestamp without time zone)
  RETURNS character varying AS
$BODY$
DECLARE
	tahap character varying;
	awal timestamp without time zone;
	akhir timestamp without time zone;
BEGIN
   SELECT array_to_string(array_agg(a.akt_jenis), ',') INTO tahap FROM devpartner.jadwal_dp j, devpartner.aktivitas_dp a WHERE j.akt_id=a.akt_id AND j.lls_id= $1 AND dtj_tglawal <= $2 AND dtj_tglakhir >= $2;
   IF tahap IS NULL THEN
	SELECT dtj_tglawal INTO awal FROM devpartner.jadwal_dp WHERE lls_id=$1 ORDER BY dtj_tglawal ASC LIMIT 1;
	SELECT dtj_tglakhir INTO akhir FROM devpartner.jadwal_dp WHERE lls_id=$1 ORDER BY dtj_tglakhir DESC LIMIT 1;
	IF awal IS NOT NULL AND awal > $2 THEN
		tahap := 'BELUM_DILAKSANAKAN';
	ELSIF akhir IS NOT NULL AND akhir < $2 THEN	
		tahap := 'SUDAH_SELESAI';
	ELSE
		tahap := 'TIDAK_ADA_JADWAL';
	END IF;
   END IF;
   RETURN tahap;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

  
CREATE TABLE devpartner.paket_dp (
    pkt_id numeric(19,0) NOT NULL,
    stk_id numeric(19,0),
    pnt_id numeric(19,0),
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    pkt_nama text NOT NULL,
    pkt_pagu numeric(18,2) NOT NULL,
    pkt_hps numeric(18,2) NOT NULL,
    pkt_lokasi text,
    pkt_id_attachment numeric(19,0),
    pkt_tgl_realisasi timestamp(6) without time zone,
    pkt_tgl_assign timestamp(6) without time zone,
    pkt_tgl_buat timestamp(6) without time zone NOT NULL,
    pkt_tgl_ppk_setuju timestamp(6) without time zone,
    pkt_status numeric(1,0) DEFAULT 0 NOT NULL,
    kls_id character(2),
    pkt_flag numeric(1,0) DEFAULT 0 NOT NULL,
    kgr_id numeric(19,0) DEFAULT 0 NOT NULL,
    ppk_id numeric,
    pkt_no_spk text,
    rup_id numeric,
    ttp_ticket_id text,
    unspsc_id integer,
    pkt_hps_enable boolean DEFAULT true,
    id_ketua_ukpbj numeric(18,2) DEFAULT NULL::numeric,
    ukpbj_id numeric(18,2) DEFAULT NULL::numeric,
    is_pkt_konsolidasi boolean,
    pkt_id_konsolidasi numeric(19,0),
    pkt_tgl_build character varying(100)
);


ALTER TABLE devpartner.paket_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.paket_dp
    ADD CONSTRAINT paket_pkey PRIMARY KEY (pkt_id);

CREATE INDEX idx_paket_auditupdate ON devpartner.paket_dp USING btree (auditupdate);

CREATE INDEX ind_paket_kategori ON devpartner.paket_dp USING btree (kgr_id);

CREATE INDEX ind_paket_nama ON devpartner.paket_dp USING btree (pkt_nama);

CREATE INDEX ind_paket_panitia ON devpartner.paket_dp USING btree (pnt_id);

CREATE INDEX ind_paket_satker ON devpartner.paket_dp USING btree (stk_id);

ALTER TABLE ONLY devpartner.paket_dp
    ADD CONSTRAINT fk_paket_panitia FOREIGN KEY (pnt_id) REFERENCES public.panitia(pnt_id);

ALTER TABLE ONLY devpartner.paket_dp
    ADD CONSTRAINT fk_pkt_id_konsolidasi_to_pkt_id FOREIGN KEY (pkt_id_konsolidasi) REFERENCES devpartner.paket_dp(pkt_id) ON UPDATE CASCADE ON DELETE RESTRICT;


CREATE TABLE devpartner.lelang_dp (
    lls_id numeric(19,0) NOT NULL,
    pkt_id numeric(19,0) NOT NULL,
    mtd_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    lls_passinggrade numeric(5,2),
    lls_versi_lelang numeric(2,0) NOT NULL,
    lls_keterangan text,
    lls_dibuat_tanggal timestamp(6) without time zone NOT NULL,
    lls_diulang_karena text,
    lls_ditutup_karena text,
    lls_terverifikasi numeric(1,0) NOT NULL,
    lls_tgl_setuju timestamp(6) without time zone,
    lls_wf_id numeric(19,0),
    lls_status numeric(1,0) DEFAULT 1,
    lls_penetapan_pemenang numeric(1,0) DEFAULT 0,
    lls_kontrak_pembayaran smallint,
    lls_kontrak_tahun numeric(1,0),
    lls_kontrak_sbd numeric(2,0),
    lls_bobot_teknis numeric(5,2),
    lls_bobot_biaya numeric(5,2),
    lls_kontrak_pekerjaan smallint,
    lls_sesi smallint DEFAULT 0 NOT NULL,
    lls_dibuka_karena text,
    lls_penawaran_ulang numeric(1,0) DEFAULT 0,
    lls_metode_penawaran numeric(1,0),
    kirim_pengumuman_pemenang boolean DEFAULT false NOT NULL,
    kirim_pengumuman_pemenang_pra boolean DEFAULT false NOT NULL,
	allow_reschedule numeric(1,0) DEFAULT 0,
	allow_financial_opening numeric(1,0) DEFAULT 0,
    tanggal_pembukaan_harga timestamp(6) without time zone,
    peg_pembukaan_harga numeric(19,0) NULL
);


ALTER TABLE devpartner.lelang_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.lelang_dp
    ADD CONSTRAINT lelang_seleksi_pkey PRIMARY KEY (lls_id);

CREATE INDEX idx_lelang_seleksi_auditupdate ON devpartner.lelang_dp USING btree (auditupdate);

CREATE INDEX ind_lelang_metode ON devpartner.lelang_dp USING btree (mtd_id);

CREATE INDEX ind_lelang_paket ON devpartner.lelang_dp USING btree (pkt_id);


CREATE TABLE devpartner.evaluasi_dp (
    eva_id numeric(19,0) NOT NULL,
    lls_id numeric(19,0),
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    eva_jenis numeric(1,0) DEFAULT 0 NOT NULL,
    eva_versi numeric(2,0) NOT NULL,
    eva_tgl_minta timestamp(6) without time zone,
    eva_tgl_setuju timestamp(6) without time zone NOT NULL,
    eva_status numeric(1,0) DEFAULT 0 NOT NULL,
    eva_wf_id numeric(19,0),
    eva_attachment_id numeric(19,0)
);

ALTER TABLE devpartner.evaluasi_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.evaluasi_dp
    ADD CONSTRAINT evaluasi_lls_id_eva_jenis_eva_versi_key UNIQUE (lls_id, eva_jenis, eva_versi);

ALTER TABLE ONLY devpartner.evaluasi_dp
    ADD CONSTRAINT evaluasi_pkey PRIMARY KEY (eva_id);

CREATE INDEX idx_evaluasi_auditupdate ON devpartner.evaluasi_dp USING btree (auditupdate);

CREATE INDEX ind_evaluasi_jenis ON devpartner.evaluasi_dp USING btree (lls_id, eva_jenis, eva_versi);

CREATE UNIQUE INDEX un_eva ON devpartner.evaluasi_dp USING btree (lls_id, eva_jenis, eva_versi);

ALTER TABLE ONLY devpartner.evaluasi_dp
    ADD CONSTRAINT fk_evaluasi_eva_llg_lelang_s FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id);

	
CREATE SEQUENCE devpartner.seq_anggaran_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE devpartner.anggaran_dp (
    stk_id numeric(19,0) NOT NULL,
    sbd_id text NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    ang_koderekening text NOT NULL,
    ang_nilai numeric(18,2) NOT NULL,
    ang_uraian text NOT NULL,
    ang_tahun numeric(4,0) NOT NULL,
    ang_id numeric(19,0) NOT NULL
);

ALTER TABLE devpartner.anggaran_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.anggaran_dp
    ADD CONSTRAINT anggaran_pkey PRIMARY KEY (ang_id);

CREATE INDEX idx_anggaran_auditupdate ON devpartner.anggaran_dp USING btree (auditupdate);

CREATE INDEX ind_anggaran_satker ON devpartner.anggaran_dp USING btree (stk_id);

CREATE INDEX ind_anggaran_sumber_dana ON devpartner.anggaran_dp USING btree (sbd_id);

CREATE INDEX ind_anggaran_tahun ON devpartner.anggaran_dp USING btree (ang_tahun);

ALTER TABLE ONLY devpartner.anggaran_dp
    ADD CONSTRAINT anggaran_stk_id_fkey FOREIGN KEY (stk_id) REFERENCES public.satuan_kerja(stk_id) ON UPDATE CASCADE ON DELETE RESTRICT;
	

CREATE TABLE devpartner.paket_anggaran_dp (
    pkt_id numeric(19,0) NOT NULL,
    ang_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    ppk_id numeric(18,0) DEFAULT NULL::numeric,
    ppk_jabatan numeric(1,0) DEFAULT NULL::numeric,
    rup_id numeric(64,0) DEFAULT NULL::numeric,
    pkt_ang_versi integer DEFAULT 1
);


ALTER TABLE devpartner.paket_anggaran_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.paket_anggaran_dp
    ADD CONSTRAINT pk_paket_anggaran PRIMARY KEY (pkt_id, ang_id);

CREATE INDEX idx_paket_anggaran_auditupdate ON devpartner.paket_anggaran_dp USING btree (auditupdate);

ALTER TABLE ONLY devpartner.paket_anggaran_dp
    ADD CONSTRAINT fk_paket_anggaran_paket FOREIGN KEY (pkt_id) REFERENCES devpartner.paket_dp(pkt_id);

ALTER TABLE ONLY devpartner.paket_anggaran_dp
    ADD CONSTRAINT fk_paket_anggaran_ppk FOREIGN KEY (ppk_id) REFERENCES public.ppk(ppk_id);

ALTER TABLE ONLY devpartner.paket_anggaran_dp
    ADD CONSTRAINT paket_anggaran_ang_id_fkey FOREIGN KEY (ang_id) REFERENCES devpartner.anggaran_dp(ang_id);

	
CREATE SEQUENCE devpartner.seq_evaluasi_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE SEQUENCE devpartner.seq_lelang_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
CREATE SEQUENCE devpartner.seq_paket_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE devpartner.paket_satker_dp (
    pks_id numeric(19,0) DEFAULT public.nextsequence('seq_paket_satker'::character varying) NOT NULL,
    pkt_id numeric(19,0) NOT NULL,
    stk_id numeric(19,0) NOT NULL,
    rup_id numeric(19,0),
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL
);

ALTER TABLE devpartner.paket_satker_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.paket_satker_dp
    ADD CONSTRAINT pk_paket_satker PRIMARY KEY (pks_id);

CREATE INDEX idx_paket_satker_pkt_id_stk_id ON devpartner.paket_satker_dp USING btree (pkt_id, stk_id);

CREATE INDEX idx_paket_satker_satker ON devpartner.paket_satker_dp USING btree (stk_id, pkt_id);

ALTER TABLE ONLY devpartner.paket_satker_dp
    ADD CONSTRAINT paket_satker_pkt_id_fkey FOREIGN KEY (pkt_id) REFERENCES devpartner.paket_dp(pkt_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY devpartner.paket_satker_dp
    ADD CONSTRAINT paket_satker_stk_id_fkey FOREIGN KEY (stk_id) REFERENCES public.satuan_kerja(stk_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE SEQUENCE devpartner.seq_paket_satker_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE devpartner.paket_ppk_dp (
    pkt_id numeric(19,0) NOT NULL,
    ppk_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    ppk_jabatan numeric(1,0)
);

ALTER TABLE devpartner.paket_ppk_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.paket_ppk_dp
    ADD CONSTRAINT paket_ppk_pkey PRIMARY KEY (pkt_id, ppk_id);

CREATE INDEX idx_paket_ppk_auditupdate ON devpartner.paket_ppk_dp USING btree (auditupdate);

ALTER TABLE ONLY devpartner.paket_ppk_dp
    ADD CONSTRAINT paket_ppk_ppk_id_fkey FOREIGN KEY (ppk_id) REFERENCES public.ppk(ppk_id) ON UPDATE CASCADE ON DELETE RESTRICT;

	
CREATE SEQUENCE devpartner.seq_paket_lokasi_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
  
CREATE TABLE devpartner.paket_lokasi_dp (
    pkl_id numeric(19,0) NOT NULL,
    pkt_id numeric(19,0) NOT NULL,
    kbp_id numeric(19,0),
    pkt_lokasi text,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    pkt_lokasi_versi integer DEFAULT 1
);

ALTER TABLE devpartner.paket_lokasi_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.paket_lokasi_dp
    ADD CONSTRAINT pk_paket_lokasi PRIMARY KEY (pkl_id);

CREATE INDEX idx_paket_lokasi_auditupdate ON devpartner.paket_lokasi_dp USING btree (auditupdate);

ALTER TABLE ONLY devpartner.paket_lokasi_dp
    ADD CONSTRAINT fk_paket_lokasi_paket FOREIGN KEY (pkt_id) REFERENCES devpartner.paket_dp(pkt_id);


CREATE TABLE devpartner.aktivitas_dp (
    akt_id numeric(19,0) NOT NULL,
    mtd_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    akt_jenis character varying(80),
    akt_urut numeric(3,0) NOT NULL,
    akt_status smallint
);

ALTER TABLE devpartner.aktivitas_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.aktivitas_dp
    ADD CONSTRAINT aktivitas_pkey PRIMARY KEY (akt_id);

CREATE INDEX idx_metode ON devpartner.aktivitas_dp USING btree (mtd_id);

CREATE SEQUENCE devpartner.seq_jadwal_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE devpartner.jadwal_dp (
    dtj_id numeric(19,0) NOT NULL,
    thp_id numeric(19,0) NOT NULL,
    lls_id numeric(19,0),
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    dtj_tglawal timestamp(6) without time zone,
    dtj_tglakhir timestamp(6) without time zone,
    dtj_keterangan text,
    akt_id numeric(4,0)
);

ALTER TABLE devpartner.jadwal_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.jadwal_dp
    ADD CONSTRAINT jadwal_pkey PRIMARY KEY (dtj_id);

CREATE INDEX idx_jadwal_auditupdate ON devpartner.jadwal_dp USING btree (auditupdate);

CREATE INDEX idx_tahapnow ON devpartner.jadwal_dp USING btree (lls_id, dtj_tglawal, dtj_tglakhir, akt_id);

CREATE INDEX ind_jadwal_lelang ON devpartner.jadwal_dp USING btree (lls_id);

CREATE INDEX ind_jadwal_tahap ON devpartner.jadwal_dp USING btree (thp_id);

CREATE INDEX ind_jadwal_tanggal ON devpartner.jadwal_dp USING btree (dtj_tglawal, dtj_tglakhir);

ALTER TABLE ONLY devpartner.jadwal_dp
    ADD CONSTRAINT fk_jadwal_aktivitas FOREIGN KEY (akt_id) REFERENCES devpartner.aktivitas_dp(akt_id);

ALTER TABLE ONLY devpartner.jadwal_dp
    ADD CONSTRAINT fk_jadwal_lelang_seleksi FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id);--


CREATE TABLE devpartner.persetujuan_dp (
    pst_id bigint NOT NULL,
    audittype character varying(1),
    auditupdate timestamp(6) without time zone,
    audituser character varying(100),
    pst_alasan text,
    pst_jenis integer,
    pst_status integer,
    pst_tgl_setuju timestamp(6) without time zone,
    lls_id bigint,
    peg_id bigint
);

ALTER TABLE devpartner.persetujuan_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.persetujuan_dp
    ADD CONSTRAINT persetujuan_pkey PRIMARY KEY (pst_id);

CREATE INDEX idx_persetujuan_auditupdate ON devpartner.persetujuan_dp USING btree (auditupdate);

CREATE INDEX idx_persetujuan_jenis ON devpartner.persetujuan_dp USING btree (pst_jenis);

CREATE INDEX idx_persetujuan_lelang ON devpartner.persetujuan_dp USING btree (lls_id);

CREATE INDEX idx_persetujuan_pegawai ON devpartner.persetujuan_dp USING btree (peg_id);

CREATE INDEX idx_persetujuan_status ON devpartner.persetujuan_dp USING btree (pst_status);

ALTER TABLE ONLY devpartner.persetujuan_dp
    ADD CONSTRAINT persetujuan_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY devpartner.persetujuan_dp
    ADD CONSTRAINT persetujuan_peg_id_fkey FOREIGN KEY (peg_id) REFERENCES public.pegawai(peg_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE SEQUENCE devpartner.seq_persetujuan_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;


CREATE TABLE devpartner.history_jadwal_dp (
    dtj_id numeric(19,0) NOT NULL,
    hjd_tanggal_edit timestamp(6) without time zone NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    hjd_tanggal_awal timestamp(6) without time zone,
    hjd_tanggal_akhir timestamp(6) without time zone,
    hjd_keterangan text
);

ALTER TABLE devpartner.history_jadwal_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.history_jadwal_dp
    ADD CONSTRAINT history_jadwal_pkey PRIMARY KEY (dtj_id, hjd_tanggal_edit);

ALTER TABLE ONLY devpartner.history_jadwal_dp
    ADD CONSTRAINT history_jadwal_dtj_id_fkey FOREIGN KEY (dtj_id) REFERENCES devpartner.jadwal_dp(dtj_id) ON UPDATE CASCADE ON DELETE RESTRICT;


CREATE TABLE devpartner.history_paket_pokja_dp (
    pnt_id numeric(19,0) NOT NULL,
    pkt_id numeric(19,0) NOT NULL,
    peg_id numeric(19,0) NOT NULL,
    tgl_perubahan timestamp without time zone NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    alasan text,
    CONSTRAINT pp_audittype_check CHECK (((audittype = 'D'::bpchar) OR (audittype = 'C'::bpchar) OR (audittype = 'U'::bpchar)))
);

ALTER TABLE devpartner.history_paket_pokja_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.history_paket_pokja_dp
    ADD CONSTRAINT history_paket_pokja_pkey PRIMARY KEY (pnt_id, pkt_id);

ALTER TABLE ONLY devpartner.history_paket_pokja_dp
    ADD CONSTRAINT fk_history_paket_pokja_paket FOREIGN KEY (pkt_id) REFERENCES devpartner.paket_dp(pkt_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY devpartner.history_paket_pokja_dp
    ADD CONSTRAINT fk_history_paket_pokja_panitia FOREIGN KEY (pnt_id) REFERENCES public.panitia(pnt_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY devpartner.history_paket_pokja_dp
    ADD CONSTRAINT fk_history_paket_pokja_pegawai FOREIGN KEY (peg_id) REFERENCES public.pegawai(peg_id) ON UPDATE CASCADE ON DELETE RESTRICT;

	
  
  
CREATE TABLE devpartner.peserta_dp (
    psr_id numeric(19,0) NOT NULL,
    rkn_id numeric(19,0) NOT NULL,
    lls_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    psr_tanggal timestamp(6) without time zone NOT NULL,
    psr_black_list numeric(1,0) NOT NULL,
    is_pemenang numeric(1,0) DEFAULT 0 NOT NULL,
    psr_harga numeric(18,2),
    psr_harga_terkoreksi numeric(18,2),
    psr_uraian text,
    psr_identitas text,
    psr_pemilik text,
    psr_pengurus text,
    psr_dkh text,
    is_pemenang_verif numeric(1,0),
    psr_alasan_menang text,
    sudah_verifikasi_sikap numeric(1,0) DEFAULT 0,
    is_dikirim_pesan boolean DEFAULT false,
    sudah_evaluasi_kualifikasi boolean DEFAULT false,
    is_pemenang_klarifikasi numeric DEFAULT 0
);

ALTER TABLE devpartner.peserta_dp OWNER TO postgres;

COMMENT ON COLUMN devpartner.peserta_dp.is_pemenang IS '0,1';

ALTER TABLE ONLY devpartner.peserta_dp
    ADD CONSTRAINT peserta_pkey PRIMARY KEY (psr_id);

CREATE INDEX idx_peserta_auditupdate ON devpartner.peserta_dp USING btree (auditupdate);

CREATE INDEX ind_peserta_lelang ON devpartner.peserta_dp USING btree (lls_id);

CREATE INDEX ind_peserta_rekanan ON devpartner.peserta_dp USING btree (rkn_id);

ALTER TABLE ONLY devpartner.peserta_dp
    ADD CONSTRAINT peserta_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY devpartner.peserta_dp
    ADD CONSTRAINT peserta_rkn_id_fkey FOREIGN KEY (rkn_id) REFERENCES public.rekanan(rkn_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE SEQUENCE devpartner.seq_peserta_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
  
CREATE TABLE devpartner.nilai_evaluasi_dp (
    nev_id numeric(19,0) NOT NULL,
    eva_id numeric(19,0) NOT NULL,
    psr_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    nev_harga numeric(18,2),
    nev_harga_terkoreksi numeric(18,2),
    nev_skor numeric(20,2),
    nev_urutan numeric(5,0),
    nev_uraian text,
    nev_lulus numeric(1,0),
    nev_id_attachment numeric(19,0),
    nev_harga_negosiasi numeric(18,2) DEFAULT NULL::numeric
);

ALTER TABLE devpartner.nilai_evaluasi_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.nilai_evaluasi_dp
    ADD CONSTRAINT nilai_evaluasi_eva_id_psr_id_key UNIQUE (eva_id, psr_id);

ALTER TABLE ONLY devpartner.nilai_evaluasi_dp
    ADD CONSTRAINT nilai_evaluasi_pkey PRIMARY KEY (nev_id);

CREATE INDEX idx_nilai_evaluasi_auditupdate ON devpartner.nilai_evaluasi_dp USING btree (auditupdate);

CREATE INDEX ind_nilai_evaluasi_evaluasi ON devpartner.nilai_evaluasi_dp USING btree (eva_id);

CREATE INDEX ind_nilai_evaluasi_peserta ON devpartner.nilai_evaluasi_dp USING btree (psr_id);

CREATE UNIQUE INDEX un_nilai_eva ON devpartner.nilai_evaluasi_dp USING btree (eva_id, psr_id);

ALTER TABLE ONLY devpartner.nilai_evaluasi_dp
    ADD CONSTRAINT fk_nilai_ev_nil_psr_peserta FOREIGN KEY (psr_id) REFERENCES devpartner.peserta_dp(psr_id);

ALTER TABLE ONLY devpartner.nilai_evaluasi_dp
    ADD CONSTRAINT fk_nilai_evaluasi_evaluasi FOREIGN KEY (eva_id) REFERENCES devpartner.evaluasi_dp(eva_id);

CREATE SEQUENCE devpartner.seq_nilai_evaluasi_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE devpartner.dok_penawaran_dp (
    dok_id numeric(19,0) NOT NULL,
    psr_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    dok_judul text,
    dok_tgljam timestamp(6) without time zone,
    dok_waktu_buka timestamp(6) without time zone,
    dok_hash character varying(40),
    dok_signature text,
    dok_id_attachment numeric(19,0) NOT NULL,
    dok_jenis numeric(1,0) NOT NULL,
    dok_disclaim numeric(1,0) DEFAULT 0,
    dok_audit_key numeric(19,0),
    dok_waktu integer,
    dok_surat text
);

ALTER TABLE devpartner.dok_penawaran_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.dok_penawaran_dp
    ADD CONSTRAINT pk_dok_penawaran PRIMARY KEY (dok_id);

CREATE INDEX idx_dok_penawaran_auditupdate ON devpartner.dok_penawaran_dp USING btree (auditupdate);

CREATE INDEX ind_dok_penawaran_blob_table ON devpartner.dok_penawaran_dp USING btree (dok_id_attachment);

CREATE INDEX ind_dok_penawaran_disclaime ON devpartner.dok_penawaran_dp USING btree (dok_disclaim);

CREATE INDEX ind_dok_penawaran_hash ON devpartner.dok_penawaran_dp USING btree (dok_hash);

CREATE INDEX ind_dok_penawaran_jenis ON devpartner.dok_penawaran_dp USING btree (dok_jenis);

CREATE INDEX ind_dok_penawaran_peserta ON devpartner.dok_penawaran_dp USING btree (psr_id);

ALTER TABLE ONLY devpartner.dok_penawaran_dp
    ADD CONSTRAINT fk_dok_penawaran_peserta FOREIGN KEY (psr_id) REFERENCES devpartner.peserta_dp(psr_id);

CREATE SEQUENCE devpartner.seq_dok_penawaran_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

  
CREATE TABLE devpartner.paket_panitia_dp (
    pkt_id numeric(19,0) NOT NULL,
    pnt_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL
);

ALTER TABLE devpartner.paket_panitia_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.paket_panitia_dp
    ADD CONSTRAINT pk_paket_panitia PRIMARY KEY (pkt_id, pnt_id);

ALTER TABLE ONLY devpartner.paket_panitia_dp
    ADD CONSTRAINT fk_paket_pp_panitia_paket_panitia FOREIGN KEY (pnt_id) REFERENCES public.panitia(pnt_id);


CREATE TABLE devpartner.dok_lelang_dp (
    dll_id numeric(19,0) NOT NULL,
    lls_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    dll_nama_dokumen text,
    dll_jenis numeric(1,0) NOT NULL,
    dll_id_attachment numeric(19,0)
);

ALTER TABLE devpartner.dok_lelang_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.dok_lelang_dp
    ADD CONSTRAINT dok_lelang_pkey PRIMARY KEY (dll_id);

CREATE INDEX idx_dok_lelang_auditupdate ON devpartner.dok_lelang_dp USING btree (auditupdate);

CREATE INDEX ind_dok_lelang_jenis ON devpartner.dok_lelang_dp USING btree (dll_jenis);

CREATE INDEX ind_dok_lelang_lelang ON devpartner.dok_lelang_dp USING btree (lls_id);

ALTER TABLE ONLY devpartner.dok_lelang_dp
    ADD CONSTRAINT dok_lelang_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE SEQUENCE devpartner.seq_dok_lelang_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

  

CREATE TABLE devpartner.riwayat_persetujuan_dp (
    rwt_pst_id bigint NOT NULL,
    audittype character varying(1),
    auditupdate timestamp(6) without time zone,
    audituser character varying(100),
    pst_alasan text,
    pst_jenis integer,
    pst_status integer,
    pst_tgl_setuju timestamp(6) without time zone,
    lls_id bigint NOT NULL,
    peg_id bigint NOT NULL
);

ALTER TABLE ONLY devpartner.riwayat_persetujuan_dp
    ADD CONSTRAINT riwayat_persetujuan_pkey PRIMARY KEY (rwt_pst_id);

CREATE SEQUENCE devpartner.seq_riwayat_persetujuan_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  

CREATE TABLE devpartner.panel_lelang (
    lls_id numeric NOT NULL,
    pnl_id numeric NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE devpartner.panel_lelang OWNER TO epns;

ALTER TABLE ONLY devpartner.panel_lelang
    ADD CONSTRAINT pk_panel_lelang_seleksi_terdaftar PRIMARY KEY (lls_id, pnl_id);

ALTER TABLE ONLY devpartner.panel_lelang
    ADD CONSTRAINT fk_panel_lelang_panel FOREIGN KEY (pnl_id) REFERENCES devpartner.panel(pnl_id) ON UPDATE RESTRICT ON DELETE RESTRICT;

ALTER TABLE ONLY devpartner.panel_lelang
    ADD CONSTRAINT fk_panel_lelang_lelang_dp FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id) ON UPDATE RESTRICT ON DELETE RESTRICT;

  
-- EMPANELMENT
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (1, 1, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (2, 1, 'C', 'ADMIN', now(), 'CLARIFICATION_FORUM', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (3, 1, 'C', 'ADMIN', now(), 'UPLOAD_EOI', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (4, 1, 'C', 'ADMIN', now(), 'EVALUATION_OF_EOI', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (5, 1, 'C', 'ADMIN', now(), 'NOTIFICATION_OF_SELECTION', 5, 1);

-- CALL DOWN
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (6, 2, 'C', 'ADMIN', now(), 'SENDING_RFP_FOR_PANEL_MEMBERS', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (7, 2, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (8, 2, 'C', 'ADMIN', now(), 'PEMASUKAN_PENAWARAN', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (9, 2, 'C', 'ADMIN', now(), 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (10, 2, 'C', 'ADMIN', now(), 'PENGUMUMAN_PEMENANG_ADM_TEKNIS', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (11, 2, 'C', 'ADMIN', now(), 'OPENING_OF_FINANCIAL_PROPOSALS', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (12, 2, 'C', 'ADMIN', now(), 'EVALUATION_OF_FINANCIAL_PROPOSALS', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (13, 2, 'C', 'ADMIN', now(), 'COMBINED_EVALUATION', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (14, 2, 'C', 'ADMIN', now(), 'FINAL_RANGKING', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (15, 2, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 10, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (16, 2, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 11, 1);

-- RCS 2 File QCBS
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (17, 3, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (18, 3, 'C', 'ADMIN', now(), 'UPLOAD_EOI_ICB', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (19, 3, 'C', 'ADMIN', now(), 'SHORTLIST_APPOINTMENT', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (20, 3, 'C', 'ADMIN', now(), 'SHORTLIST_ANNOUNCEMENT', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (21, 3, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (22, 3, 'C', 'ADMIN', now(), 'PEMASUKAN_PENAWARAN', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (23, 3, 'C', 'ADMIN', now(), 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (24, 3, 'C', 'ADMIN', now(), 'PENGUMUMAN_PEMENANG_ADM_TEKNIS', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (25, 3, 'C', 'ADMIN', now(), 'OPENING_OF_FINANCIAL_PROPOSALS', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (26, 3, 'C', 'ADMIN', now(), 'EVALUATION_OF_FINANCIAL_PROPOSALS', 10, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (27, 3, 'C', 'ADMIN', now(), 'COMBINED_EVALUATION', 11, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (28, 3, 'C', 'ADMIN', now(), 'FINAL_RANGKING', 12, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (29, 3, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 13, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (30, 3, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 14, 1);

-- RCS 2 File Elimination
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (31, 4, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (32, 4, 'C', 'ADMIN', now(), 'UPLOAD_EOI_ICB', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (33, 4, 'C', 'ADMIN', now(), 'SHORTLIST_APPOINTMENT', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (34, 4, 'C', 'ADMIN', now(), 'SHORTLIST_ANNOUNCEMENT', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (35, 4, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (36, 4, 'C', 'ADMIN', now(), 'PEMASUKAN_PENAWARAN', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (37, 4, 'C', 'ADMIN', now(), 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (38, 4, 'C', 'ADMIN', now(), 'PENGUMUMAN_PEMENANG_ADM_TEKNIS', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (39, 4, 'C', 'ADMIN', now(), 'OPENING_OF_FINANCIAL_PROPOSALS', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (40, 4, 'C', 'ADMIN', now(), 'EVALUATION_OF_FINANCIAL_PROPOSALS', 10, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (41, 4, 'C', 'ADMIN', now(), 'COMBINED_EVALUATION', 11, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (42, 4, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 12, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (43, 4, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 13, 1);

-- RCS 1 File Elimination
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (44, 5, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (45, 5, 'C', 'ADMIN', now(), 'UPLOAD_EOI_ICB', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (46, 5, 'C', 'ADMIN', now(), 'SHORTLIST_APPOINTMENT', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (47, 5, 'C', 'ADMIN', now(), 'SHORTLIST_ANNOUNCEMENT', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (48, 5, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (49, 5, 'C', 'ADMIN', now(), 'PEMASUKAN_PENAWARAN', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (50, 5, 'C', 'ADMIN', now(), 'PEMBUKAAN_PENAWARAN', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (51, 5, 'C', 'ADMIN', now(), 'COMBINED_EVALUATION_ONE_FILE', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (52, 5, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (53, 5, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 10, 1);

-- QBS
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (54, 6, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (55, 6, 'C', 'ADMIN', now(), 'UPLOAD_EOI_ICB', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (56, 6, 'C', 'ADMIN', now(), 'SHORTLIST_APPOINTMENT', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (57, 6, 'C', 'ADMIN', now(), 'SHORTLIST_ANNOUNCEMENT', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (58, 6, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (59, 6, 'C', 'ADMIN', now(), 'PEMASUKAN_PENAWARAN', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (60, 6, 'C', 'ADMIN', now(), 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (61, 6, 'C', 'ADMIN', now(), 'TECHNICAL_RANK', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (62, 6, 'C', 'ADMIN', now(), 'OPENING_OF_FINANCIAL_PROPOSALS', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (63, 6, 'C', 'ADMIN', now(), 'EVALUATION_OF_FINANCIAL_PROPOSALS', 10, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (64, 6, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 11, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (65, 6, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 12, 1);

-- CQS
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (66, 7, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (67, 7, 'C', 'ADMIN', now(), 'UPLOAD_EOI_ICB', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (68, 7, 'C', 'ADMIN', now(), 'SHORTLIST_APPOINTMENT', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (69, 7, 'C', 'ADMIN', now(), 'ISSUANCE_RFP', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (70, 7, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (71, 7, 'C', 'ADMIN', now(), 'PEMASUKAN_PENAWARAN', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (72, 7, 'C', 'ADMIN', now(), 'PEMBUKAAN_PENAWARAN', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (73, 7, 'C', 'ADMIN', now(), 'PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (74, 7, 'C', 'ADMIN', now(), 'EVALUATION_OF_FINANCIAL_PROPOSALS', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (75, 7, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 10, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (76, 7, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 11, 1);

-- POST QUALIFICATION
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (77, 8, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (78, 8, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (79, 8, 'C', 'ADMIN', now(), 'SUBMISSION_PROPOSALS', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (80, 8, 'C', 'ADMIN', now(), 'BID_OPENING', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (81, 8, 'C', 'ADMIN', now(), 'EVALUATION_PROPOSALS', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (82, 8, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (83, 8, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 7, 1);

-- PRE QUALIFICATION
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (84, 9, 'C', 'ADMIN', now(), 'PENGUMUMAN_LELANG', 1, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (85, 9, 'C', 'ADMIN', now(), 'SUBMISSION_PREQUALIFICATION_DOCUMENT', 2, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (86, 9, 'C', 'ADMIN', now(), 'DETERMINATION_PREQUALIFIED_FIRM', 3, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (87, 9, 'C', 'ADMIN', now(), 'ISSUANCE_BIDDING_DOCUMENT', 4, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (88, 9, 'C', 'ADMIN', now(), 'PREPROPOSAL_FORUM', 5, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (89, 9, 'C', 'ADMIN', now(), 'SUBMISSION_PROPOSALS', 6, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (90, 9, 'C', 'ADMIN', now(), 'BID_OPENING', 7, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (91, 9, 'C', 'ADMIN', now(), 'EVALUATION_PROPOSALS', 8, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (92, 9, 'C', 'ADMIN', now(), 'CONTRACT_NEGOTIATION', 9, 1);
INSERT INTO devpartner.aktivitas_dp (akt_id, mtd_id, audittype, audituser, auditupdate, akt_jenis, akt_urut, akt_status) VALUES (93, 9, 'C', 'ADMIN', now(), 'TANDATANGAN_KONTRAK', 10, 1);

CREATE TABLE devpartner.panel_undangan_lelang
(
  lls_id numeric NOT NULL,
  rkn_id numeric NOT NULL,
  CONSTRAINT pk_panel_undangan_lelang PRIMARY KEY (lls_id, rkn_id),
  CONSTRAINT fk_panel_undangan_lelang_lelang FOREIGN KEY (lls_id)
      REFERENCES devpartner.lelang_dp (lls_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT,
  CONSTRAINT fk_panel_undangan_lelang_rekanan FOREIGN KEY (rkn_id)
      REFERENCES rekanan (rkn_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT
);
ALTER TABLE devpartner.panel_undangan_lelang
  OWNER TO epns;
  
 CREATE TABLE devpartner.diskusi_lelang_dp (
    dsl_id_topik numeric(19,0) NOT NULL,
    psr_id numeric(19,0),
    lls_id numeric(19,0) NOT NULL,
    pnt_id numeric(19,0),
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    dsl_dokumen text,
    dsl_bab text,
    dsl_uraian text,
    dsl_tanggal timestamp(6) without time zone,
    dsl_id_attachment numeric(19,0),
    dsl_nama_file text,
    ppk_id numeric(19,0),
    thp_id numeric(19,0) NOT NULL,
    dsl_pertanyaan_id numeric(19,0),
    dsl_jenis smallint
);


ALTER TABLE devpartner.diskusi_lelang_dp OWNER TO postgres;

--
-- Name: diskusi_lelang diskusi_lelang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.diskusi_lelang_dp
    ADD CONSTRAINT diskusi_lelang_pkey PRIMARY KEY (dsl_id_topik);


--
-- Name: ind_diskusi_lelang_lelang; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_diskusi_lelang_lelang ON devpartner.diskusi_lelang_dp USING btree (lls_id);


--
-- Name: ind_diskusi_lelang_panitia; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_diskusi_lelang_panitia ON devpartner.diskusi_lelang_dp USING btree (pnt_id);


--
-- Name: ind_diskusi_lelang_peserta; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_diskusi_lelang_peserta ON devpartner.diskusi_lelang_dp USING btree (psr_id);


--
-- Name: ind_diskusi_lelang_ppk; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_diskusi_lelang_ppk ON devpartner.diskusi_lelang_dp USING btree (ppk_id);


--
-- Name: ind_diskusi_lelang_tahap; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_diskusi_lelang_tahap ON devpartner.diskusi_lelang_dp USING btree (thp_id);


--
-- Name: diskusi_lelang diskusi_lelang_lls_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.diskusi_lelang_dp
    ADD CONSTRAINT diskusi_lelang_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: diskusi_lelang diskusi_lelang_pnt_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.diskusi_lelang_dp
    ADD CONSTRAINT diskusi_lelang_pnt_id_fkey FOREIGN KEY (pnt_id) REFERENCES public.panitia(pnt_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: diskusi_lelang diskusi_lelang_ppk_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.diskusi_lelang_dp
    ADD CONSTRAINT diskusi_lelang_ppk_id_fkey FOREIGN KEY (ppk_id) REFERENCES public.ppk(ppk_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: diskusi_lelang diskusi_lelang_psr_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.diskusi_lelang_dp
    ADD CONSTRAINT diskusi_lelang_psr_id_fkey FOREIGN KEY (psr_id) REFERENCES devpartner.peserta_dp(psr_id) ON UPDATE CASCADE ON DELETE RESTRICT;


CREATE SEQUENCE devpartner.seq_diskusi_lelang_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
  
CREATE TABLE devpartner.berita_acara_dp (
    brc_id numeric(19,0) NOT NULL,
    lls_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL,
    brc_jenis_ba text NOT NULL,
    brc_id_attachment numeric(64,0),
    brt_no text,
    brt_tgl_evaluasi timestamp(6) without time zone,
    brt_info text
);


ALTER TABLE devpartner.berita_acara_dp OWNER TO postgres;

--
-- Name: berita_acara berita_acara_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.berita_acara_dp
    ADD CONSTRAINT berita_acara_pkey PRIMARY KEY (brc_id);


--
-- Name: ind_berita_acara_jenis; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_berita_acara_jenis ON devpartner.berita_acara_dp USING btree (brc_jenis_ba);


--
-- Name: ind_berita_acara_lelang; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ind_berita_acara_lelang ON devpartner.berita_acara_dp USING btree (lls_id);


--
-- Name: berita_acara berita_acara_lls_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devpartner.berita_acara_dp
    ADD CONSTRAINT berita_acara_lls_id_fkey FOREIGN KEY (lls_id) REFERENCES devpartner.lelang_dp(lls_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE SEQUENCE devpartner.seq_berita_acara_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
  
ALTER TABLE ONLY devpartner.peserta_dp 
	ADD COLUMN psr_jv_pq character varying(255) NULL,
    ADD COLUMN psr_jv_bid character varying(255) NULL,
	ADD COLUMN psr_jv_bid_pokja character varying(255) NULL,
    ADD COLUMN psr_jv_bid_confirm integer NULL,
    ADD COLUMN psr_jv_bid_auto_confirm timestamp(6) without time zone NULL;
	
ALTER TABLE ONLY devpartner.lelang_dp 
	DROP COLUMN allow_financial_opening,
	DROP COLUMN tanggal_pembukaan_harga,
	DROP COLUMN peg_pembukaan_harga;
	
ALTER TABLE ONLY devpartner.lelang_dp 
	ADD COLUMN sudah_pembukaan numeric(1,0) DEFAULT 0,
	ADD COLUMN tanggal_pembukaan timestamp(6) without time zone,
	ADD COLUMN peg_pembukaan numeric(19,0) NULL;
	
ALTER TABLE ONLY devpartner.nilai_evaluasi_dp 
	ADD COLUMN nev_gagal numeric(1,0) DEFAULT 0,
	ADD COLUMN nev_combined numeric(1,0) DEFAULT 0,
	ADD COLUMN nev_rank numeric(1,0) DEFAULT 0;
	
	
CREATE SEQUENCE devpartner.seq_jadwal_draft_dp
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE devpartner.jadwal_draft_dp (
    dtjd_id numeric(19,0) NOT NULL,
    dtjd_tglawal timestamp(6) without time zone,
    dtjd_tglakhir timestamp(6) without time zone,
    dtjd_keterangan text,
    dtj_id numeric(19,0) NOT NULL,
    audittype character(1) DEFAULT 'C'::bpchar NOT NULL,
    audituser character varying(100) DEFAULT 'ADMIN'::character varying NOT NULL,
    auditupdate timestamp(6) without time zone DEFAULT now() NOT NULL
);

ALTER TABLE devpartner.jadwal_draft_dp OWNER TO postgres;

ALTER TABLE ONLY devpartner.jadwal_draft_dp
    ADD CONSTRAINT jadwal_draft_pkey PRIMARY KEY (dtjd_id);

CREATE INDEX idx_jadwal_draft_auditupdate ON devpartner.jadwal_draft_dp USING btree (auditupdate);

CREATE INDEX ind_jadwal_draft_tanggal ON devpartner.jadwal_draft_dp USING btree (dtjd_tglawal, dtjd_tglakhir);

ALTER TABLE ONLY devpartner.jadwal_draft_dp
    ADD CONSTRAINT fk_jadwal_draft_jadwal FOREIGN KEY (dtj_id) REFERENCES devpartner.jadwal_dp(dtj_id);
