DROP TABLE IF EXISTS "public"."ukpbj";
CREATE TABLE "public"."ukpbj" (
"ukpbj_id" numeric(19) NOT NULL,
"agc_id" numeric(19) NOT NULL,
"kpl_unit_pemilihan_id" numeric(19) NOT NULL,
"audittype" char(1) COLLATE "default" DEFAULT 'C'::bpchar NOT NULL,
"audituser" varchar(100) COLLATE "default" DEFAULT 'ADMIN'::character varying NOT NULL,
"auditupdate" timestamp(6) DEFAULT now() NOT NULL,
"nama" text COLLATE "default" NOT NULL,
"alamat" text COLLATE "default",
"telepon" text COLLATE "default",
"fax" text COLLATE "default",
"tgl_daftar" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE)

;

CREATE SEQUENCE seq_ukpbj
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seq_ukpbj OWNER TO epns;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Indexes structure for table ukpbj
-- ----------------------------
CREATE INDEX "idx_ukpbj_auditupdate" ON "public"."ukpbj" USING btree ("auditupdate");
CREATE INDEX "ind_ukpbj_nama" ON "public"."ukpbj" USING btree ("nama");

-- ----------------------------
-- Primary Key structure for table ukpbj
-- ----------------------------
ALTER TABLE "public"."ukpbj" ADD PRIMARY KEY ("ukpbj_id");

-- ----------------------------
-- Foreign Key structure for table "public"."ukpbj"
-- ----------------------------
ALTER TABLE "public"."ukpbj" ADD FOREIGN KEY ("ukpbj_id") REFERENCES "public"."ukpbj" ("ukpbj_id") ON DELETE RESTRICT ON UPDATE CASCADE;
