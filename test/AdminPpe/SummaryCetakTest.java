package AdminPpe;

import models.lelang.SummaryLelang;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

public class SummaryCetakTest extends UnitTest {

    @Test
    public void cetak() {
        Long id = 7395013L;
        try {
            SummaryLelang.cetak(id);
        } catch (Exception e) {
            Logger.error(e, "Kesalahan cetak Summary report kode lelang %s", id);
        }

    }
}
