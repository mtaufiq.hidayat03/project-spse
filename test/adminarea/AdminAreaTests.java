package adminarea;

import models.agency.Pegawai;
import models.common.Berita;
import models.common.Sub_tag.JenisSubtag;
import org.junit.*;
import play.data.validation.Error;
import play.data.validation.Validation;
import play.data.validation.Validation.ValidationResult;
import play.test.UnitTest;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

public class AdminAreaTests extends UnitTest {
	Berita berita;
	Validation validation;
	@BeforeClass
	public static void setUpClass() {
	    System.out.println("@BeforeClass setUpClass");
	    
	}

	@Before
	public void setUp() {
		System.out.println("@Before setUp");
		berita = new Berita();
		validation = Validation.current();
	}
	
	@AfterClass
	public static void tearDownClass(){
	    System.out.println("@AfterClass tearDownClass");
	   
	}
	
	@After
	public void tearDown(){
		System.out.println("@After tearDown");	    
	}
	
	@Test
	public void A2_CheckBeritaFieldsValidity(){
		
		System.out.println(validation == null);
		Field[] fields = berita.getClass().getFields();
		for (Field field : fields) {
			ValidationResult valid = validation.valid(field.getName(),berita);			
			System.out.println("valid field:"+field.getName()+" status:"+valid.ok);			
		}		
		
		List<Error> errlist = validation.errors();		
		for (Error error : errlist) {
			System.out.println(error.message());
		}
		System.out.println("----------------------------------------------------");
		System.out.println("required berita title : "+validation.required(berita.brt_judul).error.message());
		System.out.println("fill required title =>");
		berita.brt_judul ="test judul req";
		System.out.println("berita valid after judul: "+validation.required( "brt_judul" ,berita).ok);
		System.out.println("----------------------------------------------------");
		System.out.println("berita valid : "+validation.valid(berita).ok);
		System.out.println("fill required data =>");
		fillBerita();
		System.out.println("berita valid after filled: "+validation.valid(berita).ok);
		System.out.println("----------------------------------------------------");
	}
	
	private void fillBerita(){
		JenisSubtag jenisSubtag = JenisSubtag.valueOf("BERITA");
		//found required fields on tabel db, so add required to property model Berita(sebelumnya tidak ada)
		berita.brt_judul = "test judul";
		berita.stg_id = jenisSubtag.toString();
		berita.brt_tanggal = new Date();
		berita.brt_isi = "content";
	}
	
	@Test
	public void A1_TestSaveBerita(){
		boolean res = false;				
		fillBerita();				
		if(validation.valid(berita).ok){
			boolean berhasil = (berita.save() > 0);
			res = berhasil;
			System.out.println( "berhasil simpan berita : "+berhasil );
			if(berhasil){				
				System.out.println( "hapus kembali sample test : "+ (Berita.delete("brt_id in ("+berita.brt_id+")") > 0) );				
			}
		}		
		assertTrue(res);
	}
	
	@Test
	public void C0_TestValidateAndSavePegawai(){
		String pass = "123456";
		Pegawai p = new Pegawai();
		System.out.println("PKEY : "+p.peg_id);				
		//required field
		p.peg_telepon="021877777";
		p.peg_nip="nipx566cc";
		p.peg_nama="siapaaja";
		p.peg_email="a@b.com";
		p.peg_no_sk="xxxxxx";
		p.peg_namauser="testsaja";
		
		System.out.println("pegawai valid:"+validation.valid(p).ok);;
		List<Error> errlist = validation.errors();		
		for (Error error : errlist) {
			System.out.println(error.message());
		}						
		try {
			Pegawai.simpan(p, false, pass, pass, "ADM_AGENCY");
			System.out.println("Simpan pegawai OK");			
			assertTrue(p!=null);
		} catch (Exception s) {
			// TODO Auto-generated catch block
			System.out.println("Simpan data GAGAL");
			s.printStackTrace();
			assertTrue(false);
		}
		
		
	}
	
}
