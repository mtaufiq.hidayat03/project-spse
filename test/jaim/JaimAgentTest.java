package jaim;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

public class JaimAgentTest extends UnitTest {

	@Test
	public void testCertificate() throws IOException
	{
		
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { 
		    new X509TrustManager() {     
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
		            return new X509Certificate[0];
		        } 
		        public void checkClientTrusted( 
		            java.security.cert.X509Certificate[] certs, String authType) {
		            } 
		        public void checkServerTrusted( 
		            java.security.cert.X509Certificate[] certs, String authType) {
		        	Logger.debug("[checkServerTrusted]", certs[0]);
		        }
		    } 
		}; 

		// Install the all-trusting trust manager
		try {
		    SSLContext sc = SSLContext.getInstance("SSL"); 
		    sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (GeneralSecurityException e) {
			e.printStackTrace();	
		} 
		// Now you can access an https URL without having the certificate in the truststore
		try { 
		    URL url = new URL("https://jaim.lkpp.go.id/public/bootstrap/css/bootstrap.css"); 
		    URLConnection conn=url.openConnection();
		    String str=IOUtils.toString(conn.getInputStream(), "UTF-8");
		    Logger.debug("[JaimAgentTest] status: %s, %s", url, StringUtils.left(str, 100));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} 
		
	}
}
