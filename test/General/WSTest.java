package General;

import models.common.ConfigurationDao;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Test;
import play.libs.WS;
import play.test.UnitTest;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class WSTest extends UnitTest {

	@Test
	public void timeoutTest() throws TimeoutException {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
//		String param = "https://sikap.lkpp.go.id/services/isCriteriaCreated?q=%2BP66btWuuwa%2BRCarP%2FdWJI8e9YZ%2F%2FTAjHC%2B1R9%2FFRzPfk2W8wP5S2ZRsQasuPRWXML2r5mxV2lPl46hWTR6V%2FKTwkXsaeD08NxmB1hX1xcSTl1dDls5XqeZS%2BkO%2FkXSzP0Z%2BXJI%2BFuOfuSXHDB%2BLrw%3D%3D";				
		try {
			String content = WS.url("http://lpse.papua.go.id").timeout(ConfigurationDao.CONNECTION_TIMEOUT).getAsync().get().getString();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		stopWatch.stop();
		System.out.println("elapsed :"+stopWatch.getTime());		
	}
}
