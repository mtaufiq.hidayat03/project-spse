package General;

import models.agency.Paket;
import models.lelang.Dok_lelang;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Dok_lelang_content;
import models.lelang.Lelang_seleksi;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

public class CopyDocTest extends UnitTest{

	@Test
	public void testCopyDoc(){
		
	}
	@Test
	public void testGetRHps(){
		Long idDest = (long) 626999;
		Long idSource = (long) 548999;
		
		String message = "Copy Dokumen Lelang : </br><ul>";

		//source
		
		Lelang_seleksi lelangSource = Lelang_seleksi.findById(idSource);
		Dok_lelang dok_lelangSource = Dok_lelang.findBy(lelangSource.lls_id, JenisDokLelang.DOKUMEN_LELANG);		
		Dok_lelang_content dok_lelang_contentSource = Dok_lelang_content.findBy(dok_lelangSource.dll_id);
		//dest
		Lelang_seleksi lelangDest = Lelang_seleksi.findById(idDest);
		Dok_lelang dok_lelangDest = Dok_lelang.findNCreateBy(lelangDest.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		Dok_lelang_content dok_lelang_contentDest = Dok_lelang_content.findNCreateBy(dok_lelangDest.dll_id, lelangDest);
		
		
		Paket paket = Paket.findById(lelangDest.pkt_id);
		Logger.info("aseplog =>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		Logger.info("aseplog idest "+idDest);
		Logger.info("aseplog idsou "+idSource);
		Logger.info("aseplog: ls=> lls_id "+lelangSource.lls_id);
		Logger.info("aseplog: doklel=> dll_id "+dok_lelangSource.dll_id);
		Logger.info("aseplog: dll_content => dll_id "+dok_lelang_contentSource.dll_id);
		Logger.info("aseplog: dll_content => dll_dkh "+dok_lelang_contentSource.dll_dkh);
		Logger.info("aseplog: dll_content => dkh "+dok_lelang_contentSource.dkh);
		Logger.info("=>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		if(dok_lelang_contentSource.dll_dkh != null && !dok_lelang_contentSource.dll_dkh.isEmpty()  ){
			
			if(dok_lelang_contentSource.dkh.total <= paket.pkt_pagu) {
				
				dok_lelang_contentDest.dll_dkh = dok_lelang_contentSource.dll_dkh;
				dok_lelang_contentDest.dkh = dok_lelang_contentSource.dkh;
				
				paket.pkt_hps = dok_lelang_contentDest.dkh.total;
				paket.save();
				
				dok_lelang_contentDest.dll_modified = true;
				
				message += "<li>Rincian Hps tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>";
			}else{
				message += "<li>Rincian Hps tidak tercopy melebihi pagu <span class='glyphicon glyphicon-remove-circle' ></span></li>";
			}
			
			dok_lelangDest.save();
			
		}else{
			message += "<li>Rincian Hps tidak tercopy (kosong) <span class='glyphicon glyphicon-remove-circle' ></span> </li>";
		}
		//new reqq
		Logger.info("aseplog: doclelcont is null? "+(dok_lelang_contentDest==null));
		if(dok_lelang_contentSource.dll_sbd_versi !=null)
			dok_lelang_contentDest.dll_sbd_versi = dok_lelang_contentSource.dll_sbd_versi;
		if(dok_lelang_contentSource.dll_versi !=null)
			dok_lelang_contentDest.dll_versi = dok_lelang_contentSource.dll_versi;
		
		if(dok_lelang_contentSource.dll_ldp != null){
			dok_lelang_contentDest.dll_ldp = dok_lelang_contentSource.dll_ldp;
			message += "<li>Dokumen LDP tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>";
		}else{
			message += "<li>Dokumen LDP tidak tercopy (kosong) <span class='glyphicon glyphicon-remove-circle' ></span> </li>";
		}
		
		if(dok_lelang_contentSource.dll_sskk != null){
			dok_lelang_contentDest.dll_sskk = dok_lelang_contentSource.dll_sskk;
			message += "<li>Rincian SSKK tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>";
		}else{
			message += "<li>Dokumen SSKK tidak tercopy (kosong) <span class='glyphicon glyphicon-remove-circle' ></span> </li>";
		}
		
		//dok_lelang_contentDest.dll_spek = dok_lelang_contentSource.dll_spek;
		//message += "<li>Dok. Spesifikasi dan Teknis tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>";
		//dok_lelang_contentDest.dll_content_html = dok_lelang_contentSource.dll_content_html;
		//dok_lelang_contentDest.dll_content_attachment = dok_lelang_contentSource.dll_content_attachment;
		
		dok_lelang_contentDest.save();
		message+="</ul>";
		Logger.info("aseplog : "+message);
	}
}
