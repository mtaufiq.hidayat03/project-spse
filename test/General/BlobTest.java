package General;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;
import play.test.UnitTest;

import java.util.List;

public class BlobTest extends UnitTest{

	@Test
	public void getDataFromBlobTable(){
//		BlobTable d = BlobTable.findById(11533,0);
//		System.out.println(d==null);
//		System.out.println(BaseModel.decrypt(d.blb_path));
//		String[] ls = BaseModel.decrypt(d.blb_path).split("\\n");
//		System.out.println(ls.length);
//		System.out.println(ls[2]);
	}
	
	@Test
	public void blobByIdTest(){
		BlobTable d = BlobTableDao.getLastById(63L);
		if(d == null)
			return;
		System.out.println(d.toString());
	}
	
	@Test
	public void blobFetchTest(){
		List<BlobTable> d = BlobTableDao.find("blb_id_content=? order by blb_versi", 63L).fetch(1);
		if(CollectionUtils.isEmpty(d))
			return;
		System.out.println(d.get(0).toString());
	}
}
