package General;

import ams.utils.AmsUtil;
import models.agency.Anggota_panitia;
import models.agency.Pegawai;
import models.osd.Certificate;
import models.osd.JenisCertificate;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;
import utils.osd.OSDUtil;

import java.util.List;

/**
 * unit test terkait certificate pokja dan penyedia
 */
public class CertificateTest extends UnitTest {

    //check validitas pokja
    @Test
    public void CheckValidCeritificate() {
        int countCert = 0;
        List<Anggota_panitia> list = Anggota_panitia.find("pnt_id=?", 1011L).fetch();
        for (Anggota_panitia anggota:list) {
            Pegawai pegawai = Pegawai.findById(anggota.peg_id);
            if (pegawai.cer_id!=null) {
                Certificate certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id, JenisCertificate.CERT_PANITIA);
                //			if(certificate==null){
                //				//jika sertifikat sedang proses renew, periksa versi sertifikat sebelumnya
                //				if(certificate.cer_jenis.equalsIgnoreCase(JenisCertificate.CSRR_PANITIA_RENEW))
                //					certificate = Certificate.getLastCertificateIfRenew(pegawai.cer_id, JenisCertificate.CERT_PANITIA);
                //			}

                if (certificate == null || certificate.cer_sn ==null) {
                    Logger.error("Terdapat anggota panitia tidak memiliki sertifikat!");
                }

                if (!AmsUtil.checkOCSPCertificate(certificate.getX509()).isValid()) {
                    Logger.error("Gagal melakukan validasi sertifikat pokja ke service OCSP atau sertifikat telah di-revoke!");
                }

                if (certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
                    if (!certificate.isCertExpired())
//                        countCert = countCert + certPanitiaValidDate(certificate, lelangItem, jadwalList);
                        countCert += 1;
                }
            }
            final boolean status = countCert == list.size();
            Logger.info( status ? "success" : "gagal");
        }
    }
}
