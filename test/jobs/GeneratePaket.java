package jobs;

import models.agency.Paket;
import models.lelang.Dok_lelang;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Jadwal;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import org.apache.commons.beanutils.BeanUtils;
import play.jobs.Job;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class GeneratePaket extends Job {

	private int rowCount;
	public GeneratePaket(int rowCount)
	
	{
		this.rowCount=rowCount;
	}
	
	//generate paket dlm jumlah besar
	
	public void doJob() throws IllegalAccessException, InvocationTargetException
	{
				
		Lelang_seleksi llsMaster=Lelang_seleksi.findById(95999l);
		Paket pktMaster=Paket.findById(llsMaster.pkt_id);
		
		for(int i=0;i<rowCount;i++)
		{
			Paket paketBaru=clonePaket(pktMaster);
			cloneLelang(llsMaster, paketBaru);
			if(i % 100==0)
				play.Logger.debug("[GeneratePaket] generating paket #%s", i);
		}
		play.Logger.debug("[GeneratePaket] DONE");
			
	}
	
//	@Test
	public void clearPaket()
	{
		List<Lelang_seleksi> list=Lelang_seleksi.find("audituser=?", "PLAY-AUTO-JOBS").fetch();
		for(Lelang_seleksi lls:list)
		{
				Paket.delete("pkt_id=?", lls.pkt_id);
				lls.delete();
			
		}
	}

	private void cloneLelang(Lelang_seleksi llsMaster, Paket paketBaru) throws IllegalAccessException, InvocationTargetException {
		// TODO Auto-generated method stub
		Lelang_seleksi lls=new Lelang_seleksi();
		BeanUtils.copyProperties(lls, llsMaster);
		lls.lls_id=null;
		lls.pkt_id=paketBaru.pkt_id;
		lls.save();
		
		//copy jadwal
		for(Jadwal jwl: llsMaster.getJadwalList())
		{
			Jadwal jwlNew=new Jadwal(); 
			BeanUtils.copyProperties(jwlNew, jwl);
			jwlNew.dtj_id=null;
			jwlNew.lls_id=lls.lls_id;
			jwlNew.save();
		}
		//copy peserta
		for(Peserta psr : llsMaster.getPesertaList())
		{
			Peserta psrNew=new Peserta();
			BeanUtils.copyProperties(psrNew, psr);
			psrNew.psr_id=null;
			psrNew.lls_id=lls.lls_id;
			psrNew.save();
		}
		
		//copy dokumen
		Dok_lelang dokLelang=new Dok_lelang();
		Dok_lelang dokLelangMaster=Dok_lelang.findBy(llsMaster.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		BeanUtils.copyProperties(dokLelang, dokLelangMaster);
		dokLelang.lls_id=lls.lls_id;
		dokLelang.dll_id=null;
		dokLelang.save();
		
	}

	private Paket clonePaket(Paket master) throws IllegalAccessException, InvocationTargetException {
		Paket paket=new Paket();
		BeanUtils.copyProperties(paket, master);
		paket.pkt_id=null;
		paket.audituser="DATA_GENERATOR";
		paket.save();
		
		
//		play.Logger.debug("[GeneratePaket] new pkt_id: %s", paket.pkt_id);
		return paket;
	}
}
