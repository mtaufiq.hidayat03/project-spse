package jobs;

import org.junit.Test;
import play.Logger;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.test.UnitTest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Arief on 07/18/17.
 */
public class BeritaAcaraJobTest extends UnitTest {

    @Test
    public void doTest() throws Exception
    {
        Logger.info("execute job test berita");
        ExecutorService EXEC = Executors.newCachedThreadPool();
        List<Callable<HttpResponse>> tasks = new ArrayList<>();
        for(int i=0;i<100;i++) {
            Logger.info("cetak ke %s", i);
            final String no = "1231230"+i;
            Callable<HttpResponse> c = new Callable<HttpResponse>() {
                @Override
                public HttpResponse call() throws Exception {
                    HttpResponse response =  WS.url("http://localhost:9000/spse4/berita_acara/2728999/cetak")
                            .setParameter("no", no)
                            .setParameter("tanggal", "18-07-2017 10:35")
                            .setParameter("jenis", "UPLOAD_BA_HASIL_LELANG").post();
                    Logger.info("response : %s", response.getStatus());
                    return response;
                }
            };
            tasks.add(c);
        }
        List<Future<HttpResponse>> results = EXEC.invokeAll(tasks);
    }
}
