package controllers.lelang;

import models.lelang.SummaryLelang;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

public class UtilityCtrTest extends UnitTest {

    @Test
    public void viewPDF() throws Exception {
        Long id = 57837114L; // kode tender
        try {
            SummaryLelang.cetak(id);
        } catch (Exception e) {
            Logger.error(e, "Kesalahan cetak Summary report kode lelang %s", id);

        }
    }
}
