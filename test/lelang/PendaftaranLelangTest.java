package lelang;

import com.google.gson.JsonObject;
import controllers.lelang.PendaftaranLelangCtr;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;
import models.sso.common.adp.util.DceSecurityV2;
import org.junit.Test;
import play.Logger;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;
import play.test.UnitTest;


public class PendaftaranLelangTest extends UnitTest {

    @Test
    public void qualifiedTest() {
        System.out.println("qualifiedTest running..... ");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("lls_id", 2560053);
        jsonObject.addProperty("rkn_id", 3768119);
        jsonObject.addProperty("rkn_namauser", "AIMPUSAT88");
        String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
        Logger.info(PendaftaranLelangCtr.URL_SIKAP_ISQUALIFIED+param);
        try {
            WSRequest request = WS.url(PendaftaranLelangCtr.URL_SIKAP_ISQUALIFIED+param);
            String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
            if(!CommonUtil.isEmpty(content)) {
                System.out.println("qualifiedTest : " +Boolean.parseBoolean(DceSecurityV2.decrypt(content)));
            }
        }catch (Exception ex){
            Logger.error(ex,"Tidak bisa konek ke SIKaP");
        }
    }

    @Test
    public void daftarLelangCepatTest() {
        System.out.println("daftarLelangCepatTest running...");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("lls_id", 2560053);
        jsonObject.addProperty("rkn_id", 3768119);
        jsonObject.addProperty("rkn_namauser", "AIMPUSAT88");
        String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
        Logger.info(PendaftaranLelangCtr.URL_SIKAP_DAFTAR_LELANG+param);
        try {
            WSRequest request = WS.url(PendaftaranLelangCtr.URL_SIKAP_DAFTAR_LELANG+param);
            String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
            if(!CommonUtil.isEmpty(content)) {
                System.out.println("daftarLelangCepatTest : " +Boolean.parseBoolean(DceSecurityV2.decrypt(content)));
            }
        }catch (Exception ex){
            Logger.error(ex,"Tidak bisa konek ke SIKaP");
        }
    }

    public static void main(String[] args) {
        String param = URLs.encodePart(DceSecurityV2.encrypt("1032108"));
        System.out.println(param);
    }
}
