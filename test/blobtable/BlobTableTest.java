package blobtable;

import org.junit.Test;
import play.Logger;
import play.libs.Crypto;
import play.test.UnitTest;

public class BlobTableTest extends UnitTest {

	@Test
	public  void decrypt()
	{
		//1. isikan blb_path di sini
		String blb_path="56fc48f80d68bf4045850da28f8e93234245a8cad1e2a6e73dfcd83053a3073504ad6a8673b919279e51b2acb496c087b47535bbc5f5b838ae188c9a0246da43ebaad0ded0f6a6f7234998c58dd8505faeb57709c2c3aeeb9a0a1f1094b9b95d5182a935b8c82c46bceddc867803a46cae7f59e203b8b30a8157b0d834383c1e35344f061d7d0466d419c1fa20c38c9661578ecad7f55a88540a5259b1728440";
		//2. buka browser dgn URL ini: http://localhost:9000/@tests/blobtable.BlobTableTest.class
		//3. hasil ada di console

		String st=Crypto.decryptAES(blb_path, "MNJK8*92lkpojdas");
		String aryPath[]=st.split("\n");
		String result=String.format("path: %s\nhash: %s\nfile: %s\nukuran: %s", 
				aryPath[0], aryPath[1], aryPath[2], aryPath[3]);
		Logger.info("result: %s", result);
	}
}
