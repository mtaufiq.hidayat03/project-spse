package log;

import models.jcommon.secure.encrypt.CipherEngineException;
import models.jcommon.secure.encrypt2.CachedKeyCipherEngine;
import models.jcommon.secure.encrypt2.CachedRSAKey;
import models.jcommon.secure.encrypt2.CipherEngine2;
import models.jcommon.secure.encrypt2.EncryptCipherEngine;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import play.Logger;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import play.test.UnitTest;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class LogFileErrorMessageTest extends UnitTest {

	/**
	 * @see controllers.admin.UtilityCtr.internalServerError(String)
	 * @throws CipherEngineException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 */
//	@Test
	public void doTest() throws CipherEngineException, IllegalBlockSizeException, BadPaddingException, IOException
	{
		CachedRSAKey key=new CachedRSAKey();
		String pub=key.getPublicKeyEncoded();
		WSRequest req= WS.url("http://localhost:9000/spse4/internalServerErrorBeta/71mc65l5j");
		req.setParameter("publicKey", pub);
		HttpResponse resp= req.post();
		CachedKeyCipherEngine decrypt=new CachedKeyCipherEngine(Cipher.DECRYPT_MODE, key.sessionKey, false);
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		decrypt.doCrypto(resp.getStream(), out);
		String spseResponse=new String(out.toByteArray());
		Logger.debug("Result: %s", spseResponse);

	}
	
	@Test
	public void doTest2Step() throws CipherEngineException, IllegalBlockSizeException, BadPaddingException, IOException
	{
		CachedRSAKey key=new CachedRSAKey();
		String pub=key.getPublicKeyEncoded();
		//Step 1
		WSRequest req= WS.url("http://localhost:9000/spse4/getLogFileErrorMessageStep1/" + key.sessionKey);
		req.setParameter("publicKey", pub);
		HttpResponse resp= req.post();//response dr server berupa Base64 dari publicKey SPSE
		CipherEngine2 decrypt=new CachedKeyCipherEngine(Cipher.DECRYPT_MODE, key.sessionKey, false);
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		decrypt.doCrypto(resp.getStream(), out);
		String spsePubKey=new String(out.toByteArray());
		String spseSessionId=resp.getHeader("X-session");
		Logger.debug("[Server]  spseSessionId: %s -> spsePubKey: %s", spseSessionId, spsePubKey);

		
		//Step 2
		//String bug_id="71mc65l5j";
		String bug_id="abc";
		CipherEngine2 encrypt=new EncryptCipherEngine(spsePubKey);
		byte[] aryBug_id=encrypt.doCrypto(bug_id.getBytes());
		String enc_bug_id=Base64.encodeBase64String(aryBug_id);

		req= WS.url("http://localhost:9000/spse4/getLogFileErrorMessageStep2/" + spseSessionId );
		req.setParameter("bugId", enc_bug_id);
		req.setParameter("publicKey", pub);
		resp= req.post();
		ByteArrayOutputStream outLogMessage=new ByteArrayOutputStream();
		decrypt.doCrypto(resp.getStream(), outLogMessage);
		String logMessage=new String(outLogMessage.toByteArray());
		Logger.debug("[Server] %s: message=\t%s", bug_id, logMessage);
		

	}
}
