import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.text.StringEscapeUtils;
import org.junit.Test;
import play.test.UnitTest;
import java.util.HashMap;
import java.util.Map;

public class BasicTest extends UnitTest {
	
	private static final Gson gson = new Gson();

    @Test
    public void aVeryImportantThingToTest() {
        assertEquals(2, 1 + 1);
    }
    
    public static void main(String[] args) {
/*    	DateTime datetime = new DateTime();
		System.out.println("datetime.getYear() is "+datetime.getYear());
		System.out.println("datetime.getDayOfMonth() is "+datetime.getDayOfMonth());
		System.out.println("datetime.getDayOfWeek() is "+datetime.getDayOfWeek());
		String content = "v2IFzvaacUbZKZDOPUPtw2tkoUs6q63lu3jhm%2B6W9ODGhhooAsCcwBOO67%2Fs247ZnGX31%2BTlDahtkZVkQ3CpnJlfyY3CwkXOWEQRZN7Bi7Wpp%2Fu5zuPU9%2BOroHU1wNh7wLk3QPdkIRSpQz%2Fa2Q7E%2BVHiIyE54d%2FeXf8m7lZQ0ds%3D";
		System.out.println(DceSecurityV2_1.decrypt(URLDecoder.decode(content)));*/

		System.out.println(StringEscapeUtils.escapeHtml4("<3"));
    }
    
    @Test
    public void gsonToJsonTest() {
    	StopWatch stopWatch = new StopWatch();
    	stopWatch.start();
    	Map<String, Object> param = new HashMap<String, Object>(1);
    	param.put("count", 14);
    	gson.toJson(param);
    	stopWatch.stop();
    	System.out.println("gson to Json : hasmap "+stopWatch.getTime());
    	stopWatch.reset();
    	stopWatch.start();
    	JsonObject jsonObject = new JsonObject();
    	jsonObject.addProperty("count", 14);
    	gson.toJson(jsonObject);
    	stopWatch.stop();
    	System.out.println("gson to Json : JsonObject "+stopWatch.getTime());
    }

}
