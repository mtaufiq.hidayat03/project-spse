package dce3client;

import com.google.gson.Gson;
import models.dce3client.QueryEngine;
import models.jcommon.secure.encrypt.CipherEngineException;
import models.jcommon.secure.encrypt2.CachedRSAKey;
import models.jcommon.secure.encrypt2.CipherEngine2;
import models.jcommon.secure.encrypt2.DecryptCipherEngine;
import models.jcommon.secure.encrypt2.EncryptCipherEngine;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import play.Logger;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import play.test.UnitTest;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.*;
import java.util.zip.GZIPInputStream;

public class QueryEngineTest extends UnitTest {

	private WSRequest req;
	private String URL="http://localhost:9000/spse4/dce3";
	private String dceSessionId;
	private CipherEngine2 ceDecryptor;
	private CipherEngine2 encrypt;
	private HttpResponse resp;
	
	@Before
	public void before() throws CipherEngineException
	{
		
		CachedRSAKey dceKeys=new CachedRSAKey();
		
		/**############## Step 1 #####################*/
		String publicKeyDCE=dceKeys.getPublicKeyEncoded();
		req= WS.url(URL + "/init1");
		req.setParameter("publicKeyDCE", publicKeyDCE);
		resp= req.post();
		String init1Resp=resp.getString();
		Logger.debug("[DCE]  Init 1 Resp[%s]: %s", resp.getStatusText(), init1Resp);
		//dapatkan session dan publicKey dari SPSE
		ceDecryptor=new DecryptCipherEngine(dceKeys.getPrivateKeyEncoded());
		String strRespo1=null;
		try {
			byte[] aryByte=Hex.decodeHex(init1Resp.toCharArray());
			aryByte=ceDecryptor.doCrypto(aryByte);
			strRespo1=new String(aryByte);
		} catch (DecoderException e) {
			e.printStackTrace();
		}
		if(strRespo1!=null)
		{
			Gson gson=new Gson();
			String[] ary=gson.fromJson(strRespo1, new String[0].getClass());
			String publicKeySPSE=ary[1];
			dceSessionId=ary[0];
			/**############## Step 2 #####################
			 * Kirimkan token ke SPSE
			 *  diambil dari row pertama Tabel peserta kolom auditupdate -> "2015-08-12 11:29:12.922936" 
			 * beserta dceSessionId 
			 * yang diencrypt dg publikKeySPSE
			 */
			String token="2015-08-12 11:29:12.922936";
			//encrypt
			encrypt=new EncryptCipherEngine(publicKeySPSE);
			String init2Token=Hex.encodeHexString(encrypt.doCrypto(token.getBytes()));
			req= WS.url(URL + "/init2/" + dceSessionId);
			Logger.debug("[DCE]  Init 2 Token: %s", token);
			req.setParameter("token", init2Token);
			resp=req.post();
			String init2Reps=resp.getString();
			Logger.debug("[DCE]  Init 2 Resp: [%s] %s", resp.getStatusText(), init2Reps);
		}
		
	}
	
//	@Test
	public void doTest()
	{
		QueryEngine qe=new QueryEngine();
		File file=qe.select("SELECT * FROM mail_queue");
		Logger.debug("FILE: %s", file);
	}
	
	@Test
	public void doSelect() throws CipherEngineException, IOException
	{
	
			
			/**#################Step 3 ######################
			 * Execute Query
			 */
			//req= WS.url(URL + "/sql/" + dceSessionId + "/c2b8770e8ddab8181a036d5ad60de89f");
			req= WS.url(URL + "/sql/" + dceSessionId );
			String sql="select * FROM pegawai";
			sql=Hex.encodeHexString(encrypt.doCrypto(sql.getBytes()));
			req.setParameter("statement", sql);
			resp= req.post();
			if(resp.success())
			{
				String md5Value=resp.getHeader("X-MD5Value");
				Logger.debug("[DCE]  [%s] sql.md5: %s", resp.getStatusText(), md5Value);			
				File outFile=new File("tmp/result.csv.gz");
				OutputStream out=new BufferedOutputStream(new FileOutputStream(outFile));
				try {
					ceDecryptor.doCrypto(resp.getStream(), out);
					out.close();
					Logger.debug("OUTPUT: %s, size: %,d", outFile, outFile.length());
				} catch (IllegalBlockSizeException | BadPaddingException e) {
					e.printStackTrace();
				}
				
			}
			else{
				Logger.debug("[DCE]  [%s] dce3.sql -> response: %s", resp.getStatusText(), resp.getString());
			}
		
			
			
	}
//	@Test
	public void doSystemInformation() throws CipherEngineException, IOException
	{
	
			
			/**#################Step 3 ######################
			 * Execute Query
			 */
			req= WS.url(URL + "/systemInformation/" + dceSessionId );
			String sql="select * FROM pegawai";
			sql=Hex.encodeHexString(encrypt.doCrypto(sql.getBytes()));
			req.setParameter("statement", sql);
			resp= req.post();
			if(resp.success())
			{
				Logger.debug("[DCE]  [%s] dce3.sql -> response: %s", resp.getStatusText(), resp.getString());
			}
		
	}
	
	
//	@Test
	public void doUpdateOrDelete() throws CipherEngineException, IOException
	{
	
			/**#################Step 3 ######################
			 * Execute Query
			 */
			//req= WS.url(URL + "/sql/" + dceSessionId + "/c2b8770e8ddab8181a036d5ad60de89f");
			req= WS.url(URL + "/sql/" + dceSessionId );
			String sql="DELETE FROM mail_queue 		WHERE 1=1";
			sql=Hex.encodeHexString(encrypt.doCrypto(sql.getBytes()));
			req.setParameter("statement", sql);
			resp= req.post();
			if(resp.success())
			{
				String md5Value=resp.getHeader("X-MD5Value");
				Logger.debug("[DCE]  [%s] sql.md5: %s", resp.getStatusText(), md5Value);
				
				InputStream is=new GZIPInputStream(resp.getStream());
				File outFile=new File("tmp/dce3.txt");
				OutputStream out=new BufferedOutputStream(new FileOutputStream(outFile));
				IOUtils.copy(is, out);
				out.close();
			}
			else{
				Logger.debug("[DCE]  [%s] dce3.sql -> response: %s", resp.getStatusText(), StringUtils.left(resp.getString(), 100));
			}
		
	}
	
//	@Test
	public void doTestInvalidRequest() throws CipherEngineException, IOException
	{
		String URL="http://localhost:9000/spse4/dce3/sql/1dc02ab5-5574-4ef5-9d46-31817ac54ac3";
		
		/**############## Step 1 #####################*/
		WSRequest req= WS.url(URL);
		req.setParameter("statement", "SELECT * FROM pegawai");
		HttpResponse resp= req.post();
		String init1Resp=resp.getString();
		Logger.debug("[DCE]  Init 1 Resp[%s]: %s", resp.getStatusText(), init1Resp);
			
	}

}
