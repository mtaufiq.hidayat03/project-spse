package bm;

import models.bm.BroadcastMessage;
import models.bm.BroadcastMessage.Category;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import play.Logger;
import play.test.UnitTest;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**Class to generate Broadcast Message Data
 * 
 * @author Mr. Andik
 *
 */
public class BroadcastMessageData extends UnitTest {
	@Test
	public void insertData() throws IOException
	{
		//runnin text
		BroadcastMessage bm=new BroadcastMessage();
		bm.bm_id=(long)UUID.randomUUID().hashCode();
		bm.category=Category.RUNNING_TEXT;
		bm.title="Awas penipuan Mama Minta Pulsa.";
		bm.roles_="PUBLIC";
		bm.url="http://lpse.blogdetik.com";
		bm.content="http://lpse.blogdetik.com";
		bm.save();
		
		//link
		byte[] binaryImage=FileUtils.readFileToByteArray(new File("test/data/banner-callcenter.jpg"));
		String base64Image=Base64.encodeBase64String(binaryImage);
		bm=new BroadcastMessage();
		bm.bm_id=(long)UUID.randomUUID().hashCode();
		bm.category=Category.STATIC_LINK;
		bm.title="Call Center LKPP";
		bm.roles_="PUBLIC";
		bm.image_data=base64Image;
		bm.url="http://lpse.blogdetik.com";
		bm.save();
		
		Logger.debug("[BM] Broadcast Message inserted");
	}
	
	@Before
	public void clear()
	{
		BroadcastMessage.deleteAll();
	}
}
