/*
 * boot the angular app
 */
var spseApp = angular.module('spseApp', ['Scope.safeApply']);

// set the appropriate labels for envelope names
spseApp.filter('envelopeName', function(){
  return function(name){

    var dict = {
      technical : 'Penawaran Teknis',
      pricing : 'Penawaran Harga'
    }

    return 'Mengunggah ' +  dict[name] || name;
  }
});

// set the appropriate labels for percentages
spseApp.filter('percentName', function(){
  return function(percent){
    return '... ' + percent.toFixed(2) + ' %';
  }
});

// the controllers
// user scope ctrl
function UserCtrl($scope) {
  $scope.logout = function(){
  	// Direct apendo API call for logout
  	// todo: safer LT17 API
    Apendo.userLogout($scope.user);
  }
  $scope.user = user;
}

// the envelopes
// envelopes scope ctrl
function EnvelopesCtrl($scope) {
  // unused for now
}
// The tabs
// 0. info 
function InfoCtrl($scope){
  $scope.info = info; // todo, set this after loading the envelope data
}

function VendorsCtrl($scope, $timeout){

  LT17.register("unpackFinished", function(ctx){
    $timeout(function(){
      if (ctx.index == undefined || ctx.index == null) {
        return;
      }
      
      $scope.vendors = $scope.vendors || [];
      if ($scope.vendors.length > 0 && ctx.index < $scope.vendors.length){
        // update the rows
        $scope.vendors[ctx.index].downloading = false;
        if (ctx.success)
        {
          $scope.vendors[ctx.index].downloaded = ctx.resultDirPath;
        }
      }

    });
  });

  $scope.update = function(ctx){
    try{
      $scope.vendors = JSON.parse(ctx.data);
    }
    catch(e){
      // if parsing err
    }
  }

  $scope.open = function(vendor, index){

    if (vendor.downloaded){
      return LT17.open(vendor.downloaded);
    }

    // todo checking, if already downloaded
    // open
    // if not
    // download
    var package = {url : vendor.technical.url, owner : vendor.user._id || vendor.user.id };
    $scope.vendors[index].downloading = true;
    
    LT17.download({
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId,
      envelope : 'technical',
      accessToken : token, // the access token
      packages : [package],
      index : index
    }, function(err, ctx){

      if (err){

      }

      var data = ctx.data;
      var row = data[0];
      var index = row.index;

      LT17.unpack({
        owner : user.id, // spse userId
        objId : info.id, // spse lelangId,
        envelope : 'technical',
        key : info.keypairs[0].private, // private key
        packageOwners : [package.owner],
        index : index
      }, function(err, ctx){

        if (err) {
          // if something is wrong, what to do, what to do?
        }

        // unpacked

      });

    });
  }

  $scope.download = function(){

    // the submissions element has following keys: url and userId, while package, requires url and owner
    var packages = [];
    var owners = [];
    angular.forEach(submissions, function(submission){
      packages.push({url : submission.url, owner : submission.userId }); 
      owners.push(submission.userId);
    });

    $scope.downloading = true;

    // todo: flow control, well I'm ok with this stacked callbacks
    console.log ("downloading...");

    // 1. download
    LT17.download({
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId,
      envelope : 'pricing',
      accessToken : token, // the access token
      packages : packages

    }, function(err, ctx){

      if (err){
        // error on download, retrying...
      }

      // then unpack
      LT17.unpack({
        owner : user.id, // spse userId
        objId : info.id, // spse lelangId,
        envelope : 'pricing',
        key : info.keypairs[0].private, // private key
        packageOwners : owners

      }, function(err, ctx){

        if (err){
          // error on unpacking, retrying...
          // the worst case: the packages cannot be unpacked completely
          // what to do? what to do?
        }

        // then sort
        LT17.sort({
          owner : user.id, // spse userId
          objId : info.id, // spse lelangId
          envelope : 'pricing',
          accessToken : token,
          url : sortingUrl,
        }, function(err, ctx){

          if (err) {
            // failed to sort the pricing data
            // what to do 
          }

          // save the data as an envelope
          LT17.save({
            owner : user.id, // spse userId
            objId : info.id, // spse lelangId
            envelopes : [
              {
                vendors : {
                  data : ctx.rawData
                }
              }
            ]
          }, function(err, saved){

            if (err) {

            }

            // data is saved

          });


          $timeout(function(){
            try{
              $scope.vendors = JSON.parse(ctx.rawData);  
            }
            catch(e){}

            $scope.downloading = false;
            $scope.downloaded = true;

          }, 1);

          // if err

          // ctx.raw should be parseable, hence we could render the list of vendors

          // [{ owner : { name : 'vendor1'}, period : 10, url : http://cumi/com/file}, ...]

          // by having this, we could generate report

          // then,

          // LT17.download({..., envelope : 'technical', packages : [url : '', owner : '']})
          // a copy goes to report folders if available (if generated, well we have the info already)

        });
      });
    });
  }
}

// helper on updating tab data
function updateTab(ctx){
  var envelopes = ctx.envelopes;
  var keys = Object.keys(envelopes);
  angular.forEach(keys, function(key){
    key = key.split('-')[0];
    var envelopeScope = angular.element('#' + key).scope();
    envelopeScope.$safeApply(function(){
      envelopeScope.update(envelopes[key]);
    });
  });
}

// hacks and inits
// small bootstrap tab ugly hacks
function initTabs(){
  // init
  var tab = $('#tabs a')[0];
  $(tab).click();
}

function init(ctx){

  // tabs onclick handler
  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    var href = $(this).attr('href');
    // update tab data by loading data from db
    var key = href.substr(1).split('-')[0];
    

    if (key != 'vendors'){
      return;
    }

    LT17.load({
      objId : ctx.objId,
      owner : ctx.owner,
      envelopes : [key]
    }, function(err, ctx){
      if (err){
        // todo show error loading envelope data
        console.log (err);
      }
      // update the tab data
      updateTab(ctx);
    });

  });

  // hacks!
  initTabs();
}

// on ready
$(function(){

  // user popover, cosmetics  
  $('#user').popover({
    content : user.address + ' ' + user.phone,
    title : user.name + ' (' + user.username + ')',
    trigger : 'hover'
  });
  
  // prepare session  
  LT17.prepare({
    owner : user.id, // spse userId
    objId : info.id // spse lelangId
  }, function(err, ctx){
    if (err) {
      // todo show error on loading session
      console.log (err);
      return;
    }
    // init the page using the loaded session
    init(ctx);
  });
});