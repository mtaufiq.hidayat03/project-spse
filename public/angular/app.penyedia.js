var pricingDetailInitialized = false;

// user ctrl
function UserCtrl($scope){
  $scope.logout = function(){
    disconnects();
    Apendo.userLogout(user);
  }
  $scope.user = user;
}

// tabs navigation ctrl 
function NavTabsCtrl(){}

// progress bars ctrl
function ProgressCtrl($scope){
  $scope.ready = false;
}

// first tab ctrl
function InfoCtrl($scope){
  $scope.info = info;
  $scope.dirty = false;
  
  /**
   * Save the period to session via context
   */
  $scope.save = function (){
    var ctx = { userId : user.id, lelangId : info.id, envelope : "info"};
    var obj = { period : $scope.info.period };
    ctx.task = 'save:info';
    ctx.data = JSON.stringify(obj);
    Apendo.taskSave(ctx);
  }

  $scope.saveDone = function (ctx){
    $scope.dirty = false;
  }

  $scope.loadDone = function (ctx) {
    var obj = JSON.parse(ctx.data);
    $scope.info.period = obj.period;
  }

  $scope.$watch("info.period", function(n, o){
    $scope.dirty = false;
    if (n && n > 0) {
      $scope.dirty = n != o;  
    }
  });

  $scope.packDone = function(ctx){}

}

// tech requirements tab ctrl
function RequirementCtrl($scope, $timeout){
  $scope.select = function (index){
    var ctx = { index : index };
    Apendo.filesSelect(ctx);
  }

  $scope.remove = function (idx, index) {
    $scope['files_' + index].splice(idx, 1);
    checkRequirementsDirty(true);
  }

  $scope.save = function (thenEncrypt){
    // mandatory fields
    var ctx = { userId : user.id, lelangId : info.id, envelope : "technical"};
    ctx.task = "save:technical";
    ctx.thenEncrypt = thenEncrypt || false;
    ctx.data = JSON.stringify(getRequirements());
    Apendo.taskSave(ctx);
  }

  $scope.encrypt = function (ctx){
    ctx.task = "pack:technical";
    if(info.pubKey[0]){
    	ctx.key = info.pubKey[0];
    	console.log ("key 0");
    } else {
    	ctx.key = info.pubKey[1];
    	console.log ("key 1");
    }
    ctx.data = getRequirements();
    ctx.workspace = Apendo.userWorkspace;
    Apendo.packStart(ctx);
  }

  $scope.open = function (name) {
    Apendo.fileOpenExternally ({ href : name });
  }

  $scope.saveDone = function (ctx) {
    $scope.dirty = false;
    if (ctx.thenEncrypt) {
      $scope.encrypting = true;
      $scope.encrypt(ctx);
    }
  }

  $scope.loadDone = function (ctx) {
    var obj = JSON.parse(ctx.data);
    for (var i in obj) {
      var row = obj[i];
      row.files = typeof row.files == 'string' ? JSON.parse(row.files) : row.files;
      $scope[row.rowName] = row.files;
    }
    checkRequirementsDirty();
  }

  $scope.packDone = function(ctx){}
}

//tech price tab ctrl
function PricingCtrl($scope){

  $scope.save = function (thenEncrypt){
    var ctx = { userId : user.id, lelangId : info.id, envelope : "pricing"};
    ctx.thenEncrypt = thenEncrypt || false;
    ctx.task = "save:pricing";
    ctx.data = JSON.stringify(getPricingDetail());
    console.log ('pricing scope save');
    Apendo.taskSave(ctx);
  }

  $scope.encrypt = function (ctx){
    ctx.task = "pack:pricing";
    if(info.pubKey[0]){
    	ctx.key = info.pubKey[0];
    	console.log ("key 0");
    } else {
    	ctx.key = info.pubKey[2];
    	console.log ("key 2");
    }
    var infoScope = angular.element('#info').scope();
    var obj = { user : user, info : info, table : getPricingDetail(), period : infoScope.info.period || 30 };
    ctx.data = JSON.stringify(obj);
    ctx.workspace = Apendo.userWorkspace;
    Apendo.packStart(ctx);
  }

  $scope.open = function (name) {
    Apendo.fileOpenExternally ({ href : name });
  }

  $scope.saveDone = function (ctx) {
    $scope.dirty = false;
    if (ctx.thenEncrypt) {
      $scope.encrypting = true;
      $scope.encrypt(ctx);
    }
  }

  $scope.loadDone = function (ctx) {
    var data = [];
    try {
      data = JSON.parse(ctx.data); 
    } catch(e){}

    if (data && data.length > 0) {
      pricingData = data;
      initPricingDetails();
      console.log('load done');
    }
  }

  $scope.packDone = function(ctx){
    console.log (ctx);
  }
}

function ActionsCtrl($scope, $timeout){

  $scope.envelopes = {};

  $scope.append = function(envelope, encrypted){
    $scope.envelopes[envelope] = encrypted.filePath;
  }

  $scope.upload = function(){
    for (var envelope in $scope.envelopes) {
      console.log ('upload envelope', envelope);
      var ctx = {
        isMultiPart : true,
        filePath : $scope.envelopes[envelope],
        url : uploadUrl + '?access_token=' + token + '&period=' + angular.element('#info').scope().info.period, //'http://posttestserver.com/post.php?dump', //uploadUrl + '?access_token=' + token,
        envelope : envelope
      }
      Apendo.uploadStart(ctx);

      checkUploading(true);
    }
  }
}

function readyToSubmit(){
  var ready = true;
  // if no pricing, splice following array
  ['technical', 'pricing'].map(function(envelope){
    var ctx = { userId : user.id, lelangId : info.id, envelope : envelope};
    var encrypted = Apendo.encrypted(ctx);
    console.log (envelope, encrypted);
    if (!encrypted.exists){
      ready = false;
    } else {
      var envelopeScope = angular.element('#' + envelope).scope();
      var actionScope = angular.element('#actions').scope();
      envelopeScope.$safeApply(function(){
        envelopeScope.encrypted = encrypted;
        // append to upload
        actionScope.append(envelope, encrypted);
      });
    }
  });
  var tasksScope = angular.element('#tasks').scope();
  tasksScope.$safeApply(function(){
    tasksScope.ready = ready;
  });
}

function initPricingDetails(){

  if (pricingData.length == 0) {
    pricingData.push({
      name : null, 
      unit1 : null, 
      volume1: 1, 
      unit2: null, 
      volume2: 1, 
      price: 0, 
      price_total: 0, 
      remark: null
    });
  }

  $("#pricing-detail").handsontable({

    // headers
    colHeaders:[ 
      "Jenis Barang", 
      "Satuan", 
      "Volume", 
      "Satuan 2", 
      "Volume 2", 
      "Harga Satuan", 
      "Total harga Satuan", 
      "Keterangan"],

    rowHeaders: true,

    data : pricingData,

    dataSchema : { 
      name : null, 
      unit1 : null, 
      volume1: 1, 
      unit2: null, 
      volume2: 1, 
      price: 0, 
      price_total: 0, 
      remark:null
    },

    contextMenu: {
      items : {
        row_above : {
          name : 'Tambah baris di atas'
        },
        row_below : {
          name : 'Tambah baris di bawah'
        },
        remove_row : {
          name : 'Hapus baris ini'
        },
        undo : {
          name : 'Undo'
        },
        redo : {
          name : 'Redo'
        }
      }
    },

    columns: [
      {data:"name"}, 
      {data:"unit1"}, 
      {data:"volume1", type:'numeric'}, 
      {data:"unit2"}, 
      {data:"volume2", type:'numeric'}, 
      {data:"price", type:'numeric', format:'Rp 0,0.00'},
      {data:"price_total",type:'numeric', format:'Rp 0,0.00'}, 
      {data:"remark"}
    ],

    cells : function(row, col, prop){
      var cellProperties = {};
      if (col == 6 && pricingData[row]) {
        cellProperties.readOnly = true;
        pricingData[row].price_total = pricingData[row].volume1 * pricingData[row].volume2 * pricingData[row].price;
      }
    },

    afterChange : function(change, source){
      updatePricingSummary();
      checkPricingDetailDirty(true);
    }
  });
}

/**
 * Check the dirtiness of tech requirements tab
 */
function checkRequirementsDirty(flag) {
  var requirementScope = angular.element('#technical').scope();
  var names = $('.requirement-name');
  var hasEmpty = false;
  for (var j = 0; j < names.length; j++) {
    var rowKeyName = 'files_' + (j + 1); 
    requirementScope[rowKeyName] = requirementScope[rowKeyName] || [];
    if (requirementScope[rowKeyName].length == 0) {
      hasEmpty = true;
      break;
    }
  }
  requirementScope.dirty = flag;
  requirementScope.filled = !hasEmpty;
}

/**
 * serialize technical requirements tab
 */
function getRequirements() {
  var requirementScope = angular.element('#technical').scope();
  var names = $('.requirement-name');
  var requirements = {};
  for (var j = 0; j < names.length; j++) {
    var rowKeyName = 'files_' + (j + 1); 
    requirementScope[rowKeyName] = requirementScope[rowKeyName] || [];
    requirements[j] = { 
      row : j, 
      rowName : rowKeyName, 
      name : $(names[j]).text(), 
      files : JSON.parse(angular.toJson(requirementScope[rowKeyName])) 
    };
  }
  return requirements;
}

/**
 * serialize pricing detail tab
 */
function getPricingDetail(){
  return $('#pricing-detail').data('handsontable').getData();
}

/**
 * Check the dirtiness of pricing detail tab
 */
function checkPricingDetailDirty(flag) {
  var pricingScope = angular.element('#pricing-detail').scope();
  pricingScope.dirty = flag;
}

function updatePricingSummary(){
  var pricingScope = angular.element('#pricing').scope();
  var total = 0;
  var ppn = 0;
  var hps_total = 0;

  pricingScope.$safeApply(function(){
    pricingScope.subtotal = total;
    pricingScope.ppn = total;
    pricingScope.total = hps_total;
  });

  for (var i = 0; i < pricingData.length; i++) {
    total += pricingData[i].volume1 * pricingData[i].volume2 * pricingData[i].price;
  }

  if (total > 0) {
    ppn = (10 * total) / 100;
    hps_total = total + ppn;

    pricingScope.$safeApply(function(){
      pricingScope.subtotal = total;
      pricingScope.ppn = ppn;
      pricingScope.total = hps_total;
    });
  }
}

 var checkUploading = function(flag){
  var actionScope = angular.element('#actions').scope();
  var pricingScope = angular.element('#pricing').scope();
  var technicalScope = angular.element('#technical').scope();

  pricingScope.$safeApply(function(){
    pricingScope.uploading = flag;
  });

  technicalScope.$safeApply(function(){
    technicalScope.uploading = flag;
  });

  actionScope.$safeApply(function(){
    actionScope.uploading = flag;
  });
}

/**
 * on ready
 */
$(function(){

  $('#user').popover({
    content : user.address + ' ' + user.phone,
    title : user.name + ' (' + user.username + ')',
    trigger : 'hover'
  });

  // init the tabs
  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    
    var href = $(this).attr('href');
    $('#pricing-detail').hide();

    var ctx = {
      userId : user.id,
      lelangId : info.lelangId || info.id
    }

    if (href == '#info') {

      // load info data
      ctx.envelope = "info";
      Apendo.taskLoad(ctx);

    } else if (href == '#technical') {

      // load technical data
      ctx.envelope = "technical";
      Apendo.taskLoad(ctx);

    } else if (href === '#pricing') {

      if (!pricingDetailInitialized) {
        // init the pricing details
        pricingDetailInitialized = true;
        initPricingDetails();
      }

      // load saved pricing data
      ctx.envelope = "pricing";
      Apendo.taskLoad(ctx);

      $('#pricing-detail').show();
    }
  });

  disconnects();

  Apendo.filesSelected.connect(onFileSelected);
  Apendo.taskSaved.connect(onTaskSaved);
  Apendo.taskLoaded.connect(onTaskLoaded);
  Apendo.packStarted.connect(onPackStarted);
  Apendo.packFinished.connect(onPackFinished);
  Apendo.sessionLoaded.connect(onSessionLoaded);
  Apendo.uploadProgress.connect(onUploadProgress);
  Apendo.uploadFinished.connect(onUploadFinished);
  Apendo.uploadError.connect(onUploadError);
  Apendo.sessionRequested({ key : user.id + ':' + info.id });

  readyToSubmit();
});

/*
 * boot the angular app
 */
var spseApp = angular.module('spseApp', ['Scope.safeApply']);

spseApp.filter('envelopeName', function(){
  return function(name){
	if(name=='technical')
		name='Penawaran Administrasi & Teknis';
	else
		name='Penawaran Harga'
    return 'Mengunggah ' +  name; //(name == 'technical' ? "Penawaran Teknis" : "Penawaran Harga")
  }
});

spseApp.filter('percentName', function(){
  return function(percent){
    return '... ' + percent.toFixed(2) + ' %';
  }
});


var onUploadError = function(ctx){
  console.log (ctx);
}

// globals ass holes
// handlers
// After a set of files is selected, this function will be called, with ctx contains info about the selected files
var onFileSelected = function(ctx){

  var requirementScope = angular.element('#technical').scope();
  var filesKeyName = 'files_' + ctx.index; 
  var arr = requirementScope[filesKeyName] || [];
  
  for (var i = 0; i < ctx.files.length; i++) {

    var file =  ctx.files[i];
    var exists = false;

    for (var j = 0; j < arr.length; j++) {
      if (arr[j].name == file) {
        exists = true;
        break;
      }
    }
    
    if (!exists) {
      arr.push({ name : file });    
    }
  }

  /**
   * update the related visual element, then check the dirtiness
   */
  requirementScope.$safeApply(function(){
    requirementScope[filesKeyName] = arr;
    checkRequirementsDirty(true);
  });
}

var onTaskSaved = function (ctx){
  var task = ctx.task;
  var arr = task.split(":");
  var scopeName = arr.pop();
  var actionName = arr.pop();
  var currentScope = angular.element('#' + scopeName).scope();
  currentScope.$safeApply(function(){
    currentScope[actionName + "Done"](ctx);
  });
}

var onTaskLoaded = function (ctx){
  var task = ctx.task;
  var arr = task.split(":");
  var scopeName = arr.pop();
  var actionName = arr.pop();
  var currentScope = angular.element('#' + scopeName).scope();
  currentScope.$safeApply(function(){
    currentScope[actionName + "Done"](ctx);
  });
}

var onPackStarted = function (ctx) {
  var currentScope = angular.element('#' + ctx.envelope).scope();
  currentScope.$safeApply(function(){
    currentScope.encrypting = true;
    currentScope.dirty = false;
  });
}

var onPackFinished = function (ctx) {
  var currentScope = angular.element('#' + ctx.envelope).scope();
  currentScope.$safeApply(function(){
    currentScope.encrypting = false;
    readyToSubmit();
    currentScope.packDone(ctx);
  });
}

var onSessionLoaded = function (ctx){
  $($('#tabs a')[2]).click();
  $($('#tabs a')[0]).click();
}

var onUploadProgress = function (ctx) {
  
  checkUploading(true);

  var actionScope = angular.element('#actions').scope();
  actionScope.$safeApply(function(){
    actionScope.uploading = true;
  });

  var tasksScope = angular.element('#tasks').scope();
  tasksScope.$safeApply(function(){
    var found = false;
    var done = true;

    tasksScope.progresses = tasksScope.progresses || [];

    for (var i = 0; i < tasksScope.progresses.length; i++){
      if (!tasksScope.progresses[i].done){
        done = false;
        break;
      }
    }

    if (done && tasksScope.progresses.length == 3) tasksScope.progresses = []; // 2 is magic number for now! argh!

    tasksScope.progresses.map(function(progress){
      if (ctx.envelope == progress.envelope && !progress.done){
        console.log (ctx.envelope);
        progress.percent = ctx.percent;
        found = true;
      }
    });

    if (!found) {
      tasksScope.progresses.push(ctx);
    }
  });
}

var onUploadFinished = function(ctx) {
  var uploading = false;
  var actionScope = angular.element('#actions').scope();
  var tasksScope = angular.element('#tasks').scope();

  tasksScope.$safeApply(function(){
    tasksScope.progresses.map(function(progress){
      progress.done = progress.percent >= 100.0;
    });
  })

  for (var i = 0; i < tasksScope.progresses.length; i++){
    if (!tasksScope.progresses[i].done){
      uploading = true;
      break;
    }
  }
  
  actionScope.$safeApply(function(){
    actionScope.uploading = uploading;
    checkUploading(uploading);
  });
}

$( window ).unload(function() {
  Apendo.filesSelected.disconnect(onFileSelected);
  Apendo.taskSaved.disconnect(onTaskSaved);
  Apendo.taskLoaded.disconnect(onTaskLoaded);
  Apendo.packStarted.disconnect(onPackStarted);
  Apendo.packFinished.disconnect(onPackFinished);
  Apendo.sessionLoaded.disconnect(onSessionLoaded);
  Apendo.uploadProgress.disconnect(onUploadProgress);
  Apendo.uploadFinished.disconnect(onUploadFinished);
});

function disconnects(){
  try{
    Apendo.filesSelected.disconnect(onFileSelected);
  }catch(ex){}
  try{
  Apendo.taskSaved.disconnect(onTaskSaved);
  }catch(ex){}
  try{
  Apendo.taskLoaded.disconnect(onTaskLoaded);
  }catch(ex){}
  try{
  Apendo.packStarted.disconnect(onPackStarted);
  }catch(ex){}
  try{
  Apendo.packFinished.disconnect(onPackFinished);
  }catch(ex){}
  try{
  Apendo.sessionLoaded.disconnect(onSessionLoaded);
  }catch(ex){}
  try{
  Apendo.uploadProgress.disconnect(onUploadProgress);
  }catch(ex){}
  try{
  Apendo.uploadFinished.disconnect(onUploadFinished);
  }catch(ex){}
}