/*
 * boot the angular app
 */
var spseApp = angular.module('spseApp', ['Scope.safeApply']);

// set the appropriate labels for envelope names
spseApp.filter('envelopeName', function(){
  return function(name){

    var dict = {
      technical : 'Penawaran Teknis',
      pricing : 'Penawaran Harga'
    }

    return 'Mengunggah ' +  dict[name] || name;
  }
});

// set the appropriate labels for percentages
spseApp.filter('percentName', function(){
  return function(percent){
    if (percent){
      return '... ' + percent.toFixed(2) + ' %';  
    } else {
      return '...';
    }
  }
});

// the controllers
// user scope ctrl
function UserCtrl($scope) {
  $scope.logout = function(){
  	// Direct apendo API call for logout
  	// todo: safer LT17 API
    Apendo.userLogout($scope.user);
  }
  $scope.user = user;
}

// progress scope ctrl
function ProgressCtrl($scope){

  // the container
  $scope.progresses = [];

  // update the progresses
  $scope.update = function(ctx){
    var exists = false;
    for (var i = 0; i < $scope.progresses.length; i++){
      if (ctx.name == $scope.progresses[i].name){
        exists = true;
        $scope.progresses[i] = ctx;
        if (ctx.success != undefined){
          $scope.progresses[i].error = !ctx.success;
        }
        break;
      }
    } 

    if (!exists){
      $scope.progresses.push(ctx);
    }
  }

  // I don't know if we need this
  $scope.remove = function(name){
    var idx = -1;
    for (var i = 0; i < $scope.progresses.length; i++){
      if (ctx.name == $scope.progresses[i].name){
        idx = i;
        break;
      }
    }

    if (idx > 0){
      $scope.progresses.splice(idx, 1);
    } 
  }
}

// actions scope ctrl
function ActionsCtrl($scope, $timeout){

  // it's ready, then it's ready
  $scope.update = function(ctx){
    $scope.ready = ctx.ready;
  }

  // upload function
  $scope.upload = function(){

    // uploading is ongoing
    $scope.uploading = true;

    // the info scope
    var infoScope = angular.element('#info').scope();

    // yes, we need to get this period data out from the info scope
    var period = infoScope.info.period;

    // register progress handler
    LT17.register('uploadProgress', function(ctx){
      updateProgresses(ctx);
    });

    // register finished handler
    LT17.register('uploadFinished', function(ctx){
      var finishedCtx = ctx;
      finishedCtx.done = true;
      updateProgresses(finishedCtx);
    });

    LT17.register('uploadError', function(ctx){
      updateProgresses(ctx);
    });

    // start the upload process
    LT17.upload({
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      accessToken : token, // the access token
      url : uploadUrl, // the url, change this to the real url
      packages : [
      // start
      {
        // 0. technical
        envelope : 'technical', // the document, will be accompanied by its md5 hash
        fields : {
          period : period || 30 // an additional field, period (masa berlaku penawaran)
          // add other required fields here
          // it can be anything, e.g
          // foo : 'bar'
        }
      }, 
      {
        // 1. pricing
        envelope : 'pricing',
        // just in case we need to add additional fields
        // fields : {} 
      }
      // end
      ]
    }, function(err, ctx){

      // unregister the listeners
      LT17.unregister(['uploadProgress', 'uploadFinished', 'uploadError']);

      if (err){
        // error upload, what to do, what to do?
        // make sure we call the unregistration of listener
      }
      
      $timeout(function(){
        // all upload process is done
        $scope.uploading = false;
      }, 1);

    });
  }
}

// the envelopes
// envelopes scope ctrl
function EnvelopesCtrl($scope) {
  // unused for now
}
// 0. info 
function InfoCtrl($scope){
  $scope.info = info; // todo, set this after loading the envelope data

  // save current state
  $scope.save = function(){
    LT17.save({
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      envelopes : [ // the envelopes
        {
          info : { // save an envelope with name: `info`
            data : {
              period : $scope.info.period // the data to be saved
            }
          }
        }
      ]
    }, function(err, ctx){
      if (err) {
        // todo handle error
        console.log (err);
        return;
      }
      $scope.dirty = false;
    });
  }

  // update data
  $scope.update = function(ctx){
    if (ctx.data){
      $scope.info.period = ctx.data.period;
    }
    $scope.dirty = false;
  }

  // watches the `period` 
  $scope.$watch('info.period', function(){
    if (LT17.infoFirstLoad){
      LT17.infoFirstLoad = false;
    } else {
      $scope.dirty = true;  
    }
  });

}
// 1. technical
function TechnicalCtrl($scope, $timeout){
  $scope.technicals = info.technicals; // todo: set this after loading the envelope data

  // check whether each row has correspondence files or not
  function checkFilledRows(){
    for (var i = 0; i < $scope.technicals.length; i++){
      $scope.technicals[i].files = $scope.technicals[i].files || [];
      if ($scope.technicals[i].files.length == 0){
        $scope.filled = false;
        break;
      } else {
        $scope.filled = true;
      }
    }
  }

  // check the package status, is exits, is rhs?
  function checkPackage(){
    LT17.check({
      type : 'pack',
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      envelopes : ['technical', 'pricing'],
    }, function(err, ctx){

      if (err){
        // todo show error
        console.log (err);
        return;
      }

      var info = ctx.result['technical'];

      if (info.exists){

        var encrypted = {
          dir : info.fileDirPath,
          date : LT17.moment(info.lastModified).fromNow()
        };

        $timeout(function(){
          $scope.encrypted = encrypted;
        }, 1);
      }

      // check for pricing too
      var tabs = Object.keys(ctx.result);
      var ready = true;
      angular.forEach(tabs, function(tab){
        var status = ctx.result[tab];
        if (!status.exists || !status.isRHS) {
          ready = false;
        } 
      });

      // update the upload button if it's ready
      updateActions({ ready : ready });
    });
  }

  // open file
  $scope.open = function(href){
    LT17.open(href);
  }

  // select (multiple) files
  $scope.select = function(index){
    LT17.selectFile({index : index}, function(err, ctx){
      var idx = ctx.index;
      var files = $scope.technicals[idx].files || [];
      angular.forEach(ctx.files, function(name){
        files.push({name : name});
      });

      // next tick, oh, next tick!
      $timeout(function(){
        $scope.technicals[idx].files = files;
        $scope.dirty = true;
        checkFilledRows();
      }, 1);
    });
  }

  // remove a file
  $scope.remove = function (technical, index){
    technical.files.splice(index, 1);
    $scope.dirty = true;
    checkFilledRows();
  }

  // save current state
  $scope.save = function(thenEncrypt){

    // prepare the FilesMap
    var files = [];
    angular.forEach($scope.technicals, function(technical){
      if (technical.files.length > 0){
        var paths = [];
        angular.forEach(technical.files, function(file){
          paths.push(file.name);
        })
        files.push({name : technical.name , paths : paths });  
      }
    });

    // save the envelope
    LT17.save({
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      envelopes : [
        {
          technical : {
            files : files
          }
        }
      ]
    }, function(err, ctx){

      if (err){
        // todo show error to user
        console.log (err);
        // return;
      }

      console.log ("saved...");

      // if thenEncrypt
      if (thenEncrypt){

        console.log ("packing...");

        // encrypting
        $scope.encrypting = true;

        // start the packing process, encrypt the saved technical data
        LT17.pack({
          owner : user.id, // spse userId
          objId : info.id, // spse lelangId
          key : info.keypairs[0].public, // public key
          envelopes : ['technical']

        }, function(err, ctx){

          $timeout(function(){
            $scope.encrypting = false;
            $scope.dirty = false;
            checkPackage();
          }, 1);

          if (err){
            console.log (err);
            return;
          }

        });

        return;

      }

      // if not thenEncrypt
      $timeout(function(){
        $scope.dirty = false;  
      }, 1)

    });
  }

  // update data
  $scope.update = function(ctx){

    checkPackage();

    $timeout(function(){

      // hopefully we always get a valid ctx files here
      var files = ctx.files;
      var paths = [];

      // it's ugly, I KNOW!
      for (var i= 0; i < $scope.technicals.length; i++){
        var appended = false;
        var name = $scope.technicals[i].name;
        for (var j = 0; j < files.length; j++){
          var file = files[j];
          if(name == file.name){
            appended = true;

            var rowFiles = [];
            angular.forEach(file.paths, function(path){
              rowFiles.push ({name : path});
            });

            // manage the selected files to be saved in the row
            paths.push({name : name, files :  rowFiles});
            break;
          } 
        }

        if (!appended){
          // push the original row
          paths.push({name : name, files : $scope.technicals[i].files || [] });
        }
        
      }

      // sync it out!
      $scope.technicals = paths;

    }, 1);


  }

  // pack the envelope
  $scope.pack = function(){}

  // restart the data, sync with server data
  $scope.restart = function(){}

}
// 2. pricing
function PricingCtrl($scope, $timeout){

  // open file
  $scope.open = function(href){
    LT17.open(href);
  }

  function checkPackage(){

    LT17.check({
      type : 'pack',
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      envelopes : ['pricing', 'technical'],
    }, function(err, ctx){

      if (err){
        // todo show error
        console.log (err);
        return;
      }

      var info = ctx.result['pricing'];

      if (info.exists && info.isRHS){

        var encrypted = {
          dir : info.fileDirPath,
          date : LT17.moment(info.lastModified).fromNow()
        };

        $timeout(function(){
          $scope.encrypted = encrypted;
        }, 1);
      }

      // check for technical too
      var tabs = Object.keys(ctx.result);
      var ready = true;
      angular.forEach(tabs, function(tab){
        var status = ctx.result[tab];
        if (!status.exists || !status.isRHS) {
          ready = false;
        } 
      });

      // set upload button to be ready
      updateActions({ ready : ready });

    })
  }

  // get the pricing detail array
  function getPricingDetail(){
    return $('#pricing-detail').data('handsontable').getData();
  }

  // get the summary 
  function getPricingSummary(){
    return {
      subtotal : $scope.subtotal,
      total : $scope.total,
      ppn : $scope.ppn
    }
  }

  // save the current state
  $scope.save = function(thenEncrypt){
    var data = {
      detail : getPricingDetail(),
      summary : getPricingSummary()
    };

    LT17.save({
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      envelopes : [
        {
          pricing : {
            data : data
          }
        }
      ]
    }, function(err, ctx){

      console.log ("saved...");

      if (err){
        // todo: show the error to user
        console.log (err);
        return;
      }

      if (thenEncrypt){
        
        // encrypting
        $scope.encrypting = true;

        console.log ("encrypting...");

        LT17.pack({
          owner : user.id, // spse userId,
          objId : info.id, // spse lelangId
          envelopes : ['pricing'],
          key : info.keypairs[0].public // if we have different keys, then we could put another key in keypairs
        }, function(err, ctx){

          $timeout(function(){
            $scope.encrypting = false;
          }, 1);
          

          if (err){
            // todo: failed to save, then show the error to the user
            console.log (err);
            return;
          }

          // it's done. it's clean
          $timeout(function(){
            $scope.dirty = false;
            checkPackage();
          }, 1);

        })

        return;

      }

      // it's saved, not dirty anymore
      $timeout(function(){
        $scope.dirty = false;
      }, 1);
          

    })
  }

  // update data
  $scope.update = function(ctx){
    var scopePricings = [];

    ctx.data = ctx.data || {};
    ctx.data.detail = ctx.data.detail || [];

    if (ctx.data.detail.length == 0){
      $scope.dirty = true;
      ctx.data.detail = info.pricings;
    }

    angular.forEach(ctx.data.detail, function(detail){
      scopePricings.push(detail);
    });

    scopePricings = scopePricings.length > 0 ? scopePricings : info.pricings;
    $scope.pricings = scopePricings;

    if (LT17.pricingFirstLoad){
      initPricingTab($scope.pricings);  
      LT17.pricingFirstLoad = false;
    }

    checkPackage();
  }

  // pack the envelope
  $scope.pack = function(){}

  // restart the data, sync with server data
  $scope.restart = function(){}
}

// hacks and inits
// small bootstrap tab ugly hacks
function initTabs(){
  // init
  var tab = $('#tabs a')[0];
  $(tab).click();
}

// helper on init pricing tab
function initPricingTab(pricingData){

  if (pricingData.length == 0) {
    pricingData.push({
      name : null, 
      unit1 : null, 
      volume1: 1, 
      unit2: null, 
      volume2: 1, 
      price: 0, 
      price_total: 0, 
      remark: null
    });
  }

  $("#pricing-detail").handsontable({

    // headers
    colHeaders:[ 
      "Jenis Barang", 
      "Satuan", 
      "Volume", 
      "Satuan 2", 
      "Volume 2", 
      "Harga Satuan", 
      "Total harga Satuan", 
      "Keterangan"],

    rowHeaders: true,

    data : pricingData,

    dataSchema : { 
      name : null, 
      unit1 : null, 
      volume1: 1, 
      unit2: null, 
      volume2: 1, 
      price: 0, 
      price_total: 0, 
      remark:null
    },

    contextMenu: {
      items : {
        row_above : {
          name : 'tambah 1 baris di atas'
        },
        row_below : {
          name : 'tambah 1 baris di bawah'
        },
        remove_row : {
          name : 'hapus baris ini'
        },
        undo : {
          name : 'undo'
        },
        redo : {
          name : 'redo'
        }
      }
    },

    columns: [
      {data:"name"}, 
      {data:"unit1"}, 
      {data:"volume1", type:'numeric'}, 
      {data:"unit2"}, 
      {data:"volume2", type:'numeric'}, 
      {data:"price", type:'numeric', format:'Rp 0,0.00'},
      {data:"price_total",type:'numeric', format:'Rp 0,0.00'}, 
      {data:"remark"}
    ],

    cells : function(row, col, prop){
      var cellProperties = {};
      if (col == 6 && pricingData[row]) {
        cellProperties.readOnly = true;
        pricingData[row].price_total = pricingData[row].volume1 * pricingData[row].volume2 * pricingData[row].price;
      }
    },

    afterChange : function(change, source){
      updatePricingSummary(pricingData);
    }
  });
}

// 
function checkPackages(){
  LT17.check({
      type : 'pack',
      owner : user.id, // spse userId
      objId : info.id, // spse lelangId
      envelopes : ['pricing', 'technical'],
    }, function(err, ctx){

      if (err){
        // todo show error
        console.log (err);
        return;
      }

      // check for technical too
      var tabs = Object.keys(ctx.result);
      var ready = true;
      angular.forEach(tabs, function(tab){
        var status = ctx.result[tab];
        if (!status.exists || !status.isRHS) {
          ready = false;
        } 
      });

      // set upload button to be ready
      updateActions({ ready : ready });
    });
}

// helper on updating tab data
function updateTab(ctx){
  var envelopes = ctx.envelopes;
  var keys = Object.keys(envelopes);
  angular.forEach(keys, function(key){
    key = key.split('-')[0];
    var envelopeScope = angular.element('#' + key).scope();
    envelopeScope.$safeApply(function(){
      envelopeScope.update(envelopes[key]);
    });
  });
  checkPackages();
}

// helper on upload and other actions
function updateActions(ctx){
  var actionsScope = angular.element('#actions').scope();
  actionsScope.$safeApply(function(){
    actionsScope.update(ctx);
  });
}

// helper on progresses scope
function updateProgresses(ctx){
  var progressesScope = angular.element('#progresses').scope();
  progressesScope.$safeApply(function(){
    progressesScope.update(ctx);
  });
}

// helper on updating pricing
function updatePricingSummary(pricingData, flag){
  var pricingScope = angular.element('#pricing').scope();
  var total = 0;
  var ppn = 0;
  var hps_total = 0;

  pricingScope.$safeApply(function(){
    pricingScope.subtotal = total;
    pricingScope.ppn = total;
    pricingScope.total = hps_total;
  });

  for (var i = 0; i < pricingData.length; i++) {
    total += pricingData[i].volume1 * pricingData[i].volume2 * pricingData[i].price;
  }

  if (total > 0) {
    ppn = (10 * total) / 100;
    hps_total = total + ppn;

    pricingScope.$safeApply(function(){
      pricingScope.subtotal = total;
      pricingScope.ppn = ppn;
      pricingScope.total = hps_total;
      // makes it dirty
      if (!LT17.pricingFirstLoad){
        pricingScope.dirty = true;  
      }
      
    });
  }
}

function init(ctx){

  // tabs onclick handler
  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');

    var href = $(this).attr('href');
    if (href.indexOf('pricing') >= 0){
      $('#pricing').show();
    } else {
      $('#pricing').hide();
    }

    // update tab data by loading data from db
    var key = href.substr(1).split('-')[0];
    LT17.load({
      objId : ctx.objId,
      owner : ctx.owner,
      envelopes : [key]
    }, function(err, ctx){
      if (err){
        // todo show error loading envelope data
        console.log (err);
      }

      // update the tab data
      updateTab(ctx);

    })
  });

  // hacks!
  initTabs();
}

// on ready
$(function(){

  // user popover, cosmetics  
  $('#user').popover({
    content : user.address + ' ' + user.phone,
    title : user.name + ' (' + user.username + ')',
    trigger : 'hover'
  });

  // firstLoad flags
  LT17.infoFirstLoad = true;
  LT17.technicalFirstLoad = true;
  LT17.pricingFirstLoad = true;
  
  // prepare session  
  LT17.prepare({
    owner : user.id, // spse userId
    objId : info.id // spse lelangId
  }, function(err, ctx){
    if (err) {
      // todo show error on loading session
      console.log (err);
      return;
    }
    // init the page using the loaded session
    init(ctx);
  });
});