function UserCtrl($scope){
     $scope.user = user;
}     

function InfoCtrl($scope){
    $scope.info = info;
}

function ServerListCtrl($scope, $window) {
     $scope.rows = []
     $scope.currentIdx = -1;
     $scope.save = function(row) {
          // get current tab, associate the selected server info to the tab
          // $window.reloadEnvelope();
          var selectedServer = $scope.rows[$scope.currentIdx];
          var serializedSelectedServer = angular.toJson(selectedServer);

          var serialized = angular.toJson($scope.currentCtx);
          var task = JSON.parse(serialized);
          task.userId = user.id;
          task.lelangId = info.lelangId;
          task.envelope = $window.current.envelope;
          task.selectedServer = JSON.parse(serializedSelectedServer);
          task.data = JSON.stringify(task, null, 2);
          task.selectServer = true;
          Apendo.taskSave(task);
        }

        $scope.select = function(row) {
          $scope.currentIdx = $scope.rows.indexOf(row);
        }
      }

      function NavTabsCtrl($scope, $window){
        $scope.navs = []
        for (var i = 0; i < envelopes.length; i++) {
          $scope.navs.push({ href : "#envelope-" + i, title : envelopes[i].title, index : i });
        }

        $scope.tabChanged = function(idx){
          if (idx == undefined || idx == null) return ;
          $window.current.envelope = idx;
          var key = user.id + ":" + info.lelangId + ":" + idx;
          var ctx = { key : key};
          ctx.workspace = window.workspace;
          Apendo.taskLoad(ctx);
        }
      }

      function TabsCtrl($scope, $window) {
        $scope.tabs = [];

        $scope.hideActions = function(){
          for (var i = 0; i < $scope.tabs.length; i++) {
            $scope.tabs[i].changed = false;
          }
        }

        $scope.saveTab = function(tab) {

          var index = $scope.tabs.indexOf(tab);
          $scope.tabs[index].changed = false;
          $scope.tabs[index].forcedToBeEncrypted = false;

          var serialized = angular.toJson(tab);
          var task = JSON.parse(serialized);
          task.envelope = $window.current.envelope;
          task.userId = user.id; 
          task.lelangId = info.lelangId;
          task.data = JSON.stringify(task);
          console.log('eksekusi TabsCtrl::saveTab')
          // this is silly, we should have another property
          // bug: when server is not set, but already bundled, if we edit row, no save buttom yet
          if (task.btnTitle.toLowerCase() == "unggah") {
            //Apendo.uploadStart(task);
            var rows = task.rows;
            var hasForm = false;
            var hasFile = false;
            for (var i = 0; i < rows.length; i++) {
              var row = rows[i];
              if (row.type == "form") hasForm = true;
              if (row.type == "file") hasFile = true;
            }

            task.hasFile = hasFile;
            task.hasForm = hasForm;
            task.workspace = window.workspace;
            Apendo.uploadStart(task);
          }
          else {
            Apendo.taskSave(task);  
          }
          
        }

        $scope.selectServerTab = function(tab) {
          var serialized = angular.toJson(tab);
          var task = JSON.parse(serialized);
          task.uploadServer = uploadServer;
          task.envelope = $window.current.envelope;
          task.userId = user.id; 
          task.lelangId = info.lelangId;
          task.data = JSON.stringify(task);
          Apendo.uploadSelectServer(task);
        }

        $scope.undoTab = function(tab) {          
          var index = $scope.tabs.indexOf(tab);          
          $scope.tabs[index].changed = false;
          var key = user.id + ":" + info.lelangId + ":" + index;
          var ctx = {
            key : key
          }
          ctx.undo = true;
          Apendo.taskLoad(ctx);
        }

        $scope.open = function(row) {

          // get context
          var context = JSON.parse(angular.toJson(row));
          context.workspace = window.workspace;

          $window.current = {
            envelope : row.envelope,
            index : row.index
          }

          if (context.type == 'file') {
            Apendo.filesSelect(context);
          } else {
            // open form
            setTimeout(function(){
              
              function remoteCallback(e, status, xhr){
                if (status != "success") {
                  $('#spse-form-modal').modal('hide');
                  // todo: alert error
                  // the remote form is failed to be loaded, show error message here
                }
              }

              $('#spse-form-modal').modal({ remote : context.url, remoteCallback : remoteCallback });  
              
              // todo show loading form 
              Apendo.log("loading ...")

            }, 250);
          }          
        }

        $scope.openFile = function(file, row) {
          var context = JSON.parse(angular.toJson(row));
          context.workspace = window.workspace;

          $window.current.index = row.index

          if (context.type == 'file') {

            // open file using coreplugin/viewerobject
            Apendo.fileOpenExternally(file);

          } else {
            $scope.open(row);
          }
        }

        $scope.removeFile = function(file) {
          var i, j;
          for (i = 0; i < $scope.tabs.length; i++) {
            var rows = $scope.tabs[i].rows
            if (rows)
            for (j = 0; j < rows.length; j++) {
              
              var idx = $scope.tabs[i].rows[j].files.indexOf(file);

              if (idx >= 0) { 

                $scope.tabs[i].rows[j].files.splice( idx, 1 );
                // TODO: physically remove the file

                if ($scope.tabs[i].rows[j].files.length == 0) {
                  $scope.tabs[i].rows[j].filled = false; 
                }

                var btnTitle = "Simpan";

                if (envelopeReady(window.current.envelope)) {
                  btnTitle = "Simpan dan Lakukan Enkripsi";
                } 

                $scope.tabs[i].forcedToBeEncrypted = false;
                $scope.tabs[i].changed = true;
                $scope.tabs[i].btnTitle = btnTitle;
                
                return;
              }            
            }
          }
        }

        for (var i = 0; i < envelopes.length; i++) {
          var envelope = envelopes[i]
          var tab = { 
            id : "envelope-" + i,
            encryptionKey : envelope.key,
            title : envelope.title,
            rows : envelope.rows,
            forcedToBeEncrypted : false
          }
          $scope.tabs.push(tab);
        }
      }

      function ProgressCtrl($scope, $timeout, $window) {
        $scope.tasks = []
        $scope.finishedTasks = []
        $scope.$watch('tasks', function(){ 
          if ($scope.tasks.length > 0) {
            var tabScope = angular.element('.tab-content').scope();
            tabScope.$safeApply(function(){
              tabScope.hideActions();
              Apendo.log("hide actions!");
            })
          }
        }, true);

        $scope.isActive = function(){
          return ($scope.tasks.length > 0);
        }

        $scope.finished = function() {
          // on finished
          $timeout(function(){
            if ($scope.finishedTasks.length > 0) {
              var id = $scope.finishedTasks.pop(0);
              var idx = $scope.index(id);
              $scope.tasks.splice(idx, 1);
            } 
            
            $window.reloadEnvelope();

          }, 800); // magic number, damn!
        }

        $scope.index = function(id){
          var i = 0;
          for (i = 0; i < $scope.tasks.length; i++) {
            if ($scope.tasks[i].id == id) {
              return i;
            }
          }
        }

        $scope.update = function(obj) {
          var idx = -1;

          idx = $scope.index(obj.id);

          if (idx >= 0) {

            $scope.tasks[idx].value = obj.value;
            Apendo.log("obj.value: " + obj.value);

            if (obj.finished) {
              $scope.finishedTasks.unshift(obj.id);
              $scope.finished();
            }

          } else {
            $scope.tasks.push(obj);
          }
        }

        $scope.resume = function(task) {
          Apendo.log("resume: " + task.path);
          Apendo.uploadResume(task);
        }
        $scope.pause = function(task) {
          Apendo.log("pause: " + task.path);
          //Apendo.uploadPause(task);
          Apendo.uploadResume(task);
        }
        $scope.cancel = function(task) {
          Apendo.log("cancel: " + task.path);
          Apendo.uploadCancel(task);
        }
      }

      function workspace(){

        var workspaceInfo = { 
          root : Apendo.userWorkspace, 
          userId : user.id, 
          lpse : info.lpse,
          lelangId : info.lelangId
        }

        var dir = workspaceInfo.root;

        var count = 0;
        for (var k in workspaceInfo) {
          if (count++ > 0) {
            dir += "/" + workspaceInfo[k];  
          }
        }

        workspaceInfo.dir = dir;
        window.workspace = workspaceInfo;

        $("#workspace").html('<strong>' + workspaceInfo.dir + '</strong>');

        Apendo.userPrepareWorkspace(workspaceInfo); 
      }

      function parseGbgPath(gbgFile){
        Apendo.log(gbgFile);
        var re = /[a-z0-9]+/g
        var res = []
        for (var i = 0; i < 4; i++) {
          var arr = re.exec(gbgFile);  
          if (!arr) return res;
          res.push(arr[0]);
        }
        return res;
      }

      function envelopeReady(idx){

        var tabScope = angular.element('.tab-content').scope();
        var rows = tabScope.tabs[idx].rows;
          
        for (var i = 0; i < rows.length; i++) {
          var row = rows[i];
          if (!row.filled) {
            return false;
          } 
        }
        return true;
      }

      $(function(){

        $('#tabs a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        })

        $($('#tabs a')[0]).click();

        // find out how to reload modal
        $(document).on('hidden.bs.modal', function (e) {
          if ($(e.target).attr('id') != "spse-server-modal") {
            $(e.target).removeData('bs.modal').html('');
          }
        });

        workspace();

        var connected = false

        function reloadEnvelope(){
          if (Apendo && window.current.envelope != undefined && window.current.envelope != null && window.workspace) {
            var key = user.id + ":" + info.lelangId + ":" + window.current.envelope;
            var scopeCtx = {
              key : key,
              workspace : window.workspace
            }
            Apendo.log("reload: " + key);
            Apendo.taskLoad(scopeCtx);
          }
        }

        // reload envelope
        window.reloadEnvelope = reloadEnvelope;

        // connections
        if (!connected) {
          connected = true;

          var tabScope = angular.element('.tab-content').scope();
          var tasksScope = angular.element('#tasks').scope();
          var serverListScope = angular.element('#servers').scope();

          // files selected
          var onFileSelected = function(ctx){

            for (var i = 0; i < ctx.files.length; i++) {
              var file = ctx.files[i];

              tabScope.$safeApply(function(){
                
                var arr = file.split("/");
                var filename = arr[arr.length - 1];

                tabScope.tabs[ctx.envelope].rows[ctx.index].filled = true;

                tabScope.tabs[ctx.envelope].rows[ctx.index].files.push({
                  href : file,
                  name : filename
                });

                var btnTitle = "Simpan";

                if (envelopeReady(window.current.envelope)) {
                  btnTitle = "Simpan dan Lakukan Enkripsi";
                } 

                tabScope.tabs[ctx.envelope].changed = true;
                tabScope.tabs[ctx.envelope].hasNoServer = false;
                tabScope.tabs[window.current.envelope].forcedToBeEncrypted = false;
                tabScope.tabs[ctx.envelope].btnTitle = btnTitle;
                
                return;

              })
          }
        };
          
          // on task loaded, tab data is loaded
        var onTaskLoaded = function(ctx){
          var raw = ctx.data;
          var obj;

          try {
            obj = JSON.parse(raw);
          } catch (e) {
            // ignoring this :-D

            var onTaskLoadedEmptyUpdate = function(){
              var rows = tabScope.tabs[window.current.envelope].rows;
              for (var i = 0; i < rows.length; i++) {
                tabScope.tabs[window.current.envelope].rows[i].files = [];
              }
            };

            tabScope.$safeApply(onTaskLoadedEmptyUpdate);
          }

          if (obj) {
            var onTaskLoadedUpdate = function(){

              tabScope.tabs[window.current.envelope].rows = obj.rows;

              var btnTitle = "Simpan";

              if (envelopeReady(window.current.envelope)) {
                btnTitle = "Simpan dan Lakukan Enkripsi";

                if (ctx.encrypted) {

                  tabScope.tabs[window.current.envelope].encrypted = ctx.encrypted;


                  if (obj.selectedServer) {
                    btnTitle = "Unggah";
                    tabScope.tabs[window.current.envelope].hasNoServer = false;
                    tabScope.tabs[window.current.envelope].selectedServer = obj.selectedServer;
                    tabScope.tabs[window.current.envelope].btnServerTitle = obj.selectedServer.name;
                  } else {
                    // todo check if server is not yet selected
                    tabScope.tabs[window.current.envelope].hasNoServer = true;
                    tabScope.tabs[window.current.envelope].btnServerTitle = "Pilih Server";
                  }
                  
                }
                
                tabScope.tabs[window.current.envelope].changed = true;
                tabScope.tabs[window.current.envelope].forcedToBeEncrypted = true;

              } else {
                tabScope.tabs[window.current.envelope].forcedToBeEncrypted = false;
              }

              tabScope.tabs[window.current.envelope].btnTitle = btnTitle;

              Apendo.log("is active: " + tasksScope.isActive());

              if (tasksScope.isActive()) {

                tasksScope.hideActions();
              }
              
              return;
            };

            tabScope.$safeApply(onTaskLoadedUpdate);
          }
        };

        // on task saved
        var onTaskSaved = function(ctx){
          // Apendo.log(JSON.stringify(ctx, null, 2));
          // TODO: do something fun here
          Apendo.log(ctx.key + " saved, encryption ready: " + ctx.encryptionReady);

          if (ctx.encryptionReady && !ctx.selectServer ) {

            // manipulate the ctx
            var scopeCtx = {
              encryptionKey : ctx.encryptionKey,
              userId : ctx.userId,
              lelangId : ctx.lelangId,
              envelope : ctx.envelope,
              envelopeId : ctx.id
            };

            var rows = ctx.rows;

            var map = {}
            var files = []
            var forms = []

            for (var i = 0; i < rows.length; i++) {
              var rowFiles = rows[i].files;
              var type = rows[i].type
              var id = rows[i].id
              map[id] = {
                files : []
              }
              for (var j = 0; j < rowFiles.length; j++) {
                var obj = rowFiles[j];
                obj.type = type;
                obj.id = id;
                obj.rowIndex = i + "";
                map[id].files.push(obj);
                if (type == "file") {
                  files.push(rowFiles[j].href);  
                } else {
                  forms.push(rowFiles[j].href);  
                }
              }
            }

            scopeCtx["jsonMap"] = map;
            scopeCtx["files"] = files;
            scopeCtx["forms"] = forms;
            scopeCtx["workspace"] = window.workspace;
            scopeCtx["name"] = ctx.key;

            Apendo.packStart(scopeCtx); 

          } else {
            reloadEnvelope();
          }
        };

        var onFormSaved = function(ctx){

          // update row with resume
          if (ctx.success) {
            var meta = ctx.meta;

            var onFormSavedUpdate = function(){

              tabScope.tabs[meta.envelope].rows[meta.index].filled = true;

              if (tabScope.tabs[meta.envelope].rows[meta.index].files.length > 0){
                tabScope.tabs[meta.envelope].rows[meta.index].files[0] = {
                  href : ctx.href,
                  name : ctx.resume
                }
              } else {

                tabScope.tabs[meta.envelope].rows[meta.index].files.push({
                  href : ctx.href,
                  name : ctx.resume
                });
              }

              var btnTitle = "Simpan";

              if (envelopeReady(window.current.envelope)) {
                btnTitle = "Simpan dan Lakukan Enkripsi";
              } 

              tabScope.tabs[meta.envelope].changed = true;
              tabScope.tabs[meta.envelope].hasNoServer = false;
              tabScope.tabs[window.current.envelope].forcedToBeEncrypted = false;
              tabScope.tabs[meta.envelope].btnTitle = btnTitle;

              return;
            }

            tabScope.$safeApply(onFormSavedUpdate);
          }
        }

        var onPackStarted = function(ctx){
          tasksScope.$safeApply(function(){
            var key = ctx.key;
            var title = "Melakukan Enkripsi untuk " + envelopes[ctx.envelope].title + " (" + ctx.type + ")" ;
            // id, title, value, finished
            tasksScope.update({id : key, title : title, value : 0});
          });
        }

        var onPackUpdated = function(ctx){
          tasksScope.$safeApply(function(){
            var key = ctx.key;
            tasksScope.update({id : key, value : ctx.pos});
          });
        }

        var onPackFinished = function(ctx){
          tasksScope.$safeApply(function(){
            // if status == false, error
            tasksScope.update({id : ctx.key, value : 100, finished : true});
          });
        }

        var onUploadStarted = function(ctx) {
          tasksScope.$safeApply(function(){

            var info = parseGbgPath(ctx.fileName);

            if (info.length == 4) {
              var envelope = info[2];
              var key = info.join(":");
              var title = "Mengunggah Paket " + envelopes[envelope].title + " (" + info[3] + ")";
              tasksScope.update({id : key, path : ctx.path, title : title, value : 0, uploading : true, paused : false });
            }

          });
        }

        var onUploadProgress = function(ctx) {
          tasksScope.$safeApply(function(){

            var info = parseGbgPath(ctx.fileName);

            if (info.length == 4) {
              var envelope = info[2];
              var key = info.join(":");
              var title = "Mengunggah Paket " + envelopes[envelope].title + " (" + info[3] + ")";
              tasksScope.update({id : key, title : title, value : ctx.percent, uploading : true, paused : false });
            }

          });
        }

        var onUploadFinished = function(ctx) {
          tasksScope.$safeApply(function(){
            
            var info = parseGbgPath(ctx.fileName);

            if (info.length == 4) {
              var envelope = info[2];
              var key = info.join(":");
              var title = "Mengunggah Paket " + envelopes[envelope].title + " (" + info[3] + ")";
              tasksScope.update({id : key, title : title, value : 100, finished : true, uploading : false, paused : false });
            }

          });
        }

        var onUploadGotServer = function(ctx) {
          // todo render ctx.servers.listServer
          serverListScope.$safeApply(function(){
            //
            //
            serverListScope.rows = [];
            serverListScope.currentCtx = ctx;
            serverListScope.currentIdx = -1;

            var previousServer = ctx.selectedServer;

            for(var i = 0; i < ctx.servers.listServer.length; i++) {
              var server = ctx.servers.listServer[i];

              // use && instead?
              if (previousServer) {
                if (server.id == previousServer.id) {
                  serverListScope.currentIdx = i;  
                }
              }
              serverListScope.rows.push(server);
            }

            $('#spse-server-modal').modal();  
          })
        }
        // common tasks
        Apendo.filesSelected.connect(onFileSelected);
        Apendo.taskLoaded.connect(onTaskLoaded);
        Apendo.taskSaved.connect(onTaskSaved);
        Apendo.formSaved.connect(onFormSaved);

        // pack plugin
        Apendo.packStarted.connect(onPackStarted);
        Apendo.packUpdated.connect(onPackUpdated);
        Apendo.packFinished.connect(onPackFinished);

        // upload plugin
        Apendo.uploadStarted.connect(onUploadStarted);
        Apendo.uploadProgress.connect(onUploadProgress);
        Apendo.uploadFinished.connect(onUploadFinished);

        Apendo.uploadGotServer.connect(onUploadGotServer);

        Apendo.log("connect");

      }
})
function toRupiah(jumlah) 
{ 
    var titik = ".";
    var nilai = new String(jumlah); 
    var pecah = []; 
    while(nilai.length > 3) 
    { 
        var asd = nilai.substr(nilai.length-3); 
        pecah.unshift(asd); 
        nilai = nilai.substr(0, nilai.length-3); 
    } 	 
    if(nilai.length > 0) { pecah.unshift(nilai); } 
    nilai = pecah.join(titik);
    return "Rp "+nilai+",00"; 
}