// user ctrl
function UserCtrl($scope){
  $scope.logout = function(){
    disconnects();
    Apendo.userLogout(user);
  }
  $scope.user = user;
}

// info ctrl
function InfoCtrl($scope){
  $scope.info = info;
}

// tabs navigation ctrl 
function NavTabsCtrl($scope){
  $scope.hasSorted = false;
}

// progress bars ctrl
function ProgressCtrl($scope){
  $scope.ready = false;
}

function ActionsCtrl($scope, $timeout){

}

function VendorsCtrl($scope){
  $scope.hasSorted = false;

  $scope.open = function(index, row){

      var ctx = {
        lpse : info.lpse,
        lelangName : info.name,
        lelangId : info.id,
        userId : user.id
      }

      if (row.extractedDir){
        Apendo.fileOpenExternally({ href : row.extractedDir});
      } else {
        ctx.url = row.technical.url;
        ctx.name = row.technical.name;
        ctx.userId = row.user._id || row.user.id;
        ctx.userName = row.user.name;
        ctx.dataType = 'technical';
        ctx.index = index;

        row.downloading = true;
        row.taskLabel = "Mengunduh...";
        Apendo.downloadStart(ctx);
      }
  }
}

function disconnects(){
  try{
  Apendo.downloadFinished.disconnect(onDownloadFinished);
  } catch(ex){}
  try{
  Apendo.unpackStarted.disconnect(onUnpackStarted);
  } catch(ex){}
  try{
  Apendo.unpackFinished.disconnect(onUnpackFinished);
  } catch(ex){}
  try{
  Apendo.payloadSubmitted.disconnect(onPayloadSubmitted);
  } catch(ex){}
  try{
  Apendo.sessionLoaded.disconnect(onSessionLoaded);
  } catch(ex){}
}

var onDownloadFinished = function(ctx){
  ctx.key = info.keypairs[0].private;
  ctx.workspace = Apendo.userWorkspace;
  console.log ('onDownloadFinished', ctx);
  // a downloadFinished
  Apendo.unpackStart(ctx);
}

var onUnpackStarted = function(ctx){
  console.log (ctx);
}

var onPayloadSubmitted = function(ctx){
  // todo if ctx.success == false
  updateCurrentTask({ status : "", hasSorted : true, filtered : JSON.parse(ctx.data) }); // todo handle exception
}

var onUnpackFinished = function(ctx){

  if (ctx.dataType == 'pricing') {
    // read the json file, and write to temp file for payload
    count++;
    payloadCtxs.push(ctx);

    // an unpackFinished

    if (count == submissions.length){

      // all unpackFinished
      var ready = true;

      for (var i = 0; i < payloadCtxs.length; i++){
        var appended = Apendo.appendPayload(payloadCtxs[i]);
        if (!appended){
          updateCurrentTask({ status : "Terjadi kesalahan!"});
          ready = false;
          break;
        }
      }
      
      if (ready){
        
        ctx.url = sorting_url;
        ctx.userId = user.id;

        updateCurrentTask({ status : "Mengurutkan..."});
        setTimeout(function () {
          console.log ('submit payload', ctx);
          Apendo.submitPayload(ctx);
          count = 0;
          payloadCtxs = [];
        }, 2000);
      } else {
        updateCurrentTask({ status : "Terjadi kesalahan. Mencoba lagi proses unduh..."});
        download();
      }
    }

  } else if (ctx.dataType == 'technical'){
    
    // an unpackFinished of technical
    console.log (ctx);

    var vendorsScope = angular.element("#vendors").scope();
    vendorsScope.$safeApply(function(){
      vendorsScope.filtered[ctx.index].downloading = false;
      vendorsScope.filtered[ctx.index].extractedDir = ctx.extractedDir;
      vendorsScope.filtered[ctx.index].taskLabel = "Lihat";
    });
  }
}

var onSessionLoaded = function (ctx){
  $($('#tabs a')[0]).click();
  updateCurrentTask({ status : "Menyiapkan..."});
  setTimeout(download, 1000);  
}

var updateNavTabs = function(ctx){
  var navTabsScope = angular.element("#tabs").scope();
  navTabsScope.$safeApply(function(){
    for (var k in ctx){
      navTabsScope[k] = ctx[k];
    }
  });
}

var updateVendors = function(ctx){
  var vendorsScope = angular.element("#vendors").scope();
  vendorsScope.$safeApply(function(){
    for (var k in ctx){
      vendorsScope[k] = ctx[k];
    }
  });
}

var updateCurrentTask = function (ctx){
  var progressScope = angular.element('#tasks').scope();
  progressScope.$safeApply(function(){
    progressScope.currentTask = ctx.status;
    progressScope.type = progressScope.type || 'info';

    updateNavTabs({hasSorted : ctx.hasSorted});
    updateVendors({hasSorted : ctx.hasSorted, filtered : ctx.filtered });

    if (ctx.hasSorted){
      // load download info
      var scopeCtx = {
        lpse : info.lpse,
        lelangName : info.name,
        lelangId : info.id,
        userId : user.id,
      }

      for (var k in ctx){
        scopeCtx[k] = ctx[k];
      }
      Apendo.checkDownloadStatus(scopeCtx);
    }

  });
}

var download = function(force){
  var ctx = {
    lpse : info.lpse,
    lelangName : info.name,
    lelangId : info.id,
    userId : user.id,
    force : force
  }

  console.log ('preparePayload', ctx);

  if (!Apendo.preparePayload(ctx)) 
  {
    console.log ("error prepare payload!");
    return;
  } else {

    updateCurrentTask({ status : "Mengunduh..."});
    submissions.map(function(submission){
      ctx.url = submission.url + "?access_token=" + token;
      ctx.name = submission.url.split("/").pop() + ".rhs";
      ctx.userId = submission.userId;
      ctx.dataType = 'pricing';
      Apendo.downloadStart(ctx);
    });
  }
}

var count = 0;
var payloadCtxs = [];

$(function(){

  $('#user').popover({
    content : user.address + ' ' + user.phone,
    title : user.name + ' (' + user.id + ')',
    trigger : 'hover'
  });

  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  disconnects();

  Apendo.downloadFinished.connect(onDownloadFinished);
  Apendo.unpackStarted.connect(onUnpackStarted);
  Apendo.unpackFinished.connect(onUnpackFinished);
  Apendo.sessionLoaded.connect(onSessionLoaded);
  Apendo.payloadSubmitted.connect(onPayloadSubmitted);
  Apendo.sessionRequested({ key : user.id + ':' + info.id });
});



/*
 * boot the angular app
 */
var spseApp = angular.module('spseApp', ['Scope.safeApply']);