var spseApp = angular.module('spseApp', ['Scope.safeApply']);

function UserCtrl($scope){
  $scope.logout = function(){
    disconnects();
    Apendo.userLogout(user);
  }
  $scope.user = user;
}

function ActionsCtrl($scope, $timeout){

}

function NavTabsCtrl($scope){
  $scope.hasSorted = false;
}

function ProgressCtrl($scope){
  $scope.ready = false;
}

function InfoCtrl($scope){
  $scope.info = info;
}

function VendorsCtrl($scope){
  $scope.hasSorted = false;

  $scope.open = function(index, row){

      var ctx = {
        lpse : info.lpse,
        lelangName : info.name,
        lelangId : info.id,
        userId : user.id
      }

      if (row.extractedDir){
        Apendo.fileOpenExternally({ href : row.extractedDir});
      } else {
        ctx.url = row.technical.url;
        ctx.name = row.technical.name;
        ctx.userId = row.user._id || row.user.id;
        ctx.userName = row.user.name;
        ctx.dataType = 'technical';
        ctx.index = index;

        row.downloading = true;
        row.taskLabel = "Mengunduh...";
        Apendo.downloadStart(ctx);
      }
  }
}

var payloadCtxs = [];

$(function(){

  $('#user').popover({
    content : user.address + ' ' + user.phone,
    title : user.name + ' (' + user.id + ')',
    trigger : 'hover'
  });

  $('#tabs a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });

  disconnects();

  Apendo.sessionLoaded.connect(onSessionLoaded);
  Apendo.downloadFinished.connect(onDownloadFinished);
  Apendo.unpackStarted.connect(onUnpackStarted);
  Apendo.unpackFinished.connect(onUnpackFinished);
  Apendo.payloadSubmitted.connect(onPayloadSubmitted);
  Apendo.sessionRequested({ key : user.id + ':' + info.id });
});

var onSessionLoaded = function (ctx){
//  console.log ("onSessionLoaded", ctx);
  $($('#tabs a')[0]).click();
  updateCurrentTask({ status : "Menyiapkan..."});
  setTimeout(download, 1000);  
}

var updateNavTabs = function(ctx){
//  console.log ("updateNavTabs", ctx);
  var navTabsScope = angular.element("#tabs").scope();
  navTabsScope.$safeApply(function(){
    for (var k in ctx){
      navTabsScope[k] = ctx[k];
    }
  });
}

var updateCurrentTask = function (ctx){
//  console.log ("updateCurrentTask", ctx);
  var progressScope = angular.element('#tasks').scope();
  progressScope.$safeApply(function(){
    progressScope.currentTask = ctx.status;
    progressScope.type = progressScope.type || 'info';

    updateNavTabs({hasSorted : ctx.hasSorted});
    updateVendors({hasSorted : ctx.hasSorted, filtered : ctx.filtered });

    if (ctx.hasSorted){
      // load download info
      var scopeCtx = {
        lpse : info.lpse,
        lelangName : info.name,
        lelangId : info.id,
        userId : user.id,
      }

      for (var k in ctx){
        scopeCtx[k] = ctx[k];
      }

 //     console.log (scopeCtx);

      Apendo.checkDownloadStatus(scopeCtx);
    }

  });
}

var updateVendors = function(ctx){
//  console.log ("updateVendors", ctx);
  var vendorsScope = angular.element("#vendors").scope();
  vendorsScope.$safeApply(function(){
    for (var k in ctx){
      vendorsScope[k] = ctx[k];
    }
  });
}

var download = function(force){
  var ctx = {
    lpse : info.lpse,
    lelangName : info.name,
    lelangId : info.id,
    userId : user.id,
    force : force
  }
//  console.log ("download", ctx);
  
  if (!Apendo.preparePayload(ctx)) 
  {
//    console.log ("error prepare payload!");
    return;
  } else {
	console.log ("download", ctx);
    updateCurrentTask({ status : "Mengunduh..."});
    submissions.map(function(submission){
      ctx.url = download_url + "?access_token=" + token  + "&id=" + submission.id;
      ctx.name = submission.id + ".rhs";
      ctx.userId = user.id;
      ctx.dataType = 'pricing';
      Apendo.downloadStart(ctx);
    });
  }
}

var onDownloadFinished = function(ctx){
  ctx.key = info.privKey[0];
  ctx.workspace = Apendo.userWorkspace;
  console.log ('onDownloadFinished', ctx);
  Apendo.unpackStart(ctx);
}

var onUnpackStarted = function(ctx){
//  console.log ("onUnpackStarted", ctx);
}

var onUnpackFinished = function(ctx){
  console.log ("onUnpackFinished", ctx);
  if (ctx.dataType == 'pricing') {
    // read the json file, and write to temp file for payload
    payloadCtxs.push(ctx);
//    // an unpackFinished
//console.log (payloadCtxs.length, submissions.length);
    if (payloadCtxs.length == submissions.length){
//      // all unpackFinished
//      var ready = true;
//
      for (var i = 0; i < payloadCtxs.length; i++){
        var appended = Apendo.appendPayload(payloadCtxs[i]);
//console.log ("payloadCtxs["+i+"]", payloadCtxs[i]);
console.log ("appended", appended);
//        if (!appended){
//          updateCurrentTask({ status : "Terjadi kesalahan!, mencoba unduh ulang file"});
//          ready = false;
//          break;
//        }
      }
      
//      if (ready){
//        ctx.url = sorting_url;
//        ctx.userId = user.id;
//
//        updateCurrentTask({ status : "Mengurutkan..."});
//        setTimeout(function () {
//          console.log ('submit payload', ctx);
//          Apendo.submitPayload(ctx);
//          payloadCtxs = [];
//        }, 2000);
//      } else {
//        updateCurrentTask({ status : "Terjadi kesalahan. Mencoba lagi proses unduh..."});
//        download();
//      }
    }

  } else if (ctx.dataType == 'technical'){
    
    // an unpackFinished of technical
//    console.log (ctx);

    var vendorsScope = angular.element("#vendors").scope();
    vendorsScope.$safeApply(function(){
      vendorsScope.filtered[ctx.index].downloading = false;
      vendorsScope.filtered[ctx.index].extractedDir = ctx.extractedDir;
      vendorsScope.filtered[ctx.index].taskLabel = "Lihat";
    });
  }
}

var onPayloadSubmitted = function(ctx){
  // todo if ctx.success == false
//  console.log ("onPayloadSubmitted", ctx);
  updateCurrentTask({ status : "", hasSorted : true, filtered : JSON.parse(ctx.data) }); // todo handle exception
}

function disconnects(){
  try{
  Apendo.sessionLoaded.disconnect(onSessionLoaded);
  } catch(ex){}
  try{
  Apendo.downloadFinished.disconnect(onDownloadFinished);
  } catch(ex){}
  try{
  Apendo.unpackStarted.disconnect(onUnpackStarted);
  } catch(ex){}
  try{
  Apendo.unpackFinished.disconnect(onUnpackFinished);
  } catch(ex){}
  try{
  Apendo.payloadSubmitted.disconnect(onPayloadSubmitted);
  } catch(ex){}
}
