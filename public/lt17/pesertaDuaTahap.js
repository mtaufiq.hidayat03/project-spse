$(function(){
	$('#user').popover({
		content : user.email,
	    title : user.name,
	    trigger : 'hover'
	});

	LT17.infoFirstLoad = true;
	LT17.technicalFirstLoad = true;
	LT17.pricingFirstLoad = true;
	  
	LT17.prepare({
		owner : user.psr_id,
		objId : adm.lls_id
	}, function (err, data){
		if (err){
			console.log("prepare session error", err);
		}
		init(data);
	})
	
	LT17.check({
		type : 'pack',
		owner : user.psr_id,
		objId : adm.lls_id,
		envelopes : ['harga', 'teknis']
	}, function(err, data){
		if (err){
			console.log("check last encrypted error", err);
		}
		
		var infoTeknis = data.result['teknis'];
		var infoHarga = data.result['harga'];
		
		if (infoTeknis.exists && infoTeknis.isRHS && infoHarga.exists && infoHarga.isRHS){
			var teknisEncTime = LT17.moment(infoTeknis.lastModified).fromNow();
			var kirimScope = angular.element('#kirim').scope();
			kirimScope.info = 'Terakhir dilakukan enkripsi ' + teknisEncTime;
			kirimScope.encrypted = true;
		}
		
	})
});

function init(data){
	$('#tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		
		var href = $(this).attr('href');
		var key = href.substr(1).split('-')[0];
		if(key!='kirim'){
			if((key=='adm' && LT17.infoFirstLoad) || (key=='teknis' && LT17.technicalFirstLoad) || (key=='harga' && LT17.pricingFirstLoad)){
				LT17.load({
					objId : data.objId,
					owner : data.owner,
					envelopes : [key]
				}, function(err, data){
					if (err){
						console.log("load session error", err);
					}
					updateTab(data);
				});
			}
		} else {
			var kirimScope = angular.element('#kirim').scope();
			kirimScope.$safeApply(function(){
				kirimScope.update();
			});
		}
	});
	
//	$('.nav-tabs a[href=#adm]').tab('show') ;
	var tab = $('#tabs a')[0];
	$(tab).click();
	  
}

function updateTab(data){
	var envelopes = data.envelopes;
	var envKeys = Object.keys(envelopes);
	angular.forEach(envKeys, function(key){
		key = key.split('-')[0];
		var envelopeScope = angular.element('#' + key).scope();
		envelopeScope.$safeApply(function(){
			envelopeScope.update(envelopes[key]);
		});
	});
}

var lt17Peserta = angular.module('lt17Peserta', ['Scope.safeApply']);

lt17Peserta.filter('envelopeName', function(){
  return function(name){

    var dict = {
      teknis : 'Penawaran Administrasi & Teknis',
      harga : 'Penawaran Harga'
    }

    return 'Mengirim ' +  dict[name] || name;
  }
});

lt17Peserta.filter('percentName', function(){
  return function(percent){
    if (percent){
      return percent.toFixed(2) + ' %';  
    } else {
      return '';
    }
  }
});

	
function UserCtrl($scope){
	$scope.user = user;
	$scope.logout = function(){
		Apendo.userLogout(user);
	}
}

function AdmCtrl($scope){
	$scope.adm = adm;
	
	$scope.save = function (){
		LT17.save({
			owner : user.psr_id,
			objId : adm.lls_id,
			envelopes : [{
	            	 adm : {
	            		 data : {
	            			 berlaku : $scope.adm.berlaku
	            		 }
	            	 }
	             }]
		}, function (err, data){
			if (err){
				console.log("save adm error", err);
			}
			$scope.dirty = false;
		});
	}
	
	$scope.update = function(data){
		if (data.data.berlaku){
			$scope.adm.berlaku = data.data.berlaku;
			$scope.dirty = false;
		} else {
			$scope.dirty = true;
		}
	}
	
	$scope.$watch('adm.berlaku', function(){
		if (LT17.infoFirstLoad){
			LT17.infoFirstLoad = false;
		} else {
			$scope.dirty = true;
		}
	});
	
	$scope.allFilled = function(){
		return $scope.adm.berlaku > 0;
	}
	
}

function TeknisCtrl($scope, $timeout){
	
	$scope.teknisList = teknisList;
	
	$scope.update = function(data){
		$timeout(function(){
			var files = data.files;
			var paths = [];
			for (var i= 0; i < $scope.teknisList.length; i++){
				var appended = false;
				var ckmNama = $scope.teknisList[i].ckmNama;
				for (var j = 0; j < files.length; j++){
					var file = files[j];
					if(ckmNama == file.name){
						appended = true;
						var rowFiles = [];
						angular.forEach(file.paths, function(path){
							rowFiles.push ({name : path});
						});
						paths.push({ckmNama : ckmNama, files :  rowFiles});
						break;
					}
				}
				
				if (!appended){
					paths.push({ckmNama : ckmNama, files : $scope.teknisList[i].files || [] });
				}
			}
			
			$scope.teknisList = paths;
			LT17.technicalFirstLoad = false;
		}, 1);
	}
	
	$scope.select = function(index){
		LT17.selectFile({index : index}, function(err, data){
			if (err){
				console.log("select teknis error", err);
			}
			
			var idx = data.index;
			var files = $scope.teknisList[idx].files || [];
			angular.forEach(data.files, function(name){
				files.push({name : name});
			});
			
			$timeout(function(){
				$scope.teknisList[idx].files = files;
				$scope.dirty = true;
			}, 1);
		});
	}

	$scope.open = function(href){
		LT17.open(href);
	}
	
	$scope.remove = function (teknis, index){
		teknis.files.splice(index, 1);
		$scope.dirty = true;
	}
	
	$scope.save = function(){
		var files = [];
		angular.forEach($scope.teknisList, function(teknis){
			if (teknis.files.length > 0){
				var paths = [];
				angular.forEach(teknis.files, function(file){
					paths.push(file.name);
				})
				files.push({name : teknis.ckmNama , paths : paths });
			}
		});

	    LT17.save({
	      owner : user.psr_id,
	      objId : adm.lls_id,
	      envelopes : [{
	          teknis : {
	        	  files : files
	          }
	      }]
	    }, function(err, data){
	    	if (err){
				console.log("save teknis error", err);
			}
			
			$timeout(function(){
				$scope.dirty = false;
			}, 1)
	    });
	}
	
	$scope.allFilled = function(){
		var filled = false;
		for (var i = 0; i < $scope.teknisList.length; i++){
			$scope.teknisList[i].files = $scope.teknisList[i].files || [];
			if ($scope.teknisList[i].files.length == 0){
				filled = false;
				break;
			} else {
				filled = true;
			}
		}
		return filled;
	}
}

function HargaCtrl($scope, $timeout){
	
	$scope.open = function(href){
		LT17.open(href);
	}
	
	function getPricingDetail(){
		return $('#pricing-detail').data('handsontable').getData();
	}
	
	function getPricingSummary(){
		return {
			subtotal : $scope.subtotal,
			total : $scope.total,
			ppn : $scope.ppn
		}
	}
	
	$scope.save = function(){
		var detailHarga = {
			detail : getPricingDetail(),
			summary : getPricingSummary()
		};
		
		LT17.save({
			owner : user.psr_id,
			objId : adm.lls_id,
			envelopes : [{
				harga : {
					data : detailHarga
				}
			}]
		}, function(err, data){
			if (err){
				console.log("save harga error", err);
			}
			
			if (err){ return; }
			
			$timeout(function(){
				$scope.dirty = false;
			}, 1);
		})
	}

	$scope.update = function(data){
		var scopeHarga = [];
		data.data = data.data || {};
		data.data.detail = data.data.detail || [];
		if (data.data.detail.length == 0){
			$scope.dirty = true;
			data.data.detail = detailHarga;
		}
		
		angular.forEach(data.data.detail, function(detail){
			scopeHarga.push(detail);
		});
		
		scopeHarga = scopeHarga.length > 0 ? scopeHarga : detailHarga;
    	initPricingDetails(scopeHarga);
    	LT17.pricingFirstLoad = false;
	}
	
	$scope.allFilled = function(){
		var data = getPricingDetail();
		for (var i = 0; i < data.length; i++){
			var detail = data[i];
			if ((detail.volume1>0 || detail.volume2>0) && detail.price){
				filled = true;
			} else {
				filled = false;
				break;
			}
		}
		return filled;
	}
}

function initPricingDetails(detailHarga){
	if (detailHarga.length == 0) {
		detailHarga.push({
			name : null,
			unit1 : null,
			volume1: 1,
			unit2: null,
			volume2: 1,
			price: 0,
			price_total: 0,
			remark: null
		});
	}
	
	$("#pricing-detail").handsontable({
		autoWrapRow: true,
		colHeaders:[
			"Jenis Barang",
			"Satuan",
			"Volume",
			"Satuan 2",
			"Volume 2",
			"Harga Satuan",
			"Total harga Satuan",
			"Keterangan"],
		rowHeaders: true,
		data : detailHarga,
		dataSchema : {
			name : null,
			unit1 : null,
			volume1: 1,
			unit2: null,
			volume2: 1,
			price: 0,
			price_total: 0,
			remark:null
	    },
	    contextMenu: ["row_above", "row_below", "remove_row", "undo", "redo"],
	    columns: [
	        {data:"name"},
			{data:"unit1"},
			{data:"volume1", type:'numeric'},
			{data:"unit2"},
			{data:"volume2", type:'numeric'},
			{data:"price", type:'numeric', format:'Rp 0,0.00'},
			{data:"price_total",type:'numeric', format:'Rp 0,0.00'},
			{data:"remark"}
	    ],
	    cells : function(row, col, prop){
	    	var cellProperties = {};
            if (col == 6 && detailHarga[row]) {
                cellProperties.readOnly = true;
                var total = 0;
                if(detailHarga[row].price > 0)
                    total = detailHarga[row].price;
                if(detailHarga[row].volume1 > 0)
                    total = total * detailHarga[row].volume1;
                if(detailHarga[row].volume2 > 0)
                    total = total * detailHarga[row].volume2;
                if(total > 0)
                    detailHarga[row].price_total = total.toFixed(2);
                //detailHarga[row].price_total = detailHarga[row].volume1 * detailHarga[row].volume2 * detailHarga[row].price;
            }
            return cellProperties;
	    },
	    afterChange : function(change, source){
	    	updatePricingSummary(detailHarga);
	    }
	});
}

function updatePricingSummary(detailHarga){
	var hargaScope = angular.element('#harga').scope();
	var subtotal = 0;
	var pajak = hargaScope.pajak;
	var pajaktotal = 0;
	var total = 0;

    hargaScope.$safeApply(function(){
    	hargaScope.subtotal = subtotal;
    	hargaScope.pajak = pajak;
    	hargaScope.pajaktotal = pajaktotal;
    	hargaScope.total = total;
    });
    var total_harga = 0;
    for (var i = 0; i < detailHarga.length; i++) {
    	total_harga = detailHarga[i].volume1 * detailHarga[i].price;
        if(detailHarga[i].volume2)
            total_harga = total_harga * detailHarga[i].volume2;
        subtotal += parseFloat(total_harga);
    }

    if (subtotal > 0) {
    	pajaktotal = (pajak * subtotal) / 100;
    	total = parseFloat(subtotal + pajaktotal);
    	hargaScope.$safeApply(function(){
	        hargaScope.subtotal = subtotal;
	        hargaScope.pajak = pajak;
	        hargaScope.pajaktotal = pajaktotal;
	        hargaScope.total = total;
	        
	        if (!LT17.pricingFirstLoad){
	        	hargaScope.dirty = true;
	        }
    	});
    }
}

function KirimCtrl($scope, $timeout){
	
	var admSimpan, teknisSimpan, hargaSimpan;
	
	$scope.update = function(){
		LT17.check({
			type : 'save',
			owner : user.psr_id,
			objId : adm.lls_id,
			envelopes : ['adm', 'harga', 'teknis']
		}, function(err, data){
			if (err){
				console.log("check saved error", err);
				return;
			}
			
			var infoAdm = data.result['adm'];
			if (infoAdm.exists){
				admSimpan = LT17.moment(infoAdm.lastModified).fromNow();
			}
			
			var infoTeknis = data.result['teknis'];
			if (infoTeknis.exists){
				teknisSimpan = LT17.moment(infoTeknis.lastModified).fromNow();
			}
			
			var infoHarga = data.result['harga'];
			if (infoHarga.exists){
				hargaSimpan = LT17.moment(infoHarga.lastModified).fromNow();
			}
		})
		
		var admScope = angular.element('#adm').scope();
		var admDirty = admScope.dirty;
		var admFilled = admScope.allFilled();
		if(admDirty){
			$scope.admSimpan = 'Belum disimpan';
		} else {
			$scope.admSimpan = 'OK, Terakhir disimpan ' + admSimpan;
		}
		if(!admFilled){
			$scope.admSimpan = 'Masa berlaku penawaran belum diisi';
		}
		
		var teknisScope = angular.element('#teknis').scope();
		var teknisDirty = teknisScope.dirty;
		var teknisFilled = teknisScope.allFilled();
		if(teknisDirty){
			$scope.teknisSimpan = 'Belum disimpan';
		} else {
			$scope.teknisSimpan = 'OK, Terakhir disimpan ' + teknisSimpan;
		}
		if(!teknisFilled){
			$scope.teknisSimpan = 'Persyaratan belum dilengkapi';
		}
		
		var hargaScope = angular.element('#harga').scope();
		var hargaDirty= hargaScope.dirty;
		var hargaFilled = hargaScope.allFilled();
		if(hargaDirty){
			$scope.hargaSimpan = 'Belum disimpan';
		} else {
			$scope.hargaSimpan = 'OK, Terakhir disimpan ' + hargaSimpan;
		}
		if(!hargaFilled){
			$scope.hargaSimpan = 'Persyaratan belum dilengkapi';
		}
		
		if(!admDirty && admFilled && !teknisDirty && teknisFilled && !hargaDirty && hargaFilled){
			$scope.ready = true;
		} else {
			$scope.ready = false;
		}
		
	}
	
	$scope.encrypt = function(){
		$scope.info = 'Proses enkripsi penawaran sedang berlangsung';
		progresses = [];
		$scope.encrypted = false; 
		$scope.ready = false;
		
		if(pubKey[0]){
			LT17.pack({
				owner : user.psr_id,
				objId : adm.lls_id,
				key : pubKey[0],
				envelopes : ['teknis', 'harga']
			}, function(err, data){
				if (err){
					console.log("pack teknis harga error", err);
				}
			});
		} else {
			LT17.pack({
				owner : user.psr_id,
				objId : adm.lls_id,
				key : pubKey[1],
				envelopes : ['teknis']
			}, function(err, data){
				if (err){
					console.log("pack teknis error", err);
				}
				
				LT17.pack({
					owner : user.psr_id,
					objId : adm.lls_id,
					key : pubKey[2],
					envelopes : ['harga']
				}, function(err, data){
					if (err){
						console.log("pack harga error", err);
					}
				});
			});
		}
		
		LT17.check({
			type : 'pack',
			owner : user.psr_id,
			objId : adm.lls_id,
			envelopes : ['harga', 'teknis']
		}, function(err, data){
			if (err){
				console.log("check encrypted error", err);
			}
			
			$timeout(function(){
				var infoTeknis = data.result['teknis'];
				var infoHarga = data.result['harga'];
				if (infoTeknis.exists && infoTeknis.isRHS && infoHarga.exists && infoHarga.isRHS){
					$scope.encrypted = true;
					$scope.ready = true;
					$scope.info = 'Proses enkripsi file penawaran berhasil';
					return;
				}
				$scope.info = 'Proses enkripsi tidak berhasil, periksa kembali dokumen penawaran Anda';
			}, 10);
			
		});
	}
	
	$scope.upload = function(){
		$scope.info = 'Proses pengiriman file penawaran sedang berlangsung';
		$scope.sending = true;
		var admScope = angular.element('#adm').scope();
		var berlaku = admScope.adm.berlaku;

		LT17.register('uploadProgress', function(data){
			updateProgresses(data);
		});
		
		LT17.register('uploadFinished', function(data){
			var finishedCtx = data;
			finishedCtx.done = true;
			updateProgresses(finishedCtx);
		});
		
		LT17.register('uploadError', function(data){
			updateProgresses(data);
		});
		
		LT17.upload({
			owner : user.psr_id,
			objId : adm.lls_id,
			accessToken : token,
			url : uploadUrl,
			packages : [
			            { envelope : 'teknis', fields : {berlaku : berlaku} },
			            { envelope : 'harga', fields : {berlaku : berlaku} }
			]
		}, function(err, data){
			if (err){
				console.log("upload error", err);
			}
			
			LT17.unregister(['uploadProgress', 'uploadFinished', 'uploadError']);
			
			$timeout(function(){
				if (err){
					$scope.info = 'Proses pengiriman file penawaran tidak berhasil, silakan coba beberapa saat lagi';
				} else {
					$scope.info = 'Proses pengiriman file penawaran berhasil';
				}
			}, 1);
			
			$timeout(function(){
				$scope.sending = false;
			}, 5000);
		});
	}
	
	$scope.progresses = [];
	
	function updateProgresses(data){
		$timeout(function(){
			var exists = false;
			for (var i = 0; i < $scope.progresses.length; i++){
				if (data.name == $scope.progresses[i].name){
					exists = true;
					$scope.progresses[i] = data;
					if (data.success != undefined){
						$scope.progresses[i].error = !data.success;
					}
					break;
				}
			}
		
			if (!exists){
				$scope.progresses.push(data);
			}
		}, 0);
	}
}