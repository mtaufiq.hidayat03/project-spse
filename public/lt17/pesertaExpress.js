$(function(){
	$('#user').popover({
		content : user.email,
	    title : user.name,
	    trigger : 'hover'
	});

	LT17.infoFirstLoad = true;
	LT17.pricingFirstLoad = true;
	  
	LT17.prepare({
		owner : user.psr_id,
		objId : lls_id
	}, function (err, data){
		if (err){
			console.log("prepare session error", err);
		}
		init(data);
	})
	
	LT17.check({
		type : 'pack',
		owner : user.psr_id,
		objId : lls_id,
		envelopes : ['harga']
	}, function(err, data){
		if (err){
			console.log("check last encrypted error", err);
		}
		
		var infoHarga = data.result['harga'];
		
		if (infoHarga.exists && infoHarga.isRHS){
			var hargaEncTime = LT17.moment(infoHarga.lastModified).fromNow();
			var kirimScope = angular.element('#kirim').scope();
			kirimScope.info = 'Terakhir dilakukan enkripsi ' + hargaEncTime;
			kirimScope.encrypted = true;
		}
		
	})
});

function init(data){
	$('#tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		
		var href = $(this).attr('href');
		var key = href.substr(1).split('-')[0];
		if(key!='kirim'){
			if((key=='surat' && LT17.infoFirstLoad) || (key=='harga' && LT17.pricingFirstLoad)){
				LT17.load({
					objId : data.objId,
					owner : data.owner,
					envelopes : [key]
				}, function(err, data){
					if (err){
						console.log("load session error", err);
					}
					updateTab(data);
				});
			}
		} else {
			var kirimScope = angular.element('#kirim').scope();
			kirimScope.$safeApply(function(){
				kirimScope.update();
			});
		}
	});
	

//	first load, click surat tab
	tabSuratClick();
	  
}

function tabSuratClick(){
//	$('.nav-tabs a[href=#surat]').tab('show') ;
	var tab = $('#tabs a')[0];
	$(tab).click();
}

function tabHargaClick(){
	var tab = $('.nav-tabs a[href=#harga]');
	$(tab).click();
}

function updateTab(data){
	var envelopes = data.envelopes;
	var envKeys = Object.keys(envelopes);
	angular.forEach(envKeys, function(key){
		key = key.split('-')[0];
		var envelopeScope = angular.element('#' + key).scope();
		envelopeScope.$safeApply(function(){
			envelopeScope.update(envelopes[key]);
		});
	});
}

var lt17Peserta = angular.module('lt17Peserta', ['Scope.safeApply']);

lt17Peserta.filter('envelopeName', function(){
  return function(name){

    var dict = {
      harga : 'Penawaran Harga'
    }

    return 'Mengirim ' +  dict[name] || name;
  }
});

lt17Peserta.filter('percentName', function(){
  return function(percent){
    if (percent){
      return percent.toFixed(2) + ' %';  
    } else {
      return '';
    }
  }
});

	
function UserCtrl($scope){
	$scope.user = user;
	$scope.logout = function(){
		Apendo.userLogout(user);
	}
}

function SuratCtrl($scope, $timeout){
	$scope.surat = surat;
	
	$scope.save = function (){
		
		var suratValue = document.getElementById("surat_penawaran").innerHTML;
		var link_harga1 = document.getElementById('link_harga').outerHTML;
		var text_berlaku = document.getElementById('text_berlaku').outerHTML;
		suratValue = suratValue.replace(link_harga, '<strong>Penawaran Harga</strong>');
		suratValue = suratValue.replace(text_berlaku, '<strong>'+$scope.surat.berlaku+'</strong>');
		
		$scope.surat.suratContent = suratValue;
		
		LT17.save({
	      owner : user.psr_id,
	      objId : lls_id,
	      envelopes : [{
         	 surat : {
         		 data : {
         			 berlaku : $scope.surat.berlaku,
         			 suratContent : $scope.surat.suratContent
         		 }
         	 }
          }]
	    }, function(err, data){
	    	if (err){
				console.log("save surat error", err);
			}
			
			$timeout(function(){
				$scope.dirty = false;
			}, 1);
	    });
		
	}
	
	$scope.update = function(data){
		if(data.data != undefined){
			if (data.data.berlaku){
				$scope.surat.berlaku = data.data.berlaku;
				$scope.surat.suratContent = data.data.suratContent;
				$scope.dirty = false;
			} else {
				$scope.dirty = true;
			}
		}
	}
	
	$scope.$watch('surat.berlaku', function(){
		if (LT17.infoFirstLoad){
			LT17.infoFirstLoad = false;
		} else {
			$scope.dirty = true;
		}
	});
	
	$scope.allFilled = function(){
		return $scope.surat.berlaku > 0;
	}
	
}

function HargaCtrl($scope, $timeout){
	
	$scope.pajak = pajak;
	
	function getPricingDetail(){
		return $('#pricing-detail').data('handsontable').getData();
	}
	
	function getPricingSummary(){
		return {
			subtotal : $scope.subtotal,
			total : $scope.total,
			pajak : $scope.pajak,
			pajaktotal : $scope.pajaktotal
		}
	}
	
	$scope.save = function(){
		var detailHarga = {
			detail : getPricingDetail(),
			summary : getPricingSummary()
		};
		
		LT17.save({
			owner : user.psr_id,
			objId : lls_id,
			envelopes : [{
				harga : {
					data : detailHarga
				}
			}]
		}, function(err, data){
			if (err){
				console.log("save harga error", err);
			}
			
			if (err){ return; }
			
			$timeout(function(){
				$scope.dirty = false;
			}, 1);
		})
	}

	$scope.update = function(data){
		if(data.data!=undefined){
			$scope.pajak = data.data.summary.pajak;
		}
		
		var scopeHarga = [];
		data.data = data.data || {};
		data.data.detail = data.data.detail || [];
		if (data.data.detail.length == 0){
			$scope.dirty = true;
			data.data.detail = detailHarga;
		}
		
		angular.forEach(data.data.detail, function(detail){
			scopeHarga.push(detail);
		});
		
		scopeHarga = scopeHarga.length > 0 ? scopeHarga : detailHarga;
    	initPricingDetails(scopeHarga);
    	LT17.pricingFirstLoad = false;
	}
	
	$scope.$watch('pajak', function(){
		if(LT17.pricingFirstLoad!=undefined && !LT17.pricingFirstLoad){
			var data = getPricingDetail();
			updatePricingSummary(data);
		}
	});
	
	$scope.allFilled = function(){
		var data = getPricingDetail();
		for (var i = 0; i < data.length; i++){
			var detail = data[i];
            if ((detail.volume1>0 || detail.volume2>0) && detail.price){ // karena ada kemungkinan vol 2 tidak diisi
				filled = true;
			} else {
				filled = false;
				break;
			}
		}
		return filled;
	}
}

function initPricingDetails(detailHarga){
	if (detailHarga.length == 0) {
		detailHarga.push({
			name : null,
			unit1 : null,
			volume1: 1,
			unit2: null,
			volume2: 1,
			price: 0,
			price_total: 0,
			remark: null
		});
	}
	
	$("#pricing-detail").handsontable({
		autoWrapRow: true,
		colWidths: [250, 60, 60, 65, 65, 120, 120, 250],
		colHeaders:[
			"Jenis Barang",
			"Satuan",
			"Volume",
			"Satuan 2",
			"Volume 2",
			"Harga Satuan",
			"Total harga Satuan",
			"Keterangan"],
		rowHeaders: true,
		data : detailHarga,
		dataSchema : {
			name : null,
			unit1 : null,
			volume1: 1,
			unit2: null,
			volume2: 1,
			price: 0,
			price_total: 0,
			remark:null
	    },
	    contextMenu: {
	    	items: {
	    		"row_above": {
	    	        disabled: function () {
	    	            return rincianFixed;
	    	          }
	    	    	},
	    	    "row_below": {
	    	        disabled: function () {
	    	            return rincianFixed;
	    	          }
	    	    	},
	    	    "hsep1": "---------",
	    	    "remove_row": {
	    	        disabled: function () {
	    	            return rincianFixed;
	    	          }
	    	    	},
	    	    "hsep2": "---------",
	    	    "undo":{},
	    	    "redo":{}
	    	}
	    },
	    columns: [
	        {data:"name"},
			{data:"unit1"},
			{data:"volume1", type:'numeric'},
			{data:"unit2"},
			{data:"volume2", type:'numeric'},
			{data:"price", type:'numeric', format:'Rp 0,0.00'},
			{data:"price_total",type:'numeric', format:'Rp 0,0.00'},
			{data:"remark"}
	    ],
	    cells : function(row, col, prop){
	    	var cellProperties = {};
            if (col == 6 && detailHarga[row]) {
                cellProperties.readOnly = true;
                var total = 0;
                if(detailHarga[row].price > 0)
                    total = detailHarga[row].price;
                if(detailHarga[row].volume1 > 0)
                    total = total * detailHarga[row].volume1;
                if(detailHarga[row].volume2 > 0)
                    total = total * detailHarga[row].volume2;
                if(total > 0)
                    detailHarga[row].price_total = total.toFixed(2);
                //detailHarga[row].price_total = detailHarga[row].volume1 * detailHarga[row].volume2 * detailHarga[row].price;
            }
	    	return cellProperties;
	    },
	    afterChange : function(change, source){
	    	updatePricingSummary(detailHarga);
	    }
	});
}

function updatePricingSummary(detailHarga){
	var hargaScope = angular.element('#harga').scope();
	var subtotal = 0;
	var pajak = hargaScope.pajak;
	var pajaktotal = 0;
	var total = 0;

    hargaScope.$safeApply(function(){
    	hargaScope.subtotal = subtotal;
    	hargaScope.pajak = pajak;
    	hargaScope.pajaktotal = pajaktotal;
    	hargaScope.total = total;
    });
    var total_harga = 0;
    for (var i = 0; i < detailHarga.length; i++) {
        total_harga = detailHarga[i].volume1 * detailHarga[i].price;
        if(detailHarga[i].volume2)
            total_harga = total_harga * detailHarga[i].volume2;
        subtotal += parseFloat(total_harga);
    }

    if (subtotal > 0) {
    	pajaktotal = (pajak * subtotal) / 100;
    	total = parseFloat(subtotal + pajaktotal);
    	hargaScope.$safeApply(function(){
	        hargaScope.subtotal = subtotal;
	        hargaScope.pajak = pajak;
	        hargaScope.pajaktotal = pajaktotal;
	        hargaScope.total = total;
	        
	        if (!LT17.pricingFirstLoad){
	        	hargaScope.dirty = true;
	        }
    	});
    }
}

function KirimCtrl($scope, $timeout){
	
	var suratSimpan, hargaSimpan;
	
	$scope.update = function(){
		LT17.check({
			type : 'save',
			owner : user.psr_id,
			objId : lls_id,
			envelopes : ['surat', 'harga']
		}, function(err, data){
			if (err){
				console.log("check saved error", err);
				return;
			}
			
			var infoSurat = data.result['surat'];
			if (infoSurat.exists){
				suratSimpan = LT17.moment(infoSurat.lastModified).fromNow();
			}
			
			var infoHarga = data.result['harga'];
			if (infoHarga.exists){
				hargaSimpan = LT17.moment(infoHarga.lastModified).fromNow();
			}
		})
		
		var suratScope = angular.element('#surat').scope();
		var suratDirty = suratScope.dirty;
		var suratFilled = suratScope.allFilled();
		if(suratDirty){
			$scope.suratSimpan = 'Belum disimpan';
		} else {
			$scope.suratSimpan = 'OK, Terakhir disimpan ' + suratSimpan;
		}
		if(!suratFilled){
			$scope.suratSimpan = 'Masa berlaku penawaran belum diisi';
		}
		
		var hargaScope = angular.element('#harga').scope();
		var hargaDirty= hargaScope.dirty;
		var hargaFilled = hargaScope.allFilled();
		if(hargaDirty){
			$scope.hargaSimpan = 'Belum disimpan';
		} else {
			$scope.hargaSimpan = 'OK, Terakhir disimpan ' + hargaSimpan;
		}
		if(!hargaFilled){
			$scope.hargaSimpan = 'Persyaratan belum dilengkapi';
		}
		console.log('suratDirty : '+suratDirty);
		console.log('suratFilled : '+suratFilled);
		console.log('hargaDirty : '+hargaDirty);
		console.log('hargaFilled : '+hargaFilled);
		console.log(!suratDirty && suratFilled && !hargaDirty && hargaFilled);
		if(!suratDirty && suratFilled && !hargaDirty && hargaFilled){
			$scope.ready = true;
		} else {
			$scope.ready = false;
		}
		
	}
	
	$scope.encrypt = function(){
		$scope.info = 'Proses enkripsi penawaran sedang berlangsung';
		progresses = [];
		$scope.encrypted = false; 
		$scope.ready = false;
		
		if(pubKey){
			LT17.pack({
				owner : user.psr_id,
				objId : lls_id,
				key : pubKey,
				envelopes : ['harga']
			}, function(err, data){
				if (err){
					console.log("pack harga error", err);
				}
			});
		}
		
		$timeout(function(){
			LT17.check({
				type : 'pack',
				owner : user.psr_id,
				objId : lls_id,
				envelopes : ['harga']
			}, function(err, data){
				if (err){
					console.log("check encrypted error", err);
				}
				
				var infoHarga = data.result['harga'];
				if (infoHarga.exists && infoHarga.isRHS){
					$scope.encrypted = true;
					$scope.ready = true;
					$scope.info = 'Proses enkripsi file penawaran berhasil';
					return;
				}
				$scope.info = 'Proses enkripsi tidak berhasil, periksa kembali penawaran Anda';
				
			});
		}, 500);
	}
	
	$scope.upload = function(){
		$scope.info = 'Proses pengiriman file penawaran sedang berlangsung';
		$scope.sending = true;
		var suratScope = angular.element('#surat').scope();
		var berlaku = suratScope.surat.berlaku;
		var suratContent = suratScope.surat.suratContent;

		LT17.register('uploadProgress', function(data){
			updateProgresses(data);
		});
		
		LT17.register('uploadFinished', function(data){
			var finishedCtx = data;
			finishedCtx.done = true;
			updateProgresses(finishedCtx);
		});
		
		LT17.register('uploadError', function(data){
			updateProgresses(data);
		});
		
		LT17.upload({
			owner : user.psr_id,
			objId : lls_id,
			accessToken : token,
			url : uploadUrl,
			packages : [
			            { envelope : 'harga', fields : {berlaku : berlaku, surat : suratContent} }
			]
		}, function(err, data){
			if (err){
				console.log("upload error", err);
			}
			
			LT17.unregister(['uploadProgress', 'uploadFinished', 'uploadError']);
			
			$timeout(function(){
				if (err){
					$scope.info = 'Proses pengiriman dokumen penawaran tidak berhasil, silakan coba beberapa saat lagi';
				} else {
					$scope.info = 'Proses pengiriman dokumen penawaran berhasil';
				}
			}, 1);
			
			$timeout(function(){
				$scope.sending = false;
			}, 5000);
		});
	}
	
	$scope.progresses = [];
	
	function updateProgresses(data){
		$timeout(function(){
			var exists = false;
			for (var i = 0; i < $scope.progresses.length; i++){
				if (data.name == $scope.progresses[i].name){
					exists = true;
					$scope.progresses[i] = data;
					if (data.success != undefined){
						$scope.progresses[i].error = !data.success;
					}
					break;
				}
			}
		
			if (!exists){
				$scope.progresses.push(data);
			}
		}, 0);
	}
}