$(function(){
	$('#user').popover({
		content : user.email,
	    title : user.name,
	    trigger : 'hover'
	});
	
	LT17.prepare({
		owner : user.peg_id,
		objId : adm.lls_id
	}, function (err, data){
		if (err){
			console.log("prepare session error", err);
		}
		init(data);
	})
});

function init(data){
	$('#tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		
		var href = $(this).attr('href');
		var key = href.substr(1).split('-')[0];

		if (key=='pesertaList'){
			LT17.load({
				objId : data.objId,
				owner : data.owner,
				envelopes : [key]
			}, function(err, data){
				if (err){
					console.log("load session error", err);
				}
				updateTab(data);
			});
		}
	});
	
	var tab = $('#tabs a')[0];
	$(tab).click();
	  
}

function updateTab(data){
	var envelopes = data.envelopes;
	var envKeys = Object.keys(envelopes);
	angular.forEach(envKeys, function(key){
		key = key.split('-')[0];
		var envelopeScope = angular.element('#' + key).scope();
		envelopeScope.$safeApply(function(){
			envelopeScope.update(envelopes[key]);
		});
	});
}

var lt17Panitia = angular.module('lt17Panitia', ['Scope.safeApply']);

lt17Panitia.filter('progresFilter', function(){
  return function(progres){
    if (progres){
      return progres.toFixed(2) + ' %';  
    } else {
      return '';
    }
  }
});

function UserCtrl($scope){
	$scope.user = user;
	$scope.logout = function(){
		Apendo.userLogout(user);
	}
}

function AdmCtrl($scope){
	$scope.adm = adm;
}

function PesertaCtrl($scope, $timeout, $sce){

	$scope.showSurat = function(suratContent){
		$scope.suratContent = $sce.trustAsHtml(suratContent);;
		$('#surat_penawaran_modal').modal('show');
	}
	
	$scope.update = function(data){
		try {
			$scope.pesertaList = JSON.parse(data.data);
		}
		catch(e){
		}
	}
	
	$scope.getHarga = function(){
		var packages = [];
		var owners = [];
		var persenTotal = 0;
		
		angular.forEach(penawaranList, function(penawaran){
			packages.push({url : downloadUrl + '?id=' + penawaran.id, owner : penawaran.psrId});
			owners.push(penawaran.psrId);
		});
		
		LT17.register('downloadStarted', function(data){
			$timeout(function(){
				$scope.downloading = true;
				$scope.persenTotal = persenTotal;
			}, 0);
			
			LT17.unregister(['downloadStarted']);
		});
		
		LT17.register('downloadProgress', function(data){
			$timeout(function(){
				persenTotal = $scope.persenTotal + (data.percent/owners.length);
				$scope.persenTotal = persenTotal;
			}, 0);
		});
		
		LT17.download({
			owner : user.peg_id,
			objId : adm.lls_id,
			envelope : 'harga',
			accessToken : token,
			packages : packages
		}, function(err, data){
			if (err){
				console.log("download harga error", err);
			} else {
				LT17.unpack({
					owner : user.peg_id,
					objId : adm.lls_id,
					envelope : 'harga',
					key : privKey,
					packageOwners : owners
				}, function(err, data){
					if (err){
						console.log("unpack harga error", err);
					} else {
						LT17.sort({
							owner : user.peg_id,
							objId : adm.lls_id,
							envelope : 'harga',
							accessToken : token,
							url : sortUrl
						}, function(err, data){
							if (err){
								console.log("sort error", err);
							} else {
								LT17.save({
									owner : user.peg_id,
									objId : adm.lls_id,
									envelopes : [{
										pesertaList : {data : data.rawData}
									}]
								}, function(err, data){
									if (err){
										console.log("save sorted error", err);
									} 
								});						
							}
							$timeout(function(){
								if(!err){
									try{
										$scope.pesertaList = JSON.parse(data.rawData);
										$scope.downloading = false;
									}
									catch(e){
										console.log(e);
									}
								}
							}, 1);
						});
					}
				});
			}
			LT17.unregister(['downloadProgress']);
		});
	}
}