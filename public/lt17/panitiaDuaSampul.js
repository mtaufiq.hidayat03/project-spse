$(function(){
	$('#user').popover({
		content : user.email,
	    title : user.name,
	    trigger : 'hover'
	});
	
	LT17.prepare({
		owner : user.peg_id,
		objId : adm.lls_id
	}, function (err, data){
		if (err){
			console.log("prepare session error", err);
		}
		init(data);
	})
});

function init(data){
	$('#tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		
		var href = $(this).attr('href');
		var key = href.substr(1).split('-')[0];

		if (key!='adm'){
			LT17.load({
				objId : data.objId,
				owner : data.owner,
				envelopes : [key]
			}, function(err, data){
				if (err){
					console.log("load session error", err);
				}
				updateTab(data);
			});
		}
	});
	
	var tab = $('#tabs a')[0];
	$(tab).click();
	  
}

function updateTab(data){
	var envelopes = data.envelopes;
	var envKeys = Object.keys(envelopes);
	angular.forEach(envKeys, function(key){
		key = key.split('-')[0];
		var envelopeScope = angular.element('#' + key).scope();
		envelopeScope.$safeApply(function(){
			envelopeScope.update(envelopes[key]);
		});
	});
}

var lt17Panitia = angular.module('lt17Panitia', ['Scope.safeApply']);

lt17Panitia.filter('progresFilter', function(){
  return function(progres){
    if (progres){
      return progres.toFixed(2) + ' %';  
    } else {
      return '';
    }
  }
});

function UserCtrl($scope){
	$scope.user = user;
	$scope.logout = function(){
		Apendo.userLogout(user);
	}
}

function AdmCtrl($scope){
	$scope.adm = adm;
}

function PesertaTeknisCtrl($scope, $timeout){
	$scope.pesertaTeknisList = penawaranTeknisList;
	
	$scope.update = function(data){
		try {
			$scope.pesertaTeknisList = JSON.parse(data.data);
		}
		catch(e){
		}
	}
	
	$scope.getTeknis = function(peserta, index){
		if (peserta.downloaded){
			return LT17.open(peserta.downloaded);
		}
		
		LT17.register('downloadStarted', function(data){
			$timeout(function(){
				$scope.pesertaTeknisList[index].downloading = true;
				$scope.downloading = true;
				LT17.unregister(['downloadStarted']);
			}, 0);
		});
		
		$scope.pesertaTeknisList[index].progres = 0;
		LT17.register('downloadProgress', function(data){
			$timeout(function(){
				$scope.pesertaTeknisList[index].progres=data.percent;
			}, 0);
		});
		
		LT17.register('downloadFinished', function(data){
			$timeout(function(){
				$scope.pesertaTeknisList[index].downloading = false;
				LT17.unregister(['downloadProgress']);
				LT17.unregister(['downloadFinished']);
			}, 0);
		});
		
		LT17.register('unpackError', function(data){
			$timeout(function(){
				$scope.downloading = false;
				$scope.pesertaTeknisList[index].decrypting = false;
				LT17.unregister(['unpackError']);
			}, 0);
		});
		
		var package = {url : downloadUrl + '?id=' + peserta.id, owner : peserta.psrId};
		
		LT17.download({
			owner : user.peg_id,
			objId : adm.lls_id,
			envelope : 'teknis',
			accessToken : token,
			packages : [package],
			index : index
		}, function(err, data){
			if (err){
				console.log("download teknis error", err);
			}

			var data = data.data;
			var row = data[0];
			var index = row.index;
			
			$scope.pesertaTeknisList[index].decrypting = true;
			LT17.unpack({
				owner : user.peg_id,
				objId : adm.lls_id,
				envelope : 'teknis',
				key : privKey[0],
				packageOwners : [package.owner],
				index : index
			}, function(err, data){
				if (err){
					console.log("unpack teknis error", err);
				}
				$timeout(function(){
					if (data.index == undefined || data.index == null) {
						return;
					}
					
					$scope.pesertaTeknisList[index].decrypting = false;
					
					$scope.pesertaTeknisList = $scope.pesertaTeknisList || [];
					if ($scope.pesertaTeknisList.length > 0 && data.index < $scope.pesertaTeknisList.length){
						if (data.success) {
							$scope.pesertaTeknisList[data.index].downloaded = data.resultDirPath;
							LT17.save({
								owner : user.peg_id,
								objId : adm.lls_id,
								envelopes : [{
									pesertaTeknisList : {data : JSON.stringify($scope.pesertaTeknisList)}
								}]
							}, function(err, data){
								if (err){
									console.log("update downloaded error", err);
								}
							});
						}
					}
					
					$scope.downloading = false;
					
				}, 0);
			});
		});
	}
}

function PesertaHargaCtrl($scope, $timeout){
	
	$scope.update = function(data){
		try {
			$scope.pesertaHargaList = JSON.parse(data.data);
		}
		catch(e){
		}
	}

	$scope.getHarga = function(){
		var packages = [];
		var owners = [];
		var persenTotal = 0;
		
		angular.forEach(penawaranHargaList, function(penawaran){
			packages.push({url : downloadUrl + '?id=' + penawaran.id, owner : penawaran.psrId});
			owners.push(penawaran.psrId);
			
		});
		
		LT17.register('downloadStarted', function(data){
			$timeout(function(){
				$scope.downloading = true;
				$scope.persenTotal = persenTotal;
			}, 0);
			
			LT17.unregister(['downloadStarted']);
		});
		
		LT17.register('downloadProgress', function(data){
			$timeout(function(){
				persenTotal = $scope.persenTotal + (data.percent/owners.length);
				$scope.persenTotal = persenTotal;
			}, 0);
		});
		
		LT17.download({
			owner : user.peg_id,
			objId : adm.lls_id,
			envelope : 'harga',
			accessToken : token,
			packages : packages
		}, function(err, data){
			if (err){
				console.log("download harga error", err);
			}
			
			LT17.unpack({
				owner : user.peg_id,
				objId : adm.lls_id,
				envelope : 'harga',
				key : privKey[1],
				packageOwners : owners
			}, function(err, data){
				if (err){
					console.log("unpack harga error", err);
				}
				
				LT17.sort({
					owner : user.peg_id,
					objId : adm.lls_id,
					envelope : 'harga',
					accessToken : token,
					url : sortUrl
				}, function(err, data){
					if (err){
						console.log("sort error", err);
					} else {
						LT17.save({
							owner : user.peg_id,
							objId : adm.lls_id,
							envelopes : [{
								pesertaHargaList : {data : data.rawData}
							}]
						}, function(err, data){
							if (err){
								console.log("save sorted error", err);
							}
						});						
					}
					
					$timeout(function(){
						if(!err){
							try{
								$scope.pesertaHargaList = JSON.parse(data.rawData);
							}
							catch(e){
							}
						}

						$scope.downloading = false;
						LT17.unregister(['downloadProgress']);
						
					}, 1);
				});
			});
		});
	}
}
