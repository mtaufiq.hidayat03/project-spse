$(function(){
	$('#user').popover({
		content : user.email,
	    title : user.name,
	    trigger : 'hover'
	});
	
	LT17.prepare({
		owner : user.peg_id,
		objId : adm.lls_id
	}, function (err, data){
		if (err){
			console.log("prepare session error", err);
		}
		init(data);
	})
});

function init(data){
	$('#tabs a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
		
		var href = $(this).attr('href');
		var key = href.substr(1).split('-')[0];

		if (key=='pesertaList'){
			LT17.load({
				objId : data.objId,
				owner : data.owner,
				envelopes : [key]
			}, function(err, data){
				if (err){
					console.log("load session error", err);
				}
				updateTab(data);
			});
		}
	});
	
	var tab = $('#tabs a')[0];
	$(tab).click();
	  
}

function updateTab(data){
	var envelopes = data.envelopes;
	var envKeys = Object.keys(envelopes);
	angular.forEach(envKeys, function(key){
		key = key.split('-')[0];
		var envelopeScope = angular.element('#' + key).scope();
		envelopeScope.$safeApply(function(){
			envelopeScope.update(envelopes[key]);
		});
	});
}

var lt17Panitia = angular.module('lt17Panitia', ['Scope.safeApply']);

lt17Panitia.filter('progresFilter', function(){
  return function(progres){
    if (progres){
      return progres.toFixed(2) + ' %';  
    } else {
      return '';
    }
  }
});

function UserCtrl($scope){
	$scope.user = user;
	$scope.logout = function(){
		Apendo.userLogout(user);
	}
}

function AdmCtrl($scope){
	$scope.adm = adm;
}

function PesertaCtrl($scope, $timeout, $sce){

	$scope.showSurat = function(suratContent){
		$scope.suratContent = $sce.trustAsHtml(suratContent);;
		$('#surat_penawaran_modal').modal('show');
	}
	
	$scope.update = function(data){
		try {
			$scope.pesertaList = JSON.parse(data.data);
		}
		catch(e){
		}
	}
	
	$scope.getHarga = function(){
		var packages = [];
		var owners = [];
		var persenTotal = 0;
		
		angular.forEach(penawaranList, function(penawaran){
			packages.push({url : downloadUrl + '?id=' + penawaran.id, owner : penawaran.psrId});
			owners.push(penawaran.psrId);
			
		});
		
		LT17.register('downloadStarted', function(data){
			$timeout(function(){
				$scope.downloading = true;
				$scope.persenTotal = persenTotal;
			}, 0);
			
			LT17.unregister(['downloadStarted']);
		});
		
		LT17.register('downloadProgress', function(data){
			$timeout(function(){
				persenTotal = $scope.persenTotal + (data.percent/owners.length);
				$scope.persenTotal = persenTotal;
			}, 0);
		});
		
		LT17.download({
			owner : user.peg_id,
			objId : adm.lls_id,
			envelope : 'harga',
			accessToken : token,
			packages : packages
		}, function(err, data){
			if (err){
				console.log("download harga error", err);
			} else {
				LT17.unpack({
					owner : user.peg_id,
					objId : adm.lls_id,
					envelope : 'harga',
					key : privKey,
					packageOwners : owners
				}, function(err, data){
					if (err){
						console.log("unpack harga error", err);
					} else {
						LT17.sort({
							owner : user.peg_id,
							objId : adm.lls_id,
							envelope : 'harga',
							accessToken : token,
							url : sortUrl
						}, function(err, data){
							if (err){
								console.log("sort error", err);
							} else {
								LT17.save({
									owner : user.peg_id,
									objId : adm.lls_id,
									envelopes : [{
										pesertaList : {data : data.rawData}
									}]
								}, function(err, data){
									if (err){
										console.log("save sorted error", err);
									} 
								});						
							}
							$timeout(function(){
								if(!err){
									try{
										$scope.pesertaList = JSON.parse(data.rawData);
										$scope.downloading = false;
									}
									catch(e){
										console.log(e);
									}
								}
							}, 1);
						});
					}
				});
			}
			LT17.unregister(['downloadProgress']);
		});
	}
	
	$scope.getTeknis = function(peserta, index){
		if (peserta.downloaded){
			return LT17.open(peserta.downloaded);
		}
		
		LT17.register('downloadStarted', function(data){
			$timeout(function(){
				$scope.pesertaList[index].downloading = true;
				$scope.downloading = true;
				LT17.unregister(['downloadStarted']);
			}, 0);
		});
		
		$scope.pesertaList[index].progres = 0;
		LT17.register('downloadProgress', function(data){
			$timeout(function(){
				$scope.pesertaList[index].progres=data.percent;
			}, 0);
		});
		
		LT17.register('downloadFinished', function(data){
			$timeout(function(){
				$scope.pesertaList[index].downloading = false;
				LT17.unregister(['downloadProgress']);
				LT17.unregister(['downloadFinished']);
			}, 0);
		});
		
		LT17.register('unpackError', function(data){
			$timeout(function(){
				$scope.downloading = false;
				$scope.pesertaList[index].decrypting = false;
				LT17.unregister(['unpackError']);
			}, 0);
		});
		
		var package = {url : peserta.urlDownload, owner : peserta.psrId };
		
		LT17.download({
			owner : user.peg_id,
			objId : adm.lls_id,
			envelope : 'admteknis',
			accessToken : token,
			packages : [package],
			index : index
		}, function(err, data){
			if (err){
				console.log("download admteknis error", err);
			}

			var data = data.data;
			var row = data[0];
			var index = row.index;
			
			$scope.pesertaList[index].decrypting = true;
			LT17.unpack({
				owner : user.peg_id,
				objId : adm.lls_id,
				envelope : 'admteknis',
				key : privKey,
				packageOwners : [package.owner],
				index : index
			}, function(err, data){
				if (err){
					console.log("unpack admteknis error", err);
					$scope.pesertaList[index].decrypting = false;
					$scope.downloading = false;
				} else {
					$timeout(function(){
						if (data.index == undefined || data.index == null) {
							return;
						}
						
						$scope.pesertaList[index].decrypting = false;
						
						$scope.pesertaList = $scope.pesertaList || [];
						if ($scope.pesertaList.length > 0 && data.index < $scope.pesertaList.length){
							if (data.success) {
								$scope.pesertaList[data.index].downloaded = data.resultDirPath;
								LT17.save({
									owner : user.peg_id,
									objId : adm.lls_id,
									envelopes : [{
										pesertaList : {data : JSON.stringify($scope.pesertaList)}
									}]
								}, function(err, data){
									if (err){
										console.log("update downloaded error", err);
									}
								});
							}
						}
						
						$scope.downloading = false;
						
					}, 1000);
				}
			});
		});
		
	}
	
}