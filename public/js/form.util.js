/**Utility untuk tampilkan konfirmasi jika 
data yang telah didedit namun window.unload.

FORM yang akan menggunakan script ini harus menambahkan atribute 'data-alert=true' sbb:
<form data-alert="true">

Tempatkan pada custom script, paling bawah */

var formModified=false;
$("form[data-alert='true'] input, form[data-alert='true'] select, form[data-alert='true'] textarea").change(function(){
	formModified=true;
	$(this).addClass("modified");
});

$("form[data-alert='true']").submit(function(){
	formModified=false;
});

$(window).bind("beforeunload", function(){
	if(formModified)
			return "Data Anda belum tersimpan, Anda yakin akan mengabaikannya?";
})

function isNumberKey(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	if (key.length == 0) return;
	var regex = /^[0-9,.\b]+$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

function isNumberKey2(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	if (key.length == 0) return;
	var regex = /^[0-9,\b]+$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

function isNumberKeyWithoutComma(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    if (key.length == 0) return;
    var regex = /^[0-9.\b]+$/;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}

function isRoundNumberKey(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	if (key.length == 0) return;
	//var regex = /^\d+$/;
	var regex = /^[0-9\b]+$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

function isPositifNumberKey(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode(key);
	if (key.length == 0) return;
	var regex = /^[0-9,\b]+$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
	else
		if (key < 0){
			theEvent.returnValue = false;
			if (theEvent.preventDefault) theEvent.preventDefault();
		}
}

function formatCurrency(num) {
	num = num.toString().replace(/\$|\,/g, '');
	if (isNaN(num)) {
		num = "0";
	}

	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num * 100 + 0.50000000001);
	cents = num % 100;
	num = Math.floor(num / 100).toString();

	if (cents < 100) {
		cents = "0" + cents;
	}

	for (var i = 0; i < Math.floor((num.length - (1+i)) / 3); i++) {
		num = num.substring(0, num.length - (4*i+3)) + '.' + num.substring(num.length - (4*i+3));
	}

	return (((sign) ? '' : '-') + num + ',' + cents);
}

var contains = function(needle) {
	// Per spec, the way to identify NaN is that it is not equal to itself
	var findNaN = needle !== needle;
	var indexOf;

	if(!findNaN && typeof Array.prototype.indexOf === 'function') {
		indexOf = Array.prototype.indexOf;
	} else {
		indexOf = function(needle) {
			var i = -1, index = -1;

			for(i = 0; i < this.length; i++) {
				var item = this[i];

				if((findNaN && item !== item) || item === needle) {
					index = i;
					break;
				}
			}

			return index;
		};
	}

	return indexOf.call(this, needle) > -1;
};



function isDuplicate(arr){
	var res = cekDuplicate(arr);
	return (res.length > 0) ;
}
function cekDuplicate(arr){
	var sorted_arr = arr.sort();
	var results = [];
	for (var i = 0; i < arr.length - 1; i++) {
		if (sorted_arr[i + 1] == sorted_arr[i]) {
			results.push(sorted_arr[i]);
		}
	}
	return results;
}


function convertLongDateThenAddNowTime(str) {
	var date = new Date(str),
		mnth = ("0" + (date.getMonth() + 1)).slice(-2),
		day = ("0" + date.getDate()).slice(-2);
	var today = new Date();
	var time = today.getHours() + ":" + today.getMinutes();//+ ":" + today.getSeconds()
	return [day, mnth, date.getFullYear()].join("-")+" "+time;
}

function convertLongDate(str) {
	var date = new Date(str),
		mnth = ("0" + (date.getMonth() + 1)).slice(-2),
		day = ("0" + date.getDate()).slice(-2);

	var time = date.getHours() + ":" + date.getMinutes();//+ ":" + today.getSeconds()
	return [day, mnth, date.getFullYear()].join("-")+" "+time;
}