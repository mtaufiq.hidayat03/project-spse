$(document).ready(function() {
    $('a.jpopup').bind("click", function(){
        height = $(this).attr('height');
        width  = $(this).attr('width');
        newwindow=window.open($(this).attr('href'),'','height=600,width=900,status=yes,toolbar=no,resizable=yes,location=no,scrollbars=yes');
        if (window.focus) {newwindow.focus()}
        return false;
	 }); 	
	 $('.checkall').click(function () {	 	
	 	$('.checkbox').each(function() {
            $(this).prop('checked',!$(this).prop('checked'));
        });
	 });
	//$('.apbn-mask').inputmask({mask: '999.99.999999.99.99.99.9999.999.999.999999'});
	//$('.npwp-mask').inputmask({mask: '99.999.999.9-999.999'})
    $('.modalActivate').click(function(){
        $('.modalScreen').fadeIn('fast');
    });
    $('.modalScreen').click(function(){
        $(this).fadeOut('fast');
    });
   $('.carousel').carousel();
   
   //form login validation
   $('#formLogin').validate({
		rules : {
			txtUserId:"required",
			txtPassword:"required"
		},
		messages:{
			txtUserId : "User ID wajib diisi",
			txtPassword : "Password wajib diisi"
		}
	});
	$(document).on('click', '.remove', function(){
	     $(this).parent().parent().remove()
	})
	$("#tambahIjin").click(function(){
	   var idx = $('#tblIjinUsaha').find(".ijin").length
	   var addIjin = '<tr >'+
	      			'<td><input type="text" name="ijin['+idx+'].chk_nama" class="form-control ijin"/></td>'+
			      			'<td><input type="text" name="ijin['+idx+'].chk_klasifikasi" class="form-control"></td>'+
			      			'<td><a href="javascript:void(0)" class="remove"><i class="fa fa-trash-o fa-lg"></i></a></td>'+
		    		   '</tr>'
	  	 $("#tblIjinUsaha").append(addIjin)     	  
	  })
	 $("#tambahSyarat").click(function(){
	     var idx = $('#tblSyarat').find(".syaratLain").length
	     var addIjin = '<tr >'+
			      			'<td colspan="2"><textarea name="syaratLain['+idx+']" cols="80" rows="2" class="form-control syaratLain"></textarea></td>'+	
			      			'<td><a href="javascript:void(0)" class="remove"><i class="fa fa-trash-o fa-lg"></i></a></td>'+	      			
		    		   '</tr>'
	  	 $('#tblSyarat').append(addIjin)   	  
	})
});