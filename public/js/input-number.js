$.fn.onChangeNumberOnly = function () {

    var timeout = null;

    $(this).on('keyup', function(){
        var view = $(this);
        var data = formatting(view.val(), getDefaultValue(view));
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (data.sanitized < 10 || data.sanitized > 100) {
                $('.alert-block').css('color', 'red');
            } else {
                $('.alert-block').css('color', '#737373');
            }
        }, 200);
        $(this).val(data.sanitized);
    });

    firstTime($(this));

    function firstTime(view) {
        view.val(formatting(view.val(), getDefaultValue(view)).sanitized);
    }

    function getDefaultValue(view) {
        var defaultValue = view.data('default');
        return defaultValue === undefined || defaultValue === '' ? '0' : defaultValue;
    }

    function formatting(value, defaultValue) {
        value = value === undefined || value === '' ? defaultValue : value;
        var data = {};
        data.sanitized = value.toString().replace(/\D/gi, '');
        if (data.sanitized !== '' && !isNaN(data.sanitized)) {
            data.sanitized = parseInt(data.sanitized);
        }
        return data;
    }

};