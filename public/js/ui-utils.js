/* ---------------------------  SCRIPT utility untuk SELECT HTML memilih kabupaten  --------------------------- */
// ------------------------------------------------------------------------------------------------------------

// Script ini digunakan untuk membuat combo-box pemilihan kabupaten yang dinamis sesuai dengan propinsi yang dipilih
$(document).ready(function(){
    $("#propinsiSelect").change(function(){
        $("#kabupatenSelect").empty();
        $.post("/spse4/json/daftar_kabupaten/"+$("#propinsiSelect option:selected").val(), {propinsiSelect : $("#propinsiSelect option:selected").val()}, function(data) {
            // jika belum memilih kabupaten
            if ($("#propinsiSelect option:selected").val() == "") {
                $("#kabupatenSelect").empty(); // kosongkan dulu elemen select ini
                $("#kabupatenSelect").append(
                    '<option value="">-- Pilih propinsi dahulu --</option>'
                );
            }
            // cek jika data terpopulasi null atau kosong
            else if (data === null) {
                $("#kabupatenSelect").empty(); // kosongkan dulu elemen select ini
                $("#kabupatenSelect").append(
                    '<option value>-- Tidak ada data --</option>'
                );
            } else {
                $("#kabupatenSelect").empty(); // kosongkan dulu elemen select ini
                $.each(data, function(i, item) {
                    $("#kabupatenSelect").append(
                        '<option value="' + item.kbp_id + '">' + item.kbp_nama + '</option>'
                    );
                })
            }
            }, "json"
        );
    });
});

/* ---------------------------  SCRIPT utility untuk SELECT HTML memilih instansi  ---------------------------- */
// ------------------------------------------------------------------------------------------------------------

// Script ini digunakan untuk membuat combo-box pemilihan kabupaten yang dinamis sesuai dengan jenis instansi yang dipilih
$(document).ready(function(){
    $("#jenisInstansiSelect").change(function(){
        $.post("/spse4/json/daftar_instansi/"+$("#jenisInstansiSelect option:selected").val(), {jenisInstansiSelect : $("#jenisInstansiSelect option:selected").val()}, function(data) {
            // jika belum memilih jenis instansi
            if ($("#jenisInstansiSelect option:selected").val() == "") {
                $("#instansiSelect").empty(); // kosongkan dulu elemen select ini
                $("#instansiSelect").append(
                    '<option value>-- Pilih jenis instansi dahulu --</option>'
                );
            }
            // cek jika data terpopulasi null atau kosong
            else if (data === null) {
                $("#instansiSelect").empty(); // kosongkan dulu elemen select ini
                $("#instansiSelect").append(
                    '<option value>-- Tidak ada data --</option>'
                );
            } else {
                $("#instansiSelect").empty(); // kosongkan dulu elemen select ini
                $.each(data, function(i, item) {
                    $("#instansiSelect").append(
                        '<option value="' + item.id + '">' + item.nama + '</option>'
                    );
                })
            }
            }, "json"
        );
    });
});

/* ---------------------------  SCRIPT utility untuk RADIO memilih metode lelang  ----------------------------- */
// ------------------------------------------------------------------------------------------------------------

// Script ini digunakan untuk membuat combo-box pemilihan kabupaten yang dinamis sesuai dengan jenis instansi yang dipilih
$(document).ready(function(){
    $("#kategoriLelangSelect,#metodePemilihanSelect").change(function(){
        $("#spanMetode").empty();
        var kategoriSelected = $("#kategoriLelangSelect option:selected").val();
        var metodePemilihanSelected = $("#metodePemilihanSelect option:selected").val();
        $.post("/spse4/jsonctr/gethtmlselectmetodeseleksi", {mtd: metodePemilihanSelected, kgr: kategoriSelected}, function(data) {
                $("#spanMetode").append(data); }, "html"
        );
    });
    $("#spanMetode").empty();
    var kategoriSelected = $("#kategoriLelangSelect option:selected").val();
    var metodePemilihanSelected = $("#metodePemilihanSelect option:selected").val();
    $.post("/spse4/jsonctr/gethtmlselectmetodeseleksi", {mtd: metodePemilihanSelected, kgr: kategoriSelected}, function(data) {
            $("#spanMetode").append(data); }, "html"
    );
});


/* -------------------------- SCRIPT utility untuk Check dan Uncheck All Combo Box  --------------------------- */
// ------------------------------------------------------------------------------------------------------------

// Script ini belum handle kasus-kasus tertentu
function checkedOrUncheckAll() {
    if (document.getElementById('checkAll').checked == false) {
        // alert('uncheck all');
        var elements = getElementsByClassName('checkbox',null);
        for (var i=0, j=elements.length; i < j; i++) {
            elements[i].checked=false;
        }
    } else {
        // alert('check all');
        var elements = getElementsByClassName('checkbox',null);
        for (var i=0, j=elements.length; i < j; i++) {
            elements[i].checked=true;
        }
    }
}
function getElementsByClassName(classname, node) {
    if(!node) node = document.getElementsByTagName("body")[0];
    var a = [];
    var re = new RegExp('\\b' + classname + '\\b');
    var els = node.getElementsByTagName("*");
    for(var i=0,j=els.length; i<j; i++)
        if(re.test(els[i].className))a.push(els[i]);
    return a;
}

/* ------------------------ SCRIPT utility untuk Add/Remove Row Tabel Secara Dinamis  ------------------------- */
// ------------------------------------------------------------------------------------------------------------
function addRow(source,target,initLength) {
    var rows = $("."+target+">tbody>tr").length - initLength + 1;
    alert(rows);
    // clone source
    var clonedRow = $("."+source).clone();
    // get all textfield
    var textfield = clonedRow.find(":text")
    // get all text area
    var textarea = clonedRow.find("textarea");
    // get all checkbox
    var checkbox = clonedRow.find(":checkbox");
    // alert(textfield.length + " " + textarea.length + " " + checkbox.length);
    // modifiy each properties
    textfield.each(function() {
        var name = $(this).attr("name"); // get name properties
        $(this).attr("name", name+"_"+rows);
        $(this).val('');
    });
    textarea.each(function() {
       var name = $(this).attr("name"); // get name properties
       $(this).attr("name", name+"_"+rows);
       $(this).val('');
    });
    checkbox.each(function() {
        $(this).val(rows);
    });
    clonedRow.removeAttr("class").appendTo("."+target);
}
