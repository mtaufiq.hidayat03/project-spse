$(function(){
    $('#date').jclock({format: '%d %B %Y %H:%M', seedTime: parseInt(${time}), utc:true, utc_offset : ${offset}});
    $('.popup').popupWindow({ centerScreen:1,height:500,width:800 }); 
});