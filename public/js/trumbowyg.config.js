$('.trumbowyg').trumbowyg({
    btns : [['viewHTML'], ['undo', 'redo'], ['formatting'], ['strong', 'em', 'del'], ['superscript', 'subscript'], ['link'], ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],['unorderedList', 'orderedList'], ['horizontalRule'], ['removeformat'], ['fullscreen']],
    autogrow : true,
    removeformatPasted: true
})