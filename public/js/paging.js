var pCarousel = {
	animationId: null,
	animationDir: 'right',
	
	//time in miliseconds to wait before scrolling
	animationTimeout: 1500,
	
	//time in miliseconds for the scrolling transition
	animationSpeed: 200,
	
	
	init: function(carousel) {
		// Disable autoscrolling if the user clicks the prev or next button.
		carousel.clip.hover(function() { pCarousel.halt() }, function() { pCarousel.animate(carousel); });
		pCarousel.animate(carousel);
		
		jQuery('#paging a.next').bind('click', function() {
        pCarousel.halt();
        carousel.next();
        return false;
    });
		
		jQuery('#paging a.previous').bind('click', function() {
        pCarousel.halt();
        carousel.prev();
        return false;
    });
		
	
	},
	
	animate: function(carousel) {
		pCarousel.animationId = setInterval( function() {
			if( carousel.last == carousel.options.size ) { pCarousel.animationDir = 'left'; }
			else if( carousel.first == 1 ) { pCarousel.animationDir = 'right'; }
	
			if( pCarousel.animationDir == 'right' ) { carousel.next(); }
			else { carousel.prev(); }
		}, pCarousel.animationTimeout );
	},
	
	halt: function() {
		if( pCarousel.animationId ) {
			clearInterval(pCarousel.animationId);
		}
	}
	
};