	var total_nilai_pengalaman = 0;
	var subunsur_pengalaman_sejenis = 0;
	var subunsur_pengalaman_selokasi = 0;
	var subunsur_pengalaman_manajerial = 0;
	var subunsur_kapasitas_perusahaan = 0;
	var subunsur_lain_pengalaman = 0; 

	function hitung_subunsur_pengalaman(){
		a = parseFloat($('#subunsur_pengalaman_sejenis').val());
		b = parseFloat($('#subunsur_pengalaman_selokasi').val());
		c = parseFloat($('#subunsur_pengalaman_manajerial').val());
		d = parseFloat($('#subunsur_kapasitas_perusahaan').val());
		e = parseFloat($('#subunsur_lain_pengalaman').val());
		f = parseFloat($('#unsur_pengalaman_perusahaan').val());
				
       	total_nilai_pengalaman = ((a + b + c + d + e)*f/100)/5;	
	}	   
	
	function hitung_subunsur_pendekatan(){
	
		a = parseFloat($('#subunsur_pemahaman').val());
		b = parseFloat($('#subunsur_kualitas').val());
		c = parseFloat($('#subunsur_hasil_kerja').val());		
		d = parseFloat($('#subunsur_fasilitas_pendukung').val()); //hanya untuk konsultansi perorangan
		e = parseFloat($('#subunsur_gagasan_baru').val());
		f = parseFloat($('#subunsur_lain_pendekatan').val());
		g = parseFloat($('#unsur_pendekatan').val());
		total_nilai_pendekatan = ((a + b + c + d + e + f)*g/100);
		$('#total_nilai_pendekatan_hide').val(parseFloat(total_nilai_pendekatan));
	}
	
	function hitung_subunsur_kualifikasi(){
		a = parseFloat($('#subunsur_pendidikan').val());
		b = parseFloat($('#subunsur_pengalaman_kerja').val());
		c = parseFloat($('#subunsur_sertifikat_keahlian').val());
		d = parseFloat($('#subunsur_lain_kualifikasi').val());
		e = parseFloat($('#unsur_kualifikasi').val());
		var total = 0

		$('[id^=bobot]').each(function() {
			var value = $(this).val();
			total = total+parseFloat(value);
		});

		var grandTotal = total / $('[id^=bobot]').length;

		total_nilai_kualifikasi = ((a + b + c + d + grandTotal)*e/100)/5;
		
		//$('#total_nilai_kualifikasi').text(parseFloat(total_nilai_kualifikasi));
		//$('#total_nilai_kualifikasi2').val(parseFloat(total_nilai_kualifikasi));
		$('#total_nilai_kualifikasi_hide').val(parseFloat(total_nilai_kualifikasi));
	}	

	//----------------------------------------------------------

	$(document).on('click', '.removeBtn', function(){
		$(this).parent().parent().remove()
	})
	$( document ).ready(function() {
		init();
		onlyNumber();
	});

	$("#tambahTenagaAhli").click(function(){
		var length = $('#tblTenagaAhli tr').length - 1
		var addTenagaAhli = '<tr><td> <div class="input-group" style="width:300px;padding-top:5px;padding-bottom:5px;">' +
				' <input type="text" id="profesi[]" name="tenagaAhli['+length+'][0]" class="form-control" /></div></td><td>' +
				'<div class="input-group" style="width:100px;padding-top:5px;"> ' +
				'<input type="text" id="bobot[]" name="tenagaAhli['+length+'][1]" class="form-control" style="width:80px;" /> ' +
				'<span class="input-group-addon">%</span></div> </td> <td><a href="javascript:void(0)" class="removeBtn">'+
				'<i class="fa fa-trash-o fa-lg" style="padding: 15px 10px 10px 10px;"></i></a></td> </tr>'

		$("#tblTenagaAhli").append(addTenagaAhli)
		document.getElementsByName("tenagaAhli["+length+"][1]")[0].defaultValue = '0.0';

		onlyNumber();

		$('[id^=bobot]').change(function (e) {
			hitung_subunsur_kualifikasi();
		});
	})

	$("[name=simpan]").click(function(){

		//validasi sub unsur pendekatan dan metodologi
		var totalSubUnsurPendekatanDanMetodologi = hitungSubUnsurPendekatanDanMetodologi() ;
		if (totalSubUnsurPendekatanDanMetodologi > 100){
			window.scrollTo(0, 1475);
			$("#errmsg_2").html("* Total nilai Sub unsur pendekatan dan metodologi lebih dari 100.").show();
			return false;
		} else if(totalSubUnsurPendekatanDanMetodologi < 100){
			window.scrollTo(0, 1475);
			$("#errmsg_2").html("* Total nilai Sub unsur pendekatan dan metodologi kurang dari 100.").show();
			return false;
		}

		//validasi sub unsur tenaga ahli
		var totalSubUnsurTenagaAhli = hitungSubUnsurTenagaAhli() ;
		if (totalSubUnsurTenagaAhli > 100){
			window.scrollTo(0, 1900);
			$("#errmsg_3").html("* Total nilai Sub unsur tenaga ahli lebih dari 100.").show();
			return false;
		} else if(totalSubUnsurTenagaAhli < 100){
			window.scrollTo(0, 1900);
			$("#errmsg_3").html("* Total nilai Sub unsur tenaga ahli kurang dari 100.").show();
			return false;
		}

		//validasi bobot
		var totalBobot = hitungBobot();
		if (totalBobot > 100){
			window.scrollTo(0, 1000);
			$("#errmsg_bobot_teknis").html("* Total bobot unsur unsur pokok lebih dari 100.").show();
			return false;
		} else if(totalBobot < 100){
			window.scrollTo(0, 1000);
			$("#errmsg_bobot_teknis").html("* Total bobot unsur unsur pokok kurang dari 100.").show();
			return false;
		}


		var err = false;
		$('[id^=profesi]').each(function() {
			if ($(this).val() === ""){
				err = true
			}
		});

		if (!err){
			var total = 0;
			$('[id^=bobot]').each(function() {
				var value = $(this).val();
				total = total+parseFloat(value);
			});
			if (total > 100){
				window.scrollTo(0, 2000);
				$("#errmsg").html("* Total bobot melebihi batas, batas maksimum total bobot = 100").show();
				return false;
			} else if(total < 100){
				window.scrollTo(0, 2000);
				$("#errmsg").html("* Total bobot kurang dari 100, total bobot harus = 100").show();
				return false;
			}
		} else {
			window.scrollTo(0, 2000);
			$("#errmsg").html("* Kolom profesi wajib diisi").show();
			return false;
		}

	});

	function init(){

		//document.getElementById("total_nilai_pengalaman_hide").defaultValue = '0.0';
		//document.getElementById("total_nilai_pendekatan_hide").defaultValue = '0.0';
		//document.getElementById("total_nilai_kualifikasi_hide").defaultValue = '0.0';

		var defaultVal = document.getElementsByName("tenagaAhli[0][1]")[0].value;
		if (defaultVal === ""){
			document.getElementsByName("tenagaAhli[0][1]")[0].defaultValue = '0.0';
		}

		$('[id^=bobot]').keypress(function (e) {
			return isNumberKey(e);
		});

		/*$('[id^=bobot]').change(function (e) {
			hitung_subunsur_kualifikasi();
		});*/

		$("#errmsg, #errmsg_1, #errmsg_2, #errmsg_3, #errmsg_bobot_teknis").hide();
	}

	function onlyNumber(){
		$('[id^=bobot]').keypress(function (e) {
			return isNumberKey(e);
		});

		$('#subunsur_pengalaman_sejenis, #subunsur_pengalaman_selokasi,#subunsur_pengalaman_manajerial,' +
				'#subunsur_kapasitas_perusahaan,#subunsur_lain_pengalaman').keypress(function (e) {
			return isNumberKey(e);
		});

		$('#subunsur_pemahaman, #subunsur_kualitas,#subunsur_hasil_kerja,' +
				'#subunsur_gagasan_baru,#subunsur_lain_pendekatan').keypress(function (e) {
			return isNumberKey(e);
		});

		$('#subunsur_pendidikan, #subunsur_pengalaman_kerja,#subunsur_sertifikat_keahlian,' +
				'#subunsur_lain_kualifikasi').keypress(function (e) {
			return isNumberKey(e);
		});

		$('#unsur_pengalaman_perusahaan, #unsur_kualifikasi,#unsur_pendekatan').keypress(function (e) {
			return isNumberKey(e);
		});
	}

	function hitungSubUnsurPengalamanPerushaan(){
		var a = parseFloat($('#subunsur_pengalaman_sejenis').val());
		var b = parseFloat($('#subunsur_pengalaman_selokasi').val());
		var c = parseFloat($('#subunsur_pengalaman_manajerial').val());
		var d = parseFloat($('#subunsur_kapasitas_perusahaan').val());
		var e = parseFloat($('#subunsur_lain_pengalaman').val());

		return a + b +c +d +e ;
	}

	function hitungSubUnsurPendekatanDanMetodologi(){
		a = parseFloat($('#subunsur_pemahaman').val());
		b = parseFloat($('#subunsur_kualitas').val());
		c = parseFloat($('#subunsur_hasil_kerja').val());
		d = parseFloat($('#subunsur_gagasan_baru').val());
		e = parseFloat($('#subunsur_lain_pendekatan').val());
		f = parseFloat($('#subunsur_fasilitas_pendukung').val()); //hanya untuk konsultansi perorangan

		return a + b +c +d +e+f;
	}

	function hitungSubUnsurTenagaAhli(){
		a = parseFloat($('#subunsur_pendidikan').val());
		b = parseFloat($('#subunsur_pengalaman_kerja').val());
		c = parseFloat($('#subunsur_sertifikat_keahlian').val());
		d = parseFloat($('#subunsur_lain_kualifikasi').val());

		return a + b +c +d;
	}

	function hitungBobot(){
		a = parseFloat($('#unsur_pengalaman_perusahaan').val());
		b = parseFloat($('#unsur_kualifikasi').val());
		c = parseFloat($('#unsur_pendekatan').val());

		return a + b +c;
	}