SPSE 4.2.x
========
``Dokumentasi ini tidak akurat. Silakan tanya kepada pada developer langsung untuk penjelasan sistem terbaru.``

Daftar Isi
1. [Requirement](#requirement)
2. [Startup Parameters](#startup-parameters)
3. [Profiling](#profiling)
4. [Melakukan Debug di Server Remote](#melakukan-debug-di-server-remote)
5. [Build Untuk *Production*](#build-untuk-production)
6. [CA](#CA)
7. [Troubleshooting CA 4.3](#TCA)

### Requirement

1. [Play 1.4.2c](https://gitlab.lkpp.go.id/play-framework/play/tree/play-1.4.2c)
2. Postgres 8.4
3. Java 1.8.40 dengan JCE

## Development Notes
### Startup Parameters
Untuk kebutuhan development, dapat ditambahkan ini pada *application launcher*
<pre>
1. -Dworflow.manager.disabled=[true|false]
	Men-disable workflow manager 
2. -Dmail.sending.disabled=[true|false]
	Men-disable pengiriman email
3. -Dcurrent.date=dd-MM-yyyy [hh:mm]
    Setting current date (seperti pada user TRAINER) sehingga datetime pada SPSE menjadi seperti
    yang tertera. Ini penting untuk workflow
</pre>

### Untuk testing jaim-agent

<pre>
  -Dworflow.manager.disabled=true -Dmail.sending.disabled=true -Djaim.jaimServer=http://localhost:9977 -Djaim.spseRoot=/home/appserv -Djaim.app_id=spse-prod-4.1.2
</pre>

### Profiling
Seringkali kita perlu mencari tahu penggunaan memory, method calls, serta kondisi saat runtime. Untuk itu kita bisa menggunakan **JVisualVM** 
dan [Java Misson Control](http://www.oracle.com/technetwork/java/javaseproducts/mission-control/index.html). Kedua tools terdapat pada folder $JAVA_HOME/bin/.
JVisualVM dapat mengakses aplikasi di mesin yang berbeda asalkan aplikasi di-run dengan tambahan argumen seperti di bawah in:
<pre>
-Dcom.sun.management.jmxremote.port=9090 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=10.1.16.8
Keterangan:
10.1.16.8 => IP dari server
9090 => port untuk console
</pre>
Sementara itu, untuk menggunakan JMC pertu tambahan argument seperti di bawah ini. 
<pre> -XX:+UnlockCommercialFeatures -XX:+FlightRecorder</pre>

### Melakukan Debug di Server Remote

1. Di server remote, misal 10.1.30.151 (bisa mode dev atau prod), tambahkan parameter 
  <pre>-Xdebug -Xrunjdwp:transport=dt_socket,address=8888,server=y,suspend=n -Dplay.debug=yes</pre>
2. Buka Project SPSE-4 di eclipse. Pastikan Anda menggunakan *revision* yang sama dengan yang akan di-debug.
2. Buka menu *Run > Debug > Debug Configuration....* Pilih: 'Connect JPDA to spse-4.0.0'. 
   <pre>host: isikan IP dari server; pada contoh 10.1.30.151
   port: isikan seperti setting di server (pada contoh: 8888)</pre>
3. Anda juga dapat melakukan *SSH tunneling* dengan mengisi *host: localhost*.

## Build Untuk *Production*

Build untuk kebutuhan production dilakukan melalui *build.xml* menggunakan [apache ant](http://ant.apache.org/) bawaan Eclipse. Target yang digunakan adalah *build-prod*. Penjelasan

1. Hasil *build-prod* disimpan pada folder *../installer-spse4* yang merupakan *clone* dari **https://gitlab.lkpp.go.id/installer/spse4.git*.
2. Versi spse-4.0, spse-4.1, atau spse-4.2 (dan seterusnya) merupakan sebuah *branch* dari gitlab tersebut. Pastikan Anda melakukan *checkout* ke branch yang benar. Isikan **$VERSION** dengan nama versi yang benar (misal SPSE-4.1).
   <pre>git clone https://gitlab.lkpp.go.id/installer/spse4.git installer-spse4
git checkout $VERSION    #switch ke branch $VERSION
git status               #memeriksa status ini ada di branch mana</pre>
3. Pastikan Environtment Variable *PLAY_HOME* mengarah ke Play versi yang tepat.
4. Jalankan ant build via eclipse dengan target *build-prod*.
5. Jika build selesai, *../installer-spse4* akan berisi hasil build.
6. Lakukan update ke git sebagai berikut
<pre>git checkout $VERSION
git pull --all
git add *							#tambahkan file hasil build
git commit -m 'build #xxx dd-MMM-dd' 	# Isikan dd-MMM-dd dengan tanggal build
git push							# push ke gitlab server
</pre>

### CA
### CA
1. Pastikan file spse-ca-truststore.jks(bundle) ada di dalam folder conf.
2. Set tanggal di table configuration dengan key <pre> amanda-start-date </pre> dengan format yyyy-MM-dd hh:mm, contoh query <pre> INSERT INTO "public"."configuration" ("cfg_category", "cfg_sub_category", "audittype", "audituser", "auditupdate", "cfg_value", "cfg_comment") VALUES ('CONFIG', 'amanda-start-date', 'C', 'ADMIN', '2017-06-08 03:44:52.375489', '2019-01-03 14:44', 'Tanggal start amanda');
 </pre> NOTE: SESUAIKAN TANGGALNYA UNTUK MASING-MASING LPSE 
3. Tambahkan config ke application.conf atau bisa menggunakan config dari JAIM pastikan aplikasi memiliki UUID, 
contoh config. 
<pre>
# dev
keystore=lpse-latihan.jks
keystore.pass=123456
keystore.server.alias=lpse-latihan
truststore=spse-ca-truststore.jks
truststore.pass=changeit
truststore.ca.alias=osdpselatihanv2
ttp.url=https://10.1.30.151:8005/kms
ttp.client.secret=ttp
ttp.client.id=client
ams_url=https://ams-dev.bssn.go.id 
ams_client_id=67543527
ams_oid=2.16.360.1.2.1.70.106
ams_client_secret=umu9-4a0p-usqg-3j37
ams_client_grant_type=client_credentials
ams_ocsp=http://cvs-osd.lemsaneg.go.id/ocspdev
ams_ocsp_key=osd-pse-demo
ams_digital_signature=Tanda Tangan Digital dan Enkripsi
</pre>
<pre>
# production 
keystore=lpse-SERVER.<REPOID>.jks // konfigurasi keystore, specific untuk setiap lpse tanyakan pada admin lpse/bsre
keystore.pass=?
keystore.server.alias=?
truststore=spse-ca-truststore.jks
truststore.pass=changeit
truststore.ca.alias=osd-pse-gen2
// kms, tanyakan akses pada BSRE
ttp.url=https://kms.lkpp.go.id/kms/
ttp.client.secret=?
ttp.client.id=?
// ams, setiap lpse akan memiliki configurasi berbeda untuk client/oid/secret, tanyakan pada BSRE
ams_url=https://ams-bsre.bssn.go.id
ams_client_id=?
ams_oid=2.16.360.1.2.1.70.106.70.xxx // .xxx otomatis akan digenerate oleh SPSE, cukup diisi 2.16.360.1.2.1.70.106.70
ams_client_secret=?
ams_client_grant_type=client_credentials
ams_ocsp=http://cvs-bsre.bssn.go.id/ocsp/osdpseg2
ams_ocsp_key=osd-pse-gen2
ams_digital_signature=LPSE AIO Tanda Tangan Digital dan Enkripsi
</pre>
<pre>
// konfig ini berlaku global prod/dev
spse_truststore=spse-ca-truststore.jks // file jks yang memiliki lebih dari satu certifikat di dalamnya dev/prod
spse_truststore_pass=changeit
</pre>

### Troubleshooting CA 4.3
Apa yang harus dilakukan jika SPSE-CA 4.3 mengalami masalah.
1. Check apakah service AMS atau kms bisa diakses melalui browser/curl.
2. Check apakah sudah mengkonfigurasi spse.uuid di application.conf.
3. Check apakah LPSE sudah mengaktifkan update otomatis di halaman edit LPSE pada JAIM.
4. Check apakah file spse-ca-truststore.jks, <lpse_specific.jks> ada di dalam folder conf, pastikan cert di dalamnya adalah yang terbaru, hubungi BSRE untuk info lebih lanjut.
5. Hubungi BSRE untuk mendapatkan konfigurasi AMS.
6. Check apakah custom konfigurasi untuk LPSE tersebut sudah benar, check fitur custom konfigurasi di JAIM.
7. Tunggu sekitar 5 menit untuk melihat status service AMS apakah ada perubahan.
8. Check apakah browser yang digunakan sudah sesuai, pastikan menggunakan Amanda Browser/Browser Spamkodok2.
9. Check apakah sudah mengkonfigurasi 'amanda-start-date' pada database pastikan apakah formatnya sudah sesuai yyyy-MM-dd hh:mm.
10. Login salah satu user POKJA/REKANAN dan akses /osdregservice/getCertServer jika mendapatkan response berupa certificate, maka config keystore berhasil.

Jika cara-cara di atas belum berhasil, bisa melihat config LPSE CA di Aplikasi SIKaP/JaIM sebagai referensi.
Untuk semua info mengenai certificate tanyakan pada BSRE.