package businesslogic;

import com.google.gson.Gson;
import jaim.agent.JaimConfig;
import jaim.common.model.ChangelogItem;
import jaim.common.model.UpdateSchedule;
import models.common.ConfigurationDao;
import org.apache.commons.lang3.ArrayUtils;
import play.Logger;
import play.cache.Cache;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

public class ServicePackChangelog {
    public static ChangelogItem[] getChangeLogs(UpdateSchedule us) {
        if (us == null)
            return null;
        String url = String.format("%s/servicePack/%s/changelogs", JaimConfig.jaimServer, us.sp_id);
        Logger.info("[JAIM] get changelog for service pack : %s", us.sp_id);
        try {
            WSRequest request = WS.url(url);
            HttpResponse resp = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
            if (resp.success()) {
                return new Gson().fromJson(resp.getJson(), ChangelogItem[].class);
            }
        } catch (Exception e) {
            Logger.error("[JAIM] Failed to get changelog");
            Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, true);
        }
        return null;
    }

    public static Optional<Date> getLastDate(ChangelogItem[] changeLogs) {
        if(ArrayUtils.isEmpty(changeLogs))
            return Optional.empty();
        final Optional<Date> last = Arrays.stream(changeLogs)
                .map(i1 -> i1.getTime())
                .filter(i -> i != null)
                .sorted()
                .reduce((first, second) -> second);
        Logger.debug("last = %s", last.isPresent()? last.get().toString() : "NULL");
        return last;
    }

}