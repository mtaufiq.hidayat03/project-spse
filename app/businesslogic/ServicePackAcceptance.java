package businesslogic;

import jaim.agent.JaimConfig;
import jaim.common.model.ErrorMessage;
import jaim.common.model.UpdateSchedule;
import models.jcommon.config.Configuration;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import utils.JsonUtil;

import java.util.Optional;

public class ServicePackAcceptance {
    final public static String OK = "ok";
    final public static String GENERIC_FAIL = "Persetujuan gagal didaftarkan";
    public static String setujuiTolakUpdate(String last_date, Long up_id, String setujuAtauTolak) {
        try {
            Optional<UpdateSchedule> updateSchedule = Optional.of(UpdateSchedule.findById(up_id));
            String lpse_versi = Configuration.getConfigurationValue("ppe.versi");
            String uuid = Play.configuration.getProperty("spse.uuid");
            final String acceptanceFormatString = setujuAtauTolak.equals("SETUJU") ? "%s/servicePack/%s/accept" : "%s/servicePack/%s/reject";
            String url = String.format(acceptanceFormatString, JaimConfig.jaimServer, updateSchedule.get().sp_id);
            WSRequest req = WS.url(url);
            req.timeout(JaimConfig.JAIM_CONNECTION_TIMEOUT).setParameter("applicationDate", last_date)
                    .setParameter("lpse_versi", lpse_versi).setParameter("uuid", uuid);
            HttpResponse resp = req.post();
            Logger.info("[JaimCtr] setujui/tolak - code: " + resp.getStatus() + " message: " + resp.getStatusText());
            if (!resp.success()) {
                Logger.error("[JaimCtr] setujui/tolak - %s", resp.getString());
                ErrorMessage errorMessage = JsonUtil.fromJson(resp.getString(), ErrorMessage.class);
                return String.format("Terdapat kendala pada proses ke JAIM SERVER. %s.", errorMessage.message);
            } else {
                Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, false);
                Logger.info("[JaimCtr] persetujuan sukses");
                return OK;
            }
        } catch (Exception e) {
            Logger.error(e, "Persetujuan gagal");
            return GENERIC_FAIL;
        }
    }
}