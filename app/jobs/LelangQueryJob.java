package jobs;

import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;
import play.jobs.Job;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class ini untuk generate lelang_query yg akan digunakan
 * di halaman Cari paket
 * @author andikyulianto@yahoo.com
 * fungsi job diganti sama trigger update_lelang_query()
 */

@Deprecated
//@OnApplicationStart(async=true)
//@On("0 0 1/1 * * ?")
//@NoTransaction
public class LelangQueryJob extends Job {

    private static AtomicBoolean isRunning=new AtomicBoolean();

    static final QueryBuilder query = new QueryBuilder("INSERT INTO lelang_query SELECT l.lls_id, p.pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_id, mtd_pemilihan, mtd_evaluasi, kgr_id, paket_instansi(p.pkt_id) as nama_instansi,lls_penawaran_ulang, ")
            .append("(SELECT kontrak_id FROM kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_id, ")
            .append("(SELECT kontrak_nilai FROM kontrak WHERE lls_id=l.lls_id ORDER BY auditupdate DESC limit 1) as kontrak_nilai, p.is_pkt_konsolidasi,")
            .append("(SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, lls_versi_lelang, ")
            .append("(SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=l.lls_id) as eva_versi, l.lls_status, tahap_now(l.lls_id, current_timestamp::timestamp without time zone) tahap_sekarang, ")
            .append("setweight(to_tsvector('english',coalesce(pkt_nama,'')), 'A') || setweight(to_tsvector('english',coalesce(paket_instansi(p.pkt_id),'')), 'B') ")
            .append("FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id WHERE lls_status=1 ");

    public void doJob()
    {
        if(isRunning.getAndSet(true))
            return;
        StopWatch sw=new StopWatch();
        sw.start();
        Query.update("truncate table lelang_query");
        Query.update(query);
        sw.stop();
        Logger.debug("[LelangQueryJob] DONE, duration: %s", sw);
    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t)
    {
        after();
    }
}
