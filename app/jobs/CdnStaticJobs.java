package jobs;

import models.common.ConfigurationDao;
import play.Logger;
import play.Play;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.libs.WS;
import play.libs.ws.HttpResponse;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Job buat check status cdn yg bisa dipake oleh SPSE
 */
@OnApplicationStart(async = true)
@Every("5mn")
public class CdnStaticJobs extends Job {

    final static String[] URLs = new String[]{"https://static.lkpp.go.id/spse", "https://static2.lkpp.go.id/spse"};

    private static AtomicBoolean isRunning=new AtomicBoolean();

    public void doJob() throws Exception {
        if(isRunning.getAndSet(true))
            return;
//        long responseTime = -1;
        if("true".equals(Play.configuration.getProperty("cdn.enabled", "false"))) {
            // check konektivitas ke cdn
            try {
                HttpResponse response = WS.url(URLs[0] + "/images/lpse.ico").getAsync().get();
                if(response.success())
                    ConfigurationDao.CDN_URL = URLs[0];
            }catch (Exception e) {
                try {
                    HttpResponse response = WS.url(URLs[1] + "/images/lpse.ico").getAsync().get();
                    if (response.success())
                        ConfigurationDao.CDN_URL = URLs[1];
                }catch (Exception ex) {
                    Logger.error(ex, "Sistem Tidak bisa konek ke CDN lkpp dan Menggunakan lokal kontent : %s", ConfigurationDao.CDN_URL);
                }
            }
            Logger.info("Sistem Meggunakan CDN url %s", ConfigurationDao.CDN_URL);
        }
    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t)
    {
        after();
    }
}
