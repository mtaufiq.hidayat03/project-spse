package jobs;

import models.common.Active_user;
import models.secman.Usrsession;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.jobs.Every;
import play.jobs.Job;
import utils.LogUtil;

import java.io.*;
import java.util.*;

/* job manages active user
	 @author arief.ardiyansah
 */
@Every("30s")
public class JobActiveUser extends Job {
	
	static final String maxAge = Play.configuration.getProperty("application.session.maxAge", "30mn");
	static final String ACTIVE_USER_CACHE = "ACTIVEUSER";

	static File FILE_CACHE = new File(Play.tmpDir, "activeuser.db");

	public JobActiveUser() {
		if(Play.mode.isDev() && FILE_CACHE.exists()) {
			Logger.info("load session dari tmp");
			try {
				ByteArrayInputStream bais = new ByteArrayInputStream(FileUtils.readFileToByteArray(FILE_CACHE));
				ObjectInputStream ois = new ObjectInputStream(bais);
				Map<String, Active_user> cache = (Map<String, Active_user>) ois.readObject();
				putAll(cache);
			} catch (IOException | ClassNotFoundException e) {
				Logger.error(e, "Kendala Populate cache");
			}
		}
	}
	
	@Override
	public void doJob() throws Exception {
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(!MapUtils.isEmpty(activeUser)) {
			Iterator<String> iter = activeUser.keySet().iterator();
			while (iter.hasNext()) {
				String sessionid = iter.next();
				if (get(sessionid) == null) {
					Logger.debug("remove by job : %s", sessionid);
					Long id = activeUser.get(sessionid);
					Usrsession.setWaktuLogout(id, new Date());
					iter.remove();
				}
			}
			Cache.set(ACTIVE_USER_CACHE, activeUser);
		}
	}	
	
	public static void add(Active_user user) {
		if(user == null)
			return;
		Cache.safeSet("user_"+user.sessionid, user, maxAge);
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(activeUser == null)
			activeUser = new HashMap<>();
		activeUser.put(user.sessionid, user.id);
		Cache.set(ACTIVE_USER_CACHE, activeUser);
	}
	
	// remove by sessionId
	public static void remove(String sessionId) {
		if(StringUtils.isEmpty(sessionId))
			return;
		Cache.safeDelete("user_"+sessionId);
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(!MapUtils.isEmpty(activeUser)) {
			activeUser.remove(sessionId);
			Cache.set(ACTIVE_USER_CACHE, activeUser);
		}
	}
	
	// remove by Active_user
	public static void remove(Active_user user) {
		if(user == null)
			return;
		Cache.safeDelete("user_"+user.sessionid);
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(!MapUtils.isEmpty(activeUser)) {
			activeUser.remove(user.sessionid);
			Cache.set(ACTIVE_USER_CACHE, activeUser);
		}
	}
	
	public static void update(Active_user user) {
		Cache.safeReplace("user_"+user.sessionid, user, maxAge);
	}
	
	public static Active_user get(String sessionId) {
		return Cache.get("user_"+sessionId);
	}
	
	public static boolean isLogged(String sessionId) {
		return Cache.get("user_"+sessionId) != null;
	}
	
	public static int getLoggedCount(Active_user user) {
		int count = 0;
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(!MapUtils.isEmpty(activeUser)) {
			for (String sessionid : activeUser.keySet()) {
				Active_user obj = get(sessionid);
				if (obj!= null && !StringUtils.isEmpty(user.sessionid) && !StringUtils.isEmpty(obj.sessionid)
						&& !user.sessionid.equals(obj.sessionid) && user.userid.equals(obj.userid))
					count += 1;
			}
		}
		return count;
	}
	
	// get All active user
	public static List<Active_user> findAll() {
		List<Active_user> list = new ArrayList<>();
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(!MapUtils.isEmpty(activeUser)) {
			for (String sesionid : activeUser.keySet()) {
				list.add(get(sesionid));
			}
		}
		return list;
	}

	public static Map<String, Active_user> findAllMap() {
		Map<String, Active_user> map = new HashMap<>();
		Map<String, Long> activeUser = Cache.get(ACTIVE_USER_CACHE, Map.class);
		if(!MapUtils.isEmpty(activeUser)) {
			for (String sesionid : activeUser.keySet()) {
				map.put(sesionid, get(sesionid));
			}
		}
		return map;
	}

	public static void putAll(Map<String, Active_user> maps) {
		if(MapUtils.isEmpty(maps))
			return;
		for (Map.Entry<String, Active_user> entry : maps.entrySet()) {
			add(entry.getValue());
		}
	}

	// temporary save active user, agar developer tidak selalu refresh
	public static void saveTemporary() {
		if(Play.mode.isDev()) {
			try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);
				oos.writeObject(findAllMap());
				FileUtils.writeByteArrayToFile(FILE_CACHE, baos.toByteArray());
				Logger.debug("write session %s to %s", findAllMap(), FILE_CACHE);
			} catch (IOException e) {
				LogUtil.error("ApplicationStop", e,"Gagal save cache to file cache.db");
			}
		}
	}
}
