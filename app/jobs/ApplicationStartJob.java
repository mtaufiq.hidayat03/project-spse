
package jobs;

import ams.clients.AmsClient;
import ams.utils.KeyStoreUtil;
import jobs.migration.DatabaseCheck;
import jobs.migration.DatabaseMigration;
import models.common.ConfigurationDao;
import models.common.KontenMultimedia;
import models.common.SesiPelatihan;
import models.jcommon.secure.encrypt2.InMemoryRSAKey;
import models.jcommon.util.DateUtil;
import play.Logger;
import play.Play;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import utils.osd.KmsClient;

import java.text.ParseException;
import java.util.Date;

/**
 * Class ini diload saat aplikasi startup
 * semua konfigurasi yang diperlukan untuk menjalankan aplikasi harus diset di sini
 * @author Arief Ardiyansah
 *
 */
@NoTransaction
@OnApplicationStart
public class ApplicationStartJob extends Job {

	@Override
	public void doJob() throws Exception {
		// check status migrasi
		if(Play.mode.isProd()) {
			Play.configuration.put("http.cacheControl", "86400"); // set 1 hari untuk improve performance
			DatabaseMigration dbmigrasi = new DatabaseMigration();
			dbmigrasi.check();
			if (dbmigrasi.isNeedMigration()) {
				dbmigrasi.doMigration();
			}
			DatabaseCheck.sequence();
		}
		Logger.info("[STARTUP] Application Started on: %s",  new Date());
		ConfigurationDao.setupConfig();
		DatabaseCheck.sequence();


		/* set current.date yang diset pada command line argument
		 * Tersedia hanya pada mode DEV
		 */
		if(Play.mode.isDev())
		{
			String currentDateStr=System.getProperty("current.date");
			if(currentDateStr!=null)
			{
				Date currentDate;
				try {
					if(currentDateStr.indexOf(':')==-1)
						currentDateStr+=" 00:00";
					currentDate = DateUtil.parseSmallDate(currentDateStr);
					DateUtil.setDate(currentDate);
					Logger.info("[STARTUP] DEV mode, set current.date=%s", currentDateStr);

				} catch (ParseException e) {
					Logger.error(e,"[STARTUP] -Dcurrent.date INVALID FORMAT: %s", e.toString());
				}
			}
		}
		KontenMultimedia.setupConfig();
		SesiPelatihan.setupConfig();

		Logger.info("Generating RSA KEYS");
		InMemoryRSAKey.generateKeys();
		Logger.info("Finished Generating RSA KEYS");

		if(KeyStoreUtil.isEnabledAmsKms()) {
			KeyStoreUtil.generate();
			KmsClient.generate();
			AmsClient.generate();
		}
	}
}