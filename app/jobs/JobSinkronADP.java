package jobs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import models.common.ConfigurationDao;
import models.jcommon.util.DateConverter;
import models.jcommon.util.DoubleConverter;
import models.rekanan.Rekanan;
import models.sso.common.RekananSSO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.libs.Crypto;
import play.libs.Json;
import play.libs.WS;
import play.libs.ws.HttpResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * job melakukan sinkronisasi rekanan lokal dengan rekanan di ADP
 * running tiap 1 jam
 *
 * @author arief ardiyansah
 */
@OnApplicationStart(async = true)
@Every("1h")
public class JobSinkronADP extends Job {

    private static AtomicBoolean isRunning = new AtomicBoolean();
    public static final Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateConverter()).registerTypeAdapter(Double.class, new DoubleConverter()).create();

    static Type empMapType = new TypeToken<Map<Long, Long>>() {}.getType();

    @Override
    public void doJob() throws Exception {
        if (isRunning.getAndSet(true))
            return;
        Logger.info("JobSinkronADP started @%s", new Date());
        List<RekananSSO> list = new ArrayList<>();
        check(list);
        if (!CollectionUtils.isEmpty(list))
            sendToADP(list);
    }

    public void after() {
        isRunning.set(false);
    }

    public void onException(Throwable t) {
        after();
    }

    String getToken() {
        String token = RandomStringUtils.randomAlphanumeric(16);
        String sign = Crypto.sign(token);
        String param = token + " " + sign + " " + ConfigurationDao.getRepoId();
        HttpResponse response = WS.url(ConfigurationDao.ADP_URL + "/sinkron/token").setHeader("authorization", "Bearer " + param).get();
        if (response.success()) {
            return response.getString();
        }
        return null;
    }

    void check(List<RekananSSO> list) {
        String token = getToken();
        if (StringUtils.isEmpty(token))
            return;
        String sign = Crypto.sign(token);
        String param = token + " " + sign + " " + ConfigurationDao.getRepoId();
        HttpResponse response = WS.url(ConfigurationDao.ADP_URL + "/sinkron/rekanan")
                .setHeader("authorization", "Bearer " + param).get();
        Logger.debug("[ADP-CHECK] response status %s content : %s", response.getStatus(), response.getString());
        if (response.success()) {
            Map<Long, Long> map = Json.fromJson(response.getString(), empMapType);
            if (MapUtils.isEmpty(map))
                return;
            List<Long> listRekananId = new ArrayList(map.keySet());
            List<RekananSSO> rekananList = Rekanan.getListRekananRepo("AND rekanan.rkn_id NOT IN (" + StringUtils.join(listRekananId, ",") + ")");
            if (CollectionUtils.isEmpty(rekananList))
                return;
            for (RekananSSO userRekanan : rekananList) {
                if (userRekanan != null && userRekanan.btu_id != null && userRekanan.btu_id.equals("10")) // vendor asing dilarang masuk ADP Pusat
                    continue;
                if(StringUtils.isEmpty(userRekanan.passw) || userRekanan.passw.equals("#")) // jika passw == '#' berarti passw pake yg ada di ADP dan data di skip
                    continue;
                if (!StringUtils.isEmpty(userRekanan.rkn_npwp) && !StringUtils.isEmpty(userRekanan.rkn_nama)
                        && !StringUtils.isEmpty(userRekanan.rkn_email) && !StringUtils.isEmpty(userRekanan.rkn_namauser)) {
                    String userid = userRekanan.rkn_namauser;
                    userRekanan.rkn_namauser = Rekanan.clearUsernameADP(userid);
                    list.add(userRekanan);
                }
            }
        }
    }

    void sendToADP(List<RekananSSO> list) {
        String token = getToken();
        if (StringUtils.isEmpty(token))
            return;
        String sign = Crypto.sign(token);
        String param = token + " " + sign + " " + ConfigurationDao.getRepoId();
        String json = gson.toJson(list);
        Logger.info("InisiasiJob : Kirim " + list.size() + " penyedia Ke inaproc ");
        try {
            HttpResponse response = WS.url(ConfigurationDao.ADP_URL + "/sinkron/rekanan/push").setHeader("authorization", "Bearer " + param)
                    .setParameter("q", Crypto.encryptAES(json, token)).post();
            Logger.debug("[ADP-SEND] response status : %s", response.getStatus());
            Logger.debug("[ADP-SEND] response content: %s", response.getString());
            if (response.success()) {
                Logger.info("[ADP-SEND] data berhasil di kirim ke ADP pada %s", new Date());
            }

        } catch (Exception e) {
            Logger.info(e, "[ADP-SEND] data gagal di kirim ke ADP pada %s", new Date());
        }
    }
}
