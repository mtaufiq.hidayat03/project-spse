package jobs;

import models.jcommon.util.DateUtil;
import models.lelang.WorkflowDao;
import play.Logger;
import play.Play;
import play.jobs.Job;

/**
 * jobs yang mengatur dan mentrigger perpindahan state suatu lelang yang sedang aktif
 * perlu diset 5 menit sekali (@Every("5mn")
 * 
 * Job ini dapat di-DISABLED (khusus pada mode DEV) dengan menambahkan pada command line 
 * 		-Dworkflow.manager.disabled=true
 * 
 * @author FirstName LastName
 *
 */
public class WorkflowManager extends Job {
	
	private static Boolean wfDisabled=null;

	@Override
	public void doJob() throws Exception {
		/* check apakah workflowmanager disabled
		Jika ya maka job ini tidak perlu dilanjutkan
		*/
		if(wfDisabled==null)
		{
			String wf=System.getProperty("workflow.manager.disabled");
			if(wf==null) wf="false";
			wfDisabled=Boolean.parseBoolean(wf) && Play.mode.isDev();
			if(wfDisabled)
			{
				Logger.info("[WorkflowManagerJob] Job has been DISABLED.");
				return;
			}
			else
			{
				if(Play.mode.isDev())
					Logger.info("[WorkflowManagerJob] Job can be disabled using -Dworkflow.manager.disabled=true");
			}
			
		}
		else
			if(wfDisabled)
				return;		
		Logger.debug("[WorkflowManagerJob] Started on: %s", DateUtil.newDate());
		WorkflowDao.updateProcess();
//		new WorkflowManager().in(10);
	}	
}
