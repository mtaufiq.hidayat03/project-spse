package jobs;

import models.common.ConfigurationDao;
import play.Logger;
import play.Play;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import utils.NtpMessage;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicBoolean;

/*
     @author arief.ardiyansah
 Job Untuk memastikan bahwa system akan berjalan sesuai dengan waktu yg sebenarnya , sesuai dengan waktu dari list server NTP
 jika waktu sistem dimajukan atau dimundurkan selama batas toleransi (60 detik) maka system akan shutdown
 */
@OnApplicationStart(async = true)
@Every("20s")
public class DateCheckerJob extends Job {
    // server NTP reference
    private static final String[] ntp_server= new String[] {"0.id.pool.ntp.org","1.id.pool.ntp.org","time2.google.com", "time1.google.com"}; // asalnya ntp.kim.lipi.go.id

    private static final int offset = 60; // batas toleransi offset time (dalam second): default 60 detik (1 menit), jika lebih dari offset maka server diubah waktunya

    private static AtomicBoolean isRunning=new AtomicBoolean();

    public void doJob() throws Exception {
        if(isRunning.getAndSet(true))
            return;
        if(!ConfigurationDao.isProduction()) // memastikan hanya jalan pada system production
            return;
        long[] offset_local = new long[4];
        int i=0;
        for (String server : ntp_server) {
            offset_local[i] = timeOffsetNtp(server);
            i++;
        }
        if((offset_local[0] > offset) && (offset_local[1] > offset) && (offset_local[2] > offset) && (offset_local[3] > offset)){
            // terjadi perubahan waktu, system harus dishutdown
            Logger.info("Terjadi perubahan waktu oleh user, system akan dimatikan.....");
            try {
                Play.stop();
                System.exit(0);
            } catch (Exception e) {
                Logger.error("System tidak dapat dimatikan...", e);
            }
        }
    }

    /* get selisih waktu lokal dengan waktu ntp server
	 * dalam satuan detik
	 */
    public long timeOffsetNtp(String serverName) {
        long result  = 0L;
        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress address = InetAddress.getByName(serverName);
            byte[] buf = new NtpMessage().toByteArray();
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 123);
            // Set the transmit timestamp *just* before sending the packet
            // ToDo: Does this actually improve performance or not?
            NtpMessage.encodeTimestamp(packet.getData(), 40, (System.currentTimeMillis() / 1000.0) + 2208988800.0);
            socket.setSoTimeout(5000);
            socket.send(packet);
            packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);
            // Immediately record the incoming timestamp
            double destinationTimestamp = (System.currentTimeMillis() / 1000.0) + 2208988800.0;

            // Process response
            NtpMessage msg = new NtpMessage(packet.getData());

            // Corrected, according to RFC2030 errata
//			double roundTripDelay = (destinationTimestamp - msg.originateTimestamp) - (msg.transmitTimestamp - msg.receiveTimestamp);
            double localClockOffset = ((msg.receiveTimestamp - msg.originateTimestamp) + (msg.transmitTimestamp - destinationTimestamp)) / 2;
            result = (long) Math.abs(localClockOffset);
            // Display response
//            Logger.info("NTP server: " + serverName);
//            Logger.info("Dest. timestamp:     " + NtpMessage.timestampToString(destinationTimestamp));
//            Logger.info("Local clock offset: " + Math.abs(localClockOffset)  + " s");
            socket.close();
        } catch (Exception e) {
        	//[Andik] log saya matikan karena bisa membanjiri file lof
//            Logger.error("ntp server "+serverName+" error : "+e.getMessage(), e);
        }
        return result;
    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t)
    {
        after();
    }
}
