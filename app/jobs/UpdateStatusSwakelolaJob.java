package jobs;

import models.agency.Paket_swakelola;
import models.nonlelang.Swakelola_seleksi;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Every;
import play.jobs.Job;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Every("30s")
@NoTransaction
public class UpdateStatusSwakelolaJob extends Job {
    //Job untuk update status paket non transaksional

	private static AtomicBoolean isRunning=new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
		if(isRunning.getAndSet(true))
			return;

    	if("true".equals(System.getProperty("workflow.manager.disabled")))
    		return;
    	try
    	{
	        List<Swakelola_seleksi> swakelolaSeleksiList = Swakelola_seleksi.getBySwakelolaByLewatTanggalSelesai();
	
	        for(Swakelola_seleksi swakelolaSeleksi : swakelolaSeleksiList){
	
	            Paket_swakelola paket = swakelolaSeleksi.getSwakelola();
	
	            paket.pkt_status = Paket_swakelola.StatusPaketSwakelola.SELESAI_LELANG;
	
	            paket.save();
	
	        }
    	}
    	catch(Exception e)
    	{
    		Logger.error("[UpdateStatusNonTransaksionalJob] %s", e);
    	}
    }

	public void after()
	{
		isRunning.set(false);
	}

	public void onException(Throwable t)
	{
		after();
	}
}
