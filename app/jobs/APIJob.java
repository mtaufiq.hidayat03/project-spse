package jobs;

import models.common.ConfigurationDao;
import models.jcommon.blob.BlobTable;
import play.Logger;
import play.jobs.Job;
import play.jobs.On;
import utils.APIUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Compact API agar penarikan data lebih efisien
 *
 * @author Mohamad Irvan Faradian
 */
@On("0 0 2 * * ?") // Dieksekusi setiap jam 02.00 malam
public class APIJob extends Job {

    private static AtomicBoolean isRunning = new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
        if (isRunning.getAndSet(true))
            return;

        int tahun = Calendar.getInstance().get(Calendar.YEAR);

        InputStream in = new ByteArrayInputStream(APIUtil.lelangLengkapJSON(tahun).getBytes(StandardCharsets.UTF_8.name()));

        String baseDir = BlobTable.getFileStorageDir();
        if (baseDir == null)
            throw new IOException(BlobTable.FILE_STORAGE_DIR + " is not defined");

        File parentDir = new File(baseDir + "/API");

        if (!parentDir.exists())
            parentDir.mkdirs();

        File file = new File(parentDir + "/" + tahun + "-" + ConfigurationDao.getRepoId() + ".json");

        // Selalu hapus existing file sebelum generate yang baru
        if (file.exists())
            file.delete();

        FileOutputStream out = new FileOutputStream(file);

        byte buffer[] = new byte[1024 * 10];
        int size;
        while ((size = in.read(buffer)) != -1) {
            out.write(buffer, 0, size);
        }

        out.close();
        in.close();

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Logger.info("File API JSON " + tahun + " berhasil dibuat: " + df.format(date));
    }

    public void after() {
        isRunning.set(false);
    }

    public void onException(Throwable t) {
        after();
    }

}
