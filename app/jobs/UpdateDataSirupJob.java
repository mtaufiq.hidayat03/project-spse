package jobs;

import models.sirup.PaketSirup;
import models.sirup.PaketSwakelolaSirup;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Query;
import play.db.jpa.NoTransaction;
import play.jobs.Every;
import play.jobs.Job;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Arief Ardiyansah
 * update data sirup
 * pada saat job dibuat data sirup sudah ada didalam spse, namun beberapa data kosong diantarany :
 * tanggal awal pengadaan, tanggal akhir pengadaan, tanggal awal pekerjaan , tanggal akhir pekerjaan, tanggal pengumuman dan tanggal kebutuhan
 */
@Every("1mn")
@NoTransaction
public class UpdateDataSirupJob extends Job {

    private static AtomicBoolean isRunning=new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
        if(isRunning.getAndSet(true))
            return;
        // get data paket penyedia
        List<SatkerIdSirup> list = Query.findList("SELECT DISTINCT rup_stk_id, tahun FROM paket_sirup WHERE tanggal_pengumuman is null limit 20", SatkerIdSirup.class); // get data limit 10
        if(!CollectionUtils.isEmpty(list)) {
            list.forEach( obj -> PaketSirup.updatePaketFromSirup(obj.rup_stk_id, obj.tahun));
        }
        // get data paket swakelola
        list = Query.findList("SELECT DISTINCT rup_stk_id, tahun FROM ekontrak.paket_swakelola_sirup WHERE tanggal_pengumuman is null limit 20", SatkerIdSirup.class); // get data limit 10
        if(!CollectionUtils.isEmpty(list)) {
            list.forEach( obj -> PaketSwakelolaSirup.updatePaketFromSirup(obj.rup_stk_id, obj.tahun));
        }
    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t)
    {
        after();
    }

    static class SatkerIdSirup {
        public String rup_stk_id;
        public Integer tahun;
    }
}
