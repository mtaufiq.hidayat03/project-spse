package jobs.devpartner;

import models.devpartner.PesertaDp;
import play.db.jpa.Transactional;
import play.jobs.Every;
import play.jobs.Job;

import java.util.concurrent.atomic.AtomicBoolean;

@Transactional
@Every("10mn")
public class AutoConfirmJvJob extends Job {

    private static AtomicBoolean isRunning=new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
        if(isRunning.getAndSet(true))
            return;

        PesertaDp.autoConfirmJv();
    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t) {
        t.printStackTrace();
        after();
    }
}
