package jobs.adp;

import models.common.ConfigurationDao;
import models.rekanan.Rekanan;
import models.sso.common.RekananSSO;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.jobs.Job;
import play.libs.Json;
import play.libs.WS;

import java.util.List;

/**
 * @author arief ardiyansah
 * Job inisiasi rekanan ke ADP menggunakan mekanisme push
 */

public class InisiasiJob extends Job {

    @Override
    public void doJob() throws Exception {
        List<RekananSSO> list = Rekanan.getListRekananRepo();
        if (CollectionUtils.isEmpty(list))
            return;
        Logger.info("InisiasiJob : Kirim " + list.size() + " penyedia Ke inaproc :");
        for (RekananSSO userRekanan : list) {
            if (userRekanan != null && userRekanan.btu_id != null && userRekanan.btu_id.equals("10")) // vendor asing dilarang masuk ADP Pusat
                continue;
            String userid = userRekanan.rkn_namauser;
            userRekanan.rkn_namauser = Rekanan.clearUsernameADP(userid);
        }
        if (CollectionUtils.isEmpty(list))
            return;
        String content = DceSecurityV2.encrypt(Json.toJson(list));
        WS.WSRequest request = WS.url(ConfigurationDao.getRestCentralService()+"/inisiasi");
        request.setHeader("authorization", Play.secretKey);
        request.setParameter("content", content);
        WS.HttpResponse response = request.postAsync().get();
        if(response.success() && !StringUtils.isEmpty(response.getString()))
            Logger.error("%s", response.getString());
    }
}
