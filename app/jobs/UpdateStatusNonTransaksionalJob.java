package jobs;

import models.agency.Paket_pl;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import play.Logger;
import play.db.jpa.NoTransaction;
import play.jobs.Every;
import play.jobs.Job;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Every("30s")
@NoTransaction
public class UpdateStatusNonTransaksionalJob extends Job {
    //Job untuk update status paket non transaksional

	private static AtomicBoolean isRunning=new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
		if(isRunning.getAndSet(true))
			return;
    	if("true".equals(System.getProperty("workflow.manager.disabled")))
    		return;
    	try
    	{
	        List<Non_spk_seleksi> nonSpkSeleksiList = Non_spk_seleksi.getByNonSpkByLewatTanggalSelesai();
	
	        for(Non_spk_seleksi nonSpkSeleksi : nonSpkSeleksiList){
	
	            Paket_pl paket = nonSpkSeleksi.getPaket();
	
	            paket.pkt_status = Paket_pl.StatusPaket.SELESAI_LELANG;
	
	            paket.save();
	
	        }
    	}
    	catch(Exception e)
    	{
    		Logger.error("[UpdateStatusNonTransaksionalJob] %s", e);
    	}
    }

	public void after()
	{
		isRunning.set(false);
	}

	public void onException(Throwable t)
	{
		after();
	}

}
