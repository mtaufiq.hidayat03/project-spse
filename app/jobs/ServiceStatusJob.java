package jobs;

import ams.clients.AmsClient;
import models.common.ServisStatus;
import play.jobs.Every;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import utils.LogUtil;
import utils.osd.CertificateUtil;
import utils.osd.KmsClient;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author HanusaCloud on 9/12/2018
 */
@OnApplicationStart(async = true)
@Every("5mn")
public class ServiceStatusJob extends Job {

    public static final String TAG = "ServiceUpTime";
    public static Date lastCheck;
    private static AtomicBoolean isRunning=new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
        if(isRunning.getAndSet(true))
            return;
        // initial value , set offline semua
        LogUtil.debug(TAG, "start gathering statuses");
        ServisStatus.adp.check();
        ServisStatus.inaproc.check();
        ServisStatus.sikap.check();
        ServisStatus.sirup.check();
//        ServisStatus.jaim.check();
        if(CertificateUtil.enableCa) {
            LogUtil.debug(TAG, "check ams activity");
            ServisStatus.ams.setStatus(AmsClient.isOnline());
            LogUtil.debug(TAG, "check kms activity");
            ServisStatus.kms.setStatus(KmsClient.isOnline());
        }
        lastCheck = new Date();
    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t)
    {
        after();
    }
}
