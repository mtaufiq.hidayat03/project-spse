package jobs;

import models.common.CONFIG;
import models.jcommon.config.Configuration;
import models.jcommon.util.DateUtil;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;
import play.libs.WS;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author arief.ardiyansah
 * Update pengumuman massal dan html_content dari server pusat (inaproc.lkpp.go.id)
 * dijadwalkan sehari 2 kali
 */
@Every("5s")
public class ContentPusatJob extends Job {

    public static String public_content;
    
    public static String pengumuman_massal;

    private static AtomicBoolean isRunning=new AtomicBoolean();

    @Override
    public void doJob() throws Exception {
        if(isRunning.getAndSet(true))
            return;
        Logger.debug("penarikan pengumuman masal at " + DateUtil.newDate());
        final String url = Configuration.getConfigurationValue(CONFIG.PUBLIC_CONTENT_URL.toString());
        final String url2 = Configuration.getConfigurationValue(CONFIG.PENGUMUMAN_MASSAL_URL.toString());
        try {
            public_content = WS.url(url).getAsync().get().getString();
            pengumuman_massal = WS.url(url2).getAsync().get().getString();
        }catch (Exception e) {
            Logger.debug(e,"penarikan pengumuman masal (%s) at " + DateUtil.newDate(), url);
        }

    }

    public void after()
    {
        isRunning.set(false);
    }

    public void onException(Throwable t)
    {
        after();
    }
}
