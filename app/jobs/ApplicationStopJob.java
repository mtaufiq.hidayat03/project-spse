package jobs;

import ams.clients.AmsAdpClient;
import ams.clients.AmsClient;
import ams.utils.KeyStoreUtil;
import play.jobs.Job;
import play.jobs.OnApplicationStop;
import utils.LogUtil;
import utils.osd.KmsClient;

@OnApplicationStop
public class ApplicationStopJob extends Job {
    @Override
    public void doJob() throws Exception {
        LogUtil.debug("ApplicationStop", "Shutdown");
        if(KeyStoreUtil.isEnabledAmsKms()) {
            AmsClient.clean();
            AmsAdpClient.clean();
            KmsClient.clean();
        }
        JobActiveUser.saveTemporary();
    }
}
