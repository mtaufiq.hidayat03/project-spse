package jobs.migration;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class PgIndex {

	private final String name;
	private final String tableName;
	private final String unique; //
	private final List<String> columns=new ArrayList<>(); //column with ASC or DESC flag -> pkt_nama DESC, lls_id ASC

	public PgIndex(String name, String schema, String tableName, String unique){
		if(unique==null)
			unique="";
		this.name = name;
		this.tableName =  schema+"."+tableName;
		this.unique = unique;
	}

	public String name() {
		return this.name;
	}
	
	public String toString() {
		return String.format("%s %s (%s)",  name, unique, StringUtils.join(columns, ',') );
	}
	
	public boolean equals(Object o)
	{
		PgIndex idx=(PgIndex)o;
		return toString().equals(idx.toString());
	}
	
	public int hashCode()
	{
		return toString().hashCode();
	}

	public void column(String columdef) {
		columns.add(columdef);
	}

	public String create() {
		return "\nCREATE "+unique+" INDEX "+name+" ON "+tableName+" ("+StringUtils.join(columns,',')+");";
	}

	public String delete() {
		return "\nDROP INDEX CONCURRENTLY  "+name+";";
	}
}
