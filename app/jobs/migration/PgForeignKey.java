package jobs.migration;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**Equality dari FK TIDAK ditentukan oleh 'name'
 * tapi ditentukan oleh 'columns' karena bisa berbeda nama
 * 
 * @author Mr. Andik
 *
 */
public class PgForeignKey  {

	private final String name;
	private final String tableName;
	private final String tablePk;
	private final List<String> columns=new ArrayList();
	private final List<String> pkColumns=new ArrayList(); //column in PK (master table)

	public PgForeignKey(String name, String tableName, String tablePk, String column, String pkColumn)
	{
		this.name=name;
		this.tableName=tableName;
		this.tablePk = tablePk;
		addColumn(column, pkColumn);
	}
	
	public void addColumn(String column, String pkColumn)
	{
		columns.add(column);
		pkColumns.add(pkColumn);
	}
//
//	public String toString()
//	{
//		return String.format(" %s FOREIGN KEY (%s) REFERENCES %s (%s)", name, StringUtils.join(columns, ','),
//				tableName, StringUtils.join(pkColumns, ','));
//	}


	
	public boolean equals(Object o)
	{
		PgForeignKey key=(PgForeignKey)o;
		return columns.equals(key.columns) && pkColumns.equals(key.pkColumns);
	}
	
	public int hashCode()
	{
		return (tableName +  StringUtils.join(columns, ',')).hashCode();
	}

	public String name() {
		return this.name;
	}

	public String create() {
		return "\nALTER TABLE "+tableName+" ADD CONSTRAINT "+name+" FOREIGN KEY ("+StringUtils.join(columns, ',')+
				") REFERENCES "+tablePk+" ("+StringUtils.join(pkColumns, ',')+");";
	}

	public String delete() {
        return "\nALTER TABLE " + tableName +" DROP CONSTRAINT " + name() + ';';
    }
}
