package jobs.migration;

import javassist.Modifier;
import models.common.ConfigurationDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.Play;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arief
 * class ini akan melakukan check sequence database
 * terkadang ada case backup-restore database yg efek sampingny sequence reset dengan nilai lebih kecil dan acak
 */
public class DatabaseCheck {

    // check sequence
    public static void sequence() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        int repoid = ConfigurationDao.getRepoId();
        Map<String, Long> mapSequence = new HashMap<>();
        List<Class<?>> list = new ArrayList<>();
        for (Class clazz : Play.classloader.getAllClasses()) {
            if (BaseTable.class.isAssignableFrom(clazz)) {
                list.add(clazz);
            }
        }
        for (Class clazz : list) {
//            Logger.info("checking sequence table %s", clazz.getName());
            Table tableAnn = (Table) clazz.getAnnotation(Table.class);
            if(tableAnn == null)
                continue;
            String table = tableAnn.name();
            // check table exist di database
            String tableOnDB = Query.find("select to_regclass(?)", String.class, table).first();
            if(StringUtils.isEmpty(tableOnDB))
                continue;
            if (clazz.getName().equals("models.rekanan.Rekanan")) {
                Long maxId = Query.find("SELECT max(rkn_id) FROM " + table + " WHERE repo_id=?", Long.class, repoid).first();
                if (maxId == null || maxId < repoid)
                    continue;
                maxId = (maxId - repoid)/1000;
                Long maxIdCurrent = mapSequence.get("seq_rekanan");
                if(maxIdCurrent == null)
                    mapSequence.put("seq_rekanan", maxId);
                else if (maxIdCurrent > 0 && maxIdCurrent < maxId){
                    mapSequence.put("seq_rekanan", maxId);
                }
            } else {
                try {
                    for (Field field : clazz.getFields()) {
                        if (Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers()) || field.isAnnotationPresent(Transient.class))
                            continue;
                        else {
                            Id id = field.getAnnotation(Id.class);
                            if (id != null && !StringUtils.isEmpty(id.sequence())) {
                                Long maxId = Query.find("SELECT max(" + field.getName() + ") FROM " + table, Long.class).first();
                                if (maxId == null)
                                    continue;
                                if(!StringUtils.isEmpty(id.function()) && id.function().equalsIgnoreCase("nextsequence")) {
                                    if (maxId < repoid)
                                        continue;
                                    maxId = (maxId - repoid) / 1000;
                                }
                                Long maxIdCurrent = mapSequence.get(id.sequence());
                                if(maxIdCurrent == null)
                                    mapSequence.put(id.sequence(), maxId);
                                else if (maxIdCurrent > 0 && maxIdCurrent < maxId){
                                    mapSequence.put(id.sequence(), maxId);
                                }
                            }
                        }
                    }
                }catch (Exception e) {
                    Logger.error(e, "[Database check sequence]: %s", e.getMessage());
                }
            }
        }
        // eksekusi sequence yg bermasalah
        mapSequence.forEach((k, v) -> {
            Long currentSequence = Query.find("SELECT last_value FROM " + k, Long.class).first();
            if(!currentSequence.equals(v) && v > 0L) {
                String altersql = "ALTER SEQUENCE " + k + " RESTART WITH " + v;
                Logger.info(altersql);
                Query.update(altersql);
            }
        });
        mapSequence.clear();
        stopWatch.stop();
        Logger.info("[DatabaseCheck]check sequence time : %s", stopWatch.getTime());
    }
}
