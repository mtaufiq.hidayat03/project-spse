package jobs.migration;

import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.util.LargeObjectRemover;
import models.jcommon.util.ObjectMonitor;
import play.Play;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;


public class LargeObjectRemoverLauncher {
	
	public static ObjectMonitor MONITOR=new ObjectMonitor();
	
	//Variable ini digunakan sebagai object monitor
	public final static String LARGE_OBJECT_MOVER_LOG="/logs/migration/large-object-remover.log";
	
	//remove file to a folder
	public String execute() throws ClassNotFoundException, SQLException, IOException
	{
		
		MONITOR.running=true;
		File file=new File(Play.applicationPath + LARGE_OBJECT_MOVER_LOG);
		file.getParentFile().mkdirs();
		PrintStream out=new PrintStream(file);
		LargeObjectRemover lom=new LargeObjectRemover(out, MONITOR);
		String tempFolder=BlobTableDao.getFileStorageDir() + "/spse4-large-object-dump";
		new File(tempFolder).mkdirs();
		String result=lom.execute(true, tempFolder);
		out.close();
		return result;
	}
}
