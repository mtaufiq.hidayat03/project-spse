package jobs.migration;

import models.agency.Agency;
import models.agency.Pegawai;
import models.common.CONFIG;
import models.jcommon.config.Configuration;
import models.jcommon.util.ObjectMonitor;
import models.sso.common.Repository;
import play.Logger;
import play.Play;
import play.db.DB;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;

/**Job ini dieksekusi oleh ApplicationStartJob
 * 
 * @author Mr. Andik
 * Untuk perbaikan foreign key. Tapi tidak jadi digunakan karena dirasa tidak perlu
 *
 */
@Deprecated 
public class DatabaseFKFixing {
	
	public static ObjectMonitor MONITOR;
	
	//Filename to store log file
	public static final String DATABASE_MIGRATION_LOG="/tmp/migration/insert-rows-to-fix-fk-error.log";
	
	private PrintWriter sqlFile;
	
	public DatabaseFKFixing()
	{
		if(MONITOR==null)
			MONITOR=new ObjectMonitor();		
	}
	
	private void notifyMonitor(String message)
	{
		synchronized (MONITOR) {
			MONITOR.status=message;
			MONITOR.notifyAll();
		}
	}
	
	public void execute()
	{
		//allow only once
		if(MONITOR.running)
		{
			Logger.warn("DatabaseMigration Job cannot run twice or more");
			return;
		}
		MONITOR.running=true;

		try {
			File file=new File(Play.applicationPath + DATABASE_MIGRATION_LOG);
			file.getParentFile().mkdirs();
			sqlFile=new PrintWriter(file);
			//DEV bisa dimatikan sementara ketika development agar startup cepat
			Logger.info("[DB MIGRATION] Database Migration");
			sqlFile.append("#### Row berikut ini harus di-insert agar FK untuk tabel tertentu dapat di-create");
			
			notifyMonitor("Perbaikan data Agency untuk relasi ke sub Agency");
			fixRelationAgencyAndSubAgency();
			
			notifyMonitor("Perbaikan data Usrtab untuk relasi ke Pegawai ");
			fixRelationPegawaiAndUserTab();
			
			notifyMonitor("Perbaikan data Repository untuk relasi ke Rekanan");
			fixRelationRekananAndRepository();
			
			//sekarang Insert ke database
			sqlFile.close();
			
			notifyMonitor("Eksekusi SQL in progress..");
			//[DEV] executeSQL();
			notifyMonitor("SELESAI, Migrasi Database Selesai.");
			
			//dan tandai bahwa migrasi telah selesai
			Configuration config=Configuration.getConfiguration(CONFIG.MIGRATION_3_TO_4_DONE.toString());
			config.cfg_value="true";
			config.save();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		MONITOR.running=false;
	
	}	
	
	private void executeSQL() throws Exception {
		LineNumberReader reader=new LineNumberReader(new InputStreamReader(new FileInputStream(Play.applicationPath + DATABASE_MIGRATION_LOG)));
		String str;
	//	DB.datasource.getConnection().setAutoCommit(false);
		while((str=reader.readLine())!=null)
		{
			Logger.debug("EXECUTE SQL: %s", str);
			if(!str.startsWith("#"))
				DB.execute(str);
		}
		//DB.datasource.getConnection().commit();
		reader.close(); 		
	}

	private void fixRelationRekananAndRepository() throws SQLException {
		ResultSet rs=DB.executeQuery("select distinct repo_id from rekanan");
		while(rs.next())
		{
			Long repo_id=rs.getLong(1);
			if(Repository.findById(repo_id)==null)
				sqlFile.format("INSERT INTO REPOSITORY(repo_id, repo_nama, repo_url) VALUES(%s, '@REPO_NAMA@','@REPO_URL@');\n", repo_id);
		}
		rs.close();
		sqlFile.flush();	
	}

	//cari peg_nip yang ada di Pegawai namun tidak ada di UserTab
	private void fixRelationPegawaiAndUserTab() throws SQLException {
		
		ResultSet rs=DB.executeQuery("select distinct peg_namauser from pegawai");		
		while(rs.next())
		{
			String peg_namauser=rs.getString(1);
			if(Pegawai.findBy(peg_namauser)==null)
				sqlFile.format("INSERT INTO USRTAB(USERID, USERNAME, EMAIL, PASSW, MOBILE1) VALUES('%s', '@USERNAME@','@EMAIL@','@PASSW@','@MOBILE1@');\n", peg_namauser);
		}
		rs.close();
		sqlFile.flush();
	}

	//perbaiki error ini: insert or update on table "agency" violates foreign key constraint "fk72f41005aaf7c826" Detail: Key (sub_agc_id)=(0) is not present in table "agency".
	private void fixRelationAgencyAndSubAgency() throws SQLException
	{
		//harus pakai JDBC karena JPA bisa error ketika sub_agc tidak ditemmukan
		ResultSet res=DB.executeQuery("select distinct sub_agc_id from agency");
		while(res.next())
		{
			Long agc_id=res.getLong(1);
			try
			{
				Agency.findById(agc_id);
			}
			catch(javax.persistence.EntityNotFoundException e)
			{
				/* AGC not found, so create SQL to insert
				JNA_ID diset 6 (Lainnya)
				*/
				sqlFile.format("INSERT INTO AGENCY(AGC_ID, AGC_NAMA, AGC_ALAMAT, AGC_TELEPON, AGC_TGL_DAFTAR, JNA_ID) " +
						"VALUES(%s, '@AGC_NAMA@','@AGC_ALAMAT@','@AGC_TELEPON@', current_timestamp, 6);\n", agc_id);
			}
		}
		res.close();
		sqlFile.flush();
	}
}
