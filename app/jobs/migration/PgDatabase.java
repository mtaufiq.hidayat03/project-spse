package jobs.migration;

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.util.*;

public class PgDatabase {

    List<Schema> schemaList = new ArrayList<>();


    public static void toJson(Connection connection, String pathfile) {
        try {
            PgDatabase obj = new PgDatabase();
            DatabaseMetaData md = connection.getMetaData();
            //informasi skema
            ResultSet rs = md.getSchemas();
            while (rs.next()) {
                Schema schema = new Schema();
                schema.name = rs.getString(1);    // "TABLE_SCHEMA"
                if (schema.name.equalsIgnoreCase("information_schema") || schema.name.equalsIgnoreCase("pg_catalog"))
                    continue;
                //informasi sequence
                ResultSet rstbl = md.getTables(null, schema.name, null, new String[]{"SEQUENCE"});
                while (rstbl.next()) {
                    String sequence = rstbl.getString("TABLE_NAME");
//                    if(!StringUtils.isEmpty(schema.name))
//                        sequence = schema+"."+sequence;
                    schema.sequences.add(sequence);
                }
                rstbl.close();
                //informasi table
                rstbl = md.getTables(null, schema.name, null, new String[]{"TABLE"});
                while (rstbl.next()) {
                    Table table = new Table();
                    table.name = rstbl.getString("TABLE_NAME");
                    // dapatkan informasi kolom
                    ResultSet rstblInfo = md.getColumns(null, schema.name, table.name, null);
                    while (rstblInfo.next()) {
                        Column column = new Column();
                        column.name = rs.getString("COLUMN_NAME");
                        column.typeName = rs.getString("TYPE_NAME");
                        int columnSize = rs.getInt("COLUMN_SIZE");
                        /* Kadang2 columnSize tidak akurat Untuk numeric perlu dibatasi 64 */
                        if(column.typeName.equals("numeric") && columnSize > 1000)
                            columnSize=64;
                        column.columnSize = columnSize;
                        column.decimalDigits = rs.getInt("DECIMAL_DIGITS");
                        String nullable ="NOT NULL";
                        if(rs.getInt("NULLABLE")== ResultSetMetaData.columnNullable)
                            nullable="NULL";
                        column.nullable = nullable;
                        column.columnDef = rs.getString("COLUMN_DEF");
                        table.columns.add(column);
                    }
                    rstblInfo.close();
                    // dapatkan informasi Foreign key
                    rstblInfo=md.getImportedKeys(null, schema.name, table.name);
                    Map<String, ForeginKey> mapFk=new HashMap<>();
                    while(rstblInfo.next())
                    {
                        String fkName=rstblInfo.getString("FK_NAME");
                        ForeginKey fk=mapFk.get(fkName);
                        String refColumn=rstblInfo.getString("FKCOLUMN_NAME");
                        String pkColumn=rstblInfo.getString("PKCOLUMN_NAME");
                        if(fk==null)
                        {
                            String pkTable=rstblInfo.getString("PKTABLE_NAME");
                            String pkSchema = rstblInfo.getString("PKTABLE_SCHEM");
                            fk=new ForeginKey();
                            fk.name=fkName;
                            fk.tableName=table.name;
                            fk.tablePk = pkTable;
                            fk.columns.add(refColumn);
                            fk.pkColumns.add(pkColumn);
                            mapFk.put(fkName, fk);
                        }
                        else {
                            fk.columns.add(refColumn);
                            fk.pkColumns.add(pkColumn);
                        }
                    }
                    rstblInfo.close();
                    table.foreignKeys.addAll(mapFk.values());
                    // dapatkan informasi primary key dan index
                    rstblInfo = md.getPrimaryKeys(null, schema.name, table.name);
                    while(rstblInfo.next())
                    {
                        table.primaryKey.columns.add(rstblInfo.getString("COLUMN_NAME"));
                        table.primaryKey.name = rstblInfo.getString("PK_NAME");//nama constraint untuk PK
                    }
                    rstblInfo.close();
                    // dapatkan informasi index
                    rstblInfo=md.getIndexInfo(null, schema.name, table.name, false, false);
                    Map<String, Index> mapIndex=new HashMap<>();
                    while(rstblInfo.next())
                    {
                        String indexName=rstblInfo.getString("INDEX_NAME"); //maybe null
                        if(indexName !=null && table.primaryKey.name != null && !table.primaryKey.name.equals(indexName)) //index ini bukan PK jadi tambahkan
                        {
                            Index index=mapIndex.get(indexName);
                            if(index==null)
                            {
                                Index idx = new Index();
                                boolean nonUnique=rstblInfo.getBoolean("NON_UNIQUE");
                                idx.name = indexName;
                                idx.tableName =  table.name;
                                idx.unique = !nonUnique ? "UNIQUE":"";
                                mapIndex.put(indexName, index);
                            }
                            String column=rstblInfo.getString("COLUMN_NAME");
                            String ascOrDesc=rstblInfo.getString("ASC_OR_DESC");
                            if(ascOrDesc.equals("D"))
                                ascOrDesc=" DESC";
                            else
                                ascOrDesc=" ASC";
                            index.columns.add(column + ascOrDesc);
                        }
                    }
                    table.indexes.addAll(mapIndex.values());
                    rstblInfo.close();
                    schema.tables.add(table);
                }
                rstbl.close();
                obj.schemaList.add(schema);
            }
            rs.close();
            File baseSkema = new File(pathfile);
            FileWriter writer = new FileWriter(baseSkema);
            writer.write(new Gson().toJson(obj));
            writer.flush();
            writer.close();
            Logger.info("generate selesai");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String compare(Connection connection) {
        StringBuilder result = new StringBuilder();
        try {
            DatabaseMetaData md = connection.getMetaData();
            ResultSet rs;
            // compare schema
            for (Schema schema : schemaList) {
                rs = md.getSchemas(null, schema.name);
                while (rs.next()) {
                    String schemaName = rs.getString("TABLE_SCHEM");    // "TABLE_SCHEMA"
                    if (StringUtils.isEmpty(schemaName))
                        result.append(schema.create());
                }
                rs.close();
                // compare sequence
                for (String sequence : schema.sequences) {
                    rs = md.getTables(null, schema.name, sequence, new String[]{"SEQUENCE"});
                    while (rs.next()) {
                        String seq = rs.getString("TABLE_NAME");
                        if (StringUtils.isEmpty(seq))
                            result.append(schema.createSequence(seq));
                    }
                    rs.close();
                }
                //compare table
                for (Table table : schema.tables) {
                    rs = md.getTables(null, schema.name, table.name, new String[]{"TABLE"});
                    boolean createTable = false;
                    while (rs.next()) {
                        String tableName = rs.getString("TABLE_NAME");
                        if (StringUtils.isEmpty(tableName)) {
                            result.append(table.create());
                            createTable = true;
                        }
                    }
                    rs.close();
                    if(!createTable) {
                        //#1 compare column
                        for (Column column : table.columns) {
                            // dapatkan informasi kolom
                            rs = md.getColumns(null, schema.name, table.name, column.name);
                            while (rs.next()) {
                                String columnName = rs.getString("COLUMN_NAME");
                                String typeName = rs.getString("TYPE_NAME");
                                String columnDef = rs.getString("COLUMN_DEF");
                                int columnSize = rs.getInt("COLUMN_SIZE");
                                /* Kadang2 columnSize tidak akurat Untuk numeric perlu dibatasi 64 */
                                if(typeName.equals("numeric") && columnSize > 1000)
                                    columnSize=64;
                                String nullable ="NOT NULL";
                                if(rs.getInt("NULLABLE")== ResultSetMetaData.columnNullable)
                                    nullable="NULL";
                                if (StringUtils.isEmpty(columnName)) { // column tidak ada
                                    result.append("\nALTER TABLE " + table.name +" ADD COLUMN " + column.toString() +';');
                                }else {
                                    //Diperiksa atas kesamaan NAMA dan TYPE
                                    if(!(columnName.equals(column.name) && typeName.equals(column.typeName) && columnSize == column.columnSize)) {
                                        result.append("\nALTER TABLE " + table.name +" ALTER COLUMN " + column.name +" TYPE " + column.getDataType() +';');
                                    }
                                    //#1 SPSE 4 is NULL but SPSE 3 NOT NULL
                                    if(nullable.equals("NOT NULL") && column.nullable.equals("NULL")){
                                        result.append("\nALTER TABLE " + column.name + " ALTER COLUMN " + column.name +" DROP NOT NULL;");
                                    }
                                    //#2 SPSE 4 is NOT-NULL but SPSE 3 NULL
                                    if(column.nullable.equals("NOT NULL") && nullable.equals("NULL") )
                                    {
                                        result.append("\nALTER TABLE "+column.name + " ALTER COLUMN " +column.name +" SET NOT NULL;");
                                    }
                                    //default value
                                    //1 terkait default: jika ada DEFAULT di versi 3 dan tidak ada di SPSE 4 maka DROP
                                    if(columnDef!=null && columnDef.contains("DEFAULT") && !column.hasDefault())
                                    {
                                        result.append("\nALTER TABLE "+column.name+" ALTER COLUMN "+column.name+" DROP DEFAULT;");
                                    }
                                    //2 terkait default: jika ada DEFAULT di versi 4 dan tidak ada di SPSE 3 maka CREATE DEFAULT
                                    if(column.hasDefault()&& columnDef!=null && !column.columnDef.equals(columnDef))
                                    {
                                        result.append("\nALTER TABLE "+column.name+" ALTER COLUMN "+column.name+" SET DEFAULT "+column.columnDef+";");
                                    }
                                }
                            }
                            rs.close();
                        }
                        //#2 compare PK

                        //#3 compare FK dilakukan terpisah

                        //#4 compare indexes
                    }
                }
            }
        } catch (Exception e) {
            Logger.error(e, "terjadi Kendala saat compare db");
        }
        return result.toString();
    }

    // delete all check constraint
    public void deleteAllCheckContrainst(Connection connection, PrintWriter logWriter) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select conrelid::regclass AS table_from, conname from  pg_constraint c join  pg_namespace n ON n.oid = c.connamespace \n" +
                    "where  nspname='public' and contype in ('c') order by conrelid::regclass");
            ResultSet rs = preparedStatement.executeQuery();
            StringBuilder dropScript = new StringBuilder();
            while(rs.next()) {
                String table = rs.getString("table_from");
                String checkName = rs.getString("conname");
                dropScript.append("ALTER TABLE ").append(table).append(" DROP CONSTRAINT \"").append(checkName).append("\";");
            }
            preparedStatement.close();
            rs.close();
            if(dropScript.length() > 0) {
                Statement st = connection.createStatement();
                executeSQL(st, dropScript.toString(), logWriter);
                st.close();
            }
            Logger.info("delete constraint check success");
        }catch (Exception e) {
            Logger.error("delete constraint check gagal");
        }
    }

    // delete all view
    public void deleteAllviews(Connection connection, PrintWriter logWriter) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select table_name from INFORMATION_SCHEMA.views WHERE table_schema ='public'");
            ResultSet rs = preparedStatement.executeQuery();
            StringBuilder dropScript = new StringBuilder();
            while(rs.next()) {
                String view = rs.getString("table_name");
                dropScript.append("DROP VIEW ").append(view).append(" CASCADE;");
            }
            preparedStatement.close();
            rs.close();
            if(dropScript.length() > 0) {
                Statement st = connection.createStatement();
                executeSQL(st, dropScript.toString(), logWriter);
                st.close();
            }
            Logger.info("delete view success");
        }catch (Exception e) {
            Logger.error("delete view gagal");
        }
    }

    public void executeSQL(Statement st, String sqlmulti, PrintWriter logWriter) {
        if(StringUtils.isEmpty(sqlmulti))
            return;
        int i=0;
//		long start = System.currentTimeMillis();
        StopWatch stopWatch = new StopWatch();
        String[] sqlSplit = sqlmulti.split(";");
        for (String sql: sqlSplit)
        {
            if(StringUtils.isEmpty(sql))
                continue;
            logWriter.println("\nExecute : " + sql);
            stopWatch.start();
            try	{
                st.executeUpdate(sql);
                logWriter.print("[OK]\n");
            } catch(Exception e){
                String str=String.format("\n#### %s %s\n", ++i, sql);
                logWriter.print(str);
                str=String.format("[ERROR] %s\n", e.getMessage());
                logWriter.print(str);

            }
            stopWatch.stop();
            logWriter.println("time execute :"+stopWatch.getTime());
            stopWatch.reset();
        }
    }

    public static class Schema {
        public String name;
        public List<Table> tables = new ArrayList<>();
        public List<String> sequences = new ArrayList<>();

        public String create() {
            return "CREATE SCHEMA " +name;
        }

        public String createSequence(String sequenceName) {
            return "CREATE SEQUENCE " +sequenceName+" INCREMENT 1 MINVALUE 1 CACHE 1;";
        }
    }

    public static class Table {
        public String name;
        public PrimaryKey primaryKey;
        public Set<ForeginKey> foreignKeys = new HashSet<>();
        public Set<Column> columns=new HashSet<>();
        public List<Index> indexes=new ArrayList<>();

        /**get DDL to Create Table except Foreign key
         *
         * @return
         */
        public String create()
        {
            StringBuilder str=new StringBuilder();
            str.append("\n\nCREATE TABLE ").append(name).append("\n(");
            //1 Create columns
            int i=0;
            for(Column column: columns)
            {
                if(i>0)
                    str.append("\n,");
                str.append(column.toString());
                i++;
            }

            str.append(");");
            //2 Create PK
            if(primaryKey != null && primaryKey.columns.size()>0)
            {
                str.append("\nALTER TABLE ").append(name).append(" ADD CONSTRAINT ").append(getPrimaryKey()).append(';');
            }

            //3 Create FK tidak dijalankan di sini
            for(ForeginKey pgNewPk: foreignKeys)
            {
                if(!foreignKeys.contains(pgNewPk))
                {
                    str.append(pgNewPk.create());
                }
            }

            //4 Create Index
            for(Index index: indexes)
            {
//			ddlCommands.add(index.create());
                str.append(index.create());
            }
            return str.toString();
        }

        public String getPrimaryKey() {
            StringBuilder str=new StringBuilder();
            if(primaryKey != null) {
                str.append(primaryKey.name).append(" PRIMARY KEY ").append('(');
                for (int i = 0; i < primaryKey.columns.size(); i++) {
                    if (i > 0)
                        str.append(',');
                    str.append(primaryKey.columns.get(i));
                }
                str.append(')');
            }
            return str.toString();
        }
    }

    public static class PrimaryKey {
        public String name;
        public List<String> columns=new ArrayList<>();
    }

    public static class ForeginKey {
        public String name;
        public String tableName;
        public String tablePk;
        public List<String> columns=new ArrayList();
        public List<String> pkColumns=new ArrayList(); //column in PK (master table)

        public String create() {
            return "\nALTER TABLE "+tableName+" ADD CONSTRAINT "+name+" FOREIGN KEY ("+StringUtils.join(columns, ',')+
                    ") REFERENCES "+tablePk+" ("+StringUtils.join(pkColumns, ',')+");";
        }

        public String delete() {
            return "\nALTER TABLE " + tableName +" DROP CONSTRAINT " + name + ';';
        }
    }

    public static class Column {
        public String name;
        public String typeName; //see DDL data type
        public int columnSize;
        public int decimalDigits;
        public String nullable;
        public String columnDef;

        public String toString() {
            return name+' '+getDataType()+' '+nullable+' '+(columnDef!=null?" DEFAULT "+columnDef:"");
        }

        public boolean hasDefault()
        {
            return columnDef!=null && columnDef.contains("DEFAULT");
        }

        public String getDataType()
        {
            if(typeName.equals("numeric")) {
                if(decimalDigits>0)
                    return typeName+'('+columnSize+','+decimalDigits+')';
                return typeName+'('+columnSize+')';
            }
            else if(typeName.equals("varchar")) {
                if(columnSize > 1000)
                    return typeName;
                return typeName+'('+columnSize+')';
            }
            else if(typeName.equals("bpchar") || typeName.equals("char")) {
                return typeName+'('+columnSize+')';
            }
            return typeName;
        }

    }

    public static class Index {
        public String name;
        public String tableName;
        public String unique; //
        public List<String> columns=new ArrayList<>(); //column with ASC or DESC flag -> pkt_nama DESC, lls_id ASC

        public String create() {
            return "\nCREATE "+unique+" INDEX "+name+" ON "+tableName+" ("+StringUtils.join(columns,',')+");";
        }

        public String delete() {
            return "\nDROP INDEX CONCURRENTLY "+name+";";
        }

    }
}
