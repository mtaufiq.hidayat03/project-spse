package jobs.migration;

import models.common.ConfigurationDao;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.PlayPlugin;
import play.db.DB;
import play.db.DBPlugin;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DatabaseMigration {

	final List<String> SQL_UPDATE_URL = Arrays.asList(
			"https://gitlab.lkpp.go.id/api/v4/projects/128/repository/files/spse-4.3.ddl.json/raw?ref=master",
			"https://gitlab.lkpp.go.id/api/v4/projects/128/repository/files/spse-4.function.sql/raw?ref=master",
			"https://gitlab.lkpp.go.id/api/v4/projects/128/repository/files/spse-update-3.6-migrasi-4.2.sql/raw?ref=master",
			"https://gitlab.lkpp.go.id/api/v4/projects/128/repository/files/spse-update-4.2-to-4.3.sql/raw?ref=master");
	final List<MigrationFile> fileMigrationList = new ArrayList<>();

	/**
	 * Lakukan uji coba tanpa melalui Play
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws SQLException
	 */
	public static void main(String[] args) throws Exception {
		DatabaseMigration db = new DatabaseMigration();
		Play.init(new File("."), "");
		PlayPlugin plugin = Play.plugin(DBPlugin.class);
		plugin.onApplicationStart();
		if("true".equals(System.getProperty("migration.generateDDL", "false"))){
			db.generateJsonSchema();
		} else {
			db.check();
			if(db.isNeedMigration()) {
				db.doMigration();
			}
			if(db.isNeeadBackup())
				System.out.println("Harap Dilakukan Backup Database SPSE Terlebih dahulu");
		}
		plugin.onApplicationStop();
		System.exit(1);
	}

	// khusus development bisa generate base skema ddl
	void generateJsonSchema() throws Exception {
		Connection connection = DB.getConnection();
		PgSchema schema = new PgSchema(connection);
		schema.generateToJson("db/spse-4.3.ddl.json");
	}

	/**
	 * Lakukan migrasi
	 */
	public void doMigration() {
		Logger.info("Proses migrasi SPSE " + ConfigurationDao.SPSE_VERSION + " Sedang Dilakukan.....");
		String logFile = Play.applicationPath + "/logs/DB.migration." + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + ".log";
		File fileLog = new File(logFile);
		fileLog.getParentFile().mkdirs();
		try (PrintWriter logWriter = new PrintWriter(fileLog)) {
			logWriter.print("\nLog file: " + logFile);
			logWriter.print("\n*********** Executing SQL.... ***********\n");
			Connection connection = DB.getConnection();
			for (MigrationFile migrationFile : fileMigrationList) 
				migrationFile.execute(connection, logWriter);
			Logger.info("Proses migrasi telah selesai dilakukan");
		}catch (Exception e) {
			Logger.error(e, "proses migration gagal, mohon ulangi kembali ");
		}
	}

	public void check() {
		java.security.Security.removeProvider("OracleUcrypto");
		for (String url : SQL_UPDATE_URL) {
			try {
				WSRequest request = WS.url(url);
				HttpResponse response = request.setHeader("Private-Token", "rxbBBq3DKKzpnGhLdN7a").timeout("5mn").get();
				if(response.success() && !StringUtils.isEmpty(response.getString())) {
					MigrationFile migrationFile = new MigrationFile();
					String[] slice = url.split("/");
					migrationFile.filename = slice[slice.length - 2];
					migrationFile.content = response.getString();
					migrationFile.hash = DigestUtils.md5Hex(migrationFile.content);
					migrationFile.change = true;
					File file = new File(Play.applicationPath + "/logs/" + migrationFile.filename);
					if (file.exists()) {
						try {
							String hash = FileUtils.readFileToString(file, "UTF-8");
							migrationFile.change = !migrationFile.hash.equals(hash);
						}catch (Exception e) {
							Logger.error(e, "kendala membaca file %s", file);
						}
					}
					fileMigrationList.add(migrationFile);
				}
			} catch (Exception e) {
				Logger.error("Gagal ambil file di %s, Harap Check Status Koneksi", url);
			}
		}
	}

	public boolean isNeeadBackup() {
		boolean dllchange=false, updatesqlChange=false;
		for(MigrationFile migrationFile:fileMigrationList) {
			if(migrationFile.change) {
				if(migrationFile.isDDl())
					dllchange = true;
				if(migrationFile.isUpdateSql())
					updatesqlChange = true;
			}
		}
		return dllchange || updatesqlChange;
	}

	public boolean isNeedMigration() {
		for(MigrationFile migrationFile:fileMigrationList) {
			if(migrationFile.change) {
				return true;
			}
		}
		return false;
	}

	static class MigrationFile {
		public String filename;
		public String content;
		public String hash;
		public boolean change;

		public boolean isDDl() {
			return filename.endsWith(".json");
		}

		public boolean isFunction() {
			return filename.endsWith("function.sql");
		}

		public boolean isUpdateSql(){
			return filename.endsWith(".sql");
		}

		public void execute(Connection connection, PrintWriter logWriter) throws Exception {
			if (change) {
				if (isDDl()) {
					PgSchema schemaBase = PgSchema.fromJson(content);
					PgSchema schemaLPSE = new PgSchema(connection);
					if (schemaBase != null) {
						// step 1 : compare dan execute skema
						String sqlCompareSkema = schemaBase.compareSchema(schemaLPSE);
						if (!StringUtils.isEmpty(sqlCompareSkema)) {
							String[] sqlSplit = sqlCompareSkema.split(";");
							PgSchema.executeSQL(connection, sqlSplit, logWriter);
						}
					}
					// step 2 : delete all check
					PgSchema.deleteAllCheckContrainst(connection, logWriter);
					// step 3 : delete view
					PgSchema.deleteAllviews(connection, logWriter);
					String sqlCompareTable = schemaBase.compareTable(schemaLPSE);
					if(!StringUtils.isEmpty(sqlCompareTable)) {
						String[] sqlSplit = sqlCompareTable.split(";");
						PgSchema.executeSQL(connection, sqlSplit, logWriter);
					}
				}
				else if (isFunction()) {
					PgSchema.executeSQL(connection, content, logWriter);
				}
				// step 5 : compare dan execute Table beserta script update
				else if (isUpdateSql()) {
					String[] sqlSplit = content.split(";");
					PgSchema.executeSQL(connection, sqlSplit, logWriter);
				}
				// write hash to file
				FileUtils.write(new File(Play.applicationPath+"/logs/"+filename), hash, "UTF-8");
			}
		}
	}
}
