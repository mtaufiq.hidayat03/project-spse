package jobs.migration;

import com.google.gson.Gson;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * schema adalah root dari component database
 * di dalam schema terdapat sequence dan table
 */
public class PgSchema {

    private final Map<String,String> mapSchema = new HashMap<>();
    private final Map<String,PgTable> mapTable = new HashMap<>();
    private final Map<String,PgSequence> mapSequence = new HashMap<>();

    public PgSchema(Connection connection) throws Exception {
        DatabaseMetaData md = connection.getMetaData();
        //informasi skema
        try(ResultSet rs = md.getSchemas()) {
            while (rs.next()) {
                String schema = rs.getString(1);    // "TABLE_SCHEMA"
                if (!(schema.equalsIgnoreCase("public") || schema.equalsIgnoreCase("ekontrak")))
                    continue;
                mapSchema.put(schema, schema);
                //informasi sequence
                try(ResultSet rstbl = md.getTables(null, schema, null, new String[]{"SEQUENCE"})) {
                    while (rstbl.next()) {
                        String sequence = rstbl.getString("TABLE_NAME");
                        if (!StringUtils.isEmpty(schema))
                            sequence = schema + "." + sequence;
                        PgSequence pgSequence = new PgSequence(sequence);
                        mapSequence.put(sequence, pgSequence);
                    }
                }
                //informasi table
                try(ResultSet rstbl = md.getTables(null, schema, null, new String[]{"TABLE"})) {
                    while (rstbl.next()) {
                        String table = rstbl.getString("TABLE_NAME");
                        PgTable pgTable = new PgTable(schema, table);
                        // dapatkan informasi kolom
                        try(ResultSet rstblInfo = md.getColumns(null, schema, table, null)) {
                            while (rstblInfo.next()) {
                                pgTable.column(new PgColumn(rstblInfo));
                            }
                        }
                        // dapatkan informasi Foreign key
                        Map<String, PgForeignKey> mapFk = new HashMap<>();
                        try(ResultSet rstblInfo = md.getImportedKeys(null, schema, table)) {
                            while (rstblInfo.next()) {
                                String fkName = rstblInfo.getString("FK_NAME");
                                PgForeignKey fk = mapFk.get(fkName);
                                String refColumn = rstblInfo.getString("FKCOLUMN_NAME");
                                String pkColumn = rstblInfo.getString("PKCOLUMN_NAME");
                                if (fk == null) {
                                    String pkTable = rstblInfo.getString("PKTABLE_NAME");
                                    String pkSchema = rstblInfo.getString("PKTABLE_SCHEM");
                                    fk = new PgForeignKey(fkName, schema + "." + table, pkSchema + "." + pkTable, refColumn, pkColumn);
                                    mapFk.put(fkName, fk);
                                } else
                                    fk.addColumn(refColumn, pkColumn);
                            }
                        }
                        pgTable.foreignKeys(mapFk.values());
                        // dapatkan informasi primary key dan index
                        try(ResultSet rstblInfo = md.getPrimaryKeys(null, schema, table)) {
                            while (rstblInfo.next()) {
                                pgTable.primaryKeyColumns(rstblInfo.getString("COLUMN_NAME"));
                                pgTable.primaryKeyName(rstblInfo.getString("PK_NAME"));//nama constraint untuk PK
                            }
                        }
                        // dapatkan informasi index
                        try(ResultSet rstblInfo = md.getIndexInfo(null, schema, table, false, false)) {
                            Map<String, PgIndex> mapIndex = new HashMap<>();
                            while (rstblInfo.next()) {
                                String indexName = rstblInfo.getString("INDEX_NAME"); //maybe null
                                if (indexName != null && pgTable.primaryKeyName() != null && !pgTable.primaryKeyName().equals(indexName)) //index ini bukan PK jadi tambahkan
                                {
                                    PgIndex index = mapIndex.get(indexName);
                                    if (index == null) {
                                        boolean nonUnique = rstblInfo.getBoolean("NON_UNIQUE");
                                        if (!nonUnique)
                                            index = new PgIndex(indexName, schema, table, "UNIQUE");
                                        else
                                            index = new PgIndex(indexName, schema, table, null);
                                        mapIndex.put(indexName, index);
                                    }
                                    String column = rstblInfo.getString("COLUMN_NAME");
                                    String ascOrDesc = rstblInfo.getString("ASC_OR_DESC");
                                    if (!StringUtils.isEmpty(ascOrDesc) && ascOrDesc.equals("D"))
                                        ascOrDesc = " DESC";
                                    else
                                        ascOrDesc = " ASC";
                                    index.column(column + ascOrDesc);
                                }
                            }
                            pgTable.indexes(mapIndex.values());
                        }
                        mapTable.put(pgTable.name(), pgTable);
                    }
                }
            }
        }
    }

    public static PgSchema fromJson(String json) throws Exception {
        return new Gson().fromJson(json, PgSchema.class);
    }

    public void generateToJson(String pathfile) {
        File baseSkema = new File(pathfile);
        try (FileWriter writer = new FileWriter(baseSkema)){
            writer.write(new Gson().toJson(this));
            writer.flush();
            Logger.info("generate selesai");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String compareSchema(PgSchema schema) {
        StringBuilder result = new StringBuilder();
        if(MapUtils.isEmpty(mapSchema))
            return result.toString();
        if(mapSchema != null && schema != null && schema.mapSchema != null) {
            for (Map.Entry<String, String> entry : mapSchema.entrySet()) {
                String schema3 = schema.mapSchema.get((String) entry.getKey());
//            String schema4 = entry.getValue();
                if (schema3 == null) {
                    result.append("CREATE SCHEMA ekontrak;");
                    result.append("\nALTER SCHEMA ekontrak OWNER TO epns;");
                }
            }
        }
        return result.toString();
    }

    /**
     * compare dengan schema lain, output script perbedaan
     * compare DDL base dan DDL DB LPSE
     */
    public String compareTable(PgSchema schema, String... sqlUpdate) {
        StringBuilder result = new StringBuilder();
        if(MapUtils.isEmpty(mapSequence))
            return result.toString();
        //compare sequence
        for(Map.Entry<String, PgSequence> entry : mapSequence.entrySet()) {
            PgSequence pgSequence3 = schema.mapSequence.get((String) entry.getKey());
            PgSequence pgSequence4 = entry.getValue();
            if(pgSequence3 == null) {
                result.append(pgSequence4.create());
            }
        }
        if(MapUtils.isEmpty(mapTable))
            return result.toString();
        //compare table
        for(Map.Entry<String, PgTable> entry : mapTable.entrySet())
        {
            PgTable pgTable3= schema.mapTable.get((String) entry.getKey());
            PgTable pgTable4= entry.getValue();
            //tidak ada di SPSE 3
            if(pgTable3==null)
            {
                result.append(pgTable4.create());
            }
            //ada di SPSE 3 tapi mungkin beda struktur
            else
            {
                //compare table DDL
                result.append(pgTable3.compareTable(pgTable4));
            }
        }
        //compare FK
        //FK dimasukkan ke list DDL terpisah
        for(Map.Entry<String, PgTable> entry : mapTable.entrySet())
        {
            PgTable pgTable3= schema.mapTable.get((String) entry.getKey());
            PgTable pgTable4= entry.getValue();
            if(pgTable3 != null)
                result.append(pgTable3.alterForeignKey(pgTable4));
        }
        if(!ArrayUtils.isEmpty(sqlUpdate)) {
            for(String sqltambahan:sqlUpdate)
                result.append(sqltambahan);
        }
        return result.toString();
    }

    // delete all check constraint
    public static void deleteAllCheckContrainst(Connection connection, PrintWriter logWriter) {
        try (PreparedStatement preparedStatement = connection.prepareStatement("select conrelid::regclass AS table_from, conname from  pg_constraint c join  pg_namespace n ON n.oid = c.connamespace \n" +
                "where  nspname='public' and contype in ('c') order by conrelid::regclass")){
            StringBuilder dropScript = new StringBuilder();
            try(ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    String table = rs.getString("table_from");
                    String checkName = rs.getString("conname");
                    dropScript.append("ALTER TABLE ").append(table).append(" DROP CONSTRAINT \"").append(checkName).append("\";");
                }
            }
            if(dropScript.length() > 0) {
                executeSQL(connection, dropScript.toString(), logWriter);
                Logger.info("delete constraint check success");
            }
        }catch (Exception e) {
            Logger.error("delete constraint check gagal");
        }
    }

    // delete all view
    public static void deleteAllviews(Connection connection, PrintWriter logWriter) {
        try (PreparedStatement preparedStatement = connection.prepareStatement("select table_name from INFORMATION_SCHEMA.views WHERE table_schema in ('public', 'ekontrak')")){
            StringBuilder dropScript = new StringBuilder();
            try(ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    String view = rs.getString("table_name");
                    dropScript.append("DROP VIEW \"").append(view).append("\" CASCADE;");
                }
            }
            if(dropScript.length() > 0) {
                executeSQL(connection, dropScript.toString(), logWriter);
                Logger.info("delete view success");
            }
        }catch (Exception e) {
            Logger.error("delete view gagal");
        }
    }

    // execute DDL or DML with multi query
    public static void executeSQL(Connection connection, String[] sqlmulti, PrintWriter logWriter) {
        if(ArrayUtils.isEmpty(sqlmulti))
            return;
		int i=0;
		StopWatch stopWatch = new StopWatch();
        try(Statement st = connection.createStatement()) {
            for (String sql : sqlmulti) {
                if (StringUtils.isEmpty(sql))
                    continue;
                logWriter.println("\nExecute : " + sql);
                Logger.info("Execute : %s", sql);
                stopWatch.start();
                try {
                    st.executeUpdate(sql);
                    logWriter.print("[OK]\n");
                    Logger.info("[OK]\n");
                } catch (Exception e) {
                    String str = String.format("\n#### %s %s\n", ++i, sql);
                    logWriter.print(str);
                    str = String.format("[ERROR] %s\n", e.getMessage());
                    logWriter.print(str);
                    Logger.info("[ERROR] %s\n", e.getMessage());
                }
                stopWatch.stop();
                logWriter.println("time execute :" + stopWatch.getTime());
                Logger.info("time execute : %s", stopWatch.getTime());
                stopWatch.reset();
            }
        }catch (SQLException e) {
            Logger.error(e, "executeSQL gagal");
        }
    }

    // execute function / trigger
    public static void executeSQL(Connection connection, String sql, PrintWriter logWriter) {
        if(StringUtils.isEmpty(sql))
            return;
        int i=0;
        StopWatch stopWatch = new StopWatch();
        try(Statement st = connection.createStatement()) {
                logWriter.println("\nExecute : " + sql);
                Logger.info("Execute : %s", sql);
                stopWatch.start();
                try {
                    st.execute(sql);
                    logWriter.print("[OK]\n");
                    Logger.info("[OK]\n");
                } catch (Exception e) {
                    String str = String.format("\n#### %s %s\n", ++i, sql);
                    logWriter.print(str);
                    str = String.format("[ERROR] %s\n", e.getMessage());
                    logWriter.print(str);
                    Logger.info("[ERROR] %s\n", e.getMessage());
                }
                stopWatch.stop();
                logWriter.println("time execute :" + stopWatch.getTime());
                Logger.info("time execute : %s", stopWatch.getTime());
                stopWatch.reset();
        }catch (SQLException e) {
            Logger.error(e, "executeSQL gagal");
        }
    }

}
