package jobs.migration;

public class PgSequence implements Comparable<PgSequence> {

    final String sequenceName;

    public PgSequence(String sequenceName) {
        this.sequenceName = sequenceName;
    }

    public String create() {
        return "CREATE SEQUENCE " +sequenceName+" INCREMENT 1 MINVALUE 1 CACHE 1;";
    }

    public String name() {
        return sequenceName;
    }

    public int compareTo(PgSequence o) {
        return name().compareTo(o.name());
    }
}
