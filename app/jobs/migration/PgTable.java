package jobs.migration;

import org.apache.commons.lang3.StringUtils;

import java.util.*;


public class PgTable implements Comparable<PgTable> {
	
	private final String tableName;
	private final List<String> primaryKeyColumns=new ArrayList<>();
	private String primaryKeyName;
	
	private final Set<PgForeignKey> foreignKeys = new HashSet<>();

	private final Map<String, PgColumn> columns=new HashMap();

	private final List<PgIndex> indexes=new ArrayList<>();

	@Override
	public int compareTo(PgTable o) {
		return name().compareTo(o.name());
	}

	public void column(PgColumn column) {
        columns.put(column.name(), column);
    }

    public void foreignKeys(Collection<PgForeignKey> list) {
	    foreignKeys.addAll(list);
    }

    public void primaryKeyColumns(String pkColumn) {
	    primaryKeyColumns.add(pkColumn);
    }

    public void primaryKeyName(String primaryKeyName) {
        this.primaryKeyName = primaryKeyName;
    }

    public String primaryKeyName() {
	    return primaryKeyName;
    }

    public void indexes(Collection<PgIndex> list) {
	    indexes.addAll(list);
    }

	public String name() {
		return tableName;
	}

	public PgTable(String schema, String tableName) {
		this.tableName = schema+"."+tableName;
	}
//
	public String toString()
	{
		return tableName;
	}
	
	public String getPrimaryKey() {
		StringBuilder str=new StringBuilder();
		str.append(primaryKeyName).
			append(" PRIMARY KEY ").append('(');
		for(int i=0;i<primaryKeyColumns.size();i++)
		{
			if(i>0)
				str.append(',');
			str.append(primaryKeyColumns.get(i));			
		}		
		str.append(')');
		return str.toString();
	}

	public String compareTable(PgTable pgTable4) {
        StringBuilder str=new StringBuilder();
		//#1 compare columns
		for(Map.Entry<String, PgColumn> stringPgColumnEntry : pgTable4.columns.entrySet())
		{
			PgColumn pgColumn4 = stringPgColumnEntry.getValue();
			PgColumn pgColumn3 = columns.get((String) stringPgColumnEntry.getKey());
			//new column
			if(pgColumn3==null)
			{
				//create DDL
                str.append("\nALTER TABLE " + pgTable4.name() +" ADD COLUMN " + pgColumn4.toString() +';');
			}
			else
			{
				//Diperiksa atas kesamaan NAMA dan TYPE
				if(!pgColumn4.equalsNameAndType(pgColumn3))
				{
					//type
                    str.append("\nALTER TABLE " + pgTable4.name() +" ALTER COLUMN " + pgColumn4.name() +" TYPE " + pgColumn4.getDataType() +';');
				}
				
				/* **************** NotNull-ness *****************
				#1 SPSE 4 is NULL but SPSE 3 NOT NULL
				*/
				if(pgColumn3.nullable().equals("NOT NULL") && pgColumn4.nullable().equals("NULL"))
				{
                    str.append("\nALTER TABLE " + pgTable4.name() + " ALTER COLUMN " + pgColumn4.name() +" DROP NOT NULL;");
				}
				//#2 SPSE 4 is NOT-NULL but SPSE 3 NULL
				if(pgColumn4.nullable().equals("NOT NULL") && pgColumn3.nullable().equals("NULL") )
				{
                   str.append("\nALTER TABLE "+pgTable4.name() + " ALTER COLUMN " +pgColumn4.name() +" SET NOT NULL;");
				}
				
				/*******************************DEFAULT  ************************* /
				 */
				//1 terkait default: jika ada DEFAULT di versi 3 dan tidak ada di SPSE 4 maka DROP
				if(pgColumn3.hasDefault() && !pgColumn4.hasDefault())
				{
                    str.append("\nALTER TABLE "+pgTable4.name()+" ALTER COLUMN "+pgColumn4.name()+" DROP DEFAULT;");
				}
				
				//2 terkait default: jika ada DEFAULT di versi 4 dan tidak ada di SPSE 3 maka CREATE DEFAULT
				if(pgColumn4.hasDefault() && !pgColumn4.columndef().equals(pgColumn3.columndef()))
				{
                    str.append("\nALTER TABLE "+pgTable4.name()+" ALTER COLUMN "+pgColumn4.name()+" SET DEFAULT "+pgColumn4.columndef()+";");
				}
				
			}
			
		}
		
		//#2 compare PK 
		if(!primaryKeyColumns.equals(pgTable4.primaryKeyColumns))
		{
			if(primaryKeyName!=null)
			{
                str.append("\nALTER TABLE " + pgTable4.name() + " DROP CONSTRAINT " +primaryKeyName + ';');
			}
            //comment
            str.append("\nALTER TABLE " + pgTable4.name() +" ADD CONSTRAINT " + pgTable4.primaryKeyName +" PRIMARY KEY " +
                    '(' +StringUtils.join(pgTable4.primaryKeyColumns, ',') +");" +"/* ALTER PRIMARY KEY from: '" + getPrimaryKey() +"' */");
		}
		
		//#3 compare FK dilakukan terpisah
		
		
		//#4 compare indexes
        //Check apakah ada index yang harus didrop & dicreate ?
        //#1: check index yg harus di-drop
        for(PgIndex pgDropIndex: indexes)
            if(!pgTable4.indexes.contains(pgDropIndex))
                str.append(pgDropIndex.delete());
        //#2: check index yg harus di-create
        for(PgIndex pgNewIndex: pgTable4.indexes)
            if(!indexes.contains(pgNewIndex))
                str.append(pgNewIndex.create());
        return str.toString();
	}

	/**Check foreign key dengan beberapa kondisi
	 * @param pgTable4
	 */
	public String alterForeignKey(PgTable pgTable4) {
        StringBuilder str=new StringBuilder();
		//Kondisi 1: apakah ada foreign key yg harus di-DROP?
		for(PgForeignKey pgDropFK: foreignKeys)
			if(!pgTable4.foreignKeys.contains(pgDropFK))
                str.append(pgDropFK.delete());
		//kondisi 2: apakah ada foreign key baru?
		for(PgForeignKey pgNewPk: pgTable4.foreignKeys)
			if(!foreignKeys.contains(pgNewPk))
                str.append(pgNewPk.create());
        return str.toString();
	}

	/**get DDL to Create Table except Foreign key
	 * 
	 * @return
	 */
	public String create()
	{
		StringBuilder str=new StringBuilder();
		str.append("\n\nCREATE TABLE ").append(name()).append("\n(");
		//1 Create columns
		int i=0;
		for(PgColumn pgColumn: columns.values())
		{
			if(i>0)
				str.append("\n,");
			str.append(pgColumn.toString());
			i++;
		}
		
		str.append(");");
//		ddlCommands.add(str.toString());
		
		//2 Create PK
//		str=new StringBuilder();
		if(primaryKeyColumns.size()>0)
		{
			str.append("\nALTER TABLE ").append(name())
				.append(" ADD CONSTRAINT ").append(getPrimaryKey())
				.append(';');
		}
//		ddlCommands.add(str.toString());
		
		//3 Create FK tidak dijalankan di sini
        for(PgForeignKey pgNewPk: foreignKeys)
        {
            if(!foreignKeys.contains(pgNewPk))
            {
                str.append(pgNewPk.create());
            }
        }
			
		//4 Create Index
		for(PgIndex index: indexes)
		{
//			ddlCommands.add(index.create());
            str.append(index.create());
		}
		return str.toString();
	}
}
