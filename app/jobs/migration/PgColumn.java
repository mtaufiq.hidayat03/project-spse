package jobs.migration;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class PgColumn implements Comparable<PgColumn>{

	private final String columnName;
	private final String typeName; //see DDL data type
	private final int columnSize;
	private final int decimalDigits;
	private final String nullable;
	private final String columnDef;

	public PgColumn(ResultSet rs) throws Exception {
        this.columnName = rs.getString("COLUMN_NAME");
        this.typeName = rs.getString("TYPE_NAME");
        int columnSize = rs.getInt("COLUMN_SIZE");
        /* Kadang2 columnSize tidak akurat Untuk numeric perlu dibatasi 64 */
        if(typeName.equals("numeric") && columnSize > 1000)
            columnSize=64;
        this.columnSize = columnSize;
        this.decimalDigits = rs.getInt("DECIMAL_DIGITS");
        String nullable ="NOT NULL";
        if(rs.getInt("NULLABLE")== ResultSetMetaData.columnNullable)
            nullable="NULL";
        this.nullable = nullable;
        this.columnDef = rs.getString("COLUMN_DEF");
    }

	public String name() {
		return this.columnName;
	}

	public String nullable() {
		return this.nullable;
	}

	public String columndef(){
		return this.columnDef;
	}
		
	public String toString()
	{
		return columnName+' '+getDataType()+' '+nullable+' '+(columnDef!=null?" DEFAULT "+columnDef:"");
	}

	@Override
	public int compareTo(PgColumn o) {
		return columnName.compareTo(o.columnName);
	}
	
	public String getDataType()
	{
		if(typeName.equals("numeric")) {
			if(decimalDigits>0)
				return typeName+'('+columnSize+','+decimalDigits+')';
			return typeName+'('+columnSize+')';
		}
		else if(typeName.equals("varchar")) {
			if(columnSize > 1000)
				return typeName;
			return typeName+'('+columnSize+')';
		}
		else if(typeName.equals("bpchar") || typeName.equals("char")) {
			return typeName+'('+columnSize+')';
		}
		return typeName;	
	}
	
	/**Check if equals AND and TYPE ONLY
	 * 	-> DEFAULT && NULL maybe not equals
	 * @param column
	 * @return
	 */
	public boolean equalsNameAndType(PgColumn column)
	{
		return columnName.equals(column.columnName) && typeName.equals(column.typeName) && columnSize == column.columnSize;
	}
	
	public boolean hasDefault()
	{
		return columnDef!=null && columnDef.contains("DEFAULT");
	}

}
