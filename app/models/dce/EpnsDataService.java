package models.dce;

import org.apache.commons.codec.binary.Base64;
import play.Logger;
import play.db.DB;
import models.sso.common.adp.util.DceSecurityV2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * *  Tidak dipakai, sekedar copy dari SPSE3
 */
@Deprecated
public class EpnsDataService {		
		
	/**
	 * Retrieves query
	 */	 
	public static String getJSON(String q) {
		
		Connection con ;
		ResultSet rs;
		String[] columnName;
		columnName = new String[50];
		int jumlahKolom=0;
	//	long waktuAwal = System.currentTimeMillis();
		int value=0;
		int record = 0;
		StringBuilder totalRecord= new StringBuilder();
		StringBuilder namaKolom= new StringBuilder("(repo_id,");
		String[] namaKolom1= new String[100];
		String decodedQ = new String(Base64.decodeBase64(q));
	    boolean tanpaBintang = false;
		 //encrypt
		
	    //decrypt        
	    String decryptedQuery = DceSecurityV2.decrypt(decodedQ).replaceAll("%20", " ");
	    StringBuilder isiKolom= new StringBuilder("[");
	    StringBuilder isiKolomTanpaBintang= new StringBuilder("[");
	  
		try
		{
			
			con = DB.getConnection();
			String[] queryArray = splitStringToArray(decryptedQuery);			
			//
			if(queryArray[1].equalsIgnoreCase("*")){
				tanpaBintang=false;
				
				String sqlcolumnname=" SELECT a.attname as \"column\" FROM  pg_catalog.pg_attribute a WHERE a.attnum > 0 AND NOT a.attisdropped   AND a.attrelid = (   SELECT c.oid   FROM pg_catalog.pg_class c  LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace  WHERE c.relname ~ '^("+queryArray[3]+ ')' +"$' AND pg_catalog.pg_table_is_visible(c.oid) )";
				java.sql.Statement cn = con.createStatement();
				rs = cn.executeQuery(sqlcolumnname);
				int index=0;
				while(rs.next())
				{
					columnName[index]=rs.getString(1);
					index++;
					isiKolom.append(rs.getString(1)).append("~|~");
					
				}
				jumlahKolom=index;
			}//select abc, bcd, cde
			else if(!queryArray[1].equals("*"))
			{
				String[] fixedColumn = splitStringToArrayComma(queryArray[1]);
				tanpaBintang=true;
				int index=0;
				//columnName[index] = fixedColumn[1].toString().replaceAll(" ", "");
				while(index<fixedColumn.length)
				{
					columnName[index] = fixedColumn[index];
					
					isiKolomTanpaBintang.append(columnName[index]).append("~|~");
					index++;
					
				}
				jumlahKolom=index;			
			//	jumlahKolom=index;
			}
			
			if(tanpaBintang==false)
			{
				
			isiKolom = new StringBuilder(isiKolom.substring(0, isiKolom.length() - 3));
			isiKolom.append(']');
			}
			else if(tanpaBintang==true)
			{
				isiKolom = new StringBuilder(isiKolomTanpaBintang + "]");
			
			}
			java.sql.Statement st = con.createStatement();
			rs = st.executeQuery(decryptedQuery);
			
			
			
			//buat nama kolom cek posisinya dimana..
			for (int i=0;i<jumlahKolom; i++){
				
				 if(columnName.length < 2) //jika kolom cuma 1
				 {
					 namaKolom.append(columnName[i]).append(')');
				 }
				 else if(i == jumlahKolom-1)//jika kolomnya dah paling akhir
				 {
					 namaKolom.append(columnName[i]).append(')');
				 }
				 else //buat handle ditengah2
				 {
					 
					 namaKolom.append(columnName[i]).append(',');
				 }
			}
			namaKolom1[0]="[";
			
			while(rs.next())
			{
				++record;
				// untuk membagi 1 index seribu record
								 
				if(record%1000==0){
					value++;
					namaKolom1[value]="";
				}
				
				//tulis INDEX DISINI untuk cek
				for (int i = 0; i < jumlahKolom; i++){
					
					if(columnName[i]!=null){					
						if(columnName.length < 2) //jika kolom cuma 1
						 {
							//Mengubah  1 single quote menjadi 2 single quote
							
							if (rs.getString(columnName[i])!=null){
								if(rs.getString(columnName[i]).contains("\'")){
									String a= rs.getString(columnName[i]).replace("\'", "\'\'");
									namaKolom1[value] = namaKolom1[value]+ '\'' +a+ '\'' + "~|~repo_id]";
								}
								else if(rs.getString(columnName[i]).contains("\\")){
									String a= rs.getString(columnName[i]).replace("\\", "\\\\");
									namaKolom1[value] = namaKolom1[value]+"E'"+a+ '\'' + "~|~repo_id]";
								}
								else
									namaKolom1[value] = namaKolom1[value]+ '\'' +rs.getString(columnName[i])+ '\'' + "~|~repo_id]";
							}
							else
								namaKolom1[value] = namaKolom1[value]+ '\'' +rs.getString(columnName[i])+ '\'' + "~|~repo_id]";
							
							
						 }
						 else if(i == jumlahKolom-1)//jika kolomnya dah paling akhir
						 {
							 
							 if (rs.getString(columnName[i])!=null){
								if(rs.getString(columnName[i]).contains("\'")){
									String a= rs.getString(columnName[i]).replace("\'", "\'\'");
									namaKolom1[value] = namaKolom1[value]+ '\'' +a+ '\'' + "~|~repo_id][";
								}
								else if(rs.getString(columnName[i]).contains("\\")){
									String a= rs.getString(columnName[i]).replace("\\", "\\\\");
									namaKolom1[value] = namaKolom1[value]+"E'"+a+ '\'' + "~|~repo_id][";
								}
								else
									 namaKolom1[value] = namaKolom1[value]+ '\'' +rs.getString(columnName[i])+ '\'' + "~|~repo_id][";
							 }
							 else
								 namaKolom1[value] = namaKolom1[value]+ '\'' +rs.getString(columnName[i])+ '\'' + "~|~repo_id][";
							 
							 }
						 
						 else //buat handle ditengah2
						 {
							 				  //namaKolom1[value]
							 if (rs.getString(columnName[i])!=null){
									if(rs.getString(columnName[i]).contains("\'")){
										String a= rs.getString(columnName[i]).replace("\'", "\'\'");
										namaKolom1[value] = namaKolom1[value]+ '\'' +a+ '\'' + "~|~";
									}
									else if(rs.getString(columnName[i]).contains("\\")){
										String a= rs.getString(columnName[i]).replace("\\", "\\\\");
										namaKolom1[value] = namaKolom1[value]+ '\'' +a+ '\'' + "~|~";
									}
									else
										 namaKolom1[value] = namaKolom1[value]+ '\'' +rs.getString(columnName[i])+ '\'' + "~|~";
								 }
							 else
								 namaKolom1[value] = namaKolom1[value]+ '\'' +rs.getString(columnName[i])+ '\'' + "~|~";
							 
						 }
					}		
				}
			}
			//kurangin kelebihan string			
			if(namaKolom1[value].length()>1)
			{
			namaKolom1[value] = namaKolom1[value].substring(0, namaKolom1[value].length()-1);
			}
			else
			{
				return DceSecurityV2.encrypt("NoEntryDetected");
			}
			
			for (int i=0; i<=value; i++){
				totalRecord.append(namaKolom1[i]);
			}
			
			
			
			rs.close();
			st.close();
			con.close();
			
		} 
		catch(SQLException ex) 
		{
			Logger.error("Query Bermasalah: "+ex.getMessage());
		}
		//long waktuAkhir = System.currentTimeMillis();
		//float result=(waktuAkhir-waktuAwal);
		return DceSecurityV2.encrypt("["+record+ ']' +isiKolom+totalRecord);
 	}
	
		
	public static String getProperties()
	{
		Connection con ;
		ResultSet rs;
		String sqlstr="";
		String[] ppe_site=new String[3];
		try
		{			
			con = DB.getConnection();
			sqlstr="select pps_id, pps_nama, pps_alamat from ppe_site";
			java.sql.Statement cn = con.createStatement();
			rs = cn.executeQuery(sqlstr);
			while(rs.next())
			{
				ppe_site[0]=rs.getString(1);
				ppe_site[1]=rs.getString(2);
				ppe_site[2]=rs.getString(3);
			}
			
		}
		
		
		
		catch(SQLException ex) 
		{
		
		}
		
		String[] config_site=new String[3];
		try
		{			
			con = DB.getConnection();
			sqlstr="select cfg_value from configuration where cfg_sub_category LIKE 'ppe%'";
			java.sql.Statement cn = con.createStatement();
			rs = cn.executeQuery(sqlstr);
			int i = 0;
			while(rs.next())
			{
				
				config_site[i]=rs.getString(1);
				
				i++;
			}
		}
		
		
		
		catch(SQLException ex) 
		{
		
		}
		
		return config_site[0]+"~|~"+config_site[1]+ "~|~"+ppe_site[2];
	}
	
	public static String[] splitStringToArray(String s) {
		String[] x = Pattern.compile(" ").split(s);
		return x;
    }
	
	public static String[] splitStringToArrayComma(String s) {
		String[] x = Pattern.compile(",").split(s);
		return x;
    }
	
	
	
}

