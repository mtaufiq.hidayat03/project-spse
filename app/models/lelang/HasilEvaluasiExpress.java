package models.lelang;

import ext.FormatUtils;
import models.common.JenisEmail;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import play.i18n.Messages;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lkpp on 2/24/2015.
 * hasil evaluasi Tender Express
 */
public class HasilEvaluasiExpress implements Serializable {

    public Long psr_id;
    public Long rkn_id;
    public String rkn_nama;
    public String rkn_npwp;
    public Integer harga; // status evaluasi harga
    public String als_harga;
    public Integer pemenang; // status evaluasi by system
    public Integer pemenang_verif; // status evaluasi akhir
    public Integer sudah_verifikasi_sikap; // status evaluasi akhir
    public Double psr_harga;
    public Double psr_harga_terkoreksi;
    public Integer pembuktian; // status pembuktian
    public Long fileHarga;
    public Integer undangan_verifikasi = 0;

    public String getPenawaran(){
    	if(psr_harga == null)
    		return Messages.get("lelang.tap");
        return FormatUtils.formatCurrencyRupiah(psr_harga);
    }

    public String getPenawaranTerkoreksi(){
    	if(psr_harga_terkoreksi == null)
    		return Messages.get("lelang.tap");
        return FormatUtils.formatCurrencyRupiah(psr_harga_terkoreksi);
    }

    public String getAlasan(){
        String alasan="";
        if(!StringUtils.isEmpty(als_harga))
            alasan += als_harga;
        return alasan;
    }
    
    public boolean isSudahUndangVerifikasi() {
    	return undangan_verifikasi > 0;
    }
    
    public static List<HasilEvaluasiExpress> findByLelang(Long lelangId) {
        String sql = "SELECT p.psr_id,r.rkn_id, r.rkn_nama, d.dok_id as fileHarga, h.nev_lulus as harga, p.psr_harga, p.psr_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif, p.sudah_verifikasi_sikap," +
                "h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, (select count(id) from mail_queue where lls_id=:lls_id and rkn_id=r.rkn_id and jenis=:jenis) as undangan_verifikasi FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id LEFT JOIN dok_penawaran d ON p.psr_id=d.psr_id AND d.dok_jenis = 2" +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 5 AND eva_versi=:versi ) q ON p.psr_id=q.psr_id " +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 3 AND eva_versi=:versi ) h ON p.psr_id=h.psr_id " +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 4 AND eva_versi=:versi ) s ON p.psr_id=s.psr_id WHERE p.lls_id=:lls_id order by psr_harga asc";
        int versi  = Evaluasi.findCurrentVersi(lelangId);
        return Query.find(sql, HasilEvaluasiExpress.class).setParameter("lls_id", lelangId).setParameter("versi", versi).setParameter("jenis", JenisEmail.UNDANGAN_VERIFIKASI.id).fetch();
    }

    public static HasilEvaluasiExpress findByPeserta(Long pesertaId) {
        String sql = "SELECT p.psr_id,r.rkn_id, r.rkn_nama, d.dok_id as fileHarga, h.nev_lulus as harga, p.psr_harga, p.psr_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif, p.sudah_verifikasi_sikap," +
                "h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, (select count(id) from mail_queue where lls_id=p.lls_id and rkn_id=p.rkn_id and jenis=:jenis) as undangan_verifikasi FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id LEFT JOIN dok_penawaran d ON p.psr_id=d.psr_id AND d.dok_jenis = 2" +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 5) q ON p.psr_id=q.psr_id " +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 3) h ON p.psr_id=h.psr_id " +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 4) s ON p.psr_id=s.psr_id WHERE p.psr_id=:id";
        return Query.find(sql, HasilEvaluasiExpress.class).setParameter("id", pesertaId).setParameter("jenis", JenisEmail.UNDANGAN_VERIFIKASI.id).first();
    }
}
