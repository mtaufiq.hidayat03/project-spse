package models.lelang;

import models.common.Tahap;
import play.db.jdbc.Query;

import java.util.List;

import static models.lelang.Evaluasi.JenisEvaluasi.*;

/**
 * model untuk undangan peserta lelang
 * bisa berupa pengumuman Pemenang Prakualifikasi atau Pengumuman Pemenang Tender
 */
public class UndangaPeserta {

    public Long lls_id;

    public Long psr_id;

    public Long rkn_id;

    public Integer is_pemenang_verif;

    public String rkn_nama;

    public String rkn_email;

    public String rkn_npwp;

    public Nilai_evaluasi.StatusNilaiEvaluasi nev_lulus;

    public static List<UndangaPeserta> findAll(Long lelangId, Tahap tahap, boolean isLelangV3) {
        Evaluasi.JenisEvaluasi jenis = null;
        if(tahap == Tahap.PENGUMUMAN_HASIL_PRA)
            jenis = isLelangV3 ? EVALUASI_KUALIFIKASI: PEMBUKTIAN_KUALIFIKASI;
        else if(tahap == Tahap.PENGUMUMAN_PEMENANG_AKHIR)
           jenis = EVALUASI_AKHIR;
        return Query.find("SELECT DISTINCT r.rkn_id, p.psr_id, p.lls_id, is_pemenang_verif, rkn_nama, rkn_email, rkn_npwp, nev_lulus FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id " +
                "LEFT JOIN nilai_evaluasi n ON n.psr_id=p.psr_id AND n.eva_id IN (SELECT eva_id FROM evaluasi e WHERE e.lls_id=p.lls_id AND eva_jenis = ? ORDER BY eva_versi DESC LIMIT 1) " +
                "WHERE p.lls_id=? ", UndangaPeserta.class, jenis, lelangId).fetch();
    }

    public boolean isLulus() {
        return nev_lulus != null && nev_lulus.isLulus();
    }
}
