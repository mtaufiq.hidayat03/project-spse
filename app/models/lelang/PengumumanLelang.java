package models.lelang;

import controllers.BasicCtr;
import models.common.Kategori;
import models.common.Metode;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * fungsi class ini untuk model bentuk pengumuman lelang di halaman muka
 *
 * @author Javax
 */

public class PengumumanLelang implements Serializable {

	public static final int ROW_MAX = 6;

	private static final Map<Kategori, List<PengumumanLelang>> kategoriMap = new ConcurrentHashMap<>(6);

	public Long lls_id;

	public String pkt_nama;

	public Double pkt_hps;

	public Date tgl_akhir_daftar; // jadwal akhir masa upload penawaran (jika pasca) dan upload dok_pra (jika prakualifikasi)

	public Kategori kgr_id;

	public Integer pkt_flag;

	public Boolean pkt_hps_enable;


	/**
	 * informasi lelang yang sedang tahap pengumuman
	 * tahap pengumuman yang dimaksud adalah :Tahap.PENGUMUMAN_LELANG, Tahap.UMUM_PRAKUALIFIKASI, Tahap.AMBIL_DOKUMEN_PEMILIHAN
	 * Tahap.AMBIL_DOKUMEN, Tahap.AMBIL_DOK_PRA
	 * hasil query akan dicache selama 1 menit
	 *
	 * @return
	 */
	public static List<PengumumanLelang> findAll() {
		List<PengumumanLelang> list = Cache.get("pengumumanLelang", List.class);
		if (CommonUtil.isEmpty(list)) {
			String id_metode_pasca = StringUtils.join(Metode.METODE_PASCA_ID, ",");
			String id_metode_pra = StringUtils.join(Metode.METODE_PRA_ID, ",");
			Date time = BasicCtr.newDate();
			QueryBuilder query = new QueryBuilder("SELECT lls_id, pkt_nama, pkt_flag, pkt_hps, pkt_hps_enable, tgl_akhir_daftar, kgr_id ")
					.append("FROM lelang_query WHERE lls_status=1 AND lls_id IN (SELECT distinct j.lls_id FROM jadwal j, aktivitas a, lelang_seleksi l WHERE j.lls_id=l.lls_id and j.akt_id=a.akt_id")
					.append(" AND ((l.mtd_pemilihan NOT IN (9,10) AND l.mtd_id IN ("+id_metode_pasca+") AND akt_jenis IN ('PENGUMUMAN_LELANG', 'AMBIL_DOKUMEN') AND dtj_tglawal <= ? AND dtj_tglakhir >= ?)", time, time)
					.append(" OR (l.mtd_pemilihan IN (9,10) AND l.lls_status=1 AND akt_jenis IN ('PENJELASAN', 'PEMASUKAN_PENAWARAN') AND dtj_tglakhir >= ?)",time)
					.append(" OR (l.mtd_pemilihan NOT IN (9,10) AND l.mtd_id IN ("+id_metode_pra+") AND akt_jenis IN ('AMBIL_DOK_PRA', 'UMUM_PRAKUALIFIKASI') AND dtj_tglawal <= ? AND dtj_tglakhir >= ?)))", time, time)
					.append("ORDER BY kgr_id ASC, lls_id DESC");
			list = Query.find(query, PengumumanLelang.class).fetch();
			if(list == null) //Cache is not allowing the null values.
				list = new ArrayList<>();
			Cache.safeSet("pengumumanLelang", list, BasicCtr.DEFAULT_CACHE);
		}
		return list;
	}

	public static List<PengumumanLelang> findByKategori(Kategori kategori) {
		List<PengumumanLelang> list = findAll();
		List<PengumumanLelang> result = new ArrayList<>();
		if (!CommonUtil.isEmpty(list)) { // organize map
			for (PengumumanLelang item : list)
				if(item.kgr_id!= null && item.kgr_id.equals(kategori)) {
					result.add(item);
				}
		}
		return result;
	}

	public Boolean isEnableViewHps(){
		return pkt_hps_enable != null ? pkt_hps_enable : true;
	}
}
