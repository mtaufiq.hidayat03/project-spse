package models.lelang;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.Kategori;
import models.common.KeyLabel;
import models.common.Kualifikasi;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.mvc.Scope;
import utils.JsonUtil;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.List;

@Table(name="CHECKLIST_MASTER")
public class Checklist_master extends BaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum ChecklistStatus {
		NOT_AKTIF, AKTIF
	}
	
	@Enumerated(EnumType.ORDINAL)
	public enum JenisChecklist {
		CHECKLIST_KUALIFIKASI(0,"Checklist Syarat Kualifikasi"),
		CHECKLIST_ADMINISTRASI(1,"Checklist Syarat Administrasi"),
		CHECKLIST_TEKNIS(2,"Checklist Syarat Teknis"),
		CHECKLIST_HARGA(3,"Checklist Syarat Harga"),
		CHECKLIST_TEKNIS_PL(4,"Checklist Syarat Teknis PL"), //untuk persyaratan teknis PL
		CHECKLIST_KUALIFIKASI_ADMINISTRASI(5,"Checklist Syarat Kualifikasi Administrasi"),
		CHECKLIST_KUALIFIKASI_TEKNIS(6,"Checklist Syarat Kualifikasi Teknis"),
		CHECKLIST_KUALIFIKASI_KEUANGAN(7,"Checklist Syarat Kualifikasi Kemampuan Keuangan"),
		CHECKLIST_KATEGORI_SANGGAHAN(8,"Checklist Kategori Sanggahan"),
		CHECKLIST_PRAKUALFIKASI_BATAL(9,"Checklist Kualifikasi Batal"),
		CHECKLIST_PASCAKUALFIKASI_BATAL(10,"Checklist Tender Batal");
		
		public final Integer id;
		public final String label;
		
		private JenisChecklist(int key, String label) {
			this.id = Integer.valueOf(key);
			this.label = label;
		}
				
		public static JenisChecklist fromValue(Integer value)
		{
			JenisChecklist jenis = null;
			if(value != null) {
				switch (value.intValue()) {
					case 0:	jenis = JenisChecklist.CHECKLIST_KUALIFIKASI;break;
					case 1:	jenis = JenisChecklist.CHECKLIST_ADMINISTRASI;break;
					case 2:	jenis = JenisChecklist.CHECKLIST_TEKNIS;break;
					case 4:	jenis = JenisChecklist.CHECKLIST_TEKNIS_PL;break;
					case 5:	jenis = JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI;break;
					case 6:	jenis = JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS;break;
					case 7:	jenis = JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN;break;
					case 8:	jenis = JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN;break;
					case 9: jenis = JenisChecklist.CHECKLIST_PRAKUALFIKASI_BATAL;break;
					case 10: jenis = JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL;break;
				}
			}
			return jenis;
		}
		
		public static final List<Integer> CHECKLIST_LDK = Arrays.asList(
				JenisChecklist.CHECKLIST_KUALIFIKASI.id,
				JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI.id,
				JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS.id,
				JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN.id
		);
		public static final List<Integer> CHECKLIST_SYARAT = Arrays.asList(
				JenisChecklist.CHECKLIST_ADMINISTRASI.id,
				JenisChecklist.CHECKLIST_TEKNIS.id,
				JenisChecklist.CHECKLIST_HARGA.id
		);
		public static final List<Integer> CHECKLIST_KUALIFIKASI_BARU = Arrays.asList(
				JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI.id,
				JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS.id,
				JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN.id
		);		
	}

	@Id
	public Integer ckm_id;

	public JenisChecklist ckm_jenis;

	public String ckm_nama;
	
	public Kategori kgr_id;
	
	public ChecklistStatus ckm_status;

	// JSON berisi key dan label untuk build table
	public String table_header;
	
	public Integer ckm_versi = Integer.valueOf(1);
	
	/** checked by default atau manual oleh user 
	*	value : 0 : idle,  diset oleh user
	*			1 : checked, diset oleh sistem, syarat itu harus ada d tiap lelang
	**/
	public Integer ckm_checked = Integer.valueOf(0);

	/* urutan checklist */
	public Integer ckm_urutan;

		
	@Transient
	public boolean isChecked() {
		return this.ckm_checked == Integer.valueOf(1);
	}
	
	@Transient
	public boolean isIjinUsaha() {
		return this.ckm_id.intValue() == 1;
	}	
	
	@Transient
	public boolean isIjinUsahaBaru() {
		return this.ckm_id.intValue() == 50;
	}	
	
	@Transient
	public boolean isSyaratLain() {
		return isSyaratLain(ckm_id);
	}
	
	@Transient
	public boolean isNotEditable() {
		int id = this.ckm_id.intValue();
		return (id == 1) || (id == 9) || (id==10) || (id==13) || (id==15) || (id == 38) || (id == 50) 
				|| (id == 51) || (id == 52) || (id == 54) || (id == 55) || (id == 56) || (id == 57) || (id == 58) || (id == 64) || (id == 66) || (id == 67)
				|| (id == 69) || (id == 70) || (id == 72) || (id == 73) || (id == 74) || (id == 75) || (id == 76) || (id == 84) || (id == 85)
				|| (id == 86) || (id == 87) || (id == 89) || (id == 90) || (id == 91) || (id == 92) || (id == 93) || (id == 98) || (id == 99) || (id == 100)
				|| (id == 101) || (id == 102) || (id == 104) 
				|| (id == 112) || (id == 113) || (id == 115) || (id == 116) || (id == 117) || (id == 118) ||  (id == 119) || (id == 124)
				|| (id == 128) || (id == 130) || (id == 131) || (id == 132) || (id == 133) || (id == 134)
				|| (id == 140) || (id == 142) || (id == 143) || (id == 144) || (id == 145) || (id == 150) || (id == 137) || (id == 146) || (id == 147);
	}

	@Transient
	public boolean isInputNumber() {
		int id = this.ckm_id.intValue();
		return (id == 60) || (id == 77) || (id == 94) || (id == 136);
	}

	@Transient
	public boolean isInputNumberMulti() {
		return false;
	}
	
	@Transient
	public boolean isInputTextMulti() {
		int id = this.ckm_id.intValue();
		return (id == 105);
	}
	
	@Transient
	public boolean isInputSelect() {
		int id = this.ckm_id.intValue();
		return (id == 53) || (id == 71) || (id == 88) || (id == 103) || (id == 114) || (id == 129) || (id == 141);
	}

	@Transient
	public boolean isConsultant() {
		int id = this.ckm_id.intValue();
		return (id == 39) || (id == 40) || (id == 41);
	} 
	
	@Transient
	public boolean isPekKonstruksi() {
		int id = this.ckm_id.intValue();
		return (id == 152) || (id == 153) || (id == 154) || (id == 155) || (id == 156) || (id == 157);
	}
	
	@Transient
	public boolean isJkKonstruksi() {
		int id = this.ckm_id.intValue();
		return (id == 158) || (id == 159) || (id == 160) || (id == 161);
	}
	
	@Transient
	public boolean isTeknisLainnya() {
		int id = ckm_id.intValue();
		return (id == 20) || (id == 21) || (id == 22) || (id == 23) || (id == 24) || (id == 25) || (id == 26) || (id == 27) || (id == 29);
	}
	@Transient
	public boolean isKonsultansiPerorangan() {
		int id = this.ckm_id.intValue();
		return (id == 39) || (id == 40) || (id == 41) || (id == 196) ||(id == 197);
	} 

	@Transient
	public boolean isConsultantPL() {
		int id = this.ckm_id.intValue();
		return (id == 46) || (id == 47) || (id == 48);
	}

	@Transient
	public boolean administrationIsNotConstruction(){
		int id = this.ckm_id.intValue();
		return (id == 16) || (id == 18);
	}
	
	@Transient
	public boolean hargaIsNotConstruction(){
		int id = this.ckm_id.intValue();
		return (id == 19) || (id == 31) || (id == 127);
	}
	
	@Transient
	public boolean isSKN() {
		int id = this.ckm_id.intValue();
		return (id == 177) || (id == 179);
	}
	
	@Transient
	public boolean isSKP() {
		int id = this.ckm_id.intValue();
		return (id == 178) || (id == 180);
	}
	
	@Transient
	public boolean ldkPlIsConstruction(){
		int id = this.ckm_id.intValue();
		return (id == 2);
	}

	/**
	 * needed only on html to define what kind of view a spec should be this one is for number only.
	 * input type: text max length: 3
	 * return boolean*/
	@Transient
	public boolean isInputNumberOnly() {
		int id = this.ckm_id.intValue();
		return (id == 2);
	}

	@Transient
	public boolean isInputTable() {
		return table_header != null && table_header != "";
	}

	@Transient
	public KeyLabel getTable() {
		return JsonUtil.fromJson(table_header, KeyLabel.class);
	}

	@Transient
	public String getCkmKey() {
		return "key" + ckm_id;
	}
	
	@Transient
	public String getKeterangan() {
		String result="";
		int id = this.ckm_id.intValue();
		switch (id) {
			case 1:result="";break;
			case 2:result="paling kurang 10% (sepuluh perseratus) dari nilai total HPS, maksimal 100% dan hanya menerima input angka!";break;
			case 3:result="";break;
			case 4:result="isi sesuai dengan jenis keahlian, persyaratan keahlian/spesialisasi, pengalaman, dan kemampuan manajerial yang diperlukan";break;
			case 5:result="isi sesuai dengan pekerjaan/bidang/subbidang sejenis yang dipersyaratkan"; break;
			case 6:result="";break;
			case 11: case 107: result="isi dengan jenis kemampuan teknis, kemampuan teknis, persyaratan pengalaman dan manajerial yang diperlukan"; break;
			case 108: result="isi dengan jenis pelatihan, kemampuan teknis, persyaratan pengalaman dan manajerial yang diperlukan"; break;
			case 12: result="sebutkan fasilitas/peralatan/perlengkapan minimum yang diperlukan, termasuk yang bersifat khusus/spesifik/berteknologi tinggi"; break;
			case 14: case 106: result="isi sesuai dengan tingkat pendidikan, jenis keahlian/spesialisasi, pengalaman dan persyaratan kemampuan manajerial yang diperlukan";break;
			case 61: case 78: case 79: case 95: case 121: result="* pastikan Jenis Keahlian, Keahlian/Spesialisasi, Pengalaman, dan Kemampuan Manajerial sudah diisi sebelum menyimpan";break;
			case 62: case 80: case 96: case 122: case 109: result="* pastikan Jenis Kemampuan, Kemampuan Teknis, Pengalaman, dan Kemampuan Manajerial sudah diisi sebelum menyimpan";break;
			case 63: case 81: case 97: case 123: result="* pastikan Nama dan Spesifikasi sudah diisi sebelum menyimpan";break;
			case 148: case 149: result="* pastikan Jenis Kemampuan, Kemampuan Teknis, Pengalaman, dan Kemampuan Manajerial sudah diisi sebelum menyimpan. Jika tidak dipersyaratkan isi dengan karakter <b>-</b>";break;
		}
		return result;
	}

	public static boolean isSyaratLain(Integer ckm_id) {
		if(ckm_id == null )
			return false;
		int id = ckm_id.intValue();
		return (id == 28) || (id == 29) || (id == 30) || (id == 36) || (id == 995) || (id == 996) || (id == 997);
	}

	// validasi post param dengan tabel_header yg sudah ada
	public void validateTableHeader(JsonArray values, Scope.Params params, Kualifikasi kualifikasi) throws Exception {
		KeyLabel keyLabel = getTable();
		if(keyLabel == null)
			return;
		String[] firstValues = params.getAll(keyLabel.key[0]);
//		if(isInputTextMulti() && kualifikasi.isKecil()) {
		if(isInputTextMulti()) {
			keyLabel.key = new String[]{"text1"};
		}
		boolean invalidData = false;
		for(int i=0; i < firstValues.length; i++) {
			JsonObject object = new JsonObject();
			for(String key : keyLabel.key) {
				String val = "";
				String[] vals = params.getAll(key);
				if(vals != null && vals.length >= i) {
					val = vals[i];
				}
				if(StringUtils.isEmpty(val)) {
					invalidData = true;
				}
				object.addProperty(key, val);
			}
			values.add(object);
		}
		if(invalidData) {
			String msg = ckm_nama;
			if (isInputNumber())
				msg = msg.replaceAll("number", " ? ");
			if (isInputTextMulti()) {
//				if(kualifikasi.isKecil()) {
					String[] split= ckm_nama.split("text");
					Logger.info("Split : %s", split);
					msg = split[0]+" ? ";
//				} else {
//					msg = msg.replaceAll("text", " ? ");
//				}
			}
			throw new Exception(msg + " wajib diisi jika diceklis (dipersyaratkan)!");
		}
	}

	public void setTableHeader(JsonArray values, Scope.Params params, Kualifikasi kualifikasi)  {
		KeyLabel keyLabel = getTable();
		String[] firstValues = params.getAll(keyLabel.key[0]);
//		if(isInputTextMulti() && kualifikasi.isKecil()) {
		if(isInputTextMulti()) {
			keyLabel.key = new String[]{"text1"};
		}
		for(int i=0; i < firstValues.length; i++) {
			JsonObject object = new JsonObject();
			for(String key : keyLabel.key) {
				String val = "";
				String[] vals = params.getAll(key);
				if(vals != null && vals.length >= i) {
					val = vals[i];
				}
				object.addProperty(key, val);
			}
			values.add(object);
		}
	}
}