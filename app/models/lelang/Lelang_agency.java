package models.lelang;

import models.common.Kategori;
import models.common.Metode;
import models.common.MetodeEvaluasi;
import models.common.MetodePemilihan;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;

import java.io.Serializable;
import java.util.List;

public class Lelang_agency implements Serializable {

	public Long lls_id;
	
	public String pkt_nama;
	
	public String agc_nama;
	
	public Double pkt_hps;
	
	public Integer kgr_id;
	
	public Integer mtd_pemilihan;
	
	public Integer mtd_evaluasi;
	
	public Integer mtd_id;

	
	public Lelang_agency(Long lls_id, String pkt_nama, String agc_nama, Double pkt_hps, Integer kgr_id,Integer mtd_id, Integer mtd_pemilihan, Integer mtd_evaluasi)
	{
		this.lls_id = lls_id;
		this.pkt_nama = pkt_nama;
		this.agc_nama = agc_nama;
		this.pkt_hps = pkt_hps;
		this.kgr_id = kgr_id;
		this.mtd_id = mtd_id;
		this.mtd_evaluasi = mtd_evaluasi;
		this.mtd_pemilihan = mtd_pemilihan;
	}
	
	public Kategori getKategori()
	{
		return Kategori.findById(kgr_id);
	}
	
	public MetodeEvaluasi getEvaluasi()
	{
		return MetodeEvaluasi.findById(mtd_evaluasi);
	}
	
	public MetodePemilihan getPemilihan()
	{
		return MetodePemilihan.findById(mtd_pemilihan);
	}
	
	public Metode getMetode()
	{
		return Metode.findById(mtd_id);
	}
	
	public static List<Lelang_agency> getSemuaLelangAktif(String filter)
	{
		String sql="select l.lls_id, p.pkt_nama, a.agc_nama, p.pkt_hps, p.kgr_id, l.mtd_id, l.mtd_pemilihan, l.mtd_evaluasi " +
				"from Lelang_seleksi l, Paket p, Satuan_kerja s, Agency a " +
				"where l.lls_status=1 and l.pkt_id=p.pkt_id and p.stk_id=s.stk_id and s.agc_id=a.agc_id ";
		if(!StringUtils.isEmpty(filter))
			sql += "and "+filter;
		sql += " order by l.lls_id desc";
		return Query.find(sql, Lelang_agency.class).fetch();
	}
}
