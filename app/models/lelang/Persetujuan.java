package models.lelang;

import controllers.BasicCtr;
import models.agency.Anggota_panitia;
import models.agency.Panitia;
import models.common.Active_user;
import models.common.StatusLelang;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name="PERSETUJUAN")
public class Persetujuan extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisPersetujuan {
		PENGUMUMAN_LELANG(0,"pengumuman") ,
		PEMENANG_LELANG(1,"pemenang"),
		PEMENANG_PRAKUALIFIKASI(2,"pemenang_pra"),
		PEMENANG_TEKNIS(3,"pemenang_teknis"),
		BATAL_LELANG(4,"tutup"),
		ULANG_LELANG(5,"ulang"),
		PRAKUALIFIKASI_BATAL(6,"pra_batal"),
		PRAKUALIFIKASI_ULANG(7,"pra_ulang");

		public final Integer id;
		public final String key;

		JenisPersetujuan(int id, String key){
			this.id = id;
			this.key = key;
		}

		public boolean isBatal(){
			return this == BATAL_LELANG;
		}

		public Integer getId(){
			return id;
		}

		public String getKey(){
			return key;
		}

		public static JenisPersetujuan getByKey(String key){
			for(JenisPersetujuan e : values()) {
				if(e.key.equals(key)) return e;
			}

			return null;
		}
	}
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusPersetujuan {
		BELUM_SETUJU, SETUJU, TIDAK_SETUJU;
		
		public boolean isSetuju() {
			return this == SETUJU;
		}
		
		public boolean isTidakSetuju() {
			return this == TIDAK_SETUJU;
		}
		
		public boolean isBelumSetuju() {
			return this == BELUM_SETUJU;
		}
	}
	

	@Id(sequence="seq_persetujuan", function="nextsequence")
	public Long pst_id;
	
	//relasi ke lelang_selesai
	public Long lls_id;
	
	//relasi ke Pegawai
	public Long peg_id;

	/*
	 * jenis persetujuan memakai enum JenisPersetujuan
	 * 0 : pengumuman lelang
	 * 1 : pengumuman pemenang
	 */
	public JenisPersetujuan pst_jenis;
	
	public Date pst_tgl_setuju;
	
	public StatusPersetujuan pst_status;
	
	public String pst_alasan;
	
	/*@Transient
	private Lelang_seleksi lelang_seleksi;	

    public Lelang_seleksi getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}
    
    @Transient
	private Pegawai pegawai;

	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}*/
	
	public static class PersetujuanLelang {
		public Long pst_id;		
		//relasi ke lelang_selesai
		public Long lls_id;		
		public JenisPersetujuan pst_jenis;		
		public Date pst_tgl_setuju;		
		public StatusPersetujuan pst_status;		
		public String pst_alasan;
		public String peg_nama;
		public String agp_jabatan;
		
		public String getAlasan() {
			if(StringUtils.isEmpty(pst_alasan))
				return "";
			return pst_alasan;
		}
		
		public String getJabatan() {
			if (agp_jabatan.equals("K")) {
				return Messages.get("lelang.ketua");
			} else if (agp_jabatan.equals("W")) {
				return Messages.get("lelang.wakil");
			} else if (agp_jabatan.equals("S")) {
				return Messages.get("lelang.sekretaris");
			} else {
				return Messages.get("lelang.anggota");
			}
		}
		
		public static List<PersetujuanLelang> findList(Long lelangId, JenisPersetujuan jenis) {
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama " +
					"FROM persetujuan p JOIN pegawai peg on p.peg_id = peg.peg_id WHERE lls_id=? AND pst_jenis=?",
					PersetujuanLelang.class, lelangId,jenis).fetch();
		}
		
		public static List<PersetujuanLelang> getPersetujuanLelangForSummary(Long lelangId){
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, agp_jabatan, peg_nama FROM persetujuan p , (SELECT peg.peg_id,peg_nama,agp_jabatan FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id " 
					+ "LEFT JOIN anggota_panitia ap ON ap.pnt_id=pn.pnt_id LEFT JOIN pegawai peg ON peg.peg_id=ap.peg_id  WHERE lls_id=? AND ap.peg_id=peg.peg_id) a "
					+ "WHERE p.peg_id=a.peg_id AND lls_id=? AND pst_jenis=? and pst_tgl_setuju is not null", PersetujuanLelang.class, lelangId, lelangId, JenisPersetujuan.PENGUMUMAN_LELANG).fetch();
		}
	}
	
	public String getAlasan() {
		if(StringUtils.isEmpty(pst_alasan))
			return "";
		return pst_alasan;
	}

	public static Persetujuan findByPegawaiPengumuman(Long pegawaiId, Long lelangId) {
        return find("peg_id=? and lls_id=? and pst_jenis=?", pegawaiId, lelangId, JenisPersetujuan.PENGUMUMAN_LELANG).first();
    }

	public static List<Persetujuan> findByLelangPersetujuanPemenang(Long lelangId) {
		return find("lls_id=? and pst_jenis=?", lelangId, JenisPersetujuan.PEMENANG_LELANG).fetch();
	}

    public static Persetujuan findByPegawaiPenetapanAkhir(Long pegawaiId, Long lelangId) {
        return find("peg_id=? and lls_id=? and pst_jenis=?", pegawaiId, lelangId, JenisPersetujuan.PEMENANG_LELANG).first();
    }
	
	public static boolean isSedangPersetujuan(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, JenisPersetujuan.PENGUMUMAN_LELANG);
		StatusLelang statusLelang = Lelang_seleksi.findStatusLelang(lelangId);
		return count.intValue() > 0 && statusLelang.isDraft();
	}
	
	public static boolean isSedangPersetujuanPemenang(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, JenisPersetujuan.PEMENANG_LELANG);
		return count.intValue() > 0;
	}

	public boolean isSedangPersetujuan() {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lls_id, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, pst_jenis);
		return count.intValue() > 0;
	}

	public static boolean isSedangPersetujuanBatalTender(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis in (?,?)", lelangId, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, JenisPersetujuan.BATAL_LELANG, JenisPersetujuan.ULANG_LELANG);
		return count.intValue() > 0;
	}

	public static boolean isSedangPersetujuanPraGagalTender(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis in (?,?)", lelangId, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, JenisPersetujuan.PRAKUALIFIKASI_BATAL, JenisPersetujuan.PRAKUALIFIKASI_ULANG);
		return count.intValue() > 0;
	}

	/**
	 * collective collegial berdasarkan perpres
	 * harus terpenuhi 50% + 1
	 *
	 * @param lelangId
	 * @param jenis
     * @return
     */
	public static boolean isApprove(Long lelangId, JenisPersetujuan jenis) {
		Long count = Persetujuan.count("lls_id=? and pst_jenis=?", lelangId, jenis);
		Long countSetuju = Persetujuan.count("lls_id=? and pst_status=? and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, jenis);
		int syaratSetuju = (count.intValue() / 2) + 1; // syarat sesuai aturan perpres (setengah + 1)
		return countSetuju >= syaratSetuju;
	}

	public boolean isApprove(){
		return Persetujuan.isApprove(lls_id,pst_jenis);
	}

	/**
	 * jika belum ada persetujuan cek shortlist ke SIKaP
	 *
	 * @param jenis
	 * @param lelangId
	 * */
	public static boolean allowSendMailSikap(Long lelangId, JenisPersetujuan jenis) {
		Long countSetuju = Persetujuan.count("lls_id=? and pst_status=? and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, jenis);
		return countSetuju == 0;
	}

	public static List<Persetujuan> getByLelangAndSetuju(Long lelangId){
		List<Persetujuan> persetujuanList = find("pst_id in (SELECT " +
				"pst_id FROM persetujuan WHERE lls_id = ? AND pst_status = ? AND pst_jenis=?)"
				, lelangId, StatusPersetujuan.SETUJU, JenisPersetujuan.PEMENANG_LELANG)
				.fetch();

		return persetujuanList;
	}

	public static void deleteAllByLelangAndJenisPengumuman(Long llsId){
		delete("lls_id = ? and pst_jenis = ?",llsId,JenisPersetujuan.PENGUMUMAN_LELANG);
	}

	public static void deleteAllByLelangAndJenisPemenang(Long llsId){
		delete("lls_id = ? and pst_jenis = ?",llsId,JenisPersetujuan.PEMENANG_LELANG);
	}

	public void deleteAllByLelangAndJenis(){
		delete("lls_id = ? and pst_jenis = ?",lls_id,pst_jenis);
	}

	public static void simpanPersetujuanBatalTender(Lelang_seleksi lelang, Boolean setuju, String alasan, JenisPersetujuan jenis){
		createPersetujuan(lelang.lls_id, jenis);
		Persetujuan persetujuan = findByPegawaiLelangAndJenis(lelang.lls_id,jenis);
		if (!setuju && !CommonUtil.isEmpty(alasan)) {
			persetujuan.pst_alasan = alasan;
			persetujuan.pst_status = Persetujuan.StatusPersetujuan.TIDAK_SETUJU; //jika ada alasan maka status tidak setuju
		}else if(setuju) {
			persetujuan.pst_alasan = null;
			persetujuan.pst_status = Persetujuan.StatusPersetujuan.SETUJU;
		}

		persetujuan.lls_id = lelang.lls_id;
		persetujuan.pst_tgl_setuju = BasicCtr.newDate();
		persetujuan.save();

		// tambahkan ke riwayat persetujuan
		Riwayat_Persetujuan.simpanRiwayatPersetujuan(persetujuan);
	}

	public static Persetujuan findByPegawaiLelangAndJenis(Long lls_id, JenisPersetujuan jenis){
		return Persetujuan.find("peg_id=? and lls_id=? and pst_jenis=?", Active_user.current().pegawaiId, lls_id, jenis).first();
	}

	public static Persetujuan findByPegawaiLelangInJenisBatal(Long lls_id){
		return Persetujuan.find("peg_id=? and lls_id=? and pst_jenis in (?,?)", Active_user.current().pegawaiId,
				lls_id, JenisPersetujuan.BATAL_LELANG, JenisPersetujuan.ULANG_LELANG).first();
	}
	
	public static Persetujuan findByPegawaiLelangInJenisBatalPra(Long lls_id){
		return Persetujuan.find("peg_id=? and lls_id=? and pst_jenis in (?,?)", Active_user.current().pegawaiId,
				lls_id, JenisPersetujuan.PRAKUALIFIKASI_BATAL, JenisPersetujuan.PRAKUALIFIKASI_ULANG).first();
	}

	public boolean allowPersetujuanBatal(){
		Long count = Persetujuan.count("lls_id=? and pst_status =? and pst_jenis=? and peg_id=?",
				lls_id, StatusPersetujuan.BELUM_SETUJU, pst_jenis, Active_user.current().pegawaiId);

		return count > 0 && !isApprove();
	}

	/**
	 * Mendapatkan Seluruh informasi Persetujuan Pembatalan
	 * @param lls_id
	 * @return
	 */
	public static List<Persetujuan> findByLelangPembatalan(Long lls_id) {
		return Persetujuan.find("lls_id=? and pst_jenis in (?,?)" , lls_id, JenisPersetujuan.BATAL_LELANG, JenisPersetujuan.ULANG_LELANG).fetch();
	}

	public static void createPersetujuan(Long lelangId, JenisPersetujuan jenis) {
		Panitia panitia = Panitia.findByLelang(lelangId);
		List<Persetujuan> persetujuanList = null;
		if(jenis == JenisPersetujuan.BATAL_LELANG || jenis == JenisPersetujuan.ULANG_LELANG)
			persetujuanList = find("lls_id=? AND (pst_jenis=? OR pst_jenis=?) AND peg_id IN (SELECT peg_id FROM anggota_panitia WHERE pnt_id=?)",
					lelangId, JenisPersetujuan.BATAL_LELANG, JenisPersetujuan.ULANG_LELANG, panitia.pnt_id).fetch();
		else
			persetujuanList = find("lls_id=? AND pst_jenis=? AND peg_id IN (SELECT peg_id FROM anggota_panitia WHERE pnt_id=?)", lelangId, jenis, panitia.pnt_id).fetch();
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return;
		List<Long> listIdAnggota = Query.find("SELECT peg_id FROM anggota_panitia WHERE pnt_id=?", Long.class, panitia.pnt_id).fetch();
		Persetujuan persetujuan = null;
		for(Long anggota:listIdAnggota) {
			persetujuan = Persetujuan.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota, jenis).first();
			if(persetujuan == null) {
				persetujuan = new Persetujuan();
				persetujuan.lls_id = lelangId;
				persetujuan.peg_id = anggota;
				persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
				persetujuan.pst_jenis = jenis;
				persetujuan.save();
			}
		}
		String sql_append = "("+StringUtils.join(listIdAnggota, ",")+")";
		Persetujuan.delete("lls_id=? AND pst_jenis=? AND pst_status=? AND peg_id NOT IN "+sql_append, lelangId, jenis, StatusPersetujuan.BELUM_SETUJU);
	}
}
