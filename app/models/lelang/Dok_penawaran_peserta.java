package models.lelang;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EnumType;

import org.sql2o.ResultSetHandler;
import org.sql2o.data.Row;
import org.sql2o.data.Table;

import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Nilai_evaluasi.StatusNilaiEvaluasi;
import play.Logger;
import play.db.jdbc.DefaultEnumConverterFactory;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

public class Dok_penawaran_peserta implements Serializable {
	
	public Long psr_id;
	
	public String rkn_nama;
	
	public Date psr_tanggal;
	
	public Dok_penawaran dok_kualifikasi;
	
	public Dok_penawaran dok_tambahan;
	
	public Dok_penawaran dok_susulan;
	
	public Dok_penawaran dok_teknis;
	
	public Dok_penawaran dok_harga;
	
	public Dok_penawaran dok_teknis_harga; // untuk tender v3
	
	public StatusNilaiEvaluasi nilai_teknis;
	
	public boolean isLulusTeknis() {
		return nilai_teknis != null && nilai_teknis.isLulus();
	}
		
	
	public static List<Dok_penawaran_peserta> findByLelang(Long lelangId) {
		QueryBuilder query = new QueryBuilder("SELECT p.psr_id, rkn_nama, psr_tanggal, n.nev_lulus AS nilai_teknis, k.dok_id AS k_dok_id, k.dok_jenis AS k_dok_jenis, k.dok_id_attachment AS k_dok_id_attachment, k.dok_disclaim AS k_dok_disclaim,");
		query.append("s.dok_id AS s_dok_id, s.dok_jenis AS s_dok_jenis, s.dok_id_attachment AS s_dok_id_attachment, s.dok_disclaim AS s_dok_disclaim,");
		query.append("t.dok_id AS t_dok_id, t.dok_jenis AS t_dok_jenis, t.dok_id_attachment AS t_dok_id_attachment, t.dok_disclaim AS t_dok_disclaim, ");
		query.append("adm.dok_id AS adm_dok_id, adm.dok_jenis AS adm_dok_jenis, adm.dok_id_attachment AS adm_dok_id_attachment, adm.dok_disclaim AS adm_dok_disclaim, adm.dok_waktu AS adm_dok_waktu, adm.dok_tgljam AS adm_dok_tgljam,");
		query.append("h.dok_id AS h_dok_id, h.dok_jenis AS h_dok_jenis, h.dok_id_attachment AS h_dok_id_attachment, h.dok_disclaim AS h_dok_disclaim, h.dok_waktu AS h_dok_waktu, h.dok_tgljam AS h_dok_tgljam,");
		query.append("adm_harga.dok_id AS adm_harga_dok_id, adm_harga.dok_jenis AS adm_harga_dok_jenis, adm_harga.dok_id_attachment AS adm_harga_dok_id_attachment, adm_harga.dok_disclaim AS adm_harga_dok_disclaim, adm_harga.dok_waktu AS adm_harga_dok_waktu,adm_harga.dok_tgljam AS adm_harga_dok_tgljam");
		query.append("FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id");
		query.append("LEFT JOIN (SELECT * FROM dok_penawaran WHERE dok_jenis = 0) k ON p.psr_id=k.psr_id");
		query.append("LEFT JOIN (SELECT * FROM dok_penawaran WHERE dok_jenis = 4) s ON p.psr_id=s.psr_id");
		query.append("LEFT JOIN (SELECT * FROM dok_penawaran WHERE dok_jenis = 5) t ON p.psr_id=t.psr_id");
		query.append("LEFT JOIN (SELECT * FROM dok_penawaran WHERE dok_jenis = 1) adm ON p.psr_id=adm.psr_id");
		query.append("LEFT JOIN (SELECT * FROM dok_penawaran WHERE dok_jenis = 2) h ON p.psr_id=h.psr_id");
		query.append("LEFT JOIN (SELECT * FROM dok_penawaran WHERE dok_jenis = 3) adm_harga ON p.psr_id=adm_harga.psr_id");
		query.append("LEFT JOIN (SELECT * FROM nilai_evaluasi WHERE eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 2 ORDER BY eva_versi DESC LIMIT 1)) n ON n.psr_id=p.psr_id", lelangId);
		query.append("WHERE p.lls_id=? ORDER BY psr_id, k.dok_tgljam, adm.dok_tgljam, h.dok_tgljam, adm_harga.dok_tgljam", lelangId);
//		Logger.info("query : %s", query.query());
		Table table = Query.findTable(query);
		List<Dok_penawaran_peserta> list = new ArrayList<Dok_penawaran_peserta>();
		for(Row row : table.rows()) {
			Dok_penawaran_peserta result = new Dok_penawaran_peserta();
			result.psr_id = row.getLong("psr_id");
			result.rkn_nama = row.getString("rkn_nama");
			result.psr_tanggal = row.getDate("psr_tanggal");
			result.nilai_teknis = StatusNilaiEvaluasi.fromValue(row.getInteger("nilai_teknis"));
			Dok_penawaran kualifikasi = new Dok_penawaran();
			kualifikasi.dok_id = row.getLong("k_dok_id");
			kualifikasi.dok_jenis = JenisDokPenawaran.fromValue(row.getInteger("k_dok_jenis"));
			kualifikasi.dok_disclaim = row.getInteger("k_dok_disclaim");
			kualifikasi.dok_id_attachment = row.getLong("k_dok_id_attachment");
			result.dok_kualifikasi = kualifikasi;
			Dok_penawaran tambahan = new Dok_penawaran();
			tambahan.dok_id = row.getLong("t_dok_id");
			tambahan.dok_jenis = JenisDokPenawaran.fromValue(row.getInteger("t_dok_jenis"));
			tambahan.dok_disclaim = row.getInteger("t_dok_disclaim");
			tambahan.dok_id_attachment = row.getLong("t_dok_id_attachment");
			result.dok_tambahan = tambahan;
			Dok_penawaran susulan = new Dok_penawaran();
			susulan.dok_id = row.getLong("s_dok_id");
			susulan.dok_jenis = JenisDokPenawaran.fromValue(row.getInteger("s_dok_jenis"));
			susulan.dok_disclaim = row.getInteger("s_dok_disclaim");
			susulan.dok_id_attachment = row.getLong("s_dok_id_attachment");
			result.dok_susulan = susulan;
			Dok_penawaran adminteknis = new Dok_penawaran();
			adminteknis.dok_id = row.getLong("adm_dok_id");
			adminteknis.dok_jenis = JenisDokPenawaran.fromValue(row.getInteger("adm_dok_jenis"));
			adminteknis.dok_disclaim = row.getInteger("adm_dok_disclaim");
			adminteknis.dok_id_attachment = row.getLong("adm_dok_id_attachment");
			adminteknis.dok_waktu = row.getInteger("adm_dok_waktu");
			adminteknis.dok_tgljam = row.getDate("adm_dok_tgljam");
			result.dok_teknis = adminteknis;
			Dok_penawaran harga = new Dok_penawaran();
			harga.dok_id = row.getLong("h_dok_id");
			harga.dok_jenis = JenisDokPenawaran.fromValue(row.getInteger("h_dok_jenis"));
			harga.dok_disclaim = row.getInteger("h_dok_disclaim");
			harga.dok_id_attachment = row.getLong("h_dok_id_attachment");
			harga.dok_waktu = row.getInteger("h_dok_waktu");
			harga.dok_tgljam = row.getDate("h_dok_tgljam");
			result.dok_harga = harga;
			Dok_penawaran admharga = new Dok_penawaran();
			admharga.dok_id = row.getLong("adm_harga_dok_id");
			admharga.dok_jenis = JenisDokPenawaran.fromValue(row.getInteger("adm_harga_dok_jenis"));
			admharga.dok_disclaim = row.getInteger("adm_harga_dok_disclaim");
			admharga.dok_id_attachment = row.getLong("adm_harga_dok_id_attachment");
			admharga.dok_waktu = row.getInteger("h_dok_waktu");
			admharga.dok_tgljam = row.getDate("adm_harga_dok_tgljam");
			result.dok_teknis_harga = admharga;
			list.add(result);
		}		
		return list;
	}
}
