package models.lelang;

import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * @author Erwin RM-Oktober
 */

@Table(name="LELANG_NASIONAL")
public class Lelang_nasional extends BaseModel {
	
	@Id
	public Long lls_id;

	public String nama_lpse;

	public String url_lpse;

	public String nama_paket;

	public String nama_agency;

	public String nama_satker;

	public Double pagu;

	public String tgl_download_dokumen;

	public int status_lelang;

	public String kualifikasi_usaha;

	public Date tgl_buat_lelang;

	public int status_eproc;

	public Double hps;

	public boolean equals(Object object) {
		if (!(object instanceof Lelang_nasional)) {
			return false;
		}
		Lelang_nasional rhs = (Lelang_nasional) object;
		return new EqualsBuilder().append(this.nama_lpse, rhs.nama_lpse)
				.append(this.url_lpse, rhs.url_lpse)
				.append(this.nama_paket, rhs.nama_paket)
				.append(this.nama_agency, rhs.nama_agency)
				.append(this.nama_satker, rhs.nama_satker)
				.append(this.pagu, rhs.pagu)
				.append(this.tgl_download_dokumen, rhs.tgl_download_dokumen)
				.append(this.status_lelang, rhs.status_lelang)
				.append(this.kualifikasi_usaha, rhs.kualifikasi_usaha)
				.append(this.tgl_buat_lelang, rhs.tgl_buat_lelang)
				.append(this.status_eproc, rhs.status_eproc)
				.append(this.hps, rhs.hps).append(this.lls_id, rhs.lls_id)
				.isEquals();
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return new HashCodeBuilder(-82280557, -700257973)
				.append(this.nama_lpse).append(this.url_lpse)
				.append(this.nama_paket).append(this.nama_agency)
				.append(this.nama_satker).append(this.pagu)
				.append(this.tgl_download_dokumen).append(this.status_lelang)
				.append(this.kualifikasi_usaha).append(this.tgl_buat_lelang)
				.append(this.status_eproc).append(this.hps).append(this.lls_id)
				.toHashCode();
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return new ToStringBuilder(this).append("nama_lpse", this.nama_lpse)
				.append("url_lpse", this.url_lpse)
				.append("nama_paket", this.nama_paket)
				.append("nama_agency", this.nama_agency)
				.append("nama_satker", this.nama_satker)
				.append("pagu", this.pagu)
				.append("tgl_download_dokumen", this.tgl_download_dokumen)
				.append("status_lelang", this.status_lelang)
				.append("kualifikasi_usaha", this.kualifikasi_usaha)
				.append("tgl_buat_lelang", this.tgl_buat_lelang)
				.append("status_eproc", this.status_eproc)
				.append("hps", this.hps).append("lls_id", this.lls_id)
				.toString();
	}

	public Object getPrimaryKey() {
		return this.nama_lpse;
	}
}
