package models.lelang;

import controllers.BasicCtr;
import ext.DatetimeBinder;
import ext.FormatUtils;
import ext.RupiahBinder;
import models.common.Active_user;
import models.common.MetodePemilihan;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.Date;
import java.util.List;

@Table(name = "reverse_auction")
public class ReverseAuction extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum StatusReverseAuction  {
        EDITABLE, NON_EDITABLE, SELESAI;

        public boolean isEditable() {
            return this == EDITABLE;
        }

        public boolean isNonEditable() {
            return this == NON_EDITABLE;
        }

        public boolean isSelesai() {
            return this == SELESAI;
        }
    }

    @Id(sequence = "seq_reverse_auction", function = "nextsequence")
    public Long ra_id;

    @Required
    public Long lls_id;

    @Required
    @As(binder = DatetimeBinder.class)
    public Date ra_waktu_mulai;

    @Required
    @As(binder = DatetimeBinder.class)
    public Date ra_waktu_selesai;

//    @Required
    @As(binder = RupiahBinder.class)
    public Double ra_min_penawaran;

    public String ra_alasan; // alasan

    public StatusReverseAuction ra_status;

    public Integer ra_versi;

//    public String ra_penawaran;

    public static ReverseAuction findByLelang(Long id) {
        Integer versi = Evaluasi.findCurrentVersi(id);
        return find("lls_id=? AND ra_versi=? order by ra_versi desc", id, versi).first();
    }

    public static ReverseAuction create(Lelang_seleksi lelang, Integer versi) {
        ReverseAuction auction = find("lls_id=? AND ra_versi=?", lelang.lls_id, versi).first();
        try {
            if(auction == null) {
                auction = new ReverseAuction();
                auction.lls_id = lelang.lls_id;
                auction.ra_versi = versi;
            }
//            Tahap tahap = Tahap.PENETAPAN_PEMENANG_AKHIR;
            if (lelang.getPemilihan().isLelangExpress()) {
                Tahap tahap = Tahap.PEMASUKAN_PENAWARAN;
                Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, tahap);
                if (jadwal != null) {
                    auction.ra_waktu_mulai = jadwal.dtj_tglawal;
                    auction.ra_waktu_selesai = jadwal.dtj_tglakhir;
                }
            }
           /* else if (lelang.getMetode().dokumen.isDuaTahap()) {
                tahap = Tahap.PEMASUKAN_PENAWARAN_BIAYA;
            }*/
            // setup batas minimal penawaran
            auction.ra_min_penawaran = lelang.getPaket().pkt_hps;
            auction.ra_status = StatusReverseAuction.NON_EDITABLE;
            if (!lelang.isExpress()) {
                Nilai_evaluasi nilai_termurah = null;
                Evaluasi evaluasi = Evaluasi.findHarga(lelang.lls_id);
                if (lelang.isPrakualifikasi()) {// prakualifikasi, ambil harga terendah dari peserta lulus evaluasi harga
                    nilai_termurah = Nilai_evaluasi.find("eva_id=? AND nev_lulus=1 ORDER BY nev_harga_terkoreksi", evaluasi.eva_id).first();
                } else { // pasca, ambil harga terendah dari peserta lulus pembuktian
                    Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
                    nilai_termurah = Nilai_evaluasi.find("eva_id=? AND psr_id IN (SELECT psr_id FROM nilai_evaluasi WHERE eva_id=? AND nev_lulus=1) ORDER BY nev_harga_terkoreksi", evaluasi.eva_id, pembuktian.eva_id).first();
                }
                auction.ra_min_penawaran = nilai_termurah != null ? nilai_termurah.nev_harga_terkoreksi:Integer.valueOf(0);
                auction.ra_status = StatusReverseAuction.EDITABLE;
            }
            auction.save();
        }catch (Exception e){
            Logger.error(e, "Gagal simpan Reverse Auction Kode Tender %s", lelang.lls_id);
        }
        return auction;
    }

    public boolean isSelesai() {
        return ra_waktu_selesai != null && ra_waktu_selesai.before(BasicCtr.newDate());
    }

    public boolean isSedangProses() {
        return (ra_waktu_selesai != null && ra_waktu_selesai.after(BasicCtr.newDate())) &&
                (ra_waktu_mulai  != null && ra_waktu_mulai.before(BasicCtr.newDate()));
    }

    public List<Peserta> getPeserta() {
        if(Lelang_seleksi.findPemilihan(lls_id).isLelangExpress())
            return Peserta.findBylelang(lls_id);
        // peserta yg lolos sampai tahapan evaluasi akhir / Penetapan pemenang
        Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lls_id);
        return Peserta.find("lls_id=? AND psr_id in (select psr_id from nilai_evaluasi where eva_id=?)", lls_id, penetapan.eva_id).fetch();
    }

    // dapatkan informasi waktu reverse auction
    public String getWaktu() {
        return FormatUtils.formatDateTimeInd(ra_waktu_mulai) + " s.d. " + FormatUtils.formatDateTimeInd(ra_waktu_selesai);
    }

    public String sisaWaktu() {
        String result = "";
        Date date = controllers.BasicCtr.newDate();
        if (ra_waktu_selesai != null && ra_waktu_selesai.after(date)) {
            DateTime now = new DateTime(date.getTime());
            DateTime jakhir = new DateTime(ra_waktu_selesai.getTime());
            Period period = new Period(now, jakhir);
            if (period.getDays() > 0) {
                result += period.getDays() + " hari";
            }
            if (period.getHours() > 0) {
                if (!StringUtils.isEmpty(result))
                    result += " / ";
                result += period.getHours() + " jam";
            }
            if (!StringUtils.isEmpty(result))
                result += " / ";
            result += period.getMinutes() + " menit";
            if (!StringUtils.isEmpty(result))
                result += " / ";
            result += period.getSeconds() + " detik";
        } else
            return "Waktu Pengiriman Habis";
        return result;
    }

    public void sendPenawaranPeserta() {
        Lelang_seleksi lelang = Lelang_seleksi.findById(lls_id);
        if (lelang.getPemilihan().isLelangExpress()) { // jika express, nilai evaluasi harga dan akhir
            List<Peserta> pesertaList = Peserta.findBylelang(lls_id);
            int jumlahPenawaran = 0;
            if (!CollectionUtils.isEmpty(pesertaList)) {
                ReverseAuctionDetil termurah = ReverseAuctionDetil.findPenawaranTermurah(ra_id);
                Peserta calongPemenang = null; // untuk menyimpan data pemenang, jika penawaran lebih dari 1
                for (Peserta peserta : pesertaList) {
                    ReverseAuctionDetil reverseAuctionDetil = ReverseAuctionDetil.find("psr_id=? AND ra_id = ? order by rad_id DESC", peserta.psr_id, ra_id).first();
                    if (reverseAuctionDetil != null) {
                            if(reverseAuctionDetil.rad_nev_harga != null & reverseAuctionDetil.rad_nev_harga > 0) {
                                peserta.dkh = reverseAuctionDetil.dkh;
                                peserta.psr_harga = reverseAuctionDetil.rad_nev_harga;
                                peserta.psr_harga_terkoreksi = reverseAuctionDetil.rad_nev_harga;
                                peserta.is_pemenang = Integer.valueOf(0);
                                if (termurah != null && peserta.psr_id.equals(termurah.psr_id)) {
                                    calongPemenang = peserta;
                                }
                                jumlahPenawaran++;
                            }

                    } else {
                        peserta.dkh = null;
                        peserta.psr_harga = null;
                        peserta.psr_harga_terkoreksi = null;
                    }
                    peserta.save();
                }
                // set pemenang jika jumlah penawaran > 1
                if(jumlahPenawaran > 1 && calongPemenang != null) {
                    calongPemenang.is_pemenang = Integer.valueOf(1);
                    calongPemenang.save();
                }
            }
            Evaluasi harga = null, penetapan = null;
            Nilai_evaluasi nilai_harga = null, nilai_akhir = null;
            if (harga == null)
                harga = Evaluasi.findNCreateHarga(lelang.lls_id);
            if (penetapan == null)
                penetapan = Evaluasi.findNCreatePenetapanPemenang(lelang.lls_id);
            // langsung diset sistem, langsung dapat pemenangny
            List<Peserta> pesertaTerurut = Peserta.findBylelang(lls_id);
            int urutan = 1;
            for (Peserta peserta : pesertaTerurut) {
                if(peserta.psr_harga == null || peserta.psr_harga == 0)
                    continue;
                nilai_harga = Nilai_evaluasi.findNCreateBy(harga.eva_id, peserta, harga.eva_jenis);
                // penawar lebih dari 3 dan harga melebihi HPS maka tidak lulus
                nilai_harga.nev_harga = peserta.psr_harga;
                nilai_harga.nev_harga_terkoreksi = peserta.psr_harga_terkoreksi;
                nilai_harga.nev_lulus = nilai_harga.isHargaOverHps(lelang.getPaket().pkt_hps) || nilai_harga.nev_harga == 0 ? Nilai_evaluasi.StatusNilaiEvaluasi.TDK_LULUS : Nilai_evaluasi.StatusNilaiEvaluasi.LULUS;
                nilai_harga.save();
                if (penetapan != null) {
                    nilai_akhir = Nilai_evaluasi.findNCreateBy(penetapan.eva_id, peserta, penetapan.eva_jenis);
                    nilai_harga = Nilai_evaluasi.findBy(harga.eva_id, nilai_akhir.psr_id);
                    if (nilai_harga.isLulus()) {
                        nilai_akhir.nev_urutan = urutan;
                        nilai_akhir.nev_lulus = nilai_akhir.nev_urutan == 1 && jumlahPenawaran > 1 ? Nilai_evaluasi.StatusNilaiEvaluasi.LULUS:Nilai_evaluasi.StatusNilaiEvaluasi.TDK_LULUS;
                        nilai_akhir.save();
                    } else {
                        nilai_akhir.delete();//hapus evaluasi akhir untuk peserta yang tidak lulus
                        // agar tidak muncul di penetapan pemenang
                    }
                }
                urutan++;
            }
//            if (penetapan != null) {
//                penetapan.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
//                penetapan.eva_tgl_setuju = BasicCtr.newDate();
//                penetapan.save();
//            }
        } else {// proses submit hasil Negosiasi
            Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lls_id);
            if (penetapan != null) {
                List<Nilai_evaluasi> pesertaList = penetapan.findPesertaList();
                if (!CollectionUtils.isEmpty(pesertaList)) {
                    for (Nilai_evaluasi peserta : pesertaList) {
                        ReverseAuctionDetil reverseAuctionDetil = ReverseAuctionDetil.find("psr_id=? AND ra_id = ? order by rad_id DESC", peserta.psr_id, ra_id).first();
                        if (reverseAuctionDetil != null) {
                            peserta.nev_harga_negosiasi = reverseAuctionDetil.rad_nev_harga;
                        } else {
                            peserta.nev_harga_negosiasi = peserta.nev_harga_terkoreksi;
                        }
                        peserta.save();
                    }
                    // set urutan
                    pesertaList = penetapan.findPesertaWithUrutNego();
                    int i = 1;
                    for (Nilai_evaluasi peserta : pesertaList) {
                        peserta.nev_urutan = i;
                        // pokja nanti yg melakukan penetapan pemenang
//                        peserta.nev_lulus = peserta.psr_id.equals(termurah.psr_id) ? Nilai_evaluasi.StatusNilaiEvaluasi.LULUS : Nilai_evaluasi.StatusNilaiEvaluasi.TDK_LULUS;
                        peserta.save();
                        i++;
                    }
                }
            }
        }
    }

    public boolean isEditable() {
        Active_user active_user = Active_user.current();
        if(ra_status.isSelesai())
            return false;
        else if(active_user.isPanitia()) { // editable == true, berarti pokja bisa edit waktu dan alasan (jika reverse ulang)
            MetodePemilihan metodePemilihan = Lelang_seleksi.findPemilihan(lls_id);
            return !metodePemilihan.isLelangExpress();
        } else if(active_user.isRekanan()) { // editable == true, berarti auction sudah dimulai
            return isSedangProses() && ra_min_penawaran != null && ra_status.isNonEditable();
        }
        return false;
    }

    public boolean isNeedAlasan() {
        return ra_waktu_mulai != null && ra_waktu_selesai != null && ra_status != null && ra_status.isNonEditable();
    }

    public boolean isJadwalNotNotNull() {
        return ra_waktu_mulai != null && ra_waktu_selesai != null;
    }

    public List<ReverseAuctionDetil> hasilAkhir() {
        return Query.find("SELECT r.* FROM (SELECT psr_id, max(rad_id) as rad_id FROM reverse_auction_detail WHERE ra_id=? GROUP BY psr_id) last, " +
                "reverse_auction_detail r WHERE last.rad_id=r.rad_id ORDER BY rad_nev_harga", ReverseAuctionDetil.class, ra_id).fetch();
    }

    public ReverseAuctionDetil termurah() {
        return ReverseAuctionDetil.findPenawaranTermurah(ra_id);
    }

    public String getStatus() {
        if(ra_status.isNonEditable() && isSelesai())
            return Messages.get("lelang.rva_sdl");
        if(ra_status.isSelesai() && isSelesai())
            return Messages.get("lelang.rva_ss");
        if(ra_status.isNonEditable() && isSedangProses())
            return Messages.get("lelang.rva_dm");
        if(ra_status.isEditable() || (ra_status.isNonEditable() && !isSedangProses()))
            return Messages.get("lelang.rva_bdm");
        return "";
    }

    public long getJumlahPenawar() {
        return Query.count("select count(distinct psr_id) from reverse_auction_detail where ra_id=?", ra_id);
    }

    // validasi waktu auction oleh pokja
    public void validate(Date currentDate) throws Exception {
        if(ra_waktu_selesai == null) {
            throw new Exception(Messages.get("lelang.tmbt"));
        }
        else if(ra_waktu_selesai == null) {
            throw new Exception(Messages.get("lelang.tsbt"));
        }
        else if(ra_waktu_mulai != null && ra_waktu_mulai.before(currentDate)) {
            throw new Exception(Messages.get("lelang.tmyamsdl"));
        }
        else if(ra_waktu_selesai != null && ra_waktu_selesai.before(currentDate)){
            throw new Exception(Messages.get("lelang.tayamsdl"));
        }
        else if(ra_waktu_selesai.before(ra_waktu_mulai)) {
            throw new Exception(Messages.get("lelang.tsmdtm"));
        }
        List<Jadwal> list = Jadwal.findByLelangNTahap(lls_id, Tahap.PENETAPAN_PEMENANG_AKHIR, Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR);
        if(!CollectionUtils.isEmpty(list)) {
            Jadwal jadwal = list.get(0);
            if(jadwal != null && ra_waktu_selesai.compareTo(jadwal.dtj_tglakhir) == 0) {
                throw new Exception(Messages.get("lelang.baj_rva"));
            }
        }
    }

    // validasi penawaran yg masuk
    public void validatePenawaran(Peserta peserta, Double nilai_total) throws Exception {
        ReverseAuctionDetil lastPenawaran = ReverseAuctionDetil.findLastPenawaran(ra_id, peserta.psr_id);
        if (ra_min_penawaran != null && ra_min_penawaran > 0.0 && ra_min_penawaran <= nilai_total) {
            Lelang_seleksi lelang = Lelang_seleksi.findById(lls_id);
            String nilaiBatas = lelang.isExpress() ? Messages.get("lelang.nilai_hps") : Messages.get("lelang.npt");
            String message = Messages.get("lelang.phlrd")+" " + nilaiBatas;
            throw new Exception(message);
        } else if (lastPenawaran != null && lastPenawaran.rad_nev_harga > 0.0 && lastPenawaran.rad_nev_harga <= nilai_total) {
            throw new Exception(Messages.get("lelang.phlrdps"));
        } else if (ReverseAuctionDetil.count("ra_id=? AND rad_nev_harga =?", ra_id, nilai_total) > 0) {
            throw new Exception(Messages.get("lelang.ptbsdppl"));
        }
    }
}
