package models.lelang;

import ext.FormatUtils;
import models.nonlelang.EvaluasiPl;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;
import play.i18n.Messages;

import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

/**
 * Model hasil Evaluasi Peserta
 * @author arif ardiyansah
 *
 */
public class HasilEvaluasi implements Serializable{

	public Long psr_id;
	public Long rkn_id;
	public String rkn_nama;
    public String rkn_npwp;
	public Double psr_harga;
	public Double psr_harga_terkoreksi;
	public Integer harga; // status evaluasi harga
	public Integer kualifikasi; // status evaluasi kualifikasi
	public String als_kualifikasi;
	public Integer pembuktian; // status pembuktian
	public String als_pembuktian;
	public Integer teknis; // status evaluasi teknis
	public String als_teknis;
	public Integer administrasi; // status evaluasi administrasi
	public String als_administrasi;
	public String als_harga;
	public Integer pemenang; // status evaluasi akhir
	public Integer pemenang_verif; // status evaluasi akhir terverifikasi
	public Double nev_harga;
	public Double nev_harga_terkoreksi;
	public Double nev_harga_negosiasi;
	public Double skor;
	public Double skor_pembuktian;
	public Double skor_teknis;
    public Double skor_harga;
	public Double skor_akhir;
	public Long fileHarga;
	public String psr_alasan_menang;
	public String psr_dkh;
	public Double pkt_pagu;
	public Double pkt_hps;

	public boolean isLulusAdmin() {
		return administrasi != null && administrasi == 1;
	}

	public boolean isTdkLulusAdmin() {
		return administrasi != null && administrasi == 0;
	}
	
	public boolean isLulusTeknis() {
		return teknis != null && teknis == 1;
	}
	
	public boolean isLulusHarga() {
		return harga != null && harga == 1;
	}
	
	public boolean isLulusPembuktian() {
		return pembuktian != null && pembuktian == 1;
	}
	
	public boolean isLulusKualifikasi() {
		return kualifikasi != null && kualifikasi == 1;
	}
	
	public boolean isPemenang() {
		return pemenang != null && pemenang == 1;
	}

	public boolean isBisaMenang() {
		return isLulusAdmin()
				&& isLulusKualifikasi()
				&& isLulusHarga()
				&& isLulusTeknis()
                ;
	}
	
	public boolean isVerified(){
		Peserta peserta = Peserta.findById(psr_id);
		return peserta.isSudahVerifikasiSikap();
	}
	
	public String getPenawaran(){
		if(nev_harga != null)
			return FormatUtils.formatCurrencyRupiah(nev_harga);
		return FormatUtils.formatCurrencyRupiah(psr_harga);
	}
	
	public String getPenawaranTerkoreksi(){
		if(nev_harga_terkoreksi != null)
			return FormatUtils.formatCurrencyRupiah(nev_harga_terkoreksi);
		return FormatUtils.formatCurrencyRupiah(psr_harga_terkoreksi);
	}

	//untuk PL
	public boolean isUploadPenawaranHargaPl(){
		return (psr_dkh != null && !psr_dkh.isEmpty());
	}
	
	public String getAlasan(){
		String alasan="";
		if(!StringUtils.isEmpty(als_administrasi))
			alasan += als_administrasi;
		if(!StringUtils.isEmpty(als_teknis))
			alasan += als_teknis;
		if(!StringUtils.isEmpty(als_harga))
			alasan += als_harga;
		if(!StringUtils.isEmpty(als_kualifikasi))
			alasan += als_kualifikasi;
		if(!StringUtils.isEmpty(als_pembuktian))
			alasan += als_pembuktian;
		if(!StringUtils.isEmpty(psr_alasan_menang))
			alasan += psr_alasan_menang;
		return alasan;
	}

	public String getAlasanKualifikasi(){
		String alasan="";
		if(!StringUtils.isEmpty(als_kualifikasi))
			alasan += als_kualifikasi;
		if(!StringUtils.isEmpty(als_pembuktian))
			alasan += als_pembuktian;
		return alasan;
	}

	public String getAlasanTeknis(){
		String alasan="";
		if(!StringUtils.isEmpty(als_administrasi))
			alasan += als_administrasi;
		if(!StringUtils.isEmpty(als_teknis))
			alasan += als_teknis;
		return alasan;
	}
	
	/**
	 * Method ini sebagai wrapper skor kualifikasi karena lelang v3 menggunakan eva_jenis 0 untuk kualifikasi
	 * sedangkan lelang v4 menggunakan eva_jenis 5 
	 * @return
	 */
	public Double getSkorKualifikasi() {
		if(skor_pembuktian != null && skor_pembuktian > 0.0) return skor_pembuktian;
		if(skor != null && skor > 0.0) return skor;
		return 0.0;
	}

	public String getHargaNegosiasi() {
		if(nev_harga_negosiasi != null)
			return FormatUtils.formatCurrencyRupiah(nev_harga_negosiasi);
		return "";
	}

	public String getHargaFinal(){
		Double hargaFinal =  nev_harga_negosiasi != null ? nev_harga_negosiasi : nev_harga_terkoreksi;
		if(hargaFinal != null)
			return FormatUtils.formatCurrencyRupiah(hargaFinal);
		return "";
	}

	public String getNotifHargaPenawaran() {
		StringJoiner notif = new StringJoiner("");
		if((nev_harga != null && nev_harga > pkt_hps) || (psr_harga != null && psr_harga >  pkt_hps))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.mnt_hps") + "</span>");
		if((nev_harga != null && nev_harga > pkt_pagu) || (psr_harga != null && psr_harga >  pkt_pagu))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.m_pagu") + "</span>");
		return notif.toString();
	}

	public String getNotifHargaPenawaranTerkoreksi() {
		StringJoiner notif = new StringJoiner("");
		if((nev_harga_terkoreksi != null && nev_harga_terkoreksi > pkt_hps) || (psr_harga_terkoreksi != null && psr_harga_terkoreksi >  pkt_hps))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.mnt_hps") + "</span>");
		if((nev_harga_terkoreksi != null && nev_harga_terkoreksi > pkt_pagu) || (psr_harga_terkoreksi != null && psr_harga_terkoreksi >  pkt_pagu))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.m_pagu") + "</span>");
		return notif.toString();
	}

	public String getNotifHargaNegosiasi() {
		StringJoiner notif = new StringJoiner("");
		if(nev_harga_terkoreksi != null && nev_harga_negosiasi!=null && nev_harga_negosiasi > pkt_hps)
			notif.add("<span class=\"badge  badge-danger\">Melebihi Nilai Total HPS</span>");
		if(nev_harga_terkoreksi != null && nev_harga_negosiasi!=null && nev_harga_negosiasi > pkt_pagu)
			notif.add("<span class=\"badge  badge-danger\">Melebihi Pagu</span>");
		return notif.toString();
	}
	
	 /**
	  * untuk dapatkan resume hasil evaluasi lelang terakhir
	  * @param lelangId
	  * @return
	  */	
	public static List<HasilEvaluasi> findByLelang(Long lelangId) {
		QueryBuilder builder = QueryBuilder.create("SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp, psr_harga, psr_harga_terkoreksi, d.dok_id as fileHarga, k.nev_lulus AS kualifikasi, k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi, a.nev_lulus AS administrasi, a.nev_uraian AS als_administrasi,")
		.append("t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga, h.nev_skor as skor_harga,  h.nev_harga, h.nev_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif, pkt_hps, pkt_pagu,")
		.append("h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, q.nev_skor as skor_pembuktian, q.nev_uraian as als_pembuktian, s.nev_skor AS skor_akhir, psr_alasan_menang, s.nev_harga_negosiasi FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id LEFT JOIN dok_penawaran d ON p.psr_id=d.psr_id AND d.dok_jenis in (2,3)")
		.append("LEFT JOIN (SELECT * FROM nilai_evaluasi n, (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 5 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) q ON p.psr_id=q.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM nilai_evaluasi n, (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 0 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) k ON p.psr_id=k.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM nilai_evaluasi n, (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 1 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) a ON p.psr_id=a.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM nilai_evaluasi n, (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 2 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) t ON p.psr_id=t.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM nilai_evaluasi n, (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 3 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) h ON p.psr_id=h.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM nilai_evaluasi n, (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = 4 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) s ON p.psr_id=s.psr_id", lelangId)
		.append("LEFT JOIN lelang_seleksi l ON p.lls_id=l.lls_id LEFT JOIN paket pkt ON pkt.pkt_id=l.pkt_id WHERE p.lls_id=? ORDER BY nev_harga asc", lelangId);
		return Query.findList(builder, HasilEvaluasi.class);
	}

	public static HasilEvaluasi findByPeserta(Long pesertaId) {
		String sql = String.join(" ", "SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp,psr_harga, psr_harga_terkoreksi, d.dok_id as fileHarga, k.nev_lulus AS kualifikasi, k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi, a.nev_lulus AS administrasi, a.nev_uraian AS als_administrasi,",
                "t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga, h.nev_skor as skor_harga,  h.nev_harga, h.nev_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif, pkt_hps, pkt_pagu, ",
                "h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, q.nev_skor as skor_pembuktian, q.nev_uraian as als_pembuktian, s.nev_skor AS skor_akhir FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id LEFT JOIN dok_penawaran d ON p.psr_id=d.psr_id AND d.dok_jenis in (2,3)",
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 5 ) q ON p.psr_id=q.psr_id",
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 0 ) k ON p.psr_id=k.psr_id",
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 1 ) a ON p.psr_id=a.psr_id",
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 2 ) t ON p.psr_id=t.psr_id",
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 3 ) h ON p.psr_id=h.psr_id",
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND eva_jenis = 4 ) s ON p.psr_id=s.psr_id",
				"LEFT JOIN lelang_seleksi l ON p.lls_id=l.lls_id LEFT JOIN paket pkt ON pkt.pkt_id=l.pkt_id WHERE p.psr_id=:id");
		return Query.find(sql, HasilEvaluasi.class).setParameter("id", pesertaId).first();
	}

	public static List<HasilEvaluasi> findByPl(Long lelangId) {
		String sql = String.join(" ","SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp,psr_harga, psr_harga_terkoreksi,k.nev_lulus AS kualifikasi,",
				"k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi, a.nev_lulus AS administrasi, a.nev_uraian AS als_administrasi,",
                "t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga, h.nev_skor as skor_harga,",
				"h.nev_harga, h.nev_harga_terkoreksi, s.nev_lulus AS pemenang, p.is_pemenang_verif AS pemenang_verif,",
                "h.nev_uraian AS als_harga, q.nev_lulus as pembuktian, s.nev_skor AS skor_akhir, psr_alasan_menang, psr_dkh, s.nev_harga_negosiasi",
				"FROM ekontrak.peserta_nonlelang p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id",
                "LEFT JOIN (SELECT * FROM ekontrak.nilai_evaluasi n, ekontrak.evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 5 AND eva_versi=:versi ) q ON p.psr_id=q.psr_id",
                "LEFT JOIN (SELECT * FROM ekontrak.nilai_evaluasi n, ekontrak.evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 0 AND eva_versi=:versi ) k ON p.psr_id=k.psr_id",
                "LEFT JOIN (SELECT * FROM ekontrak.nilai_evaluasi n, ekontrak.evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 1 AND eva_versi=:versi ) a ON p.psr_id=a.psr_id",
                "LEFT JOIN (SELECT * FROM ekontrak.nilai_evaluasi n, ekontrak.evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 2 AND eva_versi=:versi ) t ON p.psr_id=t.psr_id",
                "LEFT JOIN (SELECT * FROM ekontrak.nilai_evaluasi n, ekontrak.evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 3 AND eva_versi=:versi ) h ON p.psr_id=h.psr_id",
                "LEFT JOIN (SELECT * FROM ekontrak.nilai_evaluasi n, ekontrak.evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 4 AND eva_versi=:versi ) s ON p.psr_id=s.psr_id",
				"LEFT JOIN ekontrak.nonlelang_seleksi l ON p.lls_id=l.lls_id LEFT JOIN ekontrak.paket pkt ON pkt.pkt_id=l.pkt_id WHERE p.lls_id=:lls_id ORDER BY nev_harga asc");
		int versi  = EvaluasiPl.findCurrentVersi(lelangId);
		return Query.find(sql, HasilEvaluasi.class).setParameter("lls_id", lelangId).setParameter("versi", versi).fetch();
	}
}
