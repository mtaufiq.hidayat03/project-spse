package models.lelang;

import ext.DatetimeBinder;
import models.agency.DaftarKuantitas;
import models.common.StatusLelang;
import models.common.UploadInfo;
import models.handler.DokLelangDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.FileUtils;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name = "dok_lelang_content")
public class Dok_lelang_content extends BaseModel {

//	public static final String DEFAULT_VERSI=Play.configuration.getProperty("play.sbd.versi", "1.0"); // sbd awal Perpres 4/2015, memakai fungsi cetak

	public static final String DEFAULT_VERSI="2.0"; // sbd versi memakai model upload , Perpres 16/2018
	
	@Id
	public Long dll_id;
	
	@Id
	public Integer dll_versi;

	/**
	 * versi SBD yang dipakai
	 */
	public String dll_sbd_versi;

	/**
	 * daftar kuantitas dan harga format : json json model
	 * models.agency.Rincian_hps
	 */
	public String dll_dkh;

	/**
	 * LDP content format Json json model : models.common.LDPContent
	 */
	public String dll_ldp;

	/**
	 * field ini untuk menyimpan informasi dokumen spesifikasi dan teknis
	 */
	public Long dll_spek;

	/**
	 * field ini untuk menyimpan informasi dokumen lainnya
	 */
	public Long dll_lainnya;
	
	/**
	 * field ini untuk menyimpan nomor dan tanggal SDP
	 */
	public String dll_nomorSDP;
	
	/**
	 * field ini untuk menyimpan Jenis Kontrak
	 */
	public Integer dll_kontrak_pembayaran;
	
	
	
	@As(binder = DatetimeBinder.class)
	public Date dll_tglSDP;

	/**
	 * Syarat-syarat khusus Kontrak (SSKK) format json model
	 * models.lelang.SskkContent
	 */
	public String dll_sskk;

	public Long dll_sskk_attachment;

	public boolean dll_modified;

	public boolean dll_syarat_updated;

	public boolean dll_ldk_updated;

	/**Ini berisi dokumen HTML hasil merge dengan template velocity
	 * HTML ini di-generate setiap kali diklik 'cetak'. Bersamaan dengan itu di-generate 
	 * pula file PDF untuk di-download 
	 * 
	 */
	public String dll_content_html;

	public Long dll_content_attachment;

	@Transient
	public LDPContent ldpcontent;

	@Transient
	public DaftarKuantitas dkh;

	@Transient
	public SskkContent sskk_content;

	protected void postLoad() {
		if (!CommonUtil.isEmpty(dll_ldp))
			ldpcontent = CommonUtil.fromJson(dll_ldp, LDPContent.class);
		if (!CommonUtil.isEmpty(dll_sskk))
			sskk_content = CommonUtil.fromJson(dll_sskk, SskkContent.class);
		if (!CommonUtil.isEmpty(dll_dkh))
			dkh = CommonUtil.fromJson(dll_dkh, DaftarKuantitas.class);

	}
	
	
	public void prePersist() {
		super.prePersist();
		if (ldpcontent != null)
			dll_ldp = CommonUtil.toJson(ldpcontent);
		if (sskk_content != null)
			dll_sskk = CommonUtil.toJson(sskk_content);
		if (dkh != null)
			dll_dkh = CommonUtil.toJson(dkh);
	}
	
	@Transient
	public List<BlobTable> getDokSpek() {
		if(dll_spek == null) {
			if(isAdendum()){
				dll_spek = getSpekAdendum();
			}
		}
		if(dll_spek == null) {
			return null;
		}

		return BlobTableDao.listById(dll_spek);
	}

	@Transient
	public List<BlobTable> getDokLainnya() {
		if(dll_lainnya == null) {
			if (isAdendum())
				dll_lainnya = getDokLainnyaAdendum();
		}
		if(dll_lainnya == null) {
			return null;
		}
		return BlobTableDao.listById(dll_lainnya);
	}

	@Transient
	public List<BlobTable> getDokSskk() {
		if(dll_sskk_attachment == null) {
			if(isAdendum()){
				dll_sskk_attachment = getSskkAttachmentAdendum();
			}
		}

		if(dll_sskk_attachment == null) {
			return null;
		}

		return BlobTableDao.listById(dll_sskk_attachment);
	}

	@Transient
	public Long getSskkAttachmentAdendum() {
		return Query.find("SELECT dll_sskk_attachment FROM dok_lelang_content WHERE dll_id=? AND dll_sskk_attachment is not null ORDER BY dll_versi DESC", Long.class, dll_id).first();
	}
	@Transient
	public Long getSpekAdendum() {
		return Query.find("SELECT dll_spek FROM dok_lelang_content WHERE dll_id=? and dll_spek is not null ORDER BY dll_versi DESC", Long.class, dll_id).first();
	}

	@Transient
	public Long getDokLainnyaAdendum() {
		return Query.find("SELECT dll_lainnya FROM dok_lelang_content WHERE dll_id=? and dll_lainnya is not null ORDER BY dll_versi DESC", Long.class, dll_id).first();
	}

	public BlobTable findByFilename(String filename) {
		List<BlobTable> blobs = getDokSpek();
		if (CommonUtil.isEmpty(blobs)) {
			LogUtil.debug("DokLelangContent", "total blob is empty or object null");
			return null;
		}
		for (BlobTable model : blobs) {
			LogUtil.debug("DokLelangContent", filename + " contains " + model.blb_nama_file);
			if (model.blb_nama_file.equalsIgnoreCase(filename)) {
				return model;
			}
		}
		return null;
	}

//
	public Long simpanBlobSpek(List<BlobTable> blobList) throws Exception{
//		if(CollectionUtils.isEmpty(blobList))
//			return null;
		Long newId = getNextSequenceValue("seq_epns");

		for (BlobTable blob : blobList){
			BlobTable newBlob = new BlobTable();
			newBlob.blb_id_content = newId;
			newBlob.blb_versi = blob.blb_versi;
			newBlob.blb_date_time = blob.blb_date_time;
			newBlob.blb_mirrored = blob.blb_mirrored;
			newBlob.blb_path = blob.blb_path;
			newBlob.blb_engine = blob.blb_engine;
			newBlob.save();

			if(newId == null){
				newId = newBlob.blb_id_content;
			}
		}

		return newId;
	}

	public UploadInfo simpanSpek(File file) throws Exception {
		// asep:replace file name, except number and letter space and dot
		String path = FileUtils.sanitizedFilename(file);
		File newFile = new File(path);
//		BlobTable model = findByFilename(newFile.getName());
//		if (model != null) {
//			LogUtil.debug("DokLelangContent", "filename's duplicated");
//			return null;
//		}
		file.renameTo(newFile);
		BlobTable blob = null;
		if (dll_spek != null) {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile, dll_spek);
		} else {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile);
		}
		dll_spek = blob.blb_id_content;
		save();
		return UploadInfo.findBy(blob);
	}

	public UploadInfo simpanSskkAttachment(File file) throws Exception {
		String path = FileUtils.sanitizedFilename(file);
		File newFile = new File(path);
//		BlobTable model = findByFilename(newFile.getName());
//		if (model != null) {
//			LogUtil.debug("DokLelangContent", "filename's duplicated");
//			return null;
//		}
		file.renameTo(newFile);
		BlobTable blob = null;
		if (dll_sskk_attachment != null) {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dll_sskk_attachment);
		} else {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
		}

		dll_sskk_attachment = blob.blb_id_content;
		save();

		return UploadInfo.findBy(blob);
	}


	public UploadInfo simpanLainnya(File file) throws Exception {
		String path = FileUtils.sanitizedFilename(file);
		File newFile = new File(path);
		file.renameTo(newFile);
		BlobTable blob = null;
		if (dll_lainnya != null) {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile, dll_lainnya);
		} else {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile);
		}
		dll_lainnya = blob.blb_id_content;
		save();
		return UploadInfo.findBy(blob);
	}
	
	//dapatkan dok_lelang_content terakhir
	public static Dok_lelang_content findBy(Long dok_lelang_id) {
		return find("dll_id=? order by dll_versi desc", dok_lelang_id).first();
	}
	
	public static Dok_lelang_content findBy(Long dok_lelang_id, int versi) {
		return find("dll_id=? and dll_versi=?", dok_lelang_id, versi).first();
	}
	
	/**
	 * dapatkan dokumen lelang, jika tidak ada maka akan dibuat baru
	 * @param dok_lelangId
	 * @param
	 * @return
	 */
	public static Dok_lelang_content findNCreateBy(Long dok_lelangId, Lelang_seleksi lelang) {
		Dok_lelang_content dok_lelang_content = find("dll_id=? order by dll_versi desc", dok_lelangId).first();
		if(dok_lelang_content == null) {
			dok_lelang_content = new Dok_lelang_content();
			dok_lelang_content.dll_id = dok_lelangId;
			dok_lelang_content.dll_sbd_versi = DEFAULT_VERSI;
			dok_lelang_content.dll_versi = Integer.valueOf(1);
			dok_lelang_content.dll_modified = true;
			dok_lelang_content.save();
		}
		else if (!dok_lelang_content.dll_modified && lelang.lls_status.isAktif() && lelang.getTahapStarted().isAllowAdendum()) {
			Integer versi = dok_lelang_content.dll_versi;
			dok_lelang_content = new Dok_lelang_content();
			dok_lelang_content.dll_id = dok_lelangId;
			dok_lelang_content.dll_sbd_versi = DEFAULT_VERSI;
			dok_lelang_content.dll_versi = versi + 1;
			dok_lelang_content.dll_modified = true;
			dok_lelang_content.save();
		}
		return dok_lelang_content;
	}

	public static Dok_lelang_content findNCreateByForSyarat(Long dok_lelangId, Lelang_seleksi lelang) {

		Dok_lelang_content dok_lelang_content = find("dll_id=? order by dll_versi desc", dok_lelangId).first();
		if(dok_lelang_content == null && lelang.lls_status.isDraft()) {
			dok_lelang_content = new Dok_lelang_content();
			dok_lelang_content.dll_id = dok_lelangId;
			dok_lelang_content.dll_sbd_versi = DEFAULT_VERSI;
			dok_lelang_content.dll_versi = Integer.valueOf(1);
			dok_lelang_content.dll_modified = true;
			dok_lelang_content.dll_syarat_updated = true;
			dok_lelang_content.save();
		}
		else if (!dok_lelang_content.dll_modified && lelang.lls_status.isAktif() && lelang.getTahapStarted().isAllowAdendum()) {
			Integer versi = dok_lelang_content.dll_versi;
			dok_lelang_content = new Dok_lelang_content();
			dok_lelang_content.dll_id = dok_lelangId;
			dok_lelang_content.dll_sbd_versi = DEFAULT_VERSI;
			dok_lelang_content.dll_versi = versi + 1;
			dok_lelang_content.dll_modified = true;
			dok_lelang_content.dll_syarat_updated = true;
			dok_lelang_content.save();
		}else {
			dok_lelang_content.dll_syarat_updated = true;
			dok_lelang_content.save();
		}
		return dok_lelang_content;
	}

	public static Dok_lelang_content findNCreateByForLdk(Long dok_lelangId, StatusLelang status) {
		Dok_lelang_content dok_lelang_content = find("dll_id=? order by dll_versi desc", dok_lelangId).first();
		
		if(dok_lelang_content == null && status.isDraft()) {
			dok_lelang_content = new Dok_lelang_content();
			dok_lelang_content.dll_id = dok_lelangId;
			dok_lelang_content.dll_sbd_versi = DEFAULT_VERSI;
			dok_lelang_content.dll_versi = Integer.valueOf(1);
			dok_lelang_content.dll_modified = true;
			dok_lelang_content.dll_ldk_updated = true;
			dok_lelang_content.save();
		}
		else if (!dok_lelang_content.dll_modified && status.isAktif()) {
			Integer versi = dok_lelang_content.dll_versi;
			dok_lelang_content = new Dok_lelang_content();
			dok_lelang_content.dll_id = dok_lelangId;
			dok_lelang_content.dll_sbd_versi = DEFAULT_VERSI;
			dok_lelang_content.dll_versi = versi + 1;
			dok_lelang_content.dll_modified = true;
			dok_lelang_content.dll_ldk_updated = true;
			dok_lelang_content.save();
		}else {
			dok_lelang_content.dll_ldk_updated = true;
			dok_lelang_content.save();
		}
		return dok_lelang_content;
	}
	
	@Transient
	public BlobTable getDokumen() {
		if(dll_content_attachment == null)
			return null;
		return BlobTableDao.getLastById(dll_content_attachment);
	}
	
	@Transient
	public String getDownloadUrl() throws IllegalAccessException {
		return getDokumen().getDownloadUrl(DokLelangDownloadHandler.class);		
	}
	
	@Transient
	// adendum jika versinya > 1, jika versi==1 maka harusny draft
	public boolean isAdendum() {
		return dll_versi >  1;
	}

	public static boolean isLdpEmpty(Long dll_id) {
		return Query.count("select count(dll_id) from dok_lelang_content where dll_id=? AND length(dll_ldp) > 0", dll_id) == 0;
	}

	public static boolean isSskkEmpty(Long dll_id) {
		return Query.count("select count(dll_id) from dok_lelang_content where dll_id=? AND length(dll_sskk) > 0", dll_id) == 0;
	}

	public static boolean isDkhEmpty(Long dll_id) {
		return Query.count("select count(dll_id) from dok_lelang_content where dll_id=? AND length(dll_dkh) > 0", dll_id) == 0;
	}

	public static boolean isSpekEmpty(Long dll_id) {
		return Query.count("select count(dll_id) from dok_lelang_content where dll_id=? AND dll_spek is not null", dll_id) == 0;
	}

	public boolean isEmptyDokumen() {
		return dll_content_attachment == null || BlobTableDao.count("blb_id_content=?", dll_content_attachment) == 0;
	}

	public boolean isAllowUpload() {
		return this.isAdendum() && dll_modified;
	}

}
