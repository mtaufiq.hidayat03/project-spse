package models.lelang;

import ext.FormatUtils;
import models.agency.Kontrak;
import models.agency.Panitia;
import models.common.Metode;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Arief on 07/18/17.
 */
public class SummaryLelang {
	
	static final Template TEMPLATE = TemplateLoader.load("/lelang/template-summary.html");

    public static InputStream cetak(Long lelangId) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Lelang_detil lelang_detil = Lelang_detil.findById(lelangId);
        boolean showBobot = lelang_detil.getEvaluasi().isNilai();
        boolean prakualifikasi = lelang_detil.getMetode().kualifikasi.isPra();
        List<Persetujuan.PersetujuanLelang> persetujuanList = Persetujuan.PersetujuanLelang.getPersetujuanLelangForSummary(lelang_detil.lls_id);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("lelang_detil", lelang_detil);
        model.put("isExpress", lelang_detil.getPemilihan().isLelangExpress());
        if(lelang_detil.getKualifikasi() != null)
            model.put("kualifikasi", lelang_detil.getKualifikasi().label);
        model.put("showBobot", lelang_detil.getEvaluasi().isNilai());
        model.put("prakualifikasi", prakualifikasi);
        if (showBobot) {
            model.put("bobotTeknis", lelang_detil.lls_bobot_teknis);
            model.put("bobotBiaya", lelang_detil.lls_bobot_biaya);
        }
        Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lelang_detil.lls_id);
        Date tanggalBuat = lelang_seleksi.lls_dibuat_tanggal;
        String namaPegawai = lelang_seleksi.getNamaPegawaiPembuatan();
        String tglPembuatan = FormatUtils.formatDateTimeInd(tanggalBuat) + " Oleh " + namaPegawai;
        model.put("tglPembuatan", tglPembuatan);
        model.put("listPersetujuan", persetujuanList);

        Metode metode = lelang_detil.getMetode();
        boolean penjelasanPra = (metode == Metode.PRA_DUA_FILE_KUALITAS) || (metode == Metode.PRA_DUA_FILE_KUALITAS_BIAYA) || (metode == Metode.PRA_SATU_FILE_KONSULTANSI 
        		|| metode == Metode.PRA_DUA_TAHAP || metode == Metode.PRA_DUA_FILE);
        model.put("penjelasanPra", penjelasanPra);
        Panitia panitia = lelang_seleksi.getPanitia();
        if(panitia != null) {
            model.put("panitia", panitia);
        }
        Kontrak kontrak = Kontrak.find("lls_id=?", lelang_detil.lls_id).first();
        model.put("kontrak", kontrak);
        // reverse auction
        ReverseAuction auction  = ReverseAuction.findByLelang(lelangId);
        if(auction != null && auction.ra_status.isSelesai()) {
            model.put("hasil", auction.hasilAkhir());
            model.put("termurah", auction.termurah());
        }
        stopWatch.stop();
        Logger.info("Summary Tender %s: render param %s", lelang_detil.lls_id, stopWatch.getTime());
        return HtmlUtil.generatePDF(TEMPLATE, model);
    }

}
