package models.lelang;

import controllers.BasicCtr;
import models.agency.DaftarKuantitas;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.common.Metode;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Evaluasi.JenisEvaluasi;
import models.lelang.contracts.ParticipantContract;
import models.rekanan.Rekanan;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


@Table(name="PESERTA")
public class Peserta extends BaseModel implements ParticipantContract {
	
	@Id(sequence="seq_peserta", function="nextsequence")
	public Long psr_id;

	public Date psr_tanggal;

	public Long psr_black_list;

	public Integer is_pemenang;

	//kolom pemenang terferifikasi
	public Integer is_pemenang_verif;

	public Integer sudah_verifikasi_sikap;

	//relasi ke Lelang_seleksi
	public Long lls_id;

	//relasi ke Rekanan
	public Long rkn_id;
	
	public Double psr_harga;

	public Double psr_harga_terkoreksi;

	public String psr_uraian;
	
	public String psr_alasan_menang;

	public boolean is_dikirim_pesan = false;

	public boolean sudah_evaluasi_kualifikasi = false;

	public Integer is_pemenang_klarifikasi;
	
	/**
	 * daftar kuantitas dan harga
	 * format : json 
	 * json model models.agency.Rincian_hps
	 */
	public String psr_dkh;
	
	public String psr_identitas; // data identitas , model : rekanan.Rekanan
	
	public String psr_pemilik; // data pemilik , model : rekanan.Pemilik
	
	public String psr_pengurus; // data pengurus , model : rekanan.Pengurus
	
	/**
	 * daftar kuantitas dan harga
	 */
	@Transient
	public DaftarKuantitas dkh;
	
	@Transient
	private Lelang_seleksi lelang_seleksi;	
	@Transient
	private Rekanan rekanan;
	@Transient
	public Dok_penawaran dokPenawaranHarga;
	@Transient
	public Dok_penawaran dokPenawaranTeknis;
	@Transient
	public List<Dok_penawaran> dokPenawarans;
	@Transient
	private Nilai_evaluasi nilaiPembuktian;

	public Nilai_evaluasi getNilaiPembuktian(){
		if(nilaiPembuktian == null){
			Evaluasi pembuktian = getLelang_seleksi().getPembuktian(false);
			if(pembuktian != null){
				nilaiPembuktian = Nilai_evaluasi.findBy(pembuktian.eva_id, psr_id);
			}
		}
		return nilaiPembuktian;
	}

    public Lelang_seleksi getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}
    
    public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	

	protected void postLoad() {
		super.postLoad();
		if(!CommonUtil.isEmpty(psr_dkh))
			dkh = CommonUtil.fromJson(psr_dkh, DaftarKuantitas.class);
	}
	
	public void prePersist() {
        super.prePersist();
        if (dkh != null)
            psr_dkh = CommonUtil.toJson(dkh);
    }
	
	/**
	 * informasi nama Peserta Tender
	 * @return
	 */
	public String getNamaPeserta() {
		return Query.find("select rkn_nama from rekanan where rkn_id=?", String.class, rkn_id).first();
	}
		
	/**
	 * cek pemenang prakualifikasi
	 * @return
	 */
	@Transient
	public boolean isPemenangPraKualifikasi()
	{
		Metode metode = Lelang_seleksi.getMetodeLelang(lls_id);
		if (metode.kualifikasi.isPra()) {
			return Nilai_evaluasi.isLulus(JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI, this);
		}
		return false;
	}
	
	/**
	 * cek pemenang lelang
	 * @return
	 */
	@Transient
	public boolean isPemenang()
	{
		return Nilai_evaluasi.isLulus(JenisEvaluasi.EVALUASI_AKHIR, this);
	}
	
	/**
	 * cek pemenang lelang
	 * @return
	 */
	@Transient
	public boolean isPemenangVerif()
	{
		if(is_pemenang_verif==null)
			return false;
		return is_pemenang_verif==Integer.valueOf(1);
	}

	/**
	 * cek sudah verifikasi ke SIKaP
	 * @return
	 */
	@Transient
	public boolean isSudahVerifikasiSikap() {
		if (sudah_verifikasi_sikap == null)
			return false;
		return sudah_verifikasi_sikap == Integer.valueOf(1);
	}
	
	/**
	 * get Keterangan Hasil Tender cepat
	 * @return
	 */
	@Transient
	public String getKeteranganHasil()
	{
		String retval = psr_alasan_menang;
		
		if(! isPemenangVerif())
			if(retval==null || retval.isEmpty())
				retval = "Tidak Dilakukan Verifikasi";
		return retval;
	}
	
	@Transient
	public List<Dok_penawaran> findPenawaranList() {
		return Dok_penawaran.findPenawaranPeserta(psr_id);
	}
	
	@Transient
	public Dok_penawaran getDokKualifikasi() {
		return Dok_penawaran.findPenawaranPeserta(psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
	}
	
	@Transient
	public Dok_penawaran getDokTambahan() {
		return Dok_penawaran.findPenawaranPeserta(psr_id, JenisDokPenawaran.PENAWARAN_TAMBAHAN);
	}
	
	@Transient
	public Dok_penawaran getDokSusulan() {
		return Dok_penawaran.findPenawaranPeserta(psr_id, JenisDokPenawaran.PENAWARAN_SUSULAN);
	}
	
	@Transient
	public Dok_penawaran getFileTeknis() {
		return Dok_penawaran.findPenawaranPeserta(psr_id, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
	}
	
	@Transient
	public Dok_penawaran getFileHarga() {
		return Dok_penawaran.findPenawaranPeserta(psr_id, JenisDokPenawaran.PENAWARAN_HARGA);
	}

	@Transient
	public Dok_penawaran getFileAdminTeknisHarga() { // dipakai oleh lelang versi 3
		return Dok_penawaran.findPenawaranPeserta(psr_id, JenisDokPenawaran.PENAWARAN_HARGA);
	}
	
	/**
	 * Nilai evaluasi Administrasi
	 * @return
	 */
	@Transient
	public Nilai_evaluasi getAdmin() {
		Evaluasi admin = Evaluasi.findAdministrasi(lls_id);
		if(admin == null)
			return null;
		return Nilai_evaluasi.findBy(admin.eva_id, psr_id);
	}
	
	/**
	 * Nilai evaluasi Teknis
	 * @return
	 */
	@Transient
	public Nilai_evaluasi getTeknis() {
		Evaluasi teknis = Evaluasi.findTeknis(lls_id);
		if(teknis == null)
			return null;
		return Nilai_evaluasi.findBy(teknis.eva_id, psr_id);
	}

	public boolean isLulusTeknis() {
		Nilai_evaluasi nilai = getTeknis();
		return nilai != null && nilai.isLulus();
	}
	
	/**
	 * Nilai evaluasi Harga
	 * @return
	 */
	@Transient
	public Nilai_evaluasi getHarga() {
		Evaluasi harga = Evaluasi.findHarga(lls_id);
		if(harga == null)
			return null;
		return Nilai_evaluasi.findBy(harga.eva_id, psr_id);
	}
	
	/**
	 * Nilai evaluasi kualifikasi
	 * @return
	 */
	@Transient
	public Nilai_evaluasi getKualifikasi() {
		Evaluasi kualifikasi = Evaluasi.findKualifikasi(lls_id);
		if(kualifikasi == null)
			return null;
		return Nilai_evaluasi.findBy(kualifikasi.eva_id, psr_id);
	}
	
	/**
	 * Nilai evaluasi Pembuktian
	 * @return
	 */
	@Transient
	public Nilai_evaluasi getPembuktian() {
		Evaluasi pembuktian = Evaluasi.findPembuktian(lls_id);
		if(pembuktian == null)
			return null;
		return Nilai_evaluasi.findBy(pembuktian.eva_id, psr_id);
	}

	/**
	 * Nilai evaluasi Penetapan Pemenang
	 * @return
	 */
	@Transient
	public Nilai_evaluasi getPenetapan() {
		Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lls_id);
		if(penetapan == null)
			return null;
		return Nilai_evaluasi.findBy(penetapan.eva_id, psr_id);
	}
	
	public static List<Peserta> findBylelang(Long lelangId) {
		return find("lls_id=? order by psr_harga", lelangId).fetch();
	}

	/**
	 * cari Peserta yang menawar
	 * @param lelangId
	 * @return
	 */
	public static List<Peserta> findPenawarBylelang(Long lelangId) {
		return find("lls_id=? AND psr_harga <> 0 order by psr_harga", lelangId).fetch();
	}
	
	public static Peserta findBy(Long lelangId, Long rekananId) {
		return find("lls_id=? and rkn_id=?", lelangId, rekananId).first();
	}

	public static Peserta findBy(Long pesertaId) {
		return find("psr_id = ?", pesertaId).first();
	}
	
	public static Peserta findBy(Long lelangId, Long rekananId, Long pesertaId) {
		return find("lls_id=? and rkn_id=? and psr_id=?", lelangId, rekananId, pesertaId).first();
	}
	
	/**
	 * Fungsi {@code getPesertaIDByUsenameAndIDLelang} digunakan untuk mendapatkan informasi ID Peserta Tender berdasarkan
	 * username dalam hal ini username penyedia dan ID lelang.
	 *
	 * @param username username penyedia yang didapat dari session
	 * @param lls_id id lelang
	 * @return ID Peserta
	 */
	public static Peserta findByUsenameAndIDLelang(String username, Long lls_id) {
		Rekanan rekanan = Rekanan.findByNamaUser(username);
//		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lls_id);
		Peserta peserta = find("rkn_id = ? AND lls_id = ?", rekanan.rkn_id, lls_id).first();
		if (peserta == null) {
			return null; // bukan merupakan Peserta Tender
		} else {
			return peserta; // return id Peserta
		}
	}

	public static List<Peserta> findByIdLelang(Long idLelang) {
		return find("lls_id = ?", idLelang).fetch();
	}

	public static Peserta findPesertaPemenangVerifByIdLelang(Long idLelang) {
		return find("lls_id = ? AND is_pemenang_verif = 1", idLelang).first();
	}

	/**
	 * Rekanan mendaftar Tender
	 * @param lelangId
	 * @param rekananId
	 */
	public static void daftarLelang(Long lelangId, Long rekananId)	{
		Peserta peserta = new Peserta();
		peserta.lls_id = lelangId;
		peserta.rkn_id = rekananId;
		peserta.psr_tanggal = BasicCtr.newDate();
		peserta.psr_black_list = Long.valueOf(0);
		peserta.is_pemenang = Integer.valueOf(0);
		peserta.is_pemenang_verif = Integer.valueOf(0);
		peserta.sudah_verifikasi_sikap = Integer.valueOf(0);
		peserta.save();
	}
	
	public static List<Peserta> findWithPenawaran(Long lelangId) {	
		return find("lls_id=? and psr_id in (select distinct psr_id from Dok_penawaran where dok_jenis in (1,2,3) and dok_disclaim=1)",lelangId).fetch();
	}
	
	public static List<Peserta> findWithKualifikasi(Long lelangId) {
		return find("lls_id=? and psr_id in (select distinct psr_id from Dok_penawaran where dok_jenis =?)", lelangId, JenisDokPenawaran.PENAWARAN_KUALIFIKASI).fetch();
	}

	public static Peserta findByRekananAndLelang(Long rekananId, Long lelangId){
		return find("rkn_id=? and lls_id=?", rekananId, lelangId).first();
	}

	public boolean sudahTerimaUndanganKontrak() {
		return MailQueueDao.checkEmail(lls_id, rkn_id, JenisEmail.UNDANGAN_KONTRAK);
	}

	public String getAdmHargaFileName() {
		return '{' + getNamaPeserta() + "}-{" + lls_id + "}-{adm_harga}.rhs";
	}

	public String getAdmTeknisFileName() {
		return '{' + getNamaPeserta() + "}-{" + lls_id + "}-{adm_teknis}.rhs";
	}

	public String getAdmTeknisHargaFileName() {
		return '{' + getNamaPeserta() + "}-{" + lls_id + "}-{adm_teknis_harga}.rhs";
	}

	public String getAdmFileNameByDocumentType(JenisDokPenawaran type) {
		if (type.isHarga()) {
			return getAdmHargaFileName();
		} else if (type.isAdmTeknis()) {
			return getAdmTeknisFileName();
		} else if (type.isAdmTeknisHarga()) {
			return getAdmTeknisHargaFileName();
		}
		return "";
	}

	@Override
	public Long getParticipantId() {
		return psr_id;
	}

	@Override
	public void setDokPenawaranHarga(Dok_penawaran model) {
		this.dokPenawaranHarga = model;
	}

	@Override
	public void setDokPenawaranTekis(Dok_penawaran model) {
		this.dokPenawaranTeknis = model;
	}

	@Override
	public void setDokPenawarans(List<Dok_penawaran> list) {
		this.dokPenawarans = list;
	}

	// check kondisi penawaran seluruh Peserta Tender, sudah ada penawaran atau belum ada sama sekali
	public static boolean isHargaPenawaranAllEmpty(Long lelangId) {
		return count("lls_id=? AND psr_harga is null", lelangId) > 0;
	}

	public static Double getTotalHargaFinal(List<Peserta> pemenang, Lelang_seleksi lelang){
		Double total = 0d;
		if(!lelang.isItemized() && CollectionUtils.isNotEmpty(pemenang)){
			total = pemenang.get(0).getPenetapan().hargaFinal();
		}
		return total;
	}

	public boolean isLulusKualifikasi() {
		Evaluasi evaluasi = Evaluasi.findLastBy(lls_id, JenisEvaluasi.EVALUASI_KUALIFIKASI, Evaluasi.StatusEvaluasi.SELESAI);
		if(evaluasi != null) {
			Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findBy(evaluasi.eva_id, psr_id);
			return  nilai_evaluasi != null && nilai_evaluasi.isLulus();
		}
		return false;
	}

	public boolean isLulusPembuktian() {
		Evaluasi evaluasi = Evaluasi.findLastBy(lls_id, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI, Evaluasi.StatusEvaluasi.SELESAI);
		if(evaluasi != null) {
			Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findBy(evaluasi.eva_id, psr_id);
			return  nilai_evaluasi != null && nilai_evaluasi.isLulus();
		}
		return false;
	}

	public void resetPenawaranPrakualifikasi() {
		// simpan history
		History_penawaran.simpanHistory(this);
		is_pemenang = Integer.valueOf(0);
		is_pemenang_verif = Integer.valueOf(0);
		sudah_verifikasi_sikap = Integer.valueOf(0);
		psr_harga = null;
		psr_harga_terkoreksi = null;
		psr_uraian = null;
		psr_alasan_menang = null;
		is_dikirim_pesan = false;
		sudah_evaluasi_kualifikasi = false;
		is_pemenang_klarifikasi = Integer.valueOf(0);
		psr_dkh = null;
		dkh = null;
		psr_identitas = null;
		psr_pemilik = null;
		psr_pengurus = null;
		save();
		QueryBuilder query = new QueryBuilder("psr_id=? AND dok_jenis not in (1,2,3)", psr_id);
		List<Dok_penawaran> dokList = Dok_penawaran.find(query).fetch();
		if(!CollectionUtils.isEmpty(dokList)) {
			for(Dok_penawaran dok_penawaran : dokList) {
				History_dok_penawaran.simpanHistory(dok_penawaran);
				dok_penawaran.delete();
			}
		}
	}
	
	// reset penawaran dilakukan ketika terjadi pemasukan penawaran ulang
	public void resetPenawaran(boolean prakualifikasi) {
		// simpan history
		History_penawaran.simpanHistory(this);
		is_pemenang = Integer.valueOf(0);
		is_pemenang_verif = Integer.valueOf(0);
		sudah_verifikasi_sikap = Integer.valueOf(0);
		psr_harga = null;
		psr_harga_terkoreksi = null;
		psr_uraian = null;
		psr_alasan_menang = null;
		is_dikirim_pesan = false;
		sudah_evaluasi_kualifikasi = false;
		is_pemenang_klarifikasi = Integer.valueOf(0);
		psr_dkh = null;
		dkh = null;
		if(!prakualifikasi) { // jika bukan prakualifikasi, reset psr_identitas, psr_pemilik, psr_pengurus
			psr_identitas = null;
			psr_pemilik = null;
			psr_pengurus = null;
		}
		save();
		QueryBuilder query = new QueryBuilder("psr_id=?", psr_id);
		if(prakualifikasi) // jika prakualifikasi hanya dokumen penawaran saja yg dipindah ke history
			query.append("AND dok_jenis in (1,2,3)");
		List<Dok_penawaran> dokList = Dok_penawaran.find(query).fetch();
		if(!CollectionUtils.isEmpty(dokList)) {
			for(Dok_penawaran dok_penawaran : dokList) {
				History_dok_penawaran.simpanHistory(dok_penawaran);
				dok_penawaran.delete();
			}
		}
	}
}
