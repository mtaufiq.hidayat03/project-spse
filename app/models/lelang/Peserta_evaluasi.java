package models.lelang;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Peserta_evaluasi implements Serializable {

	public Long psr_id;
	

	public String rkn_nama;

	public Double nev_harga;

	public Double nev_harga_terkoreksi;

	public Integer administrasi;

	public String als_administrasi;

	public Integer teknis;

	public String als_teknis;

	public Integer kualifikasi;

	public String als_kualifikasi;

	public Integer akhir;
}
