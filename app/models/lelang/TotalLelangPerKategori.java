package models.lelang;

public class TotalLelangPerKategori {
	public Object kategori;

	public Integer jumlah;

	public Double pagu;

	/**
	 * Default Constructor
	 */
	public TotalLelangPerKategori() {

	}

	public TotalLelangPerKategori(Object kategori, Number jumlah, Number pagu) {
		this.kategori = kategori;
		this.jumlah = Integer.valueOf(jumlah.intValue());
		this.pagu = Double.valueOf(pagu.intValue());
	}
}
