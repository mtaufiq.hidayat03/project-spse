package models.lelang;

import ext.*;
import play.data.binding.As;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;
/**
 * model untuk Content SSKK
 * @author lkpp
 *
 */
public class SskkContent implements Serializable {

	public Integer kontrak_pembayaran;
	@As(binder = SbdRegexBinder.class)
	public String jaminan_setor_kas;
	@As(binder = SbdRegexBinder.class)
	public String wakil_sah_ppk = "-";
	@As(binder = SbdRegexBinder.class)
	public String wakil_sah_rekanan ="-";
	@As(binder = SbdRegexBinder.class)
	public String wakil_sah_pengawas = "-";
	@As(binder = SbdRegexBinder.class)
	public String pajak_rekanan="-";
	@As(binder = SbdRegexBinder.class)
	public String subkontrak_pekerjaan="-";
	@As(binder = SbdRegexBinder.class)
	public String subkontrak_penyedia="-";
	@As(binder = SbdRegexBinder.class)
	public String subkontrak_pekerjaan_khusus="-";
	@As(binder = SbdRegexBinder.class)
	public String subkontrak_penyedia_khusus="-";
	
	public String subkontrak_sanksi="-";
	@As(binder = SbdRegexBinder.class)
	public String pelaksanaan_pekerjaan;
	@As(binder = SbdRegexBinder.class)
	public String pengawas;
	
	public Date tgl_periksa_bersama;
	@Transient
	@As(binder = SbdRegexBinder.class)
	public String tgl_periksa_bersama_dt; //edit by sudi
	@As(binder = SbdRegexBinder.class)
	public String denda_keterlambatan;
	
	@As(binder = DateBinder.class)
	public Date kontrak_mulai;
	
	@As(binder = DateBinder.class)
	public Date kontrak_akhir;
	
	/*=================start barang =========================*/
	@As(binder = SbdRegexBinder.class)
	public String standar_barang="";
	@As(binder = DateBinder.class)
	public Date inspeksi_tgl;
	@As(binder = SbdRegexBinder.class)
	public String inspeksi_tgl_express; //edit by sudi
	@As(binder = SbdRegexBinder.class)
	public String inspeksi_tempat="-";
	@As(binder = SbdRegexBinder.class)
	public String inspeksi_lingkup="-";
	@As(binder = SbdRegexBinder.class)
	public String pengepakan="-";
	@As(binder = SbdRegexBinder.class)
	public String pengiriman="-";
	@As(binder = SbdRegexBinder.class)
	public String asuransi_ketentuan;
	@As(binder = SbdRegexBinder.class)
	public String asuransi_lingkup;
	@As(binder = SbdRegexBinder.class)
	public String asuransi_batas;
	
	//added by chitra
	//untuk lelang cepat
	@As(binder = SbdRegexBinder.class)
	public String pertanggungan_asuransi_cif="Tempat Tujuan Pengiriman";
	@As(binder = SbdRegexBinder.class)
	public String pertanggungan_asuransi_fob="Tempat Tujuan Pengiriman";
	@As(binder = SbdRegexBinder.class)
	public String serah_terima="Tempat Tujuan Pengiriman";
	@As(binder = SbdRegexBinder.class)
	public String cara_bayar_denda="Dipotong Dari Tagihan"; //value: Dipotong Dari Tagihan / penyedia meneytorkan ke kas negara/daerah

	@As(binder = SbdRegexBinder.class)
	public String tujuan_pengangkutan="Tempat Tujuan Pengiriman"; // value: 'Tempat Tujuan Akhir' / 'Tempat Tujuan Pengiriman'
	@As(binder = SbdRegexBinder.class)
	public String jenis_angkutan;
	@As(binder = SbdRegexBinder.class)
	public String jenis_tranportasi="darat"; // value : darat/laut/udara
	@As(binder = SbdRegexBinder.class)
	public String pemeriksaan_tempat="-";
	@As(binder = SbdRegexBinder.class)
	public String pemeriksaan_lingkup="-";
	@As(binder = SbdRegexBinder.class)
	public String ujicoba_tempat="-";
	@As(binder = SbdRegexBinder.class)
	public String ujicoba_pelaku="-";
	
	public Date tgl_penyelesaian;
	@Transient
	@As(binder = SbdRegexBinder.class)
	public String tgl_penyelesaian_dt; //edit by sudi
	@As(binder = SbdRegexBinder.class)
	public String edisi_incoterms="-";
	@As(binder = DateBinder.class)
	public Date tgl_serah_terima;
	@As(binder = SbdRegexBinder.class)
	public String tempat_tujuan_akhir;
	@As(binder = SbdRegexBinder.class)
	public String tempat_tujuan_pengiriman;
	@As(binder = SbdRegexBinder.class)
	public String masa_garansi;
	@As(binder = SbdRegexBinder.class)
	public String masa_purnajual;
	
	/*=================end barang =========================*/
	@As(binder = SbdRegexBinder.class)
	public String masa_pemeliharaan;
	
	/*=================start konstruksi =========================*/
	@As(binder = SbdRegexBinder.class)
	public String ketentuan_serah_terima="-";
	@As(binder = SbdRegexBinder.class)
	public String umur_konstruksi;
	
	@As(binder=RupiahBinder.class)
	public Double nilai_pembayaran_peralatan = Double.valueOf(0.0); //added by chitra 121015 untuk pembayaran perestasi pekerjaan
	/*=================end konstruksi=========================*/
	
	//added by chitra
	@As(binder=SbdRegexBinder.class)
	public String denda_akibat_pemutusan_sepihak;
	@As(binder = SbdRegexBinder.class)
	public String waktu_denda_akibat_pemutusan_sepihak;
	/*=================start konsultansi =========================*/
	@As(binder = SbdRegexBinder.class)
	public String batasan_penggunaan_dok;
	public Integer tanggung_jawab_profesi = Integer.valueOf(0);
	public String getTanggung_jawab_profesi_string(){
		return FormatUtils.number2Word(tanggung_jawab_profesi);
	}
	
	public Integer batas_penerbitan_spp = Integer.valueOf(0);
	public String getBatas_penerbitan_spp_string(){
		return FormatUtils.number2Word(batas_penerbitan_spp);
	}
	@As(binder = SbdRegexBinder.class)
	public String dokumen_utama_pembayaran;
	@As(binder = SbdRegexBinder.class)
	public String ketentuan_penyesuaian_harga; //added by chitra 21102015 untuk lelang cepat badan usaha
	/*=================end konsultansi=========================*/
	
	/*=================start barang dan konstruksi =========================*/
	@As(binder = SbdRegexBinder.class)
	public String sumber_dana_harga_kontrak;
	@As(binder = SbdRegexBinder.class)
	public String sumber_dana_harga_kontrak_pilihan; //add by sudi
	/*=================end barang dan konstruksi=========================*/
	@As(binder = SbdRegexBinder.class)
	public String penyerahan_pedoman;
	@As(binder = SbdRegexBinder.class)
	public String layanan_tambahan="-";
	@As(binder = SbdRegexBinder.class)
	public String waktu_pembayaran;
	@As(binder = SbdRegexBinder.class)
	public String laporan_pekerjaan="-";
	@As(binder = SbdRegexBinder.class)
	public String perolehan_haki="-";
	@As(binder = SbdRegexBinder.class)
	public String pemberian_haki="-";
	@As(binder = SbdRegexBinder.class)
	public String persetujuan_ppk;
	@As(binder = SbdRegexBinder.class)
	public String persetujuan_pp="-"; //added by chitra 20102015 untuk lelang cepat jl
	@As(binder = SbdRegexBinder.class)
	public String kepemilikan_dokumen;
	@As(binder = SbdRegexBinder.class)
	public String sanksi="-";
	@As(binder = SbdRegexBinder.class)
	public String fasilitas;
	@As(binder = SbdRegexBinder.class)
	public String peristiwa_kompensasi;
	
	@As(binder=DecimalBinder.class)
	public Double uang_muka = Double.valueOf(0.0);
	@As(binder = SbdRegexBinder.class)
	public String cara_pembayaran="Sekaligus"; // value : Termin, Bulanan, Sekaligus 
	@As(binder = SbdRegexBinder.class)
	public String ketentuan_pembayaran;
	@As(binder = SbdRegexBinder.class)
	public String dokumen_penunjang_pembayaran; //edit by chitra untuk konsultansi lelang umum dan cepat
	@As(binder = SbdRegexBinder.class)
	public String mata_uang_pembayaran; //added by chitra 121015 untuk konsultansi

	//field input berada di form-sskk
	@As(binder = SbdRegexBinder.class)
	public String dokumen_penunjang_pembayaran_prestasi; //edit by asep.as
	
	@As(binder=RupiahBinder.class)
	public Double selisih_tagihan = Double.valueOf(0.0);
	@As(binder = SbdRegexBinder.class)
	public String ketentuan_denda;
	@As(binder = SbdRegexBinder.class)
	public String pembayaran_denda;
	@As(binder = SbdRegexBinder.class)
	public String waktu_denda;
	@As(binder = SbdRegexBinder.class)
	public String asal_denda;
	@As(binder = SbdRegexBinder.class)
	public String instansi_pembuat_indeks;
	@As(binder = SbdRegexBinder.class)
	public String jenis_indeks;
	
	@As(binder=DecimalBinder.class)
	public Double nilai_indeks = Double.valueOf(0.0);
	
	@As(binder=DecimalBinder.class)
	public Double nilai_koefisien_tetap= Double.valueOf(0.0);
	
	@As(binder=DecimalBinder.class)
	public Double nilai_koefisien_komponen= Double.valueOf(0.0);
	@As(binder = SbdRegexBinder.class)
	public String pemutus_sengketa="Pengadilan Republik Indonesia"; // value: 'Pengadilan Republik Indonesia' / 'Badan Arbitrase Nasional Indonesia'
	@As(binder = SbdRegexBinder.class)
	public String ketentuan_lainnya;
	@As(binder = SbdRegexBinder.class)
	public String pemutus_sengketa_lainnya;
	
	public String getTgl_pemeriksaan () {
		return FormatUtils.formatDateInd(tgl_periksa_bersama);
	}
	
	public String getTgl_inspeksi() {
		return FormatUtils.formatDateInd(inspeksi_tgl);
	}
	
	public String getTgl_selesai() {
		return FormatUtils.formatDateInd(tgl_penyelesaian);
	}
	
	public String getTglSerahTerima() {
		return FormatUtils.formatDateInd(tgl_serah_terima);
	}
	
	public String getUangMuka() {
		if(uang_muka == null)
			return "";
		return StringFormat.rupiah(uang_muka);
	}
	
	public String getSelisihTagihan() {
		if(uang_muka == null)
			return "";
		return StringFormat.rupiah(selisih_tagihan);
	}

    public String getMasaBerlaku() {
        return FormatUtils.formatDate(kontrak_mulai, kontrak_akhir);
    }
    
    public String getKontrakMulai(){
    	return FormatUtils.formatDateInd(kontrak_mulai);
    }
}
