package models.lelang;

import controllers.BasicCtr;
import controllers.security.KeyGeneratorApendoV1;
import controllers.security.KeyGeneratorApendoV2;
import controllers.security.KeyUtilities;
import models.agency.Anggota_panitia;
import models.agency.Paket;
import models.common.ConfigurationDao;
import models.jcommon.db.base.BaseModel;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.secman.Group;
import models.secman.Usergroup;
import org.apache.commons.codec.binary.Base64;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.osd.KmsClient;
import utils.osd.OSDUtil;

import java.util.List;


@Table(name="LELANG_KEY")
public class Lelang_key extends BaseModel {

	@Id
	public Long lls_id;

	@Id
	public JenisDokPenawaran dok_jenis;

	public String public_key;

	public String private_key;

	public String modulus;
		
	public static void generateKey(Lelang_seleksi lelang) {
		if(ConfigurationDao.getVersiApendo().charAt(0) == '1')
			KeyGeneratorApendoV1.generateKey(lelang);			
		else  		
			KeyGeneratorApendoV2.generateKey(lelang);
	}	
	
	public static Lelang_key findBy(JenisDokPenawaran jenis, Long lelangId){
		return find("lls_id=? and dok_jenis=?", lelangId, jenis).first();
	}
	
	public static String getPublicKey(JenisDokPenawaran jenis, Long lelangId){
		return findBy(jenis, lelangId).public_key;
	}
	
	public static String getPrivateKey(JenisDokPenawaran jenis, Long lelangId){
		return Base64.encodeBase64String(KeyUtilities.decryptKey(findBy(jenis, lelangId).private_key));
	}

	public static String getPublicKey(JenisDokPenawaran jenis, Lelang_seleksi lelang, Paket paket){
		boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
		String key = null;
		if(isOSD) {
			try {
				if(paket.ttp_ticket_id == null) {
					List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?",paket.pnt_id).fetch();
					paket.ttp_ticket_id = OSDUtil.createKPL(paket, anggota_panitia, lelang, BasicCtr.getRequestHost());
				}
				key = KmsClient.getKPLX(paket.ttp_ticket_id, jenis.id);
				// hack: jika id paket berbeda dengan yang di-request, jadikan key = null
				if(!KmsClient.isValidIdPaket(key, paket.pkt_id)) {
					key = null;
				}
			} catch (Exception e) {
				Logger.error(e, "Gagal meminta kunci lelang");
			}
		} else {
			key = getPublicKey(jenis, lelang.lls_id);
		}
		return key;
	}

	public static String getPrivateKey(JenisDokPenawaran jenis, Lelang_seleksi lelang, Paket paket, String peg_namauser) {
		boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
		if(isOSD) {
			Group group = Usergroup.findGroupByUser(peg_namauser);
			try {
				if(paket.ttp_ticket_id == null) {
					List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?",paket.pnt_id).fetch();
					paket.ttp_ticket_id = OSDUtil.createKPL(paket, anggota_panitia, lelang, BasicCtr.LPSE_URL);
				}
				if(group.isPanitia()) {
					return KmsClient.getKPLXd(jenis.id, paket.ttp_ticket_id);
				} else if(group.isAuditor()) {
					return KmsClient.getPrivateForAuditor(paket.ttp_ticket_id, jenis.id);
				}
			} catch (Exception e) {
				Logger.error(e, "Gagal meminta kunci lelang");
			}
		} else {
			return getPrivateKey(jenis, lelang.lls_id);
		}
		return null;
	}
}
