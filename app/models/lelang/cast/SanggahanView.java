package models.lelang.cast;

import models.common.Tahap;
import play.db.jdbc.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by rayhanfrds on 7/30/17.
 */
@SuppressWarnings("serial")
public class SanggahanView implements Serializable {

	public Long sgh_id;
	public String sgh_isi;
	public String rkn_nama;
	public Date sgh_tanggal;

	public List<SanggahanView> balasan = new ArrayList<SanggahanView>();

	public static List<SanggahanView> findAllByLelangAndTahap(Long lls_id, Tahap tahap, boolean attachBalasan) {
		List<SanggahanView> list = Query.find("SELECT sgh_id, sgh_isi, sgh_tanggal, rkn_nama FROM sanggahan sgh INNER JOIN peserta psr ON psr.psr_id=sgh.psr_id " +
				"  INNER JOIN rekanan rkn ON rkn.rkn_id=psr.rkn_id WHERE psr.lls_id = ? AND sgh.thp_id= ? AND sgh.peg_id IS NULL ORDER BY sgh_id", SanggahanView.class, lls_id, tahap.id).fetch();

		if (attachBalasan) {
			for (SanggahanView sanggahanView : list) {
				sanggahanView.balasan = findAllBalasan(sanggahanView.sgh_id);
			}
		}
		return list;
	}

	public static List<SanggahanView> findAllByLelangAndTahap(Long lls_id, Tahap tahap) {
		return findAllByLelangAndTahap(lls_id, tahap, false);
	}

	public static List<SanggahanView> findAllBalasan(Long sgh_id) {
		List<SanggahanView> list = Query.find("SELECT sgh_id, sgh_isi, sgh_tanggal FROM sanggahan WHERE san_sgh_id=? ORDER BY sgh_id", SanggahanView.class, sgh_id).fetch();
		return list;
	}

}
