package models.lelang.cast;

import play.db.jdbc.Query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by rayhanfrds on 7/29/17.
 */
@SuppressWarnings("serial")
public class DokPenawaranView implements Serializable {

	public Long lls_id;
	public String dok_judul;
	public Date dok_tgljam;
	public String dok_hash;
	public String rkn_nama;

	static final String QUERY = "SELECT psr.lls_id, dok.dok_judul, dok.dok_tgljam, dok.dok_hash, rkn.rkn_nama " +
            "FROM dok_penawaran dok " +
            "INNER JOIN peserta psr ON dok.psr_id=psr.psr_id " +
            "INNER JOIN rekanan rkn ON rkn.rkn_id=psr.rkn_id " +
            "WHERE psr.lls_id = ? AND dok.dok_jenis IN (1,2,3)";

	/**
	 *
	 * @param lls_id
	 * @return
	 */
	public static List<DokPenawaranView> findAllByLelang(Long lls_id){
		List<DokPenawaranView> list = Query.find(QUERY, DokPenawaranView.class, lls_id).fetch();
		return list;
	}
}
