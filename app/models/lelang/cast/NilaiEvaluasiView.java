package models.lelang.cast;

import ext.DecimalBinder;
import ext.RupiahBinder;
import models.lelang.Nilai_evaluasi;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Query;
import play.i18n.Messages;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by rayhanfrds on 7/29/17.
 */
@SuppressWarnings("serial")
public class NilaiEvaluasiView implements Serializable {

	public Long eva_id;
	
	public Long nev_id;

	public Long psr_id;
	
	@As(binder = RupiahBinder.class)
	public Double nev_harga;
	
	@As(binder = RupiahBinder.class)
	public Double nev_harga_terkoreksi;

	@As(binder = RupiahBinder.class)
	public Double nev_harga_negosiasi;
	
	@As(binder = DecimalBinder.class)
	public Double nev_skor;
	
	public Integer nev_urutan;
	
	public String nev_uraian;

	@Required
	public Nilai_evaluasi.StatusNilaiEvaluasi nev_lulus;
	
	public Long rkn_id;

	public String rkn_nama;
	
	public Integer is_pemenang_verif;

	public String psr_alasan_menang;

	public Double psr_harga;

	public Double psr_harga_terkoreksi;

	public boolean isPemenangVerif() {
		if (is_pemenang_verif == null)
			return false;
		return is_pemenang_verif == Integer.valueOf(1);
	}

	public String getKeteranganHasil() {
		String retval = psr_alasan_menang;
		if (!isPemenangVerif())
			if (retval == null || retval.isEmpty())
				retval = Messages.get("ct.retval");
		return retval;
	}

	//TODO : clean code, avoid duplicate code
	public String calculatePrecentageHargaTerkoreksi(Double nilai_hps) {
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format((nev_harga_terkoreksi * 100) / nilai_hps);
	}

	public static List<NilaiEvaluasiView> findAllByEvaluasiWithUrutHarga(Long evaluasiId) {
		List<NilaiEvaluasiView> list = Query.find("SELECT nev.eva_id, nev.nev_id, nev.psr_id, nev_harga, nev_harga_terkoreksi, nev_harga_negosiasi, nev_skor, nev_urutan, nev_uraian, nev_lulus, rkn_nama, rkn.rkn_id, psr.is_pemenang_verif, psr.psr_alasan_menang, psr.psr_harga, psr.psr_harga_terkoreksi  " +
				"FROM nilai_evaluasi nev INNER JOIN peserta psr ON psr.psr_id=nev.psr_id INNER JOIN rekanan rkn ON rkn.rkn_id=psr.rkn_id WHERE eva_id = ? " +
				"ORDER BY nev_harga ASC, nev_urutan ASC", NilaiEvaluasiView.class, evaluasiId).fetch();
		return list;
	}

	public static List<NilaiEvaluasiView> findAllByEvaluasi(Long evaluasiId) {
		List<NilaiEvaluasiView> list = Query.find("SELECT nev.eva_id, nev.nev_id, nev.psr_id, nev_harga, nev_harga_terkoreksi, nev_harga_negosiasi, nev_skor, nev_urutan, nev_uraian, nev_lulus, rkn_nama, rkn.rkn_id, psr.is_pemenang_verif, psr.psr_alasan_menang, psr.psr_harga, psr.psr_harga_terkoreksi  " +
				"FROM nilai_evaluasi nev INNER JOIN peserta psr ON psr.psr_id=nev.psr_id INNER JOIN rekanan rkn ON rkn.rkn_id=psr.rkn_id WHERE eva_id = ? " +
				"ORDER BY psr_id ASC, nev_urutan ASC", NilaiEvaluasiView.class, evaluasiId).fetch();
		return list;
	}

	public static List<NilaiEvaluasiView> findAllPemenangByEvaluasi(Long evaluasiId) {
		List<NilaiEvaluasiView> list = Query.find("SELECT nev.eva_id, nev.nev_id, nev.psr_id, nev_harga, nev_harga_terkoreksi, nev_harga_negosiasi, nev_skor, nev_urutan, nev_uraian, nev_lulus, rkn_nama, rkn.rkn_id, psr.is_pemenang_verif, psr.psr_alasan_menang, psr.psr_harga, psr.psr_harga_terkoreksi  " +
				"FROM nilai_evaluasi nev  INNER JOIN peserta psr ON psr.psr_id=nev.psr_id INNER JOIN rekanan rkn ON rkn.rkn_id=psr.rkn_id WHERE eva_id = ? " +
				"ORDER BY nev_urutan ASC", NilaiEvaluasiView.class, evaluasiId).fetch();
		return list;
	}

	public Double getHargaFinal(){
		if(nev_harga_negosiasi != null){
			return nev_harga_negosiasi;
		}
		return nev_harga_terkoreksi;
	}

	public boolean isLulus() {
		if (nev_lulus != null) {
			return nev_lulus.isLulus();
		}
		return false;
	}

	public String getStatus() {
		if(nev_lulus != null ) {
			return nev_lulus.label;
		}
		return "-";
	}
}
