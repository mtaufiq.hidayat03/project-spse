package models.lelang.cast;

import models.common.Tahap;
import play.db.jdbc.Query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by rayhanfrds on 7/29/17.
 */
@SuppressWarnings("serial")
public class PenjelasanView implements Serializable {

	public Long lls_id;
	public String dsl_uraian;
	public Date dsl_tanggal;
	public String rkn_nama;
	public String pnt_nama;

	static final String QUERY_PESERTA = "SELECT dl.lls_id, dl.dsl_uraian, dl.dsl_tanggal, rkn.rkn_nama, '' as pnt_nama " +
            "FROM diskusi_lelang dl " +
            "INNER JOIN peserta p ON dl.psr_id = p.psr_id " +
            "INNER JOIN rekanan rkn ON rkn.rkn_id = p.rkn_id " +
            "WHERE dl.lls_id = ? AND dl.psr_id IS NOT NULL AND dl.thp_id = ? " +
            "ORDER BY dl.dsl_tanggal ";

	static final String QUERY_PANITIA = "SELECT dl.lls_id, dl.dsl_uraian, dl.dsl_tanggal, p.pnt_nama, '' as rkn_nama " +
            "FROM diskusi_lelang dl " +
            "INNER JOIN panitia p ON dl.pnt_id = p.pnt_id " +
            "WHERE dl.lls_id = ? AND dl.pnt_id IS NOT NULL AND dl.thp_id = ? " +
            "ORDER BY dl.dsl_tanggal ";

	/**
	 * @param lelangId
	 * @param tahap
	 * @return
	 */
	public static List<PenjelasanView> listTanggapanPeserta(Long lelangId, Tahap tahap) {
		List<PenjelasanView> list = Query.find(QUERY_PESERTA, PenjelasanView.class, lelangId, tahap.id).fetch();
		return list;
	}

	/**
	 * @param lelangId
	 * @param tahap
	 * @return
	 */
	public static List<PenjelasanView> listTanggapanPanitia(Long lelangId, Tahap tahap) {
		List<PenjelasanView> list = Query.find(QUERY_PANITIA, PenjelasanView.class, lelangId, tahap.id).fetch();
		return list;
	}
}
