package models.lelang.cast;

import play.db.jdbc.Query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PesertaView implements Serializable {

    public Long psr_id;

    public String rkn_nama;

    public Date psr_tanggal;

    public Double psr_harga;

    public static List<PesertaView> findByLelang(Long id) {
        return Query.find("SELECT psr_id, rkn_nama, psr_tanggal, psr_harga FROM peserta p " +
                "LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id WHERE lls_id=?", PesertaView.class, id).fetch();
    }
}
