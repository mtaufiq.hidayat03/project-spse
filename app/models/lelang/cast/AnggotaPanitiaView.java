package models.lelang.cast;

import play.db.jdbc.Query;
import play.i18n.Messages;

import java.util.List;

public class AnggotaPanitiaView {

    public Long pnt_id;

    public String agp_jabatan;

    public String peg_nip;

    public String peg_nama;


    public String getJabatan() {
        if (agp_jabatan.equals("K")) {
            return Messages.get("ct.ketua");
        } else if (agp_jabatan.equals("W")) {
            return Messages.get("ct.wakil");
        } else if (agp_jabatan.equals("S")) {
            return Messages.get("ct.sekretaris");
        } else {
            return Messages.get("ct.anggota");
        }
    }

    public static List<AnggotaPanitiaView> findByPantia(Long panitiaId) {
        return Query.find("SELECT pnt_id, agp_jabatan, peg_nip, peg_nama FROM anggota_panitia ap " +
                "LEFT JOIN pegawai p ON ap.peg_id=p.peg_id WHERE pnt_id=? ", AnggotaPanitiaView.class, panitiaId).fetch();
    }
}
