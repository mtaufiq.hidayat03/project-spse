package models.lelang;

import models.agency.DaftarKuantitas;
import models.common.Tahap;
import models.common.UploadInfo;
import models.handler.ReverseAuctionDetilDownloadHandler;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;

import java.io.File;
import java.util.List;

@Table(name="reverse_auction_detail")
public class ReverseAuctionDetil extends BaseModel {
	
	@Id(sequence="seq_reverse_auction_detail", function="nextsequence")
	public Long rad_id;
	
	public Long ra_id;
	
	public Long psr_id;
	
	public Double rad_nev_harga;

	public String rad_dkh;
	
	public Long rad_id_attachment;

	@Transient
	public DaftarKuantitas dkh;

	@Override
	protected void postLoad() {
		if(!StringUtils.isEmpty(rad_dkh))
			dkh = CommonUtil.fromJson(rad_dkh, DaftarKuantitas.class);
	}
	
	
	public static ReverseAuctionDetil findBy(Long ra_id, Long psr_id) {		
		return find("ra_id=? and psr_id=?", ra_id,psr_id).first();
	}  
	
	@Override
	protected void prePersist() {
		if(dkh != null)
			rad_dkh = CommonUtil.toJson(dkh);
		super.prePersist();
	}

	public static List<ReverseAuctionDetil> findByPeserta(Long pesertaId) {
		return find("psr_id=? order by rad_id DESC", pesertaId).fetch();
	}

	public static ReverseAuctionDetil findLastPenawaran(Long auctionId, Long pesertaId) {
		ReverseAuctionDetil lastPenawaran = find("psr_id=? AND ra_id = ? order by rad_id DESC", pesertaId, auctionId).first();
		if (lastPenawaran == null) {
			lastPenawaran = new ReverseAuctionDetil();
			lastPenawaran.ra_id = auctionId;
			lastPenawaran.psr_id = pesertaId;
			lastPenawaran.rad_nev_harga = Double.valueOf(0);

		}
		ReverseAuction auction = ReverseAuction.findById(auctionId);
		if(auction != null) {
			Dok_lelang dok_lelang = Dok_lelang.findBy(auction.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
			if(dok_lelang != null) {
				lastPenawaran.dkh = dok_lelang.getRincianHPS();
			}
		}
		if(lastPenawaran.dkh != null) {
			lastPenawaran.dkh.emptyHargaSatuan();
			
		}
		return lastPenawaran;
	}

	public static ReverseAuctionDetil findPenawaranTermurah(Long auctionId) {
		return find("ra_id=? order by rad_nev_harga ASC", auctionId).first();
	}

	/**
	 * informasi nama peserta lelang
	 * @return
	 */
	public String getNamaPeserta() {
		return Query.find("select rkn_nama from peserta p left join rekanan r ON p.rkn_id=r.rkn_id where psr_id=?", String.class, psr_id).first();
	}

	@Transient
	public BlobTable getDokumen() {
		if(rad_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(rad_id_attachment);
	}

	@Transient
	public String getDownloadUrl() {
		final BlobTable dokumen = getDokumen();
		if(dokumen ==null)
	    	return null;
		return dokumen.getDownloadUrl(ReverseAuctionDetilDownloadHandler.class);
	}

}
