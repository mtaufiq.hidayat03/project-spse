package models.lelang;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;


@Table(name="HISTORY_JADWAL")
public class History_jadwal extends BaseModel {
	
	@Id	
	public Long dtj_id;

	@Id
	public Date hjd_tanggal_edit;

	public Date hjd_tanggal_awal;

	public Date hjd_tanggal_akhir;

	public String hjd_keterangan;
	
	public String getNamaPegawai() {
		return Query.find("select peg_nama from pegawai where peg_namauser = ?", String.class, audituser).first();
	}
	
	@Transient
	private Jadwal jadwal;

	public Jadwal getJadwal() {
		if(jadwal == null)
			jadwal = Jadwal.findById(dtj_id);
		return jadwal;
	}
	
	

}
