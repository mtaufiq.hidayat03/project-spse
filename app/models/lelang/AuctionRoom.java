package models.lelang;

import com.google.gson.JsonObject;
import ext.FormatUtils;
import models.agency.DaftarKuantitas;
import models.agency.Rincian_hps;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;
import play.mvc.Http;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * room auction
 * @author Arief Ardiyansah
 */
public class AuctionRoom {

    private final Long auctionId;
    private final Map<Long, AuctionOutbondPeserta> mapPeserta;
    private static final String MAP_ROOM_CACHE="auctionroom_";

    public static AuctionRoom get(Long auctionId) {
        AuctionRoom obj = Cache.get(MAP_ROOM_CACHE+auctionId, AuctionRoom.class);
        if(obj == null) {
            obj = new AuctionRoom(auctionId);
            Cache.set(MAP_ROOM_CACHE+auctionId, obj);
        }
        return obj;
    }

    private AuctionRoom(Long auctionId) {
        this.auctionId = auctionId;
        this.mapPeserta = new HashMap<>();
    }

    // clear room
    public void clear() {
        mapPeserta.clear();
        Cache.delete(MAP_ROOM_CACHE+auctionId);
    }

    public void addPeserta(Long pesertaId, Http.Outbound outbound) {
        if(mapPeserta.containsKey(pesertaId))
            return;
        AuctionOutbondPeserta obj = new AuctionOutbondPeserta();
        obj.pesertaId = pesertaId;
        obj.outbound = outbound;
        obj.updatePenawaran(auctionId);
        mapPeserta.put(pesertaId, obj);
        Cache.set(MAP_ROOM_CACHE+auctionId, this);
    }

    public void simpan(Rincian_hps[] data, Double total, Long pesertaId) {
        JsonObject result = new JsonObject();
        AuctionOutbondPeserta auctionOutbondPeserta = mapPeserta.get(pesertaId);
        try {
            DaftarKuantitas dk = new DaftarKuantitas();
            dk.items = Arrays.asList(data);
            dk.fixed = true;
            if (total > 0) {
                dk.total = total;
            }
            ReverseAuctionDetil auctionDetil = new ReverseAuctionDetil();
            auctionDetil.ra_id = auctionId;
            auctionDetil.psr_id = pesertaId;
            auctionDetil.rad_nev_harga = total;
            auctionDetil.dkh = dk;
            auctionDetil.save();
            result.addProperty("message", "ok");
            if(auctionOutbondPeserta.outbound.isOpen())
                auctionOutbondPeserta.outbound.send(result.toString());
            // update data to mapOutbond
            auctionOutbondPeserta.updatePenawaran(auctionId);
            mapPeserta.put(pesertaId, auctionOutbondPeserta);
            ReverseAuctionDetil penawaranTermurah = ReverseAuctionDetil.findPenawaranTermurah(auctionId);
            // update status to all connected socket
            Iterator<Long> iter = mapPeserta.keySet().iterator();
            while (iter.hasNext()) {
                AuctionOutbondPeserta obj = mapPeserta.get(iter.next());
                if (obj.outbound.isOpen()) {
                    result = new JsonObject();
                    JsonObject dataO = new JsonObject();
                    dataO.addProperty("id", obj.id);
                    dataO.addProperty("penawaran", obj.penawaran);
                    dataO.addProperty("waktu_pengiriman", obj.waktu_pengiriman);
                    dataO.addProperty("keterangan", penawaranTermurah != null && obj.id != null && obj.id.equals(penawaranTermurah.rad_id) ? "Penawaran Anda Terendah" : "");
                    result.add("data", dataO);
                    obj.outbound.send(result.toString());
                } else {
                    iter.remove();
                }
            }
            Cache.set(MAP_ROOM_CACHE+auctionId, this);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.error(Messages.get("lelang.gagal_kirim_auction")+" : %s", ex.getLocalizedMessage());
            result.addProperty("message", Messages.get("lelang.log_gagal_kirim"));
            if(auctionOutbondPeserta.outbound.isOpen())
                auctionOutbondPeserta.outbound.send(result.toString());
        }
    }

    public static class AuctionOutbondPeserta {
        public Long id;
        public Long pesertaId;
        public Http.Outbound outbound;
        public String penawaran;
        public String waktu_pengiriman;

        public void updatePenawaran(Long auctionId) {
            ReverseAuctionDetil lastPenawaran = ReverseAuctionDetil.findLastPenawaran(auctionId, pesertaId);
            if(lastPenawaran != null) {
                waktu_pengiriman = FormatUtils.formatDateTimeInd(lastPenawaran.auditupdate);
                penawaran = FormatUtils.formatCurrencyRupiah(lastPenawaran.rad_nev_harga);
                id = lastPenawaran.rad_id;
            }
        }
    }
}
