package models.lelang;

/**
 * interface status tender
 * pake #{statusTender /} di template
 * Object harus implement ini
 */
public interface StatusTender {

    boolean isLelangUlang();

    boolean isDitutup();

    boolean isUbahLelang();

    boolean isEvaluasiUlang();

    boolean isKonsultansi();
    
    boolean isJkKonstruksi();

    boolean isPenawaranUlang();

    boolean isItemized();
}
