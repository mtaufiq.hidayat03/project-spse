package models.lelang;

import models.common.Tahap;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name="SANGGAH_BANDING")
public class Sanggah_banding extends BaseModel {
	
	@Id(sequence="seq_sanggah_banding", function="nextsequence")
	public Long sghb_id;

	public Date sghb_tanggal;

	public Long sghb_id_attachment;

	public String sghb_isi;

	public Long psr_id;

	public Long sghb_id_datadukung_attachment;

	public Long sgh_id;

	@Transient
	private Peserta peserta;

	@Transient
	private Sanggahan sanggahan;
	
	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}

	public Sanggahan getSanggahan() {
		if(sanggahan == null)
			sanggahan = Sanggahan.findById(sgh_id);
		return sanggahan;
	}

	@Transient
	public BlobTable getDokumen() {
		if(sghb_id_attachment == null)
			return null;
		return  BlobTableDao.getLastById(sghb_id_attachment);
	}

	@Transient
	public String getDokUrl() {
		if(sghb_id_attachment == null)
			return null;
		BlobTable blob = BlobTableDao.getLastById(sghb_id_attachment);
		return blob.getDownloadUrl(null);
	}

	@Transient
	public BlobTable getDokumenDataDukung() {
		if(sghb_id_datadukung_attachment == null)
			return null;
		return  BlobTableDao.getLastById(sghb_id_datadukung_attachment);
	}
	
	@Transient
	public String getDokDataDukungUrl() {
		if(sghb_id_datadukung_attachment == null)
			return null;
		BlobTable blob = BlobTableDao.getLastById(sghb_id_datadukung_attachment);
		return blob.getDownloadUrl(null);
	}

	public static List<Sanggah_banding> findBy(Long lelangId)
	{
		return find("psr_id in (select psr_id from Peserta where lls_id=?)", lelangId).fetch();
	}

	public static List<Sanggah_banding> findByPeserta(Long pesertaId)
	{
		return find("psr_id = ?", pesertaId).fetch();
	}
	
	public static boolean isSudahBanding(Peserta peserta, Tahap tahap)
	{
		Sanggahan sanggahan = Sanggahan.findBy(peserta.psr_id, tahap);
		if(sanggahan == null)
			return true;
		int count = (int)count("psr_id=? AND sgh_id=?", peserta.psr_id, sanggahan.sgh_id);
		return count > 0;
	}
	
	public static void simpan(String uraian, File file, File datadukung, Long pesertaId, Date date, Tahap tahap) throws Exception
	{
		Sanggahan sanggahan = Sanggahan.findBy(pesertaId, tahap);
		if(sanggahan == null)
			return;
		Sanggah_banding sb = Sanggah_banding.find("psr_id=? and sgh_id=?", pesertaId, sanggahan.sgh_id).first();
        if(sb == null) {
			sb = new Sanggah_banding();
		}
		BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, sb.sghb_id_attachment);
		sb.sghb_id_attachment = blob.blb_id_content;
		sb.sghb_isi = uraian;
		sb.sghb_tanggal = date;
		sb.psr_id = pesertaId;
		sb.sgh_id = sanggahan.sgh_id;
		if(datadukung != null) {
			BlobTable blobDatadukung = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, datadukung, sb.sghb_id_datadukung_attachment);
			sb.sghb_id_datadukung_attachment = blobDatadukung.blb_id_content;
		}
		sb.save();
	}
}