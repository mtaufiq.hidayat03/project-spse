package models.lelang;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;


@Table(name="NERACA_PESERTA")
public class Neraca_peserta extends BaseModel {

    @Id
	public Long nrp_id;
	@Id
	public Long psr_id;
	
	public String nrp_nama;

	public Date nrp_tanggal;
	
	@Transient
	private Peserta peserta;
	
	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}

}
