package models.lelang;

import models.agency.Pegawai;
import models.common.Active_user;
import models.common.Tahap;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.secman.Group;
import org.joda.time.DateTime;

import ext.DatetimeBinder;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name="SANGGAHAN")
public class Sanggahan extends BaseModel {
	
	@Id(sequence="seq_sanggahan", function="nextsequence")
	public Long sgh_id;

	public Date sgh_tanggal;

	public String sgh_isi;

	public Long sgh_id_attachment;

	public String sgh_nama_file;

	public String sgh_pengirim;

	//relasi ke Pegawai
	public Long peg_id;

	//relasi ke Peserta
	public Long psr_id;

	public Long san_sgh_id;

	public Integer thp_id;
	
	@Transient
	private Peserta peserta;
	
	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}
	
	@Transient
	private Pegawai pegawai;

	public Pegawai getPegawai() {
		if (pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}
	
	public String getDokUrl() {
		if(sgh_id_attachment == null)
			return null;
		BlobTable blob = BlobTableDao.getLastById(sgh_id_attachment);
		return blob.getDownloadUrl(null);
	}
	
	/* dapatkan informasi balasan sanggahan */
	@Transient
	public List<Sanggahan> getBalasan() {
		return find("san_sgh_id=?", sgh_id).fetch();
	}
	
	public static List<Sanggahan> getListJawabSanggahan(Long sgh_id) {
		return find("san_sgh_id=? order by sgh_id ASC", sgh_id).fetch();
	}
	
	public static int countJawabanSanggahan(Tahap tahap, Long pesertaId) {
		return (int)count("san_sgh_id in (select sgh_id from Sanggahan where thp_id=? and psr_id=?)", tahap.id, pesertaId);
	}
	
	public static int countSanggahan(Tahap tahap, Long pesertaId, Jadwal jadwalSanggah) {
		if(jadwalSanggah == null)
			return 0;
		return (int)count("thp_id=? and psr_id=? and peg_id is null and sgh_tanggal > ? and sgh_tanggal < ?", tahap.id, pesertaId, jadwalSanggah.dtj_tglawal, jadwalSanggah.dtj_tglakhir);
	}
	
	public static int countSanggahan(Long lelangId, Tahap tahap) {
		return (int)count("peg_id is null and thp_id=? and psr_id in (select psr_id from peserta where lls_id=?)", tahap.id, lelangId);
	}
	
	/* dapatkan informasi checklist kategori sanggahan */
	@Transient
	public List<Checklist_sanggah> getChecklistKategori() {
		return Checklist_sanggah.find("sgh_id=? order by ckm_id asc", sgh_id).fetch();
	}
	
	/**
	 * dapatkan informasi sanggahan dari suatu lelang
	 * @param tahap
	 * @return
	 */
	public static List<Sanggahan> findBy(Long lelangId, Tahap tahap, Long pesertaId) {
		if(pesertaId != null)
			return find("psr_id in (select psr_id from Peserta where lls_id=?) and thp_id=? and san_sgh_id is null and psr_id=?",lelangId, tahap.id, pesertaId).fetch();
		return find("psr_id in (select psr_id from Peserta where lls_id=?) and thp_id=? and san_sgh_id is null",lelangId, tahap.id).fetch();
	}
	
	/**
	 * dapatkan informasi sanggahan setiap peserta lelang
	 * @param tahap
	 * @return
	 */
	public static Sanggahan findBy(Long pesertaId, Tahap tahap) {
		return find("thp_id=? and psr_id=?",tahap.id, pesertaId).first();
	}
	
	/**
	 * dapatkan informasi balasan sanggahan dari pegawai
	 * @param tahap
	 * @return
	 */
	public static Sanggahan findByPegawai(Long pegawaiId, Tahap tahap) {
		return find("thp_id=? and peg_id=?", tahap.id, pegawaiId).first();
	}

	/**
	 * cek apakah peserta diijinkan untuk menyanggah
	 * @param group
	 * @param tahap
	 * @param peserta
	 * @param date
	 * @return
	 */
	public static boolean isAllowKirimSanggah(Lelang_seleksi lelang, Group group, Tahap tahap, Peserta peserta, Date date){
		Jadwal jadwal_sanggah = Jadwal.findByLelangNTahap(lelang.lls_id, tahap);
		if(group.isRekanan() && jadwal_sanggah != null && jadwal_sanggah.isNow(date))
		{
			int count = countSanggahan(tahap, peserta.psr_id, jadwal_sanggah);
			long jumlah_penawar = 0;
			if(tahap == Tahap.SANGGAH_PRA) {
				jumlah_penawar = Dok_penawaran.count("psr_id=? AND dok_jenis=0", peserta.psr_id);
				boolean isNotPemenang = !peserta.isLulusPembuktian();
				return count == 0 && peserta.psr_id != null && jumlah_penawar > 0 && isNotPemenang;				
			}else if(tahap == Tahap.SANGGAH_ADM_TEKNIS){
				jumlah_penawar = Dok_penawaran.count("psr_id=? AND dok_jenis in (1,3)", peserta.psr_id);
				boolean isNotPemenang = !peserta.isLulusTeknis();
				return count == 0 && peserta.psr_id != null && jumlah_penawar > 0 && isNotPemenang;
			} else {
				jumlah_penawar = Dok_penawaran.count("psr_id=? AND dok_jenis in (1,2,3) AND dok_disclaim='1'", peserta.psr_id);
				boolean isNotPemenang = lelang.isExpress() ? peserta.is_pemenang_verif != 1 : peserta.is_pemenang != 1;
				return count == 0 && peserta.psr_id != null && jumlah_penawar > 0 && isNotPemenang;
			}
		}
		return false;
	}

	/**
	 * cek apakah peserta diijinkan untuk menyanggah banding
	 * Kondisi boleh sanggah banding:
	 * - konstruksi
	 * - sudah mengirim sanggah
	 * - HPS paket > x rupiah
	 * @param group
	 * @param tahap
	 * @param peserta
	 * @param date
	 * @return
	 */
	public static boolean isAllowKirimSanggahBanding(Lelang_seleksi lelang, Group group, Tahap tahap, Peserta peserta, Date date){
		Jadwal jadwal_sanggah = Jadwal.findByLelangNTahap(lelang.lls_id, tahap);
		if(group.isRekanan() && jadwal_sanggah != null && jadwal_sanggah.isNow(date))
		{
			int count = countSanggahan(tahap, peserta.psr_id, jadwal_sanggah);
			long jumlah_penawar = 0;
			if(tahap != Tahap.SANGGAH_PRA) {
				jumlah_penawar = Dok_penawaran.count("psr_id=? AND dok_jenis in (1,2,3) AND dok_disclaim='1'", peserta.psr_id);
			}
			
			boolean isNotPemenang = lelang.isExpress() ? peserta.is_pemenang_verif != 1 : peserta.is_pemenang != 1;
			return count == 1 && peserta.psr_id!=null && jumlah_penawar > 0 &&
					lelang.getKategori().isKonstruksi() && lelang.getHpsLelang() > 0 && isNotPemenang;
		}
		return false;
	}
	
	public static boolean isAllowBalas(Group group, Long lelangId, Tahap tahap, Date date)
	{
		if(group.isPanitia())
		{
			// Jadwal jadwal = Jadwal.findByLelangNTahap(lelangId, tahap);
			// if(jadwal != null && jadwal.dtj_tglawal != null)
			// {
			// 	DateTime akhir = new DateTime(jadwal.dtj_tglakhir.getTime());				
			// 	jadwal.dtj_tglakhir = akhir.plusDays(3).toDate();//tanggal akhir jadwal +3 hari (sesuai perLem LKPP)
			// 	return jadwal.isNow(date);
				
			// }
			// sesuai perlem 3 hari kerja, validasi dilepas sementara
			return true;
		}
		return false;
	}
	
	public static Sanggahan simpan(Long lelangId, Tahap tahap,String uraian, File file, Long sanggahan_parent_id, Date date) throws Exception {
		Active_user active_user =  Active_user.current();
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		if(!lelang.lls_status.isAktif())
			throw new Exception(Messages.get("lelang.matdums"));
		Sanggahan sanggahan = new Sanggahan();		
		if (active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelangId, active_user.rekananId);
			sanggahan.psr_id = peserta.psr_id;
			sanggahan.sgh_pengirim = "R";
			if (!isAllowKirimSanggah(lelang, active_user.group, tahap, peserta, date))
				throw new Exception(Messages.get("lelang.matdums"));
		} else {
			if (active_user.isPanitia() && !isAllowBalas(active_user.group, lelangId, tahap, date)) {
				throw new Exception(Messages.get("lelang.matdumbs"));
			}
			Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
			sanggahan.peg_id = pegawai.peg_id;
			if (active_user.isPpk()) {
				sanggahan.sgh_pengirim = "K";
			} else if (active_user.isPanitia()) {
				sanggahan.sgh_pengirim = "P";
			}
		}
		sanggahan.thp_id = tahap.id;
		if (sanggahan_parent_id != null)
			sanggahan.san_sgh_id = sanggahan_parent_id;
		sanggahan.sgh_tanggal = date;
		if (file != null) {
			BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file);
			sanggahan.sgh_id_attachment = blob.blb_id_content;
		}
		if (!CommonUtil.isEmpty(uraian)) {
			uraian.replace("<br>","<br/>");
			sanggahan.sgh_isi = uraian;
			sanggahan.save();
		}
		return sanggahan;
	}
}
