package models.lelang;

import controllers.BasicCtr;
import ext.FormatUtils;
import models.common.Tahap;
import models.common.UploadInfo;
import models.handler.DokPenawaranDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Pemilik;
import models.rekanan.Pengurus;
import models.rekanan.Rekanan;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name="DOK_PENAWARAN")
public class Dok_penawaran extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisDokPenawaran {
		PENAWARAN_KUALIFIKASI(0, "Dokumen Penawaran Kualifikasi"), 
		PENAWARAN_TEKNIS_ADMINISTRASI(1, "Dokumen Penawaran Administrasi dan Teknis"),
		PENAWARAN_HARGA(2, "Dokumen Penawaran Harga"), 
		PENAWARAN_TEKNIS_ADMINISTRASI_HARGA(3, "Dokumen Penawaran Administrasi, Teknis, dan Harga"),
		PENAWARAN_TAMBAHAN(4, "Dokumen Penawaran Tambahan"), 
		PENAWARAN_SUSULAN(5, "Dokumen Penawaran Susulan"),
		PENAWARAN_KUALIFIKASI_TAMBAHAN(6,"Dokumen Penawaran Kualifikasi Tambahan"), 
		PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT(7,"Dokumen Penawaran Kualifikasi Tambahan Draft"); 

		public final Integer id;

		public final String label;

		JenisDokPenawaran(int key, String label) {
			this.id = Integer.valueOf(key);
			this.label = label;
		}

		public static JenisDokPenawaran fromValue(Integer value) {
			JenisDokPenawaran jenis = null;
			if (value != null) {
				switch (value.intValue()) {
					case 0:
						jenis = PENAWARAN_KUALIFIKASI;
						break;
					case 1:
						jenis = PENAWARAN_TEKNIS_ADMINISTRASI;
						break;
					case 2:
						jenis = PENAWARAN_HARGA;
						break;
					case 3:
						jenis = PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
						break;
					case 4:
						jenis = PENAWARAN_TAMBAHAN;
						break;
					case 5:
						jenis = PENAWARAN_SUSULAN;
						break;
					case 6:
						jenis = PENAWARAN_KUALIFIKASI_TAMBAHAN;
						break;
				}
			}
			return jenis;
		}

		public boolean isPenawaran() {
			return this == PENAWARAN_HARGA || this == PENAWARAN_TEKNIS_ADMINISTRASI || this == PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
		}
		public boolean isAdmTeknisHarga(){
			return this == PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
		}
		public boolean isAdmTeknis() {
			return this == PENAWARAN_TEKNIS_ADMINISTRASI;
		}
		public boolean isHarga() {
			return this == PENAWARAN_HARGA;
		}
	}

	@Id(sequence="seq_dok_penawaran", function="nextsequence")
	public Long dok_id;

	public String dok_judul;

	public Date dok_tgljam;

	public Date dok_waktu_buka;

	public String dok_hash;

	public String dok_signature;

	public Long dok_id_attachment;
	
	public Integer dok_waktu; // jangka waktu penawaran
	
	public String dok_surat; // perlu evaluasi, apakah menyimpan surat penawaran dengan cara ini adalah yang terbaik
	
	public JenisDokPenawaran dok_jenis;

	public Integer dok_disclaim;

	//relasi kek Peserta
	public Long psr_id;	
	
	@Transient
	private Peserta peserta;

	public Long dok_struk_id;

	public String dok_struk_hash;

	public String dok_struk_nama;




	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}

	@Transient
	public boolean isPermission() {		
		if(dok_jenis == JenisDokPenawaran.PENAWARAN_KUALIFIKASI)				
			return true;
		if(dok_id_attachment != null)
		{
			BlobTable blob = getDokumen();
			if(dok_jenis == JenisDokPenawaran.PENAWARAN_TAMBAHAN)					
				return blob.blb_versi > 0;					
			if(dok_jenis == JenisDokPenawaran.PENAWARAN_SUSULAN)
			{
				if (dok_disclaim != null && dok_disclaim == 1)
					return blob.blb_versi > 0;						
			}
			else
			{
				if (dok_disclaim != null && dok_disclaim != 0)
					return blob.blb_versi > 0;
			}
		}
		return false;
	}
	
	@Transient
	public String getTglFile() 
	{
		if(dok_tgljam!=null && dok_disclaim != null){
			return FormatUtils.formatDateTimeInd(dok_tgljam);
		}		
		return "";
	}
	
	/**
	 * digunakan untuk menampilkan dokumen kualifikasi (hanya untuk penawaran)
	 * @return
	 */
	@Transient
	public BlobTable getDokumen() {
		if(dok_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dok_id_attachment);
	}
	
	/**
	 * digunakan untuk menampilkan list dokumen kualifikasi lainnya, kualifikasi lainnya
	 * @return
	 */
	@Transient
	public List<BlobTable> getDokumenList() {
		if(dok_id_attachment == null)
			return null;
		return BlobTableDao.listById(dok_id_attachment);
	}
	
	@Transient
	public String getDokName(){
		BlobTable dok = getDokumen();
		if( dok != null){
			Long size = dok.blb_ukuran;		
			return "<span title=\"" + FormatUtils.formatFileSize(size) + "\">" +
                    dok_judul + " (" + FormatUtils.formatFileSize(size) + ")</span>";
		}
		return "";
	}
	
	@Transient
	public String getDokUrl() {
		return getDokumen().getDownloadUrl(DokPenawaranDownloadHandler.class);
	}
	
	@Transient
	public boolean isAllowed()
	{	
		if(this == null || getDokumen() == null)
			return false;
		boolean allowed = this != null && dok_disclaim != null && dok_disclaim.equals(Integer.valueOf(1));
		Lelang_seleksi lelang = Lelang_seleksi.find("lls_id in (select lls_id from peserta where psr_id=?)", psr_id).first();
		if(lelang.isPrakualifikasi())
		{
			Evaluasi evaluasi_kualifikasi = Evaluasi.findKualifikasi(lelang.lls_id);
			if(evaluasi_kualifikasi != null) {
				Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.find("eva_id=? and psr_id=?", evaluasi_kualifikasi.eva_id, psr_id).first();
				allowed = (nilai_evaluasi != null && nilai_evaluasi.nev_lulus.isLulus());
			}
		}
		return allowed;
	}
	
	@Transient
	public boolean isShowUpload() 
	{
		if (dok_disclaim != null && dok_disclaim.equals(Integer.valueOf(0)))
			return false;
		else
			return true;
	}
	
	@Transient
	public boolean isShowDisclaimer() 
	{
		if (dok_id_attachment != null){
			BlobTable blob = BlobTableDao.getLastById(dok_id_attachment);
			return blob != null && !blob.blb_hash.equals(dok_hash);
		}
		return false;
	}

	@Transient
	public String getInfoKirim() {
		if (dok_id_attachment != null) {
			BlobTable blob = BlobTableDao.getLastById(dok_id_attachment);
			if (blob != null) {
				return Messages.get("lelang.dikirim") + FormatUtils.formatDateTimeInd(blob.blb_date_time);
			}
		}
		return Messages.get("lelang.blm_dikirim");
	}

	@Transient
	public String getInfo(boolean lelangv3) {
		if (dok_id_attachment != null) {
			BlobTable blob = BlobTableDao.getLastById(dok_id_attachment);
			if (blob != null) {
				if(lelangv3) {
					return "<table><tr><td>"+Messages.get("lelang.sdp")+" </td><td>: " + FormatUtils.formatDateInd(blob.blb_date_time)
							+ "</td></tr><tr><td>"+Messages.get("lelang.nama_file")+" </td><td>: " + blob.getFileName()
							+ "</td></tr><tr><td>"+Messages.get("lelang.masa_berlaku")+" </td><td>: " + FormatUtils.formatFileSize(blob.blb_ukuran)
							+ "</td></tr><tr><td>Hash </td><td>: " + blob.blb_hash + "</td></tr></table>";
				} else {
					return "<table><tr><td>"+Messages.get("lelang.sdp")+" </td><td>: " + FormatUtils.formatDateTimeInd(blob.blb_date_time)
							+ "</td></tr><tr><td>"+Messages.get("lelang.masa_berlaku")+"</td><td>: " + dok_waktu + " "+Messages.get("penawaran.hari_kalender")+"</td></tr></table>";
				}
			}
		}
		Lelang_seleksi lelang = Lelang_seleksi.find("lls_id in (select lls_id from peserta where psr_id=?)", psr_id).first();
		Tahap tahap = Tahap.PEMASUKAN_PENAWARAN; // dua file atau satu file, tahap upload ada di Tahap.PEMASUKAN_PENAWARAN
		if (lelang.isDuaTahap()) {
			if(dok_jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI)
				tahap = Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS;
			else if(dok_jenis == JenisDokPenawaran.PENAWARAN_HARGA)
				tahap = Tahap.PEMASUKAN_PENAWARAN_BIAYA;
		}		
		Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, tahap);
		String tglAwal = "";
		String tglAkhir = "";
		if (jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
			tglAwal = FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal);
			tglAkhir = FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir);
			return Messages.get("lelang.bd_wp")+" : " + tglAwal + " s.d. " + tglAkhir;
		}
		return Messages.get("lelang.bk");
	}
	
	public static List<Dok_penawaran> findPenawaranPeserta(Long pesertaId) {
		return find("psr_id=? and dok_jenis in ('"+JenisDokPenawaran.PENAWARAN_HARGA.id+"','"+JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id+"','"+JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id+"')", pesertaId).fetch();
	}
	
	public static Dok_penawaran findPenawaranPeserta(Long pesertaId, JenisDokPenawaran jenis) {
		return find("psr_id=? and dok_jenis=?", pesertaId, jenis).first();
	}

	/**
	 * get price offering and technical documents by participantId
	 * @param pesertaId participantId
	 * @return list of dok penawaran owned by participant*/
	public static List<Dok_penawaran> findPenawaranPesertaList(Long pesertaId) {
		return find("psr_id=? AND (dok_jenis=? OR dok_jenis=?)",
				pesertaId,
				Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA,
				Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI).fetch();
	}
	
	public static List<Dok_penawaran> findAllPenawaranTeknis(Long lelangId)
	{
		return find("psr_id in (select psr_id from peserta where lls_id=?) and dok_jenis='1' and dok_disclaim='1' order by psr_id", lelangId).fetch();
	}
	
	public static List<Dok_penawaran> findAllPenawaranHarga(Long lelangId, boolean satufile)
	{
		if(satufile)
			return find("psr_id in (select psr_id from peserta where lls_id=?) and dok_jenis='2' and dok_disclaim='1' order by psr_id", lelangId).fetch();
		else { // jika 2 file atau 2 tahap perlu dicheck kelulusan di tahap evaluasi administrasi teknis
			return find("psr_id in (select psr_id from nilai_evaluasi n, evaluasi e where n.eva_id=e.eva_id and e.lls_id=? and e.eva_jenis='2' and n.nev_lulus='1' and e.eva_status = '1') " +
					"and psr_id in (select psr_id from nilai_evaluasi n, evaluasi e where n.eva_id=e.eva_id and e.lls_id=? and e.eva_jenis='5' and n.nev_lulus='1') and dok_jenis='2' and dok_disclaim='1' order by psr_id", lelangId, lelangId).fetch();
		}
	}
	
	public static void simpanPenawaran(Long pesertaId, JenisDokPenawaran jenis, File file, Integer period) throws Exception
	{
		simpanPenawaran(pesertaId, jenis, file, period, null);
	}
	
	public static void simpanPenawaran(Long pesertaId, JenisDokPenawaran jenis, File file, Integer period, String surat) throws Exception
	{
		Dok_penawaran dok_penawaran = findPenawaranPeserta(pesertaId, jenis);
		if (dok_penawaran == null) {
			dok_penawaran = new Dok_penawaran();
			dok_penawaran.psr_id = pesertaId;
			dok_penawaran.dok_jenis = jenis;
		}		
		if(period!=null){
			dok_penawaran.dok_waktu = period;
		} else {
			Dok_penawaran dok_penawaran_jenis_1 = findPenawaranPeserta(pesertaId, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
			if (dok_penawaran_jenis_1 != null) 
				dok_penawaran.dok_waktu = dok_penawaran_jenis_1.dok_waktu;
		}
		if(surat!=null && !surat.isEmpty())
			dok_penawaran.dok_surat = surat.replaceAll("\n", "");

		BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment, BasicCtr.newDate());
		dok_penawaran.dok_id_attachment = blob.blb_id_content;
		/*if(dok_penawaran.dok_disclaim == null || dok_penawaran.dok_disclaim != Integer.valueOf(1))
			dok_penawaran.dok_disclaim = Integer.valueOf(0);*/
		dok_penawaran.dok_disclaim = Integer.valueOf(1);
		dok_penawaran.dok_tgljam = BasicCtr.newDate();
		dok_penawaran.dok_judul = blob.blb_nama_file;		
		dok_penawaran.dok_hash = blob.blb_hash;
		dok_penawaran.save();
	}
	/**
	 * simpan dokumen kualifikasi peserta
	 * @param peserta
	 */
	public static void simpanKualifikasi(Peserta peserta) {
		Dok_penawaran dok = findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
		if (dok == null) {
			dok = new Dok_penawaran();
			dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI;
		} 
		dok.psr_id = peserta.psr_id;
		dok.dok_tgljam = BasicCtr.newDate();
		dok.dok_judul = "Dok Prakualifikasi.xls";
		dok.dok_id_attachment = Long.valueOf(0);
		dok.save();
		List<Pemilik> list_pemilik = Pemilik.find("rkn_id=?", peserta.rkn_id).fetch();
		List<Pengurus> list_pengurus = Pengurus.find("rkn_id=?", peserta.rkn_id).fetch();
		Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
		peserta.psr_identitas = CommonUtil.toJson(rekanan);
		peserta.psr_pemilik = CommonUtil.toJson(list_pemilik);
		peserta.psr_pengurus = CommonUtil.toJson(list_pengurus);
		peserta.save();
	}
	
	/**
	 * simpan dokumen kualifikasi peserta
	 * @param pesertaId
	 * @param blob
	 */
	public static void simpanKualifikasiLainnyaDraft(Long pesertaId, BlobTable blob, Date currentDate) {
		Dok_penawaran dok = findPenawaranPeserta(pesertaId, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
		if (dok == null) {
			dok = new Dok_penawaran();
			dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT;
		} 
		dok.psr_id = pesertaId;
		dok.dok_tgljam = currentDate;
		dok.dok_judul = "Draft Dok Kualifikasi lainnya.xls";
		dok.dok_id_attachment = blob.blb_id_content;
		dok.dok_disclaim = Integer.valueOf(1);
		dok.save();
	}	
	
	public static void simpanKualifikasiLainnya(Peserta peserta) {
	    	try {
	    		Dok_penawaran dok_draft = findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
	        if (dok_draft != null) {
	        		Dok_penawaran dok = findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN);
	            if (dok == null) {
	                dok = new Dok_penawaran();
	                dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN;
	                dok.dok_judul = "Dok Kualifikasi lainnya.xls";
	                dok.dok_id_attachment = getNextSequenceValue("seq_epns");
	                dok.dok_disclaim = Integer.valueOf(1);
	                dok.psr_id = peserta.psr_id;
	            }
	            dok.dok_tgljam = BasicCtr.newDate();
	            dok.save();
	            Integer versi = Integer.valueOf(0);
	            BlobTable blob = null; 
	            List<BlobTable> list = BlobTableDao.listById(dok_draft.dok_id_attachment);
	            if (list != null) {
		            for(BlobTable blb: list) {
		            		blob = BlobTableDao.getLastById(dok.dok_id_attachment);
		            		if (blob != null)
			            		versi = blob.blb_versi + 1;
		            		String sql="UPDATE blob_table SET blb_id_content=?, blb_versi=? where blb_id_content = ? and blb_versi= ?";
		            		Query.update(sql, dok.dok_id_attachment, versi, blb.blb_id_content, blb.blb_versi);
		            }
		        }
	        }		        
	    	} catch (Exception e){
	    		Logger.error(e, "Simpan Dokumen Kualifikasi Lainnya gagal : %s", e.getMessage());
	    	}
	}

	/**
	 * simpan dokumen penawaran latihan
	 * @param pesertaId
	 * @param jenis
	 * @param currentDate
	 * @throws Exception 
	 *//*
	public void simpanPenawaranLatihan(Long pesertaId, JenisDokPenawaran jenis) throws Exception{
		Dok_penawaran dok_penawaran = findPenawaranPeserta(pesertaId, jenis);
		if (dok_penawaran == null) {
			dok_penawaran = new Dok_penawaran();
			dok_penawaran.psr_id = pesertaId;
			dok_penawaran.dok_jenis = jenis;
		}
		File file = new File("dok_penawaran.rhs");
		BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);		
		dok_penawaran.dok_id_attachment = blob.blb_id_content;
		dok_penawaran.dok_disclaim = Integer.valueOf(1);
		dok_penawaran.dok_tgljam = controllers.BasicCtr.newDate();
		dok_penawaran.dok_judul = blob.blb_nama_file;		
		dok_penawaran.dok_hash = blob.blb_hash;
		dok_penawaran.save();	
	}*/
	
	/**
	 * menampilkan daftar dokumen penawaran 
	 * @param rekananId
	 */
	public static List<Dok_penawaran> getListDokumen (Long rekananId){
		return find("dok_id in (select dok_id from dok_penawaran d, peserta p, rekanan r where d.psr_id=p.psr_id and p.rkn_id=r.rkn_id and r.rkn_id=?)", rekananId).fetch();
	}
	
	public Long lelangId(){
		return Query.find("select lls_id from dok_penawaran d, peserta p where d.psr_id=p.psr_id and p.psr_id=?", Long.class, psr_id).first();
		
	}
	
	public static UploadInfo simpanSusulan(Peserta peserta, File file, Date currentDate) throws Exception {
		String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
		String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
		if (File.separatorChar == '\\')// windows
			path += '\\' + text;
		else
			path += "//" + text;
		File newFile = new File(path);
		file.renameTo(newFile);
		Dok_penawaran dok_penawaran = findPenawaranPeserta(peserta.psr_id,JenisDokPenawaran.PENAWARAN_SUSULAN);
		if (dok_penawaran == null){
			dok_penawaran = new Dok_penawaran();
			dok_penawaran.dok_jenis = JenisDokPenawaran.PENAWARAN_SUSULAN;
			dok_penawaran.psr_id = peserta.psr_id;
			dok_penawaran.dok_tgljam = currentDate;
			dok_penawaran.dok_judul = "DokPrakualifikasiSusulan.xls";
		}
		BlobTable blob = null;
		if (dok_penawaran.dok_id_attachment != null) {

			BlobTable oldBlob = BlobTable.find("blb_id_content = ?",dok_penawaran.dok_id_attachment).first();
			if(oldBlob != null){
				oldBlob.deleteAllVersion();
			}

			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile, dok_penawaran.dok_id_attachment);
		} else {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile);
		}

		dok_penawaran.dok_id_attachment = blob.blb_id_content;
		dok_penawaran.dok_disclaim = Integer.valueOf(1);
		dok_penawaran.save();
		return UploadInfo.findBy(blob);
	}
	
	/**
	 * check peserta tersebut penawar atau cuma asal ikut
	 * @param lelangId
	 * @param rekananId
	 * @return
	 */
	public static boolean isAnyPenawaran(Long lelangId, Long rekananId) {
		return count("psr_id in (select psr_id from peserta where lls_id=? and rkn_id=?) and dok_jenis in (0,1,2,3)", lelangId, rekananId) > 0;
	}

	public static void simpanStruk(Long pesertaId, JenisDokPenawaran jenis, File file) throws Exception {
		Dok_penawaran dok_struk = findPenawaranPeserta(pesertaId, jenis);
		if (dok_struk == null) {
			throw new Exception("Dok Penawaran tidak ditemukan");
		}

		BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, dok_struk.dok_struk_id);
		dok_struk.dok_struk_id = blob.blb_id_content;
		dok_struk.dok_struk_nama = blob.blb_nama_file;
		dok_struk.dok_struk_hash = blob.blb_hash;
		dok_struk.save();
	}

	@Transient
	public SuratPenawaranContent getSuratPenawaranContent(){
		if (!CommonUtil.isEmpty(dok_surat))
			return CommonUtil.fromJson(dok_surat, SuratPenawaranContent.class);
		return null;
	}

	@Transient
	public String getTglKirim()
	{
		BlobTable dokumen = getDokumen();
		if(dokumen != null){
			return FormatUtils.formatDateTimeInd(dokumen.blb_date_time);
		}
		return null;
	}

	@Transient
	public String getTglKirimWithoutTime()
	{
		BlobTable dokumen = getDokumen();
		if(dokumen != null){
			return FormatUtils.formatDateInd(dokumen.blb_date_time);
		}
		return null;
	}
	
	public static long countPenawaranHarga(Long lelangId) {
		return count("psr_id in (select psr_id from peserta where lls_id=?) AND dok_jenis in (2,3) AND dok_disclaim = '1'", lelangId);
	}

	public static long countPenawaranAdminTeknis(Long lelangId) {
		return count("psr_id in (select psr_id from peserta where lls_id=?) AND dok_jenis in (1,3) AND dok_disclaim = '1'", lelangId);
	}

}
