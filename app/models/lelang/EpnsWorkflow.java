package models.lelang;

import models.agency.Paket;
import models.agency.Paket.StatusPaket;
import models.common.StatusLelang;
import models.jcommon.db.base.BaseModel;
import models.lelang.Evaluasi.StatusEvaluasi;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.List;


@Table(name="WORKFLOW")
public class EpnsWorkflow extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum Process {
		LELANG_CREATE, LELANG_EVALUATE, LELANG_PRA_EVALUATE,
		// Khusus untuk lelang 2 tahap
		LELANG_PRA_EVALUATE_TEKNIK;

		public static Process fromValue(Integer value) {
			Process process = null;
			if (value != null) {
				switch (value.intValue()) {
					case 0:
						process = LELANG_CREATE;
						break;
					case 1:
						process = LELANG_EVALUATE;
						break;
					case 2:
						process = LELANG_PRA_EVALUATE;
						break;
					case 3:
						process = LELANG_PRA_EVALUATE_TEKNIK;
						break;
				}
			}
			return process;
		}

		public Integer toValue() {
			Integer value = null;
			switch (this) {
				case LELANG_CREATE:
					value = Integer.valueOf(0);
					break;
				case LELANG_EVALUATE:
					value = Integer.valueOf(1);
					break;
				case LELANG_PRA_EVALUATE:
					value = Integer.valueOf(2);
					break;
				case LELANG_PRA_EVALUATE_TEKNIK:
					value = Integer.valueOf(3);
					break;
			}
			return value;
		}

		public boolean isPersetujuanLelang() {
			return this == LELANG_CREATE;
		}

		public boolean isPersetujuanEvaluasi() {
			return this == LELANG_EVALUATE;
		}

		public boolean isPersetujuanEvaluasiPra() {
			return this == LELANG_PRA_EVALUATE;
		}

		public boolean isPersetujuanEvaluasiTeknis() {
			return this == LELANG_PRA_EVALUATE;
		}
	}
	
	@Enumerated(EnumType.ORDINAL)
	public enum State {
		START, NEED_APPROVAL, APPROVE, DECLINE, REVISE, FINISHED;

		public static State fromValue(Integer value) {
			State state = null;
			if (value != null) {
				switch (value.intValue()) {
					case 0:
						state = START;
						break;
					case 1:
						state = NEED_APPROVAL;
						break;
					case 2:
						state = APPROVE;
						break;
					case 3:
						state = DECLINE;
						break;
					case 4:
						state = REVISE;
						break;
					case 5:
						state = FINISHED;
						break;
				}
			}
			return state;
		}

		public Integer toValue() {
			Integer value = null;
			switch (this) {
				case START:
					value = Integer.valueOf(0);
					break;
				case NEED_APPROVAL:
					value = Integer.valueOf(1);
					break;
				case APPROVE:
					value = Integer.valueOf(2);
					break;
				case DECLINE:
					value = Integer.valueOf(3);
					break;
				case REVISE:
					value = Integer.valueOf(4);
					break;
				case FINISHED:
					value = Integer.valueOf(5);
					break;
			}
			return value;
		}

		public String toString() {
			String label = "";
			switch (this) {
				case START:
				case NEED_APPROVAL:
					label = Messages.get("lelang.minta_persetujuan");
					break;
				case APPROVE:
					label = Messages.get("lelang.disetujui");
					break;
				case DECLINE:
					label = Messages.get("lelang.tidak_disetujui");
					break;
				case REVISE:
					label = Messages.get("lelang.revisi");
					break;
				default:
					label = "-";
					break;
			}
			return label;
		}

		public boolean isStart() {
			return this == START;
		}

		public boolean isNeed_Approval() {
			return this == NEED_APPROVAL;
		}

		public boolean isApprove() {
			return this == APPROVE;
		}

		public boolean isDecline() {
			return this == DECLINE;
		}

		public boolean isRevised() {
			return this == REVISE;
		}

		public boolean isFinish() {
			return this == FINISHED;
		}

		public boolean isNotEditable() {
			return this == NEED_APPROVAL || this == APPROVE || this == FINISHED;
		}
	}

	@Id
	public Long wf_id;
	
	public Long foreign_wf_id;
	
	@Required
	public State wf_state;
	
	@Required
	public Process wf_process;

	public String wf_comment;
	
	public EpnsWorkflow getReply() {		
		return find("foreign_wf_id=?",wf_id).first();
	}
	
	public static Long getLastResponseId(Long wf_id){
		Long result = null;
		EpnsWorkflow obj = findById(wf_id);
		EpnsWorkflow reply = obj.getReply();
		while(reply!=null){
			result = reply.wf_id;
			reply = reply.getReply();
		}
		if(result==null)
			return wf_id;
		else
			return result;
	}
	
	public static State getLastWorkflowState(Long wf_id){
		EpnsWorkflow workflow = findById(getLastResponseId(wf_id));
		return workflow.wf_state;
	}
	
	public static Long startWorkflowEvaluasi(Evaluasi eva) {
		if (eva == null)
			return null;
		
		if (eva.eva_wf_id == null || eva.eva_wf_id.intValue() == 0) {
			Logger.debug("startWorkflowEvaluasi() for Evaluasi no="+eva.eva_id);
		} else {
			Logger.debug("startWorkflowEvaluasi() - workflow already started for Evaluasi no="+eva.eva_id);
			return eva.eva_wf_id;
		}
		if(eva.eva_jenis.isAkhir())
			return generateWorkflow(Process.LELANG_EVALUATE);
		
		return generateWorkflow(Process.LELANG_PRA_EVALUATE);
	}
	
	public static Long generateWorkflow(Process process){
		return updateWorkflow(null, null, null, process, null);
	}
	
	/**
	 * update workflow untuk lelang versi 3.x
	 * @param wf_id
	 * @param idUtama
	 * @param state
	 * @param process
	 * @param comment
	 * @return
	 */
	public static Long updateWorkflow(Long wf_id, Long idUtama, State state, Process process, String comment){
		EpnsWorkflow epnsWorkflow = new EpnsWorkflow();
		if(wf_id==null) 		// generate new workflow
			epnsWorkflow.wf_state = State.START;
		else {			
			if(process == Process.LELANG_CREATE){
				Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(idUtama);
				Paket paket = Paket.findById(lelang_seleksi.pkt_id);
				EpnsWorkflow workflow = findById(lelang_seleksi.lls_wf_id);
				workflow.wf_state = State.FINISHED;
				if(state == State.APPROVE){
					lelang_seleksi.lls_status = StatusLelang.AKTIF;
					lelang_seleksi.lls_tgl_setuju = controllers.BasicCtr.newDate();
					paket.pkt_status = StatusPaket.SEDANG_LELANG;										
					// generate key for lelang
					Lelang_key.generateKey(lelang_seleksi);
					
				}
				else if(state == State.DECLINE){
					lelang_seleksi.lls_status  = StatusLelang.DITOLAK;
					paket.pkt_status = StatusPaket.LELANG_DITOLAK;
				}
				paket.save();
				lelang_seleksi.save();		
				workflow.save();
			}
			else if(process == Process.LELANG_EVALUATE){
				Evaluasi evaluasi = Evaluasi.findById(idUtama);
				EpnsWorkflow workflow = findById(evaluasi.eva_wf_id);
				workflow.wf_state = State.FINISHED;
				if(state == State.APPROVE){
					evaluasi.eva_status = StatusEvaluasi.SELESAI;
					evaluasi.eva_tgl_setuju = controllers.BasicCtr.newDate();
					List<Nilai_evaluasi> list = Nilai_evaluasi.findBy(evaluasi.eva_id);
					for (Nilai_evaluasi nilPeserta : list) {
						if(nilPeserta.nev_lulus.isLulus()){
							Peserta peserta = Peserta.findById(nilPeserta.psr_id);
							peserta.is_pemenang = Integer.valueOf(1);
							peserta.save();
						}
					}				
				}
				else if(state == State.DECLINE){
					evaluasi.eva_status = StatusEvaluasi.DITOLAK;
					Lelang_seleksi lelang = Lelang_seleksi.findById(evaluasi.lls_id);
					Paket paket = Paket.findById(lelang.pkt_id);
					lelang.lls_status = StatusLelang.DITUTUP;
					lelang.lls_ditutup_karena = "Evaluasi Tidak Disetujui PPK";
					lelang.save();
					paket.pkt_status = StatusPaket.SELESAI_LELANG;
					paket.save();
					
				}
				evaluasi.save();
				workflow.save();
			}
			else if(process == Process.LELANG_PRA_EVALUATE || process == Process.LELANG_PRA_EVALUATE_TEKNIK){
				Evaluasi evaluasi = Evaluasi.findById(idUtama);
				EpnsWorkflow workflow = findById(evaluasi.eva_wf_id);
				workflow.wf_state = State.FINISHED;
				if(state == State.APPROVE){
					evaluasi.eva_status = StatusEvaluasi.SELESAI;
					evaluasi.eva_tgl_setuju = controllers.BasicCtr.newDate();					
				}
				else if(state == State.DECLINE){
					evaluasi.eva_status = StatusEvaluasi.DITOLAK;
					Lelang_seleksi lelang = Lelang_seleksi.findById(evaluasi.lls_id);
					Paket paket = Paket.findById(lelang.pkt_id);
					lelang.lls_status = StatusLelang.DITUTUP;
					lelang.lls_ditutup_karena = "Evaluasi Tidak Disetujui PPK";
					lelang.save();	
					paket.pkt_status = StatusPaket.SELESAI_LELANG;
					paket.save();								
				}
				evaluasi.save();
				workflow.save();
			}	
			epnsWorkflow.foreign_wf_id = wf_id;
			epnsWorkflow.wf_state = state;
			epnsWorkflow.wf_comment = comment;
		}
		epnsWorkflow.wf_process = process;
		epnsWorkflow.save();
		return epnsWorkflow.wf_id;
	}

}