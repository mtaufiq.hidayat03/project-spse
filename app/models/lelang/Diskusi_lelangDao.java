package models.lelang;

import models.common.Tahap;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import play.db.jdbc.Query;

import java.io.ByteArrayInputStream;

public class Diskusi_lelangDao {

	/**Insert data berupa json. Data ini didapat dari Ajax
	 *  
	 * @param lls_id
	 * @param panitiaId
	 * @param usernamePeserta
	 * @param data: json String[]=> {dokumen, bab, isi}
	 */
	public synchronized static void insertFromJson(Long lls_id, Long panitiaId, Long ppkId, String usernamePeserta, String[] data, Tahap tahap) {
		//ini pertanyaan apa jawaban? dilihat dari msgJson ke-0: {tanya, jawab} atau dari userNamePeserta
		//dapatkan nomor pertanyaan
		Integer nomor_pertanyaan =null;
		
		Diskusi_lelang dll=new Diskusi_lelang();
		dll.lls_id=lls_id;
		if(panitiaId!=null)
			dll.pnt_id=panitiaId;
	
		
		if(usernamePeserta!=null)
		{
			//ini pertanyaan
			Peserta peserta=Peserta.findByUsenameAndIDLelang(usernamePeserta, lls_id);
			dll.psr_id=peserta.psr_id;
			nomor_pertanyaan=Query.find("select max(dsl_nomor) from  Diskusi_lelang where lls_id=? and psr_id <> null", Integer.class, lls_id).first();
			if(nomor_pertanyaan==null)
				nomor_pertanyaan=1;
			else
				nomor_pertanyaan++;
			dll.dsl_dokumen=data[1];
			dll.dsl_bab=data[2];
			dll.dsl_uraian=data[3];
//			dll.dsl_nomor=nomor_pertanyaan;
			
		}
		else
		{
			//ini adalah jawaban
			dll.dsl_uraian=data[2];
			//cari pertanyaanya
			Long pertanyaanId=Long.parseLong(data[1]);
			dll.dsl_pertanyaan_id = pertanyaanId;
			if(panitiaId!=null)
				dll.pnt_id=panitiaId;
			if(ppkId!=null)
				dll.ppk_id=ppkId;
		}
		
		dll.dsl_tanggal=controllers.BasicCtr.newDate();
		dll.thp_id=tahap.id; 
		dll.save();
	}
	
	
	public static void attachFile(Long dsl_id, byte[] fileData, String fileName) throws Exception
	{
		ByteArrayInputStream is=new ByteArrayInputStream(fileData);
		BlobTable blob=BlobTableDao.saveInputStream(ARCHIEVE_MODE.ARCHIEVE, is, fileName);
		Diskusi_lelang dsl=Diskusi_lelang.findById(dsl_id);
		dsl.dsl_id_attachment=blob.blb_id_content;
		dsl.save();
	}	
	
	
}
