package models.lelang;

import com.google.gson.Gson;
import controllers.BasicCtr;
import ext.DateBinder;
import models.agency.Pegawai;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import models.sso.common.adp.util.ADPWSServer;
import play.cache.Cache;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name="BLACKLIST")
public class Blacklist extends BaseModel {

	//private static final String NUMBER_REGEX = "^\\d+$";
    /**
     * ID Blacklist
     */
	@Id(sequence="seq_blacklist", function="nextsequence")
    public Long bll_id;
	
	@As(binder=DateBinder.class)
	public Date bll_tglawal;
    
	@As(binder=DateBinder.class)
	public Date bll_tglakhir;

	public String bll_alasan;

	public String bll_sanksi;

	public Date bll_tglusul;

	public Date bll_tglsetuju;

	public Long bll_disetujui;
	
	public String nama_direktur;
	
	public String ktp_direktur;
	
	public String sk_blacklist;
	
	public Long bll_attachment;
	
	public String npwp_direktur;
	@As(binder=DateBinder.class)
	public Date bll_pencabutan;
	
	public String psk_blacklist;
	
	public Long bll_pcattachment;
	
	public Date bll_hapus_blacklist;

	//relasi ke Pegawai
	public Long peg_id;

	//relasi ke Rekanan
	public  Long rkn_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	@Transient
	private Pegawai pegawai;
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}
	
	@Transient
	public BlobTable getBlob() {
		if(bll_attachment == null)
			return null;
		return BlobTableDao.getLastById(bll_attachment);
	}
	
	@Transient
	public BlobTable getPenBlob() {
		if(bll_pcattachment == null)
			return null;
		return BlobTableDao.getLastById(bll_pcattachment);
	}
	
	/**
	 * Fungsi {@code findAllValidBlacklist} digunakan untuk mendapatkan daftar blacklist perusahaan yang valid
	 * untuk sekarang
	 *
	 * @return daftar blacklist yang valid sekarang
	 */
	@SuppressWarnings("unchecked")
	public static List<Blacklist> findAllValidBlacklist() {
		// TODO: Apakah cache data ini selama 1 menit terlalu lama?
		List listBlacklist;
		if ((listBlacklist = (List) Cache.get("allValidBlacklistData")) != null) {
			// continue
		} else {
			Date now = BasicCtr.newDate();
			listBlacklist = find("bll_tglawal <= ? AND bll_tglakhir >= ?", now, now).fetch();
			// add to cache for 1-minute
			Cache.safeSet("allValidBlacklistData", listBlacklist, BasicCtr.DEFAULT_CACHE);
		}
		return listBlacklist;
	}
	
	public static void BlacklistAdp (String rkn_id, String bll_id, String bll_alasan){
		//BlacklistSSO listsso1 = BlacklistSSO.findById(bllId);
		String jsonResponse = "";
//		SecureWSClient sWS = null;
		try {
			ADPWSServer client = new ADPWSServer();
    		boolean listsso = client.addBlacklistAdp(Long.valueOf(rkn_id), Long.valueOf(bll_id), bll_alasan);
   			jsonResponse = new Gson().toJson(listsso);    		
		} 
    	catch (Exception e) {
    		jsonResponse = e.toString();    		
		}    		
	}
	
	public static void simpan(Blacklist blacklist, File attachment,  File attachment2) throws Exception {
		if (attachment != null) {
			BlobTable bt = null;
			if (blacklist.bll_attachment == null) {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, attachment);
			} else {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, attachment ,blacklist.bll_attachment);
			} 	
			blacklist.bll_attachment = bt.blb_id_content; 			
		}
		if(attachment2 != null) {
			BlobTable bt = null;
			if(blacklist.bll_pcattachment == null) {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, attachment2);
			} else {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, attachment2 ,blacklist.bll_pcattachment);
			}	
			blacklist.bll_pcattachment = bt.blb_id_content; 
		}
		blacklist.save();
		BlacklistAdp(String.valueOf(blacklist.rkn_id), String.valueOf(blacklist.bll_id), String.valueOf(blacklist.bll_alasan));
	}
}
