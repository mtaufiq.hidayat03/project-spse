package models.lelang;

import models.agency.Sppbj;
import models.common.*;
import models.handler.BeritaAcaraDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.lelang.cast.NilaiEvaluasiView;
import models.lelang.cast.PesertaView;
import org.apache.commons.text.WordUtils;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;


@Table(name="BERITA_ACARA")
public class Berita_acara extends BaseModel {

	public static final List<Tahap> BERITA_ACARA_SATU_FILE = Arrays.asList(Tahap.UPLOAD_BA_PENJELASAN, Tahap.UPLOAD_BA_EVALUASI_PENAWARAN, Tahap.UPLOAD_BA_HASIL_LELANG, Tahap.PENGUMUMAN_PEMENANG_AKHIR);
	public static final List<Tahap> BERITA_ACARA_DUA_FILE = Arrays.asList(Tahap.UPLOAD_BA_PENJELASAN, Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS, Tahap.UPLOAD_BA_EVALUASI_HARGA, Tahap.UPLOAD_BA_HASIL_LELANG, Tahap.PENGUMUMAN_PEMENANG_AKHIR);
	
	static final Template TEMPLATE_BA_NEGO = TemplateLoader.load("/lelang/berita_acara/BA-Negosiasi-Template.html");
	static final Template TEMPLATE_BA = TemplateLoader.load("/lelang/berita_acara/BA-template.html");
	static final Template TEMPLATE_BA_PENJELASAN = TemplateLoader.load("/lelang/berita_acara/BA-Penjelasan-Template.html");
	static final Template TEMPLATE_BAHP_PRAKUALIFIKASI = TemplateLoader.load("/lelang/berita_acara/BAHP-prakualifikasi.html");
	
	@Id(sequence="seq_berita_acara", function="nextsequence")
	public Long brc_id;

	@Required
	public Tahap brc_jenis_ba;

	public Long brc_id_attachment;
	
	public String brt_no;
	
	public Date brt_tgl_evaluasi;

    public String brt_info;

	public String brt_tempat;

	//relasi ke Lelang_seleksi
	public Long lls_id;
	
	@Transient
	private Lelang_seleksi lelang_seleksi;
	
	public Lelang_seleksi getLelang_seleksi() {
		if(lelang_seleksi == null)
			lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}
	
	@Transient
	public List<BlobTable> getDokumen() {
		if(brc_id_attachment == null)
			return null;
		return BlobTableDao.listById(brc_id_attachment);
	}

    @Transient
    public String getJudul() {
        if(brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_PENAWARAN)
            return WordUtils.capitalizeFully(Messages.get("lelang.ba_ea"));
        else if(brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS)
            return WordUtils.capitalizeFully(Messages.get("lelang.ba_eat"));
        else if(brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_HARGA)
            return WordUtils.capitalizeFully(Messages.get("lelang.ba_eh"));
        else if(brc_jenis_ba == Tahap.UPLOAD_BA_HASIL_LELANG)
            return WordUtils.capitalizeFully(Messages.get("lelang.ba_hp"));
		else if(brc_jenis_ba == Tahap.PENGUMUMAN_PEMENANG_AKHIR && getLelang_seleksi().isAuction()) 
			return WordUtils.capitalizeFully(Messages.get("lelang.ba_ra"));
		else if(brc_jenis_ba == Tahap.PENGUMUMAN_PEMENANG_AKHIR && !getLelang_seleksi().isAuction()) 
			return WordUtils.capitalizeFully(Messages.get("lelang.ba_hn"));
		else if(brc_jenis_ba == Tahap.UPLOAD_BA_PENJELASAN_PRA)
			return WordUtils.capitalizeFully(Messages.get("lelang.ba_pk"));
		else if(brc_jenis_ba == Tahap.UPLOAD_BA_PENJELASAN && getLelang_seleksi().getMetode().kualifikasi.isPasca())
			return WordUtils.capitalizeFully(Messages.get("lelang.ba_pp"));
		else if(brc_jenis_ba == Tahap.UPLOAD_BA_PENJELASAN && !getLelang_seleksi().getMetode().kualifikasi.isPasca())
			return WordUtils.capitalizeFully(Messages.get("lelang.ba_ppp"));
        return "";
    }

    /**
     * flag Berita Acara Evaluasi Penawaran Satu file
     * @return
     */
    @Transient
    public boolean isBAPenawaran() {
        return brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_PENAWARAN;
    }

    /**
     * flag Berita Acara Hasil Pelelangan
     * @return
     */
    @Transient
    public boolean isBAHasil() {
        return brc_jenis_ba == Tahap.UPLOAD_BA_HASIL_LELANG;
    }

    /**
     * flag Berita Acara Penawaran file I (administrasi dan teknis)
     * @return
     */
    @Transient
    public boolean isBAPenawaranTeknis() {
        return brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS;
    }

    /**
     * flag is Berita Acara Penawaran file II (Harga)
     * @return
     */
    @Transient
    public boolean isBAPenawaranHarga() {
        return brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_HARGA;
    }

	@Transient
	public boolean isBAHasilNegosiasi() {
		return brc_jenis_ba == Tahap.PENGUMUMAN_PEMENANG_AKHIR;
	}
	
	@Transient
	public boolean isBAPenjelasanPra() {
		return brc_jenis_ba == Tahap.UPLOAD_BA_PENJELASAN_PRA;
	}
	
	@Transient
	public boolean isBAPenjelasan() {
		return brc_jenis_ba == Tahap.UPLOAD_BA_PENJELASAN;
	}

	/**
	 * download URL untuk setiap dokumen berita acara
	 * @param blob
	 * @return
	 */
	@Transient
	public String getDownloadUrl(BlobTable blob) {
		return blob.getDownloadUrl(BeritaAcaraDownloadHandler.class);
	}	
	
	public static String getDownloadInfo(Kategori kategori)
	{
		String info = Messages.get("berita.berita_acara_hasil_pemilihan_didownload")+" ";
		if(kategori.isKonsultansi() || kategori.isJkKonstruksi() || kategori.isKonsultansiPerorangan())
			info += Messages.get("berita.tahapan_penandatanganan_kontrak")+"";
		else
			info += " "+Messages.get("lelang.tpp");
		return info;	
	}
	
	public static UploadInfo simpan(Long lelangId, Tahap jenis, File file) throws Exception
	{
		Berita_acara bA = find("lls_id=? and brc_jenis_ba=?", lelangId,jenis).first();
		if(bA == null) {
			bA = new Berita_acara();
			bA.lls_id = lelangId;
			bA.brc_jenis_ba = jenis;	
		}
		BlobTable blob;
		if(file != null) {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, bA.brc_id_attachment);
		} else { 
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file);
		}
		bA.brc_id_attachment = blob.blb_id_content;
		bA.save();	
		// untuk keperluan response page yang memakai ajax
		return UploadInfo.findBy(blob);
	}

	public static Berita_acara findBy(Long lelangId, Tahap jenis) {		
		return find("lls_id=? and brc_jenis_ba=?", lelangId,jenis).first();
	}   
    
    @Transient
    public String getNameFromBlobTable(BlobTable d){
		String name = "no name";
		try {						
			String[] ls = BaseModel.decrypt(d.blb_path).split("\\n");			
			name = ls[2];
		} catch (Exception ignored) {}
		return name;
	}

	public String getHari(){

		return new SimpleDateFormat("EEEE", new java.util.Locale("id")).format(brt_tgl_evaluasi);
	}
	// content berita acara dalam format html
	public static InputStream cetak(Tahap jenis, Long id, Berita_acara berita_acara) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Template template;
		Map<String, Object> params = new HashMap<>();
		params.put("pnt_nama", lelang.getPaket().getPanitia().pnt_nama);
		if (jenis == Tahap.PENGUMUMAN_PEMENANG_AKHIR) {
			template = TEMPLATE_BA_NEGO;
			params.put("paket", lelang.getPaket());
			params.put("mtd_pemilihan", lelang.getPemilihan());
			params.put("lelang", lelang);
			params.put("berita_acara", berita_acara);
			params.put("instansi", Instansi.findNamaInstansiPaket(lelang.pkt_id));
			Evaluasi akhir = Evaluasi.findPenetapanPemenang(lelang.lls_id);
			params.put("akhir", akhir);
			List<NilaiEvaluasiView> pesertaHargaSorted = new ArrayList<>();
			if (akhir != null) {
				pesertaHargaSorted = akhir.findAllNilaiEvaluasiWithUrutHarga();
			}
			params.put("pesertaHargaSorted", pesertaHargaSorted);
		} else if (jenis == Tahap.UPLOAD_BA_PENJELASAN_PRA || jenis == Tahap.UPLOAD_BA_PENJELASAN) {
			template = TEMPLATE_BA_PENJELASAN;
			params.put("lelang", lelang);
			params.put("berita_acara", berita_acara);
			params.put("instansi", Instansi.findNamaInstansiPaket(lelang.pkt_id));
			Tahap tahap = (jenis == Tahap.UPLOAD_BA_PENJELASAN_PRA) ? Tahap.PENJELASAN_PRA : Tahap.PENJELASAN;
			Diskusi_lelang pembukaan = Diskusi_lelang.findPembukaan(lelang.lls_id, tahap);
			List<Diskusi_lelang> list = Diskusi_lelang.getDiskusiLelangTopik(lelang.lls_id, tahap);
			TahapStarted tahapStarted = lelang.getTahapStarted();
			params.put("hide", !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca()));
			params.put("pembukaan", pembukaan);
			params.put("list", list);
		} else {
			template = TEMPLATE_BA;
			params.put("lelang", lelang);
			params.put("jumlahPeserta", lelang.getJumlahPeserta());
			params.put("jumlahPenawar", lelang.getJumlahPenawar());
			params.put("berita_acara", berita_acara);
			params.put("instansi", Instansi.findNamaInstansiPaket(lelang.pkt_id));
			params.put("sppbj", Sppbj.findByLelang(lelang.lls_id));

			if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
				List<PesertaView> pesertaList = PesertaView.findByLelang(lelang.lls_id);
				params.put("pesertaList", pesertaList);
			}
			if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran() || berita_acara.isBAPenawaranTeknis()) {
				List<NilaiEvaluasiView> pesertaAdm = new ArrayList<>();
				Evaluasi administrasi = Evaluasi.findAdministrasi(lelang.lls_id);
				if (administrasi != null)
					pesertaAdm = administrasi.findAllNilaiEvaluasi();
				params.put("pesertaAdm", pesertaAdm);

				List<NilaiEvaluasiView> pesertaTeknis = new ArrayList<>();
				Evaluasi teknis = Evaluasi.findTeknis(lelang.lls_id);
				if (teknis != null)
					pesertaTeknis = teknis.findAllNilaiEvaluasi();
				params.put("pesertaTeknis", pesertaTeknis);
			}
			if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran() || berita_acara.isBAPenawaranHarga()) {
				List<NilaiEvaluasiView> pesertaHarga = new ArrayList<>();
				List<NilaiEvaluasiView> pesertaHargaSorted = new ArrayList<>();
				Evaluasi harga = Evaluasi.findHarga(lelang.lls_id);
				if (harga != null) {
					pesertaHarga = harga.findAllNilaiEvaluasi();
					pesertaHargaSorted = harga.findAllNilaiEvaluasiWithUrutHarga();
				}
				params.put("pesertaHarga", pesertaHarga);
				params.put("pesertaHargaSorted", pesertaHargaSorted);

				if (berita_acara.isBAHasil()) {
					params.put("hasil_lelang", true);
				} else
					params.put("hasil_lelang", false);
			}

			if (berita_acara.isBAHasil()) {
				List<NilaiEvaluasiView> pesertaKualifikasi = new ArrayList<>();
				Evaluasi kualifikasi = Evaluasi.findKualifikasi(lelang.lls_id);
				if (kualifikasi != null)
					pesertaKualifikasi = kualifikasi.findAllNilaiEvaluasi();
				params.put("pesertaKualifikasi", pesertaKualifikasi);

				List<NilaiEvaluasiView> pesertaPembuktian = new ArrayList<>();
				Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
				if (pembuktian != null)
					pesertaPembuktian = pembuktian.findAllNilaiEvaluasi();
				params.put("pesertaPembuktian", pesertaPembuktian);
				if(lelang.isPrakualifikasi()) {
					template = TEMPLATE_BAHP_PRAKUALIFIKASI;
					params.put("jumlahShortlist", lelang.getJumlahShortlist());
				}
			}
		}
		return HtmlUtil.generatePDF(template, params);
	}

	public String getNamaPanitia() {
		getLelang_seleksi();
		if(this.lelang_seleksi == null) {
			return null;
		}
		return this.lelang_seleksi.getNamaPanitia();
	}
	
	/**
	 * Dapatkan dokumen berita acara, jika tidak ada maka akan dibuat baru,
	 * Gunakan saat proses submit terkait dokumen berita acara
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static Berita_acara findNCreateBy(Long lelangId, Tahap jenis) {
		Berita_acara berita_acara = find("lls_id=? and brc_jenis_ba=?", lelangId, jenis).first();
		if(berita_acara == null) {
			berita_acara = new Berita_acara();
			berita_acara.lls_id = lelangId;
			berita_acara.brc_jenis_ba = jenis;
			berita_acara.save();
		}
		return berita_acara;
	}
	
	public UploadInfo simpan(File dokumen) {
		BlobTable blob = null;
		try {
			List<BlobTable> blobList = getDokumen();
			if (blobList != null)
				blobList.forEach(b -> b.delete());
			if (brc_id_attachment != null && getDokumen().isEmpty()) {
				brc_id_attachment = null;
			}
			if(dokumen != null)
				blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, dokumen, brc_id_attachment);
			if(blob != null)
				brc_id_attachment = blob.blb_id_content;
			save();
		} catch (Exception e){
			Logger.error(e, Messages.get("lelang.simpan_doc_ba_ggl")+" : %s", e.getMessage());
		}
		return UploadInfo.findBy(blob);
	}
}