package models.lelang;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;


@Table(name="NILAI_NERACA_PESERTA")
public class Nilai_neraca_peserta extends BaseModel {
	
	@Id
	public Long nrp_id;
	@Id
	public Long psr_id;

	@Id
	public String nrc_id_item;
	

	public Double nrp_neraca;

}
