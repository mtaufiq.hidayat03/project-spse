package models.lelang;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

/**
 * Created by Lambang on 6/14/2016.
 */

@Table(name="update_lelang_history")
public class UpdateLelangHistory extends BaseModel{

    @Id(sequence="seq_update_lelang_history", function="nextsequence")
    private Long ulh_id;

    private Long lls_id;

    private String ulh_before;

    private String ulh_after;

    public Long getUlh_id() {
        return ulh_id;
    }

    public void setUlh_id(Long ulh_id) {
        this.ulh_id = ulh_id;
    }

    public Long getLls_id() {
        return lls_id;
    }

    public void setLls_id(Long lls_id) {
        this.lls_id = lls_id;
    }

    public String getUlh_before() {
        return ulh_before;
    }

    public void setUlh_before(String ulh_before) {
        this.ulh_before = ulh_before;
    }

    public String getUlh_after() {
        return ulh_after;
    }

    public void setUlh_after(String ulh_after) {
        this.ulh_after = ulh_after;
    }

    public static UpdateLelangHistory getByLelang(Long lelangId){
        return Query.find("SELECT DISTINCT ON (lls_id)* FROM update_lelang_history WHERE lls_id = ? " +
                "ORDER BY lls_id, auditupdate DESC",
                UpdateLelangHistory.class, lelangId).first();
    }
}
