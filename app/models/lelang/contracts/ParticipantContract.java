package models.lelang.contracts;

import models.lelang.Dok_penawaran;

import java.util.List;

/**
 * @author HanusaCloud on 3/16/2018
 */
public interface ParticipantContract {

    Long getParticipantId();

    void setDokPenawaranHarga(Dok_penawaran model);
    void setDokPenawaranTekis(Dok_penawaran model);
    void setDokPenawarans(List<Dok_penawaran> list);

    default void withDokPenawarans() {
        if (getParticipantId() != null) {
            setDokPenawarans(Dok_penawaran.findPenawaranPesertaList(getParticipantId()));
        }
    }

    default void withDokPenawaranHarga() {
        if (getParticipantId() != null) {
            setDokPenawaranHarga(Dok_penawaran.findPenawaranPeserta(getParticipantId(),
                    Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA));
        }
    }

    default void withDokPenawaranTeknis() {
        if (getParticipantId() != null) {
            setDokPenawaranTekis(Dok_penawaran.findPenawaranPeserta(getParticipantId(),
                    Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI));
        }
    }

}
