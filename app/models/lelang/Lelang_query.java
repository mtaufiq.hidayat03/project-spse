package models.lelang;

import models.common.Instansi;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

public class Lelang_query {

    public static List<Instansi> findInstansi() {
        return Instansi.find("id in (SELECT distinct s.instansi_id FROM satuan_kerja s, paket_satker ps, paket p WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id AND pkt_status = 1)").fetch();
    }

    public static List<Integer> listTahunAnggaranAktif() {
        return Query.find("SELECT DISTINCT ang_tahun FROM paket p, paket_anggaran pa, anggaran a WHERE p.pkt_id=pa.pkt_id AND pa.ang_id=a.ang_id AND pkt_status = 1 ORDER BY ang_tahun", Integer.class).fetch();
    }
}
