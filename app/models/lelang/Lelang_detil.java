package models.lelang;


import controllers.BasicCtr;
import models.agency.Anggaran;
import models.agency.Paket;
import models.agency.Paket_lokasi;
import models.agency.Syarat_paket;
import models.common.*;
import models.jcommon.util.CommonUtil;
import models.lelang.Dok_lelang.JenisDokLelang;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.utils.HTML;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Lelang_detil implements StatusTender {
	
	public Long lls_id;
    public Integer mtd_id;
	public Integer lls_versi_lelang;
	public Integer lls_penetapan_pemenang;
	public StatusLelang lls_status;
	public Long pkt_id;
	public Long rup_id;
	public Integer pkt_flag;
	public Integer kgr_id;
	public String pkt_nama;
	public Double pkt_pagu;
	public Double pkt_hps;
	public String agc_nama;
    public String nama; // nama instansi
	public String stk_nama;	
	public Integer mtd_pemilihan;
	public Integer mtd_evaluasi;
	public Integer psr_total;
	public String lls_keterangan;
	public String lls_ditutup_karena;
	public String lls_diulang_karena;
	public Integer lls_kontrak_pembayaran;
	public Integer lls_kontrak_tahun;
	public Integer lls_kontrak_sbd;
	public Double lls_bobot_teknis;
	public Double lls_bobot_biaya;
	public String kls_id;
	public Date lls_dibuat_tanggal;
	public String tahaps; // tahap sekarang
    public String nama_instansi;
    public String nama_satker;
    public Integer lls_penawaran_ulang;
	public Integer lls_evaluasi_ulang;

	public Paket getPaket() {
		return Paket.findById(pkt_id);
	}

    public Metode getMetode() {
        return Metode.findById(mtd_id);
    }

    public Kategori getKategori() {
        return Kategori.findById(kgr_id);
    }

    public MetodePemilihan getPemilihan() {
        return MetodePemilihan.findById(mtd_pemilihan);
    }

    public MetodeEvaluasi getEvaluasi() {
        return MetodeEvaluasi.findById(mtd_evaluasi);
    }

    public JenisKontrak getKontrak_pembayaran() {
        return JenisKontrak.findById(lls_kontrak_pembayaran);
    }

    public JenisKontrak getKontrak_tahun() {
        return JenisKontrak.findById(lls_kontrak_tahun);
    }

    public JenisKontrak getKontrak_sbd() {
        return JenisKontrak.findById(lls_kontrak_sbd);
    }

	public boolean isPenawaranUlang()
	{
		if(lls_penawaran_ulang == null)
			return false;
		return lls_penawaran_ulang == 1;
	}

	boolean isPenawaranEvaluasiUlang() {
		return isPenawaranUlang() && isEvaluasiUlang();
	}

	public boolean isEvaluasiUlang() {
		if(lls_evaluasi_ulang == null)
			return false;
		return lls_evaluasi_ulang == 1;
	}

    public Kualifikasi getKualifikasi() {
    	if(kls_id == null)
    		return null;
        return Kualifikasi.findById(kls_id);
    }

	public boolean isLelangUlang()
	{
		if(lls_versi_lelang == null)
			return false;
		return lls_versi_lelang > 1;
	}	
	
	public boolean isItemized()
	{
		return lls_penetapan_pemenang != null && lls_penetapan_pemenang == 1;
	}

    public boolean isKonsultansi() {
        Kategori kategori = getKategori();
        return kategori != null && (kategori.isConsultant());
    }
    
    public boolean isJkKonstruksi() {
        Kategori kategori = getKategori();
        return kategori != null && (kategori.isJkKonstruksi());
    }

    public String getNamaInstansi() {
        if(CommonUtil.isEmpty(agc_nama))
            return nama;
        return agc_nama;
    }

    public String getAnggaran() {
        StringBuilder anggaran = new StringBuilder();
        //List<String> anggaranlist = Anggaran.getSumberAnggaranPaket(pkt_id);
        List<Anggaran> anggaranlist = Anggaran.findByPaket(pkt_id);
        if(CommonUtil.isEmpty(anggaranlist))
            return anggaran.toString();
//        for (String sumber_anggaran : anggaranlist) {
//            anggaran.append("<div>" + sumber_anggaran + "</div>");
//        }
        anggaran.append("<table class=\"table table-condensed table-bordered\">");
        anggaran.append("<thead><tr><th>"+ Messages.get("lelang.tahun_caps") +"</th><th>"+Messages.get("lelang.sumber_dana")+"</th><th>"+Messages.get("lelang.nilai")+"</th></tr></thead><tbody>");
        for (Anggaran sumber_anggaran : anggaranlist) {
            anggaran.append("<tr><td>").append(sumber_anggaran.ang_tahun).append("</td>").append("<td>").append(sumber_anggaran.sbd_id.name()).append("</td>").append("<td>").append(sumber_anggaran.getNilai()).append("</td></tr>");
        }
        anggaran.append("</tbody></table>");
        return anggaran.toString();
    }

    
    public String getLokasiPaket() {
        List<Paket_lokasi> listLokasi = Paket_lokasi.find("pkt_id=?", pkt_id).fetch();
        StringBuilder lokasipaket = new StringBuilder();
        if(CommonUtil.isEmpty(listLokasi))
            return lokasipaket.toString();        
        boolean first = true;
        for (Paket_lokasi lokasi : listLokasi) {
            if (!CommonUtil.isEmpty(lokasi.pkt_lokasi)) {
            	if(!first)
            		lokasipaket.append(',');
            	lokasipaket.append(HTML.htmlEscape(lokasi.pkt_lokasi ));
            	if(lokasi.getKabupaten() != null)
            		lokasipaket.append(HTML.htmlEscape(" - " + lokasi.getKabupaten().kbp_nama));
            	first = false;
            }           
        }
        return lokasipaket.toString();
    }
    
    /**
     * handle tampilan syarat kualifikasi
     * @return
     */
    public String getSyarat() {
    		Dok_lelang dok_lelang = null;
		StringBuilder content = new StringBuilder();
		if(getPemilihan().isLelangExpress()) {
			String urlsikap_showkualifikasi = BasicCtr.SIKAP_URL+"/shortlist/showQualification?q="+lls_id;
			content.append("<a href=\"").append(urlsikap_showkualifikasi).append("\" target=\"_blank\">"+Messages.get("lelang.lsyk")+"</a>");
		} else {
			if(pkt_flag != null && pkt_flag.intValue() > 1) { // paket dibuat dengan versi 4
				JenisDokLelang jenis = JenisDokLelang.DOKUMEN_LELANG;
				if(getMetode().kualifikasi.isPra())
					jenis = JenisDokLelang.DOKUMEN_LELANG_PRA;
				dok_lelang = Dok_lelang.findBy(lls_id, jenis);
				List<Checklist> ijinList = dok_lelang.getSyaratIjinUsaha();
				List<Checklist> syaratList = dok_lelang.getSyaratKualifikasi();
				List<Checklist> ijinBaruList = dok_lelang.getSyaratIjinUsahaBaru();
				List<Checklist> syaratAdminList = dok_lelang.getSyaratKualifikasiAdministrasi();
				List<Checklist> syaratTeknisList = dok_lelang.getSyaratKualifikasiTeknis();
				List<Checklist> syaratKeuanganList =  dok_lelang.getSyaratKualifikasiKeuangan();
				if(!CollectionUtils.isEmpty(ijinList) || !CollectionUtils.isEmpty(syaratList)) {
					content.append("<table class=\"table table-sm\">");
					if(!CollectionUtils.isEmpty(ijinList)){
						content.append("<tr><th>"+Messages.get("lelang.izin_usaha")+"</th></tr>");
						content.append("<tr><td><table class=\"table table-condensed\">").append("<tr><td>"+Messages.get("lelang.jenis_izin")+"</td><td>"+Messages.get("lelang.klasifikasi")+"</td></tr>");
						for(Checklist ijin : ijinList) {
							content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
						}
						content.append("</table></td></tr>");
					}
					for(Checklist syarat:syaratList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if(syarat.getJsonName() != null) {
							content.append("<b>").append(cm.ckm_nama).append("</b>");
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("<b>").append(syarat.chk_nama).append("</b>");					
						} else {
							content.append("<b>").append(cm.ckm_nama).append("</b>");
							if(!CommonUtil.isEmpty(syarat.chk_nama))
								content.append("<p>").append(syarat.chk_nama).append("</p>");
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
				if (!CollectionUtils.isEmpty(ijinBaruList) || !CollectionUtils.isEmpty(syaratAdminList)) {
					content.append("<h5>"+Messages.get("lelang.pka")+"</h5>");
					content.append("<table class=\"table table-condensed\">");
					if(!CollectionUtils.isEmpty(ijinBaruList)){
						content.append("<tr><th>"+Messages.get("lelang.izin_usaha")+"</th></tr>");
						content.append("<tr><td><table class=\"table table-condensed\">").append("<tr><td>"+Messages.get("lelang.jenis_izin")+"</td><td>"+Messages.get("lelang.bu")+""+Messages.get("lelang.sbuksk")+"</td></tr>");
						for(Checklist ijin : ijinBaruList) {
							content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
						}
						content.append("</table></td></tr>");
						content.append("</tbody>");
					}
					for(Checklist syarat:syaratAdminList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if(syarat.getJsonName() != null) {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("Syarat Kualifikasi Administrasi/Legalitas Lain<br/>");
							String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(syarat.chk_nama);
						} else {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);														
							if(!CommonUtil.isEmpty(syarat.chk_nama)) {
								String[] s = syarat.chk_nama.split("[\\r\\n]+");
								content.append("<i>");
								if (s != null) {
									for (String newline: s) {
										content.append(newline);
										content.append("<br/>");
									}
								} else 
									content.append(syarat.chk_nama);
								content.append("</i>");
							}
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
				if (!CollectionUtils.isEmpty(syaratTeknisList)) {
					content.append("<h5>"+Messages.get("lelang.pkt")+"</h5>");
					content.append("<table class=\"table table-condensed\">");
					for(Checklist syarat:syaratTeknisList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if (cm.isInputNumber()) {
							int i = 0;
							for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
								if (newline.split("number") == null || newline.split("number").length < 2) {
									content.append(newline);
								} else {
									int j = 0;
									for (String number: newline.split("number")) {
										content.append(number);
										for(Map<String, String> val : syarat.getJsonName()) {
											for(String key : table.key) {
												if (key.equals("number"+i) && j == 0) 
													content.append(val.get(key));
											}
										}
										j++;
									}
								}
								content.append("<br/>");
								i++;
							}
						} else if (cm.isInputTextMulti()) {
							for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
								int j = 1;
								for (String text: newline.split("text")) {
									content.append(text);
									for(Map<String, String> val : syarat.getJsonName()) {
										for(String key : table.key) {
											if (key.equals("text"+j)) 
												content.append(val.get(key));
										}
									}
									j++;
								}
								content.append("<br/>");
							}
						} else if (syarat.getJsonName() != null) {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("Syarat Kualifikasi Teknis Lain<br/>");
							String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(syarat.chk_nama);
						} else {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							if(!CommonUtil.isEmpty(syarat.chk_nama)) {
								String[] s = syarat.chk_nama.split("[\\r\\n]+");
								content.append("<i>");
								if (s != null) {
									for (String newline: s) {
										content.append(newline);
										content.append("<br/>");
									}
								} else 
									content.append(syarat.chk_nama);
								content.append("</i>");
							}
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
				if (!CollectionUtils.isEmpty(syaratKeuanganList)) {
					content.append("<h5>"+Messages.get("lelang.pkku")+"</h5>");
					content.append("<table class=\"table table-condensed\">");
					for(Checklist syarat:syaratKeuanganList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if(syarat.getJsonName() != null) {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("Syarat Kualifikasi Kemampuan Keuangan Lain<br/>");
							String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(syarat.chk_nama);
						} else {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							if(!CommonUtil.isEmpty(syarat.chk_nama)) {
								String[] s = syarat.chk_nama.split("[\\r\\n]+");
								content.append("<i>");
								if (s != null) {
									for (String newline: s) {
										content.append(newline);
										content.append("<br/>");
									}
								} else 
									content.append(syarat.chk_nama);
								content.append("</i>");
							}
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
			}
			else { // paket dibuat dengan versi 3
				dok_lelang = Dok_lelang.findBy(lls_id, JenisDokLelang.DOKUMEN_PRAKUALIFIKASI);
				if(dok_lelang == null)
					return "";
				content.append("<table class=\"table table-sm\">");	
				List<Checklist> syaratList = dok_lelang.getSyaratIjinUsaha();
				syaratList.addAll(dok_lelang.getSyaratKualifikasi());
				String namaSyarat = null;
				for (Checklist checklist : syaratList) {
					Checklist_master cm = Checklist_master.findById(checklist.ckm_id);
					if(cm != null)
						namaSyarat = cm.ckm_nama;
					content.append("<tr valign=\"top\">").append("<td>*</td><td>").append(namaSyarat);
					if(!CommonUtil.isEmpty(namaSyarat))
						content.append("<br />");	
					if(cm.isIjinUsaha())
					{
						content.append("<table class=\"table table-condensed\">");
						content.append("<tr><th>"+Messages.get("lelang.izin_usaha")+"</th><th>Klasifikasi</th></tr>");
						List<Syarat_paket> list = Syarat_paket.findByPaket(pkt_id);
						for (Syarat_paket syarat : list) {
							if(CommonUtil.isEmpty(syarat.srt_nama))
								continue;
							content.append("<tr><td>").append(syarat.srt_nama).append("</td>");
							content.append("<td>").append(!CommonUtil.isEmpty(syarat.srt_klasifikasi)? syarat.srt_klasifikasi:"").append("</td></tr>");
						}
						content.append("</table>");
					}
					else
					{
						content.append(checklist.chk_nama);
					}
					content.append("</td></tr>");
				}		
				content.append("</table>");
			}			
		}
		return content.toString();
    }
    
	/**
	 * cek lelang v3 atau tidak
	 * @return
	 */
	public boolean isLelangV3() {
		return pkt_flag != null && pkt_flag < 2;
	}

    public boolean isLelangV43() {
	    return pkt_flag != null && pkt_flag ==  3;
    }
	
	public String getTahapNow() {
		return Tahap.tahapInfo(tahaps, true, isLelangV3(), getPemilihan().isLelangExpress());
	}
	

    public static Lelang_detil findById(Long lelangId) {
        String sql = String.join(" ", "SELECT  l.lls_id ,l.lls_penetapan_pemenang, l.lls_versi_lelang, l.lls_status, p.pkt_id, p.rup_id, p.pkt_flag, p.pkt_nama,",
				"p.pkt_pagu, p.pkt_hps, ag.agc_nama, s.stk_nama, p.kgr_id , p.kls_id,  l.mtd_pemilihan, mtd_id, mtd_evaluasi, lls_keterangan,lls_ditutup_karena, ",
				"lls_kontrak_pembayaran, lls_dibuat_tanggal, l.lls_penawaran_ulang, lls_evaluasi_ulang,",
                "(select count(lls_id) from peserta where lls_id=l.lls_id) as psr_total, l.lls_bobot_teknis, l.lls_bobot_biaya, i.nama, tahap_now(l.lls_id, ?) AS tahaps, lls_diulang_karena, ",
                "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja sk, paket_satker ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id)) AS nama_instansi,",
                "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja sk, paket_satker ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id) AS nama_satker",
                " FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id ",
                "LEFT JOIN agency ag ON s.agc_id=ag.agc_id LEFT JOIN instansi i ON s.instansi_id=i.id WHERE l.lls_id=?");
        return Query.find(sql, Lelang_detil.class,  BasicCtr.newDate(), lelangId).first();
    }

    @Transient
    public boolean hasPemenangTerverifikasi() {
        return getPemilihan().isLelangExpress() && Peserta.count("lls_id=? and is_pemenang_verif = 1", lls_id) > 0;
    }

    @Override
    public boolean isDitutup() {
        return lls_status.isDitutup();
    }

    @Override
    public boolean isUbahLelang() {
        return UpdateLelangHistory.count("lls_id=?", lls_id) > 0;
    }
}
