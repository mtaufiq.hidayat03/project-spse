package models.lelang;

import controllers.BasicCtr;
import ext.DatetimeBinder;
import ext.FormatUtils;
import models.common.Active_user;
import models.common.MetodePemilihan;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Table(name = "jadwal_sanggah_banding")
public class JadwalSanggahBanding extends BaseModel {
	
	@Id(sequence = "seq_jdwl_sgh", function = "nextsequence")
    public Long jdwl_sgh_id;

    @Enumerated(EnumType.ORDINAL)
    public enum StatusSanggahBanding  {
        EDITABLE, NON_EDITABLE, SELESAI;

        public boolean isEditable() {
            return this == EDITABLE;
        }

        public boolean isNonEditable() {
            return this == NON_EDITABLE;
        }

        public boolean isSelesai() {
            return this == SELESAI;
        }
    }

    @Required
    public Long lls_id;

    @As(binder = DatetimeBinder.class)
    public Date sgh_waktu_mulai;

    @As(binder = DatetimeBinder.class)
    public Date sgh_waktu_selesai;

    public StatusSanggahBanding sgh_status;

    public Integer sgh_versi;
    
    public String sgh_alasan;
       
    public String getSgh_alasan() {
		return sgh_alasan;
	}

	public void setSgh_alasan(String sgh_alasan) {
		this.sgh_alasan = sgh_alasan;
	}

	public static JadwalSanggahBanding create(Lelang_seleksi lelang) {
    	JadwalSanggahBanding sgh_banding = find("lls_id=?", lelang.lls_id).first();
        try {
            if(sgh_banding == null) {
            	sgh_banding = new JadwalSanggahBanding();
            	sgh_banding.lls_id = lelang.lls_id;
            	sgh_banding.sgh_versi = 1;
            }          
            sgh_banding.sgh_status = StatusSanggahBanding.NON_EDITABLE;
            sgh_banding.save();
        }catch (Exception e){
            Logger.error(e, "Gagal simpan Jadwal Sanggah Banding Kode Tender %s", lelang.lls_id);
        }
        return sgh_banding;
    }

    public static JadwalSanggahBanding findByLelang(Long id) {
        return find("lls_id=?", id).first();
    }

    public boolean isSelesai() {
        return sgh_waktu_mulai != null && sgh_waktu_selesai.before(BasicCtr.newDate());
    }

    public boolean isSedangProses() {
        return (sgh_waktu_selesai != null && sgh_waktu_selesai.after(BasicCtr.newDate())) &&
                (sgh_waktu_mulai  != null && sgh_waktu_mulai.before(BasicCtr.newDate()));
    }

    public List<Peserta> getPeserta(){
      List<Sanggah_banding> sanggah = Sanggah_banding.findBy(lls_id);
      return sanggah.stream().
              map(s->(Peserta) Peserta.find("lls_id=? AND psr_id in (select psr_id from sanggah_banding where sgh_id = ?)", lls_id,s.sgh_id).first())
              .collect(Collectors.toList());
    }

    // dapatkan informasi waktu reverse auction
    public String getWaktu() {
        return FormatUtils.formatDateTimeInd(sgh_waktu_mulai) + " s.d. " + FormatUtils.formatDateTimeInd(sgh_waktu_selesai);
    }

    public String sisaWaktu() {
        String result = "";
        Date date = controllers.BasicCtr.newDate();
        if (sgh_waktu_selesai != null && sgh_waktu_selesai.after(date)) {
            DateTime now = new DateTime(date.getTime());
            DateTime jakhir = new DateTime(sgh_waktu_selesai.getTime());
            Period period = new Period(now, jakhir);
            if (period.getDays() > 0) {
                result += period.getDays() + " hari";
            }
            if (period.getHours() > 0) {
                if (!StringUtils.isEmpty(result))
                    result += " / ";
                result += period.getHours() + " jam";
            }
            if (!StringUtils.isEmpty(result))
                result += " / ";
            result += period.getMinutes() + " menit";
            if (!StringUtils.isEmpty(result))
                result += " / ";
            result += period.getSeconds() + " detik";
        } else
            return "Waktu Pengiriman Habis";
        return result;
    }

    public boolean isJadwalNotNotNull() {
        return sgh_waktu_mulai != null && sgh_waktu_selesai != null;
    }
    
    public boolean isEditable() {
        Active_user active_user = Active_user.current();
        if(sgh_status.isSelesai())
            return false;
        else if(active_user.isPanitia()) { // editable == true, berarti pokja bisa edit waktu dan alasan (jika reverse ulang)
            MetodePemilihan metodePemilihan = Lelang_seleksi.findPemilihan(lls_id);
            return !metodePemilihan.isLelangExpress();
        } else if(active_user.isRekanan()) { // editable == true, berarti auction sudah dimulai
            return isSedangProses() && sgh_status.isNonEditable();
        }
        return false;
    }

    public String getStatus() {
        if(sgh_status.isNonEditable() && isSelesai())
            return Messages.get("Masa Sanggah Banding sudah selesai");
        if(sgh_status.isSelesai() && isSelesai())
            return Messages.get("Masa Sanggah Banding sudah selesai");
        if(sgh_status.isNonEditable() && isSedangProses())
            return Messages.get("Masa sanggah banding sedang berjalan");
        if(sgh_status.isEditable() || (sgh_status.isNonEditable() && !isSedangProses()))
            return Messages.get("Masa Sanggah Banding belum dimulai");
        return "";
    }

    // validasi waktu auction oleh pokja
    public void validate(Date currentDate) throws Exception {
        if(sgh_waktu_selesai == null) {
            throw new Exception(Messages.get("lelang.tmbt"));
        }
        else if(sgh_waktu_selesai == null) {
            throw new Exception(Messages.get("lelang.tsbt"));
        }
        else if(sgh_waktu_mulai != null && sgh_waktu_mulai.before(currentDate)) {
            throw new Exception(Messages.get("lelang.tmyamsdl"));
        }
        else if(sgh_waktu_selesai != null && sgh_waktu_selesai.before(currentDate)){
            throw new Exception(Messages.get("lelang.tayamsdl"));
        }
        else if(sgh_waktu_selesai.before(sgh_waktu_mulai)) {
            throw new Exception(Messages.get("lelang.tsmdtm"));
        }
        List<Jadwal> list = Jadwal.findByLelangNTahap(lls_id, Tahap.SANGGAH, Tahap.SANGGAH_PRA);
        if(!CollectionUtils.isEmpty(list)) {
            Jadwal jadwal = list.get(0);
            if(jadwal != null && sgh_waktu_selesai.compareTo(jadwal.dtj_tglakhir) == 0) {
                throw new Exception(Messages.get("lelang.baj_ms"));
            }
        }
    }
}

