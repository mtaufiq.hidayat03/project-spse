package models.lelang;

import java.util.Date;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

/**
 * 
 * history penawaran peserta
 * saat terjadi pemasukan penawaran history penawaran peserta sebelumny perlu disimpan
 * @author Arief Ardiyansah
 *
 */

@Table(name = "HISTORY_PENAWARAN")
public class History_penawaran extends BaseModel {

	@Id
	public Long psr_id;
	@Id
	public Integer versi;

	public Integer is_pemenang;

	public Integer is_pemenang_verif;

	public Integer sudah_verifikasi_sikap;
	
	public Double psr_harga;

	public Double psr_harga_terkoreksi;

	public String psr_uraian;
	
	public String psr_alasan_menang;

	public boolean is_dikirim_pesan = false;

	public boolean sudah_evaluasi_kualifikasi = false;

	public Integer is_pemenang_klarifikasi;
	
	/**
	 * daftar kuantitas dan harga
	 * format : json 
	 * json model models.agency.Rincian_hps
	 */
	public String psr_dkh;
	
	public String psr_identitas; // data identitas , model : rekanan.Rekanan
	
	public String psr_pemilik; // data pemilik , model : rekanan.Pemilik
	
	public String psr_pengurus; // data pengurus , model : rekanan.Pengurus
	
	public static void simpanHistory(Peserta peserta) {
		History_penawaran history_penawaran = new History_penawaran();
		history_penawaran.psr_id = peserta.psr_id;
		Integer versi = Query.find("SELECT max(versi) FROM history_penawaran WHERE psr_id=?", Integer.class, peserta.psr_id).first();
		if(versi == null)
			versi = Integer.valueOf(0);
		history_penawaran.versi = versi + 1;
		history_penawaran.is_pemenang = peserta.is_pemenang;
		history_penawaran.is_pemenang_verif = peserta.is_pemenang_verif;
		history_penawaran.sudah_verifikasi_sikap = peserta.sudah_verifikasi_sikap;
		history_penawaran.psr_harga = peserta.psr_harga;
		history_penawaran.psr_harga_terkoreksi = peserta.psr_harga_terkoreksi;
		history_penawaran.psr_uraian = peserta.psr_uraian;
		history_penawaran.psr_alasan_menang = peserta.psr_alasan_menang;
		history_penawaran.is_dikirim_pesan = peserta.is_dikirim_pesan;
		history_penawaran.sudah_evaluasi_kualifikasi = peserta.sudah_evaluasi_kualifikasi;
		history_penawaran.is_pemenang_klarifikasi = peserta.is_pemenang_klarifikasi;
		history_penawaran.psr_dkh = peserta.psr_dkh;
		history_penawaran.psr_identitas = peserta.psr_identitas;
		history_penawaran.psr_pemilik = peserta.psr_pemilik;
		history_penawaran.psr_pengurus = peserta.psr_pengurus;
		history_penawaran.audittype = peserta.audittype;
		history_penawaran.auditupdate = peserta.auditupdate;
		history_penawaran.audituser = peserta.audituser;		
		history_penawaran.save();
	}
}
