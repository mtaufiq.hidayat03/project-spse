package models.lelang;

import models.common.Status_kepemilikan;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Peralatan;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name="PERALATAN_PESERTA")
public class Peralatan_peserta extends BaseModel {
	
	//relasi Ke Peserta
	@Id
	public Long psr_id;

	@Id
	public Long id_alat_peserta;

	public String alt_jenis;

	public Integer alt_jumlah;

	public String alt_kapasitas;

	public String alt_merktipe;

	public String alt_thpembuatan;

	public Integer alt_kondisi;

	public String alt_lokasi;

	public String alt_kepemilikan;
	
	public Status_kepemilikan skp_id;
	
	@Transient
	public String getKondisi()
    {
    	if(alt_kondisi != null && alt_kondisi.intValue() == 1)
    		return "Baik";
    	else
    		return "Rusak";
    }

	public static Peserta getPeserta(Long pesertaId){
		return Peserta.find("psr_id=?", pesertaId).first();
	}
	
	public static void simpanPeralatanPeserta(List<Peralatan> list, Long pesertaId) throws Exception{
		delete("psr_id=?", pesertaId);
	    if(!CommonUtil.isEmpty(list)) {	   
		    Peralatan_peserta psr_alat = null;
		    for(Peralatan alat: list) {            
		        psr_alat = new Peralatan_peserta();
		        psr_alat.id_alat_peserta = alat.alt_id;
		        psr_alat.psr_id = pesertaId;
		        psr_alat.alt_jenis = alat.alt_jenis;
	            psr_alat.alt_jumlah = alat.alt_jumlah;
	            psr_alat.alt_kapasitas = alat.alt_kapasitas;
	            psr_alat.alt_merktipe = alat.alt_merktipe;
	            psr_alat.alt_thpembuatan = alat.alt_thpembuatan;
	            psr_alat.alt_kondisi = alat.alt_kondisi;
	            psr_alat.alt_lokasi = alat.alt_lokasi;
	            psr_alat.alt_kepemilikan = alat.alt_kepemilikan;
	            psr_alat.skp_id = alat.skp_id;
	            psr_alat.save();	        
		   }
	    }
	} 
	

}
