package models.lelang;

import models.agency.*;
import models.agency.Paket.StatusPaket;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Evaluasi.JenisEvaluasi;
import models.rekanan.Rekanan;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.*;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table(name="LELANG_SELEKSI")
public class Lelang_seleksi extends BaseModel implements StatusTender {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisLelang {
		LELANG_NON_ITEMIZE(Messages.get("lelang.1_pemenang")), LELANG_ITEMIZE(Messages.get("lelang.lebih_dari_1_pemenang")+" <i>(Itemized)</i>");

		public final String label;
		JenisLelang(String label) {
			this.label = label;
		}
	}

	@Enumerated(EnumType.ORDINAL)
	public enum MetodePenawaranHarga {
		NON_AUCTION(Messages.get("lelang.tidak_menggunakan_e_auction")), AUCTION("E-Auction");
		public final String label;
		MetodePenawaranHarga(String label) {
			this.label = label;
		}
	}
	
	@Id(sequence="seq_lelang_seleksi", function="nextsequence")
	public Long lls_id;

	public Double lls_passinggrade;

	public Integer lls_versi_lelang;

	public String lls_keterangan;

	public Date lls_dibuat_tanggal;

	public String lls_diulang_karena;

	public String lls_ditutup_karena;
	
	public String lls_dibuka_karena;

	public Integer lls_terverifikasi;

	public Date lls_tgl_setuju;

	@Required
	public StatusLelang lls_status;

	public Long lls_wf_id;

	@Required
	public Integer mtd_pemilihan;
	
	@Required
	public Integer mtd_evaluasi;
	
	@Required
	public Integer mtd_id;

	@Required
	public JenisLelang lls_penetapan_pemenang;// 0: satu pemenang, 1: lebih dari satu pemenang (itemize)
 
    public Integer lls_kontrak_pembayaran;

    public Integer lls_kontrak_tahun;

    public Integer lls_kontrak_sbd;
    /**
     * kontrak berdasarkan jenis Pekerjaan
     * since 4.0
     */
    public Integer lls_kontrak_pekerjaan;

    public Double lls_bobot_teknis;

    public Double lls_bobot_biaya;

    public boolean is_kualifikasi_tambahan = false;

	// flag kirim pengumuman pemenang prakualifikasi sudah dilakukan atau belom.
	// "true" = sudah dikirim ,  "false" = belum dikirim
	public boolean kirim_pengumuman_pemenang_pra = false;

    // flag kirim pengumuman pemenang sudah dilakukan atau belom.
	// "true" = sudah dikirim ,  "false" = belum dikirim
    public boolean kirim_pengumuman_pemenang = false;

    //relasi ke Paket
	public Long pkt_id;

	@Transient
	private Paket paket;
	@Transient
	private Dok_lelang dokumenLelang;
	@Transient
	private Evaluasi evaluasiKualifikasi;
	@Transient
	private Evaluasi pembuktian;
	@Transient
	private Evaluasi evaluasiAkhir;
	@Transient
	private TahapNow tahapNow;
	@Transient
	private TahapStarted tahapStarted;
	@Transient
	private ReverseAuction reverseAuction;
	@Transient
	private Evaluasi teknis; // evaluasi teknis;
	
	// untuk simpan sesi pelatihan. 0 : default prod tidak boleh diisi yang lain
	public int lls_sesi;

	// untuk menandakan bahwa lelang pernah pemasukan penawaran ulang
	public Integer lls_penawaran_ulang;

	// untuk menandakan bahwa lelang pernah evaluasi ulang
	public Integer lls_evaluasi_ulang;

	// untuk menandakan sudah dilakukan pembukaan penawaran
	public Date lls_tanggal_pembukaan_admteknis;
	public Date lls_tanggal_pembukaan_harga;

	public MetodePenawaranHarga lls_metode_penawaran; // 0 : menggunakan Apendo (default), 1 : menggunakan E-Auction

	
	public Paket getPaket() {
		if(paket == null)
			paket = Paket.findById(pkt_id);
		return paket;
	}

	public Dok_lelang getDokumenLelang() {
		if (dokumenLelang == null && lls_id != null) {
			dokumenLelang = Dok_lelang.findBy(lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
		}
		return dokumenLelang;
	}

	public Evaluasi getEvaluasiKualifikasi(){
		if(evaluasiKualifikasi == null){
			evaluasiKualifikasi = Evaluasi.findKualifikasi(lls_id);
		}
		return evaluasiKualifikasi;
	}

	public Evaluasi getPembuktian(boolean findNCreate){
		if(pembuktian == null){
			if(findNCreate){
				pembuktian = Evaluasi.findNCreatePembuktian(lls_id);
			}else{
				pembuktian = Evaluasi.findPembuktian(lls_id);
			}
		}

		return pembuktian;
	}

	public Evaluasi getEvaluasiAkhir(){
		if(evaluasiAkhir == null){
			evaluasiAkhir = Evaluasi.findPenetapanPemenang(lls_id);
		}
		return evaluasiAkhir;
	}

	public TahapNow getTahapNow(){
		if(tahapNow == null){
			tahapNow = new TahapNow(this);
		}

		return tahapNow;
	}

	public TahapStarted getTahapStarted(){
		if(tahapStarted == null){
			tahapStarted = new TahapStarted(this);
		}

		return tahapStarted;
	}

	public ReverseAuction getReverseAuction(){
		if(reverseAuction == null){
			reverseAuction = ReverseAuction.findByLelang(lls_id);
		}

		return reverseAuction;
	}

	public boolean isPaketKonsolidasi(){
		Paket paket = getPaket();
		if(paket == null){
			return false;
		}

		return paket.isKonsolidasi();
	}
	
	@Transient
    public List<Checklist_gagal> getChecklistKategori() {
        return Checklist_gagal.find("lls_id=? order by ckm_id asc", lls_id).fetch();
    }

	public Panitia getPanitia(){
		return Query.find("SELECT p.* FROM panitia p LEFT JOIN paket pkt ON pkt.pnt_id=p.pnt_id WHERE pkt.pkt_id=?", Panitia.class, pkt_id).first();
	}
	
	public String getNamaPanitia(){
		return Query.find("SELECT pnt_nama FROM panitia p LEFT JOIN paket pkt ON pkt.pnt_id=p.pnt_id WHERE pkt.pkt_id=?", String.class, pkt_id).first();
	}

	public List<Peserta> getPesertaList() {
		return Peserta.findBylelang(lls_id);
	}

	public List<Rekanan> getRekananList() {
		return Rekanan.findAllPesertaLelang(lls_id);
	}

	public List<Peserta> getPenawarList() {
		return Peserta.findPenawarBylelang(lls_id);
	}
    	
	public List<Jadwal> getJadwalList() {
		return Jadwal.findByLelang(lls_id);
	}
	
	public String getNamaPaket() {
		return Query.find("select pkt_nama from paket where pkt_id=?", String.class, pkt_id).first();
	}

	@Transient 
	public Kategori getKategori() {
		return Query.find("select kgr_id from paket where pkt_id=?", Kategori.class, pkt_id).first();
	}
	
	@Transient 
	public boolean isKonsultansi() {
		Kategori kategori = getKategori();
		return kategori.isKonsultansi() || kategori.isKonsultansiPerorangan();
	}

	@Transient 
	public boolean isKonsultansiJKKonstruksi() {
		Kategori kategori = getKategori();
		return kategori.isKonsultansi() || kategori.isKonsultansiPerorangan()||kategori.isJkKonstruksi();
	}
	
	public boolean isKonstruksi() {
		Kategori kategori = getKategori();
		return kategori != null && kategori.isKonstruksi();
	}
	
	@Transient
	public boolean isJkKonstruksi() {
		Kategori kategori = getKategori();
		return kategori != null && kategori.isJkKonstruksi();
	}
	
	@Transient
	public Metode getMetode()
	{
		return Metode.findById(mtd_id);
	}

	@Transient
	public MetodePemilihan getPemilihan()
	{
		return MetodePemilihan.findById(mtd_pemilihan);
	}

	@Transient
	public MetodeEvaluasi getEvaluasi()
	{
		return MetodeEvaluasi.findById(mtd_evaluasi);

	}
	@Transient
	public JenisKontrak getKontrakPembayaran()
	{
		
		return JenisKontrak.findById(lls_kontrak_pembayaran);
	}
	
	@Transient
	public JenisKontrak getKontrakTahun()
	{
		return JenisKontrak.findById(lls_kontrak_tahun);
	}
	
	@Transient
	public JenisKontrak getKontrakSumberDana()
	{
		return JenisKontrak.findById(lls_kontrak_sbd);
	}
	
	@Transient
	public JenisKontrak getKontrakPekerjaan()
	{
		if(lls_kontrak_pekerjaan == null)
			return null;
		return JenisKontrak.findById(lls_kontrak_pekerjaan);
	}

	
	@Transient
	public void setKontrakSumberDana(JenisKontrak kontrak)
	{
		
		this.lls_kontrak_sbd = kontrak.id;
	}
	
	@Transient
	public void setKontrakPekerjaan(JenisKontrak kontrak)
	{
		this.lls_kontrak_pekerjaan = kontrak.id;
	}
	
	@Transient
	public void setKontrakPembayaran(JenisKontrak kontrak)
	{
		this.lls_kontrak_pembayaran = kontrak.id;
	}
	
	@Transient
	public void setKontrakTahun(JenisKontrak kontrak)
	{
		this.lls_kontrak_tahun = kontrak.id;
	}
	
	@Transient
	public boolean isLelangUlang()
	{
		if(lls_versi_lelang == null)
			return false;
		return lls_versi_lelang > 1;
	}

	@Transient
	public boolean isPenawaranUlang()
	{
		if(lls_penawaran_ulang == null)
			return false;
		return lls_penawaran_ulang == 1;
	}

	@Transient
	public boolean isUbahLelang()
	{
		return UpdateLelangHistory.count("lls_id=?", lls_id) > 0;
	}

	/**
	 * cek lelang v3 atau tidak
	 * @return
	 */
	@Transient
	public boolean isLelangV3() {
		Integer flag = Query.find("select pkt_flag from paket where pkt_id=?", Integer.class, pkt_id).first();
		return flag < 2;
	}

	/**
	 * cek lelang v43 atau tidak
	 * @return
	 */
	@Transient
	public boolean isLelangV43() {
		Integer flag = Query.find("select pkt_flag from paket where pkt_id=?", Integer.class, pkt_id).first();
		return flag == 3;
	}
	
	@Transient
	public boolean isPrakualifikasi()
	{
		return getMetode().kualifikasi.isPra();
	}
	
	@Transient
	public boolean isPascakualifikasi()
	{
		return getMetode().kualifikasi.isPasca();
	}
	
	@Transient
	public boolean isSatuFile()
	{
		return getMetode().dokumen.isSatuFile();
	}
	
	@Transient
	public boolean isDuaFile()
	{
		return getMetode().dokumen.isDuaFile();
	}
	
	@Transient
	public boolean isDuaTahap()
	{
		return getMetode().dokumen.isDuaTahap();
	}
	/**
	 * cek jenis lelang itemize atau bukan
	 * @return
	 */
	@Transient
	public boolean isItemized()
	{
		return lls_penetapan_pemenang != null && lls_penetapan_pemenang.equals(JenisLelang.LELANG_ITEMIZE);
	}
	
	/**
	 * cek apakah lelang ini pernah alami evaluasi ulang
	 * @return
	 */
	@Transient 
	public boolean isEvaluasiUlang() {
		if(lls_evaluasi_ulang == null)
			return false;
		return lls_evaluasi_ulang == 1;
	}
	
	/**
	 * jumlah peserta lelang
	 * @return
	 */
	@Transient 
	public int getJumlahPeserta() {
		return (int)Peserta.count("lls_id=?", lls_id);
	}

	/**
	 * jumlah peserta yang telah dievaluasi kualifikasinya
	 * @return
	 */
	@Transient
	public int getJumlahPesertaTelahEvaluasiKualifikasi() {
		return (int)Peserta.count("lls_id=? and sudah_evaluasi_kualifikasi = ?", lls_id, true);
	}

	/**
	 * jumlah peserta yang dikirimi pesan tidak lengkap
	 * @return
	 */
	@Transient
	public int getJumlahPesertaDikirimPesan() {
		return (int)Peserta.count("lls_id=? and is_dikirim_pesan = ?", lls_id, true);
	}

	/**
	 * jumlah penawar kualifikasi
	 * @return
	 */
	@Transient
	public long jumlahPenawarKualifikasi() {
		return Dok_penawaran.count("psr_id in (select psr_id from peserta where lls_id=?) and dok_jenis=?", lls_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
	}
	
	/**
	 * jumlah penawar
	 * @return
	 */
	@Transient 
	public long getJumlahPenawar() {
		if (isExpress()) {
			ReverseAuction auction = ReverseAuction.findByLelang(lls_id);
			if(auction != null) {
				return auction.getJumlahPenawar();
			} else { // tender cepat yg lama tidak memakai reverse auction
				String sql = "select count(psr_id) from (select psr_id, (select MAX(dok_id) from dok_penawaran where psr_id=p.psr_id and dok_jenis=2 and dok_disclaim=1) as harga from peserta p where p.lls_id=?) penawaran_peserta where harga is not null";
				return Query.count(sql, lls_id);
			}
		} else {
			if (getMetode().dokumen.isDuaFile()) {
				String sql = "select count(psr_id) from (select psr_id, " +
						"(select MAX(dok_id) from dok_penawaran where psr_id=p.psr_id and dok_jenis=1 and dok_disclaim=1) as teknis , " +
						"(select MAX(dok_id) from dok_penawaran where psr_id=p.psr_id and dok_jenis=2 and dok_disclaim=1) as harga from peserta p where p.lls_id=?) penawaran_peserta where teknis is not NULL and harga is not null";
				return Query.count(sql, lls_id);
			}
			String jenisDok = "dok_jenis in ('" + JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id + "','"+ JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id + "')";
			return Dok_penawaran.count("psr_id in (select psr_id from peserta where lls_id=?) AND dok_disclaim='1' AND " + jenisDok,lls_id);
		}
	}

	public long getJumlahShortlist() {
		long count = 0L;
		if(isLelangV3()) {
			count = Nilai_evaluasi.countLulus(lls_id, JenisEvaluasi.EVALUASI_KUALIFIKASI);
		} else {
			count = Nilai_evaluasi.countLulus(lls_id, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI);
		}
		return count;
	}
	
	/**
	 * cek hak akses lihat kualifikasi
	 * @return
	 */
	@Transient 
	public boolean isKualifikasiPermission(){
		MetodeKualifikasi mtdKualifikasi = getMetode().kualifikasi;
		Tahap[] tahaplist = null;
		if(mtdKualifikasi.isPasca())
			tahaplist = new Tahap[] {Tahap.PEMBUKAAN_PENAWARAN, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS,
					Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI};
		else
			tahaplist = new Tahap[] {Tahap.PEMASUKAN_DOK_PRA, Tahap.EVALUASI_DOK_PRA};
		if(Aktivitas.isTahapStarted(lls_id, tahaplist)) {	
			long jumlahPenawarKualifikasi = jumlahPenawarKualifikasi();
			long jumlahPenawar = getJumlahPenawar();
			boolean enableDownload = ConfigurationDao.isEnableDownload(lls_id);
			boolean isLelangUlang = isLelangUlang();
			if(mtdKualifikasi.isPra()){
				return true;
			}
			else
				return ((jumlahPenawar >=1 && jumlahPenawarKualifikasi >=1) || isLelangUlang || (getPemilihan().isLelangTerbatas() && jumlahPenawar >=1 && jumlahPenawarKualifikasi >=1)) && enableDownload;
		}
		return false;
	}
	
	/**
	 * cek apakah peserta diijinkan untuk kirim dokumen penawaran
	 * @return
	 */
	@Transient
	public boolean isAllowKirimPenawaran(Peserta peserta, TahapNow tahapAktif, JenisDokPenawaran jenis) {
		if ((tahapAktif.isPemasukanPenawaran() && (isDuaFile() || isSatuFile())) || (tahapAktif.isPemasukanPenawaranAdmTeknis() && jenis.isAdmTeknis()) || (tahapAktif.isPemasukanPenawaranHarga() && jenis.isHarga())) {
			if (isPrakualifikasi()) {
				int lulus = Nilai_evaluasi.countLulus(lls_id, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI);
				boolean isLelangUlang = isLelangUlang();
				boolean isPenawaranUlang = isPenawaranUlang();
				MetodePemilihan pemilihan = getPemilihan();
				if ((pemilihan.isSeleksiUmum() && lulus >= 5) || (lulus >= 3) || isLelangUlang || isPenawaranUlang || (pemilihan.isLelangTerbatas() && lulus >= 1)) {
					if (tahapAktif.isPemasukanPenawaranHarga()) {
						// hanya peserta yang lulus evaluasi admin-teknis yang
						// di ijinkan upload penawaran harga pada lelang 2 tahap
						return Nilai_evaluasi.isLulus(JenisEvaluasi.EVALUASI_TEKNIS, peserta);
					} else {
						return Nilai_evaluasi.isLulus(JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI, peserta);
					}
				}
			} else
				return true;
		}
		return false;
	}
	
	/**
	 * cek penyedia bisa mendownload BA
	 * @return
	 */
	@Transient
	public boolean isAllowDownloadBA() {
		Tahap[] tahap = new Tahap[] {Tahap.PENGUMUMAN_PEMENANG_AKHIR, Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR};
		if(getKategori().isKonsultansi() || getKategori().isJkKonstruksi() || getKategori().isKonsultansiPerorangan()){					
			tahap = new Tahap[] {Tahap.TANDATANGAN_KONTRAK};
		}

		if(getPemilihan().isLelangExpress())
			return Aktivitas.isLelangFinish(lls_id);

		return Aktivitas.isTahapStarted(lls_id, tahap);
	}
	
	public String getInfoJadwal() {
		Long jmlThp = Jadwal.count("lls_id="+lls_id);
		Long tahapKosong = Jadwal.countJadwalkosong(lls_id);
		if (jmlThp == 0)
			return Messages.get("lelang.baj");
		else if (tahapKosong == 0)
			return Messages.get("lelang.sjst");
		else
			return jmlThp + " Tahap, " + tahapKosong+ " "+Messages.get("lelang.tbmj");
	}

	public boolean isJadwalSudahTerisi(){
		Long jmlThp, tahapKosong;
		Integer metodeId = getMetode().id;
		QueryBuilder query = new QueryBuilder("lls_id=?", lls_id);
		if(getPemilihan().isLelangExpress()){
			// untuk lelang cepat aanwijzing diperbolehkan kosong
			Aktivitas akt_penjelasan = Aktivitas.findByMetode(metodeId, Tahap.PENJELASAN);
			if(akt_penjelasan != null)
				query.append("AND akt_id <> ?", akt_penjelasan.akt_id);
		} else {
			Aktivitas akt_penjelasan_pra = Aktivitas.findByMetode(metodeId, Tahap.PENJELASAN_PRA);
			if(akt_penjelasan_pra != null)
				query.append("AND akt_id <> ?", akt_penjelasan_pra.akt_id);
			Aktivitas akt_penyetaraan_teknis = Aktivitas.findByMetode(metodeId, Tahap.PENYETARAAN_TEKNIS);
			if(akt_penyetaraan_teknis != null)
				query.append("AND akt_id <> ?", akt_penyetaraan_teknis.akt_id);
		}
		jmlThp = Jadwal.count(query);
		query.append("AND (dtj_tglawal is null or dtj_tglakhir is null) ");
		tahapKosong = Jadwal.count(query);
		return jmlThp != 0 && tahapKosong == 0;
	}
	
	public String getTahapanSekarang() {
		return Jadwal.getJadwalSekarang(lls_id, true, getPemilihan().isLelangExpress(), isLelangV3());
	}
	

	public String getNamaPegawaiMenyetujui(){
		return Query.find("SELECT p.peg_nama FROM workflow w, Pegawai p WHERE w.audituser=p.peg_namauser AND "
				+ "w.foreign_wf_id IN (SELECT lls_wf_id FROM Lelang_seleksi l WHERE l.lls_id=?)", String.class,lls_id).first();
	}

	public static List<Lelang_seleksi> getLelangBySk(Long skId){
		return Query.find("SELECT l.* FROM paket_skauditor ps LEFT JOIN lelang_seleksi l ON ps.pkt_id = l.pkt_id " +
				"WHERE ps.skid=?", Lelang_seleksi.class, skId).fetch();
	}
	
	public String getNamaPegawaiPembuatan(){
		return Query.find("SELECT peg_nama FROM pegawai WHERE peg_namauser IN (SELECT audituser FROM paket WHERE pkt_id=?)", String.class, pkt_id).first();
	}
	
	public Double getHpsLelang() {
		return Query.find("select pkt_hps from paket where pkt_id=?",Double.class, pkt_id).first();
	}

	public int getJumlahSanggahan() {
		TahapNow tahapAktif = getTahapNow();
		int jumlahSanggahan = 0;
		if(tahapAktif.isSanggah())
			jumlahSanggahan += Sanggahan.countSanggahan(lls_id, Tahap.SANGGAH);
		if(tahapAktif.isSanggahAdm())
			jumlahSanggahan += Sanggahan.countSanggahan(lls_id, Tahap.SANGGAH_ADM_TEKNIS);
		if(tahapAktif.isSanggahPra())
			jumlahSanggahan += Sanggahan.countSanggahan(lls_id, Tahap.SANGGAH_PRA);
		return jumlahSanggahan;
	}

	/**
	 * membuat lelang baru dengan nilai-nilai default
	 * event ini terjadi saat pembuatan paket
	 * @param paket
	 */
	public static Lelang_seleksi buatLelangBaru(Paket paket, MetodePemilihanPenyedia metodePemilihanPenyedia) {
		//long jumlahLelang = Query.count("SELECT count(lls_id) FROM lelang_seleksi WHERE pkt_id=?", paket.pkt_id);
		Lelang_seleksi lls = Lelang_seleksi.find("pkt_id =?", paket.pkt_id).first();
		if (lls == null) { // berarti belum pernah ada lelang yang dibuat
			lls = new Lelang_seleksi();
			lls.pkt_id = paket.pkt_id;
			if(metodePemilihanPenyedia != null)
				lls.mtd_pemilihan = MetodePemilihan.findBy(metodePemilihanPenyedia).id; // default lelang umum
			if(paket.kgr_id.isKonsultansi() || paket.kgr_id.isJkKonstruksi()) {
				lls.mtd_id = Metode.PRA_DUA_FILE_KUALITAS_BIAYA.id;
				lls.mtd_evaluasi = MetodeEvaluasi.KUALITAS_BIAYA.id;
			} else if(paket.kgr_id.isKonsultansiPerorangan()) {
				lls.mtd_id = Metode.PASCA_DUA_FILE_KUALITAS.id;
				lls.mtd_evaluasi = MetodeEvaluasi.KUALITAS.id;
			} else {
				lls.mtd_id = Metode.PASCA_SATU_FILE.id;  //default pasca 1 file			
				lls.mtd_evaluasi = MetodeEvaluasi.GUGUR.id; // default sistem gugur
			}			
			lls.lls_versi_lelang = Integer.valueOf(1);
			lls.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
			lls.lls_terverifikasi = Integer.valueOf(1);
			lls.lls_status = StatusLelang.DRAFT;
			lls.lls_penetapan_pemenang = JenisLelang.LELANG_NON_ITEMIZE; // default non-itemize
			lls.setKontrakPembayaran(JenisKontrak.LUMP_SUM);
			lls.save();
		}
		return lls;
	}
	
	public static Metode getMetodeLelang(Long lelangId) {
		Integer mtd = Query.find("select mtd_id from Lelang_seleksi where lls_id=?",Integer.class, lelangId).first();
		return Metode.findById(mtd);
	}
	
	/**
	 * flow proses tutup lelang
	 * @throws Exception
	 */
	public void tutupLelang() throws Exception
	{
			lls_status = StatusLelang.DITUTUP;
			save();
			MailQueueDao.kirimPengumumanTutupLelang(this);
	}
	
	/**
	 * flow proses buka lelang
	 * @param alasan
	 * @throws Exception
	 */
	public void bukaLelang(String alasan) {
		try {
			lls_status = StatusLelang.AKTIF;
			lls_dibuka_karena = alasan;
			List<Persetujuan> list = Persetujuan.findByLelangPembatalan(lls_id);
			if(!CollectionUtils.isEmpty(list)) { // simpan history persetujuan pembatalan
				for(Persetujuan obj : list) {
					History_persetujuan.simpanHistory(obj);
					obj.delete();
				}
			}
			save();
			MailQueueDao.kirimPengumumanBukaLelang(this, alasan);
		} catch (Exception e) {
			Logger.error(e, Messages.get("lelang.gkpbl"));
		}
	}
	
	/**
	 * Proses flow Tender Ulang
	 * @param lelangId
	 * @return
	 */
	public static Long ulangLelang(Long lelangId){
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		Lelang_seleksi lelangCopy = new Lelang_seleksi();
		lelangCopy.pkt_id = lelang.pkt_id;
		lelangCopy.lls_ditutup_karena = lelang.lls_ditutup_karena;
		lelangCopy.mtd_id = lelang.mtd_id;
		lelangCopy.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
		lelangCopy.lls_terverifikasi = lelang.lls_terverifikasi;
		lelangCopy.lls_versi_lelang = lelang.lls_versi_lelang + 1;
		lelangCopy.lls_status = StatusLelang.DRAFT;
		lelangCopy.mtd_pemilihan = lelang.mtd_pemilihan;
		lelangCopy.mtd_evaluasi = lelang.mtd_evaluasi;
		lelangCopy.lls_penetapan_pemenang = lelang.lls_penetapan_pemenang;

		//update jenis kontrak
		lelangCopy.lls_kontrak_pembayaran = lelang.lls_kontrak_pembayaran;
		lelangCopy.save();

		Paket paket = Paket.findById(lelang.pkt_id);
		paket.pkt_status = StatusPaket.ULANG_LELANG;
		paket.save();

		//Handle Tender Ulang paket yang dibuat di versi sebelum 4.3
		if(!paket.isFlag43()){

			DokPersiapan dokPersiapan = DokPersiapan.findByPaket(paket.pkt_id);

			if(dokPersiapan == null){
				Dok_lelang dl = Dok_lelang.find("lls_id = ?",lelang.lls_id).first();
				Dok_lelang_content dlc = Dok_lelang_content.findBy(dl.dll_id);

				dokPersiapan = new DokPersiapan();
				dokPersiapan.pkt_id = lelang.pkt_id;
				dokPersiapan.dkh = dlc.dkh;
				dokPersiapan.dp_spek = dlc.dll_spek;
				dokPersiapan.sskkContent = dlc.sskk_content;
				dokPersiapan.dp_versi = 1;

				dokPersiapan.save();
			}

			//Karena belum ada aturan lebih lanjut terkait konsolidasi di versi 4.3, untuk sementara ambil salah satu PPK dari anggaran.
			//TODO: Perbaiki jika sudah ada aturan konsolidasi di 4.3.
			Paket_anggaran paketAnggaran = Paket_anggaran.findByPaket(lelang.pkt_id).get(0);

			PaketPpk paketPpk = new PaketPpk(lelang.pkt_id,paketAnggaran.ppk_id);
			paketPpk.save();
		}

		return lelangCopy.lls_id;
	}

	/**
	 * mendapatkan informasi status lelang , tanpa harus ambil seluruh Object lelang_seleksi
	 * @param lelangId
	 * @return
     */
	public static StatusLelang findStatusLelang(Long lelangId) {
		return Query.find("SELECT lls_status FROM lelang_seleksi WHERE lls_id=?", StatusLelang.class, lelangId).first();
	}

	/**
	 * mendapatkan informasi metode pemilihan lelang , tanpa harus ambil seluruh Object lelang_seleksi
	 * @param lelangId
	 * @return
	 */
	public static MetodePemilihan findPemilihan(Long lelangId) {
		int pemilihan = Query.find("SELECT mtd_pemilihan FROM lelang_seleksi WHERE lls_id=?", Integer.class, lelangId).first();
		return MetodePemilihan.findById(pemilihan);
	}

	/**
	 * cek lelang v3 atau tidak
	 * @return lelangId
	 */
	public static boolean isLelangV3(Long lelangId) {
		Integer flag = Query.find("select pkt_flag from paket where pkt_id in (select pkt_id from lelang_seleksi where lls_id=?)", Integer.class, lelangId).first();
		return flag < 2;
	}
	
	@Transient
	public boolean hasPemenangTerverifikasi() {
		return getPemilihan().isLelangExpress() && Peserta.count("lls_id=? and is_pemenang_verif = 1", lls_id) > 0;
	}

	public Persetujuan findPersetujuan(Long pegawaiId, boolean pemenang) {
		if (pemenang) {
			return Persetujuan.find("lls_id=? and peg_id=? and pst_jenis=?", this.lls_id, pegawaiId, Persetujuan.JenisPersetujuan.PEMENANG_LELANG).first();
		} else {
			return Persetujuan.find("lls_id=? and peg_id=? and pst_jenis=?", this.lls_id, pegawaiId, Persetujuan.JenisPersetujuan.PENGUMUMAN_LELANG).first();
		}
	}

	/**
	 * @return boolean
	 */
	@Transient
	public boolean sudahPembukaanAdmTeknis() {
		return lls_tanggal_pembukaan_admteknis != null;
	}

	/**
	 * @return boolean
	 */
	@Transient
	public boolean sudahPembukaanHarga() {
		return lls_tanggal_pembukaan_harga != null || (Peserta.count("lls_id=? AND (psr_harga_terkoreksi IS NOT NULL OR psr_harga IS NOT NULL) ", lls_id) > 0);
	}


	/**
	 * untuk satu token
	 * @return boolean
	 */
	@Transient
	public boolean sudahPembukaanPenawaran() {
		return sudahPembukaanAdmTeknis() || sudahPembukaanHarga();
	}

	@Override
	protected void preDelete() {
		if(lls_versi_lelang == 1){
			Query.update("DELETE FROM paket_lokasi WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_ppk WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_panitia WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_anggaran WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_satker WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM dok_persiapan WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket WHERE pkt_id = ?", pkt_id);
		}
		Query.update("DELETE FROM persetujuan WHERE lls_id = ?", lls_id);
		Query.update("DELETE FROM jadwal WHERE lls_id = ?", lls_id);
		Query.update("DELETE FROM checklist WHERE dll_id IN (SELECT dll_id FROM dok_lelang WHERE lls_id = ?)", lls_id);
		Query.update("DELETE FROM dok_lelang_content WHERE dll_id IN (SELECT dll_id FROM dok_lelang WHERE lls_id = ?)", lls_id);
		Query.update("DELETE FROM dok_lelang WHERE lls_id = ?", lls_id);
	}


	// lelang cepat : untuk dapat menetapkan pemenang, harus melakukan verifikasi ke SIKaP
	@Transient
	public boolean sudahVerifikasiSikap(){
		return Peserta.count("lls_id=? AND sudah_verifikasi_sikap=?", this.lls_id, Integer.valueOf(1)) > 0;
	}

	// cari lelang yg aktif , param paketId
	public static Lelang_seleksi findAktifByPaket(Long paketId) {
		return find("pkt_id=? and lls_status='1' order by lls_id DESC", paketId).first();
	}

	public static Lelang_seleksi findByPaket(Long paketId) {
		return find("pkt_id=? order by lls_id DESC", paketId).first();
	}

	public static Lelang_seleksi findByPaketNewestVersi(Long paketId) {
		return find("pkt_id=? order by lls_versi_lelang DESC", paketId).first();
	}

	public static MetodePenawaranHarga findMetodePenawaranHarga(Long lelangId) {
		return Query.find("SELECT lls_metode_penawaran FROM lelang_seleksi WHERE lls_id=?", MetodePenawaranHarga.class, lelangId).first();
	}

	public boolean isAuction() {
		return lls_metode_penawaran != null && lls_metode_penawaran.equals(MetodePenawaranHarga.AUCTION);
	}

	// check lelang cepat
	public boolean isExpress() {
		return getPemilihan().isLelangExpress();
	}

	public boolean isSedangPersetujuan() {
		return Persetujuan.isSedangPersetujuan(lls_id);
	}

	public boolean allowBatalPersetujuan(){
		Persetujuan persetujuan = Persetujuan.findByPegawaiPengumuman(Active_user.current().pegawaiId,lls_id);
		if(persetujuan != null){
			return persetujuan.pst_status.isSetuju() && isSedangPersetujuan() && lls_status.isDraft();
		}
		return false;
	}

	public boolean isSedangPersetujuanPemenang() {
		return Persetujuan.isSedangPersetujuanPemenang(lls_id);
	}

	public boolean allowBatalPersetujuanPemenang(){
		Persetujuan persetujuan = Persetujuan.findByPegawaiPenetapanAkhir(Active_user.current().pegawaiId,lls_id);
		Evaluasi evaluasi = getEvaluasiAkhir();
		if(persetujuan != null && evaluasi != null){
			return persetujuan.pst_status.isSetuju() && isSedangPersetujuanPemenang() && evaluasi.eva_status.isSedangEvaluasi();
		}
		return false;
	}

    public boolean isSedangPersetujuanBatalTender() {
        return Persetujuan.isSedangPersetujuanBatalTender(lls_id);
    }

	public boolean allowBatalPersetujuanBatalTender(){
		Persetujuan persetujuan = Persetujuan.findByPegawaiLelangInJenisBatal(lls_id);
		if(persetujuan != null){
			return persetujuan.pst_status.isSetuju() && isSedangPersetujuan() && lls_status.isAktif();
		}
		return false;
	}

	public boolean isPascaSatuFile() {
		return getMetode().kualifikasi.isPasca()
				&& getMetode().dokumen.isSatuFile();
	}

	public boolean isPascaDuaFile() {
		return getMetode().kualifikasi.isPasca()
				&& getMetode().dokumen.isDuaFile();
	}

	public boolean isPraDuaFile() {
		return getMetode().kualifikasi.isPra()
				&& getMetode().dokumen.isDuaFile();
	}

	public boolean isPraDuaTahap() {
		return getMetode().kualifikasi.isPra()
				&& getMetode().dokumen.isDuaTahap();
	}

	public boolean allowCreateSppbj(){
		int jumlahSppbj = Sppbj.getJumlahSppbjByLelangAndPpk(lls_id,Active_user.current().ppkId);

		if(isItemized()){ //jika itemized, jumlah sppbj maksimal sejumlah pemenang
			int jumlahPemenang = CollectionUtils.size(findAllWinnerCandidate());
			return jumlahSppbj < jumlahPemenang;
		}

		return jumlahSppbj == 0;

	}

	public MetodeDokumen[] getMetodeDokumenList(){
		if(isKonsultansiJKKonstruksi())
			return MetodeDokumen.metodeDokumenPraSeleksi;
		else if(isPascakualifikasi())
			return isExpress() ?  MetodeDokumen.metodeDokumenExpress : MetodeDokumen.metodeDokumenPasca;
		else if(isKonstruksi())
			return MetodeDokumen.metodeDokumenPraSeleksi;
		return  MetodeDokumen.metodeDokumenPra;
	}

	public MetodeKualifikasi[] getMetodeKualifikasiList(){
		Kategori kategori = getPaket().kgr_id;
		if (kategori.isKonsultansiPerorangan()){
			return MetodeKualifikasi.kualifikasiPasca;
		}
		else if (kategori.isKonsultansi() || kategori.isJkKonstruksi()){
			return MetodeKualifikasi.kualifikasiPra;
		}
		else{
			if(isExpress()){
				return MetodeKualifikasi.kualifikasiPasca;
			}
			return MetodeKualifikasi.kualifikasiPraPasca;
		}
	}

	public boolean isShowNotifTambahJadwalTambahan() {
		return !is_kualifikasi_tambahan && getJumlahPesertaDikirimPesan() > 0;
	}

	public boolean isAllowNego() {
		Evaluasi penetapan = getEvaluasiAkhir();
		if(penetapan != null && !isAuction()) {
			TahapNow tahapNow = getTahapNow();
			if(isKonsultansi() || isJkKonstruksi()) {
				if(getEvaluasi().isKualitas())
					return tahapNow.isKlarifikasiTeknisBiaya() && penetapan.eva_status.isSedangEvaluasi() && !isSedangPersetujuanPemenang();
				else
					return tahapNow.isKlarifikasiTeknisBiaya();
			} else  {
				return Nilai_evaluasi.count("eva_id=?",penetapan.eva_id) < 3 && penetapan.eva_status.isSedangEvaluasi() && tahapNow.isPenetapaPemenang() && !isSedangPersetujuanPemenang();
			}
		}
		return false;
	}

	@Override
	public boolean isDitutup() {
		return lls_status.isDitutup();
	}

	public List<Nilai_evaluasi> getPemenangList() {
		Evaluasi penetapan = getEvaluasiAkhir();
		if(penetapan != null && penetapan.eva_status.isSelesai()) {
			List<Nilai_evaluasi> nev = new ArrayList<>();
			List<Nilai_evaluasi> calonPemenang = isExpress() ? Query.find(
					"SELECT * FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id = ? AND psr.is_pemenang_verif = ?",
					Nilai_evaluasi.class, penetapan.eva_id, 1).fetch() : null;
			if (calonPemenang != null) {
				nev.addAll(calonPemenang);
			} else {
				calonPemenang = Query.find("SELECT * FROM nilai_evaluasi nev JOIN peserta psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id = ? AND psr.is_pemenang = ?",
						Nilai_evaluasi.class, penetapan.eva_id, 1).fetch();
				if (calonPemenang != null) {
					nev.addAll(calonPemenang);
				}
			}
			return nev;
		}
		return null;
	}

	//flag ijinkan lelang diulang
	public boolean isAllowUlang() {
		Long currentLelangId = Query.find("SELECT lls_id FROM lelang_seleksi WHERE pkt_id=? ORDER BY lls_versi_lelang DESC", Long.class, pkt_id).first();
		return isDitutup() && currentLelangId.equals(lls_id);
	}

	public Evaluasi getTeknis() {
		if(teknis == null)
			teknis = Evaluasi.findTeknis(lls_id);
		return teknis;
	}

	public boolean isSudahEvaluasiTeknis() {
		Evaluasi teknis = getTeknis();
		return teknis != null && teknis.eva_status.isSelesai();
	}

	public List<Nilai_evaluasi> findAllWinnerCandidate(){
		Evaluasi evaluasi = getEvaluasiAkhir();
		if(evaluasi == null)
			return null;
		if(isExpress())
			return Nilai_evaluasi.find("eva_id = ? and psr_id in (SELECT p.psr_id FROM peserta p where p.sudah_verifikasi_sikap = 1 " +
					"and p.lls_id = ?) order by nev_urutan", evaluasi.eva_id, lls_id).fetch();
		return Nilai_evaluasi.find("eva_id=? and nev_urutan is not null order by nev_urutan", evaluasi.eva_id).fetch();
	}

	public String getAlasanDitutup() {
		List<Checklist_gagal> checklist = Checklist_gagal.findBy(lls_id, Checklist_master.JenisChecklist.CHECKLIST_PRAKUALFIKASI_BATAL);
		if(CollectionUtils.isEmpty(checklist))
			checklist = Checklist_gagal.findBy(lls_id, Checklist_master.JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL);
		StringBuilder result = new StringBuilder();
		if(!CollectionUtils.isEmpty(checklist)){
			for(Checklist_gagal item: checklist)
				if(item != null && item.getChecklist_master() != null)
					result.append("- ").append(item.getChecklist_master().ckm_nama).append("<br/>");
		}
		if(!StringUtils.isEmpty(lls_ditutup_karena))
			result.append("- ").append(lls_ditutup_karena);
		return result.toString();
	}

	public String getAlasanDiUlang() {
		List<Checklist_gagal> checklist = Checklist_gagal.findBy(lls_id, Checklist_master.JenisChecklist.CHECKLIST_PRAKUALFIKASI_BATAL);
		if(CollectionUtils.isEmpty(checklist))
			checklist = Checklist_gagal.findBy(lls_id, Checklist_master.JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL);
		StringBuilder result = new StringBuilder();
		if(!CollectionUtils.isEmpty(checklist)){
			for(Checklist_gagal item: checklist)
				if(item != null && item.getChecklist_master() != null)
					result.append("- ").append(item.getChecklist_master().ckm_nama).append("<br/>");
		}
		if(!StringUtils.isEmpty(lls_diulang_karena))
			result.append("- ").append(lls_diulang_karena);
		return result.toString();
	}
}
