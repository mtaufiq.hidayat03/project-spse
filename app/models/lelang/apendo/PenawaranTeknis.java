package models.lelang.apendo;

import com.google.gson.JsonObject;
import ext.FormatUtils;
import models.lelang.History_dok_penawaran;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

public class PenawaranTeknis {

	private String nama;
	private String email;
	private String telpon;
	private String urlDownload;
	private String psrId;
	private Integer berlaku;
	private String surat;
	private Double totalHarga;
	private JsonObject history;

	public PenawaranTeknis(Long psrId) {
		this.psrId = psrId.toString();
		this.history = new JsonObject();
	}

	public String getSurat() {
		return surat;
	}
	public void setSurat(String surat) {
		this.surat = surat;
	}
	public Integer getBerlaku() {
		return berlaku;
	}
	public void setBerlaku(Integer berlaku) {
		this.berlaku = berlaku;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelpon() {
		return telpon;
	}
	public void setTelpon(String telpon) {
		this.telpon = telpon;
	}
	public String getUrlDownload() {
		return urlDownload;
	}
	public void setUrlDownload(String urlDownload) {
		this.urlDownload = urlDownload;
	}
	public String getPsrId() {
		return psrId;
	}
	public Double getTotalHarga() {
		return totalHarga;
	}
	public void setTotalHarga(Double totalHarga) {
		this.totalHarga = totalHarga;
	}
	public JsonObject getHistory() {
		return history;
	}

	// setup history dokumen penawaran admin, teknis untuk keperluan audit
	public void historyTeknis() {
		Long pesertaId = Long.parseLong(psrId);
		List<History_dok_penawaran> teknisList = History_dok_penawaran.findPenawaranTeknisPeserta(pesertaId);
		if(!CollectionUtils.isEmpty(teknisList)) {
			JsonObject jsonTeknis = new JsonObject();
			for(History_dok_penawaran teknis : teknisList) {
				jsonTeknis.addProperty("id", teknis.dok_id_attachment);
				jsonTeknis.addProperty("tgl_kirim", FormatUtils.formatDateTimeInd(teknis.dok_tgljam));
				jsonTeknis.addProperty("ukuran", teknis.getDokumen().blb_ukuran);
			}
			history.add("teknis", jsonTeknis);
		}
	}

	public void historyHarga() {
		Long pesertaId = Long.parseLong(psrId);
		List<History_dok_penawaran> hargaList = History_dok_penawaran.findPenawaranHargaPeserta(pesertaId);
		if(!CollectionUtils.isEmpty(hargaList)) {
			JsonObject jsonHarga = new JsonObject();
			for(History_dok_penawaran dokharga : hargaList) {
				jsonHarga.addProperty("id", dokharga.dok_id_attachment);
				jsonHarga.addProperty("tgl_kirim", FormatUtils.formatDateTimeInd(dokharga.dok_tgljam));
				jsonHarga.addProperty("ukuran", dokharga.getDokumen().blb_ukuran);
			}
			history.add("harga", jsonHarga);
		}
	}
}
