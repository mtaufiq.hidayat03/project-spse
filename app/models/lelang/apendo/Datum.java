package models.lelang.apendo;

import com.google.gson.JsonObject;
import ext.FormatUtils;
import models.agency.DaftarKuantitas;
import models.agency.Rincian_hps;
import models.lelang.*;
import models.rekanan.Rekanan;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

public class Datum {

    public List<Detail> detail = new ArrayList<Detail>();
    public Long objId;
    public Long owner;
    public Summary summary = new Summary();
    public Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public static Comparator<Datum> totalComparator = new Comparator<Datum>() {
        public int compare(Datum datum1, Datum datum2) {
            Double total1 = datum1.summary.getTotal();
            Double total2 = datum2.summary.getTotal();
//    		ascending order
            return total1.compareTo(total2);
//    		descending order
//    		return total2.compareTo(total1);
        }
    };

    public static List<PenawaranTeknis> render(Datum[] datums, Lelang_seleksi lelang, Lelang_token lelang_token, String downloadUrl, boolean auditor) {
        DaftarKuantitas daftarKuantitas;
        Rincian_hps rincian;
        List<PenawaranTeknis> pesertaList = new ArrayList<>();
        Dok_penawaran penawaranPeserta;
        Peserta pesertaObj;
        Rekanan rekanan;
        PenawaranTeknis peserta;
        Evaluasi harga = null, penetapan = null;
        Nilai_evaluasi nilai_harga, nilai_akhir;
        boolean express = lelang.getPemilihan().isLelangExpress();
        if (express) {
            harga = Evaluasi.findNCreateHarga(lelang.lls_id);
            penetapan = Evaluasi.findNCreatePenetapanPemenang(lelang.lls_id);
        }
        int urutan = 1;

        for (Datum data : datums) {
            Summary summary = data.summary;
            daftarKuantitas = new DaftarKuantitas();
            daftarKuantitas.total = summary.getTotal();
            daftarKuantitas.fixed = false;
            daftarKuantitas.items = new ArrayList<>();
            List<Detail> detailList = data.detail;
            for (Detail detail : detailList) {
                if(detail == null || detail.id == null)
                    continue;
                rincian = new Rincian_hps();
                rincian.item = detail.name;
                rincian.unit = detail.unit1;
                rincian.vol = detail.volume1;
                rincian.harga = detail.price;
                rincian.total_harga = detail.priceTotal;
                rincian.keterangan = detail.remark;
                rincian.pajak = detail.tax;
                daftarKuantitas.items.add(rincian);
            }
            pesertaObj = Peserta.findById(data.owner);
            pesertaObj.dkh = daftarKuantitas;
            pesertaObj.psr_harga = daftarKuantitas.total;
            pesertaObj.psr_harga_terkoreksi = daftarKuantitas.total;
            if(lelang_token.isPanitia())
                pesertaObj.save();

            rekanan = Rekanan.findByPeserta(pesertaObj.psr_id);
            peserta = new PenawaranTeknis(pesertaObj.psr_id);
            peserta.setNama(rekanan.rkn_nama);
            peserta.setEmail(rekanan.rkn_email);
            peserta.setTelpon(rekanan.rkn_telepon);

            if (express) { // jika express, nilai evaluasi harga dan akhir
                // langsung diset sistem, langsung dapat pemenangny
                penawaranPeserta = Dok_penawaran.findPenawaranPeserta(pesertaObj.psr_id, Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA);
                if (harga != null) {
                    nilai_harga = Nilai_evaluasi.findNCreateBy(harga.eva_id, pesertaObj, harga.eva_jenis);
                    // penawar lebih dari 3 dan harga melebihi HPS maka tidak lulus
                    nilai_harga.nev_harga = pesertaObj.psr_harga;
                    nilai_harga.nev_harga_terkoreksi = pesertaObj.psr_harga_terkoreksi;
                    nilai_harga.nev_lulus = nilai_harga.isHargaOverHps(lelang.getPaket().pkt_hps) ? Nilai_evaluasi.StatusNilaiEvaluasi.TDK_LULUS : Nilai_evaluasi.StatusNilaiEvaluasi.LULUS;
                    if(lelang_token.isPanitia())
                        nilai_harga.save();
                }
                if (penetapan != null) {
                    nilai_akhir = Nilai_evaluasi.findNCreateBy(penetapan.eva_id, pesertaObj, penetapan.eva_jenis);
                    nilai_harga = Nilai_evaluasi.findBy(harga.eva_id, nilai_akhir.psr_id);
                    if(nilai_harga.isLulus()) {
                        nilai_akhir.nev_urutan = urutan;
                        Peserta obj = Peserta.findBy(nilai_akhir.psr_id);
                        nilai_akhir.nev_lulus = nilai_akhir.nev_urutan == 1 ? Nilai_evaluasi.StatusNilaiEvaluasi.LULUS: Nilai_evaluasi.StatusNilaiEvaluasi.TDK_LULUS;
                        obj.is_pemenang = nilai_akhir.nev_urutan == 1 ? Integer.valueOf(1):Integer.valueOf(0);
                        if (lelang_token.isPanitia()) {
                            obj.save();
                            nilai_akhir.save();
                        }
                    }else {
                        nilai_akhir.delete();//hapus evaluasi akhir untuk peserta yang tidak lulus
                        // agar tidak muncul di penetapan pemenang
                    }
                }
            } else
                penawaranPeserta = Dok_penawaran.findPenawaranPeserta(pesertaObj.psr_id, Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);

            if (penawaranPeserta != null) {
                peserta.setUrlDownload(downloadUrl + "?id=" + penawaranPeserta.dok_id_attachment);
                peserta.setBerlaku(penawaranPeserta.dok_waktu);
                peserta.setSurat(penawaranPeserta.dok_surat);
            }
            peserta.setTotalHarga(pesertaObj.psr_harga_terkoreksi);
            if(auditor) {
               peserta.historyTeknis();
               peserta.historyHarga();
            }
            pesertaList.add(peserta);
            urutan++;
        }
      /*  if (express && penetapan != null) {
            penetapan.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
            penetapan.eva_tgl_setuju = controllers.BasicCtr.newDate();
            if(lelang_token.isPanitia())
                penetapan.save();
        }*/
        // save tanggal pembukaan harga
        if(lelang.lls_tanggal_pembukaan_harga == null) {
            lelang.lls_tanggal_pembukaan_harga = new Date();
            lelang.save();
        }
        return pesertaList;
    }

    // render Pembukaan Penawaran File 1 : Admin dan Teknis
    public static List<PenawaranTeknis> renderPembukaanTeknis(Datum[] datums, Lelang_seleksi lelang, String downloadUrl, boolean auditor) {
        Rekanan rekanan= null;
        PenawaranTeknis peserta= null;
        List<PenawaranTeknis> pesertaList = new ArrayList<>();
        Dok_penawaran penawaranPeserta= null;
        for (Datum data : datums) {
            // don't get confused, in pembukaan teknis we set the owner
            // as pesertaId
            rekanan = Rekanan.findByPeserta(data.owner);

            // create a new peserta
            peserta = new PenawaranTeknis(data.owner);
            peserta.setNama(rekanan.rkn_nama);
            peserta.setEmail(rekanan.rkn_email);
            peserta.setTelpon(rekanan.rkn_telepon);

            // try to get penawaran teknis administrasi
            penawaranPeserta = Dok_penawaran.findPenawaranPeserta(data.owner, Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
            if (penawaranPeserta != null) {
                peserta.setUrlDownload(downloadUrl + "?id=" + penawaranPeserta.dok_id_attachment);
                peserta.setBerlaku(penawaranPeserta.dok_waktu);
                peserta.setSurat(penawaranPeserta.dok_surat);
            }
            if(auditor) {
                peserta.historyTeknis();
            }
            pesertaList.add(peserta);
        }

        // save tanggal pembukaan adm teknis
        if(lelang.lls_tanggal_pembukaan_admteknis == null) {
            lelang.lls_tanggal_pembukaan_admteknis = new Date();
            lelang.save();
        }
        return pesertaList;
    }
}
