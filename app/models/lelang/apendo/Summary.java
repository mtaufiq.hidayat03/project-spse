package models.lelang.apendo;

import java.util.HashMap;
import java.util.Map;

public class Summary {

	private Double pajak;
	private Double pajaktotal;
	private Double subtotal;
	private Double total;
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public Double getPajak() {
		return pajak;
	}

	public void setPajak(Double pajak) {
		this.pajak = pajak;
	}

	public Double getPajaktotal() {
		return pajaktotal;
	}

	public void setPajaktotal(Double pajaktotal) {
		this.pajaktotal = pajaktotal;
	}

	public Double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(Double subtotal) {
		this.subtotal = subtotal;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
