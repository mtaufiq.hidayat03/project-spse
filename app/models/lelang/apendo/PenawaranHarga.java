package models.lelang.apendo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PenawaranHarga {

	public List<Datum> data = new ArrayList<Datum>();
	public Long objId;
	public Long owner;
	public Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public void recalculateVolume(){
		for (Datum datum: data){
			if(datum == null)
				continue;
			Double total = 0.0;
			for(Detail detail : datum.detail){
				if(detail == null || detail.id == null)
					continue;
				if(detail.volume1<=0.0)
					detail.volume1 = 1.0;
				if(detail.volume2<=0.0)
					detail.volume2 = 1.0;
				detail.priceTotal  = detail.price * detail.volume1 * detail.volume2;
				detail.taxAmount = detail.tax*detail.priceTotal/100;
				total += detail.priceTotal + detail.taxAmount ;
			}
			datum.summary.setTotal(total);
		}
	}
   
}
