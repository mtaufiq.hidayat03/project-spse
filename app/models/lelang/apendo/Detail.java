package models.lelang.apendo;

import java.util.HashMap;
import java.util.Map;

public class Detail {

	public Integer id;
	public String name;
	public Double price;
	public Double priceTotal;
	public String remark;
	public String unit1;
	public String unit2;
	public Double volume1;
	public Double volume2;
	public Double tax;
	public Double taxAmount;
	public Map<String, Object> additionalProperties = new HashMap<String, Object>();

}
