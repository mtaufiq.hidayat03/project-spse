package models.lelang;

import org.sql2o.ResultSetHandler;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * model untuk Kode UNSPSC
 * untuk kategorisasi Paket
 * @author Arief Ardiyansah
 *
 */
@Table(name = "UNSPSC")
public class Unspsc extends BaseTable {
	
	@Id
	public Integer commodity_id;
	
	public String commodity_title;
	
	public String class_title;
	
	public String family_title;
	
	public String segment_title;
		
	public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
          	 tmp[0] = rs.getString("commodity_id");
          	 tmp[1] = rs.getString("commodity_title");
          	 tmp[2] = rs.getString("class_title");
          	 tmp[3] = rs.getString("family_title");
          	 tmp[4] = rs.getString("segment_title");
          	 return tmp;
		}
	};
	
}
