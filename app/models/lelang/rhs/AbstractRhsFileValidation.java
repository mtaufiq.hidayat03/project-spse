package models.lelang.rhs;


import java.io.InputStream;
import java.util.Date;
import java.util.Locale;


public interface AbstractRhsFileValidation {


	public void setVersiApendo(String versiApendo); 
	public void setProduction(boolean isProduction);
	public void setVersiSpamkodok(String versiSpamkodok); 
	public void setMessage(Message messageSource);
	
	public interface Message
	{
		public String getMessage(String format, Object[] params);
		public String getMessage(String format, Object[] params, Locale locale);
		public String getMessage(String format);
	}
					
	public abstract boolean uploadPenawaran(Long pkt_id, Long lls_id, String pkt_nama,
			int jenisDokumen, String jenisDokumenLabel, InputStream file,
			Date CurrentDate, boolean isUseSpamkodok) throws Exception; 
		
}
