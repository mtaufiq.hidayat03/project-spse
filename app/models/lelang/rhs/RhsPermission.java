package models.lelang.rhs;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import models.common.ConfigurationDao;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.lelang.Dok_penawaran;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import play.Logger;
import play.Play;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.libs.WS;
import play.libs.ws.WSRequest;
import utils.Encrypt;

import javax.persistence.Transient;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Lambang on 8/31/2016.
 */

@Table(name="RHS_PERMISSION")
public class RhsPermission extends BaseModel{

    @Id(sequence="seq_rhs_permission", function="nextsequence")
    public Long rpn_id;

    public Long psr_id;

    public Integer rpn_allow_teknis;

    public Integer rpn_allow_harga;

    @Transient
    private Peserta peserta;

    public RhsPermission() {}

    public RhsPermission(Peserta model) {
        this.psr_id = model.psr_id;
        this.rpn_allow_harga = 0;
        this.rpn_allow_teknis = 0;
    }

    public Peserta getPeserta() {
        if(peserta == null)
            peserta = Peserta.findById(psr_id);
        return peserta;
    }

    @Transient
    public BlobTable getBlob() {
        if(psr_id == null)
            return null;

        Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(psr_id, Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA);

        return BlobTableDao.getLastById(dok_penawaran.dok_id_attachment);
    }

    public static RhsPermission getByPsrId(Long pesertaId){

        return Query.find("SELECT * FROM rhs_permission WHERE psr_id=?", RhsPermission.class, pesertaId).first();

    }

    public static Long countByLelang(Long lelangId){

        String sql = "SELECT count(p.psr_id) from peserta p INNER JOIN rhs_permission rp ON p.psr_id = rp.psr_id WHERE p.lls_id =?";

        return Query.count(sql, lelangId);

    }

    public static List<RhsPermission> getByLelang(Long lelangId){

        return Query.find("SELECT rp.* from rhs_permission rp INNER JOIN peserta p ON rp.psr_id = p.psr_id " +
                "WHERE p.lls_id =?", RhsPermission.class, lelangId).order("psr_id").fetch();

    }

    public static List<RhsPermission> getByLelangAndPermission(Long lelangId){

        return Query.find("SELECT rp.* from rhs_permission rp INNER JOIN peserta p ON rp.psr_id = p.psr_id " +
                "WHERE p.lls_id =? AND rpn_allow_harga = 1 AND rpn_allow_teknis = 1", RhsPermission.class, lelangId).order("psr_id").fetch();

    }

    public static List<RhsPermission> getPermissionByLelangId(Long lelangId){
        return Query.find("SELECT rp.* from rhs_permission rp INNER JOIN peserta p ON rp.psr_id = p.psr_id " +
                "WHERE p.lls_id =? AND (rp.rpn_allow_harga = 1 OR rp.rpn_allow_teknis = 1) ", RhsPermission.class, lelangId)
                .fetch();
    }

    public static List<RhsPermission> getByPermission(){

        return find("rpn_id in (SELECT rpn_id from rhs_permission WHERE rpn_allow_harga = 1 OR rpn_allow_teknis = 1)").fetch();

    }

    public void setPermissionByType(Long tipe) {
        if (tipe == 0) {
            this.rpn_allow_harga = this.rpn_allow_harga == 0 ? 1 : 0;
        } else if (tipe == 1) {
            this.rpn_allow_teknis = this.rpn_allow_teknis == 0 ? 1 : 0;
        }
    }

    /**
     * get latest permission if not equal to total participant,
     * search unregister participant and insert to permission if price offering or technical or both exist
     * @param lelangSeleksi Lelang_seleksi*/
    public static void insertPermissionIfNull(Lelang_seleksi lelangSeleksi) {
        Logger.debug("insert permission if null");
        List<Peserta> pesertaList = Peserta.findByIdLelang(lelangSeleksi.lls_id);
        List<RhsPermission> rhsPermissionList = RhsPermission.getByLelang(lelangSeleksi.lls_id);
        if (!pesertaList.isEmpty() && rhsPermissionList.size() != pesertaList.size()) {
            for (Peserta peserta : pesertaList) {
                RhsPermission rhsPermissionExist = RhsPermission.getByPsrId(peserta.psr_id);
                // insert ke table Rhs_permission
                if (rhsPermissionExist == null) {
                    //pengecekan apakah peserta sudah upload dok penawaran
                    peserta.withDokPenawaranHarga();
                    peserta.withDokPenawaranTeknis();
                    if (peserta.dokPenawaranHarga != null || peserta.dokPenawaranTeknis != null) {
                        new RhsPermission(peserta).save();
                    }
                }
            }
        }
    }

    /**
     * generating payload and send forensik to helpdesk
     * @param lelangId lelang id
     * @return String message*/
    public static String sendForensik(Long lelangId) throws IOException {
        Logger.debug("send forensik to help-desk");
        final String api = Play.configuration.getProperty("spse.helpdesk.url");
        Logger.debug("get permissions");
        List<RhsPermission> rhsPermissionList = RhsPermission.getPermissionByLelangId(lelangId);
        if (rhsPermissionList.isEmpty()) {
            return "Tidak ada data untuk dikirimkan ke SPSE-Helpdesk";
        }
        Logger.debug("total permission: " + rhsPermissionList.size());
        JsonArray jsonArray = new JsonArray();
        byte buffer[] = new byte[2048];
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(out);
        for (RhsPermission rhsPermission : rhsPermissionList) {
            Peserta peserta = Peserta.findBy(rhsPermission.psr_id);
            peserta.withDokPenawarans();
            Logger.debug("total document: " + peserta.dokPenawarans.size());
            if (!peserta.dokPenawarans.isEmpty()) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("psr_id", peserta.psr_id);
                jsonObject.addProperty("lls_id", peserta.lls_id);
                jsonArray.add(jsonObject);
                InputStream is = zipingRhsForensik(peserta.dokPenawarans, peserta);
                zos.putNextEntry(new ZipEntry(peserta.psr_id + "_rhs.zip"));
                int length;
                while ((length = is.read(buffer)) >= 0)
                    zos.write(buffer, 0, length);
                is.close();
            }
        }
        zos.closeEntry();
        zos.close();
        InputStream iszip = new ByteArrayInputStream(out.toByteArray());
        String params = URLEncoder.encode(Encrypt.encryptSikapParams(jsonArray.toString().getBytes("UTF-8")), "utf-8");
        String url = api + "/serviceCtr/sendForensikRhs?p="+params;
        try {
            WSRequest request = WS.url(url);
            Logger.info("send request to helpdesk");
            String response = request.body(iszip).timeout(ConfigurationDao.CONNECTION_TIMEOUT).post().getJson().toString();
            JsonParser parser = new JsonParser();
            JsonObject jsonResponse = (JsonObject) parser.parse(response);
            return jsonResponse.get("msg").getAsString();
        } catch (Exception ex) {
            Logger.error(ex, "Terjadi Kesalahan Koneksi Ke Spse Helpdesk");
            return ex.getMessage();
        }
    }

    /**
     * zipping rhs files
     * @param docs
     * @param peserta
     * @return InputStream*/
    private static InputStream zipingRhsForensik(List<Dok_penawaran> docs, Peserta peserta) throws IOException {
        Logger.info("zipping documents");
        if (!docs.isEmpty()) {
            byte buffer[] = new byte[2048];
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(out);
            int length;
            for (Dok_penawaran doc : docs) {
                BlobTable blob = BlobTableDao.getLastById(doc.dok_id_attachment);
                if (blob != null) {
                    zos.putNextEntry(new ZipEntry(peserta.getAdmFileNameByDocumentType(doc.dok_jenis)));
                    InputStream is = new FileInputStream(blob.getFile());
                    while ((length = is.read(buffer)) >= 0)
                        zos.write(buffer, 0, length);
                    is.close();
                }
            }
            zos.closeEntry();
            zos.close();
            return new ByteArrayInputStream(out.toByteArray());
        }
        return null;
    }
}
