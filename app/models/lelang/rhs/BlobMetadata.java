package models.lelang.rhs;

import java.sql.Date;

/**
 * Dipakai oleh servlet
 * Ini berisi metadata yg dikirim dari cloud
 * @author Mr. Andik
 *
 */
public class BlobMetadata {

	public Long  blb_id_content;
	
	public Integer blb_versi;
	
	public String blb_tipe;
	
	public String blb_hash;
	
	public java.util.Date blb_date_time; //Jam upload, sesuai jam CDN Mirror
	
	public boolean blb_pull_home;
	
	public Long blb_ukuran;
	
	public Date blb_pull_home_date; //jam 'ditarik pulang' ke LPSE
		
	public String blb_upload_client; //info client yg upload
	
	public String blb_cloud_url;

	public java.util.Date blb_start_upload;

	public String upload_token;
	
	//dapatkan nama file
	public String blb_nama_file; //dipakai untuk keperluan Json
	
}
