package models.lelang.rhs;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import play.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

/**
 * Class ini digunakan untuk validasi file RHS. Class ini juga akan didownload
 * oleh Cloud
 * 
 * @author Mr. Andik
 *
 */
@SuppressWarnings("serial")
public class RhsFileValidation implements AbstractRhsFileValidation, Serializable {

	private Message messageSource;
	private String versiApendo;
	private boolean isProduction;
	private String versiSpamkodok;

	private static String md5 = null;

	/**
	 * dapatkan md5 hash dari file ini
	 * 
	 * @return
	 */
	public static String getMD5Hash() {
		if (md5 != null)
			return md5;
		String path = RhsFileValidation.class.getSimpleName() + ".class";
		InputStream is = RhsFileValidation.class.getResourceAsStream(path);
		byte[] ary;
		try {
			ary = IOUtils.toByteArray(is);
			md5 = Hex.encodeHexString(DigestUtils.getMd5Digest().digest(ary));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return md5;
	}

	public void setMessage(Message messageSource) {
		this.messageSource = messageSource;
	}


	private boolean uploadPenawaranSpamkodok(Long pkt_id, Long lls_id, String namaPaket, int jenis,
			String jenisDokumenLabel, InputStream file, Date CurrentDate) throws Exception {
		// Long pesertaId = peserta.getPsr_id();
		// Long lelangId = peserta.getLls_id();
		if (jenis == 4) {
			return true;
		} else if (versiSpamkodok.length() > 1) {
			boolean valid = true;
			String configVersi = versiSpamkodok;
			int lenChek = 1000;
			byte[] bytes = new byte[lenChek];
			InputStream input = file;
			try {
				int read = input.read(bytes, 0, lenChek);
				if (read != lenChek) {
					valid = false;
					String message = messageSource.getMessage("invalid-spamkodok-file", new Object[] { configVersi });
					throw new Exception(message);
				}
			} catch (IOException e) {
				String message = "Error ketika membaca file: " + e.getMessage();
				Logger.error(message);
				throw new Exception(message);
			}

			// pengecekan #0 : validitas file rhs
			if (valid) {
				byte[] spamkodok = { 19, 16, 1, 13, 11, 15, 4, 15, 11, 27 };
				for (int i = 0; i < spamkodok.length; i++) {
					if (bytes[i] != spamkodok[i]) {
						valid = false;
						break;
					}
				}
				if (!valid) {
					String message = messageSource.getMessage("invalid-spamkodok-file", new Object[] { configVersi });
					throw new Exception(message);
				}
			}

			// pengecekan #1 : versi spamkodok yang digunakan
			String fileVersi = "1.1";
			if (valid) {
				char[] byteFileVersi = new char[3];
				char[] convertVersi = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
				if ((int) bytes[10] < 10 && (int) bytes[11] == 28 && (int) bytes[12] < 10) {
					byteFileVersi[0] = convertVersi[(int) bytes[10]];
					byteFileVersi[1] = '.';
					byteFileVersi[2] = convertVersi[(int) bytes[12]];
					fileVersi = new String(byteFileVersi);
					if (!fileVersi.equals(configVersi)) {
						String message = messageSource.getMessage("invalid-spamkodok-versi",
								new Object[] { fileVersi, configVersi });
						throw new Exception(message);
					}
				} else {
					String message = messageSource.getMessage("invalid-spamkodok-file", new Object[] { configVersi });
					throw new Exception(message);
				}
			}

			// pengecekan #2 : id lelang & jenis dokumen
			if (isProduction) {
				if (valid) {
					int checkCount = 0;
					int index = 0;
					int[] indexCheck = new int[4];
					indexCheck[checkCount] = index;
					for (int i = 0; i < bytes.length; i++) {
						if (bytes[index] == '#' && bytes[index + 1] == 0x0C && bytes[index + 2] == 0x05
								&& bytes[index + 3] == 0x0D && bytes[index + 4] == 0x13 && bytes[index + 5] == 0x01
								&& bytes[index + 6] == 0x0E && bytes[index + 7] == 0x05 && bytes[index + 8] == 0x07
								&& bytes[index + 9] == '#') {
							checkCount++;
							indexCheck[checkCount] = index + 10;
							if (checkCount == 3)
								break;
						}
						index++;
					}

					//// TEST
					// byte[] test = new byte[100];
					// for (int i = 0; i < test.length; i++)
					// test[i] = (byte) (bytes[i] + 0x10);
					// String tes = new String(test);

					Logger.debug("length check: " + checkCount);
					if (checkCount == 3) {
						byte[] check1 = new byte[indexCheck[2] - 10 - indexCheck[1]];
						for (int i = 0; i < check1.length; i++)
							check1[i] = (byte) (bytes[indexCheck[1] + i] + 0x10);
						String strIDPaket = new String(check1);
						Logger.debug("kode paket: " + strIDPaket);

						byte[] check2 = new byte[indexCheck[3] - 10 - indexCheck[2]];
						for (int i = 0; i < check2.length; i++)
							check2[i] = (byte) (bytes[indexCheck[2] + i] + 0x10);
						String strNamaPaket = new String(check2);
						Logger.debug("nama paket: " + strNamaPaket);

						// pengecekan nama paket dihilangkan, karna kejadian di
						// undip 20131223, panitia menambah/merubah info di nama
						// paket saat lelang berjalan
						if (!String.valueOf(pkt_id)
								.equals(strIDPaket)/*
													 * || !paket.getPkt_nama().
													 * equals(strNamaPaket)
													 */) {
							String message = messageSource.getMessage("invalid-spamkodok-key",
									new Object[] { namaPaket, String.valueOf(pkt_id) });
							throw new Exception(message);
						} else {
							return true;
						}
					} else {
						String message = messageSource.getMessage("invalid-spamkodok-file",
								new Object[] { configVersi });
						throw new Exception(message);
					}
				}
			} else {
				return true;
			}
		}
		return true;
	}

	public boolean uploadPenawaran(Long pkt_id, Long lls_id, String pkt_nama, int jenisDokumen,
			String jenisDokumenLabel, InputStream file, Date CurrentDate, boolean isUseSpamkodok) throws Exception {
		if (isUseSpamkodok)
			return uploadPenawaranSpamkodok(pkt_id, lls_id, pkt_nama, jenisDokumen, jenisDokumenLabel, file,
					CurrentDate);
		else
			return uploadPenawaranApendo(lls_id, pkt_nama, jenisDokumen, jenisDokumenLabel, file, CurrentDate);

	};

	private boolean uploadPenawaranApendo(Long lelangId, String namaPaket, int jenis, String jenisDokumenLabel,
			InputStream file, Date CurrentDate) throws Exception {
		// Long pesertaId = peserta.getPsr_id();
		// Long lelangId = peserta.getLls_id();

		if (jenis == 4) {// JenisDokPenawaran.PENAWARAN_TAMBAHAN.getKey()
			return true;
		} else if (versiApendo.length() > 1) {
			boolean valid = true;
			String configVersi = versiApendo;
			int lenChek = 1000;
			byte[] bytes = new byte[lenChek];
			InputStream input = file;
			try {
				int read = input.read(bytes, 0, lenChek);
				if (read != lenChek) {
					valid = false;
					String message = messageSource.getMessage("invalid-apendo-file", new Object[] { configVersi });
					input.close();
					throw new Exception(message);
				}
				input.close();
			} catch (IOException e) {
				String message = "Error reading file: " + e.getMessage();
				input.close();
				throw new Exception(message);
			}

			// pengecekan #0 : validitas file rhs
			if (valid) {
				byte[] apendo = { 1, 16, 5, 14, 4, 15, 27 };
				for (int i = 0; i < apendo.length; i++) {
					if (bytes[i] != apendo[i]) {
						valid = false;
						break;
					}
				}
				if (!valid) {
					String message = messageSource.getMessage("invalid-apendo-file", new Object[] { configVersi });
					throw new Exception(message);
				}
			}

			// pengecekan #1 : versi apendo yang digunakan
			String fileVersi = "1.1";
			if (valid) {
				char[] byteFileVersi = new char[3];
				char[] convertVersi = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
				if ((int) bytes[7] < 10 && (int) bytes[8] == 28 && (int) bytes[9] < 10) {
					byteFileVersi[0] = convertVersi[(int) bytes[7]];
					byteFileVersi[1] = '.';
					byteFileVersi[2] = convertVersi[(int) bytes[9]];
					fileVersi = new String(byteFileVersi);
					if (!fileVersi.equals(configVersi)) {
						String message = messageSource.getMessage("invalid-apendo-versi",
								new Object[] { fileVersi, configVersi });
						throw new Exception(message);
					}
				} else {
					String message = messageSource.getMessage("invalid-apendo-file", new Object[] { configVersi });
					throw new Exception(message);
				}
			}

			// pengecekan #2 : id lelang & jenis dokumen
			if (isProduction) {
				if (valid) {
					int checkCount = 0;
					int index = 0;
					int[] indexCheck = new int[4];
					indexCheck[checkCount] = index;
					for (int i = 0; i < bytes.length; i++) {
						if (bytes[index] == '#' && bytes[index + 1] == 0x0C && bytes[index + 2] == 0x05
								&& bytes[index + 3] == 0x0D && bytes[index + 4] == 0x13 && bytes[index + 5] == 0x01
								&& bytes[index + 6] == 0x0E && bytes[index + 7] == 0x05 && bytes[index + 8] == 0x07
								&& bytes[index + 9] == '#') {
							checkCount++;
							indexCheck[checkCount] = index + 10;
							if (checkCount == 3)
								break;
						}
						index++;
					}
					if (checkCount == 3) {
						// byte[] check0 = new byte[indexCheck[1]-10];
						byte[] check1 = new byte[indexCheck[2] - 10 - indexCheck[1]];
						// byte[] check2 = new
						// byte[indexCheck[3]-10-indexCheck[2]];
						// for(int i=0;i<check0.length;i++)
						// check0[i]=bytes[i];
						for (int i = 0; i < check1.length; i++)
							check1[i] = (byte) (bytes[indexCheck[1] + i] + 0x10);
						// for(int i=0;i<check2.length;i++)
						// check2[i]=bytes[indexCheck[2]+i];

						String check = new String(check1);
						Logger.debug(check);
						String[] split = check.split("\n");
						String strIDLelang = "";
						String strNamaPaket = "";
						String strJenisDokumen = "";
						boolean kenek = false;
						if (!kenek) {
							for (int i = 0; i < split.length; i++) {
								Logger.debug("line " + i + " : " + split[i]);
								if (split[i].startsWith("Kode Tender")) {
									strIDLelang = split[i].replaceFirst("Kode Tender\t\t:   ", "").trim();
									if (!strIDLelang.startsWith("Kode Tender")) {
										kenek = true;
									}
								} else if (split[i].startsWith("Nama Paket")) {
									strNamaPaket = split[i].replaceFirst("Nama Paket\t:   ", "").trim();
								} else if (split[i].startsWith("Jenis Dokumen")) {
									strJenisDokumen = split[i].replaceFirst("Jenis Dokumen\t:   ", "").trim();
									strJenisDokumen = "Dokumen " + strJenisDokumen;
								}
							}
						}
						if (!kenek) {
							for (int i = 0; i < split.length; i++) {
								Logger.debug("line " + i + " : " + split[i]);
								if (split[i].startsWith("Kode Tender"))
									strIDLelang = split[i].replaceFirst("Kode Tender :", "").trim();
								else if (split[i].startsWith("Nama Paket"))
									strNamaPaket = split[i].replaceFirst("Nama Paket :", "").trim();
								else if (split[i].startsWith("Jenis Dokumen"))
									strJenisDokumen = split[i].replaceFirst("Jenis Dokumen :", "").trim();
							}
						}

						// jenisDokumenLabel aslinya jenis.getLabel()
						if (!strIDLelang.equals(lelangId.toString()) || !strJenisDokumen.equals(jenisDokumenLabel)) {
							String message = messageSource.getMessage("invalid-apendo-key",
									new Object[] { strJenisDokumen, strNamaPaket, strIDLelang, jenisDokumenLabel,
											namaPaket, lelangId.toString() });
							throw new Exception(message);
						} else {
							return true;
						}
					} else {
						String message = messageSource.getMessage("invalid-apendo-file", new Object[] { configVersi });
						throw new Exception(message);
					}
				}
			} else {
				return true;
			}
		}

		return true;
	}

	public void setMessageSource(Message messageSource) {
		this.messageSource = messageSource;
	}

	public void setVersiApendo(String versiApendo) {
		this.versiApendo = versiApendo;
	}

	public void setProduction(boolean isProduction) {
		this.isProduction = isProduction;
	}

	public void setVersiSpamkodok(String versiSpamkodok) {
		this.versiSpamkodok = versiSpamkodok;
	}

}
