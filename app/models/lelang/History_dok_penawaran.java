package models.lelang;

import java.util.Date;
import java.util.List;

import javax.persistence.Transient;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import play.db.jdbc.Id;
import play.db.jdbc.Table;


@Table(name="HISTORY_DOK_PENAWARAN")
public class History_dok_penawaran extends BaseModel{
	
	@Id
	public Long dok_id;

	public String dok_judul;

	public Date dok_tgljam;

	public Date dok_waktu_buka;

	public String dok_hash;

	public String dok_signature;

	public Long dok_id_attachment;
	
	public Integer dok_waktu; // jangka waktu penawaran
	
	public String dok_surat; // perlu evaluasi, apakah menyimpan surat penawaran dengan cara ini adalah yang terbaik
	
	public JenisDokPenawaran dok_jenis;

	public Integer dok_disclaim;

	//relasi kek Peserta
	public Long psr_id;	

	public Long dok_struk_id;

	public String dok_struk_hash;

	public String dok_struk_nama;

	@Transient
	private Peserta peserta;
	
	// simpan history dok_penawaran. dilakukan saat action pemasukan penawaran ulang
	public static void simpanHistory(Dok_penawaran dok_penawaran) {
		if(dok_penawaran == null)
			return;
		History_dok_penawaran hisDokPenawaran = new History_dok_penawaran();
		hisDokPenawaran.dok_id = dok_penawaran.dok_id;
		hisDokPenawaran.dok_judul = dok_penawaran.dok_judul;
		hisDokPenawaran.dok_tgljam = dok_penawaran.dok_tgljam;
		hisDokPenawaran.dok_waktu_buka = dok_penawaran.dok_waktu_buka;
		hisDokPenawaran.dok_hash = dok_penawaran.dok_hash;
		hisDokPenawaran.dok_signature = dok_penawaran.dok_signature;
		hisDokPenawaran.dok_id_attachment = dok_penawaran.dok_id_attachment;
		hisDokPenawaran.dok_waktu = dok_penawaran.dok_waktu;
		hisDokPenawaran.dok_surat = dok_penawaran.dok_surat;
		hisDokPenawaran.dok_jenis = dok_penawaran.dok_jenis;
		hisDokPenawaran.dok_disclaim = dok_penawaran.dok_disclaim;
		hisDokPenawaran.psr_id = dok_penawaran.psr_id;
		hisDokPenawaran.dok_struk_id = dok_penawaran.dok_struk_id;
		hisDokPenawaran.dok_struk_hash = dok_penawaran.dok_struk_hash;
		hisDokPenawaran.dok_struk_nama = dok_penawaran.dok_struk_nama;
		hisDokPenawaran.audittype = dok_penawaran.audittype;
		hisDokPenawaran.auditupdate = dok_penawaran.auditupdate;
		hisDokPenawaran.audituser = dok_penawaran.audituser;
		hisDokPenawaran.save();
	}

	/**
	 * digunakan untuk menampilkan dokumen kualifikasi (hanya untuk penawaran)
	 * @return
	 */
	@Transient
	public BlobTable getDokumen() {
		if(dok_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dok_id_attachment);
	}

	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}

	public static List<History_dok_penawaran> findAllPenawaranTeknis(Long lelangId)
	{
		return find("psr_id in (select psr_id from peserta where lls_id=?) and dok_jenis='1' and dok_disclaim='1' order by psr_id", lelangId).fetch();
	}

	public static List<History_dok_penawaran> findAllPenawaranHarga(Long lelangId)
	{
		return find("psr_id in (select psr_id from peserta where lls_id=?) and dok_jenis='2' and dok_disclaim='1' order by psr_id", lelangId).fetch();
	}

	public static List<History_dok_penawaran> findPenawaranTeknisPeserta(Long pesertaId) {
		return find("psr_id=? and dok_jenis='1' and dok_disclaim='1'", pesertaId).fetch();
	}

	public static List<History_dok_penawaran> findPenawaranHargaPeserta(Long pesertaId) {
		return find("psr_id=? and dok_jenis='2' and dok_disclaim='1'", pesertaId).fetch();
	}
}
