package models.lelang;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master.ChecklistStatus;
import models.lelang.Checklist_master.JenisChecklist;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Table(name="CHECKLIST_GAGAL")
public class Checklist_gagal extends BaseModel {

	@Id(sequence="seq_checklist_gagal_pra", function="nextsequence")
	public Long cks_id;	
	
	//relasi ke lelang seleksi
	public Long lls_id;

	//relasi ke Checklist_master
	public Integer ckm_id;

	public Integer cks_versi = Integer.valueOf(1);
	
	@Transient
	private Lelang_seleksi lelang;
	
	@Transient
	private Checklist_master checklist_master;	
	
	public Lelang_seleksi getLelang() {
		if(lelang == null)
			lelang = Lelang_seleksi.findById(lls_id);
		return lelang;
	}

	public Checklist_master getChecklist_master() {
		if(checklist_master == null)
			checklist_master = Checklist_master.findById(ckm_id);
		return checklist_master;
	}

	public boolean isChecked() {
		return this.cks_id != null;
	}
	
	public static List<Checklist_gagal> getChecklist(JenisChecklist jenis)
	{		
		List<Checklist_gagal> checklistGagal = new ArrayList<Checklist_gagal>();
		Checklist_gagal checklist = null;
		Integer versiMaster = findCurrentVersi(JenisChecklist.CHECKLIST_PRAKUALFIKASI_BATAL);
		List<Checklist_master> masterList = new ArrayList<Checklist_master>();	
		masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and ckm_versi=? order by ckm_checked desc, ckm_urutan asc, ckm_id asc", jenis, ChecklistStatus.AKTIF, versiMaster).fetch();
		for (Checklist_master checklist_master : masterList) {
		    checklist = new Checklist_gagal();
            checklist.ckm_id = checklist_master.ckm_id;
            if(checklist_master.ckm_checked == 1) {
				checklist.save();
			}
            checklistGagal.add(checklist);
		}		
		return checklistGagal;
	}
	
	public static void simpanChecklist(Lelang_seleksi lelang, List<Checklist_gagal> checklist, JenisChecklist jenis, Integer versi) {
		List<Long> saveCheck = new ArrayList<Long>();
		Checklist_gagal chkExist = null;
		for(Checklist_gagal obj: checklist) {
			if(obj.ckm_id != null) {
				if(obj.cks_id == null) {
					obj.lls_id = lelang.lls_id;
					obj.cks_versi = versi;
					obj.save();
					saveCheck.add(obj.cks_id);
				}
				else {
					chkExist = Checklist_gagal.findById(obj.cks_id);
					chkExist.lls_id = lelang.lls_id;
					chkExist.ckm_id = obj.ckm_id;
					chkExist.cks_versi = versi;
					chkExist.save();
					saveCheck.add(chkExist.cks_id);
				}
			}			
		}

		String sql="delete from checklist_gagal where lls_id=? AND cks_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)";
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			sql += " and cks_id not in ("+join+ ')';
		}
		Query.update(sql, lelang.lls_id, versi, jenis, ChecklistStatus.AKTIF);
	}
		
	public static Integer findCurrentVersi(JenisChecklist jenis_checklist) {
	  Integer versi = Query.find("SELECT max(ckm_versi) FROM checklist_master WHERE ckm_jenis=?", Integer.class, jenis_checklist.id).first();			
	  return versi != null ? versi : Integer.valueOf(1);
	}

	public static List<Checklist_gagal> findBy(Long lelangId, JenisChecklist jenis) {
		return find("lls_id=? AND ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)", lelangId, jenis, ChecklistStatus.AKTIF).fetch();
	}

}

