package models.lelang;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import ext.DecimalBinder;
import ext.FormatUtils;
import ext.SbdRegexBinder;
import play.data.binding.As;
import play.data.validation.Required;
import play.i18n.Messages;

/**
 * model content dari dokumen Lembar Data Pemilihan (LDP)
 * @author arief
 *
 */
public class LDPContent {

	@As(binder = SbdRegexBinder.class)
	public String lingkup_pekerjaan;
	@As(binder = SbdRegexBinder.class)
	public String jangka_waktu_penyelesaian;
	public String keterangan_jangka_waktu_penyelesaian = Messages.get("lelang.hari_kalender"); // hari kalender atau hari kerja
	@As(binder = SbdRegexBinder.class)
	public String peninjauan_lapangan;
	
	@Required
	@As(binder = SbdRegexBinder.class)
	public String matauang = "Rupiah";

	@As(binder = SbdRegexBinder.class)
	public String cara_pembayaran; // hanya untu lelang express
	
	//added by chitra
	public Integer kontrak_pembayaran;
	
	/*barang*/
	public Integer garansi;
	
	/*konstruksi dan jasalainnya*/
	@As(binder=DecimalBinder.class)
	public Double masa_berlaku_jaminan_pemeliharaan;
	@As(binder = SbdRegexBinder.class)
	public String penerima_jaminan_pemeliharaan;
	@As(binder = SbdRegexBinder.class)
	public String pencairan_jaminana_pemeliharaan;
	
	/*konsultansi perorangan dan badan usaha*/
	@As(binder=DecimalBinder.class)
	public Double unsur_pendekatan = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_pemahaman = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_kualitas = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_hasil_kerja = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_fasilitas_pendukung = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_gagasan_baru = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_lain_pendekatan = Double.valueOf(0.0);
	//ganti ke string
	//@As(binder = SbdRegexBinder.class)
	//public String nilai_pendekatan;
	//@As(binder=DecimalBinder.class)
	//public Double nilai_pendekatan = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double unsur_kualifikasi = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_pendidikan = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_pengalaman_kerja = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_sertifikat_keahlian = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_lain_kualifikasi = Double.valueOf(0.0);
	// ganti ke string
	//@As(binder = SbdRegexBinder.class)
	//public String nilai_kualifikasi;
	//@As(binder=DecimalBinder.class)
	//public Double nilai_kualifikasi = Double.valueOf(0.0);
	//@As(binder=DecimalBinder.class)
	//public Double nilai_evaluasi_teknis = Double.valueOf(0.0);
	//@As(binder = SbdRegexBinder.class)
	//public String nilai_evaluasi_teknis;
	@As(binder=DecimalBinder.class)
	public Double ambang_batas_teknis = Double.valueOf(0.0);
	@As(binder = SbdRegexBinder.class)
	public String waktu_evaluasi_biaya;
	
	@As(binder=DecimalBinder.class)
	public Double biaya_personil_bulan = Double.valueOf(0.0);
	public String getBiaya_personil_bulan_string(){
		return FormatUtils.number2Word(biaya_personil_bulan);
		//return biaya_personil_bulan.toString();
	}
	
	@As(binder=DecimalBinder.class)
	public Double biaya_personil_hari = Double.valueOf(0.0);
	public String getBiaya_personil_hari_string(){
		return FormatUtils.number2Word(biaya_personil_hari);
		//return biaya_personil_hari.toString();
	}
	
	/*konsultansi badan usaha*/
	@As(binder=DecimalBinder.class)
	public Double unsur_pengalaman_perusahaan = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_pengalaman_sejenis = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_pengalaman_selokasi = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_pengalaman_manajerial = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_kapasitas_perusahaan = Double.valueOf(0.0);
	@As(binder=DecimalBinder.class)
	public Double subunsur_lain_pengalaman = Double.valueOf(0.0);
	//ganti ke String
	//public String nilai_pengalaman;
	//@As(binder=DecimalBinder.class)
	//public Double nilai_pengalaman = Double.valueOf(0.0);
	
	
	@Required
	public Integer masa_berlaku_penawaran = Integer.valueOf(30);
	
	public Integer jangka_waktu_pelaksanaan = Integer.valueOf(0);
	@As(binder = SbdRegexBinder.class)
	public String daftar_personil;
	@As(binder = SbdRegexBinder.class)
	public String peralatan_utama;
	@As(binder = SbdRegexBinder.class)
	public String bagian_subkontrak;
	@As(binder = SbdRegexBinder.class)
	public String uji_mutu_bahan;
	@As(binder = SbdRegexBinder.class)
	public String uji_mutu_alat;
	@As(binder = SbdRegexBinder.class)
	public String uji_mutu_material;
	
	//edit by chitra untuk kategori barang, jl dan konstruksi
	/*@As(binder=DecimalBinder.class)
	public Double total_sistem_gugur = Double.valueOf(0);
	public String unsur_sistem_gugur;
	
	@As(binder=DecimalBinder.class)
	public Double ambang_batas_sistemnilai = Double.valueOf(0);
	public String unsur_sistem_nilai;
	@As(binder=DecimalBinder.class)
	public Double total_sistem_nilai = Double.valueOf(0);
	
	public Integer umur_ekonomis = Integer.valueOf(0);*/
	
	@As(binder=DecimalBinder.class)
	public Double ambang_batas;
	@As(binder=DecimalBinder.class)
	public Double bobot_teknis;
	@As(binder=DecimalBinder.class)
	public Double bobot_harga;

	@As(binder = SbdRegexBinder.class)
	public String sanggahan_tembusan_ppk;
	@As(binder = SbdRegexBinder.class)
	public String sanggahan_tembusan_pa;
	@As(binder = SbdRegexBinder.class)
	public String sanggahan_tembusan_apip;

	@As(binder = SbdRegexBinder.class)
	public String pengaduan_ke;
	@As(binder = SbdRegexBinder.class)
	public String jaminan_sanggahan_banding;
	@As(binder = SbdRegexBinder.class)
	public String jaminan_sanggahan_banding_kas; // Kas Negara / Kas Daerah
	
	public Integer jaminan_pelaksanaan_masa = Integer.valueOf(0);
	@As(binder = SbdRegexBinder.class)
	public String jaminan_pelaksanaan_ke;
	@As(binder = SbdRegexBinder.class)
	public String jaminan_pelaksanaan_kas;
	
	@As(binder=DecimalBinder.class)
	public Double jaminan_uang_muka_nilai = Double.valueOf(0.0);
	@As(binder = SbdRegexBinder.class)
	public String jaminan_uang_muka_ke;
	@As(binder = SbdRegexBinder.class)
	public String jaminan_uang_muka_kas;

	public String ketentuan_lain;

	//Di simpan dalam format JSON
	@As(binder = SbdRegexBinder.class)
	public JsonArray bobotTenagaAhli;
	
	public String evaluasiTeknis;
	
	//tambahan untuk Pengadaan Langsung
	public String dok_penawaran;
	public String syarat_penyedia;
	//konsultansi
	public String batas_waktu_pelaksanaan;


	//parse json ke array
	public String[][] parseTenagaAhliToArray(){
		String[][] tenagaAhli=null;
		tenagaAhli = new String[bobotTenagaAhli.size()][2];
		for (int i = 0; i < bobotTenagaAhli.size(); i++) {
			JsonObject jsonObject = (JsonObject) bobotTenagaAhli.get(i);
			tenagaAhli[i][0] = jsonObject.get("profesi").toString();
			tenagaAhli[i][1] = jsonObject.get("bobot").toString();
		}
		return tenagaAhli;
	}

}
