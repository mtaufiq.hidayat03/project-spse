package models.lelang;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

@Table(name = "history_persetujuan")
public class History_persetujuan extends BaseModel {

    @Id
    public Long pst_id;

    //relasi ke lelang_selesai
    public Long lls_id;

    //relasi ke Pegawai
    public Long peg_id;

    /*
     * jenis persetujuan memakai enum JenisPersetujuan
     * 0 : pengumuman lelang
     * 1 : pengumuman pemenang
     */
    public Persetujuan.JenisPersetujuan pst_jenis;

    public Date pst_tgl_setuju;

    public Persetujuan.StatusPersetujuan pst_status;

    public String pst_alasan;

    public static void simpanHistory(Persetujuan persetujuan) {
        if(persetujuan != null) {
            History_persetujuan obj = new History_persetujuan();
            obj.pst_id = persetujuan.pst_id;
            obj.lls_id = persetujuan.lls_id;
            obj.peg_id = persetujuan.peg_id;
            obj.pst_jenis = persetujuan.pst_jenis;
            obj.pst_tgl_setuju  = persetujuan.pst_tgl_setuju;
            obj.pst_status = persetujuan.pst_status;
            obj.pst_alasan = persetujuan.pst_alasan;
            obj.audituser = persetujuan.audituser;
            obj.audittype = persetujuan.audittype;
            obj.auditupdate = persetujuan.auditupdate;
            obj.save();
        }
    }
}
