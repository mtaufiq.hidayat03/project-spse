package models.lelang;

import models.agency.Pegawai;
import models.jcommon.db.base.BaseModel;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.secman.Usergroup;
import org.joda.time.DateTime;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.UUID;


@Table(name="LELANG_TOKEN")
public class Lelang_token extends BaseModel {

	@Id(sequence="seq_lelang_token", function="nextsequence")
	public Long llt_id;
	
	//relasi ke Lelang_seleksi
	public Long lls_id;

	//relasi ke Peserta
	public Long psr_id;
	
	//relasi ke Pegawai
	public Long peg_id;
	
	public String llt_host;
	/**
	 * informasi key token
	 * pake system.UUID
	 */
	public String llt_key;
	
	public Date llt_expired;
	
	public JenisDokPenawaran llt_jenis; // reusing jenis from JenisDokPenawaran, but only using 1,2 and 3 
	
	@Transient
	public boolean isExpire() {
		if(llt_expired != null) {
			Date now = controllers.BasicCtr.newDate();
			return now.after(llt_expired);
		}	
		return false;
	}
	
	/**
	 * dapatkan token untuk peserta
	 * @param peserta
	 * @return
	 */
	public static String findNGeneratePeserta(Long pesertaId, Long lelangId, String host, JenisDokPenawaran jenis) {
		Lelang_token lelang_token = Lelang_token.find("psr_id=? and lls_id=? and llt_jenis=?", pesertaId, lelangId, jenis).first();
		if(lelang_token != null && lelang_token.isExpire()) {
			Lelang_token.delete("psr_id=? and lls_id=? and llt_jenis=?", pesertaId, lelangId, jenis);
			lelang_token = null;
		}
		if(lelang_token == null) {
			lelang_token = new Lelang_token();
			lelang_token.lls_id = lelangId;
			lelang_token.psr_id = pesertaId;	
			lelang_token.llt_host = host;
			lelang_token.llt_key = UUID.randomUUID().toString();
			DateTime now = new DateTime(controllers.BasicCtr.newDate().getTime());
			lelang_token.llt_expired = now.plusDays(1).toDate(); // setting expire 1 hari
			lelang_token.llt_jenis = jenis;
			lelang_token.save();	
		} 
		return lelang_token.llt_key;
	}
	
	/**
	 * dapatkan token untuk pegawai
	 * @param peserta
	 * @return
	 */
	public static String findNGeneratePegawai(Long pegawaiId, Long lelangId, String host, JenisDokPenawaran jenis) {
		Lelang_token lelang_token;
//		if(pegawaiId == 0){
//			lelang_token = Lelang_token.find("peg_id IS NOT NULL and lls_id=?", lelangId).first();
//		}else{
			lelang_token = Lelang_token.find("peg_id=? and lls_id=? and llt_jenis=?", pegawaiId, lelangId, jenis).first();
//		}
		if(lelang_token != null && lelang_token.isExpire()) {
			Lelang_token.delete("peg_id=? and lls_id=? and llt_jenis=?", pegawaiId, lelangId, jenis);
			lelang_token = null;
		}
		if(lelang_token == null) {
			lelang_token = new Lelang_token();
			lelang_token.lls_id = lelangId;
//			if(pegawaiId != 0)
				lelang_token.peg_id = pegawaiId;	
			lelang_token.llt_host = host;
			lelang_token.llt_key = UUID.randomUUID().toString();
			DateTime now = new DateTime(controllers.BasicCtr.newDate().getTime());
			lelang_token.llt_expired = now.plusDays(1).toDate(); // setting expire 1 hari
			lelang_token.llt_jenis = jenis;
			lelang_token.save();
		}
		return lelang_token.llt_key;
	}
	
	public boolean isPanitia(){
		if(peg_id==null)
			return false;
		Pegawai pegawai= Pegawai.findById(peg_id);
		
		return Usergroup.findByUser(pegawai.peg_namauser).idgroup.isPanitia();
	}
	
}
