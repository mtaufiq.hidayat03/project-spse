package models.lelang;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import ext.SbdRegexBinder;
import models.common.Kategori;
import models.common.KeyLabel;
import models.common.Kualifikasi;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master.ChecklistStatus;
import models.lelang.Checklist_master.JenisChecklist;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import play.Logger;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Scope;
import utils.JsonUtil;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Table(name="CHECKLIST_TMP")
public class Checklist_tmp extends BaseModel {

	@Id(sequence="seq_checklist_tmp", function="nextsequence")
	public Long chk_id;	
	
	//relasi ke Dok_lelang
	public Long dll_id;

	//relasi ke Checklist_master
	public Integer ckm_id;

	@As(binder = SbdRegexBinder.class)
	public String chk_nama;

	@As(binder = SbdRegexBinder.class)
	public String chk_klasifikasi;
	
	@Transient
	private Dok_lelang dok_lelang;
	@Transient
	private Checklist_master checklist_master;	
	
	public Dok_lelang getDok_lelang() {
		if(dok_lelang == null)
			dok_lelang = Dok_lelang.findById(dll_id);
		return dok_lelang;
	}

	public Checklist_master getChecklist_master() {
		if(checklist_master == null)
			checklist_master = Checklist_master.findById(ckm_id);
		return checklist_master;
	}

	/**
	 * This method is used to check whether is it valid percentage range 10-100% from chk_name
	 * @return boolean*/
	public boolean isValidForPercentage() {
		if (!StringUtils.isEmpty(chk_nama) && NumberUtils.isNumber(chk_nama)) {
			final Integer i = Integer.valueOf(chk_nama);
			return i >= 10 && i <= 100;
		}
		return false;
	}

	public boolean isBankSupportSpec() {
		return this.ckm_id != null && this.ckm_id.equals(2);
	}
	
	public boolean isChecked() {
		return this.chk_id != null;
	}
	
	public static boolean isSyaratKualifikasiBaru(Dok_lelang dok_lelang) {
		String jenis = StringUtils.join(JenisChecklist.CHECKLIST_KUALIFIKASI_BARU, ',');
	 	Checklist_tmp checklist = find("dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=0)", dok_lelang.dll_id).first();
		Checklist_tmp checklistBaru = find("dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", dok_lelang.dll_id).first();
		if (checklist == null && checklistBaru == null) {
			return true;
		} else {
			if (checklistBaru != null) return true;
			else return false;
		}
	}
	
	public static List<Checklist_tmp> getListIjinUsaha(Dok_lelang dok_lelang) {
		if(dok_lelang == null)
			return null;
		return find("ckm_id=1 and dll_id=? order by chk_id", dok_lelang.dll_id).fetch();
	}
	
	public static List<Checklist_tmp> getListIjinUsahaBaru(Dok_lelang dok_lelang) {
		if(dok_lelang == null)
			return null;
		return find("ckm_id=50 and dll_id=? order by chk_id", dok_lelang.dll_id).fetch();
	}
	
	public static List<Checklist_tmp> getListIjinUsahaBaruFlash(Dok_lelang dok_lelang, List<Checklist_tmp> ijinList) {
		if(dok_lelang == null)
			return null;
		List<Checklist_tmp> syarat = new ArrayList<Checklist_tmp>();
       	Checklist_master ijin_master = Checklist_master.findById(50);
		Checklist_tmp chkExist = null;
		if(!CommonUtil.isEmpty(ijinList)) {
            for (Checklist_tmp obj : ijinList) {
            		if (obj != null) {
					obj.ckm_id = ijin_master.ckm_id;
					obj.dll_id = dok_lelang.dll_id;
					if (obj.chk_id != null) 
						chkExist = Checklist_tmp.findById(obj.chk_id);
					if (chkExist == null)
						obj.chk_id = null;
					syarat.add(obj);
				}
            }
        }
		return syarat;
	}
	
	public static List<Checklist_tmp> getListSyaratKualifikasi(Dok_lelang dok_lelang, Kategori kategori) {
		List<Checklist_tmp> syaratList = new ArrayList<Checklist_tmp>();
       	List<Checklist_master> masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and (kgr_id=? or kgr_id is null) and ckm_id <> 1 order by ckm_checked desc, ckm_urutan asc, ckm_id asc",JenisChecklist.CHECKLIST_KUALIFIKASI, ChecklistStatus.AKTIF, kategori).fetch();
		Checklist_tmp checklist = null;
	    for (Checklist_master checklist_master : masterList) {
            if(dok_lelang == null) {
                checklist = new Checklist_tmp();
                checklist.ckm_id = checklist_master.ckm_id;
                syaratList.add(checklist);
            }
			else if(checklist_master.isSyaratLain()) {
					List<Checklist_tmp> syaratLain = find("ckm_id=? and dll_id=? order by chk_id", checklist_master.ckm_id, dok_lelang.dll_id).fetch();
					syaratList.addAll(syaratLain);
			}
			else {
				checklist = find("ckm_id=? and dll_id=?", checklist_master.ckm_id, dok_lelang.dll_id).first();
				if(checklist == null) {
						checklist = new Checklist_tmp();
						checklist.ckm_id = checklist_master.ckm_id;
						checklist.dll_id = dok_lelang.dll_id;					
				}
				syaratList.add(checklist);
			}
		}
		return syaratList;
	}
	
	public static List<Checklist_tmp> getListSyaratKualifikasi(Dok_lelang dok_lelang, Kategori kategori, JenisChecklist jenis, Kualifikasi kualifikasi) {
		List<Checklist_tmp> syaratList = new ArrayList<Checklist_tmp>();
       	Checklist_tmp checklist = null;
       	Integer versiMaster = findCurrentVersi(dok_lelang.dll_id, JenisChecklist.CHECKLIST_LDK, kategori);
       	List<Checklist_master> masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and (kgr_id=? or kgr_id is null) and ckm_id <> 50 and (ckm_versi=? or ckm_versi=0) order by ckm_checked desc, ckm_urutan asc, ckm_id asc", jenis, ChecklistStatus.AKTIF, kategori, versiMaster).fetch();
	    for (Checklist_master checklist_master : masterList) {
	        if(dok_lelang == null) {
                checklist = new Checklist_tmp();
                checklist.ckm_id = checklist_master.ckm_id;
                syaratList.add(checklist);
            } else if(checklist_master.isSyaratLain()) {
					List<Checklist_tmp> syaratLain = find("ckm_id=? and dll_id=? order by chk_id", checklist_master.ckm_id, dok_lelang.dll_id).fetch();
					syaratList.addAll(syaratLain);
			} else {
				checklist = find("ckm_id=? and dll_id=?", checklist_master.ckm_id, dok_lelang.dll_id).first();
				if(checklist == null && checklist_master.ckm_id != 123) {
						checklist = new Checklist_tmp();
						checklist.ckm_id = checklist_master.ckm_id;
						checklist.dll_id = dok_lelang.dll_id;					
				}
				if(checklist != null)
					syaratList.add(checklist);
			}
		}
		return syaratList;
	}
	
	public static List<Checklist_tmp> getListSyaratKualifikasiFlash(Dok_lelang dok_lelang, List<Checklist_tmp> syaratList, JenisChecklist jenis, Scope.Params params, Kualifikasi kualifikasi) {
		List<Checklist_tmp> syarat = new ArrayList<Checklist_tmp>();
		if(!CommonUtil.isEmpty(syaratList)) {
       		int index = 0;
       		for (Checklist_tmp obj : syaratList) {
       			String ckm_id_index = params.get(jenis.name().toLowerCase() + "_ckm_id[" + index + "]");
        			Integer ckm_id = ckm_id_index != null ? Integer.valueOf(ckm_id_index) : null;
        			if (ckm_id != null) {
                		Checklist_master cm = Checklist_master.findById(ckm_id);
                		if (obj.ckm_id == null) {
                    		obj.ckm_id = cm.ckm_id;
                    		if (obj.chk_id != null)
                    			obj.chk_klasifikasi = "unchecked";
                    } else {
                    		obj.chk_klasifikasi = "checked";
                    	}
                		if (cm.isNotEditable()) {
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.setTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					}
                		syarat.add(obj);
                }
        			index++;
            }
        }
       	return syarat;		
	}
	
	/**
	 * get syarat teknis 
	 * untuk kepentingan insert/update data lelang via form
	 * @param dok_lelang
	 * @return
	 */
	public static List<Checklist_tmp> getListSyarat(Dok_lelang dok_lelang, JenisChecklist jenis, Kategori kategori) 
	{		
		List<Checklist_tmp> syaratList = new ArrayList<Checklist_tmp>();
       	Checklist_tmp checklist = null;
		Integer versiMaster = findCurrentVersi(dok_lelang.dll_id, JenisChecklist.CHECKLIST_SYARAT, kategori);
		if (kategori.isKonsultansiPerorangan() && jenis == JenisChecklist.CHECKLIST_TEKNIS && versiMaster.intValue() == 1) kategori = Kategori.KONSULTANSI;
		List<Checklist_master> masterList = new ArrayList<Checklist_master>();
		if (!kategori.isKonstruksi() && jenis == JenisChecklist.CHECKLIST_ADMINISTRASI && versiMaster.intValue() == 1)
			masterList = Checklist_master.find("ckm_id in (16, 18) and ckm_status=? order by ckm_checked desc, ckm_urutan asc, ckm_id asc", ChecklistStatus.AKTIF).fetch();
		else
			masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and (kgr_id=? or kgr_id is null) and (ckm_versi=? or ckm_versi=0) order by ckm_checked desc, ckm_urutan asc, ckm_id asc", jenis, ChecklistStatus.AKTIF, kategori, versiMaster).fetch();
		for (Checklist_master checklist_master : masterList) {
            if(dok_lelang == null) {
                checklist = new Checklist_tmp();
                checklist.ckm_id = checklist_master.ckm_id;
                checklist.chk_nama = checklist_master.ckm_nama;
//                if(checklist_master.ckm_checked == 1) {
//					checklist.save();
//				}
                syaratList.add(checklist);
            } else if(checklist_master.isSyaratLain()) {
				List<Checklist_tmp> syaratLain = find("ckm_id=? and dll_id=? order by chk_id", checklist_master.ckm_id, dok_lelang.dll_id).fetch();
				syaratList.addAll(syaratLain);
			} else {
				checklist = find("ckm_id=? and dll_id=?", checklist_master.ckm_id, dok_lelang.dll_id).first();
				if(checklist == null) {
					checklist = new Checklist_tmp();
					checklist.ckm_id = checklist_master.ckm_id;
					checklist.dll_id = dok_lelang.dll_id;	
					checklist.chk_nama = checklist_master.ckm_nama;
//					if(checklist_master.ckm_checked == 1) {
//						checklist.save();
//					}
				}
				syaratList.add(checklist);
			}
		}		
		return syaratList;
	}
	
	public static String getInfoSyaratTeknis(Dok_lelang dok_lelang) {
		String result="belum ada data checklist";
		long count = count("dll_id=? and ckm_id in (select ckm_id from Checklist_master where ckm_jenis=? and ckm_status=?)", dok_lelang.dll_id, JenisChecklist.CHECKLIST_TEKNIS, ChecklistStatus.AKTIF);
		if(count > 0)
			result = count +" checklist";
		return result;
	}
	
	public static String getInfoSyaratKualifikasi(Dok_lelang dok_lelang) {
		String result="belum ada data checklist";
		long count = count("dll_id=? and ckm_id in (select ckm_id from Checklist_master where ckm_jenis=? and ckm_status=?)", dok_lelang.dll_id, JenisChecklist.CHECKLIST_KUALIFIKASI, ChecklistStatus.AKTIF);
		if(count > 0)
			result = count +" checklist";
		return result;
	}
	
	public static void simpanChecklist(Dok_lelang dok_lelang, List<Checklist_tmp> ijinList, List<Checklist_tmp> syarat, Scope.Params params, Kualifikasi kualifikasi) throws Exception {
		Checklist_master ijin_master = Checklist_master.findById(1);
		List<Long> saveCheck = new ArrayList<Long>();
		Checklist_tmp chkExist = null;
        if(!CommonUtil.isEmpty(ijinList)) {
            for (Checklist_tmp obj : ijinList) { // untuk izin usaha
				if (obj != null) {
					obj.ckm_id = ijin_master.ckm_id;
					if (CommonUtil.isEmpty(obj.chk_nama)) {
						throw new Exception(Messages.get("lelang.izin_usaha_req"));
					}
					if (obj.chk_id == null) {
						obj.dll_id = dok_lelang.dll_id;
						obj.save();
						saveCheck.add(obj.chk_id);
					} else {
						chkExist = Checklist_tmp.findById(obj.chk_id);
						chkExist.dll_id = dok_lelang.dll_id;
						chkExist.chk_nama = obj.chk_nama;
						chkExist.chk_klasifikasi = obj.chk_klasifikasi;
						chkExist.save();
						saveCheck.add(chkExist.chk_id);
					}
				}
            }
        }
        if(!CommonUtil.isEmpty(syarat)) {
            for (Checklist_tmp obj : syarat) {
                if (obj.ckm_id != null) {
                	Checklist_master cm = Checklist_master.findById(obj.ckm_id);
                    if (cm.isNotEditable()){
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.validateTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					} else {
	                    if (CommonUtil.isEmpty(obj.chk_nama) && !cm.isChecked()) { //deskripsi wajib diisi
		                    throw new Exception("\"" + cm.ckm_nama + "\" wajib diisi jika diceklist (dipersyaratkan)!");
	                    }
	                    if(obj.isBankSupportSpec()){
	                    	obj.chk_nama = obj.chk_nama + "%";
						}
                    }
                    if (obj.chk_id == null) {
                        obj.dll_id = dok_lelang.dll_id;
                        obj.save();
                        saveCheck.add(obj.chk_id);
                    } else {
                        chkExist = Checklist_tmp.findById(obj.chk_id);
						chkExist.dll_id = dok_lelang.dll_id;
                        chkExist.ckm_id = obj.ckm_id;
                        chkExist.chk_nama = obj.chk_nama;
                        chkExist.save();
                        saveCheck.add(chkExist.chk_id);
                    }
                }
            }
        }
		// hapus list lain yang tidak dipilih oleh user
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			String sql="delete from checklist_tmp where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+ ')';
			Query.update(sql, dok_lelang.dll_id, JenisChecklist.CHECKLIST_KUALIFIKASI, ChecklistStatus.AKTIF);
		}
	}
	
	public static void simpanChecklist(Dok_lelang dok_lelang, List<Checklist_tmp> ijinList, List<Checklist_tmp> syaratList, JenisChecklist jenis, Scope.Params params, Kualifikasi kualifikasi) throws Exception {
		List<Checklist_tmp> syarat = new ArrayList<Checklist_tmp>();
		Checklist_master ijin_master = Checklist_master.findById(50);
		List<Long> saveCheck = new ArrayList<Long>();
		Checklist_tmp chkExist = null;
		if(!CommonUtil.isEmpty(ijinList)) {
            for (Checklist_tmp obj : ijinList) { 
            		if (obj != null) {
            			obj.ckm_id = ijin_master.ckm_id;
					if (CommonUtil.isEmpty(obj.chk_nama))
						throw new Exception(Messages.get("lelang.izin_usaha_req"));
					if (obj.chk_id == null) {
						obj.dll_id = dok_lelang.dll_id;
						obj.save();
						saveCheck.add(obj.chk_id);
					} else {
						chkExist = Checklist_tmp.findById(obj.chk_id);
						if (chkExist != null) {
							chkExist.dll_id = dok_lelang.dll_id;
							chkExist.chk_nama = obj.chk_nama;
							chkExist.chk_klasifikasi = obj.chk_klasifikasi;
							chkExist.save();
	                        	saveCheck.add(chkExist.chk_id);
						} else {
							obj.chk_id = null;
							obj.dll_id = dok_lelang.dll_id;
							obj.save();
	                        	saveCheck.add(obj.chk_id);
						}						
					}
				}
            }
        }
		
		if(!CommonUtil.isEmpty(syaratList)) {
            for (Checklist_tmp obj : syaratList) {
            	    if (obj != null && obj.ckm_id != null) {
                		Checklist_master cm = Checklist_master.findById(obj.ckm_id);
                    if (cm.isNotEditable()){
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.validateTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					} else {
						if (CommonUtil.isEmpty(obj.chk_nama)) { //deskripsi wajib diisi
	                    		throw new Exception("\"" + cm.ckm_nama + "\" wajib diisi jika diceklist (dipersyaratkan)!");
	                    }
	                    if(obj.isBankSupportSpec()){
	                    		obj.chk_nama = obj.chk_nama + "%";
						}
                    }
                    if (obj.chk_id == null) {
                        obj.dll_id = dok_lelang.dll_id;
                        obj.save();
                        saveCheck.add(obj.chk_id);
                    } else {
                        chkExist = Checklist_tmp.findById(obj.chk_id);
                        if (chkExist != null) {
	                        chkExist.dll_id = dok_lelang.dll_id;
	                        chkExist.ckm_id = obj.ckm_id;
	                        chkExist.chk_nama = obj.chk_nama;
	                        chkExist.save();
	                        saveCheck.add(chkExist.chk_id);
                        } else {
	                        	obj.chk_id = null;
	    						obj.dll_id = dok_lelang.dll_id;
	    						obj.save();
	    						saveCheck.add(obj.chk_id);
                        }
                    }
                }
            }
        }
		
		// hapus list lain yang tidak dipilih oleh user
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			String sql="delete from checklist_tmp where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+ ')';
			Query.update(sql, dok_lelang.dll_id, jenis, ChecklistStatus.AKTIF);
		}
	}

	public static void simpanChecklist(Dok_lelang dok_lelang, List<Checklist_tmp> syaratList, JenisChecklist jenis, Scope.Params params, Kualifikasi kualifikasi) throws Exception {
		List<Long> saveCheck = new ArrayList<Long>();
		Checklist_tmp chkExist = null;
		if(!CommonUtil.isEmpty(syaratList)) {
        		for (Checklist_tmp obj : syaratList) {
            		if (obj != null && obj.ckm_id != null) {
                		Checklist_master cm = Checklist_master.findById(obj.ckm_id);
                    if (cm.isNotEditable()){
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.validateTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					} else {
						if (CommonUtil.isEmpty(obj.chk_nama)) { //deskripsi wajib diisi
		                    throw new Exception("\"" + cm.ckm_nama + "\" wajib diisi jika diceklist (dipersyaratkan)!");
	                    }
	                    if(obj.isBankSupportSpec()){
	                    		obj.chk_nama = obj.chk_nama + "%";
						}
                    }
                    if (obj.chk_id == null) {
                    		obj.dll_id = dok_lelang.dll_id;
                    		obj.save();
                    		saveCheck.add(obj.chk_id);
                    } else {
                    	   chkExist = Checklist_tmp.findById(obj.chk_id);
                        if (chkExist != null) {
	                        chkExist.dll_id = dok_lelang.dll_id;
	                        chkExist.ckm_id = obj.ckm_id;
	                        chkExist.chk_nama = obj.chk_nama;
	                        chkExist.save();
	                        saveCheck.add(chkExist.chk_id);
                        } else {
	                        	obj.chk_id = null;
	    						obj.dll_id = dok_lelang.dll_id;
	    						obj.save();
	    						saveCheck.add(obj.chk_id);
                        }
                    }
                }
            }
        }
		
		// hapus list lain yang tidak dipilih oleh user
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			String sql="delete from checklist_tmp where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+ ')';
			Query.update(sql, dok_lelang.dll_id, jenis, ChecklistStatus.AKTIF);
		} else {
			String sql="delete from checklist_tmp where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)";
			Query.update(sql, dok_lelang.dll_id, jenis, ChecklistStatus.AKTIF);
		}
	}
	
	public static void simpanChecklist(Dok_lelang dok_lelang, List<Checklist_tmp> syaratList, JenisChecklist jenis) {
		List<Long> saveCheck = new ArrayList<Long>();
		Checklist_tmp chkExist = null;
		for(Checklist_tmp obj: syaratList) {
			if(obj.ckm_id != null) {
				if(StringUtils.isEmpty(obj.chk_nama))
					obj.chk_nama="";
				if(Checklist_master.count("ckm_nama=?", obj.chk_nama) > 0) // check persyaratan jika sama dengan checklist master, tidak perlu disimpan
					continue;
				if(Checklist_master.isSyaratLain(obj.ckm_id)) { // check jika ada syarat lain yg sama kontennya
					chkExist = Checklist_tmp.find("ckm_id=? AND chk_nama=? AND dll_id=?", obj.ckm_id, obj.chk_nama, dok_lelang.dll_id).first();
					if(chkExist != null) {
						saveCheck.add(chkExist.chk_id);
						continue;
					}
				}
				if(obj.chk_id == null) {
					obj.dll_id = dok_lelang.dll_id;
					obj.save();
					saveCheck.add(obj.chk_id);
				}
				else {
					chkExist = Checklist_tmp.findById(obj.chk_id);
					chkExist.dll_id = dok_lelang.dll_id;
					chkExist.ckm_id = obj.ckm_id;
					chkExist.chk_nama = obj.chk_nama;
					chkExist.chk_klasifikasi = obj.chk_klasifikasi;
					chkExist.save();
					saveCheck.add(chkExist.chk_id);
				}
			}			
		}

		String sql="delete from checklist_tmp where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)";
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			sql += " and chk_id not in ("+join+ ')';
		}
		Query.update(sql, dok_lelang.dll_id, jenis, ChecklistStatus.AKTIF);
	}

	
	// copy checklist persyaratan
	public static void copyChecklist(Long dok_id_source, Long dok_id_dest) {
		List<Checklist_tmp> list = find("dll_id=?", dok_id_source).fetch();
		if(CommonUtil.isEmpty(list))
			return;
		Checklist_tmp obj = null;
		for(Checklist_tmp checklist:list) {
			obj = new Checklist_tmp();
			obj.ckm_id = checklist.ckm_id;
			obj.chk_nama = checklist.chk_nama;
			obj.chk_klasifikasi = checklist.chk_klasifikasi;
			obj.dll_id = dok_id_dest;
			obj.save();
		}
	}
	
	public static void copyChecklist(Dok_lelang dok_lelang, List<Integer> jenis_checklist) {
		String jenis = StringUtils.join(jenis_checklist, ',');
		Checklist_tmp obj = Checklist_tmp.find("dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", dok_lelang.dll_id).first();
		if(obj != null)
			return;
		Integer versi = Checklist.findCurrentVersi(dok_lelang.dll_id, jenis_checklist);
		List<Checklist> list = Checklist.find("dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+")) order by ckm_id asc, chk_id asc", dok_lelang.dll_id, versi).fetch();
		for(Checklist checklist:list) {
			obj = new Checklist_tmp();
			obj.ckm_id = checklist.ckm_id;
			obj.chk_nama = checklist.chk_nama;
			obj.chk_klasifikasi = checklist.chk_klasifikasi;
			obj.dll_id = checklist.dll_id;
			obj.save();
		}
	}
		
	public int getSortingRule() {
		if (getChecklist_master().isConsultant()) {
			return 1;
		} else if (getChecklist_master().isSyaratLain()) {
			return 2;
		}
		return 0;
	}

	public boolean isChkNameExist() {
		return !CommonUtil.isEmpty(chk_nama);
	}

	public String getMasterName() {
		return getChecklist_master() == null || CommonUtil.isEmpty(getChecklist_master().ckm_nama)
				? ""
				: getChecklist_master().ckm_nama;
	}

	public String getChkNameContent() {
		return isChkNameExist()
				? getMasterName() + " (" + chk_nama + ")"
				: getMasterName();
	}

	private static final Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();

	@Transient
	public List<Map<String, String>> getJsonName() {
		try {
			return Json.fromJson(chk_nama, type);
		} catch (Exception e) {
			Logger.debug("Json Invalid");
		}
		return null;
	}

	public static boolean isChecklistEmpty(Long dll_id) {
		return Query.count("select count(chk_id) from checklist_tmp where dll_id=?", dll_id) == 0;
	}
	
    public static Integer findCurrentVersi(Long dll_id, List<Integer> jenis_checklist, Kategori kategori) {
        	String jenis = StringUtils.join(jenis_checklist, ',');
        	Integer versi = Query.find("SELECT max(ckm_versi) FROM checklist_master WHERE ckm_jenis in ("+jenis+") and ckm_id in (select ckm_id from checklist_tmp where dll_id=?)", Integer.class, dll_id).first();
        	if (versi == null)
        		versi = Query.find("SELECT max(ckm_versi) FROM checklist_master WHERE ckm_jenis in ("+jenis+") and kgr_id=?", Integer.class, kategori).first();			
        	return versi != null ? versi : Integer.valueOf(1);
	}
}
