package models.lelang;

import models.common.KeyLabel;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master.JenisChecklist;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Table(name="CHECKLIST_EVALUASI")
public class Checklist_evaluasi extends BaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusChecklistEvaluasi {
		TDK_MEMENUHI, MEMENUHI
	}
	
	//relasi ke Checklist
	@Id	
	public Long chk_id;
	
	//relasi ke Nilai_evaluasi
	@Id
	public Long nev_id;

	public StatusChecklistEvaluasi cke_ada;	
	
	@Transient
	private Checklist checklist;
	@Transient
	private Nilai_evaluasi nilai_evaluasi;	
	
	public Checklist getChecklist() {
		if(checklist == null)
			checklist =  Checklist.findById(chk_id);
		return checklist;
	}

	public Nilai_evaluasi getNilai_evaluasi() {
		if(nilai_evaluasi == null)
			nilai_evaluasi = Nilai_evaluasi.findById(nev_id);
		return nilai_evaluasi;
	}

	@Transient
	public boolean isLulus() {
		return cke_ada == StatusChecklistEvaluasi.MEMENUHI;
	}
	
	@Transient
	public String getNamaSyarat() {
		if(chk_id == null)
			return "";
		Checklist checklist = Checklist.findById(chk_id);
		Checklist_master cm = Checklist_master.findById(checklist.ckm_id);
		KeyLabel table = cm.getTable();
		if(cm.isIjinUsaha()) {
			if(!CommonUtil.isEmpty(checklist.chk_klasifikasi)) 
				return checklist.chk_nama+"<br />"+ Messages.get("lelang.klasifikasi") +": "+checklist.chk_klasifikasi;
			else
				return checklist.chk_nama;
		} else if (cm.isIjinUsahaBaru()) {
				if(!CommonUtil.isEmpty(checklist.chk_klasifikasi)) 
					return checklist.chk_nama+"<br />"+Messages.get("lelang.bu_sbu")+": "+checklist.chk_klasifikasi;
				else
					return checklist.chk_nama;
		} else if (cm.isInputNumber()) {
			StringBuilder result = new StringBuilder();		
			int i = 0;
			for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
				if (newline.split("number") == null || newline.split("number").length < 2) {
					result.append(newline);
				} else {
					int j = 0;
					for (String number: newline.split("number")) {
						result.append(number);
						for(Map<String, String> val : checklist.getJsonName()) {
							for(String key : table.key) {
								if (key.equals("number"+i) && j == 0) 
									result.append(val.get(key));
							}
						}
						j++;
					}
				}
				result.append("<br/>");
				i++;
			}
			return result.toString();
		}  else if (cm.isInputTextMulti()) {
			StringBuilder result = new StringBuilder();		
			int i = 0;
			for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
				if (newline.split("text") == null || newline.split("text").length < 2) {
					result.append(newline);
				} else {
					int j = 0;
					for (String number: newline.split("text")) {
						result.append(number);
						for(Map<String, String> val : checklist.getJsonName()) {
							for(String key : table.key) {
								if (key.equals("text"+i) && j == 0) 
									result.append(val.get(key));
							}
						}
						j++;
					}
				}
				result.append("<br/>");
				i++;
			}
			return result.toString();						
			
		} else if(checklist.getJsonName() != null && table != null) {
			StringBuilder result = new StringBuilder(cm.ckm_nama).append("<br/>");
			result.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
			result.append("<thead>");
			for (String label : table.label) {
				result.append("<th>").append(label).append("</th>");
			}
			result.append("</thead>");
			result.append("<tbody>");
			for(Map<String, String> val : checklist.getJsonName()) {
				result.append("<tr>");
				for(String key : table.key) {
					result.append("<td>").append(val.get(key)).append("</td>");
				}
				result.append("</tr>");
			}
			result.append("</tbody>");
			result.append("</table>");
			return result.toString();
		} else if(!cm.isNotEditable() && 
				(cm.ckm_jenis == JenisChecklist.CHECKLIST_KUALIFIKASI || 
				cm.ckm_jenis == JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI ||
				cm.ckm_jenis == JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS ||
				cm.ckm_jenis == JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN)) {			
			StringBuilder result = new StringBuilder();		
			String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
			if (newlines != null) {
				for (String newline: newlines) {
					result.append(newline);
					result.append("<br/>");
				}
			} else 
				result.append(cm.ckm_nama);
			result.append("<i>");
			String[] s = checklist.chk_nama.split("[\\r\\n]+");
			if (s != null) {
				for (String newline: s) {
					result.append(newline);
					result.append("<br/>");
				}
			} else 
				result.append(checklist.chk_nama);
			result.append("</i>");
			return result.toString();
		} else if(cm.isSyaratLain()){
			StringBuilder result = new StringBuilder();		
			String[] newlines = checklist.chk_nama.split("[\\r\\n]+");
			if (newlines != null) {
				for (String newline: newlines) {
					result.append(newline);
					result.append("<br/>");
				}
			} else 
				result.append(checklist.chk_nama);
			return result.toString();
		} else {
			StringBuilder result = new StringBuilder();		
			String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
			if (newlines != null) {
				for (String newline: newlines) {
					result.append(newline);
					result.append("<br/>");
				}
			} else 
				result.append(cm.ckm_nama);
			return result.toString();
		}
	}
	
	public static Checklist_evaluasi findBy(Long nilaiId, Long chkId) {
		if(nilaiId == null)
			return null;
		return find("nev_id=? and chk_id=?", nilaiId, chkId).first();
	}

	public static List<Checklist_evaluasi> findBy(Long nilaiId) {
		return find("nev_id=?",nilaiId).fetch();
	}
	
	public static List<Checklist_evaluasi> addPersyaratan(List<Checklist> syaratList, Long nilaiId){
		List<Checklist_evaluasi> list = new ArrayList<Checklist_evaluasi>();
		if(!CommonUtil.isEmpty(syaratList)) {
			for(Checklist checklist : syaratList) {
				Checklist_evaluasi nilai_check = findBy(nilaiId, checklist.chk_id);
				if(nilai_check == null) {
					nilai_check = new Checklist_evaluasi();
					nilai_check.nev_id = nilaiId;
					nilai_check.chk_id = checklist.chk_id;
					nilai_check.cke_ada = StatusChecklistEvaluasi.TDK_MEMENUHI;
				}
				list.add(nilai_check);
			}
		}
		return list;
	}

	/**
	 * simpan checklist evaluasi peserta terhadap persyaratan
	 * jika return true maka peserta dinyatakan lulus
	 * @param syaratList
	 * @param checklist
     * @return
     */
	public static boolean simpanChecklist(List<Checklist_evaluasi> syaratList, List<Long> checklist) {
		List<Long> list_memenuhi = new ArrayList<Long>();
		for(Checklist_evaluasi checklist_evaluasi:syaratList) {
			if (checklist!=null) {
				if(checklist.contains(checklist_evaluasi.chk_id)) {
					checklist_evaluasi.cke_ada = StatusChecklistEvaluasi.MEMENUHI;
					list_memenuhi.add(checklist_evaluasi.chk_id);
				} else
					checklist_evaluasi.cke_ada = StatusChecklistEvaluasi.TDK_MEMENUHI;
			}else{
				checklist_evaluasi.cke_ada = StatusChecklistEvaluasi.TDK_MEMENUHI;
			}
			checklist_evaluasi.save();
		}
		return syaratList.size() == list_memenuhi.size();
	}
}
