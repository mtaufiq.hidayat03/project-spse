package models.lelang;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Pengalaman;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


@Table(name="PENGALAMAN_PESERTA")
public class Pengalaman_peserta extends BaseModel {

	//relasi ke Peserta
	@Id
	public Long psr_id;

	@Id
	public Long pen_id;

	public String pgl_kegiatan;

	public String pgl_lokasi;

	public String pgl_pembtgs;

	public String pgl_almtpembtgs;

	public String pgl_telppembtgs;

	public String pgl_nokontrak;

	public Date pgl_tglkontrak;

	public Double pgl_nilai;

	public Date pgl_tglprogress;

	public Float pgl_persenprogress;

	public Date pgl_slskontrak;

	public Date pgl_slsba;

	public String pgl_keterangan;
	
	@Transient
	private Peserta peserta;
	
	public static Peserta getPeserta(Long pesertaId){
		return Peserta.find("psr_id=?", pesertaId).first();
	}

	// pekerjaan sedang berjalan
	public boolean isSedangBerjalan() {
		return pgl_persenprogress != null && pgl_persenprogress.intValue() < 100;
	}

	// pekerjaan sudah selesai 100 % dan dianggap pengalaman
	public boolean isSelesai() {
		return pgl_persenprogress != null && pgl_persenprogress.intValue() == 100;
	}

	public static List<Pengalaman_peserta> findPengalamanBy(Long pesertaId) {
		return find("psr_id=? and pgl_persenprogress = '100.0'", pesertaId).fetch();
	}

	public static List<Pengalaman_peserta> findPekerjaanBy(Long pesertaId) {
		return find("psr_id=? and pgl_persenprogress < 100", pesertaId).fetch();
	}
	
	public static void simpanPengalamanPeserta(List<Pengalaman> list, Long pesertaId) throws Exception {
		delete("psr_id=?", pesertaId);
		if(!CommonUtil.isEmpty(list)) {		
			Pengalaman_peserta psr_alam = null;
			for (Pengalaman alam:list) {
				psr_alam = new Pengalaman_peserta();
				psr_alam.pen_id = alam.pgn_id;
				psr_alam.psr_id = pesertaId;
				psr_alam.pgl_kegiatan= alam.pgl_kegiatan;
				psr_alam.pgl_lokasi = alam.pgl_lokasi;
				psr_alam.pgl_pembtgs = alam.pgl_pembtgs;
				psr_alam.pgl_almtpembtgs = alam.pgl_almtpembtgs;
				psr_alam.pgl_telppembtgs = alam.pgl_telppembtgs;
				psr_alam.pgl_nokontrak = alam.pgl_nokontrak;
				psr_alam.pgl_tglkontrak = alam.pgl_tglkontrak;
				psr_alam.pgl_nilai = alam.pgl_nilai;
				psr_alam.pgl_tglprogress = alam.pgl_tglprogress;
				psr_alam.pgl_persenprogress = alam.pgl_persenprogress;
				psr_alam.pgl_slskontrak = alam.pgl_slskontrak;
				psr_alam.pgl_slsba = alam.pgl_slsba;
				psr_alam.pgl_keterangan = alam.pgl_keterangan;
				psr_alam.save();
			}
		}
	}
}
