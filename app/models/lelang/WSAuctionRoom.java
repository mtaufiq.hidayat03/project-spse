package models.lelang;

import play.libs.F;
import play.libs.F.ArchivedEventStream;
import play.libs.F.EventStream;
import play.libs.F.IndexedEvent;
import play.libs.Promise;

import java.util.List;

public class WSAuctionRoom {

    final F.ArchivedEventStream<Event> chatEvents = new ArchivedEventStream<>(100);

    /**
     * For WebSocket, when a user join the room we return a continuous event stream
     * of ChatEvent
     */
    public EventStream<Event> join(String user) {
        chatEvents.publish(new Join(user));
        return chatEvents.eventStream();
    }

    /**
     * A user leave the room
     */
    public void leave(String user) {
        chatEvents.publish(new Leave(user));
    }

    /**
     * For long polling, as we are sometimes disconnected, we need to pass
     * the last event seen id, to be sure to not miss any message
     */
    public Promise<List<IndexedEvent<Event>>> nextMessages(long lastReceived) {
        return chatEvents.nextEvents(lastReceived);
    }

    /**
     * For active refresh, we need to retrieve the whole message archive at
     * each refresh
     */
    public List<Event> archive() {
        return chatEvents.archive();
    }


    public static abstract class Event {

        final public String type;
        final public Long timestamp;

        public Event(String type) {
            this.type = type;
            this.timestamp = System.currentTimeMillis();
        }

    }

    public static class Join extends Event {

        final public String user;

        public Join(String user) {
            super("join");
            this.user = user;
        }
    }

    public static class Leave extends Event {

        final public String user;

        public Leave(String user) {
            super("leave");
            this.user = user;
        }

    }

    static WSAuctionRoom instance = null;
    public static WSAuctionRoom get() {
        if(instance == null) {
            instance = new WSAuctionRoom();
        }
        return instance;
    }
}
