package models.lelang;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master.ChecklistStatus;
import models.lelang.Checklist_master.JenisChecklist;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Table(name="CHECKLIST_SANGGAH")
public class Checklist_sanggah extends BaseModel {

	@Id(sequence="seq_checklist_sanggah", function="nextsequence")
	public Long cks_id;	
	
	//relasi ke Sanggahan
	public Long sgh_id;

	//relasi ke Checklist_master
	public Integer ckm_id;

	public Integer cks_versi = Integer.valueOf(1);
	
	@Transient
	private Sanggahan sanggahan;
	
	@Transient
	private Checklist_master checklist_master;	
	
	public Sanggahan getSanggahan() {
		if(sanggahan == null)
			sanggahan = Sanggahan.findById(sgh_id);
		return sanggahan;
	}

	public Checklist_master getChecklist_master() {
		if(checklist_master == null)
			checklist_master = Checklist_master.findById(ckm_id);
		return checklist_master;
	}

	public boolean isChecked() {
		return this.cks_id != null;
	}
	
	public static List<Checklist_sanggah> getChecklist(JenisChecklist jenis) 
	{		
		List<Checklist_sanggah> checklistSanggah = new ArrayList<Checklist_sanggah>();
		Checklist_sanggah checklist = null;
		Integer versiMaster = findCurrentVersi(JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN);
		List<Checklist_master> masterList = new ArrayList<Checklist_master>();	
		masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and ckm_versi=? order by ckm_checked desc, ckm_urutan asc, ckm_id asc", jenis, ChecklistStatus.AKTIF, versiMaster).fetch();
		for (Checklist_master checklist_master : masterList) {
		    checklist = new Checklist_sanggah();
            checklist.ckm_id = checklist_master.ckm_id;
            if(checklist_master.ckm_checked == 1) {
				checklist.save();
			}
            checklistSanggah.add(checklist);
		}		
		return checklistSanggah;
	}
	
	public static void simpanChecklist(Sanggahan sanggahan, List<Checklist_sanggah> checklist, JenisChecklist jenis, Integer versi) {
		List<Long> saveCheck = new ArrayList<Long>();
		Checklist_sanggah chkExist = null;
		for(Checklist_sanggah obj: checklist) {
			if(obj.ckm_id != null) {
				if(obj.cks_id == null) {
					obj.sgh_id = sanggahan.sgh_id;
					obj.cks_versi = versi;
					obj.save();
					saveCheck.add(obj.cks_id);
				}
				else {
					chkExist = Checklist_sanggah.findById(obj.cks_id);
					chkExist.sgh_id = sanggahan.sgh_id;
					chkExist.ckm_id = obj.ckm_id;
					chkExist.cks_versi = versi;
					chkExist.save();
					saveCheck.add(chkExist.cks_id);
				}
			}			
		}

		String sql="delete from checklist_sanggah where sgh_id=? AND cks_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)";
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			sql += " and cks_id not in ("+join+ ')';
		}
		Query.update(sql, sanggahan.sgh_id, versi, jenis, ChecklistStatus.AKTIF);
	}
		
	public static Integer findCurrentVersi(JenisChecklist jenis_checklist) {
	  Integer versi = Query.find("SELECT max(ckm_versi) FROM checklist_master WHERE ckm_jenis=?", Integer.class, jenis_checklist.id).first();			
	  return versi != null ? versi : Integer.valueOf(1);
	}

}
