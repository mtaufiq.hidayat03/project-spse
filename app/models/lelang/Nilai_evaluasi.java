package models.lelang;

import ext.DecimalBinder;
import ext.RupiahBinder;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Evaluasi.JenisEvaluasi;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;


@Table(name="NILAI_EVALUASI")
public class Nilai_evaluasi extends BaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusNilaiEvaluasi {
		TDK_LULUS ("Tidak Lulus"), LULUS("Lulus"), LULUS_TDK_SHORTLIST("Lulus Bukan Daftar Pendek");
		
		public final String label;
		
		private StatusNilaiEvaluasi(String label) {
			this.label = label;
		}
		
		public boolean isLulus() {
			return this == LULUS;
		}
		public static StatusNilaiEvaluasi fromValue(Integer value) {
			StatusNilaiEvaluasi res = null;
			if(value != null) {
				if(value.equals(0))
					res = TDK_LULUS;
				else if(value.equals(1))
					res = LULUS;
				else if(value.equals(2))
					res = LULUS_TDK_SHORTLIST;				
			}
			return res;
		}
	}
	@Id(sequence="seq_nilai_evaluasi", function="nextsequence")
	public Long nev_id;

	@As(binder=RupiahBinder.class)
	public Double nev_harga;

	@As(binder=RupiahBinder.class)
	public Double nev_harga_terkoreksi;

	@As(binder=RupiahBinder.class)
	public Double nev_harga_negosiasi;

	@As(binder=DecimalBinder.class)
	public Double nev_skor;

	public Integer nev_urutan;

	public String nev_uraian;
	@Required
	public StatusNilaiEvaluasi nev_lulus;

	public Long nev_id_attachment;

	//relasi ke Evaluasi
	public Long eva_id;

	//relasi ke Peserta
	public Long psr_id;
	
	@Transient
	private Peserta peserta;
	@Transient
	private Evaluasi evaluasi;
	
	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}	

	public Evaluasi getEvaluasi() {
		if(evaluasi == null)
			evaluasi = Evaluasi.findById(eva_id);
		return evaluasi;
	}


	@Transient
	public boolean isHargaOverHps(Double hps) {
		if (nev_harga != null) {
			return nev_harga.doubleValue() > hps.doubleValue();
		}
		return false;
	}
	@Transient
	public boolean isHargaOverPagu(Double pagu) {
		if (nev_harga != null) {
			return nev_harga.doubleValue() > pagu.doubleValue();
		}
		return false;
	}

	public String getAlasan() {
		if(CommonUtil.isEmpty(nev_uraian))
			return "";
		return nev_uraian;
	}
	
	@Transient
	public boolean isHargaTerkoreksiOverHps(Double hps) {
		if (nev_harga_terkoreksi != null) {
			return nev_harga_terkoreksi.doubleValue() > hps.doubleValue();
		}
		return false;
	}
	@Transient
	public boolean isHargaTerkoreksiOverPagu(Double pagu) {
		if (nev_harga_terkoreksi != null) {
			return nev_harga_terkoreksi.doubleValue() > pagu.doubleValue();
		}
		return false;
	}
	@Transient
	public boolean isAlasanDiisi() {
		if (!nev_lulus.isLulus()) {
			if (nev_uraian != null)
				return nev_uraian.length() > 5;
			else
				return false;
		}
		return true;
	}
	@Transient
	public boolean isLulus() {		
		if (nev_lulus != null) {
			return nev_lulus.isLulus();
		}
		return false;
	}
	
	@Transient
	public void setLulus(boolean lulus) {
		nev_lulus  = lulus ? StatusNilaiEvaluasi.LULUS : StatusNilaiEvaluasi.TDK_LULUS;
	}

	@Transient
	public boolean isShortlist(){
		if (nev_lulus != null) {
			return nev_lulus.isLulus();
		}
		return false;
	}
	
	@Transient
	public void setShortlist(boolean shortlist){
		if(shortlist)
			nev_lulus = StatusNilaiEvaluasi.LULUS;
		else if(isLulus())
			nev_lulus = StatusNilaiEvaluasi.LULUS_TDK_SHORTLIST;
	}	
	
	public static List<Nilai_evaluasi> findBy(Long evaluasiId) {
		return find("eva_id= ? ORDER BY psr_id ASC, nev_urutan ASC", evaluasiId).fetch();
	}
	
	public static List<Nilai_evaluasi> findWithUrutHarga(Long evaluasiId) {
		return find("eva_id= ? ORDER BY nev_harga ASC, nev_urutan ASC", evaluasiId).fetch();
	}

	public static List<Nilai_evaluasi> findWithUrutHargaNegosiasi(Long evaluasiId) {
		return find("eva_id= ? ORDER BY nev_harga_negosiasi ASC, nev_urutan ASC", evaluasiId).fetch();
	}

	public static List<Nilai_evaluasi> findByWithNevUrut(Long evaluasiId) {
		return find("eva_id= ? ORDER BY nev_urutan ASC", evaluasiId).fetch();
	}
	
	public static List<Nilai_evaluasi> findWithUrutSkor(Long evaluasiId) {
		return find("eva_id= ? ORDER BY nev_skor DESC, nev_urutan ASC", evaluasiId).fetch();
	}

	public static int countLulus(Long evaluasiId) {
		return (int)count("eva_id=? and nev_lulus=?", evaluasiId, StatusNilaiEvaluasi.LULUS.ordinal());
	}
	
	public static int countLulus(Long lelangId,	JenisEvaluasi jenis) {
		return (int) count("eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1) and nev_lulus = ?", lelangId, jenis, StatusNilaiEvaluasi.LULUS);
	}

	public static int countLulus(Long lelangId,	JenisEvaluasi jenis, Integer versi) {
		return (int)count("eva_id in (select eva_id from evaluasi where lls_id=? and eva_jenis=? and eva_versi=?) and nev_lulus=?", lelangId, jenis, versi, StatusNilaiEvaluasi.LULUS);
	}

	public static int countTidakLulus(Long lelangId, JenisEvaluasi jenis, Integer versi) {
		return (int)count("eva_id in (select eva_id from evaluasi where lls_id=? and eva_jenis=? and eva_versi=?) and nev_lulus=?", lelangId, jenis, versi, StatusNilaiEvaluasi.TDK_LULUS);
	}
	
	public static boolean isLulus(JenisEvaluasi jenis, Peserta peserta) {
		Long lelangId = peserta.lls_id;
		int count = (int)count("eva_id in (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1) and nev_lulus=? and psr_id=?", lelangId, jenis, StatusNilaiEvaluasi.LULUS, peserta.psr_id);
		return count > 0;
	}

	public static Nilai_evaluasi findBy(Long evaluasiId, Long pesertaId) {
		return find("eva_id=? and psr_id=?", evaluasiId, pesertaId).first();
	}

	public static Nilai_evaluasi findByPesertaAndEvaluasi(Long psr_id, Long eva_id){
		return find("psr_id = ? AND eva_id = ?", psr_id, eva_id).first();
	}
	
	public static Nilai_evaluasi findNCreateBy(Long evaluasiId, Peserta peserta, JenisEvaluasi jenis) {
		Nilai_evaluasi nilai = findBy(evaluasiId, peserta.psr_id);
		if(nilai == null) {
			nilai = new Nilai_evaluasi();
			nilai.eva_id = evaluasiId;
			nilai.psr_id = peserta.psr_id;
			if(jenis.isHarga() || jenis.isAkhir()) {
				nilai.nev_harga = peserta.psr_harga;
				nilai.nev_harga_terkoreksi = peserta.psr_harga_terkoreksi;
			}
			nilai.save();
		}
		return nilai;
	}
	
	public static List<Nilai_evaluasi> findByRekanan(Long rekananId,int jenis, Long lulus) {
		return find("nev_id in (select nev_id from nilai_evaluasi n, peserta p, rekanan r , evaluasi  e where n.eva_id = e.eva_id and n.psr_id=p.psr_id and p.rkn_id=r.rkn_id and e.eva_jenis =? and n.nev_lulus =? and r.rkn_id=?)",jenis,lulus,rekananId).fetch();
	}

	public static Nilai_evaluasi findByLulusAndLelang(Long lls_id){
		return find("nev_id in (SELECT ne.nev_id FROM nilai_evaluasi ne " +
				"INNER JOIN peserta p ON ne.psr_id = p.psr_id " +
				"WHERE p.lls_id = ? AND ne.nev_lulus = 1)", lls_id).first();
	}

	public static List<Peserta> findPemenangPrakualifikasi(Long lls_id){
		List<Nilai_evaluasi> lulusPrakualifikasi = find("eva_id IN (SELECT eva_id FROM evaluasi WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1) and nev_lulus=?",
				lls_id, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI, StatusNilaiEvaluasi.LULUS).fetch();
		List<Peserta> pemenangPrakualifikasi = lulusPrakualifikasi.stream().map(e -> e.getPeserta()).collect(Collectors.toList());
		return pemenangPrakualifikasi;
	}

	public String calculatePrecentageHarga(Double nilai_hps){

		DecimalFormat df = new DecimalFormat("#.00");

		return df.format((nev_harga * 100) / nilai_hps);
	}

	public String calculatePrecentageHargaTerkoreksi(Double nilai_hps){

		DecimalFormat df = new DecimalFormat("#.00");

		return df.format((nev_harga_terkoreksi * 100) / nilai_hps);
	}

	// cari nama peserta
	public String getNamaPeserta() {
		return Query.find("SELECT rkn_nama FROM peserta p, rekanan r WHERE r.rkn_id=p.rkn_id AND psr_id=?", String.class, psr_id).first();
	}

	public Double hargaFinal(){
		return nev_harga_negosiasi != null ? nev_harga_negosiasi : nev_harga_terkoreksi;
	}

}
