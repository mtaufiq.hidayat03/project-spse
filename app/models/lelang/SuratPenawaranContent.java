package models.lelang;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rayhanfrds on 5/5/17.
 */
public class SuratPenawaranContent implements Serializable {

	private final String title;
	private final List<String> body;
	private final String nomor;

	public SuratPenawaranContent(String title, List<String> body, String nomor){
		this.title = title;
		this.body = body;
		this.nomor = nomor;
	}

	public String getTitle() {
		return title;
	}

	public List<String> getBody() {
		return body;
	}

	public String getNomor() {
		return nomor;
	}
}
