package models.lelang;

import com.google.gson.reflect.TypeToken;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Staf_ahli;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table(name="STAF_AHLI_PSR")
public class Staf_ahli_psr extends BaseModel {
	
	@Id
	public Long stp_id;
	
	@Id
	public Long psr_id;

	public String sta_nama;

	public Date sta_tgllahir;

	public String sta_alamat;

	public String sta_jabatan;

	public String sta_pendidikan;

	public Integer sta_pengalaman;

	public String sta_keahlian;

	public String sta_email;

	public String sta_telepon;

	public String sta_jenis_kelamin;

	public String sta_kewarganearaan;

	public Integer sta_status;
	
	public String sta_cv;
	
	// field baru 4.1.2
	public String sta_npwp;

	@Transient
	public List<Staf_ahli.Cv_staf> pengalaman = new ArrayList<Staf_ahli.Cv_staf>();
	@Transient
	public List<Staf_ahli.Cv_staf> pendidikan = new ArrayList<Staf_ahli.Cv_staf>();
	@Transient
	public List<Staf_ahli.Cv_staf> sertifikasi = new ArrayList<Staf_ahli.Cv_staf>();
	@Transient
	public List<Staf_ahli.Cv_staf> bahasa = new ArrayList<Staf_ahli.Cv_staf>();
	
	public static Peserta getPeserta(Long pesertaId){
		return Peserta.find("psr_id=?", pesertaId).first();
	}

	public static void simpanStafAhliPeserta(Long pesertaId, List<Staf_ahli> listStaf) {
		 delete("psr_id=?",pesertaId);
		 if(!CommonUtil.isEmpty(listStaf)) {
			 Staf_ahli_psr stp = null;			 
			 for(Staf_ahli sta : listStaf) {
				 stp = new Staf_ahli_psr();
				 stp.psr_id = pesertaId;
				 stp.stp_id = sta.sta_id;
				 stp.sta_nama = sta.sta_nama;
				 stp.sta_tgllahir = sta.sta_tgllahir;
		         stp.sta_alamat = sta.sta_alamat;
		         stp.sta_jabatan = sta.sta_jabatan;
		         stp.sta_pendidikan = sta.sta_pendidikan;
		         stp.sta_pengalaman = sta.sta_pengalaman;
		         stp.sta_keahlian = sta.sta_keahlian;
		         stp.sta_email = sta.sta_email;
		         stp.sta_telepon = sta.sta_telepon;
		         stp.sta_jenis_kelamin = sta.sta_jenis_kelamin;
		         stp.sta_kewarganearaan = sta.sta_kewarganearaan;
		         stp.sta_status = sta.sta_status;
		         stp.sta_cv = sta.sta_cv;
		         stp.sta_npwp = sta.sta_npwp;
		 		 stp.save();		 		
			 }
		 }
	}

	@Override
	protected void postLoad() {
		if(!StringUtils.isEmpty(sta_cv)) {
			List<Staf_ahli.Cv_staf> list = CommonUtil.fromJson(sta_cv, new TypeToken<ArrayList<Staf_ahli.Cv_staf>>(){}.getType());
			if(!CollectionUtils.isEmpty(list)) {
				Staf_ahli.Cv_staf.Cv_kategori  kategori = null;
				for(Staf_ahli.Cv_staf o : list) {
					switch (o.kategori()) {
						case BAHASA: bahasa.add(o);break;
						case PELATIHAN:sertifikasi.add(o);break;
						case PENDIDIKAN:pendidikan.add(o);break;
						case PENGALAMAN:pengalaman.add(o);break;
					}
				}
			}
		}
	}
	
}
