package models.lelang;

import com.google.gson.JsonObject;
import com.thoughtworks.xstream.converters.time.PeriodConverter;

import controllers.BasicCtr;
import ext.DatetimeBinder;
import ext.FormatUtils;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.workflow.instance.ProcessInstance;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Validation;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;
import play.mvc.Scope;
import models.sso.common.adp.util.DceSecurityV2;
import org.joda.time.*;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Table(name = "JADWAL")
public class Jadwal extends BaseModel {
	
	@Id(sequence="seq_jadwal", function="nextsequence")
	public Long dtj_id;

	@As(binder = DatetimeBinder.class)
	public Date dtj_tglawal;

	@As(binder = DatetimeBinder.class)
	public Date dtj_tglakhir;

	public String dtj_keterangan;
	
	public Long lls_id;
	
	public Long akt_id;
	
	public Integer thp_id;

	@Transient
	private Lelang_seleksi lelang_seleksi;

	@Transient
	public Tahap akt_jenis;
	
	@Transient
	public int akt_urut;
	
	@Transient
	public String keterangan;

	@Transient
	private List<History_jadwal> historyList;
	
	// custome result, untuk optimasi query
	public static final ResultSetHandler<Jadwal> CUSTOME_RESULTSET = new ResultSetHandler<Jadwal>() {
		@Override
		public Jadwal handle(ResultSet rs) throws SQLException {
			Jadwal jadwal = new Jadwal();				
			jadwal.dtj_id = rs.getLong("dtj_id");
			jadwal.dtj_tglawal = rs.getTimestamp("dtj_tglawal");
			jadwal.dtj_tglakhir = rs.getTimestamp("dtj_tglakhir");
			jadwal.dtj_keterangan = rs.getString("dtj_keterangan");
			jadwal.lls_id = rs.getLong("lls_id");
			jadwal.thp_id = rs.getInt("thp_id");
			jadwal.audittype = rs.getString("audittype");
			jadwal.audituser = rs.getString("audituser");
			jadwal.auditupdate = rs.getTimestamp("auditupdate");
			jadwal.akt_id = rs.getLong("akt_id");
			jadwal.akt_jenis = Tahap.valueOf(rs.getString("akt_jenis"));
			jadwal.akt_urut = rs.getInt("akt_urut");
			jadwal.keterangan = rs.getString("keterangan");
			return jadwal;
		}
	};

	/*// pengecekan untuk pra
	public static boolean allowCreateAdendum(Long lelangId, boolean prakualifikasi) {
		List<Tahap> tahapStarted = Aktivitas.findTahapStarted(lelangId);
		boolean allowCreateAdendum = tahapStarted.contains(Tahap.PENGUMUMAN_LELANG)
				|| tahapStarted.contains(Tahap.PEMASUKAN_PENAWARAN);
		if (prakualifikasi) {
			Jadwal jadwal = findByLelangNTahap(lelangId, Tahap.PENGUMUMAN_HASIL_PRA);
			if (jadwal == null)
				jadwal = findByLelangNTahap(lelangId, Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK);
			if (jadwal != null && jadwal.dtj_tglawal != null && jadwal.dtj_tglawal.after(BasicCtr.newDate())) {
				// jika sebelum jadwal pengumuman hasil pra, maka dokumen lelang masih dianggap draft bukan adendum
				allowCreateAdendum = false;
			} else {
				allowCreateAdendum = true;
			}
		}
		return allowCreateAdendum;
	}*/

	public boolean isDtjAkhirExist() {
		return this.dtj_tglakhir != null;
	}

    public Lelang_seleksi getLelang_seleksi() {
		if(lelang_seleksi == null)
			lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}

	public List<History_jadwal> getHistoryList() {
		if(historyList == null)
			historyList = History_jadwal.find("dtj_id=?", dtj_id).fetch();
		return historyList;
	}

	/**
	 * jadwal sudah masuk tahapan saat ini
	 * @return
	 */
	@Transient
	public boolean isStart(Date curDate) {
		return (dtj_tglawal != null && curDate.after(dtj_tglawal));
	}

	/**
	 * jadwal sedang berlangsung
	 * @return
	 */
	@Transient
	public boolean isNow(Date curDate) {
		if (dtj_tglawal != null && dtj_tglakhir != null)
			return (curDate.after(dtj_tglawal) && curDate.before(dtj_tglakhir));
		return false;
	}
	
	/**
	 * jadwal sudah keluar tahapan saat ini
	 * @return
	 */
	@Transient
	public boolean isEnd(Date curDate) {
		return (dtj_tglakhir != null && curDate.after(dtj_tglakhir));
	}
	
	public long getJumlah_history() {
		return History_jadwal.count("dtj_id=?", dtj_id);
	}
	
	public static Jadwal getJadwalByJadwalAndAct(long lls_id, long akt_id){
		return Jadwal.find("lls_id=? and akt_id=?", lls_id, akt_id).first();
	}
	
	/*@Transient
	public boolean isMulaiReadOnly(Lelang_seleksi lelang)
	{
		Tahap tahap = getAktivitas().akt_jenis;		
		if(tahap != null)
		{
			return (tahap == Tahap.PEMBUKAAN_PENAWARAN  || tahap == Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS 
					|| (tahap == Tahap.EVALUASI_DOK_PRA && !lelang.getMetode().kualifikasi.isPasca())) && lelang.lls_status.isAktif();
		}
		return false;		
	}

	@Transient
	public boolean isSelesaiReadOnly(Lelang_seleksi lelang)
	{
		Tahap tahap = getAktivitas().akt_jenis;	
		if(tahap != null )
		{
			return (tahap == Tahap.PEMASUKAN_PENAWARAN || tahap == Tahap.PEMASUKAN_DOK_PRA) && lelang.lls_status.isAktif();			
		}
		return false;
	}*/	
	
	@Override
	public boolean equals(Object obj) {
		Jadwal jadwal = (Jadwal)obj;
		return equalAwal(jadwal) && equalAkhir(jadwal);
	}
	
	// ada perubahan atau tidak pada tgl awal
	@Transient
	public boolean equalAwal(Jadwal jadwal) {
		return (dtj_tglawal != null && jadwal.dtj_tglawal != null && dtj_tglawal.equals(jadwal.dtj_tglawal)); 						
	}
	
	// ada perubahan atau tidak pada tgl akhir
	@Transient
	public boolean equalAkhir(Jadwal jadwal) {
		return (dtj_tglakhir != null && jadwal.dtj_tglakhir != null && dtj_tglakhir.equals(jadwal.dtj_tglakhir));			
	}
	
	@Transient
	public String formatDate() {
		return FormatUtils.formatDate(dtj_tglawal, dtj_tglakhir);
	}
	
	@Transient
	public String formatDateTime() {
		return FormatUtils.formatDateTimeInd(dtj_tglawal)+" - "+ FormatUtils.formatDateTimeInd(dtj_tglakhir);
	}
	

	
	// validasi isian jadwal , sebelum disimpan ke DB
	public void validate(Date currentDate, boolean express, Jadwal before, Jadwal next, boolean adaPerubahanAwal, boolean adaPerubahanAkhir) throws Exception {
		boolean first = before == null; // first jika jadwal before == null		
		Tahap tahapBefore = null;
		if(before != null) 
			tahapBefore = before.akt_jenis;
		/* update: tahapan penjelasan pra dan penjelasan tender cepat juga harus divalidasi
		if(akt_jenis == Tahap.PENJELASAN_PRA || akt_jenis == Tahap.PENYETARAAN_TEKNIS ||
				tahapBefore == Tahap.PENJELASAN_PRA || tahapBefore == Tahap.PENYETARAAN_TEKNIS ||
				(express && akt_jenis == Tahap.PENJELASAN)){
			return;
		}
		*/

		if(dtj_tglawal == null) {
			throw new Exception(Messages.get("lelang.tmbt"));
		}
		else if(dtj_tglakhir == null) {
			throw new Exception(Messages.get("lelang.tsbt"));
		}		
		else if(dtj_tglawal != null && dtj_tglawal.before(currentDate) && adaPerubahanAwal)
		{
			throw new Exception(Messages.get("lelang.tmyamsdl"));
		}
		else if(dtj_tglakhir != null && dtj_tglakhir.before(currentDate) && adaPerubahanAkhir)
		{
			throw new Exception(Messages.get("lelang.tayamsdl"));
		}
		else if(dtj_tglakhir.before(dtj_tglawal))
		{
			// Role #2: awal > akhir
			throw new Exception(Messages.get("lelang.tsmdtm"));
		}
		else if (dtj_tglakhir != null && dtj_tglawal != null && before != null) {
			//utk lelang cepat jadwal Aanwijzing boleh null/optional
			//update: semua jadwal wajib diisi
			//if((!express && tahapBefore != Tahap.PENJELASAN)) {
				// Role #1: tahap sebelumnya != null except tahap pertama
				if (!first && before.dtj_tglawal == null) {
					throw new Exception(Messages.get("lelang.bjpts"));
				}
				// Role #2: awal > akhir
				if (dtj_tglakhir.before(dtj_tglawal)) {
					throw new Exception(Messages.get("lelang.tsmdtm"));
				}
				// Role #3: awal(tahap sekarang) == awal(tahap sebelumnya) ||
				// awal(tahap sekarang) > awal(tahap sebelumnya)
				if (!first)//asep
					if (dtj_tglawal.before(before.dtj_tglawal)) {
						throw new Exception(Messages.get("lelang.tgl_mptmtmdts"));
					}
			//}
			// Role #4: jadwal tahap upload dok. penawaran tidak boleh
			// sama/paralel dengan tahap pembukaan dok. penawaran
			if(akt_jenis == Tahap.PEMASUKAN_PENAWARAN || akt_jenis == Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS || akt_jenis == Tahap.PEMASUKAN_PENAWARAN_BIAYA) {
				if (next != null && next.dtj_tglakhir != null && (next.dtj_tglawal.before(dtj_tglakhir) || dtj_tglakhir.equals(next.dtj_tglawal))) {
					throw new Exception(Messages.get("lelang.tudphbtstpdp"));
				}
			}

			boolean isOverlap = dtj_tglawal.before(before.dtj_tglakhir) || dtj_tglawal.equals(before.dtj_tglakhir);

			if (akt_jenis == Tahap.PEMBUKAAN_PENAWARAN || akt_jenis == Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS
//					|| akt_jenis == Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA
					||akt_jenis == Tahap.PEMBUKAAN_PENAWARAN_BIAYA) {
				if (isOverlap) {
					throw new Exception(Messages.get("lelang.tpdphbtstudpb"));
				}
			}
			 //role 5 tidak berlaku pada perpres 16/2018
			// role #5: Jadwal Pengumuman Pemenang tidak boleh sama dengan jadwal sebelumnya
			if(akt_jenis == Tahap.PENGUMUMAN_HASIL_PRA || akt_jenis == Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS || akt_jenis == Tahap.PENGUMUMAN_PEMENANG_AKHIR) {
				if (isOverlap) {
					throw new Exception("Tahap Pengumuman Pemenang tidak boleh beririsan dengan tahap Penetapan Pemenang.");
				}
			}

			if(akt_jenis == Tahap.EVALUASI_DOK_PRA && before.akt_jenis == Tahap.PEMASUKAN_DOK_PRA){
				if (isOverlap) {
					throw new Exception(Messages.get("lelang.tedkhbt"));
				}
			}

			if(akt_jenis == Tahap.PENGUMUMAN_HASIL_PRA && before.akt_jenis == Tahap.PENETAPAN_HASIL_PRA){
				if (isOverlap) {
					throw new Exception(Messages.get("lelang.tphphbt"));
				}
			}

		}
	}
		
	public static List<Jadwal> findByLelang(Long lelangId)  {
		String sql = "select j.*, akt_jenis, akt_urut, keterangan from jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id= ? order by akt_urut asc";
		return Query.find(sql,CUSTOME_RESULTSET,lelangId).fetch();
	}
	
	public static Jadwal findByLelangNTahap(Long lelangId, Tahap tahap) {
		List<Jadwal> list = findByLelangNTahap(lelangId, new Tahap[]{tahap});	
		if(CommonUtil.isEmpty(list))
			return null;
		return list.get(0);
	}
	
	public static List<Jadwal> findByLelangNTahap(Long lelangId, Tahap... tahapList)  {
		StringBuilder sql = new StringBuilder("select j.*, akt_jenis, akt_urut, keterangan from jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id=?");
		if(!ArrayUtils.isEmpty(tahapList)) {
			sql.append("AND a.akt_jenis in ('").append(StringUtils.join(tahapList, "','")).append("')");
		}
		return Query.find(sql.toString(), CUSTOME_RESULTSET,lelangId).fetch();			
	}	

	public static Long countJadwalkosong(Long lelangId) {
		return Jadwal.count("lls_id=? and (dtj_tglawal is null or dtj_tglakhir is null)", lelangId);
	}
	
	/**Dapatkan jadwal lelang, jika saat ini ada lebih dari satu yang aktif, tampilkan  [...]
	 * 
	 * @param lelangId
	 * @return
	 */
	public static String getJadwalSekarang(Long lelangId, boolean shortVersion, boolean express, boolean lelangV3){
		Date currentDate = controllers.BasicCtr.newDate();
		String tahaps = Query.find("SELECT tahap_now(?, ?)", String.class, lelangId, currentDate).first();		
		return Tahap.tahapInfo(tahaps, shortVersion, lelangV3, express);
	}
	
	@Transient
	public String namaTahap(boolean spseV3) {
		if(akt_jenis == null)
			return "";
		else if(!spseV3 && Tahap.tahapanBA.contains(akt_jenis)) { //label tahap berita Acara Harus disesuaikan 
			return akt_jenis.getLabel().replace("Upload", "Pembuatan");
		}
		return akt_jenis.getLabel();
	}
	
    public static List<Jadwal> getJadwalkosong(Long lelangId, Metode metode, boolean lelangExpress) {
    	List<Jadwal> list = new ArrayList<Jadwal>();
    	List<Aktivitas> aktivitasList = lelangExpress ? Aktivitas.AKTIVITAS_TENDER_CEPAT : Aktivitas.findByMetode(metode.id);
    	Jadwal jadwal = null;
		for (Aktivitas aktivitas:aktivitasList) 
		{
			jadwal = Jadwal.find("akt_id=? and lls_id=?", aktivitas.akt_id, lelangId).first();
			if(jadwal == null)
				jadwal = new Jadwal();
			jadwal.lls_id = lelangId;
			jadwal.akt_id = aktivitas.akt_id;			
			jadwal.thp_id = aktivitas.akt_jenis.id;
			jadwal.akt_jenis = aktivitas.akt_jenis;
			jadwal.akt_urut  = aktivitas.akt_urut;
			jadwal.keterangan = aktivitas.keterangan;
			jadwal.save();
			list.add(jadwal);
		} 
		return list;    	
	}
    
    /**copy jadwal dan langsung simpan ke database.
     * Cara ini akan ditinggalkan karena jika jadwal error maka yang tersimpan di database adalah data yang salah
     * 
     * @param lelangId
     * @param lelangIdTujuan
     */
    public static void copyJadwal(Long lelangId, Long lelangIdTujuan) {
    	List<Jadwal> listSource = Jadwal.findByLelang(lelangIdTujuan);
    	Jadwal obj = null;
    	Date currentDate = controllers.BasicCtr.newDate();
    	String fieldName=null;
    	int i =0 ;
    	for (Jadwal jadwal : listSource) {
			obj = Jadwal.find("lls_id=? and akt_id=?", lelangId, jadwal.akt_id).first();
			fieldName = "jadwal["+i+ ']';
			if(obj != null && jadwal != null) {
				obj.dtj_tglawal = jadwal.dtj_tglawal;
				obj.dtj_tglakhir = jadwal.dtj_tglakhir;
				try {
				if(obj.dtj_tglawal != null && obj.dtj_tglawal.before(currentDate))
					throw new Exception(Messages.get("lelang.tmsl"));
				if(jadwal.dtj_tglakhir != null && jadwal.dtj_tglakhir.before(currentDate))
					throw new Exception(Messages.get("lelang.tasl"));
				}catch (Exception e){
					Logger.error(e.getMessage());
					Validation.addError(fieldName, e.getLocalizedMessage());					
				}
				obj.save();
			}	
			i++;
		}    	
    }
    
    /*Copy jadwal ke dalam List<> lalu ditampilkan pada UI -- belum di-save ke database
     * 
     */
    
    public static List<Jadwal> copyJadwalIntoCache(Long lelangId, Long lelangIdTujuan) {
    	List<Jadwal> listSource = Jadwal.findByLelang(lelangId);
    	Jadwal obj = null;
    	Date currentDate = controllers.BasicCtr.newDate();
    	String fieldName=null;
    	int i =0 ;
    	List<Jadwal> listTarget=new ArrayList<Jadwal>();
    	for (Jadwal jadwal : listSource) {
			obj = Jadwal.findByLelangNTahap(lelangIdTujuan, jadwal.akt_jenis);
			fieldName = "jadwal["+i+ ']';
			if(obj != null && jadwal != null) {
				obj.dtj_tglawal = jadwal.dtj_tglawal;
				obj.dtj_tglakhir = jadwal.dtj_tglakhir;
				try {
				if(obj.dtj_tglawal != null && obj.dtj_tglawal.before(currentDate))
					throw new Exception(Messages.get("lelang.tmsl"));
				if(jadwal.dtj_tglakhir != null && jadwal.dtj_tglakhir.before(currentDate))
					throw new Exception(Messages.get("lelang.tasl"));
				}catch (Exception e){
					Logger.error(e.getMessage());
					Validation.addError(fieldName, e.getLocalizedMessage());					
				}				
				listTarget.add(obj);
			}	
			i++;
		}
    	return listTarget;
    }

	/**
	 * simpan jadwal dengan sebelumnya mengisi keterangan hasil input,
	 * asumsi harus lolos validasi dulu ({@code lelang.JadwalCtr.validate})
	 * @param jadwalList
	 * @param lelang
	 * @param alasan alasan perubahan jadwal
	 * @return true jika tidak ada error
	 */
	public static void simpanJadwal(Lelang_seleksi lelang, List<Jadwal> jadwalList, Date currentDate, String alasan) throws Exception {
		boolean send_email = false;
		List<Jadwal> jadwalOri = Jadwal.findByLelang(lelang.lls_id);
		List<Jadwal> jadwalBefore = new ArrayList<>();
        Jadwal jadwal=null, jadwalO=null, before=null, next=null;
        String fieldName = null;
        for (int i = 0; i < jadwalList.size(); i++) {
            jadwal = jadwalList.get(i);
            jadwalO = jadwalOri.get(i);
            jadwalBefore.add(SerializationUtils.clone(jadwalO));
            Aktivitas akt = Aktivitas.findById(jadwal.akt_id); // load tahap saat ini (akt_jenis null)
            jadwal.akt_jenis = akt.akt_jenis;
            jadwal.akt_id = jadwalO.akt_id;
            // jika tidak ada informasi waktu maka set menjadi jam 23:59:59
            // Tp metode ini mungkin ada bugnya, pls dicek lagi (ADY)
            if (jadwal.dtj_tglakhir != null) {
                DateTime dateTime = new DateTime(jadwal.dtj_tglakhir.getTime());
                if (dateTime.getHourOfDay() == 0 && dateTime.getMinuteOfHour() == 0) // jika waktu jadwal terakhir 00:00
                {
                    DateTime result = dateTime.withHourOfDay(23).withMinuteOfHour(59);
                    jadwal.dtj_tglakhir = result.toDate();
                }
            }
            if(i > 0) {
                before = jadwalList.get(i - 1);
            }
            if(!lelang.isExpress() && jadwal.akt_jenis != Tahap.TANDATANGAN_KONTRAK && (i < jadwalList.size() - 1)) {
                next = jadwalList.get(i + 1);
            }
			if (lelang.lls_status.isAktif()) {
				boolean ada_perubahan = !jadwal.equals(jadwalO);
				if(ada_perubahan) {
					if (!CommonUtil.isEmpty(alasan)) {
						jadwal.dtj_keterangan = alasan;
					}
				}
				if (ada_perubahan && jadwal.dtj_tglawal != null	&& jadwal.dtj_tglakhir != null) {
					// simpan perubahan
					History_jadwal historyJadwal = new History_jadwal();
					historyJadwal.dtj_id = jadwalO.dtj_id;
					historyJadwal.hjd_tanggal_edit = currentDate;
					historyJadwal.hjd_tanggal_awal = jadwalO.dtj_tglawal;
					historyJadwal.hjd_tanggal_akhir = jadwalO.dtj_tglakhir;
					historyJadwal.hjd_keterangan = jadwal.dtj_keterangan;
					historyJadwal.save();
					send_email = true;
				}
			}
			jadwalO.dtj_tglawal = jadwal.dtj_tglawal;
			jadwalO.dtj_tglakhir = jadwal.dtj_tglakhir;
			jadwalO.dtj_keterangan = jadwal.dtj_keterangan;
			jadwalO.save();
		}
		// jika paket spse 4 harus update state saat ubah jadwal dan lelang sedang berjalan
		if(lelang.lls_status.isAktif() && !lelang.isLelangV3()) {
			ProcessInstance processInstance = WorkflowDao.findProcessBylelang(lelang);
			WorkflowDao.updateState(processInstance);
			if (lelang.getPemilihan().isLelangExpress()) {
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_PENAWARAN);
				// update jadwal reverse auction
				ReverseAuction reverseAuction = ReverseAuction.findByLelang(lelang.lls_id);
				reverseAuction.ra_waktu_mulai = jadwal.dtj_tglawal;
				reverseAuction.ra_waktu_selesai = jadwal.dtj_tglakhir;
				reverseAuction.save();
				// update shortlist di SIKaP
                boolean saved = false;
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("lls_id", lelang.lls_id);
                if (jadwal != null) {
                    jsonObject.addProperty("dtj_tglawal", jadwal.dtj_tglawal.toString());
                    jsonObject.addProperty("dtj_tglakhir", jadwal.dtj_tglakhir.toString());
                }
                String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
				try {
					WSRequest request = WS.url(BasicCtr.SIKAP_URL + "/services/updateShortlist?q=" + param);
					String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
					if (!CommonUtil.isEmpty(content)) {
						saved = Boolean.parseBoolean(DceSecurityV2.decrypt(content));
					}
				} catch (Exception e) {
					Logger.error(e, "Gagal update shortlist");
				}
                if (!saved) {
                    Logger.error("Gagal update shortlist di SIKaP");
                }
            }
            if(send_email)
            {
                try {
                    MailQueueDao.kirimPerubahanJadwalLelang(lelang, jadwalBefore, jadwalList, currentDate);
                } catch (Exception e) {
                    throw new Exception("Terjadi kendala dalam proses pengiriman email perubahan");
                }
            }
        }
	}


	public static String periodToString(Period p) {
		if(p==null)
			return null;
		p=p.normalizedStandard();
		StringBuilder str=new  StringBuilder();
		if(p.getMonths() > 0)
			str.append(p.getWeeks()).append(" "+Messages.get("lelang.bulan")+" ");
		if(p.getWeeks() > 0)
			str.append(p.getWeeks()).append(" "+Messages.get("lelang.minggu")+" ");
		if(p.getDays() > 0)
			str.append(p.getDays()).append(" "+Messages.get("lelang.hari")+" ");
		if(p.getHours() > 0)
			str.append(p.getHours()).append(" "+Messages.get("lelang.jam")+" ");
		if(p.getMinutes() > 0)
			str.append(p.getMinutes()).append(" "+Messages.get("lelang.menit")+" ");
		return str.toString();
	}
	
	public String getPeriodAsString()
	{		
		Period period=null;
		if(dtj_tglawal != null & dtj_tglakhir !=null) {
			period= new Period(dtj_tglakhir.getTime()-dtj_tglawal.getTime());
		}
		if(period==null)
			return "";
		else
			return periodToString(period);
	}
	
	public String getBusinessDays() {
		
		if(dtj_tglakhir == null || dtj_tglawal == null)
		{
			return "";
		}else {
			return String.valueOf(days(dtj_tglakhir, dtj_tglawal));
		}
	}
	
	static String days(Date start, Date end){
	    //Ignore argument check
		if(start != null || end != null ) {
			 Calendar c1 = Calendar.getInstance();
			    c1.setTime(start);
			    int w1 = c1.get(Calendar.DAY_OF_WEEK);
			    c1.add(Calendar.DAY_OF_WEEK, -w1);

			    Calendar c2 = Calendar.getInstance();
			    c2.setTime(end);
			    int w2 = c2.get(Calendar.DAY_OF_WEEK);
			    c2.add(Calendar.DAY_OF_WEEK, -w2);

			    long days = (c2.getTimeInMillis()-c1.getTimeInMillis())/(1000*60*60*24);
			    long daysWithoutWeekendDays = days-(days*2/7);

			    if (w1 == Calendar.SUNDAY && w2 != Calendar.SATURDAY) {
			        w1 = Calendar.MONDAY;
			    } else if (w1 == Calendar.SATURDAY && w2 != Calendar.SUNDAY) {
			        w1 = Calendar.FRIDAY;
			    } 

			    if (w2 == Calendar.SUNDAY) {
			        w2 = Calendar.MONDAY;
			    } else if (w2 == Calendar.SATURDAY) {
			        w2 = Calendar.FRIDAY;
			    }
			    if(daysWithoutWeekendDays-w1+w2== 0) 
			    	return "";
			    else 
			    	return String.valueOf((daysWithoutWeekendDays-w1+w2)*-1)  + " Hari Kerja";    
		}
		return "";	   
	}
	
	
	
//	public static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
//	    Calendar startCal = Calendar.getInstance();
//	    startCal.setTime(startDate);        
//
//	    Calendar endCal = Calendar.getInstance();
//	    endCal.setTime(endDate);
//
//	    int workDays = 0;
//
//	    //Return 0 if start and end are the same
//	    if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
//	        return 0;
//	    }
//
//	    if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
//	        startCal.setTime(endDate);
//	        endCal.setTime(startDate);
//	    }
//
//	    do {
//	       //excluding start date
//	        startCal.add(Calendar.DAY_OF_MONTH, 1);
//	        if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
//	            ++workDays;
//	        }
//	    } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date
//
//	    return workDays;
//	}
	
	
	
	
	
	
	
	
	
	
	
//	
//	// cek kesamaan jadwal 
//	public boolean isDateEqual(Jadwal jadwal) {
//		long awal = dtj_tglawal != null ? dtj_tglawal.getTime():0;
//		long akhir = dtj_tglakhir != null ? dtj_tglakhir.getTime():0;
//		long awalJadwal  = jadwal != null && jadwal.dtj_tglawal != null ? jadwal.dtj_tglawal.getTime():0;
//		long akhirJadwal  = jadwal != null && jadwal.dtj_tglakhir != null ? jadwal.dtj_tglakhir.getTime():0;
//		return awal == awalJadwal && akhir == akhirJadwal;
//	}

	// find Tahap mulai evaluasi ulang
	public static Jadwal findTahapMulaiEvaluasiUlang(Lelang_seleksi lelang) {
		Jadwal jadwal = null;
		if(lelang.isPascakualifikasi())
		{
			jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.EVALUASI_PENAWARAN);
			if(jadwal ==  null)
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.EVALUASI_PENAWARAN_KUALIFIKASI);
			if(lelang.isDuaFile()) {
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS);
				if(jadwal ==  null)
					jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI);
			}
		}
		else {
			Long count = Aktivitas.count("akt_id in (select akt_id from jadwal where lls_id=? and dtj_tglawal >=?) AND akt_jenis IN ('PEMBUKAAN_PENAWARAN','PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS')",
					lelang.lls_id, BasicCtr.newDate());
			if(count != null && count > 0L)
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.EVALUASI_DOK_PRA);
			else if(lelang.isSatuFile())
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMBUKAAN_PENAWARAN);
			else
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS);
		}
		return jadwal;
	}

	public static Tahap findTahapPemasukanPenawaranUlang(Lelang_seleksi lelang) {
		Tahap tahap = null;
		if (lelang.isDuaTahap()) {
			tahap = Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS;
		} else {
			tahap = Tahap.PEMASUKAN_PENAWARAN;
		}
		return tahap;
	}

	@Override
	protected void prePersist() {
		if(dtj_id == null) { // checking jadwal sebelum insert, menghidari double jadwal
			Jadwal jadwalExist = Jadwal.findByLelangNTahap(lls_id, akt_jenis);
			if(jadwalExist != null)
				jadwalExist.delete();
		}
		super.prePersist();
	}

	// check validitas list jadwal tender
	//semua jadwal akan dicek dulu valid atau tidak, kemudian akan di simpan
	public static void validate(Lelang_seleksi lelang, List<Jadwal> jadwalList, Date currentDate, String alasan, int validasiUrutan) {
		List<Jadwal> jadwalOri = Jadwal.findByLelang(lelang.lls_id);
		boolean express = lelang.getPemilihan().isLelangExpress();
		Jadwal jadwal=null, jadwalO=null,before=null, next=null;
		String fieldName = null;
		for (int i = 0; i < jadwalList.size(); i++) {
			jadwal = jadwalList.get(i);
			jadwalO = jadwalOri.get(i);
			if(jadwalO.akt_urut < validasiUrutan)
				continue;
			Aktivitas akt = Aktivitas.findById(jadwal.akt_id); // load tahap saat ini (akt_jenis null)
			jadwal.akt_jenis = akt.akt_jenis;
			jadwal.akt_id = jadwalO.akt_id;
			fieldName = "jadwal["+i+ ']';
			// jika tidak ada informasi waktu maka set menjadi jam 23:59:59
			// Tp metode ini mungkin ada bugnya, pls dicek lagi (ADY)
			if (jadwal.dtj_tglakhir != null) {
				DateTime dateTime = new DateTime(jadwal.dtj_tglakhir.getTime());
				if (dateTime.getHourOfDay() == 0 && dateTime.getMinuteOfHour() == 0) // jika waktu jadwal terakhir 00:00
				{
					DateTime result = dateTime.withHourOfDay(23).withMinuteOfHour(59);
					jadwal.dtj_tglakhir = result.toDate();
				}
			}
			if(i > 0) {
				before = jadwalList.get(i - 1);
			}
			if(!lelang.isExpress() && jadwal.akt_jenis != Tahap.TANDATANGAN_KONTRAK && (i < jadwalList.size() - 1)) {
				next = jadwalList.get(i + 1);
			}
			try {
				if (lelang.lls_status.isDraft()) {
					jadwal.validate(currentDate, express, before, next, true, true);
				} else if (lelang.lls_status.isAktif()) {
					boolean ada_perubahan = !jadwal.equals(jadwalO);
					if (ada_perubahan) {
						jadwal.validate(currentDate, express, before, next, !jadwal.equalAwal(jadwalO), !jadwal.equalAkhir(jadwalO));
						// jika alasan di-supply oleh parameter, isi keterangan perubahan seluruh jadwal yang berubah
						if(!CommonUtil.isEmpty(alasan)) {
							jadwal.dtj_keterangan = alasan;
						}
						if (CommonUtil.isEmpty(jadwal.dtj_keterangan)) {
							throw new Exception(Messages.get("lelang.hiapjpkk"));
						} else if (jadwal.dtj_keterangan.trim().length() <= 30) {
							throw new Exception(Messages.get("lelang.apjpkkhld"));
						}
					}

				}
			} catch (Exception e){
				Validation.addError(fieldName, e.getLocalizedMessage());
				Logger.error(e, e.getMessage());
			}
		}
	}

	@Override
	public void paramFlash() {
		if(dtj_id == null || dtj_tglawal==null || dtj_tglakhir==null) {
			return;
		}else {
			Scope.Flash flash = Scope.Flash.current();
			flash.put("jadwal_awal_"+dtj_id, dtj_tglawal.getTime());
			flash.put("jadwal_akhir_"+dtj_id, dtj_tglakhir.getTime());
		}
	}

	@Override
	protected void postLoad() {
		if(dtj_id == null)
			return;
		Scope.Flash flash = Scope.Flash.current();
		if(flash != null) {
			if(!StringUtils.isEmpty(flash.get("jadwal_awal_"+dtj_id)))
				dtj_tglawal = new Date(Long.parseLong(flash.get("jadwal_awal_"+dtj_id)));
			if(!StringUtils.isEmpty(flash.get("jadwal_akhir_"+dtj_id)))
				dtj_tglakhir = new Date(Long.parseLong(flash.get("jadwal_akhir_"+dtj_id)));
		}
	}

	// put existing jadwal list to flash
	public static void paramFlash(List<Jadwal> list) {
		if(CollectionUtils.isEmpty(list))
			return;
		for(Jadwal jadwal : list) {
			jadwal.paramFlash();
		}
	}
}
