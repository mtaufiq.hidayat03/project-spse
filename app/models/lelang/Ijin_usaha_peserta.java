package models.lelang;

import models.common.Kualifikasi;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.rekanan.Ijin_usaha;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


@Table(name="IJIN_USAHA_PESERTA")
public class Ijin_usaha_peserta extends BaseModel {
	
	@Id
	public Long ius_id;
	@Id
	public Long psr_id;
	
	public String ius_no;

	public Date ius_tanggal;

	public Date ius_berlaku;

	public String ius_instansi;

	public Long ius_id_attachment;

	public String ius_klasifikasi;

	public String jni_nama;

	//relasi ke Jenis_jin
	public String jni_id;
	
	public String kls_id;
	
	public String getBerlakuWarning() {
		Date now = DateUtil.newDate();
		return ius_berlaku != null && now.after(ius_berlaku) ? Messages.get("rekanan.masa_berlaku_habis") :"";
	}
	
	public void setKualifikasi(Kualifikasi kualifikasi)
	{
		this.kls_id = kualifikasi.id;
	}
	
	@Transient
	public Kualifikasi getKualifikasi()
	{
		return Kualifikasi.findById(kls_id);
	}
	
	public static List<Ijin_usaha_peserta> findBy(Long pesertaId) {
		return find("psr_id=?", pesertaId).fetch();
	}
	
	public static void simpanIjinUsahaPeserta(List<Ijin_usaha> list, Long pesertaId) 
	{
		delete("psr_id=?", pesertaId);
		if(!CommonUtil.isEmpty(list)) {			
			Ijin_usaha_peserta psrijin = null;
		    for(Ijin_usaha ijin_usaha: list) {
		    	psrijin = new Ijin_usaha_peserta();
	    		psrijin.ius_id = ijin_usaha.ius_id;
		        psrijin.psr_id = pesertaId;
		        psrijin.jni_id = ijin_usaha.jni_id;
		        psrijin.kls_id = ijin_usaha.kls_id;
		        psrijin.ius_no = ijin_usaha.ius_no;
		        psrijin.ius_tanggal = ijin_usaha.ius_tanggal;
		        psrijin.ius_berlaku = ijin_usaha.ius_berlaku;
		        psrijin.ius_instansi = ijin_usaha.ius_instansi;
		        psrijin.ius_klasifikasi = ijin_usaha.ius_klasifikasi;
		        psrijin.jni_nama = ijin_usaha.jni_nama;
		        psrijin.ius_id_attachment = ijin_usaha.ius_id_attachment;
		        psrijin.save();
		    }
		}
	}

	@Transient
	public BlobTable getBlob() {
		if(ius_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(ius_id_attachment);
	}
}
