package models.lelang;

import play.db.jdbc.Query;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class Peserta_harga_view implements Serializable {
	public Long psr_id;
	public Double psr_harga;

	public Peserta_harga_view(Long psr_id, Double psr_harga) {
		super();

		this.psr_id = psr_id;
		this.psr_harga = psr_harga;
	}

	public static List<Peserta_harga_view> getListHarga(Long id) {
		String jpql = "select p.psr_id,ph.psr_harga from Lelang_seleksi l, Peserta p,Peserta_harga ph "
				+ "where l.lls_id=p.lls_id and p.psr_id=ph.psr_id and p.is_pemenang=1 and l.lls_id=?";
		return Query.find(jpql, Peserta_harga_view.class, id).fetch();
	}

}