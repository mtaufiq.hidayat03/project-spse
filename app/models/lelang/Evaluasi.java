package models.lelang;

import models.common.Aktivitas;
import models.common.Metode;
import models.common.Tahap;
import models.common.TahapStarted;
import models.jcommon.db.base.BaseModel;
import models.lelang.cast.NilaiEvaluasiView;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Table(name="EVALUASI")
public class Evaluasi extends BaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusEvaluasi {
		SEDANG_EVALUASI, SELESAI, DITOLAK;
		public boolean isSelesai(){
			return this == SELESAI;
		}
		public boolean isSedangEvaluasi(){
			return this == SEDANG_EVALUASI;
		}
	}
	@Enumerated(EnumType.ORDINAL)
	public enum JenisEvaluasi {
		EVALUASI_KUALIFIKASI(0, "Evaluasi Kualifikasi"), 
		EVALUASI_ADMINISTRASI(1, "Evaluasi Administrasi"),
		EVALUASI_TEKNIS(2, "Evaluasi Teknis"), 
		EVALUASI_HARGA(3, "Evaluasi Harga"), 
		EVALUASI_AKHIR(4, "Evaluasi Akhir"),
		PEMBUKTIAN_KUALIFIKASI(5, "Pembuktian Kualifikasi");
		
		public final Integer id;
		public final String label;
		JenisEvaluasi(int id, String nama) {
			this.id = Integer.valueOf(id);
			this.label = nama;
		}
		
		public boolean isAdministrasi() {
			return this == EVALUASI_ADMINISTRASI;
		}
		
		public boolean isTeknis() {
			return this == EVALUASI_TEKNIS;
		}
		
		public boolean isKualifikasi() {
			return this == EVALUASI_KUALIFIKASI;
		}
		
		public boolean isHarga() {
			return this == EVALUASI_HARGA;
		}
		
		public boolean isAkhir() {
			return this == EVALUASI_AKHIR;
		}
		
		public boolean isPembuktian() {
			return this == PEMBUKTIAN_KUALIFIKASI;
		}
		
		public static JenisEvaluasi findById(Integer value) {
			if(value == null)
				return null;
			JenisEvaluasi jenis = null;
			switch (value.intValue()) {
			case 0: jenis = EVALUASI_KUALIFIKASI;break;
			case 1: jenis = EVALUASI_ADMINISTRASI;break;
			case 2: jenis = EVALUASI_TEKNIS;break;
			case 3: jenis = EVALUASI_HARGA;break;
			case 4: jenis = EVALUASI_AKHIR;break;
			case 5: jenis = PEMBUKTIAN_KUALIFIKASI;break;
			}
			return jenis;
		}
		
		public static final List<JenisEvaluasi> JENIS_EVALUASI_URUT_PASCA = Arrays.asList(EVALUASI_ADMINISTRASI, EVALUASI_TEKNIS, EVALUASI_HARGA, EVALUASI_KUALIFIKASI, PEMBUKTIAN_KUALIFIKASI, EVALUASI_AKHIR);
		public static final List<JenisEvaluasi> JENIS_EVALUASI_URUT_PRA = Arrays.asList(EVALUASI_KUALIFIKASI, PEMBUKTIAN_KUALIFIKASI, EVALUASI_ADMINISTRASI, EVALUASI_TEKNIS, EVALUASI_HARGA, EVALUASI_AKHIR);
		public static final List<JenisEvaluasi> JENIS_EVALUASI_KUALIFIKASI = Arrays.asList(EVALUASI_KUALIFIKASI, PEMBUKTIAN_KUALIFIKASI);
	}
	
	@Id(sequence="seq_evaluasi", function="nextsequence")
	public Long eva_id;
	
	@Required
	public JenisEvaluasi eva_jenis;

	public Integer eva_versi;

	public Date eva_tgl_minta;

	public Date eva_tgl_setuju;
	
	@Required
	public StatusEvaluasi eva_status;

	public Long eva_wf_id;

	//relasi ke Lelang_seleksi
	public Long lls_id;
	
	@Transient
	private Lelang_seleksi lelang_seleksi;	

    public Lelang_seleksi getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}

	/**
	 * mendapatkan objek evaluasi dari suatu lelang
	 * jika tidak ada maka akan dibuatkan
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	protected static Evaluasi findNCreate(Long lelangId, JenisEvaluasi jenis){
		Evaluasi evaluasi = findBy(lelangId, jenis);
		if (evaluasi == null) {
			evaluasi = new Evaluasi();
			evaluasi.lls_id = lelangId;
			evaluasi.eva_jenis = jenis;
			evaluasi.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
			Integer versi = Query.find("SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=? AND eva_jenis=?", Integer.class, lelangId, jenis).first();
			if(versi == null || versi == 0)
				versi = 1;
			evaluasi.eva_versi = versi;
			evaluasi.eva_tgl_setuju = controllers.BasicCtr.newDate();
			evaluasi.save();		
		}
		return evaluasi;
	}


	/**
	 * dapatkan informasi versi evaluasi terakhir
	 * @param lelangId
	 * @return
	 */
	public static Integer findCurrentVersi(Long lelangId){
		Integer currentVersi = Query.find("SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=?",Integer.class, lelangId).first();
		if(currentVersi == null || currentVersi == 0)
			currentVersi = 1;
		return currentVersi;
	}
	
	/**
	 * dapatkan jumlah evaluasi 
	 * @param lelangId
	 * @return
	 */
	public static int findCountEvaluasiUlang(Long lelangId) {
		return Query.count("SELECT count(distinct eva_versi) FROM Evaluasi WHERE lls_id=?", Integer.class, lelangId);
	}
	
	/**
	 * dapatkan evaluasi tertentu pada versi tertentu dari sebuah lelang
	 * @param lelangId
	 * @param jenis
	 * @param versi
	 * @return
	 */
	public static Evaluasi findBy(Long lelangId, JenisEvaluasi jenis, Integer versi) {
		return find("lls_id=? and eva_versi=? and eva_jenis=?", lelangId, versi, jenis).first();
	}

	/**
	 * dapatkan evaluasi saat ini dari sebuah lelang
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static Evaluasi findBy(Long lelangId, JenisEvaluasi jenis) {
		return Query.find("SELECT * FROM evaluasi WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1", Evaluasi.class, lelangId, jenis).first();
	}

	/**
	 * dapatkan evaluasi terakhir
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static Evaluasi findLastBy(Long lelangId, JenisEvaluasi jenis, StatusEvaluasi statusEvaluasi) {
		return Query.find("SELECT * FROM evaluasi WHERE lls_id=? AND eva_jenis = ? AND eva_status=? ORDER BY eva_versi DESC limit 1", Evaluasi.class, lelangId, jenis, statusEvaluasi).first();
	}
	
	/**
	 * dapatkan evaluasi administrasi terakhir tertentu dari sebuah lelang
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findAdministrasi(Long lelangId) {
		return findBy(lelangId, JenisEvaluasi.EVALUASI_ADMINISTRASI);
	}
	
	/**
	 * dapatkan evaluasi Teknis terakhir tertentu dari sebuah lelang
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findTeknis(Long lelangId) {
		return findBy(lelangId, JenisEvaluasi.EVALUASI_TEKNIS);
	}
	
	/**
	 * dapatkan evaluasi Harga terakhir tertentu dari sebuah lelang
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findHarga(Long lelangId) {
		return findBy(lelangId, JenisEvaluasi.EVALUASI_HARGA);
	}
	
	/**
	 * dapatkan evaluasi Kualifikasi terakhir tertentu dari sebuah lelang
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findKualifikasi(Long lelangId) {
		return findBy(lelangId, JenisEvaluasi.EVALUASI_KUALIFIKASI);
	}
	
	/**
	 * dapatkan Pembuktian Kualifikasi terakhir tertentu dari sebuah lelang
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findPembuktian(Long lelangId) {
		return findBy(lelangId, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI);
	}
	
	/**
	 * dapatkan evaluasi Akhir (Penetapan Pemenang) terakhir tertentu dari sebuah lelang
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findPenetapanPemenang(Long lelangId) {
		return findBy(lelangId, JenisEvaluasi.EVALUASI_AKHIR);
	}

	/**
	 * dapatkan evaluasi Administrasi , jika tidak ada maka akan dibuat baru
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findNCreateAdministrasi(Long lelangId) {
		return findNCreate(lelangId, JenisEvaluasi.EVALUASI_ADMINISTRASI);
	}
	
	/**
	 * dapatkan evaluasi Teknis , jika tidak ada maka akan dibuat baru
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findNCreateTeknis(Long lelangId) {
		return findNCreate(lelangId, JenisEvaluasi.EVALUASI_TEKNIS);
	}
	
	/**
	 * dapatkan evaluasi Harga , jika tidak ada maka akan dibuat baru
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findNCreateHarga(Long lelangId) {
		return findNCreate(lelangId, JenisEvaluasi.EVALUASI_HARGA);
	}
	
	/**
	 * dapatkan evaluasi Kualifikasi , jika tidak ada maka akan dibuat baru
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findNCreateKualifikasi(Long lelangId) {
		return findNCreate(lelangId, JenisEvaluasi.EVALUASI_KUALIFIKASI);
	}
	
	/**
	 * dapatkan pembuktian kualifikasi , jika tidak ada maka akan dibuat baru
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findNCreatePembuktian(Long lelangId) {
		return findNCreate(lelangId, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI);
	}
	/**
	 * dapatkan evaluasi akhir , jika tidak ada maka akan dibuat baru
	 * @param lelangId
	 * @return
	 */
	public static Evaluasi findNCreatePenetapanPemenang(Long lelangId) {
		return findNCreate(lelangId, JenisEvaluasi.EVALUASI_AKHIR);
	}
	
	/**
	 * dapatkan list evaluasi tertentu dari sebuah lelang
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static List<Evaluasi> findListBy(Long lelangId, JenisEvaluasi jenis) {
		return find("lls_id=? and eva_jenis=? order by eva_versi asc", lelangId, jenis).fetch();
	}
	
	/**
	 * hasil evaluasi akan muncul jika sudah terdapat pemenang (return true)
	 *  
	 * @param lelangId
	 * @since 3.2.4
	 */
	public static boolean isShowHasilEvaluasi(Long lelangId){
		Tahap[] tahaps = new Tahap[] { Tahap.PENGUMUMAN_PEMENANG_AKHIR, Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR, Tahap.PENUNJUKAN_PEMENANG };
		if(Aktivitas.isTahapStarted(lelangId, tahaps)) 
		{
			Evaluasi evaluasi = findPenetapanPemenang(lelangId);
			return (evaluasi != null && evaluasi.eva_status.isSelesai());
		}
		return false;		
	}

	/**
	 *  hasil evaluasi kualifkasi akan muncul jika sudah terdapat pemenang (return true)
	 * @param lelangId
	 * @since 3.2.4
	 */
	public static boolean isShowHasilEvaluasiKualifikasi(Long lelangId){
		Tahap[] tahaps = new Tahap[] { Tahap.PENGUMUMAN_HASIL_PRA , Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK};
		if(Aktivitas.isTahapStarted(lelangId, tahaps)) {
			Evaluasi evaluasi = findKualifikasi(lelangId);
			return (evaluasi != null && evaluasi.eva_status.isSelesai());
		}
		return false;
		
	}

	/**
	 *  hasil evaluasi teknis akan muncul jika sudah terdapat pemenang (return true)
	 * @param lelangId
	 * @since 3.2.4
	 */
	public static boolean isShowHasilEvaluasiTeknis(Long lelangId){
		if(Aktivitas.isTahapStarted(lelangId, Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS)) {
			Evaluasi evaluasi = findTeknis(lelangId);
			return (evaluasi != null && evaluasi.eva_status.isSelesai());
		}
		return false;
		
	}
	
	/**
	 * dapatkan list peserta yang lolos sampai evaluasi tertentu
	 * @return
	 */
	public List<Nilai_evaluasi> findPesertaList(boolean sortHarga) {
		if(sortHarga) {
			return Nilai_evaluasi.findWithUrutHarga(eva_id);
		}
		return Nilai_evaluasi.findBy(eva_id);
	}

	public List<Nilai_evaluasi> findPesertaWithUrutNego() {
		return Nilai_evaluasi.findWithUrutHargaNegosiasi(eva_id);
	}

	public List<Nilai_evaluasi> findPemenangList() {
		return Nilai_evaluasi.findByWithNevUrut(eva_id);
	}

	public List<Nilai_evaluasi> findPesertaListWithUrutSkor() {
		return Nilai_evaluasi.findWithUrutSkor(eva_id);
	}

	public Nilai_evaluasi findNilaiEvaluasiByPesertaAndEvaluasi(Long psr_id) {

		return Nilai_evaluasi.findByPesertaAndEvaluasi(psr_id, eva_id);
	}

	public long countPesertaList() {		
		return Nilai_evaluasi.count("eva_id=?", eva_id);
	}

	public List<Nilai_evaluasi> findPesertaList() {
		return findPesertaList(false);
	}

	public List<Nilai_evaluasi> findPesertaLulusEvaluasiList() {
		return Nilai_evaluasi.find("eva_id=? AND nev_lulus=1", eva_id).fetch();
	}

	/**
	 * untuk mendapatkan informasi jumlah peserta yang lulus evaluasi
	 * @return
	 */
	public long countLulusEvaluasi() {
		return Nilai_evaluasi.count("eva_id=? AND nev_lulus=1", eva_id);
	}

	/**
	 * Mendapatkan Informasi next evaluasi setelah satu evaluasi selesai dilakukan
	 *
	 * @param metode
     * @return
     */
	public Evaluasi findNextEvaluasi(Metode metode) {
		Evaluasi evaluasi_next = null;
		if(metode == Metode.PASCA_DUA_FILE_KUALITAS) { // pasca 2 file konsultan perorangan
			switch (eva_jenis) {
				case EVALUASI_ADMINISTRASI:evaluasi_next = Evaluasi.findNCreateKualifikasi(lls_id);break;
				case EVALUASI_TEKNIS:evaluasi_next = Evaluasi.findNCreatePembuktian(lls_id);break;
				case EVALUASI_HARGA:evaluasi_next = Evaluasi.findNCreatePenetapanPemenang(lls_id);break;
				case EVALUASI_KUALIFIKASI:evaluasi_next = Evaluasi.findNCreateTeknis(lls_id);break;
				case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = Evaluasi.findNCreateHarga(lls_id);break;
			}
		} else if(metode.kualifikasi.isPasca()) { // evaluasi proses pasca kualifikasi
			switch (eva_jenis) {
				case EVALUASI_ADMINISTRASI:evaluasi_next = Evaluasi.findNCreateKualifikasi(lls_id);break;
				case EVALUASI_TEKNIS:evaluasi_next = Evaluasi.findNCreateHarga(lls_id);break;
				case EVALUASI_HARGA:evaluasi_next = Evaluasi.findNCreatePembuktian(lls_id);break;
				case EVALUASI_KUALIFIKASI:evaluasi_next = Evaluasi.findNCreateTeknis(lls_id);break;
				case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = Evaluasi.findNCreatePenetapanPemenang(lls_id);break;
			}
		} else { // evaluasi proses prakualifikasi
			switch (eva_jenis) {
				case EVALUASI_KUALIFIKASI:evaluasi_next = Evaluasi.findNCreatePembuktian(lls_id);break;
				case EVALUASI_ADMINISTRASI:evaluasi_next = Evaluasi.findNCreateTeknis(lls_id);break;
				case EVALUASI_TEKNIS:evaluasi_next = Evaluasi.findNCreateHarga(lls_id);break;
				case EVALUASI_HARGA:evaluasi_next = Evaluasi.findNCreatePenetapanPemenang(lls_id);break;
			}
		}
		return evaluasi_next;
	}

	/**
	 * delete nilai evaluasi untuk evaluasi selanjutnya
	 * @param evaluasi
	 * @param metode
	 * @param psr_id
	 * @return
	 */
	public void deleteNextEvaluasi(Evaluasi evaluasi, Metode metode, Long psr_id) {
		Evaluasi evaluasi_next = null;
		if(metode == Metode.PASCA_DUA_FILE_KUALITAS) { // pasca 2 file konsultan perorangan
			switch (evaluasi.eva_jenis) {
				case EVALUASI_ADMINISTRASI:evaluasi_next = Evaluasi.findKualifikasi(lls_id);break;
				case EVALUASI_TEKNIS:evaluasi_next = Evaluasi.findPembuktian(lls_id);break;
				case EVALUASI_HARGA:evaluasi_next = Evaluasi.findPenetapanPemenang(lls_id);break;
				case EVALUASI_KUALIFIKASI:evaluasi_next = Evaluasi.findTeknis(lls_id);break;
				case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = Evaluasi.findHarga(lls_id);break;
			}
		}else if(metode.kualifikasi.isPasca()) { // lelang pascakualifikasi
			switch (evaluasi.eva_jenis) {
				case EVALUASI_ADMINISTRASI:evaluasi_next = Evaluasi.findKualifikasi(lls_id);break;
				case EVALUASI_TEKNIS:evaluasi_next = Evaluasi.findHarga(lls_id);break;
				case EVALUASI_HARGA:evaluasi_next = Evaluasi.findPembuktian(lls_id);break;
				case EVALUASI_KUALIFIKASI:evaluasi_next = Evaluasi.findTeknis(lls_id);break;
				case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = Evaluasi.findPenetapanPemenang(lls_id);break;
			}
		} else { // lelang prakualifikasi
			switch (evaluasi.eva_jenis) {
				case EVALUASI_KUALIFIKASI:evaluasi_next = Evaluasi.findPembuktian(lls_id);break;
				case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = Evaluasi.findAdministrasi(lls_id);break;
				case EVALUASI_ADMINISTRASI:evaluasi_next = Evaluasi.findTeknis(lls_id);break;
				case EVALUASI_TEKNIS:evaluasi_next = Evaluasi.findHarga(lls_id);break;
				case EVALUASI_HARGA:evaluasi_next = Evaluasi.findPenetapanPemenang(lls_id);break;
			}
		}

		if(evaluasi_next != null) {
			deleteNextEvaluasi(evaluasi_next, metode, psr_id); // recursive
		}
		Checklist_evaluasi.delete("nev_id in (select nev_id from nilai_evaluasi where eva_id=? and psr_id=?)", evaluasi.eva_id, psr_id);
		Nilai_evaluasi.delete("eva_id=? and psr_id=?", evaluasi.eva_id, psr_id);
	}

	/**
	 * simpan nilai evaluasi peserta
	 * @param peserta
	 * @param nilai
	 * @param metode
     */
	public void simpanNilaiPeserta(Peserta peserta, Nilai_evaluasi nilai, Metode metode, Lelang_seleksi lelang) {
		nilai.psr_id = peserta.psr_id;
		nilai.eva_id = eva_id;
		if (nilai.nev_lulus != null && nilai.nev_lulus.isLulus()) {
			nilai.nev_uraian = null;
		}
		nilai.save();
		Evaluasi evaluasi_next = findNextEvaluasi(metode);
		if(evaluasi_next != null) {
			if (nilai.nev_lulus.isLulus()) { // peserta diijinkan melanjutkan ke proses evaluasi selanjutnnya
				Nilai_evaluasi nilai_next = Nilai_evaluasi.findNCreateBy(evaluasi_next.eva_id, peserta, evaluasi_next.eva_jenis);
				if (!metode.kualifikasi.isPasca() && evaluasi_next.eva_jenis.isPembuktian()) {
					nilai_next.nev_skor = nilai.nev_skor;
				}
				if (evaluasi_next.eva_jenis.isAkhir()) {
					Evaluasi evaluasi_harga = Evaluasi.findHarga(peserta.lls_id);
					Nilai_evaluasi nilaiHarga = Nilai_evaluasi.findBy(evaluasi_harga.eva_id, peserta.psr_id);
					nilai_next.nev_harga_terkoreksi = nilaiHarga.nev_harga_terkoreksi;
				}
				nilai_next.save();
			} else {
				//tidak lulus maka evaluasi harus dihapus
				deleteNextEvaluasi(evaluasi_next, metode, peserta.psr_id);
			}
		}
		if(lelang.isPascakualifikasi() && !lelang.isItemized() && !lelang.isKonsultansi() && !lelang.isJkKonstruksi()) {
			Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lelang.lls_id);
			boolean useAuction = (lelang.getEvaluasi().isGugur() || lelang.getEvaluasi().isHargaTerendahAmbangBatas())
					&& (penetapan != null && Nilai_evaluasi.count("eva_id=?", penetapan.eva_id) == 2);
			lelang.lls_metode_penawaran = useAuction ? Lelang_seleksi.MetodePenawaranHarga.AUCTION:Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION;
			lelang.save();
		} else {
			lelang.lls_metode_penawaran = Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION;
			lelang.save();
		}
	}

	// evaluasi ulang penawaran
	public static void evaluasiUlang(Lelang_seleksi lelang)
	{
		Evaluasi evaluasiUlang = null;
		if(lelang.isPascakualifikasi())
		{
			for(JenisEvaluasi jenis : JenisEvaluasi.values())
			{
				Evaluasi evaluasiExist = findBy(lelang.lls_id, jenis);
				if(evaluasiExist == null)
					continue;
				evaluasiUlang = new Evaluasi();
				evaluasiUlang.lls_id = lelang.lls_id;
				evaluasiUlang.eva_jenis = jenis;
				evaluasiUlang.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
				evaluasiUlang.eva_versi = evaluasiExist.eva_versi + 1;
				evaluasiUlang.eva_tgl_setuju = evaluasiExist.eva_tgl_setuju;
				evaluasiUlang.save();
			}
		}
		else if(lelang.isPrakualifikasi()) {
				for (JenisEvaluasi jenis : JenisEvaluasi.values()) {
					if (jenis.isKualifikasi() || jenis.isPembuktian())
						continue;
					Evaluasi evaluasiExist = findBy(lelang.lls_id, jenis);
					if(evaluasiExist == null)
						continue;
					evaluasiUlang = new Evaluasi();
					evaluasiUlang.lls_id = lelang.lls_id;
					evaluasiUlang.eva_jenis = jenis;
					evaluasiUlang.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
					evaluasiUlang.eva_versi = evaluasiExist.eva_versi + 1;
					evaluasiUlang.eva_tgl_setuju = evaluasiExist.eva_tgl_setuju;
					evaluasiUlang.save();
				}
		}
		lelang.kirim_pengumuman_pemenang = false;
		lelang.save();
		// hapus persetujuan pemenang lelang jika ada
		Persetujuan.delete("lls_id = ? AND pst_jenis = ?", lelang.lls_id, Persetujuan.JenisPersetujuan.PEMENANG_LELANG);
		Query.update("UPDATE peserta SET is_pemenang = 0, is_pemenang_verif = 0 WHERE lls_id=?", lelang.lls_id);
	}

	// evaluasi ulang prakualifikasi
	public static void evaluasiUlangKualifikasi(Lelang_seleksi lelang) {
		if(lelang.isPascakualifikasi())
			return;
		// hanya boleh pada tender prakualifikasi
//		List<Nilai_evaluasi> ne = null;
//		List<Checklist_evaluasi> ce = null;
//		Evaluasi evaUl = null;
//		Nilai_evaluasi nilaiEvaUlang = null;
//		Checklist_evaluasi chkEvaUlang = null;
		Evaluasi evaluasiUlang = null;
		for (JenisEvaluasi jenis : JenisEvaluasi.values()) {
			if (jenis.isKualifikasi() || jenis.isPembuktian()) {
				Evaluasi evaluasiExist = findBy(lelang.lls_id, jenis);
				if(evaluasiExist == null)
					continue;
				evaluasiUlang = new Evaluasi();
				evaluasiUlang.lls_id = lelang.lls_id;
				evaluasiUlang.eva_jenis = jenis;
				evaluasiUlang.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
				Integer evaVersi = evaluasiExist.eva_versi + 1;
				evaluasiUlang.eva_versi = evaVersi;
				evaluasiUlang.eva_tgl_setuju = evaluasiExist.eva_tgl_setuju;
				evaluasiUlang.save();
			}
		}
		lelang.kirim_pengumuman_pemenang_pra = false;
		lelang.is_kualifikasi_tambahan = false;
		lelang.save();
		// hapus persetujuan pemenang lelang jika ada
		Persetujuan.delete("lls_id = ? AND pst_jenis = ?", lelang.lls_id, Persetujuan.JenisPersetujuan.PEMENANG_PRAKUALIFIKASI);
	}


	@Transient
	public List<NilaiEvaluasiView> nilaiEvaluasiViews;

	/**
	 * langsung join dengan table peserta dan rekanan, agar tidak manual attach ketika looping
	 * @return
	 */
	public List<NilaiEvaluasiView> findAllNilaiEvaluasi() {
		return NilaiEvaluasiView.findAllByEvaluasi(eva_id);
	}

	/**
	 * langsung join dengan table peserta dan rekanan, agar tidak manual attach ketika looping
	 * @return
	 */
	public List<NilaiEvaluasiView> findAllNilaiEvaluasiWithUrutHarga() {
		return NilaiEvaluasiView.findAllByEvaluasiWithUrutHarga(eva_id);
	}

	/**
	 * chek apakah ada data skor per evaluasi peserta (@link nilai_evaluasi)
	 * fungsi ini tujuanny untuk show skor ke publik
	 * @return
	 */
	public boolean isAnySkor() {
		return Nilai_evaluasi.count("nev_skor is not null AND eva_id = ?", eva_id) > 0;
	}

	public boolean isShowHargaNego() {
		return Nilai_evaluasi.count("nev_harga_negosiasi is not null AND eva_id=?", eva_id) > 0;
	}

	public boolean isEvaluasiUlang() {
		return eva_versi != null && eva_versi > 1;
	}

	// chek apakah evaluasi sudah dilakukan/blom dilakukan sama pokja , chek melalui status lulus/tidak lulusny peserta
	public boolean isSudahEvaluasi() {
		long count = Nilai_evaluasi.count("eva_id=?", eva_id);
		long countSudahEvaluasi = Nilai_evaluasi.count("nev_lulus is not null AND eva_id = ?", eva_id);
		return count > 0 && count == countSudahEvaluasi;
	}

	public boolean isEditable() {
		if(eva_jenis.isAkhir() && eva_status.isSelesai())
			return false;
		else {
			boolean allow = eva_status.isSedangEvaluasi();
			Evaluasi akhir = findPenetapanPemenang(lls_id);
			if(akhir != null && allow)
				return akhir.eva_status.isSedangEvaluasi();
			return allow;
		}
	}
}
