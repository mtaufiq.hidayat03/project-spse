package models.lelang;

import controllers.BasicCtr;
import ext.FormatUtils;
import models.agency.*;
import models.common.*;
import models.handler.DokLelangDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.kontrak.SskkPpkContent;
import models.lelang.Checklist_master.JenisChecklist;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.templates.Template;
import play.templates.TemplateLoader;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.*;


@Table(name="DOK_LELANG")
public class Dok_lelang extends BaseModel {

	@Enumerated(EnumType.ORDINAL)
	public enum JenisDokLelang {
		
		DOKUMEN_PRAKUALIFIKASI(0, "ldp.dc_pr"),// tidak dipake di spse 4, tp masih dipake di 3.5
		DOKUMEN_LELANG(1, "lelang.doc_pemilihan"),
		DOKUMEN_LELANG_PRA(2, "lelang.doc_kualifikasi"),
		DOKUMEN_ADENDUM(3, "lelang.doc_adendum"),  // tidak dipake di spse 4, tp masih dipake di 3.5
		DOKUMEN_TEKNIS(4, "ldp.dt"), // tidak dipake di spse 4, tp masih dipake di 3.5
		DOKUMEN_ADENDUM_PRA(5, "ldp.dap"); // tidak dipake di spse 4, tp masih dipake di 3.5
		
		public final Integer value;
		public final String label;
		
		private JenisDokLelang(int value, String label) {
			this.value = Integer.valueOf(value);
			this.label = label;
		}
		
		public static JenisDokLelang fromValue(Integer value){
			JenisDokLelang jenis = null;
			if(value!=null){
				switch (value.intValue()) {
					case 0:	jenis = DOKUMEN_PRAKUALIFIKASI;break;
					case 1:	jenis = DOKUMEN_LELANG;break;
					case 2:	jenis = DOKUMEN_LELANG_PRA;break;
					case 3:	jenis = DOKUMEN_ADENDUM;break;
					case 4:	jenis = DOKUMEN_TEKNIS;break;				
				}
			}
			return jenis;
		}
		
		public boolean isAdendum(){
			return this == DOKUMEN_ADENDUM;
		}

		public boolean isAdendumPra(){
			return this == DOKUMEN_ADENDUM_PRA;
		}
		
		public boolean isDokLelang(){
			return this == DOKUMEN_LELANG;
		}
		
		public boolean isKualifikasi(){
			return this == DOKUMEN_LELANG_PRA;
		}

		public String getLabel()
		{
			return Messages.get(label);
		}
	}
	
	@Id(sequence="seq_dok_lelang", function="nextsequence")
	public Long dll_id;

	public String dll_nama_dokumen;

	public JenisDokLelang dll_jenis;

	public Long dll_id_attachment;

	//relasi ke Lelang_seleksi
	public Long lls_id;	
	
	@Transient
	private Lelang_seleksi lelang_seleksi;
	@Transient
	private Dok_lelang_content dokLelangContent;

    public Lelang_seleksi getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}

	public Dok_lelang_content getDokLelangContent() {
    	if (dokLelangContent == null && dll_id != null) {
			dokLelangContent = Dok_lelang_content.findBy(dll_id);
		}
		return dokLelangContent;
	}

	public String getAuditUser() {
        return Pegawai.findBy(audituser).peg_nama;
    }
	
	public Kategori getKategori() {
		return getLelang_seleksi().getKategori();
    }
	
	@Transient
	public BlobTable getDokumen() {
		if(dll_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dll_id_attachment);
	}
	
	@Transient
	public List<BlobTable> getDokumenList() {
		if(dll_id_attachment == null)
			return null;
		return BlobTableDao.listById(dll_id_attachment);
	}
	
	@Transient
	public String getDownloadUrl() throws IllegalAccessException {
		return getDokumen().getDownloadUrl(DokLelangDownloadHandler.class);
		
	}
	
	@Transient
	public String getDownloadUrl(BlobTable blob) throws IllegalAccessException {
		return blob.getDownloadUrl(DokLelangDownloadHandler.class);
	}
	
	
	@Transient
	public List<Checklist> getSyaratIjinUsaha() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id=1 order by chk_id ASC", dll_id, versi).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratIjinUsahaBaru() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return Checklist.find("dll_id=? AND chk_versi=? and ckm_id=50 order by chk_id ASC", dll_id, versi).fetch();
	}

	@Transient
	public List<Checklist> getSyaratKualifikasi() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by chk_id, ckm_id ASC", dll_id, versi, JenisChecklist.CHECKLIST_KUALIFIKASI).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratKualifikasiAdministrasi() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 50 and (kgr_id=? or kgr_id is null) and ckm_jenis=?) order by ckm_id ASC, chk_id ASC", dll_id, versi, getKategori(), JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratKualifikasiTeknis() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 50 and (kgr_id=? or kgr_id is null) and ckm_jenis=?) order by ckm_id ASC, chk_id ASC", dll_id, versi, getKategori(), JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratKualifikasiKeuangan() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 50 and (kgr_id=? or kgr_id is null) and ckm_jenis=?) order by ckm_id ASC, chk_id ASC", dll_id, versi, getKategori(), JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratTeknis() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by ckm_id", dll_id, versi, JenisChecklist.CHECKLIST_TEKNIS).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratAdministrasi() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by ckm_id", dll_id, versi, JenisChecklist.CHECKLIST_ADMINISTRASI).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratAdministrasiUnchecked() {
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=? and ckm_checked=0) order by ckm_id", dll_id, versi, JenisChecklist.CHECKLIST_ADMINISTRASI).fetch();
	}
	
	@Transient
	public List<Checklist> getSyaratHarga() {
    	Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return Checklist.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by ckm_id", dll_id, versi, JenisChecklist.CHECKLIST_HARGA).fetch();
	}

	/**
	 * Mendapatkan LDP content terakhir yang berlaku
	 * digunakan oleh Apendo
	 * @return
	 */
	@Transient
	public LDPContent getLdpContent() {
		String content = Query.find("SELECT dll_ldp FROM dok_lelang_content WHERE dll_id=? AND dll_ldp is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, LDPContent.class);
		return null;
	}

	/**
	 * Mendapatkan Daftar Kuantitas terakhir yang berlaku
	 * digunakan oleh Apendo
	 * @return
	 */
	@Transient
	public DaftarKuantitas getRincianHPS() {
		String content = Query.find("SELECT dll_dkh FROM dok_lelang_content WHERE dll_id=? AND dll_dkh is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, DaftarKuantitas.class);
		return null;
	}

	/**
	 * Mendapatkan SSKK terakhir yang berlaku
	 * digunakan oleh Kontrak dan Adendum
	 * @return
	 */
	@Transient
	public SskkContent getSskkContent() {
		String content = Query.find("SELECT dll_sskk FROM dok_lelang_content WHERE dll_id=? AND dll_sskk is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, SskkContent.class);
		return null;
	}

	@Transient
	public SskkPpkContent getSskkContentToSkkkPpk(){
		String content = Query.find("SELECT dll_sskk FROM dok_lelang_content WHERE dll_id=? AND dll_sskk is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, SskkPpkContent.class);
		return null;
	}

	public Date getTanggalDokumen() {
		return Query.find("SELECT dll_tglSDP FROM dok_lelang_content WHERE dll_id=? AND dll_tglSDP is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", Date.class, dll_id).first();
	}


	@Transient
	public String getJenisDokumen() {
		if(dll_jenis.isKualifikasi()) {
			return "Dokumen Kualifikasi";
		}
		return "Dokumen Pemilihan";
	}

	public void simpanCetak(Lelang_seleksi lelang, Dok_lelang_content dok_lelang_content, InputStream dok) {
		DokPersiapan dokPersiapan = DokPersiapan.findByPaket(lelang.pkt_id);

		try {
				BlobTable blob = BlobTableDao.saveInputStream(ARCHIEVE_MODE.ARCHIEVE, dok, dll_nama_dokumen);
				dll_id_attachment = blob.blb_id_content;
				dok_lelang_content.dll_content_attachment = blob.blb_id_content;
				dok_lelang_content.dll_modified = false;
				dok_lelang_content.dll_syarat_updated = false;
				dok_lelang_content.dll_ldk_updated= false;

				if(dokPersiapan != null){
					dokPersiapan.dp_modified = false;
					dokPersiapan.save();
				}

		} catch (Exception e) {
			Logger.error(e, "Gagal simpan dokumen lelang %s", lls_id);
			dok_lelang_content.dll_modified = true;
		}

		dok_lelang_content.save();
		save();
	}
		
	
	public static Dok_lelang findBy(Long lelangId, JenisDokLelang jenis) {
		return find("lls_id=? and dll_jenis=?", lelangId, jenis).first();		
	}
	
	/**
	 * dapatkan dokumen lelang, jika tidak ada maka akan dibuat baru,
	 * Gunakan saat proses submit terkait dokumen lelang
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static Dok_lelang findNCreateBy(Long lelangId, JenisDokLelang jenis) {
		Dok_lelang dok_lelang = find("lls_id=? and dll_jenis=?", lelangId, jenis).first();
		if(dok_lelang == null) {
			dok_lelang = new Dok_lelang();
			dok_lelang.lls_id = lelangId;
			dok_lelang.dll_jenis = jenis;
			dok_lelang.save();
		}
		return dok_lelang;
	}

	public boolean isAllowAdendum(Lelang_seleksi lelang) {
		if (dll_jenis.isKualifikasi()) {
			return lelang.getTahapStarted().isAllowAdendumPra();
		}else if (dll_jenis.isDokLelang()) {
			return lelang.getTahapStarted().isAllowAdendum();
		}
		return false;
	}
	/**
	 * status dok lelang boleh diedit atau tidak
	 * @return
	 */
	public boolean isEditable(Lelang_seleksi lelang, Date currentDate) {	
		if (lelang.lls_status.isDraft()) {
			if (dll_jenis.isDokLelang() && lelang.isPrakualifikasi()) {
				return true;
			} else {
				return !lelang.isSedangPersetujuan();
			}
		} else if (lelang.lls_status.isAktif() && dll_jenis.isDokLelang()) {
				if(lelang.isPrakualifikasi()) {
					Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
					Evaluasi evakualifikasi = Evaluasi.findKualifikasi(lelang.lls_id);
					if(pembuktian != null && evakualifikasi != null && pembuktian.eva_status.isSelesai() && evakualifikasi.eva_status.isSelesai())
						return false;
					List<Jadwal> list = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PENGUMUMAN_HASIL_PRA, Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK);
					if(!CollectionUtils.isEmpty(list)) {
						Jadwal jadwal = list.get(0);
						if (jadwal != null && jadwal.dtj_tglawal != null && jadwal.dtj_tglawal.after(currentDate)) {
							// jika sebelum jadwal pengumuman hasil pra, maka dokumen lelang masih dianggap draft bukan adendum
							return true;
						}
					}
			} 
		}
		return false;
	}

	/**
	 * simpan Rincian HPS
	 * author Arief Ardiyansah
	 * @param lelang
	 * @param data
	 * @param fixed
     * @throws Exception
     */
	public void simpanHps(Lelang_seleksi lelang, String data, boolean fixed, boolean enableViewHps) throws Exception{
		Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dll_id, lelang);
		DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,fixed);
		Paket paket = Paket.findById(lelang.pkt_id);
		if (dk.total == null)
			throw new Exception(Messages.get("lelang.abmr_hps"));
		else if (dk.total > paket.pkt_pagu) {
			throw new Exception(Messages.get("lelang.nt_hps_pagu"));
		} else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket
			if(!dok_lelang_content.isAdendum()) {
				paket.pkt_hps = dk.total;
				paket.pkt_hps_enable = enableViewHps;
				paket.save();
			}
			dok_lelang_content.dkh = dk;
			simpan(lelang.lls_status, paket.pkt_nama, dok_lelang_content);			
		}
	}

	public void simpan(StatusLelang status, String namaPaket, Dok_lelang_content dok_lelang_content) {		
		if(CommonUtil.isEmpty(dll_nama_dokumen))
			dll_nama_dokumen = String.format("%s [%s] - %s.pdf", getJenisDokumen(), lls_id, StringUtils.abbreviate(namaPaket, 200));
		dok_lelang_content.dll_modified = true;
		dok_lelang_content.save();
		save();
	}

	public UploadInfo simpan(Dok_lelang_content dok_lelang_content, Lelang_seleksi lelang, File dokumen) {
		BlobTable blob = null;
		try {
			if(dokumen != null)
				blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, dokumen, dok_lelang_content.dll_content_attachment);
			if(blob != null)
				dok_lelang_content.dll_content_attachment = blob.blb_id_content;
			String content = cetakPreview(lelang, dok_lelang_content);
			if (!StringUtils.isEmpty(content)) {
				dok_lelang_content.dll_content_html = content.replaceAll("<br>", "<br/>");
			}
			dok_lelang_content.dll_modified = false;
			dok_lelang_content.dll_syarat_updated = false;
			dok_lelang_content.dll_ldk_updated = false;
			dok_lelang_content.save();
			if(dok_lelang_content.dll_versi == 1) {
				dll_id_attachment = blob.blb_id_content;
			}
			if(dok_lelang_content.dll_kontrak_pembayaran != null) {
				if(dok_lelang_content.isAdendum()) {
					JenisKontrak jenisKontrakLama = lelang.getKontrakPembayaran();
					Dok_lelang_content prevDokLelangContent = Dok_lelang_content.findBy(dll_id, dok_lelang_content.dll_versi - 1);
					prevDokLelangContent.dll_kontrak_pembayaran = jenisKontrakLama.id;
					prevDokLelangContent.save();
				}
				lelang.setKontrakPembayaran(JenisKontrak.findById(dok_lelang_content.dll_kontrak_pembayaran));
				lelang.save();
			}
			if(dok_lelang_content.dkh != null) {
				Paket paket = Paket.findByLelang(lelang.lls_id);
				paket.pkt_hps = dok_lelang_content.dkh.total;
				paket.save();
				if(lelang.isExpress()) {
					ReverseAuction reverseAuction = ReverseAuction.findByLelang(lelang.lls_id);
					if(reverseAuction != null) {
						reverseAuction.ra_min_penawaran = dok_lelang_content.dkh.total;
						reverseAuction.save();
					}
				}
			}
			save();

			DokPersiapan dokPersiapan = DokPersiapan.findByPaket(lelang.pkt_id);
			dokPersiapan.dp_modified = false;
			dokPersiapan.save();

		}catch (Exception e){
			Logger.error(e, Messages.get("lelang.save_doc_ggl")+" : %s", e.getMessage());
		}
		return UploadInfo.findBy(blob);
	}

	public static String cetak_content(Lelang_seleksi lelang, Dok_lelang dok_lelang, Dok_lelang_content dok_lelang_content, Template template) {
		Paket paket = Paket.findById(lelang.pkt_id);
		Kategori kategori = lelang.getKategori();
		if(CommonUtil.isEmpty(dok_lelang_content.dll_sbd_versi))
			dok_lelang_content.dll_sbd_versi = Dok_lelang_content.DEFAULT_VERSI;
		Metode metode = lelang.getMetode();
		Map<String, Object> params = new HashMap<>();
		boolean adendum = dok_lelang_content.dll_versi > 1;
		params.put("website", BasicCtr.LPSE_URL);
		params.put("adendum", adendum);
		params.put("paket", paket);
		params.put("kualifikasi", metode.kualifikasi);
		params.put("prakualifikasi", metode.kualifikasi.isPra());
		params.put("dok_lelang", dok_lelang);
		params.put("kategori", kategori);
		params.put("kontrak", lelang.getKontrakPembayaran());
		params.put("pemilihan", lelang.getPemilihan());
		params.put("dokumen", metode.dokumen);
		params.put("evaluasi", lelang.getEvaluasi());
		params.put("nomor", dok_lelang_content.dll_nomorSDP);
		params.put("tanggal", FormatUtils.formatDateInd(dok_lelang_content.dll_tglSDP));
		if(adendum) {
			params.put("versi", dok_lelang_content.dll_versi);
			DateTime tanggal_perubahan = new DateTime(dok_lelang_content.auditupdate.getTime());
			String hari = FormatUtils.day[(tanggal_perubahan.getDayOfWeek()==7 ? 0 : tanggal_perubahan.getDayOfWeek())];
			int tgl = tanggal_perubahan.getDayOfMonth();
			String bulan = FormatUtils.month[tanggal_perubahan.getMonthOfYear()-1];
			int tahun = tanggal_perubahan.getYear();
            String info = hari + " tanggal " + tgl + " bulan " + bulan +
                    " tahun " + tahun + '(' + FormatUtils.formatDateInd(tanggal_perubahan.toDate()) + ')';
            params.put("hari", info);
		}
		if (!lelang.getPemilihan().isLelangExpress() && dok_lelang_content.sskk_content!=null) {
			if(dok_lelang_content.sskk_content != null && dok_lelang_content.sskk_content.inspeksi_tgl != null){
				DateTime tgl_insp = new DateTime(dok_lelang_content.sskk_content.inspeksi_tgl.getTime());
				String hari = FormatUtils.day[(tgl_insp.getDayOfWeek()==7 ? 0 : tgl_insp.getDayOfWeek())];
				params.put("hari", hari);
			}
		}
		if(!dok_lelang.dll_jenis.isKualifikasi()) {
			Jadwal jadwalPembDok= Jadwal.getJadwalByJadwalAndAct(lelang.lls_id, 105);//pembukaan_jadwal(akt_id==105) asep
			params.put("jadwalPembDok", jadwalPembDok);
			params.put("syaratTeknis", dok_lelang.getSyaratTeknis());
			params.put("ldpcontent", dok_lelang_content.ldpcontent);
			params.put("ldk_modified", dok_lelang_content.dll_ldk_updated);
			params.put("syarat_modified", dok_lelang_content.dll_syarat_updated);
			String[][] tenagaAhli =  (dok_lelang_content.ldpcontent != null && dok_lelang_content.ldpcontent.bobotTenagaAhli!= null)? dok_lelang_content.ldpcontent.parseTenagaAhliToArray() : null;
			params.put("tenagaAhli", tenagaAhli);
			if(dok_lelang_content.sskk_content != null) {
				params.put("sskk_content", dok_lelang_content.sskk_content);
				params.put("sskk_content_kontrak_pembayaran", JenisKontrak.findById(dok_lelang_content.sskk_content.kontrak_pembayaran).label);
			}
			params.put("daftar_kuantitas", dok_lelang_content.dkh);
			Paket_satker paket_satker = Paket_satker.find("pkt_id=?", paket.pkt_id).first();
			params.put("satker", Satuan_kerja.findById(paket_satker.stk_id));
			List<BlobTable> list = dok_lelang_content.getDokSpek();
			if (!CommonUtil.isEmpty(list)) {
				StringBuilder spekTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : list) {
					spekTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				spekTemplate.append("</ol>");
				params.put("dok_spek", spekTemplate.toString());
			}
			List<BlobTable> listDokLainnya = dok_lelang_content.getDokLainnya();
			if (!CommonUtil.isEmpty(listDokLainnya)) {
				StringBuilder lainnyaTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : listDokLainnya) {
					lainnyaTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				lainnyaTemplate.append("</ol>");
				params.put("dok_lainnya", lainnyaTemplate.toString());
			}
			params.put("lelang", lelang);
		}
		params.put("doc_path", ConfigurationDao.CDN_URL+"/documents");
		params.put("panitia", lelang.getPanitia());
		params.put("ijinList", dok_lelang.getSyaratIjinUsaha());
		params.put("syaratList", dok_lelang.getSyaratKualifikasi());
		params.put("ijinBaruList", dok_lelang.getSyaratIjinUsahaBaru());
		params.put("syaratAdminList", dok_lelang.getSyaratKualifikasiAdministrasi());
		params.put("syaratTeknisList", dok_lelang.getSyaratKualifikasiTeknis());
		params.put("syaratKeuanganList", dok_lelang.getSyaratKualifikasiKeuangan());
		
		return template.render(params);
	}

	public boolean isEmptyDokumen() {
		return BlobTableDao.count("blb_id_content=?", dll_id_attachment) == 0;
	}


	public String cetakPreview(Lelang_seleksi lelang, Dok_lelang_content dok_lelang_content) {
		Paket paket = Paket.findById(lelang.pkt_id);
		Kategori kategori = lelang.getKategori();
		if(CommonUtil.isEmpty(dok_lelang_content.dll_sbd_versi))
			dok_lelang_content.dll_sbd_versi = Dok_lelang_content.DEFAULT_VERSI;
		Metode metode = lelang.getMetode();
		Map<String, Object> params = new HashMap<String, Object>();
		boolean adendum = dok_lelang_content.dll_versi > 1;
		params.put("dok_lelang_content", dok_lelang_content);
		params.put("adendum", adendum);
		params.put("lelang", lelang);
		params.put("paket", paket);
		params.put("kualifikasi", metode.kualifikasi);
		params.put("prakualifikasi", metode.kualifikasi.isPra());
		params.put("dok_lelang", this);
		params.put("kategori", kategori);
		params.put("kontrak", lelang.getKontrakPembayaran());
		params.put("pemilihan", lelang.getPemilihan());
		params.put("dokumen", metode.dokumen);
		params.put("evaluasi", lelang.getEvaluasi());
		params.put("nomor", dok_lelang_content.dll_nomorSDP);
		params.put("tanggal", FormatUtils.formatDateInd(dok_lelang_content.dll_tglSDP));
		params.put("tahunAnggaran", paket.getTahunAnggaran());
		if(adendum) {
			params.put("versi", dok_lelang_content.dll_versi);
			DateTime tanggal_perubahan = new DateTime(dok_lelang_content.auditupdate.getTime());
			String hari = FormatUtils.day[(tanggal_perubahan.getDayOfWeek()==7 ? 0 : tanggal_perubahan.getDayOfWeek())];
			int tgl = tanggal_perubahan.getDayOfMonth();
			String bulan = FormatUtils.month[tanggal_perubahan.getMonthOfYear()-1];
			int tahun = tanggal_perubahan.getYear();
			String info = hari + " "+Messages.get("lelang.tanggal")+" " + tgl + " "+Messages.get("lelang.bulan")+" " + bulan +
					" "+Messages.get("lelang.tahun")+" " + tahun + " (" + FormatUtils.formatDateInd(tanggal_perubahan.toDate()) + ')';
			params.put("hari", info);
		}
		if(!dll_jenis.isKualifikasi()) {
			params.put("syaratAdmin", getSyaratAdministrasi());
			params.put("syaratTeknis", getSyaratTeknis());
			params.put("syaratHarga", getSyaratHarga());
			params.put("ldpcontent", dok_lelang_content.ldpcontent != null ? dok_lelang_content.ldpcontent : getLdpContent());
			params.put("masa_berlaku", dok_lelang_content.ldpcontent != null ? dok_lelang_content.ldpcontent.masa_berlaku_penawaran : getMasaBerlakuPenawaran());
			params.put("syarat_modified", dok_lelang_content.dll_syarat_updated);
			params.put("syarat_cetak", !dll_jenis.isKualifikasi());
			params.put("daftar_kuantitas", dok_lelang_content.dkh != null ?dok_lelang_content.dkh : getRincianHPS());
			Paket_satker paket_satker = Paket_satker.find("pkt_id=?", paket.pkt_id).first();
			params.put("satker", Satuan_kerja.findById(paket_satker.stk_id));
			List<BlobTable> list = dok_lelang_content.getDokSpek();
			if (!CommonUtil.isEmpty(list)) {
				StringBuilder spekTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : list) {
					spekTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				spekTemplate.append("</ol>");
				params.put("dok_spek", spekTemplate.toString());
			}
			List<BlobTable> listDokLainnya = dok_lelang_content.getDokLainnya();
			if (!CommonUtil.isEmpty(listDokLainnya)) {
				StringBuilder lainnyaTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : listDokLainnya) {
					lainnyaTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				lainnyaTemplate.append("</ol>");
				params.put("dok_lainnya", lainnyaTemplate.toString());
			}
			List<BlobTable> sskk = new ArrayList<>();
			if(lelang.lls_status.isDraft()){
				DokPersiapan dokPersiapan = paket.getDokPersiapan();
				if (dokPersiapan != null) {
					sskk = dokPersiapan.getDokSskkAttachment();
				}
			} else {
				sskk = dok_lelang_content.getDokSskk();
			}
			if (!CommonUtil.isEmpty(sskk)) {
				StringBuilder sskkTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : sskk) {
					sskkTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				sskkTemplate.append("</ol>");
				params.put("dok_sskk", sskkTemplate.toString());
			}
		}
		params.put("ldk_modified", dok_lelang_content.dll_ldk_updated);
		params.put("panitia", lelang.getPanitia());
		params.put("ijinList", getSyaratIjinUsaha());
		params.put("syaratList", getSyaratKualifikasi());
		params.put("ijinBaruList", getSyaratIjinUsahaBaru());
		params.put("syaratAdminList", getSyaratKualifikasiAdministrasi());
		params.put("syaratTeknisList", getSyaratKualifikasiTeknis());
		params.put("syaratKeuanganList", getSyaratKualifikasiKeuangan());
		Template template = TemplateLoader.load("/lelang/dokumen/template/adendum.html");
		return template.render(params);
	}

	public boolean allowPersetujuan() {
		List<Dok_lelang_content> dok_lelang_contents = Dok_lelang_content.find("dll_id = ? AND dll_modified='TRUE'", dll_id).fetch();
		return dok_lelang_contents == null || dok_lelang_contents.isEmpty();
	}

	/**
	 * Mendapatkan Masa Berlaku penawaran terakhir yang berlaku
	 * digunakan oleh Apendo
	 * @return
	 */
	public Integer getMasaBerlakuPenawaran() {
		LDPContent ldpContent = getLdpContent();
		if(ldpContent != null && ldpContent.masa_berlaku_penawaran != null)
			return ldpContent.masa_berlaku_penawaran;
		return Integer.valueOf(0);
	}

	public boolean isSyaratKualifikasiBaru() {
		String jenis = StringUtils.join(JenisChecklist.CHECKLIST_KUALIFIKASI_BARU, ',');
		Integer versi = Checklist.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		Long countChecklist = Checklist.count("dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=0)", dll_id, versi);
		Long countCheclistBaru = Checklist.count("dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", dll_id, versi);
		if (countChecklist.intValue() == 0 && countCheclistBaru.intValue() == 0)
			return true;
		else if (countCheclistBaru.intValue() > 0)
			return true;
		else
			return false;
	}


}
