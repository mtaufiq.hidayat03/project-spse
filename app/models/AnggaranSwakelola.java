package models;

import ext.FormatUtils;
import models.agency.Paket_anggaran_swakelola;
import models.agency.Ppk;
import models.agency.Satuan_kerja;
import models.agency.contracts.AnggaranSwakelolaContract;
import models.common.SumberDana;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Model {@code Anggaran} merepresentasikan tabel Anggaran pada database.
 *
 * @author abdul wahid
 */
@Table(name="EKONTRAK.ANGGARAN_SWAKELOLA")
public class AnggaranSwakelola extends BaseModel implements AnggaranSwakelolaContract{

    /**
     * ID anggaran
     */
    @Id(sequence="seq_anggaran", function="nextsequence")
    public Long ang_id;
    /**
     * kode Anggaran
     */
    public String ang_koderekening;
    /**
     * Deskripsi nilai anggaran
     */
    public Double ang_nilai;
    /**
     * uraian kegiatan terkait anggaran tesebut
     */
    public String ang_uraian;
    /**
     * tahun anggaran
     */
    public Integer ang_tahun;
    /**
     * Satker yang memiliki objek anggaran ini
     */
    public Long stk_id;
    /**
     * Sumber dana anggaran
     */
    public SumberDana sbd_id;

    @Transient
    private Satuan_kerja satuan_kerja;

    /**
     * @deprecated untuk mendapatkan ppk harap menggunakan paket_anggaran*/
    @Deprecated
    @Transient
    private Ppk ppk;

    public Satuan_kerja getSatuan_kerja() {
        if(satuan_kerja == null)
            satuan_kerja = Satuan_kerja.findById(stk_id);
        return satuan_kerja;
    }

    public Ppk getPpk() {
        if (ppk != null) {
            return ppk;
        }
        Paket_anggaran_swakelola paket_anggaran = Paket_anggaran_swakelola.find("ang_id=?", ang_id).first();
        if (paket_anggaran != null && paket_anggaran.ppk_id != null)
            ppk = Ppk.findById(paket_anggaran.ppk_id);
        return ppk;
    }

    /**
     * Manipulasi nilai anggaran menjadi string dalam format rupiah
     *
     * @return string nilai anggaran dalam format rupiah
     */
    @Transient
    public String getNilai() {
        return FormatUtils.formatCurrencyRupiah(this.ang_nilai);
    }

    /**
     * Manipulasi parameter {@code nilai} yang didapat dari form menjadi tipe
     * data {@code Double}
     *
     * @param nilai string nilai anggaran
     */
    @Transient
    public void setNilai(String nilai) {
        this.ang_nilai = Double.valueOf(nilai.trim().replace("Rp", "").replace(",", "."));
    }

    /**
     * mendapatkan list anggaran dari paket
     * @return
     */
    public static List<AnggaranSwakelola> findByPaket(Long paketId) {
        return find("ang_id in (select ang_id from ekontrak.paket_anggaran_swakelola where swk_id=?)", paketId).fetch();
    }

    public static AnggaranSwakelola findByAnggaranId(Long angId) {
        return find("ang_id =?", angId).first();
    }

    @Override
    public String getPpkNama() {
        return getPpk() != null ? getPpk().getNamaPegawai() : "";
    }

    @Override
    public Long getId() {
        return ang_id;
    }

    @Override
    public boolean isPpkExist() {
        return ppk != null;
    }

    @Override
    public String getKodeRekening() {
        return ang_koderekening;
    }

    @Override
    public Double getAngNilai() {
        return ang_nilai;
    }

    @Override
    public Integer getAngTahun() {
        return ang_tahun;
    }

    @Override
    public String getSbdLabel() {
        return sbd_id != null ? sbd_id.getLabel() : "";
    }
}
