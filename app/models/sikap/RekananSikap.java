package models.sikap;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import models.common.ConfigurationDao;
import models.nonlelang.DraftPesertaNonSpk;
import models.nonlelang.DraftPesertaPl;
import models.nonlelang.nonSpk.RealisasiPenyediaNonSpk;
import models.nonlelang.nonSpk.RealisasiPenyediaSwakelola;
import play.Logger;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;
import utils.Encrypt;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// class dari SIKaP, terkait fitur
public class RekananSikap {

    public Long rkn_id;
    public String rkn_nama;
    public String rkn_alamat;
    public String rkn_kodepos;
    public String rkn_npwp;
    public String rkn_email;
    public String rkn_telepon;
    public Long repo_id;
    public String kab_name;
    public String prop_name;

    public static List<Map<String, String>> getListPenyediaByNpwpFromSikap(String npwp, String nama, Long kabupatenId, String jenisIjin, Long lelangId, String lelangType) throws UnsupportedEncodingException {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("kbp_id", kabupatenId);

        if (npwp != null && !npwp.isEmpty())
            jsonObject.addProperty("npwp", npwp);

        if (nama != null && !nama.isEmpty())
            jsonObject.addProperty("nama_perusahaan", nama);

        if (jenisIjin != null)
            jsonObject.addProperty("jni_id", jenisIjin.replaceAll("\\s",""));

        String params = URLs.encodePart(Encrypt.encryptSikapParams(jsonObject.toString().getBytes("UTF-8")));
        String url = BasicCtr.SIKAP_URL+"/services/getRekananByNPWP?q="+params;
        String response = null;
        try{
            WSRequest request = WS.url(url);
            response = request.body("content").timeout(ConfigurationDao.CONNECTION_TIMEOUT).post().getString();
        } catch (Exception e) {
            Logger.error(e, "Terjadi Kesalahan Koneksi Ke SIKaP : " + e.getLocalizedMessage());
        }

        if (response != null && !response.isEmpty()) {
            String result = Encrypt.decryptSikapParams(response);
            try {
                List<Map<String, String>> rekananList = new ArrayList<>();
                RekananSikap[] list = new Gson().fromJson(result, RekananSikap[].class);
                for (RekananSikap rekananJson : list) {
                    Map<String, String> rekanan = new HashMap<>();
                    rekanan.put("rkn_id",rekananJson.rkn_id.toString());
                    rekanan.put("rkn_nama", rekananJson.rkn_nama);
                    rekanan.put("rkn_npwp", rekananJson.rkn_npwp);
                    rekanan.put("rkn_email", rekananJson.rkn_email);
                    rekanan.put("rkn_alamat", rekananJson.rkn_alamat);
                    rekanan.put("rkn_telepon", rekananJson.rkn_telepon);
//                    rekanan.put("repo_id", rekananJson.repo_id.toString());
                    if(lelangType.equals("non_spk")) {
                        rekanan.put("isSudahTerdaftar", DraftPesertaNonSpk.isSudahTerdaftarDiDraft(lelangId, rekananJson.rkn_id) ? "1" : "0");
                    } else if(lelangType.equals("pl")) {
                        rekanan.put("isSudahTerdaftar", DraftPesertaPl.isSudahTerdaftarDiDraft(lelangId, rekananJson.rkn_id) ? "1" : "0");
                    } else if(lelangType.equals("realisasi_non_spk")) {
                        rekanan.put("isSudahTerdaftar", RealisasiPenyediaNonSpk.isSudahTerdaftar(lelangId, rekananJson.rkn_id) ? "1" : "0");
                    } else if(lelangType.equals("realisasi_swakelola")) {
                        rekanan.put("isSudahTerdaftar", RealisasiPenyediaSwakelola.isSudahTerdaftar(lelangId, rekananJson.rkn_id) ? "1" : "0");
                    }
                    if (rekanan != null)
                        rekananList.add(rekanan);
                }
                return rekananList;

            } catch (Exception e) {
                Logger.error(e,"Terjadi Kesalahan Parse Response SIKaP : " + e.getLocalizedMessage());
            }
        }
        return new ArrayList<>();

    }
}
