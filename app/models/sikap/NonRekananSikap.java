package models.sikap;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import models.common.ConfigurationDao;
import models.nonlelang.nonSpk.RealisasiNonPenyediaSwakelola;
import play.Logger;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;
import utils.Encrypt;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class NonRekananSikap {

    public Long id;
    public String nama;
    public String alamat;
    public String npwp;
    public String telp;
    public String email;
    public String inputby;
    public Integer repo_id;
    public Date createdat;
    public Date auditupdate;
    public String peg_role;
    public boolean deleted;

    public static List<Map<String, String>> getListNonPenyediaFromSikap(String npwp, String nama, Long realisasiId) throws ExecutionException,
            InterruptedException, UnsupportedEncodingException {

        JsonObject jsonObject = new JsonObject();
        if (npwp != null && !npwp.isEmpty())
            jsonObject.addProperty("npwp", npwp);
        if (nama != null && !nama.isEmpty())
            jsonObject.addProperty("nama_perusahaan", nama);
        String params = URLs.encodePart(Encrypt.encryptSikapParams(jsonObject.toString().getBytes("UTF-8")));
        StringBuilder url = new StringBuilder();
        url.append(BasicCtr.SIKAP_URL).append("/services/getNonRekanan?q=").append(params);
        String response = null;
        try {
            WSRequest request = WS.url(url.toString());
            response = request.body("content").timeout(ConfigurationDao.CONNECTION_TIMEOUT).post().getString();
        } catch (Exception e) {
            Logger.error("Terjadi Kesalahan Koneksi Ke SIKaP : " + e.getLocalizedMessage());
        }
        if (response != null && !response.isEmpty()) {
            String result = Encrypt.decryptSikapParams(response);
            try {
                List<Map<String, String>> rekananList = new ArrayList<>();
                NonRekananSikap[] list = new Gson().fromJson(result, NonRekananSikap[].class);
                for (NonRekananSikap rekananJson : list) {
                    Map<String, String> rekanan = new HashMap<>();
                    rekanan.put("rkn_id", rekananJson.id.toString());
                    rekanan.put("rkn_nama", rekananJson.nama);
                    rekanan.put("rkn_npwp", rekananJson.npwp);
                    rekanan.put("rkn_email", rekananJson.email);
                    rekanan.put("rkn_alamat", rekananJson.alamat);
                    rekanan.put("rkn_telepon", rekananJson.telp);
                    rekanan.put("isSudahTerdaftar", RealisasiNonPenyediaSwakelola.isSudahTerdaftar(realisasiId,rekananJson.id) ? "1":"0");
                    if (rekanan != null)
                        rekananList.add(rekanan);
                }
                return rekananList;
            } catch (Exception e) {
                Logger.error(e,"Terjadi Kesalahan Parse Response SIKaP : " + e.getLocalizedMessage());
            }
        }
        return null;

    }
}
