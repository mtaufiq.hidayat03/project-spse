package models.auditor;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.Paket;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Table(name="SKAUDITOR")
public class Skauditor extends BaseModel {
	
	@Id(sequence="seq_skauditor", function="nextsequence")
	public Long skid;

	@Required
	public String skno;

	@Required
	@As(binder=DateBinder.class)
	public Date skmulai;

	@Required
	@As(binder=DateBinder.class)
	public Date skakhir;

	public Long skattachmentid;

	@Transient
	public BlobTable getBlob() {
		if (skattachmentid == null) 
			return null;
		return BlobTableDao.getLastById(skattachmentid);
	}
	
	public String getMasaBerlaku() {
		return FormatUtils.formatDate(skmulai, skakhir);
	}
	
	public static boolean createPaketAuditor( Long skid,Long paketId) {
		
		if(paketId != null){
			Paket paket = Paket.findById(paketId);
			Skauditor skauditor = Skauditor.findById(skid);			
			Paket_skauditor paketsk = new Paket_skauditor();
			paketsk.pkt_id=paket.pkt_id;
			paketsk.skid=skauditor.skid;
			paketsk.save();
			return true;
		}else{
			return false;
		}
		
		
	}

	// TODO: Belum handling delete cascade terhadap tabel auditor_skauditor
	public static void hapus(Long skauditor_id) {
		Skauditor skauditor = findById(skauditor_id);
		List <Auditor_skauditor> auditorsk = Auditor_skauditor.find("skid=?", skauditor.skid).fetch();
		List <Paket_skauditor> pktsk = Paket_skauditor.find("skid=?", skauditor.skid).fetch();
		if(auditorsk!=null  ){
			for(Auditor_skauditor as : auditorsk){
				as.delete();
			}
		}
		if(pktsk!=null){
			for(Paket_skauditor pkt : pktsk){
				pkt.delete();
			}
		}
		
		skauditor.delete(); // hapus entr
		//PegawaiDao.hapus(pegawai.peg_id); // hapus juga objek yang berkaitan dengan pegawai
	}
		
	public static final ResultSetHandler<String[]> resultsetAuditor = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("skid");
			tmp[1] = rs.getString("skno");
			tmp[2] = FormatUtils.formatDateInd(rs.getTimestamp("skmulai"));
			tmp[3] = FormatUtils.formatDateInd(rs.getTimestamp("skakhir"));
			return tmp;
		}
	};
	
	public static void simpanSk(Skauditor skauditor, File attachment, String hapus) throws Exception {
		Skauditor oldSk = skauditor.skid != null ? Skauditor.findById(skauditor.skid) : null; // load dr db karena tidak disubmit di form
		Long skattachmentid = oldSk != null ? oldSk.skattachmentid : null;
		if (attachment != null) {
			BlobTable bt = null;
			if (skattachmentid == null) {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, attachment);
			} else {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, attachment, skattachmentid);
			}
			skauditor.skattachmentid = bt.blb_id_content;
		} else {
			if (skattachmentid != null && hapus == null) {
				skauditor.skattachmentid = null;
				BlobTable.delete("blb_id_content=?", skattachmentid);
			} else if (skattachmentid != null) {
				skauditor.skattachmentid = skattachmentid;
			}
		}
		skauditor.save();
	}
}
