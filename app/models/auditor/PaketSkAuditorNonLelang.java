package models.auditor;

import models.agency.Paket_pl;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Created by Lambang on 11/8/2017.
 */
@Table(name="EKONTRAK.PAKET_SKAUDITOR")
public class PaketSkAuditorNonLelang extends BaseModel {

    //relasi ke Skauditor
    @Id
    public Long skid;

    //relasi ke Paket
    @Id
    public Long pkt_id;

    @Transient
    private SkAuditorNonLelang skauditor;
    @Transient
    private Paket_pl paket;

    public SkAuditorNonLelang getSkauditor() {
        if(skauditor == null)
            skauditor = SkAuditorNonLelang.findById(skid);
        return skauditor;
    }

    public Paket_pl getPaket() {
        if(paket == null)
            paket = Paket_pl.findById(pkt_id);
        return paket;
    }

}
