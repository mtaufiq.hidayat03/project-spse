package models.auditor;

import ext.FormatUtils;
import models.agency.Pegawai;
import models.common.StatusLelang;
import models.common.StatusPl;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Table(name="AUDITOR")
public class Auditor extends BaseModel {
	
	@Id(sequence="seq_auditor", function="nextsequence")
	public Long auditorid;

	//relasi ke Pegawai
	public Long peg_id;
	
	@Transient
	private Pegawai pegawai;	
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}

	/**
	 * Fungsi {@code createAuditorFromPegawai} digunakan untuk membuat objek
	 * {@code Auditor} berdasarkan informasi objek {@code Pegawai}.
	 *
	 * @param pegawaiId objek pegawai yang berperan sebagai auditor
	 */
	public static void createAuditorFromPegawai(Long pegawaiId) {
		Auditor auditor = Auditor.find("peg_id=?", pegawaiId).first();
    	if (auditor == null) {
    		auditor = new Auditor();
		}
//		Skauditor skauditor = new Skauditor();
		auditor.peg_id = pegawaiId;
		auditor.save(); // simpan objek auditor
	}
	
	public static void createPilihAuditor(Long skid, Long[] chkAuditor){
		if(chkAuditor != null){
			for (Long auditorid : chkAuditor) {
				if(auditorid != null) {
					Auditor auditor = Auditor.findById(auditorid);
					Skauditor skauditor = Skauditor.findById(skid);					
					Auditor_skauditor auditor_skauditor = new Auditor_skauditor();
					auditor_skauditor.auditorid= auditor.auditorid;
					auditor_skauditor.skid =skauditor.skid;
					auditor_skauditor.save();
				}
			}
		}		
	}

	public static void createPilihAuditorNonLelang(Long skid, Long[] chkAuditor){
		if(chkAuditor != null){
			for (Long auditorid : chkAuditor) {
				if(auditorid != null) {
					Auditor auditor = Auditor.findById(auditorid);
					SkAuditorNonLelang skauditor = SkAuditorNonLelang.findById(skid);
					AuditorSkAuditorNonLelang auditor_skauditor = new AuditorSkAuditorNonLelang();
					auditor_skauditor.auditorid= auditor.auditorid;
					auditor_skauditor.skid =skauditor.skid;
					auditor_skauditor.save();
				}
			}
		}
	}

	/**
	 * Fungsi {@code hapus} digunakan untuk menghapus objek {@code Auditor}
	 * sekaligus dengan objek-objek lain yang berkaitan dengan penghapusan objek
	 * ini.
	 *
	 * @param auditor_id id auditor
	 */
	public static void hapus(Long auditor_id) {
		Auditor auditor = findById(auditor_id);
		  List<Auditor_skauditor> sk = Auditor_skauditor.find("auditorid=?", auditor_id).fetch();
		  for (Auditor_skauditor au : sk) {
              au.delete();
          }
		
		auditor.delete(); // hapus objek auditor
		Pegawai.hapus(auditor.peg_id); // hapus juga objek yang berkaitan dengan pegawai
	}
	
	
	/*public static String tablePaketAuditor(final Long satkerId, final Integer tahun)
	{
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_pagu"};
		String sql=null;
		String sql_count=null;
		if (satkerId != null) {
			sql = "SELECT DISTINCT p.pkt_id, pkt_nama, pkt_pagu FROM  paket p, satuan_kerja s, Anggaran a , paket_anggaran pa "
					+ "WHERE p.stk_id=s.stk_id and a.stk_id = s.stk_id and p.pkt_id=pa.pkt_id and a.ang_id=pa.ang_id "
					+ " and p.stk_id=" + satkerId + " and ang_tahun=" + tahun;
			sql_count ="SELECT COUNT(p.pkt_id) FROM paket p, satuan_kerja s , Anggaran a , paket_anggaran pa "
					+"WHERE p.stk_id=s.stk_id and a.stk_id = s.stk_id and p.pkt_id=pa.pkt_id and a.ang_id=pa.ang_id " +
					" and p.stk_id="+satkerId+" and ang_tahun="+tahun;
		} else {
			sql = "SELECT DISTINCT  p.pkt_id, pkt_nama, pkt_pagu FROM  paket p, satuan_kerja s, Anggaran a , paket_anggaran pa "
					+ "WHERE p.stk_id=s.stk_id and a.stk_id = s.stk_id and p.pkt_id=pa.pkt_id and a.ang_id=pa.ang_id "
					+ " and ang_tahun=" + tahun;
			 sql_count ="SELECT COUNT(p.pkt_id) FROM paket p, satuan_kerja s,  Anggaran a , paket_anggaran pa "
						+"WHERE p.stk_id=s.stk_id and a.stk_id = s.stk_id and p.pkt_id=pa.pkt_id and a.ang_id=pa.ang_id " +
						" and ang_tahun="+tahun;
		}
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetPaketAuditor);
	}*/
	
	public static final ResultSetHandler<String[]> resultsetAuditor = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			tmp[0] = rs.getString("auditorid");
			tmp[1] = rs.getString("peg_nama");
			tmp[2] = rs.getString("peg_nip");
			tmp[3] = rs.getString("peg_namauser");				
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetPaketAuditor = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[3];
			StatusLelang statusLelang = StatusLelang.fromValue(rs.getInt("lls_status"));
			tmp[0] = rs.getString("lls_id");
			tmp[1] = rs.getString("pkt_nama") +(statusLelang.isDitutup() ? " <span class=\"badge  badge-danger\">"+statusLelang.label+"</span>":"");
			tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_pagu"));
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetNonPaketAuditor = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[3];
			StatusPl statuspl = StatusPl.fromValue(rs.getInt("lls_status"));
			tmp[0] = rs.getString("lls_id");
			tmp[1] = rs.getString("pkt_nama") +(statuspl.isDitutup() ? " <span class=\"badge  badge-danger\">"+statuspl.label+"</span>":"");
			tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_pagu"));
			return tmp;
		}
	};
	/**
	 * flaq auditor diijinkan atau tidak akses paket
	 * @param auditorid
	 * @param paketId
	 * @return
	 */
	public static boolean isAllowAuditorAksesPaket(Long auditorid, Long paketId)
	{
		String sql="SELECT auditorid FROM paket_skauditor ps, auditor_skauditor aus WHERE ps.skid=aus.skid AND ps.pkt_id=?";	
		List<Long> auditorIdList = Query.find(sql,Long.class, paketId).fetch();
		if(!CommonUtil.isEmpty(auditorIdList))
			return auditorIdList.contains(auditorid);
		return false;
	}

	public static boolean isAllowAuditorAksesPaketNonLelang(Long auditorid, Long paketId)
	{
		String sql="SELECT auditorid FROM ekontrak.paket_skauditor ps, ekontrak.auditor_skauditor aus WHERE ps.skid=aus.skid AND ps.pkt_id=?";
		List<Long> auditorIdList = Query.find(sql,Long.class, paketId).fetch();
		if(!CommonUtil.isEmpty(auditorIdList))
			return auditorIdList.contains(auditorid);
		return false;
	}
}
