package models.auditor;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Created by Lambang on 11/8/2017.
 */
@Table(name="EKONTRAK.AUDITOR_SKAUDITOR")
public class AuditorSkAuditorNonLelang extends BaseModel {

    //relasi ke Auditor
    @Id
    public Long auditorid;

    //relasi ke Skauditor
    @Id
    public Long skid;

    @Transient
    private Auditor auditor;

    @Transient
    private SkAuditorNonLelang skauditor;

    public Auditor getAuditor() {
        if(auditor == null)
            auditor = Auditor.findById(auditorid);
        return auditor;
    }

    public SkAuditorNonLelang getSkauditor() {
        if(skauditor == null)
            skauditor = SkAuditorNonLelang.findById(skid);
        return skauditor;
    }

}
