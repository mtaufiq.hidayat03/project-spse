package models.auditor;

import play.db.jdbc.Query;

import java.util.List;

public class PaketSkAuditor {
	
	private static final String SQL = "" +
			"SELECT skid, p.pkt_id, l.lls_id, pkt_nama, pkt_pagu FROM paket_skauditor psa \n" +
			"INNER JOIN paket p ON p.pkt_id = psa.pkt_id\n" +
			"INNER JOIN lelang_seleksi l ON l.pkt_id = p.pkt_id AND l.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=l.pkt_id) \n" +
			"WHERE skid = ? " +
			"ORDER BY lls_id DESC";

	public Long skid;

	public Long pkt_id;

	public Long lls_id;
	
	public String pkt_nama;
	
	public Double pkt_pagu;

    public static List<PaketSkAuditor> findAllBySkId(long skid) {
        List<PaketSkAuditor> list =  Query.find(SQL, PaketSkAuditor.class, skid).fetch();
        return list;
    }
}
