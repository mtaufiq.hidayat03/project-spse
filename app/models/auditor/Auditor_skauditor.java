package models.auditor;


import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;


@Table(name="AUDITOR_SKAUDITOR")
public class Auditor_skauditor extends BaseModel {
	
	//relasi ke Auditor
	@Id
	public Long auditorid;

	//relasi ke Skauditor
	@Id
	public Long skid;
	
	@Transient
	private Auditor auditor;
	
	@Transient
	private Skauditor skauditor;
	
	public Auditor getAuditor() {
		if(auditor == null)
			auditor = Auditor.findById(auditorid);
		return auditor;
	}
	
	public Skauditor getSkauditor() {
		if(skauditor == null)
			skauditor = Skauditor.findById(skid);
		return skauditor;
	}	
}
