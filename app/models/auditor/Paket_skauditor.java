package models.auditor;

import models.agency.Paket;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;


@Table(name="PAKET_SKAUDITOR")
public class Paket_skauditor extends BaseModel {
	
	//relasi ke Skauditor
	@Id
	public Long skid;

	//relasi ke Paket
	@Id
	public Long pkt_id;
	
	@Transient
	private Skauditor skauditor;
	@Transient
	private Paket paket;
	
	public Skauditor getSkauditor() {
		if(skauditor == null)
			skauditor = Skauditor.findById(skid);
		return skauditor;
	}

	public Paket getPaket() {
		if(paket == null)
			paket = Paket.findById(pkt_id);
		return paket;
	}
	
}
