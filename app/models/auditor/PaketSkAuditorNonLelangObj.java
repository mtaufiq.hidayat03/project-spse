package models.auditor;

import play.db.jdbc.Query;

import java.util.List;

/**
 * Created by Lambang on 11/8/2017.
 */
public class PaketSkAuditorNonLelangObj {

    private static final String SQL = "" +
            "SELECT skid, p.pkt_id, l.lls_id, pkt_nama, pkt_pagu FROM ekontrak.paket_skauditor psa \n" +
            "INNER JOIN ekontrak.paket p ON p.pkt_id = psa.pkt_id\n" +
            "INNER JOIN ekontrak.nonlelang_seleksi l ON l.pkt_id = p.pkt_id AND l.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM ekontrak.nonlelang_seleksi WHERE pkt_id=l.pkt_id) \n" +
            "WHERE skid = ? " +
            "ORDER BY lls_id DESC";

    public Long skid;

    public Long pkt_id;

    public Long lls_id;

    public String pkt_nama;

    public Double pkt_pagu;

    public static List<PaketSkAuditorNonLelangObj> findAllBySkId(long skid) {
        List<PaketSkAuditorNonLelangObj> list =  Query.find(SQL, PaketSkAuditorNonLelangObj.class, skid).fetch();
        return list;
    }

}
