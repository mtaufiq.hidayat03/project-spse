package models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import models.agency.*;
import models.common.ConfigurationDao;
import models.common.Jenis_agency;
import models.common.SesiPelatihan;
import models.jcommon.util.DateUtil;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.Usergroup;
import models.sso.common.Kabupaten;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import play.Logger;
import play.libs.WS;
import play.libs.ws.WSRequest;
import utils.LogUtil;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by rayhanfrds on 8/14/17.
 */
public class UserMigration {
	public static final String passw = DigestUtils.md5Hex("123456");
	private static final int max = 500;

	public static void insertAgency(Integer maxParam, String mode, Integer min) {
		if (maxParam == null || maxParam == 0) {
			maxParam = max;
		}
		if (min == null || min == 0) {
			min = 1;
		}
		DateTime time = new DateTime();
		for(int i = min; i <= maxParam; i++){

			Kabupaten kbp = Kabupaten.find("kbp_nama = 'Jakarta Selatan (Kota)'").first();
			Jenis_agency jna = Jenis_agency.find("jna_jenis_agency = 'Lembaga Pemerintah Non Departemen'").first();

			String agcNama = "LKPP AGENCY "+i;
			Agency agency = null;
			if (mode.equals("update")) {
				agency = Agency.find("agc_nama = ?", agcNama).first();
			}

			if(agency == null){
				agency = new Agency();
			}

			agency.agc_nama = agcNama;
			agency.agc_alamat = "Epicentrum, Kuningan, Jakarta Selatan - DKI Jakarta";
			agency.kbp_id = kbp.kbp_id;
			agency.propinsiId = kbp.prp_id;
			agency.agc_telepon = "081000000000";
			agency.jna_id = jna.jna_id;
			agency.agc_tgl_daftar = new Date();
			agency.save();

			insertAdminAgency(agency.agc_id, i);
			insertPpk(agency.agc_id, i);
			insertPp(agency.agc_id,i);
			insertKuppbj(agency.agc_id,i, "LKPP/KUPPBJ/"+time.getYear()+"/"+time.getMonthOfYear()+"/");
			insertPokja(agency,i, "LKPP/PANITIA/"+time.getYear()+"/"+time.getMonthOfYear()+"/" );
			insertUkpbj(agency.agc_id,i);

		}
	}

	private static void insertAdminAgency(Long agcId, int i){
		String peg_namauser = "AGENCY" + i;
		Pegawai pegawai = Pegawai.findBy(peg_namauser);
		if (pegawai == null) {
			pegawai = new Pegawai();
			pegawai.passw = passw;
		}
		pegawai.peg_namauser = peg_namauser;
		pegawai.peg_nip = "00100" + i;
		pegawai.peg_nama = "Agency " + i;
		pegawai.disableuser = 0;
		pegawai.peg_isactive = -1;
		pegawai.peg_telepon = "081000000000";
		pegawai.peg_email = "agency" + i + "@agency.xxx";
		pegawai.peg_no_sk = "LKPP/AGENCY/2017/08/" + i;
		pegawai.save();

		if (pegawai.peg_id != null) {
			Usergroup.createUsergroupFromUser(pegawai.peg_namauser, Group.ADM_AGENCY);
		}

		Admin_agency admin_agency = Admin_agency.find("peg_id=?", pegawai.peg_id).first();
		if (admin_agency == null)
			admin_agency = new Admin_agency();
		admin_agency.agc_id = agcId;
		admin_agency.peg_id = pegawai.peg_id;
		admin_agency.ada_nomor_sk = pegawai.peg_no_sk;
		admin_agency.save();

	}


	public static void insertPpk(Long agcId, int i) {
		Group group = Group.PPK;
		String peg_namauser = "PPK" + i;
		Pegawai pegawai = Pegawai.findBy(peg_namauser);
		if (pegawai == null) {
			pegawai = new Pegawai();
			pegawai.passw = passw;
		}
		pegawai.peg_namauser = peg_namauser;
		pegawai.peg_nip = "00200" + i;
		pegawai.peg_nama = "PPK " + i;
		pegawai.disableuser = 0;
		pegawai.peg_isactive = -1;
		pegawai.peg_telepon = "081000000000";
		pegawai.peg_email = "ppk" + i + "@ppk.xxx";
		pegawai.peg_no_sk = "LKPP/PPK/2017/08/" + i;
		pegawai.agc_id = agcId;
		pegawai.save();

		if (pegawai.peg_id != null) {
			Usergroup.createUsergroupFromUser(pegawai.peg_namauser, group);
		}

		Ppk ppk = Ppk.find("peg_id=?", pegawai.peg_id).first();
		if (ppk == null) {
			Ppk ppk_temp = new Ppk();
			ppk_temp.peg_id = pegawai.peg_id;
			ppk_temp.ppk_valid_start = new Date();
			ppk_temp.ppk_nomor_sk = pegawai.peg_no_sk;
			ppk_temp.save();
		}
	}

	public static void insertVerifikator(Integer maxParam) {
		int total = maxParam != null ? maxParam : max;
		Group group = Group.VERIFIKATOR;
		for (int i = 1; i <= total; i++) {
			String peg_namauser = "VERIFIKATOR" + i;
			Pegawai pegawai = Pegawai.findBy(peg_namauser);
			if (pegawai == null) {
				pegawai = new Pegawai();
				pegawai.passw = passw;
			}
			pegawai.peg_namauser = peg_namauser;
			pegawai.peg_nip = "00300" + i;
			pegawai.peg_nama = "VERIFIKATOR " + i;
			pegawai.disableuser = 0;
			pegawai.peg_isactive = -1;
			pegawai.peg_telepon = "081000000000";
			pegawai.peg_email = "verifikator" + i + "@lkpp.go.id";
			pegawai.peg_no_sk = "LKPP/VERIFIKATOR/2017/08/" + i;
			pegawai.save();

			if (pegawai.peg_id != null) {
				Usergroup.createUsergroupFromUser(pegawai.peg_namauser, group);
			}
		}
	}

	public static void insertHelpdesk(Integer maxParam) {
        int total = maxParam != null ? maxParam : max;
		Group group = Group.HELPDESK;
		for (int i = 1; i <= total; i++) {
			String peg_namauser = "HELPDESK" + i;
			Pegawai pegawai = Pegawai.findBy(peg_namauser);
			if (pegawai == null) {
				pegawai = new Pegawai();
				pegawai.passw = passw;
			}
			pegawai.peg_namauser = peg_namauser;
			pegawai.peg_nip = "00400" + i;
			pegawai.peg_nama = "HELPDESK " + i;
			pegawai.disableuser = 0;
			pegawai.peg_isactive = -1;
			pegawai.peg_telepon = "081000000000";
			pegawai.peg_email = "helpdesk" + i + "@lkpp.go.id";
			pegawai.peg_no_sk = "LKPP/HELPDESK/2017/08/" + i;
			pegawai.save();

			if (pegawai.peg_id != null) {
				Usergroup.createUsergroupFromUser(pegawai.peg_namauser, group);
			}
		}
	}

	private static void insertPp(Long agcId, int i) {
		Group group = Group.PP;
		String peg_namauser = "PP" + i;
		Pegawai pegawai = Pegawai.findBy(peg_namauser);
		if (pegawai == null) {
			pegawai = new Pegawai();
			pegawai.passw = passw;
		}
		pegawai.peg_namauser = peg_namauser;
		pegawai.peg_nip = "00600" + i;
		pegawai.peg_nama = "PP " + i;
		pegawai.disableuser = 0;
		pegawai.peg_isactive = -1;
		pegawai.peg_telepon = "081000000000";
		pegawai.peg_email = "pp" + i + "@pp.xxx";
		pegawai.peg_no_sk = "LKPP/PP/2017/08/" + i;
		pegawai.agc_id = agcId;
		pegawai.save();

		if (pegawai.peg_id != null) {
			Usergroup.createUsergroupFromUser(pegawai.peg_namauser, group);
		}
	}

	public static void insertRekanan(String initRekananConf, String mode, Integer start, Integer end){
		if(initRekananConf.equals("sikap")){
			insertRekananFromSikap();
		}else{
			insertRekananDummy(mode, start, end);
		}
	}

	private static void insertRekananFromSikap() {
		Group group = Group.REKANAN;
		String URL_SIKAP_GET_REKANAN_ALL = BasicCtr.SIKAP_URL + "/services/getrekananall";
		try {
			WSRequest request = WS.url(URL_SIKAP_GET_REKANAN_ALL);
			String responses = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
			List<Rekanan> rekananListFromSikap = new Gson().fromJson(responses, new TypeToken<List<Rekanan>>(){}.getType());
			for(Rekanan rekananSikap : rekananListFromSikap){
				Rekanan rekananExisting = Rekanan.findByNamaUser(rekananSikap.rkn_namauser);
				if (rekananExisting == null) {
					rekananExisting = new Rekanan();
					rekananExisting.passw = rekananSikap.passw;
					rekananExisting.rkn_id = rekananSikap.rkn_id;
				}
				rekananExisting.rkn_namauser = rekananSikap.rkn_namauser;
				rekananExisting.rkn_nama = rekananSikap.rkn_nama;
				rekananExisting.rkn_email = rekananSikap.rkn_email;
				rekananExisting.disableuser = 0;
				rekananExisting.rkn_isactive = rekananSikap.rkn_isactive;
				rekananExisting.rkn_status = rekananSikap.rkn_status;
				rekananExisting.repo_id = rekananSikap.repo_id;
				rekananExisting.kbp_id = rekananSikap.kbp_id;
				rekananExisting.btu_id = rekananSikap.btu_id;
				rekananExisting.rkn_tgl_daftar = rekananSikap.rkn_tgl_daftar;
				rekananExisting.rkn_tgl_setuju = rekananSikap.rkn_tgl_setuju;
				rekananExisting.rkn_alamat = rekananSikap.rkn_alamat;
				rekananExisting.rkn_status_verifikasi = rekananSikap.rkn_status_verifikasi;
				rekananExisting.status_migrasi = rekananSikap.status_migrasi;
				rekananExisting.isbuiltin = rekananSikap.isbuiltin;
				rekananExisting.rkn_npwp = rekananSikap.rkn_npwp;
				rekananExisting.rkn_statcabang = rekananSikap.rkn_statcabang;
				rekananExisting.save();
				if (rekananExisting.rkn_id != null) {
					Usergroup.createUsergroupFromUser(rekananExisting.rkn_namauser, group);
				}
			}
		}catch (Exception e){
			Logger.info("Error : " + e.getLocalizedMessage());
		}
	}

	private static void insertRekananDummy(String mode, Integer start, Integer end) {
		if (start == null || start == 0) {
			start = 1;
		}
		if (end == null || end == 0) {
			end = max;
		}

		Kabupaten kbp = Kabupaten.find("kbp_nama = 'Jakarta Selatan (Kota)'").first();

		Group group = Group.REKANAN;
		for (int i = start; i <= end; i++) {
			String rkn_namauser = "REKANAN" + i;
			Rekanan rekanan = null;
			if (mode.equals("update")) {
				rekanan = Rekanan.findByNamaUser(rkn_namauser);
			}
			if (rekanan == null) {
				rekanan = new Rekanan();
				rekanan.passw = passw;
			}
			rekanan.rkn_namauser = rkn_namauser;
			rekanan.rkn_nama = "REKANAN " + i;
			rekanan.rkn_email = rkn_namauser + "@lkpp.go.id";
			rekanan.disableuser = 0;
			rekanan.rkn_isactive = -1;
			rekanan.rkn_status = 1;
			rekanan.repo_id = 999l;
			rekanan.kbp_id = kbp.kbp_id;
			rekanan.btu_id = "02";
			rekanan.rkn_tgl_daftar = BasicCtr.newDate();
			rekanan.rkn_tgl_setuju = BasicCtr.newDate();
			rekanan.rkn_alamat = "Jakarta Selatan (Kota)";
			rekanan.rkn_status_verifikasi = "1111111";
			rekanan.status_migrasi = 0;
			rekanan.isbuiltin = 0;
			rekanan.disableuser = 0;
			rekanan.rkn_npwp = getRandomNpwp();
			rekanan.rkn_statcabang = "P";
			rekanan.rkn_telepon = getRandomPhoneNumber();
			try {
				rekanan.save();
			} catch (Exception ex) {
				Logger.info("Error : " + ex.getLocalizedMessage());
			}
			if (rekanan.rkn_id != null) {
				Usergroup.createUsergroupFromUser(rekanan.rkn_namauser, group);
			}
		}
	}

	public static String getRandomNpwp() {
		final String nextLong = String.valueOf(Math.abs(new Random().nextLong()));
		final char[] charArray = nextLong.toCharArray();
		final int count = nextLong.length();
		StringBuilder where = new StringBuilder();
		for (int i = 0; i < count; i++) {
			where.append(charArray[i]);
			if (i == 1 || i == 4 || i == 7 || i == 11) {
				where.append(".");
			} else if (i == 8) {
				where.append('-');
			}
			if (where.length() == 20) {
				break;
			}
		}
		LogUtil.debug("UserMigration", nextLong);
		return where.toString();
	}

	public static String getRandomPhoneNumber() {
		final String nextLong = String.valueOf(Math.abs(new Random().nextLong()));
		return nextLong.substring(0,13);
	}

	private static void insertPokja(Agency agency, int i, String no_sk) {
		List<String> alphabets = new ArrayList<>();
		alphabets.add("a");
		alphabets.add("b");
		alphabets.add("c");

		Satuan_kerja satker = Satuan_kerja.find("stk_nama='LEMBAGA KEBIJAKAN PENGADAAN BARANGJASA PEMERINTAH'").first();
		if(satker == null){
			satker = Satuan_kerja.find("stk_nama='LEMBAGA KEBIJAKAN PENGADAAN BARANG/JASA PEMERINTAH'").first();
		}

		Panitia panitia = Panitia.find("pnt_nama=?", "Kepanitiaan " + i).first();
		if (panitia == null) {
			panitia = new Panitia();
		}
		panitia.pnt_nama = "Kepanitiaan " + i;
		panitia.pnt_no_sk = no_sk + i;
		panitia.pnt_alamat = "Epicentrum, Kuningan, Jakarta Selatan - DKI Jakarta";
		panitia.pnt_tahun = LocalDate.now().getYear();
		panitia.kbp_id = agency.kbp_id;
		panitia.propinsiId = agency.propinsiId;
		panitia.stk_id = satker.stk_id;
		panitia.agc_id = agency.agc_id;
		panitia.is_active = -1;
		panitia.save();

		no_sk = no_sk.replace("/PANITIA/", "/POKJA/");

		for (String alphabet : alphabets) {
			String peg_namauser = "POKJA" + i + alphabet;
			Pegawai pegawai = Pegawai.findBy(peg_namauser);
			if (pegawai == null) {
				pegawai = new Pegawai();
				pegawai.passw = passw;
			}
			pegawai.agc_id = agency.agc_id;
			pegawai.peg_namauser = peg_namauser;
			pegawai.peg_nip = "10500" + i + alphabet;
			pegawai.peg_nama = "POKJA " + i + alphabet;
			pegawai.disableuser = 0;
			pegawai.peg_isactive = -1;
			pegawai.peg_telepon = "081000000000";
			pegawai.peg_email = "pokja" + i + alphabet + "@pokja.xxx";
			pegawai.peg_no_sk = no_sk + i + alphabet;
			pegawai.save();


			if (pegawai.peg_id != null) {
				Usergroup.createUsergroupFromUser(pegawai.peg_namauser, Group.PANITIA);
				Anggota_panitia anggotaPanitia = Anggota_panitia.find("peg_id=?", pegawai.peg_id).first();
				if (anggotaPanitia == null)
					anggotaPanitia = new Anggota_panitia();

				anggotaPanitia.agp_jabatan = "A";
				anggotaPanitia.pnt_id = panitia.pnt_id;
				anggotaPanitia.peg_id = pegawai.peg_id;
				anggotaPanitia.save();
			}
		}

	}

	private static void insertKuppbj(Long agcId, int i, String no_sk){
		String namaUser = "KUPPBJ"+i;
		Pegawai pegawai = Pegawai.findBy(namaUser);
		if(pegawai == null){
			pegawai = new Pegawai();
			pegawai.passw = passw;
		}
		pegawai.agc_id = agcId;
		pegawai.peg_namauser = namaUser;
		pegawai.peg_nip = "10900" + i;
		pegawai.peg_nama = "KUPPBJ " + i;
		pegawai.disableuser = 0;
		pegawai.peg_isactive = -1;
		pegawai.peg_telepon = "081000000000";
		pegawai.peg_email = "kuppbj" + i + "@kuppbj.xxx";
		pegawai.peg_no_sk =  no_sk + i;
		pegawai.save();

		if (pegawai.peg_id != null) {
			Usergroup.createUsergroupFromUser(pegawai.peg_namauser, Group.KUPPBJ);
		}

	}

	private static void insertUkpbj(Long agcId, int i){
		String nama = "UKPBJ "+i;
		Ukpbj ukpbj = Ukpbj.findByName(nama);

		if(ukpbj == null){
			ukpbj = new Ukpbj();
		}

		Pegawai kuppbj = Pegawai.find("agc_id = ? and peg_namauser = ?",agcId,"KUPPBJ"+i).first();

		ukpbj.kpl_unit_pemilihan_id = kuppbj.peg_id;
		ukpbj.agc_id = agcId;
		ukpbj.nama = nama;
		ukpbj.tgl_daftar = DateUtil.newDate();
		ukpbj.alamat = "Jakarta";
		ukpbj.telepon = "021234556";
		ukpbj.fax = "021234556";

		ukpbj.save();

		//Memasukkan kepanitiaan i ke ukpbj i
		Panitia panitia = Panitia.find("pnt_nama = ?","Kepanitiaan " + i).first();
		if(panitia != null){
			panitia.ukpbj_id = ukpbj.ukpbj_id;
			panitia.save();
		}

		//Memasukkan pegawai pokja ke ukbpj i
		for(Anggota_panitia anggota : panitia.getAnggota_panitiaList()){
			Pegawai_ukpbj pu = new Pegawai_ukpbj();
			pu.peg_id = anggota.peg_id;
			pu.ukpbj_id = ukpbj.ukpbj_id;
			pu.save();
		}

	}

	public static void insertTrainer(){
		String nama = "TRAINER";
		DateTime time = new DateTime();
		for(int i = 0; i <= SesiPelatihan.getCount(); i++){
			String username = nama;
			String pegNama = nama;

			if(i > 0){
				username = username + i;
				pegNama = pegNama + " " + i;
			}

			Pegawai trainer = Pegawai.find("peg_namauser = ?",username).first();
			if(trainer == null){
				trainer = new Pegawai();
				trainer.passw = passw;
			}

			trainer.peg_namauser = username;
			trainer.peg_nama = pegNama;
			trainer.peg_nip = "19999"+i;
			trainer.peg_no_sk = "LKPP/TRAINER/"+time.getYear()+"/"+time.getMonthOfYear()+"/" + i;
			trainer.disableuser = 0;
			trainer.peg_isactive = -1;
			trainer.save();

			if (trainer.peg_id != null) {
				Usergroup.createUsergroupFromUser(trainer.peg_namauser, Group.TRAINER);
			}

		}

	}



}
