package models.secman;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

@Table(name="USERGROUP")
public class Usergroup extends BaseTable {
	
	@Id
	public String systemid;
	
	@Id
	public Group idgroup;
	
	@Id	
	public String userid;
	
	public static Group findGroupByUser(String userid) {
		return Query.find("select idgroup from usergroup where userid = ? and systemid = 'EPNS'", Group.class ,userid).first();
	}

	public static List<Group> findListGroupByUser(String userid) {
		return Query.find("select idgroup from Usergroup where userid = ? and systemid = 'EPNS'", Group.class,userid).fetch();
	}
	
	public static Usergroup findByUser(String userid) {
		return find("userid = ? and systemid = 'EPNS'",userid).first();
	}

	/**
	 * Fungsi {@code createUsergroupFromUser} digunakan untuk membuat objek
	 * {@code Usergroup} baru berdasarkan informasi objek {@code User} dan
	 * parameter {@code grupid} sebagai role user.
	 *
	 * @param userId    objek {@code User}
	 * @param group string role user yang didapatkan dari form
	 */
	public static void createUsergroupFromUser(String userId, Group group) {
		Usergroup.delete("userid = ?", userId);
		if (group != null) { // update role pengguna sesuai dengan role yang dipilih
			Usergroup usergroup = new Usergroup();
			usergroup.idgroup = group; // update referensi grup user
			usergroup.userid = userId;
			usergroup.systemid = "EPNS";
			usergroup.save();
		}
	}
	
	 /**
     * pemeriksaan jumlah trainer dalam sistem
     * ini sangat penting dijalankan saat awal startup sistem
     * jika ada user sebagai trainer maka sistem dalam Mode.Latihan
     * default mode aplikasi production
     * @return 
     * @author arief ardiyansah
     */
    public static boolean isAnyTrainer() {
   	 long count = count("idgroup = ?", Group.TRAINER);
   	 return count > 0;
    }
}
