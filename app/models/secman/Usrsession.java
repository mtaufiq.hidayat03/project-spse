package models.secman;

import controllers.BasicCtr;
import ext.FormatUtils;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;



@Table(name="USRSESSION")
public class Usrsession extends BaseTable {
	
	@Id(sequence="seq_epns", function="nextsequence")
	public Long sessionid;

	public String station;

	public Date sessiontime;

	public Date logouttime;

	public String osname;
	
	public Subsystem systemid;
	
	public String userid;	
	
	public Integer jenis; // 0 : pegawai , 1 : rekanan
	
	/**
	 * Fungsi {@code setWaktuLogout} digunakan untuk set data waktu logout
	 * pengguna pada tabel usrsession
	 *
	 * @param sessionid user id pengguna
	 */
	public static void setWaktuLogout(Long sessionid, Date currentDate) {
		if (sessionid != null) {
			Usrsession usrsession = findById(sessionid);
			if (usrsession != null) {
				usrsession.logouttime = currentDate;
				usrsession.save();
			}
		}
	}

	/**
	 * Fungsi {@code setWaktuLogin} digunakan untuk set data waktu login
	 * pengguna pada tabel usrsession
	 *
	 * @param userid     user id pengguna
	 * @param ip_address ip address pengguna
	 */
	public static Usrsession setWaktuLogin(String userid, String ip_address, boolean rekanan) {
		Usrsession usrsession = new Usrsession();
		usrsession.userid = userid; // id uppercase
		usrsession.systemid = Subsystem.EPNS;
		usrsession.sessiontime = BasicCtr.newDate();
		usrsession.station = ip_address;
		usrsession.jenis = rekanan ? 1: 0;
		usrsession.save();
		return usrsession;
	}

	/**
	 * Fungsi {@code hapusUsrsessionTerakhir} digunakan untuk menghapus data
	 * session termutakhir untuk pengguna terkait
	 *
	 * @param userid user id pengguna
	 */
	public static void hapusUsrsessionTerakhir(String userid) {
		Usrsession usrsession = find("userid = ? ORDER BY sessionid DESC", userid.toUpperCase()).first();
		if (usrsession != null) { // Handling NULL
			usrsession.delete();
		}
	}

	/**
	 * Fungsi {@code findByUserId} digunakan untuk mendapatkan daftar session
	 * pengguna
	 *
	 * @param username username pengguna
	 * @return daftar session pengguna
	 */
	public static List<Usrsession> findByUserid(String username) {
		return find("userid = ?", username.toUpperCase()).fetch();
	}
	
	/**
	 * datatable usrsession
	 * @return
	 *//*
	public static String tableAccessLog(String userid)
	{		
		String[] column = new String[]{"userid", "sessiontime", "logouttime", "station"};
		String sql ="SELECT userid, sessiontime, logouttime, station FROM usrsession ";
		String sql_count="SELECT COUNT(userid) FROM usrsession";
		if(!StringUtils.isEmpty(userid))
		{
			sql += " WHERE userid='"+userid+"'";
			sql_count += " WHERE userid='"+userid+"'";
		}
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetAccessLog);
	}
	
	public static String tableUserLog(final String userid)
	{
		String[] column = new String[]{"sessiontime", "logouttime", "station"};
		String sql ="SELECT sessiontime, logouttime, station FROM usrsession WHERE userid='"+userid+"'";
		String sql_count="SELECT COUNT(*) FROM usrsession WHERE userid='"+userid+"'";
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetUserlog);
	}*/
	
	public static final ResultSetHandler<String[]> resultsetAccessLog = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("userid");
			tmp[1] = FormatUtils.formatDateTimeInd(rs.getTimestamp("sessiontime"));
			tmp[2] = FormatUtils.formatDateTimeInd(rs.getTimestamp("logouttime"));
			tmp[3] = rs.getString("station");
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetUserlog = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[3];
			tmp[0] = FormatUtils.formatDateTimeInd(rs.getTimestamp("sessiontime"));
			tmp[1] = FormatUtils.formatDateTimeInd(rs.getTimestamp("logouttime"));
			tmp[2] = rs.getString("station");
			return tmp;
		}
	};
	
}
