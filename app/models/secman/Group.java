package models.secman;

import models.common.ConfigurationDao;
import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.STRING)
public enum Group {
	
	ADM_PPE(Subsystem.EPNS,"group.admin_ppe",0),
	ADM_AGENCY(Subsystem.EPNS,"group.admin_agency",0),
	ADM_SATKER(Subsystem.EPNS,"group.admin_satker", 0),
	AUDITOR(Subsystem.EPNS,"group.auditor",0),
	DATA_ENTRY(Subsystem.EPNS,"group.data_entry",0),
	HELPDESK(Subsystem.EPNS,"group.helpdesk",0),
	PANITIA(Subsystem.EPNS,"group.anggota_pokja",0),
	PPK(Subsystem.EPNS,"group.ppk",0),
	PP(Subsystem.EPNS,"group.pp",0),
	PPHP(Subsystem.EPNS,"group.pphp",0),
	PPK_STAFF(Subsystem.EPNS,"group.ppk_staff",0),
	REKANAN(Subsystem.EPNS,"group.rekanan",0),
	TRAINER(Subsystem.EPNS,"group.trainer",0),
	VERIFIKATOR(Subsystem.EPNS,"group.verifikator",0),
	MIGRATION(Subsystem.EPNS, "group.migration", 0),
	
	/**Ini mewakili semua user yang berhasil login.
	 * Beberapa halaman boleh diakses oleh semua user yang login, misalnya LogAccess */
	ALL_AUTHORIZED_USERS(Subsystem.EPNS, "All registered users",0),
	UKPBJ(Subsystem.EPNS, "UKPBJ", 0),
	KUPPBJ(Subsystem.EPNS, "Kepala Unit Pengelola PBJ", 0),

	/**
	 * Untuk Lelang Panel
	 */
	ADM_PANEL(Subsystem.EPNS, "group.admin_panel", 0)
	;
	
	public final Subsystem system;
	public final String label;
	public final Integer builtin;
	
	Group(Subsystem system, String label, int builtin) {
		this.system = system;
		this.label = label;
		this.builtin = Integer.valueOf(builtin);
	}	
	
	public boolean isAdminPPE(){ return this == ADM_PPE;}
	public boolean isVerifikator(){ return this == VERIFIKATOR;}
	public boolean isHelpdesk(){ return this == HELPDESK;}
	public boolean isTrainer(){ return this == TRAINER;}
	public boolean isAdminAgency(){ return this == ADM_AGENCY;}
	public boolean isPpk(){ return this == PPK;}
	public boolean isPanitia(){ return this == PANITIA;}
	public boolean isRekanan(){ return this == REKANAN;}
	public boolean isAuditor(){ return this == AUDITOR;}
	public boolean isPP(){ return this == PP;}
	public boolean isPPHP(){ return this == PPHP;}
	public boolean isUkpbj() { return this == UKPBJ; }
	public boolean isKuppbj() { return this == KUPPBJ; }
	public boolean isAdminPanel() { return this == ADM_PANEL; }

	// list group untuk AdminPPE
	public static final Group[] groupsAdminPPE = ConfigurationDao.isEnableDevPartner() ?
			new Group[] {ADM_PPE, ADM_AGENCY, HELPDESK, VERIFIKATOR, ADM_PANEL} :
			new Group[] {ADM_PPE, ADM_AGENCY, HELPDESK, VERIFIKATOR};

	//list group untuk Admin Agency
	public static final Group[] groupsAdminAgency = new Group[] {KUPPBJ, PANITIA, PPK,PP};

	public String getLabel()
	{
		return Messages.get(label);
	}
}
