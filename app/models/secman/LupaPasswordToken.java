package models.secman;

import org.joda.time.DateTime;

import java.util.Date;


public class LupaPasswordToken {
	
	public static int EXPIRES=24; //satuan jam
	
	public Date tanggal_buat;
	public String token;
	
	public boolean isExpires()
	{
		//check expires apa tidak
		DateTime tglBuat=new DateTime(tanggal_buat);
		tglBuat=tglBuat.plusHours(LupaPasswordToken.EXPIRES);
		Date now=new Date();
		boolean expires=now.after(tglBuat.toDate());
		return expires;
	}
}
