package models.nonlelang;

import models.agency.*;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Nilai_evaluasi;
import models.nonlelang.common.TahapNowPl;
import models.nonlelang.common.TahapStartedPl;
import models.nonlelang.contracts.PaketNonLelangSeleksiContract;
import models.nonlelang.form.PenawaranDataForm;
import org.apache.commons.collections4.CollectionUtils;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Table(name = "EKONTRAK.NONLELANG_SELEKSI")
public class Pl_seleksi extends BaseModel implements PaketNonLelangSeleksiContract {

	@Enumerated(EnumType.ORDINAL)
    public enum JenisLelang {
        LELANG_NON_ITEMIZE, LELANG_ITEMIZE
    }

    @Id(sequence = "seq_lelang_seleksi", function = "nextsequence")
    public Long lls_id;

    public Double lls_passinggrade;

    public Integer lls_versi_lelang;
    
    public Integer lls_flow_new;

    public String lls_keterangan;

    public Date lls_dibuat_tanggal;

    public String lls_diulang_karena;

    public String lls_ditutup_karena;

    public String lls_dibuka_karena;

    public Integer lls_terverifikasi;

    public Date lls_tgl_setuju;

    @Required
    public StatusPl lls_status;

    public Long lls_wf_id;

    @Required
    public Integer mtd_pemilihan;

    @Required
    public Integer mtd_evaluasi;

    @Required
    public JenisLelang lls_penetapan_pemenang;// 0: satu pemenang, 1: lebih dari satu pemenang (itemize)

    public Integer lls_kontrak_pembayaran;

    public Integer lls_kontrak_tahun;

    public Integer lls_kontrak_sbd;
    /**
     * kontrak berdasarkan jenis Pekerjaan
     * since 4.0
     */
    public Integer lls_kontrak_pekerjaan;

	public boolean is_kualifikasi_tambahan = false;

    //relasi ke Paket
	public Long pkt_id;
	
	public Integer mtd_kualifikasi;
   
	public Integer getMtd_kualifikasi() {
		return mtd_kualifikasi;
	}

	public void setMtd_kualifikasi(Integer mtd_kualifikasi) {
		this.mtd_kualifikasi = mtd_kualifikasi;
	}

	@Transient
	private Paket_pl paket;
	@Transient
	public Dok_pl dok_pl;
	@Transient
	public TahapNowPl tahapNowPl;
	@Transient
	private TahapStartedPl tahapStarted;
	@Transient
	public PesertaPl pesertaPl;
	@Transient
	public List<PesertaPl> participants;
	@Transient
	private EvaluasiPl evaluasiAkhir;
	@Transient
	private EvaluasiPl evaluasiKualifikasi;
	@Transient
	private EvaluasiPl pembuktian;
	
	// untuk simpan sesi pelatihan. 0 : default prod tidak boleh diisi yang lain
	public int lls_sesi; 
	
	public Paket_pl getPaket() {
		if(paket == null) 
			paket = Paket_pl.findById(pkt_id);
		return paket;
	}

	public TahapNowPl getTahapNow(){
		if(tahapNowPl == null){
			tahapNowPl = new TahapNowPl(this);
		}

		return tahapNowPl;
	}

	public TahapStartedPl getTahapStarted(){
		if(tahapStarted == null){
			tahapStarted = new TahapStartedPl(this);
		}
		return tahapStarted;
	}

	public EvaluasiPl getEvaluasiAkhir(){
		if(evaluasiAkhir == null){
				evaluasiAkhir = EvaluasiPl.findPenetapanPemenang(lls_id);
		}
		return evaluasiAkhir;
	}

	@Override
	public Long getId() {
		return this.lls_id;
	}

	@Override
	public Long getPackageId() {
		return this.pkt_id;
	}

	@Override
	public Dok_pl getDokumenLelang() {
		if(dok_pl == null && lls_id != null){
			dok_pl = Dok_pl.findBy(lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
		}
		return dok_pl;
	}

	public boolean isPaketKonsolidasi(){
		Paket_pl paket = getPaket();
		if(paket == null){
			return false;
		}

		return paket.isKonsolidasi();
	}

	// lelang cepat : untuk dapat menetapkan pemenang, harus melakukan verifikasi ke SIKaP
	@Transient
	public boolean sudahVerifikasiSikap(){
		return PesertaPl.count("lls_id=? AND sudah_verifikasi_sikap=?", this.lls_id, Integer.valueOf(1)) > 0;
	}

	@Override
	public PesertaPl getParticipant() {
		return this.pesertaPl;
	}

	@Override
	public void setPackage(Paket_pl model) {
		this.paket = model;
	}

	@Override
	public void setDocument(Dok_pl model) {
		this.dok_pl = model;
	}

	@Override
	public void setStep(TahapNowPl model) {
		this.tahapNowPl = model;
	}

	@Override
	public void setParticipant(PesertaPl model) {
		this.pesertaPl = model;
	}

	@Override
	public void setParticipants(List<PesertaPl> items) {
		this.participants = items;
	}

	public Pp getPp(){
		return Query.find("SELECT p.* FROM ekontrak.pp p LEFT JOIN ekontrak.paket pkt ON pkt.pp_id=p.pp_id WHERE pkt.pkt_id=?", Pp.class, pkt_id).first();
	}

	public Panitia getPanitia(){
		return Query.find("SELECT p.* FROM panitia p LEFT JOIN ekontrak.paket pkt ON pkt.pnt_id=p.pnt_id WHERE pkt.pkt_id=?", Panitia.class, pkt_id).first();
	}

	public String getNamaPanitia(){
		return Query.find("SELECT pnt_nama FROM panitia p LEFT JOIN ekontrak.paket pkt ON pkt.pnt_id=p.pnt_id WHERE pkt.pkt_id=?", String.class, pkt_id).first();
	}
	
	public String getNamaPaket() {
		return Query.find("select pkt_nama from ekontrak.paket where pkt_id=?", String.class, pkt_id).first();
	}
	
	@Transient 
	public Kategori getKategori() {
		return Query.find("select kgr_id from ekontrak.paket where pkt_id=?", Kategori.class, pkt_id).first();
	}
	
	@Transient
	public boolean isKonsultansi() {
		Kategori kategori = getKategori();
		return kategori.isKonsultansi() || kategori.isKonsultansiPerorangan();
	}
	public boolean isJasaKonsultansi() {
		Kategori kategori = getKategori();
		return kategori.isKonsultansi() || kategori.isKonsultansiPerorangan() || kategori.isJkKonstruksi();
	}

	public boolean isKonstruksi() {
		Kategori kategori = getKategori();
		return kategori != null && kategori.isKonstruksi();
	}
	
	public boolean isJkKonstruksi() {
		Kategori kategori = getKategori();
		return kategori != null && kategori.isJkKonstruksi();
	}
	
	public MetodeKualifikasi[] getMetodeKualifikasiList(){
		Kategori kategori = getPaket().kgr_id;
		return MetodeKualifikasi.kualifikasiPraPasca;
	}
	
	@Transient
	public boolean isPengadaanUlang()
	{
		if(lls_versi_lelang == null)
			return false;
		return lls_versi_lelang > 1;
	}
	
	@Transient
	public JenisKontrak getKontrakPembayaran()
	{
		return JenisKontrak.findById(lls_kontrak_pembayaran);
	}

	@Transient
	public JenisKontrak getKontrakTahun()
	{
		return JenisKontrak.findById(lls_kontrak_tahun);
	}
	
	@Transient
	public Metode getMetode()
	{
		return Metode.findById(mtd_pemilihan);
	}

	@Transient
	public JenisKontrak getKontrakSumberDana()
	{
		return JenisKontrak.findById(lls_kontrak_sbd);
	}

	@Transient
	public JenisKontrak getKontrakPekerjaan()
	{
		if(lls_kontrak_pekerjaan == null)
			return null;
		return JenisKontrak.findById(lls_kontrak_pekerjaan);
	}

	@Transient
	public MetodePemilihanPenyedia getPemilihan(){
		return  MetodePemilihanPenyedia.findById(mtd_pemilihan);
	}

	@Transient
	public int getJumlahPeserta() {
		return (int)PesertaPl.count("lls_id=?", lls_id);
	}

	public boolean allowCreateSppbj(){
		int jumlahSppbj = SppbjPl.getJumlahSppbjPlByLelangAndPpk(lls_id,Active_user.current().ppkId);

//		if(isItemized()){ //jika itemized, jumlah sppbj maksimal sejumlah pemenang
//			int jumlahPemenang = NilaiEvaluasiPl.findAllWinnerCandidate(getEvaluasiAkhir(false).eva_id,this).size();
//			return jumlahSppbj < jumlahPemenang;
//		}

		return jumlahSppbj == 0;

	}

	public static Pl_seleksi findByPaketNewestVersi(Long pkt_id) {
		return find("pkt_id=? order by lls_versi_lelang DESC", pkt_id).first();
	}

	/**
	 * mendapatkan informasi metode pemilihan lelang , tanpa harus ambil seluruh Object lelang_seleksi
	 * @param lelangId
	 * @return
	 */
	public static MetodePemilihanPenyedia findPemilihan(Long lelangId) {
		int pemilihan = Query.find("SELECT mtd_pemilihan FROM ekontrak.pl_seleksi WHERE lls_id=?", Integer.class, lelangId).first();
		return MetodePemilihanPenyedia.findById(pemilihan);
	}

	@Transient
	public boolean isLelangV3() {
		Integer flag = Query.find("select pkt_flag from ekontrak.paket where pkt_id=?", Integer.class, pkt_id).first();
		return flag < 2;
	}

	@Transient
	public boolean isLelangV43() {
		Integer flag = Query.find("select pkt_flag from ekontrak.paket where pkt_id=?", Integer.class, pkt_id).first();
		return flag == 3;
	}
	
	@Transient
	public boolean isPlNew() {
		Integer flag = Query.find("select lls_flow_new from ekontrak.paket where pkt_id=?", Integer.class, pkt_id).first();
		return flag == 1;
	}

	@Transient
	public boolean isPlnew() {
		Integer flag = Query.find("select lls_flow_new from ekontrak.paket where pkt_id=?", Integer.class, pkt_id).first();
		return flag == 1;
	}

	
	@Transient
	public boolean hasPemenangTerverifikasi() {
		return PesertaPl.count("lls_id=? and is_pemenang_verif = 1", lls_id) > 0;
	}

	public PersetujuanPl findPersetujuan(Long pegawaiId, boolean pemenang) {
		if (pemenang) {
			return PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", this.lls_id, pegawaiId, PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG).first();
		} else {
			return PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", this.lls_id, pegawaiId, PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG).first();
		}
	}

	@Transient
	public boolean isLelangUlang()
	{
		if(lls_versi_lelang == null)
			return false;
		return lls_versi_lelang > 1;
	}

	@Transient
	public MetodeEvaluasi getEvaluasi()
	{
		return MetodeEvaluasi.findById(mtd_evaluasi);

	}

	public EvaluasiPl getEvaluasiKualifikasi(boolean findNCreate){
		if(evaluasiKualifikasi == null && lls_id != null){
			if(findNCreate){
				evaluasiKualifikasi = EvaluasiPl.findNCreateKualifikasi(lls_id);
			}else{
				evaluasiKualifikasi = EvaluasiPl.findKualifikasi(lls_id);
			}
		}

		return evaluasiKualifikasi;
	}
	
	public EvaluasiPl getPembuktian(boolean findNCreate){
		if(pembuktian == null){
			if(findNCreate){
				pembuktian = EvaluasiPl.findNCreatePembuktian(lls_id);				
			}else{
				pembuktian = EvaluasiPl.findPembuktian(lls_id);
			}
		}

		return pembuktian;
	}

	// check lelang cepat
	public boolean isExpress() {
		return getPemilihan().isLelangExpress();
	}

	@Transient
	public long getJumlahPenawar() {
		return Query.find("SELECT COUNT(*) FROM (select DISTINCT ON (psr_id) psr_id from ekontrak.dok_penawaran " +
				"WHERE psr_id in(SELECT psr_id FROM ekontrak.peserta_nonlelang WHERE lls_id=?) " +
				"AND dok_disclaim='1' AND dok_jenis in (1,3)) as jml", Long.class,lls_id).first();
	}

	@Transient
	public boolean isPenunjukanLangsung(){
		return mtd_pemilihan.equals(MetodePemilihanPenyedia.PENUNJUKAN_LANGSUNG.id);
	}
	
	@Transient
	public boolean isPenunjukanLangsungNew(){
		if(mtd_kualifikasi == null)
			return false;
		return mtd_kualifikasi.equals(MetodeKualifikasi.PRA.id);
	}

	public List<PesertaPl> getPesertaList() {
		return PesertaPl.findByPl(lls_id);
	}
	
	public String getInfoJadwal() {
		long jmlThp = Jadwal_pl.count("lls_id="+lls_id);
		Long tahapKosong = Jadwal_pl.countJadwalkosong(lls_id);
		if (jmlThp == 0)
			return "Belum Ada Jadwal";
		else if (tahapKosong == 0)
			return "Semua Jadwal berhasil tersimpan";
		else
			return jmlThp + " Tahap, " + tahapKosong+ " Tahap belum memiliki jadwal";
	}
	
	/**
	 * membuat lelang baru dengan nilai-nilai default
	 * event ini terjadi saat pembuatan paket
	 * @param paket
	 */
	public static Pl_seleksi buatLelangBaru(Paket_pl paket, MetodePemilihanPenyedia metodePemilihanPenyedia) {
		Pl_seleksi lls = Pl_seleksi.find("pkt_id=?", paket.pkt_id).first();
		if (lls == null ){
			lls = new Pl_seleksi();
			lls.pkt_id = paket.pkt_id;
//			if(metodePemilihanPenyedia != null)
//				lls.mtd_pemilihan = MetodePemilihanPenyedia.findBy(metodePemilihanPenyedia).id;
			lls.mtd_pemilihan = metodePemilihanPenyedia.id;
//			lls.mtd_evaluasi = MetodeEvaluasi.PENUNJUKAN_LANGSUNG.id;
            lls.lls_versi_lelang = 1;
            lls.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
            lls.lls_terverifikasi = 1;
            lls.lls_status = StatusPl.DRAFT;
            lls.lls_penetapan_pemenang = JenisLelang.LELANG_NON_ITEMIZE; // default non-itemize
			lls.setKontrakPembayaran(JenisKontrak.LUMP_SUM);
            lls.save();
        }
        return lls;
    }

	/**
	 * membuat lelang baru dengan nilai-nilai default
	 * event ini terjadi saat pembuatan paket
	 * @param paket
	 */
	public static Pl_seleksi buatNonSpkBaru(Paket_pl paket, MetodePemilihanPenyedia metodePemilihanPenyedia) {
//		long jumlahLelang = Query.count("SELECT count(lls_id) FROM ekontrak.nonlelang_seleksi WHERE pkt_id=?", paket.pkt_id);
		Pl_seleksi lls = Pl_seleksi.find("pkt_id=?", paket.pkt_id).first();
		if (lls == null ){
			lls = new Pl_seleksi();
			lls.pkt_id = paket.pkt_id;
			if(metodePemilihanPenyedia != null)
				lls.mtd_pemilihan = metodePemilihanPenyedia.id;
			lls.mtd_evaluasi = MetodeEvaluasi.PENUNJUKAN_LANGSUNG.id;
			lls.lls_versi_lelang = 1;
			lls.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
			lls.lls_terverifikasi = 1;
			lls.lls_status = StatusPl.DRAFT;
			lls.lls_penetapan_pemenang = JenisLelang.LELANG_NON_ITEMIZE; // default non-itemize
			lls.setKontrakPembayaran(JenisKontrak.LUMP_SUM);
			lls.save();
		}
		return lls;
	}

    /**
     * mendapatkan informasi status lelang , tanpa harus ambil seluruh Object lelang_seleksi
     *
     * @param lelangId
     * @return
     */
    public static StatusPl findStatusLelang(Long lelangId) {
        return Query.find("SELECT lls_status FROM ekontrak.nonlelang_seleksi WHERE lls_id=?", StatusPl.class, lelangId).first();
    }

    public List<Jadwal_pl> getJadwalList() {
        return Jadwal_pl.findByLelang(lls_id);
    }

    public Boolean isJadwalSudahTerisi() {
        long jmlThp = Jadwal_pl.count("lls_id=" + lls_id);
        Long tahapKosong = Jadwal_pl.countJadwalkosong(lls_id);

        return jmlThp != 0 && tahapKosong == 0;
    }

    @Transient
    public boolean isAllowDownloadBA() {
        return AktivitasPl.isTahapStarted(lls_id, Tahap.EVALUASI_PENAWARAN);
    }

    @Transient
    public boolean isAllowKirimPenawaran(PesertaPl peserta, TahapNowPl tahapAktif) {
        return tahapAktif.isPemasukanPenawaran();
    }
    
    @Transient
    public boolean isAllowKirimPenawaranPl(PesertaPl peserta, TahapNowPl tahapAktif) {
        return AktivitasPl.isTahapStarted(lls_id, Tahap.PEMASUKAN_PENAWARAN);
    }

	/**
	 * memeriksa jenis tender itemize atau bukan
	 * @return
	 */
	@Transient
	public boolean isItemized()
	{

		return lls_penetapan_pemenang!= null && lls_penetapan_pemenang.equals(JenisLelang.LELANG_ITEMIZE);
	}

	public String getNamaPegawaiPembuatan(){
		return Query.find("SELECT peg_nama FROM pegawai WHERE peg_namauser IN (SELECT audituser FROM ekontrak.paket WHERE pkt_id=?)", String.class, pkt_id).first();
	}

	public String getNamaPegawaiMenyetujui(){
		return Query.find("SELECT p.peg_nama FROM ekontrak.workflow w, Pegawai p WHERE w.audituser=p.peg_namauser AND "
				+ "w.foreign_wf_id IN (SELECT lls_wf_id FROM ekontrak.nonlelang_seleksi l WHERE l.lls_id=?)", String.class,lls_id).first();
	}

	public static void tutupPl(Pl_seleksi pl, String alasan) throws Exception
	{
		tutupPl(pl, alasan, null);
	}

	public static void tutupPl(Pl_seleksi pl, String alasan, String aksi) throws Exception
	{
		pl.lls_status = StatusPl.DITUTUP;
		pl.lls_ditutup_karena = alasan;
		pl.save();
		Paket_pl paket_pl = Paket_pl.findById(pl.pkt_id);
		if(paket_pl != null){
			if(aksi.toUpperCase().equals("BATAL")){
				paket_pl.pkt_status = Paket_pl.StatusPaket.SELESAI_LELANG;
			} else {
				paket_pl.pkt_status = Paket_pl.StatusPaket.ULANG_LELANG;
			}
			paket_pl.save();
		}
		MailQueueDao.kirimPengumumanTutupPl(pl, alasan);
	}

	/**
	 * flow proses buka paket
	 * @param alasan
	 * @throws Exception
	 */
	public void bukaLelang(String alasan) {
		try {
			lls_status = StatusPl.AKTIF;
			lls_dibuka_karena = alasan;
			List<PersetujuanPl> list = PersetujuanPl.findByLelangPembatalan(lls_id);
			if(!CollectionUtils.isEmpty(list)) { // simpan history persetujuan pembatalan
				for(PersetujuanPl obj : list) {
//					History_persetujuan.simpanHistory(obj);
					obj.delete();
				}
			}
			save();
			MailQueueDao.kirimPengumumanBukaPl(this, alasan);
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error(e, "Gagal Kirim Pengumuman Bukan Non Tender");
		}
	}

	public static Long ulangPl(Long lelangId){
		Pl_seleksi pl = Pl_seleksi.findById(lelangId);
		Pl_seleksi plCopy = new Pl_seleksi();
		plCopy.pkt_id = pl.pkt_id;
		plCopy.lls_ditutup_karena = pl.lls_ditutup_karena;
		plCopy.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
		plCopy.lls_terverifikasi = pl.lls_terverifikasi;
		plCopy.lls_versi_lelang = pl.lls_versi_lelang + 1;
		plCopy.lls_status = StatusPl.DRAFT;
		plCopy.mtd_pemilihan = pl.mtd_pemilihan;
		plCopy.mtd_evaluasi = pl.mtd_evaluasi;
		plCopy.lls_penetapan_pemenang = pl.lls_penetapan_pemenang;
		plCopy.lls_flow_new = 1;
		plCopy.mtd_kualifikasi = 0;

		//update jenis kontrak
		plCopy.lls_kontrak_pembayaran = pl.lls_kontrak_pembayaran;
		plCopy.save();

		Paket_pl paket = Paket_pl.findById(pl.pkt_id);
		paket.pkt_status = Paket_pl.StatusPaket.ULANG_LELANG;
		paket.save();

		//Handle Non Tender Ulang paket yang dibuat di versi sebelum 4.3
		if(!paket.isFlag43()){

			DokPersiapanPl dokPersiapan = DokPersiapanPl.findLastByPaket(paket.pkt_id);

			if(dokPersiapan == null){
				Dok_pl dl = Dok_pl.find("lls_id = ?",pl.lls_id).first();
				Dok_pl_content dlc = Dok_pl_content.findBy(dl.dll_id);

				dokPersiapan = new DokPersiapanPl();
				dokPersiapan.pkt_id = pl.pkt_id;
				dokPersiapan.dkh = dlc.dkh;
				dokPersiapan.dp_spek = dlc.dll_spek;
				dokPersiapan.sskkContent = dlc.sskk_content;
				dokPersiapan.dp_versi = 1;

				dokPersiapan.save();
			}

			//Karena belum ada aturan lebih lanjut terkait konsolidasi di versi 4.3, untuk sementara ambil salah satu PPK dari anggaran.
			//TODO: Perbaiki jika sudah ada aturan konsolidasi di 4.3.
			Paket_anggaran_pl paketAnggaran = Paket_anggaran_pl.findByPaket(pl.pkt_id).get(0);

			PaketPpkPl paketPpk = new PaketPpkPl(pl.pkt_id,paketAnggaran.ppk_id);
			paketPpk.save();
		}


		return plCopy.lls_id;
	}

	@Transient
	public void setKontrakSumberDana(JenisKontrak kontrak) { this.lls_kontrak_sbd = kontrak.id; }

	@Transient
	public void setKontrakPekerjaan(JenisKontrak kontrak)
	{
		this.lls_kontrak_pekerjaan = kontrak.id;
	}

	@Transient
	public void setKontrakPembayaran(JenisKontrak kontrak) { this.lls_kontrak_pembayaran = kontrak.id; }

	@Transient
	public void setKontrakTahun(JenisKontrak kontrak)
	{
		this.lls_kontrak_tahun = kontrak.id;
	}

	public static List<Pl_seleksi> getPlBySk(Long skId){
		return Query.find("SELECT l.* FROM ekontrak.paket_skauditor ps LEFT JOIN ekontrak.nonlelang_seleksi l ON ps.pkt_id = l.pkt_id " +
				"WHERE ps.skid=?", Pl_seleksi.class, skId).fetch();
	}

	public static void hapusPl(Long lls_id){
		Pl_seleksi pl = Pl_seleksi.findById(lls_id);

		if(pl.lls_versi_lelang == 1){
			Query.update("DELETE FROM ekontrak.jadwal WHERE lls_id = ?", pl.lls_id);
			Query.update("DELETE FROM ekontrak.checklist WHERE dll_id IN (SELECT dll_id FROM ekontrak.dok_nonlelang WHERE lls_id = ?)", lls_id);
			Query.update("DELETE FROM ekontrak.dok_nonlelang_content WHERE dll_id IN (SELECT dll_id FROM ekontrak.dok_nonlelang WHERE lls_id = ?)", lls_id);
			Query.update("DELETE FROM ekontrak.dok_nonlelang WHERE lls_id = ?", lls_id);
			Query.update("DELETE FROM ekontrak.draft_peserta_nonlelang WHERE lls_id = ?", lls_id);
			Query.update("DELETE FROM ekontrak.nonlelang_seleksi WHERE lls_id = ?", lls_id);
		}

		Query.update("DELETE FROM ekontrak.paket_lokasi WHERE pkt_id = ?", pl.pkt_id);
		Query.update("DELETE FROM ekontrak.paket_pp WHERE pkt_id = ?", pl.pkt_id);
		Query.update("DELETE FROM ekontrak.paket_panitia WHERE pkt_id = ?", pl.pkt_id);
		Query.update("DELETE FROM ekontrak.paket_anggaran WHERE pkt_id = ?", pl.pkt_id);
		Query.update("DELETE FROM ekontrak.paket_satker WHERE pkt_id = ?", pl.pkt_id);
		Query.update("DELETE FROM ekontrak.paket WHERE pkt_id = ?", pl.pkt_id);
	}

	/**
	 * Reset all participant offerings if hps not equal or the current price is changing.
	 * @param model PenawaranDataForm
	 **/

	private void resetOfferings(PenawaranDataForm model) throws Exception {
		withParticipants();
		Logger.debug(CommonUtil.toJson(model));
		if ((!getPaket().pkt_hps.equals(model.getTotalDouble())
				|| comparingRincian(model.getItems()))
				&& participants != null
				&& !participants.isEmpty()) {
			Logger.debug("start reset offering");
			Logger.debug("participants: " + participants.size());
			for (PesertaPl participant : participants) {
				Logger.debug("comparing return true or total price not equal do the cleaning..");
				participant.clearPenawaran();
				participant.withDokumenLelang();
				if (participant.getDokumenLelang() != null) {
					Logger.debug("get checklist dokumen lelang");
					participant.getDokumenLelang().withChecklistPl();
					if (!CommonUtil.isEmpty(participant.getDokumenLelang().getChecklistPls())) {
						for (ChecklistPl checklistPl : participant.getDokumenLelang().getChecklistPls()) {
							Logger.debug("get checklist dokumen penawaran");
							checklistPl.withDokumenPenawaran();
							if (!CommonUtil.isEmpty(checklistPl.dokPenawaranPls)) {
								for (DokPenawaranPl dokPenawaranPl : checklistPl.dokPenawaranPls) {
									Logger.debug("clearing dok penawaran");
									dokPenawaranPl.deleteModelAndBlob();
								}
							}
						}
					}
				}
				participant.save();
			}
			Logger.debug("notify rekanan");
			MailQueueDao.kirimNotifikasiAdendumNonLelangHps(this, participants);
		}
	}
	// cari nonlelang yg aktif , param paketId
	public static Pl_seleksi findAktifByPaket(Long paketId){
		return find ("pkt_id=? and lls_status='1' order by lls_id DESC", paketId).first();
	}

	public static Pl_seleksi findByPaket(Long paketId){
		return find ("pkt_id=? order by lls_id DESC", paketId).first();
	}

	public boolean isSedangPersetujuan(){ return PersetujuanPl.isSedangPersetujuan(lls_id);}

	public boolean isSedangPersetujuanPemenang() {
		return PersetujuanPl.isSedangPersetujuanPemenang(lls_id);
	}

	public boolean allowBatalPersetujuan(){
		PersetujuanPl persetujuan = PersetujuanPl.findByPegawaiPengumuman(Active_user.current().pegawaiId, lls_id);
		if(persetujuan != null){
			return persetujuan.pst_status.isSetuju() && isSedangPersetujuan() && lls_status.isDraft();
		}
		return false;
	}

	public List<NilaiEvaluasiPl> findAllWinnerCandidate(){
		EvaluasiPl evaluasi = getEvaluasiAkhir();
		if(evaluasi == null)
			return null;
		if(isExpress())
			return Nilai_evaluasi.find("eva_id = ? and psr_id in (SELECT p.psr_id FROM ekontrak.peserta_nonlelang p where p.sudah_verifikasi_sikap = 1 " +
					"and p.lls_id = ?) order by nev_urutan", evaluasi.eva_id, lls_id).fetch();
		return Nilai_evaluasi.find("eva_id=? and nev_urutan is not null order by nev_urutan", evaluasi.eva_id).fetch();
	}


}
