package models.nonlelang;

import controllers.BasicCtr;
import models.agency.Paket_pl;
import models.agency.Panitia;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.Tahap;
import models.handler.DiskusiLelangHandler;
import models.handler.DiskusiPlHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.secman.Group;
import org.joda.time.DateTime;
import org.joda.time.Period;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


@Table(name = "EKONTRAK.DISKUSI_LELANG_PL")
public class Diskusi_lelangPl extends BaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum Jenis_diskusi {
		PEMBUKAAN, PERTANYAAN, PENJELASAN, PENUTUPAN
	}

	@Id(sequence="seq_diskusi_lelang", function="nextsequence")
	public Long dsl_id_topik;	

	public String dsl_dokumen;

	public String dsl_bab;

	public String dsl_uraian;

	public Date dsl_tanggal;

	public Long dsl_id_attachment;

	public String dsl_nama_file;

	public Integer thp_id;

	//relasi ke Lelang_seleksi
	public Long lls_id;

	//relasi ke Panitia;
	public Long pnt_id;

	//relasi ke Peserta
	public Long psr_id;

	//relasi ke Ppk
	public Long ppk_id;

	// START tambahan SPSE 4
	/**
	 * Tambahan khusus SPSE 4 Satu record bisa berfungsi sebagai 
	 * 1. Pertanyaan: jika ada psr_id 
	 * 2. Jawaban: jika tidak ada psr_id Untuk Jawaban maka refer ke Diskusi_lelang lain sebagai pertanyaan
	 */
	public Long dsl_pertanyaan_id;
	
	public Jenis_diskusi dsl_jenis;
	
	@Transient
	private Panitia panitia;
    @Transient
    private PesertaPl peserta;

	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}

    public PesertaPl getPeserta() {
        if(peserta == null)
            peserta = PesertaPl.findById(psr_id);
        return peserta;
    }

	@Transient
	public String getPengirim() {
		// dapatkan pengirim, entah panitia atau penyedia
		if (pnt_id != null)
			return Panitia.findNamaPanitia(pnt_id);
		if (psr_id != null)
            return psr_id.toString();
		return "";
	}

	@Transient
	public boolean isPertanyaan() {
		return psr_id != null;
	}
	
	@Transient
	public String getDownloadUrl() {
		if(dsl_id_attachment == null)
			return null;
		BlobTable blob= BlobTableDao.getLastById(dsl_id_attachment);
		return blob.getDownloadUrl(DiskusiPlHandler.class);	
	}
	
	
	public String getBab() {
		if(dsl_bab == null)
			return "";
		else if (dsl_bab.toLowerCase().contains("bab"))
			return dsl_bab;
		else
			return "Bab "+dsl_bab;
	}
	
	@Transient
	/** Dapatkan list jawaban */
	public List<Diskusi_lelangPl> getJawabanList() {
		return find("dsl_pertanyaan_id=? order by dsl_id_topik", dsl_id_topik).fetch();
	}
	
	public static int countTanggapanPeserta(Long lelangId) {
		return (int)count("lls_id=? and psr_id is not null", lelangId);
	}
	
	public static int countTanggapanPeserta(Long lelangId, Tahap tahap) {
		return (int)count("lls_id=? and thp_id=? and psr_id is not null", lelangId, tahap.id);
	}
	
	public static int countTanggapanPanitia(Long lelangId) {
		return (int)count("lls_id=? and pnt_id is not null", lelangId);
	}
	
	public static int countTanggapanPanitia(Long lelangId, Tahap tahap) {
		return (int)count("lls_id=? and thp_id=? and pnt_id is not null", lelangId, tahap.id);
	}
	
	public static int countTanggapanPp(Long lelangId, Tahap tahap) {
		return (int)count("lls_id=? and thp_id=? and dsl_pertanyaan_id is not null ", lelangId, tahap.id);
	}
	
	/**Dapatkan diskusi lelang list yang 'root' 
	 * 
	 * @param lls_id
	 * @return
	 */
	public static List<Diskusi_lelangPl> getDiskusiLelangTopik(Long lls_id, Tahap tahap) {
		//dapatkan semua yang root
		return find("lls_id=? and thp_id=? and dsl_jenis <> 0 and (dsl_pertanyaan_id is null or pnt_id is null) order by dsl_id_topik", lls_id, tahap.id).fetch();
	}
	
	public static void simpan(Long lelangId, Diskusi_lelangPl penjelasan, Tahap tahap, File file, Long pertanyaan_id) {
		Paket_pl paket = Paket_pl.findBy(lelangId);
		Active_user active_user = Active_user.current();
		Pl_seleksi lelang = Pl_seleksi.findById(lelangId);
		boolean btsWkt = isWriteable(lelangId, tahap, active_user.group, BasicCtr.newDate()) && lelang.lls_status.isAktif();
		if (btsWkt && penjelasan != null) {
			if (file != null) {
				try {
					BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file,penjelasan.dsl_id_attachment);
					penjelasan.dsl_id_attachment = blob.blb_id_content;
					penjelasan.dsl_nama_file = blob.blb_nama_file;
				}catch (Exception e) {
					e.printStackTrace();
					Logger.info("dokumen penjelasan %d gagal tersimpan", lelangId);
				}
			}
			penjelasan.lls_id = lelangId;
			penjelasan.thp_id = tahap.id;
			penjelasan.dsl_tanggal = controllers.BasicCtr.newDate();
			if (active_user.isPanitia()) {
				penjelasan.pnt_id =  Panitia.findByPenunjukanLangsung(lelangId).pnt_id;
				if(penjelasan.dsl_jenis == null) 
					penjelasan.dsl_jenis = Jenis_diskusi.PENJELASAN;
			}
			else if(active_user.isPP()) {
				penjelasan.pnt_id = paket.pp_id;
				if(penjelasan.dsl_jenis == null)
					penjelasan.dsl_jenis = Jenis_diskusi.PENJELASAN;
			}
			else if (active_user.isPpk()) {
				penjelasan.ppk_id = active_user.ppkId;
			} else if (active_user.isRekanan()) {
				PesertaPl peserta = PesertaPl.find("lls_id=? and rkn_id=?", lelangId,active_user.rekananId).first();
				penjelasan.psr_id = peserta.psr_id;
				penjelasan.dsl_jenis = Jenis_diskusi.PERTANYAAN;
			}
			if (pertanyaan_id != null) {
				penjelasan.dsl_pertanyaan_id = pertanyaan_id;
			}
			penjelasan.save();
		}
	}
	
	/**
	 * kirim pertanyaan otomatis untuk modul training
	 * hanya digunakan khusus oleh trainer 
	 * @param lelangId
	 * @param uraian
	 * @param tahap
	 * @param pesertaList
	 */
	public static void kirimPertanyaan(Long lelangId,String uraian, Tahap tahap, List<PesertaPl> pesertaList, Date currentDate) {
		if(CommonUtil.isEmpty(pesertaList))
			return;
		Diskusi_lelangPl penjelasan = null;		
		for(PesertaPl peserta : pesertaList) {
			penjelasan = new Diskusi_lelangPl();
			penjelasan.lls_id = lelangId;
			penjelasan.thp_id = tahap.id;
			penjelasan.dsl_tanggal = currentDate;
			penjelasan.psr_id = peserta.psr_id;			
			penjelasan.dsl_bab = "BAB I";
			penjelasan.dsl_dokumen = Messages.get("nonlelang.doc_pemilihan");
			penjelasan.dsl_uraian = !CommonUtil.isEmpty(uraian) ? uraian:Messages.get("nonlelang.contoh_pertanyaan");
			penjelasan.dsl_jenis = Jenis_diskusi.PERTANYAAN;
			penjelasan.save();
		}
	}
	
	/**
	 * kirim pertanyaan otomatis untuk modul training
	 * hanya digunakan khusus oleh trainer 
	 * @param lelangId
	 * @param tahap
	 * @param uraian
	 * @param currentDate
	 */
	public static void kirimPenjelasan(Long lelangId, String uraian, Tahap tahap, Date currentDate) {
		List<Diskusi_lelangPl> list = getDiskusiLelangTopik(lelangId, tahap);
		Panitia panitia = Panitia.findByLelang(lelangId);
		Diskusi_lelangPl penjelasan = null;
		for(Diskusi_lelangPl pertanyaan : list) {
			penjelasan = new Diskusi_lelangPl();
			penjelasan.lls_id = lelangId;
			penjelasan.thp_id = tahap.id;
			penjelasan.dsl_tanggal = currentDate;
			penjelasan.dsl_bab = "BAB I";
			penjelasan.pnt_id = panitia.pnt_id;
			penjelasan.dsl_dokumen = Messages.get("nonlelang.doc_pemilihan");
			penjelasan.dsl_uraian = !CommonUtil.isEmpty(uraian)? uraian:Messages.get("nonlelang.contoh_pertanyaan");
			penjelasan.dsl_pertanyaan_id = pertanyaan.dsl_id_topik;
			penjelasan.dsl_jenis = Jenis_diskusi.PENJELASAN;
			penjelasan.save();			
		}
	}
	
	/**Dapatkan Kalimat pembukaan oleh panitia
	 * 
	 * @param lls_id
	 * @return
	 */
	public static Diskusi_lelangPl findPembukaan(Long lls_id, Tahap tahap)
	{
		return find("lls_id=? and dsl_jenis=? AND thp_id=?", lls_id, Jenis_diskusi.PEMBUKAAN, tahap.id).first();
	}
	
	public static List<Diskusi_lelangPl> getTanggapanPeserta(Long lelangId, Tahap tahap) {
		return find("lls_id=? AND psr_id is not null AND thp_id=? ORDER BY dsl_tanggal", lelangId,tahap.id).fetch();
	}
	
	public static List<Diskusi_lelangPl> getTanggapanPanitia(Long lelangId, Tahap tahap) {
		return find("lls_id=? AND pnt_id is not null AND thp_id=? ORDER BY dsl_tanggal", lelangId,tahap.id).fetch();
	}
	
	public static boolean isWriteable(Long lls_id, Tahap tahap, Group group, Date date) {
//		ProcessInstance pi=WorkflowDao.findProcessBylelang(lls_id);
//		if(pi == null)  // prosess instance null berarti paket dibuat versi 3.5, tidak diijinkan untuk update data
//		{
			TimeZone.setDefault(TimeZone.getDefault());
			Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(lls_id, tahap);
			if (jadwal != null && jadwal.dtj_tglakhir != null && group.isPanitia()) {			
				DateTime jadwalAkhir = new DateTime(jadwal.dtj_tglakhir);
				jadwalAkhir = jadwalAkhir.plusHours(3); // 3 jam tambahan waktu bagi panitia
				jadwal.dtj_tglakhir = jadwalAkhir.toDate();				
			}
			return jadwal.isNow(date);
//		}
//		else {
//			return pi.isCurrentStateInstanceContain(tahap.toString());
//		}
	}
	
	public static String getSisaWaktu(Long lelangId, Tahap tahap){
		String result = "";
		Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(lelangId, tahap);
		Date date = controllers.BasicCtr.newDate();
		if(jadwal.dtj_tglakhir != null && jadwal.dtj_tglakhir.after(date)){	
			DateTime now = new DateTime(date.getTime());
			DateTime jakhir = new DateTime(jadwal.dtj_tglakhir.getTime());
			Period period = new Period(now, jakhir);
//			Duration durasi = new Duration(now, jakhir);	
//			long menit = durasi.getStandardMinutes();
//			long day = durasi.getStandardDays();
//			long jam = durasi.getStandardHours();			
			if(period.getDays() > 0){
				result += period.getDays() +Messages.get("nonlelang.hari");
			}
			if(period.getHours() > 0) {
//				menit = menit - (jam * 60);
				result += period.getHours() +Messages.get("nonlelang.jam");
			}
			if(period.getMinutes() > 0) {
				result += period.getMinutes() +Messages.get("nonlelang.return_waktu");
			}
		}
		else
			return Messages.get("nonlelang.return_waktu");
		return result;
	}
	
	public static boolean isEnd(Long lls_id, Tahap tahap, Date date) {
		TimeZone.setDefault(TimeZone.getDefault());
		Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(lls_id, tahap);
		if (jadwal != null && jadwal.dtj_tglakhir != null) {			
			DateTime jadwalAkhir = new DateTime(jadwal.dtj_tglakhir);
			jadwalAkhir = jadwalAkhir.plusHours(3);
			jadwal.dtj_tglakhir = jadwalAkhir.toDate();
			return jadwal.isEnd(date);
		}
		return false;
	}
}
