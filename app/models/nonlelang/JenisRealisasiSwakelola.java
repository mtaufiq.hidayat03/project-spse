package models.nonlelang;

import ext.DateBinder;
import ext.RupiahBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.nonlelang.nonSpk.RealisasiNonPenyediaSwakelola;
import models.nonlelang.nonSpk.RealisasiPenyediaSwakelola;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 10/24/2017.
 */
@Table(name = "EKONTRAK.JENIS_REALISASI_SWAKELOLA")
public class JenisRealisasiSwakelola extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum JenisRealisasiSwakelolaEnum {

        BUKTI_PEMBELIAN(1, "Bukti Pembelian"),
        KWITANSI(2, "Kwitansi"),
        SURAT_PERJANJIAN(3, "Surat Perjanjian"),
        SURAT_PESANAN(4, "Surat Pesanan"),
        SURAT_PERINTAH_KERJA(5, "Surat Perintah Kerja"),
        SURAT_PERINTAH_MULAI_KERJA(6, "Surat Perintah Mulai Kerja"),
        DOKUMEN_LAINNYA(7, "Dokumen Lainnya");

        public final Integer value;
        public final String label;

        private JenisRealisasiSwakelolaEnum(Integer value, String label) {

            this.value = value;
            this.label = label;

        }

        public static JenisRealisasiSwakelolaEnum fromValue(Integer value){
            JenisRealisasiSwakelolaEnum jenis = null;
            if(value!=null){
                switch (value.intValue()) {
                    case 1:	jenis = BUKTI_PEMBELIAN;break;
                    case 2:	jenis = KWITANSI;break;
                    case 3:	jenis = SURAT_PERJANJIAN;break;
                    case 4:	jenis = SURAT_PESANAN;break;
                    case 5:	jenis = SURAT_PERINTAH_KERJA;break;
                    case 6:	jenis = SURAT_PERINTAH_MULAI_KERJA;break;
                    case 7:	jenis = DOKUMEN_LAINNYA;break;
                }
            }
            return jenis;
        }

    }


    @Id(sequence="ekontrak.seq_jenis_realisasi_swakelola", function="nextsequence")
    public Long rsk_id;

    @Required
    public Long lls_id;

    public String rsk_nama;

    public String rsk_nama_dok;

    public String rsk_npwp;

    @Required
    public String rsk_nomor;

    @Required
    @As(binder= RupiahBinder.class)
    public Double rsk_nilai;

    @As(binder= DateBinder.class)
    public Date rsk_tanggal;

    public String rsk_keterangan;

    public Long rsk_id_attachment;

    public Integer rsk_jenis;

    public boolean is_penyedia;


    @Transient
    public BlobTable getBlob() {
        if(rsk_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(rsk_id_attachment);
    }

    @Transient
    public JenisRealisasiSwakelolaEnum getJenis() {
        return JenisRealisasiSwakelolaEnum.fromValue(rsk_jenis);
    }

    public static void simpanJenisDokumen(JenisRealisasiSwakelola jenisRealisasiSwakelola, File dlpAttachment, String hapus) throws Exception {

        if (dlpAttachment != null) {

            BlobTable bt = null;

            if (jenisRealisasiSwakelola.rsk_id == null)
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment);
            else
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment, jenisRealisasiSwakelola.rsk_id_attachment);

            jenisRealisasiSwakelola.rsk_id_attachment = bt.blb_id_content;

        } else {

            if (jenisRealisasiSwakelola.rsk_id_attachment != null && hapus == null) {

                Long id = jenisRealisasiSwakelola.rsk_id_attachment;

                jenisRealisasiSwakelola.rsk_id_attachment = null;

                BlobTable.delete("blb_id_content=?", id);

            }

            if (hapus != null)
                jenisRealisasiSwakelola.rsk_id_attachment = Long.valueOf(hapus);

        }

        jenisRealisasiSwakelola.save();

    }

    public static List<JenisRealisasiSwakelola> findBySwakelola(Long swakelolaId){

        return find("lls_id =?", swakelolaId).fetch();

    }

    public static Double getJumlahNilaiRealisasi(Long lelangId){
        String query = "SELECT SUM(rsk_nilai) FROM ekontrak.jenis_realisasi_swakelola WHERE lls_id=?";
        Double totalNilaiRealisasi = Query.find(query, Double.class, lelangId).first();

        return totalNilaiRealisasi == null ? 0d : totalNilaiRealisasi;
    }

    public static Double getJumlahNilaiRealisasi(Long lelangId, Long exceptRealisasiId){
        String query = "SELECT SUM(rsk_nilai) FROM ekontrak.jenis_realisasi_swakelola WHERE lls_id=? and rsk_id <> ?";
        Double totalNilaiRealisasi = Query.find(query, Double.class, lelangId, exceptRealisasiId).first();

        return totalNilaiRealisasi == null ? 0d : totalNilaiRealisasi;
    }

    @Transient
    public int getJumlahPenyedia(){

        if (this.is_penyedia)
            return RealisasiPenyediaSwakelola.countByRealisasi(this.rsk_id);
        else
            return RealisasiNonPenyediaSwakelola.countByRealisasi(this.rsk_id);

    }

}
