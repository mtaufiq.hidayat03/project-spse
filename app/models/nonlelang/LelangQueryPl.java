package models.nonlelang;

import controllers.BasicCtr;
import models.agency.Paket_pl;
import models.common.Instansi;
import models.common.Kategori;
import models.common.StatusPl;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;


/**
 * aw
 */
@Table(name = "EKONTRAK.LELANG_QUERY")
public class LelangQueryPl extends BaseTable {

    @Id
    public Long lls_id;

    public Long pkt_id;

    public String pkt_nama;

    public Integer pkt_flag;

    public Double pkt_hps;

    public Integer mtd_pemilihan;

    public Integer mtd_evaluasi;

    public Kategori kgr_id;

    public String nama_instansi;

    public Long kontrak_id;

    public Double kontrak_nilai;

    public boolean is_pkt_konsolidasi;

    public String anggaran;

    public Integer lls_versi_lelang;

    public Integer eva_versi;

    public StatusPl lls_status;

    public String tahap_sekarang;


    public static void save(Pl_seleksi pl, Paket_pl paket) {
        if(!pl.lls_status.isAktif())
            return;
        LelangQueryPl lelangQuery = findById(pl.lls_id);
        if(lelangQuery == null) {
            lelangQuery = new LelangQueryPl();
            lelangQuery.lls_id = pl.lls_id;
        }
        lelangQuery.pkt_id = pl.pkt_id;
        lelangQuery.pkt_flag = paket.pkt_flag;
        lelangQuery.pkt_nama = paket.pkt_nama;
        lelangQuery.pkt_hps = paket.pkt_hps;
        lelangQuery.kgr_id = paket.kgr_id;
        if(paket.is_pkt_konsolidasi != null)
            lelangQuery.is_pkt_konsolidasi = paket.is_pkt_konsolidasi;
        lelangQuery.mtd_pemilihan = pl.mtd_pemilihan;
        lelangQuery.mtd_evaluasi = pl.mtd_evaluasi;
        lelangQuery.lls_status = pl.lls_status;
        lelangQuery.lls_versi_lelang = pl.lls_versi_lelang;
        lelangQuery.anggaran = Query.find("(SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=?)", String.class, pl.pkt_id).first();
        lelangQuery.nama_instansi = Query.find("SELECT paket_instansi(?)",String.class, pl.pkt_id).first();
        lelangQuery.tahap_sekarang = Query.find("SELECT tahap_now(?, ?)", String.class, pl.lls_id, BasicCtr.newDate()).first();
        lelangQuery.eva_versi = EvaluasiPl.findCurrentVersi(pl.lls_id);
        lelangQuery.save();
    }

    public static void delete(Long id) {
        delete("lls_id=?", id);
    }

    public static List<Instansi> findInstansi() {
        return Instansi.find("id in (SELECT distinct s.instansi_id FROM satuan_kerja s, ekontrak.paket_satker ps, ekontrak.paket p WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id AND pkt_status = 1)").fetch();
    }

    public static List<Integer> listTahunAnggaranAktif() {
        return Query.find("SELECT DISTINCT ang_tahun FROM ekontrak.paket p, ekontrak.paket_anggaran pa, anggaran a WHERE p.pkt_id=pa.pkt_id AND pa.ang_id=a.ang_id AND pkt_status = 1 ORDER BY ang_tahun", Integer.class).fetch();
    }

}
