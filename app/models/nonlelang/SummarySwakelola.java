package models.nonlelang;

import ext.FormatUtils;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Lambang on 8/18/2017.
 */
public class SummarySwakelola {

    static final Template TEMPLATE = TemplateLoader.load("/swakelola/template-summary-swakelola.html");

    public static InputStream cetak(Long swkId) {

        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swkId);

        Map<String, Object> model = new HashMap<String, Object>();

        model.put("swakelola", swakelola);

        model.put("listJenisRealisasi", swakelola.getJenisRealisasi());

        Dok_swakelola dok_swakelola = Dok_swakelola.findBy(swakelola.lls_id);

        model.put("dokSwakelola", dok_swakelola);

        model.put("informasiLainnya", swakelola.getInformasiLainnya());

        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah((JenisRealisasiSwakelola.getJumlahNilaiRealisasi(swakelola.lls_id)));

        model.put("totalNilaiRealisasi", totalNilaiRealisasi);

        return HtmlUtil.generatePDF(TEMPLATE, model);

    }

}
