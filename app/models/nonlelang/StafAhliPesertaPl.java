package models.nonlelang;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Staf_ahli;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 2/22/2017.
 */
@Table(name="EKONTRAK.STAF_AHLI_PSR")
public class StafAhliPesertaPl extends BaseModel {

    @Id
    public Long stp_id;

    @Id
    public Long psr_id;

    public String sta_nama;

    public Date sta_tgllahir;

    public String sta_alamat;

    public String sta_jabatan;

    public String sta_pendidikan;

    public Integer sta_pengalaman;

    public String sta_keahlian;

    public String sta_email;

    public String sta_telepon;

    public String sta_jenis_kelamin;

    public String sta_kewarganearaan;

    public Integer sta_status;

    public String sta_cv;

    // field baru 4.1.2
    public String sta_npwp;

    public static PesertaPl getPeserta(Long pesertaId){
        return PesertaPl.find("psr_id=?", pesertaId).first();
    }

    public static void simpanStafAhliPeserta(Long pesertaId, List<Staf_ahli> listStaf) {
        delete("psr_id=?",pesertaId);
        if(!CommonUtil.isEmpty(listStaf)) {
            StafAhliPesertaPl stp = null;
            for(Staf_ahli sta : listStaf) {
                stp = new StafAhliPesertaPl();
                stp.psr_id = pesertaId;
                stp.stp_id = sta.sta_id;
                stp.sta_nama = sta.sta_nama;
                stp.sta_tgllahir = sta.sta_tgllahir;
                stp.sta_alamat = sta.sta_alamat;
                stp.sta_jabatan = sta.sta_jabatan;
                stp.sta_pendidikan = sta.sta_pendidikan;
                stp.sta_pengalaman = sta.sta_pengalaman;
                stp.sta_keahlian = sta.sta_keahlian;
                stp.sta_email = sta.sta_email;
                stp.sta_telepon = sta.sta_telepon;
                stp.sta_jenis_kelamin = sta.sta_jenis_kelamin;
                stp.sta_kewarganearaan = sta.sta_kewarganearaan;
                stp.sta_status = sta.sta_status;
                stp.sta_cv = sta.sta_cv;
                stp.sta_npwp = sta.sta_npwp;
                stp.save();
            }
        }
    }

}
