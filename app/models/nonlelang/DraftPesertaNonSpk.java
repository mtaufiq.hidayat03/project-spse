package models.nonlelang;

import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 5/10/2017.
 */
@Table(name="EKONTRAK.DRAFT_PESERTA_NON_SPK")
public class DraftPesertaNonSpk extends BaseModel {

    @Id(sequence="ekontrak.seq_draft_peserta_non_spk", function="nextsequence")
    public Long dpn_id;

    public Long rkn_id;

    public Long lls_id;

    public String dpn_nama_rekanan;

    public String dpn_email_rekanan;

    public String dpn_telp_rekanan;

    public String dpn_npwp_rekanan;

    public String dpn_alamat_rekanan;

    @Transient
    public Rekanan getRekanan()
    {
        return Rekanan.findById(rkn_id);

    }

    public static List<DraftPesertaNonSpk> findWithLelang(Long lelangId) {
        return find("lls_id=?",lelangId).fetch();
    }

    public static DraftPesertaNonSpk findWithLelangAndRekanan(Long lelangId, Long rekananId) {
        return find("lls_id=? AND rkn_id=?",lelangId, rekananId).first();
    }

    public static boolean isSudahTerdaftarDiDraft(Long lelangId, Long rkn_id){

        DraftPesertaNonSpk draftPesertaNonSpk = DraftPesertaNonSpk.findWithLelangAndRekanan(lelangId, rkn_id);

        return draftPesertaNonSpk != null ? true : false;
    }

}
