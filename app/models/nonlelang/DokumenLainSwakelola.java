package models.nonlelang;

import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.List;

/**
 * Created by Lambang on 4/28/2017.
 */
@Table(name = "EKONTRAK.DOKUMEN_LAIN_SWAKELOLA")
public class DokumenLainSwakelola extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum JenisDokLainSwakelola{

        INFORMASI_LAINNYA(1);

        public final Integer value;

        private JenisDokLainSwakelola(Integer value) {
            this.value = value;
        }

        public static JenisDokLainSwakelola fromValue(Integer value){
            JenisDokLainSwakelola jenis = null;
            if(value!=null){
                switch (value.intValue()) {
                    case 1:	jenis = INFORMASI_LAINNYA;break;
                }
            }
            return jenis;
        }

    }


    @Id(sequence="ekontrak.seq_dokumen_lain_swakelola", function="nextsequence")
    public Long dls_id;

    @Required
    public Long lls_id;

    public Integer dls_jenis;

    public Long dls_id_attachment;

    @Transient
    public BlobTable getBlob() {
        if(dls_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dls_id_attachment);
    }

    @Transient
    public JenisDokLainSwakelola getJenis() {
        return JenisDokLainSwakelola.fromValue(dls_jenis);
    }

    public static List<DokumenLainSwakelola> findBySwakelolaAndJenis(Long swakelolaId, Integer jenis){

        return find("lls_id =? AND dls_jenis=?", swakelolaId, jenis).fetch();

    }

    public static UploadInfo simpan(Long swakelolaId, Integer jenis, File file) throws Exception {

        DokumenLainSwakelola dokLain = new DokumenLainSwakelola();
        dokLain.lls_id = swakelolaId;
        dokLain.dls_jenis = jenis;

        BlobTable blob = null;
        if (file != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokLain.dls_id_attachment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }
        dokLain.dls_id_attachment = blob.blb_id_content;
        dokLain.save();

        // untuk keperluan response page yang memakai ajax
        return UploadInfo.findBy(blob);
    }

}
