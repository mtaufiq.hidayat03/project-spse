package models.nonlelang;

import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import java.util.Date;
import java.util.List;

/**
 * Created by rayhanfrds on 4/10/17.
 */
@Table(name = "EKONTRAK.RIWAYAT_PERSETUJUAN")
public class Riwayat_PersetujuanPl extends BaseModel {

	@Id(sequence = "ekontrak.seq_riwayat_persetujuan", function = "nextsequence")
	public Long rwt_pst_id;

	public Long lls_id;

	public Long peg_id;

	public PersetujuanPl.JenisPersetujuanPl pst_jenis;

	public Date pst_tgl_setuju;

	public PersetujuanPl.StatusPersetujuan pst_status;

	public String pst_alasan;


	public static class RiwayatPersetujuanPl {

		public Long lls_id;
		public PersetujuanPl.JenisPersetujuanPl pst_jenis;
		public Date pst_tgl_setuju;
		public PersetujuanPl.StatusPersetujuan pst_status;
		public String pst_alasan;
		public String peg_nama;
		public String agp_jabatan;

		public String getAlasan() {
			if(StringUtils.isEmpty(pst_alasan))
				return "";
			return pst_alasan;
		}

		public String getJabatan() {
			if (agp_jabatan.equals("K")) {
				return Messages.get("ct.ketua");
			} else if (agp_jabatan.equals("W")) {
				return  Messages.get("ct.wakil");
			} else if (agp_jabatan.equals("S")) {
				return  Messages.get("ct.sekretaris");
			} else {
				return  Messages.get("ct.anggota");
			}
		}

		public String getTglSetujuFmt(){
			if(pst_tgl_setuju != null){
				return FormatUtils.formatDateTimeInd(pst_tgl_setuju);
			}
			return "";
		}

		public static List<RiwayatPersetujuanPl> findList(Long peg_id, Long lls_id, PersetujuanPl.JenisPersetujuanPl jenis) {
			return Query.find("SELECT lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, agp_jabatan, peg_nama " +
					"FROM ekontrak.riwayat_persetujuan p, " +
                    '(' +
					"SELECT peg.peg_id, peg_nama, agp_jabatan FROM ekontrak.nonlelang_seleksi l " +
					"LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id " +
					"LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id "+
					"LEFT JOIN anggota_panitia ap ON ap.pnt_id=pn.pnt_id " +
					"LEFT JOIN pegawai peg ON peg.peg_id=ap.peg_id " +
					"WHERE lls_id=? AND ap.peg_id=peg.peg_id" +
					") a " +
					"WHERE p.peg_id=a.peg_id AND lls_id=? AND p.peg_id=? AND pst_jenis=? " +
					"ORDER BY pst_tgl_setuju ASC", RiwayatPersetujuanPl.class, lls_id, lls_id, peg_id, jenis).fetch();
		}

		public static List<RiwayatPersetujuanPl> findPpList(Long peg_id, Long lls_id, PersetujuanPl.JenisPersetujuanPl jenis) {
			return Query.find("SELECT lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama " +
					"FROM ekontrak.riwayat_persetujuan p, " +
					'(' +
					"SELECT peg.peg_id, peg_nama FROM ekontrak.nonlelang_seleksi l " +
					"LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id " +
					"LEFT JOIN ekontrak.pp pn ON pn.pp_id=p.pp_id "+
					"LEFT JOIN pegawai peg ON peg.peg_id=pn.peg_id " +
					"WHERE lls_id=? AND pn.peg_id=peg.peg_id" +
					") a " +
					"WHERE p.peg_id=a.peg_id AND lls_id=? AND p.peg_id=? AND pst_jenis=? " +
					"ORDER BY pst_tgl_setuju ASC", RiwayatPersetujuanPl.class, lls_id, lls_id, peg_id, jenis).fetch();
		}
	}

	public static void simpanRiwayatPersetujuan(Long lls_id, Long peg_id, PersetujuanPl.JenisPersetujuanPl pst_jenis, Date pst_tgl_setuju,
												PersetujuanPl.StatusPersetujuan pst_status, String pst_alasan){
		Riwayat_PersetujuanPl rwt = new Riwayat_PersetujuanPl();
		rwt.lls_id = lls_id;
		rwt.peg_id = peg_id;
		rwt.pst_jenis = pst_jenis;
		rwt.pst_tgl_setuju = pst_tgl_setuju;
		rwt.pst_status = pst_status;
		rwt.pst_alasan = pst_alasan;
		rwt.save();
	}

	public static void simpanRiwayatPersetujuan(PersetujuanPl pst){
		simpanRiwayatPersetujuan(pst.lls_id, pst.peg_id, pst.pst_jenis, pst.pst_tgl_setuju, pst.pst_status, pst.pst_alasan);
	}
}
