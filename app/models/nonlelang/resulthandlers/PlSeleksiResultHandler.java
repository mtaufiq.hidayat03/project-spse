package models.nonlelang.resulthandlers;

import ext.FormatUtils;
import models.common.*;
import org.sql2o.ResultSetHandler;
import play.i18n.Messages;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author HanusaCloud on 3/9/2018
 */
public class PlSeleksiResultHandler {

    public static final ResultSetHandler<String[]> resultsetPL = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[11];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            boolean paket43 = pkt_flag == 3;
            Integer mtd_pemilihan = rs.getInt("mtd_pemilihan");
            String pemilihanLabel = "";
            boolean express = false;
            if(paket43){
                MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(mtd_pemilihan);
                pemilihanLabel = pemilihan.getLabel();
                express = pemilihan.isLelangExpress();
            }else {
                MetodePemilihan pemilihan = MetodePemilihan.findById(mtd_pemilihan);
                pemilihanLabel = pemilihan.label;
                express = pemilihan.isLelangExpress();
            }
            tmp[0] = lelangId.toString();
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_nama");
            MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(mtd_pemilihan);
            String tenderUlangLabel = pemilihan.isPenunjukanLangsung() ? Messages.get("res.penunjukan_langsung") : Messages.get("res.pengadaan_langsung");
            if(versi > 1)
                namaLelang += " <span class=\"badge  badge-warning\">" + tenderUlangLabel + "</span>";
            tmp[1] = namaLelang;
            tmp[2] = rs.getString("nama_instansi");
            tmp[3] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, express);
            tmp[4] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
            tmp[5] = pemilihanLabel;
            tmp[6] = Kategori.findById(rs.getInt("kgr_id")).getNama()  + " - "+Messages.get("tag.ta")+" " + rs.getString("anggaran");
            tmp[7] = pkt_flag.toString();
            tmp[8] = rs.getString("kontrak_id") == null || rs.getString("kontrak_nilai") == null ? "Nilai Kontrak belum dibuat": FormatUtils.formatCurrencyRupiah(rs.getDouble("kontrak_nilai"));
            tmp[9] = rs.getString("eva_versi");
//            tmp[10] = rs.getString("lls_penawaran_ulang");
            tmp[10] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";

            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPLPanitia = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[9];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
//            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(pemilihan.id);
            String tenderUlangLabel = pemilihan.isPenunjukanLangsung() ? "Penunjukan Langsung Ulang" : "Pengadaan Langsung Ulang";
            String mtdLabel = pemilihan.label;
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[3] = rs.getString("peserta");

            tmp[4] = pkt_flag.toString();
            if(pkt_flag == 3){
                MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(pemilihan.id);
                mtdLabel = pemilihanpl.getLabel();
            }
            tmp[5] = mtdLabel;
            tmp[6] = rs.getString("lls_versi_lelang");
            tmp[7] = tenderUlangLabel;
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPlRekanan = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
//            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(pemilihan.id);
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_nama");
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = pemilihan.isPenunjukanLangsung() ? Messages.get("res.penunjukan_langsung") : Messages.get("res.pengadaan_langsung");
            String mtdLabel = pemilihan.getLabel();
            if (versi > 1)
                namaLelang += " <span class=\"badge  badge-warning\">" + tenderUlangLabel + "</span>";
            tmp[0] = lelangId.toString();
            tmp[1] = namaLelang;
            tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[3] = pkt_flag.toString();
            if(pkt_flag == 3){
                MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(pemilihan.id);
                mtdLabel = pemilihanpl.getLabel();
            }
            tmp[4] = mtdLabel;
            tmp[5] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPlBaru = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
//            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
            String[] tmp = new String[6];
            tmp[0] = rs.getString("lls_id");
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_hps"));
            tmp[3] = pemilihanpl.getLabel();
            tmp[4] = rs.getString("pkt_flag");
//            tmp[5] = pemilihan.getKey().toString();
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetSummary = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[10];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            StatusPl statuspl = StatusPl.fromValue(rs.getInt("lls_status"));
            MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama") +(statuspl.isDitutup() ? " <span class='badge  badge-danger'>" + statuspl.label + "</span>":"");
            tmp[2] = FormatUtils.formatDateInd(rs.getTimestamp("lls_dibuat_tanggal"));
            tmp[3] = rs.getString("nama");
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_pagu"));
            tmp[6] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
            if(statuspl.isAktif())
                tmp[7] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            if(statuspl.isDitutup())
                tmp[7] = Tahap.SUDAH_SELESAI.label;
            tmp[8] = null;
            tmp[9] = rs.getString("pkt_id");
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetRekapAuditor = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[9];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            StatusPl statuspl = StatusPl.fromValue(Integer.valueOf(rs.getString("lls_status")));
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_pagu"));
            tmp[3] = rs.getString("stk_nama");
            tmp[4] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihanpl.isLelangExpress());
            tmp[5] = pkt_flag.toString();
            tmp[6] = rs.getString("pkt_id");
            tmp[7] = statuspl.isDitutup() ? Messages.get("res.paket_dibatalkan") : "";
			tmp[8] = rs.getString("lls_versi_lelang");

            return tmp;
        }
    };

}
