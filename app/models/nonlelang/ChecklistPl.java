package models.nonlelang;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import ext.SbdRegexBinder;
import models.common.Kategori;
import models.common.Kualifikasi;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master;
import models.lelang.Checklist_master.ChecklistStatus;
import models.lelang.Checklist_master.JenisChecklist;
import models.nonlelang.contracts.ChecklistContract;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.binding.As;
import play.data.validation.MaxSize;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Scope;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Lambang on 2/21/2017.
 */

@Table(name="EKONTRAK.CHECKLIST")
public class ChecklistPl extends BaseModel implements ChecklistContract{

    @Id(sequence="ekontrak.seq_checklist", function="nextsequence")
    public Long chk_id;

    //relasi ke Dok_lelang
    public Long dll_id;

    //relasi ke Checklist_master
    public Integer ckm_id;

    @MaxSize(70)
    @As(binder = SbdRegexBinder.class)
    public String chk_nama;

    public String chk_klasifikasi;

    @Transient
    private Dok_pl dok_pl;

    @Transient
    private Checklist_master checklist_master;
    
    public Integer chk_versi = Integer.valueOf(1);

    @Transient
    private DokPenawaranPl dokPenawaranPl;

    @Transient
    public List<DokPenawaranPl> dokPenawaranPls;

    public Dok_pl getDok_pl() {
        if(dok_pl == null)
            dok_pl = Dok_pl.findById(dll_id);
        return dok_pl;
    }

    public Checklist_master getChecklist_master() {
        if(checklist_master == null)
            checklist_master = Checklist_master.findById(ckm_id);
        return checklist_master;
    }

    public DokPenawaranPl getDokPenawaranPl() {
        if(dokPenawaranPl == null)
            dokPenawaranPl = DokPenawaranPl.findByChecklist(chk_id);
        return dokPenawaranPl;
    }

    public List<DokPenawaranPl> getListDokPenawaranPl() {
        return DokPenawaranPl.findListPenawaranByChecklist(chk_id);
    }

    public List<DokPenawaranPl> getListDokPenawaranPlByPsr(Long psr_id) {
        return DokPenawaranPl.findByPesertaAndChecklist(psr_id, chk_id);
    }

    public List<DokPenawaranPl> getListDokPenawaranPlByPsrAndJenis(Long psr_id, Integer dok_jenis) {
        return DokPenawaranPl.findByPesertaAndChecklistAndJenis(psr_id, chk_id, dok_jenis);
    }

    public boolean isChecked() {
        return this.chk_id != null;
    }

    public static List<ChecklistPl> getListIjinUsaha(Dok_pl dok_pl) {
        if(dok_pl == null)
            return null;
        return find("ckm_id=1 and dll_id=?", dok_pl.dll_id).fetch();
    }
    
    public static List<ChecklistPl> getListIjinUsahaBaru(Dok_pl dok_pl) {
		if(dok_pl == null)
			return null;
		return find("ckm_id=50 and dll_id=? order by chk_id", dok_pl.dll_id).fetch();
	}
    
    public static List<ChecklistPl> getListIjinUsahaBaruFlash(Dok_pl dok_pl, List<ChecklistPl> ijinList) {
		if(dok_pl == null)
			return null;
		List<ChecklistPl> syarat = new ArrayList<ChecklistPl>();
       	Checklist_master ijin_master = Checklist_master.findById(50);
		ChecklistPl chkExist = null;
		if(!CommonUtil.isEmpty(ijinList)) {
            for (ChecklistPl obj : ijinList) {
            		if (obj != null) {
					obj.ckm_id = ijin_master.ckm_id;
					obj.dll_id = dok_pl.dll_id;
					if (obj.chk_id != null) 
						chkExist = ChecklistPl.findById(obj.chk_id);
					if (chkExist == null)
						obj.chk_id = null;
					syarat.add(obj);
				}
            }
        }
		return syarat;
	}

    public DokPenawaranPl getDokPenawaranByPeserta(Long psr_id) {
        DokPenawaranPl dokPenawaranPl1 =  DokPenawaranPl.findTeknisByPesertaAndChecklist(psr_id, this.chk_id);
        return dokPenawaranPl1;
    }
    
    public static List<ChecklistPl> getListSyaratKualifikasi(Dok_pl dok_pl, Kategori kategori) {
		List<ChecklistPl> syaratList = new ArrayList<ChecklistPl>();
       	List<Checklist_master> masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and (kgr_id=? or kgr_id is null) and ckm_id <> 1 order by ckm_checked desc, ckm_urutan asc, ckm_id asc",JenisChecklist.CHECKLIST_KUALIFIKASI, ChecklistStatus.AKTIF, kategori).fetch();
		ChecklistPl checklistpl = null;
	    for (Checklist_master checklist_master : masterList) {
            if(dok_pl == null) {
                checklistpl = new ChecklistPl();
                checklistpl.ckm_id = checklist_master.ckm_id;
                syaratList.add(checklistpl);
            }
			else if(checklist_master.isSyaratLain()) {
					List<ChecklistPl> syaratLain = find("ckm_id=? and dll_id=? order by chk_id", checklist_master.ckm_id, dok_pl.dll_id).fetch();
					syaratList.addAll(syaratLain);
			}
			else {
				checklistpl = find("ckm_id=? and dll_id=?", checklist_master.ckm_id, dok_pl.dll_id).first();
				if(checklistpl == null) {
						checklistpl = new ChecklistPl();
						checklistpl.ckm_id = checklist_master.ckm_id;
						checklistpl.dll_id = dok_pl.dll_id;					
				}
				syaratList.add(checklistpl);
			}
		}
		return syaratList;
	}

    public static List<ChecklistPl> getListSyaratKualifikasi(Dok_pl dok_pl, Kategori kategori, JenisChecklist jenis, boolean draft) {
    	List<ChecklistPl> syaratList = new ArrayList<ChecklistPl>();
    	Integer versi = findCurrentVersi(dok_pl.dll_id, JenisChecklist.CHECKLIST_LDK);
		Integer versiMaster = findCurrentVersi(dok_pl.dll_id, JenisChecklist.CHECKLIST_LDK, versi, kategori, draft);
		ChecklistPl checklistpl = null;
       	List<Checklist_master> masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and (kgr_id=? or kgr_id is null) and ckm_id <> 50 and (ckm_versi=? or ckm_versi=0) order by ckm_checked desc, ckm_urutan asc, ckm_id asc", jenis, ChecklistStatus.AKTIF, kategori, versiMaster).fetch();
       	for (Checklist_master checklist_master : masterList) {
	    		if(dok_pl == null) {
	    			checklistpl = new ChecklistPl();
	    			checklistpl.ckm_id = checklist_master.ckm_id;
	    			syaratList.add(checklistpl);
            }
			else if(checklist_master.isSyaratLain()) {
					List<ChecklistPl> syaratLain = find("ckm_id=? and dll_id=? and chk_versi=? order by chk_id", checklist_master.ckm_id, dok_pl.dll_id, versi).fetch();
					syaratList.addAll(syaratLain);
			}
			else {
				checklistpl = find("ckm_id=? and dll_id=? and chk_versi=?", checklist_master.ckm_id, dok_pl.dll_id, versi).first();
				if(checklistpl == null) {
						checklistpl = new ChecklistPl();
						checklistpl.ckm_id = checklist_master.ckm_id;
						checklistpl.dll_id = dok_pl.dll_id;					
				}
				syaratList.add(checklistpl);
			}
		}
		return syaratList;
	}
    
    public static List<ChecklistPl> getListSyaratKualifikasiFlash(Dok_pl dok_pl, List<ChecklistPl> syaratList, JenisChecklist jenis, Scope.Params params, Kualifikasi kualifikasi) {
		List<ChecklistPl> syarat = new ArrayList<ChecklistPl>();
		if(!CommonUtil.isEmpty(syaratList)) {
       		int index = 0;
       		for (ChecklistPl obj : syaratList) {
       			String ckm_id_index = params.get(jenis.name().toLowerCase() + "_ckm_id[" + index + "]");
        			Integer ckm_id = ckm_id_index != null ? Integer.valueOf(ckm_id_index) : null;
        			if (ckm_id != null) {
                		Checklist_master cm = Checklist_master.findById(ckm_id);
                    if (obj.ckm_id == null)
                    		obj.ckm_id = cm.ckm_id;
                    else 
                    		obj.chk_klasifikasi = "";
                		if (cm.isNotEditable()) {
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.setTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					}
                		syarat.add(obj);
                }
        			index++;
            }
        }
       	return syarat;		
	}

    public static List<ChecklistPl> getListSyarat(Dok_pl dok_pl, JenisChecklist jenis, Kategori kategori, boolean draft)
    {
        List<ChecklistPl> syaratList = new ArrayList<ChecklistPl>();
        ChecklistPl checklist = null;
        Integer versi = findCurrentVersi(dok_pl.dll_id, JenisChecklist.CHECKLIST_SYARAT);
		Integer versiMaster = findCurrentVersi(dok_pl.dll_id, JenisChecklist.CHECKLIST_SYARAT, versi, kategori, draft);		
		if (kategori.isKonsultansiPerorangan() && jenis == JenisChecklist.CHECKLIST_TEKNIS && versiMaster.intValue() == 1) kategori = Kategori.KONSULTANSI;
			List<Checklist_master> masterList = new ArrayList<Checklist_master>();			
		if (!kategori.isKonstruksi() && jenis == JenisChecklist.CHECKLIST_ADMINISTRASI && versiMaster.intValue() == 1 )
			masterList = Checklist_master.find("ckm_id in (16, 18) and ckm_status=? order by ckm_checked desc, ckm_urutan asc, ckm_id asc", ChecklistStatus.AKTIF).fetch();				
		else
			masterList = Checklist_master.find("ckm_jenis=? and ckm_status=? and (kgr_id=? or kgr_id is null) and (ckm_versi=? or ckm_versi=0) order by ckm_checked desc, ckm_urutan asc, ckm_id asc", jenis, ChecklistStatus.AKTIF, kategori, versiMaster).fetch();		
		for (Checklist_master checklist_master : masterList) {
            if(dok_pl == null) {
                checklist = new ChecklistPl();
                checklist.ckm_id = checklist_master.ckm_id;
                checklist.chk_nama = checklist_master.ckm_nama;
                if(checklist_master.ckm_checked == 1) {
                    checklist.save();
                }
                syaratList.add(checklist);
            }
            else if(checklist_master.isSyaratLain()) {
                List<ChecklistPl> syaratLain = find("ckm_id=? and dll_id=? and chk_versi=? order by chk_id", checklist_master.ckm_id, dok_pl.dll_id, versi).fetch();
                syaratList.addAll(syaratLain);
            }
            else {
                checklist = find("ckm_id=? and dll_id=? and chk_versi=?", checklist_master.ckm_id, dok_pl.dll_id, versi).first();
                if(checklist == null) {
                    checklist = new ChecklistPl();
                    checklist.ckm_id = checklist_master.ckm_id;
                    checklist.dll_id = dok_pl.dll_id;
                    checklist.chk_nama = checklist_master.ckm_nama;
                    if(checklist_master.ckm_checked == 1) {
                        checklist.save();
                    }
                }
                syaratList.add(checklist);
            }
        }
        return syaratList;
    }

    public static List<ChecklistPl> getListChecklist(Dok_pl dok_lelang, Checklist_master.JenisChecklist jenis){

        return find("dll_id=? AND ckm_id IN (SELECT ckm_id FROM checklist_master WHERE ckm_jenis = ?)", dok_lelang.dll_id, jenis.id).fetch();

    }

    public static List<ChecklistPl> getListChecklist(Long dllId, Checklist_master.JenisChecklist jenis){
        return find("dll_id=? AND ckm_id IN (SELECT ckm_id FROM checklist_master WHERE ckm_jenis = ?)",
                dllId, jenis.id).fetch();
    }

    public static String getInfoSyaratTeknis(Dok_pl dok_pl) {
        String result= Messages.get("nonlelang.blm_ada_data");
        long count = count("dll_id=? and ckm_id in (select ckm_id from Checklist_master where ckm_jenis=? and ckm_status=?)", dok_pl.dll_id, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS, Checklist_master.ChecklistStatus.AKTIF);
        if(count > 0)
            result = count +" checklist";
        return result;
    }

    public static String getInfoSyaratKualifikasi(Dok_pl dok_pl) {
        String result= Messages.get("nonlelang.blm_ada_data");
        long count = count("dll_id=? and ckm_id in (select ckm_id from Checklist_master where ckm_jenis=? and ckm_status=?)", dok_pl.dll_id, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI, Checklist_master.ChecklistStatus.AKTIF);
        if(count > 0)
            result = count +" checklist";
        return result;
    }

        public static void simpanChecklist(Dok_pl dok_pl, List<ChecklistPl> syaratList, Checklist_master.JenisChecklist jenis, Integer versi) {
        List<Long> saveCheck = new ArrayList<Long>();
        ChecklistPl chkExist = null;
        for(ChecklistPl obj: syaratList) {
            if(obj.ckm_id != null) {
                if(CommonUtil.isEmpty(obj.chk_nama))
                    obj.chk_nama="";
                if(Checklist_master.count("ckm_nama=?", obj.chk_nama) > 0) // check persyaratan jika sama dengan checklist master, tidak perlu disimpan
					continue;
				if(Checklist_master.isSyaratLain(obj.ckm_id)) { // check jika ada syarat lain yg sama kontennya
					chkExist = ChecklistPl.find("ckm_id=? AND chk_nama=? AND dll_id=? AND chk_versi=?", obj.ckm_id, obj.chk_nama, dok_pl.dll_id, versi).first();
					if(chkExist != null) {
						saveCheck.add(chkExist.chk_id);
						continue;
					}
                }
				if(obj.chk_id == null) {
					obj.dll_id = dok_pl.dll_id;
					obj.chk_versi = versi;
					obj.save();
					saveCheck.add(obj.chk_id);
				}
                else {
                    chkExist = ChecklistPl.findById(obj.chk_id);
                    chkExist.dll_id = dok_pl.dll_id;
                    chkExist.ckm_id = obj.ckm_id;
                    chkExist.chk_nama = obj.chk_nama;
                    chkExist.chk_klasifikasi = obj.chk_klasifikasi;
                    chkExist.chk_versi = versi;
                    chkExist.save();
                    saveCheck.add(chkExist.chk_id);
                }
            }
        }
        String sql="delete from ekontrak.checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)";
        if(!CommonUtil.isEmpty(saveCheck)) {
            String join = StringUtils.join(saveCheck, ",");
            sql += " and chk_id not in ("+join+ ')';
        }
        Query.update(sql, dok_pl.dll_id, jenis, Checklist_master.ChecklistStatus.AKTIF);
    }
    
    public static void simpanChecklist(Dok_pl dok_pl, List<ChecklistPl> ijinList, List<ChecklistPl> syaratList, JenisChecklist jenis, Scope.Params params, Integer versi, Kualifikasi kualifikasi) throws Exception {
		Checklist_master ijin_master = Checklist_master.findById(50);
		List<Long> saveCheck = new ArrayList<Long>();
		ChecklistPl chkExist = null;
		if(!CommonUtil.isEmpty(ijinList)) {
            for (ChecklistPl obj : ijinList) { 
            		if (obj != null) {
            			obj.ckm_id = ijin_master.ckm_id;
					if (CommonUtil.isEmpty(obj.chk_nama))
						throw new Exception("Izin Usaha wajib diisi!");
					if (obj.chk_id == null) {
						obj.dll_id = dok_pl.dll_id;
						obj.save();
						saveCheck.add(obj.chk_id);
					} else {
						chkExist = ChecklistPl.findById(obj.chk_id);
						if (chkExist != null) {
							chkExist.dll_id = dok_pl.dll_id;
							chkExist.chk_nama = obj.chk_nama;
							chkExist.chk_klasifikasi = obj.chk_klasifikasi;
							chkExist.chk_versi = versi;
							chkExist.save();
							saveCheck.add(chkExist.chk_id);
						} else {
							obj.chk_id = null;
							obj.dll_id = dok_pl.dll_id;
							obj.save();
	                        saveCheck.add(obj.chk_id);
						}						
					}
				}
            }
        }
		
		if(!CommonUtil.isEmpty(syaratList)) {
            for (ChecklistPl obj : syaratList) {
            	    if (obj != null && obj.ckm_id != null) {
                		Checklist_master cm = Checklist_master.findById(obj.ckm_id);
                    if (cm.isNotEditable()){
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.validateTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					} else {
						if (CommonUtil.isEmpty(obj.chk_nama)) { //deskripsi wajib diisi
	                    		throw new Exception("\"" + cm.ckm_nama + "\" wajib diisi jika diceklist (dipersyaratkan)!");
	                    }
	                    if(obj.isBankSupportSpec()){
	                    		obj.chk_nama = obj.chk_nama + "%";
						}
                    }
                    if (obj.chk_id == null) {
                        obj.dll_id = dok_pl.dll_id;
                        obj.save();
                        saveCheck.add(obj.chk_id);
                    } else {
                        chkExist = ChecklistPl.findById(obj.chk_id);
                        if (chkExist != null) {
	                        chkExist.dll_id = dok_pl.dll_id;
	                        chkExist.ckm_id = obj.ckm_id;
	                        chkExist.chk_nama = obj.chk_nama;
	                        chkExist.save();
	                        saveCheck.add(chkExist.chk_id);
                        } else {
	                        	obj.chk_id = null;
	    						obj.dll_id = dok_pl.dll_id;
	    						obj.save();
	    						saveCheck.add(obj.chk_id);
                        }
                    }
                }
            }
        }
		
		// hapus list lain yang tidak dipilih oleh user
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			//hapus cheklist evaluasi terlebih dahulu issue #344
			String sqlChecklistEvaluasi="delete from checklist_evaluasi where chk_id IN (select chk_id from checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+"))";

			Query.update(sqlChecklistEvaluasi, dok_pl.dll_id, jenis, ChecklistStatus.AKTIF);

			String sql="delete from ekontrak.checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+ ')';
			Query.update(sql, dok_pl.dll_id, jenis, ChecklistStatus.AKTIF);
		}
	}
    
    public static void simpanChecklist(Dok_pl dok_pl, List<ChecklistPl> syaratList, JenisChecklist jenis, Scope.Params params, Integer versi, Kualifikasi kualifikasi) throws Exception {
		List<Long> saveCheck = new ArrayList<Long>();
		ChecklistPl chkExist = null;
		if(!CommonUtil.isEmpty(syaratList)) {
        		for (ChecklistPl obj : syaratList) {
            		if (obj != null && obj.ckm_id != null) {
                		Checklist_master cm = Checklist_master.findById(obj.ckm_id);
                    if (cm.isNotEditable()){
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.validateTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					} else {
						if (CommonUtil.isEmpty(obj.chk_nama)) { //deskripsi wajib diisi
		                    throw new Exception("\"" + cm.ckm_nama + "\" wajib diisi jika diceklist (dipersyaratkan)!");
	                    }
	                    if(obj.isBankSupportSpec()){
	                    		obj.chk_nama = obj.chk_nama + "%";
						}
                    }
                    if (obj.chk_id == null) {
                    		obj.dll_id = dok_pl.dll_id;
                    		obj.save();
                    		saveCheck.add(obj.chk_id);
                    } else {
                    	   chkExist = ChecklistPl.findById(obj.chk_id);
                        if (chkExist != null) {
	                        chkExist.dll_id = dok_pl.dll_id;
	                        chkExist.ckm_id = obj.ckm_id;
	                        chkExist.chk_nama = obj.chk_nama;
	                        chkExist.chk_versi = versi;
	                        chkExist.save();
	                        saveCheck.add(chkExist.chk_id);
                        } else {
	                        	obj.chk_id = null;
	    						obj.dll_id = dok_pl.dll_id;
	    						obj.chk_versi = versi;
	    						obj.save();
	    						saveCheck.add(obj.chk_id);
                        }
                    }
                }
            }
        }
		
		// hapus list lain yang tidak dipilih oleh user
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			//hapus cheklist evaluasi terlebih dahulu issue #344
			String sqlChecklistEvaluasi="delete from ekontrak.checklist_evaluasi where chk_id IN (select chk_id from ekontrak.checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+"))";

			Query.update(sqlChecklistEvaluasi, dok_pl.dll_id, jenis, ChecklistStatus.AKTIF);

			String sql="delete from ekontrak.checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+ ')';
			Query.update(sql, dok_pl.dll_id, jenis, ChecklistStatus.AKTIF);
		} else {
			//hapus cheklist evaluasi terlebih dahulu issue #344
			String sqlChecklistEvaluasi="delete from ekontrak.checklist_evaluasi where chk_id IN (select chk_id from ekontrak.checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?))";
			Query.update(sqlChecklistEvaluasi, dok_pl.dll_id, jenis, ChecklistStatus.AKTIF);
			
			String sql="delete from ekontrak.checklist where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)";
			Query.update(sql, dok_pl.dll_id, jenis, ChecklistStatus.AKTIF);
		}
	}

    public static void simpanChecklist(Dok_pl dok_pl, List<ChecklistPl> ijinList, List<ChecklistPl> syarat, Scope.Params params, Integer versi, Kualifikasi kualifikasi) throws Exception {
		Checklist_master ijin_master = Checklist_master.findById(1);
		List<Long> saveCheck = new ArrayList<Long>();
		ChecklistPl chkExist = null;
        if(!CommonUtil.isEmpty(ijinList)) {
            for (ChecklistPl obj : ijinList) { // untuk izin usaha
				if (obj != null) {
					obj.ckm_id = ijin_master.ckm_id;
					if (CommonUtil.isEmpty(obj.chk_nama)) {
						throw new Exception("Izin Usaha wajib diisi!");
					}
					if (obj.chk_id == null) {
						obj.dll_id = dok_pl.dll_id;
						obj.chk_versi = versi;
						obj.save();
						saveCheck.add(obj.chk_id);
					} else {
						chkExist = ChecklistPl.findById(obj.chk_id);
						chkExist.dll_id = dok_pl.dll_id;
						chkExist.chk_nama = obj.chk_nama;
						chkExist.chk_klasifikasi = obj.chk_klasifikasi;
						chkExist.chk_versi = versi;
						chkExist.save();
						saveCheck.add(chkExist.chk_id);
					}
				}
            }
        }
        if(!CommonUtil.isEmpty(syarat)) {
            for (ChecklistPl obj : syarat) {
                if (obj.ckm_id != null) {
                	Checklist_master cm = Checklist_master.findById(obj.ckm_id);
                    if (cm.isNotEditable()){
	                    obj.chk_nama = "";
                    } else if(cm.table_header != null && cm.table_header != "") {
						JsonArray values = new JsonArray();
						cm.validateTableHeader(values, params, kualifikasi);
						if(values != null)
							obj.chk_nama = values.toString();
					} else {
	                    if (CommonUtil.isEmpty(obj.chk_nama) && !cm.isChecked()) { //deskripsi wajib diisi
		                    throw new Exception("\"" + cm.ckm_nama + "\" wajib diisi jika diceklis (dipersyaratkan)!");
	                    }
	                    if(obj.isBankSupportSpec()){
	                    	obj.chk_nama = obj.chk_nama + "%";
						}
                    }
                    if (obj.chk_id == null) {
                        obj.dll_id = dok_pl.dll_id;
                        obj.chk_versi = versi;
                        obj.save();
                        saveCheck.add(obj.chk_id);
                    } else {
                        chkExist = ChecklistPl.findById(obj.chk_id);
						chkExist.dll_id = dok_pl.dll_id;
                        chkExist.ckm_id = obj.ckm_id;
                        chkExist.chk_nama = obj.chk_nama;
                        chkExist.chk_versi = versi;
                        chkExist.save();
                        saveCheck.add(chkExist.chk_id);
                    }
                }
            }
        }
		// hapus list lain yang tidak dipilih oleh user
		if(!CommonUtil.isEmpty(saveCheck)) {
			String join = StringUtils.join(saveCheck, ",");
			//hapus cheklist evaluasi terlebih dahulu issue #344
			String sqlChecklistEvaluasi="delete from ekontrak.checklist_evaluasi where chk_id IN (select chk_id from ekontrak.checklist where dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+"))";
			Query.update(sqlChecklistEvaluasi, dok_pl.dll_id, versi, JenisChecklist.CHECKLIST_KUALIFIKASI, ChecklistStatus.AKTIF);

			String sql="delete from ekontrak.checklist where dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=? and ckm_status=?)"
					+ " and chk_id not in ("+join+ ')';
			Query.update(sql, dok_pl.dll_id, versi, JenisChecklist.CHECKLIST_KUALIFIKASI, ChecklistStatus.AKTIF);
		}
	}

    public static List<ChecklistPl> getCheckListFromLelang(Long lelangId,Integer jenis_dok, boolean all_data) {
        List<ChecklistPl> checklist = null;
        if (!all_data) {
            checklist = find("dll_id in (select dll_id from dok_pl where lls_id=? and dok_jenis=?) AND ckm_id IS NULL",
                    lelangId, jenis_dok).fetch();
        } else {
            checklist = find("dll_id in (select dll_id from dok_pl where lls_id=? and dok_jenis=?) ORDER BY chk_id",
                    lelangId, jenis_dok).fetch();
        }
        return checklist;
    }

    // copy checklist persyaratan
    public static void copyChecklist(Long dok_id_source, Long dok_id_dest) {
        List<ChecklistPl> list = find("dll_id=?", dok_id_source).fetch();
        if(CommonUtil.isEmpty(list))
            return;
        ChecklistPl obj = null;
        for(ChecklistPl checklist:list) {
            obj = new ChecklistPl();
            obj.ckm_id = checklist.ckm_id;
            obj.chk_nama = checklist.chk_nama;
            obj.chk_klasifikasi = checklist.chk_klasifikasi;
            obj.dll_id = dok_id_dest;
            obj.save();
        }
    }
    
    public static void copyChecklist(Dok_pl dok_pl, List<Integer> jenis_checklist) {
		String jenis = StringUtils.join(jenis_checklist, ',');
		List<ChecklistPl_tmp> list = ChecklistPl_tmp.find("dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+")) order by ckm_id asc, chk_id asc", dok_pl.dll_id).fetch();
		Integer currentVersi = ChecklistPl.findCurrentVersi(dok_pl.dll_id, jenis_checklist);
		ChecklistPl obj = null;
		for(ChecklistPl_tmp checklist:list) {
			obj = new ChecklistPl();
			obj.ckm_id = checklist.ckm_id;
			obj.chk_nama = checklist.chk_nama;
			obj.chk_klasifikasi = checklist.chk_klasifikasi;
			obj.dll_id = checklist.dll_id;
			obj.chk_versi = currentVersi + 1;
			obj.save();
		}
		Query.update("delete from ekontrak.checklist_tmp where dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", dok_pl.dll_id);
	}
    
    public static void copyChecklistTmp(Dok_pl dok_pl) {
        String sql="delete from ekontrak.checklist where dll_id=?";
        Query.update(sql, dok_pl.dll_id);
        ChecklistPl obj = null;
        List<ChecklistPl_tmp> list = ChecklistPl_tmp.find("dll_id=? order by ckm_id asc, chk_id asc", dok_pl.dll_id).fetch();
        for(ChecklistPl_tmp checklist:list) {
            obj = new ChecklistPl();
            obj.ckm_id = checklist.ckm_id;
            obj.chk_nama = checklist.chk_nama;
            obj.chk_klasifikasi = checklist.chk_klasifikasi;
            obj.dll_id = checklist.dll_id;
            obj.save();
        }
        sql="delete from ekontrak.checklist_tmp where dll_id=?";
        Query.update(sql, dok_pl.dll_id);
    }

    public int getSortingRule() {
        if (getChecklist_master().isConsultantPL()) {
            return 1;
        } else if (getChecklist_master().isSyaratLain()) {
            return 2;
        }
        return 0;
    }
    
    public boolean isBankSupportSpec() {
		return this.ckm_id != null && this.ckm_id.equals(2);
	}
    
    @Override
    public Long getChecklistId() {
        return this.chk_id;
    }
    @Override
    public void setDokumenPenawarans(List<DokPenawaranPl> items) {
        this.dokPenawaranPls = items;
    }

    private static final Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();

    @Transient
    public List<Map<String, String>> getJsonName() {
        try {
            return Json.fromJson(chk_nama, type);
        } catch (Exception e) {
            Logger.debug("Json Invalid");
        }
        return null;
    }
    
    public static boolean isChecklistEmpty(Long dll_id) {
		return Query.count("select count(chk_id) from ekontrak.checklist where dll_id=?", dll_id) == 0;
	}
	
	public static boolean isChecklistEmpty(Long dll_id, List<Integer> jenis_checklist) {
		String jenis = StringUtils.join(jenis_checklist, ',');
		Integer versi = findCurrentVersi(dll_id, jenis_checklist);		
		return Query.count("select count(chk_id) from ekontrak.checklist where dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", dll_id, versi) == 0;
	}

	public static Integer findCurrentVersi(Long dll_id, List<Integer> jenis_checklist) {
		String jenis = StringUtils.join(jenis_checklist, ',');
		Integer count = Query.find("SELECT max(chk_versi) FROM ekontrak.checklist WHERE dll_id=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", Integer.class, dll_id).first();
		return count != null ? count : Integer.valueOf(1);
	}

	public static Integer findCurrentVersi(Long dll_id, List<Integer> jenis_checklist, Integer chk_versi, Kategori kategori, boolean draft) {
        String jenis = StringUtils.join(jenis_checklist, ',');
        Integer versi = Query.find("SELECT max(ckm_versi) FROM checklist_master WHERE ckm_jenis in ("+jenis+") and ckm_id in (select ckm_id from ekontrak.checklist where dll_id=? and chk_versi=?)", Integer.class, dll_id, chk_versi).first();
        if (versi == null || draft)
        	    versi = Query.find("SELECT max(ckm_versi) FROM checklist_master WHERE ckm_jenis in ("+jenis+") and kgr_id=?", Integer.class, kategori).first();			
        return versi != null ? versi : Integer.valueOf(1);
    }

    public static boolean isSyaratKualifikasiBaru(Dok_pl dok_pl) {
		ChecklistPl checklist = find("dll_id=? and ckm_id in (1,2,3,4,5,9,10,11,12,13,14,15,28,38)", dok_pl.dll_id).first();
		ChecklistPl checklistBaru = find("dll_id=? and ckm_id > 49", dok_pl.dll_id).first();
		if (checklist == null && checklistBaru == null) {
			return true;
		} else {
			if (checklistBaru != null) return true;
			else return false;
		}
	}
    
    /**
	 * This method is used to check whether is it valid percentage range 10-100% from chk_name
	 * @return boolean*/
    public boolean isValidForPercentage() {
		if(!StringUtils.isEmpty(chk_nama)) {
			final Integer i = Integer.valueOf(chk_nama);
			return i >= 10 && i <= 100;
		}
		return false;
	}
}
