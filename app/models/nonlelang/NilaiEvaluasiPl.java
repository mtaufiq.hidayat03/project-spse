package models.nonlelang;

import ext.DecimalBinder;
import ext.RupiahBinder;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.EvaluasiPl.JenisEvaluasi;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Lambang on 2/21/2017.
 */

@Table(name = "EKONTRAK.NILAI_EVALUASI")
public class NilaiEvaluasiPl extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum StatusNilaiEvaluasi {
        TDK_LULUS("Tidak Lulus"), LULUS("Lulus"), LULUS_TDK_SHORTLIST("Lulus Bukan Daftar Pendek");

        public final String label;

        private StatusNilaiEvaluasi(String label) {
            this.label = label;
        }

        public boolean isLulus() {
            return this == LULUS;
        }
    }

    @Id(sequence = "ekontrak.seq_nilai_evaluasi", function = "nextsequence")
    public Long nev_id;

    @As(binder = RupiahBinder.class)
    public Double nev_harga;

    @As(binder = RupiahBinder.class)
    public Double nev_harga_terkoreksi;

    @As(binder = RupiahBinder.class)
    public Double nev_harga_negosiasi;

    @As(binder = DecimalBinder.class)
    public Double nev_skor;

    public Integer nev_urutan;

    public String nev_uraian;
    @Required
    public StatusNilaiEvaluasi nev_lulus;

    public Long nev_id_attachment;

    //relasi ke Evaluasi
    public Long eva_id;

    //relasi ke Peserta
    public Long psr_id;

    @Transient
    private PesertaPl peserta;
    @Transient
    private EvaluasiPl evaluasi;

    public PesertaPl getPeserta() {
        if (peserta == null)
            peserta = PesertaPl.findById(psr_id);
        return peserta;
    }

    public EvaluasiPl getEvaluasi() {
        if (evaluasi == null)
            evaluasi = EvaluasiPl.findById(eva_id);
        return evaluasi;
    }


    @Transient
    public boolean isHargaOverHps(Double hps) {
        if (nev_harga != null) {
            return nev_harga.doubleValue() > hps.doubleValue();
        }
        return false;
    }

    @Transient
    public boolean isHargaOverPagu(Double pagu) {
        if (nev_harga != null) {
            return nev_harga.doubleValue() > pagu.doubleValue();
        }
        return false;
    }

    public String getAlasan() {
        if (CommonUtil.isEmpty(nev_uraian))
            return "";
        return nev_uraian;
    }

    @Transient
    public boolean isHargaTerkoreksiOverHps(Double hps) {
        if (nev_harga_terkoreksi != null) {
            return nev_harga_terkoreksi.doubleValue() > hps.doubleValue();
        }
        return false;
    }

    @Transient
    public boolean isHargaTerkoreksiOverPagu(Double pagu) {
        if (nev_harga_terkoreksi != null) {
            return nev_harga_terkoreksi.doubleValue() > pagu.doubleValue();
        }
        return false;
    }

    @Transient
    public boolean isAlasanDiisi() {
        if (!nev_lulus.isLulus()) {
            if (nev_uraian != null)
                return nev_uraian.length() > 5;
            else
                return false;
        }
        return true;
    }

    @Transient
    public boolean isLulus() {
        if (nev_lulus != null) {
            return nev_lulus.isLulus();
        }
        return false;
    }

    @Transient
    public void setLulus(boolean lulus) {
        nev_lulus = lulus ? StatusNilaiEvaluasi.LULUS : StatusNilaiEvaluasi.TDK_LULUS;
    }

    @Transient
    public boolean isShortlist() {
        if (nev_lulus != null) {
            return nev_lulus.isLulus();
        }
        return false;
    }

    @Transient
    public void setShortlist(boolean shortlist) {
        if (shortlist)
            nev_lulus = StatusNilaiEvaluasi.LULUS;
        else if (isLulus())
            nev_lulus = StatusNilaiEvaluasi.LULUS_TDK_SHORTLIST;
    }

    public static List<NilaiEvaluasiPl> findBy(Long evaluasiId) {
        return find("eva_id= ? ORDER BY psr_id ASC, nev_urutan ASC", evaluasiId).fetch();
    }

    public static List<NilaiEvaluasiPl> findWithUrutHarga(Long evaluasiId) {
        return find("eva_id= ? ORDER BY nev_harga ASC, nev_urutan ASC", evaluasiId).fetch();
    }

    public static List<NilaiEvaluasiPl> findByWithNevUrut(Long evaluasiId) {
        return find("eva_id= ? ORDER BY nev_urutan ASC", evaluasiId).fetch();
    }

    public static NilaiEvaluasiPl findByNevId(Long id) {
        return find("nev_id = ?", id).first();
    }

    public static int countLulus(Long evaluasiId) {
        return (int) count("eva_id=? and nev_lulus=?", evaluasiId, StatusNilaiEvaluasi.LULUS.ordinal());
    }

    public static int countLulus(Long lelangId, EvaluasiPl.JenisEvaluasi jenis) {
        return countLulus(lelangId, jenis, EvaluasiPl.findCurrentVersi(lelangId));
    }

    public static int countLulus(Long lelangId, EvaluasiPl.JenisEvaluasi jenis, Integer versi) {
        return (int) count("eva_id in (select eva_id from ekontrak.evaluasi where lls_id=? and eva_jenis=? and eva_versi=?) and nev_lulus=?", lelangId, jenis, versi, StatusNilaiEvaluasi.LULUS);
    }

    public static int countLulus(Long lelangId, List<EvaluasiPl.JenisEvaluasi> jenisEvaluasiList) {
        // hitung kelulusan dari seluruh jenis nilai evaluasi

        Integer versi = EvaluasiPl.findCurrentVersi(lelangId);
        StringBuilder andEvaJenis = new StringBuilder(" AND eva_jenis IN (");
        for (Iterator<EvaluasiPl.JenisEvaluasi> it = jenisEvaluasiList.iterator(); it.hasNext(); ) {
            andEvaJenis.append(it.next().id);
            if (it.hasNext())
                andEvaJenis.append(", ");
        }
        andEvaJenis.append(") ");

        return (int) Query.count("SELECT COUNT(*) FROM ( " +
                " SELECT COUNT(*) jml_lulus, psr_id FROM ekontrak.nilai_evaluasi " +
                " WHERE eva_id IN ( " +
                "   SELECT eva_id FROM ekontrak.evaluasi WHERE lls_id = ? AND eva_versi = ? " + andEvaJenis +
                " ) AND nev_lulus = ? " +
                " GROUP by psr_id " +
                ") lulus WHERE jml_lulus = ? ", lelangId, versi, StatusNilaiEvaluasi.LULUS, jenisEvaluasiList.size());
    }

    public static int countTidakLulus(Long lelangId, EvaluasiPl.JenisEvaluasi jenis, Integer versi) {
        return (int) count("eva_id in (select eva_id from ekontrak.evaluasi where lls_id=? and eva_jenis=? and eva_versi=?) and nev_lulus=?", lelangId, jenis, versi, StatusNilaiEvaluasi.TDK_LULUS);
    }

    public static boolean isLulus(EvaluasiPl.JenisEvaluasi jenis, PesertaPl peserta) {
        Long lelangId = peserta.lls_id;
        int count = (int) count("eva_id in (select eva_id from ekontrak.evaluasi where lls_id=? and eva_jenis=? and eva_versi=?) and nev_lulus=? and psr_id=?", lelangId, jenis, EvaluasiPl.findCurrentVersi(lelangId), StatusNilaiEvaluasi.LULUS, peserta.psr_id);
        return count > 0;
    }

    public static NilaiEvaluasiPl findBy(Long evaluasiId, Long pesertaId) {
        return find("eva_id=? and psr_id=?", evaluasiId, pesertaId).first();
    }

    public static NilaiEvaluasiPl findByPesertaAndEvaluasi(Long psr_id, Long eva_id) {
        return find("psr_id = ? AND eva_id = ?", psr_id, eva_id).first();
    }
    
    public static List<PesertaPl> findPemenangPrakualifikasi(Long lls_id){
		List<NilaiEvaluasiPl> lulusPrakualifikasi = find("eva_id IN (SELECT eva_id FROM ekontrak.evaluasi WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1) and nev_lulus=?",
				lls_id, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI, StatusNilaiEvaluasi.LULUS).fetch();
		List<PesertaPl> pemenangPrakualifikasi = lulusPrakualifikasi.stream().map(e -> e.getPeserta()).collect(Collectors.toList());
		return pemenangPrakualifikasi;
	}

    public static NilaiEvaluasiPl findNCreateBy(Long evaluasiId, PesertaPl peserta, JenisEvaluasi jenis) {
        NilaiEvaluasiPl nilai = findBy(evaluasiId, peserta.psr_id);
        if (nilai == null) {
            nilai = new NilaiEvaluasiPl();
            nilai.eva_id = evaluasiId;
            nilai.psr_id = peserta.psr_id;
            nilai.nev_lulus = StatusNilaiEvaluasi.TDK_LULUS;
            if (jenis.isHarga() || jenis.isAkhir()) {
                nilai.nev_harga = peserta.psr_harga;
                nilai.nev_harga_terkoreksi = peserta.psr_harga_terkoreksi;
            }
            nilai.save();
        }
        return nilai;
    }

    public static List<NilaiEvaluasiPl> findByRekanan(Long rekananId, int jenis, Long lulus) {
        return find("nev_id in (select nev_id from ekontrak.nilai_evaluasi n, ekontrak.peserta p, rekanan r , ekontrak.evaluasi e " +
                "where n.eva_id = e.eva_id and n.psr_id=p.psr_id and p.rkn_id=r.rkn_id and e.eva_jenis =? and n.nev_lulus =? and r.rkn_id=?)", jenis, lulus, rekananId).fetch();
    }

    public static NilaiEvaluasiPl findByLulusAndLelang(Long lls_id) {
        return find("nev_id in (SELECT ne.nev_id FROM ekontrak.nilai_evaluasi ne " +
                "INNER JOIN ekontrak.peserta_nonlelang p ON ne.psr_id = p.psr_id " +
                "WHERE p.lls_id = ? AND ne.nev_lulus = ? AND ne.nev_urutan = ?)", lls_id, 1, 1).first();
    }

    public String calculatePrecentageHarga(Double nilai_hps) {

        DecimalFormat df = new DecimalFormat("#.00");

        return df.format((nev_harga * 100) / nilai_hps);
    }

    public String calculatePrecentageHargaTerkoreksi(Double nilai_hps) {

        DecimalFormat df = new DecimalFormat("#.00");

        return df.format((nev_harga_terkoreksi * 100) / nilai_hps);
    }

    public Double hargaFinal(){
        return nev_harga_negosiasi != null ? nev_harga_negosiasi : nev_harga_terkoreksi;
    }

    public static List<NilaiEvaluasiPl> findAllWinnerCandidate(Long evaId, Pl_seleksi pl){

        if(!pl.isExpress()) {
            return find("eva_id = ? and " +
                    "psr_id in (SELECT p.psr_id " +
                    "FROM ekontrak.peserta_nonlelang p where p.sudah_verifikasi_sikap = 1 " +
                    "and p.lls_id = ?) order by nev_urutan", evaId, pl.lls_id).fetch();
        }

        return find("eva_id=? and nev_urutan is not null order by nev_urutan", evaId).fetch();
    }

}
