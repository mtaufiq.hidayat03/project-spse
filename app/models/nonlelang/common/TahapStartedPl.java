package models.nonlelang.common;

import controllers.BasicCtr;
import models.common.*;
import models.lelang.Evaluasi;
import models.lelang.Jadwal;
import models.lelang.Nilai_evaluasi;
import models.lelang.Peserta;
import models.nonlelang.EvaluasiPl;
import models.nonlelang.Jadwal_pl;
import models.nonlelang.Pl_seleksi;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 2/14/2017.
 */
public class TahapStartedPl {

    public final Long plId;
    final List<Tahap> tahapList;
    final Active_user active_user;
    StatusPl status;

    public TahapStartedPl(Long plId) {
        this.plId = plId;
        Query.find("SELECT lls_status, mtd_pemilihan FROM ekontrak.nonlelang_seleksi WHERE lls_id=?", new ResultSetHandler<Object>(){

            @Override
            public Object handle(ResultSet rs)throws SQLException{
                status = StatusPl.fromValue(rs.getInt("lls_status"));
                MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
                return null;
            }

        }, plId).first();
        this.tahapList = AktivitasPl.findTahapStarted(plId);
        this.active_user = Active_user.current();
    }

    public TahapStartedPl(Pl_seleksi pl) {
        this.plId = pl.lls_id;
        this.status = pl.lls_status;
        this.tahapList = AktivitasPl.findTahapStarted(plId);
        this.active_user = Active_user.current();
        this.status = Pl_seleksi.findStatusLelang(plId);
//		Logger.info(tahapList.toString());
    }

    public boolean isPenjelasan() {
        return tahapList != null && tahapList.contains(Tahap.PENJELASAN);
    }

    public boolean isPembuatanBAHP() {
        return tahapList != null && tahapList.contains(Tahap.EVALUASI_PENAWARAN);
    }

    @SuppressWarnings(value = {"unchecked"})
    public boolean isPembukaan() {
        return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanPembukaan);
    }

    public boolean isEvaluasiPra() {
        return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasiPra);
    }

    @SuppressWarnings(value = {"unchecked"})
    public boolean isPembukaanTeknis() {
        return tahapList != null && tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS);
    }

    @SuppressWarnings(value = {"unchecked"})
    public boolean isPembukaanHarga() {
        return tahapList != null && (
                tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA)
                        || tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN_BIAYA)
        );
    }

    public boolean isEvaluasi() {
        return tahapList.contains(Tahap.EVALUASI_PENAWARAN);
    }
    
    public boolean isEvaluasiPenawaranPl() {
        return tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN);
    }
    
    public boolean isEvaluasiPl() {
        return tahapList.contains(Tahap.EVALUASI_DOK_PRA);
    }

    public boolean isEvaluasiTeknis() {
        return tahapList != null && tahapList.contains(Tahap.EVALUASI_PENAWARAN_ADM_TEKNIS);
    }

    public boolean isEvaluasiHarga() {
        return tahapList != null && tahapList.contains(Tahap.EVALUASI_PENAWARAN_BIAYA);
    }

    public boolean isPengumuman() {
        return CollectionUtils.containsAny(tahapList, Tahap.tahapanPengumumanHasilPl);
    }

    public boolean isPengumumanPemenangAdmTeknis() {
        return tahapList != null && tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS);
    }
    
    public boolean isPengumumanPemenangPra() {
		return tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK) 
				|| tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA) || tahapList.contains(Tahap.PENETAPAN_HASIL_PRA));
	}

    public boolean isPengumumanPemenangAkhir() {
        return tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI);
    }
    
    public boolean isPengumumanPemenangAkhirNew() {
        return tahapList != null && tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR);
    }

    public boolean isSanggahPra() {
        return tahapList != null && tahapList.contains(Tahap.SANGGAH_PRA);
    }

    public boolean isSanggahAdm() {
        return tahapList != null && tahapList.contains(Tahap.SANGGAH_ADM_TEKNIS);
    }

    public boolean isSanggah() {
        return tahapList != null && tahapList.contains(Tahap.SANGGAH);
    }

    public boolean isKontrak() {
        return tahapList != null && tahapList.contains(Tahap.TANDATANGAN_KONTRAK);
    }

    public boolean isPenunjukanPemenang() {
        return tahapList != null && tahapList.contains(Tahap.PENUNJUKAN_PEMENANG);
    }

    public boolean isKlarifikasiNegosiasi() {
        return tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI);
    }

    public boolean isShowPenyedia() {

        return tahapList != null && (tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN) || 
        		(tahapList.contains(Tahap.EVALUASI_DOK_PRA)));

    }

    /**
     * memeriksa apakah tahap pemasukan sudah berakhir atau belum
     * true : jika sudah berakhir
     * false : jika belum berakhir
     * @return
     */
    public boolean isPemasukanBerakhir() {
        Jadwal jadwal = Jadwal.findByLelangNTahap(plId, Tahap.PEMASUKAN_PENAWARAN);
        return jadwal != null && jadwal.dtj_tglakhir != null && jadwal.isEnd(BasicCtr.newDate());
    }

    /**
     * flag lelang boleh evaluasi ulang atau tidak
     * @return
     */
    public boolean isAllowEvaluasiUlang() {
        return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanPengumumanHasil);
    }

    /**
     * flag lelang boleh pemasukan ulang ulang atau tidak
     * @return
     */
    public boolean isAllowPemasukanUlang() {
        return tahapList != null && (tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN) || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS));
    }
    /**
     * allow show hasil evaluasi Akhir
     * @return
     */
    public boolean isShowHasilEvaluasi(){
        if(tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI)){
            EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(plId);
            return (evaluasi != null && evaluasi.eva_status.isSelesai());
        }
        return false;
    }

    /**
     * allow show hasil evaluasi Kualifikasi
     * @return
     */
    public boolean isShowHasilEvaluasiKualifikasi(){
        if(tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI)){
            EvaluasiPl evaluasi = EvaluasiPl.findKualifikasi(plId);
            //return evaluasi != null && evaluasi.eva_status.isSelesai();
            return evaluasi != null;
        }
        return false;

    }

    /**
     * allow show hasil evaluasi Teknis
     * @return
     */
    public boolean isShowHasilEvaluasiTeknis(){
        if(tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI)){
            EvaluasiPl evaluasi = EvaluasiPl.findTeknis(plId);
            //return evaluasi != null && evaluasi.eva_status.isSelesai();
            return evaluasi != null;
        }
        return false;

    }

    /**
     * otorisasi download dokumen lelang
     * @param prakualifikasi
     * @return
     */
    public boolean isAllowDownloadDok(boolean prakualifikasi) {
        if(active_user.isRekanan() && tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanAmbilDok)) {
            if(prakualifikasi) { // jika prakualifikasi , hanya peserta yang lulus prakualifikasi yg bisa download dokumen lelang
                Evaluasi evaluasi_kualifikasi = Evaluasi.findKualifikasi(plId);
                if (evaluasi_kualifikasi != null && evaluasi_kualifikasi.eva_status.isSelesai()) {
                    Peserta peserta = Peserta.findBy(plId, active_user.rekananId);
                    Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findBy(evaluasi_kualifikasi.eva_id, peserta.psr_id);
                    return nilai_evaluasi != null && nilai_evaluasi.isLulus();
                }
                return false;
            } else
                return true;
        } else {
            return active_user.isPanitia() || active_user.isAuditor();
        }
    }
    /*
     * otorisasi download dokumen prakualifikasi
     */
    public boolean isAllowDownloadDokPra() {
        if (active_user.isRekanan()) {
            return tahapList != null && (tahapList.contains(Tahap.AMBIL_DOK_PRA) || tahapList.contains(Tahap.UMUM_PRAKUALIFIKASI));
        } else {
            return active_user.isPanitia() || active_user.isAuditor() || active_user.isPP() || active_user.isPpk();
        }
    }

    /**
     * panitia diijinkan untuk mengirim/mengedit adendum
     * hanya jika paket dibuat versi 3.5, jika versi 4 memakai workflow engine
     * @return
     *
     */
    public boolean isAllowAdendum()
    {
        Date date = BasicCtr.newDate();
        if((active_user.isPP() || active_user.isPanitia() || active_user.isPpk()) && status.isAktif())
        {
            Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(plId, Tahap.PEMASUKAN_PENAWARAN);
            if(jadwal == null)
                jadwal = Jadwal_pl.findByLelangNTahap(plId, Tahap.PENJELASAN);
            if(jadwal != null && jadwal.dtj_tglakhir != null){
                DateTime jadwalAkhir = new DateTime(jadwal.dtj_tglakhir);
                jadwalAkhir = jadwalAkhir.minusDays(3); // tanggal akhir jadwal dikurangi 3 hari (sesuai perka kepala LKPP)
                return date.getTime() <= jadwalAkhir.getMillis() && tahapList.contains(Tahap.UMUM_PRAKUALIFIKASI);
            }
        }
        return false;
    }

    /**
     * otorisasi Adendum
     * @return
     */
    public boolean isAllowAdendumPra()
    {
        Date date = BasicCtr.newDate();
        if(active_user.isPanitia() || active_user.isPP())
        {
            Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(plId, Tahap.PEMASUKAN_DOK_PRA);
            if(jadwal != null && jadwal.dtj_tglakhir != null){
                DateTime jadwalAkhir = new DateTime(jadwal.dtj_tglakhir);
                jadwalAkhir = jadwalAkhir.minusDays(3); // tanggal akhir jadwal dikurangi 3 hari (sesuai perka kepala LKPP)
                return date.getTime() <= jadwalAkhir.getMillis() && tahapList.contains(Tahap.UMUM_PRAKUALIFIKASI);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "TahapStarted @"+plId+" [" + tahapList.toString() + ']';
    }

}
