package models.nonlelang.common;

import controllers.BasicCtr;
import models.common.*;
import models.nonlelang.Jadwal_pl;
import models.nonlelang.Pl_seleksi;
import org.apache.commons.collections4.CollectionUtils;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 2/13/2017.
 */
public class TahapNowPl {

    final Long plId;
    final List<Tahap> tahapList;
    final Active_user active_user;
    StatusPl status;

    public TahapNowPl(Long plId) {
        this.plId = plId;
        this.active_user = Active_user.current();
        Query.find("SELECT lls_status, mtd_pemilihan FROM ekontrak.nonlelang_seleksi WHERE lls_id=?", new ResultSetHandler<Object>() {

            @Override
            public Object handle(ResultSet rs) throws SQLException {
                status = StatusPl.fromValue(rs.getInt("lls_status"));
                MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));

                return null;
            }
        }, plId).first();
        tahapList = AktivitasPl.findTahapNow(plId);
    }

    public TahapNowPl(Pl_seleksi pl) { this(pl.lls_id, Pl_seleksi.findStatusLelang(pl.lls_id)); }

    public TahapNowPl(Long plId, StatusPl status) {
        this.plId = plId;
        this.active_user = Active_user.current();
        this.status = status;
//        ProcessInstancePl pi = WorkflowPlDao.findProcessBylelang(plId);
//        if(pi != null) {
//            tahapList = WorkflowPlDao.findAktifState(pi.process_instance_id);
//        } else {
            tahapList = AktivitasPl.findTahapNow(plId);
//        }
      
//		Logger.info("TahapNow : %s", tahapList);
    }

    public boolean isPemasukanPenawaran() {
        return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN);
    }
    
    public boolean isPembukaanPenawaran() {
        return tahapList != null && tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN);
    }
    
    public boolean isKirimPersyaratanKualifikasi() {
        return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_DOK_PRA);
    }

    public boolean isPemasukanPenawaranHarga() {
        return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN_BIAYA);
    }

    public boolean isPemasukanPenawaranAdmTeknis() {
        return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS);
    }
    
    public boolean isPemasukanDokPra() {
		return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_DOK_PRA);
	}

    public boolean isEvaluasi() {

        return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasi);
    }
    
    public boolean isPenetapanPemenangTeknis() {
        return tahapList != null && (tahapList.contains(Tahap.PENETAPAN_PEMENANG_ADM_TEKNIS)
                || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS));
    }

    public boolean isEvaluasiTeknis()  {
        return tahapList != null && (tahapList.contains(Tahap.EVALUASI_PENAWARAN));
    }

    public boolean isEvaluasiHarga() {
        return tahapList != null && (tahapList.contains(Tahap.EVALUASI_PENAWARAN));
    }

    public boolean isEvaluasiKualifikasi() {
        return tahapList != null && (tahapList.contains(Tahap.EVALUASI_PENAWARAN));
    }
    
    public boolean isEvaluasiKualifikasiNew() {
        return tahapList != null && (tahapList.contains(Tahap.EVALUASI_DOK_PRA));
    }

    public boolean isPembuktian() {
        return tahapList != null && (tahapList.contains(Tahap.EVALUASI_PENAWARAN));
    }
    
    public boolean isPembuktianKualifikasi() {
        return tahapList != null && (tahapList.contains(Tahap.VERIFIKASI_KUALIFIKASI));
    }
    
    public boolean isPenetapanHasilPra() {
        return tahapList != null && tahapList.contains(Tahap.PENETAPAN_HASIL_PRA);
    }

    public boolean isPenetapaPemenang() {
        return tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI);
    }
    
    public boolean isPenetapanPemenangPra() {
        return tahapList != null && tahapList.contains(Tahap.PENETAPAN_PEMENANG_AKHIR);
    }

    public boolean isPengumuman_Pemenang() {
        return tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI);
    }
    
    public boolean isPengumuman_PemenangNew() {
        return tahapList != null && tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR);
    }

    public boolean isPembukaan() {
        return tahapList != null && (tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN));
    }
    
    public boolean isUndanganPra() {
        return tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA) || tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK));
    }

    /**
     * peserta bisa mendaftar lelang sampai batas akhir upload penawaran / upload dok prakualifikasi
     * hanya jika paket dibuat versi 3.5, jika versi 4 memakai workflow engine
     * @param lelangId
     * @return
     *
     */
    public static boolean isAllowPendaftaran(Long lelangId)
    {
        Date now = controllers.BasicCtr.newDate();
        Long count = 0L;
        return AktivitasPl.count("akt_id IN (SELECT akt_id FROM ekontrak.jadwal WHERE lls_id=? AND dtj_tglawal <= ? AND dtj_tglakhir >= ?) AND akt_jenis in ('PEMASUKAN_PENAWARAN','UMUM_PRAKUALIFIKASI')", lelangId,now,now) > 0;        
    }



    /**
     * memeriksa apakah tahap pemasukan sudah berakhir atau belum
     * true : jika sudah berakhir
     * false : jika belum berakhir
     * @return
     */
    public boolean isPemasukanBerakhir() {
        Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(plId, Tahap.PEMASUKAN_PENAWARAN);
        return jadwal != null && jadwal.dtj_tglakhir != null && jadwal.isEnd(BasicCtr.newDate());
    }



    @Override
    public String toString() {
        if(tahapList == null || tahapList.isEmpty())
            return "TahapNow @"+plId;
        return "TahapNow @"+plId+" [" + tahapList.toString() + ']';
    }

}
