package models.nonlelang;

import models.common.KeyLabel;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 2/21/2017.
 */
@Table(name="EKONTRAK.CHECKLIST_EVALUASI")
public class ChecklistEvaluasiPl extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum StatusChecklistEvaluasi {
        TDK_MEMENUHI, MEMENUHI
    }

    //relasi ke Checklist
    @Id
    public Long chk_id;

    //relasi ke Nilai_evaluasi
    @Id
    public Long nev_id;

    public StatusChecklistEvaluasi cke_ada;

    @Transient
    private ChecklistPl checklist;
    @Transient
    private NilaiEvaluasiPl nilai_evaluasi;

    public ChecklistPl getChecklist() {
        if(checklist == null)
            checklist =  ChecklistPl.findById(chk_id);
        return checklist;
    }

    public NilaiEvaluasiPl getNilai_evaluasi() {
        if(nilai_evaluasi == null)
            nilai_evaluasi = NilaiEvaluasiPl.findById(nev_id);
        return nilai_evaluasi;
    }

    @Transient
    public boolean isLulus() {
        return cke_ada == StatusChecklistEvaluasi.MEMENUHI;
    }

    @Transient
    public String getNamaSyarat() {
        if(chk_id == null)
            return "";
        ChecklistPl checklist = ChecklistPl.findById(chk_id);
        Checklist_master cm = Checklist_master.findById(checklist.ckm_id);
        KeyLabel table = cm.getTable();
        if(cm.isIjinUsaha()) {
            if(!CommonUtil.isEmpty(checklist.chk_klasifikasi))
                return checklist.chk_nama+"<br> "+ Messages.get("nonlelang.klasifikasi") +" : "+checklist.chk_klasifikasi;
            else
                return checklist.chk_nama;
        }  else if (cm.isIjinUsahaBaru()) {
			if(!CommonUtil.isEmpty(checklist.chk_klasifikasi)) 
				return checklist.chk_nama+"<br />Bidang Usaha/Sub Bidang Usaha/Klasifikasi/Sub Klasifikasi: "+checklist.chk_klasifikasi;
			else
				return checklist.chk_nama;		
        } else if (cm.isInputNumber()) {
			StringBuilder result = new StringBuilder();		
			int i = 0;
			for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
				if (newline.split("number") == null || newline.split("number").length < 2) {
					result.append(newline);
				} else {
					int j = 0;
					for (String number: newline.split("number")) {
						result.append(number);
						for(Map<String, String> val : checklist.getJsonName()) {
							for(String key : table.key) {
								if (key.equals("number"+i) && j == 0) 
									result.append(val.get(key));
							}
						}
						j++;
					}
				}
				result.append("<br/>");
				i++;
			}
			return result.toString();
		} else if (cm.isInputTextMulti()) {
			StringBuilder result = new StringBuilder();		
			for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
				int j = 1;
				for (String text: newline.split("text")) {
					result.append(text);
					for(Map<String, String> val : checklist.getJsonName()) {
						for(String key : table.key) {
							if (key.equals("text"+j)) 
								result.append(val.get(key));
						}
					}
					j++;
				}
				result.append("<br/>");
			}
			return result.toString();
		} else if(checklist.getJsonName() != null) {
            StringBuilder result = new StringBuilder(cm.ckm_nama).append("<br/>");
            result.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
            result.append("<thead>");
            for (String label : table.label) {
                result.append("<th>").append(label).append("</th>");
            }
            result.append("</thead>");
            result.append("<tbody>");
            for(Map<String, String> val : checklist.getJsonName()) {
                result.append("<tr>");
                for(String key : table.key) {
                    result.append("<td>").append(val.get(key)).append("</td>");
                }
                result.append("</tr>");
            }
            result.append("</tbody>");
            result.append("</table>");
            return result.toString();
        } else if(!cm.isNotEditable() && cm.ckm_jenis == Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI) {
            return cm.ckm_nama +"<br/><i>" +checklist.chk_nama+"</i>";
        } else if(cm.isSyaratLain()) {
            return checklist.chk_nama;
        } else {
            return cm.ckm_nama;
        }
    }

    public static ChecklistEvaluasiPl findBy(Long nilaiId, Long chkId) {
        if(nilaiId == null)
            return null;
        return find("nev_id=? and chk_id=?", nilaiId, chkId).first();
    }

    public static List<ChecklistEvaluasiPl> findBy(Long nilaiId) {
        return find("nev_id=?",nilaiId).fetch();
    }

    public static List<ChecklistEvaluasiPl> addPersyaratan(List<ChecklistPl> syaratList, Long nilaiId){
        List<ChecklistEvaluasiPl> list = new ArrayList<ChecklistEvaluasiPl>();
        if(!CommonUtil.isEmpty(syaratList)) {
            for(ChecklistPl checklist : syaratList) {
                ChecklistEvaluasiPl nilai_check = findBy(nilaiId, checklist.chk_id);
                if(nilai_check == null) {
                    nilai_check = new ChecklistEvaluasiPl();
                    nilai_check.nev_id = nilaiId;
                    nilai_check.chk_id = checklist.chk_id;
                    nilai_check.cke_ada = StatusChecklistEvaluasi.TDK_MEMENUHI;
                }
                list.add(nilai_check);
            }
        }
        return list;
    }

    public static boolean simpanChecklist(List<ChecklistEvaluasiPl> syaratList, List<Long> checklist) {
        List<Long> list_memenuhi = new ArrayList<Long>();
        for(ChecklistEvaluasiPl checklist_evaluasi:syaratList) {
            if (checklist!=null) {
                if(checklist.contains(checklist_evaluasi.chk_id)) {
                    checklist_evaluasi.cke_ada = StatusChecklistEvaluasi.MEMENUHI;
                    list_memenuhi.add(checklist_evaluasi.chk_id);
                } else
                    checklist_evaluasi.cke_ada = StatusChecklistEvaluasi.TDK_MEMENUHI;
            }else{
                checklist_evaluasi.cke_ada = StatusChecklistEvaluasi.TDK_MEMENUHI;
            }
            checklist_evaluasi.save();
        }
        return syaratList.size() == list_memenuhi.size();
    }

}
