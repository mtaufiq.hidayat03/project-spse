package models.nonlelang;

import models.agency.Paket_satker_pl;
import models.agency.SppbjPl;
import models.common.*;
import models.handler.BeritaAcaraPlDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.nonlelang.cast.NilaiEvaluasiPlView;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by rayhanfrds on 2/21/17.
 */
@Table(name = "EKONTRAK.BERITA_ACARA_NONLELANG")
public class Berita_acara_pl extends BaseModel {

    public static final List<Tahap> TAHAP_BERITA_ACARA = Arrays.asList(Tahap.EVALUASI_PENAWARAN, Tahap.KLARIFIKASI_NEGOSIASI);

    static final Template TEMPLATE_BA_NEGO_PL = TemplateLoader.load("/nonlelang/berita_acara/BAPL-Negosiasi-Template.html");
    static final Template TEMPLATE_BA_PL = TemplateLoader.load("/nonlelang/berita_acara/BAPL-template.html");

    @Id(sequence = "ekontrak.seq_berita_acara_nonlelang", function = "nextsequence")
    public Long brc_id;

    @Required
    public Tahap brc_jenis_ba;

    public Long brc_id_attachment;

    public String brt_no;

    public Date brt_tgl_evaluasi;

    public String brt_info;

    public String brt_tempat;

    //relasi ke Pl_seleksi
    public Long lls_id;

    @Transient
    private Pl_seleksi lelang_seleksi;

    public Pl_seleksi getLelang_seleksi() {
        if (lelang_seleksi == null)
            lelang_seleksi = Pl_seleksi.findById(lls_id);
        return lelang_seleksi;
    }

    @Transient
    public List<BlobTable> getDokumen() {
        if (brc_id_attachment == null)
            return null;
        return BlobTableDao.listById(brc_id_attachment);
    }

    @Transient
    public String getJudul() {
        if (brc_jenis_ba == Tahap.KLARIFIKASI_NEGOSIASI)
            return Messages.get("nonlelang.ba_klarifikasi_nego");
        else if (brc_jenis_ba == Tahap.UPLOAD_BA_HASIL_LELANG)
            return Messages.get("nonlelang.ba_hasil_pemilihan");
        else if (brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_PENAWARAN)
            return Messages.get("nonlelang.ba_hasil_evaluasi");
        return "";
    }

    public static Berita_acara_pl findBy(Long lelangId, Tahap jenis) {
        return find("lls_id=? and brc_jenis_ba=?", lelangId, jenis).first();
    }

    public static String getDownloadInfo(Kategori kategori) {
        String info = "Berita Acara Hasil Pemilihan dapat di download pada saat ";
        if (kategori.isKonsultansi() || kategori.isJkKonstruksi() || kategori.isKonsultansiPerorangan())
            info += "setelah dilakukan Pengumuman Pemenang";
        else
            info += Messages.get("nonlelang.slth_pengumuman");
        return info;
    }

    public static UploadInfo simpan(Long lelangId, Tahap jenis, File file) throws Exception {
        Berita_acara_pl bA = find("lls_id=? and brc_jenis_ba=?", lelangId, jenis).first();
        if (bA == null) {
            bA = new Berita_acara_pl();
            bA.lls_id = lelangId;
            bA.brc_jenis_ba = jenis;
        }

        BlobTable blob = null;
        if (file != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, bA.brc_id_attachment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }
        bA.brc_id_attachment = blob.blb_id_content;
        bA.save();

        // untuk keperluan response page yang memakai ajax
        return UploadInfo.findBy(blob);
    }


    /**
     * download URL untuk setiap dokumen berita acara
     * @param blob
     * @return
     */
    @Transient
    public String getDownloadUrl(BlobTable blob) {
        return blob.getDownloadUrl(BeritaAcaraPlDownloadHandler.class);
    }


    @Transient
    public String getNameFromBlobTable(BlobTable d){
        String name = "no name";
        try {
            String[] ls = BaseModel.decrypt(d.blb_path).split("\\n");
            name = ls[2];
        } catch (Exception e) {}
        return name;
    }

    @Transient
    public boolean isBAPenawaran() {
        return brc_jenis_ba == Tahap.UPLOAD_BA_EVALUASI_PENAWARAN;
    }

    @Transient
    public boolean isBAHasil() {
        return brc_jenis_ba == Tahap.UPLOAD_BA_HASIL_LELANG;
    }

    @Transient
    public boolean isBAHasilNegosiasi() {
        return brc_jenis_ba == Tahap.KLARIFIKASI_NEGOSIASI;
    }

    public String getHari(){

        return new SimpleDateFormat("EEEE", new java.util.Locale("id")).format(brt_tgl_evaluasi);
    }

    public static InputStream cetak(Tahap jenis, Long id, Berita_acara_pl berita_acara) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        Template template = null;
        Map<String, Object> params = new HashMap<String, Object>();
        if (jenis == Tahap.KLARIFIKASI_NEGOSIASI) {
            template = TEMPLATE_BA_NEGO_PL;
            params.put("paket", pl.getPaket());
            params.put("pl", pl);
            params.put("berita_acara", berita_acara);
            params.put("instansi", Instansi.findNamaInstansiPaket(pl.pkt_id));
            EvaluasiPl akhir = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
            params.put("akhir", akhir);
            List<NilaiEvaluasiPlView> pesertaHargaSorted = new ArrayList<NilaiEvaluasiPlView>();
            if (akhir != null) {
                pesertaHargaSorted = akhir.findAllNilaiEvaluasiWithUrutHarga();
            }
            params.put("pesertaHargaSorted", pesertaHargaSorted);

        } else {
            template = TEMPLATE_BA_PL;
            params.put("pl", pl);
            params.put("paket", pl.getPaket());
            params.put("berita_acara", berita_acara);
            params.put("instansi", Paket_satker_pl.getNamaInstansiPaket(pl.pkt_id));
            EvaluasiPl akhir = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
            params.put("akhir", akhir);
            params.put("sppbj", SppbjPl.findByPl(pl.lls_id));

            if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
                List<PesertaPl> pesertaList = PesertaPl.findByPl(pl.lls_id);
                params.put("pesertaList", pesertaList);
            }
            if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
                List<NilaiEvaluasiPlView> pesertaAdm = new ArrayList<>();
                EvaluasiPl administrasi = EvaluasiPl.findAdministrasi(pl.lls_id);
                if(administrasi != null)
                    pesertaAdm = administrasi.findAllNilaiEvaluasi();
                params.put("pesertaAdm", pesertaAdm);

                List<NilaiEvaluasiPlView> pesertaTeknis = new ArrayList<>();
                EvaluasiPl teknis = EvaluasiPl.findTeknis(pl.lls_id);
                if(teknis != null)
                    pesertaTeknis = teknis.findAllNilaiEvaluasi();
                params.put("pesertaTeknis", pesertaTeknis);
            }
            if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
                List<NilaiEvaluasiPlView> pesertaHarga = new ArrayList<>();
                EvaluasiPl harga = EvaluasiPl.findHarga(pl.lls_id);
                if(harga != null){
                    pesertaHarga = harga.findAllNilaiEvaluasi();
                }
                params.put("pesertaHarga", pesertaHarga);

                if( berita_acara.isBAHasil()){
                    params.put("hasil_lelang", true);
                }
                else
                    params.put("hasil_lelang", false);
            }

            String panitia = Active_user.current().isPP() ? Messages.get("nonlelang.pejabat_pengadaan") : Messages.get("nonlelang.pokja_pemilihan");
            params.put("panitia", panitia);

            if (berita_acara.isBAHasil()) {
                List<NilaiEvaluasiPlView> pesertaKualifikasi = new ArrayList<>();
                EvaluasiPl kualifikasi = EvaluasiPl.findKualifikasi(pl.lls_id);
                if(kualifikasi != null)
                    pesertaKualifikasi = kualifikasi.findAllNilaiEvaluasi();
                params.put("pesertaKualifikasi", pesertaKualifikasi);

            }
        }
        return HtmlUtil.generatePDF(template, params);
    }
    
    /**
	 * Dapatkan dokumen berita acara, jika tidak ada maka akan dibuat baru,
	 * Gunakan saat proses submit terkait dokumen berita acara
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static Berita_acara_pl findNCreateBy(Long lelangId, Tahap jenis) {
		Berita_acara_pl berita_acara = find("lls_id=? and brc_jenis_ba=?", lelangId, jenis).first();
		if(berita_acara == null) {
			berita_acara = new Berita_acara_pl();
			berita_acara.lls_id = lelangId;
			berita_acara.brc_jenis_ba = jenis;
			berita_acara.save();
		}
		return berita_acara;
	}
	
	public UploadInfo simpan(File dokumen) {
		BlobTable blob = null;
		try {
			List<BlobTable> blobList = getDokumen();
			if (blobList != null)
				blobList.forEach(b -> b.delete());
			if (brc_id_attachment != null && getDokumen().isEmpty()) {
				brc_id_attachment = null;
			}
			if(dokumen != null)
				blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, dokumen, brc_id_attachment);
			if(blob != null)
				brc_id_attachment = blob.blb_id_content;
			save();
		} catch (Exception e){
			Logger.error(e, "Simpan Dokumen Berita Acara gagal : %s", e.getMessage());
		}
		return UploadInfo.findBy(blob);
	}
}
