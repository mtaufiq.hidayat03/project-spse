package models.nonlelang.form;

import models.agency.Rincian_hps;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 2/27/2018
 */
public class PenawaranDataForm {

    public String data = "[]";
    public boolean fixed;

    /**
     * Check whether data is empty value or []
     * @return boolean*/
    public boolean isDataEmpty() {
        return CommonUtil.isEmpty(data) || (!CommonUtil.isEmpty(data) && data.equals("[]"));
    }

    /**
     * Convert json string Rincian_hps into Rincian_hps[]
     * @return Rincian_hps*/
    public Rincian_hps[] getItems() {
        return Rincian_hps.fromJson(this.data);
    }

    /**
     * Parse string total into double
     * @return Double*/
    public Double getTotalDouble() {
        return Rincian_hps.total(getItems());
    }

}
