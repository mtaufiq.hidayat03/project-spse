package models.nonlelang;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by Lambang on 2/8/2017.
 */
@Table(name="EKONTRAK.HISTORY_JADWAL_PL")
public class HistoryJadwalPl extends BaseModel {

    @Id
    public Long dtj_id;

    @Id
    public Date hjd_tanggal_edit;

    public Date hjd_tanggal_awal;

    public Date hjd_tanggal_akhir;

    public String hjd_keterangan;

    public String getNamaPegawai() {
        return Query.find("select peg_nama from pegawai where peg_namauser = ?", String.class, audituser).first();
    }

    @Transient
    private Jadwal_pl jadwal;

    public Jadwal_pl getJadwal() {
        if(jadwal == null)
            jadwal = Jadwal_pl.findById(dtj_id);
        return jadwal;
    }



}
