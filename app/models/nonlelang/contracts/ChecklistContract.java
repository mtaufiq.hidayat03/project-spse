package models.nonlelang.contracts;

import models.nonlelang.DokPenawaranPl;

import java.util.List;

/**
 * @author HanusaCloud on 3/9/2018
 */
public interface ChecklistContract {

    Long getChecklistId();

    void setDokumenPenawarans(List<DokPenawaranPl> items);

    default void withDokumenPenawaran() {
        if (getChecklistId() != null) {
            setDokumenPenawarans(DokPenawaranPl.findListPenawaranByChecklist(getChecklistId()));
        }
    }

}
