package models.nonlelang.contracts;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;

/**
 * @author HanusaCloud on 3/9/2018
 */
public interface PenawaranContract {

    Long getDocIdAttachment();

    BlobTable getBlobTable();

    void setBlob(BlobTable model);

    default void withDocumentBlob() {
        if (getDocIdAttachment() != null) {
            setBlob(BlobTableDao.getLastById(getDocIdAttachment()));
        }
    }

    default void deleteBlobDocument() {
        if (getBlobTable() == null) {
            withDocumentBlob();
        }
        if (getBlobTable() != null) {
            getBlobTable().delete();
        }
    }

}
