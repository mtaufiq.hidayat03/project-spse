package models.nonlelang.contracts;

import models.agency.DaftarKuantitas;
import models.nonlelang.Dok_pl;
import models.rekanan.Rekanan;

/**
 * @author HanusaCloud on 2/28/2018
 */
public interface PesertaNonLelangContract {

    Long getPsrId();

    Long getLelangId();

    Long getRekananId();

    Rekanan getRekanan();

    Dok_pl getDokumenLelang();

    void setRekanan(Rekanan model);

    void setDokumenLelang(Dok_pl dok_pl);

    DaftarKuantitas getKuantitas();

    default void withDokumenLelang() {
        if (getLelangId() != null) {
            setDokumenLelang(Dok_pl.findBy(getLelangId(), Dok_pl.JenisDokPl.DOKUMEN_LELANG));
        }
    }
}
