package models.nonlelang.contracts;

import models.agency.DaftarKuantitas;
import models.agency.Paket_pl;
import models.agency.Rincian_hps;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.nonlelang.Dok_pl;
import models.nonlelang.Dok_pl_content;
import models.nonlelang.PesertaPl;
import models.nonlelang.common.TahapNowPl;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author HanusaCloud on 2/27/2018
 * this is a contract to be implemented by paket non tender,
 * this contract includes getter, setter, and also serve as relational function prefix-with(relation name),
 * simplify the need to call the same code from different place
 * example: model.withPackage() rather than Paket_pl.findBy(id),
 * every relation has an object hopefully by implementing this contract developer can learn relationship between classes.
 */
public interface PaketNonLelangSeleksiContract {

    /**
     * it's supposed to be the primary key*/
    Long getId();

    Long getPackageId();

    Dok_pl getDokumenLelang();

    PesertaPl getParticipant();

    void setPackage(Paket_pl model);

    void setDocument(Dok_pl model);

    void setStep(TahapNowPl model);

    void setParticipant(PesertaPl model);

    void setParticipants(List<PesertaPl> items);

    /**
     * retrieve and store paket relationship*/
    default void withPackage() {
        if (getPackageId() != null) {
            setPackage(Paket_pl.findById(getPackageId()));
        }
    }

    /**
     * retrieve and store dokumen paket relationship*/
    default void withDokumenLelang() {
        if (getId() != null) {
            setDocument(Dok_pl.findNCreateBy(getId(), Dok_pl.JenisDokPl.DOKUMEN_LELANG));
        }
    }

    /**
     * retrieve and store tahap relationship*/
    default void withStep() {
        if (getId() != null) {
            setStep(new TahapNowPl(getId()));
        }
    }

    /**
     * retrieve and store peserta relationship*/
    default void withParticipant(Active_user active_user) {
        if (getId() != null && active_user != null) {
            setParticipant(PesertaPl.find("lls_id=? and rkn_id=?", getId(), active_user.rekananId).first());
        }
    }

    /**
     * retrieve and store list of peserta relationship*/
    default void withParticipants() {
        if (getId() != null) {
            setParticipants(PesertaPl.findByPl(getId()));
        }
    }

    default boolean comparingRincian(Rincian_hps[] items) {
        if (getDokumenLelang() == null) {
            withDokumenLelang();
            if (getDokumenLelang().getDokumenLelangContent() == null) {
                getDokumenLelang().withDokumenLelangContent();
            }
        }
        DaftarKuantitas daftarKuantitas = Dok_pl_content.findBy(getDokumenLelang().dll_id).dkh;
        if (daftarKuantitas != null && !CommonUtil.isEmpty(items)) {
            List<Rincian_hps> rincian_hps = daftarKuantitas.items;
            if (items.length != rincian_hps.size()) {
                return true;
            }
            Iterator<Rincian_hps> iterator1 = rincian_hps.iterator();
            Iterator<Rincian_hps> iterator2 = Arrays.asList(items).iterator();
            while (iterator1.hasNext() && iterator2.hasNext()) {
                Rincian_hps model = iterator1.next();
                Rincian_hps hps = iterator2.next();
                if (!model.harga.equals(hps.harga)
                        || !model.vol.equals(hps.vol)
                        || !model.vol2.equals(hps.vol2)
                        || !model.pajak.equals(hps.pajak)) {
                    return true;
                }
            }
        }
        return false;
    }

}
