package models.nonlelang.contracts;

import models.lelang.Checklist_master;
import models.nonlelang.ChecklistPl;
import models.nonlelang.Dok_pl_content;

import java.util.List;

/**
 * @author HanusaCloud on 2/28/2018
 */
public interface DokumenNonLelangContract {

    /**
     * it's supposed to be dll_id*/
    Long getId();

    Dok_pl_content getDokumenLelangContent();

    List<ChecklistPl> getChecklistPls();

    void setDokumenContent(Dok_pl_content model);

    void setChecklistPls(List<ChecklistPl> items);

    default void withDokumenLelangContent() {
        if (getId() != null) {
            setDokumenContent(Dok_pl_content.findBy(getId()));
        }
    }

    default void withChecklistPl() {
        if (getId() != null) {
            setChecklistPls(ChecklistPl.getListChecklist(
                    getId(),
                    Checklist_master.JenisChecklist.CHECKLIST_TEKNIS));
        }
    }

    default void withPriceChecklistPl(){
        if (getId() != null) {
            setChecklistPls(ChecklistPl.getListChecklist(
                    getId(),
                    Checklist_master.JenisChecklist.CHECKLIST_HARGA));
        }
    }

}
