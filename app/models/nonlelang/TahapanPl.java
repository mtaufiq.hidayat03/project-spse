package models.nonlelang;

import models.common.Tahap;
import play.db.jdbc.Query;

import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class TahapanPl {
	
	public Long lls_id;

	public Integer akt_urut;

	public Long dtj_id;

	public Date dtj_tglawal;

	public Date dtj_tglakhir;

	public Tahap akt_jenis;

	public Integer jml_history;
	
	public boolean isNow(Date curDate) {
		if (dtj_tglawal != null && dtj_tglakhir != null)
			return (curDate.after(dtj_tglawal) && curDate.before(dtj_tglakhir));
		return false;
	}
	
	public String namaTahap() {
		if(Tahap.tahapanBA.contains(akt_jenis)) { //label tahap berita Acara Harus disesuaikan
			return akt_jenis.getLabel().replace("Upload", "Pembuatan");
		}
		return akt_jenis.getLabel();
	}
	
	public static final String SQL = "SELECT lls_id, akt_urut, dtj_id, dtj_tglawal, dtj_tglakhir , akt_jenis, "
			+ "(SELECT count(dtj_id) FROM ekontrak.history_jadwal_pl where dtj_id=j.dtj_id) jml_history "
			+ "FROM ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id=? order by akt_urut asc";
	
	public static List<TahapanPl> findByLelang(Long plId) {
		return Query.find(SQL, TahapanPl.class, plId).fetch();
	}

}
