package models.nonlelang;

import controllers.BasicCtr;
import models.agency.DaftarKuantitas;
import models.agency.Paket_pl;
import models.agency.Rincian_hps;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.contracts.PesertaNonLelangContract;
import models.nonlelang.form.PenawaranDataForm;
import models.rekanan.Rekanan;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 2/13/2017.
 */

@Table(name="EKONTRAK.PESERTA_NONLELANG")
public class PesertaPl extends BaseModel implements PesertaNonLelangContract {

    @Id(sequence="ekontrak.seq_peserta_nonlelang", function="nextsequence")
    public Long psr_id;

    public Date psr_tanggal;

    public Long psr_black_list;

    public Integer is_pemenang;

    //kolom pemenang terferifikasi
    public Integer is_pemenang_verif;

    public Integer sudah_verifikasi_sikap;

    //relasi ke Pl_seleksi
    public Long lls_id;

    //relasi ke Rekanan
    public Long rkn_id;

    public Double psr_harga;

    public Double psr_harga_terkoreksi;

    public String psr_uraian;

    public String psr_alasan_menang;

    /**
     * daftar kuantitas dan harga
     * format : json
     * json model models.agency.Rincian_hps
     */
    public String psr_dkh;

    public String psr_identitas; // data identitas , model : rekanan.Rekanan

    public String psr_pemilik; // data pemilik , model : rekanan.Pemilik

    public String psr_pengurus; // data pengurus , model : rekanan.Pengurus

    public Integer masa_berlaku_penawaran;

    public Integer lama_pekerjaan;

    public Date tgl_surat_penawaran;

    public Date tgl_penawaran_harga;

    public boolean is_dikirim_pesan = false;

    /*RELATIONSHIP OBJECTS*/
    /**
     * daftar kuantitas dan harga
     */
    @Transient
    public DaftarKuantitas dkh;

    @Transient
    private Pl_seleksi pl_seleksi;

    @Transient
    private Rekanan rekanan;

    @Transient
    public Dok_pl dokumenLelang;

    /*END OF RELATIONSHIP OBJECTS*/
    
    @Transient
    public List<DokPenawaranPl> findPenawaranList() {
        return DokPenawaranPl.findPenawaranPeserta(psr_id);
    }

    @Transient
    public DokPenawaranPl getDokKualifikasi() {
        return DokPenawaranPl.findPenawaranPeserta(psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
    }

    @Transient
    public DokPenawaranPl getDokTambahan() {
        return DokPenawaranPl.findPenawaranPeserta(psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TAMBAHAN);
    }

    @Transient
    public DokPenawaranPl getDokSusulan() {
        return DokPenawaranPl.findPenawaranPeserta(psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_SUSULAN);
    }

    @Transient
    public DokPenawaranPl getFileTeknis() {
        return DokPenawaranPl.findPenawaranPeserta(psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
    }

    @Transient
    public DokPenawaranPl getFileHarga() {
        return DokPenawaranPl.findPenawaranPeserta(psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA);
    }

    @Transient
    public DokPenawaranPl getFileAdminTeknisHarga() { // dipakai oleh lelang versi 3
        return DokPenawaranPl.findPenawaranPeserta(psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA);
    }

    @Transient
    public boolean isPemenang()
    {
        return NilaiEvaluasiPl.isLulus(EvaluasiPl.JenisEvaluasi.EVALUASI_AKHIR, this);
    }


    public Pl_seleksi getPl_seleksi() {
        if(pl_seleksi == null)
            pl_seleksi = Pl_seleksi.findById(lls_id);
        return pl_seleksi;
    }

    @Override
    public Rekanan getRekanan() {
        if(rekanan == null)
            rekanan = Rekanan.findById(rkn_id);
        return rekanan;
    }

    @Override
    public Dok_pl getDokumenLelang() {
        return this.dokumenLelang;
    }


    protected void postLoad() {
        super.postLoad();
        if(!CommonUtil.isEmpty(psr_dkh))
            dkh = CommonUtil.fromJson(psr_dkh, DaftarKuantitas.class);
    }

    /**
     * NilaiEvaluasiPl Harga
     * @return
     */
    @Transient
    public NilaiEvaluasiPl getHarga(){
        EvaluasiPl harga = EvaluasiPl.findHarga(lls_id);
        return NilaiEvaluasiPl.findBy(harga.eva_id, psr_id);
    }

    public void prePersist() {
        super.prePersist();
        //if (dkh != null)
         //   psr_dkh = CommonUtil.toJson(dkh);
    }

    /**
     * pemeriksaan pemenang non tender
     * @return
     */
    @Transient
    public boolean isPemenangVerif()
    {
        if(is_pemenang_verif==null)
            return false;
        return is_pemenang_verif==Integer.valueOf(1);
    }

    /**
     * pemeriksaan sudah verifikasi ke SIKaP
     * @return
     */
    @Transient
    public boolean isSudahVerifikasiSikap() {
        if (sudah_verifikasi_sikap == null)
            return false;
        return sudah_verifikasi_sikap == Integer.valueOf(1);
    }

    /**
     * informasi nama peserta lelang
     * @return
     */
    public String getNamaPeserta() {
        return Query.find("select rkn_nama from rekanan where rkn_id=?", String.class, rkn_id).first();
    }

    public static boolean isSudahDaftar(Long lelangId, Long rekananId) {
         return count("lls_id=? AND rkn_id=?", lelangId, rekananId) > 0;
    }


    public static void daftarPl(Long lelangId, Long rekananId)	{
        if(isSudahDaftar(lelangId, rekananId)) // check jika sudah daftar
            return;
        PesertaPl peserta = new PesertaPl();
        peserta.lls_id = lelangId;
        peserta.rkn_id = rekananId;
        peserta.psr_tanggal = BasicCtr.newDate();
        peserta.psr_black_list = Long.valueOf(0);
        peserta.is_pemenang = Integer.valueOf(0);
        peserta.is_pemenang_verif = Integer.valueOf(0);
        peserta.save();

       /* DraftPesertaPl draftPesertaPl = DraftPesertaPl.findWithLelangAndRekanan(lelangId, rekananId);

        if (draftPesertaPl != null)
            draftPesertaPl.delete();*/
    }

    public static PesertaPl findBy(Long lelangId, Long rekananId) {
        return find("lls_id=? and rkn_id=?", lelangId, rekananId).first();
    }

    public static PesertaPl findBy(Long lelangId, Long rekananId, Long pesertaId) {
        return find("lls_id=? and rkn_id=? and psr_id=?", lelangId, rekananId, pesertaId).first();
    }

    public static void simpanRincianPenawaran(PesertaPl pesertaPl, String data, String total, boolean fixed) throws Exception {

        Pl_seleksi pl = Pl_seleksi.findById(pesertaPl.lls_id);

        DaftarKuantitas dk = new DaftarKuantitas();

        if (total == null)
            total = "0.0";

        Double nilai_total = Double.parseDouble(total);

        if (!CommonUtil.isEmpty(data)) {
            Rincian_hps[] items = Rincian_hps.fromJson(data);
            if(!CommonUtil.isEmpty(items)) {
                dk.items = Arrays.asList(items);
            }
        }

        dk.fixed = fixed;

        if (nilai_total > 0)
            dk.total = nilai_total;


        Paket_pl paket = Paket_pl.findById(pl.pkt_id);

        if (dk.total == null)
            throw new Exception("Anda Belum mengisikan rincian penawaran");
       /* else if (dk.total > paket.pkt_pagu) {
            throw new Exception("Nilai Total Penawaran melebihi pagu");
        }*/ else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket

            pesertaPl.psr_dkh = CommonUtil.toJson(dk);

            pesertaPl.psr_harga = dk.total;

            pesertaPl.psr_harga_terkoreksi = dk.total;

            pesertaPl.tgl_penawaran_harga = controllers.BasicCtr.newDate();

            pesertaPl.save();

        }
    }

    /**
     * Nilai evaluasi Penetapan Pemenang
     * @return
     */
    @Transient
    public NilaiEvaluasiPl getPenetapan() {
        EvaluasiPl penetapan = EvaluasiPl.findPenetapanPemenang(lls_id);
        if(penetapan == null)
            return null;
        return NilaiEvaluasiPl.findBy(penetapan.eva_id, psr_id);
    }


    public static PesertaPl findBy(Long pesertaId) {
		return find("psr_id = ?", pesertaId).first();
	}
    
    public static List<PesertaPl> findByPl(Long lelangId) {
        return find("lls_id=? order by psr_harga", lelangId).fetch();
    }

    public static PesertaPl findByRekananAndPl(Long rekananId, Long lelangId){
        return find("rkn_id=? and lls_id=?", rekananId, lelangId).first();
    }

    public boolean sudahTerimaUndanganKontrak() {
        return MailQueueDao.checkEmail(lls_id, rkn_id, JenisEmail.UNDANGAN_KONTRAK);
    }

    public void clearPenawaran() {
        this.psr_dkh = null;
        this.psr_harga = 0D;
        this.psr_harga_terkoreksi = 0D;
        this.masa_berlaku_penawaran = null;
        this.tgl_penawaran_harga = null;
        this.tgl_surat_penawaran = null;
    }

    @Override
    public Long getPsrId() {
        return this.psr_id;
    }

    @Override
    public Long getLelangId() {
        return this.lls_id;
    }

    @Override
    public Long getRekananId() {
        return this.rkn_id;
    }

    @Override
    public void setRekanan(Rekanan model) {
        this.rekanan = model;
    }

    @Override
    public void setDokumenLelang(Dok_pl dok_pl) {
        this.dokumenLelang = dok_pl;
    }

    @Override
    public DaftarKuantitas getKuantitas() {
        return this.dkh;
    }

    public boolean hasSubmittedOfferingLetter() {
        return masa_berlaku_penawaran != null && tgl_surat_penawaran != null;
    }

    /**
     * Store participant offering
     * @param model
     * @exception Exception*/
    public void simpanRincianPenawaran(PenawaranDataForm model) throws Exception {
        DaftarKuantitas dk = new DaftarKuantitas();
        dk.fixed = model.fixed;
        Rincian_hps[] items = model.getItems();
        dk.setRincianHps(items);
        dk.total = Rincian_hps.total(items);
        psr_dkh = CommonUtil.toJson(dk);
        psr_harga = dk.total;
        psr_harga_terkoreksi = dk.total;
        tgl_penawaran_harga = controllers.BasicCtr.newDate();
        save();
    }

    public static Double getTotalHargaFinal(List<PesertaPl> pemenang, Pl_seleksi pl){
        Double total = 0d;
        if(!pl.isItemized() && CollectionUtils.isNotEmpty(pemenang)){
            total = pemenang.get(0).getPenetapan().hargaFinal();
//            for(PesertaPl peserta : pemenang) {
//                NilaiEvaluasiPl nilai = peserta.getPenetapan();
//                if(nilai != null && nilai.isLulus())
//                    total = nilai.hargaFinal();
//            }
        }
        return total;
    }

    @Transient
	private NilaiEvaluasiPl nilaiPembuktian;
    
    public NilaiEvaluasiPl getNilaiPembuktian(){
		if(nilaiPembuktian == null){
			EvaluasiPl pembuktian = getPl_seleksi().getPembuktian(false);
			if(pembuktian != null){
				nilaiPembuktian = NilaiEvaluasiPl.findBy(pembuktian.eva_id, psr_id);
			}
		}
		return nilaiPembuktian; 
	}

    public Double getJumlahPenawaran() {
        if(dkh==null)
            return 0.0;
        if(dkh.total==null)
            return 0.0;
        return dkh.total;
    }
}
