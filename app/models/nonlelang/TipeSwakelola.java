package models.nonlelang;

public enum TipeSwakelola {

    TIPE1(1, "K/L/PD PENANGGUNG JAWAB ANGGARAN"),
    TIPE2(2, "K/L/PD PELAKSANA SWAKELOLA"),
    TIPE3(3, "ORGANISASI MASYARAKAT"),
    TIPE4(4,"KELOMPOK MASYARAKAT");

    public final Integer id;
    public final String label;

    TipeSwakelola(int key, String label){
        this.id = Integer.valueOf(key);
        this.label = label;
    }

    public static TipeSwakelola findById(Integer key){
        if(key == null)
            return TIPE1;
        switch (key){
            case 1:
                return TIPE1;
            case 2:
                return TIPE2;
            case 3:
                return TIPE3;
            case 4:
                return TIPE4;
            default:
                return TIPE1;
        }
    }

    public static final TipeSwakelola[] all = new TipeSwakelola[]{
            TIPE1,
            TIPE2,
            TIPE3,
            TIPE4
    };

//    public boolean isSwakelola(){
//        return this == SWAKELOLA;
//    }

}
