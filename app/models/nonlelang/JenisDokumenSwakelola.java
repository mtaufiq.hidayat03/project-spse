package models.nonlelang;

import ext.DateBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 4/26/2017.
 */
@Table(name = "EKONTRAK.JENIS_DOKUMEN_SWAKELOLA")
public class JenisDokumenSwakelola extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum JenisDokSwakelolaEnum {

        SPK(1, "SPK"),
        KWITANSI(2, "Kwitansi"),
        SURAT_PENUNJUKAN_PEMENANG(3, "Surat penunjukan Pemenang"),
        SPMK(4, "SPMK (Surat Perintah Mulai Kerja)"),
        BERITA_ACARA_HASIL_PENUNJUKAN_LANGSUNG(5, "Berita Acara Hasil Penunjukan Langsung"),
        BERITA_ACARA_EVALUASI(6, "Berita Acara Evaluasi"),
        BERITA_ACARA_PENETAPAN(7, "Berita Acara Penetapan"),
        BERITA_ACARA_KLARIFIKASI_NEGOSIASI(8, "Berita Acara Klarifikasi Negosiasi"),
        DOKUMEN_LAINNYA(9, "Dokumen Lainnya");

        public final Integer value;
        public final String label;

        private JenisDokSwakelolaEnum(Integer value, String label) {

            this.value = value;
            this.label = label;

        }

        public static JenisDokSwakelolaEnum fromValue(Integer value){
            JenisDokSwakelolaEnum jenis = null;
            if(value!=null){
                switch (value.intValue()) {
                    case 1:	jenis = SPK;break;
                    case 2:	jenis = KWITANSI;break;
                    case 3:	jenis = SURAT_PENUNJUKAN_PEMENANG;break;
                    case 4:	jenis = SPMK;break;
                    case 5:	jenis = BERITA_ACARA_HASIL_PENUNJUKAN_LANGSUNG;break;
                    case 6:	jenis = BERITA_ACARA_EVALUASI;break;
                    case 7:	jenis = BERITA_ACARA_PENETAPAN;break;
                    case 8:	jenis = BERITA_ACARA_KLARIFIKASI_NEGOSIASI;break;
                    case 9:	jenis = DOKUMEN_LAINNYA;break;
                }
            }
            return jenis;
        }

    }


    @Id(sequence="ekontrak.seq_jenis_dokumen_swakelola", function="nextsequence")
    public Long jds_id;

    @Required
    public Long lls_id;

    @Required
    public String jds_nama_dokumen;

    @Required
    public String jds_nomor_dokumen;

    @As(binder= DateBinder.class)
    public Date jds_tanggal_dokumen;

    public String jds_keterangan;

    public Long jds_id_attachment;

    public Integer jds_jenis;

    @Transient
    public BlobTable getBlob() {
        if(jds_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(jds_id_attachment);
    }

    @Transient
    public JenisDokSwakelolaEnum getJenis() {
        return JenisDokSwakelolaEnum.fromValue(jds_jenis);
    }

    public static void simpanJenisDokumen(JenisDokumenSwakelola jenisDokumenSwakelola, File dlpAttachment, String hapus) throws Exception {

        if (dlpAttachment != null) {

            BlobTable bt = null;

            if (jenisDokumenSwakelola.jds_id == null)
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment);
            else
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment, jenisDokumenSwakelola.jds_id_attachment);

            jenisDokumenSwakelola.jds_id_attachment = bt.blb_id_content;

        } else {

            if (jenisDokumenSwakelola.jds_id_attachment != null && hapus == null) {

                Long id = jenisDokumenSwakelola.jds_id_attachment;

                jenisDokumenSwakelola.jds_id_attachment = null;

                BlobTable.delete("blb_id_content=?", id);

            }

            if (hapus != null)
                jenisDokumenSwakelola.jds_id_attachment = Long.valueOf(hapus);

        }

        jenisDokumenSwakelola.save();

    }

    public static List<JenisDokumenSwakelola> findBySwakelola(Long swakelolaId){

        return find("lls_id =?", swakelolaId).fetch();

    }

}
