package models.nonlelang.nonSpk;

import ext.DateBinder;
import models.agency.DaftarKuantitas;
import models.agency.Paket_pl;
import models.agency.Pegawai;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by Lambang on 5/3/2017.
 */
@Table(name = "EKONTRAK.DOK_NON_SPK")
public class Dok_non_spk extends BaseModel {

    @Id(sequence="ekontrak.seq_dok_non_spk", function="nextsequence")
    public Long dns_id;

    public Long lls_id;

    @As(binder = DateBinder.class)
    public Date dns_tanggal_bast;

    public Long dns_bast_attachment;

    public String dns_dkh;

    @Transient
    public DaftarKuantitas dkh;

    @Transient
    private Non_spk_seleksi non_spk_seleksi;

    public Non_spk_seleksi getNon_spk_seleksi() {
        if(non_spk_seleksi == null)
            non_spk_seleksi = non_spk_seleksi.findById(lls_id);
        return non_spk_seleksi;
    }

    public String getAuditUser() {
        return Pegawai.findBy(audituser).peg_nama;
    }

    public static Dok_non_spk findNCreateBy(Long nonSpkId) {
        Dok_non_spk dok_non_spk = find("lls_id=?", nonSpkId).first();
        if(dok_non_spk == null) {
            dok_non_spk = new Dok_non_spk();
            dok_non_spk.lls_id = nonSpkId;
            dok_non_spk.save();
        }
        return dok_non_spk;
    }

    @Transient
    public DaftarKuantitas getRincianHPS() {
        String content = Query.find("SELECT dns_dkh FROM ekontrak.dok_non_spk WHERE dns_id=? AND dns_dkh is not null ", String.class, dns_id).first();
        if (!CommonUtil.isEmpty(content))
            return CommonUtil.fromJson(content, DaftarKuantitas.class);
        return null;
    }

    public void simpanHps(Non_spk_seleksi nonSpkSeleksi, String data, boolean fixed) throws Exception {
        Dok_non_spk dok_non_spk = Dok_non_spk.findNCreateBy(nonSpkSeleksi.lls_id);
        DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,fixed);
        Paket_pl paket = Paket_pl.findById(nonSpkSeleksi.pkt_id);
        if (dk.total == null)
            throw new Exception(Messages.get("utility.abmrhps"));
        else if (dk.total > paket.pkt_pagu) {
            throw new Exception(Messages.get("utility.nthpsp"));
        } else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket
            paket.pkt_hps = dk.total;
            paket.save();
            dok_non_spk.dkh = dk;
            dok_non_spk.save();
        }
    }

    public static Dok_non_spk findBy(Long nonSpkId) {
        return find("lls_id=?", nonSpkId).first();
    }

    public static boolean isEditable(Non_spk_seleksi non_spk_seleksi) {
        if (non_spk_seleksi.lls_status.isDraft())
            return true;
        else
            return false;

    }

    @Transient
    public BlobTable getBastBlob() {
        if(dns_bast_attachment == null)
            return null;
        return BlobTableDao.getLastById(dns_bast_attachment);
    }

    protected void postLoad() {
        if (!CommonUtil.isEmpty(dns_dkh))
            dkh = CommonUtil.fromJson(dns_dkh, DaftarKuantitas.class);

    }

    public void prePersist() {
        super.prePersist();
        if (dkh != null)
            dns_dkh = CommonUtil.toJson(dkh);
    }

}
