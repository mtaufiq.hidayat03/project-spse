package models.nonlelang.nonSpk;

import ext.DateBinder;
import ext.RupiahBinder;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 5/3/2017.
 */
@Table(name = "EKONTRAK.JENIS_REALISASI_NON_SPK")
public class JenisRealisasiNonSpk extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum JenisRealisasiNonSpkEnum {

        BUKTI_PEMBELIAN(1, "Bukti Pembelian"),
        KWITANSI(2, "Kwitansi"),
        SURAT_PERJANJIAN(3, "Surat Perjanjian"),
        SURAT_PESANAN(4, "Surat Pesanan"),
        SURAT_PERINTAH_KERJA(5, "Surat Perintah Kerja"),
        SURAT_PERINTAH_MULAI_KERJA(6, "Surat Perintah Mulai Kerja"),
        DOKUMEN_LAINNYA(7, "Dokumen Lainnya");

        public final Integer value;
        public final String label;

        private JenisRealisasiNonSpkEnum(Integer value, String label) {

            this.value = value;
            this.label = label;

        }

        public static JenisRealisasiNonSpkEnum fromValue(Integer value){
            JenisRealisasiNonSpkEnum jenis = null;
            if(value!=null){
                switch (value.intValue()) {
                    case 1:	jenis = BUKTI_PEMBELIAN;break;
                    case 2:	jenis = KWITANSI;break;
                    case 3:	jenis = SURAT_PERJANJIAN;break;
                    case 4:	jenis = SURAT_PESANAN;break;
                    case 5:	jenis = SURAT_PERINTAH_KERJA;break;
                    case 6:	jenis = SURAT_PERINTAH_MULAI_KERJA;break;
                    case 7:	jenis = DOKUMEN_LAINNYA;break;
                }
            }
            return jenis;
        }

    }

    @Enumerated(EnumType.ORDINAL)
    public enum BuktiPembayaran{
        NONSPK(0,"Non SPK"),
        SPK(1,"SPK");

        public final Integer id;
        public final String label;

        BuktiPembayaran(int key, String label){
            this.id = Integer.valueOf(key);
            this.label = label;
        }

        public static BuktiPembayaran fromValue(Integer value){
            BuktiPembayaran status = null;
            if(value != null){
                switch(value.intValue()){
                    case 0:status = NONSPK;break;
                    case 1:status = SPK;break;
                    default:status = NONSPK;break;
                }
            }
            return status;
        }

        public static JenisRealisasiNonSpk.BuktiPembayaran[] getBuktiPembayaran(){
            JenisRealisasiNonSpk.BuktiPembayaran[] buktipembayaran = new JenisRealisasiNonSpk.BuktiPembayaran[] {NONSPK, SPK};

            return buktipembayaran;
        }

    }


    @Id(sequence="ekontrak.seq_jenis_realisasi_non_spk", function="nextsequence")
    public Long rsk_id;

    @Required
    public Long lls_id;

    public String rsk_nama;

    public String rsk_nama_dok;

    public String rsk_npwp;

    @Required
    public String rsk_nomor;

    @Required
    @As(binder= RupiahBinder.class)
    public Double rsk_nilai;

    @As(binder= DateBinder.class)
    public Date rsk_tanggal;

    public String rsk_keterangan;

    public Long rsk_id_attachment;

    public Integer rsk_jenis;

    public boolean is_penyedia;

    public Integer rsk_bukti_pembayaran;

    @Transient
    private Non_spk_seleksi spk_seleksi;

    public Non_spk_seleksi getSpkSeleksi(){
        if(spk_seleksi == null)
            spk_seleksi = Non_spk_seleksi.findById(lls_id);
        return spk_seleksi;
    }

    @Transient
    public List<BlobTable> getAttachment() {
        if(rsk_id_attachment == null) {
            return null;
        }
        return BlobTableDao.listById(rsk_id_attachment);
    }

    @Transient
    public BlobTable getBlob() {
        if(rsk_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(rsk_id_attachment);
    }

    @Transient
    public JenisRealisasiNonSpkEnum getJenis() {
        return JenisRealisasiNonSpkEnum.fromValue(rsk_jenis);
    }

    public static void simpanJenisDokumen(JenisRealisasiNonSpk jenisRealisasiNonSpk, File dlpAttachment, String hapus) throws Exception {

        if (dlpAttachment != null) {

            BlobTable bt = null;

            if (jenisRealisasiNonSpk.rsk_id == null)
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment);
            else
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment, jenisRealisasiNonSpk.rsk_id_attachment);

            jenisRealisasiNonSpk.rsk_id_attachment = bt.blb_id_content;

        } else {

            if (jenisRealisasiNonSpk.rsk_id_attachment != null && hapus == null) {

                Long id = jenisRealisasiNonSpk.rsk_id_attachment;

                jenisRealisasiNonSpk.rsk_id_attachment = null;

                BlobTable.delete("blb_id_content=?", id);

            }

            if (hapus != null)
                jenisRealisasiNonSpk.rsk_id_attachment = Long.valueOf(hapus);

        }

        jenisRealisasiNonSpk.save();

    }

    public static List<JenisRealisasiNonSpk> findByNonSpk(Long nonSpkId){

        return find("lls_id =?", nonSpkId).fetch();

    }

    public static Double getJumlahNilaiRealisasi(Long lelangId, Long notIncludeRsk_id){
        Double totalNilaiRealisasi;
        if(notIncludeRsk_id==null || notIncludeRsk_id.equals("")) {
            String query = "SELECT SUM(rsk_nilai) FROM ekontrak.jenis_realisasi_non_spk WHERE lls_id=?";
            totalNilaiRealisasi = Query.find(query, Double.class, lelangId).first();
        }
        else {
            String query = "SELECT SUM(rsk_nilai) FROM ekontrak.jenis_realisasi_non_spk WHERE lls_id=? and rsk_id<>?";
            totalNilaiRealisasi = Query.find(query, Double.class, lelangId,notIncludeRsk_id).first();
        }
        return totalNilaiRealisasi == null ? 0d : totalNilaiRealisasi;

    }

    @Transient
    public int getJumlahPenyedia(){

        if (this.is_penyedia)
            return RealisasiPenyediaNonSpk.countByRealisasi(this.rsk_id);
        else
            return RealisasiNonPenyediaNonSpk.countByRealisasi(this.rsk_id);

    }

    public UploadInfo simpanDokRealisasi(File file) throws Exception{
        BlobTable blob = null;
        if(rsk_id_attachment == null){
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }else if(rsk_id_attachment != null)
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, rsk_id_attachment);
        rsk_id_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }


}
