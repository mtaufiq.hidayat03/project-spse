package models.nonlelang.nonSpk;

import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 11/17/2017.
 */
@Table(name="EKONTRAK.REALISASI_PENYEDIA_SWAKELOLA")
public class RealisasiPenyediaSwakelola extends BaseModel {

    @Id(sequence="ekontrak.seq_realisasi_penyedia_swakelola", function="nextsequence")
    public Long rp_id;

    public Long rkn_id;

    public Long rsk_id;

    public String rp_nama_rekanan;

    public String rp_email_rekanan;

    public String rp_telp_rekanan;

    public String rp_npwp_rekanan;

    public String rp_alamat_rekanan;

    @Transient
    public Rekanan getRekanan()
    {
        return Rekanan.findById(rkn_id);

    }

    public static List<RealisasiPenyediaSwakelola> findWithRealisasi(Long realisasiId) {
        return find("rsk_id=?",realisasiId).fetch();
    }

    public static RealisasiPenyediaSwakelola findWithRealisasiAndRekanan(Long realisasiId, Long rekananId) {
        return find("rsk_id=? AND rkn_id=?",realisasiId, rekananId).first();
    }

    public static boolean isSudahTerdaftar(Long realisasiId, Long rkn_id){

        RealisasiPenyediaSwakelola realisasiPenyediaSwakelola = RealisasiPenyediaSwakelola.findWithRealisasiAndRekanan(realisasiId, rkn_id);

        return realisasiPenyediaSwakelola != null ? true : false;
    }

    public static int countByRealisasi(Long realisasiId){

        return (int)RealisasiPenyediaSwakelola.count("rsk_id=?", realisasiId);

    }

    public static void deleteByRealisasi(Long realiasiId){

        String sql = "DELETE FROM ekontrak.realisasi_penyedia_swakelola WHERE rsk_id = ?";

        Query.update(sql, realiasiId);

    }

}
