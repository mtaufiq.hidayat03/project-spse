package models.nonlelang.nonSpk;

import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.List;

/**
 * Created by Lambang on 5/3/2017.
 */
@Table(name = "EKONTRAK.DOKUMEN_LAIN_NON_SPK")
public class DokumenLainNonSpk extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum JenisDokLainNonSpk{

        INFORMASI_LAINNYA(1);

        public final Integer value;

        private JenisDokLainNonSpk(Integer value) {
            this.value = value;
        }

        public static JenisDokLainNonSpk fromValue(Integer value){
            JenisDokLainNonSpk jenis = null;
            if(value!=null){
                switch (value.intValue()) {
                    case 1:	jenis = INFORMASI_LAINNYA;break;
                }
            }
            return jenis;
        }

    }


    @Id(sequence="ekontrak.seq_dokumen_lain_non_spk", function="nextsequence")
    public Long dlk_id;

    @Required
    public Long lls_id;

    public Integer dlk_jenis;

    public Long dlk_id_attachment;

    @Transient
    public BlobTable getBlob() {
        if(dlk_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dlk_id_attachment);
    }

    @Transient
    public JenisDokLainNonSpk getJenis() {
        return JenisDokLainNonSpk.fromValue(dlk_jenis);
    }

    public static List<DokumenLainNonSpk> findByNonSpkAndJenis(Long nonSpkId, Integer jenis){

        return find("lls_id =? AND dlk_jenis=?", nonSpkId, jenis).fetch();

    }

    public static UploadInfo simpan(Long swakelolaId, Integer jenis, File file) throws Exception {

        DokumenLainNonSpk dokLain = new DokumenLainNonSpk();
        dokLain.lls_id = swakelolaId;
        dokLain.dlk_jenis = jenis;

        BlobTable blob = null;
        if (file != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dokLain.dlk_id_attachment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }
        dokLain.dlk_id_attachment = blob.blb_id_content;
        dokLain.save();

        // untuk keperluan response page yang memakai ajax
        return UploadInfo.findBy(blob);
    }

}
