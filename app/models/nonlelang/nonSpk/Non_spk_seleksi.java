package models.nonlelang.nonSpk;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.Paket_pl;
import models.agency.Pp;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.MaxSize;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 5/3/2017.
 */
@Table(name = "EKONTRAK.NON_SPK_SELEKSI")
public class Non_spk_seleksi extends BaseModel {



    @Enumerated(EnumType.ORDINAL)
    public enum BuktiPembayaran{
        NONSPK(0,"Non SPK"),
        SPK(1,"SPK");

        public final Integer id;
        public final String label;

        BuktiPembayaran(int key, String label){
            this.id = Integer.valueOf(key);
            this.label = label;
        }

        public static BuktiPembayaran fromValue(Integer value){
            BuktiPembayaran status = null;
            if(value != null){
                switch(value.intValue()){
                    case 0:status = NONSPK;break;
                    case 1:status = SPK;break;
                    default:status = NONSPK;break;
                }
            }
            return status;
        }

        public static Non_spk_seleksi.BuktiPembayaran[] getBuktiPembayaran(){
            Non_spk_seleksi.BuktiPembayaran[] buktipembayaran = new Non_spk_seleksi.BuktiPembayaran[] {NONSPK, SPK};

            return buktipembayaran;
        }

    }
	

    @Id(sequence = "ekontrak.seq_non_spk_seleksi", function = "nextsequence")
    public Long lls_id;

    public String lls_uraian_pekerjaan;

    public Date lls_dibuat_tanggal;

    
    
    @As(binder = DateBinder.class)
    public Date lls_tanggal_paket_mulai;
    
    @As(binder = DateBinder.class)
    public Date lls_tanggal_paket_selesai;

    public Integer lls_nilai_gabungan;

    public Long lls_jangka_waktu_pelaksanaan;

    public String lls_nama_penyedia;
    @MaxSize(20)
    public String lls_npwp_penyedia;
    @Phone
    public String lls_telp_penyedia;

    public String lls_alamat_penyedia;

    @Required
    public StatusPl lls_status;

    @Required
    public Integer mtd_pemilihan;


    public Integer lls_bukti_pembayaran;
    

    //relasi ke paket pl
    public Long pkt_id;

    @Transient
    private Paket_pl paket;

    public Paket_pl getPaket() {
        if(paket == null)
            paket = Paket_pl.findById(pkt_id);
        return paket;
    }

    @Transient
    public MetodePemilihanPenyedia getPemilihan()
    {
        return MetodePemilihanPenyedia.findById(mtd_pemilihan);
    }

    @Transient
    public BuktiPembayaran getBuktiPembayaran()
    {
        return BuktiPembayaran.fromValue(this.lls_bukti_pembayaran);
    }

    public Pp getPp(){
        return Query.find("SELECT p.* FROM ekontrak.pp p LEFT JOIN ekontrak.paket_swakelola pkt ON pkt.pp_id=p.pp_id WHERE pkt.pkt_id=?", Pp.class, pkt_id).first();
    }

    public String getNamaPaket() {
        return Query.find("select pkt_nama from ekontrak.paket where pkt_id=?", String.class, pkt_id).first();
    }
    
    @Transient
    public Kategori getKategori() {
		return Query.find("select kgr_id from ekontrak.paket where pkt_id=?", Kategori.class, pkt_id).first();
    }

    public static Non_spk_seleksi findByPaket(Long paketId){
        return find("pkt_id=? order by lls_id DESC", paketId).first();
    }

    @Transient
    private List<HistoryUbahTanggalNonSpk> historyUbahTanggalNonSpkList;


    /**
     * membuat lelang baru dengan nilai-nilai default
     * event ini terjadi saat pembuatan paket
     * @param paket
     */
    public static void buatNonSpkBaru(Paket_pl paket, MetodePemilihanPenyedia metodePemilihanPenyedia) {
        Non_spk_seleksi nonseleksi = Non_spk_seleksi.find("pkt_id = ?", paket.pkt_id).first();
        if (nonseleksi == null) { // berarti belum pernah ada lelang yang dibuat
            nonseleksi = new Non_spk_seleksi();
            nonseleksi.pkt_id = paket.pkt_id;
            if(metodePemilihanPenyedia != null)
                nonseleksi.mtd_pemilihan = metodePemilihanPenyedia.id; // default pengadaan langsung
//            nonseleksi.lls_versi_lelang = Integer.valueOf(1);
            nonseleksi.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
            nonseleksi.lls_bukti_pembayaran =  Integer.valueOf(0); // default pengadaan langsung
            nonseleksi.lls_status = StatusPl.AKTIF;
            nonseleksi.save();
        }
    }

    /**
     * mendapatkan informasi status lelang , tanpa harus ambil seluruh Object lelang_seleksi
     *
     * @param lelangId
     * @return
     */
    public static StatusPl findStatusLelang(Long lelangId) {
        return Query.find("SELECT lls_status FROM ekontrak.non_spk_seleksi WHERE lls_id=?", StatusPl.class, lelangId).first();
    }

    public static final ResultSetHandler<String[]> resultsetNonSpk = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[10];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_kegiatan");
            tmp[2] = rs.getString("agc_nama");
            if(CommonUtil.isEmpty(tmp[2]))
                tmp[2] = rs.getString("nama");
            tmp[3] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[4] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
//			tmp[5] = Metode.findById(rs.getInt("mtd_id")).label;
            tmp[5] = pemilihanpl.getLabel();
//			tmp[7] = MetodeEvaluasi.findById(rs.getInt("mtd_evaluasi")).label;
            tmp[7] = Kategori.findById(rs.getInt("kgr_id")).getNama()  + " - TA " + rs.getString("anggaran");
            tmp[8] = pkt_flag.toString();
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetNonSpkRekanan = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_kegiatan");
            if (versi > 1)
                namaLelang += " <span class=\"badge  badge-warning\">Non Tender Ulang</span>";
            tmp[0] = lelangId.toString();
            tmp[1] = namaLelang;
            tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[3] = pkt_flag.toString();
            tmp[4] = pemilihanpl.getLabel();
            tmp[5] = pemilihan.getKey().toString();
            return tmp;
        }
    };



    public static final ResultSetHandler<String[]> resultsetNonSpkPp = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
//            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
            String[] tmp = new String[6];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
//            boolean lelangV3 = pkt_flag < 2;
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = Paket_pl.StatusPaket.fromValue(rs.getInt("pkt_status")).label;
            tmp[3] = FormatUtils.formatDateInd(rs.getDate("lls_dibuat_tanggal"));;
            tmp[4] = pkt_flag.toString();
            tmp[5] = pemilihanpl.getLabel();
//            tmp[6] = rs.getInt("pkt_status") == Paket_pl.StatusPaket.SELESAI_LELANG.id ? "true" : "false";
            return tmp;
        }
    };

    public List<JenisRealisasiNonSpk> getJenisRealisasi(){

        return JenisRealisasiNonSpk.findByNonSpk(this.lls_id);

    }

    public List<DokumenLainNonSpk> getInformasiLainnya(){

        return DokumenLainNonSpk.findByNonSpkAndJenis(this.lls_id, DokumenLainNonSpk.JenisDokLainNonSpk.INFORMASI_LAINNYA.value);

    }

    public static List<Non_spk_seleksi> getByNonSpkByLewatTanggalSelesai(){

        String date = FormatUtils.formatDateTimeSecond(ConfigurationDao.isProduction()? models.jcommon.util.DateUtil.newDate() : models.jcommon.util.DateUtil.newDate(0));

        return Non_spk_seleksi.find("lls_id in (SELECT lls.lls_id FROM ekontrak.non_spk_seleksi lls, ekontrak.paket p " +
                "WHERE lls.pkt_id = p.pkt_id AND lls.lls_tanggal_paket_selesai < '"+date+"'::date AND p.pkt_status='1')").fetch();

    }

    public List<HistoryUbahTanggalNonSpk> getHistoryUbahTanggalNonSpkList() {
        if(historyUbahTanggalNonSpkList == null)
            historyUbahTanggalNonSpkList = HistoryUbahTanggalNonSpk.find("lls_id=?", lls_id).fetch();

        return historyUbahTanggalNonSpkList;
    }

    public static void hapusNonSpk(Long pktId){
        Paket_pl paket = Paket_pl.findById(pktId);
        if(paket.pkt_status.isDraft()){
            Non_spk_seleksi nonSpk = Non_spk_seleksi.findByPaket(paket.pkt_id);

            Query.update("DELETE FROM ekontrak.realisasi_non_penyedia_non_spk WHERE rsk_id IN (SELECT rsk_id FROM ekontrak.jenis_realisasi_non_spk WHERE lls_id = ?)",nonSpk.lls_id);
            Query.update("DELETE FROM ekontrak.realisasi_penyedia_non_spk WHERE rsk_id IN (SELECT rsk_id FROM ekontrak.jenis_realisasi_non_spk WHERE lls_id = ?)",nonSpk.lls_id);

            Query.update("DELETE FROM ekontrak.dok_non_spk WHERE lls_id = ?", nonSpk.lls_id);
            Query.update("DELETE FROM ekontrak.jenis_realisasi_non_spk WHERE lls_id = ?", nonSpk.lls_id);
            Query.update("DELETE FROM ekontrak.dokumen_lain_non_spk WHERE lls_id = ?", nonSpk.lls_id);
            Query.update("DELETE FROM ekontrak.non_spk_seleksi WHERE lls_id = ?", nonSpk.lls_id);

            Query.update("DELETE FROM ekontrak.paket_lokasi WHERE pkt_id = ?", paket.pkt_id);
            Query.update("DELETE FROM ekontrak.paket_pp WHERE pkt_id = ?", paket.pkt_id);
            Query.update("DELETE FROM ekontrak.paket_anggaran WHERE pkt_id = ?", paket.pkt_id);
            Query.update("DELETE FROM ekontrak.paket_satker WHERE pkt_id = ?", paket.pkt_id);
            Query.update("DELETE FROM ekontrak.paket WHERE pkt_id = ?", paket.pkt_id);
        }
    }

    public static final ResultSetHandler<String[]> resultsetNonSpkPublic = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
//            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
            String[] tmp = new String[8];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
//            boolean lelangV3 = pkt_flag < 2;
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = rs.getString("instansi");
            tmp[3] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_pagu"));
            tmp[4] = pemilihanpl.label;
            tmp[5] = Kategori.findById(rs.getInt("kgr_id")).nama;
            tmp[6] = rs.getString("ang_tahun");
            tmp[7] = pkt_flag.toString();
            return tmp;
        }
    };
}
