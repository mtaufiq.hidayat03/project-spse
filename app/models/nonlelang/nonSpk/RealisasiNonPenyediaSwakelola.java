package models.nonlelang.nonSpk;

import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 11/17/2017.
 */
@Table(name="EKONTRAK.REALISASI_NON_PENYEDIA_SWAKELOLA")
public class RealisasiNonPenyediaSwakelola extends BaseModel {

    @Id(sequence="ekontrak.seq_realisasi_non_penyedia_swakelola", function="nextsequence")
    public Long rn_id;

    public Long rkn_id;

    public Long rsk_id;

    public String rn_nama_rekanan;

    public String rn_email_rekanan;

    public String rn_telp_rekanan;

    public String rn_npwp_rekanan;

    public String rn_alamat_rekanan;

    @Transient
    public Rekanan getRekanan()
    {
        return Rekanan.findById(rkn_id);

    }

    public static List<RealisasiNonPenyediaSwakelola> findWithRealisasi(Long realisasiId) {
        return find("rsk_id=?",realisasiId).fetch();
    }

    public static RealisasiNonPenyediaSwakelola findWithRealisasiAndRekanan(Long realisasiId, Long rekananId) {
        return find("rsk_id=? AND rkn_id=?",realisasiId, rekananId).first();
    }

    public static boolean isSudahTerdaftar(Long realisasiId, Long rkn_id){

        RealisasiNonPenyediaSwakelola realisasiNonPenyediaSwakelola = RealisasiNonPenyediaSwakelola.findWithRealisasiAndRekanan(realisasiId, rkn_id);

        return realisasiNonPenyediaSwakelola != null ? true : false;
    }

    public static int countByRealisasi(Long realisasiId){

        return (int)RealisasiNonPenyediaSwakelola.count("rsk_id=?", realisasiId);

    }

    public static void deleteByRealisasi(Long realiasiId){

        String sql = "DELETE FROM ekontrak.realisasi_non_penyedia_swakelola WHERE rsk_id = ?";

        Query.update(sql, realiasiId);

    }

}
