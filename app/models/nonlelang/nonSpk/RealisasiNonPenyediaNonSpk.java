package models.nonlelang.nonSpk;

import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 11/15/2017.
 */
@Table(name="EKONTRAK.REALISASI_NON_PENYEDIA_NON_SPK")
public class RealisasiNonPenyediaNonSpk extends BaseModel {

    @Id(sequence="ekontrak.seq_realisasi_non_penyedia_non_spk", function="nextsequence")
    public Long rn_id;

    public Long rkn_id;

    public Long rsk_id;

    public String rn_nama_rekanan;

    public String rn_email_rekanan;

    public String rn_telp_rekanan;

    public String rn_npwp_rekanan;

    public String rn_alamat_rekanan;

    @Transient
    public Rekanan getRekanan()
    {
        return Rekanan.findById(rkn_id);

    }

    public static List<RealisasiNonPenyediaNonSpk> findWithRealisasi(Long realisasiId) {
        return find("rsk_id=?",realisasiId).fetch();
    }

    public static RealisasiNonPenyediaNonSpk findWithRealisasiAndRekanan(Long realisasiId, Long rekananId) {
        return find("rsk_id=? AND rkn_id=?",realisasiId, rekananId).first();
    }

    public static boolean isSudahTerdaftar(Long realisasiId, Long rkn_id){

        RealisasiNonPenyediaNonSpk realisasiNonPenyediaNonSpk = RealisasiNonPenyediaNonSpk.findWithRealisasiAndRekanan(realisasiId, rkn_id);

        return realisasiNonPenyediaNonSpk != null ? true : false;
    }

    public static int countByRealisasi(Long realisasiId){

        return (int)RealisasiNonPenyediaNonSpk.count("rsk_id=?", realisasiId);

    }

    public static void deleteByRealisasi(Long realiasiId){

        String sql = "DELETE FROM ekontrak.realisasi_non_penyedia_non_spk WHERE rsk_id = ?";

        Query.update(sql, realiasiId);

    }

}
