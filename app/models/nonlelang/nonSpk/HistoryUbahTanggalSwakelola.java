package models.nonlelang.nonSpk;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * Created by Lambang on 11/8/2017.
 */

@Table(name="EKONTRAK.HISTORY_UBAH_TANGGAL_SWAKELOLA")
public class HistoryUbahTanggalSwakelola extends BaseModel {

    @Id
    public Long lls_id;

    @Id
    public Date uts_tanggal_edit;

    public Date uts_tanggal_asli;

    public Date uts_tanggal_ubah;

    public String uts_keterangan;

}
