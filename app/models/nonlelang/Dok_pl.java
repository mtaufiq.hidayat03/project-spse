package models.nonlelang;

import controllers.BasicCtr;
import ext.FormatUtils;
import models.agency.*;
import models.common.*;
import models.handler.DokLelangDownloadHandler;
import models.handler.DokPlDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Checklist_master;
import models.lelang.Checklist_master.JenisChecklist;
import models.lelang.LDPContent;
import models.lelang.SskkContent;
import models.nonlelang.contracts.DokumenNonLelangContract;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.templates.Template;
import play.templates.TemplateLoader;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.*;

@Table(name="EKONTRAK.DOK_NONLELANG")
public class Dok_pl extends BaseModel implements DokumenNonLelangContract {

	@Enumerated(EnumType.ORDINAL)
	public enum JenisDokPl {
		DOKUMEN_LELANG(0, "Dokumen Pemilihan"),
		DOKUMEN_LELANG_PRA(1, "Dokumen Kualifikasi"),
		DOKUMEN_PRAKUALIFIKASI(2, "Dokumen Prakualifikasi"),// tidak dipake di spse 4, tp masih dipake di 3.5
		DOKUMEN_ADENDUM(3, "Dokumen Adendum"),  // tidak dipake di spse 4, tp masih dipake di 3.5
		DOKUMEN_TEKNIS(4, "Dokumen Teknis"), // tidak dipake di spse 4, tp masih dipake di 3.5
		DOKUMEN_ADENDUM_PRA(5, "Dokumen Adendum Prakualifikasi");
		
		public final Integer value;
		public final String label;
		
		private JenisDokPl(int value, String label) {
			this.value = Integer.valueOf(value);
			this.label = label;
		}
		
		public static JenisDokPl fromValue(Integer value){
			JenisDokPl jenis = null;
			if(value!=null){
				switch (value.intValue()) {
					case 0: jenis = DOKUMEN_LELANG;break;
					case 1:	jenis = DOKUMEN_LELANG_PRA;break;
					case 2:	jenis = DOKUMEN_PRAKUALIFIKASI;break;
					case 3:	jenis = DOKUMEN_ADENDUM;break;
					case 4:	jenis = DOKUMEN_TEKNIS;break;
					case 5:	jenis = DOKUMEN_ADENDUM_PRA;break;
				}
			}
			return jenis;
		}
		
		public boolean isAdendum(){
			return this == DOKUMEN_ADENDUM;
		}

		public boolean isAdendumPra(){
			return this == DOKUMEN_ADENDUM_PRA;
		}
		
		public boolean isDokLelang(){
			return this == DOKUMEN_LELANG;
		}

		public boolean isDokKualifikasi(){
			return this == DOKUMEN_LELANG_PRA;
		}

		public String getLabel()
		{
			return Messages.get(label);
		}
		
	}
	
	@Id(sequence="ekontrak.seq_dok_nonlelang", function="nextsequence")
	public Long dll_id;

	public String dll_nama_dokumen;

	public JenisDokPl dll_jenis;

	public Long dll_id_attachment;

	//relasi ke Pl_seleksi
	public Long lls_id;

	@Transient
	public List<ChecklistPl> checklistPls;
	
	@Transient
	private Pl_seleksi pl_seleksi;
	@Transient
	public Dok_pl_content dok_pl_content;

    public Pl_seleksi getPl_seleksi() {
    	if(pl_seleksi == null)
    		pl_seleksi = Pl_seleksi.findById(lls_id);
		return pl_seleksi;
	}

//	public Dok_pl_content getDok_pl_content() {
//    	if(dok_pl_content == null && dll_id != null){
//			dok_pl_content = Dok_pl_content.findBy(dll_id);
//		}
//		return dok_pl_content;
//	}

	public String getAuditUser() {
        return Pegawai.findBy(audituser).peg_nama;
    }
	
	public Kategori getKategori() {
		return getPl_seleksi().getKategori();
    }
	
	@Transient
	public BlobTable getDokumen() {
		if(dll_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dll_id_attachment);
	}
	
	@Transient
	public List<BlobTable> getDokumenList() {
		if(dll_id_attachment == null)
			return null;
		return BlobTableDao.listById(dll_id_attachment);
	}

	@Override
	public Long getId() {
		return this.dll_id;
	}

	public Dok_pl_content getDokumenLelangContent() {
    	if(dok_pl_content == null && dll_id != null){
			dok_pl_content = Dok_pl_content.findBy(dll_id);
		}
		return dok_pl_content;
	}

	@Override
	public List<ChecklistPl> getChecklistPls() {
		return this.checklistPls;
	}

	@Override
	public void setDokumenContent(Dok_pl_content model) {
		this.dok_pl_content = model;
	}

	@Override
	public void setChecklistPls(List<ChecklistPl> items) {
    	Logger.debug("set checklist");
		this.checklistPls = items;
		Logger.debug(CommonUtil.toJson(this.checklistPls));
	}

	/**
	 * status dok lelang boleh diedit atau tidak
	 * @return
     */
	public boolean isEditable(Pl_seleksi pl, Date currentDate) {
		if(pl.lls_status.isDraft()){
			if(dll_jenis.isDokLelang()){
				return true;
			}else{
				return !pl.isSedangPersetujuan();
			}
		}
//		TahapStartedPl tahapNow = new TahapStartedPl(pl.lls_id);
//		boolean editable = true;
//		if (pl.lls_status.isDraft()) {
//			return editable;
//		}
		else if (pl.lls_status.isAktif()) {
//		if (dll_jenis.isKualifikasi()) {
			
			return pl.getTahapStarted().isAllowAdendum();
//		}
//			if (dll_id != null) {
//				JenisDokPl jenis = dll_jenis;
//				Dok_pl_content dok_pl_content = null;
//				if (jenis.isDokLelang()) {
//					dok_pl_content = Dok_pl_content.findBy(dll_id);
//					editable = (dok_pl_content.dll_modified && dok_pl_content.dll_versi == 1) || tahapNow.isAllowAdendum();
//				}
//
//			}
//			 if (dll_jenis.isDokLelang()) {
//				return pl.getTahapStarted().isAllowAdendum();
//			}
		}
		return false;
	}
	
	public static Dok_pl findBy(Long lelangId, JenisDokPl jenis) {
		return find("lls_id=? and dll_jenis=?", lelangId, jenis).first();		
	}
	
	/**
	 * dapatkan dokumen pl, jika tidak ada maka akan dibuat baru
	 * @param jenis
	 * @return
	 */
	public static Dok_pl findNCreateBy(Long plId, JenisDokPl jenis) {
		Dok_pl dok_pl = find("lls_id=? and dll_jenis=?", plId, jenis).first();
		if(dok_pl == null) {
			dok_pl = new Dok_pl();
			dok_pl.lls_id = plId;
			dok_pl.dll_jenis = jenis;
			dok_pl.save();
		}
		return dok_pl;
	}

	public boolean isAllowAdendum(Pl_seleksi pl) {
		if (dll_jenis.isDokKualifikasi()) {
			return pl.getTahapStarted().isAllowAdendumPra();
		}else if (dll_jenis.isDokLelang()) {
			return pl.getTahapStarted().isAllowAdendum();
		}
		return false;
	}

	/**
	 * Mendapatkan LDP content terakhir yang berlaku
	 * digunakan oleh Apendo
	 * @return
	 */
	@Transient
	public LDPContent getLdpContent() {
		String content = Query.find("SELECT dll_ldp FROM ekontrak.dok_nonlelang_content WHERE dll_id=? AND dll_ldp is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, LDPContent.class);
		return null;
	}
	
	/**
	 * Mendapatkan Daftar Kuantitas terakhir yang berlaku
	 * digunakan oleh Apendo
	 * @return
	 */
	@Transient
	public DaftarKuantitas getRincianHPS() {
		String content = Query.find("SELECT dll_dkh FROM ekontrak.dok_nonlelang_content WHERE dll_id=? AND dll_dkh is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, DaftarKuantitas.class);
		return null;
	}
	
	/**
	 * Mendapatkan SSKK terakhir yang berlaku
	 * digunakan oleh Kontrak dan Adendum
	 * @return
	 */
	@Transient
	public SskkContent getSskkContent() {
		String content = Query.find("SELECT dll_sskk FROM ekontrak.dok_nonlelang_content WHERE dll_id=? AND dll_sskk is not null AND dll_modified='FALSE' ORDER BY dll_versi DESC", String.class, dll_id).first();
		if (!CommonUtil.isEmpty(content))
			return CommonUtil.fromJson(content, SskkContent.class);
		return null;
	}
	
//	@Transient
//	public String getJenisDokumen() {
//		if(dll_jenis.isDokLelang())
//			return "Dokumen Kualifikasi";
////		Metode metode = Lelang_seleksi.getMetodeLelang(lls_id);
//		return metode.kualifikasi.isPra() ? "Dokumen Pemilihan":"Dokumen Pengadaan";
//	}

	/**
	 * simpan Rincian HPS
	 * author Arief Ardiyansah
	 * @param pl
	 * @param data
	 * @param fixed
	 * @throws Exception
	 */
	public void simpanHps(Pl_seleksi pl, String data, boolean fixed, boolean enableViewHps) throws Exception {
		Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dll_id, pl);
		DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,fixed);
		Paket_pl paket = Paket_pl.findById(pl.pkt_id);
		if (dk.total == null)
			throw new Exception(Messages.get("nonlelang.belum_isi_hps"));
		else if (dk.total > paket.pkt_pagu) {
			throw new Exception(Messages.get("nonlelang.total_lebih_pagu"));
		} else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket
			paket.pkt_hps = dk.total;
			paket.pkt_hps_enable = enableViewHps;
			paket.save();
			dok_pl_content.dkh = dk;
			simpan(pl.lls_status, paket.pkt_nama, dok_pl_content);
		}
	}

	public void simpan(StatusPl status, String namaPaket, Dok_pl_content dok_pl_content) {
		if(CommonUtil.isEmpty(dll_nama_dokumen))
			dll_nama_dokumen = String.format("[%s] - %s.pdf", getJenisDokumen(),lls_id, StringUtils.abbreviate(namaPaket, 200));
		dok_pl_content.dll_modified = true;
		dok_pl_content.save();
		save();
	}

	public UploadInfo simpan(Dok_pl_content dok_pl_content, Pl_seleksi pl , File dokumen){
		BlobTable blob = null;
		try {
			if(dokumen != null)
				blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dokumen, dok_pl_content.dll_content_attachment);
			if(blob != null)
				dok_pl_content.dll_content_attachment = blob.blb_id_content;
			String content = cetakPreview(pl, dok_pl_content);
			if(!StringUtils.isEmpty(content)){
				dok_pl_content.dll_content_html = content.replaceAll("<br>", "<br/>");
			}
			dok_pl_content.dll_modified = false;
			dok_pl_content.dll_syarat_updated = false;
			dok_pl_content.dll_ldk_updated= false;
			dok_pl_content.save();
			if(dok_pl_content.dll_versi == 1){
				dll_id_attachment = blob.blb_id_content;
			}
			if(dok_pl_content.dll_kontrak_pembayaran != null) {
				if(dok_pl_content.isAdendum()) {
					JenisKontrak jenisKontrakLama = pl.getKontrakPembayaran();
					Dok_pl_content prevDokLelangContent = Dok_pl_content.findBy(dll_id, dok_pl_content.dll_versi - 1);
					prevDokLelangContent.dll_kontrak_pembayaran = jenisKontrakLama.id;
					prevDokLelangContent.save();
				}
				pl.setKontrakPembayaran(JenisKontrak.findById(dok_pl_content.dll_kontrak_pembayaran));
				pl.save();
			}
			save();

			DokPersiapanPl dokPersiapanPl = DokPersiapanPl.findLastByPaket(pl.pkt_id);
			dokPersiapanPl.dp_modified = false;
			dokPersiapanPl.save();

		}catch (Exception e){
			Logger.error(e, Messages.get("nonlelang.simpan_doc_non_tender_gagal")+" : %s", e.getMessage());
		}
		return UploadInfo.findBy(blob);
	}

	public void simpanCetak(Dok_pl_content dok_pl_content, InputStream dok) {
		try {
			BlobTable blob = BlobTableDao.saveInputStream(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dok, dll_nama_dokumen);
			dll_id_attachment = blob.blb_id_content;
			dok_pl_content.dll_content_attachment = blob.blb_id_content;
			dok_pl_content.dll_modified = false;
			dok_pl_content.dll_syarat_updated = false;
			dok_pl_content.dll_ldk_updated= false;
		} catch (Exception e) {
			Logger.error(e, Messages.get("nonlelang.gagal_simpan_doc")+" %s", lls_id);
			dok_pl_content.dll_modified = true;
		}
		dok_pl_content.save();
		save();
	}


	@Transient
	public List<ChecklistPl> getSyaratIjinUsaha() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id=1 order by chk_id ASC", dll_id, versi).fetch();
	}

	@Transient
	public List<ChecklistPl> getSyaratKualifikasi() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by chk_id, ckm_id ASC", dll_id, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI, versi).fetch();
	}
	
	@Transient
	public List<ChecklistPl> getSyaratKualifikasiAdministrasi() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 50 and (kgr_id=? or kgr_id is null) and ckm_jenis=?) order by ckm_id ASC, chk_id ASC", dll_id, versi, getKategori(), JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI).fetch();
	}
	
	@Transient
	public List<ChecklistPl> getSyaratKualifikasiTeknis() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 50 and (kgr_id=? or kgr_id is null) and ckm_jenis=?) order by ckm_id ASC, chk_id ASC", dll_id, versi, getKategori(), JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS).fetch();
	}
	
	@Transient
	public List<ChecklistPl> getSyaratKualifikasiKeuangan() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 50 and (kgr_id=? or kgr_id is null) and ckm_jenis=?) order by ckm_id ASC, chk_id ASC", dll_id, versi, getKategori(), JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN).fetch();
	}
	

	@Transient
	public List<ChecklistPl> getSyaratTeknis() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by ckm_id", dll_id, versi, JenisChecklist.CHECKLIST_TEKNIS).fetch();
	}
	
	@Transient
	public List<ChecklistPl> getSyaratIjinUsahaBaru() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id=50 order by chk_id ASC", dll_id, versi).fetch();
	}
	
    @Transient
    public List<ChecklistPl> getSyaratAdministrasiUnchecked() {
        Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
        return ChecklistPl.find("dll_id=? AND chk_versi=? AND ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=? and ckm_checked=0) order by ckm_id", dll_id, versi, JenisChecklist.CHECKLIST_ADMINISTRASI).fetch();
    }


	@Transient
	public List<ChecklistPl> getSyaratAdministrasi() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by ckm_id", dll_id, versi, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI).fetch();
	}

	@Transient
	public List<ChecklistPl> getSyaratHarga() {
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_SYARAT);
		return ChecklistPl.find("dll_id=? and chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_id <> 1 and ckm_jenis=?) order by ckm_id", dll_id, versi, Checklist_master.JenisChecklist.CHECKLIST_HARGA).fetch();
	}

	@Transient
	public String getJenisDokumen() {
		if(dll_jenis.isDokKualifikasi()) {
			return Messages.get("Dokumen Kualifikasi");
		}
		return Messages.get("Dokumen Pemilihan");
	}
	
	public Integer getMasaBerlakuPenawaran() {
		LDPContent ldpContent = getLdpContent();
		if(ldpContent != null && ldpContent.masa_berlaku_penawaran != null)
			return ldpContent.masa_berlaku_penawaran;
		return Integer.valueOf(0);
	}

	public static String cetak_content(Pl_seleksi pl, Dok_pl dok_pl, Dok_pl_content dok_pl_content, Template template) {
		Paket_pl paket_pl = Paket_pl.findById(pl.pkt_id);
		Kategori kategori = pl.getKategori();
		if (CommonUtil.isEmpty(dok_pl_content.dll_sbd_versi))
			dok_pl_content.dll_sbd_versi = Dok_pl_content.DEFAULT_VERSI;

		Map<String, Object> params = new HashMap<String, Object>();
		boolean adendum = dok_pl_content.dll_versi > 1;
		params.put("tahunAnggaran", pl.getPaket().getTahunAnggaran());
		params.put("website", BasicCtr.LPSE_URL);
		params.put("adendum", adendum);
		params.put("paket", paket_pl);
		params.put("dok_lelang", dok_pl);
		params.put("kategori", kategori);
		params.put("pemilihan", pl.getPemilihan());
		params.put("nomor", dok_pl_content.dll_nomorSDP);
		params.put("tanggal", FormatUtils.formatDateInd(dok_pl_content.dll_tglSDP));

		if (dok_pl_content.ldpcontent != null) {
			params.put("kontrak", dok_pl_content.ldpcontent.kontrak_pembayaran != null ? JenisKontrak.findById(dok_pl_content.ldpcontent.kontrak_pembayaran) : JenisKontrak.HARGA_SATUAN);
			params.put("ldpcontent", dok_pl_content.ldpcontent);
			params.put("kontrak_pembayaran", JenisKontrak.findById(dok_pl_content.ldpcontent.kontrak_pembayaran).label);
		}

		if (adendum) {
			params.put("versi", dok_pl_content.dll_versi);
			DateTime tanggal_perubahan = new DateTime(dok_pl_content.auditupdate.getTime());
			String hari = FormatUtils.day[(tanggal_perubahan.getDayOfWeek() == 7 ? 0 : tanggal_perubahan.getDayOfWeek())];
			int tgl = tanggal_perubahan.getDayOfMonth();
			String bulan = FormatUtils.month[tanggal_perubahan.getMonthOfYear() - 1];
			int tahun = tanggal_perubahan.getYear();
            String info = hari + " tanggal " + tgl + " bulan " + bulan +
                    " tahun " + tahun + '(' + FormatUtils.formatDateInd(tanggal_perubahan.toDate()) + ')';
            params.put("hari", info);
		}
		if (!dok_pl.dll_jenis.isDokKualifikasi()) { // untuk dokumen kualifikasi
			String[][] tenagaAhli = (dok_pl_content.ldpcontent != null && dok_pl_content.ldpcontent.bobotTenagaAhli != null) ? dok_pl_content.ldpcontent.parseTenagaAhliToArray() : null;
			params.put("ldpcontent", dok_pl_content.ldpcontent);
			params.put("syaratTeknis", dok_pl.getSyaratTeknis());
			params.put("tenagaAhli", tenagaAhli);
			params.put("daftar_kuantitas", dok_pl_content.dkh);
			params.put("satker", Satuan_kerja.findById(paket_pl.stk_id));
			params.put("ldk_modified", dok_pl_content.dll_ldk_updated);

			List<BlobTable> list = dok_pl_content.getDokSpek();
			if (!CommonUtil.isEmpty(list)) {
				StringBuilder spekTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : list) {
					spekTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				spekTemplate.append("</ol>");
				params.put("dok_spek", spekTemplate.toString());
			}
			List<BlobTable> listDokLainnya = dok_pl_content.getDokLainnya();
			if (!CommonUtil.isEmpty(listDokLainnya)) {
				StringBuilder lainnyaTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : listDokLainnya) {
					lainnyaTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokLelangDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				lainnyaTemplate.append("</ol>");
				params.put("dok_lainnya", lainnyaTemplate.toString());
			}
			params.put("lelang", pl);
			params.put("pp", paket_pl.getPp());
			params.put("ijinList", dok_pl.getSyaratIjinUsaha());
			params.put("syaratList", dok_pl.getSyaratKualifikasi());
			params.put("ijinBaruList", dok_pl.getSyaratIjinUsahaBaru());
			params.put("syaratAdminList", dok_pl.getSyaratKualifikasiAdministrasi());
			params.put("syaratTeknisList", dok_pl.getSyaratKualifikasiTeknis());
			params.put("syaratKeuanganList", dok_pl.getSyaratKualifikasiKeuangan());
		}

		params.put("doc_path", ConfigurationDao.CDN_URL+"/documents");
		params.put("isPanitia", Active_user.current().isPanitia());

		return template.render(params);

	}

	public boolean isEmptyDokumen() {
		return dll_id_attachment == null || BlobTableDao.count("blb_id_content=?", dll_id_attachment) == 0;
	}

	public String cetakPreview(Pl_seleksi pl , Dok_pl_content dok_pl_content){
		Paket_pl paket = Paket_pl.findById(pl.pkt_id);
		Kategori kategori = pl.getKategori();
		if(CommonUtil.isEmpty(dok_pl_content.dll_sbd_versi))
			dok_pl_content.dll_sbd_versi = Dok_pl_content.DEFAULT_VERSI;
		Map<String, Object> params = new HashMap<String, Object>();
		boolean adendum = dok_pl_content.dll_versi > 1;
		params.put("dok_pl_content", dok_pl_content);
		params.put("adendum", adendum);
		params.put("pl",pl);
		params.put("paket", paket);
		params.put("dok_pl", this);
		params.put("kategori", kategori);
		params.put("kontrak", pl.getKontrakPembayaran());
		params.put("pemilihan", pl.getPemilihan());
		params.put("evaluasi", pl.getEvaluasi());
		params.put("nomor", dok_pl_content.dll_nomorSDP);
		params.put("tanggal", FormatUtils.formatDateInd(dok_pl_content.dll_tglSDP));
		params.put("tahunAnggaran", paket.getTahunAnggaran());
		
		if(adendum){
			params.put("versi", dok_pl_content.dll_versi);
			DateTime tanggal_perubahan = new DateTime(dok_pl_content.auditupdate.getTime());
			String hari = FormatUtils.day[(tanggal_perubahan.getDayOfWeek() == 5 ? 0 : tanggal_perubahan.getDayOfWeek())];
			int tgl = tanggal_perubahan.getDayOfMonth();
			String bulan = FormatUtils.month[tanggal_perubahan.getMonthOfYear()-1];
			int tahun = tanggal_perubahan.getYear();
			String info = hari + Messages.get("nonlelang.date") + tgl + Messages.get("nonlelang.mounth") + bulan +
					Messages.get("nonlelang,year") + tahun + '(' + FormatUtils.formatDateInd(tanggal_perubahan.toDate()) + ')';
			params.put("hari", info);
		}
		if(!dll_jenis.isDokKualifikasi()){
			params.put("syaratAdmin", getSyaratAdministrasi());
			params.put("syaratTeknis", getSyaratTeknis());
			params.put("syaratHarga", getSyaratHarga());
			params.put("ldpcontent", dok_pl_content.ldpcontent != null ? dok_pl_content.ldpcontent : getLdpContent());
			params.put("masa_berlaku", dok_pl_content.ldpcontent != null ? dok_pl_content.ldpcontent.masa_berlaku_penawaran : getMasaBerlakuPenawaran());
			params.put("syarat_modified", dok_pl_content.dll_syarat_updated);
			params.put("syarat_cetak", !dll_jenis.isDokKualifikasi());
			params.put("daftar_kuantitas", dok_pl_content.dkh != null ?dok_pl_content.dkh : getRincianHPS());
			Paket_satker_pl paket_satker = Paket_satker_pl.find("pkt_id=?", paket.pkt_id).first();
			params.put("satker", Satuan_kerja.findById(paket_satker.stk_id));
			if(dok_pl_content.ldpcontent != null){
				params.put("ldpcontent", dok_pl_content.ldpcontent);
				params.put("masa_berlaku", dok_pl_content.ldpcontent.masa_berlaku_penawaran);
			}
			List<BlobTable> list = dok_pl_content.getDokSpek();
			if (!CommonUtil.isEmpty(list)) {
				StringBuilder spekTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : list) {
					spekTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokPlDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				spekTemplate.append("</ol>");
				params.put("dok_spek", spekTemplate.toString());
			}
			List<BlobTable> listDokLainnya = dok_pl_content.getDokLainnya();
			if (!CommonUtil.isEmpty(listDokLainnya)) {
				StringBuilder lainnyaTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : listDokLainnya) {
					lainnyaTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokPlDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				lainnyaTemplate.append("</ol>");
				params.put("dok_lainnya", lainnyaTemplate.toString());
			}
			List<BlobTable> sskk = new ArrayList<>();
			if(pl.lls_status.isDraft()){
				DokPersiapanPl dokPersiapan = paket.getDokPersiapan();
				if (dokPersiapan != null) {
					sskk = dokPersiapan.getDokSskkAttachment();	
				}
			}else{
				sskk = dok_pl_content.getDokSskk();
			}
			if (!CommonUtil.isEmpty(sskk)) {
				StringBuilder sskkTemplate = new StringBuilder("<ol id=\"files\" style=\"padding-left:15px;\">");
				for (BlobTable blob : sskk) {
					sskkTemplate.append("<li>").append(blob.getFileName()).append("<a href=\"").append(BasicCtr.LPSE_URL).append(blob.getDownloadUrl(DokPlDownloadHandler.class)).append("\" data-attach-file=\"true\"> [Download]</a></li>");
				}
				sskkTemplate.append("</ol>");
				params.put("dok_sskk", sskkTemplate.toString());
			}
		}
		params.put("ldk_modified", dok_pl_content.dll_ldk_updated);
		params.put("panitia", pl.getPanitia());
		params.put("pp", pl.getPp());
		params.put("ijinList", getSyaratIjinUsaha());
		params.put("syaratList", getSyaratKualifikasi());
		params.put("ijinBaruList", getSyaratIjinUsahaBaru());
		params.put("syaratAdminList", getSyaratKualifikasiAdministrasi());
		params.put("syaratTeknisList", getSyaratKualifikasiTeknis());
		params.put("syaratKeuanganList", getSyaratKualifikasiKeuangan());
		Template template = TemplateLoader.load("/nonlelang/dokumen/template/adendum.html");
		return template.render(params);
	}
	
	public boolean isSyaratKualifikasiBaru() {
		String jenis = StringUtils.join(JenisChecklist.CHECKLIST_KUALIFIKASI_BARU, ',');
		Integer versi = ChecklistPl.findCurrentVersi(dll_id, JenisChecklist.CHECKLIST_LDK);
		Long countChecklist = ChecklistPl.count("dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis=0)", dll_id, versi);
		Long countCheclistBaru = ChecklistPl.count("dll_id=? AND chk_versi=? and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis+"))", dll_id, versi);
		if (countChecklist.intValue() == 0 && countCheclistBaru.intValue() == 0)
			return true;
		else if (countCheclistBaru.intValue() > 0)
			return true;
		else
			return false;
	}

}
