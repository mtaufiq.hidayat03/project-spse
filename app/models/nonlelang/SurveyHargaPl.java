package models.nonlelang;

import models.common.UploadInfo;
import models.handler.SurveyHargaPlDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.FileUpload;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Created by rayhanfrds on 2/9/17.
 */
@Table(name = "EKONTRAK.SURVEY_HARGA_PL")
public class SurveyHargaPl extends BaseModel {

    @Id(sequence = "ekontrak.seq_survey_harga_pl", function = "nextsequence")
    public Long survey_id;


    public String keterangan_survey;

    @Required(message = "File wajib diisi")
    public Long survey_id_attachment;

    //relasi ke Pl_seleksi
    @Required
    public Long lls_id;

    public Long pkt_id;

    @Transient
    private Pl_seleksi pl_seleksi;

    public Pl_seleksi getPl_seleksi() {
        if (pl_seleksi == null)
            pl_seleksi = Pl_seleksi.findById(lls_id);
        return pl_seleksi;
    }

    @Transient
    public BlobTable getBlob() {
        if (survey_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(survey_id_attachment);
    }

    public static SurveyHargaPl findBy(Long lelangId) {
        return find("lls_id=?", lelangId).first();
    }

    public static List<SurveyHargaPl> findAllBy(Long lelangId) {
        return find("lls_id=?", lelangId).fetch();
    }

    public static void simpanSurveyHargaPl(Long lelangId, List<Long> survey_id, List<String> keterangan_survey, List<FileUpload> surveyAtt) throws Exception {
        StringBuilder param = new StringBuilder();
        SurveyHargaPl obj = null;
        int i = 0;

        //hapus list yang yang memiliki attribut "" dan null
        keterangan_survey.removeAll(Collections.singleton(null));
        keterangan_survey.removeAll(Collections.singleton(""));

        surveyAtt.removeAll(Collections.singleton(null));
        surveyAtt.removeAll(Collections.singleton(""));

        for (String keterangan : keterangan_survey) {
            if (keterangan == null) {
                i++;
                continue;
            }

            Long survey = survey_id.get(i);
            if (survey != null) {
                obj = SurveyHargaPl.find("lls_id=? and survey_id=?", lelangId, survey).first();
            } else {
                obj = new SurveyHargaPl();
            }

            obj.lls_id = lelangId;
            obj.keterangan_survey = keterangan;

            if (surveyAtt.size() > 0) {
                FileUpload surveyAttachment = surveyAtt.get(i);
                if (surveyAttachment != null) {
                    File fSurveyAtt = surveyAttachment.asFile();
                    BlobTable bt = null;
                    if (obj.survey_id_attachment == null) {
                        bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, fSurveyAtt);
                    } else {
                        bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, fSurveyAtt, obj.survey_id_attachment);
                    }
                    obj.survey_id_attachment = bt.blb_id_content;
                }
            }
            obj.save();

            i++;
            if (param.length() > 0)
                param.append(',');
            param.append(obj.survey_id);
        }
        if (param.length() > 0) {
            SurveyHargaPl.delete("lls_id=? AND survey_id NOT IN (" + param + ')', lelangId);
        }
    }

    @Transient
    public String getNameFromBlobTable(BlobTable d) {
        String name = "no name";
        try {
            String[] ls = BaseModel.decrypt(d.blb_path).split("\\n");
            name = ls[2];
        } catch (Exception e) {
        }
        return name;
    }

    /**
     * @param blob
     * @return
     */
    @Transient
    public String getDownloadUrl(BlobTable blob) {

        return blob.getDownloadUrl(SurveyHargaPlDownloadHandler.class);
    }

    @Transient
    public List<BlobTable> getDokumen() {
        if(survey_id_attachment == null)
            return null;
        return BlobTableDao.listById(survey_id_attachment);
    }

    public static UploadInfo simpanAttachSurvey(Long id, File file) throws Exception {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        SurveyHargaPl survey = SurveyHargaPl.find("lls_id=?" , id).first();
        if(survey == null){
            survey = new SurveyHargaPl();
            survey.lls_id = id;
            survey.pkt_id = pl.pkt_id;
        }
        BlobTable blob;
        if (file != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, survey.survey_id_attachment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }
        survey.survey_id_attachment = blob.blb_id_content;
        survey.save();

        // untuk keperluan response page yang memakai ajax
        return UploadInfo.findBy(blob);
    }

    public static SurveyHargaPl findByIdAttachement(Long survey_id_attachment) {
        return find("survey_id_attachment=? ", survey_id_attachment).first();
    }

    public static SurveyHargaPl findByBlob(Long llsId){
        return find("lls_id = ?", llsId).first();
    }

    public static SurveyHargaPl findLastByPaket(Long pkt_id) {
        return find("pkt_id=?", pkt_id).first();
    }

    public String sanitizedFilename(File file) {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;
    }
}
