package models.nonlelang;

import controllers.BasicCtr;
import models.agency.Anggota_panitia;
import models.agency.Panitia;
import models.agency.Pp;
import models.common.Active_user;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name="EKONTRAK.PERSETUJUAN")
public class PersetujuanPl extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisPersetujuanPl {
		PENGUMUMAN_LELANG(0,"pengumuman") ,
		PEMENANG_LELANG(1,"pemenang"),
		PEMENANG_TEKNIS(2,"pemenang_teknis"),
		BATAL_LELANG(3,"tutup"),
		ULANG_LELANG(4,"ulang");

		public final Integer id;
		public final String key;

		JenisPersetujuanPl(int id, String key){
			this.id = id;
			this.key = key;
		}

		public boolean isBatal(){
			return this == BATAL_LELANG;
		}

		public Integer getId(){
			return id;
		}

		public String getKey(){
			return key;
		}

		public static JenisPersetujuanPl getByKey(String key){
			for(JenisPersetujuanPl e : values()) {
				if(e.key.equals(key)) return e;
			}

			return null;
		}
	}
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusPersetujuan {
		BELUM_SETUJU, SETUJU, TIDAK_SETUJU;
		
		public boolean isSetuju() {
			return this == SETUJU;
		}
		
		public boolean isTidakSetuju() {
			return this == TIDAK_SETUJU;
		}
		
		public boolean isBelumSetuju() {
			return this == BELUM_SETUJU;
		}
	}
	

	@Id(sequence="ekontrak.seq_persetujuan", function="nextsequence")
	public Long pst_id;
	
	//relasi ke lelang_selesai
	public Long lls_id;
	
	//relasi ke Pegawai
	public Long peg_id;

	/*
	 * jenis persetujuan memakai enum JenisPersetujuanPl
	 * 0 : pengumuman lelang
	 * 1 : pengumuman pemenang
	 */
	public JenisPersetujuanPl pst_jenis;
	
	public Date pst_tgl_setuju;
	
	public StatusPersetujuan pst_status;
	
	public String pst_alasan;
	
	/*@Transient
	private Lelang_seleksi lelang_seleksi;	

    public Lelang_seleksi getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}
    
    @Transient
	private Pegawai pegawai;

	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}*/
	
	public static class PersetujuanLelangPl {
		public Long pst_id;		
		//relasi ke lelang_selesai
		public Long lls_id;		
		public JenisPersetujuanPl pst_jenis;
		public Date pst_tgl_setuju;		
		public StatusPersetujuan pst_status;		
		public String pst_alasan;
		public String peg_nama;
		public String agp_jabatan;
		
		public String getAlasan() {
			if(StringUtils.isEmpty(pst_alasan))
				return "";
			return pst_alasan;
		}
		
		public String getJabatan() {
			if (agp_jabatan.equals("K")) {
				return Messages.get("nonlelang.ketua");
			} else if (agp_jabatan.equals("W")) {
				return Messages.get("nonlelang.wakil");
			} else if (agp_jabatan.equals("S")) {
				return Messages.get("nonlelang.sekretaris");
			} else {
				return Messages.get("nonlelang.anggota");
			}
		}
		
		public static List<PersetujuanLelangPl> findList(Long lelangId, JenisPersetujuanPl jenis) {
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, agp_jabatan, peg_nama FROM ekontrak.persetujuan p , (SELECT peg.peg_id,peg_nama,agp_jabatan FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id "+
						"LEFT JOIN anggota_panitia ap ON ap.pnt_id=pn.pnt_id LEFT JOIN pegawai peg ON peg.peg_id=ap.peg_id  WHERE lls_id=? AND ap.peg_id=peg.peg_id) a WHERE p.peg_id=a.peg_id AND lls_id=? AND pst_jenis=?", PersetujuanLelangPl.class, lelangId, lelangId, jenis).fetch();
		}

		public static List<PersetujuanLelangPl> findListPp(Long lelangId, JenisPersetujuanPl jenis) {
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama FROM ekontrak.persetujuan p , (SELECT peg.peg_id,peg_nama FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id LEFT JOIN ekontrak.pp pn ON pn.pp_id=p.pp_id "+
					"LEFT JOIN pegawai peg ON peg.peg_id=pn.peg_id  WHERE lls_id=? AND pn.peg_id=peg.peg_id) a WHERE p.peg_id=a.peg_id AND lls_id=? AND pst_jenis=?", PersetujuanLelangPl.class, lelangId, lelangId, jenis).fetch();
		}

		public static List<PersetujuanLelangPl> findBatalList(Long lelangId) {
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama " +
							"FROM ekontrak.persetujuan p JOIN pegawai peg on p.peg_id = peg.peg_id WHERE lls_id=? AND pst_jenis in (?,?)",
					PersetujuanLelangPl.class, lelangId,JenisPersetujuanPl.BATAL_LELANG, JenisPersetujuanPl.ULANG_LELANG).fetch();
		}
		
		public static List<PersetujuanLelangPl> getPersetujuanLelangForSummary(Long lelangId){
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, agp_jabatan, peg_nama FROM ekontrak.persetujuan p , (SELECT peg.peg_id,peg_nama,agp_jabatan FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id "
					+ "LEFT JOIN anggota_panitia ap ON ap.pnt_id=pn.pnt_id LEFT JOIN pegawai peg ON peg.peg_id=ap.peg_id  WHERE lls_id=? AND ap.peg_id=peg.peg_id) a "
					+ "WHERE p.peg_id=a.peg_id AND lls_id=? AND pst_jenis=? and pst_tgl_setuju is not null", PersetujuanLelangPl.class, lelangId, lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG).fetch();
		}
		
		public static List<PersetujuanLelangPl> getPersetujuanNonLelangForSummary(Long lelangId){
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama, null as agp_jabatan FROM ekontrak.persetujuan p , (SELECT peg.peg_id,peg_nama,null as agp_jabatan FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id LEFT JOIN ekontrak.persetujuan per on per.lls_id=l.lls_id\r\n" + 
					"LEFT JOIN pegawai peg ON peg.peg_id=per.peg_id\r\n" + 
					"LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id WHERE l.lls_id=?) a\r\n" + 
					"WHERE p.peg_id=a.peg_id AND lls_id=? AND pst_jenis=? and pst_tgl_setuju is not null", PersetujuanLelangPl.class, lelangId, lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG).fetch();
		}
	}
	
	public String getAlasan() {
		if(StringUtils.isEmpty(pst_alasan))
			return "";
		return pst_alasan;
	}

	public static PersetujuanPl findByPegawaiPengumuman(Long pegawaiId, Long lelangId) {
        return find("peg_id=? and lls_id=? and pst_jenis=?", pegawaiId, lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG).first();
    }

	/**
	 * Mendapatkan Seluruh informasi Persetujuan Pembatalan
	 * @param lls_id
	 * @return
	 */
	public static List<PersetujuanPl> findByLelangPembatalan(Long lls_id) {
		return PersetujuanPl.find("lls_id=? and pst_jenis in (?,?)" , lls_id, JenisPersetujuanPl.BATAL_LELANG, JenisPersetujuanPl.ULANG_LELANG).fetch();
	}

	public static List<PersetujuanPl> findByLelangPersetujuanPemenang(Long lelangId) {
		return find("lls_id=? and pst_jenis=?", lelangId, JenisPersetujuanPl.PEMENANG_LELANG).fetch();
	}

    public static PersetujuanPl findByPegawaiPenetapanAkhir(Long pegawaiId, Long lelangId) {
        return find("peg_id=? and lls_id=? and pst_jenis=?", pegawaiId, lelangId, JenisPersetujuanPl.PEMENANG_LELANG).first();
    }
	
	public static boolean isSedangPersetujuan(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, JenisPersetujuanPl.PENGUMUMAN_LELANG);
		return count.intValue() > 0;
	}
	
	public static boolean isSedangPersetujuanPemenang(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, JenisPersetujuanPl.PEMENANG_LELANG);
		return count.intValue() > 0;
	}

	public boolean isSedangPersetujuan() {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lls_id, StatusPersetujuan.SETUJU, StatusPersetujuan.TIDAK_SETUJU, pst_jenis);
		return count.intValue() > 0;
	}
	
	public static void checkPersetujuanLelang(Long lelangId) {
		List<PersetujuanPl> persetujuanList = find("lls_id=? and pst_jenis=?", lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG).fetch();
		Panitia panitia = Panitia.findByPenunjukanLangsung(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanPl persetujuan = null;
			persetujuanList = new ArrayList<PersetujuanPl>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, JenisPersetujuanPl.PENGUMUMAN_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanPl();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = JenisPersetujuanPl.PENGUMUMAN_LELANG;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = str_anggota.toString();
			sql_append = sql_append.replace(",)", ")");
			PersetujuanPl.delete("lls_id=? and pst_jenis=? and "+sql_append, lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG);
		}		
	}

	public static void checkPersetujuanPp(Long lelangId) {
		List<PersetujuanPl> persetujuanList = find("lls_id=? and pst_jenis=?", lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG).fetch();
		Pp pp = Pp.findByLelang(lelangId);
//		Panitia panitia = Panitia.findByPenunjukanLangsung(lelangId);
		Long jumlah = Pp.count("pp_id=?", pp.pp_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Pp> anggotaList = Pp.find("pp_id=?", pp.pp_id).fetch();
			PersetujuanPl persetujuan = null;
			persetujuanList = new ArrayList<PersetujuanPl>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Pp anggota:anggotaList) {
				persetujuan = PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, JenisPersetujuanPl.PENGUMUMAN_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanPl();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = JenisPersetujuanPl.PENGUMUMAN_LELANG;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = str_anggota.toString();
			sql_append = sql_append.replace(",)", ")");
			PersetujuanPl.delete("lls_id=? and pst_jenis=? and "+sql_append, lelangId, JenisPersetujuanPl.PENGUMUMAN_LELANG);
		}
	}
	
	public static void checkPersetujuanPemenang(Long lelangId) {
		List<PersetujuanPl> persetujuanList = find("lls_id=? and pst_jenis=?", lelangId, JenisPersetujuanPl.PEMENANG_LELANG).fetch();
		Panitia panitia = Panitia.findByPenunjukanLangsung(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanPl persetujuan = null;
			persetujuanList = new ArrayList<PersetujuanPl>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, JenisPersetujuanPl.PEMENANG_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanPl();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = JenisPersetujuanPl.PEMENANG_LELANG;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = anggotaList.isEmpty() ? "" : " and " + str_anggota;
			sql_append = sql_append.replace(",)", ")");
			PersetujuanPl.delete("lls_id=? and pst_jenis=?"+sql_append, lelangId, JenisPersetujuanPl.PEMENANG_LELANG);
		}		
	}

	public static void checkPersetujuanPemenangPp(Long lelangId) {
		List<PersetujuanPl> persetujuanList = find("lls_id=? and pst_jenis=?", lelangId, JenisPersetujuanPl.PEMENANG_LELANG).fetch();
		Panitia panitia = Panitia.findByPenunjukanLangsung(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanPl persetujuan = null;
			persetujuanList = new ArrayList<PersetujuanPl>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, JenisPersetujuanPl.PEMENANG_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanPl();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = JenisPersetujuanPl.PEMENANG_LELANG;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = anggotaList.isEmpty() ? "" : " and " + str_anggota;
			sql_append = sql_append.replace(",)", ")");
			PersetujuanPl.delete("lls_id=? and pst_jenis=?"+sql_append, lelangId, JenisPersetujuanPl.PEMENANG_LELANG);
		}
	}

	public static void checkPersetujuanBatalLelang(Long lelangId, JenisPersetujuanPl jenis) {
		List<PersetujuanPl> persetujuanList = find("lls_id=? and (pst_jenis=? or pst_jenis=?)",
				lelangId, JenisPersetujuanPl.BATAL_LELANG, JenisPersetujuanPl.ULANG_LELANG).fetch();

		Panitia panitia = Panitia.findByPenunjukanLangsung(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanPl persetujuan = null;
			persetujuanList = new ArrayList<>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, JenisPersetujuanPl.BATAL_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanPl();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = jenis;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = str_anggota.toString();
			sql_append = sql_append.replace(",)", ")");
			PersetujuanPl.delete("lls_id=? and pst_jenis=? and "+sql_append, lelangId, JenisPersetujuanPl.BATAL_LELANG);
		}
	}

	public static void checkPersetujuanBatalPp(Long lelangId, JenisPersetujuanPl jenis) {
		List<PersetujuanPl> persetujuanList = find("lls_id=? and (pst_jenis=? or pst_jenis=?)",
				lelangId, JenisPersetujuanPl.BATAL_LELANG, JenisPersetujuanPl.ULANG_LELANG).fetch();

		Panitia panitia = Panitia.findByPenunjukanLangsung(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanPl persetujuan = null;
			persetujuanList = new ArrayList<>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanPl.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, JenisPersetujuanPl.BATAL_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanPl();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = jenis;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = str_anggota.toString();
			sql_append = sql_append.replace(",)", ")");
			PersetujuanPl.delete("lls_id=? and pst_jenis=? and "+sql_append, lelangId, JenisPersetujuanPl.BATAL_LELANG);
		}
	}

	/**
	 * collective collegial berdasarkan perpres
	 * harus terpenuhi 50% + 1
	 *
	 * @param lelangId
	 * @param jenis
     * @return
     */
	public static boolean isApprove(Long lelangId, JenisPersetujuanPl jenis) {
		Long count = PersetujuanPl.count("lls_id=? and pst_jenis=?", lelangId, jenis);
		Long countSetuju = PersetujuanPl.count("lls_id=? and pst_status=? and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, jenis);
		int syaratSetuju = (count.intValue() / 2) + 1; // syarat sesuai aturan perpres (setengah + 1)
		return countSetuju >= syaratSetuju;
	}

	public boolean isApprove(){
		return PersetujuanPl.isApprove(lls_id,pst_jenis);
	}

	/**
	 * jika belum ada persetujuan kroscek shortlist ke SIKaP
	 *
	 * @param jenis
	 * @param lelangId
	 * */
	public static boolean allowSendMailSikap(Long lelangId, JenisPersetujuanPl jenis) {
		Long countSetuju = PersetujuanPl.count("lls_id=? and pst_status=? and pst_jenis=?", lelangId, StatusPersetujuan.SETUJU, jenis);
		return countSetuju == 0;
	}

	public static List<PersetujuanPl> getByLelangAndSetuju(Long lelangId){
		List<PersetujuanPl> persetujuanList = find("pst_id in (SELECT " +
				"pst_id FROM ekontrak.persetujuan WHERE lls_id = ? AND pst_status = ? AND pst_jenis=?)"
				, lelangId, StatusPersetujuan.SETUJU, JenisPersetujuanPl.PEMENANG_LELANG)
				.fetch();

		return persetujuanList;
	}

	public static void deleteAllByLelangAndJenisPengumuman(Long llsId){
		delete("lls_id = ? and pst_jenis = ?",llsId,JenisPersetujuanPl.PENGUMUMAN_LELANG);
	}

	public static void deleteAllByLelangAndJenisPemenang(Long llsId){
		delete("lls_id = ? and pst_jenis = ?",llsId,JenisPersetujuanPl.PEMENANG_LELANG);
	}

	public void deleteAllByLelangAndJenis(){
		delete("lls_id = ? and pst_jenis = ?",lls_id,pst_jenis);
	}

	public static PersetujuanPl findByPegawaiLelangInJenisBatal(Long lls_id) {
		return PersetujuanPl.find("peg_id=? and lls_id=? and pst_jenis in (?,?)", Active_user.current().pegawaiId,
				lls_id, JenisPersetujuanPl.BATAL_LELANG, JenisPersetujuanPl.ULANG_LELANG).first();
	}

	public boolean allowPersetujuanBatal(){
		Long count = PersetujuanPl.count("lls_id=? and pst_status =? and pst_jenis=? and peg_id=?",
				lls_id, StatusPersetujuan.BELUM_SETUJU, pst_jenis, Active_user.current().pegawaiId);

		return count > 0 && !isApprove(lls_id,pst_jenis);
	}

	public static PersetujuanPl findByPegawaiLelangAndJenis(Long lls_id, JenisPersetujuanPl jenis){
		return PersetujuanPl.find("peg_id=? and lls_id=? and pst_jenis=?", Active_user.current().pegawaiId, lls_id, jenis).first();
	}

	public static void simpanPersetujuanBatalTender(Pl_seleksi pl, Boolean setuju, String alasan, JenisPersetujuanPl jenis){
		checkPersetujuanBatalLelang(pl.lls_id, jenis);
		PersetujuanPl persetujuan = findByPegawaiLelangAndJenis(pl.lls_id,jenis);
		if (!setuju && !CommonUtil.isEmpty(alasan)) {
			persetujuan.pst_alasan = alasan;
			persetujuan.pst_status = StatusPersetujuan.TIDAK_SETUJU; //jika ada alasan maka status tidak setuju
		}else if(setuju) {
			persetujuan.pst_alasan = null;
			persetujuan.pst_status = StatusPersetujuan.SETUJU;
		}

		persetujuan.lls_id = pl.lls_id;
		persetujuan.pst_tgl_setuju = BasicCtr.newDate();
		persetujuan.save();

		// tambahkan ke riwayat persetujuan
		Riwayat_PersetujuanPl.simpanRiwayatPersetujuan(persetujuan);
	}
}
