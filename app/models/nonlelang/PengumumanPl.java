package models.nonlelang;

import controllers.BasicCtr;
import models.common.Kategori;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Lambang on 2/21/2017.
 */
public class PengumumanPl implements Serializable {

    public Long lls_id;

    public String pkt_nama;

    public Double pkt_hps;

    public Date tgl_akhir_daftar; // jadwal akhir masa upload penawaran (jika pasca) dan upload dok_pra (jika prakualifikasi)

    public Kategori kgr_id;

    public Integer pkt_flag;

    public Integer mtd_pemilihan;


    /**
     * informasi lelang yang sedang tahap pengumuman
     * tahap pengumuman yang dimaksud adalah :Tahap.PENGUMUMAN_LELANG, Tahap.UMUM_PRAKUALIFIKASI, Tahap.AMBIL_DOKUMEN_PEMILIHAN
     * Tahap.AMBIL_DOKUMEN, Tahap.AMBIL_DOK_PRA
     * hasil query akan dicache selama 1 menit
     * @return
     */
    public static List<PengumumanPl> findAll() {
        List<PengumumanPl> list = Cache.get("pengumumanPl", List.class);
        if(CommonUtil.isEmpty(list)) {
            Date time = BasicCtr.newDate();
            QueryBuilder query = QueryBuilder.create("SELECT lls_id, pkt_nama, pkt_flag, pkt_hps, mtd_pemilihan, tgl_akhir_daftar, kgr_id")
                    .append("FROM ekontrak.lelang_query WHERE lls_status=1 AND lls_id IN (SELECT DISTINCT j.lls_id FROM ekontrak.jadwal j")
                    .append("WHERE j.lls_id=lls_id AND (j.thp_id IN ("+Tahap.UMUM_PRAKUALIFIKASI.id+","+Tahap.PEMASUKAN_PENAWARAN.id+" , "+Tahap.PEMBUKAAN_PENAWARAN.id+") AND j.dtj_tglawal <= ? AND j.dtj_tglakhir >= ?) ", time,time)
                    .append("OR (j.thp_id IN ("+Tahap.UMUM_PRAKUALIFIKASI.id+","+Tahap.PEMASUKAN_PENAWARAN.id+") AND j.dtj_tglakhir >= ?)) ORDER BY kgr_id ASC, lls_id DESC", time);

            list = Query.findList(query, PengumumanPl.class);
            if(list == null) //Cache is not allowing the null values.
                list = new ArrayList<>();
            Cache.safeSet("pengumumanPl", list, BasicCtr.DEFAULT_CACHE);
        }
        return list;
    }

    public static List<PengumumanPl> findByKategori(Kategori kategori) {
        List<PengumumanPl> list = findAll();
        List<PengumumanPl> result = new ArrayList<>();
        if (!CommonUtil.isEmpty(list)) { // organize map
            for (PengumumanPl item : list)
                if(item.kgr_id!= null && item.kgr_id.equals(kategori)) {
                    result.add(item);
                }
        }
        return result;
    }

}
