package models.nonlelang;

import ext.DateBinder;
import models.agency.DaftarKuantitas;
import models.agency.Paket_swakelola;
import models.agency.Pegawai;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Created by Lambang on 4/26/2017.
 */

@Table(name="EKONTRAK.DOK_SWAKELOLA")
public class Dok_swakelola extends BaseModel {

    @Id(sequence="ekontrak.seq_dok_swakelola", function="nextsequence")
    public Long dsw_id;

    //relasi ke Swakelola_seleksi
    public Long lls_id;

    @As(binder = DateBinder.class)
    public Date dsw_tanggal_bast;

    public Long dsw_bast_attachment;

    public String dsw_dkh;

    @Transient
    public DaftarKuantitas dkh;

    @Transient
    private Swakelola_seleksi swakelola_seleksi;

    public Swakelola_seleksi getSwakelola_seleksi() {
        if(swakelola_seleksi == null)
            swakelola_seleksi = Swakelola_seleksi.findById(lls_id);
        return swakelola_seleksi;
    }

    public String getAuditUser() {
        return Pegawai.findBy(audituser).peg_nama;
    }

    public static Dok_swakelola findNCreateBy(Long swakelolaId) {
        Dok_swakelola dok_swakelola = find("lls_id=?", swakelolaId).first();
        if(dok_swakelola == null) {
            dok_swakelola = new Dok_swakelola();
            dok_swakelola.lls_id = swakelolaId;
            dok_swakelola.save();
        }
        return dok_swakelola;
    }

    @Transient
    public DaftarKuantitas getRincianHPS() {
        String content = Query.find("SELECT dsw_dkh FROM ekontrak.dok_swakelola WHERE dsw_id=? AND dsw_dkh is not null ", String.class, dsw_id).first();
        if (!CommonUtil.isEmpty(content))
            return CommonUtil.fromJson(content, DaftarKuantitas.class);
        return null;
    }

    public void simpanHps(Swakelola_seleksi swakelola, String data, boolean fixed) throws Exception {
        Dok_swakelola dok_swakelola = Dok_swakelola.findNCreateBy(swakelola.lls_id);
        DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,fixed);
        Paket_swakelola paket = Paket_swakelola.findById(swakelola.swk_id);
        if (dk.total == null)
            throw new Exception(Messages.get("nonlelang.belum_isi_hps"));
        else if (dk.total > paket.pkt_pagu) {
            throw new Exception(Messages.get("nonlelang.total_lebih_pagu"));
        } else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket
            paket.pkt_hps = dk.total;
            paket.save();
            dok_swakelola.dkh = dk;
            dok_swakelola.save();
        }
    }

    public static Dok_swakelola findBy(Long swakelolaId) {
        return find("lls_id=?", swakelolaId).first();
    }

    public static boolean isEditable(Swakelola_seleksi swakelola) {
        if (swakelola.lls_status.isDraft())
            return true;
        else
            return false;

    }

    @Transient
    public BlobTable getBastBlob() {
        if(dsw_bast_attachment == null)
            return null;
        return BlobTableDao.getLastById(dsw_bast_attachment);
    }

    protected void postLoad() {
        if (!CommonUtil.isEmpty(dsw_dkh))
            dkh = CommonUtil.fromJson(dsw_dkh, DaftarKuantitas.class);

    }
    
    public void prePersist() {
        super.prePersist();
        if (dkh != null)
            dsw_dkh = CommonUtil.toJson(dkh);
    }

}
