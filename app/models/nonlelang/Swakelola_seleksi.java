package models.nonlelang;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.Paket_swakelola;
import models.agency.Pp;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.nonSpk.HistoryUbahTanggalSwakelola;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Model {@code Paket} merepresentasikan tabel paket pada database.
 *
 * @author wahid
 */
@Table(name = "EKONTRAK.SWAKELOLA_SELEKSI")
public class Swakelola_seleksi extends BaseModel {
	
	@Id(sequence = "ekontrak.seq_swakelola_seleksi", function = "nextsequence")
    public Long lls_id;

    public String lls_uraian_pekerjaan;

    public Date lls_dibuat_tanggal;

	@As(binder = DateBinder.class)
	public Date lls_tanggal_paket_mulai;
	
	@As(binder = DateBinder.class)
	public Date lls_tanggal_paket_selesai;

	public Integer lls_nilai_gabungan;

	public Long lls_jangka_waktu_pelaksanaan;

    @Required
    public StatusPl lls_status;

    //relasi ke Swakelola
	public Long swk_id;

	public Integer tipe_swakelola;
   
	@Transient
	private Paket_swakelola swakelola;

	@Transient
	private List<HistoryUbahTanggalSwakelola> historyUbahTanggalSwakelolaList;
	
	public Paket_swakelola getSwakelola() {
		if(swakelola == null) 
			swakelola = Paket_swakelola.findById(swk_id);
		return swakelola;
	}
	
	public Pp getPp(){
		return Query.find("SELECT p.* FROM ekontrak.pp p LEFT JOIN ekontrak.paket_swakelola pkt ON pkt.pp_id=p.pp_id WHERE pkt.swk_id=?", Pp.class, swk_id).first();
	}
	
	public String getNamaSwakelola() {
		return Query.find("select pkt_nama from ekontrak.paket_swakelola where swk_id=?", String.class, swk_id).first();
	}

	public static Swakelola_seleksi findByPaket(Long swktId){
		return find("swk_id=? order by lls_id DESC", swktId).first();
	}
	
	/**
	 * membuat lelang baru dengan nilai-nilai default
	 * event ini terjadi saat pembuatan paket
	 * @param paket
	 */
	public static void buatSwakelolaBaru(Paket_swakelola paket) {
//		long jumlahLelang = Query.count("SELECT count(lls_id) FROM ekontrak.swakelola_seleksi WHERE swk_id=?", paket.swk_id);
		Swakelola_seleksi swakelola_seleksi = Swakelola_seleksi.find("swk_id=?", paket.swk_id).first();
		if (swakelola_seleksi == null) { // berarti belum pernah ada lelang yang dibuat
			swakelola_seleksi = new Swakelola_seleksi();
			swakelola_seleksi.swk_id = paket.swk_id;
			swakelola_seleksi.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
			swakelola_seleksi.lls_status = StatusPl.AKTIF;
			swakelola_seleksi.save();
        }
    }

    /**
     * mendapatkan informasi status lelang , tanpa harus ambil seluruh Object lelang_seleksi
     *
     * @param lelangId
     * @return
     */
    public static StatusPl findStatusLelang(Long lelangId) {
        return Query.find("SELECT lls_status FROM ekontrak.swakelola_seleksi WHERE lls_id=?", StatusPl.class, lelangId).first();
    }
    
    public static final ResultSetHandler<String[]> resultsetPL = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[10];
			Long lelangId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			tmp[0] = lelangId.toString();
			tmp[1] = rs.getString("pkt_nama");
			tmp[2] = rs.getString("agc_nama");
			if(CommonUtil.isEmpty(tmp[2]))
				tmp[2] = rs.getString("nama");
			tmp[3] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
			tmp[4] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
//			tmp[5] = Metode.findById(rs.getInt("mtd_id")).label;
			tmp[5] = pemilihan.label;
//			tmp[7] = MetodeEvaluasi.findById(rs.getInt("mtd_evaluasi")).label;
			tmp[7] = Kategori.findById(rs.getInt("kgr_id")).getNama()  + " - TA " + rs.getString("anggaran");
			tmp[8] = pkt_flag.toString();
			return tmp;
		}
	};
    
    public static final ResultSetHandler<String[]> resultsetPLPanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			Long lelangId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			tmp[0] = lelangId.toString();
			tmp[1] = rs.getString("pkt_nama");
			tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
			tmp[3] = rs.getString("peserta");
			tmp[4] = pkt_flag.toString();
			tmp[5] = pemilihan.label;
			return tmp;	
		}
	};

    public static final ResultSetHandler<String[]> resultsetPlRekanan = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_nama");
            if (versi > 1)
                namaLelang += " (Swakelola Ulang)";
            tmp[0] = lelangId.toString();
            tmp[1] = namaLelang;
            tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[3] = pkt_flag.toString();
            tmp[4] = pemilihan.label;
            tmp[5] = pemilihan.getKey().toString();
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPlBaru = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            String[] tmp = new String[6];
            tmp[0] = rs.getString("lls_id");
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_hps"));
            tmp[3] = pemilihan.label;
            tmp[4] = rs.getString("pkt_flag");
            tmp[5] = pemilihan.getKey().toString();
            return tmp;
        }
    };

    @Transient
    public boolean isAllowDownloadBA() {
        /*Tahap tahap = null;
        if (getKategori().isBarang()) {
            tahap = Tahap.TANDATANGAN_SPK;
        } else {
            tahap = Tahap.TANDATANGAN_SMPK;
        }*/
        return Aktivitas.isTahapStarted(lls_id, Tahap.EVALUASI_PENAWARAN);
    }

	public static final ResultSetHandler<String[]> resultsetSwakelolaPpk = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			Long swkId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			tmp[0] = swkId.toString();
			tmp[1] = rs.getString("pkt_nama");
			tmp[2] = Paket_swakelola.StatusPaketSwakelola.fromValue(rs.getInt("pkt_status")).label;
			tmp[3] = FormatUtils.formatDateInd(rs.getDate("lls_dibuat_tanggal"));;
			tmp[4] = pkt_flag.toString();
			tmp[5] = rs.getInt("pkt_status") == Paket_swakelola.StatusPaketSwakelola.SELESAI_LELANG.id ? "true" : "false";
			return tmp;
		}
	};

	public List<JenisDokumenSwakelola> getJenisDokumen(){

		return JenisDokumenSwakelola.findBySwakelola(this.lls_id);

	}

	public List<JenisRealisasiSwakelola> getJenisRealisasi(){

		return JenisRealisasiSwakelola.findBySwakelola(this.lls_id);

	}

	public List<DokumenLainSwakelola> getInformasiLainnya(){

		return DokumenLainSwakelola.findBySwakelolaAndJenis(this.lls_id, DokumenLainSwakelola.JenisDokLainSwakelola.INFORMASI_LAINNYA.value);

	}

	public static List<Swakelola_seleksi> getBySwakelolaByLewatTanggalSelesai(){

		String date = FormatUtils.formatDateTimeSecond(ConfigurationDao.isProduction()? models.jcommon.util.DateUtil.newDate() : models.jcommon.util.DateUtil.newDate(0));

		return Swakelola_seleksi.find("lls_id in (SELECT lls.lls_id FROM ekontrak.swakelola_seleksi lls, ekontrak.paket_swakelola p " +
				"WHERE lls.swk_id = p.swk_id AND lls.lls_tanggal_paket_selesai < '"+date+"'::date AND p.pkt_status='1')").fetch();

	}

	public List<HistoryUbahTanggalSwakelola> getHistoryUbahTanggalSwakelolaList() {
		if(historyUbahTanggalSwakelolaList == null)
			historyUbahTanggalSwakelolaList = HistoryUbahTanggalSwakelola.find("lls_id=?", lls_id).fetch();

		return historyUbahTanggalSwakelolaList;
	}

	public static void hapusSwakelola(Long swk_id){
		Paket_swakelola paket = Paket_swakelola.findById(swk_id);
		if(paket.pkt_status.isDraft()){
			Swakelola_seleksi swakelola = Swakelola_seleksi.findByPaket(paket.swk_id);

			Query.update("DELETE FROM ekontrak.realisasi_non_penyedia_swakelola WHERE rsk_id IN (SELECT rsk_id FROM ekontrak.jenis_realisasi_swakelola WHERE lls_id = ?)",swakelola.lls_id);
			Query.update("DELETE FROM ekontrak.realisasi_penyedia_swakelola WHERE rsk_id IN (SELECT rsk_id FROM ekontrak.jenis_realisasi_swakelola WHERE lls_id = ?)",swakelola.lls_id);

			Query.update("DELETE FROM ekontrak.dok_swakelola WHERE lls_id = ?", swakelola.lls_id);
			Query.update("DELETE FROM ekontrak.jenis_realisasi_swakelola WHERE lls_id = ?", swakelola.lls_id);
			Query.update("DELETE FROM ekontrak.dokumen_lain_swakelola WHERE lls_id = ?", swakelola.lls_id);
			Query.update("DELETE FROM ekontrak.swakelola_seleksi WHERE lls_id = ?", swakelola.lls_id);

			Query.update("DELETE FROM ekontrak.paket_swakelola_lokasi WHERE swk_id = ?", swakelola.getSwakelola().swk_id);
			Query.update("DELETE FROM ekontrak.paket_anggaran_swakelola WHERE swk_id = ?", swakelola.getSwakelola().swk_id);
			Query.update("DELETE FROM ekontrak.paket_satker_swakelola WHERE swk_id = ?", swakelola.getSwakelola().swk_id);
			Query.update("DELETE FROM ekontrak.paket_swakelola WHERE swk_id = ?", swakelola.getSwakelola().swk_id);
		}

	}

	public static final ResultSetHandler<String[]> resultsetSwakelolaPublic = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[7];
			Long swkId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
//			boolean lelangV3 = pkt_flag < 2;
			tmp[0] = swkId.toString();
			tmp[1] = rs.getString("pkt_nama");
			tmp[2] = rs.getString("instansi");
			tmp[3] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_pagu"));;
			tmp[4] = pkt_flag.toString();
			tmp[5] = rs.getString("ang_tahun");
			tmp[6] = pkt_flag.toString();
			return tmp;
		}
	};


}
