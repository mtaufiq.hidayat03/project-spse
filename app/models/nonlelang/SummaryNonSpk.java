package models.nonlelang;

import ext.FormatUtils;
import models.nonlelang.nonSpk.Dok_non_spk;
import models.nonlelang.nonSpk.JenisRealisasiNonSpk;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 8/18/2017.
 */
public class SummaryNonSpk {

    static final Template TEMPLATE = TemplateLoader.load("/nonSpk/template-summary-non-spk.html");

    public static InputStream cetak(Long nonSpkId) {

        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(nonSpkId);

        Map<String, Object> model = new HashMap<String, Object>();

        model.put("nonSpk", nonSpk);

        model.put("listJenisRealisasi", nonSpk.getJenisRealisasi());

        Dok_non_spk dok_non_spk = Dok_non_spk.findBy(nonSpk.lls_id);

        model.put("dokNonSpk", dok_non_spk);

        model.put("informasiLainnya", nonSpk.getInformasiLainnya());

        List<DraftPesertaNonSpk> draftPesertaNonSpkList = DraftPesertaNonSpk.findWithLelang(nonSpk.lls_id);

        model.put("draftPesertaNonSpkList", draftPesertaNonSpkList);

        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah((JenisRealisasiNonSpk.getJumlahNilaiRealisasi(nonSpk.lls_id,null)));

        model.put("totalNilaiRealisasi", totalNilaiRealisasi);

        return HtmlUtil.generatePDF(TEMPLATE, model);

    }

}
