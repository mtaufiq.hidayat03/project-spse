package models.nonlelang;

import controllers.BasicCtr;
import ext.DatetimeBinder;
import models.agency.DaftarKuantitas;
import models.common.StatusPl;
import models.common.Tahap;
import models.common.UploadInfo;
import models.handler.DokPlDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.FileUtils;
import models.lelang.Dok_lelang_content;
import models.lelang.LDPContent;
import models.lelang.Lelang_seleksi;
import models.lelang.SskkContent;
import play.Play;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name = "ekontrak.dok_nonlelang_content")
public class Dok_pl_content extends BaseModel {

    public static final String DEFAULT_VERSI = Play.configuration.getProperty("play.sbd.versi");

    @Id
    public Long dll_id;

    @Id
    public Integer dll_versi;

    /**
     * versi SBD yang dipakai
     */
    public String dll_sbd_versi;

    /**
     * daftar kuantitas dan harga format : json json model
     * models.agency.Rincian_hps
     */
    public String dll_dkh;

    /**
     * LDP content format Json json model : models.common.LDPContent
     */
    public String dll_ldp;

	/**
	 * field ini untuk menyimpan Jenis Kontrak
	 */
	public Integer dll_kontrak_pembayaran;
	
    /**
     * field ini untuk menyimpan informasi dokumen spesifikasi dan teknis
     */
    public Long dll_spek;

    /**
     * Syarat-syarat khusus Kontrak (SSKK) format json model
     * models.lelang.SskkContent
     */
    public String dll_sskk;

    public Long dll_sskk_attachment;

    public boolean dll_modified;

    public boolean dll_syarat_updated;

    public boolean dll_ldk_updated;

    public Long dll_lainnya;

    /**
     * Ini berisi dokumen HTML hasil merge dengan template velocity
     * HTML ini di-generate setiap kali diklik 'cetak'. Bersamaan dengan itu di-generate
     * pula file PDF untuk di-download
     */
    public String dll_content_html;

    public Long dll_content_attachment;
    
    /**
	 * field ini untuk menyimpan nomor dan tanggal SDP
	 */
	public String dll_nomorSDP;
	
	@As(binder = DatetimeBinder.class)
	public Date dll_tglSDP;

    @Transient
    public LDPContent ldpcontent;

    @Transient
    public DaftarKuantitas dkh;

    @Transient
    public SskkContent sskk_content;

    protected void postLoad() {
        if (!CommonUtil.isEmpty(dll_ldp))
            ldpcontent = CommonUtil.fromJson(dll_ldp, LDPContent.class);
        if (!CommonUtil.isEmpty(dll_sskk))
            sskk_content = CommonUtil.fromJson(dll_sskk, SskkContent.class);
        if (!CommonUtil.isEmpty(dll_dkh))
            dkh = CommonUtil.fromJson(dll_dkh, DaftarKuantitas.class);

    }

    public void prePersist() {
        super.prePersist();
        if (ldpcontent != null)
            dll_ldp = CommonUtil.toJson(ldpcontent);
        if (sskk_content != null)
            dll_sskk = CommonUtil.toJson(sskk_content);
        if (dkh != null)
            dll_dkh = CommonUtil.toJson(dkh);
    }

    //dapatkan dok_lelang_content terakhir
    public static Dok_pl_content findBy(Long dok_lelang_id) {
        return find("dll_id=? order by dll_versi desc", dok_lelang_id).first();
    }

    public static Dok_pl_content findByLdp(Long dok_lelang_id) {
        return find("dll_id=? AND dll_ldp IS NOT NULL order by dll_versi desc", dok_lelang_id).first();
    }
    
    public static Dok_pl_content findBy(Long dok_pl_id, int versi) {
		return find("dll_id=? and dll_versi=?", dok_pl_id, versi).first();
	}

    @Transient
    // adendum jika versinya > 1, jika versi==1 maka harusny draft
    public boolean isAdendum() {
        return dll_versi > 1;
    }
    
    public boolean isAllowUpload() {
		return this.isAdendum() && dll_modified;
	}

    @Transient
    public List<BlobTable> getDokSpek() {
        if(dll_spek == null){
            if(isAdendum()){
                dll_spek = getSpekAdendum();
            }
        }

        if (dll_spek == null){
            return null;
        }
        return BlobTableDao.listById(dll_spek);
    }

    @Transient
    public List<BlobTable> getDokLainnya() {
        if(dll_lainnya == null) {
            if (isAdendum()) {
                dll_lainnya = getDokLainnyaAdendum();
            }
        }
        if(dll_lainnya == null) {
            return null;
        }
        return BlobTableDao.listById(dll_lainnya);
    }

    @Transient
    public List<BlobTable> getDokSskk() {
        if(dll_sskk_attachment == null){
            if(isAdendum()){
                dll_sskk_attachment = getSskkAttachmentAdendum();
            }
        }

        if(dll_sskk_attachment == null) {
            return null;
        }
        return BlobTableDao.listById(dll_sskk_attachment);
    }


    @Transient
    public Long getSskkAttachmentAdendum() {
        return Query.find("SELECT dll_sskk_attachment FROM ekontrak.dok_nonlelang_content WHERE dll_id=? AND dll_sskk_attachment is not null ORDER BY dll_versi DESC", Long.class, dll_id).first();
    }
    @Transient
    public Long getSpekAdendum() {
        return Query.find("SELECT dll_spek FROM ekontrak.dok_nonlelang_content WHERE dll_id=? and dll_spek is not null ORDER BY dll_versi DESC", Long.class, dll_id).first();
    }
    @Transient
    public Long getDokLainnyaAdendum() {
        return Query.find("SELECT dll_lainnya FROM ekontrak.dok_nonlelang_content WHERE dll_id=? and dll_lainnya is not null ORDER BY dll_versi DESC", Long.class, dll_id).first();
    }

    public BlobTable findByFilename (String filename){
        List<BlobTable> blobs = getDokSpek();
        if(CommonUtil.isEmpty(blobs)){
            LogUtil.debug("DokNonLelangContent", "total blob is empty or object null");
            return null;
        }
        for(BlobTable model : blobs){
            LogUtil.debug("DokNonLelangContent", filename + " contains " + model.blb_nama_file);
            if(model.blb_nama_file.equalsIgnoreCase(filename)){
                return model;
            }
        }
        return null;
    }

    public Long simpanBlobSpek(List<BlobTable> blobList) throws Exception{
//        if(CollectionUtils.isEmpty(blobList))
//            return null;
        Long newId = getNextSequenceValue("seq_epns");

        for(BlobTable blob : blobList){
            BlobTable newBlob = new BlobTable();
            newBlob.blb_id_content = newId;
            newBlob.blb_versi = blob.blb_versi;
            newBlob.blb_date_time = blob.blb_date_time;
            newBlob.blb_mirrored = blob.blb_mirrored;
            newBlob.blb_path = blob.blb_path;
            newBlob.blb_engine = blob.blb_engine;
            newBlob.save();

            if(newId == null){
                newId = newBlob.blb_id_content;
            }
        }
        return newId;
        }

    public String sanitizedFilename(File file){
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;

    }

    public UploadInfo simpanSpek(File file) throws Exception {
        // asep:replace file name, except number and letter space and dot
        String path = sanitizedFilename(file);
        File newFile = new File(path);
        BlobTable model = findByFilename(newFile.getName());
        if(model != null){
            LogUtil.debug("DokNonLelangContent", "filename's duplicated");
            return null;
        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dll_spek != null) {
            blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile, dll_spek);
        } else {
            blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dll_spek = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanSskkAttachment(File file) throws Exception{
        String path = FileUtils.sanitizedFilename(file);
        File newFile = new File(path);
        BlobTable model = findByFilename(newFile.getName());
        if(model != null){
            LogUtil.debug("dok_pl_content", "filename's duplicated");
            return null;
        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if(dll_sskk_attachment != null){
            blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile, dll_sskk_attachment);
        } else{
            blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dll_sskk_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

    /**
     * dapatkan dokumen pl, jika tidak ada maka akan dibuat baru
     *
     * @param dok_plId
     * @param
     * @return
     */
    public static Dok_pl_content findNCreateBy(Long dok_plId, Pl_seleksi pl) {
        boolean allowCreateAdendum = allowCreateAdendum(pl);
        Dok_pl_content dok_pl_content = find("dll_id=? order by dll_versi desc", dok_plId).first();
        if (dok_pl_content == null) {
            dok_pl_content = new Dok_pl_content();
            dok_pl_content.dll_id = dok_plId;
            dok_pl_content.dll_sbd_versi = DEFAULT_VERSI;
            dok_pl_content.dll_versi = Integer.valueOf(1);
            dok_pl_content.dll_modified = true;
            dok_pl_content.save();
        } else if (!dok_pl_content.dll_modified && pl.lls_status.isAktif() && allowCreateAdendum) {
            Integer versi = dok_pl_content.dll_versi;
            dok_pl_content = new Dok_pl_content();
            dok_pl_content.dll_id = dok_plId;
            dok_pl_content.dll_sbd_versi = DEFAULT_VERSI;
            dok_pl_content.dll_versi = versi + 1;
            dok_pl_content.dll_modified = true;
            dok_pl_content.save();
        }
        return dok_pl_content;
    }
    
    public static Dok_pl_content findNCreateByForSyarat(Long dok_lelangId, Pl_seleksi pl) {
        Dok_pl_content dok_pl_content = find("dll_id=? order by dll_versi desc", dok_lelangId).first();
        if(dok_pl_content == null && pl.lls_status.isDraft()) {
            dok_pl_content = new Dok_pl_content();
            dok_pl_content.dll_id = dok_lelangId;
            dok_pl_content.dll_sbd_versi = DEFAULT_VERSI;
            dok_pl_content.dll_versi = Integer.valueOf(1);
            dok_pl_content.dll_modified = true;
            dok_pl_content.dll_syarat_updated = true;
            dok_pl_content.save();
        } else if (!dok_pl_content.dll_modified && pl.lls_status.isAktif() && pl.getTahapStarted().isAllowAdendum()) {
            Integer versi = dok_pl_content.dll_versi;
            dok_pl_content = new Dok_pl_content();
            dok_pl_content.dll_id = dok_lelangId;
            dok_pl_content.dll_sbd_versi = DEFAULT_VERSI;
            dok_pl_content.dll_versi = versi + 1;
            dok_pl_content.dll_modified = true;
            dok_pl_content.dll_syarat_updated = true;
            dok_pl_content.save();
        }else {
            dok_pl_content.dll_syarat_updated = true;
            dok_pl_content.save();
        }
        return dok_pl_content;
    }

    public static Dok_pl_content findNCreateByForLdk(Long dok_lelangId, StatusPl status) {
        Dok_pl_content dok_pl_content = find("dll_id=? order by dll_versi desc", dok_lelangId).first();
        if(dok_pl_content == null && status.isDraft()) {
            dok_pl_content = new Dok_pl_content();
            dok_pl_content.dll_id = dok_lelangId;
            dok_pl_content.dll_sbd_versi = DEFAULT_VERSI;
            dok_pl_content.dll_versi = Integer.valueOf(1);
            dok_pl_content.dll_modified = true;
            dok_pl_content.dll_ldk_updated = true;
            dok_pl_content.save();
        }
        else if (!dok_pl_content.dll_modified && status.isAktif()) {
            Integer versi = dok_pl_content.dll_versi;
            dok_pl_content = new Dok_pl_content();
            dok_pl_content.dll_id = dok_lelangId;
            dok_pl_content.dll_sbd_versi = DEFAULT_VERSI;
            dok_pl_content.dll_versi = versi + 1;
            dok_pl_content.dll_modified = true;
            dok_pl_content.dll_ldk_updated = true;
            dok_pl_content.save();
        }else {
            dok_pl_content.dll_ldk_updated = true;
            dok_pl_content.save();
        }
        return dok_pl_content;
    }

    @Transient
    public BlobTable getDokumen() {
        if(dll_content_attachment == null)
            return null;
        return BlobTableDao.getLastById(dll_content_attachment);
    }

    @Transient
    public String getDownloadUrl() throws IllegalAccessException {
        return getDokumen().getDownloadUrl(DokPlDownloadHandler.class);
    }


    // pemeriksaan untuk pra
    public static boolean allowCreateAdendum(Pl_seleksi pl){
        boolean allowCreateAdendum = true;
//        if (lelang.isPrakualifikasi()) {
            Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(pl.lls_id, Tahap.PENGUMUMAN_HASIL_PRA);
            if (jadwal == null)
                jadwal = Jadwal_pl.findByLelangNTahap(pl.lls_id, Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK);
            if(jadwal != null && jadwal.dtj_tglawal != null	&& jadwal.dtj_tglawal.after(BasicCtr.newDate())) {
                // jika sebelum jadwal pengumuman hasil pra, maka dokumen lelang masih dianggap draft bukan adendum
                allowCreateAdendum = false;
            }
//        }
        return allowCreateAdendum;
    }

    public UploadInfo simpanLainnya(File file) throws Exception {
        // asep:replace file name, except number and letter space and dot
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        File newFile = new File(path);
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dll_spek != null) {
            blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile, dll_lainnya);
        } else {
            blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dll_lainnya = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

    public boolean isEmptyDokumen() {
        return dll_content_attachment == null || BlobTableDao.count("blb_id_content=?", dll_content_attachment) == 0;
    }

}
