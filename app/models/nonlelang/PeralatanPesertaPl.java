package models.nonlelang;

import models.common.Status_kepemilikan;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Peralatan;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;

/**
 * Created by Lambang on 2/22/2017.
 */
@Table(name="EKONTRAK.PERALATAN_PESERTA")
public class PeralatanPesertaPl extends BaseModel {

    //relasi Ke Peserta
    @Id
    public Long psr_id;

    @Id
    public Long id_alat_peserta;

    public String alt_jenis;

    public Integer alt_jumlah;

    public String alt_kapasitas;

    public String alt_merktipe;

    public String alt_thpembuatan;

    public Integer alt_kondisi;

    public String alt_lokasi;

    public String alt_kepemilikan;

    public Status_kepemilikan skp_id;

    public static PesertaPl getPeserta(Long pesertaId){
        return PesertaPl.find("psr_id=?", pesertaId).first();
    }

    public static void simpanPeralatanPeserta(List<Peralatan> list, Long pesertaId) throws Exception{
        delete("psr_id=?", pesertaId);
        if(!CommonUtil.isEmpty(list)) {
            PeralatanPesertaPl psr_alat = null;
            for(Peralatan alat: list) {
                psr_alat = new PeralatanPesertaPl();
                psr_alat.id_alat_peserta = alat.alt_id;
                psr_alat.psr_id = pesertaId;
                psr_alat.alt_jenis = alat.alt_jenis;
                psr_alat.alt_jumlah = alat.alt_jumlah;
                psr_alat.alt_kapasitas = alat.alt_kapasitas;
                psr_alat.alt_merktipe = alat.alt_merktipe;
                psr_alat.alt_thpembuatan = alat.alt_thpembuatan;
                psr_alat.alt_kondisi = alat.alt_kondisi;
                psr_alat.alt_lokasi = alat.alt_lokasi;
                psr_alat.alt_kepemilikan = alat.alt_kepemilikan;
                psr_alat.skp_id = alat.skp_id;
                psr_alat.save();
            }
        }
    }

}
