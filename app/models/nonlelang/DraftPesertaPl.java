package models.nonlelang;

import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 2/7/2017.
 */
@Table(name="EKONTRAK.DRAFT_PESERTA_NONLELANG")
public class DraftPesertaPl extends BaseModel {

    @Id(sequence="ekontrak.seq_draft_peserta_nonlelang", function="nextsequence")
    public Long dp_id;

    public Long rkn_id;

    public Long lls_id;

    public Long repo_id;

    public String dp_nama_rekanan;

    public String dp_email_rekanan;

    public String dp_telp_rekanan;

    public String dp_npwp_rekanan;

    public String dp_alamat_rekanan;

    @Transient
    public Rekanan getRekanan()
    {
        return Rekanan.findById(rkn_id);

    }

    public static List<DraftPesertaPl> findWithLelang(Long lelangId) {
        return find("lls_id=?",lelangId).fetch();
    }

    public static DraftPesertaPl findWithLelangAndRekanan(Long lelangId, Long rekananId) {
        return find("lls_id=? AND rkn_id=?",lelangId, rekananId).first();
    }

    public static boolean isSudahTerdaftarDiDraft(Long lelangId, Long rkn_id){

        DraftPesertaPl draftPesertaPl = DraftPesertaPl.findWithLelangAndRekanan(lelangId, rkn_id);

        return draftPesertaPl != null ? true : false;
    }

}


