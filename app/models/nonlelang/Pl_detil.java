package models.nonlelang;

import controllers.BasicCtr;
import models.agency.Anggaran;
import models.agency.Paket_pl;
import models.agency.Paket_pl_lokasi;
import models.common.*;
import models.jcommon.util.CommonUtil;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.lelang.Checklist_master;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.utils.HTML;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 2/13/2017.
 */
public class Pl_detil {

    public Long lls_id;
    public Integer mtd_id;
    public Integer lls_versi_lelang;
    public Integer lls_penetapan_pemenang;
    public StatusPl lls_status;
    public Long pkt_id;
    public Long rup_id;
    public Integer pkt_flag;
    public Integer kgr_id;
    public String pkt_nama;
    public Double pkt_pagu;
    public Double pkt_hps;
    public String agc_nama;
    public String nama; // nama instansi
    public String stk_nama;
    public Integer mtd_pemilihan;
    public Integer mtd_kualifikasi;
    public Integer mtd_evaluasi;
    public Integer psr_total;
    public String lls_keterangan;
    public String lls_ditutup_karena;
    public Integer lls_kontrak_pembayaran;
    public Integer lls_kontrak_tahun;
    public Integer lls_kontrak_sbd;
    public Double lls_bobot_teknis;
    public Double lls_bobot_biaya;
    public String kls_id;
    public Date lls_dibuat_tanggal;
    public String tahaps; // tahap sekarang
    public String nama_instansi;
    public String nama_satker;
    
    public Kategori getKategori() {
        return Kategori.findById(kgr_id);
    }

    public MetodePemilihanPenyedia getPemilihan() {
        return MetodePemilihanPenyedia.findById(mtd_pemilihan);
    }

    public MetodePemilihan getPemilihanpenyedia() {
        return MetodePemilihan.findById(mtd_pemilihan);
    }

    public MetodeEvaluasi getEvaluasi() {
        return MetodeEvaluasi.findById(mtd_evaluasi);
    }

    public JenisKontrak getKontrak_pembayaran() {
        return JenisKontrak.findById(lls_kontrak_pembayaran);
    }

    public JenisKontrak getKontrak_tahun() {
        return JenisKontrak.findById(lls_kontrak_tahun);
    }

    public JenisKontrak getKontrak_sbd() {
        return JenisKontrak.findById(lls_kontrak_sbd);
    }

    public Kualifikasi getKualifikasi() {
        if(kls_id == null)
            return null;
        return Kualifikasi.findById(kls_id);
    }

    public boolean isLelangUlang()
    {
        if(lls_versi_lelang == null)
            return false;
        return lls_versi_lelang > 1;
    }

    public boolean isItemized()
    {
        return lls_penetapan_pemenang != null && lls_penetapan_pemenang == 1;
    }

    public boolean isKonsultansi() {
        Kategori kategori = getKategori();
        return kategori.isKonsultansi() || kategori.isKonsultansiPerorangan();
    }
    
    public boolean isJkKonstruksi() {
        Kategori kategori = getKategori();
        return kategori != null && kategori.isJkKonstruksi();
    }

    public String getNamaInstansi() {
        if(CommonUtil.isEmpty(agc_nama))
            return nama;
        return agc_nama;
    }

    public String getAnggaran() {
        StringBuilder anggaran = new StringBuilder();
        //List<String> anggaranlist = Anggaran.getSumberAnggaranPaket(pkt_id);
        List<Anggaran> anggaranlist = Anggaran.findByPaketPl(pkt_id);
        if(CommonUtil.isEmpty(anggaranlist))
            return anggaran.toString();
//        for (String sumber_anggaran : anggaranlist) {
//            anggaran.append("<div>" + sumber_anggaran + "</div>");
//        }
        anggaran.append("<table class=\"table table-condensed table-bordered\">");
        anggaran.append("<tr><th>"+ Messages.get("ct.tahun") +"</th><th>"+Messages.get("ct.sbd")+"</th><th>"+Messages.get("ct.nilai")+"</th></tr>");
        for (Anggaran sumber_anggaran : anggaranlist) {
            anggaran.append("<tr><td>").append(sumber_anggaran.ang_tahun).append("</td>").append("<td>").append(sumber_anggaran.sbd_id.name()).append("</td>").append("<td>").append(sumber_anggaran.getNilai()).append("</td></tr>");
        }
        anggaran.append("</table>");
        return anggaran.toString();
    }


    public String getLokasiPaket() {

        List<Paket_pl_lokasi> listLokasi = Paket_pl_lokasi.find("pkt_id=?", pkt_id).fetch();

        StringBuilder lokasipaket = new StringBuilder();

        if(CommonUtil.isEmpty(listLokasi))
            return lokasipaket.toString();

        boolean first = true;

        for (Paket_pl_lokasi lokasi : listLokasi) {

            if (!CommonUtil.isEmpty(lokasi.pkt_lokasi)) {
                if(!first)
                    lokasipaket.append(',');

                lokasipaket.append(HTML.htmlEscape(lokasi.pkt_lokasi + " - " + lokasi.getKabupaten().kbp_nama));

                first = false;

            }

        }

        return lokasipaket.toString();

    }

    /**
     * return true if this package is created using new "perpres 16 2018"*/
    public boolean isFlag43() {
        return this.pkt_flag == 3;
    }
    
    public boolean isPrakualifikasi() {
    	return mtd_kualifikasi != null && mtd_kualifikasi == 0;
    }

    public boolean isLelangV3() {
        return pkt_flag < 2;
    }

    public String getTahapNow() {
        return Tahap.tahapInfo(tahaps, true, isLelangV3(), getPemilihan().isLelangExpress());
    }


    public static Pl_detil findById(Long lelangId) {
        Timestamp now = new Timestamp(BasicCtr.newDate().getTime());

        String sql = "SELECT  l.lls_id, l.mtd_kualifikasi, l.lls_penetapan_pemenang, l.lls_versi_lelang, l.lls_status, p.pkt_id, p.rup_id, p.pkt_flag, p.pkt_nama, p.pkt_pagu, p.pkt_hps, ag.agc_nama, s.stk_nama, p.kgr_id , p.kls_id,  l.mtd_pemilihan, mtd_evaluasi, lls_keterangan,lls_ditutup_karena, lls_kontrak_pembayaran, lls_dibuat_tanggal, "+
                "(select count(lls_id) from ekontrak.peserta_nonlelang where lls_id=l.lls_id) as psr_total, i.nama, ekontrak.tahap_now(l.lls_id, '"+now+"') AS tahaps, " +
                "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja sk, ekontrak.paket_satker ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id)) AS nama_instansi," +
                "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja sk, ekontrak.paket_satker ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id) AS nama_satker" +
                " FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id "+
                "LEFT JOIN agency ag ON s.agc_id=ag.agc_id LEFT JOIN instansi i ON s.instansi_id=i.id WHERE l.lls_id=?";

        return Query.find(sql, Pl_detil.class, lelangId).first();
    }

    public String getSyarat() {
        Dok_pl dok_pl = null;
        StringBuilder content = new StringBuilder();
        JenisDokPl jenis = JenisDokPl.DOKUMEN_LELANG;
		if(isPrakualifikasi())
			jenis = JenisDokPl.DOKUMEN_LELANG_PRA;
		dok_pl = Dok_pl.findBy(lls_id, jenis);
        List<ChecklistPl> ijinList =  dok_pl.getSyaratIjinUsaha();
        List<ChecklistPl> syaratList =  dok_pl.getSyaratKualifikasi();
        List<ChecklistPl> ijinBaruList = dok_pl.getSyaratIjinUsahaBaru();
		List<ChecklistPl> syaratAdminList = dok_pl.getSyaratKualifikasiAdministrasi();
		List<ChecklistPl> syaratTeknisList = dok_pl.getSyaratKualifikasiTeknis();
		List<ChecklistPl> syaratKeuanganList =  dok_pl.getSyaratKualifikasiKeuangan();
		if(!CollectionUtils.isEmpty(ijinList) || !CollectionUtils.isEmpty(syaratList)) {
			content.append("<table class=\"table table-sm\">");
			if(!CollectionUtils.isEmpty(ijinList)){
				content.append("<tr><th>Izin Usaha</th></tr>");
				content.append("<tr><td><table class=\"table table-sm\">").append("<tr><td>Jenis Izin</td><td>Klasifikasi</td></tr>");
				for(ChecklistPl ijin : ijinList) {
					content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
				}
				content.append("</table></td></tr>");
			}
			for(ChecklistPl syarat:syaratList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if(syarat.getJsonName() != null) {
					content.append("<b>").append(cm.ckm_nama).append("</b>");
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("<b>").append(syarat.chk_nama).append("</b>");					
				} else {
					content.append("<b>").append(cm.ckm_nama).append("</b>");
					if(!CommonUtil.isEmpty(syarat.chk_nama))
						content.append("<p>").append(syarat.chk_nama).append("</p>");
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		if (!CollectionUtils.isEmpty(ijinBaruList) || !CollectionUtils.isEmpty(syaratAdminList)) {
			content.append("<h5>Persyaratan Kualifikasi Administrasi/Legalitas</h5>");
			content.append("<table class=\"table table-sm\">");
			if(!CollectionUtils.isEmpty(ijinBaruList)){
				content.append("<table class=\"table table-sm table-bordered\" >");
				content.append("<thead>");
				content.append("<tr><th>Izin Usaha</th></tr>");
				content.append("</thead>");
				content.append("<tbody>");
				content.append("<tr><td><table class=\"table table-sm table-bordered\">").append("<tr><td>Jenis Izin</td><td>Bidang Usaha/Sub Bidang Usaha/Klasifikasi/Sub Klasifikasi</td></tr>");
				for(ChecklistPl ijin : ijinBaruList) {
					content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
				}
				content.append("</table></td></tr>");
				content.append("</tbody>");
			}
			for(ChecklistPl syarat:syaratAdminList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if(syarat.getJsonName() != null) {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("Syarat Kualifikasi Administrasi/Legalitas Lain<br/>");
					String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(syarat.chk_nama);
				} else {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);														
					if(!CommonUtil.isEmpty(syarat.chk_nama)) {
						String[] s = syarat.chk_nama.split("[\\r\\n]+");
						content.append("<i>");
						if (s != null) {
							for (String newline: s) {
								content.append(newline);
								content.append("<br/>");
							}
						} else 
							content.append(syarat.chk_nama);
						content.append("</i>");
					}
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		if (!CollectionUtils.isEmpty(syaratTeknisList)) {
			content.append("<h5>Persyaratan Kualifikasi Teknis</h5>");
			content.append("<table class=\"table table-sm\">");
			for(ChecklistPl syarat:syaratTeknisList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if (cm.isInputNumber()) {
					int i = 0;
					for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
						if (newline.split("number") == null || newline.split("number").length < 2) {
							content.append(newline);
						} else {
							int j = 0;
							for (String number: newline.split("number")) {
								content.append(number);
								for(Map<String, String> val : syarat.getJsonName()) {
									for(String key : table.key) {
										if (key.equals("number"+i) && j == 0) 
											content.append(val.get(key));
									}
								}
								j++;
							}
						}
						content.append("<br/>");
						i++;
					}
				}  else if (cm.isInputTextMulti()) {
					for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
						int j = 1;
						for (String text: newline.split("text")) {
							content.append(text);
							for(Map<String, String> val : syarat.getJsonName()) {
								for(String key : table.key) {
									if (key.equals("text"+j)) 
										content.append(val.get(key));
								}
							}
							j++;
						}
						content.append("<br/>");
					}
				} else if (syarat.getJsonName() != null) {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("Syarat Kualifikasi Teknis Lain<br/>");
					String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(syarat.chk_nama);
				} else {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					if(!CommonUtil.isEmpty(syarat.chk_nama)) {
						String[] s = syarat.chk_nama.split("[\\r\\n]+");
						content.append("<i>");
						if (s != null) {
							for (String newline: s) {
								content.append(newline);
								content.append("<br/>");
							}
						} else 
							content.append(syarat.chk_nama);
						content.append("</i>");
					}
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		if (!CollectionUtils.isEmpty(syaratKeuanganList)) {
			content.append("<h5>Persyaratan Kualifikasi Kemampuan Keuangan</h5>");
			content.append("<table class=\"table table-sm\">");
			for(ChecklistPl syarat:syaratKeuanganList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if(syarat.getJsonName() != null) {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("Syarat Kualifikasi Kemampuan Keuangan Lain<br/>");
					String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(syarat.chk_nama);
				} else {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					if(!CommonUtil.isEmpty(syarat.chk_nama)) {
						String[] s = syarat.chk_nama.split("[\\r\\n]+");
						content.append("<i>");
						if (s != null) {
							for (String newline: s) {
								content.append(newline);
								content.append("<br/>");
							}
						} else 
							content.append(syarat.chk_nama);
						content.append("</i>");
					}
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		return content.toString();
    }

}
