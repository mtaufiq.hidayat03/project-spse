package models.nonlelang.cast;

import ext.DecimalBinder;
import ext.RupiahBinder;
import models.nonlelang.NilaiEvaluasiPl;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Query;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Lambang on 8/18/2017.
 */
@SuppressWarnings("serial")
public class NilaiEvaluasiPlView implements Serializable {

    public Long eva_id;

    public Long psr_id;

    public Long rkn_id;

    public String rkn_nama;

    @As(binder = RupiahBinder.class)
    public Double nev_harga;

    @As(binder = RupiahBinder.class)
    public Double nev_harga_terkoreksi;

    @As(binder = RupiahBinder.class)
    public Double nev_harga_negosiasi;

    @As(binder = DecimalBinder.class)
    public Double nev_skor;

    public Integer nev_urutan;

    public String nev_uraian;

    public Integer is_pemenang_verif;

    public String psr_alasan_menang;

    public Double psr_harga;

    public Double psr_harga_terkoreksi;

    @Required
    public NilaiEvaluasiPl.StatusNilaiEvaluasi nev_lulus;

    public boolean isPemenangVerif() {
        if (is_pemenang_verif == null)
            return false;
        return is_pemenang_verif == Integer.valueOf(1);
    }

    public String getKeteranganHasil() {
        String retval = psr_alasan_menang;
        if (!isPemenangVerif())
            if (retval == null || retval.isEmpty())
                retval = "Tidak Dilakukan Verifikasi";
        return retval;
    }

    //TODO : clean code, avoid duplicate code
    public String calculatePrecentageHargaTerkoreksi(Double nilai_hps) {
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format((nev_harga_terkoreksi * 100) / nilai_hps);
    }

    public static List<NilaiEvaluasiPlView> findAllByEvaluasiWithUrutHarga(Long evaluasiId) {
        List<NilaiEvaluasiPlView> list = Query.find("SELECT nev.eva_id, nev.nev_id, nev.psr_id, nev_harga, nev_harga_terkoreksi," +
                " nev_skor, nev_urutan, nev_uraian, nev_lulus, rkn_nama, rkn.rkn_id, psr.is_pemenang_verif, psr.psr_alasan_menang," +
                " psr.psr_harga, psr.psr_harga_terkoreksi" +
                " FROM ekontrak.nilai_evaluasi nev INNER JOIN ekontrak.peserta_nonlelang psr ON psr.psr_id=nev.psr_id INNER JOIN rekanan rkn" +
                " ON rkn.rkn_id=psr.rkn_id WHERE eva_id = ? " +
                " ORDER BY nev_harga ASC, nev_urutan ASC", NilaiEvaluasiPlView.class, evaluasiId).fetch();
        return list;
    }

    public static List<NilaiEvaluasiPlView> findAllByEvaluasi(Long evaluasiId) {
        List<NilaiEvaluasiPlView> list = Query.find("SELECT nev.eva_id, nev.nev_id, nev.psr_id, nev_harga, nev_harga_terkoreksi, nev_harga_negosiasi, nev_skor, nev_urutan, nev_uraian, nev_lulus, rkn_nama, rkn.rkn_id, psr.is_pemenang_verif, psr.psr_alasan_menang, psr.psr_harga, psr.psr_harga_terkoreksi " +
                "FROM ekontrak.nilai_evaluasi nev INNER JOIN ekontrak.peserta_nonlelang psr ON psr.psr_id=nev.psr_id INNER JOIN rekanan rkn ON rkn.rkn_id=psr.rkn_id WHERE eva_id = ? " +
                "ORDER BY psr_id ASC, nev_urutan ASC", NilaiEvaluasiPlView.class, evaluasiId).fetch();
        return list;
    }

    public static List<NilaiEvaluasiPlView> findAllPemenangByEvaluasi(Long evaluasiId) {
        List<NilaiEvaluasiPlView> list = Query.find("SELECT nev.eva_id, nev.nev_id, nev.psr_id, nev_harga, nev_harga_terkoreksi," +
                " nev_skor, nev_urutan, nev_uraian, nev_lulus, rkn_nama, rkn.rkn_id, psr.is_pemenang_verif, psr.psr_alasan_menang," +
                " psr.psr_harga, psr.psr_harga_terkoreksi" +
                " FROM ekontrak.nilai_evaluasi nev INNER JOIN ekontrak.peserta_nonlelang psr ON psr.psr_id=nev.psr_id INNER JOIN rekanan rkn" +
                " ON rkn.rkn_id=psr.rkn_id WHERE eva_id = ?" +
                " ORDER BY nev_urutan ASC", NilaiEvaluasiPlView.class, evaluasiId).fetch();
        return list;
    }

    public Double getHargaFinal(){
        if(nev_harga_negosiasi != null){
            return nev_harga_negosiasi;
        }

        return nev_harga_terkoreksi;
    }

    public boolean isLulus() {
        if (nev_lulus != null) {
            return nev_lulus.isLulus();
        }
        return false;
    }

}
