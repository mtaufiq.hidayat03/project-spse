package models.nonlelang;

import ext.DatetimeBinder;
import models.common.AktivitasPl;
import models.common.MailQueueDao;
import models.common.MetodePemilihanPenyedia;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.workflow.ProcessInstancePl;
import models.nonlelang.workflow.WorkflowPlDao;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Validation;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name = "EKONTRAK.JADWAL")
public class Jadwal_pl extends BaseModel {

    @Id(sequence = "ekontrak.seq_jadwal", function = "nextsequence")
    public Long dtj_id;

    @As(binder = DatetimeBinder.class)
    public Date dtj_tglawal;

    @As(binder = DatetimeBinder.class)
    public Date dtj_tglakhir;

    public String dtj_keterangan;

    public Long lls_id;

    public Long akt_id;

    public Integer thp_id;

    @Transient
    private Pl_seleksi pl_seleksi;

    @Transient
    public Tahap akt_jenis;

    @Transient
    public int akt_urut;

    @Transient
    private List<HistoryJadwalPl> historyList;

    // custome result, untuk optimasi query
    public static final ResultSetHandler<Jadwal_pl> CUSTOME_RESULTSET = new ResultSetHandler<Jadwal_pl>() {
        @Override
        public Jadwal_pl handle(ResultSet rs) throws SQLException {
            Jadwal_pl jadwal_pl = new Jadwal_pl();
            jadwal_pl.dtj_id = rs.getLong("dtj_id");
            jadwal_pl.dtj_tglawal = rs.getTimestamp("dtj_tglawal");
            jadwal_pl.dtj_tglakhir = rs.getTimestamp("dtj_tglakhir");
            jadwal_pl.dtj_keterangan = rs.getString("dtj_keterangan");
            jadwal_pl.lls_id = rs.getLong("lls_id");
            jadwal_pl.thp_id = rs.getInt("thp_id");
            jadwal_pl.audittype = rs.getString("audittype");
            jadwal_pl.audituser = rs.getString("audituser");
            jadwal_pl.auditupdate = rs.getTimestamp("auditupdate");
            jadwal_pl.akt_id = rs.getLong("akt_id");
            jadwal_pl.akt_jenis = Tahap.valueOf(rs.getString("akt_jenis"));
            jadwal_pl.akt_urut = rs.getInt("akt_urut");
            return jadwal_pl;
        }
    };

    public static Long countJadwalkosong(Long plId) {
        return Jadwal_pl.count("lls_id=? and (dtj_tglawal is null or dtj_tglakhir is null)", plId);
    }

    public static List<Jadwal_pl> findByPl(Long plId) {
        String sql = "select j.*, akt_jenis, akt_urut from ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id= ? order by akt_urut asc";
        return Query.find(sql, CUSTOME_RESULTSET, plId).fetch();
    }

    @Transient
    public String namaTahap() {
        if (akt_jenis == null)
            return "";
        else if (Tahap.tahapanBA.contains(akt_jenis)) { //label tahap berita Acara Harus disesuaikan
            return akt_jenis.getLabel().replace("Upload", "Pembuatan");
        }
        return akt_jenis.getLabel();
    }

    /**
     * Dapatkan jadwal pl, jika saat ini ada lebih dari satu yang aktif, tampilkan  [...]
     *
     * @return
     */
    public static String getJadwalSekarang(Long lelangId, boolean shortVersion, boolean pl, boolean lelangV3) {
        Date currentDate = controllers.BasicCtr.newDate();
        String tahaps = Query.find("SELECT ekontrak.tahap_now(?, ?)", String.class, lelangId, currentDate).first();
        return Tahap.tahapInfo(tahaps, shortVersion, lelangV3, pl);
    }

    public static Jadwal_pl findByLelangNTahap(Long lelangId, Tahap tahap) {
        List<Jadwal_pl> list = findByLelangNTahap(lelangId, new Tahap[]{tahap});
        if (CommonUtil.isEmpty(list))
            return null;
        return list.get(0);
    }

    public static List<Jadwal_pl> getJadwalkosong(Long lelangId, MetodePemilihanPenyedia pemilihan, boolean pascakualifikasi) {
        List<Jadwal_pl> list = new ArrayList<Jadwal_pl>();
        List<AktivitasPl> aktivitasList = pascakualifikasi ? AktivitasPl.AKTIVITAS_PENGADAAN_LANGSUNG : AktivitasPl.findByPl(pemilihan.id);
        Jadwal_pl jadwal_pl = null;
        for (AktivitasPl aktivitas : aktivitasList) {
            jadwal_pl = Jadwal_pl.find("akt_id=? and lls_id=?", aktivitas.akt_id, lelangId).first();
            if (jadwal_pl == null)
                jadwal_pl = new Jadwal_pl();
            jadwal_pl.lls_id = lelangId;
            jadwal_pl.akt_id = aktivitas.akt_id;
            jadwal_pl.thp_id = aktivitas.akt_jenis.id;
            jadwal_pl.akt_jenis = aktivitas.akt_jenis;
            jadwal_pl.akt_urut = aktivitas.akt_urut;
            jadwal_pl.save();
            list.add(jadwal_pl);
        }
        return list;
    }
    
    public static List<Jadwal_pl> getJadwalkosongNew(Long lelangId, MetodePemilihanPenyedia pemilihan, boolean prakualifikasi) {
        List<Jadwal_pl> list = new ArrayList<Jadwal_pl>();
        List<AktivitasPl> aktivitasList = prakualifikasi && prakualifikasi ? AktivitasPl.AKTIVITAS_PENUNJUKAN_LANGSUNG : AktivitasPl.findByPl(pemilihan.id);
        Jadwal_pl jadwal_pl = null;
        for (AktivitasPl aktivitas : aktivitasList) {
            jadwal_pl = Jadwal_pl.find("akt_id=? and lls_id=?", aktivitas.akt_id, lelangId).first();
            if (jadwal_pl == null)
                jadwal_pl = new Jadwal_pl();
            jadwal_pl.lls_id = lelangId;
            jadwal_pl.akt_id = aktivitas.akt_id;
            jadwal_pl.thp_id = aktivitas.akt_jenis.id;
            jadwal_pl.akt_jenis = aktivitas.akt_jenis;
            jadwal_pl.akt_urut = aktivitas.akt_urut;
            jadwal_pl.save();
            list.add(jadwal_pl);
        }
        return list;
    }

    public static List<Jadwal_pl> findByLelangNTahap(Long lelangId, Tahap... tahapList) {
        StringBuilder sql = new StringBuilder("select j.*, akt_jenis, akt_urut from ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id=?");
        if (tahapList != null && tahapList.length > 0) {
            sql.append("AND a.akt_jenis in (");
            int i = 0;
            for (Tahap tahap : tahapList) {
                sql.append('\'').append(tahap).append('\'');
                if (i < (tahapList.length - 2))
                    sql.append(',');
            }
            sql.append(')');
        }
        return Query.find(sql.toString(), CUSTOME_RESULTSET, lelangId).fetch();
    }

    public static String periodToString(Period p) {
        if (p == null)
            return null;
        p = p.normalizedStandard();
        StringBuilder str = new StringBuilder();
        if (p.getMonths() > 0)
            str.append(p.getWeeks()).append(Messages.get("nonlelang.mounth"));
        if (p.getWeeks() > 0)
            str.append(p.getWeeks()).append(Messages.get("nonlelang.minggu"));
        if (p.getDays() > 0)
            str.append(p.getDays()).append(Messages.get("nonlelang.hari"));
        if (p.getHours() > 0)
            str.append(p.getHours()).append(Messages.get("nonlelang.jam"));
        if (p.getMinutes() > 0)
            str.append(p.getMinutes()).append(Messages.get("nonlelang.menit"));
        return str.toString();
    }

    public String getPeriodAsString() {
        Period period = null;
        if (dtj_tglawal != null & dtj_tglakhir != null)
            period = new Period(dtj_tglakhir.getTime() - dtj_tglawal.getTime());
        if (period == null)
            return "";
        else
            return periodToString(period);
    }

    public static void simpanJadwal(Pl_seleksi lelangPl, List<Jadwal_pl> jadwalList, Date currentDate, String alasan, int validasiUrutan) throws Exception {
        boolean send_email = false;
        List<Jadwal_pl> jadwalOri = Jadwal_pl.findByLelang(lelangPl.lls_id);
        List<Jadwal_pl> jadwalBefore = Jadwal_pl.findByLelang(lelangPl.lls_id);
        //semua jadwal akan diperiksa lebih dulu valid atau tidak, kemudian akan disimpan
        Jadwal_pl jadwal = null, jadwalO = null, before = null, next = null;
        String fieldName = null;
        for (int i = 0; i < jadwalList.size(); i++) {
            jadwal = jadwalList.get(i);
            jadwalO = jadwalOri.get(i);
//            jadwal.akt_id = jadwalO.akt_id;
            if(jadwalO.akt_urut < validasiUrutan)
                continue;
            AktivitasPl akt = AktivitasPl.findById(jadwal.akt_id);
            jadwal.akt_jenis = akt.akt_jenis;
            jadwal.akt_id = jadwalO.akt_id;
            fieldName = "jadwal[" + i + ']';
            // jika tidak ada informasi waktu maka set menjadi jam 23:59:59
            // Tp metode ini mungkin ada bugnya, pls dicek lagi (ADY)
            if (jadwal.dtj_tglakhir != null) {
                DateTime dateTime = new DateTime(jadwal.dtj_tglakhir.getTime());
                if (dateTime.getHourOfDay() == 0 && dateTime.getMinuteOfHour() == 0) // jika waktu jadwal terakhir 00:00
                {
                    DateTime result = dateTime.withHourOfDay(23).withMinuteOfHour(59);
                    jadwal.dtj_tglakhir = result.toDate();
                }
            }
            if (i > 0) {
                before = jadwalList.get(i - 1);
            }
            try {
                if (lelangPl.lls_status.isDraft()) {
                    jadwal.validate(currentDate, false, before, true, true);
                } else if (lelangPl.lls_status.isAktif()) {
                    boolean ada_perubahan = !jadwal.equals(jadwalO);
                    if (ada_perubahan) {
                        // jika alasan di-supply oleh parameter, isi keterangan perubahan seluruh jadwal yang berubah
                        if(!CommonUtil.isEmpty(alasan)) {
                            jadwal.dtj_keterangan = alasan;
                        }
                        if (CommonUtil.isEmpty(jadwal.dtj_keterangan)) {
                            throw new Exception(Messages.get("nonlelang.hia_pjpkk"));
                        } else if (jadwal.dtj_keterangan.trim().length() <= 30) {
                            throw new Exception(Messages.get("nonlelang.alasan_perubahan_jadwal"));
                        }
                    }
                    jadwal.validate(currentDate, false, before, !jadwal.equalAwal(jadwalO), !jadwal.equalAkhir(jadwalO));
                    if (ada_perubahan && jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
                        // simpan perubahan
                        HistoryJadwalPl historyJadwal = new HistoryJadwalPl();
                        historyJadwal.dtj_id = jadwalO.dtj_id;
                        historyJadwal.hjd_tanggal_edit = currentDate;
                        historyJadwal.hjd_tanggal_awal = jadwalO.dtj_tglawal;
                        historyJadwal.hjd_tanggal_akhir = jadwalO.dtj_tglakhir;
                        historyJadwal.hjd_keterangan = jadwal.dtj_keterangan;
                        historyJadwal.save();
                        send_email = true;
                    }
                }
                jadwalO.dtj_tglawal = jadwal.dtj_tglawal;
                jadwalO.dtj_tglakhir = jadwal.dtj_tglakhir;
                jadwalO.dtj_keterangan = jadwal.dtj_keterangan;
                jadwalO.save();
            } catch (Exception e) {
                Validation.addError(fieldName, e.getLocalizedMessage());
            }
        }
        // jika paket SPSE 4 harus update state saat ubah jadwal dan lelang sedang berjalan
        if (lelangPl.lls_status.isAktif()) {
            ProcessInstancePl processInstance = WorkflowPlDao.findProcessBylelang(lelangPl);
            WorkflowPlDao.updateState(processInstance);
            if (send_email) {
                try {
                    MailQueueDao.kirimPerubahanJadwalLelangPl(lelangPl, jadwalOri, jadwalList, currentDate);
                } catch (Exception e) {
                    throw new Exception(Messages.get("nonlelang.tkdppep"));
                }
            }
        }
    }

    public static List<Jadwal_pl> findByLelang(Long lelangId) {
        String sql = "select j.*, akt_jenis, akt_urut from ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id= ? order by akt_urut asc";
        return Query.find(sql, CUSTOME_RESULTSET, lelangId).fetch();
    }

    // validasi isian jadwal , sebelum disimpan ke DB
    public void validate(Date currentDate, boolean express, Jadwal_pl before, boolean adaPerubahanAwal, boolean adaPerubahanAkhir) throws Exception {
        boolean first = before == null; // first jika jadwal before == null
        Tahap tahapBefore = null;
        if (before != null) {
            AktivitasPl aktivitasPl = AktivitasPl.findById(before.akt_id);
            tahapBefore = Tahap.valueOf(aktivitasPl.akt_jenis.toString());
        }
        if (akt_jenis == Tahap.PENJELASAN_PRA || akt_jenis == Tahap.PENYETARAAN_TEKNIS ||
                tahapBefore == Tahap.PENJELASAN_PRA || tahapBefore == Tahap.PENYETARAAN_TEKNIS ||
                (express && akt_jenis == Tahap.PENJELASAN)) {
            return;
        }
        if (dtj_tglawal == null) {
            throw new Exception(Messages.get("nonllelang.tmbt"));
        } else if (dtj_tglakhir == null) {
            throw new Exception(Messages.get("nonlelang.tsbt"));
        } else if (dtj_tglawal != null && dtj_tglawal.before(currentDate) && adaPerubahanAwal) {
            throw new Exception(Messages.get("nonlelang.tmyamsl"));
        } else if (dtj_tglakhir != null && dtj_tglakhir.before(currentDate) && adaPerubahanAkhir) {
            throw new Exception(Messages.get("nonlelang.tayamsl"));
        } else if (dtj_tglakhir != null && dtj_tglawal != null && before != null) {
            // Role #1: tahap sebelumnya != null except tahap pertama
            if (!first && before.dtj_tglawal == null) {
                throw new Exception(Messages.get("nonlelang.bajpts"));
            }
            // Role #2: awal > akhir
            if (dtj_tglakhir.before(dtj_tglawal)) {
                throw new Exception(Messages.get("nonlelang.tsmtm"));
            }
            // Role #3: awal(tahap sekarang) == awal(tahap sebelumnya) ||
            // awal(tahap sekarang) > awal(tahap sebelumnya)
            if (!first)//asep
                if (dtj_tglawal.before(before.dtj_tglawal)) {
                    throw new Exception(Messages.get("nonlelang.tmptmtmdts"));
                }
            // Role #4: Pembukaan Dok Penawaran harus setelah Upload Dok Penawaran
            if (tahapBefore == Tahap.PEMASUKAN_PENAWARAN){
                if (dtj_tglawal.before(before.dtj_tglakhir))
                    throw new Exception(Messages.get("nonlelang.tmptpdphsta"));

            }

            // Role #5: Evaluasi Penawaran harus setelah Pembukaan
            if (tahapBefore == Tahap.PEMBUKAAN_PENAWARAN){

                if (dtj_tglawal.before(before.dtj_tglakhir))
                    throw new Exception(Messages.get("nonlelang.tmpephstatpp"));

            }
        }
    }

    // ada perubahan atau tidak pada tgl awal
    @Transient
    public boolean equalAwal(Jadwal_pl jadwal) {
        return (dtj_tglawal != null && jadwal.dtj_tglawal != null && dtj_tglawal.equals(jadwal.dtj_tglawal));
    }

    // ada perubahan atau tidak pada tgl akhir
    @Transient
    public boolean equalAkhir(Jadwal_pl jadwal) {
        return (dtj_tglakhir != null && jadwal.dtj_tglakhir != null && dtj_tglakhir.equals(jadwal.dtj_tglakhir));
    }

    /**
     * jadwal sudah masuk tahapan saat ini
     * @return
     */
    @Transient
    public boolean isStart(Date curDate) {
        return (dtj_tglawal != null && curDate.after(dtj_tglawal));
    }

    @Transient
    public boolean isNow(Date curDate) {
        if (dtj_tglawal != null && dtj_tglakhir != null)
            return (curDate.after(dtj_tglawal) && curDate.before(dtj_tglakhir));
        return false;
    }

    @Transient
    public boolean isEnd(Date curDate) {
        return (dtj_tglakhir != null && curDate.after(dtj_tglakhir));
    }

    public static Jadwal_pl getJadwalByJadwalAndAct(long lls_id, long akt_id) {
        return Jadwal_pl.find("lls_id=? and akt_id=?", lls_id, akt_id).first();
    }

    public List<HistoryJadwalPl> getHistoryList() {
        if(historyList == null)
            historyList = HistoryJadwalPl.find("dtj_id=?", dtj_id).fetch();
        return historyList;
    }

    public static void copyJadwal(Long plId, Long plIdTujuan) {
        List<Jadwal_pl> listSource = Jadwal_pl.findByPl(plIdTujuan);
        Jadwal_pl obj = null;
        Date currentDate = controllers.BasicCtr.newDate();
        String fieldName=null;
        int i =0 ;
        for (Jadwal_pl jadwal : listSource) {
            obj = Jadwal_pl.find("lls_id=? and akt_id=?", plId, jadwal.akt_id).first();
            fieldName = "jadwal["+i+ ']';
            if(obj != null && jadwal != null) {
                obj.dtj_tglawal = jadwal.dtj_tglawal;
                obj.dtj_tglakhir = jadwal.dtj_tglakhir;
                try {
                    if(obj.dtj_tglawal != null && obj.dtj_tglawal.before(currentDate))
                        throw new Exception(Messages.get("nonlelang.tmsl"));
                    if(jadwal.dtj_tglakhir != null && jadwal.dtj_tglakhir.before(currentDate))
                        throw new Exception(Messages.get("nonlelang.tasl"));
                }catch (Exception e){
                    Logger.error(e.getMessage());
                    Validation.addError(fieldName, e.getLocalizedMessage());
                }
                obj.save();
            }
            i++;
        }
    }

}
