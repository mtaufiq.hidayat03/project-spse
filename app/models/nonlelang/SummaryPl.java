package models.nonlelang;

import ext.FormatUtils;
import models.agency.Anggota_panitia;
import models.agency.Kontrak;
import models.agency.KontrakPl;
import models.agency.Panitia;
import models.agency.Pegawai;
import models.agency.Pp;
import models.agency.SppbjPl;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.lelang.Persetujuan;
import models.lelang.cast.AnggotaPanitiaView;

import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.InputStream;
import java.util.*;

/**
 * Created by Lambang on 8/18/2017.
 */
public class SummaryPl {

    static final Template TEMPLATE = TemplateLoader.load("/nonlelang/template-summary-pl.html");

    public static InputStream cetak(Long plId) {
    	StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Pl_detil pl_detil = Pl_detil.findById(plId);
        boolean prakualifikasi = pl_detil.isPrakualifikasi();
//        boolean showBobot = pl_detil.getEvaluasi().isNilai();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("pl_detil", pl_detil);
        model.put("prakualifikasi", prakualifikasi);
        if(pl_detil.getKualifikasi() != null)
            model.put("kualifikasi", pl_detil.getKualifikasi().label);
        List<PersetujuanPl.PersetujuanLelangPl> persetujuanList = PersetujuanPl.PersetujuanLelangPl.getPersetujuanNonLelangForSummary(pl_detil.lls_id);
        model.put("persetujuanList", persetujuanList);
//        model.put("showBobot", pl_detil.getEvaluasi().isNilai());
//        if (showbobot) {
//            model.put("bobotteknis", pl_detil.lls_bobot_teknis);
//            model.put("bobotbiaya", pl_detil.lls_bobot_biaya);
//        }
        Pl_seleksi pl = Pl_seleksi.findById(pl_detil.lls_id);
        Date tanggalBuat = pl.lls_dibuat_tanggal;
        String namaPegawai = pl.getNamaPegawaiPembuatan();
        String tglPembuatan = FormatUtils.formatDateTimeInd(tanggalBuat) + " Oleh " + namaPegawai;
        Date tanggalSetuju = pl.lls_tgl_setuju;
        String pegawai2 = pl.getNamaPegawaiMenyetujui();
        model.put("tglPembuatan", tglPembuatan);
	        List<Dok_pl> listDokPl = Dok_pl.find("lls_id = ? AND dll_jenis <> 0", pl_detil.lls_id).fetch();
	        if (listDokPl != null && !listDokPl.isEmpty()) {
	            List<BlobTable> blobList = new ArrayList<BlobTable>();
	            for (Dok_pl dok_pl : listDokPl) {
	                if (dok_pl != null && dok_pl.dll_id_attachment != null)
	                    blobList.addAll(BlobTableDao.listById(dok_pl.dll_id_attachment));
	            }
	            for(BlobTable bb : blobList) {
	                bb.blb_nama_file = bb.blb_nama_file.trim();
	            }
	            model.put("listDokPl", blobList);
	        }
        	List<Dok_pl> listDokPlPemilihan = Dok_pl.find("lls_id = ? AND dll_jenis = 0", pl_detil.lls_id).fetch();
	        if (listDokPlPemilihan != null && !listDokPlPemilihan.isEmpty()) {
	            List<BlobTable> blobListPemilihan = new ArrayList<BlobTable>();
	            for (Dok_pl dok_pl : listDokPlPemilihan) {
	                if (dok_pl != null && dok_pl.dll_id_attachment != null)
	                    blobListPemilihan.addAll(BlobTableDao.listById(dok_pl.dll_id_attachment));
	            }
	            for(BlobTable bb : blobListPemilihan) {
	                bb.blb_nama_file = bb.blb_nama_file.trim();
	            }
	            model.put("listDokPlPemilihan", blobListPemilihan);
	        }
        List<Jadwal_pl> jadwalList = Jadwal_pl.findByLelang(pl.lls_id);
        model.put("jadwalList", jadwalList);
        Pp pp = pl.getPp();
        Panitia panitia = pl.getPanitia();
        if(pp != null) {
        	model.put("nama_pp" , pp.getPegawai().peg_nama);
        	model.put("nip_pp" , pp.getPegawai().peg_nip);
            model.put("pp", pp);
        }
        if(panitia != null) {
        	List<AnggotaPanitiaView> listAnggota = AnggotaPanitiaView.findByPantia(panitia.pnt_id);
        	model.put("listAnggota", listAnggota);
        	model.put("nama_panitia" , panitia.pnt_nama);
        	model.put("no_sk" , panitia.pnt_no_sk);
            model.put("panitia", panitia);
        }
        List<PesertaPl> pesertList = PesertaPl.find("lls_id=?", pl_detil.lls_id).fetch();
        model.put("pesertaList", pesertList);
        List<DokPenawaranPl> penawaranList = DokPenawaranPl
                .find("psr_id IN (SELECT psr_id FROM ekontrak.peserta_nonlelang WHERE lls_id=? ORDER BY psr_tanggal) AND dok_jenis IN (1,2,3)",
                        pl_detil.lls_id)
                .fetch();
        model.put("penawaranList", penawaranList);
        List<EvaluasiPl> evaluasiList = EvaluasiPl.find("lls_id=? order by eva_versi asc", pl_detil.lls_id).fetch();
        model.put("evaluasiList", evaluasiList);
        List<NilaiEvaluasiPl> pemenangListPra = NilaiEvaluasiPl.find("eva_id IN (SELECT eva_id FROM ekontrak.evaluasi WHERE lls_id=? AND eva_jenis=0) order by nev_urutan ASC", pl_detil.lls_id).fetch();
        model.put("pemenangListPra", pemenangListPra);
        List<NilaiEvaluasiPl> pemenangList = NilaiEvaluasiPl.find("eva_id IN (SELECT eva_id FROM ekontrak.evaluasi WHERE lls_id=? AND eva_jenis=4) order by nev_urutan ASC", pl_detil.lls_id).fetch();
        model.put("pemenangList", pemenangList);
        List<SppbjPl> sppbjList = SppbjPl.find("lls_id=?", pl_detil.lls_id).fetch();
        model.put("sppbjList", sppbjList);
        model.put("blob_tableDao", new BlobTableDao());
        KontrakPl kontrak = KontrakPl.find("lls_id=?", pl_detil.lls_id).first();
        model.put("kontrak", kontrak);
        boolean adaPemenang = NilaiEvaluasiPl.countLulus(pl.lls_id, EvaluasiPl.JenisEvaluasi.EVALUASI_AKHIR) > 0;
        model.put("adaPemenang", adaPemenang);
        stopWatch.stop();
        Logger.info("Summary Tender %s: render param %s", pl_detil.lls_id, stopWatch.getTime());
        return HtmlUtil.generatePDF(TEMPLATE, model);
    }

}
