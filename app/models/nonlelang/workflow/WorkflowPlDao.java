package models.nonlelang.workflow;

import models.common.ConfigurationDao;
import models.common.MetodePemilihanPenyedia;
import models.common.SesiPelatihan;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.nonlelang.Jadwal_pl;
import models.nonlelang.Pl_seleksi;
import models.workflow.definition.Variable;
import models.workflow.instance.VariableInstance;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.db.jdbc.Query;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Lambang on 2/8/2017.
 */
public class WorkflowPlDao {

    // cari process instance yang sudah pada state END, diset menjadi status = 2 (ProcessInstance.PROCESS_STATUS.COMPLETED)
    private static final String SQL_FIND_END_STATE =  "UPDATE ekontrak.wf_process_instance SET status = 2 WHERE process_instance_id in (select process_instance_id from ekontrak.wf_state_instance si, ekontrak.wf_state s where s.state_id=si.state_id and s.name='END' and active_state=true)";
    // SQL update workflow mode production
    private static final String SQL_SET_AKTIF_STATE = "UPDATE ekontrak.wf_state_instance SET active_state = true WHERE enter_date <= ? and exit_date >= ? and process_instance_id in (select process_instance_id from ekontrak.wf_process_instance where status = 1 )";
    private static final String SQL_SET_NON_AKTIF_STATE = "UPDATE ekontrak.wf_state_instance SET active_state = false WHERE (enter_date > ? or exit_date < ?) and process_instance_id in (select process_instance_id from ekontrak.wf_process_instance where status = 1 )";
    // SQL update workflow mode Latihan
    private static final String SQL_FIND_PROCESS_ID_AKTIF = "process_instance_id in (select process_instance_id from ekontrak.wf_process_instance where status = 1) and process_instance_id in (select lls_wf_id from ekontrak.nonlelang_seleksi where lls_status= 1 and lls_sesi=?)";
    private static final String SQL_SET_AKTIF_STATE_LAT = "UPDATE ekontrak.wf_state_instance SET active_state = true WHERE enter_date <= ? and exit_date >= ? and "+SQL_FIND_PROCESS_ID_AKTIF;
    private static final String SQL_SET_NON_AKTIF_STATE_LAT = "UPDATE ekontrak.wf_state_instance SET active_state = false WHERE (enter_date > ? or exit_date < ?) and "+SQL_FIND_PROCESS_ID_AKTIF;
    /**
     * update process instance seluruh paket yang dilelang
     */
    public static void updateProcess() {
        Date date = null;
        StopWatch sw=new StopWatch();
        sw.start();
        if(ConfigurationDao.isProduction()) {
            date = DateUtil.newDate();
            Query.update(SQL_SET_AKTIF_STATE, date, date);
            Query.update(SQL_SET_NON_AKTIF_STATE, date, date);
        } else {
            List<SesiPelatihan> list = SesiPelatihan.getActive();
            if(!CommonUtil.isEmpty(list)) {
                for(SesiPelatihan sesi:list) {
                    date = DateUtil.newDate(sesi.id);
                    Query.update(SQL_SET_AKTIF_STATE_LAT, date, date, sesi.id);
                    Query.update(SQL_SET_NON_AKTIF_STATE_LAT, date, date, sesi.id);
                }
            }
        }
        Query.update(SQL_FIND_END_STATE);
        sw.stop();
        Logger.debug("[WorkflowPl] WorkflowPlDao.updateProcess(). Duration: %s ", sw.toString());
    }

    /**
     * update process instance seluruh paket yang dilelang per sesi

     public static void updateProcess(int sesi_id) {
     StopWatch sw=new StopWatch();
     sw.start();
     Date date = null;
     String join_process_instance = null;
     date = DateUtil.newDate(sesi_id);
     Logger.info("sesi %s : %s", sesi_id, date);
     List<Long> listActive = Query.find("select process_instance_id from wf_process_instance where status = ? and description like '{\"sesi\":"+sesi_id+"%'", Long.class, PROCESS_STATUS.RUNNING).fetch(); // hanya mengambil proses lelang yang aktif (lelang.status.isAktif()) dan prosesinstance dengan status RUNNING
     if(CommonUtil.isEmpty(listActive)) {
     Logger.info("[Workflow] belum ada processInstance yang aktif pada sesi %s", sesi_id);
     return;
     }
     join_process_instance = StringUtils.join(listActive, ",");
     Logger.info("process instance : %s", join_process_instance);
     Query.execute("UPDATE wf_state_instance SET active_state = true WHERE enter_date <= ? and exit_date >= ? and process_instance_id in (" + join_process_instance + ")", date, date).update();
     Query.execute("UPDATE wf_state_instance SET active_state = false WHERE (enter_date > ? or exit_date < ?) and process_instance_id in ("+join_process_instance+")", date, date).update();
     // update process instance yang sudah ke state END

     sw.stop();
     Logger.info("[Workflow] done processInstance.updateState(). Duration: %s ", sw.toString());
     }
     */

    /**
     * create prosesinstance suatu lelang
     * action terjadi saat lelang akan disetujui dan diumumkan
     * @return
     */
    public static ProcessInstancePl createProsesInstance(Long lelangId, Long lls_wf_id, MetodePemilihanPenyedia metode) {
        Logger.info("creating workflow pl instance");
        ProcessInstancePl prosesInstance = null;
        if(lls_wf_id != null) {
            Logger.info("process instance pl "+lls_wf_id);
            prosesInstance =ProcessInstancePl.getInstance(lls_wf_id);
        } else {
            WorkflowPl workflow = WorkflowPl.findById(metode.toString());
            ProcessDefinitionPl definisi = ProcessDefinitionPl.findByWorkflowId(workflow.workflow_id);
            try {
                prosesInstance = ProcessInstancePl.createByProcessDefinition(definisi, lelangId.toString());
//				updateState(prosesInstance);
//				Metode metode = Lelang_seleksiDao.getMetodeLelang(lelang.lls_id);
//				List<Jadwal> jadwalList = Jadwal.findByLelang(lelang.lls_id);
//				Date date = DateUtil.newDate(prosesInstance.description);
//				updateVariable(jadwalList, prosesInstance,date);
//				prosesInstance.startInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return prosesInstance;
    }

    /**
     * mendapatkan ProcessInstance dari suatu lelang
     * @param lelangId
     * @return
     */
    public static ProcessInstancePl findProcessBylelang(Long lelangId) {
        Long prosesId = Query.find("select lls_wf_id from ekontrak.nonlelang_seleksi where lls_id=?", Long.class, lelangId).first();
        if(prosesId == null)
            return null;
        return ProcessInstancePl.findById(prosesId);
    }

    /**
     * mendapatkan ProcessInstance dari suatu lelang
     * @param lelang
     * @return
     */
    public static ProcessInstancePl findProcessBylelang(Pl_seleksi lelang) {
        Long prosesId = lelang.lls_wf_id;
        if(prosesId == null)
            return null;
        return ProcessInstancePl.findById(prosesId);
    }

    /**
     * semua variable mempunya tipe boolean
     * @param processInstance
     * @param variableName
     * @return
     */
    public static boolean findValueVariableInstace(ProcessInstancePl processInstance, String variableName){
        VariableInstance var = processInstance.getVariableInstance(variableName);
        if(var == null)
            return false;
        return Boolean.parseBoolean(var.value);
    }

    /**
     * setup state dari workflow lelang
     * @param instance
     */
    public static void updateState(ProcessInstancePl instance) {
        if(instance == null)
            return;
        Pl_seleksi lelang = Pl_seleksi.find("lls_wf_id=?",instance.process_instance_id).first();
        if(lelang == null)
            return;
        List<Jadwal_pl> jadwalList = lelang.getJadwalList();
        StateInstancePl state = null;
        Date currentDate = DateUtil.newDate(lelang.lls_sesi);
        for (Jadwal_pl jadwal : jadwalList) {
            state = StateInstancePl.findAndCreateByProcessInstanceAndState(instance, jadwal.akt_jenis.toString(), jadwal.dtj_tglawal);
            state.enter_date = jadwal.dtj_tglawal;
            state.exit_date = jadwal.dtj_tglakhir;
            state.save();
            if ((jadwal.akt_jenis == Tahap.PEMASUKAN_PENAWARAN) && jadwal.dtj_tglawal != null) {
                StateInstancePl start = StateInstancePl.findAndCreateByProcessInstanceAndState(instance, "START",currentDate);
                start.enter_date = instance.start_date;
                start.exit_date = jadwal.dtj_tglawal; // state START akan selesai saat masuk State Pengumuman lelang/prakualifikasi
                if(start.enter_date.before(currentDate) && start.exit_date.after(currentDate))
                    start.active_state = true;
                start.save();
            }
            if ((jadwal.akt_jenis == Tahap.TANDATANGAN_SPK || jadwal.akt_jenis == Tahap.TANDATANGAN_SMPK) && jadwal.dtj_tglakhir != null) {
                StateInstancePl end = StateInstancePl.findAndCreateByProcessInstanceAndState(instance, "END",currentDate);
                end.enter_date = jadwal.dtj_tglakhir;
                end.save();
            }
        }
        if(CommonUtil.isEmpty(instance.mapVariableInstance)) {
            Set<Variable> list = instance.getProcessDefinition().getVariableList();
            if(!CommonUtil.isEmpty(list)) {
                for(Variable variable : list)
                    try {
                        instance.setVariable(variable.name, "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
            instance.save();
        }
    }

    public static List<Tahap> findAktifState(Long process_instance_id) {
        List<StatePl> list = StatePl.find("state_id in (SELECT state_id FROM ekontrak.wf_state_instance si WHERE si.active_state=true and process_instance_id=?)", process_instance_id).fetch();
        if(CommonUtil.isEmpty(list))
            return null;
        List<Tahap> tahapList = new ArrayList<Tahap>(list.size());
        for(StatePl state:list) {
            if(state == null || StringUtils.isEmpty(state.name) || state.name.equals("START") || state.name.equals("END") || state.name.equals("LELANG_ULANG"))
                continue;
            tahapList.add(Tahap.valueOf(state.name));
        }
        return tahapList;
    }

}
