package models.nonlelang.workflow;

import models.workflow.WorkflowBaseModel;
import models.workflow.definition.ProcessDefinition;
import models.workflow.definition.State;
import models.workflow.definition.StateMapper;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.h2.tools.Csv;
import play.Logger;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.script.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Lambang on 2/8/2017.
 */

@Table(name="EKONTRAK.WF_WORKFLOW")
public class WorkflowPl extends WorkflowBaseModel {

    @Id
    public String workflow_id;

    public String name;

    /**Import process definition dari file CSV.
     * Format CSV sebagai berikut
     * 1. Terdiri atas 3 kolom
     * 2. Row 1 berisi: WORKFLOW_ID, VERSION, DESCRIPTION
     * 3. Row 2 berisi header: Current State, Next State, Condition to Next State
     * 4. Row 3 dst berisi StateMapping
     * 5. Satu process diawali dengan State START
     * 6. Satu StateMapping terdiri atas 3 kolom CurrentState, NextState, CONDITION
     * 7. Nama State terdiri atas HURUF_BESAR atau underscore.
     * 8. Satu CurrentState berpindah ke NextState setelah CONDITION terpenuhi.
     * 9. CONDITION berupa expression JavaScript yang memiliki result true atau false. CONDITION mengecek nilai dari VARIAB
     * 10.
     * 8. Nama NextState yang diawali tanda @ disebut FORK.
     * 9. FORK artinya satu state yang tetap sebag
     * @param replaceExisting jika true, maka process Id yang lama akan dihapus
     * @throws Exception */
    public static void importProcessDefinition(InputStream is, boolean replaceExisting) throws Exception
    {
        Csv csv=new Csv();
        String[] columns=new String[]{"CurrenState","NextState","Condition","resetVariables"};
        Reader reader=new InputStreamReader(is);
        ResultSet rs=csv.read(reader, columns);
        //read headers (row 0 & 1)
        String[] row=readResultSet(rs);
        String namaWorkflow="", workflowId="";
        Integer versi=0;
        if(row!=null)
        {
            namaWorkflow=row[2];
            try
            {
                versi=Integer.parseInt(row[1]);
            }
            catch(NumberFormatException e)
            {
                throw new IOException("Format versi salah (baris 1 kolom 2): '" + row[1] + '\'');
            }
            workflowId=row[0];
        }
        else
            throw new IOException("Invalid file format. Row 0 is missing");
        //skip 1 row
        if((row=readResultSet(rs))==null)
            throw new IOException("Invalid file format. Row 1 is missing");
        int rowIndex=3;

        //ambil process
        ProcessDefinition process=getProcess(workflowId, namaWorkflow, versi, replaceExisting);
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");
        Set<String> variables=new TreeSet<String>();
        //sekarang baca isinya
        while((row=readResultSet(rs))!=null)
        {
            String currStateName=row[0];
            if(currStateName!=null)
            {
                char firstChar=currStateName.charAt(0);
                switch(firstChar)
                {
                    case '#': break;
                    default:
                        String nextStateName=row[1];
                        String condition=row[2];
                        String resetVariables=row[3];
                        if(condition!=null)
                        {
                            try
                            {
                                if(condition.charAt(0)=='#')//Condition bisa berisi komentar
                                    break;
                                evaluateCondition(variables, condition, jsEngine);
                                if(!StringUtils.isEmpty(resetVariables))
                                    evaluateCondition(variables, resetVariables, jsEngine);
                            }
                            catch(ScriptException e)
                            {
                                throw new Exception("Error pada condition row: " + rowIndex + '\n' + e.toString());
                            }
                        }

                        //get state
                        State currState=getState(currStateName, process);
                        StateMapper mapper=new StateMapper();
                        mapper.process_definition_id=process.process_definition_id;
                        mapper.state_id=currState.state_id;
                        mapper.condition=condition;
                        mapper.postVisitScript=resetVariables;
                        mapper.createdate = new Date();
                        State nextState=null;
                        if(nextStateName!=null)
                        {
                            boolean fork=false;
                            if(nextStateName.charAt(0)=='@')//fork
                            {
                                nextStateName=nextStateName.substring(1);
                                fork=true;
                            }
                            nextState=getState(nextStateName, process);//dapatkan next state
                            nextState.fork=fork;
                            nextState.createdate = new Date();
                            nextState.save();
                            mapper.next_state_id=nextState.state_id;
                        }
                        mapper.save();
                        Logger.debug("Adding StateMapper: %s", mapper.toString());
                        rowIndex++;
                }
            }
        }

        String str=process.validateProcess();
        if(str!=null)
            throw new Exception("Process Definition salah. State berikut ini tidak memiliki transisi ke END: " + str);

        //		tambahkan variables
        process.addVariables(variables);
        process.save();
    }

    /** Evaluate the condition.
     * Using ScriptingEngine
     * Ref: http://java.sun.com/developer/technicalArticles/J2SE/Desktop/scripting/  */
    private static void evaluateCondition(Set<String> variables, String conditionScript,  ScriptEngine jsEngine) throws ScriptException {
        parseVariables(conditionScript, variables);//get variable from conditionScript
        try {
            Bindings binding= jsEngine.createBindings();
            //put variable to binding
            for(String key: variables)
                binding.put(key, null);
            jsEngine.setBindings(binding, ScriptContext.ENGINE_SCOPE);
            Object value=jsEngine.eval(conditionScript);
            binding= jsEngine.createBindings();
            Logger.trace("Evaluate condition: %s, valid: %s", conditionScript, value);
        } catch (ScriptException ex) {
            throw new ScriptException(ex + "  ==>> " + conditionScript);
        }
    }

    /**Dapatkan nama-nama variable.
     * Variable terdiri atas HURUF BESAR dan tanda underscore
     * @param condition
     * @return
     */
    private static void parseVariables(String condition, Set<String> variables)
    {

        StringBuilder str=new StringBuilder();
        char[] LETTERS="ABCDEFGHIJKLMNOPQRSTUVWXYZ_".toCharArray();
        boolean startCapture=false;
        condition=condition+ '\n';
        for(int i=0;i<condition.length();i++)
        {
            char ch=condition.charAt(i);
            if(ArrayUtils.contains(LETTERS, ch))
            {
                startCapture=true;
                str.append(ch);
            }
            else
            {
                if(startCapture)
                {
                    //found a variable
                    variables.add(str.toString());
                    str=new StringBuilder();
                }
                startCapture=false;
            }

        }

    }

    /**Dapatkan state berdasarkan nama ini.
     * Jika tidak ada maka buat state baru & simpan
     * @return
     */
    private static State getState(String stateName, ProcessDefinition process) {
        State state=process.findState(stateName);
        if(state==null)
        {
            state=new State();
            state.name = stateName;
            state.process_definition_id=process.process_definition_id;
            state.createdate = new Date();
            state.save();
        };
        return state;
    }

    /** Dapatkan process. Jika tidak ditemukan maka create baru di database
     * @param replaceExisting untuk sementara tidak dipakai (tidak diijinkan replace existing)
     * @throws Exception jika ternyata sudah ada */
    private static ProcessDefinition getProcess(String workflowId, String namaWorkflow, Integer versi, boolean replaceExisting) throws Exception  {
        ProcessDefinition process=ProcessDefinition.findByWorkflowIdAndVersionAndStatus(workflowId, versi, ProcessDefinition.STATUS.DRAF);

//todo: cascade delete belum bisa
        if(process!=null && replaceExisting)//hapus jika diperlukan
        {
            process.delete();
            process=null;
        }
        if(process==null)
        {

            WorkflowPl workflow=findById(workflowId);
            if(workflow==null)
            {
                workflow=new WorkflowPl();
                workflow.workflow_id=workflowId;
                workflow.name=namaWorkflow;
                workflow.createdate = new Date();
                workflow.save();
            }
            //proses belum ada, buat baru
            process=new ProcessDefinition();
            process.workflow_id=workflowId;
            process.version=versi;
            //untuk sementara statusnya selalu aktif
            process.createdate = new Date();
            process.status= ProcessDefinition.STATUS.AKTIF;
            process.createdate = new Date();
            process.save();
        }
        else
            throw new Exception(String.format("ProcessDefinition sudah ada WorkflowId %s versi %s ", workflowId, versi));
        return process;
    }

    private static String[] readResultSet(ResultSet rs) throws SQLException
    {
        if(rs.next())
        {
            String[] ary=new String[4];
            ary[0]=rs.getString(1);
            ary[1]=rs.getString(2);
            ary[2]=rs.getString(3);
            ary[3]=rs.getString(4);
            //remove comment, that is started with #
//			for(int i=0; i<ary.length;i++)
//				if(ary[i]!=null)
//					if(ary[i].charAt(0)=='#')
//						ary[i]=null;
//			if(ary[0]==null)
//				return null;
            return ary;
        }
        else
            return null;
    }

}
