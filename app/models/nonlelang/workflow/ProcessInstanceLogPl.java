package models.nonlelang.workflow;

import models.workflow.WorkflowBaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * Created by Lambang on 2/8/2017.
 */

@Table(name="EKONTRAK.WF_PROCESS_INSTANCE_LOG")
public class ProcessInstanceLogPl extends WorkflowBaseModel {

    @Id(sequence="SEQ_WF_PROCESS_INSTANCE_LOG")
    public Long logId;

    public Long process_instance_id;

    public Date log_date;

    public String log_info;

}
