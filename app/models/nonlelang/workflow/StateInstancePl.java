package models.nonlelang.workflow;

import models.workflow.WorkflowBaseModel;
import models.workflow.instance.VariableInstance;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import javax.script.ScriptException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 2/8/2017.
 */

@Table(name = "EKONTRAK.WF_STATE_INSTANCE")
public class StateInstancePl extends WorkflowBaseModel{

    @Id(sequence="EKONTRAK.SEQ_WF_STATE_INSTANCE")
    public Long state_instance_id;

    public Date enter_date;

    public Date exit_date;

    public String state_id;

    public Long process_instance_id;

    /**
     * jika true artinya saat ini berada pada state ini (Bisa lebih dari 1 state
     * yg aktif
     */
    public boolean active_state;

    public void setActiveStateFalse() {
        if(active_state)//set exitDate only if changed from true to false
        {
            active_state = false;
            exit_date = new Date();
        }
    }

    public void setActiveStateTrue() {
        active_state = true;
        exit_date = null;
    }

    @Transient
    private StatePl state;
    @Transient
    private ProcessInstancePl processInstance;

    public StatePl getState() {
        if(state == null)
            state = StatePl.findById(state_id);
        return state;
    }

    public ProcessInstancePl getProcessInstance() {
        if(processInstance == null)
            processInstance = ProcessInstancePl.findById(process_instance_id);
        return processInstance;
    }

    public String toString() {
        return state_instance_id + ":" + state_id + ": "+ (active_state ? "active" : "");
    }

    /**
     * Dari state yang sekarang, coba untuk pindah ke state berikutnya
     *
     * @return true jika berhasil pindah
     * @throws ScriptException
     * @throws Exception
     * */
    public boolean gotoNextState() throws Exception {
//		processInstance.detectStateLooping(null);
        return gotoNextState(0);
    }


    /** Depth digunakan untuk keperluan debugging agar tahu call-stack nya
     *
     * @param depth
     * @return
     * @throws Exception
     */
    private boolean gotoNextState(int depth) throws Exception {
        StatePl state = getState();
        ProcessInstancePl processInstance = getProcessInstance();
        if (state.isEndState()) {
            processInstance.status = ProcessInstancePl.PROCESS_STATUS.COMPLETED;
            processInstance.save();
            return false;
        }

        // next state
        List<StateMapperPl> nextStateMapperList = state.getNextStateMapperList();
        if (nextStateMapperList == null)
            return false;
//		Logger.info("Current State: %s", state.name);
        processInstance.addLog("Current State is: " + state.name);
        Collection<VariableInstance> variableInstanceList = processInstance.mapVariableInstance.values();
        // goto next state
        int visitedStateCount = 0;
        boolean allowMoveToNextState = false;
        for (StateMapperPl stateMapper : nextStateMapperList) {
            boolean allow = stateMapper.allowGotoNext(processInstance, variableInstanceList,depth);
            if (allow) {
                StateInstancePl si = StateInstancePl.findAndCreateByProcessInstanceAndState(processInstance, stateMapper.getNextState());
                if(si.state_instance_id==null)
                {
                    si.enter_date = new Date();
                    si.process_instance_id = process_instance_id;
                    si.state_id = stateMapper.next_state_id;
                    processInstance.addLog("Move to state: " + si.getState().name);
                    processInstance.executePostVisitScript(stateMapper.postVisitScript);
                    si.save();
//					processInstance.detectStateLooping(stateMapper);
                }
                processInstance.executePostVisitScript(stateMapper.postVisitScript);  // jalankan post visit script meskipun state ini sudah pernah di-evaluasi
                // recursive
                depth = depth + 1;
                si.gotoNextState(depth);
                visitedStateCount++;
                allowMoveToNextState = true;
                //jika ini bukan fork, jika 1 nextState sudah  allow maka tidak perlu evaluasi state berikutnya
                if(!state.fork)
                    break;
            }
        }

        if (visitedStateCount == 0)
            setActiveStateTrue();
        else
            setActiveStateFalse();

        // jika ini Fork, activeState false jika semua nextState sdh active
        if (state.fork) {
            int nextStateCount = nextStateMapperList.size();
            if (visitedStateCount == nextStateCount)
                setActiveStateFalse();
            else
                setActiveStateTrue();
        }

        save();

        return allowMoveToNextState;
    }

    public boolean equals(Object obj) {
        StateInstancePl si = (StateInstancePl) obj;
        return si.state_instance_id == state_instance_id;
    }

    /**Dapatkan StateInstance yg active */
    public static List<StateInstancePl> findActiveByProcessInstanceId(Long processInstanceId) {
        return find("process_instance_id=? and active_state=true", processInstanceId).fetch();
    }

    /**Dapatkan Semua StateInstance */
    public static List<StateInstancePl> findByProcessInstanceId(Long processInstanceId) {
        return find("process_instance_id=? order by enter_date", processInstanceId).fetch();
    }

    /**Dapatkan State instance berdasrkan processInstance dan state ini *.
     * Create jika belum ada
     * @param processInstance
     * @param state
     * @return
     */
    public static StateInstancePl findAndCreateByProcessInstanceAndState(ProcessInstancePl processInstance, StatePl state) {
        StateInstancePl si=find("process_instance_id=? and state_id=?", processInstance.process_instance_id, state.state_id).first();
        if(si==null)
        {
            si=new StateInstancePl();
            si.process_instance_id=processInstance.process_instance_id;
            si.state_id=state.state_id;
            si.createdate = new Date();
        }
        return si;
    }

    public static StateInstancePl findAndCreateByProcessInstanceAndState(ProcessInstancePl processInstance, String stateName, Date date) {
        StateInstancePl si = null;

        StatePl state = StatePl.find("name=? and process_definition_id=?", stateName, processInstance.process_definition_id).first();

        if(state != null)
            si=find("process_instance_id=? and state_id=?", processInstance.process_instance_id, state.state_id).first();
        if(si==null)
        {
            si=new StateInstancePl();
            si.process_instance_id=processInstance.process_instance_id;
            si.state_id=state.state_id;
            si.enter_date = date;
            si.createdate = new Date();
        }
        return si;
    }

}
