package models.nonlelang;

import models.common.Aktivitas;
import models.common.AktivitasPl;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import models.nonlelang.cast.NilaiEvaluasiPlView;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.*;

/**
 * Created by Lambang on 2/21/2017.
 */

@Table(name="EKONTRAK.EVALUASI")
public class EvaluasiPl extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum StatusEvaluasi {
        SEDANG_EVALUASI, SELESAI, DITOLAK;
        public boolean isSelesai(){
            return this == SELESAI;
        }
        public boolean isSedangEvaluasi(){
            return this == SEDANG_EVALUASI;
        }
    }
    @Enumerated(EnumType.ORDINAL)
    public enum JenisEvaluasi {
        EVALUASI_KUALIFIKASI(0, "Evaluasi Kualifikasi"),
        EVALUASI_ADMINISTRASI(1, "Evaluasi Administrasi"),
        EVALUASI_TEKNIS(2, "Evaluasi Teknis"),
        EVALUASI_HARGA(3, "Evaluasi Harga"),
        EVALUASI_AKHIR(4, "Evaluasi Akhir"),
    	PEMBUKTIAN_KUALIFIKASI(5, "Pembuktian Kualifikasi");

        public final Integer id;
        public final String label;
        JenisEvaluasi(int id, String nama) {
            this.id = Integer.valueOf(id);
            this.label = nama;
        }

        public boolean isAdministrasi() {
            return this == EVALUASI_ADMINISTRASI;
        }

        public boolean isTeknis() {
            return this == EVALUASI_TEKNIS;
        }

        public boolean isKualifikasi() {
            return this == EVALUASI_KUALIFIKASI;
        }

        public boolean isHarga() {
            return this == EVALUASI_HARGA;
        }

        public boolean isAkhir() {
            return this == EVALUASI_AKHIR;
        }

//        public boolean isPembuktian() {
//            return this == PEMBUKTIAN_KUALIFIKASI;
//        }

        public static JenisEvaluasi findById(Integer value) {
            if(value == null)
                return null;
            JenisEvaluasi jenis = null;
            switch (value.intValue()) {
                case 0: jenis = EVALUASI_KUALIFIKASI;break;
                case 1: jenis = EVALUASI_ADMINISTRASI;break;
                case 2: jenis = EVALUASI_TEKNIS;break;
                case 3: jenis = EVALUASI_HARGA;break;
                case 4: jenis = EVALUASI_AKHIR;break;
//                case 5: jenis = PEMBUKTIAN_KUALIFIKASI;break;
            }
            return jenis;
        }

        public static final List<JenisEvaluasi> JENIS_EVALUASI_URUT_PASCA = Arrays.asList(EVALUASI_ADMINISTRASI, EVALUASI_TEKNIS, EVALUASI_HARGA, EVALUASI_KUALIFIKASI, EVALUASI_AKHIR);
        public static final List<JenisEvaluasi> JENIS_EVALUASI_URUT_PRA = Arrays.asList(EVALUASI_KUALIFIKASI, EVALUASI_ADMINISTRASI, EVALUASI_TEKNIS, EVALUASI_HARGA, EVALUASI_AKHIR);
        public static final List<JenisEvaluasi> JENIS_EVALUASI_KUALIFIKASI = Collections.singletonList(EVALUASI_KUALIFIKASI);
    }

    @Id(sequence="ekontrak.seq_evaluasi", function="nextsequence")
    public Long eva_id;

    @Required
    public JenisEvaluasi eva_jenis;

    public Integer eva_versi;

    public Date eva_tgl_minta;

    public Date eva_tgl_setuju;

    @Required
    public StatusEvaluasi eva_status;

    public Long eva_wf_id;

    public Long lls_id;

    @Transient
    private Pl_seleksi pl_seleksi;

    public Pl_seleksi getPl_seleksi() {
        if(pl_seleksi == null)
            pl_seleksi = Pl_seleksi.findById(lls_id);
        return pl_seleksi;
    }

    protected static EvaluasiPl findNCreate(Long lelangId, JenisEvaluasi jenis){
        EvaluasiPl evaluasi = findBy(lelangId, jenis);
        if (evaluasi == null) {
            evaluasi = new EvaluasiPl();
            evaluasi.lls_id = lelangId;
            evaluasi.eva_jenis = jenis;
            evaluasi.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
            Integer versi = Query.find("SELECT MAX(eva_versi) FROM ekontrak.evaluasi WHERE lls_id=? AND eva_jenis=?", Integer.class, lelangId, jenis).first();
            if(versi == null || versi == 0)
                versi = 1;
            evaluasi.eva_versi = versi;
            evaluasi.eva_tgl_setuju = controllers.BasicCtr.newDate();
            evaluasi.save();
        }
        return evaluasi;
    }

    public static Integer findCurrentVersi(Long lelangId){
        Integer currentVersi = Query.find("SELECT MAX(eva_versi) FROM ekontrak.evaluasi WHERE lls_id=?",Integer.class, lelangId).first();
        if(currentVersi == null || currentVersi == 0)
            currentVersi = 1;
        return currentVersi;
    }

    public static int findCountEvaluasiUlang(Long lelangId) {
        return Query.count("SELECT count(distinct eva_versi) FROM ekontrak.evaluasi WHERE lls_id=?", Integer.class, lelangId);
    }

    public static EvaluasiPl findBy(Long lelangId, JenisEvaluasi jenis, Integer versi) {
        return find("lls_id=? and eva_versi=? and eva_jenis=?", lelangId, versi, jenis).first();
    }

    /**
     * dapatkan evaluasi saat ini dari sebuah lelang
     * @param lelangId
     * @param jenis
     * @return
     */
    public static EvaluasiPl findBy(Long lelangId, JenisEvaluasi jenis) {
        return Query.find("SELECT * FROM ekontrak.evaluasi WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1", EvaluasiPl.class, lelangId, jenis).first();
    }

    /**
     * dapatkan evaluasi administrasi terakhir tertentu dari sebuah lelang
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findAdministrasi(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_ADMINISTRASI);
    }

    /**
     * dapatkan evaluasi Teknis terakhir tertentu dari sebuah lelang
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findTeknis(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_TEKNIS);
    }

    /**
     * dapatkan evaluasi Harga terakhir tertentu dari sebuah lelang
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findHarga(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_HARGA);
    }

    /**
     * dapatkan evaluasi Kualifikasi terakhir tertentu dari sebuah lelang
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findKualifikasi(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_KUALIFIKASI);
    }

    /**
     * dapatkan Pembuktian Kualifikasi terakhir tertentu dari sebuah lelang
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findPembuktian(Long lelangId) {
        return find("lls_id=? and eva_versi=? and eva_jenis=?", lelangId, findCurrentVersi(lelangId), JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI).first();
    }

    /**
     * dapatkan evaluasi Akhir (Penetapan Pemenang) terakhir tertentu dari sebuah non tender
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findPenetapanPemenang(Long lelangId) {
        return findBy( lelangId, JenisEvaluasi.EVALUASI_AKHIR);
    }
    /**
     * dapatkan evaluasi Administrasi , jika tidak ada maka akan dibuat baru
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findNCreateAdministrasi(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_ADMINISTRASI);
    }

    /**
     * dapatkan evaluasi Teknis , jika tidak ada maka akan dibuat baru
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findNCreateTeknis(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_TEKNIS);
    }

    /**
     * dapatkan evaluasi Harga , jika tidak ada maka akan dibuat baru
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findNCreateHarga(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_HARGA);
    }

    /**
     * dapatkan evaluasi Kualifikasi , jika tidak ada maka akan dibuat baru
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findNCreateKualifikasi(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_KUALIFIKASI);
    }
    
    

    /**
     * dapatkan pembuktian kualifikasi , jika tidak ada maka akan dibuat baru
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findNCreatePembuktian(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI);
    }
    
    
    /**
     * dapatkan evaluasi akhir , jika tidak ada maka akan dibuat baru
     * @param lelangId
     * @return
     */
    public static EvaluasiPl findNCreatePenetapanPemenang(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_AKHIR);
    }

    /**
     * dapatkan list evaluasi tertentu dari sebuah lelang
     * @param lelangId
     * @param jenis
     * @return
     */
    public static List<EvaluasiPl> findListBy(Long lelangId, JenisEvaluasi jenis) {
        return find("lls_id=? and eva_jenis=? order by eva_versi asc", lelangId, jenis).fetch();
    }

    /**
     * hasil evaluasi akan muncul jika sudah terdapat pemenang (return true)
     *
     * @param lelangId
     * @since 3.2.4
     */
    public static boolean isShowHasilEvaluasi(Long lelangId){
        Tahap[] tahaps = new Tahap[] { Tahap.PENGUMUMAN_PEMENANG_AKHIR, Tahap.PENUNJUKAN_PEMENANG, Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR };
        if(Aktivitas.isTahapStarted(lelangId, tahaps))
        {
            EvaluasiPl evaluasi = findPenetapanPemenang(lelangId);
            return (evaluasi != null && evaluasi.eva_status.isSelesai());
        }
        return false;
    }

    /**
     *  hasil evaluasi kualifkasi akan muncul jika sudah terdapat pemenang (return true)
     * @param lelangId
     * @since 3.2.4
     */
    public static boolean isShowHasilEvaluasiKualifikasi(Long lelangId){
        Tahap[] tahaps = new Tahap[] { Tahap.PENGUMUMAN_HASIL_PRA , Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK};
        if(AktivitasPl.isTahapStarted(lelangId, tahaps)) {
            EvaluasiPl evaluasi = findKualifikasi(lelangId);
            return (evaluasi != null && evaluasi.eva_status.isSelesai());
        }
        return false;

    }

    /**
     *  hasil evaluasi teknis akan muncul jika sudah terdapat pemenang (return true)
     * @param lelangId
     * @since 3.2.4
     */
    public static boolean isShowHasilEvaluasiTeknis(Long lelangId){
        if(AktivitasPl.isTahapStarted(lelangId, Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS)) {
            EvaluasiPl evaluasi = findTeknis(lelangId);
            return (evaluasi != null && evaluasi.eva_status.isSelesai());
        }
        return false;

    }

    /**
     * dapatkan list peserta yang lolos sampai evaluasi tertentu
     * @return
     */
    public List<NilaiEvaluasiPl> findPesertaList(boolean sortHarga) {
        if(sortHarga) {
            return NilaiEvaluasiPl.findWithUrutHarga(eva_id);
        }
        return NilaiEvaluasiPl.findBy(eva_id);
    }

    public NilaiEvaluasiPl findNilaiEvaluasiByPesertaAndEvaluasi(Long psr_id) {

        return NilaiEvaluasiPl.findByPesertaAndEvaluasi(psr_id, eva_id);
    }

    public List<NilaiEvaluasiPl> findPesertaLulusEvaluasiList() {
		return NilaiEvaluasiPl.find("eva_id=? AND nev_lulus=1", eva_id).fetch();
	}
    
    public List<NilaiEvaluasiPl> findPemenangList() {
        return NilaiEvaluasiPl.findByWithNevUrut(eva_id);
    }

//    public Integer countPesertaList() {
//        List<NilaiEvaluasiPl> nilai_evaluasiList = NilaiEvaluasiPl.findBy(eva_id);
//        return nilai_evaluasiList.size();
//    }
    public long countPesertaList() {
        return NilaiEvaluasiPl.count("eva_id=?", eva_id);
    }

    public List<NilaiEvaluasiPl> findPesertaList() {
        return findPesertaList(false);
    }

    /**
     * simpan nilai evaluasi peserta
     * @param peserta
     * @param nilai
     * @param pascakualifikasi
     */
    public void simpanNilaiPeserta(PesertaPl peserta, NilaiEvaluasiPl nilai, boolean pascakualifikasi) {
	    	nilai.psr_id = peserta.psr_id;
	        nilai.eva_id = eva_id;
	        if (nilai.nev_lulus != null && nilai.nev_lulus.isLulus()) {
	            nilai.nev_uraian = null;
	        }
	        nilai.save();
	        
		        EvaluasiPl evaluasi_next = findNextEvaluasi(pascakualifikasi);
		        if(nilai.nev_lulus.isLulus()) { // peserta diijinkan melanjutkan ke proses evaluasi selanjutnnya
		            NilaiEvaluasiPl nilai_next = NilaiEvaluasiPl.findNCreateBy(evaluasi_next.eva_id, peserta, evaluasi_next.eva_jenis);
		            if(evaluasi_next.eva_jenis.isAkhir()) {
		                EvaluasiPl evaluasi_harga = EvaluasiPl.findHarga(peserta.lls_id);
		                NilaiEvaluasiPl nilaiHarga = NilaiEvaluasiPl.findBy(evaluasi_harga.eva_id, peserta.psr_id);
		                nilai_next.nev_harga_terkoreksi = nilaiHarga.nev_harga_terkoreksi;
		            }
		            nilai_next.save();
		        } else { //tidak lulus maka data harus dihapus
		            ChecklistEvaluasiPl.delete("nev_id in (select nev_id from ekontrak.nilai_evaluasi where eva_id=? and psr_id=?)", evaluasi_next.eva_id, peserta.psr_id);
		            NilaiEvaluasiPl.delete("eva_id=? and psr_id=?", evaluasi_next.eva_id,peserta.psr_id);
		        }
    }
    
    public void simpanNilaiPesertaPl(PesertaPl peserta, NilaiEvaluasiPl nilai, boolean pascakualifikasi) {
    	nilai.psr_id = peserta.psr_id;
        nilai.eva_id = eva_id;
        if (nilai.nev_lulus != null && nilai.nev_lulus.isLulus()) {
            nilai.nev_uraian = null;
        }      
        nilai.save();
	        EvaluasiPl evaluasi_next = findNextEvaluasi(!pascakualifikasi);
	        if(nilai.nev_lulus.isLulus()) { // peserta diijinkan melanjutkan ke proses evaluasi selanjutnnya
	            NilaiEvaluasiPl nilai_next = NilaiEvaluasiPl.findNCreateBy(evaluasi_next.eva_id, peserta, evaluasi_next.eva_jenis);	            
	            nilai_next.nev_skor = nilai.nev_skor;	            	
	            if(evaluasi_next.eva_jenis.isAkhir()) {
	                EvaluasiPl evaluasi_harga = EvaluasiPl.findHarga(peserta.lls_id);
	                NilaiEvaluasiPl nilaiHarga = NilaiEvaluasiPl.findBy(evaluasi_harga.eva_id, peserta.psr_id);
	                nilai_next.nev_harga_terkoreksi = nilaiHarga.nev_harga_terkoreksi;
	            }
	            nilai_next.save();
	        } else { //tidak lulus maka data harus dihapus
	            ChecklistEvaluasiPl.delete("nev_id in (select nev_id from ekontrak.nilai_evaluasi where eva_id=? and psr_id=?)", evaluasi_next.eva_id, peserta.psr_id);
	            NilaiEvaluasiPl.delete("eva_id=? and psr_id=?", evaluasi_next.eva_id,peserta.psr_id);
	        }
    }

    // evaluasi ulang flow
    public static void evaluasiUlang(Long lelangId)
    {
        Integer currentVersi = findCurrentVersi(lelangId);
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", lelangId);
        params.put("versi", currentVersi);
        List<EvaluasiPl> evaluasiList = find("lls_id=? AND eva_versi=? ORDER BY eva_jenis ASC", lelangId, currentVersi).fetch(); //list evaluasi saat panitia sebelum melakukan evaluasi ulang
        EvaluasiPl evaluasiUlang = null;
        Pl_seleksi lelang = Pl_seleksi.findById(lelangId);
        /*if(lelang.isPascakualifikasi())
        {
            for(Evaluasi evaluasi : evaluasiList)
            {
//				if(evaluasi.getEva_wf_id() != null)
//				{
//					EpnsWorkflow workflow = epnsWorkflowDao.findByPrimaryKey(evaluasi.getEva_wf_id());
//					workflow.setState(State.FINISHED);
//					epnsWorkflowDao.update(workflow);
//				}
                evaluasiUlang = new Evaluasi();
                evaluasiUlang.lls_id = evaluasi.lls_id;
                evaluasiUlang.eva_jenis = evaluasi.eva_jenis;
                evaluasiUlang.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
                evaluasiUlang.eva_versi = evaluasi.eva_versi + 1;
                evaluasiUlang.eva_tgl_setuju = evaluasi.eva_tgl_setuju;
                evaluasiUlang.save();
            }
        }
        else if(lelang.isPrakualifikasi())
        {
            List<Nilai_evaluasi> ne = null;
            Evaluasi evaUl = null;
            Nilai_evaluasi nilaiEvaUlang=null;
            for(Evaluasi evaluasi : evaluasiList)
            {
//				if(evaluasi.getEva_wf_id() != null)
//				{
//						EpnsWorkflow workflow = epnsWorkflowDao.findByPrimaryKey(evaluasi.getEva_wf_id());
//						workflow.setState(State.FINISHED);
//						epnsWorkflowDao.update(workflow);
//				}
                evaluasiUlang = new Evaluasi();
                evaluasiUlang.lls_id = evaluasi.lls_id;
                evaluasiUlang.eva_jenis = evaluasi.eva_jenis;
                evaluasiUlang.eva_status = evaluasi.eva_status;
                Integer evaVersi = evaluasi.eva_versi+ 1;
                evaluasiUlang.eva_versi = evaVersi;
                evaluasiUlang.eva_tgl_setuju = evaluasi.eva_tgl_setuju;
                evaluasiUlang.save();
                if(evaluasi.eva_jenis.isKualifikasi()){
                    ne = evaluasi.findPesertaList();
                    evaUl = findBy(lelangId, JenisEvaluasi.EVALUASI_KUALIFIKASI, evaVersi);
                    for(Nilai_evaluasi nilaiEva:ne)
                    {
                        nilaiEvaUlang = new Nilai_evaluasi();
                        nilaiEvaUlang.psr_id = nilaiEva.psr_id;
                        nilaiEvaUlang.nev_uraian = nilaiEva.nev_uraian;
                        nilaiEvaUlang.nev_lulus = nilaiEva.nev_lulus;
                        nilaiEvaUlang.eva_id = evaUl.eva_id;
                        nilaiEvaUlang.save();
                    }
                }
            }
        }*/
    }

    public List<NilaiEvaluasiPlView> findAllNilaiEvaluasiWithUrutHarga() {
        return NilaiEvaluasiPlView.findAllByEvaluasiWithUrutHarga(eva_id);
    }

    /**
     * Mendapatkan Informasi next evaluasi setelah satu evaluasi selesai dilakukan
     *
     * @param pascakualifikasi
     * @return
     */
    public EvaluasiPl findNextEvaluasi(boolean pascakualifikasi) {
        EvaluasiPl evaluasi_next = null;
        if(pascakualifikasi) {
            switch (eva_jenis) {
                case EVALUASI_ADMINISTRASI:evaluasi_next = EvaluasiPl.findNCreateKualifikasi(lls_id);break;
                case EVALUASI_KUALIFIKASI:evaluasi_next = EvaluasiPl.findNCreateTeknis(lls_id);break;
                case EVALUASI_TEKNIS:evaluasi_next = EvaluasiPl.findNCreateHarga(lls_id);break;
                case EVALUASI_HARGA:evaluasi_next = EvaluasiPl.findNCreatePenetapanPemenang(lls_id);break;
            }

        } else { // lelang prakualifikasi
            switch (eva_jenis) {
                case EVALUASI_KUALIFIKASI:evaluasi_next = EvaluasiPl.findNCreatePembuktian(lls_id);break;
                case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = EvaluasiPl.findNCreateAdministrasi(lls_id);break;
                case EVALUASI_ADMINISTRASI:evaluasi_next = EvaluasiPl.findNCreateTeknis(lls_id);break;
                case EVALUASI_TEKNIS:evaluasi_next = EvaluasiPl.findNCreateHarga(lls_id);break;
                case EVALUASI_HARGA:evaluasi_next = EvaluasiPl.findNCreatePenetapanPemenang(lls_id);break;
            }
        }
        return evaluasi_next;
    }


    /**
     * langsung join dengan table peserta dan rekanan, agar tidak manual attach ketika looping
     * @return
     */
    public List<NilaiEvaluasiPlView> findAllNilaiEvaluasi() {
        return NilaiEvaluasiPlView.findAllByEvaluasi(eva_id);
    }

    public void deleteNextEvaluasi(EvaluasiPl evaluasi, Long psr_id) {
		EvaluasiPl evaluasi_next = null;
            switch (evaluasi.eva_jenis) {
//                case PEMBUKTIAN_KUALIFIKASI:evaluasi_next = EvaluasiPl.findNCreateAdministrasi(lls_id);break;
                case EVALUASI_ADMINISTRASI:evaluasi_next = EvaluasiPl.findKualifikasi(lls_id);break;
                case EVALUASI_KUALIFIKASI:evaluasi_next = EvaluasiPl.findTeknis(lls_id);break;
                case EVALUASI_TEKNIS:evaluasi_next = EvaluasiPl.findHarga(lls_id);break;
                case EVALUASI_HARGA:evaluasi_next = EvaluasiPl.findPenetapanPemenang(lls_id);break;
            }

		if(evaluasi_next != null) {
			deleteNextEvaluasi(evaluasi_next, psr_id); // recursive
		}
		ChecklistEvaluasiPl.delete("nev_id in (select nev_id from ekontrak.nilai_evaluasi where eva_id=? and psr_id=?)", evaluasi.eva_id, psr_id);
        NilaiEvaluasiPl.delete("eva_id=? and psr_id=?", evaluasi.eva_id, psr_id);
	}

}
