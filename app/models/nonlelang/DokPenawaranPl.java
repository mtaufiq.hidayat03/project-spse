package models.nonlelang;

import controllers.BasicCtr;
import ext.FormatUtils;
import models.common.Tahap;
import models.common.UploadInfo;
import models.handler.DokPenawaranPlDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.LDPContent;
import models.nonlelang.contracts.PenawaranContract;
import models.rekanan.Pemilik;
import models.rekanan.Pengurus;
import models.rekanan.Rekanan;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 2/21/2017.
 */

@Table(name="EKONTRAK.DOK_PENAWARAN")
public class DokPenawaranPl extends BaseModel implements PenawaranContract {

    @Enumerated(EnumType.ORDINAL)
    public enum JenisDokPenawaran {
        PENAWARAN_KUALIFIKASI(0, "Dokumen Penawaran Kualifikasi"),
        PENAWARAN_TEKNIS_ADMINISTRASI(1, "Dokumen Penawaran Administrasi dan Teknis"),
        PENAWARAN_HARGA(2, "Dokumen Penawaran Harga"),
        PENAWARAN_TEKNIS_ADMINISTRASI_HARGA(3, "Dokumen Penawaran Administrasi, Teknis, dan Harga"),
        PENAWARAN_TAMBAHAN(4, "Dokumen Penawaran Tambahan"),
        PENAWARAN_SUSULAN(5, "Dokumen Penawaran Susulan"),
        PENAWARAN_KUALIFIKASI_TAMBAHAN(6,"Dokumen Penawaran Kualifikasi Tambahan"),
        PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT(7,"Dokumen Penawaran Kualifikasi Tambahan Draft");

        public final Integer id;

        public final String label;

        JenisDokPenawaran(int key, String label) {
            this.id = Integer.valueOf(key);
            this.label = label;
        }

        public static JenisDokPenawaran fromValue(Integer value) {
            JenisDokPenawaran jenis = null;
            if (value != null) {
                switch (value.intValue()) {
                    case 0:
                        jenis = PENAWARAN_KUALIFIKASI;
                        break;
                    case 1:
                        jenis = PENAWARAN_TEKNIS_ADMINISTRASI;
                        break;
                    case 2:
                        jenis = PENAWARAN_HARGA;
                        break;
                    case 3:
                        jenis = PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
                        break;
                    case 4:
                        jenis = PENAWARAN_TAMBAHAN;
                        break;
                    case 5:
                        jenis = PENAWARAN_SUSULAN;
                        break;
                    case 6:
                        jenis = PENAWARAN_KUALIFIKASI_TAMBAHAN;
                        break;
                }
            }
            return jenis;
        }

        public boolean isPenawaran() {
            return this == PENAWARAN_HARGA || this == PENAWARAN_TEKNIS_ADMINISTRASI || this == PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
        }
        public boolean isAdmTeknisHarga(){
            return this == PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
        }
        public boolean isAdmTeknis() {
            return this == PENAWARAN_TEKNIS_ADMINISTRASI;
        }
        public boolean isHarga() {
            return this == PENAWARAN_HARGA;
        }
    }

    @Id(sequence="ekontrak.seq_dok_penawaran", function="nextsequence")
    public Long dok_id;

    public String dok_judul;

    public Date dok_tgljam;

    public Date dok_waktu_buka;

    public String dok_hash;

    public String dok_signature;

    public Long dok_id_attachment;

    public Integer dok_waktu; // jangka waktu penawaran

    public String dok_surat; // perlu evaluasi, apakah menyimpan surat penawaran dengan cara ini adalah yang terbaik

    public JenisDokPenawaran dok_jenis;

    public Integer dok_disclaim;

    //relasi kek Peserta
    public Long psr_id;

    public Long chk_id;

    @Transient
    public BlobTable blobTable;


    @Override
    public Long getDocIdAttachment() {
        return this.dok_id_attachment;
    }

    @Override
    public BlobTable getBlobTable() {
        return this.blobTable;
    }

    @Transient
    public BlobTable getBlob() {
        if (dok_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dok_id_attachment);
    }

    @Override
    public void setBlob(BlobTable model) {
        blobTable = model;
    }

    @Transient
    public String getDownloadUrl() {
        return getBlob() != null ? getBlob().getDownloadUrl(DokPenawaranPlDownloadHandler.class) : null;
    }

    @Transient
    private PesertaPl peserta;

    public PesertaPl getPeserta() {
        if(peserta == null)
            peserta = PesertaPl.findById(psr_id);
        return peserta;
    }

    @Transient
    private ChecklistPl checklistPl;

    public ChecklistPl getChecklistPl() {
        if(checklistPl == null)
            checklistPl = ChecklistPl.findById(chk_id);
        return checklistPl;
    }

    @Transient
    public boolean isPermission() {
        if(dok_jenis == JenisDokPenawaran.PENAWARAN_KUALIFIKASI)
            return true;
        if(dok_id_attachment != null)
        {
            BlobTable blob = BlobTableDao.getLastById(dok_id_attachment);
            if(dok_jenis == JenisDokPenawaran.PENAWARAN_TAMBAHAN)
                return blob.blb_versi > 0;
            if(dok_jenis == JenisDokPenawaran.PENAWARAN_SUSULAN)
            {
                if (dok_disclaim != null && dok_disclaim == 1)
                    return blob.blb_versi > 0;
            }
            else
            {
                if (dok_disclaim != null && dok_disclaim != 0)
                    return blob.blb_versi > 0;
            }
        }
        return false;
    }

    @Transient
    public String getTglFile()
    {
        if(dok_tgljam!=null && dok_disclaim != null){
            return FormatUtils.formatDateTimeInd(dok_tgljam);
        }
        return "";
    }

    /**
     * digunakan untuk menampilkan dokumen kualifikasi (hanya untuk penawaran)
     * @return
     */
    @Transient
    public BlobTable getDokumen() {
        if(dok_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dok_id_attachment);
    }

    /**
     * digunakan untuk menampilkan list dokumen kualifikasi lainnya, kualifikasi lainnya
     * @return
     */
    @Transient
    public List<BlobTable> getDokumenList() {
        if(dok_id_attachment == null)
            return null;
        return BlobTableDao.listById(dok_id_attachment);
    }

    @Transient
    public String getDokName(){
        BlobTable dok = getDokumen();
        if( dok != null){
            Long size = dok.blb_ukuran;
            return "<span title=\"" + FormatUtils.formatFileSize(size) + "\">" +
                    dok_judul + " (" + FormatUtils.formatFileSize(size) + ")</span>";
        }
        return "";
    }

    @Transient
    public String getDokUrl() {
        return getDokumen().getDownloadUrl(DokPenawaranPlDownloadHandler.class);
    }

    @Transient
    public boolean isAllowed()
    {
        if(this == null || getDokumen() == null)
            return false;
        boolean allowed = this != null && dok_disclaim != null && dok_disclaim.equals(Integer.valueOf(1));
        Pl_seleksi lelang = Pl_seleksi.find("lls_id in (select lls_id from ekontrak.peserta_nonlelang where psr_id=?)", psr_id).first();

        return allowed;
    }

    @Transient
    public boolean isShowUpload()
    {
        if (dok_disclaim != null && dok_disclaim.equals(Integer.valueOf(0)))
            return false;
        else
            return true;
    }

    @Transient
    public boolean isShowDisclaimer()
    {
        if (dok_id_attachment != null){
            BlobTable blob = BlobTableDao.getLastById(dok_id_attachment);
            return blob != null && !blob.blb_hash.equals(dok_hash);
        }
        return false;
    }

    @Transient
    public String getInfo(boolean lelangv3) {
        if (dok_id_attachment != null) {
            BlobTable blob = BlobTableDao.getLastById(dok_id_attachment);
            if (blob != null) {
                if(lelangv3) {
                    return "<table><tr><td>"+ Messages.get("nonlelang.sdp") +" </td><td>: " + FormatUtils.formatDateInd(blob.blb_date_time)
                            + "</td></tr><tr><td>"+ Messages.get("nonlelang.nama_file")+" </td><td>: " + blob.getFileName()
                            + "</td></tr><tr><td>"+Messages.get("nonlelang.ukuran")+" </td><td>: " + FormatUtils.formatFileSize(blob.blb_ukuran)
                            + "</td></tr><tr><td>Hash </td><td>: " + blob.blb_hash + "</td></tr></table>";
                } else {
                    return "<table><tr><td>"+ Messages.get("nonlelang.sdp") +" </td><td>: " + FormatUtils.formatDateTimeInd(blob.blb_date_time)
                            + "</td></tr><tr><td>"+ Messages.get("nonlelang.masa_berlaku") +"</td><td>: " + dok_waktu +Messages.get("nonlelang.hk")+ " </td></tr></table>";
                }
            }
        }

        Pl_seleksi lelang = Pl_seleksi.find("lls_id in (select lls_id from ekontrak.peserta_nonlelang where psr_id=?", psr_id).first();

        Tahap tahap = Tahap.PEMASUKAN_PENAWARAN; // dua file atau satu file, tahap upload ada di Tahap.PEMASUKAN_PENAWARAN

        Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(lelang.lls_id, tahap);
        String tglAwal = "";
        String tglAkhir = "";
        if (jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
            tglAwal = FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal);
            tglAkhir = FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir);
            return Messages.get("nonlelang.bk_wp") + tglAwal + " s.d. " + tglAkhir;
        }
        return Messages.get("nonlelang.bk");
    }

    public static List<DokPenawaranPl> findPenawaranPeserta(Long pesertaId) {
        return find("psr_id=? and dok_jenis in ('"+JenisDokPenawaran.PENAWARAN_HARGA.id+"','"+JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id+"','"+JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id+"')", pesertaId).fetch();
    }

    public static DokPenawaranPl findByPesertaAndJenis(Long pesertaId, JenisDokPenawaran jenis) {
        return find("psr_id=? and dok_jenis =?", pesertaId, jenis).first();
    }

    public static DokPenawaranPl findPenawaranPeserta(Long pesertaId, JenisDokPenawaran jenis) {
        return find("psr_id=? and dok_jenis=?", pesertaId, jenis).first();
    }

    public static List<DokPenawaranPl> findListPenawaranPeserta(Long pesertaId, JenisDokPenawaran jenis) {
        return find("psr_id=? and dok_jenis=? ORDER BY dok_tgljam DESC", pesertaId, jenis).fetch();
    }

    public static List<DokPenawaranPl> findAllPenawaranLelang(Long lelangId, JenisDokPenawaran jenis)
    {
        return find("psr_id in (select psr_id from ekontrak.peserta_nonlelang where lls_id=?) and dok_jenis=? order by psr_id", lelangId, jenis).fetch();
    }

    public static Map<String, Object> simpanDokPenawaran(Long pesertaId, Long lls_id, File file, Long chk_id, JenisDokPenawaran dok_jenis) throws Exception {

        /*DokPenawaranPl dok_penawaran = DokPenawaranPl.findTeknisByPesertaAndChecklist(pesertaId, chk_id);

        if (dok_penawaran == null) {
            dok_penawaran = new DokPenawaranPl();
            dok_penawaran.psr_id = pesertaId;
            dok_penawaran.dok_jenis = JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;
        }*/

        DokPenawaranPl dok_penawaran = new DokPenawaranPl();
        dok_penawaran.psr_id = pesertaId;
        dok_penawaran.dok_jenis = dok_jenis;

        Dok_pl dok_pl = Dok_pl.findBy(lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);

        int masa_berlaku_penawaran = 0;

        //get masa berlaku penawaran
        if (dok_pl != null){

            Dok_pl_content dok_pl_content = Dok_pl_content.findByLdp(dok_pl.dll_id);

            LDPContent ldpcontent = dok_pl_content.ldpcontent;

            masa_berlaku_penawaran = ldpcontent.masa_berlaku_penawaran;

        }

        BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);

        dok_penawaran.dok_id_attachment = blob.blb_id_content;
        dok_penawaran.dok_disclaim = Integer.valueOf(1);
        dok_penawaran.dok_tgljam = controllers.BasicCtr.newDate();
        dok_penawaran.dok_judul = blob.blb_nama_file;
        dok_penawaran.dok_hash = blob.blb_hash;
        dok_penawaran.chk_id = chk_id;
        dok_penawaran.dok_waktu = masa_berlaku_penawaran;

        dok_penawaran.save();

        Map<String, Object> res = new HashMap<>();

        res.put("file", UploadInfo.findBy(blob));
        res.put("dok_id", dok_penawaran.dok_id);
        res.put("chk_id", dok_penawaran.chk_id);

        return res;

    }

    @Transient
    public String getNameFromBlobTable() {
        String name = "no name";
        if (getBlob() != null) {
            try {
                String[] ls = BaseModel.decrypt(this.getBlob().blb_path).split("\\n");
                name = ls[2];
            } catch (Exception e) {
            }
        }
        return name;
    }

    public static DokPenawaranPl findByChecklist(Long chk_id){

        return find("chk_id=?", chk_id).first();

    }

    public static List<DokPenawaranPl> findListPenawaranByChecklist(Long chk_id){

        return find("chk_id=?", chk_id).fetch();

    }

    public static DokPenawaranPl findTeknisByPesertaAndChecklist(Long psr_id, Long chk_id){

        return find("psr_id=? AND chk_id=? AND dok_jenis=1", psr_id, chk_id).first();

    }

    public static List<DokPenawaranPl> findByPesertaAndChecklistAndJenis(Long psr_id, Long chk_id, Integer dok_jenis){

        return find("psr_id=? AND chk_id=? AND dok_jenis=?", psr_id, chk_id, dok_jenis).fetch();

    }



    public static List<DokPenawaranPl> findByPesertaAndChecklist(Long psr_id, Long chk_id){

        return find("psr_id=? AND chk_id=?", psr_id, chk_id).fetch();

    }

    public static void simpanPenawaran(Long pesertaId, JenisDokPenawaran jenis, File file, Integer period) throws Exception
    {
        simpanPenawaran(pesertaId, jenis, file, period, null);
    }

    public static String simpanPenawaran(Long pesertaId, JenisDokPenawaran jenis, File file, Integer period, String surat) throws Exception
    {
        DokPenawaranPl dok_penawaran = findPenawaranPeserta(pesertaId, jenis);
        if (dok_penawaran == null) {
            dok_penawaran = new DokPenawaranPl();
            dok_penawaran.psr_id = pesertaId;
            dok_penawaran.dok_jenis = jenis;
        }
        if(period!=null){
            dok_penawaran.dok_waktu = period;
        } else {
            DokPenawaranPl dok_penawaran_jenis_1 = findPenawaranPeserta(pesertaId, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
            if (dok_penawaran_jenis_1 != null)
                dok_penawaran.dok_waktu = dok_penawaran_jenis_1.dok_waktu;
        }
        if(surat!=null && !surat.isEmpty())
            dok_penawaran.dok_surat = surat.replaceAll("\n", "");
        BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);
        dok_penawaran.dok_id_attachment = blob.blb_id_content;
		/*if(dok_penawaran.dok_disclaim == null || dok_penawaran.dok_disclaim != Integer.valueOf(1))
			dok_penawaran.dok_disclaim = Integer.valueOf(0);*/
        dok_penawaran.dok_disclaim = Integer.valueOf(1);
        dok_penawaran.dok_tgljam = controllers.BasicCtr.newDate();
        dok_penawaran.dok_judul = blob.blb_nama_file;
        dok_penawaran.dok_hash = blob.blb_hash;
        dok_penawaran.save();
        return blob.blb_hash;
    }
    /**
     * simpan dokumen kualifikasi peserta
     * @param peserta
     */
    public static void simpanKualifikasi(PesertaPl peserta) {
        DokPenawaranPl dok = findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
        if (dok == null) {
            dok = new DokPenawaranPl();
            dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI;
        }
        dok.psr_id = peserta.psr_id;
        dok.dok_tgljam = BasicCtr.newDate();
        dok.dok_judul = "Dok Prakualifikasi.xls";
        dok.dok_id_attachment = Long.valueOf(0);
        dok.save();
        List<Pemilik> list_pemilik = Pemilik.find("rkn_id=?", peserta.rkn_id).fetch();
        List<Pengurus> list_pengurus = Pengurus.find("rkn_id=?", peserta.rkn_id).fetch();
        Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
        peserta.psr_identitas = CommonUtil.toJson(rekanan);
        peserta.psr_pemilik = CommonUtil.toJson(list_pemilik);
        peserta.psr_pengurus = CommonUtil.toJson(list_pengurus);
        peserta.save();
    }
        
    /**
     * simpan dokumen kualifikasi peserta
     * @param pesertaId
     * @param blob
     */
    public static void simpanKualifikasiLainnyaDraft(Long pesertaId, BlobTable blob, Date currentDate) {
    		DokPenawaranPl dok = findPenawaranPeserta(pesertaId, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
        if (dok == null) {
            dok = new DokPenawaranPl();
            dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT;
        }
        dok.psr_id = pesertaId;
        dok.dok_tgljam = currentDate;
        dok.dok_judul = "Draft Dok Kualifikasi lainnya.xls";
        dok.dok_id_attachment = blob.blb_id_content;
        dok.dok_disclaim = Integer.valueOf(1);
        dok.save();
    }
    
    public static void simpanKualifikasiLainnya(PesertaPl peserta) {
	    	try {
	    		DokPenawaranPl dok_draft = findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
	        if (dok_draft != null) {
	        		DokPenawaranPl dok = findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN);
	            if (dok == null) {
	                dok = new DokPenawaranPl();
	                dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN;
	                dok.dok_judul = "Dok Kualifikasi lainnya.xls";
	                dok.dok_id_attachment = getNextSequenceValue("seq_epns");
	                dok.dok_disclaim = Integer.valueOf(1);
	                dok.psr_id = peserta.psr_id;
	            }
	            dok.dok_tgljam = BasicCtr.newDate();
	            dok.save();
	            Integer versi = Integer.valueOf(0);
	            BlobTable blob = null; 
	            List<BlobTable> list = BlobTableDao.listById(dok_draft.dok_id_attachment);
	            if (list != null) {
		            for(BlobTable blb: list) {
		            		blob = BlobTableDao.getLastById(dok.dok_id_attachment);
		            		if (blob != null)
			            		versi = blob.blb_versi + 1;
		            		String sql="UPDATE blob_table SET blb_id_content=?, blb_versi=? where blb_id_content = ? and blb_versi= ?";
		            		Query.update(sql, dok.dok_id_attachment, versi, blb.blb_id_content, blb.blb_versi);
		            }
		        }
	        }
	    	} catch (Exception e){
	    		Logger.error(e, Messages.get("nonlelang.sdklg")+" %s", e.getMessage());
	    	}
	}

    /**
     * simpan dokumen penawaran latihan
     * @param pesertaId
     * @param jenis
     * @param currentDate
     * @throws Exception
     *//*
	public void simpanPenawaranLatihan(Long pesertaId, JenisDokPenawaran jenis) throws Exception{
		Dok_penawaran dok_penawaran = findPenawaranPeserta(pesertaId, jenis);
		if (dok_penawaran == null) {
			dok_penawaran = new Dok_penawaran();
			dok_penawaran.psr_id = pesertaId;
			dok_penawaran.dok_jenis = jenis;
		}
		File file = new File("dok_penawaran.rhs");
		BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);
		dok_penawaran.dok_id_attachment = blob.blb_id_content;
		dok_penawaran.dok_disclaim = Integer.valueOf(1);
		dok_penawaran.dok_tgljam = controllers.BasicCtr.newDate();
		dok_penawaran.dok_judul = blob.blb_nama_file;
		dok_penawaran.dok_hash = blob.blb_hash;
		dok_penawaran.save();
	}*/

    /**
     * menampilkan daftar dokumen penawaran
     * @param rekananId
     */
    public static List<DokPenawaranPl> getListDokumen (Long rekananId){
        return find("dok_id in (select dok_id from ekontrak.dok_penawaran d, ekontrak.peserta_nonlelang p, rekanan r where d.psr_id=p.psr_id and p.rkn_id=r.rkn_id and r.rkn_id=?)", rekananId).fetch();
    }

    public Long lelangId(){
        return Query.find("select lls_id from ekontrak.dok_penawaran d, ekontrak.peserta_nonlelang p where d.psr_id=p.psr_id and p.psr_id=?", Long.class, psr_id).first();

    }

    public static UploadInfo simpanSusulan(PesertaPl peserta, File file, Date currentDate) throws Exception {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        File newFile = new File(path);
        file.renameTo(newFile);
        DokPenawaranPl dok_penawaran = findPenawaranPeserta(peserta.psr_id,JenisDokPenawaran.PENAWARAN_SUSULAN);
        if (dok_penawaran == null){
            dok_penawaran = new DokPenawaranPl();
            dok_penawaran.dok_jenis = JenisDokPenawaran.PENAWARAN_SUSULAN;
            dok_penawaran.psr_id = peserta.psr_id;
            dok_penawaran.dok_tgljam = currentDate;
            dok_penawaran.dok_judul = "DokPlkualifikasiSusulan.xls";
        }
        BlobTable blob = null;
        if (dok_penawaran.dok_id_attachment != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dok_penawaran.dok_id_attachment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dok_penawaran.dok_id_attachment = blob.blb_id_content;
        dok_penawaran.dok_disclaim = Integer.valueOf(1);
        dok_penawaran.save();
        return UploadInfo.findBy(blob);
    }


    public static boolean isAnyPenawaran(Long lelangId, Long rekananId) {
        return count("psr_id in (select psr_id from ekontrak.peserta_nonlelang where lls_id=? and rkn_id=?) and dok_jenis in (0,1,2,3)", lelangId, rekananId) > 0;
    }

    @Transient
    public String getTglKirimWithoutTime()
    {
        BlobTable dokumen = getDokumen();
        if(dokumen != null){
            return FormatUtils.formatDateInd(dokumen.blb_date_time);
        }
        return null;
    }


    public void deleteModelAndBlob() {
        withDocumentBlob();
        deleteBlobDocument();
        delete();
    }

}
