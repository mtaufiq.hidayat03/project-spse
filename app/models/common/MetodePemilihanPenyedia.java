package models.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

/**
 * Metode pengadaan dari SIRUP
 */
@Enumerated(EnumType.ORDINAL)
public enum MetodePemilihanPenyedia {

    BELUM_DITENTUKAN(0,"mp.blm_dtntkn"),
    LELANG_UMUM(1, "mp.umm"),
    LELANG_SEDERHANA(2, "mp.lsdrhn"),
    LELANG_TERBATAS(3, "mp.lngtrbts"),
    SELEKSI_UMUM(4, "mp.slks"),
    SELEKSI_SEDERHANA(5, "mp.slks_sdrhn"),
    PEMILIHAN_LANGSUNG(6, "mp.plhn_lgsng"),
    PENUNJUKAN_LANGSUNG(7, "mp.pnjkn_lgsng"),
    PENGADAAN_LANGSUNG(8, "mp.pgdn_lgsng"),
    E_PURCHASING(9, "mp.epcs"),
    SAYEMBARA(10, "mp.sybr"),
    KONTES(11, "mp.knts"),
    LELANG_CEPAT(12, "mp.lc"),
    TENDER(13, "mp.tndr"),
    TENDER_CEPAT(14, "mp.tc"),
    SELEKSI(15, "mp.slkss"),
    TENDER_YANG_DIKECUALIKAN(16, "mp.pngcln"),
    TENDER_KEADAAN_DARURAT(17, "mp.drt"),
    TENDER_INTERNASIONAL(18, "mp.ti");

    public final Integer id;
    public final String label;

    MetodePemilihanPenyedia(int key, String label) {
        this.id = Integer.valueOf(key);
        this.label = label;
    }

    public String getLabel()
    {
        return Messages.get(label);
    }

    public static MetodePemilihanPenyedia findById(Integer key) {
        if(key == null)
            return BELUM_DITENTUKAN;
        switch (key) {
            case 0:
                return BELUM_DITENTUKAN;
            case 1:
                return LELANG_UMUM;
            case 2:
                return LELANG_SEDERHANA;
            case 3:
                return LELANG_TERBATAS;
            case 4:
                return SELEKSI_UMUM;
            case 5:
                return SELEKSI_SEDERHANA;
            case 6:
                return PEMILIHAN_LANGSUNG;
            case 7:
                return PENUNJUKAN_LANGSUNG;
            case 8:
                return PENGADAAN_LANGSUNG;
            case 9:
                return E_PURCHASING;
            case 10:
                return SAYEMBARA;
            case 11:
                return KONTES;
            case 12:
                return LELANG_CEPAT;
            case 13:
                return TENDER;
            case 14:
                return TENDER_CEPAT;
            case 15:
                return SELEKSI;
            case 16:
                return TENDER_YANG_DIKECUALIKAN;
            case 17:
                return TENDER_KEADAAN_DARURAT;
            case 18:
                return TENDER_INTERNASIONAL;
            default:
                return BELUM_DITENTUKAN;
        }
//			return null;
    }

    public boolean isLelangExpress(){
        return this == TENDER_CEPAT;
    }

    public boolean isPenunjukanLangsung(){
        return this == PENUNJUKAN_LANGSUNG;
    }

    public boolean isPengadaanLangsung(){
        return this == PENGADAAN_LANGSUNG;
    }

    public boolean isPl(){
        return this == PENGADAAN_LANGSUNG || this == PENUNJUKAN_LANGSUNG;
    }

    public static final MetodePemilihanPenyedia[] findAll = new MetodePemilihanPenyedia[]{LELANG_UMUM, LELANG_SEDERHANA,
                LELANG_TERBATAS, SELEKSI_UMUM, SELEKSI_SEDERHANA, PEMILIHAN_LANGSUNG, PENUNJUKAN_LANGSUNG,
                PENGADAAN_LANGSUNG, E_PURCHASING, SAYEMBARA, KONTES, LELANG_CEPAT, TENDER_YANG_DIKECUALIKAN, TENDER_KEADAAN_DARURAT, TENDER_INTERNASIONAL};

    public static final MetodePemilihanPenyedia[] listMetodePemilihanTender = new MetodePemilihanPenyedia[]{TENDER, TENDER_CEPAT, SELEKSI};

    public static MetodePemilihanPenyedia[] getNonByKategori(Kategori kategori)
    {
        return listMetodePemilihanNonTender;
    }

    public static MetodePemilihanPenyedia[] getPencatatanByKategori(Kategori kategori)
    {
        return listMetodePemilihanPencatatan;
    }

    public static final MetodePemilihanPenyedia[] listMetodePemilihanPokja = new MetodePemilihanPenyedia[]{PENUNJUKAN_LANGSUNG};
    public static final MetodePemilihanPenyedia[] listMetodePemilihanNonTender = new MetodePemilihanPenyedia[]{PENUNJUKAN_LANGSUNG, PENGADAAN_LANGSUNG};
    public static final MetodePemilihanPenyedia[] listMetodePemilihanPencatatan = new MetodePemilihanPenyedia[]{PENUNJUKAN_LANGSUNG, PENGADAAN_LANGSUNG, KONTES, SAYEMBARA,TENDER_YANG_DIKECUALIKAN, TENDER_KEADAAN_DARURAT,TENDER_INTERNASIONAL};


    // konvert metode pemilihan sirup ke metode pemilihan spse, karena ada perbedaan kodifikasi
    public static MetodePemilihanPenyedia findBy(MetodePemilihan mtdPemilihanRup) {
        MetodePemilihanPenyedia metode = PENGADAAN_LANGSUNG ;
        if(mtdPemilihanRup != null) {
            switch (mtdPemilihanRup) {
                case PENGADAAN_LANGSUNG:metode = PENGADAAN_LANGSUNG;break;
                case PENUNJUKAN_LANGSUNG:metode = PENUNJUKAN_LANGSUNG;break;
                case SAYEMBARA:metode = SAYEMBARA;break;
                case KONTES:metode = KONTES;break;

            }
        }
        return metode;
    }
}
