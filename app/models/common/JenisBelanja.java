package models.common;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

/**
 * Model Jenis Belanja berdasarkan data Jenis Belanja di SIRUP
 * @author arief Ardiyansah
 *
 */
@Enumerated(EnumType.ORDINAL)
public enum JenisBelanja {

	BARANG_JASA(1, "Barang / Jasa"),
	
	MODAL(2, "Modal");
	
	public final Integer id;
	
	public final String label;
	
	private JenisBelanja(int id, String label) {
		this.id = id;
		this.label = label;
	}
	
	public static JenisBelanja findById(Integer id) {
		if(id == null)
			return null;
		else if(id.intValue() == 1)
			return BARANG_JASA;
		else if(id.intValue() == 2) 
			return MODAL;
		else
			return null;
	}
}
