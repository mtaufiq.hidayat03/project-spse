package models.common;

import play.db.jdbc.Query;

import java.util.List;

public class JenisInstansi {

	private static final String SQL="SELECT jenis, ARRAY_TO_STRING(ARRAY_AGG(nama), ', ') AS nama FROM instansi GROUP BY jenis";
	
	public String jenis;
	
	public String nama;
	
	public static List<JenisInstansi> findAll() {
		return Query.find(SQL, JenisInstansi.class).fetch();
	}
}
