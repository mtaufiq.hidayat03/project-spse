package models.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum MetodePemilihan {
		LELANG_UMUM(0,"mp.umm", "Pelelangan Umum"),
		LELANG_SEDERHANA(1, "mp.lsdrhn", "Pelelangan Sederhana"),
		LANGSUNG(2, "mp.lngsng", "Pemilihan Langsung"),
		SELEKSI_UMUM (3, "mp.slks", "Seleksi Umum"),
		SELEKSI_TERBATAS (4, "mp.slks_sdrhn", "Seleksi Sederhana"),
		SELEKSI_LANGSUNG (5, "mp.slslngsng", "Seleksi Langsung"),
		TUNJUK_LANGSUNG (6, "mp.pnjk_slsk", "Penunjukan Langsung"),
		SWAKELOLA (7, "mp.skl", "Swakelola"),
		LELANG_TERBATAS(8, "mp.lngtrbts", "Pelelangan Terbatas"),
		LELANG_EXPRESS(9,"mp.tc", "Tender Cepat"),
		SELEKSI_EXPRESS(10, "mp.sc", "E-Seleksi Cepat"),
		PENGADAAN_LANGSUNG(11,"mp.pl", "Pengadaan Langsung"), //PL
		PENUNJUKAN_LANGSUNG(12,"mp.pnjkn_lngsng", "Penunjukan Langsung"), //PL
		KONTES(13,"mp.knts", "Kontes"), //PL
		SAYEMBARA(14,"mp.sybr", "Sayembara"),
		LELANG(15,"mp.tndr", "Tender"),
		SELEKSI(16,"mp.slkss", "Seleksi"),
		TENDER_YANG_DIKECUALIKAN(17, "mp.pngcln", "Pengecualian"),
		TENDER_KEADAAN_DARURAT(18, "mp.drt", "Darurat"),
		TENDER_INTERNASIONAL(19, "mp.ti", "Tender Internasional");

		
		public final Integer id;
		public final String label;
		public final String sbd_label; // label ketika cetak di E-SDP
		
		MetodePemilihan(int key, String label, String sbd_label){
			this.id = Integer.valueOf(key);
			this.label = label;
			this.sbd_label = sbd_label;
		}
		public Integer getKey() {
			return id;
		}
		public String getLabel() {
			return Messages.get(label);
		}
		public static MetodePemilihan findById(Integer value){
			MetodePemilihan pilih = null;
			if(value != null){
				switch(value.intValue()){
				case 0: pilih = LELANG_UMUM;break;
				case 1: pilih = LELANG_SEDERHANA;break;
				case 2: pilih = LANGSUNG;break;
				case 3: pilih = SELEKSI_UMUM;break;
				case 4: pilih = SELEKSI_TERBATAS;break;
				case 5: pilih = SELEKSI_LANGSUNG;break;
				case 6: pilih = TUNJUK_LANGSUNG;break;
				case 7: pilih = SWAKELOLA;break;
				case 8: pilih = LELANG_TERBATAS;break;
				case 9: pilih = LELANG_EXPRESS;break;
				case 10: pilih = SELEKSI_EXPRESS;break;
				case 11: pilih = PENGADAAN_LANGSUNG;break;
				case 12: pilih = PENUNJUKAN_LANGSUNG;break;
				case 13: pilih = KONTES;break;
				case 14: pilih = SAYEMBARA;break;
				case 15: pilih = LELANG;break;
				case 16: pilih = SELEKSI;break;
				case 17: pilih = TENDER_YANG_DIKECUALIKAN;break;
				case 18: pilih = TENDER_KEADAAN_DARURAT;break;
				case 19: pilih = TENDER_INTERNASIONAL;break;
				default: pilih = LELANG;break;
				}
			}
			return pilih;
		}
				
		public boolean isLelangUmum(){
			return this == LELANG_UMUM;
		}
		
		public boolean isLelangSederhana(){
			return this == LELANG_SEDERHANA;
		}
		
		public boolean isLelangTerbatas(){
			return this == LELANG_TERBATAS;
		}
		
		public boolean isSeleksiUmum(){
			return this == SELEKSI_UMUM;
		}
		
		public boolean isSeleksiSederhana(){
			return this == SELEKSI_TERBATAS;
		}
		
		public boolean isPemilihanLangsung(){
			return this == LANGSUNG;
		}
		
		public boolean isLelangExpress(){
			return this == LELANG_EXPRESS || this == SELEKSI_EXPRESS;
		}

		public boolean isPengadaanLangsung(){
		return this == PENGADAAN_LANGSUNG;
	}

		public boolean isPenunjukanLangsung(){
		return this == PENUNJUKAN_LANGSUNG;
	}
		
		public boolean isSwakelola(){
			return this == SWAKELOLA;
	}

		public boolean isPl(){
		return this == PENGADAAN_LANGSUNG || this == PENUNJUKAN_LANGSUNG;
	}

		public boolean isLelang() {
			return this == LELANG;
		}

		public boolean isSeleksi() {
			return this == SELEKSI;
		}


		public static final MetodePemilihan[] metodePemilihanKonsultan = new MetodePemilihan[] {SELEKSI};
		//list metode pemilihan untuk Tender (pengadaan barang, pekerjaan konstruksi dan Jasa Lainnya)
		public static final MetodePemilihan[] metodePemilihanTender = new MetodePemilihan[] {LELANG, LELANG_EXPRESS};

		public static final MetodePemilihan[] metodePemilihanNonKonsultan = new MetodePemilihan[] {PENGADAAN_LANGSUNG,PENUNJUKAN_LANGSUNG};
	//list metode pemilihan untuk Tender (pengadaan barang, pekerjaan konstruksi dan Jasa Lainnya)
		public static final MetodePemilihan[] metodePemilihanNonTender = new MetodePemilihan[] {PENGADAAN_LANGSUNG, PENUNJUKAN_LANGSUNG};


	/**
		 * untuk mendapatkan list metode pemilihan yang disediakan berdasarkan kategori (bisa diubah sesuai dengan Perpres)
		 * @param kategori
		 * @return
		 */
		public static MetodePemilihan[] getByKategori(Kategori kategori)
		{
			return kategori.isKonsultansi() || kategori.isJkKonstruksi() || kategori.isKonsultansiPerorangan() ? metodePemilihanKonsultan:metodePemilihanTender;
		}

//	public static MetodePemilihan[] getPlByKategori(Kategori kategori)
//	{
//		MetodePemilihan[] metode = null;
//		switch (kategori) {
//			case PENGADAAN_BARANG:metode = new MetodePemilihan[] {PENGADAAN_LANGSUNG, PENUNJUKAN_LANGSUNG, LELANG};
//				break;
//			case PEKERJAAN_KONSTRUKSI:	metode = new MetodePemilihan[] {PENGADAAN_LANGSUNG,PENUNJUKAN_LANGSUNG, LELANG};
//				break;
//			case JASA_LAINNYA:	metode = new MetodePemilihan[] {PENGADAAN_LANGSUNG, PENUNJUKAN_LANGSUNG, LELANG};
//				break;
//			case KONSULTANSI:
//			case KONSULTANSI_PERORANGAN:metode = new MetodePemilihan[] {PENGADAAN_LANGSUNG,PENUNJUKAN_LANGSUNG, SELEKSI};
//				break;
//			default:
//				break;
//		}
//		return metode;
//	}

	public static MetodePemilihan[] getPl(){
		return  new MetodePemilihan[] {PENGADAAN_LANGSUNG, PENUNJUKAN_LANGSUNG};

	}

	public static MetodePemilihan[] getNonSpkKategori()
	{
		MetodePemilihan[] metode = new MetodePemilihan[] {PENGADAAN_LANGSUNG, PENUNJUKAN_LANGSUNG};

		return metode;
	}

	public static MetodePemilihan[] getNonSpkKategori(Kategori kategori)
	{
		return kategori.isKonsultansi() || kategori.isJkKonstruksi() || kategori.isKonsultansiPerorangan() ? metodePemilihanNonKonsultan:metodePemilihanNonTender;
	}

	// konvert metode pemilihan sirup ke metode pemilihan spse, karena ada perbedaan kodifikasi
	public static MetodePemilihan findBy(MetodePemilihanPenyedia mtdPemilihanRup) {
		MetodePemilihan metode = LELANG ;
		if(mtdPemilihanRup != null) {
			switch (mtdPemilihanRup) {
				case LELANG_UMUM: metode = LELANG_UMUM;break;
				case LELANG_SEDERHANA: metode = LELANG_SEDERHANA;break;
				case LELANG_TERBATAS: metode = LELANG_TERBATAS;break;
				case SELEKSI_UMUM:metode = SELEKSI_UMUM;break;
				case SELEKSI_SEDERHANA:metode = SELEKSI_TERBATAS;break;
				case PEMILIHAN_LANGSUNG:metode = LANGSUNG;break;
				case PENUNJUKAN_LANGSUNG:metode = PENUNJUKAN_LANGSUNG;break;
				case PENGADAAN_LANGSUNG:metode = PENGADAAN_LANGSUNG;break;
				case SAYEMBARA:metode = SAYEMBARA;break;
				case KONTES:metode = KONTES;break;
				case LELANG_CEPAT: metode = LELANG_EXPRESS;break;
				case TENDER: metode = LELANG;break;
				case TENDER_CEPAT: metode = LELANG_EXPRESS;break;
				case SELEKSI: metode = SELEKSI;break;
			}
		}
		return metode;
	}
}
