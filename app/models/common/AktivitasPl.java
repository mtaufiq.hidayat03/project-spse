package models.common;

import models.jcommon.db.base.BaseModel;
import models.lelang.Jadwal;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Table(name="EKONTRAK.AKTIVITAS_PL")
public class AktivitasPl extends BaseModel {

	@Id
	public Long akt_id; // aktivitas ini adalah data master jadi tidak perlu sequence, dan diset oleh LKPP

	public Tahap akt_jenis; //Tahap
	
	public Integer akt_urut;

	public Integer akt_status; // default 1. status tahapan lelang, perbedaan peraturan sangat mungkin tahapan bisa hilang/tambah

	public static List<AktivitasPl> findByPl(Integer pemilihanId) {
		return find("akt_status = 1 order by akt_urut asc").fetch();
	}

	public static AktivitasPl findByMetodePl(Tahap tahap) {
		return find("akt_jenis=?", tahap).first();
	}
	
	/**
	 * cek apakah tahap yang diminta sudah dilalui
	 * @param plId
	 * @param tahapList
	 * @return
	 */
	public static boolean isTahapStarted(Long plId, Tahap... tahapList) {	
		Date date = controllers.BasicCtr.newDate();
		StringBuilder sqlWhere = new StringBuilder("akt_id in (select akt_id from ekontrak.jadwal where lls_id=? and dtj_tglawal <= ?)");
		if(tahapList.length > 0) {
			sqlWhere.append(" AND akt_jenis in ('");
			sqlWhere.append(StringUtils.join(tahapList, "','"));
			sqlWhere.append("')");
		}
		return AktivitasPl.count(sqlWhere.toString(), plId, date) > 0;
	}
	
	/**
	 * dapatkan tahapan sekarang dan sudah lewat
	 * @param plId
	 * @return
	 */
	public static List<Tahap> findTahapStarted (Long plId) {	
		Date date = controllers.BasicCtr.newDate();
		return Query.find(SQL_TAHAP_STARTED,Tahap.class,plId,date).fetch();
	}
	
	/**
	 * dapatkan tahapan sekarang dan sudah lewat
	 * @param plId
	 * @return
	 */
	
	public static List<Tahap> findTahapNow(Long plId) {		
		Date date = controllers.BasicCtr.newDate();				
		return Query.find(SQL_TAHAP_NOW, Tahap.class,plId,date, date).fetch();
	}
	
	//asep:
	//cek lelang apakah sudah selesai
	//asumsi tidak ada jadwal berarti belum selesai
	public static boolean isLelangFinish(Long plId){
		String sql = "select j.*  from ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id= ? order by j.dtj_tglakhir desc limit 1";
		try {
			Jadwal jadwal = Query.find(sql, Jadwal.class,plId).first();
			Date date = controllers.BasicCtr.newDate();
			return jadwal.dtj_tglakhir.before(date);		
		} catch (Exception e) {
			return false;
		}	
	}
	
	private static final String SQL_TAHAP_NOW = "select a.akt_jenis from ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id= ? and dtj_tglawal <= ? and dtj_tglakhir >= ? " ;
	private static final String SQL_TAHAP_STARTED = "select a.akt_jenis from ekontrak.jadwal j, ekontrak.aktivitas_pl a where j.akt_id=a.akt_id and j.lls_id= ? and j.dtj_tglawal <= ?";

	public static final List<AktivitasPl> AKTIVITAS_PENGADAAN_LANGSUNG = Arrays.asList(
			AktivitasPl.findByMetodePl(Tahap.PEMASUKAN_PENAWARAN), AktivitasPl.findByMetodePl(Tahap.PEMBUKAAN_PENAWARAN),
			AktivitasPl.findByMetodePl(Tahap.EVALUASI_PENAWARAN), AktivitasPl.findByMetodePl(Tahap.KLARIFIKASI_NEGOSIASI),
			AktivitasPl.findByMetodePl(Tahap.TANDATANGAN_SPK)
	);
	
	public static final List<AktivitasPl> AKTIVITAS_PENUNJUKAN_LANGSUNG = Arrays.asList(
			AktivitasPl.findByMetodePl(Tahap.UMUM_PRAKUALIFIKASI), AktivitasPl.findByMetodePl(Tahap.PEMASUKAN_DOK_PRA),AktivitasPl.findByMetodePl(Tahap.EVALUASI_DOK_PRA),AktivitasPl.findByMetodePl(Tahap.VERIFIKASI_KUALIFIKASI),
			AktivitasPl.findByMetodePl(Tahap.PENETAPAN_HASIL_PRA), AktivitasPl.findByMetodePl(Tahap.PENJELASAN), AktivitasPl.findByMetodePl(Tahap.PEMASUKAN_PENAWARAN),AktivitasPl.findByMetodePl(Tahap.PEMBUKAAN_PENAWARAN),
			AktivitasPl.findByMetodePl(Tahap.EVALUASI_PENAWARAN),AktivitasPl.findByMetodePl(Tahap.KLARIFIKASI_NEGOSIASI),AktivitasPl.findByMetodePl(Tahap.PENETAPAN_PEMENANG_AKHIR), AktivitasPl.findByMetodePl(Tahap.PENGUMUMAN_PEMENANG_AKHIR) 
	);
}
