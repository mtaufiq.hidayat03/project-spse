package models.common;

import controllers.BasicCtr;
import ext.DateBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.db.base.JdbcUtil;
import models.jcommon.util.CommonUtil;
import org.apache.commons.io.IOUtils;
import org.postgresql.core.BaseConnection;
import org.postgresql.largeobject.LargeObject;
import org.postgresql.largeobject.LargeObjectManager;
import play.Logger;
import play.Play;
import play.data.binding.As;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.imageio.ImageIO;
import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

//TODO: sebaiknya kontent ini disimpan sebagai Blob_table agar dapat ditangani sebagai satu kesatuan
/**
 * Model untuk data mengenai konten multimedia SPSE
 *
 * @author I Wayan Wiprayoga W
 */

@Table(name="KONTEN_MULTIMEDIA")
public class KontenMultimedia extends BaseModel {
	/**
	 * Enumerasi jenis konten
	 */
	@Enumerated(EnumType.ORDINAL)
	public enum JenisKonten {
		SLIDE_SHOW("Slide Show"), LINK("Link"),

		//konten untuk NAMA_LPSE dan LOGO_INSTANSI pada header
		NAMA_LPSE("lpse-nama"),
		LOGO_INSTANSI("instansi-logo");
		public final String label;
		
		private JenisKonten(String label)
		{
			this.label=label;
		}
		
	}


	@Id(sequence="seq_konten_multimedia", function="nextsequence")
	public Long media_id;

	@Required
	public JenisKonten jenis;

	@Required
	public String judul;

	@MaxSize(140)
	public String description;
	
	@Required
	@As(binder=DateBinder.class)
	public Date valid_sampai;
	
	public Long blb_id;
	
	
	public String file_name;

    /**
     * Path dari image namun belum mengandung http.path
     */
    public String path;
    
    @Transient
    public String getUrl()
    {
    	return BasicCtr.CONTEXT_PATH + path;
    }
    
    public BlobTable getBlob() {
    	if(blb_id == null)
    		return null;
    	return BlobTableDao.getLastById(blb_id);
    }

  
	/**
	 * Override fungsi penghapusan objek dengan melakukan delete terhadap objek {@link BlobTable} juga
	 */
	@Override
	public void preDelete() {
		BlobTable blob=BlobTableDao.getLastById(blb_id);
		if(blob!=null)
		{
			blob.deleteAllVersion();
		}
		//hapus juga yang di path
		File file=new File(Play.applicationPath + "/" + path);
		Logger.debug("PATH: %s", file);
		file.delete();
	}

    /**
     * Mengembalikan jenis konten dalam bentuk string
     */
    public String stringJenis() {
       return jenis.label;
    }
    
    public void setBlobTable(File file) throws Exception
    {
    	setBlobTable(file, file.getName());
    }
    
    
	private static BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();

		return resizedImage;
	}
    
    /**
     * 
     * @param file
     * @param fileName
     * @throws Exception
     */
    private void setBlobTable(File file, String fileName) throws Exception
    {
    	if(file!=null)
    	{
    		file_name=fileName;
    		if (jenis== JenisKonten.SLIDE_SHOW){
    			BufferedImage originalImage = ImageIO.read(file);
    			int type = originalImage.getType()== 0?BufferedImage.TYPE_INT_ARGB : originalImage.getType();
    			BufferedImage resizeImageJpg = resizeImage(originalImage, type,750,280 );
    			ImageIO.write(resizeImageJpg, "png",file); 
    		}
    		if (jenis== JenisKonten.LINK){
    			BufferedImage originalImage = ImageIO.read(file);
    			int type = originalImage.getType()== 0?BufferedImage.TYPE_INT_ARGB : originalImage.getType();
    			BufferedImage resizeImageJpg = resizeImage(originalImage, type, 375,140 );
    			ImageIO.write(resizeImageJpg, "png",file); 
        	}
    		
    		/* Simpan ke folder /public/images/ (lakukan ini sebelum di-save ke database karena
    		 * ketika save ternyata menghapus file di folder upload tmp	    	
    		 */
        	String basePath="/public/images/imgng/";
        	if(jenis==JenisKonten.LOGO_INSTANSI || jenis==JenisKonten.NAMA_LPSE) {
	    		path= basePath+ jenis.label + ".png";
	    		copyToPublicFolder(new FileInputStream(file));
        	} else {
	    		path= basePath+ "mm."+ UUID.randomUUID().toString() + '.' + file_name;
	    		BlobTable blb = null;
		    	if(blb_id!=null) { //delete prev if exist
		    		blb = BlobTableDao.getLastById(blb_id);
		    		if(blb != null)
		    			blb.deleteAllVersion();
		    	}
		    	copyToPublicFolder(new FileInputStream(file));
		    	blb=BlobTableDao.saveFile(ARCHIEVE_MODE.NON_ARCHIEVE, file);
	    		blb_id=blb.blb_id_content;
        	}        	
        	save();    		   
    	}
    }
    
    
   /* public void copyToPublicFolder() throws IOException, SQLException
    {
    	if(false && Play.mode==Mode.PROD)
    	{
    		if(blb_id!=null)
    		{
	    		BlobTable blob= BlobTableDao.getLastById(blb_id);
	    		if(blob!=null)
	    			copyToPublicFolder(new FileInputStream(blob.getFile()));
    		}
    	}
    	else{
    		Logger.info("app mode:"+Play.mode+" task BlobTable copyToPublicFolder");
    		copyToPublicFolder(getBlobTableFromDatabase());
    	}
    }*/
    
    private void copyToPublicFolder(InputStream is) throws IOException
    {
    	if(is==null)
    		return;
    	//path contains http.path, so remove
    	String fullPath= Play.applicationPath + path;
    	File targetFile=new File(fullPath);
    	//edit by asep:
    	//is check jika ada streamnya(file) dan targetnya exist
    	//delete exist file dengan yang ada di stream(baru)
    	//Logger.info("aseplog: copied is available: "+is.available()+" exist: "+targetFile.exists(), is.available());
    	if((is.available() > 0)){    		
	    	if(targetFile.exists())
	    		targetFile.delete();
	    	OutputStream out=new FileOutputStream(targetFile);
	    	IOUtils.copy(is, out);
	    	out.close();
	    	is.close();
    	}    	
    }
    
    
   /* private InputStream getBlobTableFromDatabase() throws SQLException
    {
    	try(Connection conn=JdbcUtil.getConnectionWithoutPool())//direct JDBC
    	{	  		
			LargeObjectManager lom= new LargeObjectManager((BaseConnection) conn);
			conn.setAutoCommit(false);
			//Delete Large object yang sebelumnya
			if(blb_id != null)
			{
				long oid =-blb_id;
				try {
					LargeObject lo = lom.open(oid);
					return lo.getInputStream();
				} catch (SQLException e) {
					return null;
				}
			}
//			conn.commit();
    	}		
		save();
		return null;
    }
    
    
    private void setBlobTableInDatabase(File file, String fileName) throws SQLException, IOException {
    	
    	Connection conn=JdbcUtil.getConnectionWithoutPool();//direct JDBC
    	try
    	{	long oid;
    		LargeObject lo;
			LargeObjectManager lom=new LargeObjectManager((BaseConnection) conn);
			conn.setAutoCommit(false);
			//Delete Large object yang sebelumnya
			if(blb_id!=null)
			{
				if(blb_id<0)
				{
					oid=-blb_id;
					try
					{
						lom.unlink(oid);
					}
					catch(Exception e)
					{
						//jika oid tdk ditemukan maka set null
						blb_id=null;
					}
				}
			}
			
			
			oid=lom.createLO();
			blb_id=-oid;//disimpan dengan nilai negatif untuk membedakan dengan penyimpanan di FileSystem
			lo=lom.open(oid);
			OutputStream out=lo.getOutputStream();
			
			InputStream in=new FileInputStream(file);
			IOUtils.copy(in, out);
			in.close();
			out.close();
			conn.commit();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	finally
    	{
    		conn.close();	
    	}
		
		save();
	}
*/
    
	/**
	 * Fungsi menampilkan daftar slide di halaman index
	 * dengan data yang dicache selama 1 menit
	 * @return
	 */
	public static List<KontenMultimedia> getSlide()
	{
		Date now = BasicCtr.newDate();
		List<KontenMultimedia> list = null;
		if(list == null || list.isEmpty())
			list = find("jenis = ? AND valid_sampai > ?", JenisKonten.SLIDE_SHOW, now).fetch();
		return list;
	}
	
	/**
	 * Fungsi menampilkan daftar banner di halaman index
	 * dengan data yang dicache selama 1 menit
	 * @return
	 */
	public static List<KontenMultimedia> getLink()
	{
		Date now = BasicCtr.newDate();
		List<KontenMultimedia> list = null;
		if(list == null || list.isEmpty())
			list = find("jenis = ? AND valid_sampai > ?", JenisKonten.LINK, now).fetch();
		return list;
	}

	public static KontenMultimedia find(JenisKonten jenisKonten) {
		KontenMultimedia  km=find("jenis=? order by media_id", jenisKonten).first();
		if(km==null)
		{
			km=new KontenMultimedia();
			km.jenis=jenisKonten;
		}
		return km;
	}

	/**Copy image ke folder /public/images/xxx
	 * @throws SQLException 
	 * @throws IOException 
	 * 
	 */
	/*public static void copyImagesToPublicFolder() throws IOException, SQLException {
		List<KontenMultimedia> list= findAll();
		for(KontenMultimedia km: list) {
			km.copyToPublicFolder(km.getBlobTableFromDatabase());
		}
	}*/
	
	public static Long simpan(Long id, KontenMultimedia konten, File file) throws Exception {
		KontenMultimedia obj = new KontenMultimedia();
		if(id != null) 
			obj = KontenMultimedia.findById(id);		
		
		obj.description = konten.description;		
		obj.jenis = konten.jenis;
		obj.judul = konten.judul;		
		obj.valid_sampai = konten.valid_sampai;
//		obj.file_name = konten.file_name;
//		obj.path = konten.path;
		if(file!=null)
			obj.setBlobTable(file);
		obj.save();
		return obj.media_id;
	}
	/*
	public static void copyImageFromDefault(KontenMultimedia.JenisKonten jenis) {
		String imageName = jenis.label;
		File imgFileSource=new File(Play.applicationPath + "/public/images/imgng/" + imageName + "-default.png");

		//copy image from Cache into temporary file 
		File imgFile;
		try {
			imgFile = File.createTempFile("image", ".png");
			FileOutputStream out=new FileOutputStream(imgFile);
			
			//asep:set cache image for view in ContentMultimediaCtr.header()
//			Cache.set(key, imgByte, "30mn");
			
			IOUtils.copy(new FileInputStream(imgFileSource), out);	
			out.flush();
			//copy image data into database
			KontenMultimedia km=find(jenis);
			km.judul=jenis.toString();
			km.setBlobTable(imgFile);
//			Cache.delete(key);	
		}  catch (Exception e) {
			e.printStackTrace();
		}		
		
	}*/

	/**Copy image from cache into image folder"
	 * 
	 * @param jenisKontent
	 * @throws Exception 
	 */
	/*public static void copyImageFromCache(KontenMultimedia.JenisKonten jenis) {
		Session session = Session.current();
		String key=session.getId() + jenis.label;
		String imgData=Cache.get(key, String.class);
		if(imgData==null)
			return;
		byte[] imgByte=Base64.decodeBase64(imgData);

		//copy image from Cache into temporary file 
		File imgFile;
		try {
			imgFile = File.createTempFile("image", ".png");
			FileOutputStream out=new FileOutputStream(imgFile);
			
			//asep:set cache image for view in ContentMultimediaCtr.header()
//			Cache.set(key, imgByte, "30mn");
			
			IOUtils.copy(new ByteArrayInputStream(imgByte), out);	
			out.flush();
			//copy image data into database
			KontenMultimedia km=find(jenis);
			km.judul=jenis.toString();
			km.setBlobTable(imgFile);
//			Cache.delete(key);	
		}  catch (Exception e) {
			e.printStackTrace();
		}		
			
	}*/
	
	public static byte[] getDevKonten(KontenMultimedia.JenisKonten jenisKontent) {
		KontenMultimedia logo = KontenMultimedia.find(jenisKontent);
		if(logo != null && logo.blb_id != null) {
			try {
				try(Connection conn=JdbcUtil.getConnectionWithoutPool())//direct JDBC
		    	{	  		
					conn.setAutoCommit(false);
					LargeObjectManager lom= new LargeObjectManager((BaseConnection) conn);
					long oid =-logo.blb_id;
					try {
						LargeObject lo = lom.open(oid);					
						return IOUtils.toByteArray(lo.getInputStream());
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
//					conn.commit();
		    	}		
			} catch (Exception e) {
				e.printStackTrace();
				Logger.error("error getting konten: " + e);
				return null;
			}
		} else {
			return null;
		}
	}
	
	// dijalankan saat system start. memastikan konten yang di database sudah tersedia di /publi/images/imgng/
	public static void setupConfig() {
		List<KontenMultimedia> list = find("jenis IN (0,1)").fetch();
		if(CommonUtil.isEmpty(list))
			return ;
		File file = null;
		InputStream is = null;
		for(KontenMultimedia konten : list) {
			if(CommonUtil.isEmpty(konten.path))
				continue;
			file = new File(Play.applicationPath + konten.path);
			if(konten.blb_id != null && !file.exists() && file.canRead()) {
				try {
					is = new FileInputStream(file);
					konten.copyToPublicFolder(is);
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
		}
	}
	
	public static boolean isPermittedDimension(File file){
		try {
			BufferedImage image = ImageIO.read(file);
			if (image != null) {
				if ((image.getWidth() == 750 && image.getHeight() == 280)
						|| (image.getWidth() == 375 && image.getHeight() == 140)){
					return true;
				} else{
					return false;
				}
			} else{
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

}
