package models.common;

import play.db.jdbc.Query;

import java.util.List;

public class Propinsi_kabupaten {

	public Long prp_id;
	
	public String prp_nama;
	
	public String kbp_nama;
	
	public static final String SQL = "SELECT p.prp_id, prp_nama, array_to_string(array_agg(kbp_nama), ' , ') AS kbp_nama "+
									  "FROM propinsi p, kabupaten k WHERE p.prp_id=k.prp_id GROUP BY p.prp_id ,p.prp_nama";
	
	public static List<Propinsi_kabupaten> findAll() {
		return Query.find(SQL, Propinsi_kabupaten.class).fetch();
	}

	public static String transformKabupatenName(String kbp_nama) {
		boolean isKota = kbp_nama.toLowerCase().contains("kota");
		boolean isKab = kbp_nama.toLowerCase().contains("kab");
		String kabClean = kbp_nama;
		if(isKota || isKab) {
			int idxLast = kbp_nama.lastIndexOf('(');
			if(idxLast != -1) {
				String prefix = isKab ? "Kabupaten" : "Kota";
				kabClean = prefix + ' ' + kbp_nama.substring(0,idxLast-1);
			}
		}
		return kabClean;
	}
}
