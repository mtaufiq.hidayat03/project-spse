package models.common;

import utils.Encrypt;

import java.io.Serializable;
import java.util.UUID;

public class UserLoginStep implements Serializable {
    public String username = null;
    public int maxWrongPass = 3;
    public int wrongPassword = 0;
    public String sessionId;
    public String uniqid;
    public static String uniqStr = "_stepPPE";
    public UserLoginStep(String _username,String _sessionId){
        username = _username;
        sessionId = _sessionId;
        uniqid = Encrypt.encrypt(UUID.randomUUID().toString());
    }

//    public static String encRypt(String json){
//        return Encrypt.encrypt(json);
//    }
}
