package models.common;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;


@Table(name="JENIS_IJIN")
public class Jenis_ijin extends BaseModel {
	
	@Id
	public String jni_id;

	public String jni_nama;

}
