package models.common;

import models.bm.BroadcastMessage;
import models.bm.BroadcastMessage.Category;

import java.util.Date;
import java.util.List;

public class BroadcastMessageClient {

	/**Dapatkan running text */
	public static List<BroadcastMessage> findRunningText() {
		Date now=controllers.BasicCtr.newDate();
		return BroadcastMessage.find("end_date>? and roles_ like '%,PUBLIC,%' and category=?", now, Category.RUNNING_TEXT).fetch();
	}

	public static BroadcastMessage findNewestStaticLink() {
		Date now=controllers.BasicCtr.newDate();
		return BroadcastMessage.find("end_date >= ?and roles_ like '%,PUBLIC,%' and category=?", now, Category.STATIC_LINK).first();
	}

}
