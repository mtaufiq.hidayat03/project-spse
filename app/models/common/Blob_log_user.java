package models.common;

import java.io.Serializable;

public class Blob_log_user implements Serializable {

	public String userid;

	public Integer count;

	/**
	 * Default Constructor
	 */
	public Blob_log_user() {

	}

	public Blob_log_user(String userid, Integer count) {
		this.userid = userid;
		this.count = count;
	}
}
