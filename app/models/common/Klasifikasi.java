package models.common;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="KLASIFIKASI")
public class Klasifikasi extends BaseModel {

	@Id
	public Long kla_id;

	public String kla_kode;

	public String kla_nama;

	// relasi ke parent
	public Long kla_kla_id;

	public Integer atr_id;
	
}
