package models.common;

import ext.DateBinder;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.URL;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.mvc.Scope;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


@Table(name="PPE_SITE")
public class Ppe_site extends BaseModel {

	@Id
	@Required
	public String pps_id;

	@Required
	public String pps_nama;

	public String pps_alamat;

	public String pps_display;

	public String pps_kontak;

	public String pps_proxy;

	@Required
	@URL
	public String pps_public_url;
	@URL
	public String pps_private_url;

	@Required
	@As(binder=DateBinder.class)
	public Date pps_tanggal_pendaftaran;

	public String pps_sk;
	
	//relasi ke Sub_tag
	@Required(message="Jenis LPSE wajib diisi")
	public String stg_id;
	@Transient
	private Sub_tag sub_tag;	
	
	public Sub_tag getSub_tag() {
		if(sub_tag == null)
			sub_tag = Sub_tag.findById(stg_id);
		return sub_tag;
	}

	/*public static String tableLpse()
	{
		String[] column = new String[]{"pps_id", "pps_nama", "pps_alamat", "pps_display"};
		String sql ="SELECT pps_id, pps_nama, pps_alamat, pps_display FROM ppe_site";
		String sql_count="SELECT COUNT(pps_id) FROM ppe_site";
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetLpse);
	}*/
	
	public static final ResultSetHandler<String[]> resultsetLpse = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("pps_id");
			tmp[1] = rs.getString("pps_nama");
			tmp[2] = rs.getString("pps_alamat");
			tmp[3] = rs.getString("pps_display");
			return tmp;
		}
	};

	@Override
	protected void postLoad() {
		// get from flash
		flash();
	}

	public void flash() {
		Scope.Flash flash = Scope.Flash.current();
		if(flash != null) {
			if(!StringUtils.isEmpty(flash.get("pps_id")))
				pps_id = flash.get("pps_id");
			if(!StringUtils.isEmpty(flash.get("pps_nama")))
				pps_nama = flash.get("pps_nama");
			if(!StringUtils.isEmpty(flash.get("pps_alamat")))
				pps_alamat = flash.get("pps_alamat");
			if(!StringUtils.isEmpty(flash.get("pps_display")))
				pps_display = flash.get("pps_display");
			if(!StringUtils.isEmpty(flash.get("stg_id")))
				stg_id = flash.get("stg_id");
			if(!StringUtils.isEmpty(flash.get("pps_kontak")))
				pps_kontak = flash.get("pps_kontak");
			if(!StringUtils.isEmpty(flash.get("pps_public_url")))
				pps_public_url = flash.get("pps_public_url");
			if(!StringUtils.isEmpty(flash.get("pps_tanggal_pendaftaran")))
				pps_tanggal_pendaftaran = new Date(Long.parseLong(flash.get("pps_tanggal_pendaftaran")));
			if(!StringUtils.isEmpty(flash.get("pps_sk")))
				pps_sk = flash.get("pps_sk");
		}
	}

	@Override
	public void paramFlash() {
		Scope.Flash flash = Scope.Flash.current();
		if(flash != null) {
			if(pps_id != null)
				flash.put("pps_id", pps_id);
			flash.put("pps_nama", pps_nama);
			flash.put("pps_alamat", pps_alamat);
			flash.put("pps_display", pps_display);
			flash.put("pps_kontak", pps_kontak);
			flash.put("pps_public_url", pps_public_url);
			if(pps_tanggal_pendaftaran != null)
				flash.put("pps_tanggal_pendaftaran", pps_tanggal_pendaftaran.getTime());
			flash.put("stg_id", stg_id);
			flash.put("pps_sk", pps_sk);
		}
	}

}
