package models.common;

import models.jcommon.db.base.BaseModel;
import models.lelang.Jadwal;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Table(name="AKTIVITAS")
public class Aktivitas extends BaseModel {

	@Id
	public Long akt_id; // aktivitas ini adalah data master jadi tidak perlu sequence, dan diset oleh LKPP
	
	public Integer mtd_id;
	
	public Tahap akt_jenis; //Tahap
	
	public Integer akt_urut;

	public String keterangan;

	public Integer akt_status; // default 1. status tahapan lelang, perbedaan peraturan sangat mungkin tahapan bisa hilang/tambah
	
	public static List<Aktivitas> findByMetode(Integer metodeId) {
		return find("mtd_id= ? and akt_status = 1 order by akt_urut asc" , metodeId).fetch();
	}
	
	public static Aktivitas findByMetode(Integer metodeId, Tahap tahap) {
		return find("mtd_id= ? and akt_jenis=?" , metodeId, tahap).first();
	}
	
	public static Aktivitas findByMetodePl(Tahap tahap) {
		return find("akt_jenis=?", tahap).first();
	}
	
	/**
	 * cek apakah tahap yang diminta sudah dilalui
	 * @param lelangId
	 * @param tahapList
	 * @return
	 */
	public static boolean isTahapStarted(Long lelangId, Tahap... tahapList) {	
		Date date = controllers.BasicCtr.newDate();
		StringBuilder sqlWhere = new StringBuilder("akt_id in (select akt_id from jadwal where lls_id=? and dtj_tglawal <= ?)");
		if(tahapList.length > 0) {
			sqlWhere.append(" AND akt_jenis in ('");
			sqlWhere.append(StringUtils.join(tahapList, "','"));
			sqlWhere.append("')");
		}
		return Aktivitas.count(sqlWhere.toString(), lelangId, date) > 0;
	}
	
	/**
	 * dapatkan tahapan sekarang dan sudah lewat
	 * @param lelangId
	 * @return
	 */
	public static List<Tahap> findTahapStarted (Long lelangId) {	
		Date date = controllers.BasicCtr.newDate();
		return Query.find(SQL_TAHAP_STARTED,Tahap.class,lelangId,date).fetch();
	}
	
	/**
	 * dapatkan tahapan sekarang dan sudah lewat
	 * @param lelangId
	 * @return
	 */
	
	public static List<Tahap> findTahapNow(Long lelangId) {		
		Date date = controllers.BasicCtr.newDate();				
		return Query.find(SQL_TAHAP_NOW, Tahap.class,lelangId,date, date).fetch();
	}
	
	//asep:
	//cek lelang apakah sudah selesai
	//asumsi tidak ada jadwal berarti belum selesai
	public static boolean isLelangFinish(Long lelangId){
		String sql = "select j.*  from jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id= ? order by j.dtj_tglakhir desc limit 1";		
		try {
			Jadwal jadwal = Query.find(sql, Jadwal.class,lelangId).first();
			Date date = controllers.BasicCtr.newDate();
			return jadwal.dtj_tglakhir.before(date);		
		} catch (Exception e) {
			return false;
		}	
	}
	
	private static final String SQL_TAHAP_NOW = "select a.akt_jenis from jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id= ? and dtj_tglawal <= ? and dtj_tglakhir >= ? " ;
	private static final String SQL_TAHAP_STARTED = "select a.akt_jenis from jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id= ? and j.dtj_tglawal <= ?";

	public static final List<Aktivitas> AKTIVITAS_TENDER_CEPAT = Arrays.asList(
			Aktivitas.findByMetode(Metode.PASCA_SATU_FILE_54.id, Tahap.PENJELASAN),Aktivitas.findByMetode(Metode.PASCA_SATU_FILE_54.id, Tahap.PEMASUKAN_PENAWARAN)
	);
}
