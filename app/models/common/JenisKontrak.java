package models.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum JenisKontrak {
	LUMP_SUM(1, Messages.get("kontrak.kategori_1")),
	HARGA_SATUAN(2, Messages.get("kontrak.kategori_2")),
	LUMP_SUM_HARGA_SATUAN(3, Messages.get("kontrak.kategori_3")),
	TERIMA_JADI(4, Messages.get("kontrak.kategori_4")),
	PERSENTASE(5, Messages.get("kontrak.kategori_5")),
	TAHUN_TUNGGAL(6, Messages.get("kontrak.kategori_6")),
	TAHUN_JAMAK(7, Messages.get("kontrak.kategori_7")),
	PENGADAAN_TUNGGAL(8, Messages.get("kontrak.kategori_8")),
	PENGADAAN_BERSAMA(9, Messages.get("kontrak.kategori_9")),
	KONTRAK_PAYUNG(10, Messages.get("kontrak.kategori_10")),
	PEKERJAAN_TUNGGAL(11, Messages.get("kontrak.kategori_11")),
	PEKERJAAN_TERINTEGRASI(12, Messages.get("kontrak.kategori_12")),
	WAKTU_PENUGASAN(13, Messages.get("kontrak.kategori_13"));

	public final Integer id;
	public final String label;
	
	JenisKontrak(int id, String label){
		this.id = Integer.valueOf(id);
		this.label = label;
	}	
	
	public static JenisKontrak findById(Integer id)
	{
		JenisKontrak jenis = null;
		if(id == null)
			return null;
		switch (id.intValue()) {
		case 1:
			jenis = LUMP_SUM;
			break;
		case 2:
			jenis = HARGA_SATUAN;
			break;
		case 3:
			jenis = LUMP_SUM_HARGA_SATUAN;
			break;
		case 4:
			jenis = TERIMA_JADI;
			break;
		case 5:
			jenis = PERSENTASE;
			break;
		case 6:
			jenis = TAHUN_TUNGGAL;
			break;
		case 7:
			jenis = TAHUN_JAMAK;
			break;
		case 8:
			jenis = PENGADAAN_TUNGGAL;
			break;
		case 9:
			jenis = PENGADAAN_BERSAMA;
			break;
		case 10:
			jenis = KONTRAK_PAYUNG;
			break;
		case 11:
			jenis = PEKERJAAN_TUNGGAL;
			break;
		case 12:
			jenis = PEKERJAAN_TERINTEGRASI;
			break;
		case 13:
			jenis = WAKTU_PENUGASAN;
			break;
		default:
			jenis = LUMP_SUM;
			break;
		}
		return jenis;
	}
	
	public boolean isLumpsum(){
		return this == LUMP_SUM;
	}
	
	public boolean isHargaSatuan() {
		return this == HARGA_SATUAN;
	}
	
	public boolean isLumpsumHargaSatuan() {
		return this == LUMP_SUM_HARGA_SATUAN;
	}

	public boolean isWaktuPenugasan(){
		return this == WAKTU_PENUGASAN;
	}
	
	public boolean isTahunTunggal() {
		return this == TAHUN_TUNGGAL;
	}
	
	public boolean isTahunJamak() {
		return this == TAHUN_JAMAK;
	}
	
	public static JenisKontrak[] getByCaraPembayaran(Boolean isConsultant)
	{
		if(isConsultant)
			return getByCaraPembayaranConsultant();
		return new JenisKontrak[]{LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN, TERIMA_JADI, KONTRAK_PAYUNG};
	}

	public static JenisKontrak[] getByCaraPembayaranConsultant()
	{
		return new JenisKontrak[]{LUMP_SUM, WAKTU_PENUGASAN, KONTRAK_PAYUNG};
	}

	public static JenisKontrak[] getByCaraPembayaranKonstruksi()
	{
		return new JenisKontrak[]{LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN};
	}

	public static JenisKontrak[] getByCaraPembayaranJasaKonstruksi()
	{
		return new JenisKontrak[]{LUMP_SUM, WAKTU_PENUGASAN};
	}

	public static JenisKontrak[] getByBebanTahunAnggaran()
	{
		return new JenisKontrak[]{TAHUN_TUNGGAL, TAHUN_JAMAK};
	}
	
	public static JenisKontrak[] getBySumberDana()
	{
		return new JenisKontrak[]{PENGADAAN_TUNGGAL, PENGADAAN_BERSAMA, KONTRAK_PAYUNG};
	}
	
	public static JenisKontrak[] getByJenisPekerjaan()
	{
		return new JenisKontrak[]{PEKERJAAN_TUNGGAL, PEKERJAAN_TERINTEGRASI};
	}
}

