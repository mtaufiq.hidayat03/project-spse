package models.common;


public enum MetodeKualifikasi {
	
		PRA (0,"Prakualifikasi"), PASCA (1, "Pascakualifikasi"), PRALULUS2(2, "Tindak lanjut Prakualifikasi ulang jumlah peserta yang lulus 1");
		
		public final Integer id;
		public final String label;
		
		MetodeKualifikasi(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}
		
		public static MetodeKualifikasi fromValue(Integer value){
			MetodeKualifikasi kualifikasi = null;			
			if(value != null){
				switch(value.intValue()){
				case 0: kualifikasi = PRA;break;
				case 1: kualifikasi = PASCA;break;
					case 2: kualifikasi = PRALULUS2;break;
				default: kualifikasi = PRA;break;
				}
			}
			return kualifikasi;
		}
				
		public boolean isPra(){
			return this == PRA;
		}
		
		public boolean isPasca(){
			return this == PASCA;
		}

		public boolean isPralulus(){
		return this == PRALULUS2;
	}

        public static final MetodeKualifikasi[] kualifikasiPasca = new MetodeKualifikasi[]{PASCA};
        public static final MetodeKualifikasi[] kualifikasiPra = new MetodeKualifikasi[]{PRA};
        public static final MetodeKualifikasi[] kualifikasiPraPasca = new MetodeKualifikasi[]{PRA, PASCA};
		public static final MetodeKualifikasi[] kualifikasiPraPasca2 = new MetodeKualifikasi[]{PRA, PRALULUS2};
}
