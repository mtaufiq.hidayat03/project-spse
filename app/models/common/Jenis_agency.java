package models.common;

import models.jcommon.db.base.BaseModel;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;


@Table(name="JENIS_AGENCY")
public class Jenis_agency extends BaseModel {

	@Id
	@Required
	public Long jna_id;

	public String jna_jenis_agency;

	public Long jen_jna_id;
	
	/**
	 * Fungsi {@code getListJenisAgency} merupakan fungsi yang digunakan untuk mengambil
	 * semua data (record) JENIS AGENCY yang mungkin
	 *
	 * @return daftar jenis agency
	 */
	public static List<Jenis_agency> findJenisAgency() {
		// mengembalikan semua record jenis agency dengan urutan ASCENDING
		return order("jna_id asc").fetch();
	}
}