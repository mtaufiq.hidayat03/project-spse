package models.common;

import controllers.BasicCtr;
import ext.DateBinder;
import ext.FormatUtils;
import models.agency.Pegawai;
import models.common.Sub_tag.JenisSubtag;
import models.handler.BeritaDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.cache.Cache;
import play.cache.CacheFor;
import play.data.binding.As;
import play.data.validation.MaxSize;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.mvc.Scope;

import javax.persistence.Transient;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;



// TODO: Please use Cache in this Entity
@Table(name="BERITA")
// try to use cache but I Don't know how it works (Andik)
public class Berita extends BaseModel {
	
	@Id(sequence="seq_berita", function="nextsequence")
	public Long brt_id;

	@Required
	@MaxSize(100)
	@MinSize(4)
	public String brt_judul;

	@Required
	public String brt_isi;

	@Required
	public Date brt_tanggal;

	public Long brt_id_attachment;

	@As(binder=DateBinder.class)
	public Date brt_expires;
	
	//relasi ke Sub_tag
	@Required
	public String stg_id;

	//relasi ke Pegawai
	public Long peg_id;
	@Transient
	private Sub_tag sub_tag;
	@Transient
	private Pegawai pegawai;
	
	public Sub_tag getSub_tag() {
		if(sub_tag == null)
			sub_tag = Sub_tag.findById(stg_id);
		return sub_tag;
	}

	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}

	@Transient
	public boolean isForum() {
		return stg_id.equals(JenisSubtag.FORUM_PENGADAAN.toString());
	}
	
	@Transient
	public String tglExpire() {
		return FormatUtils.formatDateInd(brt_expires);
	}
	
	@Transient
	public BlobTable getBlob() {
		if(brt_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(brt_id_attachment);
	}

	/**
	 * Fungsi Simpan berita
	 * @param berita
	 * @param brtAttachment
	 * @param hapus
	 * @throws Exception
     */
	public static void simpanBerita(Berita berita, File brtAttachment, Long hapus)
			throws Exception {
		if (brtAttachment != null) {
			BlobTable bt = null;
			if (berita.brt_id_attachment == null) {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, brtAttachment);
			} else {
				bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, brtAttachment, berita.brt_id_attachment);
			}
			berita.brt_id_attachment = bt.blb_id_content;

		} else {
			if (berita.brt_id_attachment != null && hapus == null) {
				Long id = berita.brt_id_attachment;
				berita.brt_id_attachment = null;
				BlobTable.delete("blb_id_content=?", id);
			}
		}
		berita.save();
		
	}
	
	
	/**
	 * Fungsi {@code getMarqueeMessage} digunakan untuk mendapatkan daftar
	 * berita dengan jenis subtag {@code JenisSubtag.MARQUEE}. Untuk mendapatkan list daftar berita
	 * semua marquee message yang valid gunakan fungsi {@link #findAllValidMarqueeMessage()}
	 *
	 * @return daftar marquee message terformat dalam satu string (digabung dengan karakter ::)
	 */
	public static String getMarqueeMessage() {
		// TODO: Apa saja yang perlu ditampilkan pada halaman muka untuk objek marquee message
		// saat ini : tampilkan semua data marquee yang valid
		StringBuilder str_buffer = new StringBuilder();
		Date now = BasicCtr.newDate();
		List<Berita> daftar_marquee = find("stg_id = ? AND brt_expires > ?", JenisSubtag.MARQUEE.toString(), now).fetch();
		if (daftar_marquee != null && !daftar_marquee.isEmpty()) {
			for (Berita marquee : daftar_marquee) {
				str_buffer.append(marquee.brt_judul).append(" :: ").append(marquee.brt_isi).append(" ** ");
			}
			return str_buffer.toString();
		}
		return null;
	}

	/**
	 * Fungsi {@code findAllValidMarqueeMessage} digunakan untuk mendapatkan semua data marquee message lokal yang valid
	 * dan kemudian simpan di-cache selama 1 menit. Jika data masih ada di-cache, gunakan data yang ada.
	 *
	 * @return daftar marquee dalam bentuk list.
	 */

	public static List<Berita> findAllValidMarqueeMessage() {
		List daftarMarquee;
		if ((daftarMarquee = (List) Cache.get("allValidMarqueeMessage")) != null) {
			// continue
		} else {
			daftarMarquee = refreshCacheValidMarquee();
		}
		return daftarMarquee;
	}

	/**
	 * Fungsi {@code findAllValidRegulasi} digunakan untuk mendapatkan semua data regulasi lokal yang valid
	 * dan kemudian simpan di-cache selama 1 menit. Jika data masih ada di-cache, gunakan data yang ada.
	 *
	 * @return daftar regulasi dalam bentuk list.
	 */
	@SuppressWarnings("unchecked")
	public static List<Berita> findAllValidRegulasi() {
		List daftarRegulasi;
		if ((daftarRegulasi = (List) Cache.get("allValidRegulasi")) != null) {
			// continue
		} else {
			// add to cache
			daftarRegulasi = refreshCacheValidRegulasi();
		}
		return daftarRegulasi;
	}
	
	/**
	 * Fungsi {@code refreshCacheValidRegulasi} digunakan untuk refresh isi cache regulasi
	 *
	 *@return daftar regulasi dalam bentuk list.
	 */
	@SuppressWarnings("unchecked")
	public static  List<Berita>  refreshCacheValidRegulasi() {
		Date now = BasicCtr.newDate();
		List daftarRegulasi = find("stg_id = ? AND brt_expires >= ?", JenisSubtag.REGULASI.toString(), now).fetch();
		Cache.safeSet("allValidRegulasi", daftarRegulasi, "1mn");
		return daftarRegulasi;
	}
	
	/**
	 * Fungsi {@code refreshCacheValidMarquee} digunakan untuk refresh isi cache marquee
	 *
	 *@return daftar Marquee dalam bentuk list.
	 */
	@SuppressWarnings("unchecked")
	public static  List<Berita>  refreshCacheValidMarquee() {
		Date now = BasicCtr.newDate();
		List marquees = find("stg_id = ? AND brt_expires > ?", JenisSubtag.MARQUEE.toString(), now).fetch();
		Cache.safeSet("allValidMarqueeMessage", marquees, "1mn");
		return marquees;
	}
	
	@SuppressWarnings("unchecked")
	public static  List<Berita>  refreshCacheValidSpecialContent() {
	Date now = BasicCtr.newDate();
	List daftarSpecialContent = find("stg_id = ? AND brt_expires >= ?", JenisSubtag.SPECIAL_CONTENT.toString(), now).fetch();
	Cache.safeSet("allValidSpecialContent", daftarSpecialContent,BasicCtr.DEFAULT_CACHE);
	return daftarSpecialContent;
	}
	
	@SuppressWarnings("unchecked")
	public static void refreshCache(String stg_id){
		if(stg_id.equals(JenisSubtag.REGULASI.toString()))
			refreshCacheValidRegulasi();
		else if(stg_id.equals(JenisSubtag.MARQUEE.toString()))
			refreshCacheValidMarquee();
		else if(stg_id.equals(JenisSubtag.SPECIAL_CONTENT.toString()))
			refreshCacheValidSpecialContent();
		else if(stg_id.equals(JenisSubtag.SYSTEM_MESSAGE.toString()))
			Cache.safeDelete("systemMsg");
	}

	/**
	 * Fungsi {@code findAllValidSpecialContent} digunakan untuk mendapatkan semua data special content lokal yang
	 * valid dan kemudian disimpan di-cache selama 1 menit. Jika data masih ada di-cache, gunakan data yang ada.
	 *
	 * @return daftar special content dalam bentuk list
	 */
	@SuppressWarnings("unchecked")
	@CacheFor("1mn")
	public static List<Berita> findAllValidSpecialContent() {
		List daftarSpecialContent;
		if ((daftarSpecialContent = (List) Cache.get("allValidSpecialContent")) != null) {
			// continue
		} else {
			daftarSpecialContent = refreshCacheValidSpecialContent();
		}
		return daftarSpecialContent;
	}

	/**
	 * Fungsi {@code getPengumumanAndBeritaTerakhir} digunakan untuk mendapatkan beberapa
	 * pengumuman dan berita terakhir untuk ditampilkan di halaman muka. Jika parameter {@code count} bernilai -1,
	 * maka hasil yang dikembalikan adalah semua data pengumuman. Data didapatkan dari cache, jika tidak
	 * ada data yang didapat dari cache ambil lagi dan kemudian simpan di-cache selama 1 menit.
	 * Berita pengumuman yang ditampilkan adalah data {@link Berita} dengan sub_tag {@link JenisSubtag#LELANG},
	 * {@link JenisSubtag#LOWONGAN}, dan {@link JenisSubtag#BERITA}
	 *
	 * @param count batas jumlah berita dan pengumuman terbaru yang ditampilkan
	 * @return beberapa daftar berita dan pengumuman
	 */

	public static List<Berita> getPengumumanAndBeritaTerakhir(int count) {
		List<Berita> list = (List<Berita>)Cache.get("getPengumumanAndBeritaTerakhir");
		if(CollectionUtils.isEmpty(list)) {
			String sql = "STG_ID in('" + JenisSubtag.LELANG.toString() + "','" + JenisSubtag.LOWONGAN.toString() + "','" + JenisSubtag.BERITA.toString() + "','" + JenisSubtag.PL.toString()+ "') ORDER by brt_tanggal DESC";
			if (count == -1) { // find all pengumuman
				list = find(sql).fetch();
			} else {
				list = find(sql).fetch(count);
			}
			Cache.add("getPengumumanAndBeritaTerakhir", list, "1mn");
		}
		return list;
	}

	/**
     *  Mendapatkan informasi tentang system message
     * @return
     * @throws SQLException 
     */
	public static String getSystemMessage() {		
		String sql="SELECT brt_isi FROM berita WHERE stg_id = ? AND brt_expires > ? ORDER BY brt_tanggal DESC LIMIT 1";
		return Query.find(sql, String.class, JenisSubtag.SYSTEM_MESSAGE.toString(), controllers.BasicCtr.newDate()).first();
	}	
	
	public static final ResultSetHandler<String[]> resultsetBerita = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[3];
			tmp[0] = rs.getString("brt_id");
			tmp[1] = rs.getString("brt_judul");
			tmp[2] = FormatUtils.formatDateTimeInd(rs.getTimestamp("brt_tanggal"));
			return tmp;
		}
	};

	@Override
	protected void postLoad() {
		// get from flash
		flash();
	}

	public void flash() {
		Scope.Flash flash = Scope.Flash.current();
		if(flash != null) {
			if(!StringUtils.isEmpty(flash.get("brt_id")))
				brt_id = Long.parseLong(flash.get("brt_id"));
			if(!StringUtils.isEmpty(flash.get("brt_judul")))
				brt_judul = flash.get("brt_judul");
			if(!StringUtils.isEmpty(flash.get("brt_isi")))
				brt_isi = flash.get("brt_isi");
			if(!StringUtils.isEmpty(flash.get("brt_id_attachment")))
				brt_id_attachment = Long.parseLong(flash.get("brt_id_attachment"));
			if(!StringUtils.isEmpty(flash.get("stg_id")))
				stg_id = flash.get("stg_id");
			if(!StringUtils.isEmpty(flash.get("brt_expires")))
				brt_expires = new Date(Long.parseLong(flash.get("brt_expires")));
			if(!StringUtils.isEmpty(flash.get("brt_tanggal")))
				brt_tanggal = new Date(Long.parseLong(flash.get("brt_tanggal")));
		}
	}

	@Override
	public void paramFlash() {
		Scope.Flash flash = Scope.Flash.current();
		if(flash != null) {
			if(brt_id != null)
				flash.put("brt_id", brt_id);
			flash.put("brt_judul", brt_judul);
			flash.put("brt_isi", brt_isi);
			if(brt_expires != null)
				flash.put("brt_expires", brt_expires.getTime());
			if(brt_id_attachment != null)
				flash.put("brt_id_attachment", brt_id_attachment);
			if(brt_tanggal != null)
				flash.put("brt_tanggal", brt_tanggal.getTime());
			flash.put("stg_id", stg_id);
		}
	}

	public String getDownloadUrl() {
		BlobTable blob = getBlob();
		if(blob == null)
			return "";
		return blob.getDownloadUrl(BeritaDownloadHandler.class);	
	}
}
