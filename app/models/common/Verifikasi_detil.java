package models.common;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

@Table(name="VERIFIKASI_DETIL")
public class Verifikasi_detil extends BaseModel {

	@Id
	public Long ver_id;

	@Id
	public Integer vdt_item;

	@Id
	public Long vdt_item_id;

	public Integer vdt_valid;

	public Date vdt_tgl_update;	

}
