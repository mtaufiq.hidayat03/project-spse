package models.common;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import java.util.List;


@Table(name="SUB_TAG")
public class Sub_tag extends BaseModel {
	
	@Enumerated(EnumType.STRING)
	public enum JenisSubtag {
		BERITA("Berita"), LOWONGAN("Lowongan Pekerjaan"), LELANG("Pengumuman Tender"),
		SPECIAL_CONTENT("Konten Khusus"), REGULASI("Regulasi"), SYSTEM_MESSAGE("Pesan Sistem"), 
		MARQUEE("Pesan Berjalan"), FORUM_PENGADAAN("Forum Pengadaan"), PENGUMUMAN("Pengumuman Massal"),

		LPSE_NASIONAL("LPSE Nasional"), LPSE_REGIONAL("LPSE Regional"), LPSE_DEPARTEMEN("LPSE Kementerian"),

		KONTES("Kontes"), PELELANGAN_SEDERHANA("Pelelangan Sederhana"), PELELANGAN_TERBATAS("Pelelangan Terbatas"), 
		PELELANGAN_UMUM("Pelelangan Umum"), PEMILIHAN_LANGSUNG("Pemilihan Langsung"), SAYEMBARA("Sayembara"), 
		SELEKSI_PEMILIHAN("Seleksi Pemilihan"), SELEKSI_UMUM("Seleksi Umum"),

		IJIN_USAHA("Izin Usaha"), PAJAK("Pajak"), AKTA("Akta"),

		SURAT_PERJANJIAN("Surat Perjanjian"), SSKK("SSKK"), BAP("BAP"), SPPBJ("SPPBJ"), SPMK("SPMK"),SURAT_TUGAS("Surat Tugas"),

		PL("Pengumuman Pengadaan/Penunjukan Langsung");
		
		public final String label;
		
		private JenisSubtag(String label) {
			this.label = label;
		}
		
		public boolean isAttachment() {
			return (this == SPECIAL_CONTENT) || (this == BERITA)
					|| (this == FORUM_PENGADAAN) || (this == LOWONGAN)
					|| (this == REGULASI) || (this == LELANG);
		}

		public boolean isExpire() {
			return (this == MARQUEE) || (this == SYSTEM_MESSAGE)
					|| (this == LOWONGAN) || (this == FORUM_PENGADAAN)
					|| (this == PENGUMUMAN);
		}
		
		public boolean isMandatoryExpiredDate(){
			return (this == MARQUEE) || (this == BERITA)
					|| (this == REGULASI);
		}

		public boolean isForum() {
			return this == FORUM_PENGADAAN;
		}

		public boolean isSpecialContent() {
			return this == SPECIAL_CONTENT;
		}

		public boolean isBerita() {
			return this == BERITA;
		}
		
		public String lowerCase() {
			return toString().toLowerCase();
		}
	}

	@Id
	public String stg_id;

	public String stg_nama;
	
	public Tag tag_id;

	
	/**
	 * Fungsi {@code findSubTagBeritaByID} digunakan untuk mendapatkan objek
	 * {@code Sub_tag} dengan jenis Tag Berita.
	 *
	 * @param stg_id sub tag id
	 * @return objek Sub_tag bersesuaian dengan stg_id
	 */
	public static Sub_tag findSubTagBerita(String stg_id) {
		return find("stg_id = ? AND tag = ?", stg_id, Tag.BERITA).first();
	}

    public static Sub_tag findSubTagPengumuman(String stg_id) {
        return find("stg_id = ? AND tag = ?", stg_id, Tag.METODE_PEMILIHAN).first();
    }

	/**
	 * Fungsi {@code findAllSubTagJenisLpse} digunakan untuk mendapatkan semua daftar {@link Sub_tag}
	 * dengan ID Tag {@link models.common.Tag.JenisTag#JENIS_LPSE}.
	 *
	 * @return daftar subtag dengan ID Tag {@link models.common.Tag.JenisTag#JENIS_LPSE}
	 */
	public static List<Sub_tag> findSubTagJenisLpse() {
		return find("tag_id = ?", Tag.JENIS_LPSE.toString()).fetch();
	}

	public static List<Sub_tag> findSubTagSyaratVerifikasi(boolean individualConsultant) {
		return find("stg_id IN ('1_KTP', '2_NPWP')").fetch();
	}
}
