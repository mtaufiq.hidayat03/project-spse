package models.common;

import controllers.BasicCtr;
import models.lelang.*;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * class ini digunakan untuk membuat simple dalam menangani flow proses lelang yang sudah dilewati
 * berguna dalam simplify definisi di controller terkait workflow lelang
 * @author arief Ardiyansah
 *
 */
public class TahapStarted {	

	public final Long lelangId;
	final List<Tahap> tahapList;
	boolean express;
	final Active_user active_user;
	StatusLelang status;
	Metode metode;
	boolean auction;
	
	public TahapStarted(Long lelangId) {
		this.lelangId = lelangId;
		Query.find("SELECT mtd_id, lls_status, mtd_pemilihan, lls_metode_penawaran FROM Lelang_seleksi WHERE lls_id=?", new ResultSetHandler<Object>() {

			@Override
			public Object handle(ResultSet rs) throws SQLException {
				metode = Metode.findById(rs.getInt("mtd_id"));
				status = StatusLelang.fromValue(rs.getInt("lls_status"));
				MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
				express = pemilihan.isLelangExpress();
				auction = rs.getInt("lls_metode_penawaran") == 1;
				return null;
			}
			
		}, lelangId).first();
		this.tahapList = Aktivitas.findTahapStarted(lelangId);
		this.active_user = Active_user.current();
//		Logger.info(tahapList.toString());
	}


	public TahapStarted(Lelang_seleksi lelang) {
		this.lelangId = lelang.lls_id;
		this.metode = lelang.getMetode();
		this.express = lelang.isExpress();
		this.status = lelang.lls_status;
		this.tahapList = Aktivitas.findTahapStarted(lelangId);
		this.active_user = Active_user.current();
		this.auction = lelang.isAuction();
	}
	
	public boolean isAmbilDokPra() {
		return tahapList != null && tahapList.contains(Tahap.AMBIL_DOK_PRA);
	}
	
	public boolean isAmbilDok() {
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanAmbilDok);
	}
	
	public boolean isPenjelasanPra() {
		return tahapList != null && tahapList.contains(Tahap.PENJELASAN_PRA);
	}
	
	public boolean isPenjelasan() {
		return tahapList != null && tahapList.contains(Tahap.PENJELASAN);
	}
	
	public boolean isPembuatanBAHP() {
		return tahapList != null && tahapList.contains(Tahap.UPLOAD_BA_HASIL_LELANG);
	}
	
	public boolean isPembukaan() {
		if(express) {
			return isPemasukanBerakhir();
		}
		
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanPembukaan);
	}
	
	public boolean isEvaluasiPra() {
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasi);
	}
	
	public boolean isPembukaanTeknis() {
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapPembukaanTeknis);
	}
	
	public boolean isPembukaanHarga() {
		return tahapList != null && (
				tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA)
				|| tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN_BIAYA)
			);
	}
	
	public boolean isEvaluasi() {
        if(express) {
           return isPemasukanBerakhir();
        }
		return CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasi);
	}
	
	public boolean isEvaluasiTeknis() {
		return tahapList != null  && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasiTeknis);
	}
	
	public boolean isEvaluasiHarga() {
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasiHarga);
	}
	
	public boolean isPengumuman() {
        if(express) {
           return isPemasukanBerakhir();
        }
		return CollectionUtils.containsAny(tahapList, Tahap.tahapanPengumumanHasil);
	}
	
	public boolean isPengumumanPemenangPra() {
		return tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK) 
				|| tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA));
	}
	
	public boolean isPengumumanPemenangAdmTeknis() {
		return tahapList != null && tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS);
	}
	
	public boolean isPenetapanPemenangAkhir() {
		if(express) {
	         return isPemasukanBerakhir();
	    }
		return tahapList != null && (tahapList.contains(Tahap.PENETAPAN_PEMENANG_AKHIR) || tahapList.contains(Tahap.PENETAPAN_PEMENANG_PENAWARAN) 
				|| tahapList.contains(Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR));
	}
	
	public boolean isPengumumanPemenangAkhir() {
		if(express) {
	         return isPemasukanBerakhir();
	    }
		return tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR) ||tahapList.contains(Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR));
	}

	public boolean isSanggahPra() {
		return tahapList != null && tahapList.contains(Tahap.SANGGAH_PRA);
	}
	
	public boolean isSanggahAdm() {
		return tahapList != null && tahapList.contains(Tahap.SANGGAH_ADM_TEKNIS);
	}
	
	public boolean isSanggah() {
		return tahapList != null && tahapList.contains(Tahap.SANGGAH);
	}
	
	public boolean isKontrak() {
		return tahapList != null && tahapList.contains(Tahap.TANDATANGAN_KONTRAK);
	}

    public boolean isPenunjukanPemenang() {
        return tahapList != null && tahapList.contains(Tahap.PENUNJUKAN_PEMENANG);
    }

	public boolean isKlarifikasiTeknisBiaya() {
		return tahapList != null && tahapList.contains(Tahap.KLARIFIKASI_TEKNIS_BIAYA);
	}


    public boolean isShowPenyedia(boolean pascakualifikasi) {
        if(express) {
            return isPembukaan();
        }
        if(pascakualifikasi)
            return tahapList != null && (tahapList.contains(Tahap.PEMBUKAAN_PENAWARAN) || tahapList.contains(Tahap.PEMASUKAN_DAN_EVALUASI_DOK_PRA)
            		|| tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS) || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI));
        else {
            if(active_user == null || active_user.isRekanan()){
                return tahapList != null && tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS);
            }
            else {
                return tahapList != null && (tahapList.contains(Tahap.EVALUASI_DOK_PRA) || tahapList.contains(Tahap.EVALUASI_PENAWARAN_ADM_TEKNIS)
                || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS));
            }
        }
    }
    
    /**
     * cek apakah tahap pemasukan sudah berakhir ato belum
     * true : jika sudah berakhir
     * false : jika belum berakhir
     * @return
     */
    public boolean isPemasukanBerakhir() {
    	Jadwal jadwal = Jadwal.findByLelangNTahap(lelangId, Tahap.PEMASUKAN_PENAWARAN);
		return jadwal != null && jadwal.dtj_tglakhir != null && jadwal.isEnd(BasicCtr.newDate());
    }

    /**
     * flag lelang boleh evaluasi ulang atau tidak
     * @return
     */
    public boolean isAllowEvaluasiUlang() {
        return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanPembukaanEvaluasiPenawaran);
    }
    
    public boolean isKirimPersyaratKualifikasi() {
    	return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasiDokPra);
    }

    /**
     * flag lelang boleh pemasukan ulang ulang atau tidak
     * @return
     */
    public boolean isAllowEvaluasiUlangPra() {
		return !tahapList.contains(Tahap.AMBIL_DOKUMEN_PEMILIHAN);
    }
    
    public boolean isEvaluasiUlangPra() {
    	return !tahapList.contains(Tahap.AMBIL_DOKUMEN_PEMILIHAN);
    }

	/**
	 * flag lelang boleh pemasukan ulang atau tidak
	 * flag lelang boleh evaluasi ulang atau tidak
	 * @return
	 */
    public boolean isEvaluasiUlang() {
    	return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanPembukaanEvaluasiPenawaran);
    }

	/**
	 * flag lelang boleh pemasukan ulang  atau tidak untuk pasca
	 * @return
	 */
	public boolean isAllowPemasukanUlangPasca() {
		return !express && tahapList != null && tahapList.contains(Tahap.EVALUASI_PENAWARAN_KUALIFIKASI);
	}

	/**
	 * flag lelang boleh pemasukan ulang Prakualifikasi
	 * @return
	 */
	public boolean isAllowPemasukanUlangPra() {
		return !express && tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA) || tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK));
	}
	/**
	 * allow show hasil evaluasi Akhir
	 * @return
     */
    public boolean isShowHasilEvaluasi(){        
    	if(express) {
			return isPemasukanBerakhir();
		}
		else if(tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR)|| tahapList.contains(Tahap.PENUNJUKAN_PEMENANG) 
				|| tahapList.contains(Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR))){
            Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(lelangId);
            return (evaluasi != null && evaluasi.eva_status.isSelesai());
        }
        return false;
	}

	/**
	 * allow show hasil evaluasi Kualifikasi
	 * @return
	 */
	public boolean isShowHasilEvaluasiKualifikasi(){
		if(tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA)|| tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK))){
			Evaluasi evaluasi = Evaluasi.findKualifikasi(lelangId);
			return evaluasi != null && evaluasi.eva_status.isSelesai();
		}
		return false;

	}

	/**
	 * allow show hasil evaluasi Teknis
	 * @return
	 */
	public boolean isShowHasilEvaluasiTeknis(){
		if(tahapList != null && tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS)){
			Evaluasi evaluasi = Evaluasi.findTeknis(lelangId);
			return evaluasi != null && evaluasi.eva_status.isSelesai();
		}
		return false;

	}

	/**
	 * otorisasi download dokumen lelang
	 * @param prakualifikasi
	 * @return
	 */
	public boolean isAllowDownloadDok(boolean prakualifikasi, boolean versi3) {
		if(express) {
			return status.isAktif() || active_user.isAuditor();
		} else if(active_user.isRekanan() && tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanAmbilDok)) {
			if(prakualifikasi) { // jika prakualifikasi , hanya peserta yang lulus prakualifikasi yg bisa download dokumen lelang
				Peserta peserta = Peserta.findBy(lelangId, active_user.rekananId);
				return peserta != null && (versi3 ? peserta.isLulusKualifikasi():peserta.isLulusPembuktian());
			} else
				return true;
		} else {
			return active_user.isPanitia() || active_user.isAuditor() || active_user.isPpk();
		}
	}
	/*
	 * otorisasi download dokumen prakualifikasi
	 */
	public boolean isAllowDownloadDokPra() {
		if (active_user.isRekanan()) {
			return tahapList != null && tahapList.contains(Tahap.AMBIL_DOK_PRA);
		} else {
			return active_user.isPanitia() || active_user.isAuditor() || active_user.isPpk();
		}
	}

	/**
	 * panitia diijinkan untuk mengirim/mengedit adendum
	 * hanya jika paket dibuat versi 3.5, jika versi 4 memakai workflow engine
	 * @return
	 *
	 */
	public boolean isAllowAdendum()
	{
		Date date = BasicCtr.newDate();
		boolean allowCreateAdendum = false;
		if((active_user.isPanitia() || active_user.isPpk()) && status.isAktif())
		{
			List<Jadwal> list = Jadwal.findByLelangNTahap(lelangId, Tahap.PEMASUKAN_PENAWARAN, Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS);
			if(!CollectionUtils.isEmpty(list)) {
				Jadwal jadwal = list.get(0);
				if(jadwal != null && jadwal.dtj_tglakhir != null){
					DateTime jadwalAkhir = new DateTime(jadwal.dtj_tglakhir);
					jadwalAkhir = jadwalAkhir.minusDays(3); // tanggal akhir jadwal dikurangi 3 hari (sesuai perlem lkpp 2018)
					allowCreateAdendum = tahapList.size() > 0 && date.getTime() <= jadwalAkhir.getMillis();
				}
				if (metode.kualifikasi.isPra()) { // pengecekan prakualifikasi
					list = Jadwal.findByLelangNTahap(lelangId, Tahap.PENGUMUMAN_HASIL_PRA, Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK);
					if(!CollectionUtils.isEmpty(list)) {
						jadwal = list.get(0);
						if (jadwal != null && jadwal.dtj_tglawal != null && jadwal.dtj_tglawal.after(BasicCtr.newDate())) {
							// jika sebelum jadwal pengumuman hasil pra, maka dokumen lelang masih dianggap draft bukan adendum
							return false;
						}
					}
				}
			}
		}
		return allowCreateAdendum;
	}

	/**
	 * otorisasi Adendum
	 * @return
     */
	public boolean isAllowAdendumPra()
	{
		Date date = BasicCtr.newDate();
		if(active_user.isPanitia())
		{
			Jadwal jadwal = Jadwal.findByLelangNTahap(lelangId, Tahap.PEMASUKAN_DOK_PRA);
			if(jadwal != null && jadwal.dtj_tglakhir != null){
				DateTime jadwalAkhir = new DateTime(jadwal.dtj_tglakhir);
				jadwalAkhir = jadwalAkhir.minusDays(3); // tanggal akhir jadwal dikurangi 3 hari
				//Jika sudah sampai batas akhir kirim persyaratan kualifikasi, adendum hilang
				return date.getTime() <= jadwalAkhir.getMillis() && tahapList.contains(Tahap.UMUM_PRAKUALIFIKASI);
			}
		}
		return false;
	}

	// check paket 4.3 dan express
	public boolean isAuctionExpress() {
		return express;
	}

	public boolean isAuction() {
		if (express) {
			return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN);
		}else if (auction && tahapList != null && (tahapList.contains(Tahap.PENETAPAN_PEMENANG_AKHIR) || tahapList.contains(Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR))) {
			Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lelangId);
			if (penetapan != null) {
				Active_user active_user = Active_user.current();
				if(active_user.isRekanan()) {
					Peserta peserta = Peserta.findByRekananAndLelang(active_user.rekananId,lelangId);
					return Nilai_evaluasi.find("psr_id = ? AND eva_id=?", peserta.psr_id, penetapan.eva_id).first() != null;
				} else if(active_user.isPanitia())
					return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public String toString() {
		return "TahapStarted @"+lelangId+" [" + tahapList.toString() + ']';
	}
	
	
}
