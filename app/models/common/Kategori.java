package models.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

/**
 * Fungsi enum Kategori Pengadaan
 * @author Arief Ardiyansah
 *
 */
@Enumerated(EnumType.ORDINAL)
public enum Kategori {
	PENGADAAN_BARANG(0, Messages.get("ct.kategori_0")),
	KONSULTANSI(1, Messages.get("ct.kategori_1")),
	PEKERJAAN_KONSTRUKSI(2, Messages.get("ct.kategori_2")),
	JASA_LAINNYA(3, Messages.get("ct.kategori_3")),
	KONSULTANSI_PERORANGAN(4, Messages.get("ct.kategori_4")),
	KONSULTANSI_KONSTRUKSI(5, Messages.get("ct.kategori_5"));

	public final Integer id;
	public final String nama;

	private Kategori(int id, String nama) {
		this.id = Integer.valueOf(id);
		this.nama = nama;
	}

	public boolean isBarang(){
		return this == PENGADAAN_BARANG;
	}

	public boolean isKonsultansi(){
		return this == KONSULTANSI;
	}

	public boolean isKonsultansiPerorangan(){
		return this == KONSULTANSI_PERORANGAN;
	}

	public boolean isKonstruksi(){
		return this == PEKERJAAN_KONSTRUKSI;
	}

	public boolean isJasaLainnya(){
		return this == JASA_LAINNYA;
	}

	public boolean isJkKonstruksi() {
		return this == KONSULTANSI_KONSTRUKSI;
	}

	public boolean isConsultant() {
		return isKonsultansi() || isKonsultansiPerorangan();
	}
	
//	public boolean isConsultantJkKonstruksi(){
//		return isKonsultansi() || isKonsultansiPerorangan() ||  isJkKonstruksi();
//	}

	public static Kategori findById(Integer id)
	{
		Kategori kategori = null;
		if(id != null) {
			switch (id.intValue()) {
			case 0:kategori = PENGADAAN_BARANG;break;
			case 1:kategori = KONSULTANSI;break;
			case 2:kategori = PEKERJAAN_KONSTRUKSI;break;
			case 3:kategori = JASA_LAINNYA;break;
			case 4:kategori = KONSULTANSI_PERORANGAN;break;
			case 5:kategori = KONSULTANSI_KONSTRUKSI;break;
			default:
				break;
			}
		}
		return kategori;
	}

	
	
	public static final Kategori[] all = new Kategori[]{
			PENGADAAN_BARANG,
			PEKERJAAN_KONSTRUKSI,
			KONSULTANSI,
			KONSULTANSI_KONSTRUKSI,
			KONSULTANSI_PERORANGAN,
			JASA_LAINNYA
	};

	public String getNama()
	{
		return Messages.get(nama);
	}
}
