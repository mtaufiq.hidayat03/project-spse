package models.common;

import models.jcommon.blob.BlobTableDao;
import models.jcommon.config.Configuration;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.sso.common.Repository;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.DB;
import play.db.jdbc.Query;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import utils.osd.CertificateUtil;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 
 * Kelas {@code ConfigurationDao} merupakan kelas DAO untuk model {@link Configuration}
 * Created by IntelliJ IDEA.
 * Date: 26/07/12 Time: 12:59
 *       
 * @author I Wayan Wiprayoga W
 */
public class ConfigurationDao {
	
	// untuk develepment / testing perlu didefinisikan revisi aplikasi
	// untuk development hanya versinya saja tidak perlu dibuild
	public static final String SPSE_VERSION="4.3u20200414";
	// setting ICB
	private static final boolean enableICB = Boolean.parseBoolean(Play.configuration.getProperty("icb.enabled", "false"));
	// URL Agregasi
	public static final String ADP_URL = Play.configuration.getProperty("adp.url", "https://inaproc.lkpp.go.id/agregasi");
	// enabled ADP
//	private static final boolean enableADP = Boolean.parseBoolean(Play.configuration.getProperty("adp.enabled", "true"));

	// default connetion timeout aplikasi ketiak mengakses URL dari luar,
	public static final String CONNECTION_TIMEOUT = Play.configuration.getProperty("connection.timeout", "5mn"); // default 5 menit
	
	public static Mode MODE = null; // Mode aplikasi (production/latihan). berbeda dengan mode dalam play!

	// setting dev partner
	private static final boolean enableDevPartner = Boolean.parseBoolean(Play.configuration.getProperty("devpartner.enabled", "false"));
	
	public enum Mode {
		LATIHAN, PRODUCTION, INSTALASI;
	}
	
	public static boolean isLatihan() {
		return MODE == Mode.LATIHAN;
	}

    public static boolean isProduction(){
        return MODE == Mode.PRODUCTION;
    }

	public static int getTotalkaptcha() {
		return Configuration.getInt(CONFIG.CAPTCHA.toString());
	}

	public static String getNamaLpse() {
		return Configuration.getConfigurationValue(CONFIG.PPE_NAMA.toString());
	}

	public static boolean isEnableInaproc() {
		return isProduction();
	}

	public static String getLpseNasionalUrl() {
		return Configuration.getConfigurationValue(CONFIG.EPNS_SITE.toString());
	}

//	public static String informasiTanggalUpdate="u";
	
	public static String informasiAgregasi = "";

	//set cdn.url default to {http.path}/public
	public static String CDN_URL = Play.configuration.get("http.path")+"/public";

	/**
	 * Fungsi {@code getRepoId} digunakan untuk mendapatkan informasi ID lPSE
	 *
	 * @return ID LPSE
	 */
	public static int getRepoId() {
		return Configuration.getInt(CONFIG.PPE_ID.toString());
	}


	/**
	 * Fungsi {@code getStorageRootpath} digunakan untuk mendapatkan informasi rootpath untuk penyimpanan file
	 *
	 * @return rootpath penyimpanan file
	 */
	public static String getStorageRootpath() {
		Configuration cfg = Configuration.find("cfg_sub_category = ?", "file-storage-rootpath").first();
		if (cfg == null) {
			return null;
		} else return cfg.cfg_value;
	}

    /**
     * dijalankan saat aplikasi startup
     * load all configuration untuk dicache, kalo ada update di Halaman Utility PPE maka seluruh cache
     * configuration harus diupdate
     */
    public static void setupConfig() {
    	String aggr = "Non Aktif";
		if(ConfigurationDao.isEnableInaproc())
			aggr = "Aktif";
		informasiAgregasi += aggr;
    	Logger.info("[STARTUP] *********** Checking Configuration Table ***********");
    	play.db.Configuration configurationdb = new play.db.Configuration(DB.DEFAULT);
		String dbUrl = configurationdb.getProperty("db.url");
		if(!StringUtils.isEmpty(dbUrl))
			ConfigurationDao.MODE = dbUrl.toLowerCase().contains("prod")? Mode.PRODUCTION:Mode.LATIHAN;
		Logger.info("Sistem berjalan dalam mode %s", ConfigurationDao.MODE);
		// setup all configuration di cache
		List<Configuration> list = Query.find("SELECT cfg_category, cfg_sub_category, cfg_value FROM configuration WHERE cfg_category=?", Configuration.class, "CONFIG").fetch();
		for (Configuration configuration : list) {
			if (CommonUtil.isEmpty(configuration.cfg_sub_category))
				continue;
			else if (!ConfigurationDao.isProduction()
					&& configuration.cfg_sub_category.equals(CONFIG.PPE_NAMA.toString()))
				configuration.cfg_value = "[MODE LATIHAN] " + configuration.cfg_value;
			Cache.add(configuration.toCache(), configuration.cfg_value, Configuration.CACHE_DURATION);
		}
//		if(!checkConfiguration())
//		{
//			Cache.add("STARTUP_ERROR", Boolean.TRUE);
//			Logger.error("[STARTUP] Error configuration during startup ");
//		}
		// setting mail sender
		Configuration cfg = Configuration.getConfiguration(MailQueue.SMTP_SENDER);
		if (cfg != null) {
			if (!cfg.cfg_value.endsWith(">")) {
				String lpse = ConfigurationDao.getNamaLpse();
				if (!lpse.startsWith("LPSE"))
					lpse = "LPSE " + lpse;
				cfg.cfg_value = String.format("\"%s\"<%s>", lpse, cfg.cfg_value);
				cfg.save();
			}
		}
    }

	/*private static void setInformasiTanggalUpdate(){
		//infromasi tanggal update
		File dir = new File(Play.applicationPath+"/app");
		if(Play.usePrecompiled)
			dir = new File(Play.applicationPath+"/precompiled");

		String[] extensions = new String[] { "html", "java" };

		List<File> files = (List<File>) FileUtils.listFiles(dir, extensions, true);

		Long max = files.get(0).lastModified();

		for (File file : files) {
			max = file.lastModified() > max ? file.lastModified() : max;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");

		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

		informasiTanggalUpdate = informasiTanggalUpdate + sdf.format(max);

		Logger.info("--Last Modified Date : "+sdf2.format(max));
	}*/
    
    public static Ppe_site getCurrentPPE_Site() {
    	Ppe_site ppe_site = Cache.get("Current_ppe_site", Ppe_site.class);
		if (ppe_site == null) {
			String ppe_id = Configuration.getConfigurationValue(CONFIG.PPE_ID.toString());
			ppe_site = Ppe_site.findById(ppe_id);
			if (ppe_site == null) {
				ppe_site = new Ppe_site();
				ppe_site.pps_nama = (null);
			}
			Cache.add("Current_ppe_site", ppe_site);
		}
		return ppe_site;
	}
    
    public static String getVersiApendo(){
		return Configuration.getConfigurationValue(CONFIG.APENDO_VERSION.toString());
	}
    
    /**
	 * Fungsi {@code nonEprocEnabled} digunakan untuk mendapatkan informasi
	 * apakah fitur pembuatan paket non-eproc aktif atau tidak. Konfigurasi
	 * dengan menggunakan sub kategori {@code non-eproc.enabled}.
	 * 
	 * @return true jika nilai properti {@code non-eproc.enabled} adalah 'true'
	 */
	public static boolean isNonEprocEnabled() {
		return Configuration.getBoolean(CONFIG.NON_EPROC_ENABLED.toString());
	}
	
	/**
	 * cek diperbolehkan download/tidak semua dokumen lelang tertentu
	 * @param lelangId
	 * @return
	 */
	public static boolean isEnableDownload(Long lelangId)
	{
		String idDisable = Configuration.getConfigurationValue(CONFIG.DISABLE_DOWNLOAD_PENAWARAN.toString());
		if(!CommonUtil.isEmpty(idDisable))
		{
			String idDisableArray[] = idDisable.trim().split(";");
			for(int i=0;i<idDisableArray.length;i++){
				if(idDisableArray[i].trim().equals(lelangId.toString()))
					return false;
			}
		}
		return true;
	}
	
	/**
	 * Rest Service Adp
	 * @return
	 */
	public static String getRestCentralService() {
		return ADP_URL +"/adprest";
	}
	
	/**
	 * Rest Service Adp Certificate
	 * @return
	 */
	public static String getRestCentralCertificateService() {
		return ADP_URL+"/certificate-rest";
	}
	public static String getRestCentralCertificateServiceAllCert(String param) {
		return getRestCentralCertificateService()+"/findlist?q="+param;
	}
	public static String getRestCentralCertificateServiceLastCert(String param) {
		return getRestCentralCertificateService()+"/findLast?q="+param;
	}
	public static String getRestCentralCertificateServiceCerIdList(String param) {
		return getRestCentralCertificateService()+"/findCerIdList?q="+param;
	}
	public static String savePemRestCentralCertificateService() {
		return getRestCentralCertificateService()+"/savePem";
	}

	public static String saveRestCentralCertificateService(String param) {
		return getRestCentralCertificateService()+"/save?q="+param;
	}
	public static String deleteRestCentralCertificateService(String param) {
		return getRestCentralCertificateService()+"/delete?q="+param;
	}
	
	/**
	 * Nama Lpse
	 * @return
	 */
	public static String getNamaRepo() {
		return Configuration.getConfigurationValue(CONFIG.PPE_NAMA.toString());
	}
	
	/**
	 * Mendapatkan Nama LPSE berdasarkan ID yang diinputkan
	 * @param repoId
	 * @return
	 */
	public static String getNamaRepo(Long repoId) {
		if(repoId != null) {
			if (repoId.equals(getRepoId()))
				return getNamaRepo();
			Repository repository = Repository.findById(repoId);
			if (repository == null && isEnableInaproc()) {
				String param = URLs.encodePart(DceSecurityV2.encrypt(repoId.toString()));
				String url_service = getRestCentralService() + "/repository?q=" + param;
				try {
					WSRequest request = WS.url(url_service);
					request.setHeader("authentication", Play.secretKey);
					HttpResponse httpResponse = request.timeout(CONNECTION_TIMEOUT).get();
					if (httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
						String response = DceSecurityV2.decrypt(httpResponse.getString());
						repository = CommonUtil.fromJson(response, Repository.class);
						repository.save();
					}
				} catch (Exception e) {
					Logger.error(e, "Kendala Akses getNamaRepo");
				}
			}
			if(repository != null)
				return repository.repo_nama;
		}
		return "A/N"; // tidak ada nama repository
	}
	
	/**Check configuration
	 * 
	 * @return
	 */
	private static boolean checkConfiguration() {
		String[] mandatoryConfig=new String[]{
				CONFIG.PPE_NAMA.toString(), CONFIG.PPE_ID.toString(),
				MailQueue.SMTP_SENDER, MailQueue.SMTP_HOST
			};
		
		boolean ok=true;
		
		   //file storage
        String fileStorage=BlobTableDao.getFileStorageDir();
        if(fileStorage!=null)
        {
        	File file=new File(fileStorage + File.separator + UUID.randomUUID());
        	try
        	{
        		file.createNewFile();
        		file.delete();
        	}
        	catch(IOException e)
        	{
        		Logger.error("[STARTUP] Please check folder permission (rwx) or existence. Cannot write to folder: '%s'", file);
        		ok=false;        		
        	}
        }
		
	
		
		for(String val:mandatoryConfig) {
            String config = Configuration.getConfigurationValue(val);
			if(config == null || config.isEmpty())
			{
				Logger.error("[STARTUP] Null value in CONFIGURATION:'CONFIG'.'%s'", val);
				ok=false;
			}
		}
		
		return ok;
	}
	
	public static boolean isEnableICB() {
		return enableICB;
	}

	public static String getVersi() {
		String spseVersion = SPSE_VERSION;
		if(CertificateUtil.enableCa) {
			spseVersion = spseVersion + "-CA";
		}
		return "SPSE v"+spseVersion;
	}
	
	public static String getAppVersion(){
		String aggr = "Non Aktif";
		if(isEnableInaproc())
			aggr = "Aktif";
		return SPSE_VERSION + " - Agregrasi Inaproc: " + aggr;
	}

	public static boolean isOSD(Date checkDate) {
		if(CertificateUtil.enableCa) {
			Date osdDate = Configuration.getDate(CONFIG.AMANDA_START_DATE.category);
			if(osdDate != null)
				return checkDate.after(osdDate);
		}
		return false;
	}

	public static boolean isEnableMultiUserInfo() {
		return Configuration.getBoolean(CONFIG.MULTI_SESSION_USER_INFO.category);
	}

	public static boolean isEnabledItemize()
	{
		return Configuration.getBoolean(CONFIG.ENABLED_ITEMIZED.toString());
	}

	public static boolean isEnableDevPartner() {
		return enableDevPartner;
	}
}
