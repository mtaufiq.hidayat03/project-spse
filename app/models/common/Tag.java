package models.common;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.STRING)
public enum Tag {

	BERITA("Berita Pengadaan"), 
	JENIS_LPSE("Jenis LPSE"), 
	JENIS_LELANG("Jenis Tender"),
	SANGGAHAN_BANDING("Sanggahan lelang"), 
	SYARAT_VERIFIKASI("Syarat Verifikasi"), 
	METODE_PEMILIHAN("Metode Pemilihan"),
	DATA_PENYEDIA("Data Penyedia"),
	KONTRAK("Kontrak"),
	PPE("Ppe");
	
	public final String label;
	
	private Tag(String label){
		this.label = label;
	}	
}
