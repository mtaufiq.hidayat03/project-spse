package models.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum StatusLelang {

	DRAFT(0, Messages.get("lelang.status0")),
	AKTIF(1,Messages.get("lelang.status1")),
	DITUTUP(2,Messages.get("lelang.status2")),
	SELESAI(3,Messages.get("lelang.status3")),
	DITOLAK(4,Messages.get("lelang.status4")),
	DRAFT_UKPBJ(5,Messages.get("lelang.status5")),
	DRAFT_POKJA(6, Messages.get("lelang.status6"));
	
	public final Integer id;
	public final String label;
	
	StatusLelang(int key, String label){
		this.id = Integer.valueOf(key);
		this.label = label;
	}
	
	public static StatusLelang fromValue(Integer value){
		StatusLelang status = null;
		if(value != null) {
			switch (value.intValue()) {
			case 0:	status = StatusLelang.DRAFT;break;
			case 1:	status = StatusLelang.AKTIF;break;
			case 2:	status = StatusLelang.DITUTUP;break;
			case 3:	status = StatusLelang.SELESAI;break;
			case 4:	status = StatusLelang.DITOLAK;break;
			default: status = StatusLelang.DRAFT;break;
			}
		}
		return status;
	}
	
	/**
	 * cek status lelang Draft atau bukan	
	 * @return
	 * @author arief 
	 * date : oktober 2009
	 */
	public boolean isDraft(){	return this == DRAFT;		}
	
	/**
	 * cek status lelang Ditutup atau bukan	 
	 * @return
	 * @author arief 
	 * date : oktober 2009
	 */
	public boolean isDitutup(){ 	return this == DITUTUP; 	}
	
	/**
	 * cek status lelang Ditolak atau tidak	 
	 * @return
	 */
	public boolean isDitolak(){ 	return this == DITOLAK;	}
	
	/**
	 * cek status lelang Aktif atau tidak	 
	 * @return
	 * @author arief 
	 * date : oktober 2009
	 */
	public boolean isAktif(){	return this == AKTIF;	}
	
	/**
	 * cek status lelang selesai atau belum
	 * method ini belum bisa digunakan karena belum ada action trigger dari user
	 * untuk mengubah status lelang menjadi selesai
	 * @return
	 * @author arief 
	 * date : oktober 2009
	 */
	public boolean isSelesai(){ 	return this == SELESAI;		}
}
