package models.common;

import controllers.BasicCtr;
import models.lelang.Jadwal;
import models.lelang.Lelang_seleksi;
import models.lelang.Persetujuan;
import models.lelang.ReverseAuction;
import org.apache.commons.collections4.CollectionUtils;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * mendapatkan Informasi Tahap lelang
 * ijin akses seluruh dokumen dan flow lelang sedang berlangsung harus diterapkan di sini
 */
public class TahapNow {

	final Long lelangId;
	final List<Tahap> tahapList;
	boolean express;
	final Active_user active_user;
	StatusLelang status;
	boolean auction;
	boolean sedangPersetujuanPemenang;

	public TahapNow(Long lelangId) {
		this.lelangId = lelangId;
		this.active_user = Active_user.current();
		Query.find("SELECT lls_status, mtd_pemilihan, lls_metode_penawaran FROM Lelang_seleksi WHERE lls_id=?", new ResultSetHandler<Object>() {
			@Override
			public Object handle(ResultSet rs) throws SQLException {
				status = StatusLelang.fromValue(rs.getInt("lls_status"));
				MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
				express = pemilihan.isLelangExpress();
				auction = rs.getInt("lls_metode_penawaran") == 1;
				return null;
			}			
		}, lelangId).first();
		tahapList = Aktivitas.findTahapNow(lelangId);
		sedangPersetujuanPemenang = Persetujuan.isSedangPersetujuanPemenang(lelangId);
	}
	
	public TahapNow(Lelang_seleksi lelang) {
		this.lelangId = lelang.lls_id;
		this.express = lelang.getPemilihan().isLelangExpress();
		this.active_user = Active_user.current();
		this.status = lelang.lls_status;
		this.auction = lelang.isAuction();
//		ProcessInstance pi = WorkflowDao.findProcessBylelang(lelangId);
//		if(pi != null) {
//			tahapList = WorkflowDao.findAktifState(pi.process_instance_id);
//		} else {
		tahapList = Aktivitas.findTahapNow(lelangId);
		sedangPersetujuanPemenang = lelang.isSedangPersetujuanPemenang();
//		}
//		Logger.info("TahapNow : %s", tahapList);
	}

	public boolean isPemasukanDokPra() {
		return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_DOK_PRA);
	}
	
	public boolean isPemasukanPenawaran() {
		return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN);
	}
	
	public boolean isTahapanEvaluasiPra() {
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.EVALUASI_DOK_PRA);
	}
	
	public boolean isPemasukanPenawaranHarga() {
		return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN_BIAYA);
	}
	
	public boolean isPemasukanPenawaranAdmTeknis() {
		return tahapList != null && tahapList.contains(Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS);
	}
	
	public boolean isEvaluasi() {
		if(express)
			return isPemasukanBerakhir();
		return tahapList != null && CollectionUtils.containsAny(tahapList, Tahap.tahapanEvaluasi);
	}
	
	public boolean isPenetapanHasilPra() {
		return tahapList != null && tahapList.contains(Tahap.PENETAPAN_HASIL_PRA);
	}
	
	public boolean isPenetapanPemenangTeknis() {
		return tahapList != null && (tahapList.contains(Tahap.PENETAPAN_PEMENANG_ADM_TEKNIS) || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS) || tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS));
	}

	public boolean isPengumumanPemenangTeknis() {
		return tahapList != null && tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS);
	}
	
	public boolean isEvaluasiTeknis() {
		return tahapList != null && (tahapList.contains(Tahap.EVALUASI_PENAWARAN) || tahapList.contains(Tahap.EVALUASI_PENAWARAN_ADM_TEKNIS) ||
				tahapList.contains(Tahap.EVALUASI_PENAWARAN_KUALIFIKASI) || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI) ||
				tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS) || tahapList.contains(Tahap.PENETAPAN_PEMENANG_ADM_TEKNIS));
	}
	
	public boolean isEvaluasiHarga() {
		return tahapList != null && (tahapList.contains(Tahap.EVALUASI_PENAWARAN) || tahapList.contains(Tahap.EVALUASI_PENAWARAN_BIAYA) ||
				tahapList.contains(Tahap.EVALUASI_PENAWARAN_KUALIFIKASI) ||
				tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA));
	}
	
	public boolean isEvaluasiKualifikasi() {
		return tahapList != null && (tahapList.contains(Tahap.PEMASUKAN_DAN_EVALUASI_DOK_PRA) || tahapList.contains(Tahap.EVALUASI_PENAWARAN_KUALIFIKASI) ||
				tahapList.contains(Tahap.EVALUASI_DOK_PRA) || tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI)
			|| tahapList.contains(Tahap.EVALUASI_PENAWARAN_KUALIFIKASI) || tahapList.contains(Tahap.EVALUASI_PENAWARAN));
	}
	
	/*
	 * public boolean isPembuktian() { return tahapList != null &&
	 * (tahapList.contains(Tahap.VERIFIKASI_KUALIFIKASI) ||
	 * tahapList.contains(Tahap.PENETAPAN_HASIL_PRA)); }
	 */
	
	public boolean isPembuktian() {
		return tahapList != null && (tahapList.contains(Tahap.VERIFIKASI_KUALIFIKASI));
	}
	
	public boolean isPenetapaPemenang() {
		if(express)
			return isPemasukanBerakhir();
		return tahapList != null && (tahapList.contains(Tahap.PENETAPAN_PEMENANG_AKHIR) || tahapList.contains(Tahap.PENETAPAN_PEMENANG_PENAWARAN)||
				 tahapList.contains(Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR));
	}

    public boolean isUndanganPra() {
        return tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA) || tahapList.contains(Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK));
    }

    public boolean isPengumuman_Pemenang() {
    	if(express)
			return isPemasukanBerakhir();
        return tahapList != null && (tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR) || tahapList.contains(Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR));
    }

	public boolean isKlarifikasiTeknisBiaya(){
		return tahapList != null && (tahapList.contains(Tahap.KLARIFIKASI_TEKNIS_BIAYA) || tahapList.contains(Tahap.KLARIFIKASI_NEGOSIASI));
	}

	public boolean isSanggahPra() {
		return tahapList != null && tahapList.contains(Tahap.SANGGAH_PRA);
	}

	public boolean isSanggahAdm() {
		return tahapList != null && tahapList.contains(Tahap.SANGGAH_ADM_TEKNIS);
	}

	public boolean isSanggah() {
		return tahapList != null && tahapList.contains(Tahap.SANGGAH);
	}

    /**
     * peserta bisa mendaftar lelang sampai batas akhir upload penawaran / upload dok prakualifikasi
     * hanya jika paket dibuat versi 3.5, jika versi 4 memakai workflow engine
     * @param lelangId
     * @return
     *
     */
    public static boolean isAllowPendaftaran(Long lelangId, boolean prakualifikasi, boolean express)
    {
    	Date now = controllers.BasicCtr.newDate();
    	Long count = 0L;
    	if(express) {
    		count = Aktivitas.count("akt_id in (select akt_id from jadwal where lls_id=? AND dtj_tglakhir >= ?) AND akt_jenis in ('PEMASUKAN_PENAWARAN')", lelangId, now);
    	} else if(prakualifikasi) {
			count = Aktivitas.count("akt_id in (select akt_id from jadwal where lls_id=? AND dtj_tglawal <= ? AND dtj_tglakhir >= ?) AND akt_jenis in ('UMUM_PRAKUALIFIKASI', 'PEMASUKAN_DOK_PRA','AMBIL_DOK_PRA')", lelangId, now, now);
    	} else { // lelang pasca
			count = Aktivitas.count("akt_id in (select akt_id from jadwal where lls_id=? AND dtj_tglawal <= ? AND dtj_tglakhir >= ?) AND akt_jenis in ('PENGUMUMAN_LELANG','AMBIL_DOKUMEN')", lelangId, now, now);
    	}
    	return count > 0;
    }

    /**
     * cek apakah tahap pemasukan sudah berakhir ato belum
     * true : jika sudah berakhir
     * false : jika belum berakhir
     * @return
     */
    public boolean isPemasukanBerakhir() {
    	Jadwal jadwal = Jadwal.findByLelangNTahap(lelangId, Tahap.PEMASUKAN_PENAWARAN);
		return jadwal != null && jadwal.dtj_tglakhir != null && jadwal.isEnd(BasicCtr.newDate());
    }

	public boolean isEvaluasiKualifikasiBerakhir() {
		Jadwal jadwal = Jadwal.findByLelangNTahap(lelangId, Tahap.EVALUASI_DOK_PRA);
		return jadwal != null && jadwal.dtj_tglakhir != null && jadwal.isEnd(BasicCtr.newDate());
	}

	public boolean isAllowPenetapanPemenangTeknis() {
		return tahapList != null && (tahapList.contains(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS) || tahapList.contains(Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS)
				||tahapList.contains(Tahap.PENETAPAN_PEMENANG_ADM_TEKNIS));
	}

	@Override
	public String toString() {
		if(tahapList == null || tahapList.isEmpty())
			return "TahapNow @"+lelangId;
		return "TahapNow @"+lelangId+" [" + tahapList.toString() + ']';
	}

	public boolean isAllowPenetapanPemenangAkhir() {
		boolean allow = isPenetapaPemenang() && !sedangPersetujuanPemenang ;
		if(allow && auction) {
			ReverseAuction auction = ReverseAuction.findByLelang(lelangId);
			allow = auction != null &&  auction.ra_status.isSelesai();
		}
		return allow;
	}

	public boolean isSedangPersetujuanPemenang() {
		return sedangPersetujuanPemenang;
	}

	public boolean isAllowEditJadwal() {
		boolean allowEditJadwal = active_user.isPanitia() && status.isAktif();
		if(express && allowEditJadwal){ // Jika tender cepat, edit jadwal sudah tidak diperbolehkan bila sudah ada yang menawar
			long jumlahPenawar = 0L;
			ReverseAuction auction = ReverseAuction.findByLelang(lelangId);
			if(auction != null) {
				jumlahPenawar = auction.getJumlahPenawar();
			} else { // tender cepat yg lama tidak memakai reverse auction
				String sql = "select count(psr_id) from (select psr_id, (select MAX(dok_id) from dok_penawaran where psr_id=p.psr_id and dok_jenis=2 and dok_disclaim=1) as harga from peserta p where p.lls_id=?) penawaran_peserta where harga is not null";
				jumlahPenawar =  Query.count(sql, lelangId);
			}
			allowEditJadwal = allowEditJadwal && ( jumlahPenawar < 2 || (isPemasukanBerakhir() && jumlahPenawar > 1));
		}
		return allowEditJadwal;
	}

}
