package models.common;

public interface IEncryptedId {
	/**
	 * Id is encrypted Implementing class must decrypt ID and set to
	 * corresponding class ID Example: setAgc_id(decryptId(id));
	 */
	public void setId(String id);

	/**
	 * Get encrypted ID from current ID
	 */
	public String getId();

}
