package models.common;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.STRING)
public enum SumberDana {
	APBD("APBD"), APBN("APBN"),  PHLN("PHLN"), PNBP("PNBP"), APBNP("APBNP"), APBDP("APBDP"), BLU("BLU"), BLUD("BLUD"), 
	BUMN("BUMN"), BUMD("BUMD"), LAINNYA("LAINNYA"), GABUNGAN_APBN_DAN_APBD("Gabungan APBN dan APBD"), SBSN("SBSN"), ADB("ADB");
	
	private final String label;
	
	private SumberDana(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
}
