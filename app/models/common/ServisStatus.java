package models.common;

import controllers.BasicCtr;
import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.Response;
import play.Logger;
import play.libs.WS;

public class ServisStatus {

    public static final String TAG = "ServiceUpTime";

    public static final ServisStatus adp = new ServisStatus("ADP", false, ConfigurationDao.getRestCentralService()+"/versi");
    public static final ServisStatus inaproc = new ServisStatus("INAPROC", false, BasicCtr.INAPROC_URL+"/api/blacklist/check/npwp"); //URL yg dipakai check API blacklist, karena API ini yg dibutuhkan SPSE
    public static final ServisStatus sikap = new ServisStatus("SIKaP", false, BasicCtr.SIKAP_URL+"/services/wastendersent");
    public static final ServisStatus sirup = new ServisStatus("SiRUP", false, BasicCtr.SIRUP_URL+"/service/program");
    public static final ServisStatus jaim = new ServisStatus("JaIM", false, "https://jaim.lkpp.go.id/servicePack/versi");
    public static final ServisStatus ams = new ServisStatus("AMS", false, "");
    public static final ServisStatus kms = new ServisStatus("KMS", false, "");
    public String jenis;
    public boolean status;
    public String statusLabel;
    public String urlCheck; // selain AMS dan KMS, maka URL wajib diisi

    public ServisStatus(String jenis, boolean status, String urlCheck) {
        this.jenis = jenis;
        this.status = status;
        this.statusLabel = status ? "Online":"Offline";
        this.urlCheck = urlCheck;
    }

    public void setStatus(boolean status) {
        this.status = status;
        statusLabel = status ? "Online":"Offline";
    }

    public void check() {
        WS.url(urlCheck).send(new AsyncCompletionHandler() {
            @Override
            public Object onCompleted(Response response) throws Exception {
                if(response.getStatusCode() == 200)
                    setStatus(true);
                else
                    setStatus(false);
                return null;
            }

            @Override
            public void onThrowable(Throwable t) {
                Logger.error(t, "kendala check servis %s", jenis);
                setStatus(false);
            }
        });
    }
}
