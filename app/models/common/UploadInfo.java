package models.common;

import ext.FormatUtils;
import models.jcommon.blob.BlobTable;

import java.io.Serializable;

/**
 * model untuk informasi file yang diupload oleh user
 * @author lkpp
 *
 */
public class UploadInfo implements Serializable {

	public Long id;
	
	public String name;
	
	public String size;
	
	public Integer versi;
	
	public String upload_date;
	
	public String download_url;
	
	public static UploadInfo findBy(BlobTable blob) {
		UploadInfo info = new UploadInfo();
		info.id = blob.blb_id_content;
		info.name = blob.blb_nama_file;
		info.size = FormatUtils.formatBytes(blob.getFile().length());
		info.versi = blob.blb_versi;
		info.download_url = blob.getDownloadUrl(null);
		info.upload_date = FormatUtils.formatDateTimeInd(blob.blb_date_time);
		return info;
	}
}
