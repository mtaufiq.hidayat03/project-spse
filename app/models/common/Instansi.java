package models.common;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import controllers.BasicCtr;
import models.agency.Paket_satker;
import models.jcommon.util.CommonUtil;
import models.sso.common.Kabupaten;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;


/**
 * Created by IntelliJ IDEA. Date: 28/08/12 Time: 17:18
 *
 * @author I Wayan Wiprayoga Wisesa
 */
@Table(name="INSTANSI")
public class Instansi extends BaseTable {
	/**
	 * Enumerasi jenis instansi
	 */
	public static enum JenisInstansi {
		KEMENTERIAN("Kementerian"), LEMBAGA("Lembaga"), PROPINSI("Propinsi"),
		KABUPATEN("Kabupaten"), KOTA("Kota"), INSTITUSI("Institusi Lainnya"),
		BUMN("BUMN");

		private String value;

		JenisInstansi(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}
	}

	/**
	 * ID Instansi
	 */
	@Id
	public String id;
	/**
	 * alamat instansi
	 */
	public String alamat;
	/**
	 * Jenis instansi
	 */
	public String jenis;
	
	
	/**
	 * Kabupaten
	 */
	public Long kbp_id;
	
	/**
	 * Nama instansi
	 */
	public String nama;	
	/**
	 * Website instansi
	 */
	public String website;
	
	public Date auditupdate;
	
	@Transient
	private Kabupaten kabupaten;
	
	public Kabupaten getKabupaten() {
		if(kabupaten == null)
			kabupaten = Kabupaten.findById(kbp_id);
		return kabupaten;
	}
	
	public static String findNamaInstansiPaket(Long paketId) {
		return Paket_satker.getNamaInstansiPaket(paketId);
	}
	
	/**
	 * Fungsi {@code findSemuaByJenis} digunakan untuk mendapatkan semua data instansi berdasarkan jenis instansi
	 * dan sudah terurut berdasarkan nama instansi secara ascending
	 *
	 * @param jenis_instansi enumerasi jenis instansi
	 * @return daftar instansi yang jenis instansinya sesuai dengan parameter {@code jenis_instansi}
	 */
	public static List<Instansi> findSemuaByJenis(JenisInstansi jenis_instansi) {
		return find("jenis = ? ORDER BY nama ASC", jenis_instansi.getValue().toUpperCase()).fetch();
	}
	
	/**
	 * serializer JSON intansi , untuk kepentingan efisiensi 
	 * jadi hanya field id dan nama yang di hasilkan, tidak semua field 
	 */
	public static final JsonSerializer<Instansi> serializer  = new JsonSerializer<Instansi>() {

		@Override
		public JsonElement serialize(Instansi instansi, Type type, JsonSerializationContext context) {
			JsonObject object = new JsonObject();
			object.addProperty("id", instansi.id);
			object.addProperty("nama", instansi.nama);
			return object;
		}
		
	};

	/**
	 * simpan instansi
	 * @param list
     */
	public static void simpan(Instansi[] list) {
		if(list != null && list.length > 0) {
			for(Instansi obj : list) {
				if (obj == null || obj.id.equals("ZONK"))
					continue;
				obj.save();
			}
		}
		Instansi.delete("id=?","ZONK"); // delete instansi dengan ID 'ZONK', di sirup terdapat data itu
	}

	public static void updateFromSirup() {
		try {
			WSRequest request = WS.url(BasicCtr.SIRUP_URL+"/service/daftarKLDI");
			Logger.debug("get data Instansi sirup");
			HttpResponse response = request.get();
			if (response.success() && !StringUtils.isEmpty(response.getString())) {
				Instansi[] list = CommonUtil.fromJson(response.getString(), Instansi[].class);
				simpan(list);
			}
		}catch (Exception e) {
			Logger.error(e,"Kendala Akses Service Instansi Sirup");
		}
	}
}
