package models.common;


public enum MetodeDokumen {
	
		SATU_FILE (0, "Satu File"),
		DUA_FILE (1, "Dua File"),
		DUA_TAHAP (2, "Dua Tahap");
		
		public final Integer id;
		public final String label;
		
		MetodeDokumen(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}
		
		public static MetodeDokumen fromValue(Integer value){
			MetodeDokumen dokkumen = null;
			if(value != null){
				switch(value.intValue()){
				case 0: dokkumen = SATU_FILE;break;
				case 1: dokkumen = DUA_FILE;break;	
				case 2: dokkumen = DUA_TAHAP;break;
				default: dokkumen = SATU_FILE;break;
				}
			}
			return dokkumen;
		}
				
		public boolean isSatuFile(){
			return this == SATU_FILE;
		}
		
		public boolean isDuaFile(){
			return this == DUA_FILE;
		}
		
		public boolean isDuaTahap(){
			return this == DUA_TAHAP;
		}

		public static final MetodeDokumen[] metodeDokumenPra = new MetodeDokumen[]{DUA_FILE, DUA_TAHAP};
		public static final MetodeDokumen[] metodeDokumenPraSeleksi = new MetodeDokumen[]{DUA_FILE};
		public static final MetodeDokumen[] metodeDokumenPasca = new MetodeDokumen[]{SATU_FILE, DUA_FILE};
		public static final MetodeDokumen[] metodeDokumenExpress = new MetodeDokumen[]{SATU_FILE};

}
