package models.common;

import ams.contracts.ActiveUserAms;
import ams.contracts.AmsContract;
import ams.contracts.AmsUserContract;
import controllers.BasicCtr;
import ext.FormatUtils;
import jobs.JobActiveUser;
import models.agency.Pegawai;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.Usrsession;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.mvc.Scope.Session;
import utils.LogUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * class ini hanya untuk menampung object session user yang sedang aktif / untuk mendapatkan informasi user yang sedang login
 * simpan seluruh data yang diperlukan untuk keperluan manejemen sesi
 * @author Arief Ardiyansah
 */

public class Active_user implements ActiveUserAms, Serializable {

	public static final String TAG = "Active_user";

	public Long id; // id session di database (from models.secman.Usrsession::sessionid)

	public String userid;
	
	public String sessionid; // id session aplikasi
	
	public String name;

	public String remoteaddress;

	public long logintime;
	
	public long lastRequest;
	
	public Group group;
	
	public Long rekananId; // jika group rekanan, field ini harus terisi
	
	public Long pegawaiId; // jika login pegawai , field ini harus terisi
	
	public Long ppkId; // jika group ppk , field ini harus terisi
	
	public Long ppId; // jika group pejabat pengadaan, field ini harus terisi
	
	public Long agencyId; // jika group agency, field ini harus terisi

	public Long ukpbjId;

	public AmsContract ams;

	public Long certId; // must be filled if it's rekanan/panitia

	/** Mendapatkan informasi user yang ditampilkan di bawah tombol logout */
	public String userlabel;
	
	// untuk kebutuhan integrasi data penyedia dengan SIKaP
	public int statusMigrasi = 0;
	
	public String getWaktu_login() { 
		return FormatUtils.formatDateTimeInd(new Date(logintime));
	}
	
	// flag user sedang logged in atau tidak
	public static boolean isLoggedIn() {
		return JobActiveUser.isLogged(Session.current().getId());
	}
	
	// jumlah login yang sama disaat bersamaan, klo ada harus ada notifikasi
	public int getLoggedCount() {
		return JobActiveUser.getLoggedCount(this);
	}
		
	//save active user ke map
	@Override
	public void save() {
		// setup user label
		 StringBuilder label = new StringBuilder();
		 String groupName = group.getLabel();
		/*
	       * Untuk nama sbb Rekanan => nama perusahaan; title: [userId] NPWP
	       * Selainnya => nama pegawai; title: [userId] NIP
	       */
	      if (isRekanan()) {
	        Rekanan rekanan = Rekanan.findByNamaUser(userid);
	        label.append("<span title=\"[").append(userid).append("] - ").append(rekanan.rkn_npwp).append("\">").append(rekanan.rkn_nama).append(" - ").append(groupName).append("</span>");
	      } else {
	        Pegawai pegawai = Pegawai.findBy(userid);
	        if (pegawai == null) {
	        	label.append(userid).append(" - ").append(groupName);
	        } else {
//	          label.append("<a href=\"").append(Router.reverse("BerandaCtr.pilihRoles")).append("\" class=\"dialog\" title=\"Pilih Kepanitiaan\" >
	          label.append("<span title=\"[").append(userid).append("] - ").append(pegawai.peg_nip).append("\">");

	          if(isPanitia()){
	          	groupName = groupName.replace("Anggota ","");
			  }

	          label.append(pegawai.peg_nama).append(!CommonUtil.isEmpty(groupName)? " - "+groupName:"").append("</span></a>");
	        }
	      }
	    userlabel = label.toString();
	    lastRequest = logintime;
	    JobActiveUser.add(this);
	    Session.current().put(BaseModel.SESSION_USERID, userid);
	}
	
	public void updateLoggedIn() {
		JobActiveUser.update(this);
	}

	@Override
	public Group getGroup() {
		return this.group;
	}

	// get All active user
	public static List<Active_user> findAll() {
		return JobActiveUser.findAll();
	}
	
	// get Current Active User
	public static Active_user current() {
		return JobActiveUser.get(Session.current().getId());
	}
	
	// clear Current Active User
	public static void clear() {
		Active_user active_user = current();
		if(active_user != null) {
			Session.current().remove(BaseModel.SESSION_USERID);
			Usrsession.setWaktuLogout(active_user.id, BasicCtr.newDate()); // set waktu logout pada tabel usrsession
			Logger.debug("remove %s from Map Active User", active_user.userid);
			JobActiveUser.remove(active_user.sessionid);
		}
	}
	
	// check setiap user terhadap action yang dilakukan ke system
	// user hanya diijinkan akses action sesuai dengan setingan controllers.security.AllowAccess
	public boolean isAuthorized(Group[] groups) { 
		
		if (!StringUtils.isEmpty(userid)) {
				// Cek apakah grup mendapat hak akses sesuai dengan anotasi yang didapat Hanya user yang aktif, jika tidak ada maka user tersebut harus di
				// logout Ijinkan untuk semua jenis user, jika daftarGroup berisi:{ALL_AUTHORIZED_USERS}
				// tidak boleh trainer dapat akses login pada sistem production
				return (ArrayUtils.contains(groups, Group.ALL_AUTHORIZED_USERS) || ArrayUtils.contains(groups, group))
						&& !(isTrainer() && ConfigurationDao.isProduction()) && sessionid.equals(Session.current().getId());
		}
		return false;
	}
	
	/**
	 * Fungsi {@code isAdminPPE} digunakan untuk pengecekan role pengguna apakah
	 * ADMIN_PPE atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role ADMIN_PPE
	 */
	public boolean isAdminPPE() {
		return group != null && group.isAdminPPE();
	}
	

	/**
	 * Fungsi {@code isAdminAgency} digunakan untuk pengecekan role pengguna
	 * apakah ADMIN_AGENCY atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role ADMIN_AGENCY
	 */
	public boolean isAdminAgency() {		
		return group != null && group.isAdminAgency();
	}
	
	/**
	 * Fungsi {@code isPanitia} digunakan untuk pengecekan role pengguna apakah
	 * PANITIA atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role PANITIA
	 */
	@Override
	public boolean isPanitia() {
		return group != null && group.isPanitia();
	}

	@Override
	public Long getRekananId() {
		return this.rekananId;
	}

	@Override
	public Long getPegawaiId() {
		return this.pegawaiId;
	}

	@Override
	public String getUniqueId() {
		return getUser() != null ? getUser().getUniqueId() : "";
	}

	/**
	 * Fungsi {@code isPpk} digunakan untuk pengecekan role pengguna apakah PPK
	 * atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role PPK
	 */
	public boolean isPpk() {		
		return group != null && group.isPpk();
	}
	
	/**
	 * Fungsi {@code isRekanan} digunakan untuk pengecekan role pengguna apakah
	 * REKANAN atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role REKANAN
	 */
	@Override
	public boolean isRekanan() {		
		return group != null && group.isRekanan();
	}
	
	/**
	 * Fungsi {@code isTrainer} digunakan untuk pengecekan role pengguna apakah
	 * TRAINER atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role TRAINER
	 */
	public boolean isTrainer() {		
		return group != null && group.isTrainer();
	}
	
	/**
	 * Fungsi {@code isVerifikator} digunakan untuk pengecekan role pengguna
	 * apakah VERIFIKATOR atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role VERIFIKATOR
	 */
	public boolean isVerifikator() {		
		return group != null && group.isVerifikator();
	}
	
	/**
	 * Fungsi {@code isHelpdesk} digunakan untuk pengecekan role pengguna apakah
	 * HELPDESK atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role HELPDESK
	 */
	public boolean isHelpdesk() {		
		return group != null && group.isHelpdesk();
	}
	
	/**
	 * Fungsi {@code isAuditor} digunakan untuk pengecekan role pengguna apakah
	 * AUDITOR atau bukan
	 * 
	 * @return true untuk pengguna yang memiliki role AUDITOR
	 */
	public boolean isAuditor() {		
		return group != null && group.isAuditor();
	}

	public boolean isUkpbj() {
		return group != null && group.isUkpbj();
	}

	public boolean isKuppbj() {
		return group != null && group.isKuppbj();
	}

	public boolean isPP() {
		return group != null && group.isPP();
	}
	
	public boolean isPPHP() {
		return group != null && group.isPPHP();
	}

	public boolean isCanEditDataPenyedia() {
		return isBelumMigrasi();
	}
	
	public boolean isBelumMigrasi() {
		return statusMigrasi == Rekanan.MIGRASI_SIKAP_BELUM;
	}
	
	public boolean isSedangMigrasi() {
		return statusMigrasi == Rekanan.MIGRASI_SIKAP_PROSES;
	}
	
	public boolean isSelesaiMigrasi() {
		return statusMigrasi == Rekanan.MIGRASI_SIKAP_SELESAI;
	}
	
	public static boolean isCurrentBelumMigrasi() {
		Active_user active_user = current();
		return active_user.isRekanan() && active_user.isBelumMigrasi();
	}
	
	public static boolean isCurrentSedangMigrasi() {
		Active_user active_user = current();
		return active_user.isRekanan() && active_user.isSedangMigrasi();
	}
	
	public static boolean isCurrentSelesaiMigrasi() {
		Active_user active_user = current();
		return active_user.isRekanan() && active_user.isSelesaiMigrasi();
	}

	@Override
	public AmsContract getAms() {
		return this.ams;
	}

	@Override
	public AmsUserContract getUser() {
		LogUtil.debug(TAG, "get user contract");
		if (isRekanan()) {
			return Rekanan.findByNamaUser(getUserId());
		} else {
			return Pegawai.findBy(getUserId());
		}
	}

	@Override
	public void setAms(AmsContract model) {
		this.ams = model;
	}

	@Override
	public String getUserId() {
		return this.userid;
	}

	@Override
	public void setCertId(Long certId) {
		LogUtil.debug("ActiveUser", "set certId");
		if (certId != null && certId != 0) {
			this.certId = certId;
		}
	}

	@Override
	public Long getCerId() {
		if (this.certId != null && this.certId != 0) {
			return this.certId;
		}
		if (isRekanan()) {
			Rekanan rekanan = Rekanan.findById(rekananId);
			if (rekanan != null) {
				this.certId = rekanan.cer_id;
			}
		} else if (isPanitia()) {
			Pegawai pegawai = Pegawai.findByPegId(pegawaiId);
			if (pegawai != null) {
				this.certId = pegawai.cer_id;
			}
		}
		return this.certId;
	}

}
