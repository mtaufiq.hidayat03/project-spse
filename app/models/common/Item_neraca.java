package models.common;

public enum Item_neraca{

	AKTIVA_TETAP(1,"A","Aktiva Tetap"),
	AKTIVA_LANCAR(2,"A","Aktiva Lancar"),
	AKTIVA_LAINNYA(3,"A","Aktiva Lainnya"),
	HUTANG_JANGKA_PANJANG(4,"P","Hutang Jangka Panjang"),
	HUTANG_JANGKA_PENDEK(5,"P","Hutang Jangka Pendek");
	
	public final int id;
	public final String kategori;
	public final String label;
	private Item_neraca(int id, String kategori,String label) {
		this.id = id;
		this.kategori = kategori;
		this.label = label;
	}
	
	public static Item_neraca findById(int id)
	{
		Item_neraca item = null;
		switch (id) {
		case 1: item = AKTIVA_TETAP; break;
		case 2: item = AKTIVA_LANCAR; break;
		case 3: item = AKTIVA_LAINNYA; break;
		case 4: item = HUTANG_JANGKA_PANJANG; break;
		case 5: item = HUTANG_JANGKA_PENDEK; break;
		default:
			break;
		}
		return item;
	}
	
	public static Item_neraca[] findByKategori(String kategori)
	{
		if(kategori.equals("A"))
		{
			return new Item_neraca[]{AKTIVA_TETAP, AKTIVA_LANCAR, AKTIVA_LAINNYA};
		}
		else {
			return new Item_neraca[]{HUTANG_JANGKA_PANJANG, HUTANG_JANGKA_PENDEK};
		}
	}
	
}
