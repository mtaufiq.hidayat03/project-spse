package models.common;

import com.google.gson.annotations.SerializedName;
import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum Status_kepemilikan {
	@SerializedName("0")
	MILIK_SENDIRI(0,"Milik Sendiri"),
	@SerializedName("1")
	SEWA(1,"Sewa"),
	@SerializedName("2")
	DUKUNGAN(2,"Dukungan");
	
	public final Integer id;
	public final String label;
	
	private Status_kepemilikan(int id, String label) {
		this.id =Integer.valueOf(id);
		this.label = label;
	}
	
	public static Status_kepemilikan findById(Long id)
	{
		if(id != null)
		{
			if(id.intValue() == 0)
				return MILIK_SENDIRI;
			else if(id.intValue() == 1)
				return SEWA;
			else if(id.intValue() == 2)
				return DUKUNGAN;
		}
		return null;
	}
}
