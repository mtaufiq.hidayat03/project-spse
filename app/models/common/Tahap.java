package models.common;

import models.jcommon.util.CommonUtil;
import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.Arrays;
import java.util.List;


/**
 * Model tahap yang yang ada dalam proses lelang
 * semua tahap harus didefinisikan di sini, sebelum diimplementasikan
 * spse 3.5 mengacu ke tahap-tahap ini, spse4 harus juga mengacu kesini
 * Penamaan tahap sebisa mungkin memakai bahasa yang mudah dipahami oleh programmer dan harus sama dengan regulasi (red: perpres 70)
 * 
 * @author Arief ardiyansah
 *
 */
@Enumerated(EnumType.STRING)
public enum Tahap {

	AMBIL_DOKUMEN_PEMILIHAN(18776,"thp.dwn_dc_pmlhn"), // untuk Proses PRA
	AMBIL_DOKUMEN(18777,"thp.1"), // untuk Proses PASCA
	AMBIL_DOK_PRA(18778,"thp.2"),
	EVALUASI_DOK_PRA(18779,"thp.3"), //18780, 18779, 18831
	EVALUASI_PENAWARAN(18781,"thp.4"),
	EVALUASI_PENAWARAN_ADM_TEKNIS(18782,"thp.5"),
	EVALUASI_PENAWARAN_BIAYA(18783,"thp.6"),
	KLARIFIKASI_TEKNIS_BIAYA(18784,"thp.7"),
	KLARIFIKASI_KEWAJARAN_HARGA(18785,"thp.8"),
	KOREKSI_ARITMATIK(18786,"thp.9"),
	SANGGAH_ADM_TEKNIS(18788,"thp.10"),
	SANGGAH(18789,"thp.11"),
	SANGGAH_PRA(18791,"thp.12"),
	PEMASUKAN_DAN_EVALUASI_DOK_PRA(18792, "thp.13"),
	PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS(18795,"thp.14"), // 18794,18795
	PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA(18796,"thp.15"),
	PEMBUKAAN_PENAWARAN_BIAYA(18797,"thp.16"),
	PEMBUKAAN_PENAWARAN(18798,"thp.17"), //18798,18834
	VERIFIKASI_KUALIFIKASI(18802,"thp.18"),
	TANDATANGAN_KONTRAK(18803,"thp.19"),
	PENETAPAN_PEMENANG_AKHIR(18804,"thp.20"),
	PENETAPAN_PEMENANG_ADM_TEKNIS(18805,"thp.21"),
	PENGUMUMAN_LELANG(18807,"thp.22"),
    UMUM_PRAKUALIFIKASI(18808,"thp.23"),
    PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK(18809,"thp.24"),
    PENGUMUMAN_PEMENANG_ADM_TEKNIS(18793,"thp.25"),
    PENGUMUMAN_PEMENANG_AKHIR(18810,"thp.26"),
    PENGUMUMAN_HASIL_PRA(18811,"thp.27"),
    PENJELASAN(18812,"thp.28"), //18812 , 18813
    PENJELASAN_PRA(18832,"thp.29"),
    PENYETARAAN_TEKNIS(18814,"thp.30"),
    PENUNJUKAN_PEMENANG(18817,"thp.31"),
    UPLOAD_BA_EVALUASI_PENAWARAN(18819,"thp.32"),
	UPLOAD_BA_EVALUASI_HARGA(18820,"thp.33"),
	UPLOAD_BA_EVALUASI_ADM_TEKNIS(18821,"thp.34"),
	UPLOAD_BA_HASIL_LELANG(18822,"thp.35"),
	PEMASUKAN_PENAWARAN_ADM_TEKNIS(18824,"thp.37"),
	PEMASUKAN_PENAWARAN_BIAYA(18826,"thp.38"),
	PEMASUKAN_DOK_PRA(18827,"thp.39"),
	PENETAPAN_PEMENANG_PENAWARAN(18828,"thp.40"),
	PENETAPAN_HASIL_PRA(18830,"thp.41"),
	PEMASUKAN_PENAWARAN(18833,"thp.42"), //18823,18825,18833
	UPLOAD_BA_PENJELASAN(18835,"thp.43"),
	UPLOAD_BA_PENJELASAN_PRA(18846,"thp.44"),
//	UPLOAD_BA_PEMBUKAAN_PENAWARAN(18836,"Upload Berita Acara Pembukaan Penawaran"),
//	UPLOAD_BA_PEMBUKAAN_PENAWARAN_ADM(18837,"Upload Berita Acara Pembukaan Penawaran Administrasi dan Teknis"),
//	UPLOAD_BA_PEMBUKAAN_PENAWARAN_HARGA(18838,"Upload Berita Acara Pembukaan Penawaran Harga"),
	UPLOAD_BA_TAMBAHAN(18839,"thp.45"),
	KLARIFIKASI_NEGOSIASI(18840,"thp.46"),
	TANDATANGAN_SPK(18841,"thp.47"),
	TANDATANGAN_SMPK(18842,"thp.48"),
	EVALUASI_PENAWARAN_KUALIFIKASI(18843, "thp.49"),
	PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI(18844, "thp.50"),
	PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR(18845, "thp.56"),
	// tahap dibawah ini tidak di pakai di Aktifitas, hanya dipakai oleh aplikasi
	BELUM_DILAKSANAKAN(0,"thp.51"),
	TIDAK_ADA_JADWAL(0, "thp.52"),
	SUDAH_SELESAI(0, "thp.53"),
	PAKET_SUDAH_SELESAI(0, "thp.54"), //untuk PL
	PAKET_BELUM_DILAKSANAKAN(0,"thp.55");

    public final Integer id;
    public final String label;
    
    private Tahap(int id, String nama) {
		this.id = Integer.valueOf(id);
		this.label = nama;
    }

    public String getLabel()
	{
		return Messages.get(label);
	}
    /**
     * definisikan cara akses halaman tahapan terkait secara popup/inline
     * default inline 
     * @return
     */
    public boolean isPopup() {
    	return (this == PENGUMUMAN_LELANG) || (this == AMBIL_DOK_PRA) || (this == AMBIL_DOKUMEN) || (this == AMBIL_DOKUMEN_PEMILIHAN);
    }
    
    public static final List<Tahap> tahapanEvaluasi = Arrays.asList(
				EVALUASI_DOK_PRA,PENETAPAN_HASIL_PRA, EVALUASI_PENAWARAN,EVALUASI_PENAWARAN_ADM_TEKNIS,
				EVALUASI_PENAWARAN_BIAYA,PENETAPAN_PEMENANG_PENAWARAN,PENETAPAN_PEMENANG_AKHIR, PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR, PEMASUKAN_PENAWARAN_ADM_TEKNIS, PEMASUKAN_PENAWARAN_BIAYA,
				PENETAPAN_HASIL_PRA,PENETAPAN_PEMENANG_ADM_TEKNIS,KOREKSI_ARITMATIK, PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS, PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA, KLARIFIKASI_NEGOSIASI,
				KLARIFIKASI_TEKNIS_BIAYA, KLARIFIKASI_KEWAJARAN_HARGA, VERIFIKASI_KUALIFIKASI, EVALUASI_PENAWARAN_KUALIFIKASI, PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI);
    
    public static final List<Tahap> tahapanPembukaanEvaluasiPenawaran = Arrays.asList(PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS, PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI, PEMBUKAAN_PENAWARAN);
    
    public static final List<Tahap> tahapanEvaluasiTeknis = Arrays.asList(
			EVALUASI_PENAWARAN,EVALUASI_PENAWARAN_ADM_TEKNIS,EVALUASI_PENAWARAN_KUALIFIKASI,PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI,
			PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS,PENETAPAN_PEMENANG_ADM_TEKNIS);

    public static final List<Tahap> tahapanEvaluasiHarga = Arrays.asList(
			EVALUASI_PENAWARAN,EVALUASI_PENAWARAN_BIAYA,EVALUASI_PENAWARAN_KUALIFIKASI,PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA
	);
    
    public static final List<Tahap> tahapanPengumumanHasil = Arrays.asList(PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK,Tahap.PENGUMUMAN_HASIL_PRA,
    		Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS,Tahap.PENGUMUMAN_PEMENANG_AKHIR, Tahap.PENETAPAN_DAN_PENGUMUMAN_PEMENANG_AKHIR);
    
    public static final List<Tahap> tahapanEvaluasiPra = Arrays.asList(Tahap.EVALUASI_DOK_PRA, Tahap.PEMASUKAN_DAN_EVALUASI_DOK_PRA, Tahap.PEMASUKAN_DOK_PRA);
    
    public static final List<Tahap> tahapanAmbilDok = Arrays.asList(Tahap.AMBIL_DOKUMEN, Tahap.AMBIL_DOKUMEN_PEMILIHAN, Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK);
    
    public static final List<Tahap> tahapanBA = Arrays.asList(Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS, Tahap.UPLOAD_BA_EVALUASI_HARGA, Tahap.UPLOAD_BA_EVALUASI_PENAWARAN, Tahap.UPLOAD_BA_HASIL_LELANG);
    
    public static final List<Tahap> tahapanPembukaan = Arrays.asList(Tahap.PEMBUKAAN_PENAWARAN, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA, Tahap.PEMBUKAAN_PENAWARAN_BIAYA, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI);

    public static final List<Tahap> tahapPembukaanTeknis = Arrays.asList(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS, Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI);

	public static final List<Tahap> tahapanPengumumanHasilPl = Arrays.asList(KLARIFIKASI_NEGOSIASI, TANDATANGAN_SMPK, TANDATANGAN_SPK);
    
	public static final List<Tahap> tahapanEvaluasiDokPra = Arrays.asList(EVALUASI_DOK_PRA, VERIFIKASI_KUALIFIKASI, PENETAPAN_HASIL_PRA, PENGUMUMAN_HASIL_PRA, SANGGAH_PRA);
	
    public static String tahapInfo(String tahaps, boolean shortVersion, boolean lelangV3, boolean express) {
    	if(!CommonUtil.isEmpty(tahaps)) {
			String[] tahap = tahaps.split(",");
			Tahap obj = null;
			if(tahap.length == 1) {
				obj = Tahap.valueOf(tahap[0]);
				if(express && obj == BELUM_DILAKSANAKAN)
					obj = Tahap.TIDAK_ADA_JADWAL;
				return obj.label;
			}
			else {
				StringBuilder result = new StringBuilder();	
				boolean first = true;				
				for(String value:tahap) {
					obj = Tahap.valueOf(value);
					if(!first)
					{
						if(shortVersion)
						{
							result.append(" [...]");
							break;
						}
						else
							result.append("<br>");
					}
					if(lelangV3 || !Tahap.tahapanBA.contains(obj))
						result.append(obj.label);
					else // penyesuaian nama tahapan BA
						result.append(obj.label.replace("Upload", "Pembuatan"));
					first = false;
				}
				return result.toString();
			}
		}			
		return "";
    }
}
