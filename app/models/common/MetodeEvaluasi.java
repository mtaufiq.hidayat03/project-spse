package models.common;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum MetodeEvaluasi {
	GUGUR (0, "Harga Terendah Sistem Gugur"),	NILAI (1, "Sistem Nilai"),	UMUR_EKONOMIS (2, "Sistem Umur Ekonomis"),
	KUALITAS (3, "Kualitas"), KUALITAS_BIAYA (4, "Kualitas dan Biaya"),	ANGGARAN (5, "Pagu Anggaran"),
	BIAYA_RENDAH (6,"Biaya Terendah"), PENUNJUKAN_LANGSUNG (7,"Penunjukan Langsung"),
	HARGA_TERENDAH_AMBANG_BATAS (8, "Harga Terendah Ambang Batas");
	
	public final Integer id;
	public final String label;
	
	MetodeEvaluasi(int key, String label){
		this.id = Integer.valueOf(key);
		this.label = label;
	}

	public static MetodeEvaluasi[] findListBy(Kategori kategori, Metode metode) {
		if(kategori.isKonsultansiPerorangan())
			return new MetodeEvaluasi[]{KUALITAS};
		else if(kategori.isKonsultansi() || kategori.isJkKonstruksi())
			return new MetodeEvaluasi[]{KUALITAS,KUALITAS_BIAYA,ANGGARAN,BIAYA_RENDAH};
		else if(metode.kualifikasi.isPasca() && metode.dokumen.isSatuFile())
			return new MetodeEvaluasi[]{GUGUR};
		else if(metode.kualifikasi.isPasca() && metode.dokumen.isDuaFile()) {
			if(kategori.isKonstruksi())
				return new MetodeEvaluasi[]{NILAI, HARGA_TERENDAH_AMBANG_BATAS};
			else
				return new MetodeEvaluasi[]{NILAI, UMUR_EKONOMIS, HARGA_TERENDAH_AMBANG_BATAS};
		}
		else {
			if(kategori.isKonstruksi())
				return new MetodeEvaluasi[]{NILAI};
			else
				return new MetodeEvaluasi[]{NILAI, UMUR_EKONOMIS};
		}
	}

	public static MetodeEvaluasi findById(Integer value){
		MetodeEvaluasi evaluasi = null;
		if(value != null){
			switch(value.intValue()){
			case 0: evaluasi = GUGUR;break;
			case 1: evaluasi = NILAI;break;	
			case 2: evaluasi = UMUR_EKONOMIS;break;
			case 3: evaluasi = KUALITAS;break;
			case 4: evaluasi = KUALITAS_BIAYA;break;
			case 5: evaluasi = ANGGARAN;break;
			case 6: evaluasi = BIAYA_RENDAH;break;
			case 7: evaluasi = PENUNJUKAN_LANGSUNG;break;
			case 8: evaluasi = HARGA_TERENDAH_AMBANG_BATAS;break;
			default: evaluasi = GUGUR;break;
			}
		}
		return evaluasi;
	}
		
	public boolean isGugur(){
		return this == GUGUR;
	}
	
	public boolean isNilai(){
		return this == KUALITAS_BIAYA || this == NILAI || this == UMUR_EKONOMIS;
	}
	
	public boolean isUmurEkonomis() {
		return this == UMUR_EKONOMIS;
	}
	
	public boolean isKualitas(){
		return this == KUALITAS;
	}
	public boolean isAnggaran(){
		return this == ANGGARAN;
	}
	public boolean isKualitasBiaya(){
		return this == KUALITAS_BIAYA;
	}
	
	public boolean isPaguAnggaran() {
		return this == ANGGARAN;
	}
	
	public boolean isBiayaTerendah() {
		return this == BIAYA_RENDAH;
	}

	public boolean isHargaTerendahAmbangBatas() {
		return this == HARGA_TERENDAH_AMBANG_BATAS;
	}
}
