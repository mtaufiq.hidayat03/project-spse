package models.common;


import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="HYPERLINK")
public class Hyperlink extends BaseModel {
	

	@Id(sequence="seq_hyperlink", function="nextsequence")
	public Long lnk_id;

	public String lnk_jenis;

	public String lnk_judul;

	public String lnk_url;

	public String lnk_keterangan;

}
