package models.common;

/**
 * Enum untuk category 'CONFIG' di model Configuration.
 * 
 * @author arief ardiyansah
 *
 */
public enum CONFIG {

	/**Tidak semua entry di tabel CONFIGURATION digunakan
	 * 
	 */
	PPE_ID("ppe.id"),
	PPE_NAMA("ppe.nama"),
	PPE_VERSI("ppe.versi"),
	APENDO_VERSION("apendo.version"),
	APENDO_URL_SERVER_1("apendo.server1"),
	APENDO_URL_SERVER_2("apendo.server2"),
	ADD_AANWIZING_TIME("aanwizing.add-hour"),
	IS_LINTAS_SATKER("paket.lintas-satker"),
	EPNS_SITE("epns.site"),
	EPNS_VERSION("epns.version"),
	ADD_ADENDUM_TIME("adendum.add-day"),
	DISABLE_DOWNLOAD_PENAWARAN("disable-download"),
	REKANAN_MENDAFTAR_EXPIRE("rekanan.mendaftar.expires"),
	REKANAN_MEDAFTAR_REMINDER("rekanan.mendaftar.reminder"),
	REKANAN_NEED_APPROVAL("rekanan-needs-approval"),
	// FTP Setting, di SPSE 35 ada tapi tidak dipakai tapi perlu di definisikan di ENUM takutnya kedepan dipake
	FTP_HOST("ftp.host"),
	FTP_USER("ftp.user"),
	FTP_PASSWORD("ftp.password"),
	FTP_RETRY("ftp.retry"),
	FTP_UPLOAD_DIR("ftp.upload.dir"),
	FTP_ENABLE_MIRRORING("ftp.enable_mirroring"),
	FTP_DOWNLOAD_URL("ftp.download_url"),
	FTP_DELAY("ftp.delay"),
	
	/*
	 */
	//FILE_STORAGE_DIR("file.storage-dir"),
	//FILE_STORAGE_DIR_ALT("file-storage-dir"),

//	SMTP_HOST("mail.smtp.host"), 
//	SMTP_SENDER("mail.sender"),  
//	SMTP_RETRY("mail.retry"),    
//	SMTP_DELAY("mail.delay"),    
//	SMTP_PORT("mail.port"),		 
//	SMTP_SENDER_PASWORD("mail.smtp.password"),
//	SMTP_BCC("mail.recipient.bcc"), 
//	SMTP_MAIL_TEST("mail.test"), 
	
	MAX_UPLOAD_SIZE("maxSizeFileUpload"),
	SYSTEM_MODUL("system.modul"),
	
	EPNS_INBOUND("epns.central-repository"),
	ENABLE_INAPROC("enable-inaproc"),
	CAPTCHA("captcha"),	
	RUP_URL("rup-url"),
	NON_EPROC_ENABLED("non-eproc.enabled"),
	ITEMIZED_ENABLED("itemize.enabled"),
	PUBLIC_CONTENT_URL("epns.public_content_url"),
	PENGUMUMAN_MASSAL_URL("epns.pengumuman_massal_url"),
	ENABLE_KIRIM_FAQ("faq-kirim.enabled"),
	ENABLED_ITEMIZED("itemized.enabled"),
	DATAMASTER_PROVINSI("datamaster.provinsi-url"), // diambil dari dce
	DATAMASTER_KABUPATEN("datamaster.kabupaten-url"), // diambil dari dce
	DATAMASTER_INSTANSI("datamaster.instansi-url"), // diambil dari dce
	DATAMASTER_SATKER("datamaster.satker-url"),// akan dipake untuk versi 4
	// OSD
	AMANDA_VERSION("amanda-version"),
	AMANDA_START_DATE("amanda-start-date"),
	KABUPATEN("kabupaten"),

    SESI_PELATIHAN("sesi-pelatihan"),
	//flag bahwa migrasi telah dilakukan
	MIGRATION_3_TO_4_DONE("migration.3to4.done"),
	// flag enable info multi session user
	MULTI_SESSION_USER_INFO("multi.session.user.info");
	
	public String category;
	
	private CONFIG(String category) {
		this.category = category;
	}
	
	/**
	 * definisi key ketika akan disimpan di play.Cache
	 * @return
	 */
	public String toCache()
	{
		return "CONFIG."+category;
	}
	
	public String toString() {
		return this.category;
	}
	
}

