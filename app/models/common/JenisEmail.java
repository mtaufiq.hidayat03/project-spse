package models.common;

/**
 * Jenis email yang mungkin dikirim oleh sistem.
 * 
 * @author idoej
 *
 */
public enum JenisEmail {

	REGISTRASI_PENYEDIA(1, "Registrasi Penyedia"),
	PENGIRIMAN_PENAWARAN (2, "Pengiriman Penawaran"),
	PENGUMUMAN_PRA(3, "Pengumuman Prakualifikasi"),
	DOK_KUALIFIKASI(4, "Pesan Dokumen Kualifikasi Tambahan"),
	PEMBATALAN_PENGAKTIFAN_LELANG(5, "Pembatalan Pengaktifan Tender"),
	PENGUMUMAN_PEMENANG(6, "Pengumuman Pemenang"),
	UNDANGAN_PL(7, "Undangan Non Tender"),
	UNDANGAN_PEMBUKTIAN_KUALIFIKASI(8, "Undangan Pembuktian Kualifikasi"),
	UNDANGAN_VERIFIKASI(9, "Undangan Verifikasi"),
	UNDANGAN_KONTRAK(10, "Undangan Kontrak"),
	PENGUMUMAN_ADENDUM(11, "Pengumuman Adendum"),
	NOTIFIKASI_SANGGAHAN(12, "Notifikasi Sanggahan"),
	UNDANGAN_AUCTION(13, "Undangan Reverse Auction"),
	UNDANGAN_ADMINISTRASI(14, "Klarifikasi Administrasi"),
	UNDANGAN_TEKNIS(15, "Klarifikasi Teknis"),
	UNDANGAN_HARGA(16, "Klarifikasi Harga"),
	UNDANGAN_KUALIFIKASI(17, "Klarifikasi Kualifikasi"),
	UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS_HARGA(18, "Klarifikasi Administrasi, Kualifikasi, Teknis, dan Harga"),
	UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS(19, "Klarifikasi Administrasi, Kualifikasi, dan Teknis"),
	UNDANGAN_ADMIN_TEKNIS(20, "Klarifikasi Administrasi dan Teknis"),
	UNDANGAN_TEKNISREVISI_HARGA(21, "Klarifikasi Teknis (Revisi) dan Harga"),
	PENGUMUMAN_PEMENANG_PRA(22,"Pengumuman Pemenang Prakualifikasi"),
	NOTIFIKASI_SANGGAHAN_BANDING(23, "Notifikasi Sanggahan Banding"),;

	public static JenisEmail getById(long id) {
		for (JenisEmail es : JenisEmail.values()) {
			if (es.id == id) {
				return es;
			}
		}
		throw new IllegalArgumentException();
	}

	public final int id;
	public final String label;
	private JenisEmail(int id, String label) {
		this.id=id;
		this.label = label;
	}

	public static enum JenisPesan {

		PEMBERITAHUAN(1),
		UNDANGAN(2);

		public final int id;

		private JenisPesan(int id) {
			this.id = id;
		}

		public boolean isPemberitahuan(int id) {
			return id == PEMBERITAHUAN.id;
		}

	}

}
