package models.common;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;


/**
 * Model untuk Metode Pelelangan
 * Semua metode lelang harus di jelaskan di model ini
 * @author Arief Ardiyansah
 *
 */
@Enumerated(EnumType.ORDINAL)
public enum Metode {

	PASCA_SATU_FILE_80(1, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),
	PASCA_DUA_FILE_80(19, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE),	
	PRA_DUA_FILE_80(2, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
	PRA_DUA_FILE_80_KONSULTANSI(4, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
	PRA_DUA_TAHAP_80(3, MetodeKualifikasi.PRA, MetodeDokumen.DUA_TAHAP),
	// Perpres 54 dan 70
	PASCA_SATU_FILE_54(41, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),
	PASCA_DUA_FILE_54(51, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE),
	PRA_DUA_FILE(42, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
	PRA_DUA_FILE_KUALITAS(46, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
	PRA_DUA_FILE_KUALITAS_BIAYA(47, MetodeKualifikasi.PRA, MetodeDokumen.DUA_FILE),
	PRA_DUA_TAHAP(44, MetodeKualifikasi.PRA, MetodeDokumen.DUA_TAHAP),
	PRA_SATU_FILE_KONSULTANSI(48, MetodeKualifikasi.PRA, MetodeDokumen.SATU_FILE), // metode ini sudah tidak ada di perpres 16/2018
	PRA_SATU_FILE(50, MetodeKualifikasi.PRA, MetodeDokumen.SATU_FILE),// metode ini sudah tidak ada di perpres 16/2018
	PASCA_SATU_FILE_KUALITAS(52, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE),// metode ini sudah tidak ada di perpres 16/2018
	PASCA_DUA_FILE_KUALITAS(53, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE), // perlem lkpp no 9/2018
	PASCA_SATU_FILE(54, MetodeKualifikasi.PASCA, MetodeDokumen.SATU_FILE), // perlem lkpp no 9/2018
	PASCA_DUA_FILE(55, MetodeKualifikasi.PASCA, MetodeDokumen.DUA_FILE); // perlem lkpp no 9/2018

	public final Integer id;
	public final MetodeKualifikasi kualifikasi;
	public final MetodeDokumen dokumen;
	public final String label;
	
	Metode(int id, MetodeKualifikasi kualifikasi, MetodeDokumen dokumen) {
		this.id = Integer.valueOf(id);
		this.kualifikasi = kualifikasi;
		this.dokumen = dokumen;
		this.label = kualifikasi.label+ ' ' +dokumen.label;
	}

	public static Metode findById(Integer value) {
		Metode metode = null;
		if (value != null) {
			switch (value.intValue()) {
				case 1:
					metode = PASCA_SATU_FILE_80;
					break;
				case 2:
					metode = PRA_DUA_FILE_80;
					break;
				case 3:
					metode = PRA_DUA_TAHAP_80;
					break;
				case 4:
					metode = PRA_DUA_FILE_80_KONSULTANSI;
					break;
				case 41:
					metode = PASCA_SATU_FILE_54;
					break;
				case 42:
					metode = PRA_DUA_FILE;
					break;
				case 44:
					metode = PRA_DUA_TAHAP;
					break;
				case 46:
					metode = PRA_DUA_FILE_KUALITAS;
					break;
				case 47:
					metode = PRA_DUA_FILE_KUALITAS_BIAYA;
					break;
				case 48:
					metode = PRA_SATU_FILE_KONSULTANSI;
					break;
				case 50:
					metode = PRA_SATU_FILE;
					break;
				case 51:
					metode = PASCA_DUA_FILE_54;
					break;
				case 52:
					metode = PASCA_SATU_FILE_KUALITAS;
					break;
				case 53:
					metode = PASCA_DUA_FILE_KUALITAS;
					break;
				case 54:
					metode = PASCA_SATU_FILE;
					break;
				case 55:
					metode = PASCA_DUA_FILE;
					break;
				default:
					metode = PASCA_SATU_FILE;
					break;
			}
		}
		return metode;
	}

	public static Metode findById(Kategori kategori, MetodeKualifikasi kualifikasi, MetodeDokumen dokumen, MetodeEvaluasi evaluasi){
		if(kualifikasi != null && kualifikasi.isPasca()) {
			if(dokumen.isSatuFile())
				return kategori.isConsultant() || kategori.isJkKonstruksi() ? PASCA_SATU_FILE_KUALITAS : PASCA_SATU_FILE;
			else if(dokumen.isDuaFile())
				return kategori.isConsultant() || kategori.isJkKonstruksi() ? PASCA_DUA_FILE_KUALITAS : PASCA_DUA_FILE;
		}else if(kualifikasi.isPra()) {
			if(dokumen.isSatuFile())
				return kategori.isConsultant() || kategori.isJkKonstruksi() ? PRA_SATU_FILE_KONSULTANSI:PRA_SATU_FILE;
			else if(dokumen.isDuaFile()) {
				if(kategori.isConsultant() || kategori.isJkKonstruksi())
					return evaluasi.isKualitas() ? PRA_DUA_FILE_KUALITAS : PRA_DUA_FILE_KUALITAS_BIAYA;
				return PRA_DUA_FILE;
			}
			else if(dokumen.isDuaTahap())
				return PRA_DUA_TAHAP;

		}
		return PASCA_SATU_FILE; // default metode
	}

	//1,19,41,51,52,54,55
	public static final Integer[] METODE_PASCA_ID = new Integer[] {
		PASCA_SATU_FILE_80.id, PASCA_DUA_FILE_80.id, PASCA_SATU_FILE_54.id, PASCA_DUA_FILE_54.id,
		PASCA_SATU_FILE_KUALITAS.id, PASCA_DUA_FILE_KUALITAS.id, PASCA_SATU_FILE.id, PASCA_DUA_FILE.id
	} ;

	//2,3,4,42,44,46,47,48,50
	public static final Integer[] METODE_PRA_ID = new Integer[] {
		PRA_DUA_FILE_80.id, PRA_DUA_TAHAP_80.id, PRA_DUA_FILE_80_KONSULTANSI.id, PRA_DUA_FILE.id, PRA_DUA_FILE_KUALITAS.id,
		PRA_DUA_FILE_KUALITAS_BIAYA.id, PRA_DUA_TAHAP.id, PRA_SATU_FILE_KONSULTANSI.id, PRA_SATU_FILE.id
	};
}
