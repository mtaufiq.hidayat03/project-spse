package models.common;

import models.jcommon.config.Configuration;
import play.cache.Cache;
import play.db.jdbc.Query;

/**Counter disimpan di CONFIGURATION dengan key COUNTER.VISITOR_COUNTER_SPSE_4 
 * 
 * @author Mr. Andik
 *
 */
public class Counter  {	

	public static final String KEY="VISITOR_COUNTER_SPSE_4";
	/**
	 * Fungsi {@code update} digunakan untuk update nilai counter pengunjung
	 * aplikasi
	 *
	 */
	public static void update() {
		Configuration conf=Configuration.find("cfg_category=? and cfg_sub_category=?", "COUNTER", KEY).first();
		if(conf==null)
		{
			conf=new Configuration();
			conf.cfg_category="COUNTER";
			conf.cfg_sub_category=KEY;
		}
		conf.cfg_value=String.valueOf(getValue()+ 1L);
		conf.save();
	}
	
	public static Long getValue()
	{
		Configuration conf=Configuration.find("cfg_category=? and cfg_sub_category=?", "COUNTER", KEY).first();
		if(conf==null)
			return 0L;
		else
		{
			return Long.parseLong(conf.cfg_value);
		}
	}

    public static Long getTotalCounter() {
		Long counter = (Long) Cache.get("total_counter");
		if (counter == null) {
			// Harusnya bertipe Integer sesuai dengan tipe di model Counter,
			// jika di DB cntr_value bertipe string , jalankan script update "spse-3.5-migrasi ke 4.sql"
			//dapatkan counter dari spse versi 4
			Long spse4counter = Query.find("select cfg_value from CONFIGURATION where cfg_category='COUNTER' and cfg_sub_category=?", Long.class, Counter.KEY).first();
			if (spse4counter == null)
				spse4counter = 0L;
			long countercount = Query.count("SELECT SUM(cntr_value) FROM Counter");
			counter =  countercount + spse4counter;
			Cache.safeSet("total_counter", counter, "10mn"); // set 5 minute cache
		}
		return counter;
    }
}