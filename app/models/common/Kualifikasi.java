package models.common;

import play.i18n.Messages;

public enum Kualifikasi {

	KECIL("21", Messages.get("kualifikasi.perusahaan_kecil")),
	NON_KECIL("22", Messages.get("kualifikasi.perusahaan_non_kecil")),
	KECIL_NON_KECIL("23", Messages.get("kualifikasi.perusahaan_kecil_non_kecil")),
	MENENGAH("24", Messages.get("kualifikasi.perusahaan_menengah")),
	BESAR("25", Messages.get("kualifikasi.perusahaan_besar"));
	
	public String id;
	public String label;
	private Kualifikasi(String id, String label) {
		this.id = id;
		this.label = label;
	}
	
	public static Kualifikasi findById(String id)
	{
		if(id.equals("21"))
			return KECIL;
		else if(id.equals("22"))
			return NON_KECIL;
		else if(id.equals("23"))
			return KECIL_NON_KECIL;
		else if(id.equals("24"))
			return MENENGAH;
		else if(id.equals("25"))
			return BESAR;
		return KECIL;
	}

	public boolean isKecil() {
		return this == KECIL;
	}

	public boolean isNonKecil() {
		return this == NON_KECIL;
	}

	public boolean isMenengah() {
		return this == MENENGAH;
	}

	public boolean isBesar() {
		return this == BESAR;
	}
}
