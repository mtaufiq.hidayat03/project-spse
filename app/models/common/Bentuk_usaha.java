package models.common;

import org.apache.commons.lang3.StringUtils;

public enum Bentuk_usaha {

	CV("01","CV"),
	PT("02","PT"),
	UD("03","UD"),
	KOPERASI("04","Koperasi"),
	FIRMA("05","Firma"),
	PERUSAHAAN_PERSEORANGAN("06","Perusahaan Perseorangan"),
	KONSULTAN_PERORANGAN("07","Konsultan Perorangan"),
	PERUSAHAAN_DAGANG("08","Perusahaan Dagang"),
	PERORANGAN("09","Perorangan"),
	PERUSAHAAN_ASING("10","Perusahaan Asing/Foreign Vendor"),
	LEMBAGA_PENYIARAN_PUBLIK("11","Lembaga Penyiaran Publik");

	public final String id;

	public final String label;

	Bentuk_usaha(String id, String label){
		this.id = id;
		this.label = label;
	}

	public boolean iskonsultanIndividu()
	{
		if(id == null)
			return false;
		return this == KONSULTAN_PERORANGAN;
	}

	public boolean isPerorangan()
	{
		if(id == null)
			return false;
		return this == PERORANGAN;
	}

	public static String findNamaBU(String id) {
		if(StringUtils.isEmpty(id))
			return "";
		Bentuk_usaha bu = null;
		switch (id) {
			case "01": bu = CV; break;
			case "02": bu = PT; break;
			case "03": bu = UD; break;
			case "04": bu = KOPERASI; break;
			case "05": bu = FIRMA; break;
			case "06": bu = PERUSAHAAN_PERSEORANGAN; break;
			case "07": bu = KONSULTAN_PERORANGAN; break;
			case "08": bu = PERUSAHAAN_DAGANG; break;
			case "09": bu = PERORANGAN; break;
			case "10": bu = PERUSAHAAN_ASING; break;
			case "11": bu = LEMBAGA_PENYIARAN_PUBLIK; break;
		}
		return bu.label;
	}

	public static Bentuk_usaha findById(String id) {
		if(StringUtils.isEmpty(id))
			return CV;
		Bentuk_usaha bu = null;
		switch (id) {
			case "01": bu = CV; break;
			case "02": bu = PT; break;
			case "03": bu = UD; break;
			case "04": bu = KOPERASI; break;
			case "05": bu = FIRMA; break;
			case "06": bu = PERUSAHAAN_PERSEORANGAN; break;
			case "07": bu = KONSULTAN_PERORANGAN; break;
			case "08": bu = PERUSAHAAN_DAGANG; break;
			case "09": bu = PERORANGAN; break;
			case "10": bu = PERUSAHAAN_ASING; break;
			case "11": bu = LEMBAGA_PENYIARAN_PUBLIK; break;
		}
		return bu;
	}

}