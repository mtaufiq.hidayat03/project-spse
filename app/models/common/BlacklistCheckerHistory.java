package models.common;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**
 * Created by Lambang on 11/2/2016.
 */
@Table(name="BLACKLIST_CHECKER_HISTORY")
public class BlacklistCheckerHistory extends BaseModel{

    @Id(sequence="seq_blacklist_checker_history", function="nextsequence")
    public Long bch_id;

    public Long lls_id;

    public Long peg_id;

    public Long rkn_id;

    public String bch_url;

    public Integer bch_type; //0=rekanan daftar lelang, 1=panitia penetapan pemenang, 2=ppk penetapan pemenag berkntrak

    public Integer bch_status; //0=false, 1=true, 2=exception

    public static void saveCheckHistory(String serviceUrl, Long pegId, Long lelangId, Integer type, Integer status, Long rknId){
        BlacklistCheckerHistory history = new BlacklistCheckerHistory();
        history.bch_status = status;
        history.bch_type = type;
        history.lls_id = lelangId;
        history.rkn_id = rknId;
        if (pegId != null)
            history.peg_id = pegId;
        history.bch_url = serviceUrl;
        history.save();
    }
}
