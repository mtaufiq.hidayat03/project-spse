package models.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum StatusPl {

    DRAFT(0,"pkt.drf"),
    AKTIF(1,"pkt.psb"),
    DITUTUP(2,"pkt.pdt"),
    SELESAI(3,"pkt.sls"),
    DITOLAK(4,"pkt.dtlk"),
    DRAFT_UKPBJ(5,"pkt.drff"),
    DRAFT_POKJA(6, "plt.drf_kj");

    public final Integer id;
    public final String label;

    StatusPl(int key, String label){
        this.id = Integer.valueOf(key);
        this.label = label;
    }

    public static StatusPl fromValue(Integer value){
        StatusPl status = null;
        if(value != null) {
            switch (value.intValue()) {
                case 0:	status = StatusPl.DRAFT;break;
                case 1:	status = StatusPl.AKTIF;break;
                case 2:	status = StatusPl.DITUTUP;break;
                case 3:	status = StatusPl.SELESAI;break;
                case 4:	status = StatusPl.DITOLAK;break;
                default: status = StatusPl.DRAFT;break;
            }
        }
        return status;
    }

    /**
     * cek status lelang Draft atau bukan
     * @return
     * @author arief
     * date : oktober 2009
     */
    public boolean isDraft(){	return this == DRAFT;		}

    /**
     * cek status lelang Ditutup atau bukan
     * @return
     * @author arief
     * date : oktober 2009
     */
    public boolean isDitutup(){ 	return this == DITUTUP; 	}

    /**
     * cek status lelang Ditolak atau tidak
     * @return
     */
    public boolean isDitolak(){ 	return this == DITOLAK;	}

    /**
     * cek status lelang Aktif atau tidak
     * @return
     * @author arief
     * date : oktober 2009
     */
    public boolean isAktif(){	return this == AKTIF;	}

    /**
     * cek status lelang selesai atau belum
     * method ini belum bisa digunakan karena belum ada action trigger dari user
     * untuk mengubah status lelang menjadi selesai
     * @return
     * @author arief
     * date : oktober 2009
     */
    public boolean isSelesai(){ 	return this == SELESAI;		}

    public String getLabel()
    {
        return Messages.get(label);
    }
}
