package models.common;

import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.sql2o.ResultSetHandler;
import controllers.BasicCtr;
import ext.FormatUtils;
import ext.StringFormat;
import models.agency.*;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.lelang.*;
import models.lelang.Nilai_evaluasi.StatusNilaiEvaluasi;
import models.nonlelang.*;
import models.rekanan.Rekanan;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.mvc.Router;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;
import utils.LogUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Implementasi Dao untuk model MailQueue
 * 
 * @author arief 
 *
 */
public class MailQueueDao {

	private static final Template PENGUMUMAN_PEMENANG_PRA_TEMPLATE = TemplateLoader.load("/email/undangan-lelang.html");
	private static final Template PENGUMUMAN_PEMENANG_TEMPLATE = TemplateLoader.load("/email/pengumuman-pemenang.html");
	private static final Template PENGUMUMAN_TUTUP_LELANG_TEMPLATE = TemplateLoader.load("/email/pengumuman-tutup-lelang.html");
	private static final Template PENGUMUMAN_BUKA_LELANG_TEMPLATE = TemplateLoader.load("/email/pengumuman-buka-lelang.html");
	private static final Template PENGUMUMAN_BUKA_PL_TEMPLATE = TemplateLoader.load("/email/pengumuman-buka-pl.html");
	private static final Template NOTIFIKASI_PENDAFTARAN_TEMPLATE = TemplateLoader.load("/email/notifikasi-pendaftaran.html");
	private static final Template NOTIFIKASI_SETUJU_PENDAFTARAN = TemplateLoader.load("/email/notifikasi-setuju-pendaftaran.html");
	private static final Template PEMBERITAHUAN_KUALIFIKASI = TemplateLoader.load("/email/pemberitahuan-kualifikasi.html");
	private static final Template UNDANGAN_PEMBUKTIAN = TemplateLoader.load("/email/undangan-pembuktian.html");
	private static final Template PEMBERITAHUAN_PEMBUKTIAN = TemplateLoader.load("/email/pemberitahuan/pembuktian-kualifikasi.html");
	private static final Template UNDANGAN_ADMINISTRASI = TemplateLoader.load("/email/undangan-administrasi.html");
	private static final Template UNDANGAN_TEKNIS = TemplateLoader.load("/email/undangan-teknis.html");
	private static final Template UNDANGAN_HARGA = TemplateLoader.load("/email/undangan-harga.html");
	private static final Template UNDANGAN_PEMBUKTIAN_PL = TemplateLoader.load("/email/undangan-pembuktian-pl.html");
	private static final Template PEMBERITAHUAN_PEMBUKTIAN_PL = TemplateLoader.load("/email/pemberitahuan/pembuktian-kualifikasi-pl.html");
	private static final Template UNDANGAN_VERIFIKASI = TemplateLoader.load("/email/undangan-verifikasi.html");
	private static final Template UNDANGAN_VERIFIKASI_PL = TemplateLoader.load("/email/undangan-verifikasi-pl.html");
	private static final Template PENGUMUMAN_PEMENANG_PRA_PL_TEMPLATE = TemplateLoader.load("/email/undangan-lelang-pl.html");
	private static final Template UNDANGAN_KONTRAK = TemplateLoader.load("/email/undangan-kontrak.html");
	private static final Template UNDANGAN_KONTRAK_PL = TemplateLoader.load("/email/undangan-kontrak-pl.html");
	private static final Template NOTIFIKASI_GANTI_PASSWORD = TemplateLoader.load("/email/notifikasi-ganti-password.html");
	private static final Template UNDANGAN_PL = TemplateLoader.load("/email/undangan-pl.html");
	private static final Template PENGUMUMAN_PEMENANG_PL_TEMPLATE = TemplateLoader.load("/email/pengumuman-pemenang-pl.html");
	private static final Template PENGUMUMAN_SPPBJ = TemplateLoader.load("/email/pengumuman-pemenang-kontrak.html");
	private static final Template PENGUMUMAN_SPPBJ_PL = TemplateLoader.load("/email/pengumuman-pemenang-kontrak-pl.html");
	private static final Template PENGUMUMAN_TUTUP_PL_TEMPLATE = TemplateLoader.load("/email/pengumuman-tutup-lelang.html");
	private static final Template PENGUMUMAN_ADENDUM = TemplateLoader.load("/email/pengumuman-adendum.html");
	private static final Template PENGUMUMAN_ADENDUM_RINCIAN_HPS = TemplateLoader.load("/email/notifikasi-rincian-hps-berubah.html");
	private static final Template NOTIFIKASI_SANGGAHAN = TemplateLoader.load("/email/notifikasi-sanggahan.html");
	private static final Template NOTIFIKASI_SANGGAH_BANDING = TemplateLoader.load("/email/undangan-sanggah-banding.html");
	private static final Template UNDANGAN_AUCTION = TemplateLoader.load("/email/undangan-reverse-auction.html");
	private static final Template PESAN_KLARIFIKASI = TemplateLoader.load("/email/pesan-klarifikasi.html");
	private static final Template PERUBAHAN_JADWAL_TENDER = TemplateLoader.load("/email/perubahan-jadwal-lelang.html");
	private static final Pattern metaPattern = Pattern.compile("(<meta (.*))");

	public static long getJumlahInbox(Long rekananId, MailQueue.BACA_STATUS bcstat) {
		/*Method ini dipanggil di setiap halaman rekanan. Oleh Karena itu perlu di-cache selama
		 * 15 detik
		 */
		String key="models.common.dao.MailQueueDao.getJumlahInbox_"+rekananId+"_"+bcstat;
		Long value=Cache.get(key, Long.class);
		if(value==null)
		{
			if(bcstat == null)
				value= MailQueue.count("rkn_id =?", rekananId);
			else
				value=MailQueue.count("rkn_id=? and bcstat=?", rekananId, bcstat);
			Cache.add(key, value, "15s");
		}
		return value;
	}
	
	public static int countPesanKualifikasi(Long lelangId, Long rkn_id)
	{
		return (int)MailQueue.count("lls_id=? and rkn_id=? and jenis = 4", lelangId, rkn_id);
	}
	
	public static List<MailQueue> findPesanKualifikasi(Long lelangId, Long rkn_id)
	{
		return MailQueue.find("lls_id=? and rkn_id=? and jenis = 4 order by id DESC", lelangId, rkn_id).fetch();
	}

	public static List<MailQueue> findPesanVerifikasi(Long lelangId, Long rkn_id)
	{
		return MailQueue.find("lls_id=? and rkn_id=? and jenis in (8,9) order by id DESC", lelangId, rkn_id).fetch();
	}

	public static List<MailQueue> findUndanganEvaluasi(Long lelangId, Long rkn_id, int jenisUndangan)
	{
		return MailQueue.find("lls_id=? and rkn_id=? and jenis = ? order by id DESC", lelangId, rkn_id, jenisUndangan).fetch();
	}

	public static List<MailQueue> findUndanganKontrak(Long lelangId, Long rkn_id, int jenisUndangan)
	{
		return MailQueue.find("lls_id=? and rkn_id=? and jenis = ? order by id DESC", lelangId, rkn_id, jenisUndangan).fetch();
	}

	public static int countByJenis(Long lelangId, int jenis){
		return Query.count("SELECT count(distinct rkn_id) FROM mail_queue WHERE lls_id=? AND jenis =?", Integer.class, lelangId, Integer.valueOf(jenis));
	}

	public static MailQueue getByJenisAndLelangAndRekanan(Long lelangId, Long rekananId, JenisEmail jenis){
		return MailQueue.find("lls_id=? and jenis = ? and rkn_id = ? ORDER BY enqueue_date DESC", lelangId, jenis.id, rekananId).first();
	}
	
	public static int countByJenisAndRekanan(Long lelangId, int jenis, long rkn_id){
		return (int)MailQueue.count("lls_id=? and jenis=? and rkn_id=?", lelangId, Integer.valueOf(jenis), rkn_id);
	}
	
	/**
	 * mengirim pengumuman hasil evaluasi prakualifikasi
	 * @return
	 */
	public static void kirimPengumumanPrakualifikasi(Lelang_seleksi lelang) throws Exception {
		List<UndangaPeserta> pesertaLelang = UndangaPeserta.findAll(lelang.lls_id, Tahap.PENGUMUMAN_HASIL_PRA, lelang.isLelangV3());
		if(CollectionUtils.isEmpty(pesertaLelang))
			return ;
		String paket = lelang.getNamaPaket();
		Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_PENAWARAN);
		if(lelang.isDuaTahap()) {
			jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS);
		}
		String tanggal_awal = FormatUtils.formatDateInd(jadwal.dtj_tglawal);
		String tanggal_akhir = FormatUtils.formatDateInd(jadwal.dtj_tglakhir);
		// kirim email sesuai hasil evaluasi kualifikasi & pembuktian
		int versi  = Evaluasi.findCurrentVersi(lelang.lls_id);
		Map<Long, StatusNilaiEvaluasi> hasilEvaluasi = new HashMap<Long, StatusNilaiEvaluasi>();
		Map<Long, String> alasanEvaluasi = new HashMap<Long, String>();
		String sql = "SELECT r.rkn_id, k.nev_lulus AS kualifikasi, k.nev_uraian AS als_kualifikasi, q.nev_lulus as pembuktian, q.nev_uraian as als_pembuktian " +
                "FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id " +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 5 AND eva_versi=:versi) q ON p.psr_id=q.psr_id " +
                "LEFT JOIN (SELECT * FROM nilai_evaluasi n, evaluasi e WHERE n.eva_id=e.eva_id AND e.lls_id=:lls_id AND eva_jenis = 0 AND eva_versi=:versi) k ON p.psr_id=k.psr_id " +
                "WHERE p.lls_id=:lls_id ORDER BY psr_harga ASC";
		List<HasilEvaluasi> evaluasiList = Query.find(sql, HasilEvaluasi.class).setParameter("lls_id", lelang.lls_id).setParameter("versi", versi).fetch();
		for(HasilEvaluasi hasil : evaluasiList){
			StatusNilaiEvaluasi status = hasil.isLulusKualifikasi() && hasil.isLulusPembuktian() ? StatusNilaiEvaluasi.LULUS : StatusNilaiEvaluasi.TDK_LULUS;
			String alasan="";
			if(!StringUtils.isEmpty(hasil.als_kualifikasi))
				alasan += hasil.als_kualifikasi;
			if(!StringUtils.isEmpty(hasil.als_pembuktian))
				alasan += hasil.als_pembuktian;
			hasilEvaluasi.put(hasil.rkn_id, status);
			alasanEvaluasi.put(hasil.rkn_id, alasan);
		}

		String uraian = null;
		StatusNilaiEvaluasi status = null;
		int i =0;
		for (UndangaPeserta undangan:pesertaLelang) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("rkn_nama", undangan.rkn_nama.toUpperCase());
			map.put("nama_panitia", lelang.getPanitia().pnt_nama);
			map.put("email", undangan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			map.put("nama_paket", paket);
			map.put("header", "Pengumuman Hasil Evaluasi Prakualifikasi");
			if(hasilEvaluasi.containsKey(undangan.rkn_id)) {
				uraian = alasanEvaluasi.get(undangan.rkn_id);
				status = hasilEvaluasi.get(undangan.rkn_id);
				map.put("lulus", status.label.toUpperCase());				
				if(!CommonUtil.isEmpty(uraian))
					map.put("alasan", "Dengan alasan "+uraian);
				if(status.isLulus()) {
					map.put("jadwal", "Anda dapat mengirim dokumen penawaran mulai tanggal " + tanggal_awal + " sampai dengan tanggal " + tanggal_akhir);
				}
			}
			else {
				map.put("lulus", "Tidak Lulus");
				map.put("alasan", "Dengan alasan Tidak mengirimkan dokumen kualifikasi");
			}
			MailQueue mq=EmailManager.createEmail(undangan.rkn_email, PENGUMUMAN_PEMENANG_PRA_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = undangan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Hasil Evaluasi Prakualifikasi";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = undangan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_PRA.id;
			mq.save();
		}
	}
	
	public static void kirimPengumumanPrakualifikasiPl(Pl_seleksi pl) throws Exception {
		String paket = pl.getNamaPaket();
		EvaluasiPl evaluasi = EvaluasiPl.findPembuktian(pl.lls_id);
		String queryPemenang = "SELECT r.rkn_nama FROM ekontrak.nilai_evaluasi n LEFT JOIN ekontrak.peserta_nonlelang p ON n.psr_id=p.psr_id LEFT JOIN rekanan r ON r.rkn_id=p.rkn_id "
				+ "WHERE n.eva_id=? AND n.nev_lulus = 1";
		String namaPemenang = Query.find(queryPemenang, String.class, evaluasi.eva_id).first();
		List<Rekanan> pesertaLelang = Query.find("SELECT r.rkn_id, rkn_nama, rkn_npwp, rkn_email FROM ekontrak.peserta_nonlelang p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id WHERE lls_id=?", Rekanan.class, pl.lls_id).fetch();
		if(CollectionUtils.isEmpty(pesertaLelang))
			return;
		MailQueue[] mails = new MailQueue[pesertaLelang.size()];
		int i =0;
		for (Rekanan rekanan : pesertaLelang) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			if(Active_user.current().isPP())
				map.put("nama_pp", pl.getPp().getPegawai().peg_nama);
			else
				map.put("nama_pp", Active_user.current().name);
			map.put("pesertaLelang", pesertaLelang);
			map.put("namaPemenang", namaPemenang);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", pl.lls_id);
			if(evaluasi != null) {
				map.put("lulus", "LULUS");
			}
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_PEMENANG_PRA_PL_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Hasil Evaluasi Prakualifikasi";
			mq.lls_id=pl.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_PEMENANG_PRA.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}
	
	/**
	 * method untuk mengirim pengumuman Pemenang lelang
	 * @return
	 */
	public static void kirimPengumumanPemenang(Lelang_seleksi lelang) throws Exception {
		List<UndangaPeserta> pesertaLelang = UndangaPeserta.findAll(lelang.lls_id, Tahap.PENGUMUMAN_PEMENANG_AKHIR, lelang.isLelangV3());
		if(CollectionUtils.isEmpty(pesertaLelang))
			return ;
		String paket = lelang.getNamaPaket();
		Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(lelang.lls_id);
		String queryPemenang = "SELECT r.rkn_id, rkn_nama, rkn_npwp, rkn_email FROM nilai_evaluasi n LEFT JOIN peserta p ON n.psr_id=p.psr_id LEFT JOIN rekanan r ON r.rkn_id=p.rkn_id "
				+ "WHERE n.eva_id=?" +(lelang.getPemilihan().isLelangExpress() ? " and p.is_pemenang_verif = 1" :" AND n.nev_lulus = 1");
		List<Rekanan> pemenang = Query.find(queryPemenang, Rekanan.class, evaluasi.eva_id).fetch();
		for (UndangaPeserta rekanan : pesertaLelang) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			map.put("nama_panitia", lelang.getPanitia().pnt_nama);
			map.put("pesertaLelang", pesertaLelang);
			map.put("pemenang", pemenang);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			map.put("preheaderText", "(LPSE) Pengumuman Pemenang Tender");
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_PEMENANG_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama; 
			mq.subject = "(LPSE) Pengumuman Pemenang Tender";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_PEMENANG.id;
			mq.save();
		}
	}

	public static void kirimPengumumanPemenangPl(Pl_seleksi pl) throws Exception {
		String paket = pl.getNamaPaket();
		EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
		String queryPemenang = "SELECT r.rkn_nama FROM ekontrak.nilai_evaluasi n LEFT JOIN ekontrak.peserta_nonlelang p ON n.psr_id=p.psr_id LEFT JOIN rekanan r ON r.rkn_id=p.rkn_id "
				+ "WHERE n.eva_id=? AND n.nev_lulus = 1";
		String namaPemenang = Query.find(queryPemenang, String.class, evaluasi.eva_id).first();
		List<Rekanan> pesertaLelang = Query.find("SELECT r.rkn_id, rkn_nama, rkn_npwp, rkn_email FROM ekontrak.peserta_nonlelang p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id WHERE lls_id=?", Rekanan.class, pl.lls_id).fetch();
		if(CollectionUtils.isEmpty(pesertaLelang))
			return;
		MailQueue[] mails = new MailQueue[pesertaLelang.size()];
		int i =0;
		for (Rekanan rekanan : pesertaLelang) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			if(Active_user.current().isPP())
				map.put("nama_pp", pl.getPp().getPegawai().peg_nama);
			else
				map.put("nama_pp", Active_user.current().name);
			map.put("pesertaLelang", pesertaLelang);
			map.put("namaPemenang", namaPemenang);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", pl.lls_id);
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_PEMENANG_PL_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Pemenang Paket";
			mq.lls_id=pl.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_PEMENANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}
	
	/**
	 * method untuk mengirim pengumuman Pemenang lelang
	 */
	public static void kirimPengumumanPemenangKontrak(Lelang_seleksi lelang, Sppbj sppbj, String nama_ppk) throws Exception {
		String paket = lelang.getNamaPaket();
		Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(lelang.lls_id);
		// data penawaran terendah
		String queryCalonPemenang = "SELECT r.rkn_id FROM nilai_evaluasi n LEFT JOIN peserta p ON n.psr_id=p.psr_id LEFT JOIN rekanan r ON r.rkn_id=p.rkn_id "
				+ "WHERE n.eva_id=?"+(lelang.getPemilihan().isLelangExpress() ? " and p.is_pemenang = 1" :" AND n.nev_lulus = 1");
		Long idCalonPemenang = Query.find(queryCalonPemenang, Long.class, evaluasi.eva_id).first();
		// data pemenang
		String namaPemenang = Query.find("SELECT r.rkn_nama FROM rekanan r WHERE r.rkn_id=?", String.class, sppbj.rkn_id).first();
		List<Rekanan> pesertaLelang = Query.find("SELECT r.rkn_id, rkn_nama, rkn_npwp, rkn_email FROM peserta p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id WHERE lls_id=?", Rekanan.class, lelang.lls_id).fetch();
		if(CollectionUtils.isEmpty(pesertaLelang))
			return;
		MailQueue[] mails = new MailQueue[pesertaLelang.size()];
		int i =0;
		for (Rekanan rekanan : pesertaLelang) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			map.put("nama_ppk", nama_ppk);
			map.put("pesertaLelang", pesertaLelang);
			map.put("namaPemenang", namaPemenang);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			map.put("alasan", idCalonPemenang.equals(sppbj.rkn_id) || sppbj.catatan_ppk == null ? "": sppbj.catatan_ppk);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_SPPBJ, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama; 
			mq.subject = "(LPSE) Pengumuman Pemenang Berkontrak Tender";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_PEMENANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	public static void kirimPengumumanPemenangKontrakPl(Pl_seleksi lelang, SppbjPl sppbj) throws Exception {
		String paket = lelang.getNamaPaket();
		EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(lelang.lls_id);
		// data penawaran terendah
		String queryCalonPemenang = "SELECT r.rkn_id FROM ekontrak.nilai_evaluasi n LEFT JOIN ekontrak.peserta_nonlelang p ON n.psr_id=p.psr_id LEFT JOIN rekanan r ON r.rkn_id=p.rkn_id "
				+ "WHERE n.eva_id=? AND n.nev_lulus = 1";
		Long idCalonPemenang = Query.find(queryCalonPemenang, Long.class, evaluasi.eva_id).first();
		String namaPemenang = Query.find("SELECT r.rkn_nama FROM rekanan r WHERE r.rkn_id=?", String.class, sppbj.rkn_id).first();
		List<Rekanan> pesertaLelang = Query.find("SELECT r.rkn_id, rkn_nama, rkn_npwp, rkn_email FROM ekontrak.peserta_nonlelang p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id WHERE lls_id=?", Rekanan.class, lelang.lls_id).fetch();
		if(CollectionUtils.isEmpty(pesertaLelang))
			return;
		MailQueue[] mails = new MailQueue[pesertaLelang.size()];
		int i =0;
		for (Rekanan rekanan : pesertaLelang) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			map.put("nama_pp", lelang.getPp().getPegawai().peg_nama);
			map.put("pesertaLelang", pesertaLelang);
			map.put("namaPemenang", namaPemenang);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			map.put("alasan", idCalonPemenang.equals(sppbj.rkn_id) || sppbj.catatan_ppk == null ? "": sppbj.catatan_ppk);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_SPPBJ_PL, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Pemenang Berkontrak PL";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_PEMENANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}
	
	/**
	 * mengirim pengumuman penutupan lelang
	 */
	public static void kirimPengumumanTutupLelang(Lelang_seleksi lelang) throws Exception {
		String namaPaket = lelang.getNamaPaket();
		List<Peserta> pesertaList = lelang.getPesertaList();
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (Peserta peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("alasan", lelang.getAlasanDitutup());
			map.put("nama_panitia", lelang.getPanitia().pnt_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);	
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_TUTUP_LELANG_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama; 
			mq.subject = "(LPSE) Pengumuman Pembatalan Tender";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PEMBATALAN_PENGAKTIFAN_LELANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	public static void kirimPengumumanTutupPl(Pl_seleksi pl, String alasan) throws Exception {
		String namaPaket = pl.getNamaPaket();
		List<PesertaPl> pesertaList = pl.getPesertaList();
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (PesertaPl peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("alasan", alasan);
			Pegawai pegawai = Pegawai.findById(Active_user.current().getPegawaiId());
			map.put("nama_pp", pegawai.peg_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", pl.lls_id);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_TUTUP_PL_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Pembatalan PL";
			mq.lls_id=pl.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PEMBATALAN_PENGAKTIFAN_LELANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}
	
	/**
	 * mengirim pengumuman penutupan lelang
	 */
	public static void kirimPengumumanBukaLelang(Lelang_seleksi lelang, String alasan) throws Exception {
		String namaPaket = lelang.getNamaPaket();
		List<Peserta> pesertaList = lelang.getPesertaList();
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (Peserta peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("alasan", alasan);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);	
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_BUKA_LELANG_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama; 
			mq.subject = "(LPSE) Pengumuman Pengaktifan Kembali Tender";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PEMBATALAN_PENGAKTIFAN_LELANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	/**
	 * mengirim pengumuman penutupan lelang
	 */
	public static void kirimPengumumanBukaPl(Pl_seleksi pl, String alasan) throws Exception {
		String namaPaket = pl.getNamaPaket();
		List<PesertaPl> pesertaList = pl.getPesertaList();
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (PesertaPl peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("alasan", alasan);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", pl.lls_id);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_BUKA_PL_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Pengaktifan Kembali Tender";
			mq.lls_id=pl.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PEMBATALAN_PENGAKTIFAN_LELANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	public static final ResultSetHandler<String[]> resultsetMail = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			tmp[0] = rs.getString("id");
			tmp[1] = rs.getString("id");
			tmp[2] = rs.getString("to_addresses");
			tmp[3] = rs.getString("subject");
			tmp[4] = FormatUtils.formatDateInd(rs.getTimestamp("enqueue_date"));
			tmp[5] = rs.getString("retry");
			return tmp;
		}
	};
	
	/**
	 * kirim email saat penawaran peserta berhasil diterima oleh server LPSE
	 * @param pesertaId
	 * @throws Exception 
	 
	public static void kirimStatusPenawaran(Peserta peserta, Dok_penawaran dok_penawaran) throws Exception {
		Lelang_seleksi lelang = peserta.getLelang_seleksi();
		boolean send_email = true;
		if(lelang.isSatuFile()) { // jika sudah kirim admin teknis dan Penawaran. maka informasi penawaran teknis saja yang tampil
			
		} 	
		if(send_email) {
			BlobTable blob = dok_penawaran.getDokumen();
			Rekanan rekanan = peserta.getRekanan();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("tanggal_jam_terima",FormatUtils.formatDateTimeSecond(dok_penawaran.dok_tgljam) + " (Waktu Server LPSE)");
			map.put("namaFile", blob.blb_nama_file);
			map.put("ukuranFile", FormatUtils.formatFileSize(blob.blb_ukuran) + " / " + FormatUtils.formatNumber(blob.blb_ukuran)+" bytes");
			map.put("nama_paket",lelang.getNamaPaket());
			map.put("lls_id",peserta.lls_id);
			map.put("email", rekanan.rkn_email);
			map.put("hash",dok_penawaran.dok_hash); 
			map.put("tgl_disclaimer", FormatUtils.formatDateTimeSecond(BasicCtr.newDate()) + " (Waktu Server LPSE)");
			MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, "notifikasi-rekanan-upload-dok-penawaran-sukses.vm", map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama; 
			mq.subject = "(LPSE) Unggah (Upload) Dokumen Penawaran";
			mq.lls_id=peserta.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = Integer.valueOf(2);
			mq.save();
		}
	}*/
	
	/**
	 * kirim notifikasi konfirmasi pendaftaran
	 * @param email
	 * @param userid
	 * @param namaPerusahaan
	 * @param alamat
	 * @throws Exception
	 */
	public static void kirimKonfirmasiPendaftaran(Long rekananId, String email, String userid, String namaPerusahaan, String alamat, String referer) throws Exception {
		Logger.debug("email : %s", email);
		Map vars=new HashMap<String, String>(); 
		vars.put("email", email);
		vars.put("username",userid);
		vars.put("rekananNama", namaPerusahaan);
		vars.put("rekananAlamat", alamat);
		vars.put("referer", referer);
		MailQueue mq=EmailManager.createEmail("\"Pendaftar LPSE\" <" + email + '>',NOTIFIKASI_PENDAFTARAN_TEMPLATE , vars);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) [USERID] Selamat Bergabung di LPSE Nasional";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=Long.parseLong("0");
		mq.rkn_id = rekananId;
		mq.jenis = JenisEmail.REGISTRASI_PENYEDIA.id;
		mq.save();
	}
	

	public static void KonfirmasiPersetujuan(Long id, String email, String username, String nama, String alamat) throws Exception {		
		Map vars = new HashMap<String, String>();
		vars.put("email", email);
		vars.put("username", username);
		vars.put("rekananNama", nama);
		vars.put("rekananAlamat", alamat);
		vars.put("nama_lpse", ConfigurationDao.getNamaLpse());
		vars.put("subject", "Konfirmasi Pendaftaran LPSE");
		MailQueue mq = EmailManager.createEmail("\"Pendaftar LPSE\" <" + email + '>', NOTIFIKASI_SETUJU_PENDAFTARAN, vars);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; // Email ini mendapat prioritas utama
		mq.ref_a = "PUBLIK"; // tambahan informasi tentang siapa yang dituju
		mq.lls_id = Long.parseLong("0");
		mq.rkn_id = id;
		mq.save();
	}
	
	/**
	 * Kirim Notifikasi Pemberitahuan Kualifikasi Kepada Peserta lelang
	 * @param rekanan
	 * @param isi
	 * @param lelangId
	 * @param namaLelang
	 * @param namaPanitia
	 * @throws Exception
	 */
	public static void kirimPesanKualifikasi(Rekanan rekanan, String isi, Long lelangId, String namaLelang, String namaPanitia) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("isi", isi);
		map.put("namaLelang", namaLelang);
		map.put("namaPanitia", namaPanitia);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email,PEMBERITAHUAN_KUALIFIKASI , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) [KUALIFIKASI] Dokumen Kualifikasi";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.DOK_KUALIFIKASI.id;
		mq.save();
	}


	/**
	 * Kirim Notifikasi Pemberitahuan Kualifikasi Kepada Peserta lelang
	 * @param rekanan
	 * @param isi
	 * @param plId
	 * @param namaPl
	 * @param namaPanitia
	 * @throws Exception
	 */
	public static void kirimPesanKualifikasiPl(Rekanan rekanan, String isi, Long plId, String namaPl, String namaPanitia) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", plId);
		map.put("isi", isi);
		map.put("namaLelang", namaPl);
		map.put("namaPanitia", namaPanitia);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email,PEMBERITAHUAN_KUALIFIKASI , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) [KUALIFIKASI] Dokumen Kualifikasi";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=plId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.DOK_KUALIFIKASI.id;
		mq.save();
	}


	/**
	 * Kirim Notifikasi Pemberitahuan Pembuktian Kualifikasi Kepada Peserta lelang
	 * @param rekanan
	 * @param lelangId
	 * @param namaLelang
	 * @param namaPanitia
	 * @param waktu
	 * @param tempat
	 * @param dibawa
	 * @param hadir
	 * @throws Exception
	 */
	public static void kirimUndanganEvaluasi(Rekanan rekanan, Long lelangId, String namaLelang, String namaPanitia,
											 Date waktu, Date sampai, String tempat, String dibawa, String hadir,
											 int jenisUndangan, Integer tipePesan) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("sampai", StringFormat.formatDateTime(sampai));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPanitia", namaPanitia);
		Template template = null;
		String judul = "";
		if(jenisUndangan == JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id) {
			template = UNDANGAN_PEMBUKTIAN;
			judul = "(LPSE) [KUALIFIKASI] Undangan Pembuktian Kualifikasi";
			if (tipePesan == JenisEmail.JenisPesan.PEMBERITAHUAN.id) {
				template = PEMBERITAHUAN_PEMBUKTIAN;
				judul = "(LPSE) [KUALIFIKASI] Pemberitahuan Pembuktian Kualifikasi";
			}
		} else if(jenisUndangan == JenisEmail.UNDANGAN_ADMINISTRASI.id) {
			template = UNDANGAN_ADMINISTRASI;
			judul = "(LPSE) [KUALIFIKASI] Undangan Klarifikasi Administrasi";
		} else if(jenisUndangan == JenisEmail.UNDANGAN_TEKNIS.id) {
			template = UNDANGAN_TEKNIS;
			judul = "(LPSE) [KUALIFIKASI] Undangan Klarifikasi Teknis";
		} else if(jenisUndangan == JenisEmail.UNDANGAN_HARGA.id) {
			template = UNDANGAN_HARGA;
			judul = "(LPSE) [KUALIFIKASI] Undangan Klarifikasi Harga";
		}
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, template , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = judul;
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = jenisUndangan;
		mq.save();
	}

	public static void kirimPesanKlarifikasi(Rekanan rekanan, Long lelangId, String namaLelang, String namaPanitia,
											 Date waktu, Date sampai, String tempat, String dibawa, String hadir,
											 int jenisUndangan, Integer tipePesan, String subject) throws Exception {

		Map<String, Object> map = new HashMap<>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("sampai", StringFormat.formatDateTime(sampai));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPanitia", namaPanitia);
		map.put("subject", subject);
		map.put("type", tipePesan);
		LogUtil.debug("MailQueue", map);
		String message;
		String must;
		if (tipePesan == JenisEmail.JenisPesan.PEMBERITAHUAN.id) {
			message = "Kami akan melakukan " + subject + " terhadap Tender:";
			must = "disiapkan";
		} else {
			message = "Kami mengundang Anda untuk menghadiri " + subject +  " terhadap Tender:";
			must = "dibawa";
		}
		map.put("message", message);
		map.put("must", must);
		MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, PESAN_KLARIFIKASI , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) [KUALIFIKASI] "
				+ ((int) map.get("type") == JenisEmail.JenisPesan.PEMBERITAHUAN.id
				? "Pemberitahuan "
				: "Undangan ")
				+ map.get("subject");
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id = lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = jenisUndangan;
		mq.enqueue_date = BasicCtr.newDate();
		mq.save();
	}

	public static void kirimUndanganPembuktianPl(Rekanan rekanan, Long lelangId, String namaLelang, String namaPp,
												 Date waktu, String tempat, String dibawa, String hadir, Integer tipePesan) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPp", namaPp);
		Template template = UNDANGAN_PEMBUKTIAN_PL;
		String subject = "(LPSE) [KUALIFIKASI] Undangan Pembuktian Kualifikasi PL";
		if (tipePesan == JenisEmail.JenisPesan.PEMBERITAHUAN.id) {
			subject = "(LPSE) [KUALIFIKASI] Pemberitahuan Pembuktian Kualifikasi PL";
			template = PEMBERITAHUAN_PEMBUKTIAN_PL;
		}
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, template, map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = subject;
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id;
		mq.save();
	}
	
	/**
	 * Kirim Notifikasi Pemberitahuan Verifkasi Kepada Peserta lelang
	 * @param rekanan
	 * @param lelangId
	 * @param namaLelang
	 * @param namaPanitia
	 * @throws Exception
	 */
	public static void kirimUndanganVerifikasi(Rekanan rekanan, Long lelangId, String namaLelang, String namaPanitia,
			Date waktu, String tempat, String dibawa, String hadir) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPanitia", namaPanitia);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email,UNDANGAN_VERIFIKASI , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) [KUALIFIKASI] Undangan Pembuktian Kualifikasi";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.UNDANGAN_VERIFIKASI.id;
		mq.save();
	}

	public static void kirimUndanganVerifikasiPl(Rekanan rekanan, Long lelangId, String namaLelang, String namaPp,
											   Date waktu, Date sampai, String tempat, String dibawa, String hadir) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("sampai", StringFormat.formatDateTime(sampai));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPp", namaPp);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email,UNDANGAN_VERIFIKASI_PL , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) [KUALIFIKASI] Undangan Pembuktian Kualifikasi";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id;
		mq.save();
	}
	
	/**
	 * Kirim Notifikasi Pemberitahuan Kontrak Kepada Peserta lelang
	 * @param rekanan
	 * @param lelangId
	 * @param namaLelang
	 * @param namaPanitia
	 * @throws Exception
	 */
	public static void kirimUndanganKontrak(Rekanan rekanan, Long lelangId, String namaLelang, String namaPanitia,
			Date waktu, Date sampai, String tempat, String dibawa, String hadir) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodeLelang", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("sampai", StringFormat.formatDateTime(sampai));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPanitia", namaPanitia);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email,UNDANGAN_KONTRAK , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) Undangan Kontrak";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.UNDANGAN_KONTRAK.id;
		mq.save();
	}

	public static void kirimUndanganKontrakPl(Rekanan rekanan, Long lelangId, String namaLelang, String namaPp,
											Date waktu, Date sampai, String tempat, String dibawa, String hadir) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rekananNama", rekanan.rkn_nama);
		map.put("kodePaket", lelangId);
		map.put("waktu", StringFormat.formatDateTime(waktu));
		map.put("sampai", StringFormat.formatDateTime(sampai));
		map.put("tempat", tempat);
		map.put("dibawa", dibawa);
		map.put("hadir", hadir);
		map.put("namaLelang", namaLelang);
		map.put("namaPp", namaPp);
		map.put("tanggalBuat", StringFormat.formatDateTime(DateUtil.newDate()));
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email,UNDANGAN_KONTRAK_PL , map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "(LPSE) Undangan Kontrak";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=lelangId;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmail.UNDANGAN_KONTRAK.id;
		mq.save();
	}

	///kirim email pemberitahuan
	public static void kirimNotifikasiGantiPassword(String userid, String email, Date date, Long rekananId) throws Exception{
		Map<String, Object> vars=new HashMap<String, Object>();
		vars.put("nama_lpse", ConfigurationDao.getNamaLpse());
		vars.put("tanggal",  StringFormat.formatDateTime(date));
		vars.put("userId", userid);
		vars.put("subject", "Penggantian Password Berhasil Dilakukan");
		MailQueue mq= EmailManager.createEmail(email, NOTIFIKASI_GANTI_PASSWORD, vars);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.subject = "Notifikasi Ganti Password";
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id = 0L;
		mq.rkn_id = rekananId;
		mq.save();
	}

	public static void kirimUndanganPl(Pl_seleksi lelang, String referer) throws Exception {
		Logger.info("*Sedang Kirim Undangan PL");
		List<DraftPesertaPl> listDraftPesertaPl = DraftPesertaPl.findWithLelang(lelang.lls_id);
		if(CollectionUtils.isEmpty(listDraftPesertaPl))
			return;
		MailQueue[] mails = new MailQueue[listDraftPesertaPl.size()];
		int i =0;
		for (DraftPesertaPl draftPesertaPl : listDraftPesertaPl) {
			Rekanan rekanan = draftPesertaPl.getRekanan();
			Pl_detil pl = Pl_detil.findById(lelang.lls_id);
			Map<String, Object> map = new HashMap<>();
			map.put("nama_paket", lelang.getNamaPaket());
			map.put("instansi", pl.nama_instansi);
			map.put("hps", FormatUtils.formatCurrencyRupiah(lelang.getPaket().pkt_hps));
			map.put("lls_id", lelang.lls_id);
			Map<String, Object> param =  new HashMap<>();
			param.put("id", lelang.lls_id);
			map.put("url", referer+Router.reverse("nonlelang.PengadaanLCtr.pengumumanPl", param).url);
			map.put("rekananNama", rekanan.rkn_nama);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, UNDANGAN_PL, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = lelang.getPemilihan().isPengadaanLangsung() ? "(LPSE) Undangan Pengadaan Langsung"
					: "(LPSE) Undangan Penunjukan Langsung";
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.UNDANGAN_PL.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	public static boolean checkEmail(Long lls_id, Long rkn_id, JenisEmail jenisEmail){
		MailQueue mq = MailQueue.find("lls_id=? AND rkn_id=? AND jenis=? ", lls_id, rkn_id, jenisEmail.id).first();
		return mq != null;
	}

	/**
	 * mengirim notifikasi adendum dokumen pengadaan / dokumen kualifikasi
	 */
	public static void kirimNotifikasiAdendum(Lelang_seleksi lelang, List<Peserta> pesertaList) throws Exception {
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		String namaPaket = lelang.getNamaPaket();
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (Peserta peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_ADENDUM, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Adendum";
			mq.lls_id = lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_ADENDUM.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}
	public static void kirimNotifikasiAdendumPl(Pl_seleksi pl, List<PesertaPl> pesertaList) throws Exception {
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		String namaPaket = pl.getNamaPaket();
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (PesertaPl peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", pl.lls_id);
			MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_ADENDUM, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Adendum";
			mq.lls_id = pl.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_ADENDUM.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	/**
	 * send non lelang hps change email*/
	public static void kirimNotifikasiAdendumNonLelangHps(Pl_seleksi lelang, List<PesertaPl> participants) throws Exception {
		if(CollectionUtils.isEmpty(participants))
			return;
		MailQueue[] mails = new MailQueue[participants.size()];
		int i =0;
		for(PesertaPl peserta : participants) {
			Rekanan rekanan = Rekanan.findByPesertaPl(peserta.psr_id);
			String namaPaket = lelang.getNamaPaket();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_ADENDUM_RINCIAN_HPS, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Pengumuman Adendum Rincian HPS";
			mq.lls_id = lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.PENGUMUMAN_ADENDUM.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	/**
	 * Kirim notifikasi kepada seluruh penawar ketika ada sanggahan
	 * @param lelang
	 * @throws Exception
	 */
	public static void kirimNotifikasiSanggahan(Lelang_seleksi lelang, List<Nilai_evaluasi> pesertaList) throws Exception {
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (Nilai_evaluasi peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findByPeserta(peserta.psr_id);
			String namaPaket = lelang.getNamaPaket();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, NOTIFIKASI_SANGGAHAN, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Notifikasi Sanggahan Lelang";
			mq.lls_id = lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.NOTIFIKASI_SANGGAHAN.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	/**
	 * Kirim Undangan Pemberitahuan Sanggah Banding kepada Peserta
	 * @throws Exception
	 */
	public static void kirimUndanganSanggahBanding(Long lelangId, String pkt_nama, String pnt_nama, Date tanggal_mulai, Date tanggal_selesai, List<Peserta> pesertaList) throws Exception {
		Logger.info("Sedang Kirim Undangan Sanggah Banding");
		MailQueue mq = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pkt_nama", pkt_nama);
		map.put("pnt_nama", pnt_nama);
		map.put("lelangId", lelangId);
		map.put("tanggal_mulai", FormatUtils.formatDateTimeInd(tanggal_mulai));
		map.put("tanggal_selesai", FormatUtils.formatDateTimeInd(tanggal_selesai));
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (Peserta peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			map.put("rkn_nama", rekanan.rkn_nama);
			mq=EmailManager.createEmail(rekanan.rkn_email, NOTIFIKASI_SANGGAH_BANDING, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Undangan Pemberitahuan Pelaksanaan Sanggah Banding";
			mq.lls_id=  lelangId;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.NOTIFIKASI_SANGGAHAN_BANDING.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	/**
	 * Kirim Undangan Pemberitahuan Reverse Auction kepada Peserta
	 * @throws Exception
	 */
	public static void kirimUndanganReverseAuction(Long lelangId, String pkt_nama, String pnt_nama, Date tanggal_mulai, Date tanggal_selesai, List<Peserta> pesertaList) throws Exception {
		Logger.info("Sedang Kirim Undangan Reverse Auction");
		MailQueue mq = null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pkt_nama", pkt_nama);
		map.put("pnt_nama", pnt_nama);
		map.put("lelangId", lelangId);
		map.put("tanggal_mulai", FormatUtils.formatDateTimeInd(tanggal_mulai));
		map.put("tanggal_selesai", FormatUtils.formatDateTimeInd(tanggal_selesai));
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (Peserta peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			map.put("rkn_nama", rekanan.rkn_nama);
			mq=EmailManager.createEmail(rekanan.rkn_email, UNDANGAN_AUCTION, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = "(LPSE) Undangan Pemberitahuan Pelaksanaan Reverse Auction";
			mq.lls_id=  lelangId;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmail.UNDANGAN_AUCTION.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}

	public static void kirimPerubahanJadwalLelang(Lelang_seleksi lelang, List<Jadwal> jadwalOriginal, List<Jadwal> jadwalList, Date waktuPerubahan) throws Exception{

		StringBuilder jadwalEmailx = new StringBuilder();
		Panitia panitia = lelang.getPanitia();

		jadwalEmailx.append("<table width=\"100%\" class=\"list-content\">" +
				"<tr>" +
				"<td><strong>Tahap</strong></td>" +
				"<td><strong>Sebelum</strong></td>" +
				"<td><strong>Sesudah</strong></td>" +
				"</tr>");
		Jadwal jadwalO = null;
		Jadwal jadwal = null;
		String keterangan = "";
		for (int i = 0; i < jadwalOriginal.size(); i++) {
			jadwalO = jadwalOriginal.get(i);
			jadwal = jadwalList.get(i);
			DateTime tglawalO = new DateTime(jadwalO.dtj_tglawal);
			DateTime tglawal = new DateTime(jadwal.dtj_tglawal);
			DateTime tglakhirO = new DateTime(jadwalO.dtj_tglakhir);
			DateTime tglakhir = new DateTime(jadwal.dtj_tglakhir);
			if (!Objects.equals(tglawal, tglawalO) || !Objects.equals(tglakhir, tglakhirO)) {
				jadwalEmailx.append("<tr><td>").append(jadwal.namaTahap(lelang.isLelangV3())).append("</td><td>");
				jadwalEmailx.append(FormatUtils.formatDateTimeInd(jadwalO.dtj_tglawal)).append(" - ").append(FormatUtils.formatDateTimeInd(jadwalO.dtj_tglakhir));
				jadwalEmailx.append("</td><td>");
				jadwalEmailx.append(FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal)).append(" - ").append(FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir));
				jadwalEmailx.append("</td></tr>");
				if(StringUtils.isEmpty(keterangan)) {
					keterangan = jadwal.dtj_keterangan;
				}
			}
		}
		jadwalEmailx.append("</table>");
		String namaPaket = lelang.getNamaPaket();
		List<Peserta> pesertaList = Peserta.find("lls_id=?", lelang.lls_id).fetch();
		List<Pegawai> pegawaiList = Pegawai.find("peg_id in (select peg_id from Anggota_panitia where pnt_id=?)", panitia.pnt_id).fetch();
		String instansi = Paket_satker.getNamaInstansiPaket(lelang.pkt_id);
		String email = null;
		MailQueue mails[] = new MailQueue[pesertaList.size() + pegawaiList.size()];
		int i =0;
		for (Peserta peserta : pesertaList)
		{
			email = Rekanan.findEmail(peserta.rkn_id);
			Map<String, Object> map = new HashMap<>();
			map.put("paket", namaPaket);
			map.put("nama_panitia", lelang.getNamaPanitia());
			map.put("email", email);
			map.put("lls_id", lelang.lls_id);
			map.put("jadwal", jadwalEmailx);
			map.put("waktuPerubahan",FormatUtils.formatDateTimeInd(waktuPerubahan));
			map.put("nama_lpse",ConfigurationDao.getNamaLpse());
			map.put("keterangan", keterangan);
			map.put("instansi", instansi);
			MailQueue mail = EmailManager.createEmail(email, PERUBAHAN_JADWAL_TENDER, map);
			mail.lls_id = lelang.lls_id;
			mail.rkn_id = peserta.rkn_id;
			mail.jenis = Integer.valueOf(7);
			mail.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mail.subject = "Notifikasi Perubahan Jadwal Tender";
			mail.save();
			mails[i] = mail;
			i++;
		}

		for (Pegawai pegawai : pegawaiList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("nama_panitia", panitia.pnt_nama);
			map.put("email", pegawai.peg_email);
			map.put("lls_id", lelang.lls_id);
			map.put("jadwal", jadwalEmailx);
			map.put("waktuPerubahan",FormatUtils.formatDateTimeInd(waktuPerubahan));
			MailQueue mail = EmailManager.createEmail(pegawai.peg_email, PERUBAHAN_JADWAL_TENDER, map);
			mail.lls_id = lelang.lls_id;
			mail.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mail.jenis = Integer.valueOf(7);
			mail.rkn_id = panitia.pnt_id;
			mail.subject = "Notifikasi Perubahan Jadwal Tender";
			mail.save();
			mails[i] = mail;
			i++;
		}
	}

	public static void kirimPerubahanJadwalLelangPl(Pl_seleksi lelang, List<Jadwal_pl> jadwalOriginal, List<Jadwal_pl> jadwalList, Date waktuPerubahan)
			throws Exception {

		StringBuilder jadwalEmailx = new StringBuilder();

		jadwalEmailx.append("<style type=text/css>table.jdwl{border-width: thin thin thin thin;border-spacing: 2px;" +
				"border-style: solid solid solid solid;border-color: black black black black;border-collapse: collapse;background-color: white;}");
		jadwalEmailx.append("table.jdwl td {border-width: 1px 1px 1px 1px;padding: 1px 1px 1px 1px;"
				+ "border-style: inset inset inset inset;border-color: black black black black;background-color: rgb(255, 245, 238);"
				+ "-moz-border-radius: 0px 0px 0px 0px;}</style>");

		jadwalEmailx.append("<table class=jdwl><tr align=center><td><strong>Tahap</strong></td>"
				+ "<td><strong>Sebelum</strong></td><td><strong>Sesudah</strong></td><td><strong>Alasan</strong></td></tr>");
		Jadwal_pl jadwalO = null;
		Jadwal_pl jadwal = null;
		for (int i = 0; i < jadwalOriginal.size(); i++) {
			jadwalO = jadwalOriginal.get(i);
			jadwal = jadwalList.get(i);
			DateTime tglawalO = new DateTime(jadwalO.dtj_tglawal);
			DateTime tglawal = new DateTime(jadwal.dtj_tglawal);
			DateTime tglakhirO = new DateTime(jadwalO.dtj_tglakhir);
			DateTime tglakhir = new DateTime(jadwal.dtj_tglakhir);
			if (!Objects.equals(tglawal, tglawalO) || !Objects.equals(tglakhir, tglakhirO)) {
				jadwalEmailx.append("<tr valign=top><td>").append(jadwal.namaTahap()).append("</td><td>");
				jadwalEmailx.append(FormatUtils.formatDateTimeInd(jadwalO.dtj_tglawal)).append(" - ").append(FormatUtils.formatDateTimeInd(jadwalO.dtj_tglakhir));
				jadwalEmailx.append("</td><td>");
				jadwalEmailx.append(FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal)).append(" - ").append(FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir));
				jadwalEmailx.append("</td><td>").append(jadwal.dtj_keterangan.toString()).append("</td></tr>");
			}
		}
		jadwalEmailx.append("</table>");
		String namaPaket = lelang.getNamaPaket();
		List<Peserta> pesertaList = Peserta.find("lls_id=?", lelang.lls_id).fetch();
		String email = null;
		Template template = TemplateLoader.load("/email/perubahan-jadwal-lelang.html");
		MailQueue mails[] = new MailQueue[pesertaList.size()];
		int i =0;
		for (Peserta peserta : pesertaList) {
			email = Rekanan.findEmail(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("email", email);
			map.put("lls_id", lelang.lls_id);
			map.put("jadwal", jadwalEmailx);
			map.put("waktuPerubahan", FormatUtils.formatDateTimeInd(waktuPerubahan));
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			MailQueue mail = EmailManager.createEmail(email, template, map);
			mail.lls_id = lelang.lls_id;
			mail.rkn_id = peserta.rkn_id;
			mail.jenis = Integer.valueOf(7);
			mail.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mail.save();
			mails[i] = mail;
			i++;
		}
       /* List<Pegawai> pegawaiList = Pegawai.find("peg_id in (select peg_id from Anggota_panitia where pnt_id=?)", panitia.pnt_id).fetch();
        for (Pegawai pegawai : pegawaiList) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("paket", namaPaket);
            map.put("email", pegawai.peg_email);
            map.put("lls_id", lelang.lls_id);
            map.put("jadwal", jadwalEmailx);
            map.put("waktuPerubahan",FormatUtils.formatDateTimeInd(waktuPerubahan));
            MailQueue mail = EmailManager.createEmail(pegawai.peg_email, template, map);
            mail.lls_id = lelang.lls_id;
            mail.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
            mail.jenis = Integer.valueOf(7);
            mail.save();
        }*/
	}

	public static InputStream cetak(MailQueue mailQueue) {
		Map<String, Object> params = new HashMap<>();
		params.put("message", metaReplacement(mailQueue.getRawBody()));
		params.put("subject", mailQueue.subject);
		Template template = TemplateLoader.load("/mailTemplate/template-undangan.html");
		return HtmlUtil.generatePDF(template, params);
	}

	public static String metaReplacement(String content) {
		Matcher matcher = metaPattern.matcher(content);
		int total = 0;
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			String found = matcher.group();
			Logger.debug("found: " + found);
			if (!found.contains(" />")) {
				total++;
				final String cleaned = matcher.group().replace(">", " />");
				matcher.appendReplacement(sb, cleaned);
			}
		}
		Logger.debug( "totalMeta: " + total);
		matcher.appendTail(sb);
		final String result = sb.toString();
		Logger.debug(result);
		return result;
	}
}
