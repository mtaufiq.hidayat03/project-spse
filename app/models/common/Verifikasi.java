package models.common;

import models.agency.Pegawai;
import models.jcommon.db.base.BaseModel;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;

@Table(name="VERIFIKASI")
public class Verifikasi extends BaseModel {

	@Id(sequence="seq_verifikasi", function="nextsequence")
	public Long ver_id;

	public Date ver_tgl_kirim;

	public Date ver_tgl_diverifikasi;

	public Date ver_tgl_baca;

	public String ver_komentar_ver;

	public Integer ver_status;

	//relasi ke Pegawai
	public Long peg_id;

	//relasi ke Rekanan
	public Long rkn_id;
	
	@Transient
	private Pegawai pegawai;
	@Transient
	private Rekanan rekanan;
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	

}
