package models.common;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name = "NEGARA")
public class Negara extends BaseTable {

    @Id
    public Long id;

    public String nama;

}
