package models.common;

import ext.FormatUtils;
import models.jcommon.config.Configuration;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author Mr. Andik
 *
 */

public class SesiPelatihan implements Serializable {

	public static final int maxSesi=Integer.parseInt(Play.configuration.getProperty("sesi.pelatihan.count", "1000"));
	public static final String SESI_PELATIHAN_AKTIF_KEY = "SESI_PELATIHAN_AKTIF_KEY";
	public static final String SESI_PELATIHAN_LIST_KEY = "SESI_PELATIHAN_LIST_KEY";
	public int id;
	public String nama;
	public boolean isActive;

	public SesiPelatihan(int id, String nama, boolean isActive) {
		this.id=id;
		this.nama = nama;
		this.isActive = isActive;
	}

	public static void init() {
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_LIST_KEY, List.class);
		if(list == null)
			list = new ArrayList<>();
		String sesi_pelatihan_json = Configuration.getConfigurationValue(CONFIG.SESI_PELATIHAN.toString());
		if(StringUtils.isEmpty(sesi_pelatihan_json)) {
			Logger.info("[SesiPelatihan] init %s sesi", maxSesi);
			list.add(new SesiPelatihan(0, "Sesi Default" , true));
			for(int i=1;i<maxSesi;i++)
				list.add(new SesiPelatihan(i, "Sesi #" + i, i<=maxSesi));//default ada 1000 sesi yang aktif
			Configuration.updateConfigurationValue(CONFIG.SESI_PELATIHAN.toString(), CommonUtil.toJson(list));
		} else {
			SesiPelatihan[] array = CommonUtil.fromJson(sesi_pelatihan_json, SesiPelatihan[].class);
			if(array != null && array.length > 0)
				list = new ArrayList<SesiPelatihan>(Arrays.asList(array));
		}
		Cache.set(SESI_PELATIHAN_LIST_KEY, list);
		// set pelatihan aktif
		setSesiAktif(list);
	}

	/**Dapatkan semua sesi pelatihan. JIka tidak ada, maka create dan insert ke DB
	 *
	 * @return
	 * @throws IOException
	 */
	public static List<SesiPelatihan> findAll()
	{
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_LIST_KEY, List.class);
		return list;
	}

	public static int getActiveCount()
	{
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_AKTIF_KEY, List.class);
		return CollectionUtils.size(list);
	}
	public static int getCount()
	{
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_LIST_KEY, List.class);
		return CollectionUtils.size(list);
	}

	public static List<SesiPelatihan> getActive() {
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_AKTIF_KEY, List.class);
		if(list == null)
			list = new ArrayList<>();
		return list;
	}

	public static SesiPelatihan findById(int id) {
		SesiPelatihan result = null;
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_LIST_KEY, List.class);
		if(!CollectionUtils.isEmpty(list)) {
			for (SesiPelatihan sesi : list) {
				if (sesi != null && sesi.id == id) {
					result = sesi;
					break;
				}
			}
		}
		return result;
	}

	public static void save(SesiPelatihan sesiPelatihan) {
		List<SesiPelatihan> list = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_LIST_KEY, List.class);
		for (SesiPelatihan sesi : list) {
			if(sesi != null && sesiPelatihan.id == sesi.id) {
				sesi.nama = sesiPelatihan.nama;
				sesi.isActive = sesiPelatihan.isActive;
				break;
			}
		}
		Configuration.updateConfigurationValue(CONFIG.SESI_PELATIHAN.toString(), CommonUtil.toJson(list));
		// set pelatihan aktif
		setSesiAktif(list);
	}

	private static void setSesiAktif(List<SesiPelatihan> list) {
		List<SesiPelatihan> listAktif = (List<SesiPelatihan>)Cache.get(SESI_PELATIHAN_AKTIF_KEY, List.class);
		if(listAktif == null)
			listAktif = new ArrayList<>();
		else
			listAktif.clear();
		for(SesiPelatihan sesi : list)
			if (sesi != null && sesi.isActive)
				listAktif.add(sesi);
		Cache.set(SESI_PELATIHAN_AKTIF_KEY, listAktif);
	}

	public String toString()
	{
		return "[" + id + ']' + nama;
	}

	public String getWaktuPelatihan() {
		return FormatUtils.formatDateTimeInd(DateUtil.newDate(id));
	}

	/**
	 * dijalankan saat aplikasi start
	 * setting
	 */
	public static void setupConfig() {
		if(ConfigurationDao.isLatihan())
			init();//Set SesiPelatihan
		// [AA] : sebelum spse 4 build 5378 informasi sesi disimpan di process_instance, ke depan informasi sesi lelang ada di lls_sesi table lelang_seleksi
//		Logger.info("Updating sesi ....");
//		if (ConfigurationDao.isProduction()) { // jika production maka set semua sesi 0
//			Query.update("UPDATE lelang_seleksi SET lls_sesi='0'");
//			Query.update("UPDATE ekontrak.nonlelang_seleksi SET lls_sesi='0'"); // untuk pl
//		} else {
//			List<SesiPelatihan> list = SesiPelatihan.getActive();
//			try
//			{
//				if (!CommonUtil.isEmpty(list)) {
//					for (SesiPelatihan sesi : list) {
//						String where = "(select process_instance_id from wf_process_instance where description like '{\"sesi\":"+ sesi.id + "%')";
//						Query.update("UPDATE lelang_seleksi SET lls_sesi = ? WHERE lls_wf_id in " + where, sesi.id);
//						//untuk PL
//						where = "(select process_instance_id from ekontrak.wf_process_instance where description like '{\"sesi\":"+ sesi.id + "%')";
//						Query.update("UPDATE ekontrak.nonlelang_seleksi SET lls_sesi = ? WHERE lls_wf_id in " + where, sesi.id);
//					}
//					Query.update("UPDATE wf_process_instance SET description = (SELECT lls_id FROM lelang_seleksi WHERE lls_wf_id=process_instance_id)");
		//untuk PL
//					Query.update("UPDATE ekontrak.wf_process_instance SET description = (SELECT lls_id FROM ekontrak.nonlelang_seleksi WHERE lls_wf_id=process_instance_id)");
//				}
//			}
//			catch(Exception e )
//			{
//				Logger.error("[ApplicationStart] ERROR: %s", e);
//			}
//		}

	}

}
