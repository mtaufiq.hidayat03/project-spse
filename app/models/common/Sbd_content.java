package models.common;

@Deprecated
public enum Sbd_content {
	
	UMUM(0,"Umum"),
	IKP(1, "Instruksi Kepada Peserta"),
	LDP(2, "Lembar Data Pemilihan"),
	LDK(3, "Lembar Data Kualifikasi"),
	ISIAN_KUALIFIKASI(4, "Isian Data Kualifikasi"),
	PETUNJUK_ISIAN_KUALIFIKASI(5, "Petunjuk Pengisian Formulir Isian Kualifikasi"),	
	TATACATA_KUALIFIKASI(6,"Tata Cara Evaluasi Kualifikasi"),	
	BENTUK_DOK_KONTRAK(7,"Bentuk Dokumen Kontrak"),
	SSUK(8,"Syarat-Syarat Umum Kontrak"),
	SSKK(9,"Syarat-Syarat Khusus Kontrak"),
	SPEK_TEKNIS(10, "Spesifikasi Teknis dan Gambar"),
	DAFTAR_KUANTITAS_HARGA(11, "Daftar Kuantitas dan Harga"),
	BENTUK_DOK_LAINNYA(12,"Bentuk Dokumen Lainnya"),
	UNDANGAN(13,"Undangan");
	
	public final Integer id;
	public final String label;
	
	private Sbd_content(int id, String label) {
		this.id = Integer.valueOf(id);
		this.label = label;
	}
	
	public static final Sbd_content[] dok_kualifikasi = new Sbd_content[] {UMUM, IKP, LDK, ISIAN_KUALIFIKASI, PETUNJUK_ISIAN_KUALIFIKASI, TATACATA_KUALIFIKASI};
	public static final Sbd_content[] dok_pengadaan = new Sbd_content[] {UMUM, IKP, LDP, LDK, PETUNJUK_ISIAN_KUALIFIKASI, TATACATA_KUALIFIKASI,
																		BENTUK_DOK_KONTRAK, SSUK, SSKK, SPEK_TEKNIS, DAFTAR_KUANTITAS_HARGA, BENTUK_DOK_LAINNYA};
	public static final Sbd_content[] dok_pemilihan = new Sbd_content[] {UMUM, IKP, LDP, BENTUK_DOK_KONTRAK, SSUK, SSKK, SPEK_TEKNIS, DAFTAR_KUANTITAS_HARGA, BENTUK_DOK_LAINNYA};
	public static final Sbd_content[] dok_express = new Sbd_content[] {UMUM, UNDANGAN, IKP, LDP, BENTUK_DOK_KONTRAK, SSKK, SPEK_TEKNIS, DAFTAR_KUANTITAS_HARGA, BENTUK_DOK_LAINNYA};
	public static final String[] bab = new String[] {"BAB I", "BAB II" , "BAB III", "BAB IV", "BAB V", "BAB VI", "BAB VII", "BAB VIII","BAB IX","BAB X","BAB XI","BAB XII","BAB XIII"};
}
