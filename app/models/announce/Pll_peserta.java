package models.announce;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

@Table(name="PLL_PESERTA")
public class Pll_peserta extends BaseModel {

	@Id(sequence="seq_pll_peserta", function="nextsequence")
	public Long plp_id;

	public String plp_nama;

	public String plp_alamat;

	public String plp_kota;

	public Integer plp_pemenang;

	//relasi ke Pll_paket
	public Long plp_paket;
	
	@Transient
	private Pll_paket pll_paket;
	
	public Pll_paket getPll_paket() {
		if(pll_paket == null)
			pll_paket = Pll_paket.findById(plp_paket);
		return pll_paket;
	}
	
	
	
}
