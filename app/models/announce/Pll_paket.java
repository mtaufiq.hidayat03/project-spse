package models.announce;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;


@Table(name="PLL_PAKET")
public class Pll_paket extends BaseModel {
	
	@Id(sequence="seq_pll_paket", function="nextsequence")
	public Long plp_paket;

	public String plp_kode_paket;

	public String plp_nama_paket;

	public Double plp_pagu;

	public String plp_bidang;

	public String plp_sub_bidang;

	public String plp_lokasi;

	public Integer plp_metode;

	public Integer plp_tahap;

	public Double plp_hps;
	
	public String kls_id;

	//relasi ke Pengumuman_lelang
	public Long pll_id;
	@Transient
	private Pengumuman_lelang pengumuman_lelang;
	
	public Pengumuman_lelang getPengumuman_lelang() {
		if(pengumuman_lelang == null)
			pengumuman_lelang = Pengumuman_lelang.findById(pll_id);
		return pengumuman_lelang;
	}
	
}
