package models.announce;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;


@Table(name="PLL_SYARAT")
public class Pll_syarat extends BaseModel {
	
	@Id(sequence="seq_pll_syarat", function="nextsequence")
	public Long syr_id;

	public Integer syr_urutan;

	public String syr_syarat;

	//relasi ke Pengumuman_lelang
	public Long pll_id;

	@Transient
	private Pengumuman_lelang pengumuman_lelang;
	
	public Pengumuman_lelang getPengumuman_lelang() {
		if(pengumuman_lelang == null)
			pengumuman_lelang = Pengumuman_lelang.findById(pll_id);
		return pengumuman_lelang;
	}
}
