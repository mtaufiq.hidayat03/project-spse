package models.announce;

import ext.FormatUtils;
import models.agency.Panitia;
import models.agency.Satuan_kerja;
import models.common.Sub_tag;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;


@Table(name="PENGUMUMAN_LELANG")
public class Pengumuman_lelang extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum Status {
		DRAFT("Draft"), PUBLISH("Diumumkan"), DELETED("Terhapus");
        public final String label;
        private Status(String label) {
            this.label = label;
        }
        public boolean isDraft() {
            return this == DRAFT;
        }
	}

	@Id(sequence="seq_pengumuman_lelang", function="nextsequence")
	public Long pll_id;

    @Required(message = "No. Pengumuman wajib diisi")
	public String pll_no_peng;

	public String pll_par_pembuka;

	public String pll_par_penutup;

    @Required
	public Date pll_tgl_buat;

    @Required
	public Status pll_status;

    @Required(message = "Tahun Anggaran wajib diisi")
	public Integer pll_tahun;

	public String pll_syarat_umum;

	public String pll_ket;

	public Date pll_tgl_surat;
	
	public Date pll_tgl_diumumkan;

	//relasi ke Panitia
	public Long pnt_id;

	//relasi ke Satuan_kerja
    @Required(message = "Satuan Kerja Harap Diisi")
	public Long stk_id;

    //relasi ke Sub_tag
    @Required(message = "Jenis Metode Harap Diisi")
	public String stg_id;
    
    @Transient
    private Panitia panitia;
    @Transient
    private Satuan_kerja satuan_kerja;
    @Transient
    private Sub_tag sub_tag;
    
    public Panitia getPanitia() {
    	if(panitia == null)
    		panitia = Panitia.findById(pnt_id);
    	return panitia;
    }
    
    
	public Satuan_kerja getSatuan_kerja() {
		if(satuan_kerja == null)
			satuan_kerja = Satuan_kerja.findById(stk_id);
		return satuan_kerja;
	}


	public Sub_tag getSub_tag() {
		if(sub_tag == null)
			sub_tag = Sub_tag.findById(stg_id);
		return sub_tag;
	}


	@Override
	public void preDelete() {
		Pll_tahap.delete("pll_id=?", pll_id);
        Pll_paket.delete("pll_id=?", pll_id);
	}
	
	public static void save(Pengumuman_lelang pgn_lelang, List<Pll_paket> paketList, Pll_tahap tahapAmbil) {
		pgn_lelang.save();
        for(Pll_paket pll_paket:paketList) {
            Pll_paket paket = new Pll_paket();
            if(pll_paket.plp_paket != null)
                paket = Pll_paket.findById(pll_paket.plp_paket);
            paket.plp_nama_paket = pll_paket.plp_nama_paket;
            paket.plp_hps = pll_paket.plp_hps;
            paket.plp_pagu = pll_paket.plp_pagu;
            paket.plp_sub_bidang = pll_paket.plp_sub_bidang;
            paket.pll_id = pgn_lelang.pll_id;
            paket.save();
        }
        Pll_tahap tahap = new Pll_tahap();
        if(tahapAmbil.tpl_id != null)
            tahap = Pll_tahap.findById(tahapAmbil.tpl_id);
        tahap.pll_tgl_awal = tahapAmbil.pll_tgl_awal;
        tahap.pll_tgl_akhir = tahapAmbil.pll_tgl_akhir;
        tahap.tempat = tahapAmbil.tempat;
        tahap.pll_id = pgn_lelang.pll_id;
        tahap.nama_tahap="Waktu & Tempat Pengambilan Dokumen";
        tahap.save();
	}
	
	/*public static String tableNonEproc(){
		String[] column = new String[]{"pl.pll_id","plp_nama_paket","stk_nama","plp_hps"};
		String sql="SELECT pl.pll_id, s.stk_nama, pp.plp_nama_paket, pp.plp_pagu ,pp.plp_hps, pt.pll_tgl_awal, pt.pll_tgl_akhir, pl.pll_status FROM pengumuman_lelang pl, pll_paket pp, pll_tahap pt, satuan_kerja s "+
                "WHERE pl.pll_id = pp.pll_id AND pl.pll_id=pt.pll_id AND pl.stk_id=s.stk_id AND pt.nama_tahap='Waktu & Tempat Pengambilan Dokumen'";
		String sql_count="SELECT COUNT(*) FROM pengumuman_lelang pl, pll_paket pp, pll_tahap pt, satuan_kerja s WHERE pl.pll_id = pp.pll_id AND pl.pll_id=pt.pll_id AND pl.stk_id=s.stk_id AND pt.nama_tahap='Waktu & Tempat Pengambilan Dokumen'";
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetNonEproc);
    }

    public static String tableNonEprocPanitia(final Long panitiaId){
    	String[] column = new String[]{"pll_id","pll_no_peng","pll_tgl_buat","pll_tahun","pll_status"};
    	String sql="SELECT pll_id, pll_no_peng, pll_status, pll_tgl_buat, pll_tahun, (SELECT COUNT(plp_paket) FROM pll_paket p WHERE pll_id=pengumuman_lelang.pll_id) as jumlah_paket FROM pengumuman_lelang WHERE pnt_id="+panitiaId;
    	String sql_count="SELECT COUNT(pll_id) FROM pengumuman_lelang WHERE pnt_id="+panitiaId;
    	return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetNonEprocPanitia);
    }*/
    
    public static final ResultSetHandler<String[]> resultsetNonEproc = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			 String[] tmp = new String[5];
             Long pll_id = rs.getLong("pll_id");
             Date tglawal = rs.getTimestamp("pll_tgl_awal");
             Date tglakhir = rs.getTimestamp("pll_tgl_akhir");
             tmp[0] = pll_id.toString();
             tmp[1] = rs.getString("plp_nama_paket");
             tmp[2] = rs.getString("stk_nama");
             tmp[3] = FormatUtils.formatCurrenciesJutaRupiah(rs.getDouble("plp_hps"));
             tmp[4] = FormatUtils.formatDate(tglawal, tglakhir);
             return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetNonEprocPanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
            Long pll_id = rs.getLong("pll_id");
            tmp[0] = pll_id.toString();
            tmp[1] = rs.getString("pll_no_peng");
            tmp[2] = FormatUtils.formatDateInd(rs.getTimestamp("pll_tgl_buat"));
            tmp[3] = rs.getString("pll_tahun");
            tmp[4] = rs.getString("jumlah_paket");
            int status = rs.getInt("pll_status");
            tmp[5] = status == 1 ? "Diumumkan":"Draft";
            return tmp;
		}
	};
}
