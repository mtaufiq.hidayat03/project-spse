package models.announce;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Pengumuman_lelang_panitia implements Serializable {
	public Long pll_id;

	public Long pnt_id;

	public String pll_no_peng;

	public Date pll_tgl_buat;

	public Integer pll_status;

	public Long jumlah_paket;

	public Integer pll_tahun;
}
