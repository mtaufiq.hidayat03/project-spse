package models.announce;

import ext.DatetimeBinder;
import models.jcommon.db.base.BaseModel;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;

@Table(name="PLL_TAHAP")
public class Pll_tahap extends BaseModel {
	
	@Id(sequence="seq_pll_tahap", function="nextsequence")
	public Long tpl_id;

	public String nama_tahap;

	@Required
	public String tempat;

	//relasi ke Pengumuman_lelang
	public Long pll_id;

    @As(binder = DatetimeBinder.class)
	public Date pll_tgl_awal;

    @As(binder = DatetimeBinder.class)
	public Date pll_tgl_akhir;
    
    @Transient
	private Pengumuman_lelang pengumuman_lelang;
	
	public Pengumuman_lelang getPengumuman_lelang() {
		if(pengumuman_lelang == null)
			pengumuman_lelang = Pengumuman_lelang.findById(pll_id);
		return pengumuman_lelang;
	}

}
