package models.announce;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Pll_view implements Serializable {
	
	public Long pll_id;

	public String plp_nama_paket;

	public String stk_nama;

	public Date pll_tgl_awal;

	public Date pll_tgl_akhir;

	public Double plp_pagu;

	public Double plp_hps;

	public Integer pll_status;
}
