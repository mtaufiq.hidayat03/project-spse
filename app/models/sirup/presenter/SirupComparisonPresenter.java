package models.sirup.presenter;

import ext.FormatUtils;
import models.agency.Paket_pl;
import models.nonlelang.nonSpk.JenisRealisasiNonSpk;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import models.sirup.Sirup;

/**
 * @author HanusaCloud on 4/2/2018
 */
public class SirupComparisonPresenter {

    private final Long pktId;
    private final Long sirupId;
    private final String paketRupId;
    private final Non_spk_seleksi seleksi;
    private final Paket_pl paket_pl;
    private final Sirup sirup;
    private final Double totalRealization;
    private final Double paketPagu;

    public SirupComparisonPresenter(Non_spk_seleksi seleksi, Paket_pl paket_pl, Sirup sirup) {
        this.pktId = paket_pl.pkt_id;
        this.sirupId = sirup.id;
        this.seleksi = seleksi;
        this.paket_pl = paket_pl;
        this.sirup = sirup;
        this.totalRealization = JenisRealisasiNonSpk.getJumlahNilaiRealisasi(seleksi.lls_id,null);
        this.paketRupId = paket_pl.getKodeRup();
        this.paketPagu = paket_pl.pkt_pagu;
    }

    public String getPaketName() {
        return paket_pl.pkt_nama;
    }

    public String getSirupName() {
        return sirup.nama;
    }

    public String getPakatRupId() {
        return paketRupId;
    }

    public String getPaketPaguString() {
        return FormatUtils.formatCurrencyRupiah(paketPagu);
    }

    public boolean isPaguBiggerThanRealization() {
        return sirup.jumlahPagu > totalRealization;
    }

}
