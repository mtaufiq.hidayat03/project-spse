package models.sirup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import ext.FormatUtils;
import models.agency.Anggaran;
import models.agency.contracts.SirupContract;
import models.common.Kategori;
import models.common.MetodePemilihanPenyedia;
import models.common.SumberDana;
import models.jcommon.util.CommonUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.JsonBinaryType;
import play.db.jdbc.Table;
import play.libs.WS;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Table(name = "paket_sirup")
public class PaketSirup extends BaseTable implements SirupContract {

    @Id
    public Long id;

    public String nama;

    @Transient
    @SerializedName("paket_lokasi_json")
    public List<PaketSirupLokasi> paket_lokasi_json;

    @JsonBinaryType
    public String paket_lokasi;

    public String volume;

    public String keterangan;

    public String spesifikasi;

    public boolean is_tkdn;

    public boolean is_pradipa;

    @Transient
    @SerializedName("paket_anggaran_json")
    public List<PaketSirupAnggaran> paket_anggaran_json;

    @JsonBinaryType
    public String paket_anggaran;

    public Double pagu;

    @Transient
    @SerializedName("paket_jenis_json")
    public List<PaketSirupJenis> paket_jenis_json;

    @JsonBinaryType
    public String paket_jenis;
    
    public Integer metode_pengadaan;

    public Date tanggal_kebutuhan;

    public Date tanggal_awal_pengadaan;

    public Date tanggal_akhir_pengadaan;

    public Date tanggal_awal_pekerjaan;

    public Date tanggal_akhir_pekerjaan;

    public Date tanggal_pengumuman;

    public String rup_stk_id;

    public Integer tahun;
    
    @Transient
    public boolean paket_aktif;

    @Transient
    public boolean paket_terhapus;

    @Transient
    public boolean paket_terumumkan;

    @Override
    protected void prePersist() {
        if(!CollectionUtils.isEmpty(paket_lokasi_json))
            paket_lokasi = CommonUtil.toJson(paket_lokasi_json);
        if(!CollectionUtils.isEmpty(paket_anggaran_json))
            paket_anggaran = CommonUtil.toJson(paket_anggaran_json);
        if(!CollectionUtils.isEmpty(paket_jenis_json))
            paket_jenis = CommonUtil.toJson(paket_jenis_json);
    }

    @Override
    protected void postLoad() {
        if(!StringUtils.isEmpty(paket_anggaran))
            paket_anggaran_json = CommonUtil.fromJson(paket_anggaran, new TypeToken<List<PaketSirupAnggaran>>(){}.getType());
        if(!StringUtils.isEmpty(paket_lokasi))
            paket_lokasi_json = CommonUtil.fromJson(paket_lokasi, new TypeToken<List<PaketSirupLokasi>>(){}.getType());
        if(!StringUtils.isEmpty(paket_jenis))
            paket_jenis_json = CommonUtil.fromJson(paket_jenis, new TypeToken<List<PaketSirupJenis>>(){}.getType());
    }

    /**
     * simpan rup_paket
     * @param list
     * @param rup_stk_id (kode satker sirup)
     * @param tahun (tahun anggaran)
     */
    public static void simpan(List<PaketSirup> list, String rup_stk_id, Integer tahun) {
        if(!CommonUtil.isEmpty(list)) {
        	// delete dulu semua paket satker dan tahun anggaran ini
            delete("rup_stk_id = ? AND tahun = ? AND id NOT IN (SELECT rup_id FROM paket_anggaran)", rup_stk_id, tahun);
            Pattern pattern = Pattern.compile("\\r?\\n");
            for (PaketSirup rup_paket : list) {
            	rup_paket.nama = pattern.matcher(rup_paket.nama.trim()).replaceAll("");
                // translasi metode, info detil https://gitlab.lkpp.go.id/eproc/spse/issues/1499
                MetodePemilihanPenyedia mtdPemilihan = MetodePemilihanPenyedia.findById(rup_paket.metode_pengadaan);
                switch (mtdPemilihan) {
                    case LELANG_UMUM:
                    case LELANG_SEDERHANA:
                    case LELANG_TERBATAS:
                    case PEMILIHAN_LANGSUNG: rup_paket.metode_pengadaan = MetodePemilihanPenyedia.TENDER.id;break;
                    case LELANG_CEPAT:rup_paket.metode_pengadaan = MetodePemilihanPenyedia.TENDER_CEPAT.id;break;
                    case SELEKSI_SEDERHANA:
                    case SELEKSI_UMUM:rup_paket.metode_pengadaan = MetodePemilihanPenyedia.SELEKSI.id;break;
                }
                rup_paket.rup_stk_id = rup_stk_id;
                rup_paket.tahun = tahun;
                rup_paket.save();
            }
        }
    }

    public static class PaketSirupLokasi {
        public Long id;
        public Long pkt_id;
        public Long id_kabupaten;
        public Long id_provinsi;
        public Date auditupdate;
        public String detil_lokasi;
    }

    public static class  PaketSirupAnggaran {
        public Long id;
        public Long pkt_id;
        public Integer jenis;
        public Integer sumber_dana;
        public String asal_dana;
        public Long asal_dana_satker;
        public String dana_apbd;
        public Long id_komponen;
        public Long id_kegiatan;
        public String mak;
        public Double pagu;
        public Integer tahun_anggaran_dana;// jika multiple
        public Long id_tp;
        public Date auditupdate;
        public Long pkt_id_client;

        public SumberDana getSumberDana() {
            if(sumber_dana != null){
                return SumberDana.values()[sumber_dana - 1];
            }
            return null;
        }

        public boolean dataLengkap(){
            return getSumberDana() != null && mak != null && !mak.isEmpty() && pagu != null;
        }
    }

    public static class PaketSirupJenis {
        public Long id;
        public Integer jenisid;
        public Long pkt_id;
        public Double jumlah_pagu;

        public Kategori getKategori() {
            Kategori kategori = null;
            if(jenisid != null) {
                switch (jenisid) {
                    case 1:
                        kategori = Kategori.PENGADAAN_BARANG;
                        break;
                    case 2:
                        kategori = Kategori.PEKERJAAN_KONSTRUKSI;
                        break;
                    case 3:
                        kategori = Kategori.KONSULTANSI;
                        break;
                    case 4:
                        kategori = Kategori.JASA_LAINNYA;
                        break;
                }
            }
            return kategori;
        }

        public boolean dataLengkap(){
            return getKategori() != null;
        }

        public String toString(){
            return "{id:"+id+",pkt_id="+pkt_id+",jenisid="+jenisid+",jumlah_pagu="+jumlah_pagu+"}";
        }
    }

    public boolean paketLengkap(){
        return paketAnggaranLengkap() && paketJenisLengkap();
    }

    public boolean paketAnggaranLengkap() {
        if(CollectionUtils.isEmpty(paket_anggaran_json))
            return false;
        for (PaketSirupAnggaran rup_anggaran : paket_anggaran_json) {
            if(!rup_anggaran.dataLengkap()){
                return false;
            }
        }
        return true;
    }

    public boolean paketJenisLengkap() {
        if(CollectionUtils.isEmpty(paket_jenis_json))
            return false;
        for (PaketSirupJenis rup_jenis : paket_jenis_json) {
            if(!rup_jenis.dataLengkap()){
                return false;
            }
        }
        return true;
    }

    public String tahunAnggaran() {
        if(CollectionUtils.isEmpty(paket_anggaran_json))
            return "";
        StringBuilder result = new StringBuilder();
        for (PaketSirupAnggaran rup_anggaran : paket_anggaran_json) {
            if(rup_anggaran.tahun_anggaran_dana == null)
                continue;

            if(result.length() > 0)
                result.append(",");
            result.append(rup_anggaran.tahun_anggaran_dana);
        }

        return result.toString().isEmpty() ? String.valueOf(tahun) : result.toString();
    }

    @Override
    public String toString() {
        return "PaketSirup{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", paket_lokasi='" + paket_lokasi + '\'' +
                ", is_tkdn=" + is_tkdn +
                '}';
    }

    public Kategori getKategori() { // ambil kategori dari index 0
        if(!CollectionUtils.isEmpty(paket_jenis_json))
            return paket_jenis_json.get(0).getKategori();
        return null;
    }

    public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            tmp[0] = rs.getString("id");
            tmp[1] = rs.getString("nama");
            tmp[4] = FormatUtils.formatCurrenciesJutaRupiah(rs.getDouble("pagu"));
            tmp[5] = rs.getString("tahun");
            if(rs.getInt("sumberdana") > 0)
                tmp[2] = SumberDana.values()[rs.getInt("sumberdana") - 1].getLabel();
            tmp[3] = MetodePemilihanPenyedia.findById(rs.getInt("metode_pengadaan")).getLabel();

//			tmp[4] = rs.getString("paket");
            return tmp;
        }
    };

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String getNama() {
        return this.nama;
    }

    @Override
    public String getSumberDanaString(){
        if(CollectionUtils.isEmpty(paket_anggaran_json))
            return "";
        StringBuilder result = new StringBuilder();
        paket_anggaran_json.forEach(item -> result.append(item.getSumberDana().getLabel()));
        return result.toString();
    }

    public List<Anggaran> getAnggaranList(Long pkt_id) {
        return Anggaran.find("ang_id in (select ang_id from paket_anggaran where rup_id=? and pkt_id=?)", id, pkt_id).fetch();
    }

    public List<Anggaran> getAnggaranList(Long pkt_id, Long ppk_id) {
        return Anggaran.find("ang_id in (select ang_id from paket_anggaran where rup_id=? and pkt_id=? and ppk_id=?)", id, pkt_id, ppk_id).fetch();
    }

    public List<Anggaran> getAnggaranPlList(Long pkt_id) {
        return Anggaran.find("ang_id in (select ang_id from ekontrak.paket_anggaran where rup_id=? and pkt_id=?)", id, pkt_id).fetch();
    }

    public List<Anggaran> getAnggaranPlList(Long pkt_id, Long ppk_id) {
        return Anggaran.find("ang_id in (select ang_id from ekontrak.paket_anggaran where rup_id=? and pkt_id=? and ppk_id=?)", id, pkt_id, ppk_id).fetch();
    }

    public static void updatePaketFromSirup(final String rup_stk_id, final Integer tahun)  {
        if(StringUtils.isEmpty(rup_stk_id) || tahun == null)
            return;
        Logger.debug("get data paket sirup stk_id %s tahun %s", rup_stk_id, tahun);
        String url = BasicCtr.SIRUP_URL + "/service2/paketPenyediaPerSatkerTampilAllStatus?idSatker=" + rup_stk_id + "&tahunAnggaran=" + tahun;
        try {
            String response = WS.url(url).getAsync().get().getString();
            if(!StringUtils.isEmpty(response)) {
                List<PaketSirup> list = gson.fromJson(response, type);
                list = list.stream().filter(item -> item.paket_aktif && !item.paket_terhapus && item.paket_terumumkan)
                        .collect(Collectors.toList());
                simpan(list, rup_stk_id, tahun);
                Logger.debug("save data paket sirup %s tahun %s", rup_stk_id, tahun);
            }
        }catch (Exception e) {
            Logger.error(e, "Kendala Akses Service Paket SIRUP");
        }
    }

    static Type type = new TypeToken<ArrayList<PaketSirup>>(){}.getType();
    static Gson gson = new GsonBuilder().registerTypeAdapter(PaketSirup.class, new PaketSirupDeserializer()).create();
}
