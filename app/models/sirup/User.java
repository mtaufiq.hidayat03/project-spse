package models.sirup;

/**
 * @author HanusaCloud on 4/2/2018
 */
public class User {

    private String idKldi;

    private String updateBy;

    private Kldi kldi;

    private String password;

    private String id;

    private String createdOn;

    private String jabatan;

    private String namaLengkap;

    private String golongan;

    private String username;

    private String createdBy;

    private String idSatker;

    private String email;

    private String telepon;

    private String role;

    private String nip;

    private String alamat;

    private String updateOn;

}
