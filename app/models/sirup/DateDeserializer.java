package models.sirup;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;
import java.util.ArrayList;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import play.Logger;

public class DateDeserializer implements JsonDeserializer<Date> {
	
	private ArrayList<SimpleDateFormat> formats = new ArrayList<SimpleDateFormat>(Arrays.asList(
			new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a"),
			new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss"),
			new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
		));
		
	@Override
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
	   try {
	        String j = json.getAsJsonPrimitive().getAsString();
	        return parseDate(j);
	    } catch (ParseException e) {
	        throw new JsonParseException(e.getMessage(), e);
	    }
	}
	
	private Date parseDate(String dateString) throws ParseException {
	    if (dateString != null && dateString.trim().length() > 0) {
	        for (SimpleDateFormat format: formats) {
		    	try {
		    		return format.parse(dateString);
		        } catch (ParseException pe) {
		        }
			}
			return null;
	    } else {
	        return null;
	    }
	}
}

