package models.sirup;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import play.Logger;
import play.libs.Json;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author arief ardiyansah
 * Json Deserializer Object PaketSwakelolaSirup
 */

public class PaketSwakelolaSirupDeserializer implements JsonDeserializer<PaketSwakelolaSirup> {

    Type lokasiType = new TypeToken<ArrayList<PaketSwakelolaSirup.PaketSirupLokasi>>(){}.getType();
    Type anggaranType = new TypeToken<ArrayList<PaketSwakelolaSirup.PaketSirupAnggaran>>(){}.getType();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
    
    @Override
    public PaketSwakelolaSirup deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        PaketSwakelolaSirup paketSirup = new PaketSwakelolaSirup();
        paketSirup.id = jsonObject.get("id").getAsLong();
        if(jsonObject.get("nama") != null && !jsonObject.get("nama").isJsonNull())
            paketSirup.nama = jsonObject.get("nama").getAsString();
        if(jsonObject.get("paket_lokasi_json") != null && jsonObject.get("paket_lokasi_json").isJsonArray())
            paketSirup.paket_lokasi_json = gson.fromJson(jsonObject.get("paket_lokasi_json").getAsJsonArray(), lokasiType);
        if(jsonObject.get("lls_volume") != null && !jsonObject.get("lls_volume").isJsonNull())
            paketSirup.volume = jsonObject.get("lls_volume").getAsString();
        if(jsonObject.get("keterangan") != null && !jsonObject.get("keterangan").isJsonNull())
            paketSirup.keterangan = jsonObject.get("keterangan").getAsString();
        if(jsonObject.get("spesifikasi") != null && !jsonObject.get("spesifikasi").isJsonNull())
            paketSirup.spesifikasi = jsonObject.get("spesifikasi").getAsString();
        if(jsonObject.get("is_pradipa") != null && !jsonObject.get("is_pradipa").isJsonNull())
            paketSirup.is_pradipa = jsonObject.get("is_pradipa").getAsBoolean();
        if(jsonObject.get("paket_anggaran_json") != null && jsonObject.get("paket_anggaran_json").isJsonArray())
        	paketSirup.paket_anggaran_json = gson.fromJson(jsonObject.get("paket_anggaran_json").getAsJsonArray(), anggaranType);
        if(jsonObject.get("jumlah_pagu") != null && !jsonObject.get("jumlah_pagu").isJsonNull())
            paketSirup.jumlah_pagu = jsonObject.get("jumlah_pagu").getAsDouble();
        if(jsonObject.get("tipe_swakelola") != null && !jsonObject.get("tipe_swakelola").isJsonNull())
            paketSirup.tipe_swakelola = jsonObject.get("tipe_swakelola").getAsInt();
        if(jsonObject.get("tanggal_kebutuhan") != null && !jsonObject.get("tanggal_kebutuhan").isJsonNull())
            paketSirup.tanggal_kebutuhan = parseDate(jsonObject.get("tanggal_kebutuhan").getAsString());
        if(jsonObject.get("tanggal_awal_pengadaan") != null && !jsonObject.get("tanggal_awal_pengadaan").isJsonNull())
            paketSirup.tanggal_awal_pengadaan = parseDate(jsonObject.get("tanggal_awal_pengadaan").getAsString());
        if(jsonObject.get("tanggal_akhir_pengadaan") != null && !jsonObject.get("tanggal_akhir_pengadaan").isJsonNull())
            paketSirup.tanggal_akhir_pengadaan = parseDate(jsonObject.get("tanggal_akhir_pengadaan").getAsString());
        if(jsonObject.get("tanggal_awal_pekerjaan") != null && !jsonObject.get("tanggal_awal_pekerjaan").isJsonNull())
            paketSirup.tanggal_awal_pekerjaan = parseDate(jsonObject.get("tanggal_awal_pekerjaan").getAsString());
        if(jsonObject.get("tanggal_akhir_pekerjaan") != null && !jsonObject.get("tanggal_akhir_pekerjaan").isJsonNull())
            paketSirup.tanggal_akhir_pekerjaan = parseDate(jsonObject.get("tanggal_akhir_pekerjaan").getAsString());
        if(jsonObject.get("tanggal_pengumuman") != null && !jsonObject.get("tanggal_pengumuman").isJsonNull())
            paketSirup.tanggal_pengumuman = parseDate(jsonObject.get("tanggal_pengumuman").getAsString());
        if(jsonObject.get("aktif") != null && !jsonObject.get("aktif").isJsonNull())
            paketSirup.aktif = jsonObject.get("aktif").getAsBoolean();
        if(jsonObject.get("umumkan") != null && !jsonObject.get("umumkan").isJsonNull())
            paketSirup.umumkan = jsonObject.get("umumkan").getAsBoolean();
        return paketSirup;
    }

    Date parseDate(String dateString) {
        try {
            return dateFormat.parse(dateString);
        }catch (Exception e) {
            Logger.error(e, "ParseException : %s", e.getMessage());
            return null;
        }
    }
}
