package models.sirup;

import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;
import play.Logger;
import play.libs.WS;
import play.libs.ws.WSRequest;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author HanusaCloud on 4/2/2018
 */
public class Sirup {

    public String kodeKldi;
    public String tanggalAkhirPekerjaan;
    public String is_delete;
    public String umumkan;
    public String metodePengadaan;
    public String aktif;
    public String sumberDanaString;
    public PaketAnggaran[] listPaketAnggaranM;
    public Long id;
    public User userM;
    public Integer bulanAwalPengadaan;
    public String keterangan;
    public String[] listPaketKonsolidasiM;
    public Integer tahunAnggaran;
    public Integer bulanAwalPekerjaan;
    public Kldi kldiM;
    public Integer bulanAkhirPekerjaan;
    public String nama;
    public Boolean isPraDipa;
    public String isFinal;
    public String spesifikasi;
    public Long idRupClient;
    public Integer bulanAkhirPengadaan;
    public Kldi kldi;
    public String auditupdate;
    public String tanggalAkhirPengadaan;
    public JenisPengadaan jenisPengadaanM;
    public String jenisPengadaan;
    public String tanggalPengumuman;
    public String createdOn;
    public Satker satuanKerja;
    public String createdBy;
    public String tanggalAwalPekerjaan;
    public Long idSatker;
    public String lokasi;
    public String volume;
    public String idKegiatan;
    public Long idProgram;
    public Double jumlahPagu;
    public String kegiatan;
    public String tanggalAwalPengadaan;

    public static Sirup getSirupById(Long id) {
        Logger.info("get sirup paket penyedia by id: " + id);
        try {
            WSRequest request = WS.url(BasicCtr.SIRUP_URL + "/service/paketPenyediaById?id=" + id);
            final String json = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get()
                    .getString();
            if (!CommonUtil.isEmpty(json)) {
                Type type = new TypeToken<List<Sirup>>(){}.getType();
                List<Sirup> sirups = CommonUtil.fromJson(json, type);
                return sirups != null && !sirups.isEmpty() ? sirups.get(0) : null;
            }
        } catch (Exception ex) {
            Logger.error(ex, ex.getMessage());
        }
        return null;
    }

}
