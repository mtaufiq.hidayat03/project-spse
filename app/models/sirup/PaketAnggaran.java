package models.sirup;

/**
 * @author HanusaCloud on 4/2/2018
 */
public class PaketAnggaran {

    private Long id;

    private String tahunAnggaranDana;

    private String asalDana;

    private AsalDanaSatker asalDanaSatkerM;

    private String asalDanaSatker;

    private String mak;

    private String pagu;

    private String auditupdate;

    private Long pktId;

    private SumberDana sumberDanaM;

    private Integer jenis;

    private Integer sumberDana;

    private AsalDana asalDanaM;

}
