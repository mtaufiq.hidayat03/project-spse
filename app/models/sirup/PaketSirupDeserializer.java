package models.sirup;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import play.Logger;
import play.libs.Json;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author arief ardiyansah
 * Json Deserializer Object PaketSirup
 */

public class PaketSirupDeserializer implements JsonDeserializer<PaketSirup> {

    Type lokasiType = new TypeToken<ArrayList<PaketSirup.PaketSirupLokasi>>(){}.getType();
    Type anggaranType = new TypeToken<ArrayList<PaketSirup.PaketSirupAnggaran>>(){}.getType();
    Type jenisType = new TypeToken<ArrayList<PaketSirup.PaketSirupJenis>>(){}.getType();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateDeserializer()).create();
    
    @Override
    public PaketSirup deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        Logger.debug("parsing json , %s", jsonObject);
        PaketSirup paketSirup = new PaketSirup();
        paketSirup.id = jsonObject.get("id").getAsLong();
        if(jsonObject.get("nama") != null && !jsonObject.get("nama").isJsonNull())
            paketSirup.nama = jsonObject.get("nama").getAsString();
        if(jsonObject.get("paket_lokasi_json") != null && jsonObject.get("paket_lokasi_json").isJsonArray())
            paketSirup.paket_lokasi_json = gson.fromJson(jsonObject.get("paket_lokasi_json").getAsJsonArray(), lokasiType);
        if(jsonObject.get("volume") != null && !jsonObject.get("volume").isJsonNull())
            paketSirup.volume = jsonObject.get("volume").getAsString();
        if(jsonObject.get("keterangan") != null && !jsonObject.get("keterangan").isJsonNull())
            paketSirup.keterangan = jsonObject.get("keterangan").getAsString();
        if(jsonObject.get("spesifikasi") != null && !jsonObject.get("spesifikasi").isJsonNull())
            paketSirup.spesifikasi = jsonObject.get("spesifikasi").getAsString();
        if(jsonObject.get("is_tkdn") != null && !jsonObject.get("is_tkdn").isJsonNull())
            paketSirup.is_tkdn = jsonObject.get("is_tkdn").getAsBoolean();
        if(jsonObject.get("is_pradipa") != null && !jsonObject.get("is_pradipa").isJsonNull())
            paketSirup.is_pradipa = jsonObject.get("is_pradipa").getAsBoolean();
        if(jsonObject.get("paket_anggaran_json") != null && jsonObject.get("paket_anggaran_json").isJsonArray())
            paketSirup.paket_anggaran_json = gson.fromJson(jsonObject.get("paket_anggaran_json").getAsJsonArray(), anggaranType);
        if(jsonObject.get("pagu") != null && !jsonObject.get("pagu").isJsonNull())
            paketSirup.pagu = jsonObject.get("pagu").getAsDouble();
        if(jsonObject.get("paket_jenis_json") != null && jsonObject.get("paket_jenis_json").isJsonArray())
            paketSirup.paket_jenis_json = Json.fromJson(jsonObject.get("paket_jenis_json").getAsJsonArray(), jenisType);
        if(jsonObject.get("metode_pengadaan") != null && !jsonObject.get("metode_pengadaan").isJsonNull())
            paketSirup.metode_pengadaan = jsonObject.get("metode_pengadaan").getAsInt();
        if(jsonObject.get("tanggal_kebutuhan") != null && !jsonObject.get("tanggal_kebutuhan").isJsonNull())
            paketSirup.tanggal_kebutuhan = parseDate(jsonObject.get("tanggal_kebutuhan").getAsString());
        if(jsonObject.get("tanggal_awal_pengadaan") != null && !jsonObject.get("tanggal_awal_pengadaan").isJsonNull())
            paketSirup.tanggal_awal_pengadaan = parseDate(jsonObject.get("tanggal_awal_pengadaan").getAsString());
        if(jsonObject.get("tanggal_akhir_pengadaan") != null && !jsonObject.get("tanggal_akhir_pengadaan").isJsonNull())
            paketSirup.tanggal_akhir_pengadaan = parseDate(jsonObject.get("tanggal_akhir_pengadaan").getAsString());
        if(jsonObject.get("tanggal_awal_pekerjaan") != null && !jsonObject.get("tanggal_awal_pekerjaan").isJsonNull())
            paketSirup.tanggal_awal_pekerjaan = parseDate(jsonObject.get("tanggal_awal_pekerjaan").getAsString());
        if(jsonObject.get("tanggal_akhir_pekerjaan") != null && !jsonObject.get("tanggal_akhir_pekerjaan").isJsonNull())
            paketSirup.tanggal_akhir_pekerjaan = parseDate(jsonObject.get("tanggal_akhir_pekerjaan").getAsString());
        if(jsonObject.get("tanggal_pengumuman") != null && !jsonObject.get("tanggal_pengumuman").isJsonNull())
            paketSirup.tanggal_pengumuman = parseDate(jsonObject.get("tanggal_pengumuman").getAsString());
        if(jsonObject.get("paket_aktif") != null && !jsonObject.get("paket_aktif").isJsonNull())
            paketSirup.paket_aktif = jsonObject.get("paket_aktif").getAsBoolean();
        if(jsonObject.get("paket_terhapus") != null && !jsonObject.get("paket_terhapus").isJsonNull())
            paketSirup.paket_terhapus = jsonObject.get("paket_terhapus").getAsBoolean();
        if(jsonObject.get("paket_terumumkan") != null && !jsonObject.get("paket_terumumkan").isJsonNull())
            paketSirup.paket_terumumkan = jsonObject.get("paket_terumumkan").getAsBoolean();
        return paketSirup;
    }

    Date parseDate(String dateString) {
        try {
            return dateFormat.parse(dateString);
        }catch (Exception e) {
            Logger.error(e, "ParseException : %s", e.getMessage());
            return null;
        }
    }

}
