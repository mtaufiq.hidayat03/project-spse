package models.sirup;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import ext.FormatUtils;
import models.agency.contracts.SirupContract;
import models.common.SumberDana;
import models.jcommon.util.CommonUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.JsonBinaryType;
import play.db.jdbc.Table;
import play.libs.WS;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Table(name = "EKONTRAK.PAKET_SWAKELOLA_SIRUP")
public class PaketSwakelolaSirup extends BaseTable implements SirupContract {

    @Id
    public Long id;

    public String nama;

    @Transient
    @SerializedName("paket_lokasi_json")
    public List<PaketSwakelolaSirup.PaketSirupLokasi> paket_lokasi_json;

    @JsonBinaryType
    public String paket_lokasi;

    public String volume;

    public String keterangan;

    public String spesifikasi;

    public boolean is_pradipa;

    @Transient
    @SerializedName("paket_anggaran_json")
    public List<PaketSwakelolaSirup.PaketSirupAnggaran> paket_anggaran_json;

    @JsonBinaryType
    public String paket_anggaran;

    public Double jumlah_pagu;

//    @JsonBinaryType
//    public String paket_jenis;

    public Integer tipe_swakelola;

    public Date tanggal_kebutuhan;

    public Date tanggal_awal_pengadaan;

    public Date tanggal_akhir_pengadaan;

    public Date tanggal_awal_pekerjaan;

    public Date tanggal_akhir_pekerjaan;

    public Date tanggal_pengumuman;

    public String rup_stk_id;

    public Integer tahun;
    
    @Transient
    public boolean aktif;

    @Transient
    public boolean umumkan;

    @Override
    protected void prePersist() {
        if(!CollectionUtils.isEmpty(paket_lokasi_json))
            paket_lokasi = CommonUtil.toJson(paket_lokasi_json);
        if(!CollectionUtils.isEmpty(paket_anggaran_json))
            paket_anggaran = CommonUtil.toJson(paket_anggaran_json);
//        if(!CollectionUtils.isEmpty(paket_jenis_json))
//            paket_jenis = CommonUtil.toJson(paket_jenis_json);
    }

    @Override
    protected void postLoad() {
        if(!StringUtils.isEmpty(paket_anggaran))
            paket_anggaran_json = CommonUtil.fromJson(paket_anggaran, new TypeToken<List<PaketSwakelolaSirup.PaketSirupAnggaran>>(){}.getType());
        if(!StringUtils.isEmpty(paket_lokasi))
            paket_lokasi_json = CommonUtil.fromJson(paket_lokasi, new TypeToken<List<PaketSwakelolaSirup.PaketSirupLokasi>>(){}.getType());
//        if(!StringUtils.isEmpty(paket_jenis))
//            paket_jenis_json = CommonUtil.fromJson(paket_jenis, new TypeToken<List<PaketSwakelolaSirup.PaketSirupJenis>>(){}.getType());
    }

    /**
     * simpan rup_paket
     * @param list
     */
    public static void simpan(List<PaketSwakelolaSirup> list , String rup_stk_id, Integer tahun) {
        if(!CommonUtil.isEmpty(list)) {
            // delete dulu semua paket satker dan tahun anggaran ini
            delete("rup_stk_id = ? AND tahun = ? AND id NOT IN (SELECT rup_id FROM ekontrak.paket_anggaran)", rup_stk_id, tahun);
            Pattern pattern = Pattern.compile("\\r?\\n");
            for (PaketSwakelolaSirup rup_paket_swakelola : list) {
                rup_paket_swakelola.nama = pattern.matcher(rup_paket_swakelola.nama.trim()).replaceAll( "");
                rup_paket_swakelola.rup_stk_id = rup_stk_id;
                rup_paket_swakelola.tahun = tahun;
                rup_paket_swakelola.save();
            }
        }
    }

    public static class PaketSirupLokasi {
        public Long id;
        public Long pkt_id;
        public Long id_kabupaten;
        public Long id_provinsi;
        public Date auditupdate;
        public String detil_lokasi;
    }


    public static class  PaketSirupAnggaran {
        public Long id;
        public Long pkt_id;
        public Integer jenis;
        public Integer sumber_dana;
        public String asal_dana;
        public Long asal_dana_satker;
        public String dana_apbd;
        public Long id_komponen;
        public Long id_kegiatan;
        public String mak;
        public Double pagu;
//        public Integer tahun_anggaran_dana;// jika multiple
        public Long id_tp;
        public Date auditupdate;
        public Long pkt_id_client;

        public models.common.SumberDana getSumberDana() {
            if(sumber_dana != null){
                return SumberDana.values()[sumber_dana - 1];
            }
            return null;
        }

        public boolean dataLengkap(){
            return getSumberDana() != null && mak != null && !mak.isEmpty() && pagu != null;
        }
    }

    public boolean paketLengkap(){
        return paketAnggaranLengkap();
    }

    public boolean paketAnggaranLengkap() {
        if(CollectionUtils.isEmpty(paket_anggaran_json))
            return false;
        for (PaketSwakelolaSirup.PaketSirupAnggaran rup_anggaran : paket_anggaran_json) {
            if(!rup_anggaran.dataLengkap()){
                return false;
            }
        }
        return true;
    }

    public String tahunAnggaran() {
        if(CollectionUtils.isEmpty(paket_anggaran_json))
            return "";
        StringBuilder result = new StringBuilder();
        for (PaketSwakelolaSirup.PaketSirupAnggaran rup_anggaran : paket_anggaran_json) {
//            if(rup_anggaran.tahun_anggaran_dana == null)
                continue;

//            if(result.length() > 0);
//                result.append(",");
//            result.append(rup_anggaran.asal_dana);
        }

        return result.toString().isEmpty() ? String.valueOf(tahun) : result.toString();
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String getNama() {
        return this.nama;
    }

    @Override
    public String getSumberDanaString(){
        if(CollectionUtils.isEmpty(paket_anggaran_json))
            return "";
        String result = paket_anggaran_json.stream().
                map(item -> item.getSumberDana().getLabel()).
                sorted().distinct().
                collect(Collectors.joining(", "));
        return result;
    }

    public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[5];
            tmp[0] = rs.getString("id");
            tmp[1] = rs.getString("nama");
            tmp[3] = FormatUtils.formatCurrenciesJutaRupiah(rs.getDouble("jumlah_pagu"));
            tmp[4] = rs.getString("tahun");
            if(rs.getInt("sumberdana") > 0)
                tmp[2] = SumberDana.values()[rs.getInt("sumberdana") - 1].getLabel();
//            tmp[3] = TipeSwakelola.findById(rs.getInt("tipe_swakelola")).label;
            return tmp;
        }
    };

    static Gson gson = new GsonBuilder().registerTypeAdapter(PaketSwakelolaSirup.class, new PaketSwakelolaSirupDeserializer()).create();    
    static Type type = new TypeToken<ArrayList<PaketSwakelolaSirup>>(){}.getType();

    public static void updatePaketFromSirup(final String rup_stk_id, final Integer tahun)  {
        if(StringUtils.isEmpty(rup_stk_id) || tahun == null)
            return;
        Logger.debug("get data paket swakelola sirup stk_id %s tahun %s", rup_stk_id, tahun);
        String url = BasicCtr.SIRUP_URL + "/service2/paketSwakelolaPerSatkerTampilAllStatus?idSatker=" + rup_stk_id + "&tahunAnggaran=" + tahun;
        try {
            String response = WS.url(url).getAsync().get().getString();
            if(!StringUtils.isEmpty(response)){
                List<PaketSwakelolaSirup> list = gson.fromJson(response, type);
                list = list.stream().filter(item -> item.aktif && item.umumkan).collect(Collectors.toList());
                simpan(list, rup_stk_id, tahun);
            }
        }catch (Exception e) {
            Logger.error(e,"Terjadi Kendala Akses ke Sirup");
        }
    }
}
