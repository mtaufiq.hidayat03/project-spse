package models.form;

import models.agency.Agency;
import models.common.Active_user;
import play.data.validation.Valid;
import play.mvc.Scope;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FormAgency {

    @Valid
    public Agency agency;
    public Agency agencyParent;
    public List<FormSelectAgency> adminAgencies;
    public List<Long> adminId;
    public List<String> noSk;

    public FormAgency(){
        this.agency = new Agency();

        this.adminAgencies = new ArrayList<>(1);
        this.adminAgencies.add(new FormSelectAgency());
    }

    public boolean isAdminAgenciesFilled(){
        FormSelectAgency admin = adminAgencies.get(0);
        if(admin.adminId == null || admin.noSk == null){
            return false;
        }

        return adminAgencies.stream().allMatch(o -> !o.noSk.isEmpty() && o.adminId != null);
    }

    public void convert(Scope.Flash flash, Active_user user) throws ParseException{
        agency = new Agency();
        agency.agc_id = flash.contains("form.agency.agc_id") && !flash.get("form.agency.agc_id").isEmpty() ?
                Long.parseLong(flash.get("form.agency.agc_id")) : null;
        agency.agc_nama = flash.get("form.agency.agc_nama");
        agency.agc_alamat = flash.get("form.agency.agc_alamat");
        agency.agc_website = flash.get("form.agency.agc_website");
        agency.jna_id = flash.get("form.agency.jna_id").isEmpty() ? null : Long.parseLong(flash.get("form.agency.jna_id"));
        agency.agc_telepon = flash.get("form.agency.agc_telepon");
        agency.kbp_id = flash.get("form.agency.kbp_id").isEmpty() ? null : Long.parseLong(flash.get("form.agency.kbp_id"));
        agency.agc_fax = flash.get("form.agency.agc_fax");
        agency.propinsiId = flash.get("form.agency.propinsiId").isEmpty() ? null : Long.parseLong(flash.get("form.agency.propinsiId"));
        agency.agc_tgl_daftar = flash.get("form.agency.agc_tgl_daftar").isEmpty() ? null :
                new SimpleDateFormat("dd-MM-yyyy").parse(flash.get("form.agency.agc_tgl_daftar"));

        if(user.isAdminAgency()){
            agencyParent = flash.get("form.agency.sub_agc_id") == null || flash.get("form.agency.sub_agc_id").isEmpty() ?
                    Agency.findAgencyByPenanggungJawab(user.userid) :
                    Agency.findById(Long.parseLong(flash.get("form.agency.sub_agc_id")));
        }

        int i = 0;
        adminAgencies = new ArrayList<>();
        if(flash.get("form.adminAgencies["+i+"].adminId") != null){
            adminAgencies = new ArrayList<>(1);
            adminAgencies.add(new FormSelectAgency());
        }

        while (flash.get("form.adminAgencies["+i+"].adminId") != null){
            FormSelectAgency admin = new FormSelectAgency();
            admin.adminId = flash.get("form.adminAgencies["+i+"].adminId").isEmpty() ? null : Long.parseLong(flash.get("form.adminAgencies["+i+"].adminId"));
            admin.noSk = flash.get("form.adminAgencies["+i+"].noSk");

            adminAgencies.add(admin);
            i++;
        }
    }

}
