package models.form;

import models.agency.Admin_agency;
import play.data.validation.Required;

import java.util.ArrayList;
import java.util.List;

public class FormSelectAgency {

    @Required
    public Long adminId;
    @Required
    public String noSk;


    public static List<FormSelectAgency> findByAgency(Long agcId){
        List<Admin_agency> adminAgencies = Admin_agency.find("agc_id=?", agcId).fetch();

        List<FormSelectAgency> result = new ArrayList<>();

        for(Admin_agency adminAgency : adminAgencies){
            FormSelectAgency formSelectAgency = new FormSelectAgency();
            formSelectAgency.adminId = adminAgency.peg_id;
            formSelectAgency.noSk = adminAgency.ada_nomor_sk;

            result.add(formSelectAgency);
        }

        return result;
    }

}
