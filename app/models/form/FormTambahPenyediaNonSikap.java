package models.form;

import play.data.validation.Email;
import play.data.validation.Phone;
import play.data.validation.Required;

public class FormTambahPenyediaNonSikap {

    @Required
    public String nama;


    public String npwp;


    public String alamat;


    @Email
    public String email;

    @Phone
    public String telp;
}
