package models.rekanan;

import models.common.Status_kepemilikan;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.validation.MaxSize;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Table(name="PERALATAN")
public class Peralatan extends BaseModel {

	@Id(sequence="seq_peralatan", function="nextsequence")
	public Long alt_id;
    @Required
	public String alt_jenis;
	@Required
	public Integer alt_jumlah;

	public String alt_kapasitas;

	public String alt_merktipe;
	@Required
	@Phone
	@MaxSize(4)
	public String alt_thpembuatan;

	public Integer alt_kondisi;

	public String alt_lokasi;

	public String alt_kepemilikan;

	//relasi ke Rekanan
	public Long rkn_id;
	
	public Status_kepemilikan skp_id;
	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	@Transient 
	public String getKondisi()
    {
    	if(alt_kondisi != null && alt_kondisi.intValue() == 1)
    		return "Baik";
    	else
    		return "Rusak";
    }
	
	/**
	 * Fungsi {@code findSemuaByNamaUser} digunakan untuk mendapatkan semua data
	 * peralatan rekanan tertentu berdasarkan nama user rekanan
	 *
	 * @param username user id rekanan
	 * @return semua daftar peralatan rekanan dengan user id sesuai dengan
	 *         parameter {@code username}
	 */
	public static List<Peralatan> findSemuaByNamaUser(String username) {
		Rekanan rekanan = Rekanan.findByNamaUser(username);
		return find("rkn_id = ?", rekanan.rkn_id).fetch();
	}
	
	public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[9];
			int kondisi = rs.getInt("alt_kondisi");
			obj[0] = rs.getString("alt_id");
			obj[1] = rs.getString("alt_jenis");
			obj[2] = rs.getString("alt_jumlah");
			obj[3] = rs.getString("alt_kapasitas");
			obj[4] = rs.getString("alt_merktipe");
			obj[5] = kondisi == 1 ? Messages.get("ct.baik") :Messages.get("ct.rusak");
			obj[6] = rs.getString("alt_thpembuatan");
			obj[7] = rs.getString("alt_lokasi");
			obj[8] = rs.getString("alt_kepemilikan");
			return obj;
		}
	};
	
}
