package models.rekanan;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;

@Table(name="NERACA")
public class Neraca extends BaseModel {

	@Id(sequence="seq_neraca", function="nextsequence")
	public String nrc_id;

	public String nrc_nama;

	public Date nrc_tanggal;

	//relasi ke rekanan
	public Long rkn_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
}
