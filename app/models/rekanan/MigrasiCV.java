package models.rekanan;

import models.jcommon.util.CommonUtil;
import models.lelang.Cv_staf_psr;
import org.sql2o.Query;
import org.sql2o.ResultSetIterable;
import org.sql2o.Sql2o;
import play.db.DB;
import play.mvc.Http;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Arief Ardiyansah
 * Tujuan class ini dibuat :
 * Utility buat handle migrasi data dari cv_staf ke dalam field sta_cv
 */
public class MigrasiCV {

    static final int BATCH_SIZE = 100000;
    static final Map<Long, String> data = new HashMap<>(BATCH_SIZE);

    public static void update() {
        Http.Response.current().writeChunk("migrasi cv staf ahli...\n");
        Sql2o sql2o = new Sql2o(DB.getDataSource());
        try (org.sql2o.Connection conn = sql2o.open()) {
            Query query = conn.createQuery("SELECT sta_id, dts_uraian, dts_waktu, dts_kategori FROM cv_staf WHERE sta_id in (SELECT sta_id FROM staf_ahli WHERE sta_cv is null) ORDER BY sta_id");
            Query query2 = conn.createQuery("UPDATE staf_ahli SET sta_cv=:data WHERE sta_id=:id");
            try (ResultSetIterable<Staf_ahli.Cv_staf> tables = query.executeAndFetchLazy(Staf_ahli.Cv_staf.class)) {
                String json = null;
                for (Staf_ahli.Cv_staf obj : tables) {
                    if (data.containsKey(obj.sta_id)) {
                        json = data.get(obj.sta_id);
                        json += ",";
                        json += CommonUtil.toJson(obj);
                        data.put(obj.sta_id, json);
                    } else {
                        data.put(obj.sta_id, CommonUtil.toJson(obj));
                    }
                    if (data.size() == BATCH_SIZE) {
                        updateBatch(query2, true);
                    }
                }
                if (!data.isEmpty()) {
                    updateBatch(query2, true);
                }
            }

            // update cv peserta
            Http.Response.current().writeChunk("migrasi cv staf ahli peserta....\n");
            query = conn.createQuery("SELECT stp_id, dts_uraian, dts_waktu, dts_kategori FROM cv_staf_psr WHERE stp_id IN (SELECT stp_id FROM staf_ahli_psr WHERE sta_cv is null) ORDER BY stp_id");
            query2 = conn.createQuery("UPDATE staf_ahli_psr SET sta_cv=:data WHERE stp_id=:id");
            try (ResultSetIterable<Cv_staf_psr> tables = query.executeAndFetchLazy(Cv_staf_psr.class)) {
                String json = null;
                for (Cv_staf_psr obj : tables) {
                    if (data.containsKey(obj.stp_id)) {
                        json = data.get(obj.stp_id);
                        json += ",";
                        json += CommonUtil.toJson(obj);
                        data.put(obj.stp_id, json);
                    } else {
                        data.put(obj.stp_id, CommonUtil.toJson(obj));
                    }
                    if (data.size() == BATCH_SIZE) {
                        updateBatch(query2, true);
                    }
                }
                if (!data.isEmpty()) {
                    updateBatch(query2, true);
                }
            }
        }
    }

    static void updateBatch(Query query, boolean array) {
        String value = null;
        for (Map.Entry<Long, String> entry : data.entrySet()) {
            if (array)
                value = '[' + entry.getValue() + ']';
            else
                value = entry.getValue();
            query.addParameter("id", entry.getKey()).addParameter("data", value).addToBatch();
        }
        query.executeBatch();
        data.clear();
    }
}
