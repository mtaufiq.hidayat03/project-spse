package models.rekanan;

import ext.DateBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


@Table(name="LANDASAN_HUKUM")
public class Landasan_hukum extends BaseModel {

	@Id(sequence="seq_landasan_hukum", function="nextsequence")
	public Long lhk_id;
	
    @Required
	public String lhk_no;
	
	@Required
    @As(binder=DateBinder.class)
    public Date lhk_tanggal;
    
    @Required
	public String lhk_notaris;

	public Long lhk_id_attachment;

	//relasi ke rekanan
	public Long rkn_id;
	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	@Transient
	public List<BlobTable> getDokumen() {
		if(lhk_id_attachment == null)
			return null;
		return BlobTableDao.listById(lhk_id_attachment);
	}
	
	public static List<Landasan_hukum> findBy(Long rekananId) {
		return find("rkn_id=? order by lhk_tanggal", rekananId).fetch();
	}
	
	public static Landasan_hukum findByRekanan(Long rekananId){
		return find("rkn_id=? order by lhk_tanggal asc",rekananId).first();
	}
	public static void simpan(Landasan_hukum akta,Landasan_hukum akta_akhir, Long rekananId) throws Exception {
		List<Landasan_hukum> landasan_hukumList = Landasan_hukum.find("rkn_id = ? order by lhk_tanggal asc ", rekananId).fetch();
    	if(CommonUtil.isEmpty(landasan_hukumList)) {    
    		 akta.rkn_id = rekananId;
    		 akta_akhir.rkn_id = rekananId;
    		 akta.save();
    		 akta_akhir.save();
    	}  else {
    		Landasan_hukum obj_akta = landasan_hukumList.get(0);
    		Landasan_hukum obj_akta_akhir = null;
	        if(landasan_hukumList.size() > 1)
	        	obj_akta_akhir = landasan_hukumList.get(1);
	        obj_akta.lhk_no = akta.lhk_no;
	        obj_akta.lhk_tanggal = akta.lhk_tanggal;
	        obj_akta.lhk_notaris = akta.lhk_notaris;
	        obj_akta.rkn_id = rekananId;
	        obj_akta.save();
	        if(obj_akta_akhir == null)
	        	obj_akta_akhir = new Landasan_hukum();
	        obj_akta_akhir.lhk_no = akta_akhir.lhk_no;
	        obj_akta_akhir.lhk_tanggal = akta_akhir.lhk_tanggal;
	        obj_akta_akhir.lhk_notaris = akta_akhir.lhk_notaris;
	        obj_akta_akhir.rkn_id = rekananId;
	        obj_akta_akhir.save();
    	}
	}   
}
