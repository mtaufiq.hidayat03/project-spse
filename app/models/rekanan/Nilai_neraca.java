package models.rekanan;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;


@Table(name="NILAI_NERACA")
public class Nilai_neraca extends BaseModel {

	@Id
	public String nrc_id_item;

	//relasi ke Neraca
	@Id
	public String nrc_id;
	
	public Double nilai;
	
}

