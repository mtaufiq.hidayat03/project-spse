package models.rekanan;

import ext.DecimalBinder;
import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;


@Table(name="PEMILIK")
public class Pemilik extends BaseModel {

    public static final Integer SATUAN_LEMBAR = Integer.valueOf(0);
    public static final Integer SATUAN_PROSENTASE = Integer.valueOf(1);

	@Id(sequence="seq_pemilik", function="nextsequence")
	public Long pml_id;
	
    @Required
	public String pml_nama;
    @Required
	public String pml_ktp;
    @Required
	public String pml_alamat;

    @Required
    @As(binder = DecimalBinder.class)
	public Double pml_saham;

	public Date pml_valid_start;

	public Date pml_valid_end;
	// field baru 4.1.2
	public String pml_npwp;
	
	@Required
	public Integer pml_satuan;

	//relasi ke Rekanan
	public Long rkn_id;

	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;

    public boolean isLembar() {
        return pml_satuan != null && pml_satuan.equals(SATUAN_LEMBAR);
    }

    public boolean isProsentase() {
        return pml_satuan != null && pml_satuan.equals(SATUAN_PROSENTASE);
    }
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	public static Long getTotalSaham(Long rekananId) {
        return Query.find("select sum(pml_saham) from Pemilik where rkn_id=?", Long.class, rekananId).first();
    }
	
	 public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

			@Override
			public String[] handle(ResultSet rs) throws SQLException {
				String[] obj = new String[5];
				int satuan = rs.getInt("pml_satuan");
				obj[0] = rs.getString("pml_id");
				obj[1] = rs.getString("pml_nama");
				obj[2] = rs.getString("pml_ktp");
				obj[3] = rs.getString("pml_alamat");
				if(satuan == 0)
					obj[4] = FormatUtils.formatNumber(rs.getLong("pml_saham")) + Messages.get("ct.lembar");
				else if(satuan == 1)
					obj[4] = rs.getString("pml_saham") + " % ";
				return obj;
			}
		};
}
