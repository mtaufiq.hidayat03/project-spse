package models.rekanan;

import ams.contracts.AmsUserContract;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import controllers.UserCtr.LoginResult;
import ext.FormatUtils;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.nonlelang.DraftPesertaPl;
import models.secman.Group;
import models.secman.LupaPasswordToken;
import models.secman.Usergroup;
import models.sso.common.Aktivasi;
import models.sso.common.Kabupaten;
import models.sso.common.RekananSSO;
import models.sso.common.Repository;
import models.sso.common.adp.StatusAktivasi;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.Play;
import play.data.validation.*;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;
import play.db.jdbc.Table;
import play.libs.Json;
import play.libs.URLs;
import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Scope;
import utils.HtmlUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.MessagingException;
import javax.persistence.Transient;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Table(name="REKANAN")
public class Rekanan extends BaseModel implements AmsUserContract {
	
	public static int REKANAN_BARU = 0;

	public static int REKANAN_DISETUJUI = 1;

	public static int REKANAN_DITOLAK = -2;

	public static int REKANAN_EXPIRED = -3;
	
	public static final int MIGRASI_SIKAP_BELUM = 0;
	public static final int MIGRASI_SIKAP_PROSES = 1;
	public static final int MIGRASI_SIKAP_SELESAI = 2;
	
	public static final String PUSAT = "P";
    public static final String CABANG = "C";

	@Id(sequence="seq_rekanan", function="nextsequence")	
	public Long rkn_id;
	
    @Required
	public String rkn_nama;
    @Required
	public String rkn_alamat;
	@Match(message = "rekanan.rkn_kodepos", value = "[0-9]{5}")
	public String rkn_kodepos;
    @Required
    @Phone
	public String rkn_telepon;
    @Phone
	public String rkn_fax;
    @Phone
	public String rkn_mobile_phone;
    
    @Required
    @MaxSize(20)
	public String rkn_npwp;

	public String rkn_pkp;

	public String rkn_statcabang;
	
	@Required
	@Email
	public String rkn_email;
	
	@URL
	public String rkn_website;

	public Date rkn_tgl_daftar;

	public Date rkn_tgl_setuju;

	public String rkn_almtpusat;
	@Phone
	public String rkn_telppusat;
	@Phone
	public String rkn_faxpusat;
	@Email
	public String rkn_emailpusat;
	
	@Required
	@Match(message = "validation.match.userid", value = "[a-zA-Z0-9.]{3,80}")
	public String rkn_namauser;

	public Integer rkn_isactive;

	public Integer rkn_status = 0;

	public String rkn_keterangan;

	public String rkn_status_verifikasi;

	public String ver_namauser;
	//relasi ke Bentuk_usaha
	@Required
	public String btu_id;
	//relasi ke Kabupaten

	public Long kbp_id;
	//relasi ke Ppe_site
	public String pps_id;
	//relasi ke Repository
	public Long repo_id;
	// OSD
	public Long cer_id;
	// integrasi dengan SIKaP
	public Integer status_migrasi = MIGRASI_SIKAP_BELUM;
	public Date last_sync_vms;
	
	@Required
	@MinSize(6)
	public String passw;

	public Integer isbuiltin = 0;

	public Integer disableuser;

	public String ams_id;

	// JSON: field ini berisi data rekanan yang diedit oleh penyedia sebelum diverifikasi oleh verifikator
	// sekaligus sebagai flag apakah rekanan ini perlu diverifikasi oleh verifikator
	// if not null/not empty then muncul di halaman verifikator
	// sehingga setelah diverifikasi, harus dinullkan kembali
	public String edited_data;

	public String kota;

	//ref to Negara
	public Long ngr_id;
	
	@Transient
	private List<Ijin_usaha> ijinUsahaList;	
	@Transient
	private List<Pajak> pajakList;
	@Transient	
	private List<Staf_ahli> stafAhliList;
	@Transient
	private List<Pengalaman> pengalamanList;
	@Transient
	private List<Peralatan> peralatanList;	
	@Transient
	private Kabupaten kabupaten;
	@Transient
	private Bentuk_usaha bentuk_usaha;
	@Transient
	private Repository repository;

	// deserialization dari field edited_data
	@Transient
	private Rekanan editedRekananData;

	@Transient
	private Negara negara;
	
	/**Saat user minta reset password, sistem mengirimkan 'signature' via email
	 * dan disimpan di kolom ini. 
	 * Signature berisi UUID dan tanggal pembuatannya. 
	 */
	public String reset_password;
	
	public Kabupaten getKabupaten() {
		if(kabupaten == null)
			kabupaten = Kabupaten.findById(kbp_id);
		return kabupaten;
	}	

	public Bentuk_usaha getBentuk_usaha() {
		if(bentuk_usaha == null)
			bentuk_usaha = Bentuk_usaha.findById(btu_id);
		return bentuk_usaha;
	}
	
	public Repository getRepository() {
		if(repository == null)
			repository = Repository.findById(repo_id);
		return repository;
	}
	
	
	public List<Ijin_usaha> getIjinUsahaList() {
		if(ijinUsahaList == null)
			ijinUsahaList = Ijin_usaha.find("rkn_id=?", rkn_id).fetch();
		return ijinUsahaList;
	}

	public List<Pajak> getPajakList() {
		if(pajakList == null)
			pajakList = Pajak.find("rkn_id=?", rkn_id).fetch();
		return pajakList;
	}

	public List<Staf_ahli> getStafAhliList() {
		if(stafAhliList == null)
			stafAhliList = Staf_ahli.find("rkn_id=?", rkn_id).fetch();
		return stafAhliList;
	}

	public List<Pengalaman> getPengalamanList() {
		if(pengalamanList == null)
			pengalamanList = Pengalaman.find("rkn_id=?", rkn_id).fetch();
		return pengalamanList;
	}

	public List<Peralatan> getPeralatanList() {
		if(peralatanList == null)
			peralatanList = Peralatan.find("rkn_id=?", rkn_id).fetch();
		return peralatanList;
	}

	public Negara getNegara() {
		if(negara == null)
			negara = Negara.findById(ngr_id);
		return negara;
	}

	public String getTglDaftar() {
		return FormatUtils.formatDateInd(rkn_tgl_daftar);
	}	
	
	public boolean isDisableUser() {
		return disableuser != null && disableuser != 0;
	}

	/**
	 * Fungsi {@code findPegawaiByNamaUser} digunakan untuk mendapatkan sebuah
	 * objek rekanan sesuai dengan <i>username</i>-nya
	 *
	 * @param nama_user username rekanan
	 * @return objek rekanan bersesuaian dengan username
	 */
	public static Rekanan findByNamaUser(String nama_user) {
		return  find("rkn_namauser = ?", nama_user.toUpperCase()).first();
	}
	
	public static String findEmail(Long rekananId) {
		return Query.find("select rkn_email from rekanan where rkn_id=?", String.class, rekananId).first();
	}
	
	public static Rekanan findByEmail(String email) {
		return  find("rkn_email = ?", email).first();
	}
	
	/**
	 * Identitas Digital Penyedia
	 *
	 * @param rekananId id rekanan
	 * @return string identitas digital penyedia
	 * @author arief
	 */
	public static String getDigIdentity(Long rekananId) {
		Rekanan rekanan = findById(rekananId);
		String limiter = "#=#";
		StringBuilder digIdentity = new StringBuilder();
		String cipDigIdentity = "";
		if (rekanan != null) {
			digIdentity = new StringBuilder(rekananId.toString() + limiter +
					rekanan.rkn_nama + limiter +
					rekanan.rkn_alamat + limiter +
					rekanan.getKabupaten().kbp_nama + limiter +
					rekanan.getKabupaten().getPropinsi().prp_nama + limiter +
					rekanan.rkn_telepon + limiter +
					rekanan.rkn_email + limiter +
					rekanan.rkn_npwp + limiter + '0' + limiter);

			int length = digIdentity.length();
			for (int i = 0; i < 16 - (length % 16); i++) {
				digIdentity.append('|');
			}

			byte[] key1 = DigestUtils.md5(rekanan.rkn_namauser);
			byte[] key2 = DigestUtils.md5(key1);
			byte[] key3 = new byte[key1.length + key2.length];
			for (int i = 0; i < key1.length; i++) {
				key3[i] = key1[i];
				key3[i + key1.length] = key2[i];
			}

			byte[] iv = new byte[16];
			for (int i = 0; i < iv.length; i++) {
				iv[i] = '0';
			}

			SecretKeySpec skeySpec = new SecretKeySpec(key3, 0, 32, "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(iv);
			Cipher c;
			try {
				c = Cipher.getInstance("AES/CBC/NoPadding");
				c.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
				byte[] encrypted = c.doFinal(digIdentity.toString().getBytes());

//				cipDigIdentity = Base64.encodeToString(encrypted);
				cipDigIdentity = Base64.encodeBase64String(encrypted);

			} catch (NoSuchAlgorithmException e) {
				Logger.error(e.getMessage());
			} catch (NoSuchPaddingException e) {
				Logger.error(e.getMessage());
			} catch (InvalidKeyException e) {
				Logger.error(e.getMessage());
			} catch (InvalidAlgorithmParameterException e) {
				Logger.error(e.getMessage());
			} catch (IllegalBlockSizeException e) {
				Logger.error(e.getMessage());
			} catch (BadPaddingException e) {
				Logger.error(e.getMessage());
			}

		}
		return HtmlUtil.textAreaView(cipDigIdentity, 45);
	}
    
    /**
	 * mendapatkan informasi Jumlah Penyedia Baru
	 * @author arief
	 */
    public static long getJumlahRekananBaru()
    {
    	return count("rkn_status = ?", Rekanan.REKANAN_BARU);
    }
    
    /**
   	 * mendapatkan informasi Jumlah Penyedia Ditolak
   	 * @author arief
   	 */
    public static long getJumlahRekananDitolak()
    {
    	return count("rkn_status = ?", Rekanan.REKANAN_DITOLAK);
    }
    
    /**
   	 * mendapatkan informasi Jumlah Penyedia Terverifikasi
   	 * @author arief
   	 */
    public static long getJumlahRekananTerverifikasi()
    {
    	return count("rkn_status = ? and repo_id=?", Rekanan.REKANAN_DISETUJUI, ConfigurationDao.getRepoId());
    }
    
    /**
   	 * mendapatkan informasi Jumlah Penyedia Roaming
   	 * @author arief
   	 */
    public static long getJumlahRekananRoaming()
    {
    	return count("repo_id <> ?", ConfigurationDao.getRepoId());
    }
    
    public static Rekanan findByPeserta(Long pesertaId) {
    	return Query.find("select r.* from rekanan r, peserta p where p.rkn_id=r.rkn_id and p.psr_id=?", Rekanan.class, pesertaId).first();
    }

	public static Rekanan findByPesertaPl(Long pesertaId) {
		return Query.find("select r.* from rekanan r, ekontrak.peserta_nonlelang p where p.rkn_id=r.rkn_id and p.psr_id=?", Rekanan.class, pesertaId).first();
	}
    
    public static final ResultSetHandler<String[]> resultsetRekanan = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[13];
			tmp[0] = rs.getString("rkn_id");
			tmp[1] = rs.getString("rkn_nama");
			tmp[2] = rs.getString("rkn_telepon");
			tmp[3] = rs.getString("rkn_alamat");
			tmp[4] = rs.getString("rkn_npwp");
			tmp[5] = rs.getString("rkn_email");
			tmp[6] = FormatUtils.formatDateInd(rs.getTimestamp("rkn_tgl_daftar"));
			tmp[7] = rs.getString("disableuser");
			tmp[8] = rs.getString("bll_id");
			tmp[9] = rs.getString("rkn_isactive");
			tmp[10] = FormatUtils.formatDateTimeSecond(rs.getTimestamp("bll_tglakhir"));
			try {
				tmp[11] = rs.getString("cer_id");
			} catch (Exception e) {
				tmp[11] = "";
			}
			tmp[12] = Bentuk_usaha.findById(rs.getString("btu_id")).label;
			return tmp;
		}
	};

    public static final ResultSetHandler<String[]> resultsetInbox = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
			tmp[0] = rs.getString("id");
			String mtd = rs.getString("l_mtd");
			String mtdLabel = "";
			if (mtd == null) {
				mtd = rs.getString("nl_mtd");
				if(mtd != null) {
					int pkt_flag = rs.getInt("pl_flag");
					mtdLabel = pkt_flag == 3 ? MetodePemilihanPenyedia.findById(Integer.parseInt(mtd)).getLabel(): MetodePemilihan.findById(Integer.parseInt(mtd)).getLabel();
				}
			} else {
				mtdLabel = MetodePemilihan.findById(Integer.parseInt(mtd)).getLabel();
			}
			Long lelangId = rs.getLong("lls_id");
			if(rs.getInt("bcstat")==1) {
				tmp[2] = "<b>" + rs.getString("subject") + "</b>";
				tmp[1] = "<b>" + FormatUtils.formatDateTimeInd(rs.getTimestamp("enqueue_date")) + "</b>";
				tmp[3] = "<b>" + (lelangId == null || lelangId == 0L ? "" : lelangId.toString())+ "</b>";
				if(!StringUtils.isEmpty(mtd))
					tmp[4] = "<b>" + mtdLabel + "</b>";
			} else {
				tmp[2] = rs.getString("subject");
				tmp[1] = FormatUtils.formatDateTimeInd(rs.getTimestamp("enqueue_date"));
				tmp[3] = (lelangId == null || lelangId == 0L)  ? "" : lelangId.toString();
				if(!StringUtils.isEmpty(mtd))
					tmp[4] = mtdLabel;
			}

			return tmp;
		}
	};
    
 // ***************** TRANSAKSI TERKAIT LOGIN ADP ***************************//
	
 	public static String modifUsernameADP(String username){
 		return "||"+username;
 	}

 	public static String clearUsernameADP(String usernameADP){
 		if(StringUtils.isEmpty(usernameADP))
 			return usernameADP;
 		if(usernameADP.contains("||"))
 			return usernameADP.replace("||", "");
 		else if (usernameADP.contains("ï¿½ï¿½ï¿½"))
 			return usernameADP.replace("ï¿½ï¿½ï¿½", "");
 		else 
 			return usernameADP.replace("ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½", "");
 	}
    
    public static void insertUserRekananFromWs(RekananSSO userRekanan){	
    	
		// check username ganda*
//		String userid = userRekanan.rkn_namauser;
//		Rekanan cekUser = findByNamaUser(userid);
//		if(cekUser != null)
//			userid = modifUsernameADP(userid);
//		userRekanan.rkn_namauser = userid;
		
		// check email ganda*
		deleteRekananEmailGanda(userRekanan);
		Rekanan cekUserEmail = findByEmail(userRekanan.rkn_email);
		if(cekUserEmail != null){
			// kirim email konfirmasi gagal update*
			try{
				sendKonfirmasiADPGagalUpdateEmail(userRekanan);
			}
			catch (MessagingException e) {
				Logger.error(e, "Kirim email gagal");
			}
//			userRekanan.rkn_email = "x_"+userRekanan.rkn_email;
		}
		userRekanan.rkn_namauser = clearUsernameADP(userRekanan.rkn_namauser);
		// insert anyway*
		insertADP(userRekanan);
	}
    
    private static void deleteRekananEmailGanda(RekananSSO userRekanan){
		
		//cek email ganda*
		Rekanan cekRek = null;
		List<Rekanan> listCekRek = find("rkn_email=?", userRekanan.rkn_email).fetch();
		if(listCekRek != null && listCekRek.size() > 0) cekRek = listCekRek.get(0);
		
		if(cekRek != null && cekRek.rkn_isactive == 0 && (cekRek.rkn_status == Rekanan.REKANAN_BARU || cekRek.rkn_status == Rekanan.REKANAN_DITOLAK)){ // ada dan belum disetujui*
			// kirim email konfirmasi *
			try{
				sendKonfirmasiADPHapusAkunToEmail(cekRek, userRekanan);
			}
			catch (MessagingException e) {
				Logger.error(e, "Kendala kirim Email konfirmasi ");
			}
			if(cekRek.rkn_namauser != null){
				// 	Delete user SECMAN & rekanan EPNS*
				Usergroup usergroupDel = new Usergroup();
				usergroupDel.systemid = "EPNS";
				usergroupDel.idgroup = Group.REKANAN;
				usergroupDel.userid = cekRek.rkn_namauser;
				usergroupDel.delete();								
			}
			
			cekRek.delete();			
		}
	}
    
    public static void updateUserRekananFromWs(RekananSSO userRekanan){
		Rekanan oldUser = findById(userRekanan.rkn_id);
		
		// check email ganda*
		deleteRekananEmailGanda(userRekanan);
		List<Rekanan> rekananList = find("rkn_email = ? AND rkn_id != ?",  userRekanan.rkn_email, oldUser.rkn_id).fetch();
		if(!rekananList.isEmpty()){
			// kirim email konfirmasi *
			try{
				sendKonfirmasiADPGagalUpdateEmail(userRekanan);
			}
			catch (MessagingException e) {
				Logger.error(e, "Kendala kirim email konfirmasi");
			}
//			userRekanan.rkn_email = oldUser.rkn_email;
		}
				
		// check username ganda*
//		String userid = oldUser.rkn_namauser;
//		if(!userRekanan.rkn_namauser.equals(userid)){
//			Rekanan cekUser = findByNamaUser(userRekanan.rkn_namauser);
//			if(cekUser != null)
//				userid = modifUsernameADP(userid);
//		}
//		userRekanan.rkn_namauser = userid;
		userRekanan.rkn_namauser = clearUsernameADP(userRekanan.rkn_namauser);
		// update anyway*
		updateADP(userRekanan);
	}
    
    public static void sendKonfirmasiADPGagalUpdateEmail(RekananSSO rekananADP) throws MessagingException{
		
		Logger.info("gagal update email " + rekananADP.rkn_email);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("lokal_lpse", ConfigurationDao.getNamaRepo());
		param.put("namauser_adp", clearUsernameADP(rekananADP.rkn_namauser));
		param.put("email_adp", clearUsernameADP(rekananADP.rkn_email));
		param.put("adp_lpse", ConfigurationDao.getNamaRepo(rekananADP.repo_id));
		
//		emailManager.sendHTMLEmailWithTemplate("notifikasi-adp-gagal-hapus-akun.vm", param, new String[] { rekananADP.getRkn_email()}, "Konfirmasi gagal sinkronisasi Email",  new Integer(0), new Long(0), rekananADP.getRkn_id());
	}
    
    public static void sendKonfirmasiADPHapusAkunToEmail(Rekanan rekananHapus, RekananSSO rekananADP) throws MessagingException{
    	
		Logger.info("emailHapusRekanan ke=" + rekananHapus.rkn_email);
		
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("namauser_hapus", rekananHapus.rkn_namauser);
		param.put("lokal_lpse", ConfigurationDao.getNamaRepo());
		param.put("namauser_adp", clearUsernameADP(rekananADP.rkn_namauser));
		param.put("adp_lpse", ConfigurationDao.getNamaRepo(rekananADP.repo_id));
		
//		emailManager.sendHTMLEmailWithTemplate("notifikasi-adp-hapus-akun.vm", param, new String[] { rekananHapus.getRkn_email()}, "Konfirmasi Penghapusan Akun",  new Integer(0), new Long(0), rekananHapus.getRkn_id());
	}   
    
    public static void insertADP(RekananSSO userRekanan) {
		if(!StringUtils.isEmpty(userRekanan.btu_id) && userRekanan.btu_id.equals("10")) // penyedia yg bentuk usaha asing (btu_id=10) tidak boleh masuk ke ADP
			return;
		String sql="INSERT INTO REKANAN (rkn_id, btu_id, cer_id, kbp_id, audittype, audituser, auditupdate, rkn_nama, rkn_alamat, rkn_kodepos, rkn_telepon, rkn_fax, rkn_mobile_phone, rkn_npwp,rkn_pkp, rkn_statcabang, rkn_email, rkn_website, rkn_tgl_daftar, rkn_tgl_setuju, rkn_almtpusat, rkn_telppusat, rkn_faxpusat, rkn_emailpusat, rkn_namauser, rkn_isactive, rkn_status,rkn_keterangan, repo_id, status_migrasi, passw, disableuser, isbuiltin) "+
				   "VALUES (:rkn_id, :btu_id, :cer_id, :kbp_id, :audittype, :audituser, :auditupdate, :rkn_nama, :rkn_alamat, :rkn_kodepos, :rkn_telepon, :rkn_fax, :rkn_mobile_phone, :rkn_npwp,:rkn_pkp, :rkn_statcabang, :rkn_email, :rkn_website, :rkn_tgl_daftar, :rkn_tgl_setuju, :rkn_almtpusat, :rkn_telppusat, :rkn_faxpusat, :rkn_emailpusat, :rkn_namauser, :rkn_isactive, :rkn_status,:rkn_keterangan, :repo_id, 0, :passw, :disableuser, :isbuiltin)";
		Query.bindUpdate(sql, userRekanan);
	}
    
 // OSD
 // TODO rkn_namapusat belum ada di adp	
 	public static void updateADP(RekananSSO userRekanan) {
 		if(!StringUtils.isEmpty(userRekanan.btu_id) && userRekanan.btu_id.equals("10")) // penyedia yg bentuk usaha asing (btu_id=10) tidak boleh masuk ke ADP
 			return;
 		String sql = "UPDATE rekanan SET rkn_id=:rkn_id,btu_id=:btu_id,cer_id=:cer_id,kbp_id=:kbp_id,audittype=:audittype,audituser=:audituser,auditupdate=:auditupdate,rkn_nama=:rkn_nama,rkn_alamat=:rkn_alamat,rkn_kodepos=:rkn_kodepos,rkn_telepon=:rkn_telepon,rkn_fax=:rkn_fax,rkn_mobile_phone=:rkn_mobile_phone,rkn_npwp=:rkn_npwp,rkn_pkp=:rkn_pkp,rkn_statcabang=:rkn_statcabang,rkn_email=:rkn_email,rkn_website=:rkn_website,rkn_tgl_daftar=:rkn_tgl_daftar,rkn_tgl_setuju=:rkn_tgl_setuju,rkn_almtpusat=:rkn_almtpusat,rkn_telppusat=:rkn_telppusat," +
 							 "rkn_faxpusat=:rkn_faxpusat,rkn_emailpusat=:rkn_emailpusat,rkn_namauser=:rkn_namauser,rkn_isactive=:rkn_isactive,rkn_status=:rkn_status,rkn_keterangan=:rkn_keterangan,repo_id=:repo_id, passw=:passw, disableuser=:disableuser, isbuiltin=:isbuiltin WHERE rkn_id=:rkn_id";
 		Query.bindUpdate(sql, userRekanan);
 	}
 	
 	public static List<RekananSSO> getListRekananRepo(String filter) {
		QueryBuilder sql= new QueryBuilder("SELECT *, (select MAX(psr_tanggal) from peserta where rkn_id=rekanan.rkn_id) AS tgl_akhir_peserta,(select COUNT(psr_id) from peserta where rkn_id=rekanan.rkn_id) AS paket FROM rekanan "+
		           "WHERE rekanan.rkn_status = 1 AND rekanan.btu_id<> '10' AND rekanan.repo_id=? ", ConfigurationDao.getRepoId());
		if(!StringUtils.isEmpty(filter)) {
			sql.append(filter);
		}
		sql.append("ORDER BY rekanan.rkn_id");
		Logger.info(sql.query());
		return Query.find(sql, RekananSSO.class).fetch();
	}

	public static List<RekananSSO> getListRekananRepo() {
		QueryBuilder sql= new QueryBuilder("SELECT *, (select MAX(psr_tanggal) from peserta where rkn_id=rekanan.rkn_id) AS tgl_akhir_peserta,(select COUNT(psr_id) from peserta where rkn_id=rekanan.rkn_id) AS paket FROM rekanan "+
				"WHERE rekanan.rkn_status = 1 AND rekanan.btu_id<> '10' AND rekanan.repo_id=? ", ConfigurationDao.getRepoId());
		sql.append("ORDER BY rekanan.rkn_id");
		Logger.info(sql.query());
		return Query.find(sql, RekananSSO.class).fetch();
	}

	// jumlah paket/tender yg pernah diikuti
	public Long getJumlahPaket() {
 		return Query.count("SELECT count(psr_id) FROM peserta WHERE rkn_id=?", rkn_id);
	}

	// convert to RekananSSO untuk kebutuhan Data ADP
	public RekananSSO toRekananSSO() {
 		if(rkn_status.equals(REKANAN_DISETUJUI)) {
 			RekananSSO rekananSSO = new RekananSSO();
			rekananSSO.rkn_id = rkn_id;
			rekananSSO.btu_id = btu_id;
			rekananSSO.cer_id = cer_id;
			rekananSSO.kbp_id = kbp_id;
			rekananSSO.repo_id = repo_id;
			rekananSSO.rkn_nama = rkn_nama;
			rekananSSO.rkn_alamat= rkn_alamat;
			rekananSSO.rkn_kodepos = rkn_kodepos;
			rekananSSO.rkn_telepon = rkn_telepon;
			rekananSSO.rkn_fax = rkn_fax;
			rekananSSO.rkn_mobile_phone = rkn_mobile_phone;
			rekananSSO.rkn_npwp = rkn_npwp;
			rekananSSO.rkn_pkp = rkn_pkp;
			rekananSSO.rkn_statcabang = rkn_statcabang;
			rekananSSO.rkn_email=rkn_email;
			rekananSSO.rkn_website=rkn_website;
			rekananSSO.rkn_tgl_daftar=rkn_tgl_daftar;
			rekananSSO.rkn_tgl_setuju=rkn_tgl_setuju;
			rekananSSO.rkn_almtpusat=rkn_almtpusat;
			rekananSSO.rkn_telppusat=rkn_telppusat;
			rekananSSO.rkn_faxpusat=rkn_faxpusat;
			rekananSSO.rkn_emailpusat=rkn_emailpusat;
			rekananSSO.rkn_namauser=rkn_namauser;
			rekananSSO.rkn_isactive=rkn_isactive;
			rekananSSO.rkn_status=rkn_status;
			rekananSSO.rkn_keterangan=rkn_keterangan;
			rekananSSO.passw=passw;
			rekananSSO.isbuiltin=isbuiltin;
			rekananSSO.disableuser=disableuser;
			rekananSSO.paket = getJumlahPaket();
			rekananSSO.tgl_akhir_peserta = Query.find("SELECT MAX(psr_tanggal) FROM peserta WHERE rkn_id=?", Date.class, rkn_id).first();
			rekananSSO.audittype=audittype;
			rekananSSO.audituser=audituser;
			rekananSSO.auditupdate=auditupdate;
			return rekananSSO;
		}
		return null;
	}
 	
 	public static List<RekananSSO> getListDuplikasi(Rekanan rekanan){
 		List<RekananSSO> list = new ArrayList<RekananSSO>();
 		if(ConfigurationDao.isEnableInaproc()) { // jika ADP aktif check ke ADP Pusat
			String param = URLs.encodePart(DceSecurityV2.encrypt(rekanan.rkn_npwp));
			String url_service = ConfigurationDao.getRestCentralService()+"/rekanan-npwp?q="+param;
			try {
				WSRequest request = WS.url(url_service);
				request.setHeader("authentication", Play.secretKey);
				String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
				if(!StringUtils.isEmpty(content)) {
					 content = DceSecurityV2.decrypt(content);
					 list = CommonUtil.fromJson(content,  new TypeToken<List<RekananSSO>>(){}.getType());
				}
			} catch (Exception e) {
				Logger.error(e, "Memeriksa duplikasi data ke ADP gagal");
			}	
 		} 		
		
		List<Rekanan> rekananDuplikasi = find("rkn_npwp=? AND rkn_status IN (1,0)", rekanan.rkn_npwp).fetch();
		if (!CommonUtil.isEmpty(rekananDuplikasi)) {
			// Tambah list dari Lokal *
			for (Rekanan rek : rekananDuplikasi) {
				// Checking redundansi data*
				if (rek.rkn_id.equals(rekanan.rkn_id))
					continue;
				if (!CommonUtil.isEmpty(list)) {
					boolean exist = false;
					for(RekananSSO obj : list)
						if(obj.rkn_id.equals(rek.rkn_id)) {
							exist = true;
							break;
						}
					if(exist) {
						list.remove(rek);
						continue;
					}
				}
				// membuat object userRekanan dari lokal //
				RekananSSO userRekanan = new RekananSSO();
				userRekanan.repo_id = rek.repo_id;
				userRekanan.rkn_id = rek.rkn_id;
				userRekanan.rkn_nama = rek.rkn_nama;
				userRekanan.rkn_alamat = rek.rkn_alamat;
				userRekanan.rkn_kodepos = rek.rkn_kodepos;
				userRekanan.rkn_npwp = rek.rkn_npwp;
				userRekanan.rkn_pkp = rek.rkn_pkp;
				userRekanan.rkn_statcabang = rek.rkn_statcabang;
				userRekanan.rkn_email = rek.rkn_email;
				userRekanan.rkn_website = rek.rkn_website;
				userRekanan.rkn_tgl_daftar = rek.rkn_tgl_daftar;
				userRekanan.rkn_tgl_setuju = rek.rkn_tgl_setuju;
				userRekanan.rkn_almtpusat = rek.rkn_almtpusat;
				userRekanan.rkn_telppusat = rek.rkn_telppusat;
				userRekanan.rkn_faxpusat = rek.rkn_faxpusat;
				userRekanan.rkn_emailpusat = rek.rkn_emailpusat;
				userRekanan.rkn_namauser = rek.rkn_namauser;
				userRekanan.rkn_status = rek.rkn_status;
				list.add(userRekanan);
			}
		}
		return list;
	}
 	
 	public static List<Rekanan> getRekananByCertJenis(String certJenis) {
		if(certJenis == null)
			return null;
		return find("cer_id IN (SELECT cer_id FROM certificate WHERE cer_jenis=? AND cer_versi = LASTVERSIONCERTIFICATE(cer_id))", certJenis).fetch();
	}
 	
 	public boolean isBelumMigrasiSikap() {
 		return status_migrasi == null || status_migrasi == MIGRASI_SIKAP_BELUM;
 	}
 	
 	public boolean isSedangMigrasiSikap() {
 		return status_migrasi != null && status_migrasi == MIGRASI_SIKAP_PROSES;
 	}
 	
 	public boolean isSelesaiMigrasiSikap() {
 		return status_migrasi != null && status_migrasi == MIGRASI_SIKAP_SELESAI;
 	}

	
	public boolean isCertRoaming(){
		String repoId = String.valueOf(ConfigurationDao.getRepoId());
		String certRepoId = cer_id.toString().substring(cer_id.toString().length()-repoId.length());
		return !certRepoId.equalsIgnoreCase(repoId);
	}

	//sementara, menunggu service dari SIKaP
	public static List<Rekanan> getRekananByNpwp(String npwp) {

		return find("rkn_npwp = ?", npwp).fetch();
	}

	public boolean isSudahTerdaftarDiDraft(Long lelangId){

		DraftPesertaPl draftPesertaPl = DraftPesertaPl.findWithLelangAndRekanan(lelangId, rkn_id);

		return draftPesertaPl != null ? true : false;
	}

	public static Rekanan findByIdAndNpwp(Long rkn_id, String npwp) {
		Rekanan rekanan = find("rkn_id = ? AND rkn_npwp = ?", rkn_id, npwp).first();
		if (rekanan == null) {
			return null;
		} else {
			return rekanan;
		}
	}
	
	/**
	 * Fungsi {@code doLogin} digunakan untuk eksekusi login pengguna
	 *
	 * @param password   password pengguna (hashed)
	 * @param ip_address ip address pengguna
	 * @return enum object {@code LoginResult}
	 */
	public static LoginResult doLogin(String userId, String password, String ip_address) {		
		Rekanan user = findByNamaUser(userId);
		if (ConfigurationDao.isEnableInaproc() && (user == null || !user.isPerusahaanAsing())) { // otentikasi ADP jika agregasi aktif
			// Hanya mengambalikan rekanan Id yang ada di ADP inaproc dan yang tidak roaming atau roming tetapi sudah teraktivasi
			JsonObject paramObj = new JsonObject();
			paramObj.addProperty("username", userId);
			paramObj.addProperty("password", password);
			paramObj.addProperty("remoteAddress", ip_address);
			paramObj.addProperty("repoId", ConfigurationDao.getRepoId());
			String param = URLs.encodePart(DceSecurityV2.encrypt(Json.toJson(paramObj)));
			String url_service = ConfigurationDao.getRestCentralService() + "/otentikasi?q=" + param;
			Long rekananId = null;
			String response;
			try {
				HttpResponse httpResponse = WS.url(url_service).setHeader("authentication", Play.secretKey)
						.timeout(ConfigurationDao.CONNECTION_TIMEOUT).getAsync().get();
				if(httpResponse.success() && StringUtils.isNotEmpty(httpResponse.getString())) {
					response = DceSecurityV2.decrypt(httpResponse.getString());
					if (StringUtils.isNotEmpty(response))
						rekananId = Json.fromJson(response, Long.class);
				}
				if (rekananId != null) {
					param = URLs.encodePart(DceSecurityV2.encrypt(rekananId.toString()));
					url_service = ConfigurationDao.getRestCentralService() + "/rekanan?q=" + param;
					httpResponse = WS.url(url_service).setHeader("authentication", Play.secretKey)
							.timeout(ConfigurationDao.CONNECTION_TIMEOUT).getAsync().get();
					if(httpResponse.success() && StringUtils.isNotEmpty(httpResponse.getString())) {
						response = DceSecurityV2.decrypt(httpResponse.getString());
						if (StringUtils.isNotEmpty(response)) { // jika ada error
							RekananSSO userRekanan = Json.fromJson(response, RekananSSO.class);
							userRekanan.passw = "#";
							long count = Rekanan.count("rkn_id=?", rekananId);
							if(count == 0L)
								Rekanan.insertUserRekananFromWs(userRekanan);
							else
								Rekanan.updateUserRekananFromWs(userRekanan);
						}
					}
					// update input username sesuai lokal (handle modifikasi username rekanan)*
					Rekanan objExist = Rekanan.findById(rekananId);
					if(objExist != null)
						userId = objExist.rkn_namauser;
					// sinkronisasi data aktivasi, asumsi data ADP yang paling benar Aktivasi *
					param = URLs.encodePart(DceSecurityV2.encrypt(rekananId.toString()));
					url_service = ConfigurationDao.getRestCentralService() + "/aktivasi-rekanan?q=" + param;
					httpResponse = WS.url(url_service).setHeader("authentication", Play.secretKey)
							.timeout(ConfigurationDao.CONNECTION_TIMEOUT).getAsync().get();
					if(httpResponse.success() && StringUtils.isNotEmpty(httpResponse.getString())) {
						response = DceSecurityV2.decrypt(httpResponse.getString());
						if (StringUtils.isNotEmpty(response)) {
							Aktivasi aktivasi = Json.fromJson(response, Aktivasi.class);
							JsonObject aktivasi_adp = Json.fromJson(response, JsonObject.class);
							if (aktivasi_adp.get("aktivasiPK") != null) {
								JsonObject json_obj_pk = aktivasi_adp.get("aktivasiPK").getAsJsonObject();
								aktivasi.aktivasi_id = json_obj_pk.get("aktivasi_id").getAsLong();
								aktivasi.rkn_id = json_obj_pk.get("rkn_id").getAsLong();
							}
							aktivasi.save();
							aktivasi = Aktivasi.find("rkn_id=?", rekananId).first();
							StatusAktivasi status = StatusAktivasi.valueOf(aktivasi.aktivasi_status);
							if(status!= null && !status.isActivated())
								Scope.Session.current().put("status_aktivasi", true); // munculkan jika penyedia tersebut belom aktivasi
						}
					}
					if(objExist.isDisableUser())
						return LoginResult.DISABLED_USER;
					return LoginResult.SUCCESS;
				}
			} catch (Exception ex) {
				Logger.error("Tidak dapat terhubung ke inaproc! %s", ex.getLocalizedMessage());
				return LoginResult.INVALID_USER_PASSWORD;
			}
		} else { // login local
			if (user == null || StringUtils.isEmpty(user.passw)) {
				return LoginResult.INVALID_USER_PASSWORD; // userId tidak ada di database
			} else {
				if (user.isDisableUser()) {
					return LoginResult.DISABLED_USER; // userId tidak aktif
				} else {
					if (!password.equals(user.passw.toLowerCase())) {
						return LoginResult.INVALID_USER_PASSWORD; // password tidak cocok
					}
					return LoginResult.SUCCESS; // login sukses
				}
			}
		}
		return LoginResult.INVALID_USER_PASSWORD;
	}	
	
	public LupaPasswordToken requestLupaPasswordToken()
	{
		LupaPasswordToken token;
		if(StringUtils.isEmpty(reset_password))
		{
			token=new LupaPasswordToken();
			token.tanggal_buat=new Date();
			token.token=UUID.randomUUID().toString();
		}
		else
		{
			token= Json.fromJson(reset_password, LupaPasswordToken.class);
			if(token != null && token.isExpires())
				//token berganti
				token.token=UUID.randomUUID().toString();
			//tanggal buat selalu berganti
			token.tanggal_buat=controllers.BasicCtr.newDate();
		}
		reset_password=Json.toJson(token);
		return token;
	}
	
	public LupaPasswordToken getLupaPasswordToken()
	{
		if(reset_password==null)
			return null;
		else
			return Json.fromJson(reset_password, LupaPasswordToken.class);
	}

	//update identitas penyedia ke ADP Pusat
	public void saveToADPCentral() throws Exception {
		if(ConfigurationDao.isEnableInaproc()) {
			RekananSSO userRekanan = new RekananSSO();
			// Update rekanan sesuai data update identitas //
			userRekanan.repo_id = repo_id;
			userRekanan.rkn_id = rkn_id;
			userRekanan.btu_id = btu_id;
			userRekanan.cer_id = cer_id;
			userRekanan.kbp_id = kbp_id;
			userRekanan.rkn_nama = rkn_nama;
			userRekanan.rkn_alamat = rkn_alamat;
			userRekanan.rkn_kodepos = rkn_kodepos;
			userRekanan.rkn_telepon = rkn_telepon;
			userRekanan.rkn_fax = rkn_fax;
			userRekanan.rkn_mobile_phone = rkn_mobile_phone;
			userRekanan.rkn_npwp = rkn_npwp;
			userRekanan.rkn_pkp = rkn_pkp;
			userRekanan.rkn_statcabang = rkn_statcabang;
			userRekanan.rkn_email = rkn_email;
			userRekanan.rkn_website = rkn_website;
			userRekanan.rkn_tgl_daftar = rkn_tgl_daftar;
			userRekanan.rkn_tgl_setuju = rkn_tgl_setuju;
			userRekanan.rkn_almtpusat = rkn_almtpusat;
			userRekanan.rkn_telppusat = rkn_telppusat;
			userRekanan.rkn_faxpusat = rkn_faxpusat;
			userRekanan.rkn_emailpusat = rkn_emailpusat;
			userRekanan.rkn_namauser = rkn_namauser;
			userRekanan.rkn_isactive = rkn_isactive;
			userRekanan.rkn_status = rkn_status;
			userRekanan.audittype = audittype;
			userRekanan.auditupdate = DateUtil.newDate();
			userRekanan.audituser = audituser;
			JsonObject paramrekanan = new JsonObject();
			paramrekanan.addProperty("rekanan", CommonUtil.toJson(userRekanan));
			paramrekanan.addProperty("repoId", ConfigurationDao.getRepoId());
			String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(paramrekanan)));
			String url_service = ConfigurationDao.getRestCentralService() + "/update-identitas?q=" + param;
			try  {
				WSRequest request = WS.url(url_service);
				request.setHeader("authentication", Play.secretKey);
				HttpResponse response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
				if (response != null && !StringUtils.isEmpty(response.getString())) { // jika ada error
					throw new Exception(DceSecurityV2.decrypt(response.getString()));
				}
			}catch (Exception e) {
				throw e;
			}
		}
	}

	public static boolean emailExist(String email){
		Rekanan check = findByEmail(email);
		if(check != null){
			return true;
		}

		return false;
	}

	public static boolean emailExist(String email, Long rkn_id) {
		if(rkn_id == null || rkn_id == 0){
			return emailExist(email);
		}

		Rekanan check = Rekanan.find("rkn_email = ? and rkn_id <> ?", email, rkn_id).first();
		if(check != null) {
			return  true;
		}

		return false;
	}

	public boolean isPerusahaanAsing() {
		return !StringUtils.isEmpty(btu_id) && btu_id.equals("10");
	}

	@Override
	public Long getCerId() {
		return this.cer_id;
	}

	@Override
	public Long getUserId() {
		return this.rkn_id;
	}

	@Override
	public String getAmsId() {
		return this.ams_id;
	}

	@Override
	public String getUniqueId() {
		return this.rkn_npwp;
	}

	@Override
	public void setCerId(Long cerId) {
		this.cer_id = cerId;
	}

	@Override
	public void setAmsId(String amsId) {
		this.ams_id = amsId;
	}

	@Override
	public void saveModel() {
		save();
	}

	public boolean isPenyediaBaru(){
		return rkn_status != null && rkn_status.equals(REKANAN_BARU);
	}

	public static List<Rekanan> findAllPesertaLelang(Long lelangId) {
		return find("SELECT r.* FROM peserta p, rekanan r WHERE p.lls_id=?", lelangId).fetch();
	}
}
