package models.rekanan;


/**
 * Model Tipe Penyedia
 * @author retno
 *
 */
public enum TipePenyedia {

	PERUSAHAAN_PERSEORANGAN(1, "Penyedia Perseorangan"),
	
	//BADAN_USAHA_KSO(2, "Penyedia Badan Usaha KSO"),
	BADAN_USAHA_KSO(2, "Penyedia Kemitraan/KSO"),
	BADAN_USAHA_NON_KSO (3, "Penyedia Badan Usaha Non KSO");
	
	public final Integer id;
	
	public final String label;
	
	private TipePenyedia(int id, String label) {
		this.id = id;
		this.label = label;
	}
	
	public static TipePenyedia findById(Integer id) {
		if(id == null)
			return null;
		else if(id.intValue() == 1)
			return PERUSAHAAN_PERSEORANGAN;

		else if(id.intValue() == 3) 
			return BADAN_USAHA_NON_KSO;
		else
			return null;
	}
	
	public static TipePenyedia getTipePenyedia(String value){
		if(value.equals(null))
			return null;
		else if(value.equals("06") || value.equals("07"))
			return PERUSAHAAN_PERSEORANGAN;
	
		else if(!value.equals("06") || !value.equals("07")) 
			return BADAN_USAHA_NON_KSO;
		else
			return null;
	}
	
	
}
