package models.rekanan;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.PesertaPl;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 2/22/2017.
 */

@Table(name="EKONTRAK.BUKTI_PAJAK")
public class BuktiPajakPl extends BaseModel{

    @Id
    public Long psr_id;

    @Id
    public Long pjk_id;

    public String pjk_npwp;

    public String pjk_no;

    public Date pjk_tanggal;

    public String pjk_jenis;

    public String pjk_periode;

    public Integer pjk_tahun;

    public Integer pjk_bulan;

    public Long pjk_id_attachment;

    @Transient
    private PesertaPl peserta;

    public static PesertaPl getPeserta(Long pesertaId){
        return PesertaPl.find("psr_id=?", pesertaId).first();
    }

    public static List<BuktiPajakPl> findBy(Long pesertaId) {
        return find("psr_id=?", pesertaId).fetch();
    }

    public static void simpanPajakPeserta(List<Pajak> list, Long pesertaId) throws Exception {
        delete("psr_id=?", pesertaId);
        if(!CommonUtil.isEmpty(list)) {
            BuktiPajakPl bukti_pajak = null;
            for (Pajak palak:list) {
                bukti_pajak = find("pjk_id=? and psr_id=?",palak.pjk_id, pesertaId).first();
                if(bukti_pajak == null)
                    bukti_pajak = new BuktiPajakPl();
                bukti_pajak.pjk_id = palak.pjk_id;
                bukti_pajak.psr_id = pesertaId;
                bukti_pajak.pjk_npwp = palak.pjk_npwp;
                bukti_pajak.pjk_no = palak.pjk_no;
                bukti_pajak.pjk_tanggal = palak.pjk_tanggal;
                bukti_pajak.pjk_jenis = palak.pjk_jenis;
                bukti_pajak.pjk_periode = palak.pjk_periode;
                bukti_pajak.pjk_tahun = palak.pjk_tahun;
                bukti_pajak.pjk_bulan= palak.pjk_bulan;
                bukti_pajak.pjk_id_attachment = palak.pjk_id_attachment;
                bukti_pajak.save();
            }
        }
    }


}
