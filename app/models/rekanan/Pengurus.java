package models.rekanan;

import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;


@Table(name="PENGURUS")
public class Pengurus extends BaseModel {

	@Id(sequence="seq_pengurus", function="nextsequence")
	public Long pgr_id;
    @Required
	public String pgr_nama;
    @Required
	public String pgr_ktp;

	public String pgr_alamat;
	@Required
	public String pgr_jabatan;

	public Integer pgr_iskomis;
	@Required
	@As(binder=DateBinder.class)
	public Date pgr_valid_start;

	@As(binder=DateBinder.class)
	public Date pgr_valid_end;

    //relasi ke Rekanan
    public Long rkn_id;
    
    // field baru 4.1.2
	public String pgr_npwp;
	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;
    
    @Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	/**
	 * Fungsi {@code findSemuaByNamaUser} digunakan untuk mendapatkan semua data
	 * peralatan rekanan tertentu berdasarkan nama user rekanan
	 *
	 * @param username user id rekanan
	 * @return semua daftar peralatan rekanan dengan user id sesuai dengan
	 *         parameter {@code username}
	 */
	public static List<Pengurus> findSemuaByNamaUser(String username) {
		Rekanan rekanan = Rekanan.findByNamaUser(username);
		return find("rkn_id = ?", rekanan.rkn_id).fetch();
	}
	
	public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[7];
			Date tgl = rs.getDate("pgr_valid_end");
			obj[0] = rs.getString("pgr_id");
			obj[1] = rs.getString("pgr_nama");
			obj[2] = rs.getString("pgr_ktp");
			obj[3] = rs.getString("pgr_alamat");
			obj[4] = rs.getString("pgr_jabatan");
			obj[5] = FormatUtils.formatDateInd(rs.getDate("pgr_valid_start"));
			obj[6] = tgl != null ? FormatUtils.formatDateInd(tgl): Messages.get("ct.sekarang");
			return obj;
		}
	};
}
