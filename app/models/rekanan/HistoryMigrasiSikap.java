package models.rekanan;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * Created by Lambang on 6/14/2016.
 */

@Table(name="history_migrasi_sikap")
public class HistoryMigrasiSikap extends BaseModel{

    @Id(sequence="seq_history_migrasi_sikap", function="nextsequence")
    public Long hms_id;

    public Long rkn_id;

    public String data_spse;

    public String data_sikap;
    
    public Date tanggal_kirim;
    
    public Date tanggal_tarik;
    
    public static HistoryMigrasiSikap create(Long rkn_id, String data_spse, Date tanggal_kirim) {
    	HistoryMigrasiSikap history = new HistoryMigrasiSikap();
    	history.rkn_id = rkn_id;
    	history.data_spse = data_spse;
    	history.tanggal_kirim = tanggal_kirim;
    	history.save();
    	return history;
    }

}
