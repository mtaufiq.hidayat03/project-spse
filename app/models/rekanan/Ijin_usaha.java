package models.rekanan;

import ext.DateBinder;
import ext.FormatUtils;
import models.common.Kualifikasi;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.DateUtil;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;


@Table(name="IJIN_USAHA")
public class Ijin_usaha extends BaseModel implements Serializable {

	@Id(sequence="seq_ijin_usaha", function="nextsequence")
	public Long ius_id;
    
    @Required
	public String ius_no;
    
    
	public Date ius_tanggal;

	@As(binder=DateBinder.class)
	public Date ius_berlaku;

	@Required
	public Integer status_berlaku; // 0 menggunakan tanggal, 1 seumur hidup

    @Required
	public String ius_instansi;
	
	public Long ius_id_attachment;


	public String ius_klasifikasi;

   @Required
	public String jni_nama;


	//relasi ke Jenis_ijin
	public String jni_id;

    
	public String kls_id;


	public Long rkn_id;

	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	public void setKualifikasi(Kualifikasi kualifikasi)
	{
		this.kls_id = kualifikasi.id;
	}
	
	@Transient
	public Kualifikasi getKualifikasi()
	{
		return Kualifikasi.findById(kls_id);
	}
	
	@Transient
	public BlobTable getBlob() {
		if(ius_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(ius_id_attachment);
	}
	
	public String getBerlakuWarning() {
		Date now = DateUtil.newDate();
		return now.after(ius_berlaku) ? Messages.get("rekanan.masa_berlaku_habis"):"";
	}
	
	/**
	 * Fungsi {@code findSemuaByNamaUser} digunakan untuk mendapatkan semua data
	 * ijin usaha rekanan tertentu berdasarkan nama user rekanan
	 *
	 * @param username user id rekanan
	 * @return semua daftar ijin usaha rekanan dengan user id sesuai dengan
	 *         parameter {@code username}
	 */
	public static List<Ijin_usaha> findSemuaByNamaUser(String username) {
		Rekanan rekanan = Rekanan.findByNamaUser(username);
		return find("rkn_id = ?", rekanan.rkn_id).fetch();
	}

    public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[6];
			Date now = DateUtil.newDate();
			Date masa_berlaku = rs.getDate("ius_berlaku");
			Integer status_berlaku = rs.getInt("status_berlaku");
			obj[0] = rs.getString("ius_id");
			obj[1] = rs.getString("jni_nama");
			obj[2] = rs.getString("ius_no");
			obj[3] = (status_berlaku == 0 ? FormatUtils.formatDateInd(masa_berlaku) : Messages.get("rekanan.seumur_hidup")) + (masa_berlaku != null && now.after(masa_berlaku)?" "+Messages.get("rekanan.expired"):"");
			obj[4] = rs.getString("ius_instansi");
			obj[5] = Kualifikasi.findById(rs.getString("kls_id")).label;
			return obj;
		}
	};
}
