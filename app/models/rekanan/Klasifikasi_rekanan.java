package models.rekanan;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="KLASIFIKASI_REKANAN")
public class Klasifikasi_rekanan extends BaseModel {

	@Id
	public Long ius_id;
	
	@Id
	public Long kla_id;
}
