package models.rekanan;

import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;


@Table(name="PAJAK")
public class Pajak extends BaseModel {
	/**
	 * jenis periode dari pajak
	 *
	 * @author arief
	 */
	public enum Periode {
		TAHUNAN('Y'), BULANAN('M');

		public final char value;

		private Periode(char value) {
			this.value = value;
		}

		public boolean isBulanan() {
			return this == BULANAN;
		}

		public boolean isTahunan() {
			return this == TAHUNAN;
		}

		public static Periode fromValue(String value) {
			if (value == null)
				return null;
			else if (value.equals("Y"))
				return TAHUNAN;
			else if (value.equals("M"))
				return BULANAN;
			return null;
		}
	}

	@Id(sequence="seq_pajak", function="nextsequence")
	public Long pjk_id;

	public String pjk_npwp;
	@Required
	public String pjk_no;
	@Required
    @As(binder=DateBinder.class)
	public Date pjk_tanggal;
    @Required
	public String pjk_jenis;

	public String pjk_periode;
	@Required
	public Integer pjk_tahun;

	public Integer pjk_bulan;

	public Long pjk_id_attachment;

	//relasi ke Rekanan
	public Long rkn_id;
	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	@Transient
	public BlobTable getBlob() {
		if(pjk_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(pjk_id_attachment);
	}
	
	/**
	 * Fungsi {@code findSemuaByNamaUser} digunakan untuk mendapatkan semua data
	 * bukti pajak rekanan tertentu berdasarkan nama user rekanan
	 *
	 * @param username user id rekanan
	 * @return semua daftar bukti pajak rekanan dengan user id sesuai dengan
	 *         parameter {@code username}
	 */
	public static List<Pajak> findSemuaByNamaUser(String username) {
		Rekanan rekanan = Rekanan.findByNamaUser(username);
		return find("rkn_id = ?", rekanan.rkn_id).fetch();
	}
    
    public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[4];
			obj[0] = rs.getString("pjk_id");
			obj[1] = rs.getString("pjk_jenis") +"- "+ FormatUtils.formatBulan(rs.getInt("pjk_bulan")) + rs.getString("pjk_tahun");
			obj[2] = FormatUtils.formatDateInd(rs.getDate("pjk_tanggal"));
			obj[3] = rs.getString("pjk_no");			
			return obj;
		}
	};
}
