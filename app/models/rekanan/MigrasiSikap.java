package models.rekanan;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import models.common.ConfigurationDao;
import models.common.Jenis_ijin;
import models.jcommon.secure.encrypt2.InMemoryKeyCipherEngine;
import models.jcommon.secure.encrypt2.InMemoryRSAKey;
import models.jcommon.util.CommonUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.Query;
import play.libs.URLs;
import play.libs.WS;
import utils.JsonUtil;
import utils.ModelValidationException;
import utils.ModelValidatorUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/*
 @author Arief Ardiyansah
 model data penarikan dari dan ke SIKaP
 */
public class MigrasiSikap {

    private static final String URL_SIKAP_UPDATE = BasicCtr.SIKAP_URL + "/migrasidatapenyedia/updatebyfilter";
    private static final String URL_SIKAP_CERT = BasicCtr.SIKAP_URL + "/migrasidatapenyedia/getServerCert";
    private static final String URL_SIKAP_TARIK = BasicCtr.SIKAP_URL + "/migrasidatapenyedia/tarikbyfilter";
//    private static final String URL_SIKAP_MIGRASI = BasicCtr.SIKAP_URL + "/migrasidatapenyedia/kirim";

    public static String updateDataPenyedia(Rekanan rekanan) throws Exception {
        int repoId = ConfigurationDao.getRepoId();
        // generate key pair
        InMemoryRSAKey key = InMemoryRSAKey.getKey();
        // ambil certificate server
        String response = WS.url(URL_SIKAP_CERT).timeout(ConfigurationDao.CONNECTION_TIMEOUT).setParameter("cert", key.getPublicKeyEncoded()).setParameter("repoId", repoId).post().getString();
        Logger.info("response = %s", response);
        InMemoryKeyCipherEngine decryptEngine = InMemoryKeyCipherEngine.getDecyptEngine(key.idxKey, true);
        String decodedResponse = new String(decryptEngine.doCrypto(Base64.decodeBase64(response)));
        Logger.info("decodedResponse = %s ", decodedResponse);
        JsonParser parser = new JsonParser();
        JsonObject jsonResponse = (JsonObject) parser.parse(decodedResponse);
        if(jsonResponse.get("status") == null || jsonResponse.get("status").getAsInt() == 0) {
            throw new Exception(decodedResponse);
        }
        String serverCert = jsonResponse.get("cert").getAsString();
        String sessionKey = jsonResponse.get("k").getAsString();


        String result = "0";
        // prepare map data object
        // convert ke json
        JsonObject paramrekanan = new JsonObject();
        paramrekanan.addProperty("rekananId", rekanan.rkn_id);
        paramrekanan.addProperty("npwp", rekanan.rkn_npwp);
        paramrekanan.addProperty("rkn_namauser", rekanan.rkn_namauser);
        paramrekanan.addProperty("repoId", ConfigurationDao.getRepoId());
//        paramrekanan.addProperty("lastUpdate", rekanan.last_sync_vms.getTime());
        // kirim
        InMemoryKeyCipherEngine encryptEngine = InMemoryKeyCipherEngine.getEncyptEngine(serverCert, true);
        String param = URLs.encodePart(Base64.encodeBase64String(encryptEngine.doCrypto(CommonUtil.toJson(paramrekanan).getBytes())));
        String url_service = URL_SIKAP_UPDATE;
        Logger.debug("Update Data Penyedia %s on : %s", rekanan.rkn_id, new Date());
        Logger.debug("URL: %s ", url_service);
        Logger.debug("param: %s ", param);
        response = WS.url(url_service).timeout(ConfigurationDao.CONNECTION_TIMEOUT)
                .setParameter("q", param)
                .setParameter("k", sessionKey)
                .setParameter("cert", key.getPublicKeyEncoded())
                .post().getString();

        // handle response
        if (StringUtils.isEmpty(response)) { // jika ada error
            throw new Exception(response);
        }
        Logger.debug("Response dari server : %s", response);
        decryptEngine = InMemoryKeyCipherEngine.getDecyptEngine(key.idxKey, true);
        decodedResponse = new String(decryptEngine.doCrypto(Base64.decodeBase64(response)));
        Logger.info("Hasil decrypt response dari server : %s", decodedResponse);
        jsonResponse = (JsonObject) parser.parse(decodedResponse);
        if(jsonResponse.get("status") != null) {
            result = String.valueOf(jsonResponse.get("status").getAsInt());
        }
        if(result.equals("1")) {
            JsonObject jsonRekanan = jsonResponse.get("rekanan").getAsJsonObject();
            try {
                // populate jenis ijin agar tidak perlu query saat looping
                HashMap<String, Jenis_ijin> mapJenisIjin = new HashMap<String, Jenis_ijin>();
                List<Jenis_ijin> jenisIjinList = new ArrayList<Jenis_ijin>();
                for(Jenis_ijin objJni : jenisIjinList){
                    if(mapJenisIjin.get(objJni.jni_id) == null){
                        mapJenisIjin.put(objJni.jni_id, objJni);
                    }
                }

                // hapus data penyedia, kecuali identitas
                Ijin_usaha.delete("rkn_id=?", rekanan.rkn_id);
                Landasan_hukum.delete("rkn_id=?", rekanan.rkn_id);
                Pemilik.delete("rkn_id=?", rekanan.rkn_id);
                Pengurus.delete("rkn_id=?", rekanan.rkn_id);
                Query.update("delete from CV_STAF where sta_id in (select sta_id from STAF_AHLI where rkn_id=?)", rekanan.rkn_id);
                Staf_ahli.delete("rkn_id=?", rekanan.rkn_id);
                Peralatan.delete("rkn_id=?", rekanan.rkn_id);
                Pengalaman.delete("rkn_id=?", rekanan.rkn_id);
                Pajak.delete("rkn_id=?", rekanan.rkn_id);

                // Izin Usaha
                JsonArray listIjinUsaha = jsonRekanan.get("ijinUsaha").getAsJsonArray();
                for(int i=0;i<listIjinUsaha.size();i++) {
                    JsonObject jsonIjinUsaha = listIjinUsaha.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonIjinUsaha.get("deleted").getAsBoolean();
                    long vms_id = jsonIjinUsaha.get("ius_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Ijin_usaha.delete("vms_id=?", vms_id);
                    } else {
                        Ijin_usaha tmpIjinUsaha = JsonUtil.fromJsonSikap(jsonIjinUsaha.toString(), Ijin_usaha.class);
                        tmpIjinUsaha.vms_id = vms_id;
                        Ijin_usaha oldIjinUsaha = Ijin_usaha.find("vms_id=?", vms_id).first();
                        if(oldIjinUsaha == null) { // belum ada di SPSE, insert
                            tmpIjinUsaha.ius_id = null;
                        } else {
                            tmpIjinUsaha.ius_id = oldIjinUsaha.ius_id;
                        }
                        Jenis_ijin jni = mapJenisIjin.get(tmpIjinUsaha.jni_id);
                        if (jni == null) { // insert jika belum ada di SPSE
                            jni = new Jenis_ijin();
                            jni.jni_id = tmpIjinUsaha.jni_id;
                            jni.jni_nama = tmpIjinUsaha.jni_nama;
                            jni.save();
                        }
                        List<String> errors = ModelValidatorUtil.validate(tmpIjinUsaha, "ijin_usaha.");
                        if(errors.isEmpty()) {
                            tmpIjinUsaha.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }

                // Akta
                JsonElement jsonPendirian = jsonRekanan.get("aktaPendirian");
                List<Landasan_hukum> landasan_hukumList = Landasan_hukum.find("rkn_id = ? order by lhk_tanggal asc ", rekanan.rkn_id).fetch(2);
                if(jsonPendirian != null) {
                    JsonObject jsonObject = jsonPendirian.getAsJsonObject();
                    long vms_id = jsonObject.get("lhk_id").getAsLong();
                    Landasan_hukum pendirian = CommonUtil.fromJson(jsonObject.toString(), Landasan_hukum.class);
                    Landasan_hukum oldPendirian = !landasan_hukumList.isEmpty() ? landasan_hukumList.get(0) : null;
                    pendirian.vms_id = vms_id;
                    if(oldPendirian == null) {
                        pendirian.lhk_id = null;
                    } else {
                        pendirian.lhk_id = oldPendirian.lhk_id;
                    }
                    List<String> errors = ModelValidatorUtil.validate(pendirian, "akta.");
                    if(errors.isEmpty()) {
                        pendirian.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                JsonElement jsonPerubahan = jsonRekanan.get("aktaPerubahan");
                if(jsonPerubahan != null) {
                    JsonObject jsonObject = jsonPerubahan.getAsJsonObject();
                    long vms_id = jsonObject.get("lhk_id").getAsLong();
                    Landasan_hukum perubahan = CommonUtil.fromJson(jsonObject.toString(), Landasan_hukum.class);
                    Landasan_hukum oldPerubahan = landasan_hukumList.size() > 1 ? landasan_hukumList.get(1) : null;
                    perubahan.vms_id = vms_id;
                    if(oldPerubahan == null) {
                        perubahan.lhk_id = null;
                    } else {
                        perubahan.lhk_id = oldPerubahan.lhk_id;
                    }
                    List<String> errors = ModelValidatorUtil.validate(perubahan, "akta.");
                    if(errors.isEmpty()) {
                        perubahan.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }

                // Pemilik
                JsonArray listPemilik = jsonRekanan.get("pemilik").getAsJsonArray();
                for(int i=0;i<listPemilik.size();i++) {
                    JsonObject jsonPemilik = listPemilik.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonPemilik.get("deleted").getAsBoolean();
                    long vms_id = jsonPemilik.get("pml_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Pemilik.delete("vms_id=?", vms_id);
                    } else {
                        Pemilik tmpPemilik = CommonUtil.fromJson(jsonPemilik.toString(), Pemilik.class);
                        tmpPemilik.pml_satuan = tmpPemilik.pml_satuan == null ? 0 : (tmpPemilik.pml_satuan - 1); // perlu dikonversi karena di SPSE: 0 dan 1, di SIKaP 1 dan 2.
                        tmpPemilik.vms_id = vms_id;
                        Pemilik oldPemilik = Pemilik.find("vms_id=?", vms_id).first();
                        if(oldPemilik == null) { // belum ada di SPSE, insert
                            tmpPemilik.pml_id = null;
                        } else {
                            tmpPemilik.pml_id = oldPemilik.pml_id;
                        }
                        List<String> errors = ModelValidatorUtil.validate(tmpPemilik, "pemilik.");
                        if(errors.isEmpty()) {
                            tmpPemilik.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }

                // Pengurus
                JsonArray listPengurus = jsonRekanan.get("pengurus").getAsJsonArray();
                for(int i=0;i<listPengurus.size();i++) {
                    JsonObject jsonPengurus = listPengurus.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonPengurus.get("deleted").getAsBoolean();
                    long vms_id = jsonPengurus.get("pgr_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Pengurus.delete("vms_id=?", vms_id);
                    } else {
                        Pengurus tmpPengurus = CommonUtil.fromJson(jsonPengurus.toString(), Pengurus.class);
                        tmpPengurus.vms_id = vms_id;
                        Pengurus oldPengurus = Pengurus.find("vms_id=?", vms_id).first();
                        if(oldPengurus == null) { // belum ada di SPSE, insert
                            tmpPengurus.pgr_id = null;
                        } else {
                            tmpPengurus.pgr_id = oldPengurus.pgr_id;
                        }
                        List<String> errors = ModelValidatorUtil.validate(tmpPengurus, "pengurus.");
                        if(errors.isEmpty()) {
                            tmpPengurus.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }

                // Tenaga Ahli
                JsonArray listStafAhli = jsonRekanan.get("tenagaAhli").getAsJsonArray();
                for(int i=0;i<listStafAhli.size();i++) {
                    JsonObject jsonStafAhli = listStafAhli.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonStafAhli.get("deleted").getAsBoolean();
                    long vms_id = jsonStafAhli.get("sta_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Staf_ahli.delete("vms_id=?", vms_id);
                    } else {
                        Staf_ahli tmpStafAhli = JsonUtil.fromJsonSikap(jsonStafAhli.toString(), Staf_ahli.class);
                        tmpStafAhli.vms_id = vms_id;
                        Staf_ahli oldStafAhli = Staf_ahli.find("vms_id=?", vms_id).first();
                        if(oldStafAhli == null) { // belum ada di SPSE, insert
                            tmpStafAhli.sta_id = null;
                        } else {
                            tmpStafAhli.sta_id = oldStafAhli.sta_id;
                        }
                        List<String> errors = ModelValidatorUtil.validate(tmpStafAhli, "staf.");
                        if(errors.isEmpty()) {
                            // CV Tenaga Ahli, dilooping karena pattern-nya sama
                            String[] fields = {"pengalaman","pendidikan","sertifikasi","bahasa"};
                            for(String field:fields) {
                                JsonArray jsonCv = jsonStafAhli.get(field).getAsJsonArray();
                                for(int j=0;j<jsonCv.size();j++) {
                                    JsonObject jsonObject = jsonCv.get(j).getAsJsonObject();
                                    // memeriksa apakah dihapus di SIKaP
                                    long cv_vms_id = jsonObject.get("cva_id").getAsLong();
                                    Staf_ahli.Cv_staf tmpCv = CommonUtil.fromJson(jsonObject.toString(), Staf_ahli.Cv_staf.class);
                                    tmpCv.vms_id = cv_vms_id;
                                    switch (tmpCv.kategori()){
                                        case PENDIDIKAN: tmpStafAhli.pendidikan.add(tmpCv);break;
                                        case PELATIHAN: tmpStafAhli.sertifikasi.add(tmpCv);break;
                                        case BAHASA: tmpStafAhli.bahasa.add(tmpCv);break;
                                        case PENGALAMAN:tmpStafAhli.pengalaman.add(tmpCv);break;
                                    }
                                }
                            }
                            tmpStafAhli.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }


                // Peralatan
                JsonArray listPeralatan = jsonRekanan.get("peralatan").getAsJsonArray();
                for(int i=0;i<listPeralatan.size();i++) {
                    JsonObject jsonPeralatan = listPeralatan.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonPeralatan.get("deleted").getAsBoolean();
                    long vms_id = jsonPeralatan.get("alt_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Peralatan.delete("vms_id=?", vms_id);
                    } else {
                        Peralatan tmpPeralatan = CommonUtil.fromJson(jsonPeralatan.toString(), Peralatan.class);
                        tmpPeralatan.vms_id = vms_id;
                        Peralatan oldPeralatan = Peralatan.find("vms_id=?", vms_id).first();
                        if(oldPeralatan == null) { // belum ada di SPSE, insert
                            tmpPeralatan.alt_id = null;
                        } else {
                            tmpPeralatan.alt_id = oldPeralatan.alt_id;
                        }
                        List<String> errors = ModelValidatorUtil.validate(tmpPeralatan, "peralatan.");
                        if(errors.isEmpty()) {
                            tmpPeralatan.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }

                // Pengalaman
                JsonArray listPengalaman = jsonRekanan.get("pengalaman").getAsJsonArray();
                for(int i=0;i<listPengalaman.size();i++) {
                    JsonObject jsonPengalaman = listPengalaman.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonPengalaman.get("deleted").getAsBoolean();
                    long vms_id = jsonPengalaman.get("pgl_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Pengalaman.delete("vms_id=?", vms_id);
                    } else {
                        Pengalaman tmpPengalaman = JsonUtil.fromJsonSikap(jsonPengalaman.toString(), Pengalaman.class);
                        tmpPengalaman.vms_id = vms_id;
                        Pengalaman oldPengalaman = Pengalaman.find("vms_id=?", vms_id).first();
                        if(oldPengalaman == null) { // belum ada di SPSE, insert
                            tmpPengalaman.pgn_id = null;
                        } else {
                            tmpPengalaman.pgn_id = oldPengalaman.pgn_id;
                        }
                        List<String> errors = ModelValidatorUtil.validate(tmpPengalaman, "pengalaman.");
                        if(errors.isEmpty()) {
                            tmpPengalaman.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }

                // Pajak
                JsonArray listPajak = jsonRekanan.get("pajak").getAsJsonArray();
                for(int i=0;i<listPajak.size();i++) {
                    JsonObject jsonPajak = listPajak.get(i).getAsJsonObject();
                    // memeriksa apakah dihapus di SIKaP
                    boolean deleted = jsonPajak.get("deleted").getAsBoolean();
                    long vms_id = jsonPajak.get("pjk_id").getAsLong();
                    if(deleted) { // hapus di SPSE
                        Pajak.delete("vms_id=?", vms_id);
                    } else {
                        Pajak tmpPajak = JsonUtil.fromJsonSikap(jsonPajak.toString(), Pajak.class);
                        tmpPajak.vms_id = vms_id;
                        Pajak oldPajak = Pajak.find("vms_id=?", vms_id).first();
                        if(oldPajak == null) { // belum ada di SPSE, insert
                            tmpPajak.pjk_id = null;
                        } else {
                            tmpPajak.pjk_id = oldPajak.pjk_id;
                        }
                        tmpPajak.pjk_npwp = "-";
                        List<String> errors = ModelValidatorUtil.validate(tmpPajak, "pajak.");
                        if(errors.isEmpty()) {
                            tmpPajak.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                    }
                }

//				// rekanan dan finish
//                JsonElement jsonElmIdentitas = jsonRekanan.get("identitas");
//                if(jsonElmIdentitas != null) {
//                    JsonObject jsonIdentitas = jsonElmIdentitas.getAsJsonObject();
//                    rekanan.rkn_kodepos = jsonIdentitas.get("rkn_kodepos") == null ? null : jsonIdentitas.get("rkn_kodepos").getAsString();
//                    rekanan.rkn_pkp = jsonIdentitas.get("rkn_pkp") == null ? null : jsonIdentitas.get("rkn_pkp").getAsString();
//                    rekanan.kbp_id = jsonIdentitas.get("kbp_id") == null ? null : jsonIdentitas.get("kbp_id").getAsLong();
//                    rekanan.rkn_telepon = jsonIdentitas.get("rkn_telepon") == null ? null : jsonIdentitas.get("rkn_telepon").getAsString();
//                    rekanan.rkn_mobile_phone = jsonIdentitas.get("rkn_mobile_phone") == null ? null : jsonIdentitas.get("rkn_mobile_phone").getAsString();
//                    rekanan.rkn_statcabang = jsonIdentitas.get("rkn_statcabang") == null ? null : jsonIdentitas.get("rkn_statcabang").getAsString();
//                    rekanan.rkn_almtpusat = jsonIdentitas.get("rkn_almtpusat") == null ? null : jsonIdentitas.get("rkn_almtpusat").getAsString();
//                    rekanan.rkn_emailpusat = jsonIdentitas.get("rkn_emailpusat") == null ? null : jsonIdentitas.get("rkn_emailpusat").getAsString();
//                    rekanan.rkn_telppusat = jsonIdentitas.get("rkn_telppusat") == null ? null : jsonIdentitas.get("rkn_telppusat").getAsString();
//                    rekanan.rkn_faxpusat = jsonIdentitas.get("rkn_faxpusat") == null ? null : jsonIdentitas.get("rkn_faxpusat").getAsString();
//                    rekanan.rkn_fax = jsonIdentitas.get("rkn_fax") == null ? null : jsonIdentitas.get("rkn_fax").getAsString();
//                    rekanan.rkn_website = jsonIdentitas.get("rkn_website") == null ? null : jsonIdentitas.get("rkn_website").getAsString();
//                    List<String> errors = ModelValidatorUtil.validate(rekanan, "rekanan.");
//                    if(!errors.isEmpty()) {
//                        throw new ModelValidationException(errors);
//                    }
//                }
                // catat waktu berdasarkan informasi yang dikirim SIKaP
                rekanan.last_sync_vms = new Date(jsonRekanan.get("lastUpdate").getAsLong());
                rekanan.save();
            } catch (ModelValidationException e) {
                e.printStackTrace();
                Logger.error(e, "Gagal update Data Penyedia: " + ModelValidatorUtil.printError(e.errors));
                result = ModelValidatorUtil.printError(e.errors);
            } catch (Exception e) {
//                e.printStackTrace();
//                Logger.error(e, "Gagal update Data Penyedia");
//                result = "0";
                throw e;
            }
        }
        return result;
    }

    public static String tarikDataPenyedia(Rekanan rekanan, Date tanggal_tarik) throws Exception {
        // memeriksa dulu apakah di lokal sudah ada copy raw data dari SIKaP
        HistoryMigrasiSikap historyMigrasi = HistoryMigrasiSikap.find("rkn_id=?",rekanan.rkn_id).first();
        // 31 mei, ubah supaya selalu request baru kalau-kalau data SIKaP ada error
        boolean skipRequest = false;//historyMigrasi.data_sikap != null;
        String result = "0";
        JsonObject jsonResponse = null;
        if(!skipRequest) { // belum ada, request ke server
            int repoId = ConfigurationDao.getRepoId();
            // generate key pair
            InMemoryRSAKey key = InMemoryRSAKey.getKey();
            // ambil certificate server
            String response = WS.url(URL_SIKAP_CERT).timeout(ConfigurationDao.CONNECTION_TIMEOUT).setParameter("cert", key.getPublicKeyEncoded()).setParameter("repoId", repoId).post().getString();
            Logger.info("response = %s", response);
            InMemoryKeyCipherEngine decryptEngine = InMemoryKeyCipherEngine.getDecyptEngine(key.idxKey, true);
            String decodedResponse = new String(decryptEngine.doCrypto(Base64.decodeBase64(response)));
            Logger.info("decodedResponse = %s ", decodedResponse);
            JsonParser parser = new JsonParser();
            jsonResponse = (JsonObject) parser.parse(decodedResponse);
            if(jsonResponse.get("status") == null || jsonResponse.get("status").getAsInt() == 0) {
                throw new Exception(decodedResponse);
            }
            String serverCert = jsonResponse.get("cert").getAsString();
            String sessionKey = jsonResponse.get("k").getAsString();

            // prepare map data object
            // convert ke json
            JsonObject paramrekanan = new JsonObject();
            paramrekanan.addProperty("rekananId", rekanan.rkn_id);
            paramrekanan.addProperty("npwp", rekanan.rkn_npwp);
            paramrekanan.addProperty("rkn_namauser", rekanan.rkn_namauser);
            paramrekanan.addProperty("repoId", ConfigurationDao.getRepoId());
            // kirim
            InMemoryKeyCipherEngine encryptEngine = InMemoryKeyCipherEngine.getEncyptEngine(serverCert, true);
            String param = URLs.encodePart(Base64.encodeBase64String(encryptEngine.doCrypto(CommonUtil.toJson(paramrekanan).getBytes())));
            String url_service = URL_SIKAP_TARIK;
            Logger.info("Tarik Data Penyedia %s on : %s", rekanan.rkn_id, new Date());
            Logger.info("URL: %s ", url_service);
            Logger.info("param: %s ", param);
            response = WS.url(url_service).timeout(ConfigurationDao.CONNECTION_TIMEOUT)
                    .setParameter("q", param)
                    .setParameter("k", sessionKey)
                    .setParameter("cert", key.getPublicKeyEncoded())
                    .post().getString();

            // handle response
            if (StringUtils.isEmpty(response)) { // jika ada error
                throw new Exception(response);
            }
            Logger.info("Response dari server : %s", response);
            decryptEngine = InMemoryKeyCipherEngine.getDecyptEngine(key.idxKey, true);
            decodedResponse = new String(decryptEngine.doCrypto(Base64.decodeBase64(response)));
            Logger.info("Hasil decrypt response dari server : %s", decodedResponse);
            parser = new JsonParser();
            jsonResponse = (JsonObject) parser.parse(decodedResponse);
            if(jsonResponse.get("status") != null) {
                result = String.valueOf(jsonResponse.get("status").getAsInt());
            }
        } else {
            result = "2";
        }
        if(skipRequest || result.equals("1") || result.equals("2")) {
            JsonObject jsonRekanan = skipRequest ? (JsonObject)new JsonParser().parse(historyMigrasi.data_sikap) : jsonResponse.get("rekanan").getAsJsonObject();
            // simpan history dulu untuk mitigasi kalau-kalau ada error setelah data penyedia dihapus
            if(!skipRequest) {
                if (historyMigrasi == null) {
                    historyMigrasi = new HistoryMigrasiSikap();
                    historyMigrasi.rkn_id = rekanan.rkn_id;
                }
                historyMigrasi.data_sikap = jsonRekanan.toString();
                historyMigrasi.tanggal_tarik = tanggal_tarik;
                historyMigrasi.save();
            }
            try {

                // populate jenis ijin agar tidak perlu query saat looping
                HashMap<String, Jenis_ijin> mapJenisIjin = new HashMap<String, Jenis_ijin>();
                List<Jenis_ijin> jenisIjinList = new ArrayList<Jenis_ijin>();
                for(Jenis_ijin objJni : jenisIjinList){
                    if(mapJenisIjin.get(objJni.jni_id) == null){
                        mapJenisIjin.put(objJni.jni_id, objJni);
                    }
                }

                // hapus data penyedia, kecuali identitas
                Ijin_usaha.delete("rkn_id=?", rekanan.rkn_id);
                Landasan_hukum.delete("rkn_id=?", rekanan.rkn_id);
                Pemilik.delete("rkn_id=?", rekanan.rkn_id);
                Pengurus.delete("rkn_id=?", rekanan.rkn_id);
                Query.update("delete from CV_STAF where sta_id in (select sta_id from STAF_AHLI where rkn_id=?)", rekanan.rkn_id);
                Staf_ahli.delete("rkn_id=?", rekanan.rkn_id);
                Peralatan.delete("rkn_id=?", rekanan.rkn_id);
                Pengalaman.delete("rkn_id=?", rekanan.rkn_id);
                Pajak.delete("rkn_id=?", rekanan.rkn_id);

                // masukkan data penyedia yang baru
                List<Ijin_usaha> ijinUsaha = JsonUtil.fromJsonSikap(jsonRekanan.get("ijinUsaha").toString(), new TypeToken<List<Ijin_usaha>>(){}.getType());
                for(Ijin_usaha tmp: ijinUsaha) {
                    tmp.vms_id = tmp.ius_id;
                    tmp.ius_id = null;

                    Jenis_ijin jni = mapJenisIjin.get(tmp.jni_id);
                    if (jni == null) { // insert jika belum ada di SPSE
                        jni = new Jenis_ijin();
                        jni.jni_id = tmp.jni_id;
                        jni.jni_nama = tmp.jni_nama;
                        jni.save();
                    }

                    List<String> errors = ModelValidatorUtil.validate(tmp, "ijin_usaha.");
                    if(errors.isEmpty()) {
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                if(jsonRekanan.get("aktaPendirian") != null) {
                    Landasan_hukum pendirian = CommonUtil.fromJson(jsonRekanan.get("aktaPendirian").toString(), Landasan_hukum.class);
                    pendirian.vms_id = pendirian.lhk_id;
                    pendirian.lhk_id = null;
                    List<String> errors = ModelValidatorUtil.validate(pendirian, "akta.");
                    if(errors.isEmpty()) {
                        pendirian.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }

                    if(jsonRekanan.get("aktaPerubahan") != null) {
                        Landasan_hukum perubahan = CommonUtil.fromJson(jsonRekanan.get("aktaPerubahan").toString(), Landasan_hukum.class);
                        perubahan.vms_id = perubahan.lhk_id;
                        perubahan.lhk_id = null;
                        errors = ModelValidatorUtil.validate(perubahan, "akta.");
                        if(errors.isEmpty()) {
                            perubahan.save();
                        } else {
                            throw new ModelValidationException(errors);
                        }
                        perubahan.save();
                    }
                }
                List<Pemilik> pemilik = CommonUtil.fromJson(jsonRekanan.get("pemilik").toString(), new TypeToken<List<Pemilik>>(){}.getType());
                for(Pemilik tmp: pemilik) {
                    tmp.pml_satuan = tmp.pml_satuan == null ? 0 : (tmp.pml_satuan - 1); // perlu dikonversi karena di SPSE: 0 dan 1, di SIKaP 1 dan 2.
                    tmp.vms_id = tmp.pml_id;
                    tmp.pml_id = null;
                    List<String> errors = ModelValidatorUtil.validate(tmp, "pemilik.");
                    if(errors.isEmpty()) {
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                List<Pengurus> pengurus = CommonUtil.fromJson(jsonRekanan.get("pengurus").toString(), new TypeToken<List<Pengurus>>(){}.getType());
                for(Pengurus tmp: pengurus) {
                    tmp.vms_id = tmp.pgr_id;
                    tmp.pgr_id = null;
                    List<String> errors = ModelValidatorUtil.validate(tmp, "pengurus.");
                    if(errors.isEmpty()) {
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                List<Staf_ahli> tenagaAhli = JsonUtil.fromJsonSikap(jsonRekanan.get("tenagaAhli").toString(), new TypeToken<List<Staf_ahli>>(){}.getType());
                for(Staf_ahli tmp: tenagaAhli) {
                    tmp.vms_id = tmp.sta_id;
                    tmp.sta_id = null;
                    List<String> errors = ModelValidatorUtil.validate(tmp, "staf.");
                    if(errors.isEmpty()) {
                        for(Staf_ahli.Cv_staf cv:tmp.pengalaman) {
                            cv.vms_id = cv.cva_id;
                            cv.sta_id = tmp.sta_id;
                        }
                        for(Staf_ahli.Cv_staf cv:tmp.pendidikan) {
                            cv.vms_id = cv.cva_id;
                            cv.sta_id = tmp.sta_id;
                        }
                        for(Staf_ahli.Cv_staf cv:tmp.sertifikasi) {
                            cv.vms_id = cv.cva_id;
                            cv.sta_id = tmp.sta_id;
                        }
                        for(Staf_ahli.Cv_staf cv:tmp.bahasa) {
                            cv.vms_id = cv.cva_id;
                            cv.sta_id = tmp.sta_id;
                        }
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                List<Peralatan> peralatan = CommonUtil.fromJson(jsonRekanan.get("peralatan").toString(), new TypeToken<List<Peralatan>>(){}.getType());
                for(Peralatan tmp: peralatan) {
                    tmp.vms_id = tmp.alt_id;
                    tmp.alt_id = null;
                    List<String> errors = ModelValidatorUtil.validate(tmp, "peralatan.");
                    if(errors.isEmpty()) {
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                List<Pengalaman> pengalaman = JsonUtil.fromJsonSikap(jsonRekanan.get("pengalaman").toString(), new TypeToken<List<Pengalaman>>(){}.getType());
                for(Pengalaman tmp: pengalaman) {
                    tmp.vms_id = tmp.pgn_id;
                    tmp.pgn_id = null;
                    List<String> errors = ModelValidatorUtil.validate(tmp, "pengalaman.");
                    if(errors.isEmpty()) {
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                List<Pajak> pajak = JsonUtil.fromJsonSikap(jsonRekanan.get("pajak").toString(), new TypeToken<List<Pajak>>(){}.getType());
                for(Pajak tmp: pajak) {
                    tmp.vms_id = tmp.pjk_id;
                    tmp.pjk_id = null;
                    tmp.pjk_npwp = "-";
                    List<String> errors = ModelValidatorUtil.validate(tmp, "pajak.");
                    if(errors.isEmpty()) {
                        tmp.save();
                    } else {
                        throw new ModelValidationException(errors);
                    }
                }
                // rekanan dan finish
                JsonObject jsonIdentitas = jsonRekanan.get("identitas").getAsJsonObject();
                rekanan.rkn_kodepos = jsonIdentitas.get("rkn_kodepos") == null ? null : jsonIdentitas.get("rkn_kodepos").getAsString();
                rekanan.rkn_pkp = jsonIdentitas.get("rkn_pkp") == null ? null : jsonIdentitas.get("rkn_pkp").getAsString();
                rekanan.kbp_id = jsonIdentitas.get("kbp_id") == null ? null : jsonIdentitas.get("kbp_id").getAsLong();
                rekanan.rkn_telepon = jsonIdentitas.get("rkn_telepon") == null ? null : jsonIdentitas.get("rkn_telepon").getAsString();
                rekanan.rkn_mobile_phone = jsonIdentitas.get("rkn_mobile_phone") == null ? null : jsonIdentitas.get("rkn_mobile_phone").getAsString();
                rekanan.rkn_statcabang = jsonIdentitas.get("rkn_statcabang") == null ? null : jsonIdentitas.get("rkn_statcabang").getAsString();
                rekanan.rkn_almtpusat = jsonIdentitas.get("rkn_almtpusat") == null ? null : jsonIdentitas.get("rkn_almtpusat").getAsString();
                rekanan.rkn_emailpusat = jsonIdentitas.get("rkn_emailpusat") == null ? null : jsonIdentitas.get("rkn_emailpusat").getAsString();
                rekanan.rkn_telppusat = jsonIdentitas.get("rkn_telppusat") == null ? null : jsonIdentitas.get("rkn_telppusat").getAsString();
                rekanan.rkn_faxpusat = jsonIdentitas.get("rkn_faxpusat") == null ? null : jsonIdentitas.get("rkn_faxpusat").getAsString();
                rekanan.rkn_fax = jsonIdentitas.get("rkn_fax") == null ? null : jsonIdentitas.get("rkn_fax").getAsString();
                rekanan.rkn_website = jsonIdentitas.get("rkn_website") == null ? null : jsonIdentitas.get("rkn_website").getAsString();
                rekanan.status_migrasi = Rekanan.MIGRASI_SIKAP_SELESAI;
                // catat waktu berdasarkan informasi yang dikirim SIKaP
                rekanan.last_sync_vms = new Date(jsonRekanan.get("lastUpdate").getAsLong());
                List<String> errors = ModelValidatorUtil.validate(rekanan, "rekanan.");
                if(errors.isEmpty()) {
                    rekanan.save();
                } else {
                    throw new ModelValidationException(errors);
                }

            } catch (ModelValidationException e) {
                e.printStackTrace();
                Logger.error(e, "Gagal update Data Penyedia: " + ModelValidatorUtil.printError(e.errors));
                result = ModelValidatorUtil.printError(e.errors);
            } catch (Exception e) {
//                e.printStackTrace();
//                Logger.error(e, "Gagal update Data Penyedia");
//                result = "0";
                throw e;
            }
        }
        return result;
    }
}
