package models.rekanan;

import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Peserta;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Table(name="LANDASAN_HUKUM_PESERTA")
public class Landasan_hukum_peserta extends BaseModel {

	@Id
	public Long lhkp_id;
	
	@Id
	public Long psr_id;

	public String lhkp_no;

	public Date lhkp_tanggal;

	public String lhkp_notaris;

	public Long lhkp_id_attachment;
	
	@Transient
	private Peserta peserta;
	

	
	public static List<Landasan_hukum_peserta> findBy(Long pesertaId) {
		return find("psr_id=? order by lhkp_tanggal", pesertaId).fetch();
    }

	public static Peserta getPeserta(Long pesertaId){
		return Peserta.find("psr_id=?", pesertaId).first();
	}
	
	public static void simpanAkta(Long pesertaId, Long rekananId)
	{
		delete("psr_id=?", pesertaId);
		Landasan_hukum landasan_hukum = null;
		Landasan_hukum landasan_hukum_akhir = null;
		List<Landasan_hukum> list = Landasan_hukum.findBy(rekananId);
        if(!CommonUtil.isEmpty(list)) {       
        	if(list.get(0) != null)
        		landasan_hukum = list.get(0);
        	if(list.size() > 1 && list.get(1) != null)
        		landasan_hukum_akhir = list.get(1);
        }	       
        if(landasan_hukum != null) { 
	        Landasan_hukum_peserta landasan_hukum_peserta = find("psr_id=? and lhkp_id=?",pesertaId, landasan_hukum.lhk_id).first();
	        if(landasan_hukum_peserta == null)
	        	landasan_hukum_peserta = new Landasan_hukum_peserta();
			landasan_hukum_peserta.psr_id = pesertaId;
		    landasan_hukum_peserta.lhkp_id = landasan_hukum.lhk_id;
		    landasan_hukum_peserta.lhkp_no = landasan_hukum.lhk_no;
		    landasan_hukum_peserta.lhkp_tanggal = landasan_hukum.lhk_tanggal;
		    landasan_hukum_peserta.lhkp_notaris = landasan_hukum.lhk_notaris;
		    landasan_hukum_peserta.lhkp_id_attachment = landasan_hukum.lhk_id_attachment;
		    landasan_hukum_peserta.save();
		}
        if(landasan_hukum_akhir != null) {
	        Landasan_hukum_peserta landasan_hukum_peserta_akhir =  find("psr_id=? and lhkp_id=?",pesertaId, landasan_hukum_akhir.lhk_id).first();
	        if(landasan_hukum_peserta_akhir == null)
	        	landasan_hukum_peserta_akhir = new Landasan_hukum_peserta();
	        landasan_hukum_peserta_akhir.psr_id = pesertaId;
	        landasan_hukum_peserta_akhir.lhkp_id = landasan_hukum_akhir.lhk_id;
	        landasan_hukum_peserta_akhir.lhkp_no = landasan_hukum_akhir.lhk_no;
	        landasan_hukum_peserta_akhir.lhkp_tanggal = landasan_hukum_akhir.lhk_tanggal;
	        landasan_hukum_peserta_akhir.lhkp_notaris = landasan_hukum_akhir.lhk_notaris;
		    landasan_hukum_peserta_akhir.lhkp_id_attachment = landasan_hukum_akhir.lhk_id_attachment;
		    landasan_hukum_peserta_akhir.save();
        }
	}
	

	
	
}
