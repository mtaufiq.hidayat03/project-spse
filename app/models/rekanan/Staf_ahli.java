package models.rekanan;

import com.google.gson.reflect.TypeToken;
import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Email;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table(name="STAF_AHLI")
public class Staf_ahli extends BaseModel {

	/**
	 * jenis status Kepegawaian dari staf ahli
	 *
	 * @author arief
	 */
	public enum StatusPegawai {
		PEGAWAI_TETAP(0), PEGAWAI_TIDAK_TETAP(1);

		public final int value;

		StatusPegawai(int value) {
			this.value = value;
		}

		public boolean isTetap() {
			return this == PEGAWAI_TETAP;
		}

		public boolean isTidakTetap() {
			return this == PEGAWAI_TIDAK_TETAP;
		}

		public static StatusPegawai fromValue(Integer value) {
			StatusPegawai obj = null;
			if (value != null) {
				switch (value.intValue()) {
					case 0:
						obj = PEGAWAI_TETAP;
						break;
					case 1:
						obj = PEGAWAI_TIDAK_TETAP;
						break;
				}
			} else
				obj = PEGAWAI_TIDAK_TETAP;
			return obj;
		}
	}

	/**
	 * Jenis Kelamin dari staf Ahli
	 *
	 * @author ridho
	 */
	public enum JenisKelamin {
		PRIA('1'), WANITA('0');

		public final char value;

		JenisKelamin(char value) {
			this.value = value;
		}

		public static JenisKelamin fromValue(String value) {
			JenisKelamin obj = null;
			if (value != null && value.equals("0"))
				obj = WANITA;
			else
				obj = PRIA;
			return obj;
		}

		public boolean isPria() {
			return this == PRIA;
		}

		public boolean isWanita() {
			return this == WANITA;
		}
	}

	@Id(sequence="seq_staf_ahli", function="nextsequence")
	public Long sta_id;

	@Required
	public String sta_nama;
	@Required
	@As(binder=DateBinder.class)
	public Date sta_tgllahir;
	@Required
	public String sta_alamat;

	public String sta_jabatan;
	@Required
	public String sta_pendidikan;
	@Required
	public Integer sta_pengalaman;
	@Required
	public String sta_keahlian;
	@Email
	public String sta_email;

	public String sta_telepon;
	@Required
	public String sta_jenis_kelamin;


	public String sta_kewarganearaan;

	public Integer sta_status;
	//relasi ke Rekanan
	public Long rkn_id;

	public String sta_cv;
	// field baru 4.1.2
	public String sta_npwp;
	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;

	@Transient
	private Rekanan rekanan;
	@Transient
	public List<Cv_staf> pengalaman = new ArrayList<Cv_staf>();
	@Transient
	public List<Cv_staf> pendidikan = new ArrayList<Cv_staf>();
	@Transient
	public List<Cv_staf> sertifikasi = new ArrayList<Cv_staf>();
	@Transient
	public List<Cv_staf> bahasa = new ArrayList<Cv_staf>();

	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}

	@Override
	protected void postLoad() {
		// untuk handle variable yang tidak ter-initialize di JAVA 10
		pengalaman = new ArrayList<Cv_staf>();
		pendidikan = new ArrayList<Cv_staf>();
		sertifikasi = new ArrayList<Cv_staf>();
		bahasa = new ArrayList<Cv_staf>();
		
		if(!StringUtils.isEmpty(sta_cv)) {
			List<Cv_staf> list = CommonUtil.fromJson(sta_cv, new TypeToken<ArrayList<Cv_staf>>(){}.getType());
			if(!CollectionUtils.isEmpty(list)) {
				Cv_staf.Cv_kategori  kategori = null;
				for(Cv_staf o : list) {
					switch (o.kategori()) {
						case BAHASA: bahasa.add(o);break;
						case PELATIHAN:sertifikasi.add(o);break;
						case PENDIDIKAN:pendidikan.add(o);break;
						case PENGALAMAN:pengalaman.add(o);break;
					}
				}
			}
		}
	}

	@Override
	protected void prePersist() {
		List<Cv_staf> list = new ArrayList<>();
		list.addAll(pengalaman);
		list.addAll(pendidikan);
		list.addAll(sertifikasi);
		list.addAll(bahasa);
		if(list.size() > 0) {
			sta_cv = CommonUtil.toJson(list);
		}
		super.prePersist();
	}

	public void validate(List<String> cvWaktu, List <Long> cva_id, List <String> cvDetil, List <String>cvKat, String kategori) {
		StringBuilder param = new StringBuilder();
		int i=0;
		Cv_staf obj = null;
		for (Long id : cva_id) {
			if (String.valueOf(id) != "null" && id != null && ((cvDetil != null && cvDetil.get(i) != null && !cvDetil.get(i).isEmpty()) || (cvWaktu != null && cvWaktu.get(i) != null && !cvWaktu.get(i).isEmpty()))) {
				obj = new Cv_staf();
				obj.sta_id = sta_id;
				obj.dts_kategori = cvKat.get(i);
				obj.dts_uraian = cvDetil.get(i);
				obj.dts_waktu = kategori != "b" ? cvWaktu.get(i):null;
				switch (obj.kategori()){
					case PENDIDIKAN: pendidikan.add(obj);break;
					case PELATIHAN: sertifikasi.add(obj);break;
					case BAHASA: bahasa.add(obj);break;
					case PENGALAMAN:pengalaman.add(obj);break;
				}
			}
			i++;
		}
	}

	public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[7];
			int status = rs.getInt("sta_status");
			obj[0] = rs.getString("sta_id");
			obj[1] = rs.getString("sta_nama");
			obj[2] = FormatUtils.formatDateInd(rs.getDate("sta_tgllahir"));
			obj[3] = rs.getString("sta_pendidikan");
			obj[4] = rs.getString("sta_pengalaman");
			obj[5] = rs.getString("sta_keahlian");
			if (status == 0)
				obj[6] = "Pegawai Tetap" ;
			else
				obj[6] = "Pegawai Tidak Tetap" ;
			return obj;
		}
	};

	public static class Cv_staf {

		public enum Cv_kategori {
			PENGALAMAN("Pengalaman", 'a'), PENDIDIKAN("Pendidikan", 'd'), PELATIHAN("Sertifikat", 'l'), BAHASA("Bahasa", 'b');

			public final String label;

			public final char value;

			Cv_kategori(String label, char value) {
				this.label = label;
				this.value = value;
			}

			public static Cv_kategori fromValue(String value) {
				Cv_kategori kategori = null;
				switch (value) {
					case "a" : kategori = PENGALAMAN;break;
					case "d" : kategori = PENDIDIKAN;break;
					case "l" : kategori = PELATIHAN;break;
					case "b" : kategori = BAHASA;break;
				}
				return kategori;
			}

			public boolean isPengalaman() {
				return this == PENGALAMAN;
			}

			public boolean isPendidikan() {
				return this == PENDIDIKAN;
			}

			public boolean isPelatihan() {
				return this == PELATIHAN;
			}

			public boolean isBahasa() {
				return this == BAHASA;
			}
		}
		//
		public Long cva_id;

		public String dts_uraian;

		public String dts_waktu;

		public String dts_kategori;

		//relasi ke Staf
		public Long sta_id;
		// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
		public Long vms_id;

		public Cv_kategori kategori() {
			return Cv_kategori.fromValue(dts_kategori);
		}
	}

	@Override
	protected void preDelete() {
		Query.update("DELETE FROM cv_staf WHERE sta_id=?", sta_id);
	}

	@Override
	public String toString() {
		return "Staf_ahli [sta_status=" + sta_status + "]";
	}

}
