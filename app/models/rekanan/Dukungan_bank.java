package models.rekanan;

import ext.DateBinder;
import ext.RupiahBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.lelang.Peserta;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;


@Table(name="DUKUNGAN_BANK")
public class Dukungan_bank extends BaseModel {
	
	@Id
	public Long psr_id;

	@Id(sequence="seq_dukungan_bank", function="nextsequence")
	public Long dkb_id;

	public String dkb_no;
	
	@As(binder = DateBinder.class)
	public Date dkb_tanggal;

	public String dkb_namabank;
	
	@As(binder = RupiahBinder.class)
	public Double dkb_nilai;

	public Long dkb_id_attachment;	
	
	//relasi ke rekanan
//	public Long rkn_id;    //TODO: mengapa perlu field ? harus kroscek dengan SPSE 35
	
	@Transient
	private Peserta peserta;
	
	public Peserta getPeserta() {
		if(peserta == null)
			peserta = Peserta.findById(psr_id);
		return peserta;
	}
	
	@Transient
	public BlobTable getDokumen() {
		if(dkb_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dkb_id_attachment);
	}
	
	public static void simpanDukunganBankPeserta(Dukungan_bank dukungan_bank, Long pesertaId, File file) throws Exception
	{
		if(dukungan_bank == null)
			return;
		Dukungan_bank dkbToSave;
		if(dukungan_bank.dkb_id == null) {
			dkbToSave = new Dukungan_bank();
			dkbToSave.psr_id = pesertaId;
		} else {
			dkbToSave = Dukungan_bank.find("dkb_id = ?", dukungan_bank.dkb_id).first();
		}
		dkbToSave.dkb_namabank = dukungan_bank.dkb_namabank;
		dkbToSave.dkb_no = dukungan_bank.dkb_no;
		dkbToSave.dkb_tanggal = dukungan_bank.dkb_tanggal;
		dkbToSave.dkb_nilai = dukungan_bank.dkb_nilai;
//	    dukungan_bank.psr_id = pesertaId;
	    if(file != null && file.getName().length() >= 1)
	    {	
	        BlobTable blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, dkbToSave.dkb_id_attachment);
	        if(blob!=null && blob.blb_id_content != null)
	        	dkbToSave.dkb_id_attachment = blob.blb_id_content;
	    } 

	    dkbToSave.save();
	}
}
