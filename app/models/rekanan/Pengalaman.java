package models.rekanan;

import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;


@Table(name="PENGALAMAN")
public class Pengalaman extends BaseModel {

	@Id(sequence="seq_pengalaman", function="nextsequence")
	public Long pgn_id;
    @Required
	public String pgl_kegiatan;
	@Required
	public String pgl_lokasi;
	@Required
	public String pgl_pembtgs;

	public String pgl_almtpembtgs;

	public String pgl_telppembtgs;

	public String pgl_nokontrak;

	@As(binder=DateBinder.class)
	public Date pgl_tglkontrak;
	@Required	
	public Double pgl_nilai;
	@As(binder=DateBinder.class)
	public Date pgl_tglprogress;
    @Required
	public Float pgl_persenprogress;
	@As(binder=DateBinder.class)
	public Date pgl_slskontrak;
	@As(binder=DateBinder.class)
	public Date pgl_slsba;

	public Long pgl_id_attachment;

	public String pgl_keterangan;

	//relasi ke Rekanan
	public Long rkn_id;
	// id di SIKaP sebagai identifier kalau ada perubahan di SIKaP
	public Long vms_id;
	
	@Transient
	private Rekanan rekanan;
	
	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	// pekerjaan sedang berjalan
	public boolean isSedangBerjalan() {
		return pgl_persenprogress != null && pgl_persenprogress.intValue() < 100;
	}
	
	// pekerjaan sudah selesai 100 % dan dianggap pengalaman
	public boolean isSelesai() {
		return pgl_persenprogress != null && pgl_persenprogress.intValue() == 100;
	}
	
	/**
	 * Fungsi {@code findSemuaByNamaUser} digunakan untuk mendapatkan semua data
	 * pengalaman rekanan tertentu berdasarkan nama user rekanan
	 *
	 * @param username user id rekanan
	 * @return semua daftar pengalaman rekanan dengan user id sesuai dengan
	 *         parameter {@code username}
	 */
	public static List<Pengalaman> findSemuaByNamaUser(String username) {
		Rekanan rekanan = Rekanan.findByNamaUser(username);
		return find("rkn_id = ?", rekanan.rkn_id).fetch();
	}
	
	public static final ResultSetHandler<String[]> resulset = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[7];
			obj[0] = rs.getString("pgn_id");
			obj[1] = rs.getString("pgl_kegiatan");
			obj[2] = rs.getString("pgl_lokasi");
			obj[3] = rs.getString("pgl_pembtgs");
			obj[4] = rs.getString("pgl_almtpembtgs");
			obj[5] = FormatUtils.formatDateInd(rs.getDate("pgl_tglkontrak"));
			obj[6] = FormatUtils.formatDateInd(rs.getDate("pgl_slskontrak"));			
			return obj;
		}
	};
}
