package models;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;
import play.libs.Json;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;


public final class Datatable {

    class Column {
        public final String nama;
        public final ColumnType type;

        public Column(String nama, ColumnType type){
            this.nama = nama;
            this.type = type;
        }
    }

    enum ColumnType {
        NUMERIC, STRING
    }

    final QueryBuilder from  = new QueryBuilder();
    final QueryBuilder select = new QueryBuilder();
    final List<Column> columns = new ArrayList<>();

    public Datatable select(String quey, Object... param) {
        select.append(quey, param);
        return this;
    }

    public Datatable from(String quey, Object... param) {
        from.append(quey, param);
        return this;
    }

    public Datatable filterAnd(String quey, Object... param) {
        from.append(from.query().toLowerCase().contains("where") ? " AND " : " WHERE ");
        from.append(quey, param);
        return this;
    }

    public Datatable filterOR(String quey, Object... param) {
        from.append(from.query().toLowerCase().contains("where") ? " OR " : " WHERE ");
        from.append(quey, param);
        return this;
    }

    // append column type string, default
    public Datatable column(String... columns) {
        for(String o : columns)
            this.columns.add(new Column(o, ColumnType.STRING));
        return this;
    }
    // append column type numeric, default
    public Datatable columnNumber(String... columns) {
        for(String o : columns)
            this.columns.add(new Column(o, ColumnType.NUMERIC));
        return this;
    }

    public static Datatable create() {
        return new Datatable();
    }

    public JsonObject render(ResultSetHandler<String[]> resultset) {
        Scope.Params params = Scope.Params.current();
        // PAGING
        final Integer start = params.get("start", Integer.class);
        final Integer length = params.get("length", Integer.class);
        QueryBuilder queryCount = new QueryBuilder("SELECT count("+ columns.get(0).nama+") ").append(from);
        long iTotal = Query.count(queryCount);
        // FILTERING
        String search = params.get("search[value]");
        QueryBuilder filter = new QueryBuilder();
        if (!StringUtils.isEmpty(search)) {
            search= StringEscapeUtils.escapeHtml4(search.trim().toLowerCase());
            String searchNumber=search.replaceAll(",", "");
            for (int i = 0; i < columns.size(); i++) {
                boolean searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
                if (searchable) {
                    if(columns.get(i).type == ColumnType.STRING) {//jika angka, jangan pakai LIKE
                        filter.append(!filter.isEmpty()?" OR ":"")
                                .append("tsv @@ plainto_tsquery('english',?) ", search);
                    }
                    else if(columns.get(i).type == ColumnType.NUMERIC && NumberUtils.isNumber(searchNumber)) {  //pakai = dan untuk angka
                        filter.append(!filter.isEmpty()?" OR ":"")
                                .append(columns.get(i).nama+"=? ", Double.parseDouble(searchNumber));
                    }
                }
            }
        }
        if (!filter.isEmpty()) {
//            Logger.info(filter.query());
            from.append(from.query().toLowerCase().contains("where") ? " AND " : " WHERE ");
            from.append("(").append(filter).append(")");
        }
        // INDIVIDUAL COLUMN FILTERING
        boolean searchable = false;
        String column_search = null;
        filter.reset();
        for (int i = 0; i < columns.size(); i++) {
            searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
            column_search = StringEscapeUtils.escapeHtml4(params.get("columns[" + i + "][search][value]"));
            if (searchable && !StringUtils.isEmpty(column_search)) {
                if(columns.get(i).type == ColumnType.STRING) {//jika angka, jangan pakai LIKE
                    filter.append(!filter.isEmpty()?" OR ":"")
                            .append("tsv @@ plainto_tsquery('english',?) ", search);
                }
                else if(columns.get(i).type == ColumnType.NUMERIC && NumberUtils.isNumber(column_search)) {  //pakai = dan untuk angka
                    filter.append(!filter.isEmpty()?" OR ":"").append(columns.get(i).nama+"=? ", new Double(column_search));
                }
            }
        }
        if (!filter.isEmpty()) {
            from.append(from.query().toLowerCase().contains("where") ? " AND " : " WHERE ");
            from.append("(").append(filter).append(")");
        }
        queryCount.reset();
        queryCount.append("SELECT count("+ columns.get(0).nama+") ").append(from);
        long iFilteredTotal = Query.count(queryCount);
        // ORDERING
        String requestColumn = null;
        int columnIdx = 0;
        boolean firstOrder = true;
        for (int i = 0; i < columns.size(); i++) {
            requestColumn = StringEscapeUtils.escapeHtml4(params.get("order[" + i + "][column]"));
            if (StringUtils.isEmpty(requestColumn))
                continue;
            columnIdx = Integer.parseInt(requestColumn);
            boolean orderable = Boolean.parseBoolean(params.get("columns[" + i + "][orderable]"));
            if (orderable) {
                String order_status = params.get("order[" + i + "][dir]") != null ? params.get("order[" + i + "][dir]").toLowerCase():"";
                if (firstOrder) {
                    from.append(" ORDER BY ").append(columns.get(columnIdx).nama).append(order_status.contains("desc")? " desc ":" asc ");
                    firstOrder = false;
                } else {
                    from.append(",").append(columns.get(columnIdx).nama).append(order_status.contains("desc")? " desc ":" asc ");
                }
            }
        }
        if (start != null && length >= 0) {
            from.append("\n OFFSET ? LIMIT ?", start, length);
        }
        select.append(from);
//        Logger.info(select.query());
        List<String[]> data = Query.find(select, resultset).fetch();
        JsonObject output = new JsonObject();
        output.addProperty("draw", params.get("draw"));
        output.addProperty("recordsTotal", iTotal);
        output.addProperty("recordsFiltered", iFilteredTotal);
        output.add("data", Json.toJsonTree(data));
        return output;
    }

}
