package models.dce2;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import play.Logger;
import play.db.DB;

import java.io.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Uji coba
 https://lpse.surabaya.go.id/eproc/appwsdce/json/MnRmZHBCRk9OVFU4UUUxRGVrQTZaOERPQVlYbzVjamVnRHZXNXpjcTFuRG5ndnVlMlRmZkhweURyVGpSMVBtb2syQXRDbStiYlVNVA0Kc3l5UTNlTkNhbys1dWt5Q1BEaEdMV3RrSXgyVFNGaEFYM21rQVRNSmQzNEw3TGs4bmJHbms1LzkyMGpwMjI0dVBoRDU5TEYvcmRGWA0KWVhYUUhrVFhxcWVpMS9mZXN1WWZTLzhTZ0lHU3lwT3NodndmU2RHODUvOHREbjFkWE1OMXBjOUtYOWd2MDdpQWdjcTJEVHd2clp5aA0KOEtGN0dLZUZIWUtoNmpaTjlhTkVCQTVoNzI5WlkxNUxmS2RYellIRlBaYTcwTmJiWE1GWlJUZThCazJyb21DMjNoTERXazBJeGxoeQ0KUlRncitka3hIMkw4V3FGemd2TytVUXVvdFhibEVhQUZzYUx4OFdocU9EbG5lRTJUb2RKTm5vVU44ZTA9DQo
 */
/**
 * DCE Service versi 2.1
 * 
 * Ini merupakan improvement dari EpnsDataService2<br> 
 * 1. kembalian berupa CSV, dan	GZ sehingga ukuran file kecil 
 * 2. perubahan Parameter web service<br>
 * 
 * @author Mr. Andik
 *
 *  Tidak dipakai, sekedar copy dari SPSE3
 */
@Deprecated
public class EpnsDataService2_1 {	
	
	public static File getDataCsv(String query) throws FileNotFoundException, IOException, SQLException {
		BaseConnection pgConn = (BaseConnection) DB.getConnection();

		//Gunakan Copy manager untuk mendapat data dalam format CSV dan CEPAT
		CopyManager cm = new CopyManager(pgConn);
		String sql = String.format("copy (%s) to stdout  CSV HEADER", query);
		/*
		 * Simpan hasil di temporary folder. Nama file sudah ditentukan dan akan
		 * di-delete setelah didownload. Jika tidak didownload maka file tetap
		 * ada dan akan di-replace saat query berikutnya. Sengaja tidak
		 * di-generate dengan random filename supaya tidak banyak sampah karena
		 * java tidak bisa delete file sampah secara otomatis
		 * 
		 * 
		 */
		//dapatkan nama table
		Pattern pattern=Pattern.compile("select.*from (\\w+).*$");
		Matcher m= pattern.matcher(query);
		String table;
		if(m.matches())
			table = m.group(1);
		else
			table=UUID.randomUUID().toString();
		File file = new File(String.format("%s/spse.%s.csv.gz", FileUtils.getTempDirectoryPath(), table));

		StopWatch sw = new StopWatch();
		sw.start();
		OutputStream out = new BufferedOutputStream(new FileOutputStream(file), 1024 * 10);
		cm.copyOut(sql, out);
		out.close();
		sw.stop();
		Logger.debug(String.format("SQL %s, csv size: %,d, duration: %s, %s", query, file.length(), sw, file));
		return file;
	}

	/**
	 * Dapatkan struktur (nama-nama kolom) dari tabel, output CSV dengan kolom:
	 * name, type, pk contoh: name , type, size, pk lls_id, 12 , 1 , false
	 * lls_id, 12 , 255 , true untuk type bisa dilihat di: java.sql.Types
	 * 
	 * @param table
	 */
	public static String getStructure(String table) {
		try {
			Connection conn = DB.getConnection();
			DatabaseMetaData md = conn.getMetaData();

			// dapatkan PK
			ResultSet rsPk = md.getPrimaryKeys(conn.getCatalog(), null, table);
			Set<String> setPk = new HashSet<String>();
			while (rsPk.next())
				setPk.add(rsPk.getString("COLUMN_NAME").toLowerCase());
			rsPk.close();

			// dapatkan kolom
			ResultSet rs = md.getColumns(conn.getCatalog(), null, table, "");
			StringBuilder str = new StringBuilder(1024);
			// col header
			str.append("name,type,size,pk\n");
			while (rs.next()) {
				String colName = rs.getString("COLUMN_NAME");
				str.append(colName).append(',');
				str.append(rs.getString("DATA_TYPE")).append(',');
				str.append(rs.getString("COLUMN_SIZE")).append(',');
				if (setPk.contains(colName))
					str.append("true\n");
				else
					str.append("false\n");
			}
			return str.toString();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

	}
}