package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.jcommon.blob.BlobTable;

public class SurveyHargaPlDownloadHandler implements DownloadSecurityHandler {
	/**
	 * TODO: Seharusnya semua yang boleh download file didefinisikan di sini
	 * @param secureIdBlobTable objek {@link BlobTable}
	 * @return nilai true memperbolehkan download
	 */
	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		return true;
	}
}
