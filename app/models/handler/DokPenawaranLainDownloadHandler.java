package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.agency.Paket;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.lelang.Dok_penawaran;
import models.lelang.Peserta;
import models.rekanan.Rekanan;

/**
 * Created by Lambang on 4/19/2016.
 */

public class DokPenawaranLainDownloadHandler implements DownloadSecurityHandler {

    @Override
    public boolean allowDownload(BlobTable secureIdBlobTable) {
        Active_user active_user = Active_user.current();
        if(active_user == null)
			return false;
        Dok_penawaran dok_penawaran = Dok_penawaran.find("dok_id_attachment=?", secureIdBlobTable.blb_id_content).first();

        if(dok_penawaran  != null) {
            Peserta peserta = Peserta.findById(dok_penawaran.psr_id);

            if (active_user.isRekanan()) {
                Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
                if (rekanan != null){
                    return rekanan.rkn_id == active_user.rekananId;
                }
            }
            if (active_user.isPanitia()) {
                Paket paket = Paket.find("pkt_id IN (select pkt_id from lelang_seleksi where lls_id=?) and pnt_id IN (select pnt_id from anggota_panitia where peg_id=?)", peserta.lls_id, active_user.pegawaiId).first();
                return paket != null;
            }
        }

        return active_user.isAuditor();
    }
}
