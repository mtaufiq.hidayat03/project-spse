package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;

public class DokPenawaranDownloadHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		if(active_user.isPanitia() || active_user.isAuditor())
			return true;
		return false;
	}

}
