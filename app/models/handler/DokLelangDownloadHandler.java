package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.jcommon.blob.BlobTable;
/**
 * handler untuk dok_lelang
 * @author arief Ardiyansah
 *
 */
public class DokLelangDownloadHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
//		Active_user active_user = Active_user.current();
//		if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
//			Dok_lelang dok_lelang = Dok_lelang.find("dll_id_attachment=?", secureIdBlobTable.blb_id_content).first(); // untuk spse 3
//			Dok_lelang_content dok_lelang_content = Dok_lelang_content.find("dll_content_attachment=? OR dll_spek=?", secureIdBlobTable.blb_id_content, secureIdBlobTable.blb_id_content).first(); // untuk SPSE 4
//			if(dok_lelang_content != null && dok_lelang == null) {
//				dok_lelang = Dok_lelang.findById(dok_lelang_content.dll_id);
//			}
//			if(dok_lelang != null) {
//				Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", dok_lelang.lls_id, active_user.rekananId).first();
//				return peserta != null;
//			}
//		}
//		return active_user.isAuditor() || active_user.isPpk() || active_user.isPanitia();
		return true;// sementara dibuat true karena ada issue beda domain tapi 1 aplikasi
	}

}
