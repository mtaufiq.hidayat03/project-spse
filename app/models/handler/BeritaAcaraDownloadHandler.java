package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.lelang.Berita_acara;
import models.lelang.Peserta;

/**
 * handle untuk berita acara
 * @author arief Ardiyansah
 *
 */
public class BeritaAcaraDownloadHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {	
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
			Berita_acara berita_acara = Berita_acara.find("brc_id_attachment=?", secureIdBlobTable.blb_id_content).first();
			if(berita_acara != null) {
				Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", berita_acara.lls_id, active_user.rekananId).first();
				return peserta != null;
			}
		}	
		return active_user.isAuditor() || active_user.isPpk() || active_user.isPanitia();
	}

}
