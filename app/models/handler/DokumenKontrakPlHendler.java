package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.agency.KontrakPl;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.kontrak.PesananPl;

public class DokumenKontrakPlHendler implements DownloadSecurityHandler {

    private static final String WHERE = "kontrak_id_attacment=:id OR kontrak_id_attacment2=:id OR kontrak_sskk_attacment=:id "
            + "OR kontrak_id IN (SELECT kontrak_id FROM ekontrak.pesanan WHERE pes_attachment=:id) "
            + "OR kontrak_id IN (SELECT kontrak_id FROM ekontrak.ba_pembayaran WHERE cetak_bap_attachment=:id OR cetak_bast_attachment=:id)";

    @Override
    public boolean allowDownload(BlobTable secureIdBlobTable) {
        Active_user active_user = Active_user.current();
        if(active_user == null)
            return false;
        if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
            KontrakPl kontrak = KontrakPl.find(WHERE).setParameter("id", secureIdBlobTable.blb_id_content).first();
            if(kontrak != null) {
                PesananPl peserta = PesananPl.find("lls_id=? and rkn_id=?", kontrak.lls_id, active_user.rekananId).first();
                return peserta != null;
            }
        }
        return active_user.isAuditor() || active_user.isPpk() || active_user.isPanitia() || active_user.isPP();
    }
}
