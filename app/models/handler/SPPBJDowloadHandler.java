package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.agency.Sppbj;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;

/**
 * handler untuk SPPBJ
 * @author arief Ardiyansah
 *
 */

public class SPPBJDowloadHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		if(active_user.isRekanan()) { // hanya pemenang lelang		
			Sppbj sppbj = Sppbj.find("sppbj_attachment=? and rkn_id=?", secureIdBlobTable.blb_id_content, active_user.rekananId).first();
			return sppbj != null;
		}
		return active_user.isAuditor() || active_user.isPpk();
	}

}
