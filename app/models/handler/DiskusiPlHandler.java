package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.lelang.Diskusi_lelang;
import models.lelang.Peserta;
import models.nonlelang.Diskusi_lelangPl;
import models.nonlelang.PesertaPl;

public class DiskusiPlHandler implements DownloadSecurityHandler {
	
	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		else if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
			Diskusi_lelangPl diskusi = Diskusi_lelangPl.find("dsl_id_attachment=?", secureIdBlobTable.blb_id_content).first();
			if(diskusi != null) {
				PesertaPl peserta = PesertaPl.find("lls_id=? and rkn_id=?", diskusi.lls_id, active_user.rekananId).first();
				return peserta != null;
			}
		}
		return active_user.isAuditor() || active_user.isPanitia();
	}
}
