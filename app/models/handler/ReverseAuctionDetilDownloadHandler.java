package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.lelang.Peserta;
import models.lelang.ReverseAuction;
import models.lelang.ReverseAuctionDetil;

/**
 * handler untuk reverse_auction_detail
 * @author rezaprima
 *
 */
public class ReverseAuctionDetilDownloadHandler implements DownloadSecurityHandler {

    @Override
    public boolean allowDownload(BlobTable secureIdBlobTable) {
		Active_user active_user = Active_user.current();
		if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
			ReverseAuctionDetil rad = ReverseAuctionDetil.find("rad_id_attachment=?", secureIdBlobTable.blb_id_content).first(); // untuk spse 3
			if(rad != null) {
			    ReverseAuction ra = ReverseAuction.findById(rad.ra_id);
			    Long lls_id = ra.lls_id;
				Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", lls_id, active_user.rekananId).first();
				return peserta != null;
			}
		}
        return active_user.isPanitia();
    }

}
