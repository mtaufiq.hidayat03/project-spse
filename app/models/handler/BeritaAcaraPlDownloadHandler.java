package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.nonlelang.Berita_acara_pl;
import models.nonlelang.PesertaPl;

/**
 * Created by rayhanfrds on 2/21/17.
 */
public class BeritaAcaraPlDownloadHandler implements DownloadSecurityHandler {

    @Override
    public boolean allowDownload(BlobTable secureIdBlobTable) {
        Active_user active_user = Active_user.current();
        if (active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
            Berita_acara_pl berita_acara = Berita_acara_pl.find("brc_id_attachment=?", secureIdBlobTable.blb_id_content.toString()).first();
            if (berita_acara != null) {
                PesertaPl peserta = PesertaPl.find("lls_id=? and rkn_id=?", berita_acara.lls_id, active_user.rekananId).first();
                return peserta != null;
            }
        }
        return active_user.isAuditor() || active_user.isPP() || active_user.isPpk() || active_user.isPanitia();
    }
}
