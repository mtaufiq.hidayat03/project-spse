package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.agency.Paket_pl;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.nonlelang.PesertaPl;
import models.rekanan.DukunganBankPl;
import models.rekanan.Rekanan;

/**
 * Created by Lambang on 4/19/2016.
 */
public class DokDukunganBankPlDownloadHandler implements DownloadSecurityHandler {

    @Override
    public boolean allowDownload(BlobTable secureIdBlobTable) {
        Active_user active_user = Active_user.current();
        if(active_user == null)
			return false;
        DukunganBankPl dukungan_bank = DukunganBankPl.find("dkb_id_attachment=?", secureIdBlobTable.blb_id_content).first();

        if(dukungan_bank  != null) {
            PesertaPl peserta = PesertaPl.findById(dukungan_bank.psr_id);
            
            if (active_user.isRekanan()) {
                Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
                if (rekanan != null){
                    return rekanan.rkn_id == active_user.rekananId;
                }
            }
            if (active_user.isPanitia()) {
                Paket_pl paket = Paket_pl.find("pkt_id IN (select pkt_id from ekontrak.nonlelang_seleksi where lls_id=?) and pnt_id IN (select pnt_id from anggota_panitia where peg_id=?)", peserta.lls_id, active_user.pegawaiId).first();
                return paket != null;
            }
        }

        return active_user.isAuditor();
    }
}
