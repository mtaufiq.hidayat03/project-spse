package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.nonlelang.DokPenawaranPl;
import models.nonlelang.PesertaPl;

/**
 * Created by Lambang on 3/8/2017.
 */
public class DokPenawaranPlDownloadHandler implements DownloadSecurityHandler {
    @Override
    public boolean allowDownload(BlobTable secureIdBlobTable) {
        Active_user active_user = Active_user.current();
        if (active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
            DokPenawaranPl dokPenawaranPl = DokPenawaranPl.find("dok_id_attachment=?", secureIdBlobTable.blb_id_content).first();
            if (dokPenawaranPl != null) {
                PesertaPl peserta = PesertaPl.find("psr_id=? and rkn_id=?", dokPenawaranPl.psr_id, active_user.rekananId).first();
                return peserta != null;
            }
        }
        return active_user.isAuditor() || active_user.isPP() || active_user.isPpk() || active_user.isPanitia();
    }
}
