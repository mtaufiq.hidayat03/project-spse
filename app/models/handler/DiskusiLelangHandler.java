package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.lelang.Diskusi_lelang;
import models.lelang.Peserta;

public class DiskusiLelangHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		else if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
			Diskusi_lelang diskusi = Diskusi_lelang.find("dsl_id_attachment=?", secureIdBlobTable.blb_id_content).first();
			if(diskusi != null) {
				Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", diskusi.lls_id, active_user.rekananId).first();
				return peserta != null;
			}
		}
		return active_user.isAuditor() || active_user.isPanitia();
	}

}
