package models.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.jcommon.blob.BlobTable;

/**
 * Kelas {@code BeritaDownloadHandler} digunakan untuk handling download File yang berhubungan dengan
 * entity {@link models.common.Berita}
 *
 * Created by IntelliJ IDEA.
 * Date: 12/12/12
 * Time: 17:35
 * @author I Wayan Wiprayoga W
 */
public class BeritaDownloadHandler implements DownloadSecurityHandler {
	/**
	 * TODO: Seharusnya semua yang boleh download file didefinisikan di sini
	 * @param secureIdBlobTable objek {@link BlobTable}
	 * @return nilai true memperbolehkan download
	 */
	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		return true;
	}
}
