package models.kontrak;

import models.jcommon.db.base.BaseModel;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
/**
 * Domain class untuk Table Pesanan
 * @author retno
 *
 */
@Table(name="PESANAN_BARANG")
public class Pesanan_barang extends BaseModel {
	
	@Id(sequence="seq_barang", function="nextsequence")
	public Long brg_id;
	
	//relasi ke Pesenan_barang
	@Required
	public Long pes_id;
		
	public String nama_brng;

	public String satuan ;
	
	public Long kuantitas;
	
	public Long harga_satuan;
	
	public Long total;
	
	@Transient
	private Pesanan_barang pesanan;

	public Pesanan_barang getPesanan() {
		if(pesanan == null)
			pesanan = Pesanan.findById(pes_id);
		return pesanan;
	}
	
}
