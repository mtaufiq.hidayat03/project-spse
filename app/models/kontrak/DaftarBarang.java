package models.kontrak;

import ext.DecimalBinder;
import play.data.binding.As;

import java.io.Serializable;
import java.util.List;

public class DaftarBarang implements Serializable {

	public List<Rincian_Barang> items;
	
	
	public Double total_pembayaran;
	
	public Double total_ppn; // total ppn (10 % dari total pembayaran)

	@As(binder= DecimalBinder.class)
	public Double total; // total nilai;
	

}
