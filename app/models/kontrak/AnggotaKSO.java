package models.kontrak;

import play.data.validation.Required;

import java.io.Serializable;

public class AnggotaKSO implements Serializable {
    @Required(message="Nama Anggota wajib di isi")
    public String nama_anggota;
    public String alamat;
    public AnggotaKSO(String _nama_anggota){
        this.nama_anggota = _nama_anggota;
    }
}
