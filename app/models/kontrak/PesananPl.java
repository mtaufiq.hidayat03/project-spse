package models.kontrak;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.KontrakPl;
import models.agency.Pegawai;
import models.agency.Ppk;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 3/24/2017.
 */
@Table(name="EKONTRAK.PESANAN")
public class PesananPl extends BaseModel {

    static final Template TEMPLATE_PESANAN_BARANG_PL = TemplateLoader.load("/kontrakpl/template/SuratPesananBarang-pl.html");
    static final Template TEMPLATE_PENGADAAN_LANGSUNG = TemplateLoader.load("/kontrakpl/template/spmk-pengadaanlangsung.html");
    static final Template TEMPLATE_PENUNJUKAN_LANGSUNG = TemplateLoader.load("/kontrakpl/template/spmk-penunjukanlangsung.html");

    
    @Id(sequence="ekontrak.seq_pesanan_pl", function="nextsequence")
    public Long pes_id;

    //relasi ke Kontrak
    @Required
    public Long kontrak_id;

    @Required
    public String pes_no;


    @As(binder= DateBinder.class)
    public Date pes_tgl;


    /**
     * data form isian pesanan
     * format json
     * model models.kontrak.PesananContent
     */
    public String pes_content;

    public Long pes_attachment;

    @Transient
    public PesananContent getPesananContent() {
        return CommonUtil.fromJson(pes_content, PesananContent.class);
    }

    @Transient
    private KontrakPl kontrak;

    public KontrakPl getKontrak() {
        if(kontrak == null)
            kontrak = KontrakPl.findById(kontrak_id);
        return kontrak;
    }

    public static PesananPl findByKontrak(Long kontrak_id){
        return find("kontrak_id=?", kontrak_id).first();
    }


    /**
     * data form isian pesanan
     * format json
     * model models.kontrak.DaftarBarang
     */

    public String pes_barang;

    @Transient
    public DaftarBarang db;

    @Transient
    public BlobTable getBlob() {
        if(pes_attachment == null)
            return null;
        return BlobTableDao.getLastById(pes_attachment);
    }

    @Transient
    public List<BlobTable> getDokumen() {
        if(pes_attachment == null)
            return null;
        return BlobTableDao.listById(pes_attachment);
    }

    public static InputStream cetak(KontrakPl kontrak, Pl_seleksi pl, Long pesananId) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format", new FormatUtils());
        model.put("pl", pl);
        Active_user userPPK = Active_user.current();
        Ppk ppk = Ppk.findById(userPPK.ppkId);
        model.put("ppk", ppk);

        model.put("pegawai", Pegawai.find("peg_id=?", ppk.peg_id).first());
        model.put("kontrak", kontrak);
        PesananPl pesanan = PesananPl.findById(pesananId);
        PesananContent content = pesanan.getPesananContent();
        model.put("pesanan", pesanan);
        model.put("content",content);
        PesertaPl peserta = PesertaPl.findBy(pl.lls_id, kontrak.rkn_id);
        model.put("pemenang", peserta.getRekanan());
        if(pl.isJasaKonsultansi()) {
        	model.put("isJasaKonsultansi", "Jasa Konsultansi");
        }
        if(pesanan.pes_barang!=null){
            DaftarBarang db = CommonUtil.fromJson(pesanan.pes_barang, DaftarBarang.class);
            List<Rincian_Barang> rincian = db.items;
            model.put("db", db);
            model.put("rincian", rincian);
        }
        if (pl.getKategori().isBarang()){
            return HtmlUtil.generatePDF(TEMPLATE_PESANAN_BARANG_PL, model);
        } 
//        else if (pl.getKategori().isKonstruksi()){
//            return HtmlUtil.generatePDF(TEMPLATE_TENDER_PERMEN_PL, model);
//        }
        else {
        	if(pl.getPemilihan().isPenunjukanLangsung()) {
        		return HtmlUtil.generatePDF(TEMPLATE_PENUNJUKAN_LANGSUNG, model);
        	}
        	else {
        		return HtmlUtil.generatePDF(TEMPLATE_PENGADAAN_LANGSUNG, model);
        	}
        	
        }
    }

    public UploadInfo simpanDok(File file) throws Exception {
        BlobTable blob = null;
        if (pes_id != null) {
            // sppbj.sppbj_attachment = Long.parseLong("123");
            if (pes_attachment == null) {
                // jika baru upload file, gunakan fungsi save file dengan 2 parameter
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
            } else if (pes_attachment != null) {
                // jika update file yang di-upload gunakan fungsi save file dengan 3 parameter
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, pes_attachment);
            }
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }
        pes_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

}
