package models.kontrak;

import ext.DateBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;

/**
 * Created by Lambang on 3/31/2017.
 */
@Table(name = "EKONTRAK.DOK_LAIN")
public class DokLainPl extends BaseModel {

    @Id(sequence="ekontrak.seq_dok_lain_pl", function="nextsequence")
    public Long dlp_id;

    @Required
    public Long lls_id;

    @Required
    public String dlp_nama_dokumen;

    @Required
    public String dlp_nomor_dokumen;

    @As(binder= DateBinder.class)
    public Date dlp_tanggal_dokumen;

    public String dlp_keterangan;

    public Long dlp_id_attachment;

    public static DokLainPl findByPl(Long lelangId){
        return find("lls_id=?", lelangId).first();
    }

    @Transient
    public BlobTable getBlob() {
        if(dlp_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dlp_id_attachment);
    }

    public static void simpanDokLain(DokLainPl dokLainPl, File dlpAttachment, String hapus) throws Exception {

        if (dlpAttachment != null) {

            BlobTable bt = null;

            if (dokLainPl.dlp_id == null)
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment);
            else
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment, dokLainPl.dlp_id_attachment);



            dokLainPl.dlp_id_attachment = bt.blb_id_content;

        } else {

            if (dokLainPl.dlp_id_attachment != null && hapus == null) {

                Long id = dokLainPl.dlp_id_attachment;

                dokLainPl.dlp_id_attachment = null;

                BlobTable.delete("blb_id_content=?", id);

            }

            if (hapus != null)
                dokLainPl.dlp_id_attachment = Long.valueOf(hapus);

        }

        dokLainPl.save();

    }

}
