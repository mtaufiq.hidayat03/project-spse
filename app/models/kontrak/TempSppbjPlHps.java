package models.kontrak;

import models.agency.DaftarKuantitas;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="EKONTRAK.TEMP_SPPBJ_HPS")
public class TempSppbjPlHps extends BaseTable {

    @Id
    public Long lls_id;
    @Id
    public Long ppk_id;

    public Long psr_id;
    public String dkh;

    public static void simpanHps(Long llsId, Long psrId, String data) throws Exception{
        DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,true);
        Long ppkId = Active_user.current().ppkId;

        TempSppbjPlHps tmp = findByPlAndPpk(llsId);
        if(tmp == null){
            tmp = new TempSppbjPlHps();
            tmp.ppk_id = ppkId;
            tmp.lls_id = llsId;
        }

        tmp.psr_id = psrId;
        tmp.dkh = CommonUtil.toJson(dk);
        tmp.save();
    }

    public static TempSppbjPlHps findByPlAndPpk(Long llsId){
        return TempSppbjPlHps.find("lls_id = ? and ppk_id = ?", llsId, Active_user.current().ppkId).first();
    }
}
