package models.kontrak;

import ext.DateBinder;
import ext.DatetimeBinder;
import play.data.binding.As;
import play.data.validation.Required;

import java.io.Serializable;
import java.util.Date;
/**
 * model untuk Content Pesanan pada Econtract
 * @author retno
 *
 */
public class PesananContent implements Serializable {

	@Required
	public String wakil_sah_rekanan;

	@Required
	public String jabatan_wakil_rekanan;
	
	public String rincian_barang;
	
	@As(binder=DateBinder.class)
	public Date tgl_brng_diterima;

	@Required
	public String waktu_penyelesaian;
	
	@As(binder = DatetimeBinder.class)
	public Date tgl_pekerjaan_selesai;

	public String alamat_kirim;

	@Required
	public String kota_pesanan;	
	
	public String lingkup_pekerjaan;	

	public String jaminan;

}
