package models.kontrak;

import java.io.Serializable;
import java.util.Date;

public class Kontrak_kso implements Serializable {
	
	 public String anggota_kso;
	 
	 public String wakil_kso;

	 public String jabatan_wakil_kso;

	 public String alamat_kso;
	 
	 public String no_kso;
	 
	 public Date tgl_surat_kso;
}