package models.kontrak;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.lelang.Lelang_seleksi;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Max;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Domain class untuk Table Pesanan
 * @author retno
 *
 */
@Table(name="BA_PEMBAYARAN")
public class BA_Pembayaran extends BaseModel {
	
	@Id(sequence="seq_ba_pembayaran", function="nextsequence")
	public Long bap_id;

	// Menambahkan pesan eror kalau data tidak diisi, digunakan di form-ba-pembayaran.html by Hodeifa A.B.
	// Penambahan pada field bap_no, bap_tgl, bast_no, bast_tgl,besar_pembayaran, pphp_id
	@Required (message="No. BAP wajib diisi")
	public String bap_no;
	
	@Required (message="Tanggal BAP wajib diisi")
	@As(binder=DateBinder.class)
	public Date bap_tgl;	
	

	@Required (message="No. BAST wajib diisi")
	public String bast_no; 
	
	@Required (message="Tanggal BAST wajib diisi")
	@As(binder=DateBinder.class)
	public Date bast_tgl;	
	
	@Max(100)
	public Long progres_fisik;
	
	@Required (message="Besar Pembayaran wajib diisi")
	public Double besar_pembayaran;

	public Long pphp_id;

	public String pphp_nama;

	public Long pphp_nip;
	
	//relasi ke Kontrak
	@Required
	public Long kontrak_id;

	public String jabatan_penandatangan_sk;

	public String kontrak_wakil_penyedia;

	public String kontrak_jabatan_wakil;

	public Long bap_id_attachment;
	public Long cetak_bast_attachment;
	public Long cetak_bap_attachment;
	
	@Transient
	private Pegawai pegawai;
	@Transient
	private Kontrak kontrak;
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(pphp_id);
		return pegawai;
	}
	public Kontrak getKontrak() {
		if(kontrak == null)
			kontrak = Kontrak.findById(kontrak_id);
		return kontrak;
	}
	
	@Transient
	public BlobTable getBlob() {
		if(bap_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(bap_id_attachment);
	}
	
	@Transient
	public BlobTable getBlobBast() {
		if(cetak_bast_attachment == null)
			return null;
		return BlobTableDao.getLastById(cetak_bast_attachment);
	}

	@Transient
	public List<BlobTable> getDokumenBast() {
		if(cetak_bast_attachment == null){
			return null;
		}
		return BlobTableDao.listById(cetak_bast_attachment);
	}
	
	@Transient
	public BlobTable getBlobBap() {
		if(cetak_bap_attachment == null)
			return null;
		return BlobTableDao.getLastById(cetak_bap_attachment);
	}

	@Transient
	public List<BlobTable> getDokumenBap() {
		if(cetak_bap_attachment == null){
			return null;
		}
		return BlobTableDao.listById(cetak_bap_attachment);
	}

	public static BA_Pembayaran findByKontrak(Long kontrak_id){
		return find("kontrak_id=?", kontrak_id).first();
	}

	public static InputStream cetak(Long bapId) {
		BA_Pembayaran bap =BA_Pembayaran.findById(bapId);
		Kontrak kontrak = Kontrak.findById(bap.kontrak_id);
		Kontrak_kso kso = kontrak.getKontrakKSO();
		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(kontrak.lls_id);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("format", new FormatUtils());
		model.put("kso",kso);
		model.put("lelang", lelang_seleksi);
		Sppbj sppbj = Sppbj.find("lls_id=?", kontrak.lls_id).first();
		model.put("sppbj", sppbj);
		model.put("kontrak",kontrak);
		model.put("bast",bap);
		Active_user userPPK = Active_user.current();
		Ppk ppk = Ppk.findById(userPPK.ppkId);
		model.put("ppk", ppk);
		// TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
		Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM paket_satker WHERE pkt_id=?)", lelang_seleksi.pkt_id).first();
		model.put("satker", satker);
		model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
		if(bap.bast_tgl!=null){
			DateTime tgl_bast = new DateTime(bap.bast_tgl.getTime());
			String hari = FormatUtils.day[(tgl_bast.getDayOfWeek()==7 ? 0 : tgl_bast.getDayOfWeek())];
			String bulan = FormatUtils.month[tgl_bast.getMonthOfYear()-1];
//			String tanggal = FormatUtils.number2Word(tgl_bast.getDayOfMonth());
//			String tahun = FormatUtils.number2Word(tgl_bast.getYear());
			model.put("hari",hari);
			model.put("bulan",bulan);
//			model.put("tanggal", tanggal);
			model.put("tahun",tgl_bast.getYear());
		}
		String nilai = FormatUtils.terbilang(bap.besar_pembayaran);
		model.put("besar_pembayaran",nilai);
		Template template = TemplateLoader.load("/kontrak/template/berita-acara-pembayaran.html");
		return HtmlUtil.generatePDF(template, model);
	}

	public static InputStream cetak_bast(Long bapId) {
		BA_Pembayaran bap =BA_Pembayaran.findById(bapId);
		Kontrak kontrak = Kontrak.findById(bap.kontrak_id);
		Kontrak_kso kso = kontrak.getKontrakKSO();
		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(kontrak.lls_id);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("format", new FormatUtils());
		model.put("kso",kso);
		model.put("bast", bap);
		model.put("kontrak",kontrak);
		model.put("lelang", lelang_seleksi);
		Active_user userPPK = Active_user.current();
		Ppk ppk = Ppk.findById(userPPK.ppkId);
		model.put("ppk", ppk);
		// TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
		Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM paket_satker WHERE pkt_id=?)", lelang_seleksi.pkt_id).first();
		model.put("satker", satker);
		model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
		if(bap.bast_tgl!=null){
			DateTime tgl_bast = new DateTime(bap.bast_tgl.getTime());
			String hari = FormatUtils.day[(tgl_bast.getDayOfWeek()==7 ? 0 : tgl_bast.getDayOfWeek())];
			String bulan = FormatUtils.month[tgl_bast.getMonthOfYear()-1];
			String tanggal = FormatUtils.terbilang(tgl_bast.getDayOfMonth());
			String tahun = FormatUtils.terbilang(tgl_bast.getYear());
			model.put("hari", hari);
			model.put("bulan", bulan);
			model.put("tanggal", tanggal);
			model.put("tahun", tgl_bast.getYear());
		}
		Template template = TemplateLoader.load("/kontrak/template/berita-acara-serah-terima.html");
		return HtmlUtil.generatePDF(template, model);
	}

	public UploadInfo simpanDokBast(File file) throws Exception {
		BlobTable blob = null;
		if (cetak_bast_attachment == null) {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
		} else if (cetak_bast_attachment != null)
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, cetak_bast_attachment);
		cetak_bast_attachment = blob.blb_id_content;
		save();
		return UploadInfo.findBy(blob);
	}

	public UploadInfo simpanDokBap(File file) throws Exception {
		BlobTable blob = null;
		if (cetak_bap_attachment == null) {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
		} else if (cetak_bap_attachment != null)
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, cetak_bap_attachment);
		cetak_bap_attachment = blob.blb_id_content;
		save();
		return UploadInfo.findBy(blob);
	}
}
