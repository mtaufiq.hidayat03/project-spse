package models.kontrak;

import ext.DateBinder;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;

/**
 * Created by Lambang on 4/3/2017.
 */
@Table(name = "DOK_LAIN")
public class DokLain extends BaseModel {

    @Id(sequence="seq_dok_lain", function="nextsequence")
    public Long dlp_id;

    @Required
    public Long lls_id;

    @Required
    public String dlp_nama_dokumen;

    @Required
    public String dlp_nomor_dokumen;

    @As(binder= DateBinder.class)
    public Date dlp_tanggal_dokumen;

    public String dlp_keterangan;

    public Long dlp_id_attachment;

    public static DokLain findByLelang(Long lelangId){
        return find("lls_id=?", lelangId).first();
    }

    @Transient
    public BlobTable getBlob() {
        if(dlp_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dlp_id_attachment);
    }

    public static void simpanDokLain(DokLain dokLain, File dlpAttachment, String hapus) throws Exception {

        if (dlpAttachment != null) {

            BlobTable bt = null;

            if (dokLain.dlp_id == null)
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment);
            else
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, dlpAttachment, dokLain.dlp_id_attachment);

            dokLain.dlp_id_attachment = bt.blb_id_content;

        } else {

            if (dokLain.dlp_id_attachment != null && hapus == null) {

                Long id = dokLain.dlp_id_attachment;

                dokLain.dlp_id_attachment = null;

                BlobTable.delete("blb_id_content=?", id);

            }

            if (hapus != null)
                dokLain.dlp_id_attachment = Long.valueOf(hapus);

        }

        dokLain.save();

    }

}
