package models.kontrak;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.Kontrak;
import models.agency.Pegawai;
import models.agency.Ppk;
import models.agency.Sppbj;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Domain class untuk Table Pesanan
 * @author retno
 *
 */

@Table(name="PESANAN")
public class Pesanan extends BaseModel {
	
	static final Template TEMPLATE_PESANAN_BARANG = TemplateLoader.load("/kontrak/template/SuratPesananBarang.html");
	static final Template TEMPLATE_SELEKSI_PERMEN_PERPRES= TemplateLoader.load("/kontrak/template/spmk-seleksi-permen-perpres.html");
	static final Template TEMPLATE_TENDER_PERMEN = TemplateLoader.load("/kontrak/template/spmk-tender-permen.html");
	
	@Id(sequence="seq_pesanan", function="nextsequence")
	public Long pes_id;
	
	//relasi ke Kontrak
	@Required
	public Long kontrak_id;
	
	@Required
	public String pes_no;
	
	
	@As(binder=DateBinder.class)
	public Date pes_tgl;


	/**
	 * data form isian pesanan
	 * format json
	 * model models.kontrak.PesananContent
	 */
	public String pes_content;

	public Long pes_attachment;
	
	@Transient
	public PesananContent getPesananContent() {
		return CommonUtil.fromJson(pes_content, PesananContent.class);
	}
	
	@Transient
	private Kontrak kontrak;

	public Kontrak getKontrak() {
		if(kontrak == null)
			kontrak = Kontrak.findById(kontrak_id);
		return kontrak;
	}

	public static Pesanan findByKontrak(Long pes_id, Long kontrak_id){
		return find("pes_id=? and kontrak_id=?",pes_id, kontrak_id).first();
	}
	
	
	/**
	 * data form isian pesanan
	 * format json
	 * model models.kontrak.DaftarBarang
	 */
	
	public String pes_barang;
	
	@Transient
	public DaftarBarang db;
	
	@Transient
	public BlobTable getBlob() {
		if(pes_attachment == null)
			return null;
		return BlobTableDao.getLastById(pes_attachment);
	}

	@Transient
	public List<BlobTable> getDokumen() {
		if(pes_attachment == null){
			return null;
		}
		return BlobTableDao.listById(pes_attachment);
	}


	public static InputStream cetak(Kontrak kontrak, Lelang_seleksi lelang_seleksi, Long id) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("format", new FormatUtils());
		model.put("lelang", lelang_seleksi);
		Active_user userPPK = Active_user.current();
		Ppk ppk = Ppk.findById(userPPK.ppkId);
		model.put("ppk", ppk);
		// TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
		// UPDATE: Alamat Satker diisi manual ke SPPBJ, hasil diskusi dengan Rizky 6 Mar 2020. reza
//		Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM paket_satker WHERE pkt_id=?)", lelang_seleksi.pkt_id).first();
//		model.put("satker", satker);
		Sppbj sppbj = Sppbj.findByLelang(lelang_seleksi.lls_id);
		model.put("alamat_satker", sppbj.alamat_satker);
		model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
		model.put("kontrak", kontrak);
		Pesanan pesanan = Pesanan.findById(id);
		PesananContent content = pesanan.getPesananContent();
		model.put("pesanan", pesanan);
		model.put("content",content);
		Peserta peserta = Peserta.findBy(lelang_seleksi.lls_id, kontrak.rkn_id);
		model.put("pemenang", peserta.getRekanan());
		if(pesanan.pes_barang!=null){
			DaftarBarang db = CommonUtil.fromJson(pesanan.pes_barang, DaftarBarang.class);
			List<Rincian_Barang> rincian = db.items;
			model.put("db", db);
			model.put("rincian", rincian);
		}
		 if (lelang_seleksi.getKategori().isBarang()) {
			 return HtmlUtil.generatePDF(TEMPLATE_PESANAN_BARANG, model);
		 }else if(lelang_seleksi.getKategori().isKonstruksi()){
			 return HtmlUtil.generatePDF(TEMPLATE_TENDER_PERMEN, model);
		 }else {
			 return HtmlUtil.generatePDF(TEMPLATE_SELEKSI_PERMEN_PERPRES, model);
		 }
	}

	public UploadInfo simpanDok(File file) throws Exception {
		BlobTable blob = null;
		if (pes_id != null) {
			// sppbj.sppbj_attachment = Long.parseLong("123");
			if (pes_attachment == null) {
				// jika baru upload file, gunakan fungsi save file dengan 2 parameter
				blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
			} else if (pes_attachment != null) {
				// jika update file yang di-upload gunakan fungsi save file dengan 3 parameter
				blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, pes_attachment);
			}
		} else {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
		}
		pes_attachment = blob.blb_id_content;
		save();
		return UploadInfo.findBy(blob);
	}

}
