package models.kontrak;

import ext.*;
import org.apache.commons.lang3.StringUtils;
import play.data.binding.As;
import play.data.validation.Required;

import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * model untuk Content SSKK pada Econtract
 * @author wahid
 *
 */
public class SskkPpkPlContent implements Serializable{

    public Integer kontrak_pembayaran;

    public String jaminan_setor_kas;

    public String wakil_sah_ppk ;

    public String wakil_sah_rekanan ;

    public String wakil_sah_pengawas ;

    public String pajak_rekanan ;;

    public String subkontrak_pekerjaan ;;

    public String subkontrak_penyedia ;;

    public String subkontrak_pekerjaan_khusus ;;

    public String subkontrak_penyedia_khusus ;;

    public String subkontrak_sanksi ;;

    public String pelaksanaan_pekerjaan;

    public String pengawas;

    @As(binder = DateBinder.class)
    public Date tgl_periksa_bersama;
    @Transient
    public String tgl_periksa_bersama_dt; //edit by sudi

    public String denda_keterlambatan;

    @As(binder = DateBinder.class)
    @Required
    public Date kontrak_mulai;

    @As(binder = DateBinder.class)
    @Required
    public Date kontrak_akhir;

	/*=================start barang =========================*/

    public String standar_barang="";
    @As(binder = DateBinder.class)
    public Date inspeksi_tgl;

    public String inspeksi_tgl_express; //edit by sudi

    public String inspeksi_tempat="-";

    public String inspeksi_lingkup="-";

    public String pengepakan="-";

    public String pengiriman="-";

    public String asuransi_ketentuan;

    public String asuransi_lingkup;

    public String asuransi_batas;

    //added by chitra
    //untuk lelang cepat
    public String pertanggungan_asuransi_cif="Tempat Tujuan Pengiriman";

    public String pertanggungan_asuransi_fob="Tempat Tujuan Pengiriman";

    public String serah_terima="Tempat Tujuan Pengiriman";

    public String cara_bayar_denda="Dipotong Dari Tagihan"; //value: Dipotong Dari Tagihan / penyedia meneytorkan ke kas negara/daerah


    public String tujuan_pengangkutan="Tempat Tujuan Pengiriman"; // value: 'Tempat Tujuan Akhir' / 'Tempat Tujuan Pengiriman'

    public String jenis_angkutan;

    public String jenis_tranportasi="darat"; // value : darat/laut/udara

    public String pemeriksaan_tempat="-";

    public String pemeriksaan_lingkup="-";

    public String ujicoba_tempat="-";

    public String ujicoba_pelaku="-";
    @As(binder = DateBinder.class)
    public Date tgl_penyelesaian;
    @Transient
    public String tgl_penyelesaian_dt; //edit by sudi

    public String edisi_incoterms="-";
    @As(binder = DateBinder.class)
    public Date tgl_serah_terima;

    public String tempat_tujuan_akhir;

    public String tempat_tujuan_pengiriman;

    public String masa_garansi;

    public String masa_purnajual;

	/*=================end barang =========================*/

    public String masa_pemeliharaan;

    /*=================start konstruksi =========================*/
    public String ketentuan_serah_terima="-";

    public String umur_konstruksi;

    @As(binder= RupiahBinder.class)
    public Double nilai_pembayaran_peralatan = Double.valueOf(0.0); //added by chitra 121015 untuk pembayaran perestasi pekerjaan
	/*=================end konstruksi=========================*/

    //added by chitra
    @As(binder= DecimalBinder.class)
    public Double denda_akibat_pemutusan_sepihak;
    public String waktu_denda_akibat_pemutusan_sepihak;
    /*=================start konsultansi =========================*/
    public String batasan_penggunaan_dok;
    public Integer tanggung_jawab_profesi = Integer.valueOf(0);
    public String getTanggung_jawab_profesi_string(){
        return FormatUtils.number2Word(tanggung_jawab_profesi);
    }

    public Integer batas_penerbitan_spp = Integer.valueOf(0);
    public String getBatas_penerbitan_spp_string(){
        return FormatUtils.number2Word(batas_penerbitan_spp);
    }

    public String dokumen_utama_pembayaran;

    public String ketentuan_penyesuaian_harga; //added by chitra 21102015 untuk lelang cepat badan usaha
	/*=================end konsultansi=========================*/

    /*=================start barang dan konstruksi =========================*/
    public String sumber_dana_harga_kontrak;
    public String sumber_dana_harga_kontrak_pilihan; //add by sudi
	/*=================end barang dan konstruksi=========================*/

    public String penyerahan_pedoman="-";

    public String layanan_tambahan="-";

    public String waktu_pembayaran;

    public String laporan_pekerjaan="-";

    public String perolehan_haki="-";

    public String pemberian_haki="-";

    public String persetujuan_ppk="-";

    public String persetujuan_pp="-"; //added by chitra 20102015 untuk lelang cepat jl

    public String kepemilikan_dokumen="-";

    public String sanksi="-";

    public String fasilitas="-";

    public String peristiwa_kompensasi="-";

    @As(binder=DecimalBinder.class)
    public Double uang_muka = Double.valueOf(0.0);

    public String cara_pembayaran="Sekaligus"; // value : Termin, Bulanan, Sekaligus

    public String ketentuan_pembayaran;

    public String dokumen_penunjang_pembayaran; //edit by chitra untuk konsultansi lelang umum dan cepat

    public String mata_uang_pembayaran; //added by chitra 121015 untuk konsultansi

    //field input berada di form-sskk
    public String dokumen_penunjang_pembayaran_prestasi; //edit by asep.as

    @As(binder=RupiahBinder.class)
    public Double selisih_tagihan = Double.valueOf(0.0);

    public String ketentuan_denda;

    public String pembayaran_denda;

    public String waktu_denda;

    public String asal_denda;

    public String instansi_pembuat_indeks;

    public String jenis_indeks;

    @As(binder=DecimalBinder.class)
    public Double nilai_indeks = Double.valueOf(0.0);

    @As(binder=DecimalBinder.class)
    public Double nilai_koefisien_tetap= Double.valueOf(0.0);

    @As(binder=DecimalBinder.class)
    public Double nilai_koefisien_komponen= Double.valueOf(0.0);

    public String pemutus_sengketa="Pengadilan Republik Indonesia"; // value: 'Pengadilan Republik Indonesia' / 'Badan Arbitrase Nasional Indonesia'

    public String ketentuan_lainnya="-";

    public String getTgl_pemeriksaan () {
        return FormatUtils.formatDateInd(tgl_periksa_bersama);
    }

    public String getTgl_inspeksi() {
        return FormatUtils.formatDateInd(inspeksi_tgl);
    }

    public String getTgl_selesai() {
        return FormatUtils.formatDateInd(tgl_penyelesaian);
    }

    public String getTglSerahTerima() {
        return FormatUtils.formatDateInd(tgl_serah_terima);
    }

    public String getUangMuka() {
        if(uang_muka == null)
            return "";
        return StringFormat.rupiah(uang_muka);
    }

    public String getSelisihTagihan() {
        if(uang_muka == null)
            return "";
        return StringFormat.rupiah(selisih_tagihan);
    }

    public String getMasaBerlaku() {
        return FormatUtils.formatDate(kontrak_mulai, kontrak_akhir);
    }

    public String getKontrakMulai(){
        return FormatUtils.formatDateInd(kontrak_mulai);
    }

    public boolean isTermin() {
        return !StringUtils.isEmpty(cara_pembayaran) && cara_pembayaran.equals("Termin");
    }

    public boolean isBulanan() {
        return !StringUtils.isEmpty(cara_pembayaran) && cara_pembayaran.equals("Bulan");
    }

    public boolean isSekaligus() {
        return !StringUtils.isEmpty(cara_pembayaran) && cara_pembayaran.equals("Sekaligus");
    }
}
