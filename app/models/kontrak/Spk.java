package models.kontrak;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 3/29/2017.
 */
@Table(name="EKONTRAK.SPK")
public class Spk extends BaseModel {

    static final Template TEMPLATE_SPK_PL = TemplateLoader.load("/kontrakpl/template/Spk-pl.html");

    @Id(sequence="ekontrak.seq_spk", function="nextsequence")
    public Long spk_id;

    //relasi ke Kontrak
    @Required
    public Long kontrak_id;

    @Required
    public String spk_no;

    @As(binder= DateBinder.class)
    public Date spk_tgl;

    /**
     * data form isian pesanan
     * format json
     * model models.kontrak.PesananContent
     */
    public String spk_content;

    public Long spk_attachment;

    @Transient
    public PesananContent getPesananContent() {
        return CommonUtil.fromJson(spk_content, PesananContent.class);
    }

    @Transient
    private KontrakPl kontrak;

    public KontrakPl getKontrak() {
        if(kontrak == null)
            kontrak = KontrakPl.findById(kontrak_id);
        return kontrak;
    }

    public static Spk findByKontrak(Long kontrak_id){
        return find("kontrak_id=?", kontrak_id).first();
    }


    /**
     * data form isian pesanan
     * format json
     * model models.kontrak.DaftarBarang
     */

    public String spk_barang;

    @Transient
    public DaftarBarang db;

    @Transient
    public BlobTable getBlob() {
        if(spk_attachment == null)
            return null;
        return BlobTableDao.getLastById(spk_attachment);
    }

    @Transient
    public List<BlobTable> getDokumen() {
        if(spk_attachment == null){
            return null;
        }
        return BlobTableDao.listById(spk_attachment);
    }

    public static InputStream cetak(Pl_seleksi pl, SppbjPl sppbjPl, Long spkId) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format", new FormatUtils());
        model.put("pl", pl);
        Paket_pl paket = pl.getPaket();
        model.put("paket", paket);
        Active_user userPPK = Active_user.current();
        Ppk ppk = Ppk.findById(userPPK.ppkId);
        model.put("ppk", ppk);
        Pegawai pegawai = Pegawai.findById(ppk.peg_id);
        model.put("pegawai", pegawai);
        model.put("sppbj", sppbjPl);
        KontrakPl kontrak = KontrakPl.find("lls_id=? and rkn_id=?",pl.lls_id, sppbjPl.rkn_id).first();
        model.put("kontrak", kontrak);
        Spk spk = Spk.findById(spkId);
        PesananContent content = spk.getPesananContent();
        model.put("spk", spk);
        model.put("content",content);
        PesertaPl peserta = PesertaPl.findBy(pl.lls_id, sppbjPl.rkn_id);

        model.put("pemenang", peserta.getRekanan());
        if(spk.spk_barang!=null){
            DaftarBarang db = CommonUtil.fromJson(spk.spk_barang, DaftarBarang.class);
            List<Rincian_Barang> rincian = db.items;
            model.put("db", db);
            model.put("rincian", rincian);
        }

        return HtmlUtil.generatePDF(TEMPLATE_SPK_PL, model);
    }

    public UploadInfo simpanDok(File file) throws Exception {
        BlobTable blob = null;
        if(spk_attachment == null){
            // jika baru upload file, gunakan fungsi save file dengan 2 parameter
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }else{
            // jika update file yang di-upload gunakan fungsi save file dengan 3 parameter
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, spk_attachment);
        }
        spk_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }
}
