package models.kontrak;

import ext.DecimalBinder;
import play.data.binding.As;

import java.io.Serializable;

/**
 * model untuk Rincian Barang
 * @author Retno
 *
 */

public class Rincian_Barang implements Serializable {	

	public String item;

	//sudah tidak terpakai, untuk sementara ini kolom tidak di hapus dahulu
	public String satuan;
	//sudah tidak terpakai, untuk sementara ini kolom tidak di hapus dahulu
	public Double kuantitas;

	@As(binder= DecimalBinder.class)
	public Double harga;

	@As(binder=DecimalBinder.class)
	public Double total_harga;

	//penambahan kolom baru terkait dengan rincian hps di ambil dari penawaran peserta yang ber kontrak
	public String unit;

	public Double vol;

	public String unit2;

	public Double vol2;

	public String keterangan;

	public Double pajak;
	
}
