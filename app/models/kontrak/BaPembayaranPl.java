package models.kontrak;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.nonlelang.Pl_seleksi;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Max;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 4/4/2017.
 */
@Table(name = "EKONTRAK.BA_PEMBAYARAN")
public class BaPembayaranPl extends BaseModel{

    @Id(sequence="ekontrak.seq_ba_pembayaran_pl", function="nextsequence")
    public Long bap_id;

    @Required(message="No. BAP wajib diisi")
    public String bap_no;

    @Required (message="Tanggal BAP wajib diisi")
    @As(binder= DateBinder.class)
    public Date bap_tgl;


    @Required (message="No. BAST wajib diisi")
    public String bast_no;

    @Required (message="Tanggal BAST wajib diisi")
    @As(binder=DateBinder.class)
    public Date bast_tgl;

    @Max(100)
    public Long progres_fisik;

    @Required (message="Besar Pembayaran wajib diisi")
    public Double besar_pembayaran;

    public Long pphp_id;


    public String pphp_nama;


    public Long pphp_nip;

    @Required
    public Long kontrak_id;

    public String jabatan_penandatangan_sk;

    public String kontrak_wakil_penyedia;

    public String kontrak_jabatan_wakil;

    public Long bap_id_attachment;
    public Long cetak_bast_attachment;
    public Long cetak_bap_attachment;

    @Transient
    private Pegawai pegawai;
    @Transient
    private KontrakPl kontrak;

    public Pegawai getPegawai() {
        if(pegawai == null)
            pegawai = Pegawai.findById(pphp_id);
        return pegawai;
    }
    public KontrakPl getKontrak() {
        if(kontrak == null)
            kontrak = KontrakPl.findById(kontrak_id);
        return kontrak;
    }

    @Transient
    public BlobTable getBlob() {
        if(bap_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(bap_id_attachment);
    }

    @Transient
    public BlobTable getBlobBast() {
        if(cetak_bast_attachment == null)
            return null;
        return BlobTableDao.getLastById(cetak_bast_attachment);
    }

    @Transient
    public BlobTable getBlobBap() {
        if(cetak_bap_attachment == null)
            return null;
        return BlobTableDao.getLastById(cetak_bap_attachment);
    }

    @Transient
    public List<BlobTable> getDokumenBast() {
        if(cetak_bast_attachment == null)
            return null;
        return BlobTableDao.listById(cetak_bast_attachment);
    }

    @Transient
    public List<BlobTable> getDokumenBap() {
        if(cetak_bap_attachment == null)
            return null;
        return BlobTableDao.listById(cetak_bap_attachment);
    }

    public static void simpan(BaPembayaranPl bap, File file, String hapus, File file_bast, File file_bap) throws Exception {
        if (file != null) {
            BlobTable bt = null;
            if (bap.bap_id_attachment == null) {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
            } else {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, bap.bap_id_attachment);
            }
            bap.bap_id_attachment = bt.blb_id_content;
        }
        if (file_bast != null) {
            BlobTable bt = null;
            if (bap.cetak_bast_attachment == null) {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file_bast);
            } else {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file_bast, bap.cetak_bast_attachment);
            }
            bap.cetak_bast_attachment = bt.blb_id_content;
        }
        if (file_bap != null) {
            BlobTable bt = null;
            if (bap.cetak_bap_attachment == null) {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file_bap);
            } else {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file_bap, bap.cetak_bap_attachment);
            }
            bap.cetak_bap_attachment = bt.blb_id_content;
        }
        if(file==null && file_bast==null && file_bap==null) {
            if(bap.bap_id_attachment!=null && !(hapus.indexOf(Long.toString(bap.bap_id_attachment))>-1)){
                BlobTable.delete("blb_id_content=?",bap.bap_id_attachment);
                bap.bap_id_attachment= null;
            }
            if(bap.cetak_bast_attachment!=null && !(hapus.indexOf(Long.toString(bap.cetak_bast_attachment))>-1)){
                BlobTable.delete("blb_id_content=?",bap.cetak_bast_attachment);
                bap.cetak_bast_attachment= null;
            }
            if(bap.cetak_bap_attachment!=null && !(hapus.indexOf(Long.toString(bap.cetak_bap_attachment))>-1)){
                BlobTable.delete("blb_id_content=?",bap.cetak_bap_attachment);
                bap.cetak_bap_attachment= null;
            }
        }
        bap.save();
    }

    public static InputStream cetak(Long bapId) {
        BaPembayaranPl bap =BaPembayaranPl.findById(bapId);
        KontrakPl kontrak = KontrakPl.findById(bap.kontrak_id);
        Kontrak_kso kso = kontrak.getKontrakKSO();
        Pl_seleksi pl = Pl_seleksi.findById(kontrak.lls_id);
        PesananPl pesanan = PesananPl.find("kontrak_id=?", kontrak.kontrak_id).first();
        Spk spk = Spk.find("kontrak_id=?", kontrak.kontrak_id).first();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format", new FormatUtils());
        model.put("kso",kso);
        model.put("pl", pl);
        SppbjPl sppbj = SppbjPl.find("lls_id=?", kontrak.lls_id).first();
        model.put("sppbj", sppbj);
        model.put("kontrak",kontrak);
        model.put("bast",bap);
        Active_user userPPK = Active_user.current();
        Ppk ppk = Ppk.findById(userPPK.ppkId);
        model.put("ppk", ppk);
        // TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
        Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM ekontrak.paket_satker WHERE pkt_id=?)", pl.pkt_id).first();
        model.put("satker", satker);
        model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
        if(pesanan == null && spk != null){
            model.put("content", spk.getPesananContent());
        }else if(pesanan != null){
            model.put("contentx", pesanan.getPesananContent());
        }
        if(bap.bast_tgl!=null){
            DateTime tgl_bast = new DateTime(bap.bast_tgl.getTime());
            String hari = FormatUtils.day[(tgl_bast.getDayOfWeek()==7 ? 0 : tgl_bast.getDayOfWeek())];
            String bulan = FormatUtils.month[tgl_bast.getMonthOfYear()-1];
//            String tanggal = FormatUtils.number2Word(tgl_bast.getDayOfMonth());
//            String tahun = FormatUtils.number2Word(tgl_bast.getYear());
            model.put("hari",hari);
            model.put("bulan",bulan);
//            model.put("tanggal", tanggal);
            model.put("tahun",tgl_bast.dayOfYear());
        }
        String nilai = FormatUtils.terbilang(bap.besar_pembayaran);
        model.put("besar_pembayaran",nilai);
        Template TEMPLATE_BA_PEMBAYARAN_PL = TemplateLoader.load("/kontrakpl/template/BA-PembayaranPl.html");
        return HtmlUtil.generatePDF(TEMPLATE_BA_PEMBAYARAN_PL, model);
    }


    public static InputStream cetak_bast(Long bapId) {
        BaPembayaranPl bap =BaPembayaranPl.findById(bapId);
        KontrakPl kontrak = KontrakPl.findById(bap.kontrak_id);
        Kontrak_kso kso = kontrak.getKontrakKSO();
        Pl_seleksi pl = Pl_seleksi.findById(kontrak.lls_id);
        PesananPl pesanan = PesananPl.find("kontrak_id=?", kontrak.kontrak_id).first();
        Spk spk = Spk.find("kontrak_id=?", kontrak.kontrak_id).first();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format", new FormatUtils());
        model.put("kso",kso);
        model.put("bast", bap);
        model.put("kontrak",kontrak);
        model.put("pl", pl);
        Active_user userPPK = Active_user.current();
        Ppk ppk = Ppk.findById(userPPK.ppkId);
        model.put("ppk", ppk);
        // TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
        Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM ekontrak.paket_satker WHERE pkt_id=?)", pl.pkt_id).first();
        model.put("satker", satker);
        model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
        if(pesanan == null && spk != null){
            model.put("content", spk.getPesananContent());
        }else if(pesanan != null){
            model.put("contentx", pesanan.getPesananContent());
        }
        if(bap.bast_tgl!=null){
            DateTime tgl_bast = new DateTime(bap.bast_tgl.getTime());
            String hari = FormatUtils.day[(tgl_bast.getDayOfWeek()==7 ? 0 : tgl_bast.getDayOfWeek())];
            String bulan = FormatUtils.month[tgl_bast.getMonthOfYear()-1];
            String tanggal = FormatUtils.number2Word(tgl_bast.getDayOfMonth());
            String tahun = FormatUtils.number2Word(tgl_bast.getYear());
            model.put("hari", hari);
            model.put("bulan", bulan);
            model.put("tanggal", tanggal);
            model.put("tahun", tahun);
        }
        Template TEMPLATE_BA_SERAHTERIMA_PL = TemplateLoader.load("/kontrakpl/template/BA-SerahTerimaPl.html");
        return HtmlUtil.generatePDF(TEMPLATE_BA_SERAHTERIMA_PL, model);
    }

    public UploadInfo simpanDokBast(File file) throws Exception {
        BlobTable blob = null;
        if (cetak_bast_attachment == null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        } else if (cetak_bast_attachment != null)
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, cetak_bast_attachment);
        cetak_bast_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanDokBap(File file) throws Exception {
        BlobTable blob = null;
        if (cetak_bap_attachment == null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        } else if (cetak_bap_attachment != null)
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, cetak_bap_attachment);
        cetak_bap_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

}
