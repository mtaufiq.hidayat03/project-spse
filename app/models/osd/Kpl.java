package models.osd;

import models.jcommon.db.base.BaseModel;
import org.apache.commons.codec.binary.Base64;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="kpl")
public class Kpl extends BaseModel {
	
	@Id(sequence="seq_kpl", function="nextsequence")
	public Long kpl_id;
	public Long pkt_id;
	public String kpl_content;
	public String kpl_sign;
	
//	public Kpl getKplByPaket(Long pkt_id){
//		if(pkt_id == null)
//			return null;
//		return db.findObject(SQL_SELECT + " WHERE pkt_id=:p1", clazz, pkt_id);
//	}
	
	public static String getKpl(Long pkt_id){
		Kpl kpl = Kpl.find("pkt_id=?", pkt_id).first();
		byte[] content = Base64.decodeBase64(kpl.kpl_content);
		byte[] sign = Base64.decodeBase64(kpl.kpl_sign);
		byte[] delimiter = new byte[]{35,12,5,13,19,1,14,5,7,35};
		byte[] all = new byte[content.length+sign.length+delimiter.length];
		boolean stop=false;
		int i=0;
		while(!stop){
			stop=true;
			if(i<content.length){
				all[i]=content[i];
				stop=false;
			}
			if(i<delimiter.length){
				all[i+content.length]=delimiter[i];
				stop=false;
			}
			if(i<sign.length){
				all[i+content.length+delimiter.length]=sign[i];
				stop=false;
			}
			i++;
		}
		return Base64.encodeBase64String(all);
	}
}
