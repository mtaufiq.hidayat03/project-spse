package models.osd;

import ams.contracts.ActiveUserAms;
import ams.models.response.ams.AmsCertificateResponse;
import ams.utils.AmsUtil;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.osd.CertificateUtil;

import javax.persistence.Transient;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Table(name = "CERTIFICATE")
public class Certificate extends BaseModel {

	@Id
	public Long cer_id;
	@Id
	public Integer cer_versi;
	public String cer_jenis;
	public String cer_sn;
	public String cer_issuer_dn;
	public String cer_subject_dn;
	public String cer_pem;
	public String cer_username;
	public String csr_pem;
	public Long repoId;

	@Transient
	public String ams_enroll_token;

	public String getCer_issuer_cn() {
		String[] DN = cer_issuer_dn.split(",");
		for (int i = 0; i < DN.length; i++)
			if (DN[i].startsWith("CN") || DN[i].startsWith("cn"))
				return DN[i].substring(3, DN[i].length());
		return null;
	}

	public String getCer_subject_dn() {
		return cer_subject_dn;
	}

	public void setCerSubjectDn(String cer_subject_dn) {
		if (cer_subject_dn != null) {
			String[] DN = cer_subject_dn.split(",");
			StringBuilder subjectDN = new StringBuilder();
			for (int i = 0; i < DN.length; i++) {
				if (DN[i].startsWith(" "))
					DN[i].replaceFirst(" ", "");
				subjectDN.append(DN[i]);
				if (i != DN.length - 1)
					subjectDN.append(',');
			}
			this.cer_subject_dn = subjectDN.toString();
		}
	}

	public String getKeterangan() {
		if (cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY) || cer_jenis.equals(JenisCertificate.CSRR_ADMIN_PPE)
				|| cer_jenis.equals(JenisCertificate.CSRR_PANITIA) || cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR))
			return KeteranganCertificate.CERT_PERMINTAAN_BARU;
		else if (cer_jenis.equals(JenisCertificate.CSR_ADMIN_AGENCY_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_ADMIN_PPE_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_PANITIA_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_PENYEDIA_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_VERIFIKATOR_REJECTED)) {
			return KeteranganCertificate.CERT_DITOLAK;
		} else if (cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY)
				|| cer_jenis.equals(JenisCertificate.CERT_ADMIN_PPE) || cer_jenis.equals(JenisCertificate.CERT_PANITIA)
				|| cer_jenis.equals(JenisCertificate.CERT_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR)) {
			if (cer_sn == null)
				return KeteranganCertificate.CERT_BARU_BELUM_AKTIVASI;
			else {
				if (cer_sn.equalsIgnoreCase(getSerialNumberFromPEM())) {
					if (isCertExpired())
						return KeteranganCertificate.CERT_HABIS_MASA_BERLAKU;
					else
						return KeteranganCertificate.CERT_AKTIF;
				} else
					return KeteranganCertificate.CERT_PEMBARUAN_BELUM_AKTIVASI;
			}
		} else if (cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_AGENCY)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_PPE)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_PANITIA)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_VERIFIKATOR)) {
			return KeteranganCertificate.CERT_PERMINTAAN_PEMBATALAN;
		} else if (cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_AGENCY)
				|| cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_PPE)
				|| cer_jenis.equals(JenisCertificate.REVOKED_PANITIA)
				|| cer_jenis.equals(JenisCertificate.REVOKED_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.REVOKED_VERIFIKATOR)) {
			return KeteranganCertificate.CERT_TIDAK_AKTIF;
		} else if (cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY_RENEW)
				|| cer_jenis.equals(JenisCertificate.CSRR_ADMIN_PPE_RENEW)
				|| cer_jenis.equals(JenisCertificate.CSRR_PANITIA_RENEW)
				|| cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA_RENEW)
				|| cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR_RENEW))
			return KeteranganCertificate.CERT_PERMINTAAN_PEMBARUAN;
		else
			return "N/A";
	}

	public String getKeterangan(int lastVersi) {
		if (cer_versi < lastVersi)
			return KeteranganCertificate.CERT_TIDAK_AKTIF;
		else
			return getKeterangan();
	}

	public String getRangeCertificateValidDate() {
		String rangeValidDate = "";
		X509Certificate x509Certificate;
		if (cer_pem != null) {
			try {
				x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(cer_pem);
				if (x509Certificate != null) {
					// Date startValidCert = x509Certificate.getNotBefore();
					Date endValidCert = x509Certificate.getNotAfter();
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					rangeValidDate = dateFormat.format(endValidCert);
				}
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		}
		return rangeValidDate;
	}

	public Date getExpiredDate() {
		Date rangeValidDate = null;
		X509Certificate x509Certificate;
		if (cer_pem != null) {
			try {
				x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(cer_pem);
				if (x509Certificate != null) {
					rangeValidDate = x509Certificate.getNotAfter();
				}
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		}
		return rangeValidDate;
	}

	public String getValidDate(int lastVersi) {
		if (cer_versi == lastVersi) {
			if (cer_sn == null)
				return "";
			else {
				if (cer_sn.equalsIgnoreCase(getSerialNumberFromPEM()))
					return getRangeCertificateValidDate();
				else
					return "";
			}
		} else {
			return "";
		}
	}

	public boolean isCertExpired() {
		boolean expired = true;
		if (cer_pem != null) {
			X509Certificate x509Certificate;
			try {
				x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(cer_pem);
				if (x509Certificate != null) {
					Date endValidCert = x509Certificate.getNotAfter();
					Date now = new Date();
					if (now.before(endValidCert))
						expired = false;
				}
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		}
		return expired;
	}

	public String getKeteranganSertifikat() {
		String keterangan = null;
		if (Arrays.asList(JenisCertificate.groupCsrr).contains(cer_jenis)) {
			keterangan = KeteranganCertificate.CERT_PERMINTAAN_BARU;
		} else if (Arrays.asList(JenisCertificate.groupCsrrRenew).contains(cer_jenis)) {
			keterangan = KeteranganCertificate.CERT_PERMINTAAN_PEMBARUAN;
		} else if (Arrays.asList(JenisCertificate.groupRejected).contains(cer_jenis)) {
			keterangan = KeteranganCertificate.CERT_DITOLAK;
		} else if (Arrays.asList(JenisCertificate.groupRejected).contains(cer_jenis)) {
			keterangan = KeteranganCertificate.CERT_DITOLAK;
		}
		return keterangan;
	}

	public String getSerialNumberFromPEM() {
		String sn = null;
		if (cer_pem != null) {
			try {
				X509Certificate x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(cer_pem);
				if (x509Certificate != null) {
					sn = new String(Hex.encode(x509Certificate.getSerialNumber().toByteArray())).toUpperCase();
				}
			} catch (CertificateException e) {
			}
		}
		return sn;
	}

	public static Long nextSequence() {
		return Query.find("SELECT NEXTSEQUENCE('SEQ_CERTIFICATE')", Long.class).first();
	}
	
	public boolean isActive() {
		return cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY)
				|| cer_jenis.equals(JenisCertificate.CERT_ADMIN_PPE)
				|| cer_jenis.equals(JenisCertificate.CERT_PANITIA)
				|| cer_jenis.equals(JenisCertificate.CERT_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR);
	}
	
	public boolean isRevokeRequest() {
		return cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_AGENCY)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_PPE)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_PANITIA)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.REVOKE_REQ_VERIFIKATOR);
	}
	
	public boolean isRevoked() {
		return cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_AGENCY)
				|| cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_PPE)
				|| cer_jenis.equals(JenisCertificate.REVOKED_PANITIA)
				|| cer_jenis.equals(JenisCertificate.REVOKED_PENYEDIA)
				|| cer_jenis.equals(JenisCertificate.REVOKED_VERIFIKATOR);
	}
	
	public boolean isRejected() {
		return cer_jenis.equals(JenisCertificate.CSR_ADMIN_AGENCY_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_ADMIN_PPE_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_PANITIA_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_PENYEDIA_REJECTED)
				|| cer_jenis.equals(JenisCertificate.CSR_VERIFIKATOR_REJECTED);
	}

	public String getBase64CSRPEM() {
		return Base64.toBase64String(csr_pem.getBytes());
	}

	public boolean setDnFromAmsCertificate(AmsCertificateResponse response) {
		if (CommonUtil.isEmpty(response.certificate)) {
			return false;
		}
		X509Certificate x509Certificate = response.getAsCertificate();
		if(x509Certificate!=null){
			this.cer_sn = new String(Hex.encode(x509Certificate.getSerialNumber().toByteArray())).toUpperCase();
			this.cer_issuer_dn = x509Certificate.getIssuerDN().getName();
			this.cer_subject_dn = x509Certificate.getSubjectDN().getName();
			return true;
		}
		return false;
	}

	public X509Certificate getX509() {
		if (CommonUtil.isEmpty(cer_pem)) {
			return null;
		}
		try {
			return (X509Certificate) CertificateUtil.getCertFromPEM(AmsUtil.oneLineFormat(cer_pem));
		} catch (CertificateException e) {
			return null;
		}
	}

	public String toJson() {
		return  CommonUtil.toJson(this);
	}

	public boolean isCsrExist() {
		return !CommonUtil.isEmpty(csr_pem);
	}

	public boolean isCerExist() {
		return !CommonUtil.isEmpty(cer_pem);
	}

	public void forceRevokedCertificate(boolean isRekanan) {
		if (isRekanan) {
			this.cer_jenis = JenisCertificate.REVOKED_PENYEDIA;
		} else {
			this.cer_jenis = JenisCertificate.REVOKED_PANITIA;
		}
	}

	public boolean isCsrRenew() {
		return this.cer_jenis.equals(JenisCertificate.CSRR_PANITIA_RENEW)
				|| this.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PENYEDIA);
	}

	public void forceSetRejected(ActiveUserAms user) {
		if (user.isRekanan()) {
			this.cer_jenis = JenisCertificate.CSR_PENYEDIA_REJECTED;
		} else if (user.isPanitia()) {
			this.cer_jenis = JenisCertificate.CSR_PANITIA_REJECTED;
		}
	}

	public void setEnrollToken(String enrollToken) {
		if (CommonUtil.isEmpty(this.ams_enroll_token)
				&& !CommonUtil.isEmpty(enrollToken)) {
			this.ams_enroll_token = enrollToken;
		}
	}

}
