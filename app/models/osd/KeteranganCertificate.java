package models.osd;

public class KeteranganCertificate {

	public static final String CERT_AKTIF = "Sertifikat Aktif";
	public static final String CERT_TIDAK_AKTIF = "Sertifikat Tidak Aktif";
	public static final String CERT_DITOLAK= "Permintaan Sertifikat Ditolak";
	public static final String CERT_PERMINTAAN_BARU = "Permintaan Sertifikat Baru";
	public static final String CERT_PERMINTAAN_PEMBARUAN = "Permintaan Permbaruan Sertifikat";
	public static final String CERT_PERMINTAAN_PEMBATALAN = "Permintaan Pembatalan Sertifikat";
	public static final String CERT_BARU_BELUM_AKTIVASI = "Sertifikat Baru Belum Diaktivasi";
	public static final String CERT_PEMBARUAN_BELUM_AKTIVASI = "Sertifikat Pembaruan Belum Diaktivasi";
	public static final String CERT_HABIS_MASA_BERLAKU = "Masa Berlaku Sertifikat Habis";
	
}
