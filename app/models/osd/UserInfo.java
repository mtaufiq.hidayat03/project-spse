package models.osd;

public class UserInfo {
	public Long cer_id = null;
	public String lpseRepoId;
	
	public String endDateCert = null;
	public String namaPengguna = null;
	public String email = null;
	public String kabkota = null;
	public String provinsi = null;
	public String sn = null;
	public String lpseIdCertOrigin = null;
	public String lpseUrlCertOrigin = null;
	public String lpseNameCertOrigin = null;
	public String keyLength = "2048";
	public Boolean op = null;
	public Boolean isRoaming = null;
	public Boolean canRequestCSR = false;
	public Boolean canRequestCSRRenew = false;
	public String description = null;
	public String error = null;
}
