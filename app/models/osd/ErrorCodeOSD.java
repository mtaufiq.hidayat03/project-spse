package models.osd;

public enum ErrorCodeOSD {
	CERT_UNTRUSTED(100, "Certificate verification failed"),				/*gagal memverifikasi sertifikat*/
	CERT_NOT_YET_VALID(101, "Certificate not yet valid"),				/*sertifikat belum memasuki masa berlaku*/
	CERT_EXPIRED(102, "Certificate Expired"),							/*masa berlaku sertifikat habis*/
	
	UNKNOWN_ERROR(-1, "Unknown Error"),									/*default value untuk error code (umum/untuk semua user)*/
	SUCCEEDED(0, "Succeeded"),											/*proses berhasil (umum/untuk semua user)*/
	FAILED(1, "Failed"),												/*proses gagal (umum/untuk semua user)*/
	NOT_AUTHORIZED_USER(2, "Not Authorized Access!"),					/*user tidak memiliki hak akses untuk suatu resource (umum/untuk semua user)*/
	PARAMS_ERROR(3, "Can't get secured params"),						/*error saat membuka parameter terenkripsi (umum/untuk semua user)*/
	CERT_ALREADY_ACTIVATED(4, "Certificate Already Activated"),			/*sertifikat sudah aktif (umum/untuk semua user)*/
	DATA_NOT_FOUND(5, "Data Not Found"),								/*data yang diminta tidak ditemukan atau null (umum/untuk semua user)*/
	DATA_ALREADY_EXIST(6, "Data Already Exist"),						/*data sudah ada (umum/untuk semua user)*/
	GET_CERTIFICATE_FAILED(7, "Get Certificate Failed"),				/*sertifikat yang diminta tidak ditemukan atau null (umum/untuk semua user)*/
	GET_LINK_FAILED(8, "Get Link Attachment Failed"),					/*error saat meminta link untuk download suatu dokumen (umum/untuk semua user)*/
	OPERATION_NOT_PERMITTED(9, "Operation Not Permitted"),				/*operasi tidak diijinkan (umum/untuk semua user)*/

	FILE_NOT_FOUND(10, "File Not Found"),								/*file yang diminta tidak ada (umum/untuk semua user)*/
	CSRR_NOT_FOUND(11, "Request Certificate Not Found"),				/*tidak ada daftar pe-request sertifikat baru (saat verifikator ingin melihat siapa aja yang request sertifikat)*/
	DKPL_NOT_FOUND(12, "Draft KPL Not Found"),							/*tidak ada daftar paket baru yang belum ditandatangani (saat ppk ingin menandatangani paket)*/
	JENIS_PENAWARAN_NOT_FOUND(13, "Jenis Penawaran Not Found"),			/*tidak ada jenis penawaran dari penyedia (saat panitia akan membuka dokumen penawaran)*/
	PAKET_NOT_FOUND(14, "Paket Not Found"),								/*tidak ada daftar paket (saat penyedia ingin upload dokumen/sedang tidak ikut lelang)*/
	DRAFT_PAKET_NOT_FOUND(15, "Darft Paket Not Found"),		/*tidak ada paket yang dokumen lelang atau adendumnya dapat ditambah/diubah*/
	DOK_PENAWARAN_NOT_FOUND(16, "Dokumen Penawaran Not Found"),			/*tidak ada penyedia yang mengirimkan penawaran (saat panitia akan membuka dokumen penawaran)*/
	PESERTA_NOT_FOUND(17, "Peserta Not Found"),							/*lelang yang tidak ada pesertanya (saat panitia akan membuka dokumen penawaran)*/
	FILE_ALREADY_SIGNED(18, "File Already Signed"),						/*file sudah ditandatangani)*/
	CSRR_NOT_VERIFIED(19, "CSRR Not Yet Verified"),
	CSRR_REJECTED(20, "CSRR Rejected"),
	
	DATA_NOT_COMPLETE(21, "Data Not Complete"),
	INVALID_LPSE(22, "Invalid LPSE for Certificate Registration"),		/*penyedia pernah melakukan pendaftaran sertifikat di lpse lain*/
	CRR_NOT_FOUND(23, "Request Certificate Not Found"),
	CERT_DOESNOT_MATCH(24, "Sertifikat existing tidak dikenali"),	
	CERT_HAVE_BEEN_RENEWED(25, "Sertifikat sudah pernah diperpanjang"),
	ALREADY_HAVE_CERT(26, "User sudah memiliki sertifikat"),
	FAILED_REQUEST_RENEW(27, "Gagal request pembaruan sertifikat karena tidak punya sertifikat aktif"),
	USER_NOT_ADP(28, "Pengguna belum ADP"),
	CERT_NOT_FOUND(29, "Sertifikat Tidak Ditemukan");
	
	private int code;
	private String label;
	
	ErrorCodeOSD(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	public String getNotif(){
		return this.label + " (Error " + this.code + ')';
	}
	
	public String getLabel(){
		return this.label;
	}
	
	public String getCode(){
		return "ERR"+code;
	}
	
	public static String getDefault(){
		return UNKNOWN_ERROR.getCode();
	}
	
	public static ErrorCodeOSD getErrorCode(String code){
		if(code.startsWith("ERR")) code=code.substring(3);
		int error = Integer.valueOf(code).intValue();
		return getErrorCode(error);
	}	
	
	public static ErrorCodeOSD getErrorCode(int code){
		ErrorCodeOSD errorCode;
		switch(code){
			case 0 : errorCode = SUCCEEDED;break;
			case 1 : errorCode = FAILED;break;
			case 2 : errorCode = NOT_AUTHORIZED_USER;break;
			case 3 : errorCode = PARAMS_ERROR;break;
			case 4 : errorCode = CERT_ALREADY_ACTIVATED;break;
			case 5 : errorCode = DATA_NOT_FOUND;break;
			case 6 : errorCode = DATA_ALREADY_EXIST;break;
			case 7 : errorCode = GET_CERTIFICATE_FAILED;break;
			case 8 : errorCode = GET_LINK_FAILED;break;
			case 9 : errorCode = OPERATION_NOT_PERMITTED;break;
			default : errorCode = UNKNOWN_ERROR;
		}
		return errorCode;
	}
	
}