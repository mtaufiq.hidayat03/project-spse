package models.osd;

public class JenisCertificate {

	public static int FILE_KPL = 0;
	public static int FILE_DOK_LELANG = 1;
	public static int FILE_DOK_PENAWARAN = 2;
	
	public static final String NEW_VALUE = "Baru";
	public static final String RENEW_VALUE = "Pembaruan";
	
	public static final String CSRR_PENYEDIA = "csrr-penyedia";
	public static final String CSRR_ADMIN_PPE = "csrr-admin-ppe";
	public static final String CSRR_ADMIN_AGENCY = "csrr-admin-agency";
	public static final String CSRR_VERIFIKATOR = "csrr-verifikator";
	public static final String CSRR_PANITIA = "csrr-panitia";
	
	public static final String CSRR_PENYEDIA_RENEW = "csrr-penyedia-renew";
	public static final String CSRR_ADMIN_PPE_RENEW = "csrr-admin-ppe-renew";
	public static final String CSRR_ADMIN_AGENCY_RENEW = "csrr-admin-agency-renew";
	public static final String CSRR_VERIFIKATOR_RENEW = "csrr-verifikator-renew";
	public static final String CSRR_PANITIA_RENEW = "csrr-panitia-renew";
	
//	public static final String CSR_PENYEDIA = "csr-penyedia";
//	public static final String CSR_ADMIN_PPE = "csr-admin-ppe";
//	public static final String CSR_ADMIN_AGENCY = "csr-admin-agency";
//	public static final String CSR_VERIFIKATOR = "csr-verifikator";
//	public static final String CSR_PANITIA = "csr-panitia";
	
	public static final String CSR_PENYEDIA_REJECTED = "csr-penyedia-rejected";
	public static final String CSR_ADMIN_PPE_REJECTED = "csr-admin-ppe-rejected";
	public static final String CSR_ADMIN_AGENCY_REJECTED = "csr-admin-agency-rejected";
	public static final String CSR_VERIFIKATOR_REJECTED = "csr-verifikator-rejected";
	public static final String CSR_PANITIA_REJECTED = "csr-panitia-rejected";
	
	public static final String CERT_PENYEDIA = "cert-penyedia";
	public static final String CERT_ADMIN_PPE = "cert-admin-ppe";
	public static final String CERT_ADMIN_AGENCY = "cert-admin-agency";
	public static final String CERT_VERIFIKATOR = "cert-verifikator";
	public static final String CERT_PANITIA = "cert-panitia";
	
	public static final String REVOKE_REQ_PENYEDIA = "req-revoke-penyedia";
	public static final String REVOKE_REQ_ADMIN_PPE = "req-revoke-admin-ppe";
	public static final String REVOKE_REQ_ADMIN_AGENCY = "req-revoke-admin-agency";
	public static final String REVOKE_REQ_VERIFIKATOR = "req-revoke-verifikator";
	public static final String REVOKE_REQ_PANITIA = "req-revoke-panitia";
	
	public static final String REVOKED_PENYEDIA = "revoked-penyedia";
	public static final String REVOKED_ADMIN_PPE = "revoked-admin-ppe";
	public static final String REVOKED_ADMIN_AGENCY = "revoked-admin-agency";
	public static final String REVOKED_VERIFIKATOR = "revoked-verifikator";
	public static final String REVOKED_PANITIA = "revoked-panitia";
	
	public static final String PROFILE_PENYEDIA="PENYEDIA";
	public static final String PROFILE_ADMIN_PPE="PPE";
	public static final String PROFILE_ADMIN_AGENCY="ADM-AGENCY";
	public static final String PROFILE_VERIFIKATOR="VERIFIKATOR";
	public static final String PROFILE_PANITIA="PANITIA";
	
	// Group Revoked
	public static final String[] groupRevoked = new String[] {REVOKED_PENYEDIA, REVOKED_ADMIN_PPE, REVOKED_ADMIN_AGENCY, REVOKED_VERIFIKATOR, REVOKED_PANITIA};
	
	// Group Rejected
	public static final String[] groupRejected = new String[] {CSR_PENYEDIA_REJECTED, CSR_ADMIN_PPE_REJECTED, CSR_ADMIN_AGENCY_REJECTED, CSR_VERIFIKATOR_REJECTED, CSR_PANITIA_REJECTED};
	
	// Group Cert
	public static final String[] groupCert = new String[] {CERT_PENYEDIA, CERT_ADMIN_PPE, CERT_ADMIN_AGENCY, CERT_VERIFIKATOR, CERT_PANITIA};
	
	// Group CSRR
	public static final String[] groupCsrr = new String[] {CSRR_PENYEDIA, CSRR_ADMIN_PPE, CSRR_ADMIN_AGENCY, CSRR_VERIFIKATOR, CSRR_PANITIA};	

	// Group CSRR Renew
	public static final String[] groupCsrrRenew = new String[] {CSRR_PENYEDIA_RENEW, CSRR_ADMIN_PPE_RENEW, CSRR_ADMIN_AGENCY_RENEW, CSRR_VERIFIKATOR_RENEW, CSRR_PANITIA_RENEW};	

	// Group Request Revoke
	public static final String[] groupRevokeReq = new String[] {REVOKE_REQ_PENYEDIA, REVOKE_REQ_ADMIN_PPE, REVOKE_REQ_ADMIN_AGENCY, REVOKE_REQ_VERIFIKATOR, REVOKE_REQ_PANITIA};
		
}
