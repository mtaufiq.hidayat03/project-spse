package models.devpartner.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.devpartner.BeritaAcaraDp;
import models.devpartner.PesertaDp;
import models.jcommon.blob.BlobTable;

/**
 * handle untuk berita acara
 * @author idoej
 *
 */
public class BeritaAcaraDpDownloadHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {	
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
			BeritaAcaraDp berita_acara = BeritaAcaraDp.find("brc_id_attachment=?", secureIdBlobTable.blb_id_content).first();
			if(berita_acara != null) {
				PesertaDp peserta = PesertaDp.find("lls_id=? and rkn_id=?", berita_acara.lls_id, active_user.rekananId).first();
				return peserta != null;
			}
		}	
		return active_user.isPpk() || active_user.isPanitia();
	}

}
