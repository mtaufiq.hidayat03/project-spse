package models.devpartner.handler;

import ext.FormatUtils;
import models.common.Active_user;
import models.common.Kategori;
import models.common.StatusLelang;
import models.devpartner.common.MetodeDp;
import models.devpartner.common.TahapDp;
import models.lelang.Evaluasi;
import org.sql2o.ResultSetHandler;
import play.i18n.Messages;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PaketDpResultHandler {

    public static final ResultSetHandler<String[]> resultsetPaketList = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Long lelangId = rs.getLong("lls_id");
            MetodeDp metode = MetodeDp.findById(rs.getInt("mtd_id"));
            String jadwal = TahapDp.tahapInfo(rs.getString("tahaps"), true);
            StatusLelang status = StatusLelang.fromValue(rs.getInt("lls_status"));
            Active_user user = Active_user.current();
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = kgr.isKonsultansi() ? Messages.get("ct.seleksi_ulang") : Messages.get("ct.tender_ulang");
            String[] tmp = new String[16];
            tmp[0] = rs.getString("pkt_id");
            tmp[1] = rs.getString("pkt_nama");
            int currentEvaVersi = Evaluasi.findCurrentVersi(lelangId);
            boolean isFinished = false;
            if (status.isDraft()) {
                tmp[2] = "Draft";
            } else  {
                if (jadwal.equalsIgnoreCase("Tender Sudah Selesai") && !status.isDraft()) {
                    tmp[2] = "Tender Sudah Selesai";
                    isFinished = true;
                } else {
                    tmp[2] = status.label;
                }
            }
            tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = lelangId.toString();
            tmp[6] = metode.getLabel();
            tmp[7] = rs.getString("lls_versi_lelang");
            //show create tender button
            tmp[8] = String.valueOf(user.isPanitia());
            //show delete button
            tmp[9] = String.valueOf(status.isDraft() && Active_user.current().isPpk() && rs.getString("ukpbj_id") == null);
            //show view lelang
            tmp[10] = String.valueOf((status.isAktif() || isFinished || tmp[2].equals("Lelang Ditutup")) && user.isPanitia());
            //Panitia
            tmp[11] = String.valueOf(user.isPanitia());
            //PPK
            tmp[12] = String.valueOf(user.isPpk());

            tmp[13] = String.valueOf(currentEvaVersi);

            tmp[14] = rs.getString("lls_penawaran_ulang");

            tmp[15] = tenderUlangLabel;

            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPaketKuppbj = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Long lelangId = rs.getLong("lls_id");
            MetodeDp metode = MetodeDp.findById(rs.getInt("mtd_id"));
            String jadwal = TahapDp.tahapInfo(rs.getString("tahaps"), true);
            StatusLelang status = StatusLelang.fromValue(rs.getInt("lls_status"));
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = kgr.isKonsultansi() ? Messages.get("ct.seleksi_ulang") : Messages.get("ct.tender_ulang");
            String[] tmp = new String[10];
            tmp[0] = rs.getString("pkt_id");
            tmp[1] = rs.getString("pkt_nama");
            if (status.isDraft()) {
                tmp[2] = Messages.get("lelang.status_draft");
            } else  {
                if (jadwal.equalsIgnoreCase("Tender Sudah Selesai") && !status.isDraft()) {
                    tmp[2] = Messages.get("lelang.status_selesai");
                } else {
                    tmp[2] = status.label;
                }
            }
            tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = lelangId.toString();
            tmp[6] = metode.getLabel();
            tmp[7] = rs.getString("lls_versi_lelang");
            //show create tender button
            tmp[8] = rs.getString("pnt_nama");

            tmp[9] = tenderUlangLabel;

            return tmp;
        }
    };
}
