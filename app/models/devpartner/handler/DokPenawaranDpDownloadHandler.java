package models.devpartner.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.jcommon.blob.BlobTable;

/**
 * handler untuk dok_penawaran
 * @author idoej
 *
 */
public class DokPenawaranDpDownloadHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		return true;// sementara dibuat true karena ada issue beda domain tapi 1 aplikasi
	}

}
