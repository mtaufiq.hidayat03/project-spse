package models.devpartner.handler;

import controllers.jcommon.blob.DownloadSecurityHandler;
import models.common.Active_user;
import models.devpartner.DiskusiLelangDp;
import models.devpartner.PesertaDp;
import models.jcommon.blob.BlobTable;

public class DiskusiLelangDpHandler implements DownloadSecurityHandler {

	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return false;
		else if(active_user.isRekanan()) { // hanya peserta yang bisa download dokumen
			DiskusiLelangDp diskusi = DiskusiLelangDp.find("dsl_id_attachment=?", secureIdBlobTable.blb_id_content).first();
			if(diskusi != null) {
				PesertaDp peserta = PesertaDp.find("lls_id=? and rkn_id=?", diskusi.lls_id, active_user.rekananId).first();
				return peserta != null;
			}
		}
		return active_user.isPanitia();
	}

}
