package models.devpartner.handler;

import ext.FormatUtils;
import models.common.Kategori;
import models.devpartner.common.MetodeDp;
import models.devpartner.common.TahapDp;
import org.sql2o.ResultSetHandler;
import play.i18n.Messages;

import java.sql.ResultSet;

/**
 * @author idoej
 */
public class LelangDpResultHandler {

    public static final ResultSetHandler<String[]> resultsetLelangPanitia = (ResultSet rs) -> {

        Integer versi = rs.getInt("lls_versi_lelang");
        String namaLelang = rs.getString("pkt_nama");
        Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
        String tenderUlangLabel = Messages.get("ct.tender_ulang");
        if (versi > 1)
            namaLelang += " <span class='label label-warning'>" + tenderUlangLabel + "</span>";
        String[] tmp = new String[6];
        Long lelangId = rs.getLong("lls_id");
        MetodeDp metode = MetodeDp.findById(rs.getInt("mtd_id"));
        tmp[0] = lelangId.toString();
        tmp[1] = namaLelang;
        tmp[2] = TahapDp.tahapInfo(rs.getString("tahaps"), true);
        tmp[3] = rs.getString("peserta");
        tmp[4] = metode.getLabel();
        return tmp;
    };

    public static final ResultSetHandler<String[]> resultsetLelangBaru = (ResultSet rs) -> {

        MetodeDp pemilihan = MetodeDp.findById(rs.getInt("mtd_id"));
        String[] tmp = new String[4];
        tmp[0] = rs.getString("lls_id");
        tmp[1] = rs.getString("pkt_nama");
        tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_hps"));
        tmp[3] = pemilihan.getLabel() + " " + (rs.getInt("lls_versi_lelang") > 1 ? "Ulang" : "");
        return tmp;
    };


    public static final ResultSetHandler<String[]> resultsetLelangRekanan = (ResultSet rs) -> {

        String[] tmp = new String[3];
        Long lelangId = rs.getLong("lls_id");
        String namaLelang = rs.getString("pkt_nama");
        tmp[0] = lelangId.toString();
        tmp[1] = namaLelang;
        tmp[2] = TahapDp.tahapInfo(rs.getString("tahaps"), true);
        return tmp;
    };

}
