package models.devpartner;

import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import java.util.Date;
import java.util.List;

/**
 * Created by idoej.
 */
@Table(name = "riwayat_persetujuan_dp", schema = "devpartner")
public class RiwayatPersetujuanDp extends BaseModel {

	@Id(sequence = "seq_riwayat_persetujuan", function = "nextsequence")
	public Long rwt_pst_id;

	public Long lls_id;

	public Long peg_id;

	public PersetujuanDp.JenisPersetujuan pst_jenis;

	public Date pst_tgl_setuju;

	public PersetujuanDp.StatusPersetujuan pst_status;

	public String pst_alasan;


	public static class RiwayatPersetujuanLelang {

		public Long lls_id;
		public PersetujuanDp.JenisPersetujuan pst_jenis;
		public Date pst_tgl_setuju;
		public PersetujuanDp.StatusPersetujuan pst_status;
		public String pst_alasan;
		public String peg_nama;
		public String agp_jabatan;

		public String getAlasan() {
			if(StringUtils.isEmpty(pst_alasan))
				return "";
			return pst_alasan;
		}

		public String getJabatan() {
			if (agp_jabatan.equals("K")) {
				return Messages.get("lelang.ketua");
			} else if (agp_jabatan.equals("W")) {
				return Messages.get("lelang.wakil");
			} else if (agp_jabatan.equals("S")) {
				return Messages.get("lelang.sekretaris");
			} else {
				return Messages.get("lelang.anggota");
			}
		}

		public String getTglSetujuFmt(){
			if(pst_tgl_setuju != null){
				return FormatUtils.formatDateTimeInd(pst_tgl_setuju);
			}
			return "";
		}

		public static List<RiwayatPersetujuanLelang> findList(Long peg_id, Long lls_id, PersetujuanDp.JenisPersetujuan jenis) {
			return Query.find("SELECT lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, agp_jabatan, peg_nama " +
					"FROM devpartner.riwayat_persetujuan_dp p, " +
                    '(' +
					"SELECT peg.peg_id, peg_nama, agp_jabatan FROM devpartner.lelang_dp l " +
					"LEFT JOIN devpartner.paket_dp p ON l.pkt_id=p.pkt_id " +
					"LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id "+
					"LEFT JOIN anggota_panitia ap ON ap.pnt_id=pn.pnt_id " +
					"LEFT JOIN pegawai peg ON peg.peg_id=ap.peg_id " +
					"WHERE lls_id=? AND ap.peg_id=peg.peg_id" +
					") a " +
					"WHERE p.peg_id=a.peg_id AND lls_id=? AND p.peg_id=? AND pst_jenis=? " +
					"ORDER BY pst_tgl_setuju ASC", RiwayatPersetujuanLelang.class, lls_id, lls_id, peg_id, jenis).fetch();
		}
	}

	public static void simpanRiwayatPersetujuan(Long lls_id, Long peg_id, PersetujuanDp.JenisPersetujuan pst_jenis, Date pst_tgl_setuju, PersetujuanDp.StatusPersetujuan pst_status, String pst_alasan){
		RiwayatPersetujuanDp rwt = new RiwayatPersetujuanDp();
		rwt.lls_id = lls_id;
		rwt.peg_id = peg_id;
		rwt.pst_jenis = pst_jenis;
		rwt.pst_tgl_setuju = pst_tgl_setuju;
		rwt.pst_status = pst_status;
		rwt.pst_alasan = pst_alasan;
		rwt.save();
	}

	public static void simpanRiwayatPersetujuan(PersetujuanDp pst){
		simpanRiwayatPersetujuan(pst.lls_id, pst.peg_id, pst.pst_jenis, pst.pst_tgl_setuju, pst.pst_status, pst.pst_alasan);
	}
}
