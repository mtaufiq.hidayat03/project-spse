package models.devpartner;

import models.common.UploadInfo;
import models.devpartner.handler.DokPenawaranDpDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Table(name="dok_penawaran_dp", schema = "devpartner")
public class DokPenawaranDp extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisDokPenawaran {
		PENAWARAN_EOI(0, "doc.eoi"),
		PENAWARAN_TEKNIS_ADMINISTRASI(1, "doc.teknis"),
		PENAWARAN_HARGA(2, "doc.harga"),
		PENAWARAN_TEKNIS_ADMINISTRASI_HARGA(3, "doc.teknis_harga");

		public final Integer id;

		public final String label;

		JenisDokPenawaran(int key, String label) {
			this.id = Integer.valueOf(key);
			this.label = label;
		}

		public static JenisDokPenawaran fromValue(Integer value) {
			JenisDokPenawaran jenis = null;
			if (value != null) {
				switch (value.intValue()) {
					case 0:
						jenis = PENAWARAN_EOI;
						break;
					case 1:
						jenis = PENAWARAN_TEKNIS_ADMINISTRASI;
						break;
					case 2:
						jenis = PENAWARAN_HARGA;
						break;
					case 3:
						jenis = PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
						break;
				}
			}
			return jenis;
		}

		public boolean isPenawaran() {
			return this == PENAWARAN_HARGA || this == PENAWARAN_TEKNIS_ADMINISTRASI || this == PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
		}
		public boolean isEoi(){
			return this == PENAWARAN_EOI;
		}
		public boolean isAdmTeknisHarga(){
			return this == PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
		}
		public boolean isAdmTeknis() {
			return this == PENAWARAN_TEKNIS_ADMINISTRASI;
		}
		public boolean isHarga() {
			return this == PENAWARAN_HARGA;
		}
	}

	@Id(sequence="devpartner.seq_dok_penawaran_dp", function="nextsequence")
	public Long dok_id;

	public String dok_judul;

	public Date dok_tgljam;

	public Date dok_waktu_buka;

	public String dok_hash;

	public String dok_signature;

	public Long dok_id_attachment;

	public String dok_surat; // perlu evaluasi, apakah menyimpan surat penawaran dengan cara ini adalah yang terbaik

	public JenisDokPenawaran dok_jenis;

	public Integer dok_disclaim;

	//relasi kek Peserta
	public Long psr_id;

	@Transient
	private PesertaDp peserta;

	public PesertaDp getPeserta() {
		if(peserta == null)
			peserta = PesertaDp.findById(psr_id);
		return peserta;
	}

	@Transient
	public BlobTable getBlob() {
		if (dok_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dok_id_attachment);
	}

	public static DokPenawaranDp findPenawaranPeserta(Long pesertaId, DokPenawaranDp.JenisDokPenawaran jenis) {
		return find("psr_id=? and dok_jenis=?", pesertaId, jenis).first();
	}

	/**
	 * get price offering and technical documents by participantId
	 * @param pesertaId participantId
	 * @return list of dok penawaran owned by participant*/
	public static List<DokPenawaranDp> findPenawaranPesertaList(Long pesertaId) {
		return find("psr_id=? AND (dok_jenis=? OR dok_jenis=?)",
				pesertaId,
				DokPenawaranDp.JenisDokPenawaran.PENAWARAN_HARGA,
				DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI).fetch();
	}

    @Transient
    public BlobTable getDokumen() {
        if(dok_id_attachment == null)
            return null;
        return BlobTableDao.getLastById(dok_id_attachment);
    }

    @Transient
    public String getDownloadUrl() {
        return getDokumen().getDownloadUrl(DokPenawaranDpDownloadHandler.class);

    }

	public static Map<String, Object> simpanDokPenawaran(Long pesertaId, File file, DokPenawaranDp.JenisDokPenawaran dok_jenis) throws Exception {
		DokPenawaranDp dok_penawaran = new DokPenawaranDp();
		dok_penawaran.psr_id = pesertaId;
		dok_penawaran.dok_jenis = dok_jenis;

		BlobTable blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);

		dok_penawaran.dok_id_attachment = blob.blb_id_content;
		dok_penawaran.dok_disclaim = Integer.valueOf(1);
		dok_penawaran.dok_tgljam = controllers.BasicCtr.newDate();
		dok_penawaran.dok_judul = blob.blb_nama_file;
		dok_penawaran.dok_hash = blob.blb_hash;
		dok_penawaran.save();

		Map<String, Object> res = new HashMap<>();

		res.put("file", UploadInfo.findBy(blob));
		res.put("dok_id", dok_penawaran.dok_id);

		return res;
	}


	public void deleteModelAndBlob() {
		BlobTableDao.getLastById(dok_id_attachment).delete();
		delete();
	}

	public static List<DokPenawaranDp> findByPsrAndJenis(Long psr_id, Integer dok_jenis) {
		return find("psr_id=? AND dok_jenis=?", psr_id, dok_jenis).fetch();
	}

	public static boolean isAnyPenawaran(Long lelangId, Long rekananId) {
		return count("psr_id in (select psr_id from devpartner.peserta_dp where lls_id=? and rkn_id=?) and dok_jenis in (0,1,2,3)", lelangId, rekananId) > 0;
	}
}
