package models.devpartner;

import models.common.UploadInfo;
import models.handler.BeritaAcaraDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


@Table(name="BERITA_ACARA_DP", schema = "devpartner")
public class BeritaAcaraDp extends BaseModel {

	@Enumerated(EnumType.STRING)
	public enum JenisBA {
		SHORTLIST_APPOINTMENT(1, "ba.shortlist_appointment"),
		SHORTLIST_ANNOUNCEMENT(2, "ba.shortlist_announcement"),
		TECHNICAL_EVALUATION(3, "ba.tech_eva"),
		FINANCIAL_EVALUATION(4, "ba.fin_eva"),
		BID_OPENING(5, "ba.bin_open"),
		FINAL_EVALUATION(6, "ba.final_evaluation"),
		CONTRACT_NEGOTIATION(7, "ba.contract_nego");

		public final Integer id;
		public final String label;

		JenisBA(Integer id, String label) {
			this.id = id;
			this.label = label;
		}
	}

	@Id(sequence="devpartner.seq_berita_acara_dp", function="nextsequence")
	public Long brc_id;

	@Required
	public JenisBA brc_jenis_ba;

	public Long brc_id_attachment;

	public String brt_no;

	public Date brt_tgl_evaluasi;

    public String brt_info;

	//relasi ke Lelang_seleksi
	public Long lls_id;

	@Transient
	private LelangDp lelang_seleksi;

	public LelangDp getLelang_seleksi() {
		if(lelang_seleksi == null)
			lelang_seleksi = LelangDp.findById(lls_id);
		return lelang_seleksi;
	}

	@Transient
	public List<BlobTable> getDokumen() {
		if(brc_id_attachment == null)
			return null;
		return BlobTableDao.listById(brc_id_attachment);
	}

    @Transient
    public String getJudul() {
        return Messages.get(brc_jenis_ba.label);
    }

	/**
	 * download URL untuk setiap dokumen berita acara
	 * @param blob
	 * @return
	 */
	@Transient
	public String getDownloadUrl(BlobTable blob) {
		return blob.getDownloadUrl(BeritaAcaraDownloadHandler.class);
	}

	public static UploadInfo simpan(Long lelangId, JenisBA jenis, File file) throws Exception
	{
		BeritaAcaraDp bA = find("lls_id=? and brc_jenis_ba=?", lelangId,jenis).first();
		if(bA == null) {
			bA = new BeritaAcaraDp();
			bA.lls_id = lelangId;
			bA.brc_jenis_ba = jenis;
		}
		BlobTable blob;
		if(file != null) {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, bA.brc_id_attachment);
		} else {
			blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file);
		}
		bA.brc_id_attachment = blob.blb_id_content;
		bA.save();
		// untuk keperluan response page yang memakai ajax
		return UploadInfo.findBy(blob);
	}

	public static BeritaAcaraDp findBy(Long lelangId, JenisBA jenis) {
		return find("lls_id=? and brc_jenis_ba=?", lelangId,jenis).first();
	}

    @Transient
    public String getNameFromBlobTable(BlobTable d){
		String name = "no name";
		try {
			String[] ls = BaseModel.decrypt(d.blb_path).split("\\n");
			name = ls[2];
		} catch (Exception ignored) {}
		return name;
	}

	public String getHari(){

		return new SimpleDateFormat("EEEE", new Locale("id")).format(brt_tgl_evaluasi);
	}

	public String getNamaPanitia() {
		getLelang_seleksi();
		if(this.lelang_seleksi == null) {
			return null;
		}
		return this.lelang_seleksi.getNamaPanitia();
	}
	
	/**
	 * Dapatkan dokumen berita acara, jika tidak ada maka akan dibuat baru,
	 * Gunakan saat proses submit terkait dokumen berita acara
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static BeritaAcaraDp findNCreateBy(Long lelangId, JenisBA jenis) {
		BeritaAcaraDp berita_acara = find("lls_id=? and brc_jenis_ba=?", lelangId, jenis).first();
		if(berita_acara == null) {
			berita_acara = new BeritaAcaraDp();
			berita_acara.lls_id = lelangId;
			berita_acara.brc_jenis_ba = jenis;
			berita_acara.save();
		}
		return berita_acara;
	}
	
	public UploadInfo simpan(File dokumen) {
		BlobTable blob = null;
		try {
			List<BlobTable> blobList = getDokumen();
			if (blobList != null)
				blobList.forEach(b -> b.delete());
			if (brc_id_attachment != null && getDokumen().isEmpty()) {
				brc_id_attachment = null;
			}
			if(dokumen != null)
				blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, dokumen, brc_id_attachment);
			if(blob != null)
				brc_id_attachment = blob.blb_id_content;
			save();
		} catch (Exception e){
			Logger.error(e, Messages.get("lelang.simpan_doc_ba_ggl")+" : %s", e.getMessage());
		}
		return UploadInfo.findBy(blob);
	}
}