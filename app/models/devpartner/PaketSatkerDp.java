package models.devpartner;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

@Table(name = "paket_satker_dp", schema = "devpartner")
public class PaketSatkerDp extends BaseModel {

    @Id(sequence = "devpartner.seq_paket_satker_dp", function = "nextsequence")
    public Long pks_id;

    //relasi ke model Satuan_kerja
    public Long stk_id;

    //relasi ke model Paket
    public Long pkt_id;

    //relasi ke model Rup_paket
    public Long rup_id;

    public static String getNamaSatkerPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, devpartner.paket_satker_dp p WHERE p.stk_id=s.stk_id AND pkt_id=?", String.class, paketId).first();
    }

    public static String getAlamatSatkerPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_alamat), ', ') FROM satuan_kerja s, devpartner.paket_satker_dp p WHERE p.stk_id=s.stk_id AND pkt_id=?", String.class, paketId).first();
    }

    public static String getNamaInstansiPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi " +
                "WHERE id in (SELECT instansi_id FROM satuan_kerja s, devpartner.paket_satker_dp p WHERE p.stk_id=s.stk_id AND pkt_id=?)", String.class, paketId).first();
    }

    public static String getNamaKabupaten(Long paketId) {
    	return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct k.kbp_nama), ', ') FROM instansi i inner join kabupaten k on i.kbp_id=k.kbp_id " +
                "WHERE i.id in (SELECT instansi_id FROM satuan_kerja s, devpartner.paket_satker_dp p WHERE p.stk_id=s.stk_id AND pkt_id=?)", String.class, paketId).first();
    }

    public static String getKodeRupPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(rup_id), ', ') FROM devpartner.paket_satker_dp WHERE pkt_id=?", String.class, paketId).first();
    }
}
