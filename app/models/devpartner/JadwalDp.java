package models.devpartner;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import controllers.BasicCtr;
import ext.DatetimeBinder;
import ext.FormatUtils;
import models.common.JadwalFlash;
import models.devpartner.common.AktivitasDp;
import models.devpartner.common.MailDpDao;
import models.devpartner.common.MetodeDp;
import models.devpartner.common.TahapDp;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Validation;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table(name = "jadwal_dp", schema = "devpartner")
public class JadwalDp extends BaseModel {

	@Id(sequence="devpartner.seq_jadwal_dp", function="nextsequence")
	public Long dtj_id;

	@As(binder = DatetimeBinder.class)
	public Date dtj_tglawal;

	@As(binder = DatetimeBinder.class)
	public Date dtj_tglakhir;

	public String dtj_keterangan;

	public Long lls_id;

	public Long akt_id;

	public Integer thp_id;

	@Transient
	private LelangDp lelang_seleksi;

	@Transient
	public TahapDp akt_jenis;

	@Transient
	public int akt_urut;

	@Transient
	private List<HistoryJadwalDp> historyList;

	@Transient
	private JadwalDraftDp jadwal_draft;

	// custome result, untuk optimasi query
	public static final ResultSetHandler<JadwalDp> CUSTOME_RESULTSET = new ResultSetHandler<JadwalDp>() {
		@Override
		public JadwalDp handle(ResultSet rs) throws SQLException {
			JadwalDp jadwal = new JadwalDp();
			jadwal.dtj_id = rs.getLong("dtj_id");
			jadwal.dtj_tglawal = rs.getTimestamp("dtj_tglawal");
			jadwal.dtj_tglakhir = rs.getTimestamp("dtj_tglakhir");
			jadwal.dtj_keterangan = rs.getString("dtj_keterangan");
			jadwal.lls_id = rs.getLong("lls_id");
			jadwal.thp_id = rs.getInt("thp_id");
			jadwal.audittype = rs.getString("audittype");
			jadwal.audituser = rs.getString("audituser");
			jadwal.auditupdate = rs.getTimestamp("auditupdate");
			jadwal.akt_id = rs.getLong("akt_id");
			jadwal.akt_jenis = TahapDp.valueOf(rs.getString("akt_jenis"));
			jadwal.akt_urut = rs.getInt("akt_urut");
			return jadwal;
		}
	};

	public boolean isDtjAkhirExist() {
		return this.dtj_tglakhir != null;
	}

    public LelangDp getLelang_seleksi() {
		if(lelang_seleksi == null)
			lelang_seleksi = LelangDp.findById(lls_id);
		return lelang_seleksi;
	}

	public List<HistoryJadwalDp> getHistoryList() {
		if(historyList == null)
			historyList = HistoryJadwalDp.find("dtj_id=?", dtj_id).fetch();
		return historyList;
	}

	public JadwalDraftDp getJadwalDraft() {
		if (jadwal_draft == null) {
			jadwal_draft = JadwalDraftDp.findByJadwal(dtj_id);
		}
		return jadwal_draft;
	}

	/**
	 * jadwal sudah masuk tahapan saat ini
	 * @return
	 */
	@Transient
	public boolean isStart(Date curDate) {
		return (dtj_tglawal != null && curDate.after(dtj_tglawal));
	}

	/**
	 * jadwal sedang berlangsung
	 * @return
	 */
	@Transient
	public boolean isNow(Date curDate) {
		if (dtj_tglawal != null && dtj_tglakhir != null)
			return (curDate.after(dtj_tglawal) && curDate.before(dtj_tglakhir));
		return false;
	}

	/**
	 * jadwal sudah keluar tahapan saat ini
	 * @return
	 */
	@Transient
	public boolean isEnd(Date curDate) {
		return (dtj_tglakhir != null && curDate.after(dtj_tglakhir));
	}

	public long getJumlah_history() {
		return HistoryJadwalDp.count("dtj_id=?", dtj_id);
	}

	public static JadwalDp getJadwalByJadwalAndAct(long lls_id, long akt_id){
		return JadwalDp.find("lls_id=? and akt_id=?", lls_id, akt_id).first();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof JadwalDp) {
			JadwalDp jadwal = (JadwalDp) obj;
			return equalAwal(jadwal) && equalAkhir(jadwal);
		} else if (obj instanceof JadwalDraftDp) {
			JadwalDraftDp jadwal = (JadwalDraftDp) obj;
			return equalAwal(jadwal) && equalAkhir(jadwal);
		}

		return false;
	}

	// ada perubahan atau tidak pada tgl awal
	@Transient
	public boolean equalAwal(JadwalDp jadwal) {
		return (dtj_tglawal != null && jadwal.dtj_tglawal != null && dtj_tglawal.equals(jadwal.dtj_tglawal));
	}

	// ada perubahan atau tidak pada tgl akhir
	@Transient
	public boolean equalAkhir(JadwalDp jadwal) {
		return (dtj_tglakhir != null && jadwal.dtj_tglakhir != null && dtj_tglakhir.equals(jadwal.dtj_tglakhir));
	}

	// ada perubahan atau tidak pada tgl awal
	@Transient
	public boolean equalAwal(JadwalDraftDp jadwal) {
		return (dtj_tglawal != null && jadwal.dtjd_tglawal != null && dtj_tglawal.equals(jadwal.dtjd_tglawal));
	}

	// ada perubahan atau tidak pada tgl akhir
	@Transient
	public boolean equalAkhir(JadwalDraftDp jadwal) {
		return (dtj_tglakhir != null && jadwal.dtjd_tglakhir != null && dtj_tglakhir.equals(jadwal.dtjd_tglakhir));
	}

	@Transient
	public String formatDate() {
		return FormatUtils.formatDate(dtj_tglawal, dtj_tglakhir);
	}

	@Transient
	public String formatDateTime() {
		return FormatUtils.formatDateTimeInd(dtj_tglawal)+" - "+ FormatUtils.formatDateTimeInd(dtj_tglakhir);
	}

	@Transient
	public String namaTahap() {
		if(akt_jenis == null)
			return "";

		return akt_jenis.getLabel();
	}

	public void validate(Date currentDate, JadwalDp before, JadwalDp pengumuman, boolean first, boolean mustFutureDate) throws Exception {
		if (mustFutureDate) {
			if (dtj_tglawal != null && dtj_tglawal.before(currentDate)) {
				throw new Exception(Messages.get("lelang.tmyamsdl"));
			}
			if (dtj_tglakhir != null && dtj_tglakhir.before(currentDate)) {
				throw new Exception(Messages.get("lelang.tayamsdl"));
			}
		}
		if (dtj_tglakhir != null && dtj_tglawal != null) {
			// Rule #1: awal > akhir
			if (dtj_tglakhir.before(dtj_tglawal)) {
				throw new Exception(Messages.get("lelang.tsmdtm"));
			}

			if (!first) {
				TahapDp thpBefore = before.akt_jenis;

				// Rule #2: tahap sebelumnya != null except tahap pertama
				// this is OLD SPSE ICB validation for rule 1, I don't understand "(thpBefore != TahapDp.CLARIFICATION_FORUM
				//					|| thpBefore != TahapDp.PREPROPOSAL_FORUM)" part because it always return true
				/***
				 if (!first && before.dtj_tglawal == null && (thpBefore != TahapDp.CLARIFICATION_FORUM
				 || thpBefore != TahapDp.PREPROPOSAL_FORUM)) {
				 throw new Exception(Messages.get("belum.ada.jadwal.tahap.sebelumnya"));
				 }
				 */
				if (before.dtj_tglawal == null) {
					throw new Exception(Messages.get("lelang.bjpts"));
				}

				// Rule #3: awal(tahap sekarang) == awal(tahap sebelumnya) ||
				// awal(tahap sekarang) > awal(tahap sebelumnya)
				if (before.dtj_tglawal != null
						&& (thpBefore != TahapDp.CLARIFICATION_FORUM || thpBefore != TahapDp.PREPROPOSAL_FORUM)) {
					if (dtj_tglawal.before(before.dtj_tglawal))
						throw new Exception(
								Messages.get("lelang.tgl_mptmtmdts"));
				}
				// Rule #4: jadwal tahap upload dok. penawaran tidak boleh
				// sama/paralel dengan tahap pembukaan dok. penawaran
				if (thpBefore == TahapDp.UPLOAD_EOI || thpBefore == TahapDp.PEMASUKAN_PENAWARAN) {
					if (dtj_tglawal.before(before.dtj_tglakhir)
							|| dtj_tglawal.equals(before.dtj_tglakhir)) {
						throw new Exception(
								Messages.get("tanggal.pembukaan.setelah.upload"));
					}
				}
				// Rule #5: jadwal pengumuman teknis dan pembukaan penawaran biaya harus sama
//				if (thpBefore == Tahap.PENGUMUMAN_PEMENANG_ADM_TEKNIS) {
//					String tglPengumuman = FormatUtils.formatDateTime(before.dtj_tglawal) + "|" + FormatUtils.formatDateTime(before.dtj_tglakhir);
//					String tglPembukaan = FormatUtils.formatDateTime(now.getDtj_tglawal()) + "|" + FormatUtils.formatDateTime(now.getDtj_tglakhir());
//					if (!tglPengumuman.equals(tglPembukaan)) {
//						throw new Exception(
//								Messages.get("tanggal.pengumuman.harus.sama.pembukaan"));
//					}
//				}
			}
			// Rule #6: akhir jadwal pengumuman eoi harus sama dengan akhir jadwal submission eoi
			if (akt_jenis == TahapDp.UPLOAD_EOI || akt_jenis == TahapDp.UPLOAD_EOI_ICB) {
				JadwalDp jadwalPengumumanEoi = pengumuman;
				String tglPengumuman = FormatUtils.formatTime(jadwalPengumumanEoi.dtj_tglakhir);
				String tglUpload = FormatUtils.formatTime(dtj_tglakhir);
				if (!tglPengumuman.equals(tglUpload)) {
					throw new Exception(
							Messages.get("tanggal.pengumuman.harus.sama.upload"));
				}
			}
		} else {
			throw new Exception(Messages.get("lelang.tmbt"));
		}
	}

	public static List<JadwalDp> findByLelang(Long lelangId)  {
		String sql = "select j.*, akt_jenis, akt_urut from devpartner.jadwal_dp j, devpartner.aktivitas_dp a where j.akt_id=a.akt_id and j.lls_id= ? order by akt_urut asc";
		return Query.find(sql,CUSTOME_RESULTSET,lelangId).fetch();
	}

	public static JadwalDp findByLelangNTahap(Long lelangId, TahapDp tahap) {
		List<JadwalDp> list = findByLelangNTahap(lelangId, new TahapDp[]{tahap});
		if(CommonUtil.isEmpty(list))
			return null;
		return list.get(0);
	}

	public static List<JadwalDp> findByLelangNTahap(Long lelangId, TahapDp... tahapList)  {
		StringBuilder sql = new StringBuilder("select j.*, akt_jenis, akt_urut from devpartner.jadwal_dp j, devpartner.aktivitas_dp a where j.akt_id=a.akt_id and j.lls_id=?");
		if(!ArrayUtils.isEmpty(tahapList)) {
			sql.append("AND a.akt_jenis in ('").append(StringUtils.join(tahapList, "','")).append("')");
		}
		return Query.find(sql.toString(), CUSTOME_RESULTSET,lelangId).fetch();
	}

	public static Long countJadwalkosong(Long lelangId) {
		return JadwalDp.count("lls_id=? and (dtj_tglawal is null or dtj_tglakhir is null)", lelangId);
	}

	/**Dapatkan jadwal lelang, jika saat ini ada lebih dari satu yang aktif, tampilkan  [...]
	 *
	 * @param lelangId
	 * @return
	 */
	public static String getJadwalSekarang(Long lelangId, boolean shortVersion){
		Date currentDate = BasicCtr.newDate();
		String tahaps = Query.find("SELECT devpartner.tahap_now(?, ?)", String.class, lelangId, currentDate).first();
		return TahapDp.tahapInfo(tahaps, shortVersion);
	}

    public static List<JadwalDp> getJadwalkosong(Long lelangId, MetodeDp metode) {
    	List<JadwalDp> list = new ArrayList<JadwalDp>();
    	List<AktivitasDp> aktivitasList = AktivitasDp.findByMetode(metode.id);
    	JadwalDp jadwal = null;
		for (AktivitasDp aktivitas:aktivitasList)
		{
			jadwal = JadwalDp.find("akt_id=? and lls_id=?", aktivitas.akt_id, lelangId).first();
			if(jadwal == null)
				jadwal = new JadwalDp();
			jadwal.lls_id = lelangId;
			jadwal.akt_id = aktivitas.akt_id;
			jadwal.thp_id = aktivitas.akt_jenis.id;
			jadwal.akt_jenis = aktivitas.akt_jenis;
			jadwal.akt_urut  = aktivitas.akt_urut;
			jadwal.save();
			list.add(jadwal);
		}
		return list;
	}

    /**copy jadwal dan langsung simpan ke database.
     * Cara ini akan ditinggalkan karena jika jadwal error maka yang tersimpan di database adalah data yang salah
     *
     * @param lelangId
     * @param lelangIdTujuan
     */
    public static void copyJadwal(Long lelangId, Long lelangIdTujuan) {
    	List<JadwalDp> listSource = JadwalDp.findByLelang(lelangIdTujuan);
    	JadwalDp obj = null;
    	Date currentDate = BasicCtr.newDate();
    	String fieldName=null;
    	int i =0 ;
    	for (JadwalDp jadwal : listSource) {
			obj = JadwalDp.find("lls_id=? and akt_id=?", lelangId, jadwal.akt_id).first();
			fieldName = "jadwal["+i+ ']';
			if(obj != null && jadwal != null) {
				obj.dtj_tglawal = jadwal.dtj_tglawal;
				obj.dtj_tglakhir = jadwal.dtj_tglakhir;
				try {
				if(obj.dtj_tglawal != null && obj.dtj_tglawal.before(currentDate))
					throw new Exception(Messages.get("lelang.tmsl"));
				if(jadwal.dtj_tglakhir != null && jadwal.dtj_tglakhir.before(currentDate))
					throw new Exception(Messages.get("lelang.tasl"));
				}catch (Exception e){
					Logger.error(e.getMessage());
					Validation.addError(fieldName, e.getLocalizedMessage());
				}
				obj.save();
			}
			i++;
		}
    }

    /*Copy jadwal ke dalam List<> lalu ditampilkan pada UI -- belum di-save ke database
     *
     */

    public static List<JadwalDp> copyJadwalIntoCache(Long lelangId, Long lelangIdTujuan) {
    	List<JadwalDp> listSource = JadwalDp.findByLelang(lelangId);
    	JadwalDp obj = null;
    	Date currentDate = BasicCtr.newDate();
    	String fieldName=null;
    	int i =0 ;
    	List<JadwalDp> listTarget=new ArrayList<JadwalDp>();
    	for (JadwalDp jadwal : listSource) {
			obj = JadwalDp.findByLelangNTahap(lelangIdTujuan, jadwal.akt_jenis);
			fieldName = "jadwal["+i+ ']';
			if(obj != null && jadwal != null) {
				obj.dtj_tglawal = jadwal.dtj_tglawal;
				obj.dtj_tglakhir = jadwal.dtj_tglakhir;
				try {
				if(obj.dtj_tglawal != null && obj.dtj_tglawal.before(currentDate))
					throw new Exception(Messages.get("lelang.tmsl"));
				if(jadwal.dtj_tglakhir != null && jadwal.dtj_tglakhir.before(currentDate))
					throw new Exception(Messages.get("lelang.tasl"));
				}catch (Exception e){
					Logger.error(e.getMessage());
					Validation.addError(fieldName, e.getLocalizedMessage());
				}
				listTarget.add(obj);
			}
			i++;
		}
    	return listTarget;
    }

	/**
	 * simpan jadwal dengan sebelumnya mengisi keterangan hasil input,
	 * asumsi harus lolos validasi dulu ({@code lelang.JadwalCtr.validate})
	 * @param jadwalList
	 * @param lelang
	 * @param alasan alasan perubahan jadwal
	 * @return true jika tidak ada error
	 */
	public static void simpanJadwal(LelangDp lelang, List<JadwalDp> jadwalList, Date currentDate, String alasan) throws Exception {
		boolean send_email = false;
		List<JadwalDp> jadwalOri = JadwalDp.findByLelang(lelang.lls_id);
		List<JadwalDp> jadwalBefore = new ArrayList<>(jadwalOri);
		JadwalDp jadwal=null, jadwalO=null;
		for (int i = 0; i < jadwalList.size(); i++) {
			jadwal = jadwalList.get(i);
			jadwalO = jadwalOri.get(i);
			AktivitasDp akt = AktivitasDp.findById(jadwal.akt_id); // load tahap saat ini (akt_jenis null)
			jadwal.akt_jenis = akt.akt_jenis;
			jadwal.akt_id = jadwalO.akt_id;
			if (lelang.lls_status.isAktif()) {
				boolean ada_perubahan = !jadwal.equals(jadwalO);
				if(ada_perubahan) {
					if (!CommonUtil.isEmpty(alasan)) {
						jadwal.dtj_keterangan = alasan;
					}
				}
				if (ada_perubahan && jadwal.dtj_tglawal != null	&& jadwal.dtj_tglakhir != null) {
					// simpan perubahan
					HistoryJadwalDp historyJadwal = new HistoryJadwalDp();
					historyJadwal.dtj_id = jadwalO.dtj_id;
					historyJadwal.hjd_tanggal_edit = currentDate;
					historyJadwal.hjd_tanggal_awal = jadwalO.dtj_tglawal;
					historyJadwal.hjd_tanggal_akhir = jadwalO.dtj_tglakhir;
					historyJadwal.hjd_keterangan = jadwal.dtj_keterangan;
					historyJadwal.save();
					send_email = true;
				}
			}
			jadwalO.dtj_tglawal = jadwal.dtj_tglawal;
			jadwalO.dtj_tglakhir = jadwal.dtj_tglakhir;
			jadwalO.dtj_keterangan = jadwal.dtj_keterangan;
			jadwalO.save();
		}
		// jika paket spse 4 harus update state saat ubah jadwal dan lelang sedang berjalan
		if(lelang.lls_status.isAktif()) {
			if(send_email)
			{
				try {
					MailDpDao.kirimPerubahanJadwalLelang(lelang, jadwalBefore, jadwalList, currentDate);
				} catch (Exception e) {
					throw new Exception(Messages.get("lelang.tkdppep"));
				}
			}
		}
	}

	/**
	 * simpan jadwal baru yang membutuhkan persetujuan admin ppe
	 * asumsi harus lolos validasi dulu ({@code devpartner.JadwalDp.validate})
	 * @param jadwalList
	 * @param lelang
	 * @param alasan alasan perubahan jadwal
	 * @return true jika tidak ada error
	 */
	public static void simpanJadwalDraft(LelangDp lelang, List<JadwalDp> jadwalList, String alasan) throws Exception {
		List<JadwalDp> jadwalOri = JadwalDp.findByLelang(lelang.lls_id);
		JadwalDp jadwal=null, jadwalO=null;
		for (int i = 0; i < jadwalList.size(); i++) {
			JadwalDraftDp jadwalDraft = new JadwalDraftDp();
			jadwal = jadwalList.get(i);
			jadwalO = jadwalOri.get(i);
			jadwalDraft.dtjd_tglawal = jadwal.dtj_tglawal;
			jadwalDraft.dtjd_tglakhir = jadwal.dtj_tglakhir;
			jadwalDraft.dtjd_keterangan = alasan;
			jadwalDraft.dtj_id = jadwalO.dtj_id;
			jadwalDraft.save();
		}
	}

	/**
	 * approve jadwal baru yang membutuhkan persetujuan admin ppe
	 * @param lelang
	 * @return true jika tidak ada error
	 */
	public static void approveJadwalDraft(LelangDp lelang, Date currentDate) throws Exception {
		boolean send_email = false;
		List<JadwalDp> jadwalList = JadwalDp.findByLelang(lelang.lls_id);
		List<JadwalDp> jadwalBefore = new ArrayList<>(jadwalList);
		for (int i = 0; i < jadwalList.size(); i++) {
			JadwalDp jadwal = jadwalList.get(i);
			JadwalDraftDp jadwalDraft = JadwalDraftDp.findByJadwal(jadwal.dtj_id);
			boolean ada_perubahan = !jadwal.equals(jadwalDraft);
			if (ada_perubahan) {
				// simpan perubahan
				HistoryJadwalDp historyJadwal = new HistoryJadwalDp();
				historyJadwal.dtj_id = jadwal.dtj_id;
				historyJadwal.hjd_tanggal_edit = currentDate;
				historyJadwal.hjd_tanggal_awal = jadwal.dtj_tglawal;
				historyJadwal.hjd_tanggal_akhir = jadwal.dtj_tglakhir;
				historyJadwal.hjd_keterangan = jadwal.dtj_keterangan;
				historyJadwal.save();
				send_email = true;
			}

			jadwal.dtj_keterangan = jadwalDraft.dtjd_keterangan;
			jadwal.dtj_tglawal = jadwalDraft.dtjd_tglawal;
			jadwal.dtj_tglakhir = jadwalDraft.dtjd_tglakhir;
			jadwal.save();
		}

		JadwalDraftDp.deleteByLelang(lelang.lls_id);

		if(send_email)
		{
			try {
				MailDpDao.kirimPerubahanJadwalLelang(lelang, jadwalBefore, jadwalList, currentDate);
			} catch (Exception e) {
				throw new Exception(Messages.get("lelang.tkdppep"));
			}
		}
	}

	public static String periodToString(Period p) {
		if(p==null)
			return null;
		p=p.normalizedStandard();
		StringBuilder str=new  StringBuilder();
		if(p.getMonths() > 0)
			str.append(p.getWeeks()).append(" "+Messages.get("lelang.bulan")+" ");
		if(p.getWeeks() > 0)
			str.append(p.getWeeks()).append(" "+Messages.get("lelang.minggu")+" ");
		if(p.getDays() > 0)
			str.append(p.getDays()).append(" "+Messages.get("lelang.hari")+" ");
		if(p.getHours() > 0)
			str.append(p.getHours()).append(" "+Messages.get("lelang.jam")+" ");
		if(p.getMinutes() > 0)
			str.append(p.getMinutes()).append(" "+Messages.get("lelang.menit")+" ");
		return str.toString();
	}

	public String getPeriodAsString()
	{
		Period period=null;
		if(dtj_tglawal != null & dtj_tglakhir !=null)
			period= new Period(dtj_tglakhir.getTime()-dtj_tglawal.getTime());
		if(period==null)
			return "";
		else
			return periodToString(period);
	}

	// find TahapDp mulai evaluasi ulang
	public static JadwalDp findTahapMulaiEvaluasiUlang(LelangDp lelang) {
		JadwalDp jadwal = null;
//		if(lelang.isPascakualifikasi())
//		{
//			jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.EVALUASI_PENAWARAN);
//			if(jadwal ==  null)
//				jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.EVALUASI_PENAWARAN_KUALIFIKASI);
//			if(lelang.isDuaFile()) {
//				jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS);
//				if(jadwal ==  null)
//					jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI);
//			}
//		}
//		else {
//			Long count = AktivitasDp.count("akt_id in (select akt_id from devpartner.jadwal_dp where lls_id=? and dtj_tglawal >=?) AND akt_jenis IN ('PEMBUKAAN_PENAWARAN','PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS')",
//					lelang.lls_id, BasicCtr.newDate());
//			if(count != null && count > 0L)
//				jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.EVALUASI_DOK_PRA);
//			else if(lelang.isSatuFile())
//				jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.PEMBUKAAN_PENAWARAN);
//			else
//				jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, TahapDp.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS);
//		}
		return jadwal;
	}

	public static TahapDp findTahapPemasukanPenawaranUlang(LelangDp lelang) {
		return TahapDp.PEMASUKAN_PENAWARAN;
	}

	@Override
	protected void prePersist() {
		if(dtj_id == null) { // checking jadwal sebelum insert, menghidari double jadwal
			JadwalDp jadwalExist = JadwalDp.findByLelangNTahap(lls_id, akt_jenis);
			if(jadwalExist != null)
				jadwalExist.delete();
		}
		super.prePersist();
	}

	// check validitas list jadwal tender
	//semua jadwal akan dicek dulu valid atau tidak, kemudian akan di simpan
	public static void validate(LelangDp lelang, List<JadwalDp> jadwalList, Date currentDate, String alasan, int validasiUrutan, boolean harusBerubah) {
		List<JadwalDp> jadwalOri = JadwalDp.findByLelang(lelang.lls_id);
		JadwalDp jadwal=null, jadwalO=null,before=null;
		String fieldName = null;
		for (int i = validasiUrutan; i < jadwalList.size(); i++) {
			jadwal = jadwalList.get(i);
			jadwalO = jadwalOri.get(i);
			AktivitasDp akt = AktivitasDp.findById(jadwal.akt_id); // load tahap saat ini (akt_jenis null)
			jadwal.akt_jenis = akt.akt_jenis;
			jadwal.akt_id = jadwalO.akt_id;
			fieldName = "jadwal["+i+ ']';
			// jika tidak ada informasi waktu maka set menjadi jam 23:59:59
			// Tp metode ini mungkin ada bugnya, pls dicek lagi (ADY)
			if (jadwal.dtj_tglakhir != null) {
				DateTime dateTime = new DateTime(jadwal.dtj_tglakhir.getTime());
				if (dateTime.getHourOfDay() == 0 && dateTime.getMinuteOfHour() == 0) // jika waktu jadwal terakhir 00:00
				{
					DateTime result = dateTime.withHourOfDay(23).withMinuteOfHour(59);
					jadwal.dtj_tglakhir = result.toDate();
				}
			}
			if(i > 0) {
				before = jadwalList.get(i - 1);
			}
			try {
				if (lelang.lls_status.isDraft()) {
					jadwal.validate(currentDate, before, jadwalList.get(0), i==0, lelang.lls_status.isDraft());
				} else if (lelang.lls_status.isAktif()) {
					boolean ada_perubahan = harusBerubah || !jadwal.equals(jadwalO);
					if (ada_perubahan) {
						jadwal.validate(currentDate, before, jadwalList.get(0), i==0, lelang.lls_status.isDraft() || harusBerubah);
						// jika alasan di-supply oleh parameter, isi keterangan perubahan seluruh jadwal yang berubah
						if(!CommonUtil.isEmpty(alasan)) {
							jadwal.dtj_keterangan = alasan;
						}
						if (CommonUtil.isEmpty(jadwal.dtj_keterangan)) {
							throw new Exception(Messages.get("lelang.hiapjpkk"));
						} else if (jadwal.dtj_keterangan.trim().length() <= 30) {
							throw new Exception(Messages.get("lelang.apjpkkhld"));
						}
					}

				}
			} catch (Exception e){
				Validation.addError(fieldName, e.getLocalizedMessage());
				Logger.error(e, e.getMessage());
			}
		}
	}

	public static List<JadwalFlash> getJadwalFlash(List<JadwalDp> jadwalList) {
		JadwalFlash obj = null;
		List<JadwalFlash> jadwalFlash = new ArrayList<JadwalFlash>();
		for(JadwalDp jadwal : jadwalList) {
			obj = new JadwalFlash();
			obj.dtj_id = jadwal.dtj_id;
			obj.dtj_tglawal = jadwal.dtj_tglawal;
			obj.dtj_tglakhir = jadwal.dtj_tglakhir;
			jadwalFlash.add(obj);
		}
		return jadwalFlash;
	}

	public static List<JadwalDp> getJadwalList(List<JadwalFlash> jadwalFlash, List<JadwalDp> jadwalList) {
		return getJadwalList(jadwalFlash, jadwalList, true, null);
	}

	public static List<JadwalDp> getJadwalList(List<JadwalFlash> jadwalFlash, List<JadwalDp> jadwalList, boolean allowEditPast, Date now) {
		for(JadwalDp jadwal : jadwalList) {
			for(JadwalFlash jf : jadwalFlash) {
				if (jadwal.dtj_id.equals(jf.dtj_id)) {
					if (allowEditPast || jadwal.dtj_tglawal.after(now)) {
						jadwal.dtj_tglawal = jf.dtj_tglawal;
					}
					if (allowEditPast || jadwal.dtj_tglakhir.after(now)) {
						jadwal.dtj_tglakhir = jf.dtj_tglakhir;
					}
				}
			}
		}
		return jadwalList;
	}

	public static class DateConverter implements JsonDeserializer<Date> {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
		DateFormat df2 = new SimpleDateFormat("MMM dd,yyyy hh:mm:ss a");
		DateFormat df3 = new SimpleDateFormat("MMM dd,yyyy");

		@Override
		public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
				throws JsonParseException {
			try {
				return df.parse(json.getAsString());
			} catch (ParseException e) {
				try {
					return df2.parse(json.getAsString());
				} catch (ParseException ex) {
					try {
						return df3.parse(json.getAsString());
					} catch (ParseException ex3) {
						return null;
					}
				}
			}
		}

	}
}
