package models.devpartner;

import ext.FormatUtils;
import models.agency.Ppk;
import models.agency.Satuan_kerja;
import models.common.SumberDana;
import models.devpartner.contracts.AnggaranDpContract;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Collections;
import java.util.List;


/**
 * Model {@code Anggaran} merepresentasikan tabel Anggaran pada database.
 *
 * @author I Wayan Wiprayoga W
 */
@Table(name="anggaran_dp", schema = "devpartner")
public class AnggaranDp extends BaseModel implements AnggaranDpContract {

	/**
	 * ID anggaran
	 */
	@Id(sequence="devpartner.seq_anggaran_dp", function="nextsequence")
	public Long ang_id;
	/**
	 * kode Anggaran
	 */
	public String ang_koderekening;
	/**
	 * Deskripsi nilai anggaran
	 */
	public Double ang_nilai;
	/**
	 * uraian kegiatan terkait anggaran tesebut
	 */
	public String ang_uraian;
	/**
	 * tahun anggaran
	 */
	public Integer ang_tahun;
	/**
	 * Satker yang memiliki objek anggaran ini
	 */
	public Long stk_id;
	/**
	 * Sumber dana anggaran
	 */
	public SumberDana sbd_id;
	
	@Transient
	private Satuan_kerja satuan_kerja;

	/**
	 * @deprecated untuk mendapatkan ppk harap menggunakan paket_anggaran*/
	@Deprecated
	@Transient
	private Ppk ppk;
	/**
	 * @deprecated untuk mendapatkan ppk harap menggunakan paket_anggaran_pl*/
	@Deprecated
	@Transient
	private Ppk ppkPl;

	public Satuan_kerja getSatuan_kerja() {
		if(satuan_kerja == null)
			satuan_kerja = Satuan_kerja.findById(stk_id);
		return satuan_kerja;
	}

	public Ppk getPpk() {
		if (ppk != null) {
			return ppk;
		}
		PaketAnggaranDp paket_anggaran = PaketAnggaranDp.find("ang_id=?", ang_id).first();
		if (paket_anggaran != null && paket_anggaran.ppk_id != null)
			ppk = Ppk.findById(paket_anggaran.ppk_id);
		return ppk;
	}

	/**
	 * Manipulasi nilai anggaran menjadi string dalam format rupiah
	 *
	 * @return string nilai anggaran dalam format rupiah
	 */
	@Transient
	public String getNilai() {
		return FormatUtils.formatCurrencyRupiah(this.ang_nilai);
	}

	/**
	 * Manipulasi parameter {@code nilai} yang didapat dari form menjadi tipe
	 * data {@code Double}
	 *
	 * @param nilai string nilai anggaran
	 */
	@Transient
	public void setNilai(String nilai) {
		this.ang_nilai = Double.valueOf(nilai.trim().replace("Rp", "").replace(",", "."));
	}
	
	/**
	 * mendapatkan list anggaran dari paket
	 * @return
	 */
	public static List<AnggaranDp> findByPaket(Long paketId) {
		if(paketId == null)
			return Collections.EMPTY_LIST;
		return find("ang_id in (select ang_id from devpartner.paket_anggaran_dp where pkt_id=?)", paketId).fetch();
	}
	
	public static AnggaranDp findByAnggaranId(Long angId) {
		return find("ang_id =?", angId).first();
	}
	
	public static List<String> getSumberAnggaranPaket(Long paketId)
	{
		return Query.find("SELECT (a.ang_tahun ||' - '|| s.sbd_ket) as label FROM devpartner.anggaran_dp a,devpartner.paket_anggaran_dp p, Sumber_dana s where a.ang_id = p.ang_id "+
			"and a.sbd_id = s.sbd_id and p.pkt_id =?", String.class, paketId).fetch();

	}
	
	public static List<Integer> listTahunAnggaran() {
		return Query.find("SELECT DISTINCT ang_tahun FROM devpartner.anggaran_dp ORDER BY ang_tahun", Integer.class).fetch();
	}

	@Override
	public String getPpkNama() {
		return getPpk() != null ? getPpk().getNamaPegawai() : "";
	}

	@Override
	public Long getId() {
		return ang_id;
	}

	@Override
	public boolean isPpkExist() {
		return ppk != null;
	}

	@Override
	public String getKodeRekening() {
		return ang_koderekening;
	}

	@Override
	public Double getAngNilai() {
		return ang_nilai;
	}

	@Override
	public Integer getAngTahun() {
		return ang_tahun;
	}

	@Override
	public String getSbdLabel() {
		return sbd_id != null ? sbd_id.getLabel() : "";
	}

}
