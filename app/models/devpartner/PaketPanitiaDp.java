package models.devpartner;

import models.agency.Paket;
import models.agency.Panitia;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Model {@code Paket_panitia} merepresentasikan tabel paket_panitia pada
 * database.
 *
 * @author I Wayan Wiprayoga W
 */

@Table(name="paket_panitia_dp", schema = "devpartner")
public class PaketPanitiaDp extends BaseModel {

	/**
	 * Paket yang dibuat oleh panitia tertentu
	 */
	@Id
	public Long pkt_id;
	/**
	 * Objek kepanitiaan yang membuat paket
	 */
	@Id
	public Long pnt_id;

	@Transient
	private PaketDp paket;
	@Transient
	private Panitia panitia;

	public PaketPanitiaDp() {}

	public PaketPanitiaDp(Paket model) {
		this.pkt_id = model.pkt_id;
		this.pnt_id = model.pnt_id;
	}

	public static PaketPanitiaDp getByPktIdAndPntId(Long pktId, Long pntId) {
		return find("pkt_id=? and pnt_id=?", pktId, pntId).first();
	}

	public static PaketPanitiaDp getByPktId(Long pktId){
		return find("pkt_id=?", pktId).first();
	}
	
	public PaketDp getPaket() {
		if(paket == null)
			paket = PaketDp.findById(pkt_id);
		return paket;
	}
	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}

}
