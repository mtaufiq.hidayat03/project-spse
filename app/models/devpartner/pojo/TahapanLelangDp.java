package models.devpartner.pojo;

import models.devpartner.common.TahapDp;
import play.db.jdbc.Query;

import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
public class TahapanLelangDp {
	
	public Long lls_id;

	public Integer akt_urut;

	public Long dtj_id;

	public Date dtj_tglawal;

	public Date dtj_tglakhir;

	public TahapDp akt_jenis;

	public Integer jml_history;
	
	public boolean isNow(Date curDate) {
		if (dtj_tglawal != null && dtj_tglakhir != null)
			return (curDate.after(dtj_tglawal) && curDate.before(dtj_tglakhir));
		return false;
	}
	
	public String namaTahap() {
		return akt_jenis.getLabel();
	}
	
	public static final String SQL = "SELECT lls_id, akt_urut, dtj_id, dtj_tglawal, dtj_tglakhir , akt_jenis, "
			+ "(SELECT count(dtj_id) FROM devpartner.history_jadwal_dp where dtj_id=j.dtj_id) jml_history "
			+ "FROM devpartner.jadwal_dp j, devpartner.aktivitas_dp a where j.akt_id=a.akt_id and j.lls_id=? order by akt_urut asc";
	
	public static List<TahapanLelangDp> findByLelang(Long lelangId) {
		return Query.find(SQL, TahapanLelangDp.class, lelangId).fetch();
	}
}
