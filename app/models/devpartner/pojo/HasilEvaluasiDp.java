package models.devpartner.pojo;

import ext.FormatUtils;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;
import play.i18n.Messages;

import java.io.Serializable;
import java.util.List;
import java.util.StringJoiner;

/**
 * Model hasil Evaluasi Peserta
 * @author idoej
 *
 */
public class HasilEvaluasiDp implements Serializable{

	public Long psr_id;
	public Long rkn_id;
	public String rkn_nama;
    public String rkn_npwp;
	public Double psr_harga;
	public Double psr_harga_terkoreksi;
	public Integer harga; // status evaluasi harga
	public Integer kualifikasi; // status shortlist
	public String als_kualifikasi;
	public Integer teknis; // status evaluasi teknis
	public String als_teknis;
	public String als_harga;
	public Integer pemenang; // status evaluasi akhir
	public Double nev_harga;
	public Double nev_harga_terkoreksi;
	public Double nev_harga_negosiasi;
	public Double skor;
	public Double skor_teknis;
    public Double skor_harga;
	public Double skor_akhir;
	public Double pkt_pagu;
	public Double pkt_hps;
	public String psr_alasan_menang;
	public Integer final_rank;
	public Integer rank;
	public Integer gagal_kontrak;

	public boolean isLulusTeknis() {
		return teknis != null && teknis == 1;
	}
	
	public boolean isLulusHarga() {
		return harga != null && harga == 1;
	}
	
	public boolean isLulusKualifikasi() {
		return kualifikasi != null && kualifikasi == 1;
	}

	public boolean isLulusRank() {
		return rank != null && rank == 1;
	}

	public boolean isPemenang() {
		return pemenang != null && pemenang == 1;
	}

	public boolean isGagalKontrak() {
		return gagal_kontrak != null && gagal_kontrak == 1;
	}

	public String getPenawaran(){
		if(nev_harga != null)
			return FormatUtils.formatCurrencyRupiah(nev_harga);
		return FormatUtils.formatCurrencyRupiah(psr_harga);
	}
	
	public String getPenawaranTerkoreksi(){
		if(nev_harga_terkoreksi != null)
			return FormatUtils.formatCurrencyRupiah(nev_harga_terkoreksi);
		return FormatUtils.formatCurrencyRupiah(psr_harga_terkoreksi);
	}

	public String getAlasan(){
		String alasan="";
		if(!StringUtils.isEmpty(als_teknis))
			alasan += als_teknis;
		if(!StringUtils.isEmpty(als_harga))
			alasan += als_harga;
		if(!StringUtils.isEmpty(als_kualifikasi))
			alasan += als_kualifikasi;
		if(!StringUtils.isEmpty(psr_alasan_menang))
			alasan += psr_alasan_menang;
		return alasan;
	}

	public String getAlasanKualifikasi(){
		String alasan="";
		if(!StringUtils.isEmpty(als_kualifikasi))
			alasan += als_kualifikasi;
		return alasan;
	}

	public String getAlasanTeknis(){
		String alasan="";
		if(!StringUtils.isEmpty(als_teknis))
			alasan += als_teknis;
		return alasan;
	}
	
	public String gethargaNegosiasi() {
		if(nev_harga_negosiasi != null)
			return FormatUtils.formatCurrencyRupiah(nev_harga_negosiasi);
		return "";
	}

	public String getHargaFinal(){
		Double hargaFinal =  nev_harga_negosiasi != null ? nev_harga_negosiasi : nev_harga_terkoreksi;
		if(hargaFinal != null)
			return FormatUtils.formatCurrencyRupiah(hargaFinal);
		return "";
	}

	public String getNotifHargaPenawaran() {
		StringJoiner notif = new StringJoiner("");
		if((nev_harga != null && nev_harga > pkt_hps) || (psr_harga != null && psr_harga >  pkt_hps))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.mnt_hps") + "</span>");
		if((nev_harga != null && nev_harga > pkt_pagu) || (psr_harga != null && psr_harga >  pkt_pagu))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.m_pagu") + "</span>");
		return notif.toString();
	}

	public String getNotifHargaPenawaranTerkoreksi() {
		StringJoiner notif = new StringJoiner("");
		if((nev_harga_terkoreksi != null && nev_harga_terkoreksi > pkt_hps) || (psr_harga_terkoreksi != null && psr_harga_terkoreksi >  pkt_hps))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.mnt_hps") + "</span>");
		if((nev_harga_terkoreksi != null && nev_harga_terkoreksi > pkt_pagu) || (psr_harga_terkoreksi != null && psr_harga_terkoreksi >  pkt_pagu))
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.m_pagu") + "</span>");
		return notif.toString();
	}

	public String getNotifHargaNegosiasi() {
		StringJoiner notif = new StringJoiner("");
		if(nev_harga_terkoreksi != null && nev_harga_negosiasi!=null && nev_harga_negosiasi > pkt_hps)
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.mnt_hps") + "</span>");
		if(nev_harga_terkoreksi != null && nev_harga_negosiasi!=null && nev_harga_negosiasi > pkt_pagu)
			notif.add("<span class=\"label label-danger\">" + Messages.get("lelang.m_pagu") + "</span>");
		return notif.toString();
	}
	
	 /**
	  * untuk dapatkan resume hasil evaluasi lelang terakhir
	  * @param lelangId
	  * @return
	  */	
	public static List<HasilEvaluasiDp> findByLelang(Long lelangId) {
		QueryBuilder builder = QueryBuilder.create("SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp, psr_harga, psr_harga_terkoreksi, k.nev_lulus AS kualifikasi, k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi,")
		.append("t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga,  h.nev_harga, h.nev_harga_terkoreksi, h.nev_skor AS skor_harga, s.nev_lulus AS pemenang, pkt_hps, pkt_pagu,")
		.append("h.nev_uraian AS als_harga, s.nev_skor AS skor_akhir, psr_alasan_menang, s.nev_harga_negosiasi, s.nev_urutan as final_rank, s.nev_rank as rank, s.nev_gagal as gagal_kontrak ")
		.append("FROM devpartner.peserta_dp p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id ")
		.append("LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, (SELECT eva_id FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = 0 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) k ON p.psr_id=k.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, (SELECT eva_id FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = 1 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) t ON p.psr_id=t.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, (SELECT eva_id FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = 2 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) h ON p.psr_id=h.psr_id", lelangId)
		.append("LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, (SELECT eva_id FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = 3 ORDER BY eva_versi DESC limit 1) e WHERE n.eva_id=e.eva_id) s ON p.psr_id=s.psr_id", lelangId)
		.append("LEFT JOIN devpartner.lelang_dp l ON p.lls_id=l.lls_id LEFT JOIN devpartner.paket_dp pkt ON pkt.pkt_id=l.pkt_id WHERE p.lls_id=? ORDER BY nev_harga asc", lelangId);
		return Query.findList(builder, HasilEvaluasiDp.class);
	}

	public static HasilEvaluasiDp findByPeserta(Long pesertaId) {
		String sql = String.join(" ", "SELECT p.psr_id,r.rkn_id, r.rkn_nama, r.rkn_npwp, psr_harga, psr_harga_terkoreksi, k.nev_lulus AS kualifikasi, k.nev_skor AS skor, k.nev_uraian AS als_kualifikasi,",
				"t.nev_lulus AS teknis, t.nev_skor as skor_teknis, t.nev_uraian AS als_teknis, h.nev_lulus as harga,  h.nev_harga, h.nev_harga_terkoreksi, h.nev_skor AS skor_harga, s.nev_lulus AS pemenang, pkt_hps, pkt_pagu,",
				"h.nev_uraian AS als_harga, s.nev_skor AS skor_akhir, psr_alasan_menang, s.nev_harga_negosiasi, s.nev_urutan as final_rank, s.nev_rank as rank, s.nev_gagal as gagal_kontrak ",
				"FROM devpartner.peserta_dp p LEFT JOIN rekanan r ON p.rkn_id=r.rkn_id ",
                "LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, devpartner.evaluasi_dp e WHERE n.eva_id=e.eva_id AND eva_jenis = 0 ) k ON p.psr_id=k.psr_id",
                "LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, devpartner.evaluasi_dp e WHERE n.eva_id=e.eva_id AND eva_jenis = 1 ) t ON p.psr_id=t.psr_id",
                "LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, devpartner.evaluasi_dp e WHERE n.eva_id=e.eva_id AND eva_jenis = 2 ) h ON p.psr_id=h.psr_id",
                "LEFT JOIN (SELECT * FROM devpartner.nilai_evaluasi_dp n, devpartner.evaluasi_dp e WHERE n.eva_id=e.eva_id AND eva_jenis = 3 ) s ON p.psr_id=s.psr_id",
				"LEFT JOIN devpartner.lelang_dp l ON p.lls_id=l.lls_id LEFT JOIN devpartner.paket_dp pkt ON pkt.pkt_id=l.pkt_id WHERE p.psr_id=:id");
		return Query.find(sql, HasilEvaluasiDp.class).setParameter("id", pesertaId).first();
	}

}
