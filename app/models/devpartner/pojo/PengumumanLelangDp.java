package models.devpartner.pojo;

import controllers.BasicCtr;
import models.common.Kategori;
import models.common.Metode;
import models.devpartner.common.KategoriDp;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * fungsi class ini untuk model bentuk pengumuman lelang di halaman muka
 *
 * @author Javax
 */

public class PengumumanLelangDp {

	public static final int ROW_MAX = 6;

	private static final Map<Kategori, List<PengumumanLelangDp>> kategoriMap = new ConcurrentHashMap<>(6);

	public Long lls_id;

	public String pkt_nama;

	public Date tgl_akhir_daftar; // jadwal akhir masa upload penawaran (jika pasca) dan upload dok_pra (jika prakualifikasi)

	public Kategori kgr_id;

	public Boolean pkt_hps_enable;
	
	public Double pkt_hps;


	/**
	 * informasi lelang yang sedang tahap pengumuman
	 * tahap pengumuman yang dimaksud adalah :Tahap.PENGUMUMAN_LELANG, Tahap.UMUM_PRAKUALIFIKASI, Tahap.AMBIL_DOKUMEN_PEMILIHAN
	 * Tahap.AMBIL_DOKUMEN, Tahap.AMBIL_DOK_PRA
	 * hasil query akan dicache selama 1 menit
	 *
	 * @return
	 */
	public static List<PengumumanLelangDp> findAll() {
		List<PengumumanLelangDp> list = (List<PengumumanLelangDp>) Cache.get("pengumumanLelangDp");
		if (CommonUtil.isEmpty(list)) {
			String id_metode_pasca = StringUtils.join(Metode.METODE_PASCA_ID, ",");
			String id_metode_pra = StringUtils.join(Metode.METODE_PRA_ID, ",");
			Date time = BasicCtr.newDate();
			QueryBuilder query = new QueryBuilder("SELECT l.lls_id, pkt_nama, kgr_id, j.dtj_tglakhir as tgl_akhir_daftar ")
					.append("FROM devpartner.lelang_dp l ")
					.append("inner join devpartner.paket_dp p on l.pkt_id=p.pkt_id ")
					.append("inner join devpartner.jadwal_dp j on l.lls_id=j.lls_id ")
					.append("inner join devpartner.aktivitas_dp a on a.akt_id=j.akt_id and a.akt_urut=1 ")
					.append("where l.lls_status = 1 and j.dtj_tglawal <= ? and j.dtj_tglakhir >= ?", time, time)
					.append("ORDER BY kgr_id ASC, lls_id DESC");
			list = Query.find(query, PengumumanLelangDp.class).fetch();
			if(list == null) //Cache is not allowing the null values.
				list = new ArrayList<>();
			Cache.safeSet("pengumumanLelangDp", list, BasicCtr.DEFAULT_CACHE);
		}
		return list;
	}

	public static Map<Kategori, List<PengumumanLelangDp>> findAsMap() {
		for (Kategori kategori : KategoriDp.all) {
			kategoriMap.put(kategori, new ArrayList<PengumumanLelangDp>());
		}
		List<PengumumanLelangDp> list = findAll();
		if (!CommonUtil.isEmpty(list)) { // organize map
			for (PengumumanLelangDp item : list)
				if(kategoriMap.get(item.kgr_id) != null && !kategoriMap.get(item.kgr_id).contains(item)) {
					kategoriMap.get(item.kgr_id).add(item);
				}
		}
		return kategoriMap;
	}
	public Boolean isEnableViewHps(){
		return pkt_hps_enable != null ? pkt_hps_enable : true;
	}
}
