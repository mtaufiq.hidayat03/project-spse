package models.devpartner.pojo;

import models.LOVItem;
import models.devpartner.common.MetodeDp;
import play.db.jdbc.Query;

import java.util.Date;
import java.util.List;

/**
 * Note: Componen LOV for DP only
 * 
 * @author idoej
 *
 */
public class LOVItemDp {
	public static List<LOVItem> getLelangSamaMetodeYangHanyaAdaDiJadwal(Long lls_id, MetodeDp metode) {
		Date now=controllers.BasicCtr.newDate();
		int urut = 1;
		return Query.find("select lls_id as id, pkt_nama as label from devpartner.lelang_dp l, devpartner.paket_dp p " +
				"where exists(select lls_id from devpartner.jadwal_dp j join devpartner.aktivitas_dp a on j.akt_id = a.akt_id " +
				"where j.lls_id = l.lls_id AND dtj_tglawal > ? AND a.akt_urut = ?) "
				+ "and l.pkt_id=p.pkt_id and l.lls_id <> ? and l.mtd_id = ? order by pkt_nama", LOVItem.beanCreatorString, now, urut, lls_id, metode.id).fetch();
	}
}
