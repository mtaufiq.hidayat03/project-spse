package models.devpartner.pojo;


import controllers.BasicCtr;
import models.common.Kategori;
import models.common.StatusLelang;
import models.devpartner.AnggaranDp;
import models.devpartner.PaketDp;
import models.devpartner.PaketLokasiDp;
import models.devpartner.common.*;
import models.jcommon.util.CommonUtil;
import models.lelang.StatusTender;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.utils.HTML;

import java.util.Date;
import java.util.List;

public class LelangDpDetil implements StatusTender {
	
	public Long lls_id;
    public Integer mtd_id;
	public Integer lls_versi_lelang;
	public Integer lls_penetapan_pemenang;
	public StatusLelang lls_status;
	public Long pkt_id;
	public Long rup_id;
	public Integer kgr_id;
	public String pkt_nama;
	public Double pkt_pagu;
	public Double pkt_hps;
	public String agc_nama;
    public String nama; // nama instansi
	public String stk_nama;	
	public Integer psr_total;
	public String lls_keterangan;
	public String lls_ditutup_karena;
	public String lls_diulang_karena;
	public Integer lls_kontrak_pembayaran;
	public Integer lls_kontrak_tahun;
	public Integer lls_kontrak_sbd;
	public Double lls_bobot_teknis;
	public Double lls_bobot_biaya;
	public String kls_id;
	public Date lls_dibuat_tanggal;
	public String tahaps; // tahap sekarang
    public String nama_instansi;
    public String nama_satker;
    public Integer lls_penawaran_ulang;

	public PaketDp getPaket() {
		return PaketDp.findById(pkt_id);
	}

    public MetodeDp getMetode() {
        return MetodeDp.findById(mtd_id);
    }

    public Kategori getKategori() {
        return Kategori.findById(kgr_id);
    }

    public MetodeEvaluasiDp getEvaluasi() {
        return getMetode().evaluasi;
    }

    public JenisKontrakDp getKontrak_pembayaran() {
        return JenisKontrakDp.findById(lls_kontrak_pembayaran);
    }

    public KontrakTahunDp getKontrak_tahun() {
        return KontrakTahunDp.findById(lls_kontrak_tahun);
    }

    public SumberDanaDp getKontrak_sbd() {
        return SumberDanaDp.findById(lls_kontrak_sbd);
    }

	public boolean isPenawaranUlang()
	{
		if(lls_penawaran_ulang == null)
			return false;
		return lls_penawaran_ulang == 1;
	}

	@Override
	public boolean isItemized() {
		return false;
	}

	boolean isPenawaranEvaluasiUlang() {
		return isPenawaranUlang() && isEvaluasiUlang();
	}

	public boolean isEvaluasiUlang() {
		return false;
	}

	@Override
	public boolean isKonsultansi() {
		return false;
	}

	@Override
	public boolean isJkKonstruksi() {
		return false;
	}

	public boolean isLelangUlang()
	{
		if(lls_versi_lelang == null)
			return false;
		return lls_versi_lelang > 1;
	}	
	
    public String getNamaInstansi() {
        if(CommonUtil.isEmpty(agc_nama))
            return nama;
        return agc_nama;
    }

    public String getAnggaran() {
        StringBuilder anggaran = new StringBuilder();
        List<AnggaranDp> anggaranlist = AnggaranDp.findByPaket(pkt_id);
        if(CommonUtil.isEmpty(anggaranlist))
            return anggaran.toString();

        anggaran.append("<table class=\"table table-condensed table-bordered\">");
        anggaran.append("<thead><tr><th>"+ Messages.get("lelang.tahun_caps") +"</th><th>"+Messages.get("lelang.sumber_dana")+"</th><th>"+Messages.get("lelang.nilai")+"</th></tr></thead><tbody>");
        for (AnggaranDp sumber_anggaran : anggaranlist) {
            anggaran.append("<tr><td>").append(sumber_anggaran.ang_tahun).append("</td>").append("<td>").append(sumber_anggaran.sbd_id.name()).append("</td>").append("<td>").append(sumber_anggaran.getNilai()).append("</td></tr>");
        }
        anggaran.append("</tbody></table>");
        return anggaran.toString();
    }

    
    public String getLokasiPaket() {
        List<PaketLokasiDp> listLokasi = PaketLokasiDp.find("pkt_id=?", pkt_id).fetch();
        StringBuilder lokasipaket = new StringBuilder();
        if(CommonUtil.isEmpty(listLokasi))
            return lokasipaket.toString();        
        boolean first = true;
        for (PaketLokasiDp lokasi : listLokasi) {
            if (!CommonUtil.isEmpty(lokasi.pkt_lokasi)) {
            	if(!first)
            		lokasipaket.append(',');
            	lokasipaket.append(HTML.htmlEscape(lokasi.pkt_lokasi + " - " + lokasi.getKabupaten().kbp_nama));
            	first = false;
            }           
        }
        return lokasipaket.toString();
    }

	public String getTahapInfo() {
		return TahapDp.tahapInfo(tahaps, true);
	}

	public TahapDpNow getTahapNow() {
		return new TahapDpNow(lls_id);
	}
	

    public static LelangDpDetil findById(Long lelangId) {
        String sql = String.join(" ", "SELECT  l.lls_id ,l.lls_penetapan_pemenang, l.lls_versi_lelang, l.lls_status, p.pkt_id, p.rup_id, p.pkt_flag, p.pkt_nama,",
				"p.pkt_pagu, p.pkt_hps, ag.agc_nama, s.stk_nama, p.kgr_id , p.kls_id, mtd_id, lls_keterangan, lls_ditutup_karena, ",
				"lls_kontrak_pembayaran, lls_dibuat_tanggal, l.lls_penawaran_ulang,",
                "(select count(lls_id) from devpartner.peserta_dp where lls_id=l.lls_id) as psr_total, l.lls_bobot_teknis, l.lls_bobot_biaya, i.nama, devpartner.tahap_now(l.lls_id, ?) AS tahaps, lls_diulang_karena, ",
                "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja sk, devpartner.paket_satker_dp ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id)) AS nama_instansi,",
                "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja sk, devpartner.paket_satker_dp ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id) AS nama_satker",
                " FROM devpartner.lelang_dp l LEFT JOIN devpartner.paket_dp p ON l.pkt_id=p.pkt_id LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id ",
                "LEFT JOIN agency ag ON s.agc_id=ag.agc_id LEFT JOIN instansi i ON s.instansi_id=i.id WHERE l.lls_id=?");
        return Query.find(sql, LelangDpDetil.class,  BasicCtr.newDate(), lelangId).first();
    }

    @Override
    public boolean isDitutup() {
        return lls_status.isDitutup();
    }

	@Override
	public boolean isUbahLelang() {
		return false;
	}
}
