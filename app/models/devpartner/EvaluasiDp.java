package models.devpartner;

import models.devpartner.common.MetodeDp;
import models.jcommon.db.base.BaseModel;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Table(name = "evaluasi_dp", schema = "devpartner")
public class EvaluasiDp extends BaseModel {

    @Enumerated(EnumType.ORDINAL)
    public enum StatusEvaluasi {
        SEDANG_EVALUASI, SELESAI, DITOLAK;

        public boolean isSelesai() {
            return this == SELESAI;
        }

        public boolean isSedangEvaluasi() {
            return this == SEDANG_EVALUASI;
        }
    }

    @Enumerated(EnumType.ORDINAL)
    public enum JenisEvaluasi {
        SHORTLIST(0, "Shortlist"),
        EVALUASI_TEKNIS(1, "Evaluasi Teknis"),
        EVALUASI_HARGA(2, "Evaluasi Harga"),
        EVALUASI_AKHIR(3, "Evaluasi Akhir");

        public final Integer id;
        public final String label;

        JenisEvaluasi(int id, String nama) {
            this.id = Integer.valueOf(id);
            this.label = nama;
        }

        public boolean isShortlist() {
            return this == SHORTLIST;
        }

        public boolean isTeknis() {
            return this == EVALUASI_TEKNIS;
        }


        public boolean isHarga() {
            return this == EVALUASI_HARGA;
        }

        public boolean isAkhir() {
            return this == EVALUASI_AKHIR;
        }


        public static JenisEvaluasi findById(Integer value) {
            if (value == null)
                return null;
            for (JenisEvaluasi jenis: JenisEvaluasi.values()) {
                if (jenis.id.equals(value)) return jenis;
            }
            return null;
        }

        public static final List<JenisEvaluasi> URUTAN_PASCA = Arrays.asList(EVALUASI_TEKNIS, EVALUASI_HARGA, EVALUASI_AKHIR);
        public static final List<JenisEvaluasi> URUTAN_PRA = Arrays.asList(SHORTLIST, EVALUASI_TEKNIS, EVALUASI_HARGA, EVALUASI_AKHIR);
        public static final List<JenisEvaluasi> URUTAN_PANEL = Arrays.asList(EVALUASI_AKHIR);
    }

    @Id(sequence = "devpartner.seq_evaluasi_dp", function = "nextsequence")
    public Long eva_id;

    @Required
    public JenisEvaluasi eva_jenis;

    public Integer eva_versi;

    public Date eva_tgl_minta;

    public Date eva_tgl_setuju;

    @Required
    public StatusEvaluasi eva_status;

    public Long eva_wf_id;

    //relasi ke Lelang_seleksi
    public Long lls_id;

    @Transient
    private LelangDp lelang_seleksi;

    public LelangDp getLelang_seleksi() {
        if (lelang_seleksi == null)
            lelang_seleksi = LelangDp.findById(lls_id);
        return lelang_seleksi;
    }

    /**
     * mendapatkan objek evaluasi dari suatu lelang
     * jika tidak ada maka akan dibuatkan
     *
     * @param lelangId
     * @param jenis
     * @return
     */
    protected static EvaluasiDp findNCreate(Long lelangId, JenisEvaluasi jenis) {
        EvaluasiDp evaluasi = findBy(lelangId, jenis);
        if (evaluasi == null) {
            evaluasi = new EvaluasiDp();
            evaluasi.lls_id = lelangId;
            evaluasi.eva_jenis = jenis;
            evaluasi.eva_status = StatusEvaluasi.SEDANG_EVALUASI;
            Integer versi = Query.find("SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=? AND eva_jenis=?", Integer.class, lelangId, jenis).first();
            if (versi == null || versi == 0)
                versi = 1;
            evaluasi.eva_versi = versi;
            evaluasi.eva_tgl_setuju = controllers.BasicCtr.newDate();
            evaluasi.save();
        }
        return evaluasi;
    }


    /**
     * dapatkan informasi versi evaluasi terakhir
     *
     * @param lelangId
     * @return
     */
    public static Integer findCurrentVersi(Long lelangId) {
        Integer currentVersi = Query.find("SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id=?", Integer.class, lelangId).first();
        if (currentVersi == null || currentVersi == 0)
            currentVersi = 1;
        return currentVersi;
    }

    /**
     * dapatkan evaluasi tertentu pada versi tertentu dari sebuah lelang
     *
     * @param lelangId
     * @param jenis
     * @param versi
     * @return
     */
    public static EvaluasiDp findBy(Long lelangId, JenisEvaluasi jenis, Integer versi) {
        return find("lls_id=? and eva_versi=? and eva_jenis=?", lelangId, versi, jenis).first();
    }

    /**
     * dapatkan evaluasi saat ini dari sebuah lelang
     *
     * @param lelangId
     * @param jenis
     * @return
     */
    public static EvaluasiDp findBy(Long lelangId, JenisEvaluasi jenis) {
        return Query.find("SELECT * FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1", EvaluasiDp.class, lelangId, jenis).first();
    }

    /**
     * dapatkan evaluasi terakhir
     *
     * @param lelangId
     * @param jenis
     * @return
     */
    public static EvaluasiDp findLastBy(Long lelangId, JenisEvaluasi jenis, StatusEvaluasi statusEvaluasi) {
        return Query.find("SELECT * FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = ? AND eva_status=? ORDER BY eva_versi DESC limit 1", EvaluasiDp.class, lelangId, jenis, statusEvaluasi).first();
    }

    /**
     * dapatkan evaluasi administrasi terakhir tertentu dari sebuah lelang
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findShortlist(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.SHORTLIST);
    }

    /**
     * dapatkan evaluasi Teknis terakhir tertentu dari sebuah lelang
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findTeknis(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_TEKNIS);
    }

    /**
     * dapatkan evaluasi Harga terakhir tertentu dari sebuah lelang
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findHarga(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_HARGA);
    }

    /**
     * dapatkan evaluasi Akhir (Penetapan Pemenang) terakhir tertentu dari sebuah lelang
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findAkhir(Long lelangId) {
        return findBy(lelangId, JenisEvaluasi.EVALUASI_AKHIR);
    }

    /**
     * dapatkan evaluasi Administrasi , jika tidak ada maka akan dibuat baru
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findNCreateShortlist(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.SHORTLIST);
    }

    /**
     * dapatkan evaluasi Teknis , jika tidak ada maka akan dibuat baru
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findNCreateTeknis(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_TEKNIS);
    }

    /**
     * dapatkan evaluasi Harga , jika tidak ada maka akan dibuat baru
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findNCreateHarga(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_HARGA);
    }

    /**
     * dapatkan evaluasi akhir , jika tidak ada maka akan dibuat baru
     *
     * @param lelangId
     * @return
     */
    public static EvaluasiDp findNCreateAkhir(Long lelangId) {
        return findNCreate(lelangId, JenisEvaluasi.EVALUASI_AKHIR);
    }

    /**
     * dapatkan list evaluasi tertentu dari sebuah lelang
     *
     * @param lelangId
     * @param jenis
     * @return
     */
    public static List<EvaluasiDp> findListBy(Long lelangId, JenisEvaluasi jenis) {
        return find("lls_id=? and eva_jenis=? order by eva_versi asc", lelangId, jenis).fetch();
    }

    public EvaluasiDp findNextEvaluasi(MetodeDp metode) {
        List<JenisEvaluasi> urutanEvaluasi = null;
        if (metode.isEmpanelment()) { // pasca 2 file konsultan perorangan
            urutanEvaluasi = JenisEvaluasi.URUTAN_PANEL;
        } else if(metode.kualifikasi.isPreQualification()) { // evaluasi proses pasca kualifikasi
            urutanEvaluasi = JenisEvaluasi.URUTAN_PRA;
        } else { // evaluasi proses prakualifikasi
            urutanEvaluasi = JenisEvaluasi.URUTAN_PASCA;
        }

        for (int i = 0; i < urutanEvaluasi.size() - 1; i++) { // tidak perlu sampai ujung loopnya
            JenisEvaluasi jenis = urutanEvaluasi.get(i);
            if (jenis.equals(eva_jenis)) {
                return EvaluasiDp.findNCreate(lls_id, urutanEvaluasi.get(i+1));
            }
        }

        return null;
    }

    public static void deleteNextEvaluasi(EvaluasiDp evaluasi, MetodeDp metode, Long psr_id) {
        EvaluasiDp evaluasi_next = null;
        List<JenisEvaluasi> urutanEvaluasi = null;
        if (metode.isEmpanelment()) { // pasca 2 file konsultan perorangan
            urutanEvaluasi = JenisEvaluasi.URUTAN_PANEL;
        } else if(metode.kualifikasi.isPreQualification()) { // evaluasi proses pasca kualifikasi
            urutanEvaluasi = JenisEvaluasi.URUTAN_PRA;
        } else { // evaluasi proses prakualifikasi
            urutanEvaluasi = JenisEvaluasi.URUTAN_PASCA;
        }
        for (int i = 0; i < urutanEvaluasi.size() - 1; i++) { // tidak perlu sampai ujung loopnya
            JenisEvaluasi jenis = urutanEvaluasi.get(i);
            if (jenis.equals(evaluasi.eva_jenis)) {
                evaluasi_next = EvaluasiDp.findBy(evaluasi.lls_id, urutanEvaluasi.get(i+1));
            }
        }

        if(evaluasi_next != null) {
            deleteNextEvaluasi(evaluasi_next, metode, psr_id); // recursive
        }
        NilaiEvaluasiDp.delete("eva_id=? and psr_id=?", evaluasi.eva_id, psr_id);
    }

    /**
     * simpan nilai evaluasi peserta
     * @param peserta
     * @param nilai
     * @param metode
     */
    public void simpanNilaiPeserta(PesertaDp peserta, NilaiEvaluasiDp nilai, MetodeDp metode) {
        nilai.psr_id = peserta.psr_id;
        nilai.eva_id = eva_id;
        if (nilai.nev_lulus != null && nilai.nev_lulus.isLulus()) {
            nilai.nev_uraian = null;
        }
        nilai.save();
        EvaluasiDp evaluasi_next = findNextEvaluasi(metode);
        if(nilai.nev_lulus.isLulus() && evaluasi_next != null) { // peserta diijinkan melanjutkan ke proses evaluasi selanjutnnya
            NilaiEvaluasiDp nilai_next = NilaiEvaluasiDp.findNCreateBy(evaluasi_next.eva_id, peserta, evaluasi_next.eva_jenis);
            if(evaluasi_next.eva_jenis.isAkhir()) {
                EvaluasiDp evaluasi_harga = EvaluasiDp.findHarga(peserta.lls_id);
                NilaiEvaluasiDp nilaiHarga = NilaiEvaluasiDp.findBy(evaluasi_harga.eva_id, peserta.psr_id);
                nilai_next.nev_harga_terkoreksi = nilaiHarga.nev_harga_terkoreksi;
            }
            nilai_next.save();
        } else if (evaluasi_next != null) {
            //tidak lulus maka evaluasi harus dihapus
            deleteNextEvaluasi(evaluasi_next, metode, peserta.psr_id);
        }
    }

    public List<NilaiEvaluasiDp> findPesertaLulus() {
        return NilaiEvaluasiDp.find("eva_id=? AND nev_lulus=1", eva_id).fetch();
    }

    public List<NilaiEvaluasiDp> findPeserta() {
        return NilaiEvaluasiDp.find("eva_id=?", eva_id).fetch();
    }

    public List<NilaiEvaluasiDp> findPesertaWithUrutSkor() {
        return NilaiEvaluasiDp.findWithUrutSkor(eva_id);
    }

    public NilaiEvaluasiDp findSkorTertinggi() {
        return NilaiEvaluasiDp.findSkorTertinggi(eva_id);
    }

    public NilaiEvaluasiDp findRankTertinggi() {
        return NilaiEvaluasiDp.findRankTertinggi(eva_id);
    }

    public boolean isExistsPesertaRank() {
        return NilaiEvaluasiDp.count("eva_id=? AND nev_rank = 1", eva_id) > 0;
    }

    public List<NilaiEvaluasiDp> findPesertaWithUrutRank() {
        return NilaiEvaluasiDp.findWithUrutRank(eva_id);
    }
}
