package models.devpartner;

import models.agency.Ppk;
import models.devpartner.contracts.AnggaranDpContract;
import models.devpartner.contracts.PaketAnggaranDpContract;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by idoej
 */
@Table(name="paket_anggaran_dp", schema = "devpartner")
public class PaketAnggaranDp extends BaseModel implements PaketAnggaranDpContract {
	//relasi ke model Paket
	@Id
	public Long pkt_id;

	//relasi ke model Anggaran
	@Id
	public Long ang_id;

	//relasi ke table PPK
	public Long ppk_id;

	public Integer ppk_jabatan;

	//relasi ke model Rup_paket
	public Long rup_id;

	// versi paket anggaran. disamakan dengan versi lelang
	public Integer pkt_ang_versi;

	@Transient
	public AnggaranDp anggaran;
	@Transient
	public Ppk ppk;

    // list ppk_id dari paket
	public static String findPPKByPaket(Long pkt_id) {
		return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct(ppk_id)), ', ') FROM paket_anggaran WHERE pkt_id=?", String.class, pkt_id).first();
	}


	public static List<PaketAnggaranDp> findByPaket(Long paketId) {
		return find("pkt_id=?", paketId).fetch();
	}

	public static List<PaketAnggaranDp> findByPaketAndVersion(Long paketId, Integer versi) {
		return find("pkt_id=? and pkt_ang_versi = ?", paketId, versi).fetch();
	}

	public String getNamaPegawai() {
		return Query.find("SELECT peg_nama FROM pegawai p, ppk WHERE p.peg_id=ppk.peg_id AND ppk_id=?",String.class, ppk_id).first();
	}

	public static boolean isPpkInThisPaket(Long pktId, Long userId) {
		return count("pkt_id=? AND ppk_id=?", pktId, userId) > 0;
	}

	public static void updatePpkAllAnggaranByPaket(Long pkt_id, Long ppk_id){
		Query.update("UPDATE paket_anggaran SET ppk_id = ? where pkt_id = ?",ppk_id,pkt_id);
	}

	@Override
	public Long getAngId() {
		return this.ang_id;
	}

	@Override
	public Long getPpkId() {
		return this.ppk_id;
	}

	@Override
	public Long getPktId() {
		return this.pkt_id;
	}

	@Override
	public AnggaranDpContract getAnggaranContract() {
		return getAnggaran();
	}

	public AnggaranDp getAnggaran() {
		if (this.anggaran == null) {
			this.anggaran = AnggaranDp.findByAnggaranId(getAngId());
		}
		return this.anggaran;
	}

	@Override
	public void setAnggaran(AnggaranDp anggaran) {
		this.anggaran = anggaran;
	}

	@Override
	public Ppk getPpk() {
		if (this.ppk == null) {
			this.ppk = Ppk.findById(getPpkId());
		}
		return this.ppk;
	}

	@Override
	public boolean isPpkExist() {
		return getPpk() != null;
	}

	@Override
	public String getPpkNama() {
		return isPpkExist() ? getPpk().getNamaPegawai() : "";
	}

}
