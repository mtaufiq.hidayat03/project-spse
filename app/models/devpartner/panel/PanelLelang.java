package models.devpartner.panel;

import models.devpartner.LelangDp;
import models.devpartner.common.MetodeDp;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;

@Table(name = "DEVPARTNER.PANEL_LELANG")
public class PanelLelang extends BaseModel {

    @Id
    public Long pnl_id;

    @Id
    public Long lls_id;

    public static PanelLelang createOrUpdate(Long pnl_id, Long lls_id) {
        PanelLelang panelLelang = PanelLelang.find("lls_id = ?", lls_id).first();
        if (panelLelang == null) {
            panelLelang = new PanelLelang();
            panelLelang.lls_id = lls_id;
        }
        panelLelang.pnl_id = pnl_id;
        panelLelang.save();

        return panelLelang;
    }

    public static PanelLelang findBy(LelangDp lelang) {
        return PanelLelang.find("lls_id = ?", lelang.lls_id).first();
    }

    public static List<PanelLelang> findAllBy(Panel panel, MetodeDp metode) {
        return PanelLelang.find("pnl_id = ? and lls_id in (select lls_id from devpartner.lelang_dp where mtd_id = ?)", panel.pnl_id, metode.id).fetch();
    }

    public Panel getPanel() {
        return Panel.findById(pnl_id);
    }

    public LelangDp getLelang() {
        return LelangDp.findById(lls_id);
    }
}
