package models.devpartner.panel;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.data.validation.MaxSize;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Table(name = "DEVPARTNER.PANEL")
public class Panel extends BaseModel {

    @Id(sequence = "devpartner.seq_panel", function = "nextsequence")
    public Long pnl_id;
    @Required
    @MaxSize(100)
    @MinSize(4)
    public String nama_panel;

    @Required
    public String keterangan;

    @Required
    public Long pnl_size;

    public Long member_details_id;
    public Long contract_award_id;
    public Long pnl_owner_id;

    public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[5];
            tmp[0] = rs.getString("pnl_id");
            tmp[1] = rs.getString("nama_panel");
            tmp[2] = rs.getString("keterangan");
            tmp[3] = String.valueOf(rs.getLong("jumlah_vendor"));
            tmp[4] = String.valueOf(rs.getLong("pnl_size"));
            return tmp;
        }
    };

    public boolean isRightfulOwner(Long userId) {
        return pnl_owner_id.equals(userId);
    }

    @Transient
    public Long getTotalVendor() {
        return PanelRekanan.count("pnl_id = ?", pnl_id);
    }

    @Transient
    public BlobTable getMemberDetailsBlob() {
        if(member_details_id == null)
            return null;
        return BlobTableDao.getLastById(member_details_id);
    }

    @Transient
    public BlobTable getContractAwardBlob() {
        if(contract_award_id == null)
            return null;
        return BlobTableDao.getLastById(contract_award_id);
    }

    public void simpan(Panel submittedData, File memberDetails, Long hapus, File contractAward, Long hapusAward) throws Exception {
        if (member_details_id != null && hapus == null) {
            Long id = member_details_id;
            member_details_id = null;
            BlobTable.delete("blb_id_content=?", id);
        }

        if (memberDetails != null) {
            BlobTable bt = null;
            if (member_details_id == null) {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, memberDetails);
            } else {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, memberDetails, member_details_id);
            }
            member_details_id = bt.blb_id_content;
        }

        if (contract_award_id != null && hapusAward == null) {
            Long id = contract_award_id;
            contract_award_id = null;
            BlobTable.delete("blb_id_content=?", id);
        }

        if (contractAward != null) {
            BlobTable bt = null;
            if (contract_award_id == null) {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, contractAward);
            } else {
                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, contractAward, contract_award_id);
            }
            contract_award_id = bt.blb_id_content;
        }



        nama_panel = submittedData.nama_panel;
        keterangan = submittedData.keterangan;
        pnl_size = submittedData.pnl_size;
        save();
    }

    public static void hapus(List<Long> ids) {
        String join = StringUtils.join(ids, ",");
        List<Long> blobIds = new ArrayList<>();
        List<Panel> panels = Panel.find("pnl_id in ("+join+ ')').fetch();
        for(Panel panel: panels) {
            if (panel.member_details_id != null) {
                blobIds.add(panel.member_details_id);
            }
            if (panel.contract_award_id != null) {
                blobIds.add(panel.contract_award_id);
            }
        }
        String blobs = StringUtils.join(blobIds, ",");
        BlobTable.delete("blb_id_content in ("+blobs+ ')');
        Panel.delete("pnl_id in ("+join+ ')');
    }
}
