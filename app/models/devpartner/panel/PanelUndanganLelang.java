package models.devpartner.panel;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.util.List;

/**
 * Domain model class untuk mapping ke table undangan_paket
 * Sebagai container yang menyimpan informasi rekanan mana saja yang diundang dalam sebuah lelang "Lelang Panel"
 * Satu rekanan bisa terdafar di banyak lelang
 *
 * @author idoej
 *
 */
@Table(name="panel_undangan_lelang", schema = "devpartner")
public class PanelUndanganLelang extends BaseTable {
	@Id
	public Long lls_id;
	@Id
	public Long rkn_id;

	public static boolean isInvited(Long lls_id, Long rkn_id) {
		return count("lls_id = ? and rkn_id = ?", lls_id, rkn_id) > 0;
	}

	public static PanelUndanganLelang findBy(Long lls_id, Long rekananId) {
		return find("lls_id = ? and rkn_id = ?", lls_id, rekananId).first();
	}

	/**
	 * TODO handle joint venture dan kirim email
	 * @param lls_id
	 * @param ids
	 */
	public static void tambah(Long lls_id, List<Long> ids) {
		for (Long id : ids) {
			boolean isRekananPanelSaved = true;
			PanelUndanganLelang panelUndanganLelang = new PanelUndanganLelang();
			panelUndanganLelang.lls_id = lls_id;
			panelUndanganLelang.rkn_id = id;
			PanelUndanganLelang panelRekananDuplicate = findBy(lls_id, id);
			if (panelRekananDuplicate == null) {
				panelUndanganLelang.save();
			} else {
				isRekananPanelSaved = false;
			}

			if (isRekananPanelSaved) {
				// di sini kirim email beserta info JV
			}
		}
	}
}
