package models.devpartner.panel;

import models.rekanan.Rekanan;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Domain model class untuk mapping ke table panel_rekanan
 * Sebagai container yang menyimpan informasi rekanan mana saja yang ada di sebuah panel
 * Satu rekanan bisa terdafar di banyak panel
 * 
 * @author idoej
 *
 */
@Table(name="DEVPARTNER.PANEL_REKANAN")
public class PanelRekanan extends BaseTable {
	@Id
	public Long pnl_id;
	@Id
	public Long rkn_id;

	@Transient
	private Rekanan rekanan;

	@Transient
	private Panel panel;

	public Rekanan getRekanan() {
		if (rekanan == null) {
			rekanan = Rekanan.findById(rkn_id);
		}
		return rekanan;
	}

	public Panel getPanel() {
		if (panel == null) {
			panel = Panel.findById(pnl_id);
		}
		return panel;
	}

	public static List<PanelRekanan> findByPanel(Long panelId) {
		return find("pnl_id = ?", panelId).fetch();
	}

	public static PanelRekanan findBy(Long panelId, Long rekananId) {
		return find("pnl_Id = ? and rkn_id = ?", panelId, rekananId).first();
	}

	public static void hapus(Long pnl_id, List<Long> ids) {
		String join = StringUtils.join(ids, ",");
		PanelRekanan.delete("pnl_id= ? and rkn_id in ("+join+ ')', pnl_id);
	}

	/**
	 * TODO handle joint venture dan kirim email
	 * @param pnl_id
	 * @param ids
	 */
	public static void tambah(Long pnl_id, List<Long> ids) {
		for (Long id : ids) {
			boolean isRekananPanelSaved = true;
			PanelRekanan panelRekanan = new PanelRekanan();
			panelRekanan.pnl_id = pnl_id;
			panelRekanan.rkn_id = id;
			PanelRekanan panelRekananDuplicate = findBy(pnl_id, id);
			if (panelRekananDuplicate == null) {
				panelRekanan.save();
			} else {
				isRekananPanelSaved = false;
			}

			if (isRekananPanelSaved) {
				// di sini kirim email beserta info JV
			}
		}
	}
}
