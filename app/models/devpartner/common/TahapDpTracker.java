package models.devpartner.common;

import models.common.Active_user;
import models.devpartner.LelangDp;

import java.util.List;
import java.util.Optional;

public class TahapDpTracker {

	public Long lelangId;
	public LelangDp lelang;
	public Active_user active_user;
	public List<TahapDp> tahapList;


	public boolean isPengumumanLelang() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPengumumanLelang());
	}

	public boolean isUploadEoi() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isUploadEoi());
	}

	public boolean isPenawaran() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPenawaran());
	}

	public boolean isPembukaan() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPembukaan());
	}

	public boolean isPembukaanHarga() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPembukaanHarga());
	}

	public boolean isPenjelasan() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPenjelasan());
	}

	public boolean isPenetapanShortlist() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPenetapanShortlist());
	}

	public boolean isPengumumanShortlist() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPengumumanShortlist());
	}

	public boolean isEvaluasi() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isEvaluasi());
	}

	public boolean isEvaluasiTeknis() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isEvaluasiTeknis());
	}

	public boolean isEvaluasiHarga() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isEvaluasiHarga());
	}

	public boolean isEvaluasiAkhir() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isEvaluasiAkhir());
	}

	public boolean isPengumumanTeknis() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPengumumanTeknis());
	}

	public boolean isPengumumanPemenangAkhir() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPengumumanPemenangAkhir());
	}

	public boolean isTampilkanPenawaran() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isTampilkanPenawaran());
	}

	public boolean isPenetapanPemenang() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isPenetapanPemenang());
	}

	public boolean isFinalRanking() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isFinalRanking());
	}

	public boolean isContractNegotiation() {
		return tahapList != null && tahapList.stream().anyMatch(tahapDp -> tahapDp.isContractNegotiation());
	}

	public TahapDp getTahapPenjelasan() {
		if (tahapList != null){
			Optional<TahapDp> option = tahapList.stream().filter(tahapDp -> tahapDp.isPenjelasan()).findFirst();
			return option.isPresent() ? option.get() : null;
		}

		return null;
	}

	public TahapDp getTahapUploadEoi() {
		if (tahapList != null){
			Optional<TahapDp> option = tahapList.stream().filter(tahapDp -> tahapDp.isUploadEoi()).findFirst();
			return option.isPresent() ? option.get() : null;
		}

		return null;
	}

	public TahapDp getTahapPengumumanShortlist() {
		if (tahapList != null){
			Optional<TahapDp> option = tahapList.stream().filter(tahapDp -> tahapDp.isPengumumanShortlist()).findFirst();
			return option.isPresent() ? option.get() : null;
		}

		return null;
	}

	public TahapDp getTahapPenawaran() {
		if (tahapList != null){
			Optional<TahapDp> option = tahapList.stream().filter(tahapDp -> tahapDp.isPenawaran()).findFirst();
			return option.isPresent() ? option.get() : null;
		}

		return null;
	}

	public TahapDp getTahapPembukaanHarga() {
		if (tahapList != null){
			Optional<TahapDp> option = tahapList.stream().filter(tahapDp -> tahapDp.isPembukaanHarga()).findFirst();
			return option.isPresent() ? option.get() : null;
		}

		return null;
	}
}
