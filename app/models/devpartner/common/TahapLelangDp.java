package models.devpartner.common;

import models.common.Active_user;
import models.devpartner.LelangDp;

/**
 * mendapatkan Informasi Tahap lelang
 */
public class TahapLelangDp extends TahapDpTracker {

	public TahapLelangDp(Long lelangId) {
		this.lelangId = lelangId;
		this.active_user = Active_user.current();
		tahapList = AktivitasDp.findTahapLelang(lelangId);
	}

	public TahapLelangDp(LelangDp lelang) {
		this.lelangId = lelang.lls_id;
		this.active_user = Active_user.current();
		tahapList = AktivitasDp.findTahapLelang(lelangId);
	}

}
