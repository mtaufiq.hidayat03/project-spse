package models.devpartner.common;

/**
 * Jenis email yang mungkin dikirim oleh sistem.
 * 
 * @author idoej
 *
 */
public enum JenisEmailDp {

	PERUBAHAN_JADWAL(101, "Perubahan Jadwal"),
	PENGUMUMAN_SHORTLIST(102, "Pengumuman Shortlist"),
	PERUBAHAN_JV_SHORTLIST(103, "Perubahan Joint Venture Shortlist"),
	PERUBAHAN_JV_BID(103, "Perubahan Joint Venture Penawaran"),
	PEMBUKAAN_PENAWARAN(104, "Pembukaan Penawaran"),
	PEMBUKAAN_HARGA(105, "Pembukaan Penawaran Harga"),
	UNDANGAN_PEMBUKAAN_HARGA(106, "Undangan Pembukaan Penawaran Harga"),
	UNDANGAN_NEGOSIASI(107, "Undangan Negosiasi Kontrak"),
	PENGUMUMAN_PEMENANG(108, "Pengumuman Pemenang"),
	PENGUMUMAN_PEMENANG_EMPANELMENT(109, "Pengumuman Pemenang Empanelment"),
	PENGUMUMAN_TEKNIS(110, "Pengumuman Teknis"),
	PEMBATALAN_LELANG(111, "Pembatalan Tender");

	public static JenisEmailDp getById(long id) {
		for (JenisEmailDp es : JenisEmailDp.values()) {
			if (es.id == id) {
				return es;
			}
		}
		throw new IllegalArgumentException();
	}

	public final int id;
	public final String label;
	JenisEmailDp(int id, String label) {
		this.id=id;
		this.label = label;
	}


}
