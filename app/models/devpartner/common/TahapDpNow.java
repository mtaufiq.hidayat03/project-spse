package models.devpartner.common;

import models.common.Active_user;
import models.common.StatusLelang;
import models.devpartner.LelangDp;
import models.devpartner.PersetujuanDp;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * mendapatkan Informasi Tahap lelang
 * ijin akses seluruh dokumen dan flow lelang sedang berlangsung harus diterapkan di sini
 */
public class TahapDpNow extends TahapDpTracker {

	StatusLelang status;
	boolean sedangPersetujuanPemenang;

	public TahapDpNow(Long lelangId) {
		this.lelangId = lelangId;
		this.active_user = Active_user.current();
		Query.find("SELECT lls_status, mtd_id, lls_metode_penawaran FROM devpartner.lelang_dp WHERE lls_id=?", new ResultSetHandler<Object>() {
			@Override
			public Object handle(ResultSet rs) throws SQLException {
				status = StatusLelang.fromValue(rs.getInt("lls_status"));
				MetodeDp pemilihan = MetodeDp.findById(rs.getInt("mtd_id"));
				return null;
			}
		}, lelangId).first();
		tahapList = AktivitasDp.findTahapNow(lelangId);
		sedangPersetujuanPemenang = PersetujuanDp.isSedangPersetujuanPemenang(lelangId);
	}

	public TahapDpNow(LelangDp lelang) {
		this.lelangId = lelang.lls_id;
		this.active_user = Active_user.current();
		this.status = lelang.lls_status;
		tahapList = AktivitasDp.findTahapNow(lelangId);
		sedangPersetujuanPemenang = lelang.isSedangPersetujuanPemenang();
	}

	public boolean isSedangPersetujuanPemenang() {
		return sedangPersetujuanPemenang;
	}
}
