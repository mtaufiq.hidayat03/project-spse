package models.devpartner.common;

import ext.FormatUtils;
import models.agency.Panitia;
import models.agency.Pegawai;
import models.common.ConfigurationDao;
import models.devpartner.*;
import models.devpartner.panel.PanelLelang;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.rekanan.Rekanan;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementasi Dao untuk model MailQueue
 * 
 * @author arief 
 *
 */
public class MailDpDao {
	private static final Template PERUBAHAN_JADWAL_TENDER = TemplateLoader.load("/email/perubahan-jadwal-lelang.html");
	private static final Template PENGUMUMAN_SHORTLIST_LULUS = TemplateLoader.load("/email/devpartner/lulus-shortlist.html");
	private static final Template PENGUMUMAN_SHORTLIST_TDK_LULUS = TemplateLoader.load("/email/devpartner/gagal-shortlist.html");
	private static final Template PERUBAHAN_JV_SHORTLIST = TemplateLoader.load("/email/devpartner/perubahan-jv-pq.html");
	private static final Template PERUBAHAN_JV_BID = TemplateLoader.load("/email/devpartner/perubahan-jv-bid.html");
	private static final Template PENGUMUMAN_TEKNIS_LULUS = TemplateLoader.load("/email/devpartner/pengumuman-teknis-lulus.html");
	private static final Template PENGUMUMAN_TEKNIS_GAGAL = TemplateLoader.load("/email/devpartner/pengumuman-teknis-gagal.html");
	private static final Template PEMBUKAAN_PENAWARAN = TemplateLoader.load("/email/devpartner/pembukaan-penawaran.html");
	private static final Template PEMBUKAAN_HARGA = TemplateLoader.load("/email/devpartner/pembukaan-harga.html");
	private static final Template UNDANGAN_PEMBUKAAN_HARGA = TemplateLoader.load("/email/devpartner/undangan-pembukaan-harga.html");
	private static final Template UNDANGAN_NEGOSIASI = TemplateLoader.load("/email/devpartner/undangan-negosiasi.html");
	private static final Template PENGUMUMAN_PEMENANG_EMPANELMENT = TemplateLoader.load("/email/devpartner/pengumuman-pemenang-empanelment.html");
	private static final Template PENGUMUMAN_PEMENANG = TemplateLoader.load("/email/devpartner/pengumuman-pemenang.html");
	private static final Template PENGUMUMAN_TUTUP_LELANG_TEMPLATE = TemplateLoader.load("/email/pengumuman-tutup-lelang.html");

	public static void kirimPerubahanJadwalLelang(LelangDp lelang, List<JadwalDp> jadwalOriginal, List<JadwalDp> jadwalList, Date waktuPerubahan) throws Exception{

		StringBuilder jadwalEmailx = new StringBuilder();
		Panitia panitia = lelang.getPanitia();

		jadwalEmailx.append("<table width=\"100%\" class=\"list-content\">" +
				"<tr>" +
				"<td><strong>Tahap/Stage</strong></td>" +
				"<td><strong>Sebelum/Before</strong></td>" +
				"<td><strong>Sesudah/After</strong></td>" +
				"</tr>");
		JadwalDp jadwalO = null;
		JadwalDp jadwal = null;
		String keterangan = "";
		for (int i = 0; i < jadwalOriginal.size(); i++) {
			jadwalO = jadwalOriginal.get(i);
			jadwal = jadwalList.get(i);
			DateTime tglawalO = new DateTime(jadwalO.dtj_tglawal);
			DateTime tglawal = new DateTime(jadwal.dtj_tglawal);
			DateTime tglakhirO = new DateTime(jadwalO.dtj_tglakhir);
			DateTime tglakhir = new DateTime(jadwal.dtj_tglakhir);
			if (!Objects.equals(tglawal, tglawalO) || !Objects.equals(tglakhir, tglakhirO)) {
				jadwalEmailx.append("<tr><td>").append(jadwal.namaTahap()).append("</td><td>");
				jadwalEmailx.append(FormatUtils.formatDateTimeInd(jadwalO.dtj_tglawal)).append(" - ").append(FormatUtils.formatDateTimeInd(jadwalO.dtj_tglakhir));
				jadwalEmailx.append("</td><td>");
				jadwalEmailx.append(FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal)).append(" - ").append(FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir));
				jadwalEmailx.append("</td></tr>");
				if(StringUtils.isEmpty(keterangan)) {
					keterangan = jadwal.dtj_keterangan;
				}
			}
		}
		jadwalEmailx.append("</table>");
		String namaPaket = lelang.getNamaPaket();
		List<PesertaDp> pesertaList = PesertaDp.find("lls_id=?", lelang.lls_id).fetch();
		List<Pegawai> pegawaiList = Pegawai.find("peg_id in (select peg_id from anggota_panitia where pnt_id=?)", panitia.pnt_id).fetch();
		String instansi = PaketSatkerDp.getNamaInstansiPaket(lelang.pkt_id);
		String email = null;
		MailQueue mails[] = new MailQueue[pesertaList.size() + pegawaiList.size()];
		int i =0;
		for (PesertaDp peserta : pesertaList)
		{
			email = Rekanan.findEmail(peserta.rkn_id);
			Map<String, Object> map = new HashMap<>();
			map.put("paket", namaPaket);
			map.put("nama_panitia", lelang.getNamaPanitia());
			map.put("email", email);
			map.put("lls_id", lelang.lls_id);
			map.put("jadwal", jadwalEmailx);
			map.put("waktuPerubahan",FormatUtils.formatDateTimeInd(waktuPerubahan));
			map.put("nama_lpse",ConfigurationDao.getNamaLpse());
			map.put("keterangan", keterangan);
			map.put("instansi", instansi);
			MailQueue mail = EmailManager.createEmail(email, PERUBAHAN_JADWAL_TENDER, map);
			mail.lls_id = lelang.lls_id;
			mail.rkn_id = peserta.rkn_id;
			mail.jenis = Integer.valueOf(7);
			mail.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mail.subject = Messages.getMessage("en","email.perubahan_jadwal") + " | " + Messages.getMessage("id","email.perubahan_jadwal");
			mail.save();
			mails[i] = mail;
			i++;
		}

		for (Pegawai pegawai : pegawaiList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("nama_panitia", panitia.pnt_nama);
			map.put("email", pegawai.peg_email);
			map.put("lls_id", lelang.lls_id);
			map.put("jadwal", jadwalEmailx);
			map.put("waktuPerubahan",FormatUtils.formatDateTimeInd(waktuPerubahan));
			MailQueue mail = EmailManager.createEmail(pegawai.peg_email, PERUBAHAN_JADWAL_TENDER, map);
			mail.lls_id = lelang.lls_id;
			mail.prioritas = MailQueue.MAIL_PRIORITAS.HIGH;
			mail.jenis = Integer.valueOf(7);
			mail.rkn_id = panitia.pnt_id;
			mail.subject = Messages.getMessage("en","email.perubahan_jadwal") + " | " + Messages.getMessage("id","email.perubahan_jadwal");
			mail.save();
			mails[i] = mail;
			i++;
		}
	}

	public static void kirimPengumumanShortlist(LelangDp lelang) throws Exception {
		// kirim email sesuai hasil evaluasi kualifikasi & pembuktian
		EvaluasiDp evaluasi  = EvaluasiDp.findShortlist(lelang.lls_id);
		List<NilaiEvaluasiDp> nilaiEvaluasiList = lelang.getMetode().isCqs() ? Arrays.asList(evaluasi.findSkorTertinggi()) : evaluasi.findPeserta	();
		for (NilaiEvaluasiDp nilaiEvaluasi:nilaiEvaluasiList) {
			Rekanan rekanan = nilaiEvaluasi.getPeserta().getRekanan();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lelang", lelang);
			map.put("rekanan", rekanan);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, nilaiEvaluasi.isLulus() ? PENGUMUMAN_SHORTLIST_LULUS : PENGUMUMAN_SHORTLIST_TDK_LULUS, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.pengumuman_shortlist") + " | " + Messages.getMessage("id","email.pengumuman_shortlist");
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.PENGUMUMAN_SHORTLIST.id;
			mq.save();
		}
	}

	public static void kirimPerubahanJvPq(PesertaDp peserta) throws Exception {
		// kirim email perubahan JV ke rekanan
		Rekanan rekanan = peserta.getRekanan();
		LelangDp lelang = peserta.getLelang_seleksi();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("lelang", lelang);
		map.put("rekanan", rekanan);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PERUBAHAN_JV_SHORTLIST, map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.ref_a = rekanan.rkn_nama;
		mq.subject = Messages.getMessage("en","email.perubahan.jv.pq") + " | " + Messages.getMessage("id","email.perubahan.jv.pq");
		mq.lls_id = lelang.lls_id;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmailDp.PERUBAHAN_JV_SHORTLIST.id;
		mq.save();
	}

	public static void kirimPerubahanJvBid(PesertaDp peserta) throws Exception {
		// kirim email perubahan JV ke rekanan
		Rekanan rekanan = peserta.getRekanan();
		LelangDp lelang = peserta.getLelang_seleksi();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("lelang", lelang);
		map.put("rekanan", rekanan);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PERUBAHAN_JV_BID, map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.ref_a = rekanan.rkn_nama;
		mq.subject = Messages.getMessage("en","email.perubahan.jv.bid") + " | " + Messages.getMessage("id","email.perubahan.jv.bid");
		mq.lls_id = lelang.lls_id;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmailDp.PERUBAHAN_JV_BID.id;
		mq.save();
	}

	public static void kirimPengumumanTeknis(Long lls_id) throws Exception {
		LelangDp lelang = LelangDp.findById(lls_id);
		String paket = lelang.getNamaPaket();
		List<PesertaDp> pesertaLelang = PesertaDp.findWithPenawaran(lelang.lls_id);
		for (PesertaDp peserta : pesertaLelang) {
			Rekanan rekanan = peserta.getRekanan();
			NilaiEvaluasiDp teknis = peserta.getTeknis();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			map.put("nama_panitia", lelang.getPanitia().pnt_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, teknis.isLulus() ? PENGUMUMAN_TEKNIS_LULUS : PENGUMUMAN_TEKNIS_GAGAL, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.pengumuman_teknis") + " | " + Messages.getMessage("id","email.pengumuman_teknis");;
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.PENGUMUMAN_TEKNIS.id;
			mq.save();
		}
	}

	public static void kirimInfoPembukaan(LelangDp lelang) throws Exception {
		List<PesertaDp> pesertaList = null;
		if (lelang.isPreQualification()) {
			EvaluasiDp shortlist = EvaluasiDp.findShortlist(lelang.lls_id);
			List<NilaiEvaluasiDp> nilaiEvaluasiList = lelang.getMetode().isCqs() ? Arrays.asList(shortlist.findSkorTertinggi()) : shortlist.findPesertaLulus();
			if (nilaiEvaluasiList != null && !nilaiEvaluasiList.isEmpty()) {
				pesertaList = nilaiEvaluasiList.stream().map(NilaiEvaluasiDp::getPeserta).collect(Collectors.toList());
			}
		} else {
			pesertaList = PesertaDp.findWithPenawaran(lelang.lls_id);
		}
		if (pesertaList != null && !pesertaList.isEmpty()) {
			for (PesertaDp peserta : pesertaList) {
				Rekanan rekanan = peserta.getRekanan();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("lelang", lelang);
				map.put("rekanan", rekanan);
				MailQueue mq = EmailManager.createEmail(rekanan.rkn_email, PEMBUKAAN_PENAWARAN, map);
				mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
				mq.ref_a = rekanan.rkn_nama;
				mq.subject = Messages.getMessage("en", "email.pembukaan") + " | " + Messages.getMessage("id", "email.pembukaan");
				mq.lls_id = lelang.lls_id;
				mq.rkn_id = rekanan.rkn_id;
				mq.jenis = JenisEmailDp.PEMBUKAAN_PENAWARAN.id;
				mq.save();
			}
		}
	}

	public static void kirimInfoPembukaanHarga(LelangDp lelang) throws Exception {
		EvaluasiDp teknis = EvaluasiDp.findTeknis(lelang.lls_id);
		List<NilaiEvaluasiDp> pesertaList = lelang.getMetode().isQbs() ? Arrays.asList(teknis.findSkorTertinggi()) : teknis.findPesertaLulus();
		for (NilaiEvaluasiDp nilaiEvaluasi:pesertaList) {
			Rekanan rekanan = nilaiEvaluasi.getPeserta().getRekanan();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lelang", lelang);
			map.put("rekanan", rekanan);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PEMBUKAAN_HARGA, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.pembukaan_harga") + " | " + Messages.getMessage("id","email.pembukaan_harga");
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.PEMBUKAAN_HARGA.id;
			mq.save();
		}
	}

	public static void kirimUndanganPembukaanHarga(LelangDp lelang, String waktu, String lokasi, String catatan) throws Exception {
		EvaluasiDp teknis = EvaluasiDp.findTeknis(lelang.lls_id);
		List<NilaiEvaluasiDp> pesertaList = lelang.getMetode().isQbs() ? Arrays.asList(teknis.findSkorTertinggi()) : teknis.findPesertaLulus();
		for (NilaiEvaluasiDp nilaiEvaluasi:pesertaList) {
			Rekanan rekanan = nilaiEvaluasi.getPeserta().getRekanan();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("lelang", lelang);
			map.put("rekanan", rekanan);
			map.put("waktu", waktu);
			map.put("lokasi", lokasi);
			map.put("catatan", catatan);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, UNDANGAN_PEMBUKAAN_HARGA, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.undangan_pembukaan_harga") + " | " + Messages.getMessage("id","email.undangan_pembukaan_harga");
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.UNDANGAN_PEMBUKAAN_HARGA.id;
			mq.save();
		}
	}

	public static void kirimUndanganNegosiasi(LelangDp lelang, String content) throws Exception {
		EvaluasiDp akhir = EvaluasiDp.findAkhir(lelang.lls_id);
		NilaiEvaluasiDp nilaiEvaluasi= akhir.findRankTertinggi();
		Rekanan rekanan = nilaiEvaluasi.getPeserta().getRekanan();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("lelang", lelang);
		map.put("rekanan", rekanan);
		map.put("content", content);
		MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, UNDANGAN_NEGOSIASI, map);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.ref_a = rekanan.rkn_nama;
		mq.subject = Messages.getMessage("en","email.undangan_negosiasi") + " | " + Messages.getMessage("id","email.undangan_negosiasi");
		mq.lls_id=lelang.lls_id;
		mq.rkn_id = rekanan.rkn_id;
		mq.jenis = JenisEmailDp.UNDANGAN_NEGOSIASI.id;
		mq.save();
	}

	public static void kirimPengumumanPemenangEmpanelment(Long lls_id) throws Exception {
		LelangDp lelang = LelangDp.findById(lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
		PanelLelang panelLelang = PanelLelang.findBy(lelang);
		List<NilaiEvaluasiDp> pesertaLelang = evaluasi.findPesertaLulus();
		for (NilaiEvaluasiDp nilaiEvaluasi : pesertaLelang) {
			Rekanan rekanan = nilaiEvaluasi.getPeserta().getRekanan();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("namaPanel", panelLelang.getPanel().nama_panel);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_PEMENANG_EMPANELMENT, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.pengumuman_pemenang_empanelment") + " | " + Messages.getMessage("id","email.pengumuman_pemenang_empanelment");;
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.PENGUMUMAN_PEMENANG_EMPANELMENT.id;
			mq.save();
		}
	}

	public static void kirimPengumumanPemenang(Long lls_id) throws Exception {
		LelangDp lelang = LelangDp.findById(lls_id);
		String paket = lelang.getNamaPaket();
		EvaluasiDp teknis = EvaluasiDp.findTeknis(lelang.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
		String queryPemenang = "SELECT r.rkn_id, rkn_nama, rkn_npwp, rkn_email FROM devpartner.nilai_evaluasi_dp n LEFT JOIN devpartner.peserta_dp p ON n.psr_id=p.psr_id LEFT JOIN rekanan r ON r.rkn_id=p.rkn_id "
				+ "WHERE n.eva_id=?  AND n.nev_lulus = 1";
		Rekanan pemenang = Query.find(queryPemenang, Rekanan.class, evaluasi.eva_id).first();
		List<NilaiEvaluasiDp> pesertaLelang = teknis.findPesertaLulus();
		for (NilaiEvaluasiDp nilaiEvaluasi : pesertaLelang) {
			Rekanan rekanan = nilaiEvaluasi.getPeserta().getRekanan();
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", paket);
			map.put("rkn_nama", rekanan.rkn_nama.toUpperCase());
			map.put("nama_panitia", lelang.getPanitia().pnt_nama);
			map.put("pesertaLelang", pesertaLelang);
			map.put("pemenang", pemenang);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			map.put("nama_lpse", ConfigurationDao.getNamaLpse());
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_PEMENANG, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.pengumuman_pemenang") + " | " + Messages.getMessage("id","email.pengumuman_pemenang");;
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.PENGUMUMAN_PEMENANG.id;
			mq.save();
		}
	}

	/**
	 * mengirim pengumuman penutupan lelang
	 */
	public static void kirimPengumumanTutupLelang(LelangDp lelang, String alasan) throws Exception {
		String namaPaket = lelang.getNamaPaket();
		List<PesertaDp> pesertaList = lelang.getPesertaList();
		if(CollectionUtils.isEmpty(pesertaList))
			return;
		MailQueue[] mails = new MailQueue[pesertaList.size()];
		int i =0;
		for (PesertaDp peserta : pesertaList) {
			Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("paket", namaPaket);
			map.put("rkn_nama", rekanan.rkn_nama);
			map.put("alasan", alasan);
			map.put("nama_panitia", lelang.getPanitia().pnt_nama);
			map.put("email", rekanan.rkn_email);
			map.put("lls_id", lelang.lls_id);
			MailQueue mq=EmailManager.createEmail(rekanan.rkn_email, PENGUMUMAN_TUTUP_LELANG_TEMPLATE, map);
			mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
			mq.ref_a = rekanan.rkn_nama;
			mq.subject = Messages.getMessage("en","email.pengumuman_batal_lelang") + " | " + Messages.getMessage("id","email.pengumuman_batal_lelang");;
			mq.lls_id=lelang.lls_id;
			mq.rkn_id = rekanan.rkn_id;
			mq.jenis = JenisEmailDp.PEMBATALAN_LELANG.id;
			mq.save();
			mails[i] = mq;
			i++;
		}
	}
}
