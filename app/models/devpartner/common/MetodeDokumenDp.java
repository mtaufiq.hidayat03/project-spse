package models.devpartner.common;


import play.i18n.Messages;

public enum MetodeDokumenDp {

		SATU_FILE (0, "mddp.one_envelope"),
		DUA_FILE (1, "mddp.two_envelope");

		public final Integer id;
		public final String label;

		MetodeDokumenDp(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}

	public String getLabel() {
		return Messages.get(label);
	}
		
		public static MetodeDokumenDp fromValue(Integer value){
			MetodeDokumenDp dokkumen = null;
			if(value != null){
				switch(value.intValue()){
				case 0: dokkumen = SATU_FILE;break;
				case 1: dokkumen = DUA_FILE;break;	
				default: dokkumen = SATU_FILE;break;
				}
			}
			return dokkumen;
		}
				
		public boolean isSatuFile(){
			return this == SATU_FILE;
		}
		
		public boolean isDuaFile(){
			return this == DUA_FILE;
		}
}
