package models.devpartner.common;

import models.common.Kategori;

public class KategoriDp {
    public static final Kategori[] all = new Kategori[]{
            Kategori.PEKERJAAN_KONSTRUKSI,
            Kategori.KONSULTANSI
    };
}
