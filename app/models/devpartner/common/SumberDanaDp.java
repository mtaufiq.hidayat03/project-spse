package models.devpartner.common;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum SumberDanaDp {
	ADB(1, "Asian Development Bank", false), WB(2, "World Bank"),  CINA_EXIM_BANK(3, "China EXIM Bank"), JICA(4, "JICA"), EDCF(5, "Economic Development Corporation Fund");

	public final Integer id;
	public final String label;
	public final boolean mandatoryReoi;

	SumberDanaDp(int id, String label, boolean mandatoryReoi){
		this.id = Integer.valueOf(id);
		this.label = label;
		this.mandatoryReoi = mandatoryReoi;
	}

	SumberDanaDp(int id, String label){
		this(id, label, true);
	}

	public String getLabel() {
		return label;
	}

	public static SumberDanaDp findById(Integer id)
	{
		for (SumberDanaDp sbd: SumberDanaDp.values()) {
			if (sbd.id.equals(id)) return sbd;
		}
		return ADB;
	}
}
