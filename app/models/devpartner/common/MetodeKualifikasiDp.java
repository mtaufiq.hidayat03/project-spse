package models.devpartner.common;


import play.i18n.Messages;

public enum MetodeKualifikasiDp {

    EMPANELMENT(0, "mpdp.empanelment", false),
    CALL_DOWN(1, "mpdp.calldown", false),
    RCS(2, "mpdp.rcs", true),
    QBS(3, "mpdp.qbs", true),
    CQS(4, "mpdp.cqs", true),
    POST_QUAL(5, "mpdp.post_qual", false),
    PRE_QUAL(6, "mpdp.pre_qual", true);

    public final Integer id;
    public final String label;
    public final boolean preQual;

    MetodeKualifikasiDp(int key, String label, boolean preQual) {
        this.id = Integer.valueOf(key);
        this.label = label;
        this.preQual = preQual;
    }

    public static MetodeKualifikasiDp fromValue(Integer value) {
        for (MetodeKualifikasiDp kualifikasi: MetodeKualifikasiDp.values()) {
            if (kualifikasi.id.equals(value)) return kualifikasi;
        }

        return EMPANELMENT;
    }

    public String getLabel() {
        return Messages.get(label);
    }

    public boolean isPreQualification() {
        return preQual;
    }
}
