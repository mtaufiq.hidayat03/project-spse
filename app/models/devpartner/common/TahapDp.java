package models.devpartner.common;

import models.jcommon.util.CommonUtil;
import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Model tahap yang yang ada dalam proses lelang dev partner
 * semua tahap harus didefinisikan di sini, sebelum diimplementasikan
 *
 * @author idoej
 *
 */
@Enumerated(EnumType.STRING)
public enum TahapDp {

	PENGUMUMAN_LELANG(1,"tahap_dp.pengumuman", Arrays.asList(Rule.PENGUMUMAN_LELANG)),
	SENDING_RFP_FOR_PANEL_MEMBERS(2,"tahap_dp.sending.rfp", Arrays.asList(Rule.PENGUMUMAN_LELANG)),

	ISSUANCE_BIDDING_DOCUMENT(3,"tahap_dp.issuance.bidding.doc", Arrays.asList(Rule.PENGUMUMAN_SHORTLIST)),
	ISSUANCE_RFP(4,"tahap_dp.issuance.rfp", Arrays.asList(Rule.PENGUMUMAN_SHORTLIST)),

	CLARIFICATION_FORUM(5,"tahap_dp.clarification.forum", Arrays.asList(Rule.PENJELASAN)),
	PREPROPOSAL_FORUM(6,"tahap_dp.preproposal.forum", Arrays.asList(Rule.PENJELASAN)),

	SUBMISSION_PREQUALIFICATION_DOCUMENT(7,"tahap_dp.submission.prequal", Arrays.asList(Rule.UPLOAD_EOI)),
	UPLOAD_EOI_ICB(8,"tahap_dp.upload.eoi.icb", Arrays.asList(Rule.UPLOAD_EOI)),
	UPLOAD_EOI(9,"tahap_dp.upload.eoi", Arrays.asList(Rule.PENAWARAN, Rule.TAMPILKAN_PENAWARAN)),

	DETERMINATION_PREQUALIFIED_FIRM(10,"tahap_dp.determination.prequal", Arrays.asList(Rule.TAMPILKAN_INFO_PESERTA, Rule.PENETAPAN_SHORTLIST)),
	SHORTLIST_APPOINTMENT(11,"tahap_dp.shortlist.appointment", Arrays.asList(Rule.TAMPILKAN_INFO_PESERTA, Rule.PENETAPAN_SHORTLIST)),
	SHORTLIST_ANNOUNCEMENT(12,"tahap_dp.shortlist.announcement", Arrays.asList(Rule.PENGUMUMAN_SHORTLIST)),

	SUBMISSION_PROPOSALS(13,"tahap_dp.submission.proposal", Arrays.asList(Rule.PENAWARAN)),
	PEMASUKAN_PENAWARAN(14,"tahap_dp.pemasukan.penawaran", Arrays.asList(Rule.PENAWARAN)),

	PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS(15,"tahap_dp.pembukaan.eval.teknis", Arrays.asList(Rule.TAMPILKAN_INFO_PESERTA, Rule.TAMPILKAN_PENAWARAN, Rule.EVALUASI_TEKNIS, Rule.PEMBUKAAN)),
	OPENING_OF_FINANCIAL_PROPOSALS(16,"tahap_dp.opening.financial", Arrays.asList(Rule.PEMBUKAAN_HARGA)),
	PEMBUKAAN_PENAWARAN(17,"tahap_dp.pembukaan.penawaran", Arrays.asList(Rule.TAMPILKAN_INFO_PESERTA, Rule.TAMPILKAN_PENAWARAN, Rule.PEMBUKAAN)),
	BID_OPENING(18,"tahap_dp.bid_opening", Arrays.asList(Rule.TAMPILKAN_INFO_PESERTA, Rule.TAMPILKAN_PENAWARAN, Rule.PEMBUKAAN)),

	TECHNICAL_RANK(19,"tahap_dp.tech.rank", Arrays.asList(Rule.PENGUMUMAN_TEKNIS)),
	PENGUMUMAN_PEMENANG_ADM_TEKNIS(20,"tahap_dp.pengumuman.teknis", Arrays.asList(Rule.PENGUMUMAN_TEKNIS)),

	EVALUATION_PROPOSALS(21,"tahap_dp.evaluation.proposal", Arrays.asList(Rule.EVALUASI_TEKNIS, Rule.EVALUASI_HARGA, Rule.EVALUASI, Rule.EVALUASI_AKHIR)),
	COMBINED_EVALUATION(22,"tahap_dp.combined.eval", Arrays.asList(Rule.EVALUASI, Rule.EVALUASI_AKHIR)),
	COMBINED_EVALUATION_ONE_FILE(23,"tahap_dp.combined.eval", Arrays.asList(Rule.EVALUASI_TEKNIS, Rule.EVALUASI_HARGA, Rule.EVALUASI, Rule.EVALUASI_AKHIR)),
	EVALUATION_OF_FINANCIAL_PROPOSALS(24,"tahap_dp.eval.financial", Arrays.asList(Rule.EVALUASI_HARGA)),

	FINAL_RANGKING(25,"tahap_dp.final.rank", Arrays.asList(Rule.PENETAPAN_PEMENANG, Rule.FINAL_RANKING)),
	EVALUATION_OF_EOI(26,"tahap_dp.evaluation_eoi", Arrays.asList(Rule.TAMPILKAN_INFO_PESERTA, Rule.EVALUASI, Rule.PENETAPAN_PEMENANG)),
	NOTIFICATION_OF_SELECTION(27,"tahap_dp.notification_selection", Arrays.asList(Rule.PENGUMUMAN_PEMENANG_AKHIR)),

	CONTRACT_NEGOTIATION(28,"tahap_dp.contract.nego", Arrays.asList(Rule.CONTRACT_NEGOTIATION, Rule.PENETAPAN_PEMENANG)),
	TANDATANGAN_KONTRAK(29,"tahap_dp.ttd.kontrak", Arrays.asList(Rule.PENGUMUMAN_PEMENANG_AKHIR)),

	BELUM_DILAKSANAKAN(0,"thp.51"),
	TIDAK_ADA_JADWAL(0, "thp.52"),
	SUDAH_SELESAI(0, "thp.53"),
	PAKET_SUDAH_SELESAI(0, "thp.54"), //untuk PL
	PAKET_BELUM_DILAKSANAKAN(0,"thp.55");

    public final Integer id;
    public final String label;
    private final List<Rule> rules;

	private TahapDp(int id, String nama, List<Rule> rules) {
		this.id = Integer.valueOf(id);
		this.label = nama;
		this.rules = rules;
	}

	private TahapDp(int id, String nama) {
		this(id, nama, null);
	}

    public String getLabel()
	{
		return Messages.get(label);
	}

	public static String tahapInfo(String tahaps, boolean shortVersion) {
    	if(!CommonUtil.isEmpty(tahaps)) {
			String[] tahap = tahaps.split(",");
			TahapDp obj = null;
			if(tahap.length == 1) {
				obj = TahapDp.valueOf(tahap[0]);
				return obj.label;
			}
			else {
				StringBuilder result = new StringBuilder();	
				boolean first = true;				
				for(String value:tahap) {
					obj = TahapDp.valueOf(value);
					if(!first)
					{
						if(shortVersion)
						{
							result.append(" [...]");
							break;
						}
						else
							result.append("<br>");
					}
					result.append(obj.label);
					first = false;
				}
				return result.toString();
			}
		}			
		return "";
    }

	private boolean hasRule(Rule targetRule) {
		if (rules != null) {
			for (Rule rule: rules) {
				if (rule.equals(targetRule)) {
					return true;
				}
			}
		}

		return false;
	}

	public boolean isPengumumanLelang() {
		return hasRule(Rule.PENGUMUMAN_LELANG);
	}

	public boolean isUploadEoi() {
		return hasRule(Rule.UPLOAD_EOI);
	}

	public boolean isPengumumanPemenangAkhir() {
		return hasRule(Rule.PENGUMUMAN_PEMENANG_AKHIR);
	}

	public boolean isPembukaan() {
		return hasRule(Rule.PEMBUKAAN);
	}

	public boolean isPembukaanHarga() {
		return hasRule(Rule.PEMBUKAAN_HARGA);
	}

	public boolean isPenjelasan() {
		return hasRule(Rule.PENJELASAN);
	}

	public boolean isPenetapanShortlist() {
		return hasRule(Rule.PENETAPAN_SHORTLIST);
	}

	public boolean isPengumumanShortlist() {
		return hasRule(Rule.PENGUMUMAN_SHORTLIST);
	}

	public boolean isEvaluasi() {
		return hasRule(Rule.EVALUASI);
	}

	public boolean isEvaluasiTeknis() {
		return hasRule(Rule.EVALUASI_TEKNIS);
	}

	public boolean isEvaluasiHarga() {
		return hasRule(Rule.EVALUASI_HARGA);
	}

	public boolean isEvaluasiAkhir() {
		return hasRule(Rule.EVALUASI_AKHIR);
	}

	public boolean isPenawaran() {
		return hasRule(Rule.PENAWARAN);
	}

	public boolean isTampilkanPenawaran() {
		return hasRule(Rule.TAMPILKAN_PENAWARAN);
	}

	public boolean isPengumumanTeknis() {
		return hasRule(Rule.PENGUMUMAN_TEKNIS);
	}

	public boolean isPenetapanPemenang() {
		return hasRule(Rule.PENETAPAN_PEMENANG);
	}

	public boolean isFinalRanking() {
		return hasRule(Rule.FINAL_RANKING);
	}

    public boolean isContractNegotiation() {
        return hasRule(Rule.CONTRACT_NEGOTIATION);
    }

	public boolean canTampilkanInfoPeserta() {
		return hasRule(Rule.TAMPILKAN_INFO_PESERTA);
	}

	private static List<TahapDp> listByRule(Rule rule) {
		List<TahapDp> tahaps = new ArrayList<>();
		for (TahapDp tahap: values()) {
			if (tahap.hasRule(rule)) {
				tahaps.add(tahap);
			}
		}
		return tahaps;
	}

	public static List<TahapDp> listPengumumanLelang() {
		return listByRule(Rule.PENGUMUMAN_LELANG);
	}

    private enum Rule {
		PENGUMUMAN_LELANG,
		UPLOAD_EOI,
		PENAWARAN,
    	PENGUMUMAN_PEMENANG_AKHIR,
		PEMBUKAAN,
		PEMBUKAAN_HARGA,
		PENJELASAN,
		PENETAPAN_SHORTLIST,
		PENGUMUMAN_SHORTLIST,
		EVALUASI,
		EVALUASI_TEKNIS,
		EVALUASI_HARGA,
		EVALUASI_AKHIR,
		PENGUMUMAN_TEKNIS,
		TAMPILKAN_INFO_PESERTA,
		TAMPILKAN_PENAWARAN,
		FINAL_RANKING,
		PENETAPAN_PEMENANG,
        CONTRACT_NEGOTIATION
	}
}
