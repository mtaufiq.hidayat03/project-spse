package models.devpartner.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import models.common.Kategori;
import models.devpartner.DokLelangDp;
import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.Arrays;
import java.util.List;

import static models.devpartner.DokLelangDp.JenisDokLelang.*;
import static models.devpartner.common.JenisKontrakDp.*;
import static models.devpartner.common.SumberDanaDp.*;


/**
 * Model untuk Metode Pelelangan
 * Semua metode lelang harus di jelaskan di model ini
 * @author idoej
 *
 */
@Enumerated(EnumType.ORDINAL)
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MetodeDp {
	// TODO: cek apakah menggunakan list of dokumen cocok dibanding langsung reoi, eoi dan dok lelang secara statik. karena di tempat lain, reoi, eoi dan dok lelang itu statik
	EMPANELMENT(1, MetodeKualifikasiDp.EMPANELMENT, MetodeDokumenDp.SATU_FILE, MetodeEvaluasiDp.ELIMINATION,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY), new DokumenFlag(DOKUMEN_LELANG, "doc.reoi")),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	CALL_DOWN(2, MetodeKualifikasiDp.CALL_DOWN, MetodeDokumenDp.DUA_FILE, MetodeEvaluasiDp.QCBS,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI, "doc.loi"), new DokumenFlag(DOKUMEN_LELANG)),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	RCS_2_QCBS(3, MetodeKualifikasiDp.RCS, MetodeDokumenDp.DUA_FILE, MetodeEvaluasiDp.QCBS,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY), new DokumenFlag(DOKUMEN_REOI), new DokumenFlag(DOKUMEN_LELANG, false)),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	RCS_2_ELIM(4, MetodeKualifikasiDp.RCS, MetodeDokumenDp.DUA_FILE, MetodeEvaluasiDp.ELIMINATION,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY), new DokumenFlag(DOKUMEN_REOI), new DokumenFlag(DOKUMEN_LELANG, false)),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	RCS_1_ELIM(5, MetodeKualifikasiDp.RCS, MetodeDokumenDp.SATU_FILE, MetodeEvaluasiDp.ELIMINATION,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY), new DokumenFlag(DOKUMEN_REOI), new DokumenFlag(DOKUMEN_LELANG, false)),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	QBS(6, MetodeKualifikasiDp.QBS, MetodeDokumenDp.DUA_FILE, MetodeEvaluasiDp.QBS,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY), new DokumenFlag(DOKUMEN_REOI), new DokumenFlag(DOKUMEN_LELANG, false)),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	CQS(7, MetodeKualifikasiDp.CQS, MetodeDokumenDp.SATU_FILE, MetodeEvaluasiDp.CQS_ELIMINATION,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY), new DokumenFlag(DOKUMEN_REOI), new DokumenFlag(DOKUMEN_LELANG, false)),
			Arrays.asList(LUMP_SUM, HARGA_SATUAN, LUMP_SUM_HARGA_SATUAN),
			Arrays.asList(ADB, WB, JICA, EDCF)),
	POST_QUAL(8, MetodeKualifikasiDp.POST_QUAL, MetodeDokumenDp.SATU_FILE, MetodeEvaluasiDp.PASS_FAIL,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY, "doc.bid_notif"), new DokumenFlag(DOKUMEN_LELANG, "doc.bid")),
			Arrays.asList(LUMP_SUM, TIME_BASED, LUMP_SUM_TIME_BASED),
			Arrays.asList(ADB, CINA_EXIM_BANK, JICA)),
	PRE_QUAL(9, MetodeKualifikasiDp.PRE_QUAL, MetodeDokumenDp.SATU_FILE, MetodeEvaluasiDp.PASS_FAIL,
			Arrays.asList(new DokumenFlag(DOKUMEN_REOI_SUMMARY, "doc.invitation_for_applicant"), new DokumenFlag(DOKUMEN_REOI, "doc.pq"), new DokumenFlag(DOKUMEN_LELANG, false, "doc.bid")),
			Arrays.asList(LUMP_SUM, TIME_BASED, LUMP_SUM_TIME_BASED),
			Arrays.asList(ADB, CINA_EXIM_BANK, JICA));

	public final Integer id;
	public final MetodeKualifikasiDp kualifikasi;
	public final MetodeDokumenDp dokumen;
	public final MetodeEvaluasiDp evaluasi;
	public final List<DokumenFlag> docs;
	public final List<JenisKontrakDp> jenisKontrakList;
	public final List<SumberDanaDp> sumberDanaList;

	MetodeDp(int id, MetodeKualifikasiDp kualifikasi, MetodeDokumenDp dokumen, MetodeEvaluasiDp evaluasi,
			 List<DokumenFlag> docs, List<JenisKontrakDp> jenisKontrakList, List<SumberDanaDp> sumberDanaList) {
		this.id = Integer.valueOf(id);
		this.kualifikasi = kualifikasi;
		this.dokumen = dokumen;
		this.evaluasi = evaluasi;
		this.docs = docs;
		this.jenisKontrakList = jenisKontrakList;
		this.sumberDanaList = sumberDanaList;
	}

	public static MetodeDp findById(Integer value) {
		for (MetodeDp metode: MetodeDp.values()) {
			if (metode.id.equals(value)) return metode;
		}
		return EMPANELMENT;
	}

	public static final MetodeDp[] metodeKonsultan = new MetodeDp[]{EMPANELMENT, CALL_DOWN, RCS_2_QCBS, RCS_2_ELIM, RCS_1_ELIM, QBS, CQS};

	public static final MetodeDp[] metodeKonstruksi = new MetodeDp[]{POST_QUAL, PRE_QUAL};

	public static MetodeDp[] getByKategori(Kategori kategori) {
		return kategori.isKonsultansi() ? metodeKonsultan : metodeKonstruksi;
	}

	public boolean hasReoiSummary() {
		for (DokumenFlag doc: docs) {
			if (doc.jenis.isReoiSummary()) return true;
		}
		return false;
	}

	public boolean hasReoi() {
		for (DokumenFlag doc: docs) {
			if (doc.jenis.isReoi()) return true;
		}
		return false;
	}

	public boolean isMandatoryReoiSummaryPengumuman() {
		for (DokumenFlag doc: docs) {
			if (doc.jenis.isReoiSummary()) return doc.mandatoryPengumuman;
		}
		return false;
	}

	public boolean isMandatoryReoiPengumuman() {
		for (DokumenFlag doc: docs) {
			if (doc.jenis.isReoi()) return doc.mandatoryPengumuman;
		}
		return false;
	}

	public boolean isMandatoryDokLelang() {
		for (DokumenFlag doc: docs) {
			if (doc.jenis.isDokLelang()) return doc.mandatoryPengumuman;
		}
		return false;
	}

	public String getDokLabel(DokLelangDp.JenisDokLelang jenisDok) {
		for (DokumenFlag doc: docs) {
			if (doc.jenis.value.equals(jenisDok.value)) return doc.label;
		}
		return null;
	}

	public static class DokumenFlag {
		public DokLelangDp.JenisDokLelang jenis;
		public boolean mandatoryPengumuman;
		public String label;

		public DokumenFlag(DokLelangDp.JenisDokLelang jenis, boolean mandatoryPengumuman, String label) {
			this.jenis = jenis;
			this.mandatoryPengumuman = mandatoryPengumuman;
			this.label = label;
		}

		public DokumenFlag(DokLelangDp.JenisDokLelang jenis) {
			this(jenis, true, jenis.label);
		}

		public DokumenFlag(DokLelangDp.JenisDokLelang jenis, boolean mandatoryPengumuman) {
			this(jenis, mandatoryPengumuman, jenis.label);
		}

		public DokumenFlag(DokLelangDp.JenisDokLelang jenis, String label) {
			this(jenis, true, label);
		}
	}

	public boolean isEmpanelment() {
		return kualifikasi.equals(MetodeKualifikasiDp.EMPANELMENT);
	}

	public boolean isPanel() {
		return isEmpanelment() || isCallDown();
	}

	public boolean isCallDown() {
		return kualifikasi.equals(MetodeKualifikasiDp.CALL_DOWN);
	}

	public boolean isCqs() {
		return kualifikasi.equals(MetodeKualifikasiDp.CQS);
	}

	public boolean isQbs() {
		return kualifikasi.equals(MetodeKualifikasiDp.QBS);
	}

	public String getLabel() {
		return Messages.get(kualifikasi.label) + " | " + Messages.get(dokumen.label) + " | " + Messages.get(evaluasi.label);
	}

	public boolean isEnableJv() {
		return !isPanel();
	}
}
