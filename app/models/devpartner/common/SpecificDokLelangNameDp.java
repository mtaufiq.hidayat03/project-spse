package models.devpartner.common;

import models.devpartner.DokLelangDp;

import java.util.Arrays;
import java.util.List;

public class SpecificDokLelangNameDp {

    public final MetodeDp metode;
    public final SumberDanaDp sumberDana;
    public final DokLelangDp.JenisDokLelang dokLelang;
    public final String dokLabel;

    private static final List<SpecificDokLelangNameDp> CONFIG = Arrays.asList(
            create(MetodeDp.RCS_2_QCBS, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY, "doc.reoi"),
            create(MetodeDp.RCS_2_QCBS, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI, "doc.tor"),
            create(MetodeDp.RCS_2_ELIM, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY, "doc.reoi"),
            create(MetodeDp.RCS_2_ELIM, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI, "doc.tor"),
            create(MetodeDp.RCS_1_ELIM, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY, "doc.reoi"),
            create(MetodeDp.RCS_1_ELIM, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI, "doc.tor"),
            create(MetodeDp.QBS, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY, "doc.reoi"),
            create(MetodeDp.QBS, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI, "doc.tor"),
            create(MetodeDp.CQS, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY, "doc.reoi"),
            create(MetodeDp.CQS, SumberDanaDp.WB, DokLelangDp.JenisDokLelang.DOKUMEN_REOI, "doc.tor")
    );

    private SpecificDokLelangNameDp(MetodeDp metode, SumberDanaDp sumberDana, DokLelangDp.JenisDokLelang dokLelang, String dokLabel) {
        this.metode = metode;
        this.sumberDana = sumberDana;
        this.dokLelang = dokLelang;
        this.dokLabel = dokLabel;
    }

    private static SpecificDokLelangNameDp create(MetodeDp metode, SumberDanaDp sumberDana, DokLelangDp.JenisDokLelang dokLelang, String dokLabel) {
        return new SpecificDokLelangNameDp(metode, sumberDana, dokLelang, dokLabel);
    }

    private boolean isMatch(MetodeDp metode, SumberDanaDp sumberDana, DokLelangDp.JenisDokLelang dokLelang) {
        return this.metode.id.equals(metode.id)
                && this.sumberDana.id.equals(sumberDana.id)
                && this.dokLelang.value.equals(dokLelang.value);
    }

    public static SpecificDokLelangNameDp get(MetodeDp metode, SumberDanaDp sumberDana, DokLelangDp.JenisDokLelang dokLelang) {
        for (SpecificDokLelangNameDp config: CONFIG) {
            if (config.isMatch(metode, sumberDana, dokLelang)) {
                return config;
            }
        }
        return null;
    }

}
