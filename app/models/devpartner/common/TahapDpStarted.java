package models.devpartner.common;

import models.common.Active_user;
import models.common.StatusLelang;
import models.devpartner.LelangDp;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * class ini digunakan untuk membuat simple dalam menangani flow proses lelang yang sudah dilewati
 * berguna dalam simplify definisi di controller terkait workflow lelang
 * @author idoej
 *
 */
public class TahapDpStarted extends TahapDpTracker {

	StatusLelang status;
	MetodeDp metode;

	public TahapDpStarted(Long lelangId) {
		this.lelangId = lelangId;
		Query.find("SELECT mtd_id, lls_status, mtd_pemilihan, lls_metode_penawaran FROM devpartner.Lelang_dp WHERE lls_id=?", new ResultSetHandler<Object>() {

			@Override
			public Object handle(ResultSet rs) throws SQLException {
				metode = MetodeDp.findById(rs.getInt("mtd_id"));
				status = StatusLelang.fromValue(rs.getInt("lls_status"));
				return null;
			}

		}, lelangId).first();
		this.tahapList = AktivitasDp.findTahapStarted(lelangId);
		this.active_user = Active_user.current();
//		Logger.info(tahapList.toString());
	}


	public TahapDpStarted(LelangDp lelang) {
		this.lelangId = lelang.lls_id;
		this.metode = lelang.getMetode();
		this.status = lelang.lls_status;
		this.tahapList = AktivitasDp.findTahapStarted(lelangId);
		this.active_user = Active_user.current();
	}
	
	@Override
	public String toString() {
		return "TahapStarted @"+lelangId+" [" + tahapList.toString() + ']';
	}

	// TODO: benerin

	/**
	 * Method ini (sepertinya) untuk flagging apakah nama penyedia yang sebenarnya ditampilkan. Jika tidak, maka dimasking.
	 * Ada beberapa tempat yang membutuhkan flag ini, yaitu penjelasan dan daftar peserta.
	 * @return
	 */
	public boolean isShowPenyedia() {
		return tahapList.stream().anyMatch(tahapDp -> tahapDp.canTampilkanInfoPeserta());
	}
}
