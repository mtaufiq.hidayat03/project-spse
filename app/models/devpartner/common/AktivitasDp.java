package models.devpartner.common;

import models.devpartner.JadwalDp;
import models.jcommon.db.base.BaseModel;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;

@Table(name="aktivitas_dp", schema = "devpartner")
public class AktivitasDp extends BaseModel {

	@Id
	public Long akt_id; // aktivitas ini adalah data master jadi tidak perlu sequence, dan diset oleh LKPP
	
	public Integer mtd_id;
	
	public TahapDp akt_jenis; //Tahap
	
	public Integer akt_urut;

	public Integer akt_status; // default 1. status tahapan lelang, perbedaan peraturan sangat mungkin tahapan bisa hilang/tambah
	
	public static List<AktivitasDp> findByMetode(Integer metodeId) {
		return find("mtd_id= ? and akt_status = 1 order by akt_urut asc" , metodeId).fetch();
	}
	
	/**
	 * cek apakah tahap yang diminta sudah dilalui
	 * @param lelangId
	 * @param tahapList
	 * @return
	 */
	public static boolean isTahapStarted(Long lelangId, TahapDp... tahapList) {
		Date date = controllers.BasicCtr.newDate();
		StringBuilder sqlWhere = new StringBuilder("akt_id in (select akt_id from devpartner.jadwal_dp where lls_id=? and dtj_tglawal <= ?)");
		if(tahapList.length > 0) {
			sqlWhere.append(" AND akt_jenis in ('");
			sqlWhere.append(StringUtils.join(tahapList, "','"));
			sqlWhere.append("')");
		}
		return AktivitasDp.count(sqlWhere.toString(), lelangId, date) > 0;
	}

	/**
	 * dapatkan tahapan lelang
	 * @param lelangId
	 * @return
	 */
	public static List<TahapDp> findTahapLelang(Long lelangId) {
		return Query.find(SQL_TAHAP_LELANG,TahapDp.class,lelangId).fetch();
	}

	/**
	 * dapatkan tahapan sekarang dan sudah lewat
	 * @param lelangId
	 * @return
	 */
	public static List<TahapDp> findTahapStarted (Long lelangId) {
		Date date = controllers.BasicCtr.newDate();
		return Query.find(SQL_TAHAP_STARTED,TahapDp.class,lelangId,date).fetch();
	}
	
	/**
	 * dapatkan tahapan sekarang dan masih berlangsung
	 * @param lelangId
	 * @return
	 */
	
	public static List<TahapDp> findTahapNow(Long lelangId) {
		Date date = controllers.BasicCtr.newDate();				
		return Query.find(SQL_TAHAP_NOW, TahapDp.class,lelangId,date, date).fetch();
	}
	
	//asep:
	//cek lelang apakah sudah selesai
	//asumsi tidak ada jadwal berarti belum selesai
	public static boolean isLelangFinish(Long lelangId){
		String sql = "select j.*  from jadwal j, aktivitas a where j.akt_id=a.akt_id and j.lls_id= ? order by j.dtj_tglakhir desc limit 1";		
		try {
			JadwalDp jadwal = Query.find(sql, JadwalDp.class,lelangId).first();
			Date date = controllers.BasicCtr.newDate();
			return jadwal.dtj_tglakhir.before(date);		
		} catch (Exception e) {
			return false;
		}	
	}

	private static final String SQL_TAHAP_LELANG = "select a.akt_jenis from devpartner.jadwal_dp j, devpartner.aktivitas_dp a where j.akt_id=a.akt_id and j.lls_id= ?" ;
	private static final String SQL_TAHAP_NOW = "select a.akt_jenis from devpartner.jadwal_dp j, devpartner.aktivitas_dp a where j.akt_id=a.akt_id and j.lls_id= ? and dtj_tglawal <= ? and dtj_tglakhir >= ? " ;
	private static final String SQL_TAHAP_STARTED = "select a.akt_jenis from devpartner.jadwal_dp j, devpartner.aktivitas_dp a where j.akt_id=a.akt_id and j.lls_id= ? and j.dtj_tglawal <= ?";

}
