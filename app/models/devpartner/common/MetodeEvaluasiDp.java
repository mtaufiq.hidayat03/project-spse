package models.devpartner.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum MetodeEvaluasiDp {
	ELIMINATION (0, "mep.elim"), QCBS (1, "mep.qcbs"),
	QBS (2, "mep.qbs"), PASS_FAIL (3, "mep.pass_fail"), CQS_ELIMINATION (4, "mep.elim");

	public final Integer id;
	public final String label;

	MetodeEvaluasiDp(int key, String label){
		this.id = Integer.valueOf(key);
		this.label = label;
	}

	public String getLabel() {
		return Messages.get(label);
	}

	public static MetodeEvaluasiDp findById(Integer value){
		MetodeEvaluasiDp evaluasi = null;
		if(value != null){
			switch(value.intValue()){
			case 0: evaluasi = ELIMINATION;break;
			case 1: evaluasi = QCBS;break;
			case 2: evaluasi = QBS;break;
			case 3: evaluasi = PASS_FAIL;break;
			case 4: evaluasi = CQS_ELIMINATION;break;
			default: evaluasi = ELIMINATION;break;
			}
		}
		return evaluasi;
	}
		
	public boolean isGugur(){
		return this == ELIMINATION || this == PASS_FAIL;
	}

	public boolean isNilai(){
		return this == QCBS || this == QBS || this == CQS_ELIMINATION;
	}

	public boolean isNilaiHarga(){
		return this == QCBS;
	}

	public boolean isQcbs() {
		return this == QCBS;
	}

	public boolean isCqs() {
		return this == CQS_ELIMINATION;
	}

	public boolean isQbs() {
		return this == QBS;
	}

	public boolean isAutoCombinedEvaluation() {
		return this == QBS || this == CQS_ELIMINATION;
	}
}
