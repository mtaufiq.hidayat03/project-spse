package models.devpartner.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum JenisKontrakDp {
	LUMP_SUM(1, Messages.get("jenis_kontrak.lumpsum")),
	HARGA_SATUAN(2, Messages.get("jenis_kontrak.harga_satuan")),
	LUMP_SUM_HARGA_SATUAN(3, Messages.get("jenis_kontrak.gabungan_lumpsum_harga_satuan")),
	TIME_BASED(4, Messages.get("jenis_kontrak.time_based")),
	LUMP_SUM_TIME_BASED(5, Messages.get("jenis_kontrak.gabungan_lumpsum_time_based"));

	public final Integer id;
	public final String label;
	
	JenisKontrakDp(int id, String label){
		this.id = Integer.valueOf(id);
		this.label = label;
	}	
	
	public static JenisKontrakDp findById(Integer id)
	{
		for (JenisKontrakDp jenis: JenisKontrakDp.values()) {
			if (jenis.id.equals(id)) return jenis;
		}
		return LUMP_SUM;
	}

	public boolean isLumpsum(){
		return this == LUMP_SUM;
	}

	public boolean isHargaSatuan() {
		return this == HARGA_SATUAN;
	}

	public boolean isLumpsumHargaSatuan() {
		return this == LUMP_SUM_HARGA_SATUAN;
	}

	public boolean isTimeBased() {
		return this == TIME_BASED;
	}

	public boolean isLumpsumTimeBased() {
		return this == LUMP_SUM_TIME_BASED;
	}
}

