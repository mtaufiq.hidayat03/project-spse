package models.devpartner.common;

import play.db.jdbc.Enumerated;
import play.i18n.Messages;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum KontrakTahunDp {
	TAHUN_TUNGGAL(1, Messages.get("tahun_anggaran.tunggal")),
	TAHUN_JAMAK(2, Messages.get("tahun_anggaran.jamak"));

	public final Integer id;
	public final String label;

	KontrakTahunDp(int id, String label){
		this.id = Integer.valueOf(id);
		this.label = label;
	}	
	
	public static KontrakTahunDp findById(Integer id)
	{
		KontrakTahunDp jenis = null;
		if(id == null)
			return null;
		switch (id.intValue()) {
		case 1:
			jenis = TAHUN_TUNGGAL;
			break;
		case 2:
			jenis = TAHUN_JAMAK;
			break;
		default:
			jenis = TAHUN_TUNGGAL;
			break;
		}
		return jenis;
	}
}

