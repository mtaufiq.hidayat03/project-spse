package models.devpartner;

import models.agency.Pegawai;
import models.common.Kategori;
import models.common.UploadInfo;
import models.devpartner.handler.DokLelangDpDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.io.File;
import java.util.List;

import static models.jcommon.blob.BlobTable.ARCHIEVE_MODE;


@Table(name="dok_lelang_dp", schema = "devpartner")
public class DokLelangDp extends BaseModel {

	@Enumerated(EnumType.ORDINAL)
	public enum JenisDokLelang {

		DOKUMEN_REOI_SUMMARY(0, "doc.reoi_summary"),// REOI Summary, invitation for applicant, bidding notification
		DOKUMEN_LELANG(1, "doc.rfp"),// RFP, Bidding document
		DOKUMEN_REOI(2, "doc.reoi"), // REOI, letter of invitation, PQ doc
		DOKUMEN_TAMBAHAN(3, "doc.tambahan");

		public final Integer value;
		public final String label;

		JenisDokLelang(int value, String label) {
			this.value = Integer.valueOf(value);
			this.label = label;
		}

		public static JenisDokLelang fromValue(Integer value){
			JenisDokLelang jenis = null;
			if(value!=null){
				switch (value.intValue()) {
					case 0:	jenis = DOKUMEN_REOI_SUMMARY;break;
					case 1:	jenis = DOKUMEN_LELANG;break;
					case 2:	jenis = DOKUMEN_REOI;break;
					case 3:	jenis = DOKUMEN_TAMBAHAN;break;
				}
			}
			return jenis;
		}

		public boolean isReoiSummary(){
			return this == DOKUMEN_REOI_SUMMARY;
		}


		public boolean isDokLelang(){
			return this == DOKUMEN_LELANG;
		}

		public boolean isReoi(){
			return this == DOKUMEN_REOI;
		}

		public boolean isTambahan(){
			return this == DOKUMEN_TAMBAHAN;
		}
	}

	@Id(sequence="devpartner.seq_dok_lelang_dp", function="nextsequence")
	public Long dll_id;

	public String dll_nama_dokumen;

	public JenisDokLelang dll_jenis;

	public Long dll_id_attachment;

	//relasi ke Lelang_seleksi
	public Long lls_id;

	@Transient
	private LelangDp lelang_seleksi;

    public LelangDp getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = LelangDp.findById(lls_id);
		return lelang_seleksi;
	}

	public String getAuditUser() {
        return Pegawai.findBy(audituser).peg_nama;
    }

	public Kategori getKategori() {
		return getLelang_seleksi().getKategori();
    }

	@Transient
	public BlobTable getDokumen() {
		if(dll_id_attachment == null)
			return null;
		return BlobTableDao.getLastById(dll_id_attachment);
	}

	@Transient
	public List<BlobTable> getDokumenList() {
		if(dll_id_attachment == null)
			return null;
		return BlobTableDao.listById(dll_id_attachment);
	}

	@Transient
	public String getDownloadUrl() {
		return getDokumen().getDownloadUrl(DokLelangDpDownloadHandler.class);

	}

	@Transient
	public String getDownloadUrl(BlobTable blob) {
		return blob.getDownloadUrl(DokLelangDpDownloadHandler.class);
	}

	public static DokLelangDp findBy(Long lelangId, JenisDokLelang jenis) {
		return find("lls_id=? and dll_jenis=?", lelangId, jenis).first();
	}

	/**
	 * dapatkan dokumen lelang, jika tidak ada maka akan dibuat baru,
	 * Gunakan saat proses submit terkait dokumen lelang
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static DokLelangDp findNCreateBy(Long lelangId, JenisDokLelang jenis) {
		DokLelangDp dok_lelang = find("lls_id=? and dll_jenis=?", lelangId, jenis).first();
		if(dok_lelang == null) {
			dok_lelang = new DokLelangDp();
			dok_lelang.lls_id = lelangId;
			dok_lelang.dll_jenis = jenis;
			dok_lelang.save();
		}
		return dok_lelang;
	}

	public boolean allowPersetujuan() {
		return true;
	}

	public UploadInfo simpan(LelangDp lelang, File dokumen) {
		BlobTable blob = null;
		try {
			if(dokumen != null)
				blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, dokumen);
			if(blob != null)
				dll_id_attachment = blob.blb_id_content;
			save();

		}catch (Exception e){
			Logger.error(e, Messages.get("lelang.save_doc_ggl")+" : %s", e.getMessage());
		}
		return UploadInfo.findBy(blob);
	}
}
