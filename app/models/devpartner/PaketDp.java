package models.devpartner;

import ext.FormatUtils;
import ext.RupiahBinder;
import models.agency.Panitia;
import models.agency.Pegawai;
import models.agency.Satuan_kerja;
import models.agency.Ukpbj;
import models.agency.contracts.SirupContract;
import models.common.Active_user;
import models.common.Kategori;
import models.common.Kualifikasi;
import models.devpartner.contracts.PaketDpContract;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.sirup.PaketSirup;
import models.sirup.PaketSirup.PaketSirupAnggaran;
import models.sirup.PaketSirup.PaketSirupLokasi;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model {@code Paket} merepresentasikan tabel paket pada database.
 *
 * @author idoej
 */


@Table(name = "devpartner.paket_dp")
public class PaketDp extends BaseModel implements PaketDpContract {

	@Enumerated(EnumType.ORDINAL)
	public enum StatusPaket {

		DRAFT(0,"Draft"),
		SEDANG_LELANG(1,"Tender Sedang Berjalan"),
		SELESAI_LELANG(2,"Tender Sudah Selesai"), // di versi 3.5 belum pernah dipakai
		ULANG_LELANG(3,"Tender Diulang"),
		LELANG_DITOLAK(4,"Tender Ditolak");

		public final Integer id;
		public final String label;

		StatusPaket(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}

		public static StatusPaket fromValue(Integer value){
			StatusPaket status = null;
			if(value != null){
				switch(value.intValue()){
					case 0:status = DRAFT;break;
					case 1:status = SEDANG_LELANG;break;
					case 2:status = SELESAI_LELANG;break;
					case 3:status = ULANG_LELANG;break;
					case 4:status = LELANG_DITOLAK;break;
					default:status = DRAFT;break;
				}
			}
			return status;
		}

		public boolean isDraft(){
			return this == DRAFT;
		}

		public boolean isSedangLelang(){
			return this == SEDANG_LELANG;
		}

		public boolean isSelesaiLelang(){
			return this == SELESAI_LELANG;
		}

		public boolean isUlangLelang(){
			return this == ULANG_LELANG;
		}

		public boolean isLelangDitolak(){
			return this == LELANG_DITOLAK;
		}
	}

	@Override
	protected void preDelete(){
		if(pkt_status.isDraft()) {
			LelangDp lelang = LelangDp.findByPaket(pkt_id);
			Query.update("DELETE FROM devpartner.persetujuan_dp WHERE lls_id = ?", lelang.lls_id);
			Query.update("DELETE FROM devpartner.jadwal_dp WHERE lls_id = ?", lelang.lls_id);
//			Query.update("DELETE FROM devpartner.checklist_dp WHERE dll_id IN (SELECT dll_id FROM dok_lelang WHERE lls_id = ?)", lelang.lls_id);
//			Query.update("DELETE FROM devpartner.dok_lelang_dp WHERE lls_id = ?", lelang.lls_id);
			Query.update("DELETE FROM devpartner.lelang_dp WHERE lls_id = ?", lelang.lls_id);

			Query.update("DELETE FROM devpartner.paket_lokasi_dp WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM devpartner.paket_ppk_dp WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM devpartner.paket_anggaran_dp WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM devpartner.paket_satker_dp WHERE pkt_id = ?", pkt_id);
		}

	}

	/**
	 * ID Paket
	 */
	@Id(sequence="devpartner.seq_paket_dp", function="nextsequence")
	public Long pkt_id;
	/**
	 * Nama paket
	 */
	@Required
	public String pkt_nama;
	/**
	 * Nilai pagu paket
	 */
	@Required
	@As(binder=RupiahBinder.class)
	public Double pkt_pagu;
	/**
	 * Nilai HPS (Harga Perkiraan Sendiri) paket
	 */
	@As(binder=RupiahBinder.class)
	public Double pkt_hps;

	/**
	 * View HPS (Harga Perkiraan Sendiri) paket
	 */
	public Boolean pkt_hps_enable;

	/**
	 * dokumen terkait pembuatan paket
	 * (info: sudah tidak dipakai lagi)
	 */
	public Long pkt_id_attachment;

	/**
	 * tanggal realisasi paket
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_realisasi;

	/**
	 * tanggal paket di kirimkan dari panitia ke ppk,
	 * semenjak versi 3.5 di set saat pembuatan paket karena tidak ada persetujuan ppk dari panitia
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_assign;
	/**
	 * Tanggal pembuatan paket
	 */
	public Date pkt_tgl_buat;
	/**
	 * Tanggal persetujuan paket oleh PPK
	 */
	public Date pkt_tgl_ppk_setuju;
	/**
	 * Status paket
	 */
	@Required
	public StatusPaket pkt_status;

	/**
	 * Kualifikasi pelelangan
	 */
	public String kls_id;

	/**
	 * Kepanitiaan pembuat paket lelang
	 */
	@Required
	public Long pnt_id;
	/**
	 * Kategori pengadaan/lelang
	 */
	@Required
	public Kategori kgr_id;
	/**
	 * Nomor surat rencana pelaksanaan
	 */
	public String pkt_no_spk;

	public Long ukpbj_id;

	// informasi build spse
	public String pkt_tgl_build;

	@Transient
	private Panitia panitia;

	@Transient
	private PaketPpkDp paketPpk;
	@Transient
	private Ukpbj ukpbj;
	@Transient
	private LelangDp lelang;

	@Transient
	private List<PaketLokasiDp> locations;

	@Transient
	public List<PaketAnggaranDp> paketAnggarans;

	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}

	public List<PaketAnggaranDp> getPaketAnggarans() {
		if (CollectionUtils.isEmpty(paketAnggarans)) {
			Integer newestVersion = getVersiPaketAnggaran(getPktId());
			paketAnggarans = PaketAnggaranDp.findByPaketAndVersion(getPktId(), newestVersion);
		}
		return paketAnggarans;
	}

	private static Integer getVersiPaketAnggaran(Long pktId) {
		PaketAnggaranDp paket_anggaran = PaketAnggaranDp.find("pkt_id=? order by pkt_ang_versi desc", pktId).first();
		return paket_anggaran==null || paket_anggaran.pkt_ang_versi == null ? 1 : paket_anggaran.pkt_ang_versi;
	}


	@Override
	public Long getPktId() {
		return this.pkt_id;
	}

	@Override
	public Long getUkpbjId() {
		return this.ukpbj_id;
	}

	@Override
	public List<PaketLokasiDp> getPaketLocations() {
		if (CommonUtil.isEmpty(locations)) {
			withLocations();
		}
		return locations;
	}

	@Override
	public PaketPpkDp getPaketPpk() {
		if (paketPpk == null) {
			withPaketPpk();
		}
		return paketPpk;
	}

	@Override
	public Ukpbj getUkpbj() {
		if (ukpbj == null) {
			ukpbj = Ukpbj.findById(ukpbj_id);
		}
		return ukpbj;
	}

	@Override
	public void setPaketLocations(List<PaketLokasiDp> items) {
		this.locations = items;
	}

	@Override
	public void setPaketPpk(PaketPpkDp model) {
		this.paketPpk = model;
	}

	@Override
	public void setUkpbj(Ukpbj model) {
		this.ukpbj = model;
	}

	public String getKodeRup() {
		return PaketSatkerDp.getKodeRupPaket(pkt_id);
	}

	public String getNamaInstansi() {
		return PaketSatkerDp.getNamaInstansiPaket(pkt_id);
	}

	public String getNamaKabupaten() {
		return PaketSatkerDp.getNamaKabupaten(pkt_id);
	}

	public String getNamaSatker() {
		return PaketSatkerDp.getNamaSatkerPaket(pkt_id);
	}

	public String getAlamatSatker() {
		return PaketSatkerDp.getAlamatSatkerPaket(pkt_id);
	}

	public List<Pegawai> getPpkList() {
		return Pegawai.find("peg_id in (SELECT peg_id FROM ppk k, paket_anggaran p WHERE k.ppk_id=p.ppk_id AND p.pkt_id=?)", pkt_id).fetch();
	}

	public List<PaketLokasiDp> getPaketLokasi()
	{
		return PaketLokasiDp.find("pkt_id=?",pkt_id).fetch();
	}

	public void setKualifikasi(Kualifikasi kualifikasi)
	{
		this.kls_id = kualifikasi.id;
	}

	@Transient
	public Kualifikasi getKualifikasi()
	{
		return Kualifikasi.findById(kls_id);
	}

	@Transient
	public String getSumberDana() {
		List<String> result =Query.find("select distinct (a.sbd_id) from devpartner.anggaran_dp a, devpartner.paket_anggaran_dp pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, pkt_id).fetch();
		return StringUtils.join(result, ",");
	}

	@Transient
	public String getTahunAnggaran() {
		List<String> result = Query.find("select distinct (a.ang_tahun) from devpartner.anggaran_dp a, devpartner.paket_anggaran_dp pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, pkt_id).fetch();
		return StringUtils.join(result, ",");
	}

	public List<? extends SirupContract> getSirupListForUI() {
		return getRupList();
	}

	public List<PaketSirup> getRupList() {
		return getRupList(pkt_id);
	}

	public static List<PaketSirup> getRupList(long pktId) {
		Integer lastVersion = getVersiPaketAnggaran(pktId);
		return Query.find("SELECT ps.* FROM paket_sirup ps INNER JOIN devpartner.paket_anggaran_dp pa ON ps.id=pa.rup_id WHERE pa.pkt_id=? AND pkt_ang_versi=? ORDER BY pa.auditupdate ASC",
				PaketSirup.class, pktId, lastVersion).fetch();
	}

	public static PaketDp buatPaketFromSirup(Long rup_id, Long panitiaId) {
		PaketSirup rup = PaketSirup.findById(rup_id);
		PaketDp paket = new PaketDp();
		paket.pkt_nama = rup.nama;
		paket.pkt_pagu = rup.pagu;
		paket.pkt_hps = Double.valueOf(0);
		paket.pnt_id = panitiaId;
		paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
		paket.pkt_status = PaketDp.StatusPaket.DRAFT;
		paket.kgr_id = rup.getKategori();
		paket.save();
		Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
		PaketSatkerDp ps = new PaketSatkerDp();
		ps.stk_id = satker.stk_id;
		ps.pkt_id = paket.pkt_id;
		ps.rup_id = rup.id;
		ps.save();
		Active_user user = Active_user.current();
		paket.createPaketPpk(user);
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)) {
			AnggaranDp anggaran = null;
			PaketAnggaranDp paket_anggaran = null;
			for (PaketSirupAnggaran rup_anggaran : rup.paket_anggaran_json) {
				anggaran = new AnggaranDp();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup_anggaran.tahun_anggaran_dana == null ? rup.tahun : rup_anggaran.tahun_anggaran_dana;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = satker.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran = new PaketAnggaranDp();
				paket_anggaran.pkt_id = paket.pkt_id;
				paket_anggaran.ang_id = anggaran.ang_id;
				paket_anggaran.ppk_id = user.ppkId;
				paket_anggaran.ppk_jabatan = 0;
				paket_anggaran.rup_id = rup.id;
				paket_anggaran.pkt_ang_versi = Integer.valueOf(1);
				paket_anggaran.save();
			}
		}
		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			for(PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				PaketLokasiDp pkt_lokasi = new PaketLokasiDp();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.pkt_id = paket.pkt_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = Integer.valueOf(1);
				pkt_lokasi.save();
			}
		}

		// langsung buat lelang baru
		LelangDp.buatLelangBaru(paket);

		return paket;
	}

	public boolean isSedangPersetujuan() {
		Long lelangId = Query.find("SELECT lls_id FROM devpartner.lelang_dp WHERE pkt_id=? ORDER BY lls_id DESC", Long.class, pkt_id).first();
		return PersetujuanDp.isSedangPersetujuan(lelangId);
	}

	public boolean allowEdit() {
		return (pkt_status.isDraft() || pkt_status.isUlangLelang()) && !isSedangPersetujuan();
	}

	public boolean isEditable(){
		return allowEdit() && !Active_user.current().isPanitia();
	}

	public boolean isEditableByKuppbj() {
		return isEditable() && Active_user.current().isKuppbj();
	}

	public boolean isEditableExceptKuppbj() {
		return isEditable() && !Active_user.current().isKuppbj();
	}

	public boolean isHpsExist() {
		return pkt_hps != null && pkt_hps > 0;
	}

	public String getHpsFormatted() {
		return isHpsExist() ? FormatUtils.formatCurrencyRupiah(pkt_hps) : "Rp. 0";
	}

	public String getPaguFormatted() {
		return pkt_pagu != null ? FormatUtils.formatCurrencyRupiah(pkt_pagu) : "Rp. 0";
	}

	public static PaketDp simpanBuatPaket(PaketDp paket, PaketLokasiDp[] lokasi){
		if (paket.pkt_tgl_buat == null)
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
		if (paket.pkt_hps == null)
			paket.pkt_hps = Double.valueOf(0);
		if (paket.pkt_status == null)
			paket.pkt_status = PaketDp.StatusPaket.DRAFT; // default status draft
		if (paket.kgr_id == null)
			paket.kgr_id = Kategori.PENGADAAN_BARANG; // default pengadaan barang jasa
		if (paket.pkt_tgl_assign == null)
			paket.pkt_tgl_assign = controllers.BasicCtr.newDate();
		paket.save();
		PaketLokasiDp obj = null;
		List<Long> saved = new ArrayList<>(lokasi.length);
		for (PaketLokasiDp lok : lokasi) {
			if (lok.pkl_id == null)
				obj = new PaketLokasiDp();
			else
				obj = PaketLokasiDp.findById(lok.pkl_id);
			obj.kbp_id = lok.kbp_id;
			obj.pkt_lokasi_versi = 1;
			obj.pkt_id = paket.pkt_id;
			if (!StringUtils.isEmpty(lok.pkt_lokasi)) {
				obj.pkt_lokasi = lok.pkt_lokasi;
				obj.save();
			}
			saved.add(obj.pkl_id);
		}
		if(!CommonUtil.isEmpty(saved)) {
			String join = StringUtils.join(saved, ",");
			if(!join.isEmpty())
				Query.update("delete from paket_lokasi where pkt_id=? and pkl_id not in ("+join+ ')', paket.pkt_id);
		}
		LelangDp lls = LelangDp.buatLelangBaru(paket);
		LogUtil.debug("TAG", lls);
		return paket;
	}

	public boolean showExtendedPpkInput() {
		return getPaketLocations().size() > 0;
	}

	public boolean allowEditUkpbj() {
		return !isSedangPersetujuan() && pkt_status.isDraft() && Active_user.current().isPpk();
	}

	public LelangDp getLelang(){
		if(lelang == null){
			lelang = LelangDp.findByPaket(pkt_id);
		}

		return lelang;
	}

	public static PaketDp findByLelang(Long lelangId) {
		return Query.find("SELECT * FROM devpartner.paket_dp WHERE pkt_id IN (SELECT pkt_id FROM devpartner.lelang_dp WHERE lls_id=?)", PaketDp.class, lelangId).first();
	}
}
