package models.devpartner;

import controllers.BasicCtr;
import models.devpartner.contracts.ParticipantDpContract;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.DateUtil;
import models.rekanan.Rekanan;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.db.jpa.Transactional;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


@Table(name="peserta_dp", schema = "devpartner")
public class PesertaDp extends BaseModel implements ParticipantDpContract {
	
	@Id(sequence="devpartner.seq_peserta_dp", function="nextsequence")
	public Long psr_id;

	public Date psr_tanggal;

	public Long psr_black_list;

	public Integer is_pemenang;

	//kolom pemenang terferifikasi
	public Integer is_pemenang_verif;

	public Integer sudah_verifikasi_sikap;

	//relasi ke Lelang_seleksi
	public Long lls_id;

	//relasi ke Rekanan
	public Long rkn_id;
	
	public Double psr_harga;

	public Double psr_harga_terkoreksi;

	public String psr_uraian;
	
	public String psr_alasan_menang;

	public boolean is_dikirim_pesan = false;

	public boolean sudah_evaluasi_kualifikasi = false;

	public Integer is_pemenang_klarifikasi;
	
	/**
	 * daftar kuantitas dan harga
	 * format : json 
	 * json model models.agency.Rincian_hps
	 */
	public String psr_dkh;
	
	public String psr_identitas; // data identitas , model : rekanan.Rekanan
	
	public String psr_pemilik; // data pemilik , model : rekanan.Pemilik
	
	public String psr_pengurus; // data pengurus , model : rekanan.Pengurus

	// Joint Venture
	public String psr_jv_pq;
	public String psr_jv_bid;
	public String psr_jv_bid_pokja;
	public Integer psr_jv_bid_confirm = 0;
	public Date psr_jv_bid_auto_confirm;

	@Transient
	private LelangDp lelang_seleksi;
	@Transient
	private Rekanan rekanan;
	@Transient
	public DokPenawaranDp dokPenawaranHarga;
	@Transient
	public DokPenawaranDp dokPenawaranTeknis;
	@Transient
	public List<DokPenawaranDp> dokPenawarans;
	@Transient
	private NilaiEvaluasiDp nilaiPembuktian;

    public LelangDp getLelang_seleksi() {
    	if(lelang_seleksi == null)
    		lelang_seleksi = LelangDp.findById(lls_id);
		return lelang_seleksi;
	}
    
    public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}
	
	/**
	 * informasi nama peserta lelang
	 * @return
	 */
	public String getNamaPeserta() {
		return Query.find("select rkn_nama from rekanan where rkn_id=?", String.class, rkn_id).first();
	}

	@Override
	public Long getParticipantId() {
		return psr_id;
	}

	@Override
	public void setDokPenawaranHarga(DokPenawaranDp model) {
		this.dokPenawaranHarga = model;
	}

	@Override
	public void setDokPenawaranTekis(DokPenawaranDp model) {
		this.dokPenawaranTeknis = model;
	}

	@Override
	public void setDokPenawarans(List<DokPenawaranDp> list) {
		this.dokPenawarans = list;
	}

	public static PesertaDp findBy(Long lelangId, Long rekananId) {
		return find("lls_id=? and rkn_id=?", lelangId, rekananId).first();
	}

	public boolean isShortlisted() {
		EvaluasiDp evaShortlist = EvaluasiDp.findLastBy(lls_id, EvaluasiDp.JenisEvaluasi.SHORTLIST, EvaluasiDp.StatusEvaluasi.SELESAI);
		if(evaShortlist != null) {
			if(evaShortlist.getLelang_seleksi().getMetode().isCqs()) {
				NilaiEvaluasiDp highestScore = NilaiEvaluasiDp.getHighestScore(evaShortlist);
				NilaiEvaluasiDp pesertaEval = NilaiEvaluasiDp.findBy(evaShortlist.eva_id, psr_id);
				if (highestScore != null && pesertaEval != null) {
					return highestScore.psr_id.equals(pesertaEval.psr_id) && pesertaEval.isLulus();
				}
				return false;
			}

			// else
			NilaiEvaluasiDp nilai_evaluasi = NilaiEvaluasiDp.findBy(evaShortlist.eva_id, psr_id);
			return nilai_evaluasi != null && nilai_evaluasi.isLulus();
		}
		return false;
	}

	public boolean isLulusTeknis() {
		EvaluasiDp evaTeknis = EvaluasiDp.findLastBy(lls_id, EvaluasiDp.JenisEvaluasi.EVALUASI_TEKNIS, EvaluasiDp.StatusEvaluasi.SELESAI);
		if(evaTeknis != null) {
			if(evaTeknis.getLelang_seleksi().getMetode().isQbs()) {
				NilaiEvaluasiDp highestScore = NilaiEvaluasiDp.getHighestScore(evaTeknis);
				NilaiEvaluasiDp pesertaEval = NilaiEvaluasiDp.findBy(evaTeknis.eva_id, psr_id);
				if (highestScore != null && pesertaEval != null) {
					return highestScore.psr_id.equals(pesertaEval.psr_id) && pesertaEval.isLulus();
				}
				return false;
			}

			// else
			NilaiEvaluasiDp nilai_evaluasi = NilaiEvaluasiDp.findBy(evaTeknis.eva_id, psr_id);
			return nilai_evaluasi != null && nilai_evaluasi.isLulus();
		}
		return false;
	}

	public boolean isLulusAkhir() {
		EvaluasiDp evaAkhir = EvaluasiDp.findLastBy(lls_id, EvaluasiDp.JenisEvaluasi.EVALUASI_AKHIR, EvaluasiDp.StatusEvaluasi.SELESAI);
		if(evaAkhir != null) {
			if(evaAkhir.getLelang_seleksi().getMetode().isQbs()) {
				NilaiEvaluasiDp highestScore = NilaiEvaluasiDp.getHighestScore(evaAkhir);
				NilaiEvaluasiDp pesertaEval = NilaiEvaluasiDp.findBy(evaAkhir.eva_id, psr_id);
				if (highestScore != null && pesertaEval != null) {
					return highestScore.psr_id.equals(pesertaEval.psr_id) && pesertaEval.isLulus();
				}
				return false;
			}

			// else
			NilaiEvaluasiDp nilai_evaluasi = NilaiEvaluasiDp.findBy(evaAkhir.eva_id, psr_id);
			return nilai_evaluasi != null && nilai_evaluasi.isLulus();
		}
		return false;
	}

	public static PesertaDp findByRekananAndLelang(Long rekananId, Long lelangId){
		return find("rkn_id=? and lls_id=?", rekananId, lelangId).first();
	}

	/**
	 * Rekanan mendaftar lelang
	 * @param lelangId
	 * @param rekananId
	 */
	public static void daftarLelang(Long lelangId, Long rekananId)	{
		PesertaDp peserta = new PesertaDp();
		peserta.lls_id = lelangId;
		peserta.rkn_id = rekananId;
		peserta.psr_tanggal = BasicCtr.newDate();
		peserta.psr_black_list = Long.valueOf(0);
		peserta.is_pemenang = Integer.valueOf(0);
		peserta.is_pemenang_verif = Integer.valueOf(0);
		peserta.sudah_verifikasi_sikap = Integer.valueOf(0);
		peserta.save();
	}

	public static List<PesertaDp> findBylelang(Long lelangId) {
		return find("lls_id=? order by psr_harga", lelangId).fetch();
	}

	public static List<PesertaDp> findWithPenawaran(Long lelangId) {
		return find("lls_id=? and psr_id in (select distinct psr_id from devpartner.dok_penawaran_dp where dok_jenis in (1,2,3))",lelangId).fetch();
	}

	@Transient
	public boolean hasPenawaran() {
		return getLelang_seleksi().isSatuFile() ? getFileAdminTeknisHarga() != null : getFileTeknis() != null && getFileHarga() != null;
	}

	@Transient
	public DokPenawaranDp getFileTeknis() {
		return DokPenawaranDp.findPenawaranPeserta(psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
	}

	@Transient
	public DokPenawaranDp getFileHarga() {
		return DokPenawaranDp.findPenawaranPeserta(psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_HARGA);
	}

	@Transient
	public DokPenawaranDp getFileAdminTeknisHarga() {
		return DokPenawaranDp.findPenawaranPeserta(psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA);
	}

	@Transient
	public DokPenawaranDp getFileEoi() {
		return DokPenawaranDp.findPenawaranPeserta(psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_EOI);
	}

	@Transient
	public NilaiEvaluasiDp getShortlist() {
		EvaluasiDp evaluasi = EvaluasiDp.findShortlist(lls_id);
		if(evaluasi == null)
			return null;
		return NilaiEvaluasiDp.findBy(evaluasi.eva_id, psr_id);
	}

	@Transient
	public NilaiEvaluasiDp getTeknis() {
		EvaluasiDp evaluasi = EvaluasiDp.findTeknis(lls_id);
		if(evaluasi == null)
			return null;
		return NilaiEvaluasiDp.findBy(evaluasi.eva_id, psr_id);
	}

	@Transient
	public NilaiEvaluasiDp getHarga() {
		EvaluasiDp evaluasi = EvaluasiDp.findHarga(lls_id);
		if(evaluasi == null)
			return null;
		return NilaiEvaluasiDp.findBy(evaluasi.eva_id, psr_id);
	}

	@Transient
	public NilaiEvaluasiDp getAkhir() {
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lls_id);
		if(evaluasi == null)
			return null;
		return NilaiEvaluasiDp.findBy(evaluasi.eva_id, psr_id);
	}

	@Transient
	public boolean isConfirmedBid() {
		return psr_jv_bid_confirm != null && psr_jv_bid_confirm == 1;
	}

	@Transient
	public boolean isWaitingConfirmationBid() {
		return !isConfirmedBid() && psr_jv_bid_pokja != null;
	}

	@Transactional
	public static void autoConfirmJv() {
		List<PesertaDp> candidates = PesertaDp.find("psr_jv_bid_confirm = 0 and psr_jv_bid_auto_confirm <= ?", DateUtil.newDate()).fetch();
		for (PesertaDp peserta: candidates) {
			peserta.psr_jv_bid_confirm = 1;
			peserta.psr_jv_bid = peserta.psr_jv_bid_pokja;
			peserta.psr_jv_bid_pokja = null;
			peserta.psr_jv_bid_auto_confirm = null;
			peserta.save();
		}
//		StringBuilder sql = new StringBuilder();
//		sql
//				.append("update devpartner.peserta_dp set psr_jv_bid = psr_jv_bid_pokja, psr_jv_bid_pokja = null, psr_jv_bid_confirm = 1,psr_jv_bid_auto_confirm = null ")
//				.append("where psr_jv_bid_confirm = 0 and psr_jv_bid_auto_confirm <= ?");
//		Query.update(sql.toString(), DateUtil.newDate());
	}
}
