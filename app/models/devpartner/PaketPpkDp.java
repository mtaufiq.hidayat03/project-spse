package models.devpartner;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

/**
 * @author idoej
 */
@Table(name = "paket_ppk_dp", schema = "devpartner")
public class PaketPpkDp extends BaseModel {

    @Id
    public Long pkt_id;
    @Id
    public Long ppk_id;

    public Integer ppk_jabatan;

    public PaketPpkDp(Long pktId, Long ppkid){
        this.pkt_id = pktId;
        this.ppk_id = ppkid;
        this.ppk_jabatan = 0;
    }

    public boolean isItMyPaket(Long ppkId) {
        return ppk_id.equals(ppkId);
    }

    public static PaketPpkDp findByPaketAndPpk(Long pkt_id, Long ppk_id){
        return PaketPpkDp.find("pkt_id = ? and ppk_id = ?",pkt_id,ppk_id).first();
    }

    public static List<PaketPpkDp> findByPaket(Long pkt_id){
        return PaketPpkDp.find("pkt_id = ? order by auditupdate desc",pkt_id).fetch();
    }

    public String getNamaPpk(){
        return Query.find("SELECT peg_nama FROM pegawai p, ppk WHERE p.peg_id=ppk.peg_id AND ppk_id=?",String.class, ppk_id).first();
    }
	 
    public void deleteOldPpk(){
        PaketPpkDp.delete("pkt_id = ? and ppk_id <> ?",pkt_id,ppk_id);
    }
    
    public void deleteOldPpkNew(Long old_ppk_id){
        PaketPpkDp.delete("pkt_id = ? and ppk_id = ?",pkt_id,old_ppk_id);
    }

}
