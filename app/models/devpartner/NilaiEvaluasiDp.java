package models.devpartner;

import ext.DecimalBinder;
import ext.RupiahBinder;
import models.jcommon.db.base.BaseModel;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.List;


@Table(name="nilai_evaluasi_dp", schema = "devpartner")
public class NilaiEvaluasiDp extends BaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusNilaiEvaluasi {
		TDK_LULUS ("Tidak Lulus"), LULUS("Lulus"), LULUS_TDK_SHORTLIST("Lulus Bukan Daftar Pendek");
		
		public final String label;
		
		private StatusNilaiEvaluasi(String label) {
			this.label = label;
		}
		
		public boolean isLulus() {
			return this == LULUS;
		}

	}
	@Id(sequence="devpartner.seq_nilai_evaluasi_dp", function="nextsequence")
	public Long nev_id;

	@As(binder=RupiahBinder.class)
	public Double nev_harga;

	@As(binder=RupiahBinder.class)
	public Double nev_harga_terkoreksi;

	@As(binder=RupiahBinder.class)
	public Double nev_harga_negosiasi;

	@As(binder=DecimalBinder.class)
	public Double nev_skor;

	public Integer nev_urutan;

	public String nev_uraian;
	@Required
	public StatusNilaiEvaluasi nev_lulus;

	public Long nev_id_attachment;

	//relasi ke Evaluasi
	public Long eva_id;

	//relasi ke Peserta
	public Long psr_id;

	@Required
	public Integer nev_gagal = 0;
	@Required
	public StatusNilaiEvaluasi nev_combined;
	@Required
	public StatusNilaiEvaluasi nev_rank;
	
	@Transient
	private PesertaDp peserta;
	@Transient
	private EvaluasiDp evaluasi;
	
	public PesertaDp getPeserta() {
		if(peserta == null)
			peserta = PesertaDp.findById(psr_id);
		return peserta;
	}	

	public EvaluasiDp getEvaluasi() {
		if(evaluasi == null)
			evaluasi = EvaluasiDp.findById(eva_id);
		return evaluasi;
	}

	@Transient
	public boolean isLulus() {
		if (nev_lulus != null) {
			return nev_lulus.isLulus();
		}
		return false;
	}

	@Transient
	public boolean isLulusCombined() {
		if (nev_combined != null) {
			return nev_combined.isLulus();
		}
		return false;
	}

	@Transient
	public boolean isLulusRank() {
		if (nev_rank != null) {
			return nev_rank.isLulus();
		}
		return false;
	}

	@Transient
	public boolean isGagal() {
		if (nev_gagal != null) {
			return nev_gagal == 1;
		}
		return false;
	}

	public static List<NilaiEvaluasiDp> findBy(Long evaluasiId) {
		return find("eva_id= ? ORDER BY psr_id ASC, nev_urutan ASC", evaluasiId).fetch();
	}

	public static NilaiEvaluasiDp findBy(Long evaluasiId, Long pesertaId) {
		return find("eva_id=? and psr_id=?", evaluasiId, pesertaId).first();
	}

	public static NilaiEvaluasiDp getHighestScore(EvaluasiDp evaluasi) {
		return find("eva_id = ? AND nev_lulus = 1 AND (nev_gagal = 0 or nev_gagal is null) order by nev_skor desc", evaluasi.eva_id).first();
	}

	public static int countLulus(Long lelangId,	EvaluasiDp.JenisEvaluasi jenis) {
		return (int) count("eva_id IN (SELECT eva_id FROM devpartner.evaluasi_dp WHERE lls_id=? AND eva_jenis = ? ORDER BY eva_versi DESC limit 1) and nev_lulus = ?", lelangId, jenis, NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS);
	}

	public static int countLulus(Long evaluasiId) {
		return (int)count("eva_id=? and nev_lulus=?", evaluasiId, NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS.ordinal());
	}

	public static NilaiEvaluasiDp findNCreateBy(Long evaluasiId, PesertaDp peserta, EvaluasiDp.JenisEvaluasi jenis) {
		NilaiEvaluasiDp nilai = findBy(evaluasiId, peserta.psr_id);
		if(nilai == null) {
			nilai = new NilaiEvaluasiDp();
			nilai.eva_id = evaluasiId;
			nilai.psr_id = peserta.psr_id;
			if(jenis.isHarga() || jenis.isAkhir()) {
				nilai.nev_harga = peserta.psr_harga;
				nilai.nev_harga_terkoreksi = peserta.psr_harga_terkoreksi;
			}
			nilai.save();
		}
		return nilai;
	}

	public static List<NilaiEvaluasiDp> findWithUrutSkor(Long evaluasiId) {
		return find("eva_id= ? ORDER BY nev_skor DESC, nev_urutan ASC", evaluasiId).fetch();
	}

	public static NilaiEvaluasiDp findSkorTertinggi(Long evaluasiId) {
		return find("eva_id= ? and nev_lulus = 1 and (nev_gagal = 0 or nev_gagal is null) ORDER BY nev_skor DESC, nev_urutan ASC", evaluasiId).first();
	}

	public static List<NilaiEvaluasiDp> findWithUrutRank(Long evaluasiId) {
		return find("eva_id= ? ORDER BY nev_urutan ASC", evaluasiId).fetch();
	}

	public static NilaiEvaluasiDp findRankTertinggi(Long evaluasiId) {
		return find("eva_id= ? and nev_rank = 1 and (nev_gagal = 0 or nev_gagal is null) ORDER BY nev_urutan asc", evaluasiId).first();
	}

	public boolean isHasOtherCandidate() {
		if (getEvaluasi().getLelang_seleksi().getMetode().evaluasi.isCqs()) {
			EvaluasiDp shortlist = getEvaluasi().getLelang_seleksi().getShortlist();
			return count("eva_id= ? and nev_lulus = 1 and (nev_gagal = 0 or nev_gagal is null) and psr_id != ?", shortlist.eva_id, psr_id) > 0;
		} else if (getEvaluasi().getLelang_seleksi().getMetode().evaluasi.isQbs()) {
			EvaluasiDp teknis = getEvaluasi().getLelang_seleksi().getTeknis();
			return count("eva_id= ? and nev_lulus = 1 and (nev_gagal = 0 or nev_gagal is null) and psr_id != ?", teknis.eva_id, psr_id) > 0;
		} else {
			return count("eva_id= ? and nev_rank = 1 and (nev_gagal = 0 or nev_gagal is null) and psr_id != ?", eva_id, psr_id) > 0;
		}
	}
}
