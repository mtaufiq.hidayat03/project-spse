package models.devpartner;

import models.agency.Anggota_panitia;
import models.agency.Panitia;
import models.common.Active_user;
import models.common.StatusLelang;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name="persetujuan_dp", schema = "devpartner")
public class PersetujuanDp extends BaseModel {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisPersetujuan {
		PENGUMUMAN_LELANG(0,"pengumuman") ,
		PEMENANG_LELANG(1,"pemenang"),
		PEMENANG_PRAKUALIFIKASI(2,"pemenang_pra"),
		PEMENANG_TEKNIS(3,"pemenang_teknis"),
		BATAL_LELANG(4,"tutup"),
		ULANG_LELANG(5,"ulang");

		public final Integer id;
		public final String key;

		JenisPersetujuan(int id, String key){
			this.id = id;
			this.key = key;
		}

		public boolean isBatal(){
			return this == BATAL_LELANG;
		}

		public Integer getId(){
			return id;
		}

		public String getKey(){
			return key;
		}

		public static JenisPersetujuan getByKey(String key){
			for(JenisPersetujuan e : values()) {
				if(e.key.equals(key)) return e;
			}

			return null;
		}
	}

	@Enumerated(EnumType.ORDINAL)
	public enum StatusPersetujuan {
		BELUM_SETUJU, SETUJU, TIDAK_SETUJU;

		public boolean isSetuju() {
			return this == SETUJU;
		}

		public boolean isTidakSetuju() {
			return this == TIDAK_SETUJU;
		}

		public boolean isBelumSetuju() {
			return this == BELUM_SETUJU;
		}
	}


	@Id(sequence="devpartner.seq_persetujuan_dp", function="nextsequence")
	public Long pst_id;

	//relasi ke lelang_selesai
	public Long lls_id;

	//relasi ke Pegawai
	public Long peg_id;

	/*
	 * jenis persetujuan memakai enum JenisPersetujuan
	 * 0 : pengumuman lelang
	 * 1 : pengumuman pemenang
	 */
	public JenisPersetujuan pst_jenis;

	public Date pst_tgl_setuju;

	public StatusPersetujuan pst_status;

	public String pst_alasan;

	public static class PersetujuanLelang {
		public Long pst_id;
		//relasi ke lelang_selesai
		public Long lls_id;
		public JenisPersetujuan pst_jenis;
		public Date pst_tgl_setuju;
		public StatusPersetujuan pst_status;
		public String pst_alasan;
		public String peg_nama;
		public String agp_jabatan;

		public String getAlasan() {
			if(StringUtils.isEmpty(pst_alasan))
				return "";
			return pst_alasan;
		}

		public String getJabatan() {
			if (agp_jabatan.equals("K")) {
				return Messages.get("lelang.ketua");
			} else if (agp_jabatan.equals("W")) {
				return Messages.get("lelang.wakil");
			} else if (agp_jabatan.equals("S")) {
				return Messages.get("lelang.sekretaris");
			} else {
				return Messages.get("lelang.anggota");
			}
		}

		public static List<PersetujuanLelang> findList(Long lelangId, JenisPersetujuan jenis) {
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama " +
					"FROM devpartner.persetujuan_dp p JOIN pegawai peg on p.peg_id = peg.peg_id WHERE lls_id=? AND pst_jenis=?",
					PersetujuanLelang.class, lelangId,jenis).fetch();
		}

		public static List<PersetujuanLelang> findBatalList(Long lelangId) {
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, peg_nama " +
					"FROM devpartner.persetujuan_dp p JOIN pegawai peg on p.peg_id = peg.peg_id WHERE lls_id=? AND pst_jenis in (?,?)",
					PersetujuanLelang.class, lelangId, JenisPersetujuan.BATAL_LELANG, JenisPersetujuan.ULANG_LELANG).fetch();
		}

		public static List<PersetujuanLelang> getPersetujuanLelangForSummary(Long lelangId){
			return Query.find("SELECT pst_id, lls_id, pst_jenis, pst_tgl_setuju, pst_status, pst_alasan, agp_jabatan, peg_nama FROM devpartner.persetujuan_dp p , (SELECT peg.peg_id,peg_nama,agp_jabatan FROM devpartner.lelang_dp l LEFT JOIN devpartner.paket_dp p ON l.pkt_id=p.pkt_id LEFT JOIN panitia pn ON pn.pnt_id=p.pnt_id "
					+ "LEFT JOIN anggota_panitia ap ON ap.pnt_id=pn.pnt_id LEFT JOIN pegawai peg ON peg.peg_id=ap.peg_id  WHERE lls_id=? AND ap.peg_id=peg.peg_id) a "
					+ "WHERE p.peg_id=a.peg_id AND lls_id=? AND pst_jenis=? and pst_tgl_setuju is not null", PersetujuanLelang.class, lelangId, lelangId, JenisPersetujuan.PENGUMUMAN_LELANG).fetch();
		}
	}

	public String getAlasan() {
		if(StringUtils.isEmpty(pst_alasan))
			return "";
		return pst_alasan;
	}

	public static boolean isSedangPersetujuan(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lelangId, PersetujuanDp.StatusPersetujuan.SETUJU, PersetujuanDp.StatusPersetujuan.TIDAK_SETUJU, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG);
		StatusLelang statusLelang = LelangDp.findStatusLelang(lelangId);
		return count.intValue() > 0 && statusLelang.isDraft();
	}

	public boolean isSedangPersetujuan() {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lls_id, PersetujuanDp.StatusPersetujuan.SETUJU, PersetujuanDp.StatusPersetujuan.TIDAK_SETUJU, pst_jenis);
		return count.intValue() > 0;
	}

	public static boolean isSedangPersetujuanPemenang(Long lelangId) {
		Long count = count("lls_id=? and (pst_status=? or pst_status=?) and pst_jenis=?", lelangId, PersetujuanDp.StatusPersetujuan.SETUJU, PersetujuanDp.StatusPersetujuan.TIDAK_SETUJU, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG);
		return count.intValue() > 0;
	}

	/**
	 * collective collegial berdasarkan perpres
	 * harus terpenuhi 50% + 1
	 *
	 * @param lelangId
	 * @param jenis
	 * @return
	 */
	public static boolean isApprove(Long lelangId, PersetujuanDp.JenisPersetujuan jenis) {
		Long count = PersetujuanDp.count("lls_id=? and pst_jenis=?", lelangId, jenis);
		Long countSetuju = PersetujuanDp.count("lls_id=? and pst_status=? and pst_jenis=?", lelangId, PersetujuanDp.StatusPersetujuan.SETUJU, jenis);
		int syaratSetuju = (count.intValue() / 2) + 1; // syarat sesuai aturan perpres (setengah + 1)
		return countSetuju >= syaratSetuju;
	}

	public boolean isApprove(){
		return PersetujuanDp.isApprove(lls_id,pst_jenis);
	}

	public static void deleteAllByLelangAndJenisPemenang(Long llsId){
		delete("lls_id = ? and pst_jenis = ?",llsId, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG);
	}

	public static PersetujuanDp findByPegawaiLelangAndJenis(Long lls_id, PersetujuanDp.JenisPersetujuan jenis){
		return PersetujuanDp.find("peg_id=? and lls_id=? and pst_jenis=?", Active_user.current().pegawaiId, lls_id, jenis).first();
	}

	public void deleteAllByLelangAndJenis(){
		delete("lls_id = ? and pst_jenis = ?",lls_id,pst_jenis);
	}

	public static void deleteAllByLelangAndJenisPengumuman(Long llsId){
		delete("lls_id = ? and pst_jenis = ?",llsId, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG);
	}

	public static PersetujuanDp findByPegawaiPengumuman(Long pegawaiId, Long lelangId) {
		return find("peg_id=? and lls_id=? and pst_jenis=?", pegawaiId, lelangId, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG).first();
	}

	public static void checkPersetujuanBatalLelang(Long lelangId, PersetujuanDp.JenisPersetujuan jenis) {
		List<PersetujuanDp> persetujuanList = find("lls_id=? and (pst_jenis=? or pst_jenis=?)",
				lelangId, PersetujuanDp.JenisPersetujuan.BATAL_LELANG, PersetujuanDp.JenisPersetujuan.ULANG_LELANG).fetch();

		Panitia panitia = Panitia.findByLelangDp(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanDp persetujuan = null;
			persetujuanList = new ArrayList<>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanDp.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, PersetujuanDp.JenisPersetujuan.BATAL_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanDp();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = PersetujuanDp.StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = jenis;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = str_anggota.toString();
			sql_append = sql_append.replace(",)", ")");
			PersetujuanDp.delete("lls_id=? and pst_jenis=? and "+sql_append, lelangId, PersetujuanDp.JenisPersetujuan.BATAL_LELANG);
		}
	}

	public static void checkPersetujuanPemenang(Long lelangId) {
		List<PersetujuanDp> persetujuanList = find("lls_id=? and pst_jenis=?", lelangId, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG).fetch();
		Panitia panitia = Panitia.findByLelangDp(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanDp persetujuan = null;
			persetujuanList = new ArrayList<PersetujuanDp>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanDp.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanDp();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = PersetujuanDp.StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = anggotaList.isEmpty() ? "" : " and " + str_anggota;
			sql_append = sql_append.replace(",)", ")");
			PersetujuanDp.delete("lls_id=? and pst_jenis=?"+sql_append, lelangId, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG);
		}
	}

	public static void checkPersetujuanLelang(Long lelangId) {
		List<PersetujuanDp> persetujuanList = find("lls_id=? and pst_jenis=?", lelangId, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG).fetch();
		Panitia panitia = Panitia.findByLelangDp(lelangId);
		Long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id);
		if(!CommonUtil.isEmpty(persetujuanList) && jumlah == persetujuanList.size())
			return ;
		else {
			List<Anggota_panitia> anggotaList = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
			PersetujuanDp persetujuan = null;
			persetujuanList = new ArrayList<PersetujuanDp>(anggotaList.size());
			StringBuilder str_anggota = new StringBuilder("pst_id not in (");
			for(Anggota_panitia anggota:anggotaList) {
				persetujuan = PersetujuanDp.find("lls_id=? and peg_id=? and pst_jenis=?", lelangId, anggota.peg_id, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG).first();
				if(persetujuan == null) {
					persetujuan = new PersetujuanDp();
					persetujuan.lls_id = lelangId;
					persetujuan.peg_id = anggota.peg_id;
					persetujuan.pst_status = PersetujuanDp.StatusPersetujuan.BELUM_SETUJU;
					persetujuan.pst_jenis = PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG;
					persetujuan.save();
				}
				persetujuanList.add(persetujuan);
				str_anggota.append('\'').append(persetujuan.pst_id).append("',");
			}
			str_anggota.append(')');
			String sql_append = str_anggota.toString();
			sql_append = sql_append.replace(",)", ")");
			PersetujuanDp.delete("lls_id=? and pst_jenis=? and "+sql_append, lelangId, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG);
		}
	}

	public static PersetujuanDp findByPegawaiPenetapanAkhir(Long pegawaiId, Long lelangId) {
		return find("peg_id=? and lls_id=? and pst_jenis=?", pegawaiId, lelangId, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG).first();
	}
}
