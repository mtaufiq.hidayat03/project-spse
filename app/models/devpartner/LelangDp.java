package models.devpartner;

import models.agency.Panitia;
import models.agency.Pegawai;
import models.common.*;
import models.devpartner.common.*;
import models.devpartner.contracts.StatusTenderDp;
import models.devpartner.panel.PanelLelang;
import models.jcommon.db.base.BaseModel;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Table(name="lelang_dp", schema = "devpartner")
public class LelangDp extends BaseModel implements StatusTenderDp {
	@Enumerated(EnumType.ORDINAL)
	public enum JenisLelang {
		LELANG_NON_ITEMIZE(Messages.get("lelang.1_pemenang")), LELANG_ITEMIZE(Messages.get("lelang.lebih_dari_1_pemenang")+" <i>(Itemized)</i>");

		public final String label;
		JenisLelang(String label) {
			this.label = label;
		}
	}

	@Id(sequence="devpartner.seq_lelang_dp", function="nextsequence")
	public Long lls_id;

	public Double lls_passinggrade;

	public Integer lls_versi_lelang;

	public String lls_keterangan;

	public Date lls_dibuat_tanggal;

	public String lls_diulang_karena;

	public String lls_ditutup_karena;
	
	public String lls_dibuka_karena;

	public Integer lls_terverifikasi;

	public Date lls_tgl_setuju;

	@Required
	public StatusLelang lls_status;

	public Long lls_wf_id;

	@Required
	public Integer mtd_id;

    public Integer lls_kontrak_pembayaran;

    public Integer lls_kontrak_tahun;

    public Integer lls_kontrak_sbd;
    /**
     * kontrak berdasarkan jenis Pekerjaan
     * since 4.0
     */
    public Integer lls_kontrak_pekerjaan;

    public Double lls_bobot_teknis;

    public Double lls_bobot_biaya;

	// flag kirim pengumuman pemenang prakualifikasi sudah dilakukan atau belom.
	// "true" = sudah dikirim ,  "false" = belum dikirim
	public boolean kirim_pengumuman_pemenang_pra = false;

    // flag kirim pengumuman pemenang sudah dilakukan atau belom.
	// "true" = sudah dikirim ,  "false" = belum dikirim
    public boolean kirim_pengumuman_pemenang = false;

    //relasi ke Paket
	public Long pkt_id;
   
	@Transient
	private PaketDp paket;
	@Transient
	private DokLelangDp dokumenLelang;
	@Transient
	private EvaluasiDp evaluasiKualifikasi;
	@Transient
	private EvaluasiDp evaluasiTeknis;
	@Transient
	private EvaluasiDp evaluasiHarga;
	@Transient
	private EvaluasiDp evaluasiAkhir;
	@Transient
	private TahapDpNow tahapNow;
	@Transient
	private TahapDpStarted tahapStarted;
	@Transient
	private TahapLelangDp tahapLelang;

	// untuk simpan sesi pelatihan. 0 : default prod tidak boleh diisi yang lain
	public int lls_sesi;

	// untuk menandakan bahwa lelang pernah pemasukan penawaran ulang
	public Integer lls_penawaran_ulang;

	public Integer allow_reschedule;
	public Integer sudah_pembukaan=0;
	public Date tanggal_pembukaan;
	public Long peg_pembukaan;

	public PaketDp getPaket() {
		if(paket == null)
			paket = PaketDp.findById(pkt_id);
		return paket;
	}

	public DokLelangDp getDokumenLelang() {
		if (dokumenLelang == null && lls_id != null) {
			dokumenLelang = DokLelangDp.findBy(lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_LELANG);
		}
		return dokumenLelang;
	}

	public EvaluasiDp getShortlist(){
		if(evaluasiKualifikasi == null){
			evaluasiKualifikasi = EvaluasiDp.findShortlist(lls_id);
		}
		return evaluasiKualifikasi;
	}

	public EvaluasiDp getTeknis(){
		if(evaluasiTeknis == null){
			evaluasiTeknis = EvaluasiDp.findTeknis(lls_id);
		}
		return evaluasiTeknis;
	}

	public EvaluasiDp getHarga(){
		if(evaluasiHarga == null){
			evaluasiHarga = EvaluasiDp.findHarga(lls_id);
		}
		return evaluasiHarga;
	}


	public EvaluasiDp getEvaluasiAkhir(){
		if(evaluasiAkhir == null){
			evaluasiAkhir = EvaluasiDp.findAkhir(lls_id);
		}
		return evaluasiAkhir;
	}

	@Transient
	public JenisKontrakDp getKontrakPembayaran()
	{

		return JenisKontrakDp.findById(lls_kontrak_pembayaran);
	}

	@Transient
	public KontrakTahunDp getKontrakTahun()
	{

		return KontrakTahunDp.findById(lls_kontrak_tahun);
	}

	@Transient
	public SumberDanaDp getSumberDana()
	{

		return SumberDanaDp.findById(lls_kontrak_sbd);
	}

	/**
	 * membuat lelang baru dengan nilai-nilai default
	 * event ini terjadi saat pembuatan paket
	 * @param paket
	 */
	public static LelangDp buatLelangBaru(PaketDp paket) {
		//long jumlahLelang = Query.count("SELECT count(lls_id) FROM lelang_seleksi WHERE pkt_id=?", paket.pkt_id);
		LelangDp lls = LelangDp.find("pkt_id =?", paket.pkt_id).first();
		if (lls == null) { // berarti belum pernah ada lelang yang dibuat
			lls = new LelangDp();
			lls.pkt_id = paket.pkt_id;
			if(paket.kgr_id.isKonsultansi()) {
				lls.mtd_id = MetodeDp.RCS_2_QCBS.id;
			} else {
				lls.mtd_id = MetodeDp.POST_QUAL.id;
			}
			lls.lls_versi_lelang = Integer.valueOf(1);
			lls.lls_dibuat_tanggal = controllers.BasicCtr.newDate();
			lls.lls_terverifikasi = Integer.valueOf(1);
			lls.lls_status = StatusLelang.DRAFT;
			lls.setKontrakPembayaran(JenisKontrak.LUMP_SUM);
			lls.save();
		}
		return lls;
	}

	@Transient
	public void setKontrakPembayaran(JenisKontrak kontrak)
	{
		this.lls_kontrak_pembayaran = kontrak.id;
	}

	public static LelangDp findByPaketNewestVersi(Long paketId) {
		return find("pkt_id=? order by lls_versi_lelang DESC", paketId).first();
	}

	public static StatusLelang findStatusLelang(Long lelangId) {
		return Query.find("SELECT lls_status FROM devpartner.lelang_dp WHERE lls_id=?", StatusLelang.class, lelangId).first();
	}

	// cari lelang yg aktif , param paketId
	public static LelangDp findAktifByPaket(Long paketId) {
		return find("pkt_id=? and lls_status='1' order by lls_id DESC", paketId).first();
	}

	public static LelangDp findByPaket(Long paketId) {
		return find("pkt_id=? order by lls_id DESC", paketId).first();
	}

	public Panitia getPanitia(){
		return Query.find("SELECT p.* FROM panitia p LEFT JOIN devpartner.paket_dp pkt ON pkt.pnt_id=p.pnt_id WHERE pkt.pkt_id=?", Panitia.class, pkt_id).first();
	}

	public String getNamaPanitia(){
		return Query.find("SELECT pnt_nama FROM panitia p LEFT JOIN devpartner.paket_dp pkt ON pkt.pnt_id=p.pnt_id WHERE pkt.pkt_id=?", String.class, pkt_id).first();
	}

	public String getNamaPaket() {
		return Query.find("select pkt_nama from devpartner.paket_dp where pkt_id=?", String.class, pkt_id).first();
	}

	@Transient
	public Kategori getKategori() {
		return Query.find("select kgr_id from devpartner.paket_dp where pkt_id=?", Kategori.class, pkt_id).first();
	}

	@Transient
	public MetodeDp getMetode()
	{
		return MetodeDp.findById(mtd_id);
	}

	public String getInfoJadwal() {
		Long jmlThp = JadwalDp.count("lls_id="+lls_id);
		Long tahapKosong = JadwalDp.countJadwalkosong(lls_id);
		if (jmlThp == 0)
			return Messages.get("lelang.baj");
		else if (tahapKosong == 0)
			return Messages.get("lelang.sjst");
		else
			return tahapKosong+ " "+Messages.get("lelang.tbmj");
	}

//	@Transient
//	public boolean isPrakualifikasi()
//	{
//		return getMetode().kualifikasi.isPra();
//	}
//
//	@Transient
//	public boolean isPascakualifikasi()
//	{
//		return getMetode().kualifikasi.isPasca();
//	}

	@Transient
	public boolean isLelangUlang()
	{
		if(lls_versi_lelang == null)
			return false;
		return lls_versi_lelang > 1;
	}

	public boolean isSedangPersetujuan() {
		return PersetujuanDp.isSedangPersetujuan(lls_id);
	}

	public boolean isSedangPersetujuanPemenang() {
		return PersetujuanDp.isSedangPersetujuanPemenang(lls_id);
	}

	public boolean allowBatalPersetujuan(){
		PersetujuanDp persetujuan = PersetujuanDp.findByPegawaiPengumuman(Active_user.current().pegawaiId,lls_id);
		if(persetujuan != null){
			return persetujuan.pst_status.isSetuju() && isSedangPersetujuan() && lls_status.isDraft();
		}
		return false;
	}

	@Override
	public boolean isDitutup() {
		return lls_status.isDitutup();
	}

	@Override
	public boolean isEvaluasiUlang() {
		return EvaluasiDp.count("lls_id=? and eva_versi > 1", lls_id) > 0;
	}

	@Override
	public boolean isKonsultansi() {
		Kategori kategori = getKategori();
		return kategori.isKonsultansi() || kategori.isKonsultansiPerorangan();
	}

	@Override
	public boolean isPenawaranUlang()
	{
		if(lls_penawaran_ulang == null)
			return false;
		return lls_penawaran_ulang == 1;
	}

	@Transient
	public boolean isAllowReschedule() {
		return allow_reschedule != null && allow_reschedule == 1;
	}

	@Transient
	public MetodeEvaluasiDp getEvaluasi()
	{
		return MetodeDp.findById(mtd_id).evaluasi;

	}

	@Transient
	public boolean isSudahPembukaanPenawaran() {
		return tanggal_pembukaan != null;
	}

	@Transient
	public boolean isDataLengkap() {
		boolean complete = lls_kontrak_pembayaran != null && lls_kontrak_tahun != null && lls_kontrak_sbd != null;

		if (getMetode().evaluasi.isQcbs()) {
			complete = complete && lls_bobot_teknis > 0 && lls_bobot_biaya > 0;
		}

		if (getMetode().evaluasi.isNilai()) {
			complete = complete && lls_passinggrade > 0;
		}

		if (getMetode().isPanel()) {
			complete = complete && PanelLelang.findBy(this) != null;
		}

		return complete;
	}

	@Transient
	public boolean isJadwalLengkap() {
		return JadwalDp.count("lls_id="+lls_id) > 0 && JadwalDp.countJadwalkosong(lls_id) == 0;
	}

	public PersetujuanDp findPersetujuan(Long pegawaiId, boolean pemenang) {
		if (pemenang) {
			return PersetujuanDp.find("lls_id=? and peg_id=? and pst_jenis=?", this.lls_id, pegawaiId, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG).first();
		} else {
			return PersetujuanDp.find("lls_id=? and peg_id=? and pst_jenis=?", this.lls_id, pegawaiId, PersetujuanDp.JenisPersetujuan.PENGUMUMAN_LELANG).first();
		}
	}

	private String getJenisDokLabel(DokLelangDp.JenisDokLelang jenis) {
		// first, check whether this lelang combination of method, document type and funding source has specific document name configuration
		// if exists such config, use that, else use default document name
		SpecificDokLelangNameDp config = SpecificDokLelangNameDp.get(getMetode(), getSumberDana(), jenis);
		if (config != null) return Messages.get(config.dokLabel);

		String label = getMetode().getDokLabel(jenis);
		return label == null ? "misconfig bidding method" : Messages.get(label);
	}

	public String getReoiSummaryLabel() {
		return getJenisDokLabel(DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY);
	}

	public String getReoiLabel() {
		return getJenisDokLabel(DokLelangDp.JenisDokLelang.DOKUMEN_REOI);
	}

	public String getDokLelangLabel() {
		return getJenisDokLabel(DokLelangDp.JenisDokLelang.DOKUMEN_LELANG);
	}

	public String getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran jenis) {
		if (jenis.isEoi()) {
			return Messages.get(isKonsultansi() ? jenis.label : "doc.pq");
		}
		return Messages.get(jenis.isAdmTeknisHarga() && getMetode().isEmpanelment() ? "doc.eoi" : jenis.label);
	}

	public String getPenawaranPqLabel() {
		return getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran.PENAWARAN_EOI);
	}

	public String getPenawaranSatuFileLabel() {
		return getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA);
	}

	public String getPenawaranTeknisLabel() {
		return getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
	}

	public String getPenawaranHargaLabel() {
		return getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran.PENAWARAN_HARGA);
	}

	public TahapDpStarted getTahapStarted(){
		if(tahapStarted == null){
			tahapStarted = new TahapDpStarted(this);
		}

		return tahapStarted;
	}

	public TahapDpNow getTahapNow(){
		if(tahapNow == null){
			tahapNow = new TahapDpNow(this);
		}

		return tahapNow;
	}

	public TahapLelangDp getTahapLelang(){
		if(tahapLelang == null){
			tahapLelang = new TahapLelangDp(this);
		}

		return tahapLelang;
	}

	public List<NilaiEvaluasiDp> getPemenangList() {
		EvaluasiDp penetapan = getEvaluasiAkhir();
		if(penetapan != null && penetapan.eva_status.isSelesai()) {
			List<NilaiEvaluasiDp> nev = new ArrayList<>();
			List<NilaiEvaluasiDp> calonPemenang = Query.find("SELECT * FROM devpartner.nilai_evaluasi_dp nev JOIN devpartner.peserta_dp psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id = ? AND psr.is_pemenang = ?",
					NilaiEvaluasiDp.class, penetapan.eva_id, 1).fetch();
			if (calonPemenang != null) {
				nev.addAll(calonPemenang);
			}
			return nev;
		}
		return null;
	}

	public boolean isPreQualification() {
		return getMetode().kualifikasi.isPreQualification();
	}

	public boolean isSatuFile() {
		return getMetode().dokumen.isSatuFile();
	}

	public boolean isDuaFile() {
		return getMetode().dokumen.isDuaFile();
	}

	public boolean isNilai() {
		return getMetode().evaluasi.isNilai();
	}

	public boolean isNilaiHarga() {
		return getMetode().evaluasi.isNilaiHarga();
	}

	public boolean isManualTechnicalPass() {
		return !getMetode().evaluasi.isNilai() || lls_passinggrade == 0;
	}

	@Transient
	public int getJumlahPeserta() {
		return (int) PesertaDp.count("lls_id=?", lls_id);
	}

	@Transient
	public long getJumlahPenawar() {
		if (getMetode().dokumen.isDuaFile()) {
			String sql = "select count(psr_id) from (select psr_id, " +
					"(select MAX(dok_id) from devpartner.dok_penawaran_dp where psr_id=p.psr_id and dok_jenis=1 and dok_disclaim=1) as teknis , " +
					"(select MAX(dok_id) from devpartner.dok_penawaran_dp where psr_id=p.psr_id and dok_jenis=2 and dok_disclaim=1) as harga from peserta p where p.lls_id=?" +
					") penawaran_peserta where teknis is not NULL and harga is not null";
			return Query.count(sql, lls_id);
		}
		String jenisDok = "dok_jenis in ('" + DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id + "','"+ DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id + "')";
		return DokPenawaranDp.count("psr_id in (select psr_id from devpartner.peserta_dp where lls_id=?) AND dok_disclaim='1' AND " + jenisDok,lls_id);
	}

	public List<PesertaDp> getPesertaList() {
		return PesertaDp.findBylelang(lls_id);
	}

	@Transient
	public Pegawai getPegawaiPembukaan() {
		return Pegawai.findByPegId(peg_pembukaan);
	}

	@Transient
	public boolean hasPemenang() {
		return PesertaDp.count("lls_id=? and is_pemenang = 1", lls_id) > 0;
	}

	/**
	 * flow proses tutup lelang
	 * @param alasan
	 * @throws Exception
	 */
	public void tutupLelang(String alasan) throws Exception
	{
		lls_status = StatusLelang.DITUTUP;
		lls_ditutup_karena = alasan;
		save();
		MailDpDao.kirimPengumumanTutupLelang(this, alasan);
	}
}
