package models.devpartner.contracts;

import models.sso.common.Kabupaten;

/**
 * @author idoej
 */
public interface PaketLokasiDpContract {

    boolean isPklIdExist();
    Long getPrpId();
    Kabupaten getKabupaten();

}
