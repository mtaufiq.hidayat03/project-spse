package models.devpartner.contracts;

import models.devpartner.DokPenawaranDp;

import java.util.List;

/**
 * @author HanusaCloud on 3/16/2018
 */
public interface ParticipantDpContract {

    Long getParticipantId();

    void setDokPenawaranHarga(DokPenawaranDp model);
    void setDokPenawaranTekis(DokPenawaranDp model);
    void setDokPenawarans(List<DokPenawaranDp> list);

    default void withDokPenawarans() {
        if (getParticipantId() != null) {
            setDokPenawarans(DokPenawaranDp.findPenawaranPesertaList(getParticipantId()));
        }
    }

    default void withDokPenawaranHarga() {
        if (getParticipantId() != null) {
            setDokPenawaranHarga(DokPenawaranDp.findPenawaranPeserta(getParticipantId(),
                    DokPenawaranDp.JenisDokPenawaran.PENAWARAN_HARGA));
        }
    }

    default void withDokPenawaranTeknis() {
        if (getParticipantId() != null) {
            setDokPenawaranTekis(DokPenawaranDp.findPenawaranPeserta(getParticipantId(),
                    DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI));
        }
    }

}
