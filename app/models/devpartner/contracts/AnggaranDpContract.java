package models.devpartner.contracts;

import ext.FormatUtils;

/**
 * @author HanusaCloud on 8/15/2018
 */
public interface AnggaranDpContract {

    String getKodeRekening();
    Double getAngNilai();
    Integer getAngTahun();
    String getSbdLabel();
    String getPpkNama();
    Long getId();
    boolean isPpkExist();

    default String getAngNilaiFormatted() {
        return getAngNilai() != null ? FormatUtils.formatCurrencyRupiah(getAngNilai()) : "Rp. 0";
    }

}
