package models.devpartner.contracts;

import groovy.lang.Closure;
import models.agency.Ppk;
import models.devpartner.AnggaranDp;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 8/16/2018
 * class ini ada untuk menjamin compatibility object antara paket_anggaran dan paket_anggaran_pl
 * untuk digunakan pada {@link ext.PaketTag#_anggaranTable(Map, Closure, PrintWriter, GroovyTemplate.ExecutableTemplate, int)}
 */
public interface PaketAnggaranDpContract {

    Long getAngId();
    Long getPpkId();
    Long getPktId();
    AnggaranDpContract getAnggaranContract();
    void setAnggaran(AnggaranDp anggaran);
    Ppk getPpk();
    boolean isPpkExist();
    String getPpkNama();

    default String getUpdatePpkUrl(String action) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", getPktId());
        if (action.contains("PaketDpCtr")) {
            params.put("ang_id", getAngId());
        } else {
            params.put("angId", getAngId());
        }
        return Router.reverse(action, params).url;
    }

    default String optionLabel() {
        if (isPpkExist()) {
            return Messages.get("menu.edit");
        }
        return Messages.get("menu.pilih");
    }

}
