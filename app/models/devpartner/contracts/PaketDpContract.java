package models.devpartner.contracts;

import models.agency.PaketPpk;
import models.agency.Ukpbj;
import models.common.Active_user;
import models.devpartner.PaketLokasiDp;
import models.devpartner.PaketPpkDp;
import utils.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 5/3/2018
 */
public interface PaketDpContract {

    String TAG = "PaketDpContract";

    Long getPktId();
    Long getUkpbjId();
    List<PaketLokasiDp> getPaketLocations();
    PaketPpkDp getPaketPpk();
    Ukpbj getUkpbj();

    void setPaketLocations(List<PaketLokasiDp> items);
    void setPaketPpk(PaketPpkDp model);
    void setUkpbj(Ukpbj model);


    default void withLocations() {
        if (getPktId() != null) {
            setPaketLocations(PaketLokasiDp.getNewestByPktId(getPktId()));
        }
    }

    default void withPaketPpk() {
        if (getPktId() != null) {
            setPaketPpk(PaketPpk.find("pkt_id =?", getPktId()).first());
        }
    }

    /**
     * create paket ppk if the creator of this paket is actual PPK*/
    default void createPaketPpk(Active_user user) {
        LogUtil.debug(TAG, "create paket PPK");
        if (getPktId() != null && user.isPpk()) {
            PaketPpkDp paketPpk = new PaketPpkDp(getPktId(), user.ppkId);
            LogUtil.debug(TAG, paketPpk);
            paketPpk.save();
            setPaketPpk(paketPpk);
        }
    }

    /**
     * create paket ppk if the creator of this paket is actual PPK*/
    default void createPaketPpk(Long ppkId) {
        LogUtil.debug(TAG, "create paket PPK");
        PaketPpkDp paketPpk = new PaketPpkDp(getPktId(), ppkId);
        LogUtil.debug(TAG, paketPpk);
        paketPpk.save();
        setPaketPpk(paketPpk);
    }

    /**
     * check is this paket actually my paket and i am PPK*/
    default boolean isThisMyPaketAsPpk(Active_user user) {
        return user != null && user.isPpk() && getPaketPpk() != null && getPaketPpk().isItMyPaket(user.ppkId);
    }

}
