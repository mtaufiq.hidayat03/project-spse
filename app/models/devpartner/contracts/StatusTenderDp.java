package models.devpartner.contracts;

/**
 * interface status tender
 * pake #{statusTender /} di template
 * Object harus implement ini
 */
public interface StatusTenderDp {

    boolean isLelangUlang();

    boolean isDitutup();

    boolean isEvaluasiUlang();

    boolean isKonsultansi();

    boolean isPenawaranUlang();
}
