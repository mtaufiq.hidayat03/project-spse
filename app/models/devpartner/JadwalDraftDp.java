package models.devpartner;

import ext.DatetimeBinder;
import models.jcommon.db.base.BaseModel;
import org.joda.time.Period;
import play.data.binding.As;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import java.util.Date;


@Table(name = "jadwal_draft_dp", schema = "devpartner")
public class JadwalDraftDp extends BaseModel {

	@Id(sequence="devpartner.seq_jadwal_draft_dp", function="nextsequence")
	public Long dtjd_id;

	@As(binder = DatetimeBinder.class)
	public Date dtjd_tglawal;

	@As(binder = DatetimeBinder.class)
	public Date dtjd_tglakhir;

	public String dtjd_keterangan;

	public Long dtj_id;

	public static JadwalDraftDp findByJadwal(Long dtj_id) {
		return find("dtj_id = ?", dtj_id).first();
	}

	public static boolean isWaitingConfirmation(long lelangId) {
		String sql = "SELECT count(1) FROM devpartner.jadwal_draft_dp jd "
				+ "INNER JOIN devpartner.jadwal_dp j ON jd.dtj_id = j.dtj_id "
				+ "WHERE j.lls_id = ?";
		return Query.count(sql, lelangId) > 0;
	}

	public static void deleteByLelang(long lelangId) {
		delete("dtj_id in (select dtj_id from devpartner.jadwal_dp where lls_id = ?)", lelangId);
	}

	public static String periodToString(Period p) {
		if(p==null)
			return null;
		p=p.normalizedStandard();
		StringBuilder str=new  StringBuilder();
		if(p.getMonths() > 0)
			str.append(p.getWeeks()).append(" "+ Messages.get("lelang.bulan")+" ");
		if(p.getWeeks() > 0)
			str.append(p.getWeeks()).append(" "+Messages.get("lelang.minggu")+" ");
		if(p.getDays() > 0)
			str.append(p.getDays()).append(" "+Messages.get("lelang.hari")+" ");
		if(p.getHours() > 0)
			str.append(p.getHours()).append(" "+Messages.get("lelang.jam")+" ");
		if(p.getMinutes() > 0)
			str.append(p.getMinutes()).append(" "+Messages.get("lelang.menit")+" ");
		return str.toString();
	}

	public String getPeriodAsString()
	{
		Period period=null;
		if(dtjd_tglawal != null & dtjd_tglakhir !=null)
			period= new Period(dtjd_tglakhir.getTime()-dtjd_tglawal.getTime());
		if(period==null)
			return "";
		else
			return periodToString(period);
	}
}
