package models.agency;

import controllers.BasicCtr;
import ext.FormatUtils;
import ext.RupiahBinder;
import models.agency.contracts.PaketContract;
import models.agency.contracts.SirupContract;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.Lelang_seleksi;
import models.lelang.Persetujuan;
import models.lelang.Unspsc;
import models.secman.Usergroup;
import models.sirup.PaketSirup;
import models.sirup.PaketSirup.PaketSirupAnggaran;
import models.sirup.PaketSirup.PaketSirupLokasi;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * Model {@code Paket} merepresentasikan tabel paket pada database.
 *
 * @author I Wayan Wiprayoga W
 */


@Table(name = "paket")
public class Paket extends BaseModel implements PaketContract {

	@Enumerated(EnumType.ORDINAL)
	public enum StatusPaket {

		DRAFT(0,"Draft"),
		SEDANG_LELANG(1,"Tender Sedang Berjalan"),
		SELESAI_LELANG(2,"Tender Sudah Selesai"), // di versi 3.5 belum pernah dipakai
		ULANG_LELANG(3,"Tender Diulang"),
		LELANG_DITOLAK(4,"Tender Ditolak");
		
		public final Integer id;
		public final String label;
		
		StatusPaket(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}	
		
		public static StatusPaket fromValue(Integer value){
			StatusPaket status = null;
			if(value != null){
				switch(value.intValue()){
					case 0:status = DRAFT;break;
					case 1:status = SEDANG_LELANG;break;
					case 2:status = SELESAI_LELANG;break;
					case 3:status = ULANG_LELANG;break;
					case 4:status = LELANG_DITOLAK;break;
					default:status = DRAFT;break;
				}
			}
			return status;
		}
		
		public boolean isDraft(){
			return this == DRAFT;
		}
		
		public boolean isSedangLelang(){
			return this == SEDANG_LELANG;
		}
		
		public boolean isSelesaiLelang(){
			return this == SELESAI_LELANG;
		}
		
		public boolean isUlangLelang(){
			return this == ULANG_LELANG;
		}
		
		public boolean isLelangDitolak(){
			return this == LELANG_DITOLAK;
		}
	}

	@Override
	protected void preDelete(){
		if(pkt_status.isDraft()) {
			Lelang_seleksi lelang = Lelang_seleksi.findByPaket(pkt_id);
			if(lelang != null) {
				Query.update("DELETE FROM persetujuan WHERE lls_id = ?", lelang.lls_id);
				Query.update("DELETE FROM jadwal WHERE lls_id = ?", lelang.lls_id);
				Query.update("DELETE FROM checklist WHERE dll_id IN (SELECT dll_id FROM dok_lelang WHERE lls_id = ?)", lelang.lls_id);
				Query.update("DELETE FROM dok_lelang_content WHERE dll_id IN (SELECT dll_id FROM dok_lelang WHERE lls_id = ?)", lelang.lls_id);
				Query.update("DELETE FROM dok_lelang WHERE lls_id = ?", lelang.lls_id);
				Query.update("DELETE FROM lelang_seleksi WHERE lls_id = ?", lelang.lls_id);
			}
			Query.update("DELETE FROM paket_lokasi WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_ppk WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_panitia WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_anggaran WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM paket_satker WHERE pkt_id = ?", pkt_id);
			Query.update("DELETE FROM dok_persiapan WHERE pkt_id = ?", pkt_id);
		}

	}
	
	/**
	 * Kode Paket
	 */
	@Id(sequence="seq_paket", function="nextsequence")
	public Long pkt_id;
	/**
	 * Nama paket
	 */
	@Required
	public String pkt_nama;
	/**
	 * Nilai pagu paket
	 */
	@Required
	@As(binder=RupiahBinder.class)
	public Double pkt_pagu;
	/**
	 * Nilai HPS (Harga Perkiraan Sendiri) paket
	 */
	@As(binder=RupiahBinder.class)
	public Double pkt_hps;

	/**
	 * View HPS (Harga Perkiraan Sendiri) paket
	 */
	public Boolean pkt_hps_enable;

	/**
	 * dokumen terkait pembuatan paket
	 * (info: sudah tidak dipakai lagi)
	 */
	public Long pkt_id_attachment;
	
	/**
	 * tanggal realisasi paket 
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_realisasi;
	
	/**
	 * tanggal paket di kirimkan dari panitia ke ppk, 
	 * semenjak versi 3.5 di set saat pembuatan paket karena tidak ada persetujuan ppk dari panitia
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_assign;
	/**
	 * Tanggal pembuatan paket
	 */
	public Date pkt_tgl_buat;
	/**
	 * Tanggal persetujuan paket oleh PPK
	 */
	public Date pkt_tgl_ppk_setuju;
	/**
	 * Status paket
	 */
	@Required
	public StatusPaket pkt_status;
	
	/**
	 * flag jenis paket berdasarkan aturan dan versi
	 * 0 : Jenis paket dibuat dengan spse versi 2.x, memakai kepres 80
	 * 1 : Jenis paket dibuat dengan spse versi 3.x, memakai perpres 54 & 70
	 * 2 : Jenis paket dibuat dengan spse versi 4.x, memakai perpres 54 & 70
	 * 3 : Jenis paket dibuat dengan spse versi 4.3, memakai perpres 16 2018
	 */
	@Required
	public Integer pkt_flag = 0;
	/**
	 * Kualifikasi pelelangan
	 */
	public String kls_id;
	
	/**
	 * Kepanitiaan pembuat paket lelang
	 */
	@Required
	public Long pnt_id;
	/**
	 * Kategori pengadaan/lelang
	 */
	@Required
	public Kategori kgr_id;
	/**
	 * Nomor surat rencana pelaksanaan
	 */
	public String pkt_no_spk;
	
	public Integer unspsc_id;
	// OSD
	public String ttp_ticket_id;

	public Long ukpbj_id;

	public Boolean is_pkt_konsolidasi = false;

	public Long pkt_id_konsolidasi;

	// informasi build spse
	public String pkt_tgl_build;

	@Transient
	private Panitia panitia;

	@Transient
	private Unspsc unspsc;

	@Transient
	private PaketPpk paketPpk;
	@Transient
	private Ukpbj ukpbj;
	@Transient
	private Lelang_seleksi lelang_seleksi;
	@Transient
	DokPersiapan dokPersiapan;

	@Transient
	private List<Paket_lokasi> locations;

	@Transient
	public List<Paket_anggaran> paketAnggarans;

	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}

	public Unspsc getUnspsc() {
		if(unspsc == null)
			unspsc = Unspsc.findById(unspsc_id);
		return unspsc;
	}

	public List<Paket_anggaran> getPaketAnggarans() {
		if (CollectionUtils.isEmpty(paketAnggarans)) {
			Integer newestVersion = getVersiPaketAnggaran(getPktId());
			paketAnggarans = Paket_anggaran.findByPaketAndVersion(getPktId(), newestVersion);
		}
		return paketAnggarans;
	}

	private static Integer getVersiPaketAnggaran(Long pktId) {
		Paket_anggaran paket_anggaran = Paket_anggaran.find("pkt_id=? order by pkt_ang_versi desc", pktId).first();
		return paket_anggaran==null || paket_anggaran.pkt_ang_versi == null ? 1 : paket_anggaran.pkt_ang_versi;
	}

	public DokPersiapan getDokPersiapan(){
		if(dokPersiapan == null){
			dokPersiapan = DokPersiapan.findByPaket(pkt_id);
		}

		return dokPersiapan;
	}

	public Lelang_seleksi getLelangSeleksi(){
		if(lelang_seleksi == null){
			lelang_seleksi = Lelang_seleksi.findByPaket(pkt_id);
		}

		return lelang_seleksi;
	}

	public boolean isKonsolidasi(){
		if(is_pkt_konsolidasi == null){
			return false;
		}

		return is_pkt_konsolidasi;
	}

	@Override
	public Long getPktId() {
		return this.pkt_id;
	}

	@Override
	public Long getUkpbjId() {
		return this.ukpbj_id;
	}

	@Override
	public List<Paket_lokasi> getPaketLocations() {
		if (CommonUtil.isEmpty(locations)) {
			withLocations();
		}
		return locations;
	}

	@Override
	public PaketPpk getPaketPpk() {
		if (paketPpk == null) {
			withPaketPpk();
		}
		return paketPpk;
	}

	@Override
	public Ukpbj getUkpbj() {
		if (ukpbj == null) {
			ukpbj = Ukpbj.findById(ukpbj_id);
		}
		return ukpbj;
	}

	@Override
	public void setPaketLocations(List<Paket_lokasi> items) {
		this.locations = items;
	}

	@Override
	public void setPaketPpk(PaketPpk model) {
		this.paketPpk = model;
	}

	@Override
	public void setUkpbj(Ukpbj model) {
		this.ukpbj = model;
	}


	public void setFlag43() {
		this.pkt_flag = 3;
		this.pkt_tgl_build = ConfigurationDao.SPSE_VERSION;
	}

	/**
	 * return true if this package is created using new "perpres 16 2018"*/
	public boolean isFlag43() {
		return this.pkt_flag == 3;
	}

	public boolean isFlag42() {
		return this.pkt_flag == 2;
	}
	
	public boolean isPaketV3() {
		return pkt_flag < 2;
	}

	public List<? extends SirupContract> getSirupListForUI() {
		if (isFlag43()) {
			return getRupList();
		}
		return getRupList42();
	}

	public List<PaketSirup> getRupList() {
		return getRupList(pkt_id);
	}

	public static List<PaketSirup> getRupList(long pktId) {
		Integer lastVersion = getVersiPaketAnggaran(pktId);
		return Query.find("SELECT ps.* FROM paket_sirup ps INNER JOIN paket_anggaran pa ON ps.id=pa.rup_id WHERE pa.pkt_id=? AND pkt_ang_versi=? ORDER BY pa.auditupdate ASC",
				PaketSirup.class, pktId, lastVersion).fetch();
	}

	public List<Rup_paket> getRupList42() {
		return getRupList42(pkt_id);
	}

	public static List<Rup_paket> getRupList42(long pktId) {
		return Query.find("SELECT * FROM paket_sirup WHERE id IN (SELECT DISTINCT rup_id FROM paket_satker WHERE pkt_id=?)", Rup_paket.class, pktId).fetch();
	}

	public List<PaketSirup> getRupListByPpk(long ppkId) {
		return getRupListByPpk(pkt_id, ppkId);
	}

	public static List<PaketSirup> getRupListByPpk(long pktId, long ppkId) {
		return Query.find("SELECT rup.* FROM paket_sirup AS rup JOIN paket_satker AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.pkt_id=? AND id IN (select rup_id FROM paket_anggaran WHERE pkt_id=? AND ppk_id=?)\n" +
				"ORDER BY ps.pks_id ASC", PaketSirup.class, pktId, pktId, ppkId).fetch();
	}

	public List<Rup_paket> getRupListByPpk42(long ppkId) {
		return getRupListByPpk42(pkt_id, ppkId);
	}

	public static List<Rup_paket> getRupListByPpk42(long pktId, long ppkId) {
		return Query.find("SELECT rup.* FROM rup_paket AS rup JOIN paket_satker AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.pkt_id=? AND id IN (select rup_id FROM paket_anggaran WHERE pkt_id=? AND ppk_id=?)\n" +
				"ORDER BY ps.pks_id ASC", Rup_paket.class, pktId, pktId, ppkId).fetch();
	}

	public String getKodeRup() {
		return Paket_satker.getKodeRupPaket(pkt_id);
	}

	public String getNamaInstansi() {
		return Paket_satker.getNamaInstansiPaket(pkt_id);
	}

	public String getNamaKabupaten() {
		return Paket_satker.getNamaKabupaten(pkt_id);
	}

	public String getNamaSatker() {
		return Paket_satker.getNamaSatkerPaket(pkt_id);
	}

	public String getAlamatSatker() {
		return Paket_satker.getAlamatSatkerPaket(pkt_id);
	}

	public List<Pegawai> getPpkList() {
		return Pegawai.find("peg_id in (SELECT peg_id FROM ppk k, paket_anggaran p WHERE k.ppk_id=p.ppk_id AND p.pkt_id=?)", pkt_id).fetch();
	}

	public List<Paket_lokasi> getPaketLokasi()
	{
		return Paket_lokasi.find("pkt_id=?",pkt_id).fetch();
	}
	
	public void setKualifikasi(Kualifikasi kualifikasi)
	{
		this.kls_id = kualifikasi.id;
	}
	
	@Transient
	public Kualifikasi getKualifikasi()
	{
		if(kls_id == null)
			return null;
		return Kualifikasi.findById(kls_id);
	}	
	
	@Transient
	public String getSumberDana() {
		List<String> result =Query.find("select distinct (a.sbd_id) from Anggaran a, Paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, pkt_id).fetch();
		return StringUtils.join(result, ",");
	}
	
	@Transient
	public String getTahunAnggaran() {
		List<String> result = Query.find("select distinct (a.ang_tahun) from Anggaran a, Paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, pkt_id).fetch();
		return StringUtils.join(result, ",");
	}
	
	/**
	 * flag paket persetujuan
	 * hanya paket yang dibuat pada versi 4 (pkt_flag=2) memakai persetujuan
	 * @return
	 */
	@Transient 
	public boolean isPaket_persetujuan() {
		return pkt_flag != null && pkt_flag.intValue() >= 2;
	}

	public boolean isPaketKonsolidasi() {
		return Paket_satker.count("pkt_id=?", pkt_id) > 1;
	}
	
	/**Dapatkan lokasi paket sebagai string */
	public String getLokasiPekerjaanString()
	{
		List<Paket_lokasi> list=getPaketLokasi();
		if(list.size()==0)
			return "";
		StringBuilder str=new StringBuilder();
		for(Paket_lokasi lokasi: list)
		{
			if(str.length()>0)
				str.append(", ");
			str.append(lokasi.getKabupaten().kbp_nama).append(' ').append(lokasi.pkt_lokasi==null?"":lokasi.pkt_lokasi);			
		}
		return str.toString();
	}
	
	public static Paket simpanBuatPaket(Paket paket, Paket_lokasi[] lokasi){
		if (paket.pkt_flag == null)
			paket.setFlag43(); // flag 2 untuk paket yang dibuat dengan versi 4
		if (paket.pkt_tgl_buat == null)
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
		if (paket.pkt_hps == null)
			paket.pkt_hps = Double.valueOf(0);
		if (paket.pkt_status == null)
			paket.pkt_status = StatusPaket.DRAFT; // default status draft
		if (paket.kgr_id == null)
			paket.kgr_id = Kategori.PENGADAAN_BARANG; // default pengadaan barang jasa
		if (paket.pkt_tgl_assign == null)
			paket.pkt_tgl_assign = controllers.BasicCtr.newDate();
		paket.save();
		if (!paket.isFlag43()) {
			Paket_panitia paket_panitia = Paket_panitia.getByPktIdAndPntId(paket.pkt_id, paket.pnt_id);
			if (paket_panitia == null) {
				paket_panitia = new Paket_panitia(paket);
				paket_panitia.save();
			}
		}
		Paket_lokasi obj = null;
		List<Long> saved = new ArrayList<>(lokasi.length);
		for (Paket_lokasi lok : lokasi) {
			if (lok.pkl_id == null)
				obj = new Paket_lokasi();
			else
				obj = Paket_lokasi.findById(lok.pkl_id);
			obj.kbp_id = lok.kbp_id;
			obj.pkt_lokasi_versi = 1;
			obj.pkt_id = paket.pkt_id;
			if (!StringUtils.isEmpty(lok.pkt_lokasi)) {
				obj.pkt_lokasi = lok.pkt_lokasi;
				obj.save();
			}
			saved.add(obj.pkl_id);
		}
		if(!CommonUtil.isEmpty(saved)) {
			String join = StringUtils.join(saved, ",");
			if(!join.isEmpty())
				Query.update("delete from paket_lokasi where pkt_id=? and pkl_id not in ("+join+ ')', paket.pkt_id);
		}
		Lelang_seleksi lls = Lelang_seleksi.buatLelangBaru(paket, null);
		LogUtil.debug("TAG", lls);
		return paket;
	}

	/**
	 *
	 * @param rup_id
	 * @return
	 */
	public static Paket updatePaketRup(Long rup_id, Long paketId) {
		LogUtil.debug(TAG,"Updating RUP");
		PaketSirup rup = PaketSirup.findById(rup_id);
		Paket paket = Paket.findById(paketId);
		Paket_satker ps = Paket_satker.find("pkt_id=?", paket.pkt_id).first();
		if(ps==null) {
			Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
			ps = new Paket_satker();
			ps.stk_id = satker.stk_id;
			ps.pkt_id = paket.pkt_id;
			ps.rup_id = rup.id;
			ps.save();
		}

		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findByPaketNewestVersi(paket.pkt_id);
		Active_user user = Active_user.current();
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)) {
			Anggaran anggaran = null;
			Paket_anggaran paket_anggaran = null;
			Paket_anggaran.delete("pkt_id=?", paket.pkt_id);
			Double pagu = Double.valueOf(0);
			for (PaketSirupAnggaran rup_anggaran : rup.paket_anggaran_json) {
				anggaran = new Anggaran();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup_anggaran.tahun_anggaran_dana == null ? rup.tahun : rup_anggaran.tahun_anggaran_dana;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = ps.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				pagu += anggaran.ang_nilai ;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran = new Paket_anggaran();
				paket_anggaran.pkt_id = paket.pkt_id;
				paket_anggaran.ang_id = anggaran.ang_id;
				paket_anggaran.ppk_id = user.ppkId;
				paket_anggaran.ppk_jabatan = 0;
				paket_anggaran.rup_id = rup.id;
				paket_anggaran.pkt_ang_versi = lelang_seleksi.lls_versi_lelang;
				paket_anggaran.save();
			}
			paket.pkt_pagu = pagu;
		}
		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			Paket_lokasi.delete("pkt_id=?", paket.pkt_id);
			for(PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				Paket_lokasi pkt_lokasi = new Paket_lokasi();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.pkt_id = paket.pkt_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = lelang_seleksi.lls_versi_lelang;
				pkt_lokasi.save();
			}
		}
		paket.save();
		return paket;
	}


	/**
	 *
	 * @param rup_id
	 * @return
	 */
	public static Paket buatPaketFromSirup(Long rup_id, Long paketId, Long panitiaId) {
		PaketSirup rup = PaketSirup.findById(rup_id);
		Paket paket = null;
		if(paketId == null) {
			paket = new Paket();
			paket.pkt_nama = rup.nama;
			paket.pkt_pagu = rup.pagu;
			paket.pkt_hps = Double.valueOf(0);
			paket.pnt_id = panitiaId;
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
			paket.pkt_status = StatusPaket.DRAFT;
			paket.setFlag43(); // default flag paket yang dibuat diversi 4
			paket.kgr_id = rup.getKategori();
		} else { // paket konsolidasi (multi kode rup)
			paket = Paket.findById(paketId);
			if(!paket.pkt_nama.contains(rup.nama)) // jika nama paket sama tidak perlu ditambahkan
				paket.pkt_nama += ',' + rup.nama;
			paket.pkt_pagu += rup.pagu;
		}
		paket.save();
		Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
		Paket_satker ps = new Paket_satker();
		ps.stk_id = satker.stk_id;
		ps.pkt_id = paket.pkt_id;
		ps.rup_id = rup.id;
		ps.save();
		Active_user user = Active_user.current();
		paket.createPaketPpk(user);
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)) {
			Anggaran anggaran = null;
			Paket_anggaran paket_anggaran = null;
			for (PaketSirupAnggaran rup_anggaran : rup.paket_anggaran_json) {
				anggaran = new Anggaran();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup_anggaran.tahun_anggaran_dana == null ? rup.tahun : rup_anggaran.tahun_anggaran_dana;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = satker.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran = new Paket_anggaran();
				paket_anggaran.pkt_id = paket.pkt_id;
				paket_anggaran.ang_id = anggaran.ang_id;
				paket_anggaran.ppk_id = user.ppkId;
				paket_anggaran.ppk_jabatan = 0;
				paket_anggaran.rup_id = rup.id;
				paket_anggaran.pkt_ang_versi = Integer.valueOf(1);
				paket_anggaran.save();
			}
		}
		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			for(PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				Paket_lokasi pkt_lokasi = new Paket_lokasi();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.pkt_id = paket.pkt_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = Integer.valueOf(1);
				pkt_lokasi.save();
			}
		}
		//hanya jika paket belum pernah dibuat (pkt_id == null) langsung create lelangbaru
		if(paketId == null) {
			// langsung buat lelang baru
			MetodePemilihanPenyedia metode = MetodePemilihanPenyedia.findById(rup.metode_pengadaan);
			Lelang_seleksi.buatLelangBaru(paket, metode);
		}
		return paket;
	}

	public static Paket buatPaketKonsolidasi(List<Paket> pakets, Long pkt_id) throws Exception {
		Paket existingPaket;
		if(!CollectionUtils.isEmpty(pakets)) {
			existingPaket = pakets.get(0);
			Paket paket;
			if(pkt_id == null) {
				paket = new Paket();
				paket.pkt_tgl_buat = BasicCtr.newDate();
				paket.pkt_status = StatusPaket.DRAFT;
				paket.setFlag43(); // default flag paket yang dibuat diversi 4
				paket.kgr_id = existingPaket.kgr_id;
				paket.ukpbj_id = existingPaket.ukpbj_id;
				paket.is_pkt_konsolidasi = true;
				paket.pkt_nama = "";
				paket.pkt_pagu = Double.valueOf(0);
				paket.pkt_hps = Double.valueOf(0);
				paket.save();
			} else {
				paket = Paket.findById(pkt_id);
			}
			
			// merge semua detail masing2 paket
			List<DaftarKuantitas> daftarKuantitas = new ArrayList<>();
			List<BlobTable> daftarDokSpek = new ArrayList<>();
			List<BlobTable> daftarDokSskk = new ArrayList<>();
			List<BlobTable> daftarDokLainnya = new ArrayList<>();
			for(Paket p : pakets) {
				if(pkt_id == null) {
					paket.pkt_nama += p.pkt_nama.concat(", ");
				}
				paket.pkt_pagu += p.pkt_pagu;
				paket.pkt_hps += p.pkt_hps;

				// merge paket satker
				Paket_satker existingPaketSatker = Paket_satker.find("pkt_id = ?", p.pkt_id).first();
				if(existingPaketSatker != null) {
					Paket_satker ps = new Paket_satker();
					ps.stk_id = existingPaketSatker.stk_id;
					ps.pkt_id = paket.pkt_id;
					ps.rup_id = existingPaketSatker.rup_id;
					ps.save();
				}

				// merge paket ppk
				PaketPpk existingPaketPpk = p.getPaketPpk();
				if(existingPaketPpk != null) {
					paket.createPaketPpk(existingPaketPpk.ppk_id);
				}

				// merge paket anggaran
				List<Paket_anggaran> existingPaList = Paket_anggaran.findByPaket(p.pkt_id);
				for(Paket_anggaran pa : existingPaList) {
					Paket_anggaran newPa = new Paket_anggaran();
					newPa.pkt_id = paket.pkt_id;
					newPa.ang_id = pa.ang_id;
					newPa.ppk_id = pa.ppk_id;
					newPa.ppk_jabatan = pa.ppk_jabatan;
					newPa.rup_id = pa.rup_id;
					newPa.pkt_ang_versi = 1;
					newPa.save();
				}

				// merge paket lokasi
				List<Paket_lokasi> existingPLokasi = Paket_lokasi.find("pkt_id = ?", p.pkt_id).fetch();
				for(Paket_lokasi pLokasi : existingPLokasi) {
					Paket_lokasi pkt_lokasi = new Paket_lokasi();
					pkt_lokasi.pkt_lokasi = pLokasi.pkt_lokasi;
					pkt_lokasi.pkt_id = paket.pkt_id;
					pkt_lokasi.kbp_id = pLokasi.kbp_id;
					pkt_lokasi.pkt_lokasi_versi = 1;
					pkt_lokasi.save();
				}

				DokPersiapan dp = DokPersiapan.findByPaket(p.pkt_id);
				if(dp != null) {
					daftarKuantitas.add(dp.dkh);
					if(!CollectionUtils.isEmpty(dp.getDokSpek()))
						daftarDokSpek.addAll(dp.getDokSpek());
					if(!CollectionUtils.isEmpty(dp.getDokSskkAttachment()))
						daftarDokSskk.addAll(dp.getDokSskkAttachment());
					if(!CollectionUtils.isEmpty(dp.getDokLainnya()))
						daftarDokLainnya.addAll(dp.getDokLainnya());
				}

				p.pkt_id_konsolidasi = paket.pkt_id;
				p.save();
			}
			// merge dok persiapan
			DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket);
			if(!CollectionUtils.isEmpty(daftarKuantitas)) {
				if(dokPersiapan.dkh == null)
					dokPersiapan.dkh = new DaftarKuantitas();
				for(DaftarKuantitas dk : daftarKuantitas) {
					if(dk == null) {
						continue;
					}
					if(dokPersiapan.dkh.items == null)
						dokPersiapan.dkh.items = new ArrayList<>();
					dokPersiapan.dkh.items.addAll(dk.items);
					dokPersiapan.dkh.fixed = dk.fixed;
					if(dokPersiapan.dkh.total == null)
						dokPersiapan.dkh.total = Double.valueOf(0);
					if(dk.total != null)
						dokPersiapan.dkh.total += dk.total;
				}
			}			
			if(!CollectionUtils.isEmpty(daftarDokSpek)) {
				if(!CollectionUtils.isEmpty(dokPersiapan.getDokSpek()))
					daftarDokSpek.addAll(dokPersiapan.getDokSpek());
				dokPersiapan.dp_spek = dokPersiapan.mergeAttachment(daftarDokSpek);
			}
			if(!CollectionUtils.isEmpty(daftarDokSskk)) {
				if(!CollectionUtils.isEmpty(dokPersiapan.getDokSskkAttachment()))
					daftarDokSskk.addAll(dokPersiapan.getDokSskkAttachment());
				dokPersiapan.dp_sskk_attachment = dokPersiapan.mergeAttachment(daftarDokSskk);
			}
			if(!CollectionUtils.isEmpty(daftarDokLainnya)) {
				if(!CollectionUtils.isEmpty(dokPersiapan.getDokLainnya()))
					daftarDokLainnya.addAll(dokPersiapan.getDokLainnya());
				dokPersiapan.dp_lainnya = dokPersiapan.mergeAttachment(daftarDokLainnya);
			}
			dokPersiapan.save();
			if(pkt_id == null){
				paket.pkt_nama = paket.pkt_nama.substring(0, paket.pkt_nama.length() - 2);
			}
			paket.save();

			/**
			 * Create hanya jika belum pernah/baru
			 */
			if(pkt_id == null) {
				MetodePemilihanPenyedia metode = MetodePemilihanPenyedia.findById(null);
				Lelang_seleksi.buatLelangBaru(paket, metode);
			}
			return paket;
		}

		return null;
	}
	
	public static String getNamaPaketFromlelang(Long lelangId) {
		return Query.find("select pkt_nama from paket where pkt_id IN (select pkt_id from lelang_seleksi where lls_id=?)", String.class, lelangId).first();
	}
	
	public static Paket findByLelang(Long lelangId) {
		return Query.find("SELECT * FROM paket WHERE pkt_id IN (SELECT pkt_id FROM lelang_seleksi WHERE lls_id=?)", Paket.class, lelangId).first();
	}

	public Boolean isEnableViewHps(){
		return pkt_hps_enable != null ? pkt_hps_enable : true;
	}

	public static boolean isFlag43(Long lelangId) {
		Integer flag = Query.find("SELECT pkt_flag FROM paket WHERE pkt_id IN (SELECT pkt_id FROM lelang_seleksi WHERE lls_id=?)", Integer.class, lelangId).first();
		return flag ==  3;
	}

	public boolean allowEdit() {
		return (pkt_status.isDraft() || pkt_status.isUlangLelang()) && !isSedangPersetujuan();
	}

	public boolean allowEditUkpbj() {
		return !isSedangPersetujuan() && pkt_status.isDraft() && Active_user.current().isPpk();
	}

	public boolean isEditable(){
		boolean valid = allowEdit();
		if (isFlag43()) {
			valid = valid && !Active_user.current().isPanitia();
		}
		return valid;
	}

	public boolean isEditableByKuppbj() {
		return isEditable() && Active_user.current().isKuppbj();
	}

	public boolean isEditableExceptKuppbj() {
		return isEditable() && !Active_user.current().isKuppbj();
	}

	public boolean isEditableByKuppbjKonsolidasi() {
		return isEditable() && isKonsolidasi() && Active_user.current().isKuppbj() && pkt_status.isDraft();
	}

	public boolean isCreatedByPpk() {
		return isFlag43() && getPaketPpk() != null;
	}

	public boolean isAllowEditPpk() {
		return pkt_status.isDraft() && !isCreatedByPpk();
	}

	public boolean isAllowToShowSaveButton(boolean documentFilled) {
		if (isCreatedByPpk() && showExtendedPpkInput()) {
			return allowEdit()
					&& isHpsExist()
					&& documentFilled
					&& Active_user.current().isPpk();
		}
		return allowEdit();
	}

	public String getHpsFormatted() {
		return isHpsExist() ? FormatUtils.formatCurrencyRupiah(pkt_hps) : "Rp. 0";
	}

	public String getPaguFormatted() {
		return pkt_pagu != null ? FormatUtils.formatCurrencyRupiah(pkt_pagu) : "Rp. 0";
	}

	public boolean isHpsExist() {
		return pkt_hps != null && pkt_hps > 0;
	}


	public boolean showExtendedPpkInput() {
		return getPaketLocations().size() > 0;
	}

	public static long countPaketOwnedByPegawai(Pegawai pegawai) {
		if(pegawai == null) {
			return -1;
		}
		Usergroup group = Usergroup.findByUser(pegawai.peg_namauser);
		long paketOwned = 0;
		if(group.idgroup.isKuppbj()) {
			paketOwned += Query.count("SELECT COUNT(*) FROM paket p JOIN ukpbj u ON p.ukpbj_id = u.ukpbj_id " +
					"AND u.kpl_unit_pemilihan_id = ?", pegawai.peg_id);
			paketOwned += Query.count("SELECT COUNT(*) FROM ekontrak.paket p JOIN ukpbj u ON p.ukpbj_id = u.ukpbj_id " +
					"AND u.kpl_unit_pemilihan_id = ?", pegawai.peg_id);
		}
		if(group.idgroup.isPpk()) {
			paketOwned += Query.count("SELECT COUNT(*) FROM paket p JOIN paket_ppk pp ON p.pkt_id = pp.pkt_id " +
					"JOIN ppk ppk on pp.ppk_id = ppk.ppk_id AND ppk.peg_id = ?", pegawai.peg_id);
			paketOwned += Query.count("SELECT COUNT(*) FROM ekontrak.paket p JOIN ekontrak.paket_ppk pp ON p.pkt_id = pp.pkt_id " +
					"JOIN ppk ppk on pp.ppk_id = ppk.ppk_id AND ppk.peg_id = ?", pegawai.peg_id);
		}
		if(group.idgroup.isPanitia()) {
			paketOwned += Query.count("SELECT COUNT(*) FROM paket p JOIN paket_panitia pp ON p.pkt_id = pp.pkt_id " +
					"JOIN anggota_panitia ap ON pp.pnt_id = ap.pnt_id AND ap.peg_id = ?", pegawai.peg_id);
			paketOwned += Query.count("SELECT COUNT(*) FROM ekontrak.paket p JOIN ekontrak.paket_panitia pp ON p.pkt_id = pp.pkt_id " +
					"JOIN anggota_panitia ap ON pp.pnt_id = ap.pnt_id AND ap.peg_id = ?", pegawai.peg_id);
		}
		if(group.idgroup.isPP()) {
			paketOwned += Query.count("SELECT COUNT(*) FROM ekontrak.paket p JOIN ekontrak.pp pp ON p.pp_id = pp.pp_id " +
					"AND pp.peg_id = ?", pegawai.peg_id);
		}

		return paketOwned;
	}

	public boolean isSedangPersetujuan() {
		Long lelangId = Query.find("SELECT lls_id FROM lelang_seleksi WHERE pkt_id=? ORDER BY lls_id DESC", Long.class, pkt_id).first();
		if(lelangId == null)
			return false;
		return Persetujuan.isSedangPersetujuan(lelangId);
	}
	
	public boolean isSanggahanExistOnTender() {
		Lelang_seleksi lelang = Lelang_seleksi.findAktifByPaket(pkt_id);
		return lelang != null && lelang.getJumlahSanggahan() > 0;
	}

	public static List<Paket> findByIds(String ids) {
		return find("pkt_id in (" + ids + ")").fetch();
	}

	public static List<Paket> findByKonsolidasiId(Long pkt_id_konsolidasi) {
		return find("pkt_id_konsolidasi = ?", pkt_id_konsolidasi).fetch();
	}
}
