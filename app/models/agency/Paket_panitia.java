package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Model {@code Paket_panitia} merepresentasikan tabel paket_panitia pada
 * database.
 *
 * @author I Wayan Wiprayoga W
 */

@Table(name="PAKET_PANITIA")
public class Paket_panitia extends BaseModel {

	/**
	 * Paket yang dibuat oleh panitia tertentu
	 */
	@Id
	public Long pkt_id;
	/**
	 * Objek kepanitiaan yang membuat paket
	 */
	@Id
	public Long pnt_id;
	
	@Transient
	private Paket paket;
	@Transient
	private Panitia panitia;

	public Paket_panitia() {}

	public Paket_panitia(Paket model) {
		this.pkt_id = model.pkt_id;
		this.pnt_id = model.pnt_id;
	}

	public static Paket_panitia getByPktIdAndPntId(Long pktId, Long pntId) {
		return find("pkt_id=? and pnt_id=?", pktId, pntId).first();
	}

	public static Paket_panitia getByPktId(Long pktId){
		return find("pkt_id=?", pktId).first();
	}
	
	public Paket getPaket() {
		if(paket == null)
			paket = Paket.findById(pkt_id);
		return paket;
	}
	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}

}
