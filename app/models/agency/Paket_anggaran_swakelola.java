package models.agency;

import models.AnggaranSwakelola;
import models.agency.contracts.AnggaranSwakelolaContract;
import models.agency.contracts.PaketAnggaranSwakelolaContract;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 4/18/2017.
 */
@Table(name="EKONTRAK.PAKET_ANGGARAN_SWAKELOLA")
public class Paket_anggaran_swakelola extends BaseModel implements PaketAnggaranSwakelolaContract{

    @Id
    public Long swk_id;

    @Id
    public Long ang_id;

    @Transient
    private Paket_swakelola paket;
    @Transient
    private AnggaranSwakelola anggaran;
    @Transient
    private Ppk ppk;

    //relasi ke table PPK
    public Long ppk_id;

    //relasi ke model Rup_paket
    public Long rup_id;

    @Override
    public Long getAngId() {
        return this.ang_id;
    }

    @Override
    public Long getPpkId() {
        return this.ppk_id;
    }

    @Override
    public Long getPktId() {
        return this.swk_id;
    }

    @Override
    public AnggaranSwakelolaContract getAnggaranContract() {
        return getAnggaran();
    }

    public AnggaranSwakelola getAnggaran() {
        if(anggaran == null)
            anggaran = AnggaranSwakelola.findById(ang_id);
        return anggaran;
    }

    public Paket_swakelola getPaket() {
        if(paket == null)
            paket = Paket_swakelola.findById(swk_id);
        return paket;
    }

    @Override
    public void setAnggaran(AnggaranSwakelola anggaran) {
        this.anggaran = anggaran;
    }

    @Override
    public Ppk getPpk(){
        if(ppk == null){
            ppk = Ppk.findById(getPpkId());
        }
        return ppk;
    }

    public static List<Paket_anggaran_swakelola> findByPaket(Long swkId) {
        return find("swk_id =?", swkId).fetch();
    }

    @Override
    public boolean isPpkExist() {
        return getPpk() != null;
    }

    @Override
    public String getPpkNama() {
        return isPpkExist() ? getPpk().getNamaPegawai() : "";
    }


}
