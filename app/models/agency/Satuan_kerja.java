package models.agency;

import models.common.Instansi;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Table(name="SATUAN_KERJA")
public class Satuan_kerja extends BaseModel {

	/**
	 * ID satuan kerja
	 */
    @Id(sequence="seq_satuan_kerja", function="nextsequence")
	public Long stk_id;
	/**
	 * Nama satuan kerja
	 */
	@Required
	public String stk_nama;
	/**
	 * Alamat satker
	 */
	@Required
	public String stk_alamat;
	/**
	 * Nomor telepon satuan kerja
	 */
	@Required
	public String stk_telepon;
	/**
	 * Kontak satuan kerja
	 */
	@Required
	public String stk_contact_person;
	/**
	 * Nomor faksimili satuan kerja
	 */
	public String stk_fax;
	/**
	 * Kode pos satuan kerja berada
	 */
	public String stk_kodepos;

	//relasi Agency
	public Long agc_id;
	/**
	 * Kode satker, menyesuaikan dengan SPSE v 3.5
	 */
	@Required
	public String stk_kode;
	/**
	 * Instansi dimana satuan kerja bernaung
	 */
	@Required
	public String instansi_id;
	
	
	/**
	 * kode satker yang ada di RUP
	 * ini pasti berbeda dengan spse
	 */
	public String rup_stk_id;

	// tahun satker aktif , sumber dari sirup
	public String rup_stk_tahun;
	
	@Transient
	private Agency agency;
	
	@Transient
	private Instansi instansi;
	
	public Agency getAgency() {
		if(agency == null)
			agency = Agency.findById(agc_id);
		return agency;
	}
	
	
	public Instansi getInstansi() {
		if(instansi == null)
			instansi = Instansi.findById(instansi_id);
		return instansi;
	}


	/**
	 * Fungsi {@code findSemuaSatkerByAgency} digunakan untuk mendapatkan daftar
	 * objek {@code Satuan_kerja} yang merupakan bagian suatu agency.
	 *
	 * @param agc_id ID agency
	 * @return semua daftar satker yang merupakan bagian dari agency yang
	 *         didefinsikan pada parameter {@code agc_id}
	 */
	public static List<Satuan_kerja> findBy(Long agc_id) {;
		return find("agc_id = ?", agc_id).fetch(); // cari semua objek satker yang sesuai dengan agency-nya
	}

	/**
	 * Fungsi {@code findByIdAndAgency} digunakan untuk mendapatkan objek {@code Satuan_kerja} berdasarkan ID satuan kerja dan agency yang berada diatasnya. Hal ini dimaksudkan untuk proteksi data untuk tiap admin agency.
	 *
	 * @param satkerId id satker
	 * @param agencyId agency yang berada diatasnya
	 * @return objek {@code Satuan_kerja} yang sesuai dengan parameter {@code stk_id} dan {@code agency}
	 */
	public static Satuan_kerja findBy(Long satkerId, Long agencyId) {
		return find("stk_id = ? AND agc_id = ?", satkerId, agencyId).first();
	}
	
	/**
	 * Fungsi {@code hapus} digunakan untuk menghapus data satuan kerja dari database
	 *
	 * @param stk_id id satker yang akan dihapus
	 */
	public static void hapus(Long stk_id) {
		Satuan_kerja satker = findById(stk_id);
		satker.delete(); // hapus objek ini
	}
	

	/*public static String tableSatker(final Long agc_id)
	{
		String[] column = new String[]{"stk_id", "stk_nama", "stk_alamat", "stk_contact_person", "stk_telepon", "stk_fax"};
		String sql ="SELECT stk_id, stk_nama, stk_alamat, stk_contact_person, stk_telepon, stk_fax FROM satuan_kerja";
		String sql_count="SELECT COUNT(stk_id) FROM satuan_kerja";
		if(agc_id != null) {
			sql += " where agc_id='"+agc_id+"'";
			sql_count += " where agc_id='"+agc_id+"'";
		}		
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetSatker);
	}*/
	
	public static final ResultSetHandler<String[]> resultsetSatker = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
			tmp[0] = rs.getString("stk_nama");
			tmp[1] = rs.getString("stk_alamat");
			tmp[2] = rs.getString("stk_contact_person");
			tmp[3] = rs.getString("stk_telepon");
			tmp[4] = rs.getString("stk_fax");
			return tmp;
		}
	};
	
	
}
