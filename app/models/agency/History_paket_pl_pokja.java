package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;

@Table(name="EKONTRAK.HISTORY_PAKET_POKJA")
public class History_paket_pl_pokja extends BaseModel {

    @Id
    public Long pkt_id;

    @Id
    public Long pnt_id;

    public Long peg_id;

    public Date tgl_perubahan;

    public String alasan;


    public static List<History_paket_pl_pokja> findByPaket(Long pkt_id){
        return find("pkt_id = ? order by tgl_perubahan desc",pkt_id).fetch();
    }

    public String getNamaPokja(){
        return Query.find("SELECT pnt_nama FROM panitia WHERE pnt_id=?",String.class, pnt_id).first();
    }

    public String getNamaPegawai() {
        return Query.find("SELECT peg_nama FROM pegawai WHERE peg_id=?", String.class, peg_id).first();
    }

}
