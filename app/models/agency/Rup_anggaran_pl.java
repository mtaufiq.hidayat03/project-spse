package models.agency;

import models.common.SumberDana;

import java.util.Date;

public class Rup_anggaran_pl {
	
public Long id;
	
	public Long pktId;
    
	public Integer jenis;
    
	public Integer sumberDana;
    
	public String asalDanaApbn;
    
	public String mak;
    
	public Double pagu;
    
	public Date auditupdate;

	public SumberDana getSumberDana() {
		return SumberDana.values()[sumberDana - 1];
	}

}
