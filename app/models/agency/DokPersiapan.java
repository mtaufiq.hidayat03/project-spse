package models.agency;

import models.common.Tahap;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import play.utils.LogUtils;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Table(name="dok_persiapan")
public class DokPersiapan extends BaseModel{

    public static final String TAG = "DokPersiapan";

    @Id(sequence="seq_dok_persiapan", function="nextsequence")
    public Long dp_id;

    @Id
    public Integer dp_versi;

    public Long pkt_id;

    public String dp_dkh;

    public Long dp_spek;

    public String dp_sskk;

    public Long dp_sskk_attachment;

    public boolean dp_modified;

    @Transient
    public DaftarKuantitas dkh;

    @Transient
    public SskkContent sskkContent;

    /**
     * field ini untuk menyimpan informasi dokumen lainnya
     */
    public Long dp_lainnya;

    @Override
    protected void postLoad() {
        if (!CommonUtil.isEmpty(dp_dkh))
            dkh = CommonUtil.fromJson(dp_dkh, DaftarKuantitas.class);
        if(!CommonUtil.isEmpty(dp_sskk))
            sskkContent = CommonUtil.fromJson(dp_sskk,SskkContent.class);
    }

    public void prePersist() {
        super.prePersist();
        if (dkh != null)
            dp_dkh = CommonUtil.toJson(dkh);
        if (sskkContent != null)
            dp_sskk = CommonUtil.toJson(sskkContent);
    }

    @Transient
    public List<BlobTable> getDokSpek() {
        if(dp_spek == null){
            return null;
        }
        return BlobTableDao.listById(dp_spek);
    }

    @Transient
    public List<BlobTable> getDokSskkAttachment() {
        if(dp_sskk_attachment == null){
            return null;

        }
        return BlobTableDao.listById(dp_sskk_attachment);
    }

    @Transient
    public List<BlobTable> getDokLainnya() {
        if(dp_lainnya == null){
            return null;
        }
        return BlobTableDao.listById(dp_lainnya);
    }

    /**
     * Mendapatkan Daftar Kuantitas terakhir yang berlaku
     * digunakan oleh Apendo
     * @return
     */
    @Transient
    public DaftarKuantitas getRincianHPS() {
        String content = Query.find("SELECT dp_dkh FROM dok_persiapan WHERE pkt_id=? AND dp_dkh is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", String.class, pkt_id).first();
        if (!CommonUtil.isEmpty(content))
            return CommonUtil.fromJson(content, DaftarKuantitas.class);

        return null;
    }

    @Transient
    public Long getSpek() {
        return Query.find("SELECT dp_spek FROM dok_persiapan WHERE pkt_id=? AND dp_spek is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public Long getSpekAdendum() {
        return Query.find("SELECT dp_spek FROM dok_persiapan WHERE pkt_id=? AND dp_spek is not null ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public Long getSskkAttachment() {
        return Query.find("SELECT dp_sskk_attachment FROM dok_persiapan WHERE pkt_id=? AND dp_sskk_attachment is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public Long getSskkAttachmentAdendum() {
        return Query.find("SELECT dp_sskk_attachment FROM dok_persiapan WHERE pkt_id=? AND dp_sskk_attachment is not null ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public SskkContent getOldSskkContent() {
        String content = Query.find("SELECT dp_sskk FROM dok_persiapan WHERE pkt_id=? AND dp_sskk is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", String.class, pkt_id).first();
        if (!CommonUtil.isEmpty(content))
            return CommonUtil.fromJson(content, SskkContent.class);
        return null;
    }

    public static DokPersiapan findByPaket(Long pkt_id) {
        return find("pkt_id=? and dp_versi=?", pkt_id,1).first();
    }

    public static DokPersiapan findOrCreateByPaket(Long pkt_id){
        Paket paket = Paket.findById(pkt_id);
        return findOrCreateByPaket(paket);
    }

    public static DokPersiapan findOrCreateByPaket(Paket paket){
        Lelang_seleksi lelang = Lelang_seleksi.findByPaket(paket.pkt_id);
        DokPersiapan dokPersiapan = paket.getDokPersiapan();

        if(dokPersiapan == null){
            dokPersiapan = new DokPersiapan();
            dokPersiapan.pkt_id = paket.pkt_id;

            if(!paket.isFlag43()){
                Dok_lelang dokLelang = Dok_lelang.findBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
                Dok_lelang_content dokLelangContent = dokLelang.getDokLelangContent();
                dokPersiapan.dp_versi = dokLelangContent.dll_versi;
            }else{
                dokPersiapan.dp_versi = 1;
            }

            dokPersiapan.save();
        }

        return dokPersiapan;

    }

    public void simpanHps(Paket paket, String data, boolean fixed, boolean enableViewHps) throws Exception{
        DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,fixed);
        if (dk.total == null)
            throw new Exception(Messages.get("agency.abmr_hps"));
        else if (dk.total > paket.pkt_pagu) {
            throw new Exception(Messages.get("agency.nt_hps_mp"));
        } else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket

            paket.pkt_hps = dk.total;
            paket.pkt_hps_enable = enableViewHps;
            paket.save();

            dkh = dk;
            dp_modified = true;

            if(paket.ukpbj_id != null || isAdendum()){
                transferHpsToDokLelang();
            }

            save();

        }
    }

    public Long simpanBlobSpek(List<BlobTable> blobList) throws Exception{
        Long newId = getNextSequenceValue("seq_epns");

        for (BlobTable blob : blobList){
            BlobTable newBlob = new BlobTable();
            newBlob.blb_id_content = newId;
            newBlob.blb_versi = blob.blb_versi;
            newBlob.blb_date_time = blob.blb_date_time;
            newBlob.blb_mirrored = blob.blb_mirrored;
            newBlob.blb_path = blob.blb_path;
            newBlob.blb_engine = blob.blb_engine;
            newBlob.save();

            if(newId == null){
                newId = newBlob.blb_id_content;
            }
        }
        return newId;
    }


    public Long mergeAttachment(List<BlobTable> blobList) throws Exception{
        Long newId = getNextSequenceValue("seq_epns");

        int i = 0;
        for (BlobTable blob : blobList){
            BlobTable newBlob = new BlobTable();
            newBlob.blb_id_content = newId;
            newBlob.blb_versi = i++;
            newBlob.blb_date_time = blob.blb_date_time;
            newBlob.blb_mirrored = blob.blb_mirrored;
            newBlob.blb_path = blob.blb_path;
            newBlob.blb_engine = blob.blb_engine;
            newBlob.save();

            if(newId == null){
                newId = newBlob.blb_id_content;
            }
        }

        return newId;
    }


    public UploadInfo simpanSpek(File file) throws Exception {
        String path = sanitizedFilename(file);
        File newFile = new File(path);
//        BlobTable model = findByFilename(newFile.getName(), getDokSpek());
//        if (model != null) {
//            LogUtil.debug("DokLelangContent", "filename's duplicated");
//            return null;
//        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dp_spek != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_spek);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dp_spek = blob.blb_id_content;

        Paket paket = Paket.findById(pkt_id);
        if (paket.ukpbj_id != null || paket.isFlag42()) {
            transferDokPersiapanToDokLelang();
        }

        save();

        return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanSskkAttachment(File file) throws Exception {
        String path = sanitizedFilename(file);
        File newFile = new File(path);
//        BlobTable model = findByFilename(newFile.getName(), getDokSskkAttachment());
//        if (model != null) {
//            LogUtil.debug("DokLelangContent", "filename's duplicated");
//            return null;
//        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dp_sskk_attachment != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_sskk_attachment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }

        dp_sskk_attachment = blob.blb_id_content;

        Paket paket = Paket.findById(pkt_id);

        if(paket.ukpbj_id != null || paket.isFlag42()){
            transferSskkToDokLelang();
        }

        save();


        return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanLainnya(File file) throws Exception {
        String path = sanitizedFilename(file);
        File newFile = new File(path);
//        BlobTable model = findByFilename(newFile.getName(), getDokSpek());
//        if (model != null) {
//            LogUtil.debug("DokLelangContent", "filename's duplicated");
//            return null;
//        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dp_lainnya != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_lainnya);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dp_lainnya = blob.blb_id_content;

        Paket paket = Paket.findById(pkt_id);
        if (paket.ukpbj_id != null || paket.isFlag42()) {
            transferDokPersiapanToDokLelang();
        }
        save();

        return UploadInfo.findBy(blob);
    }

    public BlobTable findByFilename(String filename,List<BlobTable> blobs) {
        if (CommonUtil.isEmpty(blobs)) {
            LogUtil.debug("DokLelangContent", "total blob is empty or object null");
            return null;
        }
        for (BlobTable model : blobs) {
            LogUtil.debug("DokLelangContent", filename + " contains " + model.blb_nama_file);
            if (model.blb_nama_file.equalsIgnoreCase(filename)) {
                return model;
            }
        }
        return null;
    }

    public String sanitizedFilename(File file) {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;
    }

    public boolean isAdendum(){
        return dp_versi > 1;
    }

    public boolean isEditable(Paket paket, Date currentDate){
	      if(paket.pkt_status.isSedangLelang()) {
	    	  Lelang_seleksi lelang = Lelang_seleksi.findByPaket(pkt_id);
              if(lelang.isPrakualifikasi()) {
                  Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
                  Evaluasi evakualifikasi = Evaluasi.findKualifikasi(lelang.lls_id);
                  if(pembuktian != null && evakualifikasi != null && pembuktian.eva_status.isSelesai() && evakualifikasi.eva_status.isSelesai())
                      return false;
                  List<Jadwal> list = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PENGUMUMAN_HASIL_PRA, Tahap.PENGUMUMAN_HASIL_PRA_DAN_AMBIL_DOK);
                  if(!CollectionUtils.isEmpty(list)) {
                      Jadwal jadwal = list.get(0);
                      if(jadwal != null && jadwal.dtj_tglawal != null	&& jadwal.dtj_tglawal.after(currentDate)) {
                          // jika sebelum jadwal pengumuman hasil pra, maka dokumen lelang masih dianggap draft bukan adendum
                          return true;
                      }
                  }
              }
              return false;
	      }
	      return paket.isEditable();
    }

    public void transferDokPersiapanToDokLelang() {
        LogUtils.debug(TAG, "transfer document to lelang");
        Lelang_seleksi lelang = Lelang_seleksi.findByPaket(this.pkt_id);
        Dok_lelang dokLelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        Dok_lelang_content dokLelangContent = Dok_lelang_content.findNCreateBy(dokLelang.dll_id, lelang);

        dokLelangContent.dkh = this.dkh;
        dokLelangContent.dll_spek = this.dp_spek;
        dokLelangContent.sskk_content = this.sskkContent;
        dokLelangContent.dll_sskk_attachment = this.dp_sskk_attachment;
        dokLelangContent.dll_lainnya = this.dp_lainnya;
        dokLelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dokLelangContent);
    }

    public void transferHpsToDokLelang() {
        Lelang_seleksi lelang = Lelang_seleksi.findByPaket(this.pkt_id);
        Dok_lelang dokLelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        Dok_lelang_content dokLelangContent = Dok_lelang_content.findNCreateBy(dokLelang.dll_id, lelang);

        dokLelangContent.dkh = this.dkh;
        dokLelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dokLelangContent);
    }

    public void transferSskkToDokLelang() {
        Lelang_seleksi lelang = Lelang_seleksi.findByPaket(this.pkt_id);
        Dok_lelang dokLelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        Dok_lelang_content dokLelangContent = Dok_lelang_content.findNCreateBy(dokLelang.dll_id, lelang);

        dokLelangContent.sskk_content = this.sskkContent;
        dokLelangContent.dll_sskk_attachment = this.dp_sskk_attachment;

        dokLelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dokLelangContent);
    }

    public boolean lengkap(){
        return dp_sskk_attachment != null && dp_dkh != null && dp_spek != null;
    }


}
