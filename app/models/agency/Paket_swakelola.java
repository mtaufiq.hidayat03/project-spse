package models.agency;

import ext.FormatUtils;
import ext.RupiahBinder;
import models.AnggaranSwakelola;
import models.agency.contracts.PaketSwakelolaContract;
import models.agency.contracts.SirupContract;
import models.common.Active_user;
import models.common.MetodePemilihan;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.Swakelola_seleksi;
import models.sirup.PaketSwakelolaSirup;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Model {@code Paket} merepresentasikan tabel paket pada database.
 *
 * @author wahid
 */
@Table(name = "ekontrak.PAKET_SWAKELOLA")
public class Paket_swakelola extends BaseModel implements PaketSwakelolaContract {
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusPaketSwakelola {

		DRAFT(0,Messages.get("paket.paket_pl0")),
		SEDANG_LELANG(1,Messages.get("paket.paket_pl1")),
		SELESAI_LELANG(2,Messages.get("paket.paket_pl2")), // di versi 3.5 belum pernah dipakai
		ULANG_LELANG(3,Messages.get("paket.paket_pl3")),
		LELANG_DITOLAK(4,Messages.get("paket.paket_pl4"));
		
		public final Integer id;
		public final String label;
		
		StatusPaketSwakelola(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}	
		
		public static StatusPaketSwakelola fromValue(Integer value){
			StatusPaketSwakelola status = null;
			if(value != null){
				switch(value.intValue()){
					case 0:status = DRAFT;break;
					case 1:status = SEDANG_LELANG;break;
					case 2:status = SELESAI_LELANG;break;
					case 3:status = ULANG_LELANG;break;
					case 4:status = LELANG_DITOLAK;break;
					default:status = DRAFT;break;
				}
			}
			return status;
		}
		
		public boolean isDraft(){
			return this == DRAFT;
		}
		
		public boolean isSedangLelang(){
			return this == SEDANG_LELANG;
		}
		
		public boolean isSelesaiLelang(){
			return this == SELESAI_LELANG;
		}
		
		public boolean isUlangLelang(){
			return this == ULANG_LELANG;
		}
		
		public boolean isLelangDitolak(){
			return this == LELANG_DITOLAK;
		}
	}
	
	/**
	 * Kode Paket
	 */
	@Id(sequence="ekontrak.seq_paket_swakelola", function="nextsequence")
	public Long swk_id;
	/**
	 * Nama paket
	 */
	@Required
	public String pkt_nama;
	/**
	 * Nilai pagu paket
	 */
	@Required
	@As(binder=RupiahBinder.class)
	public Double pkt_pagu;
	/**
	 * Nilai HPS (Harga Perkiraan Sendiri) paket
	 */
	@As(binder=RupiahBinder.class)
	public Double pkt_hps;
	
	/**
	 * dokumen terkait pembuatan paket
	 * (info: sudah tidak dipakai lagi)
	 */
	public Long pkt_id_attachment;
	
	/**
	 * tanggal realisasi paket 
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_realisasi;
	
	/**
	 * tanggal paket di kirimkan dari panitia ke ppk, 
	 * semenjak versi 3.5 di set saat pembuatan paket karena tidak ada persetujuan ppk dari panitia
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_assign;
	/**
	 * Tanggal pembuatan paket
	 */
	public Date pkt_tgl_buat;
	/**
	 * Tanggal persetujuan paket oleh PPK
	 */
	public Date pkt_tgl_ppk_setuju;
	/**
	 * Status paket
	 */
	@Required
	public StatusPaketSwakelola pkt_status;
	
	/**
	 * flag jenis paket berdasarkan aturan dan versi
	 * 0 : Jenis paket dibuat dengan spse versi 2.x, memakai kepres 80
	 * 1 : Jenis paket dibuat dengan spse versi 3.x, memakai perpres 54 & 70
	 * 2 : Jenis paket dibuat dengan spse versi 4.x, memakai perpres 54 & 70
	 */
	@Required
	public Integer pkt_flag = 0;
	/**
	 * Kualifikasi pelelangan
	 */
	public String kls_id;
	
	/**
	 * Kepanitiaan pembuat paket lelang
	 */
//	@Required
//	public Long pnt_id;
	/**
	 * Relasi paket terhadap PPK
	 */
	@Required(message="ppk belum dipilih")
	public Long ppk_id;
	/**
	 * Relasi paket terhadap PP
	 */
	@Required
	public Long pp_id;
	/**
	 * Satuan kerja kepemilikan paket
	 */
	@Required(message="Satuan Kerja wajib diisi")
	public Long stk_id;

	
	/**
	 * Kategori pengadaan/lelang
	 */
//	@Required
//	public Kategori kgr_id;
	/**
	 * Nomor surat rencana pelaksanaan
	 */
	@Required
	public String pkt_no_spk;	
	
	//relasi ke RupPaketPenyedia
	public Long rup_id;

	@Transient
	private PaketPpkSwakelola paketPpk;
	
	public Integer unspsc_id;
	
	@Transient
	private Ppk ppk;
	@Transient
	private Pp pp;
	@Transient
	private Satuan_kerja satuan_kerja;
	@Transient
	private RupPaketSwakelola rup;

	@Deprecated
	@Transient
	public List<AnggaranSwakelola> anggaranList;
	@Transient
	public List<Paket_anggaran_swakelola> paketAnggarans;

	@Transient
	private List<Paket_swakelola_lokasi> locations;

	@Override
	public Long getPktId() {
		return this.swk_id;
	}

	public List<Paket_anggaran_swakelola> getPaketAnggarans() {
		if (CollectionUtils.isEmpty(this.paketAnggarans)) {
			this.paketAnggarans = Paket_anggaran_swakelola.findByPaket(getPktId());
		}
		return paketAnggarans;
	}
	
	public Ppk getPpk() {
		if(ppk == null)
			ppk = Ppk.findById(ppk_id);
		return ppk;
	}

	/**
	 * return true if this package is created using new "perpres 16 2018"*/
	public boolean isFlag43() {
		return this.pkt_flag == 3;
	}
	
	public Pp getPp() {
		if(pp == null)
			pp = Pp.findById(pp_id);
		return pp;
	}

	@Override
	public List<AnggaranSwakelola> getAnggarans() {
		if (CommonUtil.isEmpty(anggaranList)) {
			withAnggarans();
		}
		return this.anggaranList;
	}

	@Override
	public List<Paket_swakelola_lokasi> getPaketLocations() {
		if (CommonUtil.isEmpty(locations)) {
			withLocations();
		}
		return locations;
	}

//	@Override
	public PaketPpkSwakelola getPaketPpk() {
		if (paketPpk == null) {
			withPaketPpk();
		}
		return paketPpk;
	}
	
	public Satuan_kerja getSatuan_kerja() {
		if(satuan_kerja == null)
			satuan_kerja = Satuan_kerja.findById(stk_id);
		return satuan_kerja;
	}	
	
	public RupPaketSwakelola getRup() {
		if(rup == null)
			rup = RupPaketSwakelola.findById(rup_id);
		return rup;
	}

	public void setFlag43() {
		this.pkt_flag = 3;
	}
	
	public List<Paket_swakelola_lokasi> getPaketLokasi()
	{
		return Paket_swakelola_lokasi.find("swk_id=?",swk_id).fetch();
	}
	
	public static void simpanBuatPaket (Paket_swakelola swakelola, Paket_swakelola_lokasi[] lokasi){
		Paket_anggaran_swakelola anggaran = Paket_anggaran_swakelola.find("swk_id =?", swakelola.swk_id).first();
		if (swakelola.pkt_flag == null)
			swakelola.setFlag43(); // flag 2 untuk paket yang dibuat dengan versi 4
		if (swakelola.pkt_tgl_buat == null)
			swakelola.pkt_tgl_buat = controllers.BasicCtr.newDate();
		if (swakelola.pkt_hps == null)
			swakelola.pkt_hps = Double.valueOf(0);
		if (swakelola.pkt_status == null)
			swakelola.pkt_status = StatusPaketSwakelola.DRAFT; // default status draft
		if (swakelola.pkt_tgl_assign == null)
			swakelola.pkt_tgl_assign = controllers.BasicCtr.newDate();
		if(swakelola.ppk_id == null)
			swakelola.ppk_id = anggaran.ppk_id;
		swakelola.save();
		if (swakelola.swk_id != null) {
			Paket_swakelola_lokasi obj;
			List<Long> saved = new ArrayList<>(lokasi.length);
			for (Paket_swakelola_lokasi lok : lokasi) {
				if (lok.pkl_id == null)
					obj = new Paket_swakelola_lokasi();
				else
					obj = Paket_swakelola_lokasi.findById(lok.pkl_id);
				obj.kbp_id = lok.kbp_id;
				obj.swk_id = swakelola.swk_id;
				obj.pkt_lokasi_versi = 1;
				if (!StringUtils.isEmpty(lok.pkt_lokasi)) {
					obj.pkt_lokasi = lok.pkt_lokasi;
					obj.save();					
				}
				if(obj.pkl_id != null)
					saved.add(obj.pkl_id);
			}
			if(!CommonUtil.isEmpty(saved)) {
				String join = StringUtils.join(saved, ",");
				if(!join.isEmpty())
					Query.update("delete from ekontrak.paket_swakelola_lokasi where swk_id=? and pkl_id not in ("+join+ ')', swakelola.swk_id);
			}
//			Swakelola_seleksi.buatSwakelolaBaru(swakelola);
		}
	}
	
	/**
	 * 
	 * @param rup_id
	 * @return
	 */
	public static Paket_swakelola buatPaketSwakelolaFromSirup(Long rup_id, Long paketId, Long ppkId) {
		PaketSwakelolaSirup rup = PaketSwakelolaSirup.findById(rup_id);
		Paket_swakelola paket = null;
		if(paketId == null) {
			paket = new Paket_swakelola();
			paket.pkt_nama = rup.nama;
			paket.pkt_pagu = rup.jumlah_pagu;
			paket.pkt_hps = Double.valueOf(0);
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
			paket.pkt_status = StatusPaketSwakelola.DRAFT;
			paket.setFlag43(); // default flag paket yang dibuat diversi 4.3
			paket.ppk_id = ppkId;
		} else {
			paket = Paket_swakelola.findById(paketId);
			if(!paket.pkt_nama.contains(rup.nama)) // jika nama paket sama tidak perlu ditambahkan
				paket.pkt_nama += ',' + rup.nama;
			paket.pkt_pagu += rup.jumlah_pagu;
		}

		paket.save();
		Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
		Paket_satker_swakelola pss = new Paket_satker_swakelola();
		pss.stk_id = satker.stk_id;
		pss.swk_id = paket.swk_id;
		pss.rup_id = rup.id;
		pss.save();
		Active_user user = Active_user.current();
		paket.createPaketPpk(user);
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)){
			AnggaranSwakelola anggaran;
			Paket_anggaran_swakelola paket_anggaran_swakelola = null;
			for(PaketSwakelolaSirup.PaketSirupAnggaran rup_anggaran:rup.paket_anggaran_json) {
				anggaran = new AnggaranSwakelola();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup.tahun;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = satker.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran_swakelola = new Paket_anggaran_swakelola();
				paket_anggaran_swakelola.swk_id = paket.swk_id;
				paket_anggaran_swakelola.ang_id =anggaran.ang_id;
				paket_anggaran_swakelola.ppk_id = user.ppkId;
				paket_anggaran_swakelola.rup_id = rup.id;
				paket_anggaran_swakelola.save();
		}

		}

		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			for(PaketSwakelolaSirup.PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				Paket_swakelola_lokasi pkt_lokasi = new Paket_swakelola_lokasi();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.swk_id = paket.swk_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = Integer.valueOf(1);
				pkt_lokasi.save();
			}
		}
		// langsung buat lelang baru
		Swakelola_seleksi.buatSwakelolaBaru(paket);
		return paket;
	}
	
	public static final ResultSetHandler<String[]> resultsetPaketpp = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Long plId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isPl());
			StatusPaketSwakelola status = StatusPaketSwakelola.fromValue(rs.getInt("pkt_status"));
			String[] tmp = new String[8];
			tmp[0] = rs.getString("swk_id");		
			tmp[1] = rs.getString("pkt_nama");
			if (status.isDraft()) {
				tmp[2] = "Draft";
			} else  {
				if (jadwal.equalsIgnoreCase("Swakelola Sudah Selesai") && !status.isDraft()) {
					tmp[2] = "Swakelola Sudah Selesai";
				} else {
					tmp[2] = status.label;
				}
			} 
			tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
			tmp[4] = rs.getString("stk_nama");
			tmp[5] = plId.toString();
			tmp[6] = pkt_flag.toString();
			tmp[7] = pemilihan.label;
			return tmp;
		}
	};
	
	public static String getNamaPaketFromlelang(Long lelangId) {
		return Query.find("select pkt_nama from ekontrak.paket where pkt_id IN (select pkt_id from ekontrak.nonlelang_seleksi where lls_id=?)", String.class, lelangId).first();
	}

	public static Paket_swakelola findBy(Long swaId) {
		return Query.find("SELECT * FROM ekontrak.paket_swakelola WHERE swk_id IN (SELECT swk_id FROM ekontrak.swakelola_seleksi WHERE lls_id=?)", Paket_swakelola.class, swaId).first();
	}

	@Transient
	public String getSumberDana() {
		List<String> result =Query.find("select distinct (a.sbd_id) from ekontrak.anggaran_swakelola a, ekontrak.paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, swk_id).fetch();
		return StringUtils.join(result, ",");
	}

	@Transient
	public String getTahunAnggaran() {
		List<String> result = Query.find("select distinct (a.ang_tahun) from ekontrak.anggaran_swakelola a, ekontrak.paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, swk_id).fetch();
		return StringUtils.join(result, ",");
	}

	public static List<PaketSwakelolaSirup> getRupPLList(Long pktId) {
		return Query.find("SELECT rup.* FROM ekontrak.paket_swakelola_sirup AS rup JOIN ekontrak.paket_satker_swakelola AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.swk_id=? ORDER BY ps.pks_id ASC", PaketSwakelolaSirup.class, pktId).fetch();
	}

//	public List<RupPaketSwakelola> getRupList() {
//		return RupPaketSwakelola.find("id IN (SELECT rup_id FROM ekontrak.paket_satker_swakelola WHERE swk_id=?)", swk_id).fetch();
//	}

	public List<? extends SirupContract> getSirupListForUI() {
		if (isFlag43()) {
			return getRupPLList();
		}
		return getRupList42();
	}

	public List<PaketSwakelolaSirup> getRupPLList() {
		return getRupPLList(swk_id);
	}

	public List<RupPaketSwakelola> getRupList42() {
		return Query.find("SELECT rup.* FROM rup_paket AS rup JOIN ekontrak.paket_satker_swakelola AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.swk_id=? ORDER BY ps.pks_id ASC", RupPaketSwakelola.class, swk_id).fetch();
	}

	@Override
	public void setAnggarans(List<AnggaranSwakelola> items) {
		this.anggaranList = items;
	}

	@Override
	public void setPaketPpk(PaketPpkSwakelola model) {
		this.paketPpk = model;
	}

	public boolean isCreatedByPpk() {
		return isFlag43() && getPaketPpk() != null;
	}

	public boolean isAllowEditPpk() {
		return pkt_status.isDraft() && !isCreatedByPpk();
	}

	public String getKodeRup() {
		return Paket_satker_swakelola.getKodeRupPaket(swk_id);
	}

	public boolean isEditable(){
		return pkt_status.isDraft() && !Active_user.current().isPP();
	}
	@Override
	public void setPaketLocations(List<Paket_swakelola_lokasi> items) {
		this.locations = items;
	}

	public String getNamaInstansi() {
		return Paket_satker_swakelola.getNamaInstansiPaket(swk_id);
	}

	public String getNamaSatker() {
		return Paket_satker_swakelola.getNamaSatkerPaket(swk_id);
	}

	public String getAlamatSatker() {
		return Paket_satker_swakelola.getAlamatSatkerPaket(swk_id);
	}

	public String getNamaKabupaten() {
		return Paket_satker_swakelola.getNamaKabupaten(swk_id);
	}

}
