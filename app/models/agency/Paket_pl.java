package models.agency;

import ext.FormatUtils;
import ext.RupiahBinder;
import models.agency.contracts.PaketPlContract;
import models.agency.contracts.SirupContract;
import models.common.*;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.SurveyHargaPl;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import models.sirup.PaketSirup;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import utils.LogUtil;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model {@code Paket} merepresentasikan tabel paket pada database.
 *
 * @author wahid
 */

@Table(name = "EKONTRAK.PAKET")
public class Paket_pl extends BaseModel implements PaketPlContract {
	
	@Enumerated(EnumType.ORDINAL)
	public enum StatusPaket {

		DRAFT(0,Messages.get("paket.paket_pl0")),
		SEDANG_LELANG(1,Messages.get("paket.paket_pl1")),
		SELESAI_LELANG(2,Messages.get("paket.paket_pl2")), // di versi 3.5 belum pernah dipakai
		ULANG_LELANG(3,Messages.get("paket.paket_pl3")),
		LELANG_DITOLAK(4,Messages.get("paket.paket_pl4"));
		
		public final Integer id;
		public final String label;
		
		StatusPaket(int key, String label){
			this.id = Integer.valueOf(key);
			this.label = label;
		}	
		
		public static StatusPaket fromValue(Integer value){
			StatusPaket status = null;
			if(value != null){
				switch(value.intValue()){
					case 0:status = DRAFT;break;
					case 1:status = SEDANG_LELANG;break;
					case 2:status = SELESAI_LELANG;break;
					case 3:status = ULANG_LELANG;break;
					case 4:status = LELANG_DITOLAK;break;
					default:status = DRAFT;break;
				}
			}
			return status;
		}
		
		public boolean isDraft(){
			return this == DRAFT;
		}
		
		public boolean isSedangLelang(){
			return this == SEDANG_LELANG;
		}
		
		public boolean isSelesaiLelang(){
			return this == SELESAI_LELANG;
		}
		
		public boolean isUlangLelang(){
			return this == ULANG_LELANG;
		}
		
		public boolean isLelangDitolak(){
			return this == LELANG_DITOLAK;
		}

	}

	@Enumerated(EnumType.ORDINAL)
	public enum JenisPaketEnum {

		PL(0, "Pengadaan Langsung"),
		NON_SPK(1, "Non SPK/Paket Lainnya");

		public final Integer value;
		public final String label;

		private JenisPaketEnum(Integer value, String label) {
			this.value = value;
			this.label = label;
		}

		public static JenisPaketEnum fromValue(Integer value){
			JenisPaketEnum jenis = null;
			if(value!=null){
				switch (value.intValue()) {
					case 0:	jenis = PL;break;
					case 1:	jenis = NON_SPK;break;
				}
			}
			return jenis;
		}

	}

	@Override
	protected void preDelete(){
		if(pkt_status.isDraft()){
			Pl_seleksi pl = Pl_seleksi.findByPaket(pkt_id);
				Query.update("DELETE FROM ekontrak.jadwal WHERE lls_id = ?", pl.lls_id);
				Query.update("DELETE FROM ekontrak.persetujuan WHERE lls_id = ?", pl.lls_id);
				Query.update("DELETE FROM ekontrak.checklist WHERE dll_id IN (SELECT dll_id FROM ekontrak.dok_nonlelang WHERE lls_id = ?)", pl.lls_id);
				Query.update("DELETE FROM ekontrak.dok_nonlelang_content WHERE dll_id IN (SELECT dll_id FROM ekontrak.dok_nonlelang WHERE lls_id = ?)", pl.lls_id);
				Query.update("DELETE FROM ekontrak.dok_nonlelang WHERE lls_id = ?", pl.lls_id);
				Query.update("DELETE FROM ekontrak.draft_peserta_nonlelang WHERE lls_id = ?", pl.lls_id);
				Query.update("DELETE FROM ekontrak.nonlelang_seleksi WHERE lls_id = ?", pl.lls_id);
				Query.update("DELETE FROM ekontrak.non_spk_seleksi WHERE lls_id = ?", pl.lls_id);

				Query.update("DELETE FROM ekontrak.paket_lokasi WHERE pkt_id = ?", pkt_id);
				Query.update("DELETE FROM ekontrak.paket_ppk WHERE pkt_id = ?", pkt_id);
				Query.update("DELETE FROM ekontrak.paket_pp WHERE pkt_id = ?", pkt_id);
				Query.update("DELETE FROM ekontrak.paket_panitia WHERE pkt_id = ?", pkt_id);
				Query.update("DELETE FROM ekontrak.paket_anggaran WHERE pkt_id = ?", pkt_id);
				Query.update("DELETE FROM ekontrak.paket_satker WHERE pkt_id = ?", pkt_id);
				Query.update("DELETE FROM ekontrak.dok_persiapan WHERE pkt_id = ?", pkt_id);

		}

	}
	
	/**
	 * Kode Paket
	 */
	@Id(sequence="seq_paket", function="nextsequence")
	public Long pkt_id;
	/**
	 * Nama paket
	 */
	@Required
	public String pkt_nama;
	/**
	 * Nilai pagu paket
	 */
	@Required
	@As(binder=RupiahBinder.class)
	public Double pkt_pagu;
	/**
	 * Nilai HPS (Harga Perkiraan Sendiri) paket
	 */
	@As(binder=RupiahBinder.class)
	public Double pkt_hps;

	/**
	 * View HPS (Harga Perkiraan Sendiri) paket
	 */
	public Boolean pkt_hps_enable;
	
	/**
	 * dokumen terkait pembuatan paket
	 * (info: sudah tidak dipakai lagi)
	 */
	public Long pkt_id_attachment;
	
	/**
	 * tanggal realisasi paket 
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_realisasi;
	
	/**
	 * tanggal paket di kirimkan dari panitia ke ppk, 
	 * semenjak versi 3.5 di set saat pembuatan paket karena tidak ada persetujuan ppk dari panitia
	 * (info: sudah tidak dipakai lagi)
	 */
	public Date pkt_tgl_assign;
	/**
	 * Tanggal pembuatan paket
	 */
	public Date pkt_tgl_buat;
	/**
	 * Tanggal persetujuan paket oleh PPK
	 */
	public Date pkt_tgl_ppk_setuju;
	/**
	 * Status paket
	 */
	@Required
	public StatusPaket pkt_status;
	
	/**
	 * flag jenis paket berdasarkan aturan dan versi
	 * 0 : Jenis paket dibuat dengan spse versi 2.x, memakai kepres 80
	 * 1 : Jenis paket dibuat dengan spse versi 3.x, memakai perpres 54 & 70
	 * 2 : Jenis paket dibuat dengan spse versi 4.x, memakai perpres 54 & 70
	 */
	@Required
	public Integer pkt_flag = 0;
	/**
	 * Kualifikasi pelelangan
	 */
	public String kls_id;
	
	/**
	 * Kepanitiaan pembuat paket lelang
	 */
//	@Required
//	public Long pnt_id;
	/**
	 * Relasi paket terhadap PPK
	 */
	@Required(message="ppk belum dipilih")
	public Long ppk_id;
	/**
	 * Relasi paket terhadap PP
	 */
	@Required
	public Long pp_id;

	public Long pnt_id;
	/**
	 * Satuan kerja kepemilikan paket
	 */
	@Required(message="Satuan Kerja wajib diisi")
	public Long stk_id;
	
	/**
	 * Kategori pengadaan/lelang
	 */
	@Required
	public Kategori kgr_id;
	/**
	 * Nomor surat rencana pelaksanaan
	 */
	@Required
	public String pkt_no_spk;	
	
	//relasi ke RupPaketPenyedia
	public Long rup_id;
	
	public Integer unspsc_id;

//	@Required
	public Long ukpbj_id;

	public Boolean is_pkt_konsolidasi = false;

	public Long pkt_id_konsolidasi;

	public Integer pkt_jenis;

	@Transient
	private PaketPpkPl paketPpk;
	@Transient
	private Paket_pp paketPp;
	@Transient
	private Ukpbj ukpbj;
	@Transient
	private Pl_seleksi pl_seleksi;
	@Transient
	private Non_spk_seleksi spk_seleksi;
	@Transient
	DokPersiapanPl dokPersiapan;
	@Transient
	SurveyHargaPl suveyHarga;

	@Transient
	private List<Paket_pl_lokasi> locations;
	/**
	 * @deprecated dikarenakan penggunaan relasi antar class untuk memudahkan developer memahami relasinya,
	 * untuk mendapatkan anggaran harap menggunakan paketAnggaran.anggaran*/
	@Deprecated
	@Transient
	public List<Anggaran> anggaranList;
	@Transient
	public List<Paket_anggaran_pl> paketAnggarans;

	/**
	 * flag paket persetujuan
	 * hanya paket yang dibuat pada versi 4 ke atas (pkt_flag!=1) memakai persetujuan
	 * @return
	 */
	@Transient
	public boolean isPaket_persetujuan() {
		return pkt_flag != null && pkt_flag.intValue() != 1;
	}
	
	@Transient
	private Ppk ppk;
	@Transient
	private Pp pp;
	@Transient
	private Panitia panitia;
	@Transient
	private Satuan_kerja satuan_kerja;
	@Transient
	private Rup_paket rup;

	public boolean isKonsolidasi(){
		if(is_pkt_konsolidasi == null){
			return false;
		}

		return is_pkt_konsolidasi;
	}

	@Override
	public Long getPktId() {
		return this.pkt_id;
	}

	@Override
	public Long getUkpbjId() {
		return this.ukpbj_id;
	}

	@Override
	public List<Paket_pl_lokasi> getPaketLocations() {
		if (CommonUtil.isEmpty(locations)) {
			withLocations();
		}
		return locations;
	}

	@Override
	public List<Anggaran> getAnggarans() {
		if (CommonUtil.isEmpty(anggaranList)) {
			withAnggarans();
		}
		return this.anggaranList;
	}

	public DokPersiapanPl getDokPersiapan(){
		if(dokPersiapan == null){
			dokPersiapan = DokPersiapanPl.findLastByPaket(pkt_id);
		}
		return dokPersiapan;
	}

	public SurveyHargaPl getDokSuvey(){
		if(suveyHarga == null){
			suveyHarga = SurveyHargaPl.findLastByPaket(pkt_id);
		}
		return suveyHarga;
	}

	@Override
	public PaketPpkPl getPaketPpk() {
		if (paketPpk == null) {
			withPaketPpk();
		}
		return paketPpk;
	}

	@Override
	public Paket_pp getPaketPp() {
		if (paketPp == null) {
			withPaketPp();
		}
		return paketPp;
	}

	@Override
	public Ukpbj getUkpbj() {
		if (ukpbj == null) {
			ukpbj = Ukpbj.findById(ukpbj_id);
		}
		return ukpbj;
	}

	public List<Paket_anggaran_pl> getPaketAnggarans() {
		if (CollectionUtils.isEmpty(this.paketAnggarans)) {
			Integer newestVersion = getVersiPaketAnggaran(getPktId());
			this.paketAnggarans = Paket_anggaran_pl.findByPaketAndVersion(getPktId(), newestVersion);
		}
		return paketAnggarans;
	}

	@Override
	public Pl_seleksi getPlSeleksi() {
		if(pl_seleksi == null){
			pl_seleksi = Pl_seleksi.findByPaket(pkt_id);
		}
		return pl_seleksi;
	}

//	@Override
	public Non_spk_seleksi getSpkSeleksi() {
		if(spk_seleksi == null){
			spk_seleksi = Non_spk_seleksi.findByPaket(pkt_id);
		}
		return spk_seleksi;
	}

	@Override
	public void setAnggarans(List<Anggaran> items) {
		this.anggaranList = items;
	}

	@Override
	public void setPaketLocations(List<Paket_pl_lokasi> items) {
		this.locations = items;
	}

	@Override
	public void setPaketPpk(PaketPpkPl model) {
		this.paketPpk = model;
	}

	@Override
	public void setPaketPp(Paket_pp model) {
		this.paketPp = model;
	}

	@Override
	public void setUkpbj(Ukpbj model) {
		this.ukpbj = model;
	}

	@Override
	public void setPlSeleksi(Pl_seleksi model) {
		this.pl_seleksi = model;
	}

	public void setNonSplSeleksi(Non_spk_seleksi model) {
		this.spk_seleksi = model;
	}
	
	public Ppk getPpk() {
		if(ppk == null)
			ppk = Ppk.findById(ppk_id);
		return ppk;
	}

	public void setFlag43() {
		this.pkt_flag = 3;
	}

	public void setFlag42() {
		this.pkt_flag = 2;
	}

	/**
	 * return true if this package is created using new "perpres 16 2018"*/
	public boolean isFlag43() {
		return this.pkt_flag == 3;
	}

	public Pp getPp() {
		if(pp == null)
			pp = Pp.findById(pp_id);
		return pp;
	}

	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}
	
	public Satuan_kerja getSatuan_kerja() {
		if(satuan_kerja == null)
			satuan_kerja = Satuan_kerja.findById(stk_id);
		return satuan_kerja;
	}

	public String getKodeRup() {
		return Paket_satker_pl.getKodeRupPaket(pkt_id);
	}

	public String getNamaInstansi() {
		return Paket_satker_pl.getNamaInstansiPaket(pkt_id);
	}

	public String getNamaSatker() {
		return Paket_satker_pl.getNamaSatkerPaket(pkt_id);
	}

	public String getAlamatSatker() {
		return Paket_satker_pl.getAlamatSatkerPaket(pkt_id);
	}

	public String getNamaKabupaten() {
		return Paket_satker_pl.getNamaKabupaten(pkt_id);
	}

	public List<? extends SirupContract> getSirupListForUI() {
		if (isFlag43()) {
			return getRupList();
		}
		return getRupList42();
	}

	public List<PaketSirup> getRupListByPpk(long ppkId) {
		return getRupListByPpk(pkt_id, ppkId);
	}

	public static List<PaketSirup> getRupListByPpk(long pktId, long ppkId) {
		return Query.find("SELECT rup.* FROM paket_sirup AS rup JOIN ekontrak.paket_satker AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.pkt_id=? AND id IN (select rup_id FROM ekontrak.paket_anggaran WHERE pkt_id=? AND ppk_id=?)\n" +
				"ORDER BY ps.pks_id ASC", PaketSirup.class, pktId, pktId, ppkId).fetch();
	}

	public List<Rup_paket> getRupListByPpk42(long ppkId) {
		return getRupListByPpk42(pkt_id, ppkId);
	}

	public static List<Rup_paket> getRupListByPpk42(long pktId, long ppkId) {
		return Query.find("SELECT rup.* FROM rup_paket AS rup JOIN ekontrak.paket_satker AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.pkt_id=? AND id IN (select rup_id FROM ekontrak.paket_anggaran WHERE pkt_id=? AND ppk_id=?)\n" +
				"ORDER BY ps.pks_id ASC", Rup_paket.class, pktId, pktId, ppkId).fetch();
	}

	public List<PaketSirup> getRupList() {
		return getRupList(pkt_id);
	}

	public List<Pegawai> getPpkList() {
		return Pegawai.find("peg_id in (SELECT peg_id FROM ppk k, ekontrak.paket_anggaran p WHERE k.ppk_id=p.ppk_id AND p.pkt_id=?)", pkt_id).fetch();
	}

	public static List<PaketSirup> getRupList(Long pktId) {
		Integer lastVersion = getVersiPaketAnggaran(pktId);
		return Query.find("SELECT rup.* FROM paket_sirup AS rup\n" +
				"JOIN ekontrak.paket_anggaran AS pa ON pa.rup_id=rup.id\n" +
				"WHERE pa.pkt_id=? AND pa.pkt_ang_versi=?", PaketSirup.class, pktId, lastVersion).fetch();
	}

	private static Integer getVersiPaketAnggaran(Long pktId) {
	    Paket_anggaran_pl paket_anggaran = Paket_anggaran_pl.find("pkt_id=? order by pkt_ang_versi desc", pktId).first();
	    return paket_anggaran == null || paket_anggaran.pkt_ang_versi == null ? 1 : paket_anggaran.pkt_ang_versi;
	}

	public List<Rup_paket> getRupList42() {
		return Query.find("SELECT rup.* FROM rup_paket AS rup JOIN ekontrak.paket_satker AS ps ON rup.id=ps.rup_id\n" +
				"WHERE ps.pkt_id=? ORDER BY ps.pks_id ASC", Rup_paket.class, pkt_id).fetch();
	}

	public boolean isPaketKonsolidasi() {
		return Paket_satker_pl.count("pkt_id=?", pkt_id) > 1;
	}
	
	public List<Paket_pl_lokasi> getPaketLokasi()
	{
		return Paket_pl_lokasi.find("pkt_id=?",pkt_id).fetch();
	}
	
	public static Paket_pl simpanBuatPaket (Paket_pl paket, Paket_pl_lokasi[] lokasi){
		if (paket.pkt_flag == null)
			paket.setFlag43();  // flag 3 untuk paket yang dibuat dengan versi 4.3
		if (paket.pkt_tgl_buat == null)
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
		if (paket.pkt_hps == null)
			paket.pkt_hps = Double.valueOf(0);
		if (paket.pkt_status == null)
			paket.pkt_status = StatusPaket.DRAFT; // default status draft
		if (paket.kgr_id == null)
			paket.kgr_id = Kategori.PENGADAAN_BARANG; // default pengadaan barang jasa
		if (paket.pkt_tgl_assign == null)
			paket.pkt_tgl_assign = controllers.BasicCtr.newDate();
		paket.save();
		if (!paket.isFlag43()) {
			Paket_pp paket_pp = Paket_pp.find("pkt_id=? and pp_id=?", paket.pkt_id, paket.pp_id).first();
			if (paket_pp == null) {
				paket_pp = new Paket_pp();
				paket_pp.save();
			}
		}
			Paket_pl_lokasi obj = null;
			List<Long> saved = new ArrayList<Long>(lokasi.length);
			for (Paket_pl_lokasi lok : lokasi) {
				if (lok.pkl_id == null)
					obj = new Paket_pl_lokasi();
				else
					obj = Paket_pl_lokasi.findById(lok.pkl_id);
				obj.kbp_id = lok.kbp_id;
				obj.pkt_id = paket.pkt_id;
				obj.pkt_lokasi_versi = 1;
				if (!StringUtils.isEmpty(lok.pkt_lokasi)) {
					obj.pkt_lokasi = lok.pkt_lokasi;
					obj.save();					
				}
				saved.add(obj.pkl_id);
			}
			if(!CommonUtil.isEmpty(saved)) {
				String join = StringUtils.join(saved, ",");
				if(!join.isEmpty())
					Query.update("delete from ekontrak.paket_lokasi where pkt_id=? and pkl_id not in ("+join+ ')', paket.pkt_id);
			}
			Pl_seleksi lls = Pl_seleksi.buatLelangBaru(paket, null);
			LogUtil.debug("TAG", lls);
			paket.setPlSeleksi(lls);
			return paket;
//		}
	}

	public static void simpanBuatPaketNonSpk (Paket_pl paket, Paket_pl_lokasi[] lokasi){
		Paket_anggaran_pl anggaran_pl = Paket_anggaran_pl.find("pkt_id=?", paket.pkt_id).first();
		if (paket.pkt_flag == null)
			paket.setFlag43(); // flag 2 untuk paket yang dibuat dengan versi 4
		if (paket.pkt_tgl_buat == null)
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
		if (paket.pkt_hps == null)
			paket.pkt_hps = Double.valueOf(0);
		if (paket.pkt_status == null)
			paket.pkt_status = StatusPaket.DRAFT; // default status draft
		if (paket.kgr_id == null)
			paket.kgr_id = Kategori.PENGADAAN_BARANG; // default pengadaan barang jasa
		if (paket.pkt_tgl_assign == null)
			paket.pkt_tgl_assign = controllers.BasicCtr.newDate();
		if(paket.ppk_id == null)
			paket.ppk_id = anggaran_pl.ppk_id;
		paket.save();
		if (paket.pkt_id != null) {

			Paket_pl_lokasi obj = null;
			List<Long> saved = new ArrayList<Long>(lokasi.length);
			for (Paket_pl_lokasi lok : lokasi) {
				if (lok.pkl_id == null)
					obj = new Paket_pl_lokasi();
				else
					obj = Paket_pl_lokasi.findById(lok.pkl_id);
				obj.kbp_id = lok.kbp_id;
				obj.pkt_id = paket.pkt_id;
				obj.pkt_lokasi_versi = 1;
				if (!StringUtils.isEmpty(lok.pkt_lokasi)) {
					obj.pkt_lokasi = lok.pkt_lokasi;
					obj.save();
				}
				saved.add(obj.pkl_id);
			}
			if(!CommonUtil.isEmpty(saved)) {
				String join = StringUtils.join(saved, ",");
				if(!join.isEmpty())
					Query.update("delete from ekontrak.paket_lokasi where pkt_id=? and pkl_id not in ("+join+ ')', paket.pkt_id);
			}
			//Pl_seleksi.buatLelangBaru(paket);
		}
	}

	public static void simpanBuatPaketPenunjukan (Paket_pl paket, Paket_pl_lokasi[] lokasi){
		if (paket.pkt_flag == null)
			paket.pkt_flag = Integer.valueOf(2); // flag 2 untuk paket yang dibuat dengan versi 4
		if (paket.pkt_tgl_buat == null)
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
		if (paket.pkt_hps == null)
			paket.pkt_hps = Double.valueOf(0);
		if (paket.pkt_status == null)
			paket.pkt_status = StatusPaket.DRAFT; // default status draft
		if (paket.kgr_id == null)
			paket.kgr_id = Kategori.PENGADAAN_BARANG; // default pengadaan barang jasa
		if (paket.pkt_tgl_assign == null)
			paket.pkt_tgl_assign = controllers.BasicCtr.newDate();
		paket.save();
		if (paket.pkt_id != null) {
			Paket_pl_panitia paket_panitia = Paket_pl_panitia.find("pkt_id=? and pnt_id=?", paket.pkt_id, paket.pnt_id).first();
			if (paket_panitia == null) {
				paket_panitia = new Paket_pl_panitia();
				paket_panitia.pkt_id = paket.pkt_id;
				paket_panitia.pnt_id = paket.pnt_id;
				paket_panitia.save();
			}
			Paket_pl_lokasi obj = null;
			List<Long> saved = new ArrayList<Long>(lokasi.length);
			for (Paket_pl_lokasi lok : lokasi) {
				if (lok.pkl_id == null)
					obj = new Paket_pl_lokasi();
				else
					obj = Paket_pl_lokasi.findById(lok.pkl_id);
				obj.kbp_id = lok.kbp_id;
				obj.pkt_id = paket.pkt_id;
				if (!StringUtils.isEmpty(lok.pkt_lokasi)) {
					obj.pkt_lokasi = lok.pkt_lokasi;
					obj.save();
				}
				saved.add(obj.pkl_id);
			}
			if(!CommonUtil.isEmpty(saved)) {
				String join = StringUtils.join(saved, ",");
				if(!join.isEmpty())
					Query.update("delete from ekontrak.paket_lokasi where pkt_id=? and pkl_id not in ("+join+ ')', paket.pkt_id);
			}

			Pl_seleksi.buatLelangBaru(paket, null);

		}

	}

	/**
	 * 
	 * @param rup_id
	 * @return
	 */
	public static Paket_pl buatPaketFromSirup(Long rup_id, Long paketId, Long ppId) {
		PaketSirup rup = PaketSirup.findById(rup_id);
		Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
		Paket_pl paket;
		if(paketId == null) {
			paket = new Paket_pl();
			paket.pkt_nama = rup.nama;;
			paket.pkt_pagu = rup.pagu;
			paket.pkt_hps = Double.valueOf(0);
			paket.pp_id = ppId;
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
			paket.pkt_status = StatusPaket.DRAFT;
			paket.setFlag43(); // default flag paket yang dibuat diversi 4.3
			paket.kgr_id = rup.getKategori();
			paket.pkt_jenis = Integer.valueOf(0);
		} else { // paket konsolidasi (multi kode rup)
			paket = Paket_pl.findById(paketId);
			if(!paket.pkt_nama.contains(rup.nama)) // jika nama paket sama tidak perlu ditambahkan
				paket.pkt_nama += ',' + rup.nama;
			paket.pkt_pagu += rup.pagu;
		}
		paket.save();
		Paket_satker_pl ps = new Paket_satker_pl();
		ps.stk_id = satker.stk_id;
		ps.pkt_id = paket.pkt_id;
		ps.rup_id = rup.id;
		ps.save();
		Active_user user = Active_user.current();
		paket.createPaketPpk(user);
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)){
			Anggaran anggaran;
			Paket_anggaran_pl paket_anggaran;
			for(PaketSirup.PaketSirupAnggaran rup_anggaran: rup.paket_anggaran_json) {
				anggaran = new Anggaran();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup_anggaran.tahun_anggaran_dana == null ? rup.tahun : rup_anggaran.tahun_anggaran_dana;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = satker.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran = new Paket_anggaran_pl();
				paket_anggaran.pkt_id = paket.pkt_id;
				paket_anggaran.ang_id =anggaran.ang_id;
				paket_anggaran.ppk_id = user.ppkId;
				paket_anggaran.ppk_jabatan = 0;
				paket_anggaran.rup_id = rup.id;
				paket_anggaran.pkt_ang_versi = Integer.valueOf(1);
				paket_anggaran.save();
			}
		}

		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			for(PaketSirup.PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				Paket_pl_lokasi pkt_lokasi = new Paket_pl_lokasi();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.pkt_id = paket.pkt_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = Integer.valueOf(1);
				pkt_lokasi.save();
			}
		}

		//hanya jika paket belum pernah dibuat (pkt_id == null) langsung create lelangbaru
		if(paketId == null) {
			// langsung buat paket baru
			MetodePemilihanPenyedia metode = MetodePemilihanPenyedia.findById(rup.metode_pengadaan);
			Pl_seleksi.buatLelangBaru(paket, metode);
		}
		return paket;

	}

	public static Paket_pl buatPaketNonSpkFromSirup(Long rup_id, Long paketId, Long ppkId) {
		PaketSirup rup = PaketSirup.findById(rup_id);
		Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
		Paket_pl paket = null;
		if(paketId == null) {
			paket = new Paket_pl();
			paket.pkt_nama = rup.nama;;
			paket.pkt_pagu = rup.pagu;
			paket.pkt_hps = Double.valueOf(0);
			paket.pkt_tgl_buat = controllers.BasicCtr.newDate();
			paket.pkt_status = StatusPaket.DRAFT;
			paket.setFlag43(); // default flag paket yang dibuat diversi 4.3
			paket.kgr_id = rup.getKategori();
			paket.pkt_jenis = Integer.valueOf(1);
			paket.ppk_id = ppkId;
		} else { // paket konsolidasi (multi kode rup)
			paket = Paket_pl.findById(paketId);
			if(!paket.pkt_nama.contains(rup.nama)) // jika nama paket sama tidak perlu ditambahkan
				paket.pkt_nama += ',' + rup.nama;
			paket.pkt_pagu += rup.pagu;
		}
		paket.save();
		Paket_satker_pl ps = new Paket_satker_pl();
		ps.stk_id = satker.stk_id;
		ps.pkt_id = paket.pkt_id;
		ps.rup_id = rup.id;
		ps.save();
		Active_user user = Active_user.current();
		paket.createPaketPpk(user);
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)) {
			Anggaran anggaran = null;
			Paket_anggaran_pl paket_anggaran = null;
			for (PaketSirup.PaketSirupAnggaran rup_anggaran : rup.paket_anggaran_json) {
				anggaran = new Anggaran();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup_anggaran.tahun_anggaran_dana == null ? rup.tahun : rup_anggaran.tahun_anggaran_dana;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = satker.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran = new Paket_anggaran_pl();
				paket_anggaran.pkt_id = paket.pkt_id;
				paket_anggaran.ang_id = anggaran.ang_id;
				paket_anggaran.ppk_id = user.ppkId;
				paket_anggaran.ppk_jabatan = 0;
				paket_anggaran.rup_id = rup.id;
				paket_anggaran.pkt_ang_versi = Integer.valueOf(1);
				paket_anggaran.save();
			}
		}
		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			for(PaketSirup.PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				Paket_pl_lokasi pkt_lokasi = new Paket_pl_lokasi();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.pkt_id = paket.pkt_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = Integer.valueOf(1);
				pkt_lokasi.save();
			}
		}
		//hanya jika paket belum pernah dibuat (pkt_id == null) langsung create lelangbaru
		if(paketId == null) {
			// langsung buat paket baru
			MetodePemilihanPenyedia metode = MetodePemilihanPenyedia.findById(rup.metode_pengadaan);
//			Pl_seleksi.buatLelangBaru(paket, metode);
			Non_spk_seleksi.buatNonSpkBaru(paket, metode);
		}
		return paket;

	}

	/**
	 *
	 * @param rup_id
	 * @return
	 */
	public static Paket_pl updatePaketRup(Long rup_id, Long paketId, Long ppId) {
		PaketSirup rup = PaketSirup.findById(rup_id);
		Paket_pl paket = Paket_pl.findById(paketId);

		Paket_satker_pl ps = Paket_satker_pl.find("pkt_id=?", paket.pkt_id).first();
		if(ps==null) {
			Satuan_kerja satker = Satuan_kerja.find("rup_stk_id=? and agc_id is null", rup.rup_stk_id).first();
			ps = new Paket_satker_pl();
			ps.stk_id = satker.stk_id;
			ps.pkt_id = paket.pkt_id;
			ps.rup_id = rup.id;
			ps.save();
		}
		Active_user user = Active_user.current();
//		paket.createPaketPpk(user);
		Pl_seleksi lelang = Pl_seleksi.findByPaketNewestVersi(paket.pkt_id);
		if(!CollectionUtils.isEmpty(rup.paket_anggaran_json)){
			Anggaran anggaran;
			Paket_anggaran_pl paket_anggaran;
			for(PaketSirup.PaketSirupAnggaran rup_anggaran: rup.paket_anggaran_json) {
				anggaran = new Anggaran();
				anggaran.ang_koderekening = rup_anggaran.mak;
				anggaran.ang_tahun = rup_anggaran.tahun_anggaran_dana == null ? rup.tahun : rup_anggaran.tahun_anggaran_dana;
				anggaran.sbd_id = rup_anggaran.getSumberDana();
				anggaran.stk_id = ps.stk_id;
				anggaran.ang_nilai = rup_anggaran.pagu;
				anggaran.ang_uraian = rup.nama;
				anggaran.save();
				paket_anggaran = new Paket_anggaran_pl();
				paket_anggaran.pkt_id = paket.pkt_id;
				paket_anggaran.ang_id =anggaran.ang_id;
				paket_anggaran.ppk_id = user.ppkId;
				paket_anggaran.ppk_jabatan = 0;
				paket_anggaran.rup_id = rup.id;
				paket_anggaran.pkt_ang_versi = lelang.lls_versi_lelang;
				paket_anggaran.save();
			}
		}

		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)) {
			for(PaketSirup.PaketSirupLokasi lokasi : rup.paket_lokasi_json) {
				Paket_pl_lokasi pkt_lokasi = new Paket_pl_lokasi();
				pkt_lokasi.pkt_lokasi = lokasi.detil_lokasi;
				pkt_lokasi.pkt_id = paket.pkt_id;
				pkt_lokasi.kbp_id = lokasi.id_kabupaten;
				pkt_lokasi.pkt_lokasi_versi = lelang.lls_versi_lelang;
				pkt_lokasi.save();
			}
		}
		return paket;
	}

	public static final ResultSetHandler<String[]> resultsetPaketpp = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Long lelangId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
//			MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(pemilihan.id);
			String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isPl());
			StatusPl status = StatusPl.fromValue(rs.getInt("lls_status"));
			String mtdLabel = pemilihan.label;
			String[] tmp = new String[8];
			tmp[0] = rs.getString("pkt_id");
			tmp[1] = rs.getString("pkt_nama");
			if (status.isDraft()) {
				tmp[2] = Messages.get("pdt.drf");
			} else  {
				if (jadwal.equalsIgnoreCase("Paket Sudah Selesai") && !status.isDraft()) {
					tmp[2] = Messages.get("pdt.sls");
				} else {
					tmp[2] = status.getLabel();
				}
			}
			tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
			tmp[4] = rs.getString("stk_nama");
			tmp[5] = lelangId.toString();
			tmp[6] = pkt_flag.toString();
			if(pkt_flag == 3){
				MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findById(pemilihan.id);
				mtdLabel = pemilihanpl.getLabel();
			}
			tmp[7] = mtdLabel;
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetPaketpanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Long lelangId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
			String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihanpl.isPl());
			StatusPaket status = StatusPaket.fromValue(rs.getInt("lls_status"));
			Active_user user = Active_user.current();
			String[] tmp = new String[16];
			tmp[0] = rs.getString("pkt_id");
			tmp[1] = rs.getString("pkt_nama");
            boolean isFinished = false;
            final boolean isVersion4 = pkt_flag.equals(2) || pkt_flag.equals(3);
            if (status.isDraft()) {
				tmp[2] = "Draft";
			} else  {
				if (jadwal.equalsIgnoreCase("Penunjukan Sudah Selesai") && !status.isDraft()) {
					tmp[2] = "Penunjukan Sudah Selesai";
				} else {
					tmp[2] = status.label;
				}
			}
			tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
			tmp[4] = rs.getString("stk_nama");
			tmp[5] = lelangId.toString();
			tmp[6] = pkt_flag.toString();
			tmp[7] = pemilihanpl.getLabel();
//			tmp[8] = StatusLelang.fromValue(rs.getInt("lls_status")).label;
			tmp[8] = rs.getString("lls_versi_lelang");
			//show create tender button
			tmp[9] = String.valueOf((isVersion4) && user.isPanitia());
			//show delete button
			tmp[10] = String.valueOf(status.isDraft() && Active_user.current().isPpk() && rs.getString("ukpbj_id") == null);
			//show view lelang
			tmp[11] = String.valueOf((isFinished || tmp[2].equals("Lelang Ditutup")) && user.isPanitia());
			//labeling
			tmp[12] = isVersion4 ? "spse 4" : "spse 3";
			//highlight
			tmp[13] = isVersion4 ? "badge badge-danger" : "badge badge-success";
			//Panitia
			tmp[14] = String.valueOf(user.isPanitia());
			//PPK
			tmp[15] = String.valueOf(user.isPpk());
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetPaketNonSpkPp = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Long lelangId = rs.getLong("lls_id");
			Integer pkt_flag = rs.getInt("pkt_flag");
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
			String[] tmp = new String[8];
			tmp[0] = rs.getString("pkt_id");
			tmp[1] = rs.getString("pkt_nama");
			tmp[2] = StatusPaket.fromValue(rs.getInt("pkt_status")).label;
			tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
			tmp[4] = rs.getString("stk_nama");
			tmp[5] = lelangId.toString();
			tmp[6] = pkt_flag.toString();
			tmp[7] = pemilihanpl.getLabel();
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetNonTenderPpk = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Long lelangId = rs.getLong("lls_id");
			Integer pktFlag = rs.getInt("pkt_flag");
			boolean lelangV3 = pktFlag < 2;
			MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
			String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
			Paket_pl.StatusPaket status = Paket_pl.StatusPaket.fromValue(rs.getInt("pkt_status"));
//			Paket_pl paket = Paket_pl.findById(rs.getLong("pkt_id"));
			Active_user user = Active_user.current();
			String[] tmp = new String[15];
			tmp[0] = rs.getString("pkt_id");
			tmp[1] = rs.getString("pkt_nama");
			boolean isFinished = false;
			final boolean isVersion4 = pktFlag.equals(2) || pktFlag.equals(3);
			if (status.isDraft()) {
				tmp[2] = "Draft";
			} else  {
				if (jadwal.equalsIgnoreCase("Pencatatan Sudah Selesai") && !status.isDraft()) {
					tmp[2] = "Pencatatan Sudah Selesai";
					isFinished = true;
				} else {
					tmp[2] = status.label;
				}
			}
			tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
			tmp[4] = rs.getString("stk_nama");
			tmp[5] = lelangId.toString();
			tmp[6] = pktFlag.toString();
			tmp[7] = pemilihanpl.getLabel();
//			tmp[8] = rs.getString("lls_versi_lelang");
			//show create tender button
			tmp[8] = String.valueOf((isVersion4) && !status.isSelesaiLelang() && user.isPP());
			//show delete button
			tmp[9] = String.valueOf(status.isDraft() && Active_user.current().isPpk());
			//show view lelang
			tmp[10] = String.valueOf((status.isSedangLelang() || isFinished || tmp[2].equals("Lelang Ditutup")) && user.isPanitia());
			//labeling
			tmp[11] = isVersion4 ? "spse 4" : "spse 3";
			//highlight
			tmp[12] = isVersion4 ? "badge badge-danger" : "badge badge-success";
			//Panitia
			tmp[13] = String.valueOf(user.isPP());
			//PPK
			tmp[14] = String.valueOf(user.isPpk());
			return tmp;
		}
	};

	
	public static String getNamaPaketFromlelang(Long lelangId) {
		return Query.find("select pkt_nama from ekontrak.paket where pkt_id IN (select pkt_id from ekontrak.nonlelang_seleksi where lls_id=?)", String.class, lelangId).first();
	}
	
	public static Paket_pl findBy(Long plId) {
		return Query.find("SELECT * FROM ekontrak.paket WHERE pkt_id IN (SELECT pkt_id FROM ekontrak.nonlelang_seleksi WHERE lls_id=?)", Paket_pl.class, plId).first();
	}

	public static Paket_pl findByNonSpk(Long plId) {
		return Query.find("SELECT * FROM ekontrak.paket WHERE pkt_id IN (SELECT pkt_id FROM ekontrak.non_spk_seleksi WHERE lls_id=?)", Paket_pl.class, plId).first();
	}
	
	public static Paket_pl findByLelang(Long plId) {
		return Query.find("SELECT * FROM ekontrak.paket WHERE pkt_id IN (SELECT pkt_id FROM ekontrak.nonlelang_seleksi WHERE lls_id=?)", Paket_pl.class, plId).first();
	}

	@Transient
	public JenisPaketEnum getJenisPaket() {
		return JenisPaketEnum.fromValue(pkt_jenis);
	}
	

	@Transient
	public String getSumberDana() {
		List<String> result =Query.find("select distinct (a.sbd_id) from anggaran a, ekontrak.paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, pkt_id).fetch();
		return StringUtils.join(result, ",");
	}

	@Transient
	public String getTahunAnggaran() {
		List<String> result = Query.find("select distinct (a.ang_tahun) from anggaran a, ekontrak.paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=?", String.class, pkt_id).fetch();
		return StringUtils.join(result, ",");
	}

	public Boolean isEnableViewHps(){
		return pkt_hps_enable != null ? pkt_hps_enable : true;
	}

	public boolean isCreatedByPpk() {
		return isFlag43() && getPaketPpk() != null;
	}

	public boolean isAllowEditPpk() {
		return pkt_status.isDraft() && !isCreatedByPpk();
	}

	public boolean allowEdit() {
		return (pkt_status.isDraft() || pkt_status.isUlangLelang());
	}

	public boolean allowEditUkpbj() {

		return pkt_status.isDraft() || !Active_user.current().isPpk();
	}

	public boolean isEditable(){
		boolean valid = allowEdit();
		if (isFlag43()) {
			valid = valid && !Active_user.current().isPanitia() && !Active_user.current().isPP();
		}
		return valid;
	}

	public boolean isEditableExceptKuppbj() {
		return isEditable() && !Active_user.current().isKuppbj();
	}

	public boolean isEditableByKuppbj() {
		return isEditable() && Active_user.current().isKuppbj();
	}

	public boolean isEditableByKuppbjKonsolidasi() {
		return isEditable() && isKonsolidasi() && Active_user.current().isKuppbj();
	}

	public boolean showExtendedPpkInput() {
		return getPaketLocations().size() > 0;
	}

	public boolean isHpsExist() {
		return pkt_hps != null && pkt_hps > 0;
	}

	public String getHpsFormatted() {
		return isHpsExist() ? FormatUtils.formatCurrencyRupiah(pkt_hps) : "Rp. 0";
	}

	public String getPaguFormatted() {
		return pkt_pagu != null ? FormatUtils.formatCurrencyRupiah(pkt_pagu) : "Rp. 0";
	}

	public boolean isAllowToShowSaveButton(boolean documentFilled) {
		if (isCreatedByPpk() && showExtendedPpkInput()) {
			return allowEdit()
					&& isHpsExist()
					&& documentFilled
					&& Active_user.current().isPpk();
		}
		return allowEdit();
	}

	public String getPpName() {
		if (getPaketPp() != null && getPaketPp().getPp() != null && getPaketPp().getPp().getPegawai() != null) {
			return getPaketPp().getPp().getPegawai().peg_nama;
		}
		return null;
	}

	public boolean isPpNameExist() {
		return !CommonUtil.isEmpty(getPpName());
	}

	public String getPntNama() {
		return getPanitia() != null ? getPanitia().pnt_nama : null;
	}

	/**
	 * Gunakan PaketPlSimple
	 * Hanya select yang dibutuhan
	 * @param pkt_id_konsolidasi
	 * @return
	 */
	public static List<PaketPlSimple> findSimpleByKonsolidasiId(Long pkt_id_konsolidasi) {
		return PaketPlSimple.findAll("p.pkt_id_konsolidasi = ?", pkt_id_konsolidasi);
	}

	@Transient
	public Kualifikasi getKualifikasi()
	{
		if(kls_id == null)
			return null;
		return Kualifikasi.findById(kls_id);
	}

}
