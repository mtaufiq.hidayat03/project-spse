package models.agency;

import com.google.gson.reflect.TypeToken;
import ext.DateBinder;
import ext.FormatUtils;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.kontrak.AnggotaKSO;
import models.kontrak.Kontrak_kso;
import models.kontrak.SskkPpkPlContent;
import models.nonlelang.Pl_seleksi;
import models.rekanan.Rekanan;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Created by Lambang on 3/24/2017.
 */
@Table(name="EKONTRAK.KONTRAK")
public class KontrakPl extends BaseModel {

    public static final String PENYEDIA_PERSEORANGAN = "Penyedia Perseorangan";
    public static final String PENYEDIA_KEMITRAAN_KSO = "Penyedia Kemitraan/KSO";
    public static final String PENYEDIA_BADAN_USAHA_NON_KSO = "Penyedia Badan Usaha Non KSO";
    @Id(sequence="ekontrak.seq_kontrak_pl", function="nextsequence")
    public Long kontrak_id;
    //relasi ke Pl_seleksi
    @Required
    public Long lls_id;
    //relasi ke Rekanan
    public Long rkn_id;

    @Required
    public Long ppk_id;
    @Required
    public String kontrak_no;
    @Required
    public Double kontrak_nilai;
    @Required
    public String kontrak_norekening;

    public String kontrak_pemilik_rekening;

    public String kontrak_nama_bank;
    @Required
    @As(binder= DateBinder.class)
    public Date kontrak_tanggal;

    @As(binder=DateBinder.class)
    public Date kontrak_mulai;

    @As(binder=DateBinder.class)
    public Date kontrak_akhir;

    public String kontrak_lingkup;

    public Long kontrak_id_attacment;

    public Long kontrak_id_attacment2;

    public Long kontrak_sskk_attacment;

    public String kontrak_kota;

    public String kontrak_waktu;

    public String kontrak_wakil_penyedia;

    public String kontrak_jabatan_wakil;

    public String kontrak_tipe_penyedia;

    public String kontrak_kso;

    public String kontrak_sskk;

    public String kontrak_sskk_perubahan;
    
    /**
       ## tambahan field penyesuaian implementasi tgl, 17 maret 2020
       dengan implementasi PermenPUPR07-2019
     */
    public String kontrak_lingkup_pekerjaan;
//
//    @Required(message = "no surat perjanjian dibutuhkan")
//    public String no_surat_perjanjian;
//
//    @Required(message = "tanggal surat perjanjian dibutuhkan")
//    @As(binder=DateBinder.class)
//    public Date tanggal_surat_perjanjian;
//
//    @Required(message = "kota surat perjanjian dibutuhkan")
//    public String kota_surat_perjanjian;
//    //end

    public String alasanubah_kontrak_nilai;

    @Required(message = "nama pemilik rekening diperlukan")
    public String kontrak_namapemilikrekening;//BANK

    @Required(message = "Jabatan PPK wajib diisi")
    public String jabatan_ppk_kontrak;
    public String anggota_kso;
    @Transient
    public List<AnggotaKSO> anggotakso;
    //end here
    boolean isNonKSO() {
        return PENYEDIA_BADAN_USAHA_NON_KSO.equals(this.kontrak_tipe_penyedia);
    }

    boolean isKSO() {
        return PENYEDIA_KEMITRAAN_KSO.equals(this.kontrak_tipe_penyedia);
    }

    boolean isPerorangan() {
        return PENYEDIA_PERSEORANGAN.equals(this.kontrak_tipe_penyedia);
    }

    @Transient
    public SskkPpkPlContent getSskkContent() {
        return CommonUtil.fromJson(kontrak_sskk, SskkPpkPlContent.class);
    }

    @Transient
    public Kontrak_kso getKontrakKSO() {
        return CommonUtil.fromJson(kontrak_kso, Kontrak_kso.class);
    }

    @Transient
    public List<AnggotaKSO> getListAnggotaKSO() {
        return CommonUtil.fromJson(anggota_kso, new TypeToken<List<AnggotaKSO>>(){}.getType());
    }

    @Transient
    public BlobTable getBlob() {
        if(kontrak_id_attacment == null)
            return null;
        return BlobTableDao.getLastById(kontrak_id_attacment);
    }

    @Transient
    public BlobTable getBlob2() {
        if(kontrak_id_attacment2 == null)
            return null;
        return BlobTableDao.getLastById(kontrak_id_attacment2);
    }

    @Transient
    public BlobTable getBlobSskk() {
        if(kontrak_sskk_attacment == null)
            return null;
        return BlobTableDao.getLastById(kontrak_sskk_attacment);
    }

    @Transient
    public String getMasaBerlaku() {
        if(kontrak_mulai == null || kontrak_akhir == null )
            return "";
        return FormatUtils.formatDate(kontrak_mulai, kontrak_akhir);
    }
    @Transient
    private Pl_seleksi pl_seleksi;
    @Transient
    private Ppk ppk;
    @Transient
    private Rekanan rekanan;

    public Pl_seleksi getPl_seleksi() {
        if(pl_seleksi == null)
            pl_seleksi = Pl_seleksi.findById(lls_id);
        return pl_seleksi;
    }

    public Ppk getPpk() {
        if(ppk == null)
            ppk = Ppk.findById(ppk_id);
        return ppk;
    }

    public Rekanan getRekanan() {
        if(rekanan == null)
            rekanan = Rekanan.findById(rkn_id);
        return rekanan;
    }

    public static void simpanKontrak(KontrakPl kontrak, File file, File file2, String hapus1, String hapus2) throws Exception {
        if (file == null && file2 == null) {
            if (kontrak.kontrak_id_attacment != null && hapus1 == null) {
                BlobTable.delete("blb_id_content=?", kontrak.kontrak_id_attacment);
                kontrak.kontrak_id_attacment = null;
            }
            if (kontrak.kontrak_id_attacment2 != null && hapus2 == null) {
                BlobTable.delete("blb_id_content=?", kontrak.kontrak_id_attacment2);
                kontrak.kontrak_id_attacment2 = null;
            }
        } else {
            if (file != null) {
                BlobTable bt = null;
                if(kontrak.kontrak_id_attacment == null) {
                    // jika baru upload file, gunakan fungsi save file dengan 2 parameter
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
                } else {
                    // jika update file yang di-upload gunakan fungsi save file dengan 3 parameter
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, kontrak.kontrak_id_attacment);
                }
                kontrak.kontrak_id_attacment = bt.blb_id_content;
            }
            if (file2 != null) {
                BlobTable bt2=null;
                if (kontrak.kontrak_id_attacment2 == null) {
                    // jika baru upload file, gunakan fungsi save file dengan 2 parameter
                    bt2 = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file2);

                } else {
                    // jika update file yang di-upload gunakan fungsi save file dengan 3 parameter
                    bt2 = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file2, kontrak.kontrak_id_attacment2);

                }
                kontrak.kontrak_id_attacment2 = bt2.blb_id_content;
            }
        }
        if(kontrak.kontrak_mulai ==null)
            kontrak.kontrak_mulai = kontrak.kontrak_tanggal;
        if(kontrak.kontrak_akhir ==null)
            kontrak.kontrak_akhir = kontrak.kontrak_tanggal;
        if(kontrak.kontrak_lingkup ==null)
            kontrak.kontrak_lingkup = "";
        kontrak.save();
    }

    public List<BlobTable> getDokSskkAttachment(){
        if(kontrak_sskk_attacment == null){
            return null;

        }
        return BlobTableDao.listById(kontrak_sskk_attacment);
    }

    public UploadInfo simpanSskkAttachment(File file) throws Exception {
        String path = sanitizedFilename(file);
        File newFile = new File(path);
        BlobTable model = findByFilename(newFile.getName(), getDokSskkAttachment());

        if (model != null) {
            LogUtil.debug("Kontrak", "filename's duplicated");
            return null;
        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (kontrak_sskk_attacment != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, kontrak_sskk_attacment);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }

        kontrak_sskk_attacment = blob.blb_id_content;

        save();
        return UploadInfo.findBy(blob);
    }

    public String sanitizedFilename(File file) {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;
    }

    public BlobTable findByFilename(String filename,List<BlobTable> blobs) {
        if (CommonUtil.isEmpty(blobs)) {
            LogUtil.debug("Kontrak", "total blob is empty or object null");
            return null;
        }
        for (BlobTable model : blobs) {
            LogUtil.debug("Kontrak", filename + " contains " + model.blb_nama_file);
            if (model.blb_nama_file.equalsIgnoreCase(filename)) {
                return model;
            }
        }
        return null;
    }

    @Transient
    public List<BlobTable> getDokAttachment2() {
        if(kontrak_id_attacment2 == null){
            return null;
        }
        return BlobTableDao.listById(kontrak_id_attacment2);
    }

    public UploadInfo simpanAttachment2(File file) throws Exception {
        String path = sanitizedFilename(file);
        File newFile = new File(path);
        file.renameTo(newFile);
        BlobTable blob = null;
        if (kontrak_id_attacment2 != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, kontrak_id_attacment2);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        kontrak_id_attacment2 = blob.blb_id_content;
        save();

        return UploadInfo.findBy(blob);
    }

    // find progress terbesar
    public Long progressMax() {
        Long progress = Query.find("SELECT MAX(progres_fisik) FROM ekontrak.ba_pembayaran WHERE kontrak_id=?", Long.class, kontrak_id).first();
        if(progress == null)
            progress = 0L;
        return progress;
    }


}
