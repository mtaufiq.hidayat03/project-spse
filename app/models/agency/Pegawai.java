package models.agency;

import ams.contracts.AmsUserContract;
import com.google.gson.Gson;
import controllers.BasicCtr;
import controllers.UserCtr.LoginResult;
import models.common.MailQueueDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.secman.Group;
import models.secman.LupaPasswordToken;
import models.secman.Usergroup;
import models.secman.Usrsession;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.data.validation.*;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Model {@code Pegawai} merepresentasikan tabel pegawai pada database.
 * TODO: Field untuk data scan dokumen penunjukan
 *
 * @author I Wayan Wiprayoga W
 */

@Table(name="PEGAWAI")
public class Pegawai extends BaseModel implements AmsUserContract {

	@Id(sequence="seq_pegawai", function="nextsequence")
	public Long peg_id;

	@Required(message="NIP tidak boleh kosong")
	//@Unique
	public String peg_nip;
	/**
	 * Nama pegawai
	 */
	@Required
	public String peg_nama;
	/**
	 * Alamat pegawai
	 */
	public String peg_alamat;
	/**
	 * Nomor telepon pegawai
	 */
	@Required (message="Nomor Telepon tidak boleh kosong")
	@Phone
	public String peg_telepon;
	/**
	 * Nomor mobile pegawai
	 */
	@Phone
	public String peg_mobile;


	public String peg_masa_berlaku;
	/**
	 * E-mail pegawai
	 */
	@Required
	@Email
	public String peg_email;
	/**
	 * Golongan, Pangkat, Jabatan, no_sk tidak lagi dipakai
	 * namun akan menempel pada Grup: Admin Agency, Admin LPSE, PPK, Panitia, Verifikator, dll
	 */
	public String peg_golongan;
	public String peg_pangkat;
	public String peg_jabatan;
	public String peg_no_pbj;
	@Required(message="NO SK tidak boleh kosong")
	public String peg_no_sk;
	public Integer peg_isactive;

	// relasi Agency
	public Long agc_id;


	/** LInk ke User
	 */
	@Required(message="User ID tidak boleh kosong")
	@Match(message = "validation.match.userid", value = "[a-zA-Z0-9.]{3,80}")
	public String peg_namauser;

	//ref to Certificate
	public Long cer_id;

	@MinSize(6)
	public String passw;

	public Integer isbuiltin;

	public Integer disableuser;

	/**Saat user minta reset password, sistem mengirimkan 'signature' via email
	 * dan disimpan di kolom ini.
	 * Signature berisi UUID dan tanggal pembuatannya.
	 */
	public String reset_password;
	public String ams_id;

	public Long ukpbj_id;

	@Transient
	private Agency agency;


	public Agency getAgency() {
		if(agency == null)
			agency = Agency.findById(agc_id);
		return agency;
	}

	public boolean isDisableUser() {
		return disableuser != null && disableuser != 0;
	}


	/**@Since 4.0 Kolom baru peg_tanggal_lahir
	 * digunakan untuk mengecek kesamaan identitas,
	public Date peg_tanggal_lahir;

	///Hanya untuk migrasi
	public String peg_nama_migration;
	 */


	/**
	 * Set the peg_namauser menjadi UPPERCASE
	 */
	public void setPeg_namauser(String aValue) {
		if (aValue != null && aValue.length() > 0) {
			this.peg_namauser = aValue.toUpperCase();
		}
	}

	/**Khusus untuk migrasi. Dapatkan panitia di mana orang ini terdaftar
	 *
	 */
	@Transient
	public List<Long> getPanitiaListMigration()
	{
		return Query.find("select pnt_id from Anggota_panitia where peg_id=? order by auditupdate desc", Long.class , peg_id).fetch();
	}

	/**
	 * Fungsi {@code findBy} digunakan untuk mendapatkan sebuah
	 * data pegawai berdasarkan nama user pegawai
	 *
	 * @param userid username pegawai
	 * @return objek pegawai yang bersesuaian dengan parameter
	 * @{code username}
	 */
	public static Pegawai findBy(String userid) {
		return find("peg_namauser = ?", userid.toUpperCase()).first();
	}

	/**
	 * find by pegawai.peg_id
	 * @param pegId peg_id
	 * @return pegawai*/
	public static Pegawai findByPegId(Long pegId) {
		return find("peg_id = ?", pegId).first();
	}

	/**
	 * get all of my ukpbj that i have been created.
	 * @param agencyId your agency id
	 * @return collections of active employee that you have created*/
	public static List<Pegawai> findUkpbjByAgency(Long agencyId) {
		String query = "SELECT p.* FROM pegawai p JOIN usergroup u ON u.userid = p.peg_namauser " +
				"WHERE p.agc_id = ? AND p.peg_masa_berlaku >=  ? AND u.idgroup = ?";
		return Query.find(query, Pegawai.class, agencyId, BasicCtr.newDate(),  Group.UKPBJ.toString()).fetch();
	}

	/**
	 * get all of my ukpbj that i have been created.
	 * @param agencyId your agency id
	 * @return collections of active employee that you have created*/
	public static List<Pegawai> findKuppbjByAgency(Long agencyId) {
		String query = "SELECT p.* FROM pegawai p JOIN usergroup u ON u.userid = p.peg_namauser " +
				"WHERE p.agc_id = ? AND u.idgroup = ?";
		return Query.find(query, Pegawai.class, agencyId, Group.KUPPBJ.toString()).fetch();
	}


	public static String findNamaPegawaiBy(String userid) {
		return Query.find("select peg_nama from Pegawai where peg_namauser = ?", String.class, userid.toUpperCase()).first();
	}

	public static String listNamaAnggotaByPanitiaToString(Long pnt_id){
		return Query.find("SELECT string_agg(peg_nama, ', ') as peg_nama from pegawai p join anggota_panitia a on a.peg_id = p.peg_id where a.pnt_id = ?", String.class,pnt_id).first();
	}

	/**
	 * Fungsi {@code findPegawaiByAgency} digunakan untuk mendapatkan objek
	 * {@code Pegawai} yang sesuai dengan agency-nya. Hal ini diperlukan sebagai
	 * proteksi terhadap data pegawai yang hanya dapat diakses oleh admin agency
	 * yang berwenang.
	 *
	 * @param pegawaiId nip pegawai
	 * @param agencyId  session agency id
	 * @return objek pegawai yang bersesuaian, jika {@code agc_id} tidak sesuai
	 *         (Bukan admin agency yang berwenang) objek akan bernilai {@code null}
	 */
	public static Pegawai findPegawaiByAgency(Long pegawaiId, Long agencyId) {
		return find("peg_id = ? AND agc_id = ?", pegawaiId, agencyId).first();
	}

	/**
	 * Fungsi {@code hapus} digunakan untuk menghapus objek {@code Pegawai} sekaligus menghapus entri-entri terkait penghapusan pegawai yaitu objek {@code Usergroup} dan {@code User}.
	 *
	 * @param pegawaiId ID pegawai
	 */

	public static void hapus(Long... pegawaiId) {
		if(pegawaiId != null && pegawaiId.length > 0) {
			String join = StringUtils.join(pegawaiId, ",");
			List<String> userid = Query.find("select peg_namauser from pegawai where peg_id in ("+join+ ')', String.class).fetch();
			Admin_agency.delete("peg_id in ("+join+ ')');
			delete("peg_id in ("+join+ ')');
			if(!CommonUtil.isEmpty(userid)) {
				StringBuilder sb = new StringBuilder();
				for(String uid: userid){
					sb.append(",'").append(uid).append('\'');
				}
				join = sb.substring(1);
				Usrsession.delete("userid in ("+join+ ')');
				Usergroup.delete("userid in ("+join+ ')');
			}
		}
	}

	public static void simpan(Pegawai pegawai, boolean chkPassword, String newPassword, String ulangiPassword, String group) {
		boolean newUser = pegawai.peg_id == null;

		pegawai.disableuser = 0;
		pegawai.peg_namauser = pegawai.peg_namauser.toUpperCase();

		// ganti password dan password yang dimasukan valid
		if ((newUser || chkPassword) && StringUtils.isNotEmpty(newPassword) && newPassword.equals(ulangiPassword)) {
			pegawai.passw = DigestUtils.md5Hex(newPassword); // update password

			pegawai.save(); // simpan objek user yang sudah di-update

			if(chkPassword) {
				//kirim email pemberitahuan
				try {
					MailQueueDao.kirimNotifikasiGantiPassword(pegawai.peg_namauser, pegawai.peg_email, BasicCtr.newDate(), null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			// jika tidak ada perubahan password, isi kembali password karena tidak dibinding
			pegawai.passw = Pegawai.findBy(pegawai.peg_namauser).passw;
			pegawai.save();
		}

		saveUserModel(pegawai, group);
	}

	public static void simpan(Pegawai pegawai, boolean chkPassword, String newPassword, String group) {
		boolean newUser = pegawai.peg_id == null;
		pegawai.disableuser = 0;
		pegawai.peg_namauser = pegawai.peg_namauser.toUpperCase();

		// ganti password dan password yang dimasukan valid
		if ((newUser || chkPassword) && StringUtils.isNotEmpty(newPassword)) {
			pegawai.passw = DigestUtils.md5Hex(newPassword); // update password
			pegawai.save(); // simpan objek user yang sudah di-update

			if(chkPassword) {
				//kirim email pemberitahuan
				try {
					MailQueueDao.kirimNotifikasiGantiPassword(pegawai.peg_namauser, pegawai.peg_email, BasicCtr.newDate(), null);
				} catch (Exception e) {
					Logger.error(e, "kirim notifikasi Ganti Password gagal");
				}
			}
		} else {
			// jika tidak ada perubahan password, isi kembali password karena tidak dibinding
			pegawai.passw = Pegawai.findBy(pegawai.peg_namauser).passw;
			pegawai.save();
		}

		saveUserModel(pegawai, group);
	}

	private static void saveUserModel(Pegawai pegawai, String group) {
		if (pegawai.peg_id != null && StringUtils.isNotEmpty(group)) { // tidak ganti password
			Usergroup.createUsergroupFromUser(pegawai.peg_namauser, Group.valueOf(group)); // simpan model usergroup terkait dengan pegawai
			if (group.equals("PPK")) {
				Ppk ppk = Ppk.find("peg_id=?", pegawai.peg_id).first();
				if (ppk == null) {
					Ppk ppk_temp = new Ppk();
					ppk_temp.peg_id = pegawai.peg_id;
					ppk_temp.ppk_valid_start = new Date();
					ppk_temp.ppk_nomor_sk = pegawai.peg_no_sk;
					ppk_temp.save();
				}
			}
			if (group.equals("PP")) {
				Pp pp = Pp.find("peg_id=?", pegawai.peg_id).first();
				if (pp == null) {
					Pp pp_temp = new Pp();
					pp_temp.peg_id = pegawai.peg_id;
					pp_temp.pp_valid_start = new Date();
					pp_temp.pp_nomor_sk = pegawai.peg_no_sk;
					pp_temp.save();
				}
			}
		}
	}

	public boolean checkValidNIP(String nip){
		return (this.find("", nip).first())==null;
	}

	public boolean getIsActive(){
		if(this.peg_isactive != 0){
			return true;
		}else{
			return false;
		}
	}

	public static final ResultSetHandler<String[]> resultsetPegawai = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			tmp[0] = rs.getString("peg_id");
			tmp[1] = rs.getString("peg_nama");
			tmp[2] = rs.getString("peg_nip");
			tmp[3] = rs.getString("peg_namauser");
			tmp[4] = Group.valueOf(rs.getString("idgroup")).getLabel();
			
			try {
				tmp[5] = rs.getString("cer_id");
			} catch (Exception e) {
				tmp[5] = "";
			}
			return tmp;
		}
	};

	public static List<Pegawai> getPegawaiByCertIds(String certJenis, Long agencyId){
		if(certJenis == null)
			return null;
		String sql = "cer_id IN ("+certJenis+ ')';
		if(agencyId!=null)
			sql = sql + " AND agc_id="+agencyId;
		return find(sql).fetch();
	}

	/**
	 * Fungsi {@code doLogin} digunakan untuk eksekusi login pengguna
	 *
	 * @param userId    user id pengguna
	 * @param password   password pengguna (hashed)
	 * @param ip_address ip address pengguna
	 * @return enum object {@code LoginResult}
	 */
	public static LoginResult doLogin(String userId, String password, String ip_address) {
		Pegawai user = findBy(userId);
		if (user == null || StringUtils.isEmpty(user.passw)) {
			return LoginResult.INVALID_USER_PASSWORD; // userId tidak ada di database
		} else {
			if (user.isDisableUser() || !user.getIsActive()) {
				return LoginResult.DISABLED_USER; // userId tidak aktif
			} else {
				if (!password.equals(user.passw.toLowerCase())) {
					return LoginResult.INVALID_USER_PASSWORD; // password tidak cocok
				}
				return LoginResult.SUCCESS; // login sukses
			}
		}
	}

	public static LoginResult doLogin(String userId, String ip_address) {
		Pegawai user = findBy(userId);
		if (user == null) {
			return LoginResult.INVALID_USER_ID; // userId tidak ada di database
		} else {
			if (user.isDisableUser() || !user.getIsActive()) {
				return LoginResult.DISABLED_USER; // userId tidak aktif
			} else {
				return LoginResult.SUCCESS; // login sukses
			}
		}
	}


	public LupaPasswordToken requestLupaPasswordToken() {
		Gson gson = new Gson();
		LupaPasswordToken token;
		if (reset_password == null) {
			token = new LupaPasswordToken();
			token.tanggal_buat = new Date();
			token.token = UUID.randomUUID().toString();
		} else {
			token = gson.fromJson(reset_password, LupaPasswordToken.class);
			if (token.isExpires())
				//token berganti
				token.token = UUID.randomUUID().toString();
			//tanggal buat selalu berganti
			token.tanggal_buat = controllers.BasicCtr.newDate();
		}
		reset_password = gson.toJson(token);
		return token;
	}

	public LupaPasswordToken getLupaPasswordToken() {
		if (reset_password == null)
			return null;
		else
			return new Gson().fromJson(reset_password, LupaPasswordToken.class);
	}

	@Override
	public Long getCerId() {
		return this.cer_id;
	}

	@Override
	public Long getUserId() {
		return this.peg_id;
	}

	@Override
	public String getAmsId() {
		return this.ams_id;
	}

	@Override
	public String getUniqueId() {
		return this.peg_nip;
	}

	@Override
	public void setCerId(Long cerId) {
		this.cer_id = cerId;
	}

	@Override
	public void setAmsId(String amsId) {
		this.ams_id = amsId;
	}

	@Override
	public void saveModel() {
		save();
	}
}