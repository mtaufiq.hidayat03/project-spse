package models.agency;

import models.agency.Paket.StatusPaket;
import models.jcommon.db.base.BaseModel;
import models.sso.common.Kabupaten;
import org.sql2o.ResultSetHandler;
import play.data.validation.Email;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.data.validation.URL;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Model {@code Panitia} mereprensentasikan tabel panitia pada database.
 *
 * @author I Wayan Wiprayoga W
 */
@Table(name="PANITIA")
public class Panitia extends BaseModel {

	/**
	 * ID Kepanitiaan
	 */
	@Id(sequence="seq_panitia", function="nextsequence")
	public Long pnt_id;
	/**
	 * Nama kepanitiaan
	 */
	@Required
	public String pnt_nama;
	/**
	 * Tahun kepanitiaan
	 */
	public Integer pnt_tahun;
	/**
	 * SK Kepanitiaan
	 */
	@Required
	public String pnt_no_sk;
	/**
	 * Satuan kerja kepanitiaan
	 */
	@Required
	public Long stk_id;

	// ******************************** NEW COLUMN IN SPSE 4 ********************************

	/**
	 * Alamat Pokja
	 */
	public String pnt_alamat;
	/**
	 * Kabupaten
	 */
	public Long kbp_id;
    
    @Transient
    public Long propinsiId;
	/**
	 * Email Pokja
	 */
	@Email
	public String pnt_email;
	/**
	 * Website Pokja
	 */
	@URL
	public String pnt_website;
	/**
	 * Telepon Pokja
	 */
	@Phone
	public String pnt_telp;
	/**
	 * Fax Pokja
	 */
	@Phone
	public String pnt_fax;
	
	// relasi Agency
	public Long agc_id;

	public Long ukpbj_id;

	@Transient
	private Satuan_kerja satuan_kerja;
	@Transient
	private Kabupaten kabupaten;
	@Transient
	private Ukpbj ukpbj;
	

	public Integer is_active; // 0 non aktif, -1 aktif
	
	@Transient
	private Pegawai pegawai;
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(pnt_id);
		return pegawai;
	}
	
	public Satuan_kerja getSatuan_kerja() {
		if(satuan_kerja == null)
			satuan_kerja = Satuan_kerja.findById(stk_id);
		return satuan_kerja;
	}
	
	public Kabupaten getKabupaten() {
		if(kabupaten == null)
			kabupaten = Kabupaten.findById(kbp_id);
		return kabupaten;
	}

	@Transient
	public Ukpbj getKetuaUkpbj() {
		if (ukpbj == null && ukpbj_id != null) {
			ukpbj = Ukpbj.findById(ukpbj_id);
		}
		return ukpbj;
	}

	@Transient
	public List<Anggota_panitia> getAnggota_panitiaList()
	{
		return Anggota_panitia.find("pnt_id=?", pnt_id).fetch();
	}

	/**
	 * Fungsi {@code findByIDAndAgency} digunakan untuk mendapatkan objek
	 * {@code Panitia} dengan menggunakan informasi ID kepanitiaan dan ID
	 * Agency. Hal ini dimaksudkan untuk authorisasi terhadap data kepanitiaan
	 * yang dibuat oleh admin agency tertentu.
	 *
	 * @param pnt_id id kepanitiaan
	 * @param agencyID id agency yang didapat dari session
	 * @return objek {@code Panitia} yang bersesuaian dengan admin agency yang
	 *         membuatnya
	 */
//	public static Panitia findByIDAndAgency(Long pnt_id) {
//		return find("pnt_id = ? AND audituser=?", pnt_id,Active_user.current().userid).first();
//	}

	/**
	 * mendapatkan informasi jumlah paket draft dari kepanitiaan
	 * @param panitiaId
	 * @return
	 */
	public static Long getJumlahPaketDraft(Long panitiaId)
	{
		return Paket_panitia.count("pnt_id = ? and pkt_id in (select pkt_id from paket where pkt_status=?)", panitiaId, StatusPaket.DRAFT);
	}
	
	/**
	 * mendapatkan informasi jumlah paket aktif dari kepanitiaan
	 * @param panitiaId
	 * @return
	 */
	public static Long getJumlahPaketAktif(Long panitiaId)
	{
		return Paket_panitia.count("pnt_id = ? and pkt_id in (select pkt_id from paket where pkt_status=?)",panitiaId, StatusPaket.SEDANG_LELANG);
	}
	
	/**
	 * mendapatkan informasi jumlah paket yang mengalami lelang ulang dari kepanitiaan
	 * @param panitiaId
	 * @return
	 */
	public static Long getJumlahPaketDiulang(Long panitiaId)
	{
		return Paket_panitia.count("pnt_id = ? and pkt_id in (select pkt_id from paket where pkt_status=?)",panitiaId, StatusPaket.ULANG_LELANG);
	}

	/**
	 * Mendapatkan Informasi Nama Kepanitiaan
	 * @param panitiaId
	 * @return
     */
	public static String findNamaPanitia(Long panitiaId) {
		return Query.find("select pnt_nama from Panitia where pnt_id=?",String.class, panitiaId).first();
	}

	/**
	 * Mendapatkan Object Panitia
	 * @param lelangId
	 * @return
     */
	public static Panitia findByLelang(Long lelangId) {
		return find("pnt_id in (select p.pnt_id from paket p, lelang_seleksi l where p.pkt_id=l.pkt_id and l.lls_id=?)", lelangId).first();
	}

	public static Panitia findByPenunjukanLangsung(Long lelangId) {
		return find("pnt_id in (select p.pnt_id from ekontrak.paket p, ekontrak.nonlelang_seleksi l where p.pkt_id=l.pkt_id and l.lls_id=?)", lelangId).first();
	}
	public static Panitia findByLelangDp(Long lelangId) {
		return find("pnt_id in (select p.pnt_id from devpartner.paket_dp p, devpartner.lelang_dp l where p.pkt_id=l.pkt_id and l.lls_id=?)", lelangId).first();
	}

	public static List<Panitia > findByPegawai(Long pegawaiId) {
		return find("pnt_id in (select p.pnt_id from Anggota_panitia ap, Panitia p where peg_id=? and ap.pnt_id=p.pnt_id) ORDER BY pnt_id DESC", pegawaiId).fetch();
	}

	public static List<Panitia> findByUkpbj(Long ukpbj_id){
		return find("ukpbj_id = ?",ukpbj_id).fetch();
	}

	public boolean hadPaket() {
		if(pnt_id == null) {
			return false;
		}
		long jmlPktPl = Paket_pl_panitia.count("pnt_id = ?", pnt_id);
		long jmlPkt = Paket_panitia.count("pnt_id = ?", pnt_id);
		return (jmlPkt > 0 || jmlPktPl > 0);
	}

	public static final ResultSetHandler<String[]> resultsetPanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
			tmp[0] = rs.getString("pnt_id");
			tmp[1] = rs.getString("pnt_nama");
			tmp[2] = rs.getString("stk_nama");
			tmp[3] = rs.getString("is_active");
			tmp[4] = rs.getString("jumlah");
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetPilihPanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
			tmp[0] = rs.getString("pnt_id");
			tmp[1] = rs.getString("pnt_no_sk");
			tmp[2] = rs.getString("pnt_nama");
			tmp[3] = String.valueOf(rs.getInt("pnt_tahun"));

			Long pntId = rs.getLong("pnt_id");
			tmp[4] = rs.getString("peg_nama");
			return tmp;
		}
	};
}
