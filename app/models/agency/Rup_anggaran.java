package models.agency;

import models.common.SumberDana;

import java.util.Date;

public class Rup_anggaran {
	
	public Long id;
	
	public Long pktId;
    
	public Integer jenis;
    
	public Integer sumberDana;
    
	public String asalDanaApbn;
    
	public String mak;
    
	public Double pagu;

	public Integer tahunAnggaranDana;
    
	public Date auditupdate;

	public boolean dataLengkap(){
		return getSumberDana() != null && mak != null && !mak.isEmpty() && pagu != null;
	}

	public SumberDana getSumberDana() {
		if(sumberDana != null){
			return SumberDana.values()[sumberDana - 1];
		}
		return null;
	}
}
