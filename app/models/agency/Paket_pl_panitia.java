package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Created by Lambang on 11/20/2017.
 */
@Table(name="EKONTRAK.PAKET_PANITIA")
public class Paket_pl_panitia extends BaseModel {

    /**
     * Paket yang dibuat oleh panitia tertentu
     */
    @Id
    public Long pkt_id;
    /**
     * Objek kepanitiaan yang membuat paket
     */
    @Id
    public Long pnt_id;

    @Transient
    private Paket_pl paket;
    @Transient
    private Panitia panitia;

    public Paket_pl getPaket() {
        if(paket == null)
            paket = Paket_pl.findById(pkt_id);
        return paket;
    }
    public Panitia getPanitia() {
        if(panitia == null)
            panitia = Panitia.findById(pnt_id);
        return panitia;
    }

    public static Paket_pl_panitia getByPktId(Long pktId){
        return find("pkt_id=?", pktId).first();
    }

}
