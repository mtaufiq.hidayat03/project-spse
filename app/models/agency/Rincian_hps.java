package models.agency;

import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * model untuk Daftar Kuantitas dan harga / Rincian HPS
 * @author Arief Ardiyansah
 *
 */

public class Rincian_hps implements Serializable {	

	public String item;

	public String unit;

	public Double vol;
	
	public String unit2;

	public Double vol2;

	public Double harga;

	public Double total_harga;		
	
	public String keterangan;
	
	public Double pajak;
	
	public Double getTotalHargaBeforePajak(){
		if(total_harga == null)
			return 0.0;
		return total_harga/(1 + (pajak/100));
	}
	
	public Double getTotalHargaAfterPajak(){
		if(total_harga == null)
			return 0.0;
		return total_harga*(1 + (pajak/100));
	}

	public String getCleanedItem() {
		return this.item.replaceAll("\\s+", " ");
	}

	public String getCleanedKeterangan() {
		return this.keterangan.replaceAll("\\s+", " ");
	}

	// deserialize rincian_hps from json
	public static Rincian_hps[] fromJson(String value) {
		if(StringUtils.isEmpty(value))
			return null;
		Rincian_hps[] items = CommonUtil.fromJson(value, Rincian_hps[].class);
		if(!CommonUtil.isEmpty(items)) {
			for(Rincian_hps obj:items) {
				if (obj != null && !CommonUtil.isEmpty(obj.item)){
					obj.item = obj.item.replaceAll("\\s+", " ");;
					obj.keterangan = obj.keterangan.replaceAll("\\s+", " ");
				}
			}
		}
		return items;
	}

	// get total rincian hps
	public static Double total(Rincian_hps[] items) {
		Double total = 0.0;
		if(!ArrayUtils.isEmpty(items)) {
			for(Rincian_hps item : items)
				total += item.total_harga;
		}
		return total;
	}
}
