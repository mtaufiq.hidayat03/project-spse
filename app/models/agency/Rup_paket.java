package models.agency;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import ext.FormatUtils;
import models.agency.contracts.SirupContract;
import models.common.Kategori;
import models.common.MetodePemilihanPenyedia;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * model paket sirup
 * seluruh informasi menggunakan data sirup
 * @author LKPP
 *
 */
@Table(name = "rup_paket")
public class Rup_paket extends BaseTable implements SirupContract {

	@Id
	public Long id;
	
	public String nama;
	
	public String kegiatan;
	
	public Integer jenisBelanja;
	
	public Integer jenisPengadaan;
	
	public String volume;
	
	public Integer metodePengadaan;
	
	public Date tanggalAwalPengadaan;
	
	public Date tanggalAkhirPengadaan;
	
	public Date tanggalAwalPekerjaan;
	
	public Date tanggalAkhirPekerjaan;
	
	public String lokasi;
	
	public String keterangan;
	
	public Integer tahunAnggaran;
	
	public String idSatker;
	
	public String kodeKldi;
	
	public boolean aktif;
	
	public boolean  is_delete;
	
	public Date auditupdate;
	
	public String sumberDanaString;
	
	public String listAnggaran;
	@Transient
	public Rup_anggaran[] listPaketAnggaranM; // format json Anggaran list
	@Transient
	public Rup_anggaran_pl[] listPaketAnggaranPl; // format json Anggaran list
	
	public Double jumlah_pagu;


	public String getJenisBelanjaLabel() {
		if(jenisBelanja == 1)
			return "Barang/jasa";
		else if (jenisBelanja == 2)
			return "Modal";
		return "";			
	}
	
	public Kategori getKategori() {
		Kategori kategori = null;
		switch(jenisPengadaan) {
			case 1:kategori = Kategori.PENGADAAN_BARANG;break;
			case 2:kategori = Kategori.PEKERJAAN_KONSTRUKSI;break;
			case 3:kategori = Kategori.KONSULTANSI;break;
			case 4:kategori = Kategori.JASA_LAINNYA;break;			
		}
		return kategori;
		
	}

	public String getPemilihan() {
		MetodePemilihanPenyedia metode = MetodePemilihanPenyedia.findById(metodePengadaan);
		if(metode != null){
			return metode.getLabel();
		}
		return "Belum Ditentukan";
	}
	
	public Rup_anggaran[] getListPaketAnggaran() {
		if(listPaketAnggaranM == null && !StringUtils.isEmpty(listAnggaran)) {
			Gson gson=  new GsonBuilder().setDateFormat("MMM dd,yyyy").create();
			listPaketAnggaranM = gson.fromJson(listAnggaran, Rup_anggaran[].class);
		}
		return listPaketAnggaranM;
	}

	public boolean paketAnggaranLengkap() {
		for (Rup_anggaran rup_anggaran : listPaketAnggaranM) {
			if(!rup_anggaran.dataLengkap()){
				return false;
			}
		}

		return true;
	}

	public Rup_anggaran_pl[] getListPaketAnggaranPl() {
		if(listPaketAnggaranPl == null && !StringUtils.isEmpty(listAnggaran)) {
			Gson gson=  new GsonBuilder().setDateFormat("MMM dd,yyyy").create();
			listPaketAnggaranPl = gson.fromJson(listAnggaran, Rup_anggaran_pl[].class);
		}
		return listPaketAnggaranPl;
	}
/*

	public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {
		
		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			tmp[0] = rs.getString("id");
			tmp[1] = rs.getString("nama");
			tmp[4] = FormatUtils.formatCurrenciesJutaRupiah(rs.getDouble("jumlah_pagu"));
			tmp[5] = rs.getString("tahunAnggaran");
			tmp[2] = rs.getString("sumberdanastring");

			Rup_paket rup = Rup_paket.findById(rs.getInt("id"));

			tmp[3] = rup.getPemilihan();

//			tmp[4] = rs.getString("paket");
			return tmp;
		}
	};
*/

	public static final ResultSetHandler<String[]> resultsetPl = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			tmp[0] = rs.getString("id");
			tmp[1] = rs.getString("nama");
			tmp[4] = FormatUtils.formatCurrenciesJutaRupiah(rs.getDouble("jumlah_pagu"));
			tmp[5] = rs.getString("tahunAnggaran");
			tmp[2] = rs.getString("sumberdanastring");

			Rup_paket rup = Rup_paket.findById(rs.getInt("id"));

			tmp[3] = rup.getPemilihan();
//			tmp[4] = rs.getString("paket");
			return tmp;
		}
	};

	/**
	 * simpan rup_paket
	 * @param list
     */
	public static void simpan(Rup_paket[] list) {
		if(!CommonUtil.isEmpty(list)) {
			for (Rup_paket rup_paket : list) {
				rup_paket.listAnggaran = CommonUtil.toJson(rup_paket.listPaketAnggaranM);
				if (!ArrayUtils.isEmpty(rup_paket.listPaketAnggaranM)) {
					Double pagu = 0.0;
					for (Rup_anggaran anggaran : rup_paket.listPaketAnggaranM) {
						pagu += anggaran.pagu;
					}
					rup_paket.jumlah_pagu = pagu;
				}
				rup_paket.nama = rup_paket.nama.trim().replaceAll("\\r?\\n", "");
				rup_paket.save();
			}
		}
	}

	public List<Anggaran> getAnggaranList(Long pkt_id) {
		return Anggaran.find("ang_id in (select ang_id from paket_anggaran where rup_id=? and pkt_id=?)", id, pkt_id).fetch();
	}

	public List<Anggaran> getAnggaranList(Long pkt_id, Long ppk_id) {
		return Anggaran.find("ang_id in (select ang_id from paket_anggaran where rup_id=? and pkt_id=? and ppk_id=?)", id, pkt_id, ppk_id).fetch();
	}

	public List<Anggaran> getAnggaranPlList(Long pkt_id) {
		return Anggaran.find("ang_id in (select ang_id from ekontrak.paket_anggaran where rup_id=? and pkt_id=?)", id, pkt_id).fetch();
	}

	public List<Anggaran> getAnggaranPlList(Long pkt_id, Long ppk_id) {
		return Anggaran.find("ang_id in (select ang_id from ekontrak.paket_anggaran where rup_id=? and pkt_id=? and ppk_id=?)", id, pkt_id, ppk_id).fetch();
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public String getNama() {
		return this.nama;
	}

	@Override
	public String getSumberDanaString() {
		return this.sumberDanaString;
	}
}
