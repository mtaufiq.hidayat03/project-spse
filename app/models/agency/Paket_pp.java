package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Model {@code Paket_pp} merepresentasikan tabel paket_pp pada database.
 *
 * @author Wahid
 */
@Table(name="EKONTRAK.PAKET_PP")
public class Paket_pp extends BaseModel{
	/**
	 * Paket yang dibuat oleh pp tertentu
	 */
	@Id
	public Long pkt_id;
	/**
	 * Objek pp yang membuat paket
	 */
	@Id
	public Long pp_id;
	
	@Transient
	private Paket_pl paket;
	@Transient
	private Pp pp;

	public static Paket_pp getByPktId(Long pktId){
		return find("pkt_id=?", pktId).first();
	}

	
	public Paket_pl getPaket() {
		if(paket == null)
			paket = Paket_pl.findById(pkt_id);
		return paket;
	}
	
	public Pp getPp() {
		if(pp == null)
			pp = Pp.findById(pp_id);
		return pp;
	}

}
