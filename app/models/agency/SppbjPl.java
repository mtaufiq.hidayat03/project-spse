package models.agency;

import ext.DateBinder;
import ext.FormatUtils;
import models.common.Active_user;
import models.common.Kategori;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.kontrak.Kontrak_kso;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import models.rekanan.Landasan_hukum;
import models.rekanan.Rekanan;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Lambang on 3/14/2017.
 */
@Table(name="EKONTRAK.SPPBJ")
public class SppbjPl extends BaseModel {

    static final Template TEMPLATE_SPPBJ_PL = TemplateLoader.load("/kontrakpl/template/SPPBJ-Barang-pl.html");
    static final Template TEMPLATE_SPPBJ_KONSTRUKSI_PL = TemplateLoader.load("/kontrakpl/template/SPPBJ-Konstruksi-pl.html");

    static final Template TEMPLATE_SURAT_PERJANJIAN_PL = TemplateLoader.load("/kontrakpl/template/SuratPerjanjianBarang-pl.html");
    static final Template TEMPLATE_SURAT_PERJANJIAN_KSO_NKSO_KONSTRUKSI_PL = TemplateLoader.load("/kontrakpl/template/surat-perjanjian-barang-kso-nkso-konstruksi-pl.html");
    static final Template TEMPLATE_SURAT_PERJANJIAN_PERORANGAN_PL = TemplateLoader.load("/kontrakpl/template/surat-perjanjian-barang-perorangan-pl.html");
    static final Template TEMPLATE_SURAT_PERJANJIAN_PL_KSO= TemplateLoader.load("/kontrakpl/template/surat-perjanjian-barang-kso-pl.html");
    static final Template TEMPLATE_SURAT_PERJANJIAN_PL_NKSO = TemplateLoader.load("/kontrakpl/template/surat-perjanjian-barang-nkso-pl.html");

    public static final Integer STATUS_DRAFT = Integer.valueOf(0);
    public static final Integer STATUS_TERKIRIM = Integer.valueOf(1);
    public static final Integer STATUS_DICABUT = Integer.valueOf(2);

    @Id(sequence="ekontrak.seq_sppbj_pl", function="nextsequence")
    public Long sppbj_id;

    @Required
    public String sppbj_no;

    @As(binder= DateBinder.class)
    public Date sppbj_tgl_buat;

    @Required
    @As(binder=DateBinder.class)
    public Date sppbj_tgl_kirim;

    //relasi ke Ppk
    @Required
    public Long ppk_id;

    //relasi ke Rekanan
    @Required
    public Long rkn_id;

    //relasi ke pl_seleksi
    @Required
    public Long lls_id;

    public Long sppbj_attachment;
    public Integer sppbj_status;

    public String sppbj_kota;
    public String sppbj_lamp;
    public String sppbj_tembusan;

    public String catatan_ppk;
    public Double harga_final = 0d;

    public Double jaminan_pelaksanaan = 0d;

    public boolean sudah_kirim_pengumuman = false;

    /**
     ## tambahan field penyesuaian implementasi tgl, 17 maret 2020
     dengan implementasi PermenPUPR07-2019
     */
    public String alamat_satker;
    @Required(message = "Jabatan PPK dibutuhkan")
    public String jabatan_ppk_sppbj;
    //end penyesuaian

    @Transient
    private Pl_seleksi pl;
    @Transient
    private Ppk ppk;
    @Transient
    private Rekanan rekanan;

    public String sppbj_dkh;

    @Transient
    public DaftarKuantitas dkh;

    protected void postLoad() {
        if (!CommonUtil.isEmpty(sppbj_dkh))
            dkh = CommonUtil.fromJson(sppbj_dkh, DaftarKuantitas.class);

    }

    public Pl_seleksi getPl() {
        if(pl == null)
            pl = Pl_seleksi.findById(lls_id);
        return pl;
    }

    public Ppk getPpk() {
        if(ppk == null)
            ppk = Ppk.findById(ppk_id);
        return ppk;
    }

    public Rekanan getRekanan() {
        if(rekanan == null)
            rekanan = Rekanan.findById(rkn_id);
        return rekanan;
    }

    public static SppbjPl findByPl(Long lls_id){
        return find("lls_id=?", lls_id).first();
    }


    @Transient
    public String getStatus()
    {
        Integer status = sppbj_status;
        if(status == Sppbj.STATUS_DRAFT)
            return "Draft";
        else if(status == Sppbj.STATUS_TERKIRIM)
            return "Terkirim";
        else if(status == Sppbj.STATUS_DICABUT)
            return "Dibatalkan";
        else
            return "";
    }

    public String sanitizedFilename(File file) {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;
    }

    @Transient
    public BlobTable getBlob() {
        if(sppbj_attachment == null)
            return null;
        return BlobTableDao.getLastById(sppbj_attachment);
    }

    @Transient
    public List<BlobTable> getAttachment() {
        if(sppbj_attachment == null) {
            return null;
        }
        return BlobTableDao.listById(sppbj_attachment);
    }

    public UploadInfo simpanSppbj(File file) throws Exception{
        BlobTable blob = null;
        if(sppbj_attachment == null){
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
        }else if(sppbj_attachment != null)
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, sppbj_attachment);
        sppbj_attachment = blob.blb_id_content;
        save();
        return UploadInfo.findBy(blob);
    }

    public static InputStream cetak(Pl_seleksi pl, SppbjPl sppbjPl) {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format", new FormatUtils());
        model.put("pl", pl);
        Active_user userPPK = Active_user.current();
        Ppk ppk = Ppk.findById(userPPK.ppkId);
        model.put("ppk", ppk);

        Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM ekontrak.paket_satker WHERE pkt_id=?)", pl.pkt_id).first();
        model.put("satker", satker);
        model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
        model.put("sppbjPl", sppbjPl);
        PesertaPl peserta = PesertaPl.findBy(pl.lls_id, sppbjPl.rkn_id);
        model.put("pemenang", peserta);
        Kategori kategori = pl.getKategori();
        model.put("kategori", kategori);
        if(sppbjPl.sppbj_tembusan!=null){
            String str = sppbjPl.sppbj_tembusan;
            List<String> tembusan= Arrays.asList(str.split("\\s*,\\s*"));
            model.put("tembusan", tembusan);
        }
        Double hargaFinal= sppbjPl.harga_final;
        String terbilang = FormatUtils.number2Word(hargaFinal.longValue());
        model.put("terbilang", terbilang);

        Paket_pl paket= Paket_pl.findByLelang(pl.lls_id);
        String namapanitia= "__________________";
        String namaukpbj= "__________________";
        Panitia panitia = paket.getPanitia();
        if(panitia!=null) {
            namapanitia=panitia.pnt_nama;
            Ukpbj ukpbj= Ukpbj.findById(panitia.ukpbj_id);
            namaukpbj=ukpbj.nama;
        }
        model.put("namapanitia", namapanitia);
        model.put("namaukpbj", namaukpbj);

        String perihal="________";

        if(paket!=null)perihal= paket.pkt_nama;
        model.put("perihal", perihal);

        String dok_tgl="___________________";
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");
        dok_tgl = sdfr.format( peserta.tgl_penawaran_harga );
        model.put("dok_tgl", dok_tgl);

        String nomor="_________________";
        //Date batasBuildDate=new SimpleDateFormat("yyyyMMdd").parse("2019-10-10");
        //if(paket.pkt_tgl_build != null){
        //    Date buildDate = new SimpleDateFormat("yyyyMMdd").parse(paket.pkt_tgl_build.substring(4));
        //    if (buildDate.after(batasBuildDate))
                nomor = peserta.psr_id.toString();
        //}
        model.put("nomor", nomor);

        //PEKERJAAN_KONSTRUKSI (Permen lampiran 2)
        String terbilangJaminan ="";
        if(sppbjPl.jaminan_pelaksanaan.intValue()>0)
            terbilangJaminan = FormatUtils.number2Word(sppbjPl.jaminan_pelaksanaan);
        model.put("terbilangJaminan", terbilangJaminan);
        String masaBerlaku="_________________",terbilangMasaBerlaku ="_________________";
        if(peserta!=null){
            masaBerlaku = peserta.masa_berlaku_penawaran.toString();
            Long mb= Long.valueOf(peserta.masa_berlaku_penawaran);
            terbilangMasaBerlaku=FormatUtils.number2Word(mb);
        }
        model.put("masaBerlaku", masaBerlaku);
        model.put("terbilangMasaBerlaku", terbilangMasaBerlaku);
        String kota= sppbjPl.sppbj_kota;
        StringUtils.capitalize(kota);
        model.put("kota", kota);

        if(kategori.isJkKonstruksi() || kategori.isKonstruksi())
            return HtmlUtil.generatePDF(TEMPLATE_SPPBJ_KONSTRUKSI_PL, model);
        else
        return HtmlUtil.generatePDF(TEMPLATE_SPPBJ_PL, model);


    }

    public static InputStream cetak_perjanjian(KontrakPl kontrak) {
        Long plId = kontrak.lls_id;
        Pl_seleksi pl = kontrak.getPl_seleksi();
        SppbjPl sppbjPl = SppbjPl.find("lls_id=?", plId).first();
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format", new FormatUtils());
        model.put("pl", pl);
        Kategori kategori = pl.getKategori();
        model.put("kategori",kategori);
        model.put("sppbj", sppbjPl);
        PesertaPl peserta = PesertaPl.findBy(plId, sppbjPl.rkn_id);
        model.put("pemenang", peserta);
        model.put("kontrak", kontrak);
        Active_user userPPK = Active_user.current();
        Ppk ppk = Ppk.findById(userPPK.ppkId);
        model.put("ppk", ppk);
        model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
        if(kontrak!=null){
            DateTime tanggal_kontrak = new DateTime(kontrak.kontrak_tanggal.getTime());
            String hari = FormatUtils.day[(tanggal_kontrak.getDayOfWeek()==7 ? 0 : tanggal_kontrak.getDayOfWeek())];
            int tgl = tanggal_kontrak.getDayOfMonth();
            String bulan = FormatUtils.month[tanggal_kontrak.getMonthOfYear()-1];
            int tahun = tanggal_kontrak.getYear();
            model.put("hari", hari);
            model.put("tgl", tgl);
            model.put("bulan", bulan);
            model.put("tahun", tahun);
            DateTime sppbj_tanggal_buat = new DateTime(sppbjPl.sppbj_tgl_buat.getTime());
            int tglSppbj = sppbj_tanggal_buat.getDayOfMonth();
            String bulanSppbj = FormatUtils.month[sppbj_tanggal_buat.getMonthOfYear()-1];
            int tahunSppbj = sppbj_tanggal_buat.getYear();
            model.put("tglsppbj", tglSppbj);
            model.put("bulansppbj", bulanSppbj);
            model.put("tahunsppbj", tahunSppbj);
            String nilaiKontrak = FormatUtils.number2Word(kontrak.kontrak_nilai.longValue());
            model.put("nilai_kontrak",nilaiKontrak);
            model.put("nilai_kontrak_nominal",FormatUtils.formatDesimal(kontrak.kontrak_nilai.longValue()));
            List<Landasan_hukum> list = Landasan_hukum.findBy(sppbjPl.rkn_id);
            Landasan_hukum akta = null;
            if (list.size()!=0) {
                akta = list.get(0);
            }

            model.put("akta", akta);
            if (kontrak.kontrak_kso!=null){
                Kontrak_kso kso = kontrak.getKontrakKSO();
                model.put("kso", kso);
                String[] anggota = kso.anggota_kso.split(",");
                model.put("anggota", anggota);
            }
        }
        if(kategori.isJkKonstruksi() || kategori.isKonstruksi()){
            return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_KSO_NKSO_KONSTRUKSI_PL, model);
        }
        if(kontrak.isPerorangan())
            return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_PERORANGAN_PL, model);
        if(kontrak.isKSO())
            return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_PL_KSO, model);
        if(kontrak.isNonKSO())
            return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_PL_NKSO, model);
        return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_PL, model);
    }

    public void simpanHps(String data) throws Exception{
        DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,false);
        sppbj_dkh = CommonUtil.toJson(dk);
        save();
    }


    public static int getJumlahSppbjPlByLelangAndPpkAndRekanan(Long lelangId, Long ppkId, Long rekananId) {
        return (int)SppbjPl.count("lls_id=? AND ppk_id=? AND rkn_id=?", lelangId, ppkId, rekananId);
    }

    public static int getJumlahSppbjPlByLelangAndPpk(Long lelangId, Long ppkId) {
        return (int)SppbjPl.count("lls_id=? AND ppk_id=?", lelangId, ppkId);
    }

}
