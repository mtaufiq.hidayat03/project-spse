package models.agency;

import models.agency.contracts.PaketLokasiContract;
import models.jcommon.db.base.BaseModel;
import models.sso.common.Kabupaten;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

/**
 * Created by Lambang on 4/19/2017.
 */
@Table(name="EKONTRAK.PAKET_SWAKELOLA_LOKASI")
public class Paket_swakelola_lokasi extends BaseModel implements PaketLokasiContract{

    /**
     * Id tabel
     */
    @Id(sequence="ekontrak.seq_paket_swakelola_lokasi", function="nextsequence")
    public Long pkl_id;
    /**
     * Relasi dengan tabel {@code paket}
     */
    public Long swk_id;

    @Required
    public Long kbp_id;
    /**
     * Detil lokasi pelelangan
     */
    @Required
    public String pkt_lokasi;

    // versi paket lokasi. disamakan dengan versi lelang
    public Integer pkt_lokasi_versi;

    @Transient
    private Paket_swakelola paket;
    @Transient
    private Kabupaten kabupaten;

    public Paket_swakelola getPaket() {
        if(paket == null)
            paket = Paket_swakelola.findById(swk_id);
        return paket;
    }

    public static Integer getLatestVersion(Long swk_id) {
        Paket_swakelola_lokasi paket_lokasi = Paket_swakelola_lokasi.find("swk_id=? order by pkt_lokasi_versi desc", swk_id).first();
        return paket_lokasi==null || paket_lokasi.pkt_lokasi_versi == null ? 1 : paket_lokasi.pkt_lokasi_versi;
    }

    public static List<Paket_swakelola_lokasi> getNewestByPktId(Long swk_id) {
        Integer latestVersion = getLatestVersion(swk_id);
        return find("swk_id=? and pkt_lokasi_versi=?", swk_id, latestVersion).fetch();
    }

    @Override
    public boolean isPklIdExist() {
        return pkl_id != null;
    }

    @Override
    public Long getPrpId() {
        return getKabupaten() != null ? getKabupaten().prp_id : null;
    }

    public Kabupaten getKabupaten() {
        if(kabupaten == null)
            kabupaten = Kabupaten.findById(kbp_id);
        return kabupaten;
    }

}
