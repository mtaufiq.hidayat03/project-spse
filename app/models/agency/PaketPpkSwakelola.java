package models.agency;


import models.common.Active_user;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**
 * @author Abdul Wahid on 8/2/2019
 */
@Table(name = "paket_ppk_swakelola", schema = "ekontrak")
public class PaketPpkSwakelola extends BaseModel{

    @Id
    public Long swk_id;
    @Id
    public Long ppk_id;

    public PaketPpkSwakelola(Long swk_id, Long ppk_id){
        this.swk_id = swk_id;
        this.ppk_id = ppk_id;

    }

    public PaketPpkSwakelola(Long swk_id, Active_user active_user) {
        this.swk_id = swk_id;
        this.ppk_id = active_user.ppkId;

    }

    public boolean isItMyPaket(Long ppkId) {
        return ppk_id.equals(ppkId);
    }
}
