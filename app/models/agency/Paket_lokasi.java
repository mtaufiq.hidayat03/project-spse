package models.agency;

import models.agency.contracts.PaketLokasiContract;
import models.common.Active_user;
import models.jcommon.db.base.BaseModel;
import models.sso.common.Kabupaten;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Collections;
import java.util.List;

/**
 * Kelas model untuk tabel {@code paket_lokasi} di database. Digunakan untuk membuat draf lelang.
 * Created by IntelliJ IDEA.
 * Date: 04/09/12
 * Time: 15:02
 *
 * @author I Wayan Wiprayoga W
 */
@Table(name="PAKET_LOKASI")
public class Paket_lokasi extends BaseModel implements PaketLokasiContract {
	/**
	 * Id tabel
	 */
	@Id(sequence="seq_paket_lokasi", function="nextsequence")
	public Long pkl_id;
	/**
	 * Relasi dengan tabel {@code paket}
	 */
	public Long pkt_id;
	
	@Required
	public Long kbp_id;
	/**
	 * Detil lokasi pelelangan
	 */
	@Required
	public String pkt_lokasi;

	public Integer pkt_lokasi_versi;
	
	@Transient
	private Paket paket;
	@Transient
	private Kabupaten kabupaten;

	public static Integer getLatestVersion(Long pkt_id) {
		Paket_lokasi paket_lokasi = Paket_lokasi.find("pkt_id=? order by pkt_lokasi_versi desc", pkt_id).first();
		return paket_lokasi==null || paket_lokasi.pkt_lokasi_versi == null ? 1 : paket_lokasi.pkt_lokasi_versi;
	}
    public static List<Paket_lokasi> getNewestByPktId(Long pkt_id) {
		Integer latestVersion = getLatestVersion(pkt_id);
		return find("pkt_id=? and pkt_lokasi_versi=?", pkt_id, latestVersion).fetch();
    }

    public Paket getPaket() {
		if(paket == null)
			paket = Paket.findById(pkt_id);
		return paket;
	}

	@Override
	public Kabupaten getKabupaten() {
		if(kabupaten == null)
			kabupaten = Kabupaten.findById(kbp_id);
		return kabupaten;
	}

	public boolean showTrashCan(boolean isPaketDraft, int totalItems) {
		return isPaketDraft && totalItems > 1 && !Active_user.current().isKuppbj();
	}

	@Override
	public Long getPrpId() {
		return getKabupaten() != null ? getKabupaten().prp_id : null;
	}

	@Override
	public boolean isPklIdExist() {
		return pkl_id != null;
	}


	public static List<Paket_lokasi> findByPaket(Long paketId) {
		if(paketId == null)
			return Collections.EMPTY_LIST;
		return find("pkt_id=?", paketId).fetch();
	}
}
