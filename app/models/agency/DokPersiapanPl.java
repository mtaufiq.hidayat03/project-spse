package models.agency;

import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.lelang.SskkContent;
import models.nonlelang.Dok_pl;
import models.nonlelang.Dok_pl_content;
import models.nonlelang.Pl_seleksi;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.util.Date;
import java.util.List;

@Table(name="ekontrak.dok_persiapan")
public class DokPersiapanPl extends BaseModel{

    public static final String TAG = "DokPersiapanPl";

    @Id(sequence="seq_dok_persiapan", function="nextsequence")
    public Long dp_id;

    @Id
    public Integer dp_versi;

    public Long pkt_id;

    public String dp_dkh;

    public Long dp_spek;

    public String dp_sskk;

    public Long dp_sskk_attachment;

    public Long dp_survey;

    public boolean dp_modified;

    @Transient
    public DaftarKuantitas dkh;

    @Transient
    public SskkContent sskkContent;

    /**
     * field ini untuk menyimpan informasi dokumen lainnya
     */
    public Long dp_lainnya;

    @Transient
    public List<BlobTable> dokSpek;

    @Override
    protected void postLoad() {
        if (!CommonUtil.isEmpty(dp_dkh))
            dkh = CommonUtil.fromJson(dp_dkh, DaftarKuantitas.class);
        if(!CommonUtil.isEmpty(dp_sskk))
            sskkContent = CommonUtil.fromJson(dp_sskk,SskkContent.class);
    }

    public void prePersist() {
        super.prePersist();
        if (dkh != null)
            dp_dkh = CommonUtil.toJson(dkh);
        if (sskkContent != null)
            dp_sskk = CommonUtil.toJson(sskkContent);
    }

    @Transient
    public List<BlobTable> getDokSpek() {
        if(dp_spek == null){
           return null;
            }
        return BlobTableDao.listById(dp_spek);
    }

    @Transient
    public List<BlobTable> getDokSskkAttachment() {
        if(isAdendum()){
            dp_sskk_attachment = getSskkAttachment();
        }

        if(dp_sskk_attachment == null){
            return null;

        }
        return BlobTableDao.listById(dp_sskk_attachment);
    }

    public List<BlobTable> getDokLainnya(){
        if(dp_lainnya == null){
            return null;
        }
        return BlobTableDao.listById(dp_lainnya);
    }

    public List<BlobTable> getDokSurvey(){
        if(dp_survey == null){
            return null;
        }
        return BlobTableDao.listById(dp_survey);
    }


    /**
     * Mendapatkan Daftar Kuantitas terakhir yang berlaku
     * digunakan oleh Apendo
     * @return
     */
    @Transient
    public DaftarKuantitas getRincianHPS() {
        String content = Query.find("SELECT dp_dkh FROM ekontrak.dok_persiapan WHERE pkt_id=? AND dp_dkh is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", String.class, pkt_id).first();
        if (!CommonUtil.isEmpty(content)){
            return CommonUtil.fromJson(content, DaftarKuantitas.class);
        }else{
            Pl_seleksi pl = Pl_seleksi.findByPaket(pkt_id);
            Dok_pl dokPl = pl.getDokumenLelang();
            Dok_pl_content dok_pl_content = dokPl.getDokumenLelangContent();
            DaftarKuantitas dk = dok_pl_content.dkh;

            if(dk == null){
                dk = dokPl.getRincianHPS();
            }
            return dk;

        }
    }


    @Transient
    public Long getSpek() {
        return Query.find("SELECT dp_spek FROM ekontrak.dok_persiapan WHERE pkt_id=? AND dp_spek is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public Long getSpekAdendum() {
        return Query.find("SELECT dp_spek FROM ekontrak.dok_persiapan WHERE pkt_id=? AND dp_spek is not null ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public Long getSskkAttachment() {
        return Query.find("SELECT dp_sskk_attachment FROM ekontrak.dok_persiapan WHERE pkt_id=? AND dp_sskk_attachment is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", Long.class, pkt_id).first();
    }

    @Transient
    public SskkContent getOldSskkContent() {
        String content = Query.find("SELECT dp_sskk FROM ekontrak.dok_persiapan WHERE pkt_id=? AND dp_sskk is not null AND dp_modified='FALSE' ORDER BY dp_versi DESC", String.class, pkt_id).first();
        if (!CommonUtil.isEmpty(content))
            return CommonUtil.fromJson(content, SskkContent.class);
        return null;
    }

    public static DokPersiapanPl findOrCreateByPaket(Long pkt_id){
        Paket_pl paket = Paket_pl.findById(pkt_id);
        return findOrCreateByPaket(paket);
    }

    public static DokPersiapanPl findOrCreateByPaket(Paket_pl paket){
        Pl_seleksi pl = Pl_seleksi.findByPaket(paket.pkt_id);
        DokPersiapanPl dokPersiapan = paket.getDokPersiapan();

        if(dokPersiapan == null){
            dokPersiapan = new DokPersiapanPl();
            dokPersiapan.pkt_id = paket.pkt_id;

            if(!paket.isFlag43()){
                Dok_pl dokPl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
                Dok_pl_content dok_pl_content = dokPl.getDokumenLelangContent();
                dokPersiapan.dp_versi = dok_pl_content.dll_versi;
            }else{
                dokPersiapan.dp_versi = 1;
            }

            dokPersiapan.save();
        }

        return dokPersiapan;

    }

    public static DokPersiapanPl findLastByPaket(Long pkt_id) {
        return find("pkt_id=? and dp_versi=?", pkt_id,1).first();
    }


    public static void simpanHps(Paket_pl paket, String data, boolean fixed, boolean enableViewHps) throws Exception{
        DokPersiapanPl dokPersiapan = findOrCreateByPaket(paket);
        DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,fixed);

        if (dk.total == null)
            throw new Exception(Messages.get("agency.abmr_hps"));
        else if (dk.total > paket.pkt_pagu) {
            throw new Exception(Messages.get("agency.nt_hps_mp"));
        } else { // hanya diijinkan simpan jika jumlahnya tidak melibihi dengan pagu paket

            paket.pkt_hps = dk.total;
            paket.pkt_hps_enable = enableViewHps;
            paket.save();

            dokPersiapan.dkh = dk;
            dokPersiapan.dp_modified = true;

            if(paket.ukpbj_id != null || paket.getPaketPp() != null){
                dokPersiapan.transferDokPersiapanToDokLelang();
            }

            dokPersiapan.save();

        }
    }

    public Long simpanBlobSpek(List<BlobTable> blobList) throws Exception{
        Long newId = getNextSequenceValue("seq_epns");

        for (BlobTable blob : blobList){
            BlobTable newBlob = new BlobTable();
            newBlob.blb_id_content = newId;
            newBlob.blb_versi = blob.blb_versi;
            newBlob.blb_date_time = blob.blb_date_time;
            newBlob.blb_mirrored = blob.blb_mirrored;
            newBlob.blb_path = blob.blb_path;
            newBlob.blb_engine = blob.blb_engine;
            newBlob.save();

            if(newId == null){
                newId = newBlob.blb_id_content;
            }
        }
        return newId;
    }

    public UploadInfo simpanSpek(File file) throws Exception {
        String path = sanitizedFilename(file);
        File newFile = new File(path);
        BlobTable model = findByFilename(newFile.getName(), getDokSpek());
        if (model != null) {
            LogUtil.debug("DokLelangContent", "filename's duplicated");
            return null;
        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dp_spek != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_spek);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }
        dp_spek = blob.blb_id_content;

        Paket_pl paket = Paket_pl.findById(pkt_id);
        if(paket.ukpbj_id != null || paket.getPaketPp() != null){
            transferDokPersiapanToDokLelang();
        }
        save();
        return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanSskkAttachment(File file) throws Exception{
        String path = sanitizedFilename(file);
        File newFile = new File(path);
            file.renameTo(newFile);
            BlobTable blob = null;
            if (dp_sskk_attachment != null) {
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_sskk_attachment);
            } else {
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
            }

            dp_sskk_attachment = blob.blb_id_content;
            Paket_pl paket = Paket_pl.findById(pkt_id);

            if(paket.ukpbj_id != null){
                transferDokPersiapanToDokLelang();
            }
            save();

            return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanLainnya(File file) throws Exception{
        String path = sanitizedFilename(file);
        File newFile = new File(path);
        BlobTable model = findByFilename(newFile.getName(), getDokLainnya());
        if (model != null) {
            LogUtil.debug("DokLelangContent", "filename's duplicated");
            return null;
        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dp_lainnya != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_lainnya);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }

        dp_lainnya = blob.blb_id_content;
        Paket_pl paket = Paket_pl.findById(pkt_id);

        if(paket.ukpbj_id != null){
            transferDokPersiapanToDokLelang();
        }
        save();

        return UploadInfo.findBy(blob);
    }

    public UploadInfo simpanSurvey(File file) throws Exception{
        String path = sanitizedFilename(file);
        File newFile = new File(path);
        BlobTable model = findByFilename(newFile.getName(), getDokSurvey());
        if (model != null) {
            LogUtil.debug("DokSurveyt", "filename's duplicated");
            return null;
        }
        file.renameTo(newFile);
        BlobTable blob = null;
        if (dp_survey != null) {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, dp_survey);
        } else {
            blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
        }

        dp_survey = blob.blb_id_content;
        save();

        return UploadInfo.findBy(blob);
    }

    public BlobTable findByFilename(String filename, List<BlobTable> blobs) {
        if (CommonUtil.isEmpty(blobs)) {
            LogUtil.debug("DokPersiapanPl", "total blob is empty or object null");
            return null;
        }
        for (BlobTable model : blobs) {
            LogUtil.debug("DokPersiapanPl", filename + " contains " + model.blb_nama_file);
            if (model.blb_nama_file.equalsIgnoreCase(filename)) {
                return model;
            }
        }
        return null;
    }

    public String sanitizedFilename(File file) {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;
    }

    public boolean isAdendum(){
        return dp_versi > 1;
    }

    public boolean isEditable(Paket_pl paket, Date currentDate){
        if(paket.pkt_status.isSedangLelang()){
            Pl_seleksi pl = Pl_seleksi.findByPaket(pkt_id);
            Dok_pl dok_pl = pl.getDokumenLelang();
            return paket.isEditable() || dok_pl.isEditable(pl, currentDate);
        }
        return paket.isEditable();
    }

    public void transferDokPersiapanToDokLelang() {
        Pl_seleksi lelang = Pl_seleksi.findByPaket(this.pkt_id);
        Dok_pl dokLelang = Dok_pl.findNCreateBy(lelang.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        Dok_pl_content dokLelangContent = Dok_pl_content.findNCreateBy(dokLelang.dll_id, lelang);

        dokLelangContent.dkh = this.dkh;
        dokLelangContent.dll_spek = this.dp_spek;
        dokLelangContent.sskk_content = this.sskkContent;
        dokLelangContent.dll_sskk_attachment = this.dp_sskk_attachment;
        dokLelangContent.dll_lainnya = this.dp_lainnya;
        dokLelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dokLelangContent);
    }

    public void transferHpsToDokPl(){
        Pl_seleksi pl = Pl_seleksi.findByPaket(this.pkt_id);
        Dok_pl dokpl = Dok_pl.findNCreateBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dokpl.dll_id, pl);

        dok_pl_content.dkh = this.dkh;
        dokpl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
    }

    public void transferSskkToDokPl(){
        Pl_seleksi pl = Pl_seleksi.findByPaket(this.pkt_id);
        Dok_pl dokpl = Dok_pl.findNCreateBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dokpl.dll_id, pl);

        dok_pl_content.sskk_content = this.sskkContent;
        dok_pl_content.dll_content_attachment = this.dp_sskk_attachment;

        dokpl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
    }

    public boolean lengkap(){
        return dp_sskk_attachment != null && dp_dkh != null && dp_spek != null;
    }


}
