package models.agency;

import models.common.Klasifikasi;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;

/**
 * Model {@code Klasifikasi_paket} merepresentasikan tabel Klasifikasi_paket
 * pada database.
 *
 * @author I Wayan Wiprayoga W
 * @author Arief Ardiyansah
 */

@Table(name="KLASIFIKASI_PAKET")
public class Klasifikasi_paket extends BaseModel {

	/**
	 * Sama maknanya dengan variabel {@code kla_id}, namun dengan referensi
	 * model {@code Klasifikasi}
	 */
	@Id
	public Long kla_id;
	/**
	 * Sama maknanya dengan variabel {@code id_syarat_paket}, namun dengan
	 * referensi model {@code Syarat_paket}
	 */
	@Id
	public Long id_syarat_paket;
	@Transient
	private Klasifikasi klasifikasi;
	@Transient
	private Syarat_paket syarat_paket;
	
	public Klasifikasi getKlasifikasi() {
		if(klasifikasi == null)
			klasifikasi = Klasifikasi.findById(kla_id);
		return klasifikasi;
	}
	
	public Syarat_paket getSyarat_paket() {
		if(syarat_paket == null)
			syarat_paket = Syarat_paket.findById(id_syarat_paket);
		return syarat_paket;
	}
}
