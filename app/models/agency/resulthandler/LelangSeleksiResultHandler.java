package models.agency.resulthandler;

import ext.FormatUtils;
import models.common.*;
import models.jcommon.util.CommonUtil;
import org.sql2o.ResultSetHandler;
import play.i18n.Messages;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author HanusaCloud on 5/4/2018
 */
public class LelangSeleksiResultHandler {

    public static final ResultSetHandler<String[]> resultsetLelang = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[14];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            tmp[0] = lelangId.toString();
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_nama");
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
            if(versi > 1)
                namaLelang += " <span class='badge  badge-warning'>" + tenderUlangLabel + "</span>";
            tmp[1] = namaLelang;
            tmp[2] = rs.getString("nama_instansi");
            tmp[3] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[4] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
            tmp[5] = Metode.findById(rs.getInt("mtd_id")).label;
            tmp[6] = pemilihan.label;
            tmp[7] = MetodeEvaluasi.findById(rs.getInt("mtd_evaluasi")).label;
            tmp[8] = Kategori.findById(rs.getInt("kgr_id")).getNama()  + " - "+Messages.get("tag.ta")+" " + rs.getString("anggaran");
            tmp[9] = pkt_flag.toString();
            tmp[10] = rs.getString("kontrak_id") == null || rs.getString("kontrak_nilai") == null ? "Nilai Kontrak belum dibuat" : FormatUtils.formatCurrencyRupiah(rs.getDouble("kontrak_nilai"));
            tmp[11] = rs.getString("lls_evaluasi_ulang");
            tmp[12] = rs.getString("lls_penawaran_ulang");
            tmp[13] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";

            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetForensik = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[3];
            Long lelangId = rs.getLong("lls_id");
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = rs.getString("auditupdate");
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetLelangBaru = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            String[] tmp = new String[6];
            tmp[0] = rs.getString("lls_id");
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_hps"));
            tmp[3] = pemilihan.label+" "+(rs.getInt("lls_versi_lelang") > 1 ?"Ulang":"");
            tmp[4] = rs.getString("pkt_flag");
            tmp[5] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetLelangPanitia = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_nama");
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
            if(versi > 1)
                namaLelang += " <span class='badge  badge-warning'>" + tenderUlangLabel + "</span>";
            String[] tmp = new String[7];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            tmp[0] = lelangId.toString();
            tmp[1] = namaLelang;
            tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[3] = rs.getString("peserta");
            tmp[4] = pkt_flag.toString();
            tmp[5] = pemilihan.label;
            tmp[6] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetLelangRekanan = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            Integer versi = rs.getInt("lls_versi_lelang");
            String namaLelang = rs.getString("pkt_nama");
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
            if(versi > 1)
                namaLelang += " <span class='badge  badge-warning'>" + tenderUlangLabel + "</span>";
            tmp[0] = lelangId.toString();
            tmp[1] = namaLelang;
            tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[3] = pkt_flag.toString();
            tmp[4] = pemilihan.label;
            tmp[5] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetSummary = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[10];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            StatusLelang statusLelang = StatusLelang.fromValue(rs.getInt("lls_status"));
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama") +(statusLelang.isDitutup() ? " <span class='badge  badge-danger'>" + statusLelang.label + "</span>":"");
            tmp[2] = FormatUtils.formatDateInd(rs.getTimestamp("lls_dibuat_tanggal"));
            tmp[3] = rs.getString("nama");
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_pagu"));
            tmp[6] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
            if(statusLelang.isAktif())
                tmp[7] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            if(statusLelang.isDitutup())
                tmp[7] = Tahap.SUDAH_SELESAI.getLabel();
            tmp[8] = null;
            tmp[9] = rs.getString("pkt_id");
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetkontrak = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[10];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = rs.getString("agc_nama");
            if(CommonUtil.isEmpty(tmp[2]))
                tmp[2] = rs.getString("nama");
            tmp[3] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[4] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_hps"));
            tmp[5] = Metode.findById(rs.getInt("mtd_id")).label;
            tmp[6] = MetodePemilihan.findById(rs.getInt("mtd_pemilihan")).label;
            tmp[7] = MetodeEvaluasi.findById(rs.getInt("mtd_evaluasi")).label;
            tmp[8] = Kategori.findById(rs.getInt("kgr_id")).getNama();
            tmp[9] = rs.getString("brc_id");
            return tmp;
        }
    };


    public static final ResultSetHandler<String[]> resultsetRekapAuditor = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[9];
            Long lelangId = rs.getLong("lls_id");
            Integer pkt_flag = rs.getInt("pkt_flag");
            boolean lelangV3 = pkt_flag < 2;
            StatusLelang statusLelang = StatusLelang.fromValue(Integer.valueOf(rs.getString("lls_status")));
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            tmp[0] = lelangId.toString();
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = FormatUtils.formatCurrenciesJuta(rs.getDouble("pkt_pagu"));
            tmp[3] = rs.getString("stk_nama");
            tmp[4] = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            tmp[5] = pkt_flag.toString();
            tmp[6] = rs.getString("pkt_id");
            tmp[7] = statusLelang.isDitutup() ? Messages.get("ct.tender_batal") : "";
            tmp[8] = rs.getString("lls_versi_lelang");

            return tmp;
        }
    };

}
