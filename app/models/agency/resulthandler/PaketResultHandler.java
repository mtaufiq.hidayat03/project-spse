package models.agency.resulthandler;

import ext.FormatUtils;
import models.common.*;
import models.lelang.Evaluasi;
import org.sql2o.ResultSetHandler;
import play.i18n.Messages;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author HanusaCloud on 5/3/2018
 */
public class PaketResultHandler {


    public static final ResultSetHandler<String[]> resultsetPaket = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[4];
            tmp[0] = rs.getString("pkt_id");
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = rs.getString("stk_nama");
            tmp[3] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_pagu"));
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPaketList = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Long lelangId = rs.getLong("lls_id");
            Integer pktFlag = rs.getInt("pkt_flag");
            boolean lelangV3 = pktFlag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            StatusLelang status = StatusLelang.fromValue(rs.getInt("lls_status"));
            Active_user user = Active_user.current();
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
            String[] tmp = new String[20];
            tmp[0] = rs.getString("pkt_id");
            tmp[1] = rs.getString("pkt_nama");
            boolean isFinished = false;
            final boolean isVersion4 = pktFlag.equals(2) || pktFlag.equals(3);
            if (status.isDraft()) {
                tmp[2] = "Draft";
            } else  {
                if (jadwal.equalsIgnoreCase("Tender Sudah Selesai") && !status.isDraft()) {
                    tmp[2] = "Tender Sudah Selesai";
                    isFinished = true;
                } else {
                    tmp[2] = status.label;
                }
            }
            tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = lelangId.toString();
            tmp[6] = pktFlag.toString();
            tmp[7] = pemilihan.label;
            tmp[8] = rs.getString("lls_versi_lelang");
            //show create tender button
            tmp[9] = String.valueOf((isVersion4) && user.isPanitia());
            //show delete button
            tmp[10] = String.valueOf(status.isDraft() && Active_user.current().isPpk() && rs.getString("ukpbj_id") == null);
            //show view lelang
            tmp[11] = String.valueOf((status.isAktif() || isFinished || tmp[2].equals("Lelang Ditutup")) && user.isPanitia());
            //labeling
            tmp[12] = isVersion4 ? "spse 4" : "spse 3";
            //highlight
            tmp[13] = isVersion4 ? "badge badge-danger" : "badge badge-success";
            //Panitia
            tmp[14] = String.valueOf(user.isPanitia());
            //PPK
            tmp[15] = String.valueOf(user.isPpk());

            tmp[16] = rs.getString("lls_evaluasi_ulang");

            tmp[17] = rs.getString("lls_penawaran_ulang");

            tmp[18] = tenderUlangLabel;

            tmp[19] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";

            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPaketKuppbj = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Long lelangId = rs.getLong("lls_id");
            Integer pktFlag = rs.getInt("pkt_flag");
            boolean lelangV3 = pktFlag < 2;
            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
            String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3, pemilihan.isLelangExpress());
            StatusLelang status = StatusLelang.fromValue(rs.getInt("lls_status"));
            Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
            String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
            String[] tmp = new String[16];
            tmp[0] = rs.getString("pkt_id");
            tmp[1] = rs.getString("pkt_nama");
            final boolean isVersion4 = pktFlag.equals(2) || pktFlag.equals(3);
            if (status.isDraft()) {
                tmp[2] = "Draft";
            } else  {
                if (jadwal.equalsIgnoreCase("Tender Sudah Selesai") && !status.isDraft()) {
                    tmp[2] = "Tender Sudah Selesai";
                } else {
                    tmp[2] = status.label;
                }
            }
            tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = lelangId.toString();
            tmp[6] = pktFlag.toString();
            tmp[7] = pemilihan.label;
            tmp[8] = rs.getString("lls_versi_lelang");
            //show create tender button
            tmp[9] = rs.getString("pnt_nama");
            //labeling
            tmp[12] = isVersion4 ? "spse 4" : "spse 3";
            //highlight
            tmp[13] = isVersion4 ? "badge badge-danger" : "badge badge-success";

            tmp[14] = tenderUlangLabel;

            tmp[15] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPaketKonsolidasi = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[4];
            tmp[0] = rs.getString("pkt_id");
            tmp[1] = rs.getString("pkt_nama");
            tmp[2] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
            tmp[3] = rs.getString("pkt_id_konsolidasi");

            return tmp;
        }
    };
}
