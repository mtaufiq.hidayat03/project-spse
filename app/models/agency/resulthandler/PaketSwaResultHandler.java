package models.agency.resulthandler;


import ext.FormatUtils;
import models.agency.Paket_swakelola;
import models.agency.Paket_swakelola.StatusPaketSwakelola;
import models.common.Active_user;
import org.sql2o.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author HanusaCloud on 5/3/2018
 */
public class PaketSwaResultHandler {

    public static final ResultSetHandler<String[]> resultsetPaketList = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            Long swakelolaId = rs.getLong("lls_id");
            Integer pktFlag = rs.getInt("pkt_flag");
            boolean lelangV3 = pktFlag < 2;
//            MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
//            String jadwal = Tahap.tahapInfo(rs.getString("tahaps"), true, lelangV3);
            StatusPaketSwakelola status = StatusPaketSwakelola.fromValue(rs.getInt("pkt_status"));
            Active_user user = Active_user.current();
//            TipeSwakelola kgr = TipeSwakelola.findById(rs.getInt("tipe_swakelola"));
//            String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
            String[] tmp = new String[20];
            tmp[0] = rs.getString("swk_id");
            tmp[1] = rs.getString("pkt_nama");
//            int currentEvaVersi = Evaluasi.findCurrentVersi(lelangId);
            boolean isFinished = false;
            final boolean isVersion4 = pktFlag.equals(2) || pktFlag.equals(3);
            tmp[2] = Paket_swakelola.StatusPaketSwakelola.fromValue(rs.getInt("pkt_status")).label;
            tmp[3] = FormatUtils.formatDateInd(rs.getDate("pkt_tgl_buat"));
            tmp[4] = rs.getString("stk_nama");
            tmp[5] = swakelolaId.toString();
            tmp[6] = pktFlag.toString();
//            tmp[7] = pemilihan.label;
//            tmp[8] = rs.getString("lls_versi_lelang");
            //show create tender button
            tmp[7] = String.valueOf((isVersion4) && user.isPpk());
            //show delete button
            tmp[8] = String.valueOf(status.isDraft() && Active_user.current().isPpk());
            //show view lelang
//            tmp[9] = String.valueOf(( isFinished || tmp[2].equals("Swakelola Ditutup")) && user.isPpk());
            //labeling
            tmp[9] = isVersion4 ? "spse 4" : "spse 3";
            //highlight
            tmp[10] = isVersion4 ? "badge badge-danger" : "badge badge-success";
            //Panitia
//            tmp[12] = String.valueOf(user.isPpk());
            //PPK
//            tmp[15] = String.valueOf(user.isPpk());

//            tmp[16] = String.valueOf(currentEvaVersi);

//            tmp[17] = rs.getString("lls_penawaran_ulang");

//            tmp[18] = tenderUlangLabel;

//            tmp[19] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";

            return tmp;
        }
    };

}
