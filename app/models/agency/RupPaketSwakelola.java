package models.agency;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controllers.BasicCtr;
import ext.FormatUtils;
import models.agency.contracts.SirupContract;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import play.mvc.Http;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by Lambang on 4/13/2017.
 */
@Table(name = "EKONTRAK.RUP_PAKET_SWAKELOLA")
public class RupPaketSwakelola extends BaseTable implements SirupContract {

    @Id
    public Long id;

    public String nama;

    public String llsVolume;

    public Date tanggalAwalPekerjaan;

    public Date tanggalAkhirPekerjaan;

    public String lokasi;

    public String keterangan;

    public Integer tahunAnggaran;

    public String idSatker;

    public String kodeKldi;

    public boolean umumkan;

    public boolean aktif;

    public boolean  is_delete;

    public Date auditupdate;

    public String sumberDanaString;

    public String listAnggaran;
    @Transient
    public Rup_anggaran[] listPaketAnggaranM; // format json Anggaran list

    public Double jumlah_pagu;

    public Rup_anggaran[] getListPaketAnggaran() {
        if(listPaketAnggaranM == null && !StringUtils.isEmpty(listAnggaran)) {
            Gson gson=  new GsonBuilder().setDateFormat("MMM dd,yyyy").create();
            listPaketAnggaranM = gson.fromJson(listAnggaran, Rup_anggaran[].class);
        }
        return listPaketAnggaranM;
    }

    public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[5];
            tmp[0] = rs.getString("id");
            tmp[1] = rs.getString("nama");
            tmp[2] = FormatUtils.formatCurrenciesJutaRupiah(rs.getDouble("jumlah_pagu"));
            tmp[3] = rs.getString("tahunAnggaran");
            return tmp;
        }
    };

    /**
     * simpan rup_paket
     * @param list
     */
    public static void simpan(RupPaketSwakelola[] list ,String rup_stk_id, Integer tahun) {
        if(!CommonUtil.isEmpty(list)) {
            for (RupPaketSwakelola rup_paket_swakelola : list) {
                rup_paket_swakelola.listAnggaran = CommonUtil.toJson(rup_paket_swakelola.listPaketAnggaranM);
                if (!ArrayUtils.isEmpty(rup_paket_swakelola.listPaketAnggaranM)) {
                    Double pagu = 0.0;
                    for (Rup_anggaran anggaran : rup_paket_swakelola.listPaketAnggaranM) {
                        pagu += anggaran.pagu;
                    }
                    rup_paket_swakelola.jumlah_pagu = pagu;
                }
                rup_paket_swakelola.nama = rup_paket_swakelola.nama.trim().replaceAll("\\r?\\n", "");
                rup_paket_swakelola.idSatker = rup_stk_id;
                rup_paket_swakelola.tahunAnggaran = tahun;
                rup_paket_swakelola.save();
            }
        }
    }

    public boolean paketAnggaranLengkap() {
        if(listPaketAnggaranM == null) {
            return false;
        }
        for (Rup_anggaran rup_anggaran : listPaketAnggaranM) {
            if(!rup_anggaran.dataLengkap()){
                return false;
            }
        }

        return true;
    }

    private static final String URL_API_PAKET_SWA_SIRUP = BasicCtr.SIRUP_URL+"/service/paketSwakelolaPerSatkerTampil";

    public static void updateFromSirup(String rup_stk_id, Integer tahun) {
        try {
            WSRequest request = WS.url(BasicCtr.SIRUP_URL+"/service/paketSwakelolaPerSatkerTampil?idSatker=" + rup_stk_id + "&tahunAnggaran=" + tahun);
            Logger.debug("get data paket swakelola sirup");
            HttpResponse response = request.get();
            if (response.getStatus() == Http.StatusCode.OK) {
                String content = response.getString();
                RupPaketSwakelola[] list = CommonUtil.fromJson(content, RupPaketSwakelola[].class);
                simpan(list,rup_stk_id, tahun);
            }
        }catch (Exception e) {
            Logger.error(e, "Kendala Akses Service Paket Swakelola Sirup");
        }
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public String getNama() {
        return this.nama;
    }

    @Override
    public String getSumberDanaString() {
        return this.sumberDanaString;
    }

}
