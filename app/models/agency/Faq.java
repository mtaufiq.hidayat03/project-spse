package models.agency;

import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Email;
import play.data.validation.MinSize;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

/**
 * Model {@code Faq} merepresentasikan tabel faq pada database. Kelas ini
 * mengimplementasikan <i>interface</i> {@code IEncryptedId} untuk enkripsi
 * ID-nya.<br />
 * TODO: Perlukah enkripsi ID untuk model ini?
 *
 * @author Oscar Kurniawan
 * @author I Wayan Wiprayoga W
 */

@Table(name="FAQ")
public class Faq extends BaseModel {

	/**
	 * Enum {@code FaqStatus} merupakan enumerasi untuk status FAQ.<br />
	 * Status terjawab bernilai 1 dan status belum terjawab bernilai 0.
	 */
	@Enumerated(EnumType.ORDINAL)
	public enum FaqStatus {
		SUDAH_DIJAWAB(2, "Sudah Di Jawab"), TERJAWAB(1, "Terjawab"), BELUM_TERJAWAB(0,"Belum Terjawab");

		public int value;
		public String label;

		/**
		 * Default constructor
		 * @param i value
		 */
		FaqStatus(int i, String label) {
			this.value = i;
			this.label = label;
		}
		/**
		 * Fungsi {@code getValue} digunakan untuk mendapatkan nilai status FAQ
		 *
		 * @return nilai status FAQ
		 */
		public int getValue() {
			return this.value;
		}
        public static FaqStatus findById(Integer value) {
        	FaqStatus faq_status = null;
            
            if(value != null) {
                switch (value.intValue()) {
                    case 0: faq_status = FaqStatus.BELUM_TERJAWAB;break;
                    case 1: faq_status = FaqStatus.TERJAWAB;break;
                    case 2: faq_status = FaqStatus.SUDAH_DIJAWAB;break;
                }
            }
            return faq_status;
        }

	}

	/**
	 * ID FAQ. Auto-generated ID dengan menggunakan custom generator.
	 */
	@Id(sequence="seq_faq", function="nextsequence")
	public Long faq_id;
	/**
	 * Pertanyaan FAQ.
	 */
	@Required
	@MinSize(10) // mungkin bisa lebih tinggi nilai minimumnya
	public String faq_pertanyaan;
	/**
	 * Jawaban FAQ.
	 */
	public String faq_jawaban;
	/**
	 * Nama pengirim FAQ.
	 */
	@Required
	public String faq_nama;
	/**
	 * E-mail pengirim FAQ.
	 */
	@Required
	@Email
	public String faq_email;
	/**
	 * Status FAQ, terjawab atau belum terjawab.<br />
	 * TODO: Maybe there are more faq status
	 * status
	 */
	@Required
	public FaqStatus faq_status;
	/**
	 * Tanggal pengiriman pertanyaan
	 */
	@As(binder=DateBinder.class)
	public Date faq_tglkirim;
	/**
	 * Tanggal pertanyaan dijawab.
	 */
	@As(binder=DateBinder.class)
	public Date faq_tgljawab;
	/**
	 * Pegawai yang menjawab FAQ.
	 */
	public Long peg_id;

	/**
	 * Fungsi {@code setId} digunakan untuk set nilai ID dari nilai terenkripsi untuk objek FAQ ini.
	 *
	 * @param id id ter-enkripsi

	@Override
	public void setId(String id) {
		this.faq_id = decryptId(id);
	}

	/**
	 * Fungsi {@code getId} digunakan untuk mendapatkan id ter-enkripsi objek FAQ ini.
	 *
	 * @return string id ter-enkripsi

	@Override
	public String getId() {
		return encryptId(this.faq_id);
	}*/
	
	/**
	 * Fungsi {@code hapus} digunakan untuk menghapus entri objek {@code Faq}
	 * sesuai dengan parameter id yang diberikan.
	 *
	 * @param faq_id id FAQ
	 */
	public static void hapus(Long faq_id) {
		Faq faq = findById(faq_id);
		faq.delete(); // hapus objek FAQ
	}

	/**
	 * Fungsi {@code findFaqByPegawaiAndID} digunakan untuk mendapatkan objek
	 * {@code Faq} sesuai dengan ID dan pegawai yang membuatnya. Hal ini untuk
	 * membatasi hak akses terhadap data Faq.
	 *
	 * @param id     id FAQ
	 * @param userid username pengguna didapat dari session
	 * @return objek FAQ yang bersesuaian dengan ID dan pegawai yang membuatnya
	 */
	public static Faq findFaqByPegawaiAndID(Long id, String userid) {
		Pegawai pegawai = Pegawai.findBy(userid);
		return find("faq_id = ? AND peg_id = ?", id, pegawai.peg_id).first();
	}
	
	/**
	 * Fungsi {@code findFaqByPegawaiAndID} digunakan untuk mendapatkan objek
	 * {@code Faq} sesuai dengan ID dan pegawai yang membuatnya. Hal ini untuk
	 * membatasi hak akses terhadap data Faq.
	 *
	 * @param id     id FAQ
	 * @return objek FAQ yang bersesuaian dengan ID dan pegawai yang membuatnya
	 */
	public static Faq findFaqByID(Long id) {
		Faq faq =find("faq_id = ? ", id).first();
		return faq;
	}
	
	/*public static String tableFaq(final Integer status)
	{
		String[] column = new String[]{"faq_id", "faq_pertanyaan", "faq_jawaban"};
		String sql ="SELECT faq_id, faq_pertanyaan, faq_jawaban FROM faq";
		String sql_count ="SELECT COUNT(faq_id) FROM faq";
		if(status != null) {
			sql += " WHERE faq_status="+status;
			sql_count += " WHERE faq_status="+status;
		}
		sql += " ORDER BY faq_id desc";
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetFaq);	
	}
    
    public static String tableFaqAdmin()
	{
    	String[] column = new String[]{"faq_id", "faq_pertanyaan", "faq_tglkirim", "faq_status"};
    	String sql ="SELECT faq_id, faq_pertanyaan, faq_tglkirim, faq_status FROM faq";
    	String sql_count ="SELECT COUNT(faq_id) FROM faq";	
    	return HtmlUtil.datatableResponse(sql, sql_count, column,resultsetFaqAdmin);
	}
    */
    
    public static final ResultSetHandler<String[]> resultsetFaq = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("faq_id");
			tmp[1] = rs.getString("faq_pertanyaan");
			tmp[2] = rs.getString("faq_jawaban");
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetFaqAdmin = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("faq_id");
			tmp[1] = rs.getString("faq_pertanyaan");
			tmp[2] = FormatUtils.formatDateInd(rs.getTimestamp("faq_tglkirim"));
			tmp[3] = rs.getString("faq_status");
			return tmp;
		}
	};
	
	public boolean isSudahDijawab(){
		return faq_status.equals(FaqStatus.SUDAH_DIJAWAB);
	}
}
