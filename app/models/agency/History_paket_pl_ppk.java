package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.Date;
import java.util.List;

@Table(name="EKONTRAK.HISTORY_PAKET_PPK")
public class History_paket_pl_ppk extends BaseModel {

    @Id
    public Long pkt_id;

    @Id
    public Long ppk_id;

    public Long peg_id;

    public Date tgl_perubahan;

    public String alasan;


    public static List<History_paket_pl_ppk> findByPaket(Long pkt_id){
        return History_paket_pl_ppk.find("pkt_id = ? order by tgl_perubahan desc",pkt_id).fetch();
    }

    public String getNamaPpk(){
        return Query.find("SELECT peg_nama FROM pegawai p, ppk WHERE p.peg_id=ppk.peg_id AND ppk_id=?",String.class, ppk_id).first();
    }

    public String getNamaPegawai(){
        return Query.find("SELECT peg_nama FROM pegawai WHERE peg_id=?",String.class, peg_id).first();
    }
}
