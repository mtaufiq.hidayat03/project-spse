package models.agency.contracts;

import models.AnggaranSwakelola;
import models.agency.PaketPpkSwakelola;
import models.agency.Paket_swakelola_lokasi;
import models.common.Active_user;
import utils.LogUtil;

import java.util.List;

/**
 * Model {@code PaketSwakelolaContract} .
 *
 * @author wahid
 */
public interface PaketSwakelolaContract {

    String TAG = "PaketSwakelolaContract";

    Long getPktId();
    List<Paket_swakelola_lokasi> getPaketLocations();
    List<AnggaranSwakelola> getAnggarans();
    PaketPpkSwakelola getPaketPpk();
//    Swakelola_seleksi getSwaSeleksi();

    void setAnggarans(List<AnggaranSwakelola> items);
    void setPaketLocations(List<Paket_swakelola_lokasi> items);
    void setPaketPpk(PaketPpkSwakelola model);
//    void setSwaSeleksi(Swakelola_seleksi model);

    /**
     * create paket ppk if the creator of this paket is actual PPK*/
    default void createPaketPpk(Active_user user) {
        LogUtil.debug(TAG, "create paket PPK");
        if (getPktId() != null && user.isPpk()) {
            PaketPpkSwakelola paketPpk = new PaketPpkSwakelola(getPktId(), user);
            LogUtil.debug(TAG, paketPpk);
            paketPpk.save();
            setPaketPpk(paketPpk);
        }
    }

    default void withAnggarans() {
        if (getPktId() != null) {
            setAnggarans(AnggaranSwakelola.findByPaket(getPktId()));
        }
    }

//    default void withLocations() {
//        if (getPktId() != null) {
//            setPaketLocations(Paket_swakelola_lokasi.find("swk_id=?", getPktId()).fetch());
//        }
//    }

    default void withLocations() {
        if (getPktId() != null) {
            setPaketLocations(Paket_swakelola_lokasi.getNewestByPktId(getPktId()));
        }
    }

    default void withPaketPpk() {
        if (getPktId() != null) {
            setPaketPpk(PaketPpkSwakelola.find("swk_id =?", getPktId()).first());
        }
    }
}
