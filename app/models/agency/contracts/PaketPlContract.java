package models.agency.contracts;

import models.agency.*;
import models.common.Active_user;
import models.nonlelang.Pl_seleksi;
import utils.LogUtil;

import java.util.List;

/**
 * Model {@code PaketPlContract} .
 *
 * @author wahid
 */
public interface PaketPlContract {

    String TAG = "PaketPlContract";

    Long getPktId();
    Long getUkpbjId();
    List<Paket_pl_lokasi> getPaketLocations();
    List<Anggaran> getAnggarans();
    PaketPpkPl getPaketPpk();
    Paket_pp getPaketPp();
    Ukpbj getUkpbj();
    Pl_seleksi getPlSeleksi();

    void setAnggarans(List<Anggaran> items);
    void setPaketLocations(List<Paket_pl_lokasi> items);
    void setPaketPpk(PaketPpkPl model);
    void setPaketPp(Paket_pp model);
    void setUkpbj(Ukpbj model);
    void setPlSeleksi(Pl_seleksi model);

    default void withAnggarans() {
        if (getPktId() != null) {
            setAnggarans(Anggaran.findByPaketPl(getPktId()));
        }
    }

    default void withLocations() {
        if (getPktId() != null) {
            setPaketLocations(Paket_pl_lokasi.getNewestByPktId(getPktId()));
        }
    }

    default void withPaketPpk() {
        if (getPktId() != null) {
            setPaketPpk(PaketPpkPl.find("pkt_id =?", getPktId()).first());
        }
    }

    default void withPaketPp() {
        if (getPktId() != null) {
            setPaketPp(Paket_pp.find("pkt_id =?", getPktId()).first());
        }
    }

    /**
     * create paket ppk if the creator of this paket is actual PPK*/
    default void createPaketPpk(Active_user user) {
        LogUtil.debug(TAG, "create paket PPK");
        if (getPktId() != null && user.isPpk()) {
            PaketPpkPl paketPpkpl = new PaketPpkPl(getPktId(), user);
            LogUtil.debug(TAG, paketPpkpl);
            paketPpkpl.save();
            setPaketPpk(paketPpkpl);
        }
    }
    /**
     * check is this paket actually my paket and i am PPK*/
    default boolean isThisMyPaketAsPpk(Active_user user) {
        return user != null && user.isPpk() && getPaketPpk() != null && getPaketPpk().isItMyPaket(user.ppkId);
    }
}
