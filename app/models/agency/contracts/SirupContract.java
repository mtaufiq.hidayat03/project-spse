package models.agency.contracts;

import play.mvc.Router;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 8/15/2018
 */
public interface SirupContract {

    Long getId();
    String getNama();
    String getSumberDanaString();

    default String getDeleteUrl(Long pkt_id, String deleteAction) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", pkt_id);
        params.put("rupId", getId());
        return Router.reverse(deleteAction, params).url;
    }

}
