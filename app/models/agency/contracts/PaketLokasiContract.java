package models.agency.contracts;

import models.sso.common.Kabupaten;

/**
 * @author HanusaCloud on 8/20/2018
 */
public interface PaketLokasiContract {

    boolean isPklIdExist();
    Long getPrpId();
    Kabupaten getKabupaten();

}
