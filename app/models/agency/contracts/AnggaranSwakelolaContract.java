package models.agency.contracts;

import ext.FormatUtils;


/**
 * @author awahid on 2/7/2019
 */
public interface AnggaranSwakelolaContract {

    String getKodeRekening();
    Double getAngNilai();
    Integer getAngTahun();
    String getSbdLabel();
    String getPpkNama();
    Long getId();
    boolean isPpkExist();

    default String getAngNilaiFormatted() {
        return getAngNilai() != null ? FormatUtils.formatCurrencyRupiah(getAngNilai()) : "Rp. 0";
    }
}
