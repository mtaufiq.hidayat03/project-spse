package models.agency.contracts;


import groovy.lang.Closure;
import models.AnggaranSwakelola;
import models.agency.Ppk;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author awahid on 2/7/2019
 * class ini ada untuk menjamin compatibility object antara paket_anggaran_swakelola
 * untuk digunakan pada {@link ext.PaketTag#_anggaranTable(Map, Closure, PrintWriter, GroovyTemplate.ExecutableTemplate, int)}
 */
public interface PaketAnggaranSwakelolaContract {

    Long getAngId();
    Long getPpkId();
    Long getPktId();
    AnggaranSwakelolaContract getAnggaranContract();
    void setAnggaran(AnggaranSwakelola anggaran);
    Ppk getPpk();
    boolean isPpkExist();
    String getPpkNama();

    default String getUpdatePpkUrl(String action) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", getPktId());
        if (action.contains("PaketSwakelolaCtr")) {
            params.put("ang_id", getAngId());
        } else {
            params.put("angId", getAngId());
        }
        return Router.reverse(action, params).url;
    }

    default String optionLabel() {
        if (isPpkExist()) {
            return Messages.get("menu.edit");
        }
        return Messages.get("menu.pilih");
    }
}
