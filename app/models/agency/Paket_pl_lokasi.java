package models.agency;

import models.agency.contracts.PaketLokasiContract;
import models.jcommon.db.base.BaseModel;
import models.sso.common.Kabupaten;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;

@Table(name="EKONTRAK.PAKET_LOKASI")
public class Paket_pl_lokasi extends BaseModel implements PaketLokasiContract {
	/**
	 * Id tabel
	 */
	@Id(sequence="ekontrak.seq_paket_lokasi", function="nextsequence")
	public Long pkl_id;
	/**
	 * Relasi dengan tabel {@code paket}
	 */
	public Long pkt_id;
	
	@Required
	public Long kbp_id;
	/**
	 * Detil lokasi pelelangan
	 */
	@Required
	public String pkt_lokasi;

	// versi paket lokasi. disamakan dengan versi lelang
	public Integer pkt_lokasi_versi;

	@Transient
	private Paket_pl paket;
	@Transient
	private Kabupaten kabupaten;

	public static Integer getLatestVersion(Long pkt_id) {
		Paket_pl_lokasi paket_lokasi = Paket_pl_lokasi.find("pkt_id=? order by pkt_lokasi_versi desc", pkt_id).first();
		return paket_lokasi==null || paket_lokasi.pkt_lokasi_versi == null ? 1 : paket_lokasi.pkt_lokasi_versi;
	}
	public static List<Paket_pl_lokasi> getNewestByPktId(Long pkt_id) {
		Integer latestVersion = getLatestVersion(pkt_id);
		return find("pkt_id=? and pkt_lokasi_versi=?", pkt_id, latestVersion).fetch();
	}

	public Paket_pl getPaket() {
		if(paket == null)
			paket = Paket_pl.findById(pkt_id);
		return paket;
	}

	@Override
	public boolean isPklIdExist() {
		return pkl_id != null;
	}

	@Override
	public Long getPrpId() {
		return getKabupaten() != null ? getKabupaten().prp_id : null;
	}

	@Override
	public Kabupaten getKabupaten() {
		if(kabupaten == null)
			kabupaten = Kabupaten.findById(kbp_id);
		return kabupaten;
	}

}
