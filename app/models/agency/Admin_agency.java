package models.agency;

import models.jcommon.db.base.BaseModel;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;


/**Ini untuk mapping siapa admin di suatu agency
 * 
 * @author andik
 */

@Table(name="ADMIN_AGENCY")
public class Admin_agency extends BaseModel {
	
	@Id
	public Long agc_id;
	
	@Id
	public Long peg_id;
	
	@Required
	public String ada_nomor_sk;
	
	@Required
	public Date ada_valid_start;
	
	public Date ada_valid_end;
	
	public Long ada_id_attachment;
	
	@Transient
	private Agency agency;
	@Transient
	private Pegawai pegawai;

	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}

	public Agency getAgency() {
		if(agency == null)
			agency = Agency.findById(agc_id);
		return agency;
	}

	public static void nonActiveAdmAgc(List<Admin_agency> adminAgencies) {
		if(adminAgencies != null && !adminAgencies.isEmpty()) {
			for(Admin_agency aa: adminAgencies) {
				List<Agency> agencies = Agency.find("sub_agc_id = ?", aa.agc_id).fetch();
				for(Agency a: agencies) {
					nonActiveAgc(a);
				}
			}
		}
	}

	private static void nonActiveAgc(Agency agency) {
		if(agency != null) {
			Admin_agency admin_agency = Admin_agency.find("agc_id = ?", agency.agc_id).first();
			Pegawai pegawai = Pegawai.find("peg_id = ?", admin_agency.peg_id).first();
			if(pegawai != null) {
				Logger.debug("[PegawaiCtr] nonActiveAgc - Ganti status admin sub agency ke %s. peg_id: %s, agc_id: %s",
						0, pegawai.peg_id, agency.agc_id);
				pegawai.peg_isactive = 0;
				pegawai.save();
			}
			Agency agcChld = Agency.find("sub_agc_id = ?", agency.agc_id).first();
			nonActiveAgc(agcChld);
		}
	}
}
