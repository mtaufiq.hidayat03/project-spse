package models.agency;

import ext.DateBinder;
import ext.FormatUtils;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 5/18/2018
 */
@Table(name = "ukpbj")
public class Ukpbj extends BaseModel {

    @Id(sequence = "seq_ukpbj", function = "nextsequence")
    public Long ukpbj_id;
    @Required
    public Long agc_id;
    @Required
    public Long kpl_unit_pemilihan_id;
    @Required
    public String nama;
    @Required
    public String alamat;
    @Required
    @Phone
    public String telepon;

    @Phone
    public String fax;

    @Required
    @As(binder= DateBinder.class)
    public Date tgl_daftar;

    @Transient
    private Pegawai ketua;

    public static final ResultSetHandler<String[]> resultset = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            tmp[0] = rs.getString("ukpbj_id");
            tmp[1] = rs.getString("nama");
            tmp[2] = rs.getString("alamat");
            tmp[3] = rs.getString("telepon");
            tmp[4] = rs.getString("fax");
            tmp[5] = FormatUtils.formatDateInd(rs.getDate("tgl_daftar"));
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPaket = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[5];
            tmp[0] = rs.getString("pkt_nama");
            tmp[1] = Paket.StatusPaket.fromValue(rs.getInt("pkt_status")).label;
            tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_pagu"));
            tmp[3] = rs.getString("stk_nama");
            tmp[4] = rs.getString("pnt_nama");
            return tmp;
        }
    };

    public static final ResultSetHandler<String[]> resultsetPilihUkpbj = new ResultSetHandler<String[]>() {

        @Override
        public String[] handle(ResultSet rs) throws SQLException {
            String[] tmp = new String[6];
            tmp[0] = rs.getString("nama");
            tmp[1] = rs.getString("alamat");
            tmp[2] = rs.getString("peg_nama");
            tmp[3] = rs.getString("ukpbj_id");
            tmp[4] = rs.getString("kpl_unit_pemilihan_id");
            tmp[5] = rs.getString("ukpbj_id");

            return tmp;
        }
    };

    @Transient
    public List<Pegawai_ukpbj> getPegawaiList() {
        return Pegawai_ukpbj.find("ukpbj_id=?", ukpbj_id).fetch();
    }

    public static List<Ukpbj> findAllUkpbjByKepala(Long pegawaiId) {
        return find("ukpbj_id in (select ukpbj_id from ukpbj where kpl_unit_pemilihan_id=?)", pegawaiId).fetch();
    }

    public static Ukpbj findByName(String name){
        return find("nama = ?", name).first();
    }

}
