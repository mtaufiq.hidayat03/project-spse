package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

/**
 * @author HanusaCloud on 5/2/2018
 */
@Table(name = "paket_ppk", schema = "public")
public class PaketPpk extends BaseModel {

    @Id
    public Long pkt_id;
    @Id
    public Long ppk_id;
    
    public Integer ppk_jabatan;

    public PaketPpk(Long pktId, Long ppkid){
        this.pkt_id = pktId;
        this.ppk_id = ppkid;
        this.ppk_jabatan = 0;
    }

    public boolean isItMyPaket(Long ppkId) {
        return ppk_id.equals(ppkId);
    }

    public static PaketPpk findByPaketAndPpk(Long pkt_id, Long ppk_id){
        return PaketPpk.find("pkt_id = ? and ppk_id = ?",pkt_id,ppk_id).first();
    }

    public static List<PaketPpk> findByPaket(Long pkt_id){
        return PaketPpk.find("pkt_id = ? order by auditupdate desc",pkt_id).fetch();
    }

    public String getNamaPpk(){
        return Query.find("SELECT peg_nama FROM pegawai p, ppk WHERE p.peg_id=ppk.peg_id AND ppk_id=?",String.class, ppk_id).first();
    }
	 
    public void deleteOldPpk(){
        PaketPpk.delete("pkt_id = ? and ppk_id <> ?",pkt_id,ppk_id);
    }
    
    public void deleteOldPpkNew(Long old_ppk_id){
        PaketPpk.delete("pkt_id = ? and ppk_id = ?",pkt_id,old_ppk_id);
    }

}
