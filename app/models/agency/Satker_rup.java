package models.agency;

import controllers.BasicCtr;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.Query;
import play.libs.WS;

import java.util.Date;


/**
 * model satuan kerja yang ada di Rencana Pengadaan (RUP)
 * ini akan disimpan kedalam table satuan_kerja 
 * Created by arief ardiyansah
 */
public class Satker_rup {
	
    public String id;

    public String idSatker;

    public String idKldi;

    public String alamat;

    
    public boolean aktif;
  
    
    public String fax;

    
    public String kodepos;

    public String nama;

    public String telepon;

    public Date auditupdate;

    public String tahunAktif;
    
    public static void simpan(Satker_rup[] list) {
		if(ArrayUtils.isEmpty(list))
			return;		
    	final String sql = "SELECT stk_id FROM satuan_kerja WHERE agc_id is null and rup_stk_id = ?";
		Satuan_kerja satker = null;
		Long satkerId = null;
		for (Satker_rup satker_rup : list) {
			satkerId = Query.find(sql, Long.class, satker_rup.id).first();
			if (satkerId == null)
				satker = new Satuan_kerja();
			else
				satker = Satuan_kerja.findById(satkerId);
			satker.stk_nama = satker_rup.nama;
			satker.stk_alamat = !StringUtils.isEmpty(satker_rup.alamat) ? satker_rup.alamat : satker.stk_alamat;
			satker.stk_telepon = satker_rup.telepon;
			satker.stk_fax = satker_rup.fax;
			satker.stk_kodepos = satker_rup.kodepos;
			satker.stk_kode = satker_rup.idSatker;
			satker.instansi_id = satker_rup.idKldi;
			satker.rup_stk_id = satker_rup.id;
			satker.rup_stk_tahun = satker_rup.tahunAktif;
			satker.save();
		}
	}

	public static void updateFromSirup(String id, Integer tahun) {
		Logger.debug("get data satker sirup %s tahun %s", id, tahun);
		String url = BasicCtr.SIRUP_URL+"/service/daftarSatkerByKLDI?kldi=" + id+"&tahunAnggaran=" + tahun;
		try {
			String response = WS.url(url).getAsync().get().getString();
			if(!StringUtils.isEmpty(response)) {
				Satker_rup[] list = CommonUtil.fromJson(response, Satker_rup[].class);
				simpan(list);
				Logger.debug("save data satker sirup %s tahun %s", id, tahun);
			}
		}catch (Exception e) {
			Logger.error(e, "Kendala Akses Service Satker SIRUP");
		}
	}
}	
