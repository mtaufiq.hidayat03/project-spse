package models.agency;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import ext.DateBinder;
import ext.FormatUtils;
import models.common.Active_user;
import models.common.Kategori;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.kontrak.AnggotaKSO;
import models.kontrak.Kontrak_kso;
import models.lelang.Dok_penawaran;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import models.rekanan.Landasan_hukum;
import models.rekanan.Rekanan;
import models.rekanan.TipePenyedia;
import org.joda.time.DateTime;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Domain model class untuk mapping ke table sppbj
 * @author arief
 *
 */
@Table(name="SPPBJ")
public class Sppbj extends BaseModel {
	
	static final Template TEMPLATE_SPPBJ = TemplateLoader.load("/kontrak/template/SPPBJ-Barang.html");
	static final Template TEMPLATE_SPPBJ_KONSTRUKSI = TemplateLoader.load("/kontrak/template/SPPBJ-Konstruksi.html");
	static final Template TEMPLATE_SURAT_PERJANJIAN = TemplateLoader.load("/kontrak/template/SuratPerjanjianBarang.html");
	static final Template TEMPLATE_SURAT_PERJANJIAN_KSO_NKSO = TemplateLoader.load("/kontrak/template/surat-perjanjian-barang-kso-nkso.html");//metode tender
	static final Template TEMPLATE_SURAT_PERJANJIAN_KSO_NKSO_SELEKSI = TemplateLoader.load("/kontrak/template/surat-perjanjian-barang-kso-nkso-seleksi.html");//metode seleksi
	
	public static final Integer STATUS_DRAFT = Integer.valueOf(0);
	public static final Integer STATUS_TERKIRIM = Integer.valueOf(1);
	public static final Integer STATUS_DICABUT = Integer.valueOf(2);
	
	@Id(sequence="seq_sppbj", function="nextsequence")	
	public Long sppbj_id;	
	
	@Required
	public String sppbj_no;
	
	@As(binder=DateBinder.class)
	public Date sppbj_tgl_buat;

	@Required
	@As(binder=DateBinder.class)
	public Date sppbj_tgl_kirim;
	
	//relasi ke Ppk
	@Required
	public Long ppk_id;
	
	//relasi ke Rekanan
	@Required
	public Long rkn_id;
	
	//relasi ke Lelang_seleksi
	@Required
	public Long lls_id;
	
	public Long sppbj_attachment;
	public Integer sppbj_status;

	@Required
	public String sppbj_kota;
	public String sppbj_lamp;
	public String sppbj_tembusan;
	
	public String catatan_ppk;
	public String catatan_hargakontrak;
	public Double harga_final = 0d;
	public Double jaminan_pelaksanaan = 0d;

	// untuk detail harga
	public String sppbj_dkh;

	@Required
	public String alamat_satker;

	public boolean sudah_kirim_pengumuman = false;

	@Required
	public String jabatan_ppk_sppbj;
	
	@Transient
	private Lelang_seleksi lelang_seleksi;
	@Transient
	private Ppk ppk;
	@Transient
	private Rekanan rekanan;
	@Transient
	public DaftarKuantitas dkh;

	protected void postLoad() {
		if (!CommonUtil.isEmpty(sppbj_dkh))
			dkh = CommonUtil.fromJson(sppbj_dkh, DaftarKuantitas.class);

	}
	
	public Lelang_seleksi getLelang_seleksi() {
		if(lelang_seleksi == null)
			lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}

	public Ppk getPpk() {
		if(ppk == null)
			ppk = Ppk.findById(ppk_id);
		return ppk;
	}

	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}

	public static Sppbj findByLelang(Long lls_id){
		return find("lls_id=?", lls_id).first();
	}


	@Transient
	public String getStatus()
	{
		Integer status = sppbj_status;
		if(status == Sppbj.STATUS_DRAFT)
			return "Draft";
		else if(status == Sppbj.STATUS_TERKIRIM)
			return "Terkirim";
		else if(status == Sppbj.STATUS_DICABUT)
			return "Dibatalkan";
		else
			return "";		
	}

	@Transient
	public BlobTable getBlob() {
		if(sppbj_attachment == null)
			return null;
		return BlobTableDao.getLastById(sppbj_attachment);
	}
	@Transient
	public List<BlobTable> getDokumen() {
		if(sppbj_attachment == null){
			return null;
		}
		return BlobTableDao.listById(sppbj_attachment);
	}

	public static InputStream cetak(Lelang_seleksi lelang_seleksi, Sppbj sppbj) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("format", new FormatUtils());
		model.put("lelang", lelang_seleksi);
		Active_user userPPK = Active_user.current();
		Ppk ppk = Ppk.findById(userPPK.ppkId);
		model.put("ppk", ppk);
		// TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
		Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM paket_satker WHERE pkt_id=?)", lelang_seleksi.pkt_id).first();
		model.put("satker", satker);
		model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
		model.put("sppbj", sppbj);
		Peserta peserta = Peserta.findBy(lelang_seleksi.lls_id, sppbj.rkn_id);
		model.put("pemenang", peserta);
		Kategori kategori = lelang_seleksi.getKategori();
		model.put("kategori", kategori);
		model.put("isAuction",lelang_seleksi.getReverseAuction() != null);

		if(sppbj.sppbj_tembusan!=null){
			String str = sppbj.sppbj_tembusan;
			List<String> tembusan= Arrays.asList(str.split("\\s*,\\s*"));
			model.put("tembusan", tembusan);
		}
		String terbilangPenawaran = FormatUtils.number2Word(peserta.psr_harga);
		model.put("terbilangPenawaran", terbilangPenawaran);

		//Jasa Konsultansi Badan Usaha Konstruksi
		Double hargaFinal= peserta.getPenetapan().hargaFinal();
		String terbilang = FormatUtils.number2Word(hargaFinal.longValue());
		model.put("terbilang", terbilang);
		Panitia panitia = Panitia.findByLelang(lelang_seleksi.lls_id);
		model.put("panitia", panitia);
		Ukpbj ukpbj= Ukpbj.findById(panitia.ukpbj_id);
		model.put("ukpbj", ukpbj);

		Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA);
		String perihal="";

		if(dok_penawaran.dok_surat!=null) {
			Gson gson = new Gson();
			JsonElement element = gson.fromJson(dok_penawaran.dok_surat, JsonElement.class);
			JsonObject jsonObject = element.getAsJsonObject();
			String[] arr = dok_penawaran.dok_surat.split("\",\"");

			for (String find : arr) {
				if (find.contains("Perihal")) {
					perihal = find.replace("Perihal :", "").replace("Perihal:", "");
				}
			}
		}
		model.put("perihal", perihal);

		SimpleDateFormat sdfr = new SimpleDateFormat("dd/MMM/yyyy");
		String dok_tgl = sdfr.format( dok_penawaran.dok_tgljam );
		model.put("dok_tgl", dok_tgl);

		Paket paket=Paket.findByLelang(lelang_seleksi.lls_id);
		String nomor="";
		Date batasBuildDate= null;
		try {
			batasBuildDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-10");//Rizky, yang build pakai tgl aplikasi tgl tersebut, nomer suratnya kosong, diatasi itu nomer surartnya di isikan
			if(paket.pkt_tgl_build != null){
				Date buildDate = new SimpleDateFormat("yyyyMMdd").parse(paket.pkt_tgl_build.substring(4));
				if (buildDate.after(batasBuildDate))
					nomor = peserta.psr_id.toString();
			}
		} catch (ParseException e) {
			Logger.error("parse error:",e.getMessage());
		}

		model.put("nomor", nomor);

		//PEKERJAAN_KONSTRUKSI (Permen lampiran 2)
        String terbilangJaminan ="";
        if(sppbj.jaminan_pelaksanaan.intValue()>0)
            terbilangJaminan = FormatUtils.number2Word(sppbj.jaminan_pelaksanaan);
        model.put("terbilangJaminan", terbilangJaminan);
        String masaBerlaku = dok_penawaran.dok_waktu.toString();
		model.put("masaBerlaku", masaBerlaku);
        String terbilangMasaBerlaku=FormatUtils.number2Word(dok_penawaran.dok_waktu);
        model.put("terbilangMasaBerlaku", terbilangMasaBerlaku);



		if(kategori.isJkKonstruksi() || kategori.isKonstruksi())
        return HtmlUtil.generatePDF(TEMPLATE_SPPBJ_KONSTRUKSI, model);
		else
		return HtmlUtil.generatePDF(TEMPLATE_SPPBJ, model);
	}

	public static InputStream cetak_perjanjian(Kontrak kontrak) {
		Long lelangId = kontrak.lls_id;
		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lelangId);
		Sppbj sppbj = Sppbj.find("lls_id=?", lelangId).first();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("format", new FormatUtils());
		model.put("lelang", lelang_seleksi);
		Kategori kategori = lelang_seleksi.getKategori();
		model.put("kategori",kategori);
		model.put("sppbj", sppbj);
		
		if(sppbj != null) {
			DateTime tanggal_sppbj = new DateTime(sppbj.sppbj_tgl_kirim.getTime());
			int tglsppbj = tanggal_sppbj.getDayOfMonth();
			String hariSppbj = FormatUtils.day[(tanggal_sppbj.getDayOfWeek()==7 ? 0 : tanggal_sppbj.getDayOfWeek())];
			String bulansppbj = FormatUtils.month[tanggal_sppbj.getMonthOfYear()-1];
			int tahunsppbj = tanggal_sppbj.getYear();
			model.put("tanggal_sppbj", tanggal_sppbj);
			model.put("tglsppbj", tglsppbj);
			model.put("hariSppbj", hariSppbj);
			model.put("bulansppbj", bulansppbj);
			model.put("tahunsppbj", tahunsppbj);
			
		}
		Peserta peserta = Peserta.findBy(lelangId, sppbj.rkn_id);
		model.put("pemenang", peserta);
		model.put("kontrak", kontrak);
		//23 maret 2020, ppk diambil dari sppbj
		//Active_user userPPK = Active_user.current();
		//Ppk ppk = Ppk.findById(userPPK.ppkId);
		Ppk ppk = sppbj.getPpk();
		model.put("ppk", ppk);
		// TODO : penentuan satker , sementara solved jika paket single, namun jika paket konsolidasi perlu dibuat manual saja inputan alamat satker
		Satuan_kerja satker = Satuan_kerja.find("stk_id IN (SELECT stk_id FROM paket_satker WHERE pkt_id=?)", lelang_seleksi.pkt_id).first();
//		model.put("satker", satker);
		model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
		model.put("kontrakJenis", lelang_seleksi.getKontrakPembayaran().label);
		if(kontrak!=null){
			DateTime tanggal_kontrak = new DateTime(kontrak.kontrak_tanggal.getTime());
			String hari = FormatUtils.day[(tanggal_kontrak.getDayOfWeek()==7 ? 0 : tanggal_kontrak.getDayOfWeek())];
			int tgl = tanggal_kontrak.getDayOfMonth();
			String bulan = FormatUtils.month[tanggal_kontrak.getMonthOfYear()-1];
			int tahun = tanggal_kontrak.getYear();
			model.put("hari", hari);
			model.put("tgl", tgl);
			model.put("bulan", bulan);
			model.put("tahun", tahun);
			model.put("nilai_kontrak_nominal",FormatUtils.formatDesimal(kontrak.kontrak_nilai));
			String nilaiKontrak = FormatUtils.number2Word(kontrak.kontrak_nilai.longValue());
			model.put("nilai_kontrak",nilaiKontrak);
			List<Landasan_hukum> list = Landasan_hukum.findBy(sppbj.rkn_id);
			Landasan_hukum akta = null;
			if (list.size()!=0) {
				akta = list.get(0);
			}

			model.put("akta", akta);
			if (kontrak.kontrak_kso!=null){
				Kontrak_kso kso = kontrak.getKontrakKSO();
				model.put("kso", kso);
				String[] anggota = kso.anggota_kso.split(",");
				List<String> strings = new ArrayList<>();
				strings.addAll(Arrays.asList(anggota));
				List<AnggotaKSO> lst = kontrak.getListAnggotaKSO();
				strings.addAll(lst.stream().map(o -> o.nama_anggota).collect(Collectors.toList()));
				model.put("anggota", strings);
			}
		}
		//jika penyedia perorangan dan bukan pengadaan konstruksi, atau jasa konsultansi konstruksi, pake sk d1
		// karena di PermenPUPR07-2019 tidak ada sampel penyedia perorangan (maka pake SK D.1)
		if (!kontrak.kontrak_tipe_penyedia.equalsIgnoreCase(TipePenyedia.PERUSAHAAN_PERSEORANGAN.label) && !(kategori.isKonstruksi() || kategori.isJkKonstruksi())){
			return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN, model);
		}else{
			//selain itu pake sk kemenpu
			String durasipemeliharaan = FormatUtils.number2Word(kontrak.lama_durasi_pemeliharaan.longValue());
			model.put("durasipemeliharaan", durasipemeliharaan);
			String durasipenyerahan1 = FormatUtils.number2Word(kontrak.lama_durasi_penyerahan1.longValue());
			model.put("durasipenyerahan1", durasipenyerahan1);
			if(kategori.isJkKonstruksi()){ //lampiran 1
				return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_KSO_NKSO_SELEKSI, model);
			}//lampiran 2
			return HtmlUtil.generatePDF(TEMPLATE_SURAT_PERJANJIAN_KSO_NKSO, model);//tender
		}
	}
	
	/**
	 * simpan Rincian HPS
	 * @param data
     * @throws Exception
     */
	public void simpanHps(String data) throws Exception{
		DaftarKuantitas dk = DaftarKuantitas.populateDaftarKuantitas(data,true);
		sppbj_dkh = CommonUtil.toJson(dk);
		save();
	}

	public static int getJumlahSppbjByLelangAndPpk(Long lelangId, Long ppkId) {
		return (int)Sppbj.count("lls_id=? AND ppk_id=?", lelangId, ppkId);
	}

	public boolean isNoteEmpty() {
		return CommonUtil.isEmpty(catatan_ppk);
	}

	public UploadInfo simpanDok(File file) throws Exception {
		BlobTable blob = null;
		if (sppbj_attachment == null) {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
		} else if (sppbj_attachment != null)
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, sppbj_attachment);
		sppbj_attachment = blob.blb_id_content;
		save();
		return UploadInfo.findBy(blob);
	}


}
