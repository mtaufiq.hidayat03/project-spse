package models.agency;

import ext.DateBinder;
import ext.FormatUtils;
import models.agency.Paket.StatusPaket;
import models.common.Jenis_agency;
import models.form.FormSelectAgency;
import models.jcommon.db.base.BaseModel;
import models.sso.common.Kabupaten;
import org.sql2o.ResultSetHandler;
import play.data.binding.As;
import play.data.validation.MaxSize;
import play.data.validation.Phone;
import play.data.validation.Required;
import play.data.validation.URL;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Model {@code Agency} merepresentasikan tabel agency pada database. Kelas ini
 * mengimplementasikan <i>interface</i> {@code IEncryptedId} yang digunakan
 * untuk enkripsi data ID agency.
 *
 * @author I Wayan Wiprayoga W
 */

@Table(name="AGENCY")
public class Agency extends BaseModel {

	/**
	 * ID Agency
	 */
	@Id(sequence="seq_agency", function="nextsequence")
	public Long agc_id;
	/**
	 * Admin Agency yang bertanggung jawab
	 */
  
	/**
	 * Nama agency
	 */
	@Required
	public String agc_nama;

	/**
	 * Alamat agency
	 */
	@Required(message="Alamat wajib diisi")
	@MaxSize(500)
	public String agc_alamat;
	/**
	 * Telepon agency
	 */
	@Phone
	@Required
	public String agc_telepon;
	/**
	 * Fax agency
	 */
	@Phone
	public String agc_fax;
	/**
	 * Alamat website agency
	 */
	@URL
	public String agc_website;
	/**
	 * Tanggal pendaftaran agency
	 */
	@Required
	@As(binder=DateBinder.class)
	public Date agc_tgl_daftar;
	
	// relasi Jenis_agency
	@Required
	public Long jna_id;
	/**
	 * Kabupaten tempan agency berada
	 */
    @Required(message="Kabupaten/Kota wajib diisi")
	public Long kbp_id;
    
    @Required (message="Provinsi wajib diisi")
    @Transient
    public Long propinsiId;

    @Transient
    public Long pjId;
    
    @Transient
    public String skPjId;
    
	/**
	 * Induk agency jika objek ini merupakan sub-agency
	 */
	public Long sub_agc_id;
	/**
	 * Apakah agency merupakan LPSE
	 */
	public int is_lpse;
	
	@Transient
	public boolean isLpse;
	
	@Transient
	private Kabupaten kabupaten;
	@Transient
	private Jenis_agency jenis_agency;
	@Transient
	private Agency sub_agency;
	
	public Agency getSub_agency() {
		if(sub_agc_id != null)
			sub_agency = findById(sub_agc_id);
		return sub_agency;
	}
	
	public Agency getSub_agencyOri() {
		if(sub_agc_id == null)
			sub_agency = findById(sub_agc_id);
		return sub_agency;
	}
	
	public Kabupaten getKabupaten() {
		if(kabupaten == null)
			kabupaten = Kabupaten.findById(kbp_id);
		return kabupaten;
	}
	
	public Jenis_agency getJenis_agency() {
		if(jenis_agency == null)
			jenis_agency = Jenis_agency.findById(jna_id);
		return jenis_agency;
	}

	/**
	 * Set ID from encrypted ID
	 * @param id encrypted ID

	@Override
	public void setId(String id) {
		agc_id = (decryptId(id));
	}*/

	/**
	 * Get encrypted ID from current ID

	@Override
	public String getId() {
		return encryptId(agc_id);
	}*/
	
	/**
	 * Fungsi {@code findAgencyByPegawai} digunakan untuk mendapatkan sebuah
	 * objek {@code Agency} berdasarkan pegawai yang menjadi penganggung jawab
	 * agency
	 *
	 * @param pegawaiId objek {@code Pegawai}
	 * @return objek {@code Agency} yang bersesuaian dengan penganggung jawab
	 *         agency
	 */
	public static Agency findAgencyByPegawai(Long pegawaiId) {
		return find("agc_id in (select agc_id from admin_agency where peg_id=?)", pegawaiId).first();
	}
	
	public static List<Agency> findAllAgencyByPegawai(Long pegawaiId) {
		return find("agc_id in (select agc_id from admin_agency where peg_id=?)", pegawaiId).fetch();
	}


	/**
	 * Fungsi {@code findAgencyByPenanggungJawab} digunakan untuk mendapatkan
	 * sebuah objek {@link Agency} berdasarkan informasi username penanggung jawab.
	 *
	 * @param username username penanggung jawab
	 * @return objek {@link Agency} yang bersesuaian dengan penanggung jawab
	 * 		   agency
	 */
	public static Agency findAgencyByPenanggungJawab(String username) {
		Pegawai pegawai = Pegawai.findBy(username);
		return findAgencyByPegawai(pegawai.peg_id);
	}
	
	public static void simpanAgency(Agency agency, List <FormSelectAgency> agencies) throws Exception {
		FormSelectAgency admin = agencies.get(0);
		if(admin.adminId == null || admin.noSk == null){
			throw new NullPointerException();
		}

		agency.save();
		StringBuilder param = new StringBuilder();
		for (FormSelectAgency a : agencies) {
			if(a != null) {
				Admin_agency obj = Admin_agency.find("peg_id=? and agc_id=?", a.adminId, agency.agc_id).first();
				if (obj == null) {
					obj = new Admin_agency();
					obj.agc_id = agency.agc_id;
					obj.peg_id = a.adminId;
					obj.ada_nomor_sk = a.noSk;
					obj.save();
	
				} else {
					obj.agc_id = agency.agc_id;
					obj.peg_id = a.adminId;
					obj.ada_nomor_sk = a.noSk;
					obj.save();
				}
				if (param.length() > 0)
					param.append(',');
				param.append(a.adminId);
			}
		}
		if (param.length() > 0) {
			Admin_agency.delete("agc_id=? aND peg_id NOT IN (" + param + ')', agency.agc_id);
		}
	}
	
	 /**
	  * table Agency data dalam format Json untuk keperluan Datatable
	  * @param agc_id
	  * @return
	  */
	/*public static String tableAgency(final Long agc_id)
	{
		String[] column = new String[]{"agc_id", "agc_nama", "agc_alamat", "agc_telepon", "is_lpse"};
		String sql = "SELECT agc_id, agc_nama, agc_alamat, agc_telepon, is_lpse FROM agency";
		String sql_count = "SELECT COUNT(agc_id) FROM agency";
		if(agc_id != null) {
			sql += " WHERE sub_agc_id="+agc_id;
			sql_count += " WHERE sub_agc_id="+agc_id;
		}			
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetAgency);
	}
	
	public static String tablePaketAgency(final Long agc_id, final Integer tahun)
	{
		String[] column = new String[]{"pkt_nama", "pkt_status", "pkt_pagu", "stk_nama"};
		String sql = "SELECT pkt_nama, pkt_status, pkt_pagu, s.stk_nama, pp.ppk_id, pe.peg_id FROM paket p , satuan_kerja s , ppk pp , pegawai pe "+
	    			"where p.ppk_id =pp.ppk_id and pp.peg_id = pe.peg_id and p.stk_id=s.stk_id AND EXTRACT(YEAR FROM pkt_tgl_buat) = '"+tahun+"'";
		 String sql_count = "SELECT COUNT(pkt_id) FROM paket p , satuan_kerja s , ppk pp , pegawai pe where p.ppk_id =pp.ppk_id and pp.peg_id = pe.peg_id "+  
	    			"and p.stk_id=s.stk_id AND EXTRACT(YEAR FROM pkt_tgl_buat) = '"+tahun+"'";
		if(agc_id != null) {
			sql += " AND pe.agc_id="+agc_id;
			sql_count += " AND pe.agc_id="+agc_id;
		}			
		return HtmlUtil.datatableResponse(sql, sql_count, column, resultsetPaketAgency);
	}
	*/
	public static final ResultSetHandler<String[]> resultsetAgency = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[6];
			tmp[0] = rs.getString("agc_id");
			tmp[1] = rs.getString("agc_nama");
			tmp[2] = rs.getString("agc_alamat");
			tmp[3] = rs.getString("agc_telepon");
			tmp[4] = rs.getString("audituser");
			tmp[5] = rs.getString("is_lpse");
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetPaketAgency = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("pkt_nama");
			tmp[1] = StatusPaket.fromValue(rs.getInt("pkt_status")).label;
			tmp[2] = FormatUtils.formatCurrencyRupiah(rs.getDouble("pkt_pagu"));
			tmp[3] = rs.getString("stk_nama");
			return tmp;
		}
	};
}
	

