package models.agency;

import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Model {@code Anggota_panitia} merepresentasikan tabel anggota_panitia pada
 * database
 *
 * @author I Wayan Wiprayoga W
 */
@Table(name="ANGGOTA_PANITIA")
public class Anggota_panitia extends BaseModel {

	/**
	 * ID kepanitiaan sebagai referensi ke tabel panitia
	 */
	@Id
	public Long pnt_id;
	/**
	 * NIP pegawai sebagai referensi ke tabel pegawai
	 */
	@Id
	public Long peg_id;
	/**
	 * Jabatan pegawai dalam kepanitiaan
	 */
	public String agp_jabatan;
	@Transient
	private Panitia panitia;
	@Transient
	private Pegawai pegawai;	
	
	public Panitia getPanitia() {
		if(panitia == null)
			panitia = Panitia.findById(pnt_id);
		return panitia;
	}

	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}

	@Transient
	public boolean isKetua() {
		return agp_jabatan != null && agp_jabatan.equals("K");
	}

	/**
	 * Untuk mendapatkan jabatan pegawai dalam kepanitaan
	 *
	 * @return
	 */
	@Transient
	public String getJabatan() {
		if (agp_jabatan.equals("K")) {
			return Messages.get("agency.ketua");
		} else if (agp_jabatan.equals("W")) {
			return Messages.get("agency.wakil");
		} else if (agp_jabatan.equals("S")) {
			return Messages.get("agency.sekretaris");
		} else {
			return Messages.get("agency.anggota");
		}
	}
	
	/**
	 * check jabatan Ketua Panitia
	 * @param panitiaId
	 * @param pegawaiId
	 * @return
	 */
	public static boolean isKetuaPanitia(Long panitiaId, Long pegawaiId) {		
		return count("pnt_id=? AND peg_id=? AND agp_jabatan='K'", panitiaId, pegawaiId) > 0;
	}	
	
	public static boolean isAnggotaPanitia(Long panitiaId, Long pegawaiId) {		
		return count("pnt_id=? AND peg_id=?", panitiaId, pegawaiId) > 0;
	}

	/**
	 * Fungsi {@code createAnggotaPanitia} digunakan untuk membuat entri data anggota panitia di database dengan
	 * menggunakan informasi NIP Pegawai dan ID Panitia.
	 *
	 * @param pegawaiId pegawai
	 * @param pnt_id id panitia
	 * @return true jika berhasil membuat entri di database
	 */
	public static void createAnggotaPanitia(Long pegawaiId, Long pnt_id) {
		Panitia panitia = Panitia.findById(pnt_id); // cari objek kepanitiaan
		Pegawai pegawai = Pegawai.findById(pegawaiId); // cari objek pegawai
		if (panitia != null && pegawai != null) {
			Anggota_panitia anggota_panitia = new Anggota_panitia();
			anggota_panitia.pnt_id = panitia.pnt_id;
			anggota_panitia.peg_id = pegawaiId;
			anggota_panitia.agp_jabatan="A";
			anggota_panitia.save(); // pastikan data bisa disimpan
		} 
	}
	


	/**
	 * Fungsi {@code getDaftarKepanitiaanPegawai} digunakan untuk mendapatkan daftar kepanitiaan seorang pegawai
	 * dengan menggunakan informasi NIP Pegawai.
	 *
	 * @param pegawaiId pegawai
	 * @return daftar kepanitiaan pegawai
	 */
	public static List<Panitia> findKepanitiaanPegawai(Long pegawaiId) {
		return Query.find("select p.* from Anggota_panitia ap, Panitia p where peg_id=? and ap.pnt_id=p.pnt_id", Panitia.class, pegawaiId).fetch();
	}
	
	public static Anggota_panitia findKetuaPanitia(Long panitiaId) {		
		return find("pnt_id=? AND agp_jabatan='K'", panitiaId).first();
	}

	public static final ResultSetHandler<String[]> resultsetAnggotaPanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("peg_id");
			tmp[1] = rs.getString("peg_nama");
			tmp[2] = rs.getString("peg_namauser");
			tmp[3] = rs.getString("peg_nip");
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetCalonAnggotaPanitia = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("peg_nip");
			tmp[1] = rs.getString("peg_nama");
			tmp[2] = rs.getString("peg_namauser");
			tmp[3] = rs.getString("peg_id");
			return tmp;
		}
	};

}
