  package models.agency;

import com.google.gson.reflect.TypeToken;
import ext.DateBinder;
import ext.FormatUtils;
import models.common.Kategori;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.db.base.BaseModel;
import models.jcommon.util.CommonUtil;
import models.kontrak.AnggotaKSO;
import models.kontrak.Kontrak_kso;
import models.kontrak.SskkPpkContent;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import models.rekanan.Rekanan;
import models.sirup.PaketSwakelolaSirup;
import org.joda.time.DateTime;
import play.data.binding.As;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;
import utils.LogUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Domain class untuk Table kontrak
 * @author arief
 *
 */

@Table(name="KONTRAK")
public class Kontrak extends BaseModel  {
	
	static final Template TEMPLATE_SSKK_EXPRESS= TemplateLoader.load("/kontrak/template/sskk_express.html");
	static final Template TEMPLATE_SSKK_BARANG = TemplateLoader.load("/kontrak/template/SSKK-Barang.html");
	
	@Id(sequence="seq_kontrak", function="nextsequence")
	public Long kontrak_id;
	//relasi ke Lelang_seleksi
	@Required
	public Long lls_id;
	//relasi ke Rekanan
	public Long rkn_id;
	
	@Required
	public Long ppk_id;
	// 22 maret 2020, jabatan ppk diisi manual, dan ppk diambil dari sppbj pada saat buat dan simpan
	@Required(message = "Jabatan PPK wajib diisi")
	public String jabatan_ppk_kontrak;
	public String anggota_kso;
	@Transient
	public List<AnggotaKSO> anggotakso;

	@Required(message = "Nomor Surat Perjanjian wajib diisi")
	public String kontrak_no;
	@Required
	public Double kontrak_nilai;
	@Required(message = "No. Rekening Bank wajib diisi")
	public String kontrak_norekening;
	@Required(message = "Nama Bank wajib diisi")
	public String kontrak_namarekening;//BANK
	@Required(message = "Nama Pemilik Rekening wajib diisi")
	public String kontrak_namapemilikrekening;//BANK
	@Required(message = "Tanggal Surat Perjanjian wajib diisi")
	@As(binder=DateBinder.class)
	public Date kontrak_tanggal;	
	
	@As(binder=DateBinder.class)
	public Date kontrak_mulai;	
	
	@As(binder=DateBinder.class)
	public Date kontrak_akhir;

	//dipakai untuk informasi lainnya
	public String kontrak_lingkup;
	
	public Long kontrak_id_attacment;
	
	public Long kontrak_id_attacment2;
	
	public Long kontrak_sskk_attacment;

	@Required(message = "Kota Surat Perjanjian wajib diisi")
	public String kontrak_kota;
	
	public String kontrak_waktu;

	public String kontrak_wakil_penyedia;

	public String kontrak_jabatan_wakil;
	
	public String kontrak_tipe_penyedia;

	@Required(message = "Lingkup Pekerjaan wajib diisi")
	public String kontrak_lingkup_pekerjaan;
		
	

	public String kontrak_kso;
	/**
	 * data form isian sskk
	 * format json
	 * model models.lelang.SskkContent
	 */
	
	public String kontrak_sskk;
	
	/**
	 * data form isian sskk hasil adendum
	 * format json
	 * model models.lelang.SskkContent
	 */
	
	public String kontrak_sskk_perubahan;

	/**
	 * @asepabdulsofyan
	 * penyesuaian pertanggal 2020-09-03
	 * dengan implementasi PermenPUPR07-2019 untuk kontraktor
	 * 20 maret 2020, input dikembalikan ke -> kontrak_no, kontrak_tanggal, kontrak_kota
	 */
	//@Required(message = "no surat perjanjian dibutuhkan")
	//public String no_surat_perjanjian;

	//@Required(message = "tanggal surat perjanjian dibutuhkan")
	//@As(binder=DateBinder.class)
	//public Date tanggal_surat_perjanjian;

	//@Required(message = "kota surat perjanjian dibutuhkan")
	//public String kota_surat_perjanjian;

	public String alasanubah_kontrak_nilai;
	//end PermenPUPR07-2019

	/**
	 * 7 april 2020
	 * untuk permenpu(konstruksi,dan konsultansi konstruksi)
	 * input sk pemenang, dan tgl
	 */
	public String no_skpemenang;
	@As(binder=DateBinder.class)
	public Date tgl_skpemenang;
	public Integer lama_durasi_penyerahan1;
	public Integer lama_durasi_pemeliharaan;
	public String kode_akun_kegiatan;

	@Transient
	public SskkPpkContent getSskkContent() {
		SskkPpkContent sskkPpkContent = CommonUtil.fromJson(kontrak_sskk, SskkPpkContent.class);
		return sskkPpkContent != null ? sskkPpkContent : new SskkPpkContent();
	}
	
	@Transient
	public SskkPpkContent getSskkContentPerubahan() {
		return CommonUtil.fromJson(kontrak_sskk_perubahan, SskkPpkContent.class);
	}
	
	@Transient
	public Kontrak_kso getKontrakKSO() {
		return CommonUtil.fromJson(kontrak_kso, Kontrak_kso.class);
	}

	@Transient
	public List<AnggotaKSO> getListAnggotaKSO() {
		return CommonUtil.fromJson(anggota_kso, new TypeToken<List<models.kontrak.AnggotaKSO>>(){}.getType());
	}

	@Transient
	public BlobTable getBlob() {
		if(kontrak_id_attacment == null)
			return null;
		return BlobTableDao.getLastById(kontrak_id_attacment);
	}
	
	@Transient
	public BlobTable getBlob2() {
		if(kontrak_id_attacment2 == null)
			return null;
		return BlobTableDao.getLastById(kontrak_id_attacment2);
	}
	
	@Transient
	public BlobTable getBlobSskk() {
		if(kontrak_sskk_attacment == null)
			return null;
		return BlobTableDao.getLastById(kontrak_sskk_attacment);
	}
	
	@Transient
	public String getMasaBerlaku() {
		if(kontrak_mulai == null || kontrak_akhir == null )
			return "";
		return FormatUtils.formatDate(kontrak_mulai, kontrak_akhir);
	}
	@Transient
	private Lelang_seleksi lelang_seleksi;
	@Transient
	private Ppk ppk;
	@Transient
	private Rekanan rekanan;	
	
	public Lelang_seleksi getLelang_seleksi() {
		if(lelang_seleksi == null)
			lelang_seleksi = Lelang_seleksi.findById(lls_id);
		return lelang_seleksi;
	}

	public Ppk getPpk() {
		if(ppk == null)
			ppk = Ppk.findById(ppk_id);
		return ppk;
	}

	public Rekanan getRekanan() {
		if(rekanan == null)
			rekanan = Rekanan.findById(rkn_id);
		return rekanan;
	}

	@Transient
	public Kontrak hapusSskk(Kontrak kontrak) {
		kontrak.kontrak_sskk = null;
		kontrak.save();
		return kontrak;
	}

	public static InputStream cetak(Sppbj sppbj) {
		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(sppbj.lls_id);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("format", new FormatUtils());
		model.put("lelang", lelang_seleksi);
		model.put("sppbj", sppbj);
		model.put("rekanan", sppbj.getRekanan());
		Peserta peserta = Peserta.findBy(lelang_seleksi.lls_id, sppbj.rkn_id);
		model.put("pemenang", peserta);
		Kategori kategori = lelang_seleksi.getKategori();
		model.put("kategori",kategori);
		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
		SskkPpkContent sskk_content = kontrak.getSskkContent();
		model.put("sskk_content",sskk_content);
		if (sskk_content.uang_muka != null) {
			Double persen_uangmuka = Math.round(10000.00 * sskk_content.uang_muka / kontrak.kontrak_nilai) / 100.00;
			model.put("persen_uangmuka", FormatUtils.formatDesimal(persen_uangmuka));
		}
		if (sskk_content.inspeksi_tgl != null) {
			DateTime tgl_insp = new DateTime(sskk_content.inspeksi_tgl.getTime());
			String hari = FormatUtils.day[(tgl_insp.getDayOfWeek() == 7 ? 0 : tgl_insp.getDayOfWeek())];
			model.put("hari", hari);
		
		}
		Ppk ppk = Ppk.findById(sppbj.ppk_id);
		model.put("ppk", ppk);
		model.put("pegawai", Pegawai.find("peg_id=?",ppk.peg_id).first());
		
		if (lelang_seleksi.getPemilihan().isLelangExpress()) {
			return HtmlUtil.generatePDF(TEMPLATE_SSKK_EXPRESS, model);
		} else {
			return HtmlUtil.generatePDF(TEMPLATE_SSKK_BARANG, model);
		}		
	}

	public List<BlobTable> getDokSskkAttachment(){
		if(kontrak_sskk_attacment == null){
			return null;

		}
		return BlobTableDao.listById(kontrak_sskk_attacment);
	}

	public UploadInfo simpanSskkAttachment(File file) throws Exception {
		String path = sanitizedFilename(file);
		File newFile = new File(path);
		BlobTable model = findByFilename(newFile.getName(), getDokSskkAttachment());

		if (model != null) {
			LogUtil.debug("Kontrak", "filename's duplicated");
			return null;
		}
		file.renameTo(newFile);
		BlobTable blob = null;
		if (kontrak_sskk_attacment != null) {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, kontrak_sskk_attacment);
		} else {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
		}

		kontrak_sskk_attacment = blob.blb_id_content;

		save();
		return UploadInfo.findBy(blob);
	}

	public String sanitizedFilename(File file) {
		String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
		String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
		if (File.separatorChar == '\\')// windows
			path += '\\' + text;
		else
			path += "//" + text;
		return path;
	}
	public BlobTable findByFilename(String filename,List<BlobTable> blobs) {
		if (CommonUtil.isEmpty(blobs)) {
			LogUtil.debug("Kontrak", "total blob is empty or object null");
			return null;
		}
		for (BlobTable model : blobs) {
			LogUtil.debug("Kontrak", filename + " contains " + model.blb_nama_file);
			if (model.blb_nama_file.equalsIgnoreCase(filename)) {
				return model;
			}
		}
		return null;
	}

	@Transient
	public List<BlobTable> getDokAttachment2() {
		if(kontrak_id_attacment2 == null){
			return null;
		}
		return BlobTableDao.listById(kontrak_id_attacment2);
	}

	public UploadInfo simpanAttachment2(File file) throws Exception {
		String path = sanitizedFilename(file);
		File newFile = new File(path);
		file.renameTo(newFile);
		BlobTable blob = null;
		if (kontrak_id_attacment2 != null) {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile, kontrak_id_attacment2);
		} else {
			blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, newFile);
		}
		kontrak_id_attacment2 = blob.blb_id_content;
		save();

		return UploadInfo.findBy(blob);
	}

	// find progress terbesar
	public Long progressMax() {
		Long progress = Query.find("SELECT MAX(progres_fisik) FROM ba_pembayaran WHERE kontrak_id=?", Long.class, kontrak_id).first();
		if(progress == null)
			progress = 0L;
		return progress;
	}

}
