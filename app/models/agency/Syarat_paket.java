package models.agency;


import models.common.Jenis_ijin;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Collections;
import java.util.List;


@Table(name="SYARAT_PAKET")
public class Syarat_paket extends BaseModel {

	@Id(sequence="seq_syarat_paket", function="nextsequence")
	public Long id_syarat_paket;

	public String srt_nama;

	public String srt_klasifikasi;

	//relasi ke Jenis_ijin
	public String jni_id;
	
	//relasi ke paket
	public Long pkt_id;

	@Transient
	private Paket paket;
	@Transient
	private Jenis_ijin jenis_ijin;
	
	public Paket getPaket() {
		if(paket == null)
			paket = Paket.findById(pkt_id);
		return paket;
	}

	public Jenis_ijin getJenis_ijin() {
		if(jenis_ijin == null)
			jenis_ijin = Jenis_ijin.findById(jni_id);
		return jenis_ijin;
	}

	public static List<Syarat_paket> findByPaket(Long paketId) {
		if(paketId == null)
			return Collections.EMPTY_LIST;
		return find("pkt_id=?", paketId).fetch();
	}
	
}
