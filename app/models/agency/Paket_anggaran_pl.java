package models.agency;

import models.agency.contracts.AnggaranContract;
import models.agency.contracts.PaketAnggaranContract;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.List;


@Table(name="EKONTRAK.PAKET_ANGGARAN")
public class Paket_anggaran_pl extends BaseModel implements PaketAnggaranContract {
	@Id
	public Long pkt_id;

	@Id
	public Long ang_id;

	//versi paket anggaran
	public Integer pkt_ang_versi;

	@Transient
	private Paket_pl paket;
	@Transient
	private Anggaran anggaran;
	@Transient
	private Ppk ppk;

	//relasi ke table PPK
	public Long ppk_id;

	public Integer ppk_jabatan;

	//relasi ke model Rup_paket
	public Long rup_id;

	public static Paket_anggaran_pl getNewInstance() {
		Paket_anggaran_pl paket_anggaran_pl = new Paket_anggaran_pl();
		paket_anggaran_pl.setAnggaran(new Anggaran());
		return paket_anggaran_pl;
	}

	// list ppk_id dari paket
	public static String findPPKByPaket(Long pkt_id) {
		return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct(ppk_id)), ', ') FROM ekontrak.paket_anggaran WHERE pkt_id=?", String.class, pkt_id).first();
	}

	public static List<Paket_anggaran_pl> findByPaket(Long pktId) {
		return find("pkt_id =?", pktId).fetch();
	}

	public static List<Paket_anggaran_pl> findByPaketAndVersion(Long pktId, Integer versi) {
		return find("pkt_id=? and pkt_ang_versi = ?", pktId, versi).fetch();
	}

	public static void updatePpkAllAnggaranByPaket(Long pkt_id, Long ppk_id){
		Query.update("UPDATE ekontrak.paket_anggaran SET ppk_id = ? where pkt_id = ?",ppk_id,pkt_id);
	}

	public Paket_pl getPaket() {
		if(paket == null)
			paket = Paket_pl.findById(pkt_id);
		return paket;
	}

	@Override
	public Long getAngId() {
		return this.ang_id;
	}

	@Override
	public Long getPpkId() {
		return this.ppk_id;
	}

	@Override
	public Long getPktId() {
		return this.pkt_id;
	}

	@Override
	public AnggaranContract getAnggaranContract() {
		return getAnggaran();
	}

	public Anggaran getAnggaran() {
		if(anggaran == null)
			anggaran = Anggaran.findById(getAngId());
		return anggaran;
	}

	@Override
	public void setAnggaran(Anggaran anggaran) {
		this.anggaran = anggaran;
	}

	@Override
	public Ppk getPpk(){
		if(ppk == null){
			ppk = Ppk.findById(getPpkId());
		}
		return ppk;
	}

	@Override
	public boolean isPpkExist() {
		return getPpk() != null;
	}

	@Override
	public String getPpkNama() {
		return isPpkExist() ? getPpk().getNamaPegawai() : "";
	}

}
