package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

@Table(name="EKONTRAK.HISTORY_PAKET_PP")
public class History_paket_pp extends BaseModel {

    @Id
    public Long pkt_id;

    @Id
    public Long pp_id;

    public Long peg_id;

    public Date tgl_perubahan;

    public String alasan;

    @Transient
    private Pp pp;

    public Pp getPp() {
        if(pp == null)
            pp = Pp.findById(pp_id);
        return pp;
    }


    public static List<History_paket_pp> findByPaket(Long pkt_id){
        return find("pkt_id = ? order by tgl_perubahan desc",pkt_id).fetch();
    }

    public String getNamaPp(){
        return Query.find("SELECT peg_nama FROM pegawai WHERE peg_id=?",String.class, getPp().peg_id).first();
    }

    public String getNamaPegawai(){
        return Query.find("SELECT peg_nama FROM pegawai WHERE peg_id=?",String.class, peg_id).first();
    }
}
