package models.agency;

import models.jcommon.util.CommonUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DaftarKuantitas implements Serializable {

	public List<Rincian_hps> items;
	
	public Double total_pembayaran;
	
	public Double total_ppn; // total ppn (10 % dari total pembayaran)
	
	public Double total; // total nilai;
	
	/**
	 * flag item dalam daftar kuantitas ini boleh diubah atau tidak oleh peserta lelang pada saat menawar
	 * true : peserta tidak boleh mengubah item2 dalam daftar kuantitas dan harga
	 * false : peserta boleh mengubah item2 dalam daftar kuantitas dan harga
	 * default true; 
	 */
	public boolean fixed = true;

	public void setRincianHps(Rincian_hps[] items) {
		if (!CommonUtil.isEmpty(items)) {
			this.items = new ArrayList<>();
			for (Rincian_hps obj : items) {
				if (obj != null && !CommonUtil.isEmpty(obj.item)) {
					obj.item = obj.getCleanedItem();
					obj.keterangan = obj.getCleanedKeterangan();
					this.items.add(obj);
				}
			}
		}
	}


	// set harga satuan ke 0, untuk penawaran peserta lelang perlu diset 0 semua harga satuan dari pokja
	public void emptyHargaSatuan() {
		if (!CommonUtil.isEmpty(items)) {
			for (Rincian_hps rincian : items) {
				rincian.harga = Double.valueOf(0);
				rincian.total_harga = Double.valueOf(0);
			}
		}
	}

	public static DaftarKuantitas populateDaftarKuantitas(String data, boolean fixed){
		DaftarKuantitas dk = new DaftarKuantitas();
		if (!CommonUtil.isEmpty(data)) {
			Rincian_hps[] items = Rincian_hps.fromJson(data);
			if(!CommonUtil.isEmpty(items)) {
				dk.items = Arrays.asList(items);
				dk.total = Rincian_hps.total(items);
			}
		}
        dk.fixed = fixed;
		return dk;
	}

}
