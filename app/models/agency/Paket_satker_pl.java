package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

/**
 * Created by Lambang on 10/2/2017.
 */
@Table(name = "ekontrak.paket_satker")
public class Paket_satker_pl extends BaseModel {

    @Id(sequence = "ekontrak.seq_paket_satker", function = "nextsequence")
    public Long pks_id;

    //relasi ke model Satuan_kerja
    public Long stk_id;

    //relasi ke model Paket
    public Long pkt_id;

    //relasi ke model Rup_paket
    public Long rup_id;

    public static String getNamaSatkerPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=?", String.class, paketId).first();
    }

    public static String getNamaInstansiPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi " +
                "WHERE id in (SELECT instansi_id FROM satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=?)", String.class, paketId).first();
    }

    public static String getKodeRupPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(rup_id), ', ') FROM ekontrak.paket_satker WHERE pkt_id=?", String.class, paketId).first();
    }

    public static String getAlamatSatkerPaket(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_alamat), ', ') FROM public.satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=?", String.class, paketId).first();
    }

    public static String getNamaKabupaten(Long paketId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct k.kbp_nama), ', ') FROM instansi i inner join kabupaten k on i.kbp_id=k.kbp_id " +
                "WHERE i.id in (SELECT instansi_id FROM satuan_kerja s, ekontrak.paket_satker p WHERE p.stk_id=s.stk_id AND pkt_id=?)", String.class, paketId).first();
    }

}
