package models.agency;

import ext.FormatUtils;
import models.common.Kategori;
import models.common.MetodePemilihan;
import models.common.MetodePemilihanPenyedia;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import play.i18n.Messages;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**Untuk versi 4, ada tambahan kolom ppk_nomor_sk dan ppk_id_attachment
 * 
 * @author andik
 *
 */

@Table(name="PPK")
public class Ppk extends BaseModel {
	

	@Id(sequence="seq_ppk", function="nextsequence")
	public Long ppk_id;

	public Date ppk_valid_start;

	public Date ppk_valid_end;

	public Long peg_id;

	/**@since SPSE 4.0 
	*/
	@Required
	public String ppk_nomor_sk;

	public String ppk_unit_kerja_sk;
	
	/**@since SPSE 4.0 
	*/
	public Long ppk_id_attachment;
	
	/**@since SPSE 4.0 
	*/
	public String ppk_pembuat_sk;
	
	@Transient
	private Pegawai pegawai;
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}

	public String getNamaPegawai() {
		return Query.find("SELECT peg_nama FROM pegawai WHERE peg_id=?",String.class, peg_id).first();
	}
	
	public static List<Ppk> findBy(Long pegawaiId) {
		return find("peg_id=?",pegawaiId).fetch();
	}
	
	public static List<Ppk> findByLelang(Long lelangId) {
		return find("ppk_id in (select pa.ppk_id from paket p, lelang_seleksi l, paket_anggaran pa where l.pkt_id=p.pkt_id and p.pkt_id=pa.pkt_id and l.lls_id=?)", lelangId).fetch();
	}
	
	public static Ppk findByLelangAndPegawai(Long lelangId, long pegId) {
		return find("peg_id = ? and ppk_id in (select pppk.ppk_id from paket p, lelang_seleksi l, paket_anggaran pppk where l.pkt_id=p.pkt_id and p.pkt_id=pppk.pkt_id and l.lls_id=?)", pegId, lelangId).first();
	}

	public static Ppk findByPlAndPegawai(Long plId, long pegId) {
		return find("peg_id = ? and ppk_id in (select pppk.ppk_id from ekontrak.paket p, ekontrak.nonlelang_seleksi l, ekontrak.paket_anggaran pppk where l.pkt_id=p.pkt_id and p.pkt_id=pppk.pkt_id and l.lls_id=?)", pegId, plId).first();
	}
	
	public static Ppk findByPl(Long plId) {
		return find("ppk_id in (select pppk.ppk_id from ekontrak.paket p, ekontrak.nonlelang_seleksi l, ekontrak.paket_anggaran pppk where l.pkt_id=p.pkt_id and p.pkt_id=pppk.pkt_id and l.lls_id=?)", plId).first();
	}
	
	
	public static final ResultSetHandler<String[]> resultsetPPK= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("ppk_id");
			tmp[1] = rs.getString("peg_nip");
			tmp[2] = rs.getString("peg_namauser");
			tmp[3] = rs.getString("peg_nama");
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetPaketPPK= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Integer pkt_flag = rs.getInt("pkt_flag");
			Long lelangId = rs.getLong("lls_id");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			String tahaps = rs.getString("tahaps");
			String tahapLabel = Tahap.tahapInfo(tahaps, true, lelangV3, pemilihan.isLelangExpress());
			Integer versi = rs.getInt("lls_versi_lelang");
			String namaLelang = rs.getString("pkt_nama");
			Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
			String tenderUlangLabel = (kgr.isKonsultansi() || kgr.isJkKonstruksi()) ? "Seleksi Ulang" : "Tender Ulang";
			Integer status = rs.getInt("status");
			String tahapNow = rs.getString("tahaps");
			if(versi > 1)
				namaLelang += " <span class='badge  badge-warning'>" + tenderUlangLabel + "</span>";
			boolean pemenang = false;
			if(status == 1) {
				if(pemilihan.isLelangExpress())
					pemenang = true;
				else {
					pemenang = tahapNow.contains("PENUNJUKAN_PEMENANG") || tahapNow.contains("TANDATANGAN_KONTRAK") || tahapNow.contains("SUDAH_SELESAI");
					List<String> lastResponses = Query.find("select (select sgh_pengirim from sanggahan jawab where jawab.san_sgh_id = tanya.sgh_id order by auditupdate desc limit 1) " +
							"from sanggahan tanya where psr_id in (select psr_id from peserta where lls_id=?) and san_sgh_id is null", String.class, lelangId).fetch();
					for (String lastResponse : lastResponses) {
						if (lastResponse == null || !lastResponse.equals("P")) {
							pemenang = false;
							break;
						}
					}
				}
			}
			String[] tmp = new String[8];
			tmp[0] = rs.getString("lls_id");
			tmp[1] = namaLelang;
			tmp[2] = tahapLabel;
			tmp[3] = pemenang ? "true":"false";
			tmp[4] = rs.getString("kgr_id");
			tmp[5] = pemilihan.label;
			tmp[6] = pkt_flag.toString();
			tmp[7] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetNonPaketPPK= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Integer pkt_flag = rs.getInt("pkt_flag");
			Long lelangId = rs.getLong("lls_id");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihanPenyedia pemilihan = MetodePemilihanPenyedia.findById(rs.getInt("mtd_pemilihan"));
//			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
//			MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
			String tahaps = rs.getString("tahaps");
			String tahapLabel = Tahap.tahapInfo(tahaps, true, lelangV3, pemilihan.isLelangExpress());
			Integer versi = rs.getInt("lls_versi_lelang");
			String namaLelang = rs.getString("pkt_nama");
			Kategori kgr = Kategori.findById(rs.getInt("kgr_id"));
			String tenderUlangLabel = pemilihan.isPenunjukanLangsung() ? Messages.get("ct.pnj_lgs_ul") : Messages.get("ct.pngd_lgs_ul");
			Integer status = rs.getInt("status");
			String tahapNow = rs.getString("tahaps");
			if(versi > 1)
				namaLelang += " <span class='badge  badge-warning'>" + tenderUlangLabel + "</span>";
			boolean pemenang = false;
			if(status == 1) {
				if(pemenang || tahapNow.contains("TANDATANGAN_SPK") && tahapNow.contains("TANDATANGAN_KONTRAK") && tahapNow.contains("SUDAH_SELESAI"))
					pemenang = false;
				else{
					if(pemenang = tahapNow.contains("TANDATANGAN_SPK") || tahapNow.contains("TANDATANGAN_KONTRAK") || tahapNow.contains("SUDAH_SELESAI"))
					pemenang = true;

				}


			}

			String[] tmp = new String[9];
			tmp[0] = rs.getString("lls_id");
			tmp[1] = namaLelang;
			tmp[2] = tahapLabel;
			tmp[3] = pemenang ? "true":"false";
			tmp[4] = rs.getString("kgr_id");
			tmp[5] = pemilihan.getLabel();
			tmp[6] = pkt_flag.toString();
			tmp[7] = rs.getBoolean("is_pkt_konsolidasi") ? "1" : "0";
			tmp[8] = String.valueOf(rs.getInt("sudah_verifikasi_sikap"));
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetPpkSppjb= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[9];
			Double hargaKontrak = rs.getDouble("kontrak_nilai");
			if(hargaKontrak == 0L) {
				hargaKontrak = rs.getDouble("harga_final");
			}
			tmp[0] = rs.getString("sppbj_no");
			tmp[1] = FormatUtils.formatDateInd(rs.getDate("sppbj_tgl_kirim"));
			tmp[2] = rs.getString("rkn_nama");
			tmp[3] = rs.getString("sppbj_id");
			tmp[4] = FormatUtils.formatCurrencyRupiah(hargaKontrak);
			tmp[5] = rs.getString("kgr_id");
			tmp[6] = rs.getString("kontrak_id");
			tmp[7] = rs.getString("kontrak_sskk");
			tmp[8] = rs.getString("pes_id");
			return tmp;
		}
	};
}
