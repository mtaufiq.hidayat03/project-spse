package models.agency;

import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Model {@code Anggota_panitia} merepresentasikan tabel anggota_panitia pada
 * database
 *
 * @author I Wayan Wiprayoga W
 */
@Table(name="PEGAWAI_UKPBJ")
public class Pegawai_ukpbj extends BaseModel {

	/**
	 * ID kepanitiaan sebagai referensi ke tabel ukpbj
	 */
	@Id
	public Long ukpbj_id;
	/**
	 * NIP pegawai sebagai referensi ke tabel pegawai
	 */
	@Id
	public Long peg_id;

	@Transient
	private Ukpbj ukpbj;
	@Transient
	private Pegawai pegawai;	
	
	public Ukpbj getUkpbj() {
		if(ukpbj == null)
			ukpbj = Ukpbj.findById(ukpbj_id);
		return ukpbj;
	}

	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}

	/**
	 * Fungsi {@code createAnggotaUkpbj} digunakan untuk membuat entri data anggota panitia di database dengan
	 * menggunakan informasi NIP Pegawai dan ID UKPBJ.
	 *
	 * @param pegawaiId pegawai
	 * @param ukpbjId id ukpbj
	 * @return true jika berhasil membuat entri di database
	 */
	public static void createPegawai(Long pegawaiId, Long ukpbjId) {
		Ukpbj ukpbj = Ukpbj.findById(ukpbjId); // cari objek ukpbj
		Pegawai pegawai = Pegawai.findById(pegawaiId); // cari objek pegawai
		if (ukpbj != null && pegawai != null) {
			Pegawai_ukpbj pegawaiUkpbj = new Pegawai_ukpbj();
			pegawaiUkpbj.ukpbj_id = ukpbj.ukpbj_id;
			pegawaiUkpbj.peg_id = pegawaiId;
			pegawaiUkpbj.save(); // pastikan data bisa disimpan
		} 
	}

	public static final ResultSetHandler<String[]> resultsetPegawaiUkpbj = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
			tmp[0] = rs.getString("peg_id");
			tmp[1] = rs.getString("peg_nama");
			tmp[2] = rs.getString("peg_namauser");
			tmp[3] = rs.getString("peg_nip");
			tmp[4] = rs.getString("peg_email");
			return tmp;
		}
	};
	
	public static final ResultSetHandler<String[]> resultsetCalonPegawaiUkpbj = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[4];
			tmp[0] = rs.getString("peg_nip");
			tmp[1] = rs.getString("peg_nama");
			tmp[2] = rs.getString("peg_namauser");
			tmp[3] = rs.getString("peg_id");
			return tmp;
		}
	};

}
