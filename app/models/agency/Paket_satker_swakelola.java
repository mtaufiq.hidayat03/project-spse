package models.agency;

import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

/**
 * Created by Lambang on 10/23/2017.
 */
@Table(name = "EKONTRAK.PAKET_SATKER_SWAKELOLA")
public class Paket_satker_swakelola extends BaseModel {

    @Id(sequence = "ekontrak.seq_paket_satker_swakelola", function = "nextsequence")
    public Long pks_id;

    //relasi ke model Satuan_kerja
    public Long stk_id;

    //relasi ke model Paket
    public Long swk_id;

    //relasi ke model Rup_paket
    public Long rup_id;

    public static String getNamaSatkerPaket(Long swakelolaId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=?", String.class, swakelolaId).first();
    }

    public static String getNamaInstansiPaket(Long swakelolaId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi " +
                "WHERE id in (SELECT instansi_id FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=?)", String.class, swakelolaId).first();
    }

    public static String getKodeRupPaket(Long swakelolaId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(rup_id), ', ') FROM ekontrak.paket_satker_swakelola WHERE swk_id=?", String.class, swakelolaId).first();
    }

    public static String getAlamatSatkerPaket(Long swakelolaId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_alamat), ', ') FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=?", String.class, swakelolaId).first();
    }

    public static String getNamaKabupaten(Long swakelolaId) {
        return Query.find("SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct k.kbp_nama), ', ') FROM instansi i inner join kabupaten k on i.kbp_id=k.kbp_id " +
                "WHERE i.id in (SELECT instansi_id FROM satuan_kerja s, ekontrak.paket_satker_swakelola p WHERE p.stk_id=s.stk_id AND swk_id=?)", String.class, swakelolaId).first();
    }

}
