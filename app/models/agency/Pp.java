package models.agency;

import models.common.MetodePemilihan;
import models.common.MetodePemilihanPenyedia;
import models.common.Tahap;
import models.jcommon.db.base.BaseModel;
import org.sql2o.ResultSetHandler;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**Untuk versi 4
 * 
 * @author wahid
 *
 */
@Table(name="EKONTRAK.PP")
public class Pp extends BaseModel{
	
	@Id(sequence="ekontrak.seq_pp", function="nextsequence")
	public Long pp_id;

	public Date pp_valid_start;

	public Date pp_valid_end;

	public Long peg_id;

	/**@since E-kontrak 
	*/
	@Required
	public String pp_nomor_sk;
	
	/**@since E-kontrak 
	*/
	public Long pp_id_attachment;
	
	/**@since E-kontrak 
	*/
	public String pp_pembuat_sk;
	
	@Transient
	private Pegawai pegawai;
	
	public Pegawai getPegawai() {
		if(pegawai == null)
			pegawai = Pegawai.findById(peg_id);
		return pegawai;
	}
	
	public static List<Pp> findByPegawai(Long ppId) {
		return find("pp_id in (select p.pp_id from ekontrak.pp p where peg_id=?) ORDER BY pp_id DESC",ppId).fetch();
	}


	public static Pp findBy(Long pegawaiId) {
		return find("peg_id=?",pegawaiId).first();
	}
	
	public static Pp findByLelang1(Long lelangId, Long ppId) {
		return find("lls_id=? and pp_id=?", lelangId, ppId).first();
	}
	
	public static Pp findByLelang(Long lelangId) {
		return find("pp_id in (select pp_id from ekontrak.paket p, ekontrak.nonlelang_seleksi l where l.pkt_id=p.pkt_id and l.lls_id=?)", lelangId).first();
	}
	
	/**
	 * check jabatan Pejabat Pengadaan
	 * @param ppId
	 * @param pegawaiId
	 * @return
	 */
	
	public static boolean isPp(Long ppId, Long pegawaiId) {		
		return count("pp_id=? AND peg_id=?", ppId, pegawaiId) > 0;
	}
	
	public static final ResultSetHandler<String[]> resultsetPP= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[3];
			tmp[0] = rs.getString("pp_id");
			tmp[1] = rs.getString("peg_nip");
			tmp[2] = rs.getString("peg_nama");
			return tmp;
		}
	};
		
	public static final ResultSetHandler<String[]> resultsetPaketPP= new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			Integer pkt_flag = rs.getInt("pkt_flag");
			boolean lelangV3 = pkt_flag < 2;
			MetodePemilihan pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan"));
			MetodePemilihanPenyedia pemilihanpl = MetodePemilihanPenyedia.findBy(pemilihan);
			String[] tmp = new String[7];
			tmp[0] = rs.getString("lls_id");
			tmp[1] = rs.getString("pkt_nama");
			tmp[2] = Tahap.tahapInfo(rs.getString("tahaps"), true,  lelangV3, pemilihanpl.isPl());
			tmp[3] = rs.getInt("status") == 1 ? "true":"false"; // lelang sudah selesai jika status == 1
			tmp[4] = rs.getString("kgr_id");
			tmp[5] = pemilihanpl.getLabel();
			tmp[6] = pkt_flag == 2 ? "Spse 4": "Spse 3";
			return tmp;
		}
	};

	public static final ResultSetHandler<String[]> resultsetPilihPp = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] tmp = new String[5];
			tmp[0] = rs.getString("pp_id");
			tmp[1] = rs.getString("peg_nip");
			tmp[2] = rs.getString("peg_nama");
			tmp[3] = rs.getString("peg_namauser");

			Long ppId = rs.getLong("pp_id");
//			tmp[4] = Pegawai.listNamaAnggotaByPanitiaToString(pntId);
			return tmp;
		}
	};
}
