package models.agency;

import models.common.Active_user;
import models.jcommon.db.base.BaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.util.List;

/**
 * @author Abdul Wahid on 8/2/2019
 */
@Table(name = "paket_ppk", schema = "ekontrak")
public class PaketPpkPl extends BaseModel {

    @Id
    public Long pkt_id;
    @Id
    public Long ppk_id;
    public Integer ppk_jabatan;

    public PaketPpkPl(Long pkt_id, Long ppk_id){
        this.pkt_id = pkt_id;
        this.ppk_id = ppk_id;
        this.ppk_jabatan = 0;
    }

    public PaketPpkPl(Long pkt_id, Active_user active_user) {
        this.pkt_id = pkt_id;
        this.ppk_id = active_user.ppkId;
        this.ppk_jabatan = 0;
    }

    public boolean isItMyPaket(Long ppkId) {
        return ppk_id.equals(ppkId);
    }

    public static PaketPpkPl findByPaketAndPpk(Long pkt_id, Long ppk_id){
        return PaketPpkPl.find("pkt_id = ? and ppk_id = ?",pkt_id,ppk_id).first();
    }

    public static List<PaketPpkPl> findByPaket(Long pkt_id){
        return PaketPpkPl.find("pkt_id = ? order by auditupdate desc", pkt_id).fetch();
    }

    public String getNamaPpk(){
        return Query.find("SELECT peg_nama FROM pegawai p, ppk WHERE p.peg_id=ppk.peg_id AND ppk_id=?",String.class, ppk_id).first();
    }

    public void deleteOldPpk(){
        PaketPpkPl.delete("pkt_id = ? and ppk_id <> ?",pkt_id,ppk_id);
    }

    public void deleteOldPpkNew(Long old_ppk_id){
        PaketPpkPl.delete("pkt_id = ? and ppk_id = ?",pkt_id,old_ppk_id);
    }
}
