package models.agency;


import play.db.jdbc.Query;

import java.util.List;

/**
 * Digunakan untuk ambil paket sesimple mungkin.
 */
public class PaketPlSimple {

    public Long pkt_id;
    public String pkt_nama;
    public String nama_instansi;

    private static final String SELECT = "SELECT p.pkt_id, p.pkt_nama, stk.stk_nama as nama_instansi";
    private static final String TABLE_NAME = "ekontrak.paket p LEFT JOIN ekontrak.paket_satker ps ON p.pkt_id = ps.pkt_id " +
            "LEFT JOIN satuan_kerja stk ON ps.stk_id = stk.stk_id";

    private static String buildQuery(String where) {
        return SELECT + " FROM " + TABLE_NAME + " WHERE " + where;
    }

    public static List<PaketPlSimple> findAll(String where, Object... params) {
        String query = buildQuery(where);
        return Query.find(query, PaketPlSimple.class, params).fetch();
    }

    public static PaketPlSimple find(String where, Object... params) {
        String query = buildQuery(where);
        return Query.find(query, PaketPlSimple.class, params).first();
    }
}
