package models.entity;

import play.i18n.Messages;
import play.mvc.Router;

public enum ItemDataPenyedia {
	IDENTITAS("menu.identitas_perusahaan", "rekanan.RekananCtr.identitas"),
	IZIN_USAHA("menu.izin_usaha", "rekanan.IjinUsahaCtr.list"),
	AKTA("menu.akta", "rekanan.AktaCtr.list"),
	PEMILIK("menu.pemilik", "rekanan.PemilikCtr.list"),
	PENGURUS("menu.pengurus", "rekanan.PengurusCtr.list"),
	TENAGA_AHLI("menu.tenaga_ahli", "rekanan.Staf_AhliCtr.list"),
	PERALATAN("menu.peralatan", "rekanan.PeralatanCtr.list"),
	PENGALAMAN("menu.pengalaman", "rekanan.PengalamanCtr.list"),
	PAJAK("menu.pajak", "rekanan.PajakCtr.list"),
	MIGRASI_SIKAP("menu.integrasi_sikap", "rekanan.MigrasiSikapCtr.index");

	public final String page;

	public final String caption;
	
	public final String action;

	ItemDataPenyedia(String caption, String action) {
		this.caption = caption;
		this.action = action;
		this.page = Router.reverse(action).url;

	}

	public String getCaption(){
		return Messages.get(caption);
	}

}
