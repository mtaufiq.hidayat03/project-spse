package models.entity;

import play.i18n.Messages;
import play.mvc.Router;

/**
 * Created by JavanLabs on 2/23/2016.
 */
public enum ItemDaftarAuditor {
    DAFTAR_AUDITOR("menu.daftar_auditor", "AuditorCtr.index"),
    DAFTAR_SK("menu.daftar_surat_tugas", "AuditorCtr.sk"),
    DAFTAR_SK_NON_LELANG("menu.daftar_srt_tgs_non_tender", "AuditorCtr.skNonLelang");

    public final String page;

    public final String caption;

    public final String action;

    ItemDaftarAuditor(String caption, String action) {
        this.caption = caption;
        this.action = action;
        this.page = Router.reverse(action).url;
    }

    public  String getCaption(){
        return Messages.get(caption);
    }
}
