package models.entity;

public enum EvaluasiEntity {
	// Jenis Evaluasi
	EVALUASI_KUALIFIKASI(0, "Evaluasi Kualifikasi"), EVALUASI_ADMINISTRASI(1, "Evaluasi Administrasi"),
	EVALUASI_TEKNIS(2, "Evaluasi Teknis"), EVALUASI_BIAYA(3, "Evaluasi Biaya"), EVALUASI_AKHIR(4, "Evaluasi Akhir"),
	EVALUASI_NONE(-1, "Tidak Ada Evaluasi"),

	// Status Evaluasi
	EVALUASI_DRAFT(0, "Draft"), EVALUASI_APPROVED(1, "Disetujui"), EVALUASI_DECLINE(2, "Ditolak"),

	// flag nilai evaluasi
	NIlAI_EVALUASI_TAK_LULUS(0, "tidak lulus"), NILAI_EVALUASI_LULUS(1, "lulus"),

	// Jenis Evaluasi by Aktivitas
	CURRENT_EVALUASI_NONE(-1, "Tidak Ada Aktivitas Evaluasi"), CURRENT_EVALUASI_KUALIFIKASI(0, "Aktivitas Evaluasi Kualifikasi"),
	CURRENT_EVALUASI_ADMINISTRASI_TEKNIS(1, "Aktivitas Evaluasi Administrasi Teknis"), CURRENT_EVALUASI_BIAYA(2, "Aktivitas Evaluasi Biaya"),
	CURRENT_EVALUASI_SEMUA_PENAWARAN(3, "Aktivitas Evaluasi Terhadap semua Penawaran"), CURRENT_EVALUASI_AKHIR(4, "Aktivitas Usulan Calon pemenang");

	public final Integer id;

	public final String label;

	EvaluasiEntity(int id, String nama) {
		this.id = Integer.valueOf(id);
		this.label = nama;
	}
}
