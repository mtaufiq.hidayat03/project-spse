package models.entity;

public enum ContentType {
	pdf("application/pdf", "pdf"), excel("application/vnd.ms-excel", "xls"),
	doc("application/msword", "doc"), jpg("image/jpeg", "jpeg"),
	gif("image/gif", "gif"), exe("application/executable", "exe"),
	html("text/html", "html"), zip("application/zip", "zip");

	public final String type;

	public final String extension;

	ContentType(String type, String extension) {
		this.type = type;
		this.extension = extension;
	}
}
