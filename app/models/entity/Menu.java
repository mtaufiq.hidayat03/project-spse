package models.entity;

import models.secman.Group;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Router;

/**
 * Kelas {@code Menu} merupakan kelas yang berisi definsi menu aplikasi. baik untuk user terdaftar
 * ataupun menu halaman publik.<br />
 *
 * Catatan untuk SPSE 4:<br />
 * 1. Nama page diubah, contoh RekananDataMain menjadi Rekanan.DataMain * (controller.action) {case insensitive}
 * ADDED BY arief 
 * TODO : definisi URL di menu tidak type-safety, kemungkinan salah ketik sangat besar
 */

public enum Menu {
	/**
	 * Definsi menu untuk halaman publik
	 */
	INDEX("menu.beranda", "PublikCtr.index"),
	HOME("menu.beranda", "BerandaCtr.index"),
	PUBLIK_CARI_LELANG("menu.caripaket", "lelang.LelangCtr.index"),
	PUBLIK_REGULASI("menu.regulasi", "https://jdih.lkpp.go.id/"),
	PUBLIK_KONTAK_KAMI("menu.kontak","PublikCtr.kontakKami"),
	PUBLIK_SPECIAL_CONTENT("menu.specialkonten", "PublikCtr.special"),
	PUBLIK_DAFTAR_HITAM("menu.daftarhitam", "http://inaproc.id/daftar-hitam"),
	PUBLIK_TANYA_JAWAB("menu.tanyajawab","admin.FaqCtr.list"),
	PUBLIK_PENDAFTARAN_PENYEDIA("menu.regispenyedia", "PublikCtr.mendaftarEmail"),
	PUBLIK_LOGIN("menu.login", "PublikCtr.login"),
	/**
	 * Definisi menu spesifik untuk user dengan role {@code REKANAN}
	 */

	REKANAN_DATA("menu.datapenyedia","rekanan.RekananCtr.identitas"),
	LELANG_BARU("menu.paketbaru", "lelang.LelangCtr.lelangBaru"),
	REKANAN_INBOX("menu.inbox", "rekanan.RekananCtr.inbox"),
	
	
	/**
	 * Definisi menu spesifik untuk user dengan role {@code PANITIA}
	 */

	PANITIA_DAFTAR_PAKET("menu.daftarpaket", "lelang.PaketCtr.index"), // Menu lihat semua daftar paket lelang
	PANITIA_NON_EPROC("menu.non_eprocurement", "NonEprocCtr.index"), // Menu membuat paket non-eprocurement
//	PANITIA_IDENTITAS("Kepanitiaan/Pokja", "admin.PanitiaCtr.identitas"), // informasi Kepanitiaan
	PANITIA_BERITA("menu.berita", "admin.BeritaCtr.panitia"),
	PP_DAFTAR_PAKET("menu.daftarpaket", "nonlelang.PaketPlCtr.index"), //Menu lihat semua daftar paket lelang pl
//	DAFTAR_PAKET_PILIHAN_LAINNYA("Daftar Paket Lainnya", "nonlelang.PaketPlCtr.lain"), // Menu lihat semua daftar paket lelang lainnya
	PPK_DAFTAR_SWAKELOLA("menu.swakelola", "swakelola.PaketSwakelolaCtr.index"), // Menu lihat semua daftar paket lelang swakelola

	PP_BERITA("menu.berita", "admin.BeritaCtr.pp"),
	/**
	 * Definisi menu spesifik untuk user dengan role {@code PPK}
	 */

	/**
	 * Definisi menu spesifik untuk user dengan role {@code VERIFIKATOR}
	 */

	VERIFIKATOR_REKANAN("menu.verifikator_rekanan", "VerifikatorCtr.rekanan"),
	/**
	 * Definisi menu spesifik untuk user dengan role {@code HELPDESK}
	 */

	HELPDESK_EMAIL("menu.email", "admin.EmailCtr.index"),

	/**
	 * Definsi menu spesifik untuk user dengan role {@code ADMIN AGENCY}
	 */

	ADM_AGENCY_IDENTITAS("menu.agency_identitas", "admin.AgencyCtr.identitas"), // Menu identitas agency
	ADM_AGENCY_SATKER("menu.agency_satuankerja", "admin.SatkerCtr.index"), // Menu daftar satker
	ADM_AGENCY_PAKET("menu.agency_paket", "admin.AgencyCtr.paket"),
	ADM_AGENCY_PANITIA("menu.agency_pokja", "admin.PanitiaCtr.index"), // Menu daftar panitia
	/**
	 * Definisi menu spesifik untuk user dengan role {@code ADMIN PPE}
	 */

	ADM_PPE_FAQ("FAQ", "admin.FaqCtr.index"), // Menu daftar FAQ
//	ADM_PPE_MASTER("Data Master", "admin.data_master"), // 
	ADM_PPE_UTILITY("menu.utility", "admin.EmailCtr.index"), // Menu lihat status pengiriman email
	ADM_PPE_AUDITOR("menu.auditor", "AuditorCtr.index"), // Menu daftar auditor

	/**
	 * Definsi menu spesifik untuk user dengan role {@code AUDITOR}
	 */

	/**
	 * Definsi menu user dengan role {@code TRAINER}
	 */

	/**
	 * Definisi menu untuk user secara umum
	 */
	USER_LOGIN_LOG( "menu.akseslog","UserCtr.logUser"), // Menu untuk melihat log akses pengguna
	USER_LOGOUT("menu.logout", "LoginCtr.logout"), // Menu Logout
	GANTI_PASSWORD("menu.gantipassword","UserCtr.gantiPassword"), // Menu untuk mengganti pssword
	/**
	 * Definsi menu terkait dengan administrasi AGENCY dan SUB-AGENCY
	 */
	ADMIN_AGENCIES("menu.sub_agency", "admin.AgencyCtr.index"), // Menu daftar sub-agency (user Admin Agency)

	PPE_AGENCIES("menu.agency", "admin.AgencyCtr.index"), // Menu daftar agency (user Admin PPE)
	/**
	 * Definsi menu terkait dengan FAQ
	 */
	ADMIN_FAQ("menu.faq", "admin.FaqCtr.index"), // Menu lihat daftar FAQ
	/**
	 * Definsi menu terkait dengan administrasi kePEGAWAIan
	 */
	ADMIN_PEGAWAI("menu.pegawai","admin.PegawaiCtr.index"),// Menu daftar pegawai
	
	DATA_MASTER("menu.datamaster", "admin.UtilityCtr.datamaster"),
	/**
	 * Deklarasi daftar menu untuk masing-masing role user
	 */
	//menu rekanan non-etendering PL
//	HOME_PL("Beranda Non E-Tendering", "BerandaCtr.rekananPl");
//	PL_BARU("PL Baru", "nonlelang.PengadaanLCtr.plBaru");

	//menu non-transaksional di PPK
	NON_TRANSAKSIONAL("menu.non_traksional","BerandaCtr.PpkNonTransaksional"),
	DAFTAR_PAKET_PPK("menu.daftar_paket_ppk","nonlelang.PaketPlCtr.indexNonSpk"),

	//menu swakelola ppk
	SWAKELOLA("menu.swakelola_pencatatan", "BerandaCtr.PpkSwakelola"),
	ADMIN_AGENCY_UKPBJ("menu.ukpbj","admin.UkpbjCtr.index"),

	UKPBJ_IDENTITAS("menu.identitas", "admin.UkpbjCtr.identitas"), // Menu identitas agency
	UKPBJ_PAKET("menu.paket", "admin.UkpbjCtr.paket"); // menu paket yg diassign

	public static Menu[] MENU_PANITIA = new Menu[]{HOME, PANITIA_DAFTAR_PAKET, PANITIA_BERITA, USER_LOGIN_LOG, GANTI_PASSWORD};
	public final static Menu[] MENU_PPE = new Menu[]{HOME, PPE_AGENCIES, ADMIN_PEGAWAI, ADM_PPE_AUDITOR, ADM_PPE_UTILITY, USER_LOGIN_LOG, GANTI_PASSWORD};
	public final static Menu[] MENU_AGENCY = new Menu[]{HOME, ADMIN_PEGAWAI, ADMIN_AGENCY_UKPBJ, ADMIN_AGENCIES, ADM_AGENCY_SATKER, ADM_AGENCY_PAKET,ADM_AGENCY_PANITIA, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static Menu[] MENU_PP = new Menu[]{HOME, PP_DAFTAR_PAKET,PP_BERITA, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static Menu[] MENU_PPK = new Menu[]{HOME, PANITIA_DAFTAR_PAKET, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static final Menu[] MENU_VERIFIKATOR = new Menu[]{HOME, VERIFIKATOR_REKANAN, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static final Menu[] MENU_HELPDESK = new Menu[]{HOME, HELPDESK_EMAIL, DATA_MASTER, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static final Menu[] MENU_TRAINER = new Menu[]{HOME, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static final Menu[] MENU_REKANAN = new Menu[]{HOME, REKANAN_DATA, LELANG_BARU, REKANAN_INBOX, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static final Menu[] MENU_AUDITOR = new Menu[]{HOME, USER_LOGIN_LOG, GANTI_PASSWORD};
	public static final Menu[] MENU_PUBLIK = new Menu[]{INDEX, PUBLIK_CARI_LELANG, PUBLIK_REGULASI, PUBLIK_SPECIAL_CONTENT, PUBLIK_DAFTAR_HITAM, PUBLIK_KONTAK_KAMI}; // menu untuk publik
	public static final Menu[] MENU_UKPBJ = new Menu[]{HOME, PANITIA_DAFTAR_PAKET, ADM_AGENCY_PANITIA, USER_LOGIN_LOG, GANTI_PASSWORD};
//	public final static Menu[] MENU_REKANAN_PL = new Menu[]{HOME, REKANAN_DATA};
	//Dua array menu untuk keperluan login (Publik) dan logout (User terdaftar)
    //public final static Menu[] MENU_LOGIN = new Menu[]{PUBLIK_PENDAFTARAN_PENYEDIA, PUBLIK_LOGIN};
	//public final static Menu[] MENU_LOGOUT = new Menu[]{USER_LOGOUT};
	/**
	 * Judul menu
	 */
	public final String caption;

	/**
	 * mengacu pada action pada controller, untuk alasan type-safety, kemudahan development dan tidak tergantung pada route.conf 
	 */
	public final String action ;
	
	/**
	 * URL target, menggunakan Router.reverse untuk alasan type-safety, kemudahan development dan tidak tergantung pada route.conf
	 */
	public final String page;
	/**
	 * Konstruktor alternatif
	 *
	 * @param caption string judul menu
	 * @param action    url halaman terformat sesuai kaidah variabel {@code page}
	 */
	Menu(String caption, String action) {
		this.caption = caption;

		this.action = action;

		if(action.contains("http")){
			this.page = action;
		}else{
			this.page = Router.reverse(action).url;
		}
	}

	 /** Fungsi {@code getMenuLeft} digunakan untuk mendapatkan daftar menu utama untuk tiap user
      *
      * @param groupName enumerasi grup pengguna yang didefinisikan pada kelas {@link Group}
      * @return daftar menu
      */
     public static Menu[] findByGroup(Group groupName) {
         if (groupName == null) {
             return Menu.MENU_PUBLIK;
         } else {
             switch (groupName) {
                 case PANITIA:
                     return Menu.MENU_PANITIA;
                 case ADM_PPE:
                     return Menu.MENU_PPE;
                 case ADM_AGENCY:
                     return Menu.MENU_AGENCY;
                 case PPK:
                     return Menu.MENU_PPK;
                 case PP:
                     return Menu.MENU_PP;
                 case VERIFIKATOR:
                     return Menu.MENU_VERIFIKATOR;
                 case HELPDESK:
                     return Menu.MENU_HELPDESK;
                 case TRAINER:
                     return Menu.MENU_TRAINER;
                 case REKANAN:
                     return Menu.MENU_REKANAN;
                 case AUDITOR:
                     return Menu.MENU_AUDITOR;
				 case UKPBJ:
				 case KUPPBJ:
				 	 return Menu.MENU_UKPBJ;
                 default:
                     return Menu.MENU_PUBLIK; // user tidak punya grup berarti termasuk akses publik
             }
         }


     }

	public  String getCaption(){
		return Messages.get(caption);
	}

	public String getCaptionUpperCase(){
     	return Messages.get(caption).toUpperCase();
	}
}
