package models.tus;

import play.cache.Cache;

import java.io.Serializable;

public class UuidStore implements Serializable {

	private static final String UUID_CACHE = "UUID_TABLE_";
	public String uuid;
	public int finalLength;
	public int currentoffset;
	public String filename;
	public String fileowner;
	public Long rkn_id;
	
	public static boolean isExistUuid(String uuid) {
		return findById(uuid) != null;
	}
	
	public static UuidStore findById(String uuid) {
		return Cache.get(UUID_CACHE+uuid, UuidStore.class);
	}
	
	public void save() {
		Cache.set(UUID_CACHE+uuid, this);
	}
	
	public void delete() {
		Cache.delete(UUID_CACHE+uuid);
	}
}
