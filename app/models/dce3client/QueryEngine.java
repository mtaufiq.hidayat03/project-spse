package models.dce3client;

import models.jcommon.util.TempFileManager;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import play.Logger;
import play.Play;
import play.db.Configuration;
import play.db.DB;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;

/**Sudah dites untuk Postgres 8.4.x dan 9.5.x
 * @author Andik
 *
 */
public class QueryEngine {

	private String url;
	private String user;
	private String pass;
	
	public String error;//error yg dihasilkan
	public long rows=-1;
	public String md5Value;
	
	public enum SqlCommands
	{
		SELECT, UPDATE, DELETE, ALTER, DROP, INSERT;
	}
		
	public QueryEngine()
	{
		Configuration configuration = new Configuration(DB.DEFAULT);
		this.url=configuration.getProperty("db.url");
		this.user=configuration.getProperty("db.user");
		this.pass=configuration.getProperty("db.pass");
	}
	
	/**Lakukan eksekusi SQL
	 * @param sql
	 * @return
	 */
	public File select(String sql)
	{
		try(BaseConnection conn=(BaseConnection)DriverManager.getConnection(url, user, pass))
		{
			
			File file=executeCopy(conn, sql);
			//dapatkan MD5 dari file ini
			md5Value= Hex.encodeHexString(DigestUtils.md5(new BufferedInputStream(new FileInputStream(file))));
			//lakukan enkripsi file ini dengan pu
			Logger.debug("[SPSE] SQL: %s, MD5: %s, rows: %,d size: %,d, file: %s", sql, md5Value, rows, file.length(), file.getPath());
			
			return file;
		}
		catch(SQLException | IOException e)
		{
			error=e.getMessage();
			Logger.error("[QueryEngine] %s, SQL: %s", e, sql);
			return null;
		}
	}
	
	private File executeCopy(BaseConnection conn, String sqlSelect) throws SQLException, IOException
	{
		CopyManager cm=conn.getCopyAPI();
		String sqlCopy=String.format(" COPY (%s) TO STDOUT  csv header DELIMITER ','  ", sqlSelect); //harus jalan di postgres 8.
		File file=TempFileManager.createFileInTemporaryFolder("Query." + UUID.randomUUID() + ".csv.gz");
		FileOutputStream fout=new FileOutputStream(file);
		OutputStream out=new BufferedOutputStream(fout, 1024*10);
		GZIPOutputStream gzip=new GZIPOutputStream(out); //jika sebagai gzip
		rows=cm.copyOut(sqlCopy, gzip);
		gzip.close();
		out.close();
		fout.close();
		return file;
	}
	
	/**Untuk DDL atau Insert
	 * @param sql
	 * @return
	 */
	public boolean alterDropInsert(String sql)
	{
		try(BaseConnection conn=(BaseConnection)DriverManager.getConnection(url, user, pass))
		{
			Statement st=conn.createStatement();
			st.executeUpdate(sql);
			st.close();
			return true;
		} catch (SQLException e) {
			error=e.getMessage();
			return false;
		}
	}

	/**Untuk update/delete
	 * Dibedakan dangan alterDropInsert karena
	 * 1. ada pengecekan terhadap WHERE
	 * 2. lakukan backup dengan export data yg di-update ke file CSV
	 * 
	 * @param sql
	 * @return jumlah row yg diupdate-delete
	 *  -1 jika gagal
	 */
	public int updateDelete(String sql, boolean createBackup)
	{
		try(Connection conn=DB.getConnection())
		{
			//DELETE FROM table_name
			//UPDATE table_name
			Pattern patternUpdate=Pattern.compile("update\\s+(\\w+)\\s+.*\\s+where\\s+(.*)$", Pattern.CASE_INSENSITIVE);
			Pattern patternDelete=Pattern.compile("delete\\s+from\\s+(\\w+)\\s+where\\s+(.*)$", Pattern.CASE_INSENSITIVE);
			Matcher m=patternUpdate.matcher(sql);
			if(!m.matches())
				m=patternDelete.matcher(sql);
			if(m.matches())
			{
				String where=m.group(2);
				String table=m.group(1);
				Logger.debug("[QueryEngine] table:%s, where:%s", table, where);
				Statement st=conn.createStatement();
				int rows=st.executeUpdate(sql);
				return rows;
			}
			else
			{
				error="UPDATE/DELETE harus menggunakan WHERE";
				return -1;
			}
		} catch (SQLException e) {
			error=e.getMessage();
			return -1;
		}
	}
	
}
