package models.dce3client;

import play.cache.Cache;
import play.db.jdbc.Query;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**Session manager untuk DCE
 * 
 */
public class KeyManager {

	private static Map<String, DceKey> map=new HashMap<>();
	
	public static boolean isTokenValid(String tokenFromDCE)
	{
		String tokenSPSE=Cache.get("dceTokeSPSE", String.class);
		if(tokenSPSE==null)
		{
			tokenSPSE= Query.find("SELECT auditupdate::text FROM peserta ORDER BY psr_id LIMIT 1", String.class).first();
			Cache.set("dceTokenSPSE", tokenSPSE);
		}
		return tokenFromDCE.equals(tokenSPSE);
	}
	
	public static void addKey(String dceSessionId, DceKey dceKey)
	{
		map.put(dceSessionId, dceKey);
	}
	
	public static DceKey getKey(String dceSessionId)
	{
		DceKey key= map.get(dceSessionId);
		if(key!=null)
			key.lastActivityTime=new Date();//update last activity
		return key;
	}

	public static void removeKey(String dceSessionId) {
		map.remove(dceSessionId);
	}

}
