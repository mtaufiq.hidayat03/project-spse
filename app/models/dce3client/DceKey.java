package models.dce3client;

import org.joda.time.Duration;

import java.util.Date;

public class DceKey {

	public String dceSessionId;
	public String dcePublicKey;//kunci server DCE
	public String spsePrivateKey;
	public String spsePubliKey;
	public Date lastActivityTime;
	
	public DceKey()
	{
		lastActivityTime=new Date();
	}

	//expires kalau sudah 6 jam
	public boolean isKeyExpires() {
		Duration dur=new Duration(new Date().getTime() - lastActivityTime.getTime());
		return (dur.getStandardHours() > 6);
	}
}
