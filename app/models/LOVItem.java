package models;

import models.common.*;
import models.jcommon.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.cache.CacheFor;
import play.db.jdbc.JdbcQuery;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Note: CacheFor("xx") telah di-delete oleh ANDIK karena CacheFor hanya berlaku untuk action; tidak untuk model
 * 
 * @author Mr. Andik
 *
 */
public class LOVItem {
	
	public Object id;

	public String label;
	
	public static final ResultSetHandler<LOVItem> beanCreator = new ResultSetHandler<LOVItem>() {

		@Override
		public LOVItem handle(ResultSet rs) throws SQLException {
			LOVItem obj = new LOVItem();
			obj.id = rs.getLong("id");
			obj.label = rs.getString("label");
			return obj;
		}
	};
	
	public static final ResultSetHandler<LOVItem> beanCreatorString = new ResultSetHandler<LOVItem>() {

		@Override
		public LOVItem handle(ResultSet rs) throws SQLException {
			LOVItem obj = new LOVItem();
			obj.id = rs.getString("id");
			obj.label = rs.getString("label");
			return obj;
		}
	};

	@CacheFor("1h")
	public static List<LOVItem> getPegawaiPpk() {
		return Query.find("select ppk_id as id, peg.peg_nama ||' - '|| peg.peg_nip as label from ppk p, pegawai peg where p.peg_id=peg.peg_id order by peg.peg_nama asc", beanCreator).fetch();
	}
	
	/**
	 * mengambil data satker dari rup , ditandai dengan agc_id null
	 * @param id
	 * @return
	 */
	public static List<LOVItem> getSatkerPusat(String id, Integer tahun) {
		if(tahun == null)
			tahun = DateUtil.getTahunSekarang();
		QueryBuilder sql = new QueryBuilder("select stk_id as id, stk_nama as label from satuan_kerja where  agc_id is null AND rup_stk_tahun ilike ?", "%"+tahun+"%");
		if(!StringUtils.isEmpty(id))
			sql.append("AND instansi_id=?", id);
		sql.append("order by stk_nama asc");
		return Query.find(sql, beanCreator).fetch();
	}
	
	public static List<LOVItem> getSatkerAgency(Long agc_id) {		
		return Query.find("select stk_id as id, stk_nama as label from satuan_kerja  where agc_id="+agc_id+" order by stk_nama asc", beanCreator).fetch();
	}

	public static List<LOVItem> getSatkerAll() {
		return Query.find("select stk_id as id, stk_nama as label from satuan_kerja order by stk_nama asc", beanCreator).fetch();
	}

	public static List<LOVItem> getKabupaten(Object prp_id) {
		return Query.find("select kbp_id as id, kbp_nama as label from kabupaten where prp_id="+prp_id+" order by prp_id, kbp_nama", beanCreator).fetch();
	}
	
	public static List<LOVItem> getPropinsi() {
		return Query.find("select prp_id as id, prp_nama as label from propinsi order by prp_nama", beanCreator).fetch();
	}
	
	public static List<LOVItem> getInstansi() {
		return Query.find("select id, nama as label from instansi order by nama", beanCreatorString).fetch();
	}

	public static List<LOVItem> getInstansiByKabupaten(Long kbpId) {
		return Query.find("select id, nama as label from instansi where kbp_id = ? order by nama", beanCreatorString, kbpId).fetch();
	}
	
	public static List<LOVItem> getSumberDana() {
		return Query.find("select sbd_id as id, sbd_ket as label from sumber_dana order by sbd_ket", beanCreatorString).fetch();
	}
	
	public static List<LOVItem> getLelangSamaMetode(Long lls_id, Metode metode, MetodePemilihan pemilihan) {
		return Query.find("select lls_id as id, pkt_nama as label from lelang_seleksi l, paket p " +
						"where l.pkt_id=p.pkt_id and l.lls_id <> ? and l.mtd_pemilihan = ? and l.mtd_id= ? order by pkt_nama", beanCreatorString, lls_id, pemilihan.id, metode.id).fetch();
	}
	
	/**Untuk kebutuhan copy Jadwal lelang. Dicopy dari lelang yang memiliki jadwal belum dilewati
	 * 
	 * @param lls_id
	 * @param metode
	 * @param pemilihan
	 * @return
	 */
	public static List<LOVItem> getLelangSamaMetodeYangHanyaAdaDiJadwal(Long lls_id, Metode metode, MetodePemilihan pemilihan) {
		Date now=controllers.BasicCtr.newDate();
		int urut = 1;
		if(pemilihan.isLelangExpress()){
			urut = 3;
		}
		return Query.find("select lls_id as id, pkt_nama as label from lelang_seleksi l, paket p " +
				"where exists(select lls_id from jadwal j join aktivitas a on j.akt_id = a.akt_id " +
				"where j.lls_id = l.lls_id AND dtj_tglawal > ? AND a.akt_urut = ?) "
						+ "and l.pkt_id=p.pkt_id and l.lls_id <> ? and l.mtd_pemilihan = ? and l.mtd_id= ? order by pkt_nama", beanCreatorString, now, urut, lls_id, pemilihan.id, metode.id).fetch();
	}

	public static List<LOVItem> getPlSamaMetodeYangHanyaAdaDiJadwal(Long lls_id, MetodePemilihanPenyedia pemilihan) {
		Date now=controllers.BasicCtr.newDate();
		return Query.find("select lls_id as id, pkt_nama as label from ekontrak.nonlelang_seleksi l, ekontrak.paket p " +
				"where exists(select lls_id from ekontrak.jadwal j join ekontrak.aktivitas_pl ap on j.akt_id = ap.akt_id " +
				"where j.lls_id = l.lls_id AND j.dtj_tglawal > ? AND ap.akt_urut = 1) "
				+ "and l.pkt_id=p.pkt_id and l.lls_id <> ? and l.mtd_pemilihan = ? order by pkt_nama", beanCreatorString, now, lls_id, pemilihan.id).fetch();
	}
	
	public static List<LOVItem> getPaketRupByKldiAndSatker(String instansiId, Long satkerId){
		return Query.find("select p.id as id, p.nama as label from rup_paket_penyedia inner join instansi a on p.kode_kldi=a.id AND a.id = ? " + 
						"inner join satuan_kerja s on p.id_satker = s.rup_stk_id AND  s.stk_id= ?", beanCreatorString,instansiId, satkerId).fetch();
	}
	
	public static List<LOVItem> getAdminAgency(Long agcId, Active_user user) {
		StringBuilder sql = new StringBuilder();
		sql.append("select peg_id as id, peg_nama || '- '|| peg_nip as label from pegawai where peg_namauser in (select userid from usergroup where idgroup='ADM_AGENCY')");

		if(user.isAdminPPE()){
			sql.append(" AND agc_id IS NULL");
		}else if(user.isAdminAgency()){
			sql.append(" AND agc_id = :agcId");
		}

		sql.append(" order by peg_nama");

		JdbcQuery query = Query.find(sql.toString(), beanCreator);
		if (user.isAdminAgency()){
		    query.setParameter("agcId",agcId);
        }

		return query.fetch();
	}

    public static List<LOVItem> getMetodeNonEproc() {
        return Query.find("select stg_id as id, stg_nama as label from sub_tag where tag_id='METODE_PEMILIHAN'", beanCreatorString).fetch();
    }
    
    public static List<LOVItem> getJenisLpse() {
        return Query.find("select stg_id as id, stg_nama as label from sub_tag where tag_id='JENIS_LPSE'", beanCreatorString).fetch();
    }
    
    public static List<LOVItem> getJenisAgency() {
        return Query.find("select jna_id as id, jna_jenis_agency as label from jenis_agency order by jen_jna_id", beanCreator).fetch();
    }
    
	public static List<LOVItem> getSatkerAuditor(Object agc_id) {
		return Query.find("select stk_id as id, stk_nama as label from satuan_kerja  where agc_id="+agc_id+" order by agc_id, stk_nama", beanCreator).fetch();
	}
    
    public static List<LOVItem> getAgency() {
        return Query.find("select agc_id as id, agc_nama as label from agency order by agc_nama asc", beanCreator).fetch();
    }
    
    public static List<LOVItem> getAgencySatker() {
        return Query.find("select agency.agc_id as id, agc_nama as label from agency , satuan_kerja where satuan_kerja.agc_id = agency.agc_id order by agc_nama asc", beanCreator).fetch();
    }
    
    public static List<LOVItem> getBentukUsaha() {
		List<LOVItem> list = new ArrayList<>();
		for(Bentuk_usaha obj:Bentuk_usaha.values()) {
			LOVItem lov = new LOVItem();
			lov.id = obj.id;
			lov.label = obj.label;
			list.add(lov);
		}
        return list;
    }
    
	public static List<LOVItem> getPegawaiPphp() {
		return Query.find("select peg_id as id, peg.peg_nama ||' - '|| peg.peg_nip as label from  pegawai peg,usergroup u where  peg.peg_namauser=u.userid AND u.idgroup='PPHP' order by peg.peg_nama asc", beanCreator).fetch();
	}

	public static List<LOVItem> getPanitia(Long pegawaiId) {
		return Query.find("select p.pnt_id as id, p.pnt_nama as label from Anggota_panitia ap, Panitia p where peg_id=? and ap.pnt_id=p.pnt_id",
				beanCreator, pegawaiId).fetch();
	}

	public static List<LOVItem> getJenisIjin() {
		return Query.find("select jni_id as id, jni_nama as label from jenis_ijin order by jni_nama asc", beanCreator).fetch();
	}

	public static List<LOVItem> getNegara() {
		return Query.find("select id, nama as label from negara order by nama asc", beanCreator).fetch();
	}
	
}
