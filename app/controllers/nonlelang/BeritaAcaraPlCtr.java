package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.PdfType;
import models.agency.Paket_satker_pl;
import models.common.Active_user;
import models.common.Instansi;
import models.common.Tahap;
import models.common.UploadInfo;
import models.nonlelang.*;
import models.secman.Group;
import play.Logger;
import play.cache.CacheFor;
import play.data.binding.As;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * Created by rayhanfrds on 2/22/17.
 */
public class BeritaAcaraPlCtr extends BasicCtr {

    public static void index(Long id) {
        //
    }

    @AllowAccess({Group.REKANAN, Group.PP, Group.PPK, Group.PANITIA})
    public static void uploadInformasiLainnya(Long id, File file) {
    		otorisasiDataPl(id); // check otorisasi data lelang
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            UploadInfo info = Berita_acara_pl.simpan(id, Tahap.UPLOAD_BA_TAMBAHAN, file);
            List<UploadInfo> files = new ArrayList<UploadInfo>();
            files.add(info);
            result.put("success", true);
            result.put("files", files);
        }catch(Exception e) {
            Logger.error("Kesalahan saat upload informasi lainnya: detail %s", e.getMessage());
            result.put("result", Messages.get("flash.ksuil"));
			result.put("success", false);
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PP, Group.PANITIA})
    @CacheFor("15s")
    public static void preview(Long id, Tahap jenis, String no, @As(binder= DatetimeBinder.class) Date tanggal, String info){
        Berita_acara_pl berita_acara = new Berita_acara_pl();
        berita_acara.brt_no = no;
        berita_acara.brt_tgl_evaluasi = tanggal;
        berita_acara.brt_info = info;
        berita_acara.brc_jenis_ba = jenis;
        Pl_seleksi pl = Pl_seleksi.findById(id);
        renderArgs.put("berita_acara", berita_acara);
        renderArgs.put("pl", pl);
        renderArgs.put("paket", pl.getPaket());
        renderArgs.put("instansi", Paket_satker_pl.getNamaInstansiPaket(pl.pkt_id));
        EvaluasiPl akhir = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
        renderArgs.put("akhir", akhir);
        if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
            renderArgs.put("pesertaList", PesertaPl.findByPl(pl.lls_id));
        }
        if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
            EvaluasiPl administrasi = EvaluasiPl.findAdministrasi(pl.lls_id);
            if(administrasi != null)
                renderArgs.put("pesertaAdm", administrasi.findPesertaList());
            EvaluasiPl teknis = EvaluasiPl.findTeknis(pl.lls_id);
            if(teknis != null)
                renderArgs.put("pesertaTeknis", teknis.findPesertaList());
        }
        if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
            EvaluasiPl harga = EvaluasiPl.findHarga(pl.lls_id);
            if(harga != null)
                renderArgs.put("pesertaHarga", harga.findPesertaList());
        }
        if (berita_acara.isBAHasil()) {
            EvaluasiPl kualifikasi = EvaluasiPl.findKualifikasi(pl.lls_id);
            if(kualifikasi != null)
                renderArgs.put("pesertaKualifikasi", kualifikasi.findPesertaList());
            EvaluasiPl pembuktian = EvaluasiPl.findKualifikasi(pl.lls_id);
            if(pembuktian != null)
                renderArgs.put("pesertaPembuktian", pembuktian.findPesertaList());
        }
        String panitia = Active_user.current().isPP() ? Messages.get("flash.pejabat_pengadaan") : Messages.get("flash.pokja_pilihan");
        renderArgs.put("panitia", panitia);

        renderTemplate("nonlelang/berita_acara/baPlPreview.html");

    }

    public static void cetak(Long id, Tahap jenis, String no, @As(binder= DatetimeBinder.class) Date tanggal, String info, String tempat) {
        checkAuthenticity();
        otorisasiDataPl(id);
        Berita_acara_pl berita_acara = Berita_acara_pl.findBy(id, jenis);
        if(berita_acara == null) {
            berita_acara = new Berita_acara_pl();
            berita_acara.lls_id = id;
            berita_acara.brc_jenis_ba = jenis;
        }
        berita_acara.brt_no = no;
        berita_acara.brt_tgl_evaluasi = tanggal;
        berita_acara.brt_info = info;
        if (tempat != null && !tempat.equalsIgnoreCase(""))
            berita_acara.brt_tempat = tempat;
        try {
            InputStream is = Berita_acara_pl.cetak(jenis, id, berita_acara);
            if(is != null) {
            		renderBinary(is, berita_acara.getJudul() + '-' + id + ".pdf");
                flash.success(berita_acara.getJudul() + Messages.get("flash.tdc"));
            }
        }catch (Exception e){
            Logger.error("@nonlelang.BeritaAcaraPlCtr.cetak : %s", e.getCause());
            flash.error(berita_acara.getJudul() + Messages.get("flash.gdc_mu"));
        }
//        PengadaanLCtr.view(id);
    }

    @AllowAccess({Group.PP, Group.PANITIA})
    @CacheFor("15s")
    public static void previewHasilNegosiasi(Long id, Tahap jenis, String no, @As(binder= DatetimeBinder.class) Date tanggal
            , String info, String tempat){
        Berita_acara_pl berita_acara = new Berita_acara_pl();
        berita_acara.brt_no = no;
        berita_acara.brt_tgl_evaluasi = tanggal;
        berita_acara.brt_info = info;
        berita_acara.brc_jenis_ba = jenis;
        berita_acara.brt_tempat = tempat;
        Pl_seleksi pl = Pl_seleksi.findById(id);
        renderArgs.put("berita_acara", berita_acara);
        renderArgs.put("pl", pl);
        renderArgs.put("paket", pl.getPaket());
        renderArgs.put("instansi", Instansi.findNamaInstansiPaket(pl.pkt_id));
        renderArgs.put("harga", EvaluasiPl.findHarga(pl.lls_id));
        EvaluasiPl akhir = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
        renderArgs.put("akhir", akhir);
        NilaiEvaluasiPl nilaiPemenang = NilaiEvaluasiPl.find("eva_id=? and nev_lulus = 1", akhir.eva_id).first();
        renderArgs.put("nilaiPemenang", nilaiPemenang);
//        renderArgs.put("peserta",PesertaPl.findById(nilaiPemenang.psr_id));
        renderTemplate("nonlelang/berita_acara/baPlPreviewHasilNegosiasi.html");

    }
    
    @AllowAccess({Group.PP, Group.PANITIA})
    public static void formBa(Long id, Tahap jenis, String aksi) {
    		Berita_acara_pl berita_acara = Berita_acara_pl.findBy(id, jenis);
    		renderArgs.put("id",id);
		renderArgs.put("berita_acara",berita_acara);
		renderArgs.put("jenis",jenis);
		renderArgs.put("today",newDate());
		String labelDokumen = "";
		renderArgs.put("labelDokumen",labelDokumen);
		Map<String, Object> params=new HashMap();
		params.put("id", id);
		String ref = Router.getFullUrl("lelang.LelangCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}
		renderArgs.put("referer",ref);
		if ("upload".equals(aksi)) {
			renderTemplate("nonlelang/berita_acara/upload-ba.html");	
		} else if ("cetak".equals(aksi)) {
			renderTemplate("nonlelang/berita_acara/cetak-ba.html");
		}
    }
    
    @AllowAccess({Group.PP, Group.PANITIA})
	public static void uploadBaSubmit(Long id, @Valid @PdfType File file, String no,
									   @As(binder= DatetimeBinder.class) Date tanggal, 
									   String tempat, String info, String ref, Tahap jenis) {
		otorisasiDataPl(id);
        String namaDokumen = Messages.get("flash.ba");
		if (validation.hasErrors()) {
			flash.error(Messages.get("flash.udyduhd"), namaDokumen);
		} else {
			Berita_acara_pl berita_acara = Berita_acara_pl.findNCreateBy(id, jenis);
			try {
				berita_acara.brt_no = no;
				berita_acara.brt_tgl_evaluasi = tanggal;
				if (tempat != null && !tempat.equalsIgnoreCase("")) {
					berita_acara.brt_tempat = tempat;	
				}
				berita_acara.brt_info = info;
				berita_acara.simpan(file);
				flash.success(Messages.get("flash.updh"), namaDokumen);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Berita Acara");
			}
		}
		redirect(ref);
	}    
}
