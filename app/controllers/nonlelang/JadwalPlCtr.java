package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Anggota_panitia;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.nonlelang.HistoryJadwalPl;
import models.nonlelang.Jadwal_pl;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.TahapanPl;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import models.secman.Group;
import play.Logger;
import play.i18n.Messages;

import java.util.Date;
import java.util.List;

public class JadwalPlCtr extends BasicCtr {

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void list(Long id, Long plCopyId) {
		Active_user active_user = Active_user.current();
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		renderArgs.put("pl", pl);
		renderArgs.put("plCopyId", plCopyId);
		List<Jadwal_pl> jadwalList = Jadwal_pl.findByPl(pl.lls_id);
		if (CommonUtil.isEmpty(jadwalList)) {
				jadwalList = Jadwal_pl.getJadwalkosong(pl.lls_id,pl.getPemilihan(), pl.getPemilihan().isPl());
		}
		int urutStart = 0;
		renderArgs.put("urutStart", urutStart);
		renderArgs.put("jadwalList", jadwalList);
		renderArgs.put("active_user", active_user);
		render("nonlelang/jadwal/jadwal-pl-edit.html");
		
	}
	
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void listNew(Long id, Long plCopyId) {
		Active_user active_user = Active_user.current();
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		renderArgs.put("pl", pl);
		renderArgs.put("plCopyId", plCopyId);
		List<Jadwal_pl> jadwalLists = Jadwal_pl.findByPl(pl.lls_id);
		if (CommonUtil.isEmpty(jadwalLists)) {
				jadwalLists = Jadwal_pl.getJadwalkosongNew(pl.lls_id,pl.getPemilihan(), pl.getPemilihan().isPenunjukanLangsung());
		}
		int urutStart = 0;
		renderArgs.put("urutStart", urutStart);
		renderArgs.put("jadwalList", jadwalLists);
		renderArgs.put("active_user", active_user);
		render("nonlelang/jadwal/jadwal-pl-edit.html");
		
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void simpan(Long id, List<Jadwal_pl> jadwalList, String keterangan) {
		checkAuthenticity();
		if(Active_user.current().isPanitia())
			otorisasiDataPl(id); // check otorisasi data lelang
		if(Active_user.current().isPP())
			otorisasiDataPl(id); // check otorisasi data lelang
//		Paket_pl paket = Paket_pl.findByLelang(id);
		List<Anggota_panitia> anggotaPanitia = null;
		Pl_seleksi lelang = Pl_seleksi.findById(id);

		try {
			boolean harusBerubah = params._contains("harus_berubah");
			if (harusBerubah) {
				validation.min(keterangan.length(), 31).key("keterangan");
				if (validation.hasErrors()) {
					params.flash("keterangan");
					validation.keep();
//					valid = false;
				}
			}
			Jadwal_pl.simpanJadwal(lelang, jadwalList, newDate(), keterangan, 1);
			if (validation.hasErrors()) {
				params.flash();
				validation.keep();
			} else {
				flash.success(Messages.get("flash.djntndr_smpn"));
			}
		} catch (Exception e) {
			Logger.error(e, "Simpan Jadwal gagal");
			flash.error(Messages.get("flash.djntndrggl"));
		}

		if(Active_user.current().isPP() || Active_user.current().isPanitia()){
		/*Ada bug untuk kasus spt ini.
		 * 1. Jadwal telah terisi dengan baik tanpa error
		 * 2. User mengedit jadwal pada baris ke-x dari nilai A menjadi B
		 * 3. Saat disimpan, baris tsb error
		 * 4. yang ditampilkan adalah nilai A; harusnya adalah nilai B (yg error).
		 * 		Ini membuat user bingung; katanya error tapi kok bener (yg lama)
		 *
		 *  Tidak boleh di-render() di sini karena kalau di-refresh berarti submit()
		 *  Jadi harus di-redirect ke edit.
		 *
		 *  Action edit() harus menggunakan jadwalList dari action simpan() BUKAN load dari database.
		 *  Sebenarnya bisa pakai AJAX tapi modifikasi ini jd lebih banyak
		 */
//			Cache.set("jadwalList" + lelang.lls_id + session.getId(), jadwalList, "30s");
			list(id,null);
		}else{
			ubahJadwalLelang(id);
		}
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void ubahJadwalLelang(Long lelangId) {
		if(lelangId != null)
		{
			Pl_seleksi lelang = Pl_seleksi.findById(lelangId);
			if(lelang!=null){
				List<Jadwal_pl> jadwalList = Jadwal_pl.findByLelang(lelangId);
				if(CommonUtil.isEmpty(jadwalList)) {
					jadwalList = Jadwal_pl.getJadwalkosong(lelangId,lelang.getPemilihan(), lelang.getPemilihan().isPl());
				}

				renderArgs.put("lelang", lelang);
				renderArgs.put("jadwalList", jadwalList);
			}
			else if(lelangId!=0){
				flash.error(Messages.get("flash.ntndr_blm_tdftr"));
			}
		}
		render("lelang/jadwal/jadwal-edit-ppe.html");
	}
	
	/**
     * Fungsi {@code tahap} digunakan untuk menampilkan detail tahapan lelang
     * dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
    public static void tahap(Long id) {
    	notFoundIfNull(id);
		renderArgs.put("pl",Pl_seleksi.findById(id));
		renderArgs.put("date",newDate()); // tanggal sekarang
		renderArgs.put("list",TahapanPl.findByLelang(id));
        renderTemplate("/nonlelang/jadwal/tahap.html");
	}

	public static void tahappencatatan(Long id) {
		notFoundIfNull(id);
		renderArgs.put("pl", Non_spk_seleksi.findById(id));
		renderArgs.put("date",newDate()); // tanggal sekarang
		renderArgs.put("list",TahapanPl.findByLelang(id));
		renderTemplate("/nonlelang/jadwal/tahappencatatan.html");
	}

	@AllowAccess({Group.PANITIA, Group.PP})
	public static void copy(Long id, Long plCopyId) {
		checkAuthenticity();
		otorisasiDataPl(id); // check otorisasi data lelang
		Jadwal_pl.copyJadwal(id,plCopyId);
		list(id, null);
	}

	public static void jadwalPl(Long id) {
		Pl_seleksi lls = Pl_seleksi.findById(id);
		Date date = newDate(); // tanggal sekarang
		List<TahapanPl> list = TahapanPl.findByLelang(id);
		render("/nonlelang/jadwal/jadwal-pl.html", lls, list, date);
	}

	/**
	 * Fungsi {@code tahap} digunakan untuk menampilkan detail history tahapan lelang
	 * dalam halaman popup
	 * @param id parameter id lelang yang dilihat
	 */
	public static void history(Long id) {
		renderArgs.put("historyList", HistoryJadwalPl.find("dtj_id=? order by hjd_tanggal_edit desc", id).fetch());
		renderTemplate("nonlelang/jadwal/history.html");
	}

}
