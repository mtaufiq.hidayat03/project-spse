package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.nonlelang.*;
import models.nonlelang.common.TahapNowPl;
import models.rekanan.*;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 2/22/2017.
 */
public class KualifikasiPlCtr extends BasicCtr {

    /**
     * Render halaman pemasukan data kualifikasi Susulan
     *
     * @param id id non tender
     */
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.PP})
    public static void susulan(Long id){
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPl(peserta.lls_id); // check otorisasi data non tender
        renderArgs.put("peserta", peserta);
        Active_user active_user = Active_user.current();
        renderArgs.put("pl", Pl_seleksi.findById(peserta.lls_id));
        DokPenawaranPl dokPenawaran = peserta.getDokSusulan();
        renderArgs.put("allow_download", active_user.isAuditor() || active_user.isPanitia() || active_user.isPP() || peserta.rkn_id == active_user.rekananId);
        if(dokPenawaran != null) {
            renderArgs.put("list", dokPenawaran.getDokumenList());
        }
        renderTemplate("nonlelang/kualifikasi/kualifikasi-susulan-rekanan.html");
    }
    
    // staf ahli peserta
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR, Group.PP})
    public static void staf_ahli(Long id, Long pesertaId) {
        PesertaPl peserta = PesertaPl.findById(pesertaId);
        otorisasiDataPl(peserta.lls_id);
        renderArgs.put("peserta", peserta);
        System.out.println("ada ? " + peserta.lls_id);
        renderArgs.put("staf_ahli_psr", StafAhliPesertaPl.find("stp_id=? and psr_id=?", id, pesertaId).first());
        renderTemplate("nonlelang/kualifikasi/kualifikasi-pl-view-ahli.html");
    }
    
    // pengalaman peserta
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR, Group.PP})
    public static void pengalaman(Long id, Long pesertaId) {
        PesertaPl peserta = PesertaPl.findById(pesertaId);
        otorisasiDataPl(peserta.lls_id);
        renderArgs.put("psr_pengalaman",PengalamanPesertaPl.find("pen_id=? AND psr_id=?", id, pesertaId).first());
        renderTemplate("nonlelang/kualifikasi/kualifikasi-pl-view-pengalaman.html");
    }
    
    // peralatan
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR})
    public static void peralatan(Long id, Long pesertaId) {
        PesertaPl peserta = PesertaPl.findById(pesertaId);
        otorisasiDataLelang(peserta.lls_id);
        renderArgs.put("psr_peralatan",PeralatanPesertaPl.find("id_alat_peserta=? AND psr_id=?", id, pesertaId).first());
        renderTemplate("nonlelang/kualifikasi/kualifikasi-pl-view-peralatan.html");
    }

    public static void kirimSusulan(Long id){
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPesertaPl(peserta);
        renderArgs.put("peserta", peserta);
        Active_user active_user = Active_user.current();
        renderArgs.put("pl", Pl_seleksi.findById(peserta.lls_id));
        renderArgs.put("allow_download", active_user.isAuditor() || active_user.isPanitia() || active_user.isPP() || peserta.rkn_id == active_user.rekananId);
        DokPenawaranPl dokPenawaran = peserta.getDokSusulan();
        boolean editable = peserta.rkn_id == active_user.rekananId;
        if(dokPenawaran != null)
            renderArgs.put("list", dokPenawaran.getDokumenList());
        renderArgs.put("editable", editable);
        renderTemplate("nonlelang/kualifikasi/kualifikasi-kirim-susulan.html");

    }

    @AllowAccess({Group.REKANAN})
    public static void submitSusulan(Long id, File file){
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPesertaPl(peserta);
        Map<String, Object> result = new HashMap<>();
        try{
            List<UploadInfo> files = new ArrayList<>();
            files.add(DokPenawaranPl.simpanSusulan(peserta, file, newDate()));
            result.put("files", files);
        } catch (Exception e){
            Logger.info(e, "Kesalahan saat upload susulan");
        }
        renderJSON(result);
    }



    @AllowAccess({Group.REKANAN})
    public static void kirim(Long id) {
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPlPeserta(peserta);
        Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
        TahapNowPl tahapAktif = pl.getTahapNow();
        if(pl.isPenunjukanLangsungNew()) {
        	boolean allow_kualifikasi = tahapAktif.isKirimPersyaratanKualifikasi();
    		if (!allow_kualifikasi) forbidden(Messages.get("not-authorized")); 
            renderArgs.put("allow_kualifikasi", allow_kualifikasi);
            DukunganBankPl dukunganBank = DukunganBankPl.find("psr_id = ?", peserta.psr_id).first();
            DokPenawaranPl dok_lain = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN);
            DokPenawaranPl dok_lain_draft = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
            render("nonlelang/kualifikasi/kualifikasi-kirim.html", peserta, pl, dukunganBank, dok_lain, dok_lain_draft);
        }
        else {
        	boolean allow_kualifikasi = tahapAktif.isPemasukanPenawaran();
			if (!allow_kualifikasi) forbidden(Messages.get("not-authorized")); 
	        renderArgs.put("allow_kualifikasi", allow_kualifikasi);
	        DukunganBankPl dukunganBank = DukunganBankPl.find("psr_id = ?", peserta.psr_id).first();
	        DokPenawaranPl dok_lain = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN);
	        DokPenawaranPl dok_lain_draft = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
	        render("nonlelang/kualifikasi/kualifikasi-kirim.html", peserta, pl, dukunganBank, dok_lain, dok_lain_draft);
        
        	}
        }

    @AllowAccess({Group.REKANAN})
    public static void simpan(Long id, Long[] ijin,Long[] pajak, DukunganBankPl dukunganBank, File fileDukungan,
                              Long[] staf, Long[] pengalaman, Long[] alat) throws Exception {

        checkAuthenticity();
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPesertaPl(peserta);
        Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
        if(!(rekanan.getBentuk_usaha().iskonsultanIndividu() || !rekanan.getBentuk_usaha().isPerorangan()) && ijin == null)
            flash.error("Data Izin Usaha minimal ada 1 (satu) yang dipilih");
        else {
            String join = StringUtils.join(ijin, ",");
            String joinPajak  = StringUtils.join(pajak, ",");
            List<Ijin_usaha> ijinList = Ijin_usaha.find("ius_id in ("+join+ ')').fetch();
            List<Pajak> pajakList = Pajak.find("pjk_id in ("+joinPajak+ ')').fetch();
            IjinUsahaPesertaPl.simpanIjinUsahaPeserta(ijinList, peserta.psr_id);
            BuktiPajakPl.simpanPajakPeserta(pajakList, peserta.psr_id);
            if(fileDukungan != null || dukunganBank.dkb_id != null)
                DukunganBankPl.simpanDukunganBankPeserta(dukunganBank, peserta.psr_id, fileDukungan);
            LandasanHukumPesertaPl.simpanAkta(peserta.psr_id, peserta.rkn_id);
            String joinStaf  = StringUtils.join(staf, ",");
            List<Staf_ahli> staf_ahliList = Staf_ahli.find("sta_id in ("+joinStaf+ ')').fetch();
            StafAhliPesertaPl.simpanStafAhliPeserta(peserta.psr_id, staf_ahliList);
            String joinPengalaman = StringUtils.join(pengalaman, ",");
            List<Pengalaman> pengalamanList = Pengalaman.find("pgn_id in ("+joinPengalaman+ ')').fetch();
            PengalamanPesertaPl.simpanPengalamanPeserta(pengalamanList, peserta.psr_id);
            String joinAlat = StringUtils.join(alat, ",");
            List<Peralatan> peralatanList = Peralatan.find("alt_id in ("+joinAlat+ ')').fetch();
            PeralatanPesertaPl.simpanPeralatanPeserta(peralatanList, peserta.psr_id);
            DokPenawaranPl.simpanKualifikasi(peserta);
            DokPenawaranPl.simpanKualifikasiLainnya(peserta);
            flash.success(Messages.get("flash.data_ktt"));
        }
        //kirim(id);
        PengadaanLCtr.view(peserta.lls_id);
    }

    @AllowAccess({Group.REKANAN})
    public static void uploadPersyaratanLain(Long id, File file) {
    		PesertaPl peserta = PesertaPl.findById(id);

        otorisasiDataPesertaPl(peserta);

        DokPenawaranPl dok_penawaran = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);

        Map<String, Object> result = new HashMap<String, Object>(1);

        try {

            BlobTable blob = null;

            if(dok_penawaran != null && dok_penawaran.dok_id_attachment != null)
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);
            else
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);

            DokPenawaranPl.simpanKualifikasiLainnyaDraft(peserta.psr_id, blob, newDate());

            List<UploadInfo> files = new ArrayList<UploadInfo>();

            UploadInfo info = UploadInfo.findBy(blob);

            files.add(info);

            result.put("files", files);

        }catch(Exception e) {

            Logger.error("Kesalahan saat upload dokumen persyaratan lainnya: detail %s", e.getMessage());

            e.printStackTrace();
        }

        renderJSON(result);
    }

    @AllowAccess({Group.REKANAN})
    public static void hapusKualifikasiLainnyaDraft(Long id, Integer versi) {
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPesertaPl(peserta);
        DokPenawaranPl dok_penawaran = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
        if(dok_penawaran == null)
            return;
        BlobTable blob = BlobTable.findById(dok_penawaran.dok_id_attachment, versi);
        if(blob != null)
            blob.delete();
    }
    
    @AllowAccess({Group.REKANAN})
    public static void hapusKualifikasiLainnya(Long id, Integer versi) {

        PesertaPl peserta = PesertaPl.findById(id);

        otorisasiDataPesertaPl(peserta);

        DokPenawaranPl dok_penawaran = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN);

        if(dok_penawaran == null)
            return;

        BlobTable blob = BlobTable.findById(dok_penawaran.dok_id_attachment, versi);

        if(blob != null)
            blob.delete();

    }

    @AllowAccess({Group.REKANAN, Group.PP, Group.AUDITOR, Group.PPK, Group.PANITIA})
    public static void preview(Long id) {
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPlPeserta(peserta);
        if(!Active_user.current().isRekanan()) {
            renderArgs.put("rekanan", CommonUtil.fromJson(peserta.psr_identitas, Rekanan.class));
            renderArgs.put("listPengurus",CommonUtil.fromJson(peserta.psr_pengurus, Pengurus[].class));
            renderArgs.put("listPemilik",CommonUtil.fromJson(peserta.psr_pemilik, Pemilik[].class));
        }
        renderArgs.put("listIjinUsaha", IjinUsahaPesertaPl.find("psr_id=?", id).fetch());
        renderArgs.put("dukunganBank", DukunganBankPl.find("psr_id=?", id).first());
        renderArgs.put("aktaList", LandasanHukumPesertaPl.findBy(id));
        renderArgs.put("dok_lain", DokPenawaranPl.findPenawaranPeserta(id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN));
        renderArgs.put("listPajak", BuktiPajakPl.find("psr_id=?", id).fetch());
        renderArgs.put("listStafAhli", StafAhliPesertaPl.find("psr_id=?", id).fetch());
        renderArgs.put("listPengalaman", PengalamanPesertaPl.find("psr_id=?", id).fetch());
        renderArgs.put("listPeralatan", PeralatanPesertaPl.find("psr_id=?", id).fetch());
        DokPenawaranPl dokPenawaran = peserta.getDokSusulan();
        if(dokPenawaran != null) {
            renderArgs.put("dok_susulan", dokPenawaran.getDokumenList());
        }
        renderTemplate("nonlelang/kualifikasi/kualifikasi-pl-all.html");
    }

}
