package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.common.Tahap;
import models.common.TahapNow;
import models.nonlelang.EvaluasiPl;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.common.TahapNowPl;
import models.nonlelang.NilaiEvaluasiPl;
import models.lelang.UndangaPeserta;
import models.secman.Group;
import play.Logger;
import play.i18n.Messages;

/**
 * Controller terkait undangan di tender
 * @author Arief Ardiyansah
 */
public class UndanganPlCtr extends BasicCtr {

    /**
     * aktivitas pengumuman pemenang hak akses Panitia dan peserta
     *
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void undangan(Long id) {
        otorisasiDataPl(id); // check otorisasi data lelang
        Pl_seleksi lelang = Pl_seleksi.findById(id);
        notFoundIfNull(lelang);
        TahapNowPl tahapNow = lelang.getTahapNow();
        boolean undanganPra = tahapNow.isUndanganPra();
        boolean adaPemenang = lelang.getPemilihan().isPenunjukanLangsung() ? lelang.hasPemenangTerverifikasi(): NilaiEvaluasiPl.countLulus(lelang.lls_id, (undanganPra ? EvaluasiPl.JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI : EvaluasiPl.JenisEvaluasi.EVALUASI_AKHIR)) > 0;
        
        int jumlahEmail = MailQueueDao.countByJenis(lelang.lls_id, (undanganPra ? JenisEmail.PENGUMUMAN_PRA.id : JenisEmail.PENGUMUMAN_PEMENANG.id));
        EvaluasiPl evaluasi = undanganPra ? EvaluasiPl.findPembuktian(id):EvaluasiPl.findPenetapanPemenang(id);
        Integer versiEvaluasi = evaluasi != null ? evaluasi.eva_versi:Integer.valueOf(1);
        boolean sudahKirim = jumlahEmail == (lelang.getJumlahPeserta() * versiEvaluasi);
        Tahap tahap = undanganPra ? Tahap.PENGUMUMAN_HASIL_PRA : Tahap.PENGUMUMAN_PEMENANG_AKHIR;
        renderArgs.put("pesertaList", UndangaPeserta.findAll(lelang.lls_id, tahap, lelang.isLelangV3()));
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahap", tahap);
        renderArgs.put("undanganPra", undanganPra);
        renderArgs.put("adaPemenang", adaPemenang);
        renderArgs.put("sudahKirim", sudahKirim);
        renderTemplate("lelang/lelang-undangan.html");
    }



    /**
     * aktivitas pengumuman pemenang hak akses Panitia dan peserta
     *
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void kirimUndangan(Long id) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data lelang
        Pl_seleksi lelang = Pl_seleksi.findById(id);
        TahapNowPl tahapNow = lelang.getTahapNow();
        if (tahapNow.isUndanganPra() || tahapNow.isPengumuman_Pemenang()) {
            try {
                if (tahapNow.isUndanganPra()) {
                    MailQueueDao.kirimPengumumanPrakualifikasiPl(lelang);
                    flash.success(Messages.get("flash.upempratdk"));
                } else {
                    MailQueueDao.kirimPengumumanPemenangPl(lelang);
                    flash.success(Messages.get("flash.ifpptk"));
                }
            } catch (Exception e) {
                Logger.error(e, Messages.get("flash.kslhkru"));
                flash.error(Messages.get("flash.mtkslhtknsp"));
            }
        } else {
            flash.error(Messages.get("flash.mwaupth"));
        }
        PengadaanLCtr.view(id);
    }

}
