package controllers.nonlelang;

import java.util.stream.Collectors;
import ext.DateBinder;
import ext.DatetimeBinder;
import ext.PdfType;

import controllers.BasicCtr;
import controllers.lelang.DokumenCtr;
import controllers.lelang.LelangCtr;
import controllers.security.AllowAccess;
import models.agency.DaftarKuantitas;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.TempFileManager;
import models.lelang.Checklist;
import models.lelang.Checklist_master;
import models.lelang.Checklist_tmp;
import models.lelang.Dok_lelang;
import models.lelang.Dok_lelang_content;
import models.lelang.Evaluasi;
import models.lelang.LDPContent;
import models.lelang.Lelang_seleksi;
import models.lelang.Nilai_evaluasi;
import models.lelang.Peserta;
import models.lelang.Checklist_master.JenisChecklist;
import models.nonlelang.ChecklistPl;
import models.nonlelang.ChecklistPl_tmp;
import models.nonlelang.Dok_pl;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.nonlelang.Dok_pl_content;
import models.nonlelang.NilaiEvaluasiPl;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.nonlelang.common.TahapStartedPl;
import models.secman.Group;
import play.Logger;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.templates.Template;
import utils.HtmlUtil;
import utils.JsonUtil;

import java.io.File;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import com.google.gson.reflect.TypeToken;

/**
 *
 * Controller terkait proses Adendum
 */
public class AdendumPlCtr extends BasicCtr {
	
	@AllowAccess({Group.PANITIA, Group.PP})
    public static void cetak(Long id, String nomorSDP, @As(binder= DatetimeBinder.class) Date tglSDP) throws Exception {
        checkAuthenticity();
        Dok_pl dok_pl = Dok_pl.findById(id);
        Pl_seleksi pl = Pl_seleksi.findById(dok_pl.lls_id);
        otorisasiDataPl(pl); // check otorisasi data lelang
        if(pl.lls_status.isDraft()) {
            // cek kelengkapan dokumen dilakukan saat lelang masih draft
            boolean express  = pl.getPemilihan().isLelangExpress();
            Kategori kategori = pl.getKategori();
            StringBuilder complete = new StringBuilder();
            if (dok_pl != null && dok_pl.dll_id != null) {
                Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
                if(!express && (pl.isPenunjukanLangsungNew() || dok_pl.dll_jenis.isDokKualifikasi() || dok_pl.dll_jenis.isAdendumPra())) {
                    List<ChecklistPl> ijinList = dok_pl.getSyaratIjinUsaha();
                    List<ChecklistPl> syaratList = dok_pl.getSyaratKualifikasi();
                    if (CommonUtil.isEmpty(ijinList)) // empty ijin usaha
                        complete.append("- Persyaratan Kualifikasi (Izin Usaha).\n ");
                    if (CommonUtil.isEmpty(syaratList))
                        complete.append("- Persyaratan Kualifikasi (Syarat).\n ");
                }
                if (dok_pl.dll_jenis.isDokLelang() || dok_pl.dll_jenis.isAdendum()) {
                    if(!express) {
                        if (CommonUtil.isEmpty(dok_pl.getSyaratTeknis()))
                            complete.append("- Persyaratan Dokumen Teknis.\n ");
                        if (kategori.isKonstruksi() && CommonUtil.isEmpty(dok_pl.getSyaratHarga()))
                            complete.append("- Persyaratan Dokumen Harga.\n ");
                    }
                    if(CommonUtil.isEmpty(dok_pl_content.dll_ldp))
                        complete.append("- Lembar Data Pemilihan.\n ");
                    if(CommonUtil.isEmpty(dok_pl_content.dll_dkh))
                        complete.append("- Rincian HPS.\n ");
                    if(CommonUtil.isEmpty(dok_pl_content.dll_sskk))
                        complete.append("- Syarat-Syarat Khusus Kontrak (SSKK).\n ");
                    if(dok_pl_content.dll_spek == null)
                        complete.append("- Spesifikasi Teknis dan Gambar.\n ");
                }
            }
            if (complete.length() > 0)
                flash.error("Kelengkapan Yang Belum Disimpan: " + complete);
        }
        // jika ada error  tidak perlu disimpan
        if(!flash.contains("error")){
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            if(dok_pl_content == null)
                return;
            else if(dok_pl_content.dll_modified) {
            	dok_pl_content.dll_nomorSDP = nomorSDP;
            	dok_pl_content.dll_tglSDP = tglSDP;

                StopWatch sw = new StopWatch();
                sw.start();
                Template template = null;
                if(dok_pl.dll_jenis.isDokKualifikasi()) {
                    template = DokumenPlCtr.TEMPLATE_DOK_KUALIFIKASI;
                }else { // untuk dokumen pengadaan/pemilihan
                    template = DokumenPlCtr.TEMPLATE_DOK_PENGADAAN ;
                }
                String content = Dok_pl.cetak_content(pl, dok_pl, dok_pl_content, template);
                if (!StringUtils.isEmpty(content)) {
                	dok_pl_content.dll_content_html = content.replace("<br>", "<br/>");
                    InputStream dok = HtmlUtil.generatePDF(template.name+"-"+id, "#{extends 'borderTemplate.html' /} "+dok_pl_content.dll_content_html);
                    // simpan ke dok_lelang
                    sw.stop();
                    if (dok == null) {
                        dok = IOUtils.toInputStream(dok_pl_content.dll_content_html, "UTF-8");
                        File file = TempFileManager.createFileInTemporaryFolder("InvalidDocLelangTemplate-lls_id#" + pl.lls_id + ".html");
                        FileUtils.copyInputStreamToFile(dok, file);
                        Logger.error("Gagal generate PDF untuk lls_id %s. Template dokumen yang error disimpan di: %s", pl.lls_id, file);
                    } else {
                        Logger.debug("Generate PDF document: %s, , duration: %s", dok_pl.dll_nama_dokumen, sw);
                    }
                    dok_pl.simpanCetak(dok_pl_content, dok);
                }

                // kirim email notifikasi jika adendum
                if(dok_pl_content.dll_versi > 1){
                    TahapStartedPl tahapStarted = pl.getTahapStarted();
                    List<PesertaPl> pesertaList = pl.getPesertaList();
                    if (pl.isPenunjukanLangsungNew()) {
                        Evaluasi pembuktian = Evaluasi.findPembuktian(pl.lls_id);
                        boolean allow_pengumuman_pra = pembuktian != null && pembuktian.eva_status.isSelesai() && tahapStarted.isPengumumanPemenangPra();
                        if (allow_pengumuman_pra) {
                            // hanya kirim ke pemenang prakualifikasi
                            pesertaList = NilaiEvaluasiPl.findPemenangPrakualifikasi(pl.lls_id);
                        }
                    }

                    if(pesertaList != null) {
                        try {
                            MailQueueDao.kirimNotifikasiAdendumPl(pl, pesertaList);
                        } catch (Exception ex) {
                            Logger.error("Gagal kirim notifikasi adendum untuk lls_id %s.", pl.lls_id);
                        }
                    }
                }
            }
        }
        PengadaanLCtr.view(dok_pl.lls_id);
    }

    @AllowAccess({Group.PANITIA, Group.PP})
    public static void masaBerlakuPenawaran(Long id) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data non tender
        renderArgs.put("id", id); // kode non tender
        Dok_pl dok_pl = Dok_pl.findBy(id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        renderArgs.put("editable", dok_pl.isAllowAdendum(pl));
        if (dok_pl != null) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            if(dok_pl_content != null && dok_pl_content.ldpcontent != null)
                renderArgs.put("masaberlaku", dok_pl_content.ldpcontent.masa_berlaku_penawaran);
        }
        renderTemplate("nonlelang/dokumen/masa-berlaku-penawaran-adendum.html");
    }

    @AllowAccess({Group.PANITIA, Group.PP})
    public static void masaBerlakuPenawaranSubmit(Long id, Integer masaberlaku) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data non tender
        Dok_pl dok_pl = Dok_pl.findNCreateBy(id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        boolean editable = dok_pl.isAllowAdendum(pl);
        if(!editable){
            forbidden(Messages.get("flash.atbmpd"));
        }else{
            Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
            if(dok_pl_content != null) {
                LDPContent ldpContent = dok_pl_content.ldpcontent != null ? dok_pl_content.ldpcontent:new LDPContent();
                ldpContent.masa_berlaku_penawaran = masaberlaku;
                dok_pl_content.ldpcontent = ldpContent;
                dok_pl_content.dll_modified = true;
                dok_pl_content.save();
            }
        }
        PengadaanLCtr.view(id);
    }
    
    /**
     * form Checklist Persyaratan
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void checklist(Long id) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang        
        Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        renderArgs.put("pl", pl);
        renderArgs.put("dok_pl", dok_pl);
        renderArgs.put("editable", dok_pl.isAllowAdendum(pl));
        Kategori kategori = pl.getKategori();
        renderArgs.put("kategori", kategori);
        ChecklistPl_tmp.copyChecklist(dok_pl, JenisChecklist.CHECKLIST_SYARAT);
        List<ChecklistPl_tmp> syaratAdmin = ChecklistPl_tmp
                .getListSyarat(dok_pl, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI, kategori)
                .stream()
                .sorted(Comparator.comparing(ChecklistPl_tmp::getSortingRule))
                .collect(Collectors.toList());
        renderArgs.put("syaratAdmin",syaratAdmin);
        final boolean isConsultant = kategori.isConsultant();
        List<ChecklistPl_tmp> syaratTeknis = ChecklistPl_tmp
                .getListSyarat(dok_pl, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS, kategori)
                .stream()
                .filter(c -> isConsultant == c.getChecklist_master().isConsultant() || c.getChecklist_master().isSyaratLain())
                .sorted(Comparator.comparing(ChecklistPl_tmp::getSortingRule))
                .collect(Collectors.toList());
        renderArgs.put("syaratTeknis", syaratTeknis);
        renderArgs.put("syaratHarga", ChecklistPl_tmp.getListSyarat(dok_pl, Checklist_master.JenisChecklist.CHECKLIST_HARGA, kategori));
        if (dok_pl != null) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            if (dok_pl_content != null) {
                renderArgs.put("adendum", dok_pl_content.isAdendum());
            }
        }
        renderTemplate("nonlelang/dokumen/form-checklist-penawaran-adendum.html");
    }
    
    /**
     * Simpan Form Checklist Persyaratan
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void checklistSubmit(Long id,  List<ChecklistPl_tmp> syarat, List<ChecklistPl_tmp> syaratAdmin, List<ChecklistPl_tmp> syaratHarga) {
        checkAuthenticity();
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang
        Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        boolean editable = dok_pl.isAllowAdendum(pl);
        if (!editable)
            forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
        try {
            boolean emptyTeknis = true;
            boolean emptyHarga = true;
            // do validation first
            if (syarat != null) {
                for (int i = 0; i < syarat.size(); i++) {
                    if (syarat.get(i).ckm_id != null) {
                        emptyTeknis= false;
                        validation.valid(syarat.get(i));
                    }
                }
            }
            if (syaratHarga != null) {
                for (int i = 0; i < syaratHarga.size(); i++) {
                    if (syaratHarga.get(i).ckm_id != null) {
                        emptyHarga = false;
                        validation.valid(syaratHarga.get(i));
                    }
                }
            }

            if(!validation.hasErrors()) {
                if (emptyTeknis)
                    flash.error("Persyaratan Teknis pada Persyaratan Dokumen Penawaran wajib diisi!");
                else if (pl.getKategori().isKonstruksi() && emptyHarga)
                    flash.error("Persyaratan Harga pada Persyaratan Dokumen Penawaran wajib diisi!");
                else {
                    // do the save
                    if (!emptyTeknis)
                    	ChecklistPl_tmp.simpanChecklist(dok_pl, syarat, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS); // simpan checklist penawaran

                    if (syaratAdmin != null)
                    	ChecklistPl_tmp.simpanChecklist(dok_pl, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI); // simpan checklist penawaran

                    ChecklistPl_tmp.simpanChecklist(dok_pl, syaratHarga, Checklist_master.JenisChecklist.CHECKLIST_HARGA); // simpan checklist penawaran

                    Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateByForSyarat(dok_pl.dll_id, pl);
                    dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);

                    flash.success("Persyaratan Dokumen telah tersimpan");
                }
            } else {
                StringBuilder sb = new StringBuilder();
                for(play.data.validation.Error err:validation.errors()) {
                    if(!StringUtils.isEmpty(err.getKey())) {
                        sb.append("<br/>").append(err.message());
                    }
                }
                flash.error("Persyaratan Dokumen gagal tersimpan:"+ sb);
            }
        }catch (Exception e) {
            Logger.error(e, "Gagal Simpan Persyaratan Dokumen");
            flash.error("Persyaratan Dokumen gagal tersimpan");
        }
        checklist(id);
    }
    
    @AllowAccess({Group.PP, Group.PANITIA})
	public static void ldk(Long id) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data nonlelang
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		Kategori kategori = pl.getKategori();
		renderArgs.put("pl", pl);
		renderArgs.put("kategori", kategori);
		JenisDokPl jenis = pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA:JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, jenis);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
		boolean isSyaratKualifikasiBaru = dok_pl.isSyaratKualifikasiBaru();
		ChecklistPl_tmp.copyChecklist(dok_pl, JenisChecklist.CHECKLIST_LDK);
		renderArgs.put("isNew", ChecklistPl.isChecklistEmpty(dok_pl.dll_id, JenisChecklist.CHECKLIST_LDK)); ///check apakah sudah terdapat checklist di tender ini atau tidak		
		if(!kategori.isKonsultansiPerorangan()) {
			List<ChecklistPl_tmp> ijinList = isSyaratKualifikasiBaru ? 
					(flash.get("flashError") != null ? Cache.get("ijinList_"+dok_pl.dll_id, List.class) : ChecklistPl_tmp.getListIjinUsahaBaru(dok_pl))
					: ChecklistPl_tmp.getListIjinUsaha(dok_pl);
			if (flash.get("flashError") != null)
				Cache.safeDelete("ijinList_"+dok_pl.dll_id);
			if (CommonUtil.isEmpty(ijinList)) { // dokumen lelang belum ada
				ijinList = new ArrayList<ChecklistPl_tmp>(1);
				ChecklistPl_tmp checklist = new ChecklistPl_tmp();
				checklist.ckm_id = isSyaratKualifikasiBaru ? 50 : 1;
				ijinList.add(checklist);
			}
			renderArgs.put("ijinList", ijinList);
		}
		
		if (isSyaratKualifikasiBaru) {
	 		List<ChecklistPl_tmp> syaratAdmin = new ArrayList<ChecklistPl_tmp>();
	 		List<ChecklistPl_tmp> syaratTeknis = new ArrayList<ChecklistPl_tmp>();
	 		List<ChecklistPl_tmp> syaratKeuangan = new ArrayList<ChecklistPl_tmp>();    	 		
	 		if (flash.get("flashError") != null) {
	 			syaratAdmin = Cache.get("syaratAdmin_"+dok_pl.dll_id, List.class);
    	 		syaratTeknis = Cache.get("syaratTeknis_"+dok_pl.dll_id, List.class);
    	 		syaratKeuangan = Cache.get("syaratKeuangan_"+dok_pl.dll_id, List.class);
    	 		Cache.safeDelete("syaratAdmin_"+dok_pl.dll_id);
    	 		Cache.safeDelete("syaratTeknis_"+dok_pl.dll_id);
    	 		Cache.safeDelete("syaratKeuangan_"+dok_pl.dll_id);
	 		} else {
	 			syaratAdmin = ChecklistPl_tmp.getListSyaratKualifikasi(dok_pl, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI);
    	 		syaratTeknis = ChecklistPl_tmp.getListSyaratKualifikasi(dok_pl, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS);
    	 		syaratKeuangan = ChecklistPl_tmp.getListSyaratKualifikasi(dok_pl, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN);
	 		}
	 		renderArgs.put("syaratAdmin", syaratAdmin);
	 		renderArgs.put("syaratTeknis", syaratTeknis);
	 		renderArgs.put("syaratKeuangan", syaratKeuangan);
	 		renderTemplate("nonlelang/dokumen/form-ldk-adendum-baru.html");
	} else {
		renderArgs.put("syaratList", ChecklistPl_tmp.getListSyaratKualifikasi(dok_pl, kategori));
		renderTemplate("nonlelang/dokumen/form-ldk-adendum.html");
	}
	}
    
    @AllowAccess({Group.PANITIA, Group.PP})
	public static void ldkSubmitBaru(Long id, List<ChecklistPl_tmp> ijin, List<ChecklistPl_tmp> syaratAdmin, List<ChecklistPl_tmp> syaratTeknis, List<ChecklistPl_tmp> syaratKeuangan) throws Exception {
		checkAuthenticity();
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data lelang
		Kategori kategori = pl.getKategori();
        JenisDokPl jenis = pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA:JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, jenis);
        Kualifikasi kualifikasi = pl.getPaket().getKualifikasi();
		boolean editable = dok_pl.isAllowAdendum(pl);
		if(!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			ChecklistPl_tmp.simpanChecklist(dok_pl, ijin, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, kualifikasi);
			ChecklistPl_tmp.simpanChecklist(dok_pl, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, kualifikasi);
			ChecklistPl_tmp.simpanChecklist(dok_pl, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, kualifikasi);
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateByForLdk(dok_pl.dll_id, pl.lls_status);
			dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
			flash.success("Persyaratan Kualifikasi berhasil tersimpan");
		} catch (Exception e) {
			if(!kategori.isKonsultansiPerorangan())
				Cache.set("ijinList_"+dok_pl.dll_id, ChecklistPl_tmp.getListIjinUsahaBaruFlash(dok_pl, ijin));
				Cache.set("syaratAdmin_"+dok_pl.dll_id, ChecklistPl_tmp.getListSyaratKualifikasiFlash(dok_pl, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, kualifikasi));
				Cache.set("syaratTeknis_"+dok_pl.dll_id, ChecklistPl_tmp.getListSyaratKualifikasiFlash(dok_pl, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, kualifikasi));
				Cache.set("syaratKeuangan_"+dok_pl.dll_id, ChecklistPl_tmp.getListSyaratKualifikasiFlash(dok_pl, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, kualifikasi));
				flash.put("flashError", true);
				flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
				Logger.error(e, e.getMessage());
		}
		ldk(id);
	}
    
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void uploadDokKualifikasi(Long id) {
        Dok_pl dok_pl = Dok_pl.findById(id);
        renderArgs.put("id", dok_pl.lls_id);
        renderArgs.put("dokId", id);
        renderArgs.put("dok_lelang", dok_pl);
        renderArgs.put("dok_pl_content", Dok_pl_content.findBy(dok_pl.dll_id));
        renderArgs.put("today", newDate());
        renderTemplate("nonlelang/dokumen/upload-dok-kualifikasi-adendum.html");
    }
    
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void dokKualifikasiSubmit(Long id, @Valid @PdfType File file, String nomorSDP, @As(binder= DateBinder.class) Date tglSDP) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data lelang
        if (validation.hasErrors()) {
            String namaDokumen = pl.isPenunjukanLangsungNew() ? "Tender/Seleksi" : "Pemilihan";
            flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
        } else {
            Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG_PRA);
            Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
            try {
            	if (dok_pl_content.dll_ldk_updated)
	            	ChecklistPl.copyChecklist(dok_pl, JenisChecklist.CHECKLIST_LDK);
	            	dok_pl_content.dll_nomorSDP = nomorSDP;
	            	dok_pl_content.dll_tglSDP = tglSDP;
	            	dok_pl.simpan(dok_pl_content, pl, file);
	                flash.success("Upload Dokumen Kualifikasi berhasil.");
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Dokumen Kualifikasi");
            }
        }
        PengadaanLCtr.view(id);
    }

    @AllowAccess({Group.PPK, Group.PANITIA, Group.KUPPBJ, Group.PP})
    public static void hps(Long id){
        Pl_seleksi pl = Pl_seleksi.findById(id);
        renderArgs.put("pl", pl);
        boolean fixed = true;
        String data = "[]";
        DaftarKuantitas dk;
        boolean editable;
        Active_user activeUser = Active_user.current();
        Dok_pl dok_pl = pl.getDokumenLelang();
        Dok_pl_content dok_pl_content = dok_pl.getDokumenLelangContent();
        dk = dok_pl_content.dkh;
        if(dk == null) {
            dk = dok_pl.getRincianHPS();
        }
        editable = pl.getTahapStarted().isAllowAdendum() && activeUser.isPpk();
        if(dk != null){
            fixed = dk.fixed;
            if (dk.items != null) {
                data = CommonUtil.toJson(dk.items);
            }
        }
        renderArgs.put("data", data);
        renderArgs.put("editable", editable);
        renderArgs.put("fixed", fixed);
        renderArgs.put("enableViewHps", pl.getPaket().isEnableViewHps());
        renderArgs.put("draft", pl.lls_status.isDraft());
        renderTemplate("nonlelang/dokumen/form-pl-hps-adendum.html");
    }
    
    @AllowAccess({Group.PPK})
    public static void hpsSubmit(Long id, String data, boolean fixed, boolean enableViewHps) {
        checkAuthenticity();
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang
        Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        boolean editable = dok_pl.isAllowAdendum(pl);
        Map<String, Object> result = new HashMap<>(1);
        if(!editable)
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        else {
            try {
            	dok_pl.simpanHps(pl,  data, fixed, enableViewHps);
                result.put("result", "ok");
            } catch (Exception e) {
                Logger.error("Gagal Menyimpan HPS : %s", e.getLocalizedMessage());
                result.put("result", e.getLocalizedMessage());
            }
        }
        renderJSON(result);
    }
    
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void uploadDokNonTender(Long id) {
    	Pl_seleksi pl = Pl_seleksi.findById(id);
        Dok_pl dok_pl = Dok_pl.findById(id);
        renderArgs.put("id", dok_pl.lls_id);
        renderArgs.put("dokId", id);
        renderArgs.put("dok_pl", dok_pl);
        renderArgs.put("dok_pl_content", Dok_pl_content.findBy(dok_pl.dll_id));
        renderArgs.put("today", newDate());

        // Dokumen Tender/Seleksi adalah label untuk Tender Prakualifikasi
        // Dokumen Pemilihan adalah label untuk Tender Pascakualifikasi
        String labelDokumen = "Tender/Seleksi";
        if(!(dok_pl.getPl_seleksi().isPenunjukanLangsungNew())){
            labelDokumen = "Pemilihan";
        }
        renderArgs.put("labelDokumen", labelDokumen);
        renderTemplate("nonlelang/dokumen/upload-dok-pemilihan-adendum.html");
    }
    
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void doknontenderSubmit(Long id, @Valid @PdfType File file, String nomorSDP, @As(binder= DateBinder.class) Date tglSDP) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang
        String namaDokumen = pl.isPenunjukanLangsungNew() ? "Tender/Seleksi" : "Pemilihan";
        if (validation.hasErrors()) {
            flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
        } else {
            Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
            Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
            try {
            		if (dok_pl_content.dll_ldk_updated)
            			ChecklistPl.copyChecklist(dok_pl, JenisChecklist.CHECKLIST_LDK);
            		if (dok_pl_content.dll_syarat_updated)
            			ChecklistPl.copyChecklist(dok_pl, JenisChecklist.CHECKLIST_SYARAT);
            		// cetak konten html
            		dok_pl_content.dll_nomorSDP = nomorSDP;
            		dok_pl_content.dll_tglSDP = tglSDP;
            		dok_pl.simpan(dok_pl_content, pl, file);
                flash.success("Upload Dokumen %s berhasil.", namaDokumen);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Dokumen Tender/Seleksi");
            }
        }
        PengadaanLCtr.view(id);
    }
    
    @AllowAccess({Group.PANITIA,Group.KUPPBJ, Group.PPK, Group.PP})
    public static void spek(Long id) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang
        Kategori kategori = pl.getKategori();
        renderArgs.put("pl", pl);
        renderArgs.put("kategori", kategori);
        Active_user activeUser = Active_user.current();
        Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        renderArgs.put("editable", pl.getTahapStarted().isAllowAdendum() && activeUser.isPpk());
        if(dok_pl != null) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            if(dok_pl_content != null) {
                renderArgs.put("spek",dok_pl_content.getDokSpek());
                renderArgs.put("adendum", dok_pl_content.isAdendum());
            }
        }
        renderArgs.put("isPanitia",activeUser.isPanitia());
        renderTemplate("nonlelang/dokumen/form-pl-spek-adendum.html");
    }
    
    @AllowAccess({Group.PPK})
    public static void spekSubmit(Long id, File file) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang
        Map<String, Object> result = new HashMap<>(1);
        Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id,	Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
        boolean editable = dok_pl.isAllowAdendum(pl);
        if(!editable)
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        else {
            try {
                if (dok_pl_content.isAdendum() && dok_pl_content.dll_spek == null) {
                    List<BlobTable> blobTableList = dok_pl_content.getDokSpek();
                    dok_pl_content.dll_spek = dok_pl_content.simpanBlobSpek(blobTableList);
                }
                UploadInfo model = dok_pl_content.simpanSpek(file);
                if (model == null) {
                    result.put("success", false);
                    result.put("message", "File dengan nama yang sama telah ada di server!");
                    renderJSON(result);
                }
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload spek");
                result.put("success", false);
                result.put("result", "Kesalahan saat upload spek");
            }
        }
        renderJSON(result);
    }
    
    @AllowAccess({Group.PPK })
    public static void hapusSpek(Long id, Integer versi) {
        otorisasiDataPl(id); // check otorisasi data nonlelang
        Pl_seleksi pl = Pl_seleksi.findById(id);
        Dok_pl dok_pl = Dok_pl.findBy(id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        Map<String, Object> result = new HashMap<String, Object>(1);
        if(dok_pl.isAllowAdendum(pl)) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
            if (dok_pl_content.isAdendum() && dok_pl_content.dll_spek == null) {
                List<BlobTable> blobList = dok_pl_content.getDokSpek();
                blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                try {
                    dok_pl_content.dll_spek = dok_pl_content.simpanBlobSpek(blobList);
                    result.put("success", true);
                    dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
                } catch (Exception e) {
                    Logger.error(e, "File Anda tidak dapat dihapus");
                    result.put("success", false);
                    result.put("message", "File Anda tidak dapat dihapus");
                }
            }  else {
                BlobTable blob = BlobTable.findById(dok_pl_content.dll_spek, versi);
                if (blob != null)
                    blob.delete();
                dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
            }
        }else {
            result.put("success",false);
            result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }
    
    @AllowAccess({Group.PANITIA,Group.KUPPBJ, Group.PPK, Group.PP})
    public static void docSskk(Long id) {
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl); // check otorisasi data nonlelang
        TahapStartedPl tahapStarted = pl.getTahapStarted();
        Kategori kategori = pl.getKategori();
        renderArgs.put("pl", pl);
        renderArgs.put("kategori", kategori);
        renderArgs.put("isConsultant", kategori.isConsultant() || kategori.isJkKonstruksi());
        Active_user activeUser = Active_user.current();
        Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        renderArgs.put("editable", tahapStarted.isAllowAdendum() && activeUser.isPpk());
        if(dok_pl != null) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            if(dok_pl_content != null) {
            	renderArgs.put("sskk", dok_pl_content.getDokSskk());
                renderArgs.put("adendum", dok_pl_content.isAdendum());
            }
        }
        renderTemplate("nonlelang/dokumen/form-upload-sskk-adendum.html");
    }
    
    @AllowAccess({Group.PPK})
    public static void docSskkSubmit(Long id, File file){
        Pl_seleksi pl = Pl_seleksi.findById(id);
        otorisasiDataPl(pl);
        Map<String, Object> result = new HashMap<>(1);
        Dok_pl dok_pl = pl.getDokumenLelang();
        if (dok_pl != null && pl.getTahapStarted().isAllowAdendum()) {
            try {
                Dok_pl_content dok_pl_content = dok_pl.getDokumenLelangContent();
                if (dok_pl_content.isAdendum() && dok_pl_content.dll_sskk_attachment == null) {
                    List<BlobTable> blobTableList = dok_pl_content.getDokSskk();
                    dok_pl_content.dll_sskk_attachment = dok_pl_content.simpanBlobSpek(blobTableList);
                    dok_pl_content.dll_modified = true;
                }
                UploadInfo model = dok_pl_content.simpanSskkAttachment(file);
                if (model == null) {
                    result.put("success", false);
                    result.put("message", "File dengan nama yang sama telah ada di server!");
                    renderJSON(result);
                }
                List<UploadInfo> files = new ArrayList<UploadInfo>();
                files.add(model);
                dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, e.getLocalizedMessage());
                result.put("result", "Kesalahan saat upload sskk");
            }
        }else{
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }
    
    @AllowAccess({ Group.PPK })
    public static void hapusSskk(Long id, Integer versi) {
        otorisasiDataPl(id); // check otorisasi data nonlelang
        Map<String, Object> result = new HashMap<String, Object>(1);
        Pl_seleksi pl = Pl_seleksi.findById(id);
        Dok_pl dok_pl = Dok_pl.findBy(id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        if(dok_pl.isAllowAdendum(pl)) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
            if (dok_pl_content != null) {
                if (dok_pl_content.isAdendum() && dok_pl_content.dll_sskk_attachment == null) {
                    List<BlobTable> blobList = dok_pl_content.getDokSskk();
                    blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                    try {
                        dok_pl_content.dll_sskk_attachment = dok_pl_content.simpanBlobSpek(blobList);
                        result.put("success", true);
                        dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
                    } catch (Exception e) {
                        Logger.error(e, "File Anda tidak dapat dihapus");
                        result.put("success", false);
                        result.put("message", "File Anda tidak dapat dihapus");
                    }

                }  else {
                    BlobTable blob = BlobTable.findById(dok_pl_content.dll_sskk_attachment, versi);
                    if (blob != null)
                        blob.delete();
                    dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
                }
            }
        }else {
            result.put("success",false);
            result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }
}
