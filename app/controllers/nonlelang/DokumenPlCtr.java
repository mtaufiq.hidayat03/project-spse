package controllers.nonlelang;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.*;
import models.agency.DaftarKuantitas;
import models.agency.DokPersiapanPl;
import models.agency.Paket_pl;
import models.agency.Rincian_hps;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.TempFileManager;
import models.lelang.Checklist_master;
import models.lelang.Checklist_master.JenisChecklist;
import models.lelang.LDPContent;
import models.lelang.SskkContent;
import models.nonlelang.*;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.nonlelang.common.TahapNowPl;
import models.nonlelang.common.TahapStartedPl;
import models.nonlelang.form.PenawaranDataForm;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.LocalDate;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class DokumenPlCtr extends BasicCtr {

	public static final Template TEMPLATE_EVALUASI_TEKNIS_PL = TemplateLoader.load("/nonlelang/dokumen/template/evaluasiTeknisPL.html");
	public static final Template TEMPLATE_DOK_KUALIFIKASI = TemplateLoader.load("/nonlelang/dokumen/template/dok-kualifikasi-pl.html");
	public static final Template TEMPLATE_DOK_PENGADAAN =TemplateLoader.load("/nonlelang/dokumen/template/dok-pengadaan-pl.html");


	@AllowAccess({ Group.PP, Group.AUDITOR, Group.REKANAN, Group.PPK, Group.PANITIA })
	public static void preview(Long id, Integer versi) {
		if (versi == null)
			versi = Integer.valueOf(1);
		Dok_pl dok_pl = Dok_pl.findById(id);
		otorisasiDataPl(dok_pl.lls_id); // check otorisasi data lelang
		renderArgs.put("dok_pl", dok_pl);
		Pl_seleksi pl = Pl_seleksi.findById(dok_pl.lls_id);
		Dok_pl_content content = Dok_pl_content.findBy(dok_pl.dll_id, versi);
		renderArgs.put("pp", pl.getPp());
		renderArgs.put("server", getRequestHost());
		renderArgs.put("content", content != null ? content.dll_content_html : "");
		renderArgs.put("dokContent", content);
		renderArgs.put("pemilihan", pl.getPemilihan());
		renderArgs.put("is43", pl.isLelangV43());
		renderTemplate("nonlelang/dokumen/dok_pl.html");
	}

	/**
	 * form LDP
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void ldp(Long id){
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		Paket_pl paket = Paket_pl.findById(pl.pkt_id);
		renderArgs.put("pl", pl);
		renderArgs.put("paket", paket);
		renderArgs.put("kategori", pl.getKategori());
		renderArgs.put("pp", pl.getPp());
		renderArgs.put("server", getRequestHost());
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
		//list tenaga ahli
		String[][] tenagaAhli=null;
		LDPContent ldpcontent = null;
		if(dok_pl != null) {
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if (dok_pl_content != null) {
				ldpcontent = dok_pl_content.ldpcontent;
				renderArgs.put("adendum", dok_pl_content.isAdendum());
			}
			if (ldpcontent == null && !pl.lls_status.isDraft())
				ldpcontent = dok_pl.getLdpContent();
			if (ldpcontent != null && ldpcontent.bobotTenagaAhli != null) {
				tenagaAhli = new String[ldpcontent.bobotTenagaAhli.size()][2];
				for (int i = 0; i < ldpcontent.bobotTenagaAhli.size(); i++) {
					JsonObject jsonObject = (JsonObject) ldpcontent.bobotTenagaAhli.get(i);
					tenagaAhli[i][0] = jsonObject.get("profesi").toString();
					tenagaAhli[i][1] = jsonObject.get("bobot").toString();
				}
			}
		}
		if (ldpcontent == null) {
			ldpcontent = new LDPContent();
			ldpcontent.kontrak_pembayaran = pl.lls_kontrak_pembayaran;
		}

		if(StringUtils.isEmpty(ldpcontent.evaluasiTeknis)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("kategori", pl.getKategori());
			ldpcontent.evaluasiTeknis = TEMPLATE_EVALUASI_TEKNIS_PL.render(params);
		}
		renderArgs.put("ldpcontent", ldpcontent);
		renderArgs.put("tenagaAhli", tenagaAhli);
		renderTemplate("nonlelang/dokumen/form-pl-ldp.html");
	}
	/**
	 * simpan Form LDP
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void ldpSubmit(Long id, @Valid LDPContent ldpcontent, String[][] tenagaAhli){
		checkAuthenticity();
		otorisasiDataPl(id); // check otorisasi data lelang
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			Pl_seleksi pl = Pl_seleksi.findById(id);
			if(!CommonUtil.isEmpty(tenagaAhli)) {
				JsonArray jsonArray = new JsonArray();

				for(int i = 0; i < tenagaAhli.length; i++){
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("profesi", tenagaAhli[i][0]);
					jsonObject.addProperty("bobot", tenagaAhli[i][1]);
					jsonArray.add(jsonObject);
				}
				ldpcontent.bobotTenagaAhli = jsonArray;
			}
			Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
			boolean editable = dok_pl.isEditable(pl, newDate());
			if (!editable)
				forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
			dok_pl = Dok_pl.findNCreateBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
			if (ldpcontent != null) {
				try {
					dok_pl_content.ldpcontent = ldpcontent; // simpan LDP
					dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);

					pl.lls_kontrak_pembayaran = ldpcontent.kontrak_pembayaran;
					pl.save();

					flash.success("Lembar Data Pemilihan tersimpan");
				} catch (Exception e) {
					Logger.error(e, "Gagal Simpan LDK");
					flash.error("Lembar Data Pemilihan gagal tersimpan");
				}
			}
		}
		ldp(id);

	}

	/**
	 * form HPS
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA,Group.PPK, Group.KUPPBJ})
	public static void hps(Long id, String type){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Pl_seleksi pl = Pl_seleksi.findByPaket(id);
		renderArgs.put("pl", pl);

		boolean fixed = true;
		String data = "[]";
		DaftarKuantitas dk = null;
		boolean adendum = false;
		boolean editable;
		Active_user active_user = Active_user.current();
		if(pl.lls_status.isDraft()){
			DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
			dk = dokPersiapan.dkh;
			editable = dokPersiapan.isEditable(paket, newDate()) && active_user.isPpk();
		} else{
			Dok_pl dok_pl = pl.getDokumenLelang();
			Dok_pl_content dok_pl_content = dok_pl.getDokumenLelangContent();
			dk = dok_pl_content.dkh;
			if(dk == null){
				dk = dok_pl.getRincianHPS();
			}
			editable = dok_pl.isEditable(pl, newDate()) && active_user.isPpk() && type.equals("adendum");
		}
		if (dk != null) {
			fixed = dk.fixed;
			if (dk.items != null) {
				data = CommonUtil.toJson(dk.items);
			}
		}

		renderArgs.put("editable", editable);
		renderArgs.put("data", data);
		renderArgs.put("fixed", fixed);
		renderArgs.put("enableViewHps", paket.isEnableViewHps());
		renderArgs.put("isFlag43",paket.isFlag43());
		renderArgs.put("draft", pl.lls_status.isDraft());

		Map<String, Object> params=new HashMap();
		params.put("id",pl.lls_id);
		String ref = Router.getFullUrl("nonlelang.PengadaanLCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}
		renderArgs.put("paket", paket);
		renderArgs.put("backUrl", ref);
		renderArgs.put("isEdit", !ref.contains("pl"));
		renderArgs.put("isPp", Active_user.current().isPP());
		renderArgs.put("isPanitia", Active_user.current().isPanitia());
		renderTemplate("nonlelang/dokumen/form-pl-hps.html" );
	}

	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void hpsPpk(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Active_user activeUser = Active_user.current();
		boolean fixed = true;
		String data = "[]";
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
		DaftarKuantitas dk = dokPersiapan.dkh;
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		if(dk != null) {
			fixed = dk.fixed;
			if (dk.items != null) {
				data = CommonUtil.toJson(dk.items);
			}
		}
		renderArgs.put("paket", paket);
		renderArgs.put("data", data);
		renderArgs.put("editable", editable);
		renderArgs.put("fixed", fixed);
		renderArgs.put("enableViewHps", paket.isEnableViewHps());
		renderTemplate("nonlelang/dokumen/form-pl-hps-persiapan.html");

	}

	private static Router.ActionDefinition getPaketNonBackUrl(TahapStartedPl tahapStarted, Long paketId) {
		if(tahapStarted.isAllowAdendum()){
			return Router.reverse("nonlelang.PengadaanLCtr.view").add("id", tahapStarted.plId);
		} else if(Active_user.current().isPpk())
			return Router.reverse("nonlelang.PaketPlCtr.edit").add("id", paketId).add("step", 2);
		else if(Active_user.current().isPP() && !tahapStarted.isAllowAdendum()){
			return Router.reverse("nonlelang.PengadaanLCtr.edit").add("id", tahapStarted.plId);
		}else if(Active_user.current().isPanitia() && !tahapStarted.isAllowAdendum()){
			return Router.reverse("nonlelang.PengadaanLCtr.edit").add("id", tahapStarted.plId);
		}
		return null;
	}
	/**
	 * Simpan Form Rincian HPS
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void hpsSubmit(Long id, String data, boolean fixed, boolean enableViewHps) {
		checkAuthenticity();
		otorisasiDataPl(id); // check otorisasi data non tender
		Pl_seleksi pl = Pl_seleksi.findById(id);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		boolean editable = dok_pl.isEditable(pl, newDate());
		Map<String, Object> result = new HashMap<>(1);
		if (!editable) {
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		} else {
			try {
				dok_pl.simpanHps(pl, data, fixed, enableViewHps);
				result.put("result", "ok");
			} catch (Exception e) {
				Logger.error("Gagal Menyimpan HPS : %s", e.getLocalizedMessage());
				result.put("result", "Gagal menyimpan HPS, mohon periksa inputan Anda!");
			}
		}
		renderJSON(result);
	}

	@AllowAccess({Group.PPK})
	public static void hpsPpkSubmit(Long id, String data, boolean fixed, boolean enableViewHps){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Map<String, Object> result = new HashMap<String, Object>(1);
		try{
			DokPersiapanPl.simpanHps(paket, data, fixed, enableViewHps);
			result.put("result", "ok");
		}catch (Exception e){
			Logger.error("Gagal Menyimpan HPS : %s", e.getLocalizedMessage());
			result.put("result", e.getLocalizedMessage());
		}

		renderJSON(result);
	}

	/**
	 * form SPEK
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA, Group.KUPPBJ, Group.PPK})
	public static void spek(Long id){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data non tender
		Kategori kategori = pl.getKategori();
		renderArgs.put("pl", pl);
		renderArgs.put("kategori", kategori);
		Active_user activeUser = Active_user.current();
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id,JenisDokPl.DOKUMEN_LELANG);
		renderArgs.put("editable", pl.getTahapStarted().isAllowAdendum() && activeUser.isPpk());
		if(dok_pl != null) {
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if(dok_pl_content != null) {
				renderArgs.put("spek",dok_pl_content.getDokSpek());
				renderArgs.put("adendum", dok_pl_content.isAdendum());
			}
		}
		renderArgs.put("isPp", activeUser.isPP());
		renderArgs.put("isPanitia", activeUser.isPanitia());
		renderTemplate("nonlelang/dokumen/form-pl-spek.html");
	}

	/**
	 * form Spek/KAK untuk PPP,
	 * Terkait Dokumen Persiapan (@models.agency.DokPersiapan)
	 * adendum tidak dilakukan ppk di halaman
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void spekPpk(Long id, String type){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		renderArgs.put("paket", paket);
		Active_user activeUser = Active_user.current();
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
		List<BlobTable> spek = dokPersiapan.getDokSpek();
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		renderArgs.put("editable", editable);
		renderArgs.put("spek", spek);
		renderArgs.put("isFlag43",paket.isFlag43());
		renderTemplate("nonlelang/dokumen/form-spek-persiapan.html");
	}

	@AllowAccess({Group.PPK})
	public static void spekPpkSubmit(Long id, File file) {
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapanPl = paket.getDokPersiapan();
		if(dokPersiapanPl != null && dokPersiapanPl.isEditable(paket, newDate())){
			try{
				if(dokPersiapanPl.isAdendum() && dokPersiapanPl.dp_spek == null){
					List<BlobTable> blobTableList = dokPersiapanPl.getDokSpek();
					dokPersiapanPl.dp_spek = dokPersiapanPl.simpanBlobSpek(blobTableList);
//					dokPersiapanPl.dp_modified = true;
				}
				UploadInfo model = dokPersiapanPl.simpanSpek(file);
				if(model == null){
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			}catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload spek");
				result.put("result", "Kesalahan saat upload spek");
			}
		}else{
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}


	/**
	 * Simpan Form Spek/KAK
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void spekSubmit(Long id, File file) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data nontender
		Map<String, Object> result = new HashMap<String, Object>(1);
		Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id,	JenisDokPl.DOKUMEN_LELANG);
		Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
		boolean editable = dok_pl.isEditable(pl, newDate());
		if(!editable)
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		else {
			try {
				if(dok_pl_content.isAdendum() && dok_pl_content.dll_spek == null){
					List<BlobTable> blobTableList = dok_pl_content.getDokSpek();
					dok_pl_content.dll_spek = dok_pl_content.simpanBlobSpek(blobTableList);
				}
				UploadInfo model = dok_pl_content.simpanSpek(file);
				if(model == null){
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(model);
				dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload spek");
				result.put("success", false);
				result.put("result", "Kesalahan saat upload spek");
			}
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PP, Group.PANITIA, Group.PPK })
	public static void hapusSpek(Long id, Integer versi) {
		otorisasiDataPl(id); // check otorisasi data lelang
		Dok_pl dok_pl = Dok_pl.findBy(id,JenisDokPl.DOKUMEN_LELANG);
		Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
		if (dok_pl_content != null /*&& dok_lelang_content.dll_modified*/) {
			BlobTable blob = BlobTable.findById(dok_pl_content.dll_spek,versi);
			if (blob != null)
				blob.delete();
			if(dok_pl_content.dll_spek != null && dok_pl_content.getDokSpek().isEmpty()){
				dok_pl_content.dll_spek = null;
			}
			dok_pl_content.dll_modified = true;
			dok_pl_content.save();
		}
	}

	@AllowAccess({ Group.PPK })
	public static void hapusSpekPpk(Long id, Integer versi) {
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);

		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapanPl = paket.getDokPersiapan();
		if (dokPersiapanPl != null) {
			if(dokPersiapanPl.isEditable(paket, newDate())){
				if(dokPersiapanPl.isAdendum() && dokPersiapanPl.dp_spek == null){
					List<BlobTable> blobList = dokPersiapanPl.getDokSpek();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));

					try{
						dokPersiapanPl.dp_spek = dokPersiapanPl.simpanBlobSpek(blobList);
						dokPersiapanPl.dp_modified = true;
						dokPersiapanPl.save();
						if(paket.ukpbj_id != null || paket.pp_id != null || paket.pkt_flag == 2){
							dokPersiapanPl.transferDokPersiapanToDokLelang();
						}
						result.put("success", true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success",false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else{
					if(dokPersiapanPl.dp_spek != null){
						BlobTable blob = BlobTable.findById(dokPersiapanPl.dp_spek,versi);
						if (blob != null){
							blob.delete();
						}
						if (CollectionUtils.isEmpty(dokPersiapanPl.getDokSpek())) {
							dokPersiapanPl.dp_spek = null;
						}
					}
					dokPersiapanPl.dp_modified = true;
					dokPersiapanPl.save();

					if(paket.ukpbj_id != null || paket.pp_id != null || paket.pkt_flag == 2){
						dokPersiapanPl.transferDokPersiapanToDokLelang();
					}
					result.put("success",true);
				}
			}else{
				result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
			}

		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}
		renderJSON(result);
	}


	/**
	 * * Cetak Dokumen Pengadaan atau Pemilihan
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void cetak(Long id, String nomorSDP, @As(binder= DatetimeBinder.class) Date tglSDP) throws IOException {
		checkAuthenticity();
		Dok_pl dok_pl = Dok_pl.findById(id);
		otorisasiDataPl(dok_pl.lls_id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(dok_pl.lls_id);
		if(pl.lls_status.isDraft()) {
			// cek kelengkapan dokumen dilakukan saat lelang masih draft
			Kategori kategori = pl.getKategori();
			StringBuilder complete = new StringBuilder();
			if (dok_pl != null && dok_pl.dll_id != null) {
				Dok_pl_content dok_pl_con = Dok_pl_content.findBy(dok_pl.dll_id);
				if (dok_pl.dll_jenis.isDokLelang() || dok_pl.dll_jenis.isAdendum()) {
					if(CommonUtil.isEmpty(dok_pl_con.dll_ldp))
						complete.append("- Lembar Data Pemilihan.\n ");
					if(CommonUtil.isEmpty(dok_pl_con.dll_dkh))
						complete.append("- Rincian HPS.\n ");
					if(CommonUtil.isEmpty(dok_pl_con.dll_sskk))
						complete.append("- Syarat-Syarat Khusus Kontrak (SSKK).\n ");
					if(Active_user.current().isPanitia() && dok_pl_con.dll_spek == null)
						complete.append("- Spesifikasi Teknis dan Gambar.\n ");
				}
			}
			if (complete.length() > 0)
				flash.error("Kelengkapan Yang Belum Disimpan: " + complete);
		}
		// jika ada error  tidak perlu disimpan
		if(!flash.contains("error")){
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if(dok_pl_content == null)
				return;
			else if(dok_pl_content.dll_modified) {
				dok_pl_content.dll_nomorSDP = nomorSDP;
				dok_pl_content.dll_tglSDP = tglSDP;

				StopWatch sw = new StopWatch();
				sw.start();
				Template template = dok_pl.dll_jenis.isDokKualifikasi() ? TEMPLATE_DOK_KUALIFIKASI:TEMPLATE_DOK_PENGADAAN;
				String content = Dok_pl.cetak_content(pl, dok_pl, dok_pl_content, template);
				if (!StringUtils.isEmpty(content)) {
					dok_pl_content.dll_content_html = content.replace("<br>", "<br/>");
					InputStream dok = HtmlUtil.generatePDF(template.name+"-"+id, "#{extends 'borderTemplatePl.html' /} "+dok_pl_content.dll_content_html);
					// simpan ke dok_lelang
					sw.stop();
					if (dok == null) {
						dok = IOUtils.toInputStream(dok_pl_content.dll_content_html, "UTF-8");
						File file = TempFileManager.createFileInTemporaryFolder("InvalidDocPlTemplate-lls_id#" + dok_pl.lls_id + ".html");
						FileUtils.copyInputStreamToFile(dok, file);
						Logger.error("Gagal generate PDF untuk lls_id %s. Template dokumen yang error disimpan di: %s", pl.lls_id, file);
					} else {
						Logger.debug("Generate PDF document: %s, , duration: %s", dok_pl.dll_nama_dokumen, sw);
					}

					dok_pl.simpanCetak(dok_pl_content, dok);
				}

			}
		}
		if (pl.lls_status.isDraft())
			PengadaanLCtr.edit(dok_pl.lls_id);
		else
			PengadaanLCtr.view(dok_pl.lls_id);
	}

	@AllowAccess({ Group.PP, Group.PANITIA })
	public static void hapusInformasiLainnya(Long id, Integer versi) {
		BlobTable blob = BlobTable.findById(id,versi);
		if (blob != null)
			blob.delete();

	}


	@AllowAccess({Group.REKANAN})
	public static void viewKirimPenawaranPeserta(Long id) {
		otorisasiDataPl(id); // check otorisasi data lelang

		Active_user active_user = Active_user.current();
		Pl_seleksi pl = Pl_seleksi.findById(id);
		Paket_pl paket_pl = Paket_pl.findById(pl.pkt_id);
		Long nilai_pagu = paket_pl.pkt_pagu.longValue();
		renderArgs.put("pl", pl);
		renderArgs.put("paket_pl", paket_pl);
		renderArgs.put("nilai_pagu", nilai_pagu);
		PesertaPl pesertaPl = PesertaPl.find("lls_id=? and rkn_id=?", pl.lls_id, active_user.rekananId).first();
		if (!pesertaPl.hasSubmittedOfferingLetter()) {
			forbidden("Anda belum mengirim surat penawaran atau menyetujui surat penawaran!");
		}
		renderArgs.put("pesertaPl", pesertaPl);
		TahapNowPl tahapAktif = new TahapNowPl(pl.lls_id);

		if (pesertaPl == null || !tahapAktif.isPemasukanPenawaran()) //otorisasi kirim penawaran
			forbidden(Messages.get("not-authorized"));

		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		boolean editable = true;
		//boolean isSudahUpload = false;

		String data = "[]";
		String dataHps = "[]";
		boolean fixed = true;

		DaftarKuantitas dk = pesertaPl.dkh != null ? pesertaPl.dkh : null;
		DaftarKuantitas hps = null;
		Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);

		renderArgs.put("dataHps", dataHps);
		if(dok_pl_content != null) {
			hps = dok_pl_content.dkh;
			renderArgs.put("adendum", dok_pl_content.isAdendum());
		}

		if(hps == null && !pl.lls_status.isDraft())
			hps = dok_pl.getRincianHPS();

		if (hps != null) {
			fixed = hps.fixed;
			if (hps.items != null)
				dataHps = CommonUtil.toJson(hps.items);
		}

		if(dk == null && hps != null) {
			List<Rincian_hps> rincian_hpsList = new ArrayList<>();
			for(Rincian_hps rincian_hps : hps.items){
				rincian_hps.harga = 0d;
				rincian_hps.total_harga =  0d;
				rincian_hpsList.add(rincian_hps);
			}

			hps.items = rincian_hpsList;
			data = CommonUtil.toJson(hps.items);

		} else if(dk == null) {
			dk = new DaftarKuantitas();
			DokPenawaranPl dokPenawaranPl = DokPenawaranPl.findPenawaranPeserta(pesertaPl.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA);
		}

		if (dk != null) {
			fixed = dk.fixed;
			if (dk.items != null)
				data = CommonUtil.toJson(dk.items);

			DokPenawaranPl dokPenawaranPl = DokPenawaranPl.findPenawaranPeserta(pesertaPl.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA);

			//isSudahUpload = dokPenawaranPl != null;
		}

//		if(pl.getKategori().isKonstruksi()){
			pl.withParticipant(Active_user.current());
			pl.getParticipant().withDokumenLelang();
			pl.getParticipant().getDokumenLelang().withPriceChecklistPl();

			renderArgs.put("checklistPlList",pl.getParticipant().getDokumenLelang().getChecklistPls());
			renderArgs.put("dokJenis",DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA.id);

//		}
		renderArgs.put("editable", editable);
		renderArgs.put("data", data);
		renderArgs.put("fixed", fixed);
		renderArgs.put("dok_pl", dok_pl);

		renderTemplate("nonlelang/dokumen/form-penawaran-pl.html");
	}

	@AllowAccess({Group.REKANAN})
	public static void uploadPenawaran(Long id, File file) throws Exception{

		PesertaPl pesertaPl = PesertaPl.findById(id);

		otorisasiDataPl(pesertaPl.lls_id); // check otorisasi data lelang

		DokPenawaranPl dokPenawaranPl = DokPenawaranPl.findPenawaranPeserta(pesertaPl.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA);

		Map<String, Object> result = new HashMap<String, Object>(1);

		try {

			DokPenawaranPl.simpanPenawaran(pesertaPl.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA, file, null);

			result.put("result", "ok");
			result.put("file", file);

		} catch (Exception e) {

			result.put("result", e.getMessage());

		}

		renderJSON(result);
	}


	@AllowAccess({Group.REKANAN})
	public static void penawaranSubmit(Long id, PenawaranDataForm model) throws Exception {
		checkAuthenticity();
		if (model.isDataEmpty()) {
			throw new Exception("Anda Belum mengisikan rincian penawaran");
		}
		PesertaPl pesertaPl = PesertaPl.findById(id);
		otorisasiDataPl(pesertaPl.lls_id);
		JsonObject result = new JsonObject();
		try {
			if (pesertaPl != null) {
				pesertaPl.simpanRincianPenawaran(model);
			}
			result.addProperty("result", "ok");
		} catch (Exception e) {
			result.addProperty("result", e.getMessage());
		}
		renderJSON(result);
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void ldk(Long id) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data nonlelang
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		Kategori kategori = pl.getKategori();
		renderArgs.put("pl", pl);
		renderArgs.put("kategori", kategori);
		JenisDokPl jenis = pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA:JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, jenis);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
		boolean isSyaratKualifikasiBaru = dok_pl.isSyaratKualifikasiBaru();
		renderArgs.put("isNew", ChecklistPl.isChecklistEmpty(dok_pl.dll_id, JenisChecklist.CHECKLIST_LDK)); ///check apakah sudah terdapat checklist di tender ini atau tidak
		if(!kategori.isKonsultansiPerorangan()) {
			List<ChecklistPl> ijinList = isSyaratKualifikasiBaru ? 
					(flash.get("flashError") != null ? Cache.get("ijinList_"+dok_pl.dll_id, List.class) : ChecklistPl.getListIjinUsahaBaru(dok_pl))
					: ChecklistPl.getListIjinUsaha(dok_pl);
			if (flash.get("flashError") != null)
				Cache.safeDelete("ijinList_"+dok_pl.dll_id);
			if (CommonUtil.isEmpty(ijinList)) { // dokumen lelang belum ada
				ijinList = new ArrayList<ChecklistPl>(1);
				ChecklistPl checklist = new ChecklistPl();
				checklist.ckm_id = isSyaratKualifikasiBaru ? 50 : 1;
				ijinList.add(checklist);
			}
			renderArgs.put("ijinList", ijinList);
		}
		
		if (isSyaratKualifikasiBaru) {
	 		List<ChecklistPl> syaratAdmin = new ArrayList<ChecklistPl>();
	 		List<ChecklistPl> syaratTeknis = new ArrayList<ChecklistPl>();
	 		List<ChecklistPl> syaratKeuangan = new ArrayList<ChecklistPl>();    	 		
	 		if (flash.get("flashError") != null) {
	 			syaratAdmin = Cache.get("syaratAdmin_"+dok_pl.dll_id, List.class);
    	 		syaratTeknis = Cache.get("syaratTeknis_"+dok_pl.dll_id, List.class);
    	 		syaratKeuangan = Cache.get("syaratKeuangan_"+dok_pl.dll_id, List.class);
    	 		Cache.safeDelete("syaratAdmin_"+dok_pl.dll_id);
    	 		Cache.safeDelete("syaratTeknis_"+dok_pl.dll_id);
    	 		Cache.safeDelete("syaratKeuangan_"+dok_pl.dll_id);
    	 		renderArgs.put("isOk", false);
	 		} else {
	 			syaratAdmin = ChecklistPl.getListSyaratKualifikasi(dok_pl, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, pl.lls_status.isDraft());
    	 		syaratTeknis = ChecklistPl.getListSyaratKualifikasi(dok_pl, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, pl.lls_status.isDraft());
    	 		syaratKeuangan = ChecklistPl.getListSyaratKualifikasi(dok_pl, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, pl.lls_status.isDraft());
    	 		renderArgs.put("isOk", true);
	 		}
	 		renderArgs.put("syaratAdmin", syaratAdmin);
	 		renderArgs.put("syaratTeknis", syaratTeknis);
	 		renderArgs.put("syaratKeuangan", syaratKeuangan);
	 		renderTemplate("nonlelang/dokumen/form-ldk-baru.html");
	} else {
		renderArgs.put("syaratList", ChecklistPl.getListSyaratKualifikasi(dok_pl, kategori));
		renderTemplate("nonlelang/dokumen/form-ldk.html");
	}
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void ldkSubmit(Long id, List<ChecklistPl> checklist, List<ChecklistPl> ijin) throws Exception {
		checkAuthenticity();
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data nonlelang
		if (!CommonUtil.isEmpty(checklist)) {
			boolean valid = true;
			for (ChecklistPl check : checklist) {
				if (check.isBankSupportSpec() && !check.isValidForPercentage()) {
					valid = false;
					break;
				}
			}
			if (!valid) {
				flash.error("Nominal dukungan keuangan minimal 10%% maksimal 100%%!");
				ldk(id);
			}
		}

		JenisDokPl jenis = pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA:JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, jenis);
		boolean editable = dok_pl.isEditable(pl, newDate());
		if(!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			ChecklistPl.simpanChecklist(dok_pl, ijin, checklist, params, Integer.valueOf(1), pl.getPaket().getKualifikasi());
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateByForLdk(dok_pl.dll_id, pl.lls_status);
			dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
			flash.success("Persyaratan Kualifikasi berhasil tersimpan");
		}catch (Exception e) {
			flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
			Logger.error(e, "Persyaratan Kualifikasi gagal tersimpan. %s", e.getLocalizedMessage());
		}
		ldk(id);
	}
	
	@AllowAccess({Group.PANITIA, Group.PP})
	public static void ldkSubmitBaru(Long id, List<ChecklistPl> ijin, List<ChecklistPl> syaratAdmin, List<ChecklistPl> syaratTeknis, List<ChecklistPl> syaratKeuangan) throws Exception {
		checkAuthenticity();
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data lelang
		Kategori kategori = pl.getKategori();
        JenisDokPl jenis = pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA:JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, jenis);
		Kualifikasi kualifikasi = pl.getPaket().getKualifikasi();
		boolean editable = dok_pl.isEditable(pl, newDate());
		if(!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			ChecklistPl.simpanChecklist(dok_pl, ijin, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, Integer.valueOf(1), kualifikasi);
			ChecklistPl.simpanChecklist(dok_pl, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, Integer.valueOf(1), kualifikasi);
			ChecklistPl.simpanChecklist(dok_pl, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, Integer.valueOf(1), kualifikasi);
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateByForLdk(dok_pl.dll_id, pl.lls_status);
			dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
			flash.success("Persyaratan Kualifikasi berhasil tersimpan");
		} catch (Exception e) {
			if(!kategori.isKonsultansiPerorangan())
				Cache.set("ijinList_"+dok_pl.dll_id, ChecklistPl.getListIjinUsahaBaruFlash(dok_pl, ijin));
				Cache.set("syaratAdmin_"+dok_pl.dll_id, ChecklistPl.getListSyaratKualifikasiFlash(dok_pl, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, kualifikasi));
				Cache.set("syaratTeknis_"+dok_pl.dll_id, ChecklistPl.getListSyaratKualifikasiFlash(dok_pl, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, kualifikasi));
				Cache.set("syaratKeuangan_"+dok_pl.dll_id, ChecklistPl.getListSyaratKualifikasiFlash(dok_pl, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, kualifikasi));
				flash.put("flashError", true);
				flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
				Logger.error(e, e.getMessage());
		}
		ldk(id);
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklist(Long id) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data nonlelang
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
		renderArgs.put("pl", pl);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
		Kategori kategori = pl.getKategori();
		renderArgs.put("kategori", kategori);
		
		List<ChecklistPl> syaratAdmin = ChecklistPl
				.getListSyarat(dok_pl, JenisChecklist.CHECKLIST_ADMINISTRASI, kategori, pl.lls_status.isDraft())
				.stream()
				.sorted(Comparator.comparing(ChecklistPl::getSortingRule))
				.collect(Collectors.toList());
		renderArgs.put("syaratAdmin", syaratAdmin);
		
		final boolean isConsultantPL = kategori.isConsultant();
		List<ChecklistPl> syaratTeknis = ChecklistPl
				.getListSyarat(dok_pl, JenisChecklist.CHECKLIST_TEKNIS, kategori, pl.lls_status.isDraft())
				.stream()
				.filter(c -> isConsultantPL == c.getChecklist_master().isConsultant() || c.getChecklist_master().isSyaratLain())
				.sorted(Comparator.comparing(ChecklistPl::getSortingRule))
				.collect(Collectors.toList());
		renderArgs.put("syaratTeknis", syaratTeknis);
		renderArgs.put("syaratHarga", ChecklistPl.getListSyarat(dok_pl, JenisChecklist.CHECKLIST_HARGA, kategori, pl.lls_status.isDraft()));
		if(dok_pl != null) {
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if (dok_pl_content != null)
				renderArgs.put("adendum", dok_pl_content.isAdendum());
		}
		renderTemplate("nonlelang/dokumen/form-checklist-penawaran-pl.html");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void nomorSDP(Long id){
		Dok_pl dok_pl = Dok_pl.findById(id);
		otorisasiDataPl(dok_pl.lls_id);
		renderArgs.put("id", id);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("dok_pl_content", Dok_pl_content.findBy(dok_pl.dll_id));
		renderTemplate("nonlelang/dokumen/form-nomorSdpPl.html");

	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklistSubmit(Long id,  List<ChecklistPl> syarat, List<ChecklistPl> syaratAdmin, List<ChecklistPl> syaratHarga) {
		checkAuthenticity();
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		boolean editable = dok_pl.isEditable(pl, newDate());
		if (!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {

			if(dok_pl == null)
				dok_pl = Dok_pl.findNCreateBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
			boolean emptyTeknis = true;
			boolean emptyHarga = true;
			// do validation first
			if (syarat != null) {
				for (int i = 0; i < syarat.size(); i++) {
					if (syarat.get(i).ckm_id != null) {
						emptyTeknis= false;
						validation.valid(syarat.get(i));
					}
				}
			}

			if (syaratHarga != null) {
				for (int i = 0; i < syaratHarga.size(); i++) {
					if (syaratHarga.get(i).ckm_id != null) {
						emptyHarga = false;
						validation.valid(syaratHarga.get(i));
					}
				}
			}
			if(!validation.hasErrors()) {
				// do the save
				if (emptyTeknis)
					flash.error("Persyaratan Teknis pada Persyaratan Dokumen Penawaran wajib diisi!");
				else if (pl.getKategori().isKonstruksi() && emptyHarga)
					flash.error("Persyaratan Harga pada Persyaratan Dokumen Penawaran wajib diisi!");
				else {
					// do the save
					if (!emptyTeknis)
						ChecklistPl.simpanChecklist(dok_pl, syarat, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS, Integer.valueOf(1)); // simpan checklist penawaran
					if (syaratAdmin != null)
						ChecklistPl.simpanChecklist(dok_pl, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI, Integer.valueOf(1)); // simpan checklist penawaran
						ChecklistPl.simpanChecklist(dok_pl, syaratHarga, Checklist_master.JenisChecklist.CHECKLIST_HARGA, Integer.valueOf(1)); // simpan checklist penawaran
						Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateByForSyarat(dok_pl.dll_id, pl);
						dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
						flash.success("Persyaratan Dokumen tersimpan");
				}
			} else {
				StringBuilder sb = new StringBuilder();
				for(play.data.validation.Error err:validation.errors()) {
					if(!StringUtils.isEmpty(err.getKey())) {
						sb.append("<br/>").append(err.message());
					}
				}
				flash.error("Persyaratan Dokumen gagal tersimpan:"+ sb);
			}
		}catch (Exception e) {
			Logger.error(e, "Gagal Simpan Persyaratan Dokumen");
			flash.error("Persyaratan Dokumen gagal tersimpan");
		}
		checklist(id);
	}

	@AllowAccess({Group.REKANAN})
	public static void viewKirimSuratPenawaran(Long id){
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		renderArgs.put("pl", pl);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		LDPContent ldpcontent = null;
		if(dok_pl != null) {
			Dok_pl_content dok_pl_content = Dok_pl_content.findByLdp(dok_pl.dll_id);
			if (dok_pl_content != null)
				ldpcontent = dok_pl_content.ldpcontent;
		}
		renderArgs.put("ldpcontent", ldpcontent);
		Active_user active_user = Active_user.current();
		PesertaPl peserta = PesertaPl.findBy(pl.lls_id, active_user.rekananId);
		renderArgs.put("peserta", peserta);
		String namaLpse = ConfigurationDao.getNamaLpse();
		String alamatLpse = ConfigurationDao.getNamaLpse();
		renderArgs.put("namaLpse", namaLpse);
        renderArgs.put("alamatLpse", alamatLpse);
		TahapNowPl tahapNowPl = new TahapNowPl(pl.lls_id);
		Tahap tahapSekarang = null;
		if(tahapNowPl.isPemasukanPenawaran())
			tahapSekarang = Tahap.PEMASUKAN_PENAWARAN;
		Jadwal_pl jadwalpl = Jadwal_pl.findByLelangNTahap(pl.lls_id, tahapSekarang);
		if(jadwalpl != null){
			renderArgs.put("tglPenutupan", jadwalpl.dtj_tglakhir);
		}

//		List<UndanganPeserta> upList = UndanganPeserta.findAll(pl.lls_id, tahapSekarang, pl.isLelangV3());

		renderArgs.put("nomorUndanganPl", "NOMORUNDANGANPL");
		renderArgs.put("tanggalPl", "TANGGALPL");
		renderArgs.put("jumlahPenawaran", peserta.getJumlahPenawaran());
		renderArgs.put("jumlahPenawaranKata", FormatUtils.terbilang(peserta.getJumlahPenawaran()));
		if(peserta.lama_pekerjaan==null)
			peserta.lama_pekerjaan = 0;
		renderArgs.put("lamaPekerjaan", peserta.lama_pekerjaan);
		renderArgs.put("lamaPekerjaanTerbilang", FormatUtils.terbilang(peserta.lama_pekerjaan));
		renderTemplate("nonlelang/dokumen/surat-penawaran-pl.html");
	}

	@AllowAccess({Group.REKANAN})
	public static void viewKirimPenawaranTeknis(Long id) {
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		notFoundIfNull(pl);
		Active_user active_user = Active_user.current();
		pl.withParticipant(active_user);
		notFoundIfNull(pl.getParticipant());
		if (!pl.getParticipant().hasSubmittedOfferingLetter()) {
			forbidden("Anda belum mengirim surat penawaran atau menyetujui surat penawaran!");
		}
		pl.getParticipant().withDokumenLelang();
		notFoundIfNull(pl.getParticipant().getDokumenLelang());
		pl.getParticipant().getDokumenLelang().withChecklistPl();
		Map<String, Object> params = new HashMap<>();
		params.put("pl", pl);
		params.put("pesertaPl", pl.getParticipant());
		params.put("checklistPlList", pl.getParticipant().getDokumenLelang().getSyaratTeknis());
		params.put("dokJenis",DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id);
		renderTemplate("nonlelang/dokumen/form-penawaran-teknis-pl.html", params);
	}

	@AllowAccess({Group.REKANAN})
	public static void submitPenawaranTeknis(Long lls_id, Long psr_id, Long chk_id, @Valid @DokumenType File file) throws Exception {

		Map<String, Object> result = new HashMap<>();

		try {

			if (file != null) {

				int filenameLength = FilenameUtils.removeExtension(file.getName()).length();

				if (filenameLength <= 80) {

					Map<String, Object> res = DokPenawaranPl.simpanDokPenawaran(psr_id, lls_id, file, chk_id,
							DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);

					UploadInfo info = (UploadInfo) res.get("file");

					List<UploadInfo> files = new ArrayList<UploadInfo>();

					files.add(info);

					result.put("files", files);
					result.put("dok_id", res.get("dok_id"));
					result.put("chk_id", res.get("chk_id"));

				} else
					result.put("error", "Dokumen Teknis Gagal di simpan: Judul dokumen maksimal 80 karakter.");
			}

		} catch (Exception e){

			result.put("error", "Dokumen Teknis Gagal di simpan: Judul dokumen maksimal 80 karakter.");

			Logger.error("Kesalahan saat upload penawaran teknis: detail %s", e.getMessage());

			e.printStackTrace();

		}

		renderJSON(result);

	}

	@AllowAccess({Group.REKANAN})
	public static void hapusPenawaranTeknis(Long dok_id) {
		DokPenawaranPl dokPenawaranPl = DokPenawaranPl.findById(dok_id);
		JsonObject result = new JsonObject();
		try {
			dokPenawaranPl.deleteModelAndBlob();
			result.addProperty("status", "ok");
		} catch (Exception e) {
			Logger.error("Hapus Penawaran Teknis", e.getLocalizedMessage());
			result.addProperty("status", "error");
			result.addProperty("message", "Dokumen gagal dihapus!");
		}
		renderJSON(result);
	}

	@AllowAccess({Group.REKANAN})
	public static void uploadPenawaranHargaKonstruksi(Long lls_id, Long psr_id, Long chk_id, @Valid @DokumenType File file) throws Exception {

		Map<String, Object> result = new HashMap<>();

		try {

			if (file != null) {

				int filenameLength = FilenameUtils.removeExtension(file.getName()).length();

				if (filenameLength <= 80) {

					Map<String, Object> res = DokPenawaranPl.simpanDokPenawaran(psr_id, lls_id, file, chk_id,
							DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA);

					UploadInfo info = (UploadInfo) res.get("file");

					List<UploadInfo> files = new ArrayList<UploadInfo>();

					files.add(info);

					result.put("files", files);
					result.put("dok_id", res.get("dok_id"));
					result.put("chk_id", res.get("chk_id"));

				} else
					result.put("error", "Dokumen Harga Gagal di simpan: Judul dokumen maksimal 80 karakter.");
			}

		} catch (Exception e){

			result.put("error", "Dokumen Harga Gagal di simpan: Judul dokumen maksimal 80 karakter.");

			Logger.error("Kesalahan saat upload penawaran teknis: detail %s", e.getMessage());

			e.printStackTrace();

		}

		renderJSON(result);

	}

	@AllowAccess({Group.REKANAN})
	public static void hapusPenawaranHargaKonstruksi(Long dok_id) {
		DokPenawaranPl dokPenawaranPl = DokPenawaranPl.findById(dok_id);
		JsonObject result = new JsonObject();
		try {
			dokPenawaranPl.deleteModelAndBlob();
			result.addProperty("status", "ok");
		} catch (Exception e) {
			Logger.error("Hapus Dokumen Harga Konstruksi", e.getLocalizedMessage());
			result.addProperty("status", "error");
			result.addProperty("message", "Dokumen gagal dihapus!");
		}
		renderJSON(result);
	}

	public static void submitSuratPenawaran(PesertaPl peserta){

		Dok_pl dok_pl = Dok_pl.findBy(peserta.lls_id, JenisDokPl.DOKUMEN_LELANG);
		Dok_pl_content dok_pl_content = Dok_pl_content.findByLdp(dok_pl.dll_id);
		if (peserta.masa_berlaku_penawaran >= dok_pl_content.ldpcontent.masa_berlaku_penawaran) {

			//dok_pl_content.ldpcontent = ldpcontent;

			//dok_pl_content.save();
			PesertaPl newPeserta = PesertaPl.findById(peserta.psr_id);
			newPeserta.masa_berlaku_penawaran = peserta.masa_berlaku_penawaran;
			newPeserta.lama_pekerjaan = peserta.lama_pekerjaan;
			newPeserta.tgl_surat_penawaran = controllers.BasicCtr.newDate();
			newPeserta.save();

			//update masa berlaku penawaran di penawaran teknis
			List<DokPenawaranPl> listDokPenawaranTeknis = DokPenawaranPl.findListPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
			for (DokPenawaranPl dokPenawaranPl : listDokPenawaranTeknis){

				dokPenawaranPl.dok_waktu = peserta.masa_berlaku_penawaran;

				dokPenawaranPl.save();

			}

		} else
			flash.error("Masa Berlaku penawaran harus lebih dari sebelumnya");

		viewKirimSuratPenawaran(peserta.lls_id);

	}

	@AllowAccess({Group.PANITIA, Group.PP, Group.PPK})
	public static void sskk(Long id){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		renderArgs.put("lelang", pl);
		renderArgs.put("isConsultant", pl.getKategori().isConsultant() || pl.getKategori().isJkKonstruksi());
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		Active_user active_user = Active_user.current();
		boolean editable = dok_pl.isEditable(pl, newDate());
		boolean isFlag43 = pl.getPaket().isFlag43();
		if(isFlag43){
			editable = editable && active_user.isPpk();
		}
		renderArgs.put("editable", editable);
		renderArgs.put("isFlagV3",isFlag43);
		renderArgs.put("isPanitia", active_user.isPanitia());
		renderArgs.put("isPP", active_user.isPP());
		Kategori kategori = pl.getKategori();
		renderArgs.put("kategori", kategori);
		SskkContent sskk_content = null;
		if(dok_pl != null){
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if(dok_pl_content != null){
				renderArgs.put("adendum", dok_pl_content.isAdendum());
				sskk_content = dok_pl_content.sskk_content;
			}
			if(sskk_content == null && !pl.lls_status.isDraft()){
				sskk_content = dok_pl.getSskkContent();
			}
		}
		if(sskk_content == null){
			sskk_content = new SskkContent();
			sskk_content.kontrak_pembayaran = pl.lls_kontrak_pembayaran;
		}
		renderArgs.put("backUrl", getPaketNonBackUrl(tahapStarted, pl.pkt_id));
		renderArgs.put("urlSubmit",Router.reverse("nonlelang.DokumenPlCtr.sskkSubmit").add("id", pl.lls_id));
		renderArgs.put("sskk_content", sskk_content);
		renderTemplate("nonlelang/dokumen/form-sskk.html");
	}

	@AllowAccess({Group.PP, Group.PANITIA, Group.KUPPBJ, Group.PPK})
	public static void docSskk(Long id){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data non tender
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		Kategori kategori = pl.getKategori();
		renderArgs.put("pl", pl);
		renderArgs.put("kategori", kategori);
		renderArgs.put("isConsultant", kategori.isConsultant() || kategori.isJkKonstruksi());
		Active_user activeUser = Active_user.current();
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id,JenisDokPl.DOKUMEN_LELANG);
		boolean isFlag43 = pl.getPaket().isFlag43();
		renderArgs.put("editable", tahapStarted.isAllowAdendum() && activeUser.isPpk());
		if(dok_pl != null) {
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if(dok_pl_content != null) {
				renderArgs.put("sskk",dok_pl_content.getDokSskk());
				renderArgs.put("adendum", dok_pl_content.isAdendum());
			}
		}
		renderTemplate("nonlelang/dokumen/form-upload-sskk.html");
	}

	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void sskkPpk(Long id){
		Paket_pl paket =Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Pl_seleksi pl = Pl_seleksi.findByPaket(id);
		renderArgs.put("pl", pl);
		Kategori kategori = pl.getKategori();
		renderArgs.put("kategori", kategori);
		renderArgs.put("isConsultant", kategori.isConsultant() || kategori.isJkKonstruksi());
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(id);
		renderArgs.put("editable", dokPersiapan.isEditable(paket, newDate()) && Active_user.current().isPpk());
		renderArgs.put("isFlagV3", paket.isFlag43());
		renderArgs.put("isPanitia", Active_user.current().isPanitia());
		renderArgs.put("isPP", Active_user.current().isPP());
		renderArgs.put("adendum", dokPersiapan.isAdendum());
		SskkContent sskkContent = dokPersiapan.sskkContent;
		if(sskkContent == null){
			if(!pl.lls_status.isDraft()){
				sskkContent = dokPersiapan.getOldSskkContent();
			}else{
				sskkContent = new SskkContent();
				sskkContent.kontrak_pembayaran = pl.lls_kontrak_pembayaran;
			}
		}
		renderArgs.put("backUrl", Router.reverse("nonlelang.PaketPlCtr.edit").add("id", paket.pkt_id));
		renderArgs.put("urlSubmit", Router.reverse("nonlelang.DokumenPlCtr.sskkPpkSubmit").add("id", pl.pkt_id));
		renderArgs.put("sskk_content", sskkContent);

		renderTemplate("nonlelang/dokumen/form-sskk.html");

	}
	
	@AllowAccess({Group.PPK, Group.KUPPBJ, Group.PANITIA, Group.PP})
	public static void uploadSskk(Long id, String type){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Active_user activeUser = Active_user.current();
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
		List<BlobTable> sskk = dokPersiapan.getDokSskkAttachment();
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		renderArgs.put("paket", paket);
		renderArgs.put("editable", editable);
		renderArgs.put("isFlagV3", paket.isFlag43());
		renderArgs.put("sskk", sskk);
		renderArgs.put("isPanitia", Active_user.current().isPanitia());
		renderArgs.put("isPP", Active_user.current().isPP());
		renderTemplate("nonlelang/dokumen/form-upload-sskk-persiapan.html");

	}

	@AllowAccess({Group.PPK})
	public static void uploadSskkSubmit(Long id, File file){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Map<String, Object> result = new HashMap<>(1);
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket.pkt_id);
		if(dokPersiapan != null && dokPersiapan.isEditable(paket, newDate())){
			try{
				UploadInfo model = dokPersiapan.simpanSskkAttachment(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			}catch (Exception e) {
				Logger.error(e, e.getLocalizedMessage());
				result.put("result", "Kesalahan saat upload sskk");
			}
		}else{
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}

	public static void uploadSskkDokPl(Long id, File file){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);
		Map<String, Object> result = new HashMap<>(1);
		Dok_pl dok_pl = pl.getDokumenLelang();
		if(dok_pl != null && dok_pl.isEditable(pl, newDate())){
			try{
				Dok_pl_content dok_pl_content = dok_pl.getDokumenLelangContent();
				if(dok_pl_content.isAdendum() && dok_pl_content.dll_sskk_attachment == null){
					List<BlobTable> blobTableList = dok_pl_content.getDokSskk();
					dok_pl_content.dll_sskk_attachment = dok_pl_content.simpanBlobSpek(blobTableList);
					dok_pl_content.dll_modified = true;
				}
					UploadInfo model = dok_pl_content.simpanSskkAttachment(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			}catch (Exception e) {
				Logger.error(e, e.getLocalizedMessage());
				result.put("result", "Kesalahan saat upload sskk");
			}
		}else{
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}

	public static void hapusSskkAttachment(Long id, Integer versi){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);

		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapan = paket.getDokPersiapan();
		if(dokPersiapan != null){
			if(dokPersiapan.isEditable(paket, newDate())){
				if(dokPersiapan.isAdendum() && dokPersiapan.dp_sskk_attachment == null){
					List<BlobTable> blobList = dokPersiapan.getDokSskkAttachment();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));

					try{
						dokPersiapan.dp_sskk_attachment = dokPersiapan.simpanBlobSpek(blobList);
						dokPersiapan.dp_modified = true;
						dokPersiapan.save();
						if(paket.ukpbj_id != null || paket.pp_id != null || paket.pkt_flag == 2){
							dokPersiapan.transferSskkToDokPl();
						}
						result.put("success", true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success", false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else{
					BlobTable blob = BlobTable.findById(dokPersiapan.dp_sskk_attachment, versi);
					if(blob != null){
						blob.delete();
					}
					if(dokPersiapan.dp_sskk_attachment != null && dokPersiapan.getDokSskkAttachment().isEmpty()){
						dokPersiapan.dp_sskk_attachment = null;
					}
					dokPersiapan.dp_modified = true;
					dokPersiapan.save();

					if(paket.ukpbj_id != null || paket.pp_id != null || paket.pkt_flag == 2){
						dokPersiapan.transferSskkToDokPl();
					}
					result.put("success",true);
				}
			}else{
				result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
			}

		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}
		renderJSON(result);
	}

	/**
	 * Simpan SSKK PPK
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void sskkPpkSubmit(Long id,@Valid SskkContent sskk_content){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Pl_seleksi pl = Pl_seleksi.findByPaket(id);
		boolean valid = false;
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			sskkError(id,sskk_content);
		} else {
			DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(id);
			boolean editable = dokPersiapan.isEditable(paket, newDate());
			if (!editable)
				forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
			if (sskk_content != null) {
				Date now = newDate();
				LocalDate nowLd = LocalDate.fromDateFields(now);
				LocalDate kontrakMulaiLd = sskk_content.kontrak_mulai != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_mulai) : null;
				LocalDate kontrakAkhirLd = sskk_content.kontrak_akhir != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_akhir) : null;

				if (kontrakMulaiLd != null && kontrakMulaiLd.isBefore(nowLd)
						|| kontrakAkhirLd != null && kontrakAkhirLd.isBefore(nowLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku yang Anda masukkan sudah lewat");
				} else if (kontrakAkhirLd != null && kontrakMulaiLd != null
						&& kontrakAkhirLd.isBefore(kontrakMulaiLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku akhir kontrak mendahului awal kontrak");
				} else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.after(sskk_content.kontrak_akhir)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi setelah akhir kontrak");
				} else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi mendahului mulai kontrak");
				} else if (sskk_content.tgl_serah_terima != null && sskk_content.tgl_serah_terima.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Serah Terima Pekerjaan mendahului mulai kontrak");
				}else {// simpan SSKK
					dokPersiapan.sskkContent = sskk_content;
					try {
						dokPersiapan.save();
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (paket.ukpbj_id != null) {
						try {
							dokPersiapan.transferDokPersiapanToDokLelang();
						} catch (Exception e) {
							flash.error(e.getLocalizedMessage());
						}
					}
					pl.lls_kontrak_pembayaran = sskk_content.kontrak_pembayaran;
					pl.save();

					flash.success("Syarat-Syarat Khusus Kontrak telah tersimpan ");

				}
				}
		}
		if(valid)
			sskkPpk(id);
		else
			sskkError(id,sskk_content);

	}

	/**
	 * form SSKK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA, Group.PP, Group.PPK})
	public static void sskkError(Long id, SskkContent sskk_content){
		if(Active_user.current().isPanitia() || Active_user.current().isPP()){
			otorisasiDataPl(id);
		}else{
			Paket_pl paket = Paket_pl.findById(id);
			otorisasiDataPaketPl(paket);
			id = Pl_seleksi.findByPaket(id).lls_id;
		}
		Pl_seleksi pl = Pl_seleksi.findById(id);
		renderArgs.put("pl", pl);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
		renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
		renderArgs.put("kategori", pl.getKategori());
		if(sskk_content == null){
			sskk_content = new SskkContent();
			sskk_content.kontrak_pembayaran = pl.lls_kontrak_pembayaran;
		}
		renderArgs.put("sskk_content", sskk_content);
		Router.ActionDefinition urlSubmit = Active_user.current().isPanitia() || Active_user.current().isPP()?
				Router.reverse("nonlelang.DokumenPlCtr.sskkSubmit").add("id", pl.lls_id) :
				Router.reverse("nonlelang.DokumenPlCtr.sskkPpkSubmit").add("id", pl.pkt_id);

	}

	@AllowAccess({Group.PANITIA, Group.PP})
	public static void sskkSubmit(Long id, @Valid  SskkContent sskk_content){
		checkAuthenticity();
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);// check otorisasi data lelang
		boolean valid = false;
		if(validation.hasErrors()){
			validation.keep();
			params.flash();
			sskkError(id,sskk_content);
		}else{
			Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
			boolean editable = dok_pl.isEditable(pl, newDate());
			if(!editable)
				forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
			if(sskk_content != null){
				Date now = newDate();
				LocalDate nowLd = LocalDate.fromDateFields(now);
				LocalDate kontrakMulaiLd = sskk_content.kontrak_mulai != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_mulai) : null;
				LocalDate kontrakAkhirLd = sskk_content.kontrak_akhir != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_akhir) : null;

				if (kontrakMulaiLd != null && kontrakMulaiLd.isBefore(nowLd)
						|| kontrakAkhirLd != null && kontrakAkhirLd.isBefore(nowLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku yang Anda masukkan sudah lewat");
				} else if (kontrakAkhirLd != null && kontrakMulaiLd != null
						&& kontrakAkhirLd.isBefore(kontrakMulaiLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku akhir kontrak mendahului awal kontrak");
				}else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.after(sskk_content.kontrak_akhir)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi setelah akhir kontrak");
				} else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi mendahului mulai kontrak");
				} else if (sskk_content.tgl_serah_terima != null && sskk_content.tgl_serah_terima.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Serah Terima Pekerjaan mendahului mulai kontrak");
				}else{
					Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
					dok_pl_content.sskk_content = sskk_content;
					pl.lls_kontrak_pembayaran = sskk_content.kontrak_pembayaran;
					pl.save();
					dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
					flash.success("Syarat-Syarat Khusus Kontrak telah tersimpan ");
					valid = true;
				}
			}
		}
		if(valid)
			sskk(id);
		else
			sskkError(id,sskk_content);

	}

	/**
	 * upload dok lainnya oleh PPK saat proses persiapan non tender bukan buat adendum dokumen
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void lainnyaPpk(Long id, String type){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Active_user active_user = Active_user.current();
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
		List<BlobTable> lainnya = dokPersiapan.getDokLainnya();
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && active_user.isPpk();
		renderArgs.put("paket", paket);
		renderArgs.put("lainnya", lainnya);
		renderArgs.put("editable", editable);
		renderArgs.put("isKuppbj", Active_user.current().isKuppbj());
		renderTemplate("nonlelang/dokumen/form-info-lainnya-persiapan.html");
	}

	/**
	 * form Dokumen Pengadaan Lainnya
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA, Group.KUPPBJ, Group.PPK})
	public static void lainnya(Long id) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data non tender
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		renderArgs.put("pl", pl);
		renderArgs.put("kategori", pl.getKategori());
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id,JenisDokPl.DOKUMEN_LELANG);
		renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
		if(dok_pl != null) {
			Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
			if(dok_pl_content != null) {
				renderArgs.put("lainnya",dok_pl_content.getDokLainnya());
				renderArgs.put("adendum", dok_pl_content.isAdendum());
			}
		}
		renderArgs.put("backUrl", getPaketNonBackUrl(tahapStarted, pl.pkt_id));
		renderArgs.put("isKuppbj", Active_user.current().isKuppbj());
		renderTemplate("nonlelang/dokumen/form-info-lainnya-pl.html");

	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void lainnyaSubmit(Long id, File file) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);// check otorisasi data non tender
		Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id,	JenisDokPl.DOKUMEN_LELANG);
		Map<String, Object> result = new HashMap<String, Object>(1);
		if(dok_pl.isEditable(pl, newDate())){
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
			try {
				if(dok_pl_content.isAdendum() && dok_pl_content.dll_lainnya == null){
					List<BlobTable> blobTableList = dok_pl_content.getDokLainnya();
					if(!CollectionUtils.isEmpty(blobTableList))
						dok_pl_content.dll_lainnya = dok_pl_content.simpanBlobSpek(blobTableList);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(dok_pl_content.simpanLainnya(file));
				dok_pl.simpan(pl.lls_status, pl.getNamaPaket(), dok_pl_content);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload informasi lainnya");
				result.put("success", false);
				result.put("result", "Kesalahan saat upload informasi lainnya");
			}
		}else {
			result.put("success", false);
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);

	}

	/**
	 * Simpan Form Lainnya
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void lainnyaPpkSubmit(Long id, File file){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);// check otorisasi data non tender
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapan = paket.getDokPersiapan();
		if(dokPersiapan != null && dokPersiapan.isEditable(paket, newDate())){
			try{
				UploadInfo model = dokPersiapan.simpanLainnya(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload informasi lainnya");
				result.put("result", "Kesalahan saat upload informasi lainnya");
			}
		}else{
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}

		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void hapusLainnyaPpk(Long id, Integer versi) {
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);// check otorisasi data lelang

		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null) {
			if(dokPersiapan.isEditable(paket, newDate())){
				if(dokPersiapan.isAdendum() && dokPersiapan.dp_lainnya == null){
					List<BlobTable> blobList = dokPersiapan.getDokLainnya();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));
					try{
						dokPersiapan.dp_lainnya = dokPersiapan.simpanBlobSpek(blobList);
						dokPersiapan.dp_modified = true;
						dokPersiapan.save();
						if (paket.ukpbj_id != null) {
							dokPersiapan.transferDokPersiapanToDokLelang();
						}
						result.put("success",true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success",false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else {
					if (dokPersiapan.dp_lainnya != null) {
						BlobTable blob = BlobTable.findById(dokPersiapan.dp_lainnya, versi);
						if (blob != null) {
							blob.delete();
						}
						if (CollectionUtils.isEmpty(dokPersiapan.getDokLainnya())) {
							dokPersiapan.dp_lainnya = null;
						}
					}
					dokPersiapan.dp_modified = true;
					dokPersiapan.save();

					if (paket.ukpbj_id != null) {
						dokPersiapan.transferDokPersiapanToDokLelang();
					}
					result.put("success",true);
				}
			}else {
				result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
			}
		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PP, Group.PANITIA })
	public static void hapusLainnya(Long id, Integer versi) {

		otorisasiDataPl(id); // check otorisasi data lelang

		Dok_pl dok_pl = Dok_pl.findBy(id,JenisDokPl.DOKUMEN_LELANG);

		Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);

		if (dok_pl_content != null /*&& dok_lelang_content.dll_modified*/) {

			BlobTable blob = BlobTable.findById(dok_pl_content.dll_lainnya,versi);

			if (blob != null)
				blob.delete();

		}

	}
	// shortcut buat cetak ulang
	public static void cetakUlang(@Required Long id, @Required Integer versi) throws IOException {
		Dok_pl dok_pl = Dok_pl.findById(id);
		if(dok_pl == null)
			return;
		Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id, versi);
		if(dok_pl_content == null)
			return;
		StopWatch sw = new StopWatch();
		sw.start();
		Template template = dok_pl.dll_jenis.isDokKualifikasi() ? TEMPLATE_DOK_KUALIFIKASI:TEMPLATE_DOK_PENGADAAN;
		Pl_seleksi pl = Pl_seleksi.findById(dok_pl.lls_id);
		String content = Dok_pl.cetak_content(pl, dok_pl, dok_pl_content, template);
		if (!StringUtils.isEmpty(content)) {
			dok_pl_content.dll_content_html = content.replace("<br>", "<br/>");
			InputStream dok = HtmlUtil.generatePDF(template.name+"-"+id, "#{extends 'borderTemplatePl.html' /} "+dok_pl_content.dll_content_html);
			// simpan ke dok_lelang
			if (dok == null) {
				dok = IOUtils.toInputStream(dok_pl_content.dll_content_html, "UTF-8");
				File file = TempFileManager.createFileInTemporaryFolder("InvalidDocPlTemplate-lls_id#" + dok_pl.lls_id + ".html");
				FileUtils.copyInputStreamToFile(dok, file);
				Logger.error("Gagal generate PDF untuk lls_id %s. Template dokumen yang error disimpan di: %s", pl.lls_id, file);
			} else {
				Logger.debug("Generate PDF document: %s, , duration: %s", dok_pl.dll_nama_dokumen, sw);
			}
			dok_pl.simpanCetak(dok_pl_content, dok);
			response.writeChunk("Dokumen Non tender "+pl.lls_id+" versi "+versi+" sudah berhasil dicetak ulang");
		}
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void masaBerlakuPenawaran(Long id){
	otorisasiDataPl(id);// check otorisasi data non tender
	renderArgs.put("id", id);
	Pl_seleksi pl = Pl_seleksi.findById(id);
	Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
	renderArgs.put("editable", dok_pl.isEditable(pl, newDate()));
	if(dok_pl != null){
		Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
		if(dok_pl_content != null && dok_pl_content.ldpcontent != null)
			renderArgs.put("masaberlaku", dok_pl_content.ldpcontent.masa_berlaku_penawaran);
	}
		renderTemplate("nonlelang/dokumen/masa-berlaku-penawaran.html");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void masaBerlakuPenawaranSubmit(Long id, Integer masaberlaku){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);  // check otorisasi data non tender
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
		boolean editable = dok_pl.isEditable(pl, newDate());
		if(!editable){
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		}else{
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
			if(dok_pl_content != null){
				LDPContent ldpContent = dok_pl_content.ldpcontent != null ? dok_pl_content.ldpcontent:new LDPContent();
				ldpContent.masa_berlaku_penawaran = masaberlaku;
				dok_pl_content.ldpcontent = ldpContent;
				dok_pl_content.dll_modified = true;
				dok_pl_content.save();
			}
		}
			PengadaanLCtr.edit(id);
	}

	public static void uploadDokNonTender(Long id){
		Dok_pl dok_pl = Dok_pl.findById(id);
		renderArgs.put("id", dok_pl.lls_id);
		renderArgs.put("dokId", id);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("dok_pl_content", Dok_pl_content.findBy(dok_pl.dll_id));
		renderArgs.put("today", newDate());

		Map<String, Object> params=new HashMap();
		params.put("id",dok_pl.lls_id);
		String ref = Router.getFullUrl("nonlelang.PengadaanLCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}

		renderArgs.put("referer",ref);
		renderTemplate("nonlelang/dokumen/upload-dok-pemilihan.html");
	}
	
	@AllowAccess({Group.PANITIA, Group.PP})
    public static void uploadDokKualifikasi(Long id) {
		Dok_pl dok_pl = Dok_pl.findById(id);
		renderArgs.put("id", dok_pl.lls_id);
		renderArgs.put("dokId", id);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("dok_pl_content", Dok_pl_content.findBy(dok_pl.dll_id));
		renderArgs.put("today", newDate());

		Map<String, Object> params=new HashMap();
		params.put("id",dok_pl.lls_id);
		String ref = Router.getFullUrl("lelang.LelangCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}

		renderArgs.put("referer",ref);
        renderTemplate("nonlelang/dokumen/upload-dok-kualifikasi.html");
    }

	/**
     * Simpan Form Dokumen Kualifikasi
     * @param id
     */
	@AllowAccess({Group.PANITIA, Group.PP})
	public static void dokKualifikasiSubmit(Long id, @Valid @PdfType File file, String nomorSDP,
											@As(binder= DateBinder.class) Date tglSDP) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data lelang
		if (validation.hasErrors()) {
			String namaDokumen = pl.isPenunjukanLangsungNew()  ? "Tender/Seleksi" : "Pemilihan";
			flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
		} else {
			Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG_PRA);
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
			try {
				dok_pl_content.dll_nomorSDP = nomorSDP;
				dok_pl_content.dll_tglSDP = tglSDP;
				dok_pl.simpan(dok_pl_content, pl, file);
				flash.success("Upload Dokumen Kualifikasi berhasil.");
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Kualifikasi");
			}
		}
		PengadaanLCtr.edit(id);
	}

	/**
	 * Simpan Form Dokumen Pengadaan
	 * @param id
	 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void doknontenderSubmit(Long id, @Valid @PdfType File file, String nomorSDP,
										  @As(binder= DateBinder.class) Date tglSDP, String ref){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);// check otorisasi data non tender
		if (validation.hasErrors()) {
			flash.error("Untuk Dokumen Pemilihan yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>");
		}else{
			Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl );
			try{
				// cetak konten html
				dok_pl_content.dll_nomorSDP = nomorSDP;
				dok_pl_content.dll_tglSDP = tglSDP;
				dok_pl.simpan(dok_pl_content, pl, file);
				flash.success("Upload Dokumen Pemilhan berhasil.");
			}catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Pemilihan");
			}
		}
		redirect(ref);
	}

	public static void hapusDokNonTender(Long id, Integer versi){
		otorisasiDataPl(id);// check otorisasi data non lelang
		Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
		Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
		if(dok_pl_content != null /*&& dok_pl_content.dll_modified*/){
			try {
				BlobTable blob = BlobTable.findById(dok_pl_content.dll_content_attachment, versi);
				if (blob != null)
					blob.delete();
				if(dok_pl_content.isEmptyDokumen())
					dok_pl_content.dll_content_attachment = null;
				if(dok_pl.isEmptyDokumen())
					dok_pl.dll_id_attachment = null;
				dok_pl_content.dll_modified = true;
				dok_pl_content.save();
				dok_pl.save();

			}catch (Exception e) {
				Logger.error(e, "Gagal Hapus Dokumen non tender");
			}

		}
	}

	/**
	 * form Jenis Paket
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void editJenisKontrak(Long id) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl); // check otorisasi data non tender
		Kategori kategori = pl.getKategori();
		renderArgs.put("pl", pl);
		renderArgs.put("kategori", kategori);
		Dok_pl dok_pl = Dok_pl.findNCreateBy(pl.lls_id,JenisDokPl.DOKUMEN_LELANG);
		final boolean isEditable = dok_pl.isEditable(pl, newDate());
		renderArgs.put("editable", isEditable);
		renderTemplate("nonlelang/dokumen/edit-jenis-paket.html");
	}
	/**
	 * simpan Jenis Paket
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void editJenisPaketSubmit(Long id, Integer kontrak_pembayaran) throws Exception {
		checkAuthenticity();
		Pl_seleksi lelang = Pl_seleksi.findById(id);
		otorisasiDataPl(lelang); // check otorisasi data lelang
		Logger.info("Kontrak Pembayaran = %d", kontrak_pembayaran);
		
		JenisKontrak kontrak = JenisKontrak.findById(kontrak_pembayaran);
		if(kontrak!=null) {
			Dok_pl dok_pl = Dok_pl.findNCreateBy(lelang.lls_id,JenisDokPl.DOKUMEN_LELANG);
			Dok_pl_content dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, lelang);
			dok_pl_content.dll_kontrak_pembayaran = lelang.lls_kontrak_pembayaran;
			lelang.setKontrakPembayaran(kontrak);
			lelang.save();
			dok_pl_content.save();
			flash.success("Update Jenis Kontrak berhasil.");
		}
		else {
			flash.error("kontrak pembayaran tidak dikenal");
		}
		PengadaanLCtr.view(id);
	}

	/**
	 * upload dok survey oleh PPK saat proses persiapan non tender bukan buat adendum dokumen
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ, Group.PP})
	public static void surveyHargaPpk(Long id, String type){
		Paket_pl paket = Paket_pl.findById(id);
//		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPaketPl(paket);
		Active_user active_user = Active_user.current();
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
		List<BlobTable> survey = dokPersiapan.getDokSurvey();
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && active_user.isPpk();
//		renderArgs.put("pl", pl);
		renderArgs.put("paket", paket);
		renderArgs.put("survey", survey);
		renderArgs.put("editable", editable);
		renderArgs.put("isKuppbj", Active_user.current().isKuppbj());
		renderArgs.put("isDraf", paket.pkt_status.isDraft());
		renderTemplate("nonlelang/pl-survey-harga-persiapan.html");
	}

	/**
	 * Simpan Form Survey
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void surveyPpkSumbit(Long id, File file){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);// check otorisasi data non tender
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapan = paket.getDokPersiapan();
		if(dokPersiapan != null && dokPersiapan.isEditable(paket, newDate())){
			try{
				UploadInfo model = dokPersiapan.simpanSurvey(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload survey harga");
				result.put("result", "Kesalahan saat upload survey harga");
			}
		}else{
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}

		renderJSON(result);
	}

	@AllowAccess({Group.PPK})
	public static void hapusSurveyHargaPpk(Long id, Integer versi) {
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);// check otorisasi data lelang

		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapanPl dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null) {
			if(dokPersiapan.isEditable(paket, newDate())){
				if(dokPersiapan.isAdendum() && dokPersiapan.dp_survey == null){
					List<BlobTable> blobList = dokPersiapan.getDokSurvey();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));
					try{
						dokPersiapan.dp_survey = dokPersiapan.simpanBlobSpek(blobList);
						dokPersiapan.dp_modified = true;
						dokPersiapan.save();
						if (paket.ukpbj_id != null) {
							dokPersiapan.transferDokPersiapanToDokLelang();
						}
						result.put("success",true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success",false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else {
					if (dokPersiapan.dp_survey != null) {
						BlobTable blob = BlobTable.findById(dokPersiapan.dp_survey, versi);
						if (blob != null) {
							blob.delete();
						}
						if (CollectionUtils.isEmpty(dokPersiapan.getDokSurvey())) {
							dokPersiapan.dp_survey = null;
						}
					}
					dokPersiapan.dp_modified = true;
					dokPersiapan.save();

//					if (paket.ukpbj_id != null) {
//						dokPersiapan.transferDokPersiapanToDokLelang();
//					}
					result.put("success",true);
				}
			}else {
				result.put("message", "Anda tidak bisa melakukan Perubahan Data Dukung HPS");
			}
		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}
		renderJSON(result);

	}


	/**
	 * form Dokumen Survey Harga
	 * @param id
	 */
	@AllowAccess({Group.PANITIA, Group.PP})
	public static void surveyHarga(Long id, String type){
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		Paket_pl paket = Paket_pl.findById(pl.pkt_id);
		Active_user activeUser = Active_user.current();
		DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(paket);
		List<BlobTable> survey = dokPersiapan.getDokSurvey();
		renderArgs.put("pl", pl);
		renderArgs.put("paket", paket);
		renderArgs.put("survey", survey);
		renderArgs.put("surveyHarga", SurveyHargaPl.findByBlob(pl.lls_id));
//		renderArgs.put("editable", tahapStarted.isAllowAdendum() && activeUser.isPP());
		renderArgs.put("editable", pl.lls_status.isDraft());
		renderTemplate("nonlelang/pl-survey-harga.html");
	}


	@AllowAccess({Group.PP, Group.PANITIA})
	public static void uploadSurveyHarga(Long id, File file) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		otorisasiDataPl(pl);
//		Paket_pl paket = Paket_pl.findById(pl.pkt_id);
		Map<String, Object> result = new HashMap<>(1);
			try {
				UploadInfo info = SurveyHargaPl.simpanAttachSurvey(pl.lls_id, file);
				if (info == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(info);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error("Kesalahan saat upload survey harga: detail %s", e.getMessage());
				result.put("result", "Kesalahan saat upload survey harga");
				result.put("success", false);
			}
		renderJSON(result);
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void hapusSurveyHarga(Long id, Integer versi) {
		SurveyHargaPl surveyHargaPl = SurveyHargaPl.find("lls_id=?", id).first();
		if(surveyHargaPl != null && surveyHargaPl.survey_id_attachment != null){
			BlobTable blob = BlobTable.findById(surveyHargaPl.survey_id_attachment, versi);
			if (blob != null)
				blob.delete();

			if (surveyHargaPl.survey_id_attachment != null && surveyHargaPl.getDokumen().isEmpty()) {
				surveyHargaPl.survey_id_attachment = null;
			}
			surveyHargaPl.delete();
		}

	}


}
