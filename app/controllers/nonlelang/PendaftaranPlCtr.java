package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import models.agency.Anggaran;
import models.agency.Paket_pl_lokasi;
import models.common.Active_user;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_detil;
import models.nonlelang.common.TahapNowPl;
import models.rekanan.Rekanan;
import models.secman.Group;
import utils.BlacklistCheckerUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Lambang on 2/13/2017.
 */
public class PendaftaranPlCtr extends BasicCtr {

    @AllowAccess({Group.REKANAN})
    public static void index(Long id, boolean setuju) throws IOException, InterruptedException, ExecutionException {
        Long rekananId = Active_user.current().rekananId;
        Pl_detil pl = Pl_detil.findById(id);
        PesertaPl peserta = PesertaPl.find("lls_id=? and rkn_id=?", pl.lls_id, rekananId).first();
        boolean allowed = TahapNowPl.isAllowPendaftaran(pl.lls_id) && (peserta == null);
        List<Anggaran> anggaranList = Anggaran.findByPaketPl(pl.pkt_id);
        List<Paket_pl_lokasi> lokasiList = Paket_pl_lokasi.find("pkt_id=?", pl.pkt_id).fetch();
        String inaproc_url = BasicCtr.INAPROC_URL + "/daftar-hitam";

        render("nonlelang/pendaftaranPesertaPl.html", pl, allowed, anggaranList, lokasiList, inaproc_url);
    }

    public static void submit(Long id, boolean setuju) {
        checkAuthenticity();
        Long rekananId = Active_user.current().rekananId;
        if (setuju) {
            PesertaPl.daftarPl(id, rekananId);
        }
        BerandaCtr.nonLelang();
    }

    public static Integer pengecekanBlacklist(Long llsId){

        Active_user active_user = Active_user.current();

        Rekanan rekanan = Rekanan.findById(active_user.rekananId);

        return BlacklistCheckerUtil.checkBlacklistStatusPl(rekanan, null, llsId, 0);

    }

}
