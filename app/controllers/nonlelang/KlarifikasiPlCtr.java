package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.agency.Paket_pl;
import models.agency.Panitia;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.nonlelang.EvaluasiPl;
import models.nonlelang.NilaiEvaluasiPl;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.common.TahapNowPl;
import models.secman.Group;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.Router;
import utils.LogUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 10/11/2018
 */
public class KlarifikasiPlCtr extends BasicCtr {

    public static final String TAG = "KlarifikasiPlCtr";
    public static final Map<String, String> MAP_KLARIFIKASI;

    static {
        MAP_KLARIFIKASI = new HashMap<>();
        MAP_KLARIFIKASI.put("UNDANGAN_PEMBUKTIAN", "Pembuktian Kualifikasi");
        MAP_KLARIFIKASI.put("UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS_HARGA", "Klarifikasi Administrasi, Kualifikasi, Teknis dan Harga");
        MAP_KLARIFIKASI.put("UNDANGAN_HARGA", "Klarifikasi Harga");
        MAP_KLARIFIKASI.put("UNDANGAN_KUALIFIKASI", "Klarifikasi Kualifikasi");
        MAP_KLARIFIKASI.put("UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS", "Klarifikasi Administrasi, Kualifikasi dan Teknis");
        MAP_KLARIFIKASI.put("UNDANGAN_ADMIN_TEKNIS", "Klarifikasi Administrasi dan Teknis");
        MAP_KLARIFIKASI.put("UNDANGAN_TEKNISREVISI_HARGA", "Klarifikasi Teknis (Revisi) dan Harga");
        MAP_KLARIFIKASI.put("UNDANGAN_VERIFIKASI", "Verifikasi");
    }

    /**
     * @param id peserta id
     * @param jenis jenis undangan/pemberitahuan*/
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void kirimPesan(Long id, int jenis) {
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPesertaPl(peserta); // check otorisasi data peserta
        Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
        JenisEmail jenisEmail = JenisEmail.getById(jenis);
        renderArgs.put("peserta", peserta);
        renderArgs.put("pl", pl);
        renderArgs.put("tahapStarted", pl.getTahapStarted());
        renderArgs.put("jenisUndangan", jenis);
        //renderArgs.put("namaPanitia", pl.getPanitia().pnt_nama);
        renderArgs.put("list", MailQueueDao.findUndanganEvaluasi(pl.lls_id, peserta.rkn_id, jenis));
        renderArgs.put("messageTypes", JenisEmail.JenisPesan.values());
        renderArgs.put("key", jenisEmail.name());

        Router.ActionDefinition backUrl = Router.reverse("nonlelang.EvaluasiPlCtr.detail").add("id",peserta.psr_id).addRef("kualifikasi");

        renderArgs.put("backUrl", backUrl);
        renderTemplate("nonlelang/evaluasi/kirim-undangan-verifikasi-pl.html");
    }

    private static String getKeyMessage(Pl_seleksi pl, PesertaPl peserta) {
        TahapNowPl tahapNow = pl.getTahapNow();
        EvaluasiPl harga = EvaluasiPl.findHarga(pl.lls_id);
        EvaluasiPl admin = EvaluasiPl.findNCreateAdministrasi(pl.lls_id);

        NilaiEvaluasiPl nilai_admin = NilaiEvaluasiPl
                .findNCreateBy(admin.eva_id, peserta,EvaluasiPl.JenisEvaluasi.EVALUASI_ADMINISTRASI);

        EvaluasiPl teknis = EvaluasiPl.findTeknis(pl.lls_id);
        NilaiEvaluasiPl nilai_teknis = null;
        if (teknis != null) {
            nilai_teknis = NilaiEvaluasiPl.findBy(teknis.eva_id, peserta.psr_id);
        }
        EvaluasiPl kualifikasi = pl.isPenunjukanLangsungNew()
                ? EvaluasiPl.findNCreateKualifikasi(pl.lls_id)
                : EvaluasiPl.findKualifikasi(pl.lls_id);
        NilaiEvaluasiPl nilai_kualifikasi = null;
        if (kualifikasi != null) {
            nilai_kualifikasi = NilaiEvaluasiPl.findBy(kualifikasi.eva_id, peserta.psr_id);
        }
        if (tahapNow.isPembuktian()) {
            return "PEMBUKTIAN_KUALIFIKASI";
        }
        
        return "";
    }

    @AllowAccess({Group.PANITIA, Group.PP})
    public static void submit(Long id, int jenisUndangan,
                              @Required @As(binder= DatetimeBinder.class) Date waktu,
                              @Required @As(binder=DatetimeBinder.class) Date sampai, @Required String tempat,
                              @Required String dibawa, @Required String hadir,
                              String key
    ) {
        checkAuthenticity();
        Date now = newDate();
        if (waktu != null && waktu.before(now)) {
            validation.addError("waktu", Messages.get("flash.wyamsl"));
        }
        if (waktu != null && sampai != null && sampai.before(waktu)) {
            validation.addError("sampai", Messages.get("flash.wsmwm"));
        }
        Integer tipePesan = params.get("tipe_pesan", Integer.class);
        LogUtil.debug(TAG, "message type: " + tipePesan);
        if (tipePesan == null || tipePesan <= 0) {
            validation.addError("tipe_pesan",Messages.get("flash.gkmptpyak"));
        }
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error(Messages.get("flash.gkmcia"));
            kirimPesan(id, jenisUndangan);
        }
        try {
            PesertaPl peserta = PesertaPl.findById(id);
            otorisasiDataPl(peserta.lls_id); // check otorisasi data nonlelang
            String namaPaket = Paket_pl.getNamaPaketFromlelang(peserta.lls_id);
            String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
            MailQueueDao.kirimPesanKlarifikasi(
                    peserta.getRekanan(), peserta.lls_id, namaPaket, namaPanitia,
                    waktu, sampai, tempat, dibawa,
                    hadir, jenisUndangan, tipePesan, MAP_KLARIFIKASI.get(key));
            flash.success(Messages.get("flash.pbt"));
        } catch(Exception e) {
            flash.error(Messages.get("flash.tktpp"));
            Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
        }
        kirimPesan(id, jenisUndangan);
    }

}

