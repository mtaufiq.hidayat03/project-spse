package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.*;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.Instansi;
import models.common.Kategori;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.nonlelang.*;
import models.nonlelang.common.TahapNowPl;
import models.secman.Group;
import models.sirup.PaketSirup;
import models.sso.common.Kabupaten;
import org.apache.commons.collections4.CollectionUtils;
import play.Logger;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.mvc.Http.Cookie;
import play.mvc.Router;
import play.utils.LogUtils;
import utils.LogUtil;

import java.util.*;

public class PaketPlCtr extends BasicCtr {

	public static final String TAG = "PaketPlCtr";
	private static List<String> STEPPERS = Arrays.asList(Messages.get("paket.data_paket"), Messages.get("paket.dokumen_persiapan"));
	
	@AllowAccess({Group.PP, Group.PPK,Group.KUPPBJ,Group.PANITIA})
	public static void index(Long ppId, Integer status){
		if(status == null)
			renderArgs.put("status", Paket_pl.StatusPaket.SEDANG_LELANG); // default status paket
		Active_user user = Active_user.current();
		Map<String, Object> params = new HashMap<>();
		String url;
		String templateUrl = "nonlelang/paket/paket-nonlelang-list.html";
		if(user.isPpk()){
			params.put("ppkId", user.ppkId);
			params.put("status", status);
			url = Router.reverse("DataTableCtr.paketPpkNonTender", params).url;
		} else if(user.isKuppbj()) {
			params.put("ukpbjId",user.ukpbjId);
			params.put("status", status);
			url = Router.reverse("DataTableCtr.paketNonKuppbj", params).url;
			templateUrl = "nonlelang/paket/paket-list-kuppbj.html";
			renderArgs.put("isAllowCreateKonsolidasi", true);
		} else if(user.isPP()){
			params.put("ppId", user.ppId);
			params.put("status", status);
			url = Router.reverse("DataTableCtr.paketPp", params).url;
		} else {
			params.put("panitiaId", user.isPanitia());
			params.put("status", status);
			url = Router.reverse("DataTableCtr.PengadaanLPp", params).url;
		}
		renderArgs.put("ppId", ppId);
		renderArgs.put("url", url);
		renderArgs.put("isAllowCreate", user.isPpk());
		renderTemplate(templateUrl);
	}

	@AllowAccess({Group.PPK})
	public static void indexNonSpk(Long ppkId, Integer status){
		renderTemplate("nonSpk/paket/paket-non-spk-list.html",
				getPencatatanPresenter(ppkId, status, Active_user.current()));
	}

	private static Map<String, Object> getPencatatanPresenter(Long ppkId, Integer status, Active_user user) {
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> params = new HashMap<>();
		String url;
			params.put("ppkId", user.ppkId);
			params.put("status", status);
			url = Router
					.reverse("DataTableCtr.paketPpkPencatatan", params)
					.toString();
		map.put("url", url);
		map.put("allowCreate", user.isPpk());
		return map;
	}

	@AllowAccess({Group.PANITIA, Group.PP})
	public static void indexPenunjukanLangsung(Long panitiaId, Integer status){
		if(status == null)
			renderArgs.put("status", Paket_pl.StatusPaket.SEDANG_LELANG); // default status paket
		Active_user user = Active_user.current();
		Map<String, Object> params = new HashMap<>();
		String url;
		String templateUrl = "penunjukanLangsung/paket/paket-penunjukan-langsung-list.html";

			params.put("ppkId", user.ppkId);
			params.put("status", status);
			url = Router.reverse("DataTableCtr.paketPenunjukanLangsung", params).url;

		renderArgs.put("panitiaId", panitiaId);
		renderArgs.put("url", url);
		renderArgs.put("isAllowCreate", user.isPpk());
		renderTemplate(templateUrl);
	}

	@AllowAccess({Group.PP, Group.PPK, Group.KUPPBJ})
	public static void view_pl(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		renderArgs.put("paket", paket);
		renderArgs.put("pp", paket.getPp());
		renderArgs.put("lokasiList", Paket_pl_lokasi.find("pkt_id=?", paket.pkt_id).fetch());
		renderArgs.put("anggaranList", Anggaran.findByPaketPl(id));
		renderTemplate("nonlelang/paket/paket-pl-view.html");
	}

	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void edit(Long id){
		LogUtils.debug("TAG", "EDIT PAGE");
		Paket_pl paket = Paket_pl.findById(id);
		Kategori kategori = paket.kgr_id;
		Active_user user = Active_user.current();
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		Integer currentStepper = params.get("step", Integer.class);
		if (currentStepper == null)
			currentStepper = 1;
		renderArgs.put("draft", !paket.getPlSeleksi().isLelangV3() && paket.pkt_status.isDraft());
		renderArgs.put("currentStep", currentStepper);
		renderArgs.put("steppers", STEPPERS);
		renderArgs.put("kategori", kategori);
		Pl_seleksi pl = Pl_seleksi.findByPaket(paket.pkt_id);
		if(currentStepper != null && currentStepper == 2) {
			renderArgs.put("paket", paket);
			if(pl != null){
				renderArgs.put("kontrak_pembayaran", pl.lls_kontrak_pembayaran);
			}
			DokPersiapanPl dokPersiapanPl = paket.getDokPersiapan();
			renderArgs.put("paket", paket);
			renderArgs.put("pl", pl);
			renderArgs.put("dokPersiapan", dokPersiapanPl);
			renderArgs.put("surveyHarga", SurveyHargaPl.findByBlob(pl.lls_id));
			renderArgs.put("isPpk", user.isPpk());
			renderArgs.put("isKuppbj", user.isKuppbj());
			renderArgs.put("isPanitia", user.isPanitia());
			renderArgs.put("isPp", user.isPP());
			renderArgs.put("isKuppbjEditable", paket.getPanitia()==null);
			renderArgs.put("allowToShowSaveButton", paket.isEditable() && dokPersiapanPl != null && dokPersiapanPl.dp_spek != null
					&& dokPersiapanPl.dp_sskk_attachment != null);
			renderArgs.put("saveButtonTitle", user.isPpk() && paket.showExtendedPpkInput() ? "Simpan dan Membuat Paket" : "Simpan");
			renderArgs.put("historyPokja", History_paket_pl_pokja.findByPaket(id));
			renderArgs.put("historyPp", History_paket_pp.findByPaket(id));
			renderTemplate("/nonlelang/paket/paket-pl-edit-part2.html");

		}else{
			//cookie
			Cookie cookie=request.cookies.get("paket_edit_propinsi_id");
			Long propinsiId=null;
			if(cookie!=null && !CommonUtil.isEmpty(cookie.value))
				propinsiId=Long.parseLong(cookie.value);
			paket.getPanitia();
			paket.getPp();
			paket.withLocations();
			if (CommonUtil.isEmpty(paket.getPaketLocations())) {
				Paket_pl_lokasi pl_lokasi = new Paket_pl_lokasi();
				if (propinsiId != null) {
					// default propinsi
					List<Kabupaten> listKab = Kabupaten.findByPropinsi(propinsiId);
					if (!listKab.isEmpty())
						pl_lokasi.kbp_id = listKab.get(0).kbp_id;
				}
				paket.setPaketLocations(new ArrayList<>(1));
				paket.getPaketLocations().add(pl_lokasi);
			}
			paket.getPaketAnggarans();
			if (CollectionUtils.isEmpty(paket.getPaketAnggarans())) {
				paket.paketAnggarans = new ArrayList<>(1);
				paket.paketAnggarans.add(new Paket_anggaran_pl());
			}
			renderArgs.put("paket", paket);
			if(paket.isKonsolidasi()) {
				renderArgs.put("konsolidasiList", Paket_pl.findSimpleByKonsolidasiId(paket.pkt_id));
			}
			Boolean isTenderUlang = pl!=null && pl.lls_versi_lelang>1;
			renderArgs.put("isPpk", user.isPpk());
			renderArgs.put("isTenderUlang", isTenderUlang);
			LogUtils.debug("TAG", "isPPK = %b", user.isPpk());
			LogUtils.debug("TAG", "isTenderUlang= %b", isTenderUlang);
			LogUtils.debug("TAG", "lls_versi_lelang= %d", pl.lls_versi_lelang);
			renderTemplate("nonlelang/paket/paket-pl-edit-part1.html");
		}

	}
	
	@AllowAccess({Group.PP, Group.PPK, Group.KUPPBJ, Group.PANITIA})
	public static void simpan (Long id, Paket_pl paket, Paket_pl_lokasi[] lokasi, Integer step){
		checkAuthenticity();
		Active_user user = Active_user.current();
		Paket_pl obj = Paket_pl.findById(id);
		otorisasiDataPaketPl(obj); // check otorisasi data paket
		if (!obj.isEditable() || (user.isKuppbj() && !obj.isKonsolidasi())) {
			redirectToStepTwo(id);
		}

		if(obj.getPanitia()!=null && obj.ukpbj_id!=paket.ukpbj_id){
			flash.error(Messages.get("flash.kupbj_tdk_ubh"));
		}
		if(Paket_anggaran_pl.find("pkt_id=? and ppk_id is null", id).fetch().size() > 0){
			//ppk wajib diisi untuk setiap paket anggaran
			validation.addError("paket.ppk", "PPK wajib diisi");
		}

		for (int i = 0; i < lokasi.length; i++) {
			if (!validation.required(lokasi[i].kbp_id).ok) {
				validation.addError("paket_lokasi_" + i + ".kbp_id", "Lokasi wajib diisi", "var");
			}
			if (!validation.required(lokasi[i].pkt_lokasi).ok) {
				validation.addError("paket_lokasi_" + i + ".pkt_lokasi", "Detail Lokasi wajib diisi", "var");
			}
		}

		if (validation.hasErrors()) {			
			params.flash(); // add http parameters to the flash scope
			flash.error("Data Paket gagal disimpan. Periksa kembali inputan Anda!");
			validation.keep();
			edit(id);
			// re-attach attributes and render edit page with new object
		}
		obj.pkt_no_spk = paket.pkt_no_spk;
		if(obj.pkt_status.isDraft()){
			obj.pkt_nama = paket.pkt_nama.replace("", "").replace("\"", "");
		}
		Paket_pl.simpanBuatPaket(obj, lokasi);
		redirectToStepTwo(id);
	}

	private static void redirectToStepTwo(Long id) {
		redirect(Router.reverse("nonlelang.PaketPlCtr.edit").add("id", id).add("step", 2).url);
	}

	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void simpanPartTwo(Long id, Integer kontrak_pembayaran){
		checkAuthenticity();
		Paket_pl obj = Paket_pl.findById(id);
		Active_user user = Active_user.current();
		if(user.isKuppbj() && !obj.isKonsolidasi()){
			if(obj.getPanitia() == null){
				flash.error("Belum memilih Pokja Pemilihan!");
				redirectToStepTwo(id);
			}
			flash.success("&{'flash.data_paket_telah_tersimpan'}");
			edit(id);
		}
		if (obj.getPpName() == null && obj.ukpbj_id == null && obj.isFlag43()) {
			validation.addError("penanggung_jawab", "Silakan pilih Penanggung Jawab terlebih dahulu.", "var");
		}
		if (validation.hasErrors()) {
			flash.error("Data Paket gagal disimpan. Periksa kembali inputan Anda!");
			params.flash(); // add http parameters to the flash scope
			validation.keep();
			redirectToStepTwo(id);

		}else{
			if (obj.ukpbj_id != null || obj.pp_id != null || obj.isFlag43()) {
				DokPersiapanPl dokPersiapan = DokPersiapanPl.findLastByPaket(obj.pkt_id);
				dokPersiapan.transferDokPersiapanToDokLelang();
			}
			obj.save();
			Pl_seleksi pl_seleksi = Pl_seleksi.findByPaket(obj.pkt_id);
			if(kontrak_pembayaran != null)
				pl_seleksi.lls_kontrak_pembayaran = kontrak_pembayaran;
			if(pl_seleksi.isPenunjukanLangsung()) {
				pl_seleksi.lls_flow_new = 1;
				pl_seleksi.mtd_kualifikasi = 0;
			}
			else {
				pl_seleksi.mtd_kualifikasi = 1;
			}
				pl_seleksi.save();
			flash.success("&{'flash.data_paket_telah_tersimpan'}");
			edit(id);
		}
	}

	@AllowAccess({Group.PPK})
	public static void editNonSpk(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		Active_user user = Active_user.current();
		notFoundIfNull(paket);
		otorisasiDataPaketNonSpk(paket); // check otorisasi data paket
		//cookie
		Cookie cookie=request.cookies.get("paket_edit_propinsi_id");
		Long propinsiId=null;
		if(cookie!=null && !CommonUtil.isEmpty(cookie.value))
			propinsiId=Long.parseLong(cookie.value);
		paket.getPpk();
		paket.withLocations();
		if (CommonUtil.isEmpty(paket.getPaketLocations())) {
			Paket_pl_lokasi pl_lokasi = new Paket_pl_lokasi();
			if (propinsiId != null) {
				// default propinsi
				List<Kabupaten> listKab = Kabupaten.findByPropinsi(propinsiId);
				if (!listKab.isEmpty())
					pl_lokasi.kbp_id = listKab.get(0).kbp_id;
			}
			paket.setPaketLocations(new ArrayList<>(1));
			paket.getPaketLocations().add(pl_lokasi);
		}
		paket.getPaketAnggarans();
		if(CommonUtil.isEmpty(paket.getAnggarans())) {
			paket.setAnggarans(new ArrayList<>(1));
			paket.getAnggarans().add(new Anggaran());
		}
		renderArgs.put("isPpk", user.isPpk());
		renderArgs.put("paket", paket);
		renderArgs.put("draft", paket.pkt_status.isDraft());
		renderTemplate("nonlelang/paket/paket-non-spk-edit.html");
	}

	@AllowAccess({Group.PPK})
	public static void simpanNonSpk (Long id, Paket_pl paket, Paket_pl_lokasi[] lokasi){
		checkAuthenticity();
		Paket_pl obj = Paket_pl.findById(id);
		otorisasiDataPaketNonSpk(obj); // check otorisasi data paket

		List<Paket_pl_lokasi> paketLokasiList = new ArrayList<>(Arrays.asList(lokasi));

		paketLokasiList.removeIf(current -> current.pkt_lokasi == null && current.kbp_id == null && current.pkl_id != null);

		lokasi = paketLokasiList.toArray(new Paket_pl_lokasi[0]);

		validateLokasi(lokasi);

		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep();

			List<Paket_pl_lokasi> lokasiList = new ArrayList<>();

			for (Paket_pl_lokasi aLokasi : lokasi) {

				Paket_pl_lokasi paketPlLokasi = new Paket_pl_lokasi();
				paketPlLokasi.kbp_id = aLokasi.kbp_id;
				paketPlLokasi.pkt_lokasi = aLokasi.pkt_lokasi;
				paketPlLokasi.pkt_lokasi_versi = 1;

				lokasiList.add(paketPlLokasi);
			}

			List<Anggaran> anggaranList = Anggaran.findByPaketPl(id);
			if(CommonUtil.isEmpty(anggaranList)) {
				anggaranList = new ArrayList<>(1);
				anggaranList.add(new Anggaran());
			}
			String instansiId = null;
			if(paket.stk_id != null) {
				Satuan_kerja satker = Satuan_kerja.findById(paket.stk_id);
				instansiId = satker.instansi_id;
			}

			boolean draft = paket.pkt_status.isDraft();
			renderArgs.put("paket", paket);
			renderArgs.put("lokasiList", lokasiList);
			renderArgs.put("anggaranList", anggaranList);
			renderArgs.put("instansiId", instansiId);
			renderArgs.put("draft", draft);
			renderTemplate("nonlelang/paket/paket-non-spk-edit.html");

		} else {
			if(obj.isPaketKonsolidasi() && obj.pkt_status.isDraft()){
				obj.pkt_nama = paket.pkt_nama;
			}
			if(obj.pkt_status.isDraft()){
				obj.pkt_nama = paket.pkt_nama.replace("", "").replace("\"", "");
			}
			Paket_pl.simpanBuatPaketNonSpk(obj, lokasi);
			flash.success("&{'flash.data_paket_telah_tersimpan'}");
		}
		editNonSpk(id);

	}

	private static void validateLokasi(Paket_pl_lokasi[] lokasi) {
		for (int i=0;i< lokasi.length;i++) {
			if(!validation.required(lokasi[i].kbp_id).ok)
				validation.addError("paket_lokasi_"+i+".kbp_id", "Lokasi wajib diisi", "var");

			if(!validation.required(lokasi[i].pkt_lokasi).ok)
				validation.addError("paket_lokasi_"+i+".pkt_lokasi", "Detail Lokasi wajib diisi", "var");
		}
	}


	/**
	 * tampilan informasi rencana pengadaan
	 */
	@AllowAccess({Group.PPK})
	public static void rencana_nonlelang(Long paketId, String instansiId, Long satkerId, Integer tahun, Integer metodePemilihan){
		int tahunNow = DateUtil.getTahunSekarang();
		renderArgs.put("paketId", paketId);
		if(CommonUtil.isEmpty(instansiId)) {
			//dapatkan id Instansi dari cookies
			Cookie cookieInstansi=request.cookies.get("sirup_instansi_id");	
			if(cookieInstansi!=null && !CommonUtil.isEmpty(cookieInstansi.value))
				instansiId=cookieInstansi.value;
		}
		if(satkerId == null) {
			Cookie cookieSatker=request.cookies.get("sirup_satker_id");	
			if(cookieSatker != null && !CommonUtil.isEmpty(cookieSatker.value))
				satkerId=Long.parseLong(cookieSatker.value);			
		}
		if(tahun == null) {		
			Cookie cookieTahun=request.cookies.get("sirup_tahun");
			if(cookieTahun!=null && !cookieTahun.value.equals("")){
				tahun=Integer.parseInt(cookieTahun.value);
			}else{
				tahun = tahunNow;
			}
		}

		if(metodePemilihan == null) {
			Cookie cookieMetodePemilihan =request.cookies.get("sirup_metode_pemilihan");
			if(cookieMetodePemilihan != null)
				metodePemilihan= Integer.parseInt(cookieMetodePemilihan.value);
		}
		renderArgs.put("tahun", tahun);
		renderArgs.put("instansiId", instansiId);
		renderArgs.put("satkerId", satkerId);
		renderArgs.put("metodePemilihan", metodePemilihan);
		renderTemplate("nonlelang/paket/rencana-paket-pl.html");
		
	}

	@AllowAccess({Group.PPK})
	public static void hapus(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);

		if(paket.ukpbj_id != null || paket.getPaketPp() != null){
			flash.error(Messages.get("flash.mptbhkk"));
			index(null,null);
		}

		try {
			paket.delete();
		} catch (Exception ex) {
			Logger.info(ex,"Error delete " + ex.getLocalizedMessage());
			flash.error(Messages.get("flash.gagal_menghapus_paket_non_tender")+" " + paket.pkt_id);
			index(null, null);
		}

		index(null, null);
	}

	@AllowAccess({Group.PP, Group.PPK, Group.PANITIA})
	public static void hapus_rup(Long id, Long rupId){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		Paket_satker_pl paket_satker = Paket_satker_pl.find("pkt_id=? AND rup_id=? ", id, rupId).first();
		PaketSirup rup = PaketSirup.findById(rupId);
		Anggaran anggaran;
		paket.pkt_nama = paket.pkt_nama.replace(',' +rup.nama,"");
		for(PaketSirup.PaketSirupAnggaran rup_anggaran:rup.paket_anggaran_json) {
			anggaran = Anggaran.find("ang_koderekening=? AND ang_tahun=? AND stk_id=? AND ang_id in (SELECT ang_id FROM ekontrak.paket_anggaran WHERE pkt_id=?)",
					rup_anggaran.mak, rup_anggaran.tahun_anggaran_dana, paket_satker.stk_id, paket.pkt_id).first();
			if(anggaran != null) {
				paket.pkt_pagu = paket.pkt_pagu - anggaran.ang_nilai;
				Paket_anggaran_pl.delete("pkt_id=? AND ang_id=?", paket.pkt_id, anggaran.ang_id);
				anggaran.delete();
			}
		}
		paket_satker.delete();
		paket.save();
		edit(id);
	}

	@AllowAccess({Group.PPK})
	public static void rencanaNonSpk(Long paketId,String instansiId, Long satkerId, Integer tahun, Integer metodePemilihan){
		int tahunNow = DateUtil.getTahunSekarang();
		renderArgs.put("paketId", paketId);
		if(CommonUtil.isEmpty(instansiId)) {
			//dapatkan id Instansi dari cookies
			Cookie cookieInstansi=request.cookies.get("sirup_instansi_id");
			if(cookieInstansi!=null && !CommonUtil.isEmpty(cookieInstansi.value))
				instansiId=cookieInstansi.value;
		}
		if(satkerId == null) {
			Cookie cookieSatker=request.cookies.get("sirup_satker_id");
			if(cookieSatker != null && !CommonUtil.isEmpty(cookieSatker.value))
				satkerId=Long.parseLong(cookieSatker.value);
		}
		if(tahun == null) {
			Cookie cookieTahun=request.cookies.get("sirup_tahun");
			if(cookieTahun!=null && !cookieTahun.value.equals("")){
				tahun=Integer.parseInt(cookieTahun.value);
			}else{
				tahun = tahunNow;
			}
		}
		if(metodePemilihan == null) {
			Cookie cookieMetodePemilihanPenyedia=request.cookies.get("sirup_metode_pemilihan");
			if(cookieMetodePemilihanPenyedia != null)
				metodePemilihan= Integer.parseInt(cookieMetodePemilihanPenyedia.value);
		}
		renderArgs.put("tahun", tahun);
		renderArgs.put("instansiId", instansiId);
		renderArgs.put("satkerId", satkerId);
		renderArgs.put("metodePemilihan", metodePemilihan);
		renderTemplate("nonSpk/paket/rencana-paket-non-spk.html");

	}
	
	/**
	 * tampilan popup informasi detail rencana paket tertentu berdasarkan kode rup
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void rencanaViewLangsung(Long id, Long paketId){
		PaketSirup paket = PaketSirup.findById(id);
		renderArgs.put("paketId", paketId);
		renderArgs.put("paket", paket);
		if(!CollectionUtils.isEmpty(paket.paket_anggaran_json)) {
			renderArgs.put("anggaranList", paket.paket_anggaran_json);
		}
		renderArgs.put("satker", Satuan_kerja.find("rup_stk_id = ?", paket.rup_stk_id).first());
		List<Panitia> panitiaList = Panitia.findByPegawai(Active_user.current().pegawaiId);
		boolean isOSD = ConfigurationDao.isOSD(BasicCtr.newDate());
		renderArgs.put("isOSD", isOSD);
//		Map<Long,List<String>> mapKepanitiaan = new HashMap<>();
		Map<Long,Boolean> mapBuatPaket = new HashMap<>();
/*		if(isOSD){
			for(Panitia panitia:panitiaList) {
				List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?", panitia.pnt_id).fetch();
				List<String> infoPanitia = new ArrayList<>();
				boolean canCreate = true;
				for(Anggota_panitia anggota:anggota_panitia){
					boolean validCert = false;
					String info = "tidak memiliki sertifikat aktif";
					Pegawai pegawai = Pegawai.findById(anggota.peg_id);
					if (pegawai.cer_id != null){
						Certificate certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id, JenisCertificate.CERT_PANITIA);

						if(certificate!=null && certificate.cer_sn!=null){
							if(certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
								if (!certificate.isCertExpired()) {
									info = certificate.getRangeCertificateValidDate();
									validCert = true;
								}
							}
						}
						if(certificate==null){
							info = "tidak dapat mengambil data sertifikat";
						}
					}
					canCreate = canCreate && validCert;
					infoPanitia.add(pegawai.peg_nama + " <strong>(" + info + ")</strong>");
				}
				mapKepanitiaan.put(panitia.pnt_id, infoPanitia);
				mapBuatPaket.put(panitia.pnt_id, canCreate);
			}
		}*/
		renderArgs.put("panitiaList", panitiaList);
		//renderArgs.put("mapKepanitiaan", mapKepanitiaan);
		renderArgs.put("mapBuatPaket", mapBuatPaket);
		renderArgs.put("isPanitia",Active_user.current().isPanitia());
		renderTemplate("nonlelang/paket/rencana-view-pl.html");

		}

	@AllowAccess({Group.PPK})
	public static void rencanaViewNonSpk(Long id, Long paketId){
		PaketSirup paket = PaketSirup.findById(id);
		renderArgs.put("paketId", paketId);
		renderArgs.put("paket", paket);
		if(!CollectionUtils.isEmpty(paket.paket_anggaran_json)) {
			renderArgs.put("anggaranList", paket.paket_anggaran_json);
		}
		renderArgs.put("satker", Satuan_kerja.find("rup_stk_id = ?", paket.rup_stk_id).first());
		Map<Long,List<String>> mapKepanitiaan = new HashMap<>();
		Map<Long,Boolean> mapBuatPaket = new HashMap<>();
		renderArgs.put("mapKepanitiaan", mapKepanitiaan);
		renderArgs.put("mapBuatPaket", mapBuatPaket);
		renderArgs.put("isPpk", Active_user.current().isPpk());
		renderTemplate("nonlelang/paket/rencana-view-non-spk.html");
	}

	//========================= BEGIN IMPLEMENT EDIT RUP UNTUK LELANG ULANG =========================

	/**
	 * tampilan informasi rencana pengadaan
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void editRencana(Long id, String instansiId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		int tahunNow = DateUtil.getTahunSekarang();
		renderArgs.put("paketId", id);
		if(CommonUtil.isEmpty(instansiId)) {
			//dapatkan id Instansi dari cookies
			Cookie cookieInstansi=request.cookies.get("sirup_instansi_id");
			if(cookieInstansi!=null && !CommonUtil.isEmpty(cookieInstansi.value))
				instansiId=cookieInstansi.value;
		}
		if(satkerId == null) {
			Cookie cookieSatker=request.cookies.get("sirup_satker_id");
			if(cookieSatker != null && !CommonUtil.isEmpty(cookieSatker.value))
				satkerId=Long.parseLong(cookieSatker.value);
		}
		if(tahun == null) {
			Cookie cookieTahun=request.cookies.get("sirup_tahun");
			if(cookieTahun!=null && !cookieTahun.value.equals("")){
				tahun=Integer.parseInt(cookieTahun.value);
			}else{
				tahun = tahunNow;
			}
		}

		if(metodePemilihan == null) {
			Cookie cookieMetodePemilihan =request.cookies.get("sirup_metode_pemilihan");
			if(cookieMetodePemilihan != null)
				metodePemilihan= Integer.parseInt(cookieMetodePemilihan.value);
		}
		renderArgs.put("tahun", tahun);
		renderArgs.put("instansiId", instansiId);
		renderArgs.put("satkerId", satkerId);
		renderArgs.put("metodePemilihan", metodePemilihan);
		renderTemplate("nonlelang/paket/edit-rup.html");
	}

	/**
	 * tampilan popup informasi detail rencana paket tertentu berdasarkan kode rup
	 * @param id
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void editRencanaView(Long id, Long paketId) {
		PaketSirup paket = PaketSirup.findById(id);
		renderArgs.put("paketId", paketId);
		renderArgs.put("paket", paket);
		if(paketId != null) {
			Paket_pl paketObj = Paket_pl.findById(paketId);
			if(paketObj != null)
				renderArgs.put("allow_update", paket.pagu >= paketObj.pkt_hps); // allow update jika pagu rup > hps paket existing
		}
		if(!CollectionUtils.isEmpty(paket.paket_anggaran_json)) {
			renderArgs.put("anggaranList", paket.paket_anggaran_json);
		}
		renderArgs.put("satker", Satuan_kerja.find("rup_stk_id = ?", paket.rup_stk_id).first());
		List<Panitia> panitiaList = Panitia.findByPegawai(Active_user.current().pegawaiId);
		Map<Long,Boolean> mapBuatPaket = new HashMap<>();

		renderArgs.put("panitiaList", panitiaList);
		renderArgs.put("mapBuatPaket", mapBuatPaket);
		renderArgs.put("isPanitia",Active_user.current().isPanitia());
		renderTemplate("nonlelang/paket/edit-rencana-view.html");
	}

	// TODO: hapus ini
	/**
	 * Pembuatan paket dari rencana Pengadaan (RUP)
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void editRencanaBuatPaket(Long id, Long paketId, Long panitiaId) {
		checkAuthenticity();
		LogUtil.debug(TAG,"Update RUP");
		Paket_pl paket = Paket_pl.updatePaketRup(id, paketId, panitiaId);
		edit(paket.pkt_id);
	}


	//========================= END   IMPLEMENT EDIT RUP UNTUK LELANG ULANG =========================

	/**
	 * tampilan popup informasi detail rencana paket swakelola tertentu berdasarkan kode rup
	 * @param id
	 */
	
	@AllowAccess({Group.PPK})
	public static void buatPaket(Long id, Long paketId, Long panitiaId){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.buatPaketFromSirup(id, paketId, panitiaId);
		edit(paket.pkt_id);
		
	}

	@AllowAccess({Group.PPK})
	public static void buatPaketNonSpk(Long id, Long paketId, Long ppkId){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.buatPaketNonSpkFromSirup(id, paketId, ppkId);
		editNonSpk(paket.pkt_id);

	}

	@AllowAccess({Group.PPK})
	public static void pilihPp(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		Pl_seleksi pl = Pl_seleksi.findByPaket(id);
		renderArgs.put("paket", paket);
		boolean sedangLelang = paket.pkt_status.isSedangLelang();
		renderArgs.put("sedangLelang", sedangLelang);
		renderArgs.put("oldPp", paket.getPp());
		renderArgs.put("llsId", pl.lls_id);
//		renderArgs.put("backUrl", Router.reverse("nonlelang.PaketPlCtr.edit").add("id", paket.pkt_id).add("step", 2));
		renderTemplate("nonlelang/paket/paket-pp.html");
	}

	@AllowAccess({Group.PPK})
	public static void pilihUkpbj(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		renderArgs.put("paket", paket);
		if(paket.pkt_status.isSelesaiLelang()){
			Pl_seleksi pl = Pl_seleksi.findAktifByPaket(paket.pkt_id);
			renderArgs.put("pl", pl);
		}
		renderArgs.put("backUrl", Router.reverse("nonlelang.PaketPlCtr.edit").add("id", paket.pkt_id).add("step", 2));
		renderTemplate("nonlelang/paket/paket-ukpbj.html");
	}

	@AllowAccess({Group.PPK})
	public static void batalPilihPp(Long id, Long ppId){
		Paket_pl paket = Paket_pl.findById(id);
		Paket_pp.delete("pkt_id = ? and pp_id = ?",paket.pkt_id, ppId);

		redirectToStepTwo(id);
	}

	@AllowAccess({Group.PPK})
	public static void batalPilihUkpbj(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		paket.ukpbj_id = null;
		paket.save();

		redirectToStepTwo(id);
	}

	@AllowAccess({Group.PPK})
	public static void submit_pp(Long id , Long ppId, Long old_pp_id, String alasan){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);

		Pl_seleksi pl = Pl_seleksi.findByPaket(id);

		if(old_pp_id != null) {
			if (alasan == null) {
				//validasi
				flash.error("Anda belum memasukkan alasan mengganti Pejabat Pengadaan.");
				params.flash(); // add http parameters to the flash scope
				validation.keep();
				redirect(Router.reverse("nonlelang.PaketPlCtr.pp")
						.add("id", id)
						.url);
			}

			//Riwayat
			History_paket_pp history = new History_paket_pp();
			history.pkt_id = paket.pkt_id;
			history.pp_id = old_pp_id;
			history.alasan = alasan;
			history.peg_id = Active_user.current().pegawaiId;
			history.tgl_perubahan = newDate();
			history.save();
			//Pengecekan Persetujuan

			// Ganti Persetujuan
			if(!pl.lls_status.isDraft()){
				PersetujuanPl persetujuan = PersetujuanPl.findByPegawaiLelangAndJenis(pl.lls_id, PersetujuanPl.JenisPersetujuanPl.BATAL_LELANG);
				if(persetujuan == null){
					persetujuan = PersetujuanPl.findByPegawaiLelangAndJenis(pl.lls_id, PersetujuanPl.JenisPersetujuanPl.ULANG_LELANG);
				}
				if(persetujuan != null && !persetujuan.isApprove()){
					if(persetujuan.isSedangPersetujuan()){
						persetujuan.deleteAllByLelangAndJenis();
					}
				}
			}else{
				PersetujuanPl.deleteAllByLelangAndJenisPengumuman(pl.lls_id);
			}
		}


		Paket_pp paket_pp = Paket_pp.getByPktId(paket.pkt_id);
		if(paket_pp == null){
			paket_pp = new Paket_pp();
			paket_pp.pkt_id = paket.pkt_id;
		}
		paket_pp.pp_id = ppId;
		paket_pp.save();

		paket.pp_id = ppId;
		paket.save();
		redirectToStepTwo(id);
	}

	@AllowAccess({Group.PPK})
	public static void submit_ukpbj(Long id , Long ukpbj_id){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		paket.ukpbj_id = ukpbj_id;
		paket.save();

		redirectToStepTwo(id);
	}
	
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void ppk(Long id, Long ppk_id, Long angId, Long llsId, String insId) {
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		// List Instansi untuk filter ppk
		renderArgs.put("paket", paket);
		renderArgs.put("ang_id", angId);
		renderArgs.put("insId", insId);
		renderArgs.put("llsId", llsId);
		renderArgs.put("oldPpkId", ppk_id);
//		List <Instansi> instansis = Instansi.findAll();
		if(paket.pkt_status.isSedangLelang()) {
			Pl_seleksi pl = Pl_seleksi.findAktifByPaket(paket.pkt_id);
			renderArgs.put("pl", pl);
		}
		renderTemplate("nonlelang/paket/paket-pl-ppk.html");
		
	}

	@AllowAccess({Group.PANITIA, Group.PP})
	public static void alasanGantiPpk(Long id, Long oldPpkId, Long llsId, String insId, Long ppkId, Long ang_id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		Ppk oldPpk = Ppk.findById(oldPpkId);
		Ppk newPpk = Ppk.findById(ppkId);
		renderArgs.put("paket", paket);
		renderArgs.put("ppkId",ppkId);
		renderArgs.put("oldPpk",oldPpk);
		renderArgs.put("newPpk",newPpk);
		renderArgs.put("ang_id", ang_id);
		renderArgs.put("insId", insId);
		renderArgs.put("llsId", llsId);
		renderTemplate("nonlelang/paket/paket-ganti-ppk.html");
	}
	
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void submit_ppk(Long id, Long llsId, Long angId, Long ppkId, Long old_ppk_id, String alasan){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket
		//Long old_ppk_id = null;
        Ppk ppk = Ppk.findById(ppkId);
        if(angId != null){
			Paket_anggaran_pl paketAnggaranPl = Paket_anggaran_pl.find("pkt_id=? and ang_id=?", paket.pkt_id, angId).first();
			if(paketAnggaranPl != null && paketAnggaranPl.ppk_id != null){
				old_ppk_id = paketAnggaranPl.ppk_id;

			}
			paketAnggaranPl.ppk_id = ppk.ppk_id;
			paketAnggaranPl.save();
		}else{
			//Karena di 4.3 semua Anggaran otomatis dipegang oleh PPK yang bertanggung jawab atas paketnya
			Paket_anggaran_pl.updatePpkAllAnggaranByPaket(paket.pkt_id, ppkId);
		}

		History_paket_pl_ppk history = new History_paket_pl_ppk();
		history.pkt_id = paket.pkt_id;
		history.ppk_id = old_ppk_id;
		history.alasan = alasan;
		history.peg_id = Active_user.current().pegawaiId;
		history.tgl_perubahan = newDate();
		history.save();

		if(paket.isFlag43()){
			PaketPpkPl paketPpk = PaketPpkPl.findByPaketAndPpk(paket.pkt_id, old_ppk_id);
			paketPpk.ppk_id = ppkId;
			paketPpk.save();
			paketPpk.deleteOldPpkNew(old_ppk_id);
		}

		if(llsId != null){
			//update history lelang untuk perubahan ppk di fitur ubah lelang
			PengadaanLCtr.updateHistoryLelangForPpk(llsId, old_ppk_id);
//			LelangCtr.updateHistoryLelangForPpk(llsId, old_ppk_id);
		} else {
			Pl_seleksi pl = Pl_seleksi.findByPaket(paket.pkt_id);
			PengadaanLCtr.view(pl.lls_id);
//			edit(id);
		}
		
	}
	@AllowAccess({Group.KUPPBJ})
	public static void panitia(Long id){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);// check otorisasi data paket
		renderArgs.put("paket", paket);

		boolean sedangLelang = paket.pkt_status.isSedangLelang();
		renderArgs.put("sedangLelang", sedangLelang);
		renderArgs.put("oldPanitia", paket.getPanitia());
		renderArgs.put("llsId", paket.getPlSeleksi().lls_id);

		if(paket.pkt_status.isSelesaiLelang()){
			Pl_seleksi pl = Pl_seleksi.findAktifByPaket(paket.pkt_id);
			renderArgs.put("pl", pl);
		}

		renderTemplate("nonlelang/paket/paket-panitia.html");

	}

	@AllowAccess({Group.KUPPBJ})
	public static void submit_panitia(Long id, Long pntId, Long old_pnt_id, String alasan){
		checkAuthenticity();
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket);
		Pl_seleksi pl = paket.getPlSeleksi();

		if(old_pnt_id != null){
			if(alasan == null){
				//validasi
				flash.error("Anda belum memasukkan alasan mengganti Pokja.");
				params.flash(); // add http parameters to the flash scope
				validation.keep();
				redirect(Router.reverse("lelang.PaketPlCtr.panitia")
						.add("id", id)
						.url);
			}

			//Riwayat
			History_paket_pl_pokja history = new History_paket_pl_pokja();
			history.pkt_id = paket.pkt_id;
			history.pnt_id = old_pnt_id;
			history.alasan = alasan;
			history.peg_id = Active_user.current().pegawaiId;
			history.tgl_perubahan = newDate();
			history.save();

			//Pengecekan Persetujuan
			// Ganti Persetujuan
			if(paket.pkt_status.isDraft()){
				PersetujuanPl.deleteAllByLelangAndJenisPengumuman(pl.lls_id);
			}
			// Tahap penetapan pemenang
			TahapNowPl tahapNow = new TahapNowPl(pl.lls_id);
			if(paket.pkt_status.isSedangLelang() && tahapNow.isPenetapaPemenang() &&
					!PersetujuanPl.isApprove(pl.lls_id, PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG)){
				//Jika sedang persetujuan pemenang, reset persetujuannya
				if(pl.isSedangPersetujuanPemenang()){
					PersetujuanPl.deleteAllByLelangAndJenisPemenang(pl.lls_id);
				}

				//Jika sudah ada pemenang, reset pemenangnya
				List<NilaiEvaluasiPl> pemenang = Query.find("SELECT * FROM ekontra.nilai_evaluasi n " +
								"JOIN ekontrak.evaluasi e ON e.eva_id = n.eva_id WHERE e.lls_id = ? AND e.eva_jenis = ? AND n.nev_lulus = ?",
						NilaiEvaluasiPl.class,pl.lls_id, EvaluasiPl.JenisEvaluasi.EVALUASI_AKHIR,
						NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS).fetch();
				if(pemenang != null){
					for(NilaiEvaluasiPl nev : pemenang){
						nev.nev_lulus = NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
						nev.save();
					}
				}
			}
		}

		Paket_pl_panitia paketPanitia = Paket_pl_panitia.getByPktId(paket.pkt_id);
		if(paketPanitia == null){
			paketPanitia = new Paket_pl_panitia();
			paketPanitia.pkt_id = paket.pkt_id;
		}
		paketPanitia.pnt_id = pntId;
		paketPanitia.save();

		paket.pnt_id = pntId;
		paket.save();

		redirectToStepTwo(id);
	}

	@AllowAccess({Group.KUPPBJ})
	public static void alasanGantiPokja(Long id, Long oldPanitiaId, Long llsId, Long panitiaId){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket

		Panitia oldPanitia = Panitia.findById(oldPanitiaId);
		Panitia newPanitia = Panitia.findById(panitiaId);

		renderArgs.put("paket", paket);
		renderArgs.put("panitiaId",panitiaId);
		renderArgs.put("oldPanitia",oldPanitia);
		renderArgs.put("newPanitia",newPanitia);
		renderArgs.put("llsId", llsId);

		renderTemplate("nonlelang/paket/paket-ganti-pokja.html");
	}


	@AllowAccess({Group.PPK})
	public static void alasanGantiPp(Long id, Long oldPpId, Long llsId, Long ppId){
		Paket_pl paket = Paket_pl.findById(id);
		otorisasiDataPaketPl(paket); // check otorisasi data paket

		Pp oldPp = Pp.findById(oldPpId);
		Pp newPp = Pp.findById(ppId);

		renderArgs.put("paket", paket);
		renderArgs.put("ppId",ppId);
		renderArgs.put("oldPp",oldPp);
		renderArgs.put("newPp",newPp);
		renderArgs.put("llsId", llsId);

		renderTemplate("nonlelang/paket/paket-ganti-pp.html");
	}
}
