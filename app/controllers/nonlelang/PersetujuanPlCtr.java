package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.nonlelang.*;
import models.secman.Group;
import play.data.validation.Validation;

import java.util.concurrent.ExecutionException;

public class PersetujuanPlCtr extends BasicCtr {

    /**
     * Persetujuan Pemenang non tender
     * @param id
     * @param alasan
     * @param setuju
     */
    @AllowAccess({Group.PANITIA})
    public static void pemenang(Long id, String alasan, boolean setuju){
        checkAuthenticity();
        otorisasiDataPl(id);// check otorisasi data non tender
        Pl_seleksi pl = Pl_seleksi.findById(id);
        if (!setuju && CommonUtil.isEmpty(alasan)) {
            Validation.addError("alasan", "Alasan wajib diisi");
        }else if (!setuju && !CommonUtil.isEmpty(alasan) && alasan.length() <= 10) {
            Validation.addError("alasan", "Alasan minimal 10 karakter");
        }

        if(validation.hasErrors()) {
            validation.keep();
            params.flash();
            EvaluasiPLCtr.persetujuan(id);
        }
        else {
            try{
                simpanPersetujuanPemenang(id, alasan, setuju);
            }catch (Exception e){
                flash.error(e.getMessage());
            }
            if(pl.isPenunjukanLangsungNew()) {
                EvaluasiPLCtr.indexpl(id);
            }else{
                EvaluasiPLCtr.index(id);
            }

        }
    }

    private static void simpanPersetujuanPemenang(Long id, String alasan, boolean setuju){
        PersetujuanPl persetujuan = PersetujuanPl.findByPegawaiLelangAndJenis(id, PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG);

        if (!setuju && !CommonUtil.isEmpty(alasan)) {
            persetujuan.pst_alasan = alasan;
            persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.TIDAK_SETUJU; //jika ada alasan maka status tidak setuju
        }else if(setuju){
            persetujuan.pst_alasan = null;
            persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.SETUJU;
        }

        persetujuan.pst_tgl_setuju = BasicCtr.newDate();
        persetujuan.save();

        // tambahkan ke riwayat persetujuan
        Riwayat_PersetujuanPl.simpanRiwayatPersetujuan(persetujuan);

        if(PersetujuanPl.isApprove(id, PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG)){
            EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
            evaluasi.eva_status = EvaluasiPl.StatusEvaluasi.SELESAI;
            evaluasi.eva_tgl_setuju = newDate();
            evaluasi.save();
            for(NilaiEvaluasiPl nilai : NilaiEvaluasiPl.findBy(evaluasi.eva_id)){
                if(nilai.isLulus()){
                    PesertaPl peserta = PesertaPl.findById(nilai.psr_id);
                    peserta.is_pemenang = Integer.valueOf(1);
                    peserta.save();
                }
            }
        }
    }

    @AllowAccess({Group.PANITIA})
    public static void riwayatPersetujuan(Long pst_id) throws InterruptedException, ExecutionException{
        PersetujuanPl persetujuan = PersetujuanPl.findById(pst_id);
        otorisasiDataNonTender(persetujuan.pst_id);
        renderArgs.put("listRiwayatPersetujuan", Riwayat_PersetujuanPl.RiwayatPersetujuanPl.findList(persetujuan.peg_id, persetujuan.lls_id, persetujuan.pst_jenis));
        renderTemplate("nonlelang/riwayat-persetujuan.html");
    }

    @AllowAccess({Group.PP})
    public static void riwayatPersetujuanPp(Long pst_id) throws InterruptedException, ExecutionException{
        PersetujuanPl persetujuan = PersetujuanPl.findById(pst_id);
        otorisasiDataNonTender(persetujuan.pst_id);
        renderArgs.put("listRiwayatPersetujuan", Riwayat_PersetujuanPl.RiwayatPersetujuanPl.findPpList(persetujuan.peg_id, persetujuan.lls_id, persetujuan.pst_jenis));
        renderTemplate("nonlelang/riwayat-persetujuan-pp.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void batalPersetujuanPengumuman(Long llsId){
        Active_user active_user = Active_user.current();
        PersetujuanPl persetujuan = PersetujuanPl.findByPegawaiPengumuman(active_user.pegawaiId, llsId);
        persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.BELUM_SETUJU;
        persetujuan.pst_tgl_setuju = null;
        persetujuan.save();

        PengadaanLCtr.edit(llsId);
    }
}
