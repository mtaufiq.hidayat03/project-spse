package controllers.nonlelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.agency.DaftarKuantitas;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.Tahap;
import models.lelang.LDPContent;
import models.nonlelang.*;
import models.nonlelang.common.TahapStartedPl;
import models.secman.Group;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by Lambang on 8/24/2017.
 */
public class PesertaPlCtr extends BasicCtr {

    static final Template TEMPLATE_PL_RINCIAN_PENAWARAN = TemplateLoader.load("/kontrakpl/template/RincianPenawaranPl.html");
    static final Template TEMPLATE_SURAT_PENAWARAN = TemplateLoader.load("/nonlelang/dokumen/template/surat-penawaran.html");


    public static void index(Long id) {

        Pl_seleksi pl = Pl_seleksi.findById(id);

        TahapStartedPl tahapStarted = new TahapStartedPl(id);

        boolean hide = !tahapStarted.isShowPenyedia();

        render("nonlelang/peserta-pl.html", pl, hide, tahapStarted);

    }

    @AllowAccess({ Group.PANITIA, Group.AUDITOR, Group.PPK , Group.REKANAN, Group.PP})
    public static void penawaran(Long id) {
        otorisasiDataPl(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        // peserta yang tidak menawar tidak bisa akses data penawaran peserta
        renderArgs.put("not_allowed", active_user.isRekanan() && !DokPenawaranPl.isAnyPenawaran(id, active_user.rekananId));
        Group group = active_user.group;
        Pl_seleksi pl = Pl_seleksi.findById(id);
        TahapStartedPl tahapStarted = new TahapStartedPl(id);
        boolean hide = !tahapStarted.isShowPenyedia();

        render("nonlelang/penawaran-peserta-pl.html", pl, hide, tahapStarted, group, active_user);

    }

    /**
     * cetak rincian Penawaran Peserta epl
     */
    @AllowAccess({Group.PP, Group.PANITIA, Group.AUDITOR,Group.PPK})
    public static void cetak_rencian_penawaran(long id){
        PesertaPl pesertapl = PesertaPl.findById(id);
        otorisasiDataPl(pesertapl.lls_id); //cek otorisasi data pl
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("format" , new FormatUtils());
        Pl_seleksi pl = Pl_seleksi.findById(pesertapl.lls_id);
        String namaPeserta = pesertapl.getNamaPeserta();
        String namaPaket = pl.getNamaPaket();
        DaftarKuantitas dkh = pesertapl.dkh != null ? pesertapl.dkh:null;
        model.put("dkh", dkh);
        model.put("namaPeserta", namaPeserta);
        model.put("namaPaket", namaPaket);
        model.put("pesertapl", pesertapl);
        response.contentType = "application/pdf";
        InputStream is = HtmlUtil.generatePDF(TEMPLATE_PL_RINCIAN_PENAWARAN, model);
        renderBinary(is);


    }

    @AllowAccess({Group.PP, Group.PPK, Group.PANITIA, Group.REKANAN, Group.AUDITOR})
    public static void cetakSuratPenawaranPeserta(Long id) {
        PesertaPl pesertapl = PesertaPl.findById(id);
        otorisasiDataPl(pesertapl.lls_id); //cek otorisasi data pl
        Pl_seleksi pl = Pl_seleksi.findById(pesertapl.lls_id);
        DokPenawaranPl.JenisDokPenawaran jenisDokPenawaran = pl.getPemilihan().isPl() ?
                DokPenawaranPl.JenisDokPenawaran.PENAWARAN_HARGA : DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;
        DokPenawaranPl penawaranPeserta = DokPenawaranPl.findPenawaranPeserta(pesertapl.psr_id, jenisDokPenawaran);
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("pesertapl", pesertapl);
        Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        LDPContent ldpcontent = new LDPContent();
        if (dok_pl != null) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findByLdp(dok_pl.dll_id);
            if (dok_pl_content != null)
                ldpcontent = dok_pl_content.ldpcontent;
        }
        if(pl.isPenunjukanLangsungNew()) {
	        Tahap tahapSekarang = Tahap.PEMBUKAAN_PENAWARAN;
	        Jadwal_pl jadwalpl = Jadwal_pl.findByLelangNTahap(pesertapl.lls_id, tahapSekarang);
	           if(jadwalpl != null){
	               renderArgs.put("tglPenutupan", jadwalpl.dtj_tglakhir);
	           }
	        model.put("nama_lpse", ConfigurationDao.getNamaLpse());
	        model.put("pl", pl);
	        model.put("nama_rekanan", pesertapl.getRekanan().rkn_nama);
	        model.put("tglPenutupan", jadwalpl.dtj_tglakhir);
	        model.put("masa_berlaku_penawaran", pesertapl.masa_berlaku_penawaran == null ? ldpcontent.masa_berlaku_penawaran :pesertapl.masa_berlaku_penawaran);
	        response.contentType = "application/pdf";
	        InputStream is = HtmlUtil.generatePDF(TEMPLATE_SURAT_PENAWARAN, model);
	        renderBinary(is);
        }
        else {
        	Tahap tahapSekarang = Tahap.PEMASUKAN_PENAWARAN;
	        Jadwal_pl jadwalpl = Jadwal_pl.findByLelangNTahap(pesertapl.lls_id, tahapSekarang);
	           if(jadwalpl != null){
	               renderArgs.put("tglPenutupan", jadwalpl.dtj_tglakhir);
	           }
	        model.put("nama_lpse", ConfigurationDao.getNamaLpse());
	        model.put("pl", pl);
	        model.put("nama_rekanan", pesertapl.getRekanan().rkn_nama);
	        model.put("tglPenutupan", jadwalpl.dtj_tglakhir);
	        model.put("masa_berlaku_penawaran", pesertapl.masa_berlaku_penawaran == null ? ldpcontent.masa_berlaku_penawaran :pesertapl.masa_berlaku_penawaran);
	        response.contentType = "application/pdf";
	        InputStream is = HtmlUtil.generatePDF(TEMPLATE_SURAT_PENAWARAN, model);
	        renderBinary(is);
                  }
    }

    @AllowAccess({Group.PP, Group.PPK, Group.AUDITOR, Group.REKANAN, Group.PANITIA})
    public static void rincian_penawaran(Long id) {
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
        String namaPeserta = peserta.getNamaPeserta();
        String namaPaket = pl.getNamaPaket();
        boolean allowDetilRincian = !Active_user.current().isRekanan();
        DaftarKuantitas dkh = peserta.dkh != null ? peserta.dkh : null;
        render("nonlelang/evaluasi/rincian-penawaran-pl.html", dkh, namaPeserta, namaPaket, peserta, allowDetilRincian);

    }

    @AllowAccess({Group.PANITIA, Group.PPK, Group.AUDITOR, Group.REKANAN, Group.PP})
    public static void rincian_adminteknis(Long id) {
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
        Pl_seleksi lelang = Pl_seleksi.findById(peserta.lls_id);
        Dok_pl dok_pl = Dok_pl.findBy(lelang.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        String namaPeserta = peserta.getNamaPeserta();
        String namaPaket = lelang.getNamaPaket();
        List<ChecklistPl> syaratAdmin = dok_pl.getSyaratAdministrasi();
        List<ChecklistPl> syaratTeknis = dok_pl.getSyaratTeknis();

        render("nonlelang/evaluasi/rincian-admin-teknis-pl.html", namaPeserta, namaPaket, peserta, syaratAdmin, syaratTeknis);
    }

}