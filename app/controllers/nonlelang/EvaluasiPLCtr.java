package controllers.nonlelang;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.DecimalBinder;
import ext.RupiahArrayBinder;
import models.agency.*;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.lelang.Dok_lelang;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Evaluasi;
import models.lelang.HasilEvaluasi;
import models.nonlelang.*;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.nonlelang.common.TahapNowPl;
import models.nonlelang.common.TahapStartedPl;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jdbc.Query;
import play.libs.URLs;
import play.mvc.Router;
import utils.BlacklistCheckerUtil;
import utils.LogUtil;
import models.sso.common.adp.util.DceSecurityV2;

import java.util.*;
import java.util.stream.Collectors;

public class EvaluasiPLCtr extends BasicCtr{

	public static final String TAG = "EvaluasiPLCtr";
	
	@AllowAccess({Group.PP,Group.AUDITOR,Group.PPK,Group.PANITIA})
	public static void index(Long id) {
		otorisasiDataPl(id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		Active_user active_user = Active_user.current();
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		TahapNowPl tahapAktif = new TahapNowPl(id);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(id);
		PersetujuanPl persetujuan_pegawai = PersetujuanPl.findByPegawaiPenetapanAkhir(active_user.pegawaiId, id);
		boolean editable = tahapAktif.isEvaluasi() && !persetujuan;
		EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
		if(evaluasi != null) {
			boolean sudah_ada_penetapan = NilaiEvaluasiPl.countLulus(evaluasi.eva_id) > 0;
			boolean allow_persetujuan = persetujuan_pegawai != null && (persetujuan_pegawai.pst_status.isBelumSetuju() || persetujuan_pegawai.pst_status.isTidakSetuju()) && sudah_ada_penetapan;
			if(evaluasi != null)
				editable = evaluasi.eva_status.isSedangEvaluasi();
			boolean allow_penetapan = tahapAktif.isPenetapaPemenang() && !persetujuan;
			renderArgs.put("allow_persetujuan", allow_persetujuan);
			renderArgs.put("sudahVerifikasiSikap", pl.sudahVerifikasiSikap() && tahapAktif.isPenetapaPemenang());
			renderArgs.put("ada_yang_bisa_jadi_pemenang", adaYangBisaJadiPemenang(pl));
			renderArgs.put("allow_penetapan", allow_penetapan);
		}
		renderArgs.put("evaluasi", evaluasi);
		renderArgs.put("active_user", active_user);
		renderArgs.put("tahapAktif", tahapAktif);
		renderArgs.put("editable", editable);
		renderTemplate("nonlelang/evaluasi/evaluasi.html");
	}

	private static boolean adaYangBisaJadiPemenang(Pl_seleksi pl) {
		return HasilEvaluasi.findByPl(pl.lls_id).stream().anyMatch(o->o.isBisaMenang());
	}
	
	@AllowAccess({Group.PP,Group.AUDITOR,Group.PPK,Group.PANITIA})
	public static void indexpl(Long id) {
		otorisasiDataPl(id); // check otorisasi data nonlelang
		Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		Active_user active_user = Active_user.current();
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		TahapNowPl tahapAktif = new TahapNowPl(id);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(id);
		PersetujuanPl persetujuan_pegawai = PersetujuanPl.findByPegawaiPenetapanAkhir(active_user.pegawaiId, id);
		boolean editable = tahapAktif.isEvaluasi() && !persetujuan;
		EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
		EvaluasiPl pembuktian = EvaluasiPl.findPembuktian(id);
		if(pl.isPenunjukanLangsungNew()) {
			if(pembuktian != null) {
				boolean allow_penetapan_pra = tahapAktif.isPenetapanHasilPra() && pembuktian.eva_status.isSedangEvaluasi() && (active_user.isPanitia() || active_user.isPP());
				renderArgs.put("allow_penetapan_pra", allow_penetapan_pra && !(dok_pl.isEmptyDokumen()));
				if(pembuktian.eva_status.isSelesai() && !allow_penetapan_pra)
					renderArgs.put("status_penetapan_pra", "Penetapan Pemenang Prakualifikasi sudah dilakukan.");
			}
		}		
		if(evaluasi != null) {
			boolean sudah_ada_penetapan = NilaiEvaluasiPl.countLulus(evaluasi.eva_id) > 0;
			boolean allow_persetujuan = persetujuan_pegawai != null && (persetujuan_pegawai.pst_status.isBelumSetuju() || persetujuan_pegawai.pst_status.isTidakSetuju()) && sudah_ada_penetapan;
			if(evaluasi != null) {
				editable = evaluasi.eva_status.isSedangEvaluasi();
			}
			// check status pembuktian
			boolean sudahdibuktikan  = false;
			if(pembuktian != null) {
				sudahdibuktikan = NilaiEvaluasiPl.count("nev_lulus in (0,1,2) AND eva_id =?", pembuktian.eva_id) > 0;
			}
			boolean allow_penetapan = tahapAktif.isPenetapanPemenangPra() && active_user.isPanitia() && sudahdibuktikan;
			renderArgs.put("allow_persetujuan", allow_persetujuan);
			renderArgs.put("sudahVerifikasiSikap", tahapAktif.isPenetapanPemenangPra());
			renderArgs.put("allow_penetapan", allow_penetapan);
		}
		renderArgs.put("evaluasi", evaluasi);
		renderArgs.put("active_user", active_user);
		renderArgs.put("tahapAktif", tahapAktif);
		renderArgs.put("editable", editable);
		renderTemplate("nonlelang/evaluasi/evaluasi-pl.html");
	}

	@AllowAccess({Group.PP,Group.AUDITOR,Group.PPK,Group.PANITIA})
	public static void detail(Long id) {
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPl(peserta.lls_id);
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		Active_user active_user = Active_user.current();
		TahapStartedPl tahapStarted = new TahapStartedPl(pl.lls_id);
		TahapNowPl tahapNow = new TahapNowPl(pl.lls_id);
		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(pl.lls_id);
		DokPenawaranPl dokPenawaran = peserta.getDokSusulan();
		if(dokPenawaran != null) {
			renderArgs.put("dok_susulan", dokPenawaran.getDokumenList());
		}
		renderArgs.put("persetujuan", persetujuan);
		renderArgs.put("id", id);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapNow", tahapNow);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("dokumen", peserta.getFileTeknis());
		renderArgs.put("dokumen_kualifikasi", peserta.getDokKualifikasi());
		renderArgs.put("dokumen_harga", peserta.getFileHarga());
		renderArgs.put("peserta", peserta);
		boolean sudahUndangPembuktian = MailQueueDao.countByJenisAndRekanan(pl.lls_id, JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id, peserta.rkn_id) > 0;
		renderArgs.put("sudahUndangPembuktian", sudahUndangPembuktian);
		if(pl.isPenunjukanLangsungNew()) {
			Dok_pl dok_kualifikasi_pl = Dok_pl.findBy(pl.lls_id, pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA : JenisDokPl.DOKUMEN_LELANG);			
			if (dok_kualifikasi_pl != null) {
				EvaluasiPl kualifikasi = pl.getEvaluasiKualifikasi(true);
				renderArgs.put("kualifikasi", kualifikasi);				
			if(kualifikasi != null) {
				NilaiEvaluasiPl nilai_kualifikasi = NilaiEvaluasiPl.findNCreateBy(kualifikasi.eva_id, peserta, EvaluasiPl.JenisEvaluasi.EVALUASI_KUALIFIKASI);
				boolean allow_kualifikasi = tahapNow.isEvaluasiKualifikasiNew() && kualifikasi.eva_status.isSedangEvaluasi() && (active_user.isPP() || active_user.isPanitia());
				renderArgs.put("allow_kualifikasi", allow_kualifikasi);
				renderArgs.put("nilai_kualifikasi", nilai_kualifikasi);
				NilaiEvaluasiPl nilai_pembuktian = null;
				EvaluasiPl pembuktian = pl.getPembuktian(false);
				if(pembuktian != null)
					nilai_pembuktian = peserta.getNilaiPembuktian();					
				if(nilai_kualifikasi != null) {
					renderArgs.put("allow_kirim_undangan", allow_kualifikasi && active_user.isPP() || active_user.isPanitia());
					List<ChecklistEvaluasiPl> syaratKualifikasi = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratIjinUsaha(), nilai_kualifikasi.nev_id);
					syaratKualifikasi.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasi(), nilai_kualifikasi.nev_id));
					List<ChecklistEvaluasiPl> syaratIjinUsahaBaru = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratIjinUsahaBaru(), nilai_kualifikasi.nev_id);
					List<ChecklistEvaluasiPl> syaratKualifikasiAdmin = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasiAdministrasi(), nilai_kualifikasi.nev_id);
					List<ChecklistEvaluasiPl> syaratKualifikasiTeknis = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasiTeknis(), nilai_kualifikasi.nev_id);
					List<ChecklistEvaluasiPl> syaratKualifikasiKeuangan = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasiKeuangan(), nilai_kualifikasi.nev_id);
					renderArgs.put("syaratKualifikasi", syaratKualifikasi);
					renderArgs.put("syaratIjinUsahaBaru", syaratIjinUsahaBaru);
					renderArgs.put("syaratKualifikasiAdmin", syaratKualifikasiAdmin);
					renderArgs.put("syaratKualifikasiTeknis", syaratKualifikasiTeknis);
					renderArgs.put("syaratKualifikasiKeuangan", syaratKualifikasiKeuangan);
				}
				// khusus spse 4, ada proses pembuktian kualifikasi
					renderArgs.put("pembuktian", pembuktian);
					if (pembuktian != null) {
						renderArgs.put("nilai_pembuktian", nilai_pembuktian);						
						if (nilai_pembuktian != null) {
							boolean allow_pembuktian = (!persetujuan || pl.isPenunjukanLangsungNew()) && tahapNow.isPembuktianKualifikasi() && pembuktian.eva_status.isSedangEvaluasi();
							//renderArgs.put("allow_kirim_undangan", allow_pembuktian);
							renderArgs.put("allow_pembuktian", allow_pembuktian && (active_user.isPanitia() || active_user.isPP()));
							MailQueue mail = MailQueueDao.getByJenisAndLelangAndRekanan(pl.lls_id
									, peserta.rkn_id , JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);
							renderArgs.put("mail", mail);
							List<ChecklistEvaluasiPl> buktiKualifikasi = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratIjinUsaha(), nilai_pembuktian.nev_id);
							buktiKualifikasi.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasi(), nilai_pembuktian.nev_id));
							List<ChecklistEvaluasiPl> buktiIjinUsahaBaru = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratIjinUsahaBaru(), nilai_pembuktian.nev_id);
							List<ChecklistEvaluasiPl> buktiKualifikasiAdmin = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasiAdministrasi(), nilai_pembuktian.nev_id);						
							List<ChecklistEvaluasiPl> buktiKualifikasiTeknis = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasiTeknis(), nilai_pembuktian.nev_id);
							List<ChecklistEvaluasiPl> buktiKualifikasiKeuangan = ChecklistEvaluasiPl.addPersyaratan(dok_kualifikasi_pl.getSyaratKualifikasiKeuangan(), nilai_pembuktian.nev_id);														
							renderArgs.put("buktiKualifikasi", buktiKualifikasi);
							renderArgs.put("buktiIjinUsahaBaru", buktiIjinUsahaBaru);
							renderArgs.put("buktiKualifikasiTeknis", buktiKualifikasiTeknis);
							renderArgs.put("buktiKualifikasiAdmin", buktiKualifikasiAdmin);
							renderArgs.put("buktiKualifikasiTeknis", buktiKualifikasiTeknis);
							renderArgs.put("buktiKualifikasiKeuangan", buktiKualifikasiKeuangan);
							renderArgs.put("buktiKualifikasi", buktiKualifikasi);
						}
					}
			}

			Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
			boolean allow_admin_teknis = tahapNow.isEvaluasiTeknis() && (active_user.isPP() || active_user.isPanitia());
			EvaluasiPl admin = EvaluasiPl.findNCreateAdministrasi(pl.lls_id);
			renderArgs.put("admin", admin);

			if (admin != null) {
				NilaiEvaluasiPl nilai_admin = NilaiEvaluasiPl.findNCreateBy(admin.eva_id, peserta, EvaluasiPl.JenisEvaluasi.EVALUASI_ADMINISTRASI);
				renderArgs.put("nilai_admin", nilai_admin);
				if (nilai_admin != null) {
					List<ChecklistEvaluasiPl> syaratAdmin = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratAdministrasi(), nilai_admin.nev_id);
					renderArgs.put("syaratAdmin", syaratAdmin);
				}
			}

			EvaluasiPl teknis = EvaluasiPl.findTeknis(pl.lls_id);
			renderArgs.put("teknis", teknis);

			if (teknis != null) {
				NilaiEvaluasiPl nilai_teknis = NilaiEvaluasiPl.findBy(teknis.eva_id, peserta.psr_id);
				renderArgs.put("nilai_teknis", nilai_teknis);
				if (nilai_teknis != null) {
					List<ChecklistEvaluasiPl> syaratTeknis = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratTeknis(), nilai_teknis.nev_id);
					renderArgs.put("syaratTeknis", syaratTeknis);
				}
			}

			renderArgs.put("allow_admin_teknis", allow_admin_teknis && (active_user.isPP() || active_user.isPanitia()));
			EvaluasiPl harga = EvaluasiPl.findHarga(pl.lls_id);
			renderArgs.put("harga", harga);

			if (harga != null) {
				boolean allow_harga = tahapNow.isEvaluasiHarga();
				NilaiEvaluasiPl nilai_harga = NilaiEvaluasiPl.findBy(harga.eva_id, peserta.psr_id);
				if (nilai_harga != null) {
					List<ChecklistEvaluasiPl> syaratHarga = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratHarga(), nilai_harga.nev_id);

					renderArgs.put("syaratHarga", syaratHarga);
				}
				renderArgs.put("allow_harga", allow_harga && (active_user.isPP() || active_user.isPanitia()));
				renderArgs.put("nilai_harga", nilai_harga);
			}
		}

		String nama = peserta.getNamaPeserta();
		renderArgs.put("nama", nama);
		renderTemplate("nonlelang/evaluasi/evaluasi-detail-pl.html");
		}
		else {
			Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
			if (dok_pl != null) {
//			EvaluasiPl kualifikasi = EvaluasiPl.findNCreateKualifikasi(pl.lls_id);
			EvaluasiPl kualifikasi = pl.getEvaluasiKualifikasi(true);
			renderArgs.put("kualifikasi", kualifikasi);
			boolean allow_kualifikasi = tahapNow.isEvaluasiKualifikasi() && kualifikasi.eva_status.isSedangEvaluasi() && (active_user.isPanitia() || active_user.isPP());
			renderArgs.put("allow_kualifikasi", allow_kualifikasi);
			if(kualifikasi != null) {
				NilaiEvaluasiPl nilai_kualifikasi = NilaiEvaluasiPl.findNCreateBy(kualifikasi.eva_id, peserta, EvaluasiPl.JenisEvaluasi.EVALUASI_KUALIFIKASI);
				renderArgs.put("nilai_kualifikasi", nilai_kualifikasi);
				
				if(nilai_kualifikasi != null) {
					renderArgs.put("allow_kirim_undangan", allow_kualifikasi && active_user.isPP() || active_user.isPanitia());
					List<ChecklistEvaluasiPl> syaratKualifikasi = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratIjinUsaha(), nilai_kualifikasi.nev_id);					
					syaratKualifikasi.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratKualifikasi(), nilai_kualifikasi.nev_id));
					List<ChecklistEvaluasiPl> syaratIjinUsahaBaru = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratIjinUsahaBaru(), nilai_kualifikasi.nev_id);
					List<ChecklistEvaluasiPl> syaratKualifikasiAdmin = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratKualifikasiAdministrasi(), nilai_kualifikasi.nev_id);
					List<ChecklistEvaluasiPl> syaratKualifikasiTeknis = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratKualifikasiTeknis(), nilai_kualifikasi.nev_id);
					List<ChecklistEvaluasiPl> syaratKualifikasiKeuangan = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratKualifikasiKeuangan(), nilai_kualifikasi.nev_id);
					renderArgs.put("syaratKualifikasi", syaratKualifikasi);
					renderArgs.put("syaratIjinUsahaBaru", syaratIjinUsahaBaru);
					renderArgs.put("syaratKualifikasiAdmin", syaratKualifikasiAdmin);
					renderArgs.put("syaratKualifikasiTeknis", syaratKualifikasiTeknis);
					renderArgs.put("syaratKualifikasiKeuangan", syaratKualifikasiKeuangan);
				}
			}

				boolean allow_admin_teknis = tahapNow.isEvaluasiTeknis() && (active_user.isPP() || active_user.isPanitia());
				EvaluasiPl admin = EvaluasiPl.findNCreateAdministrasi(pl.lls_id);
				renderArgs.put("admin", admin);

				if (admin != null) {
					NilaiEvaluasiPl nilai_admin = NilaiEvaluasiPl.findNCreateBy(admin.eva_id, peserta, EvaluasiPl.JenisEvaluasi.EVALUASI_ADMINISTRASI);
					renderArgs.put("nilai_admin", nilai_admin);

					if (nilai_admin != null) {
						List<ChecklistEvaluasiPl> syaratAdmin = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratAdministrasi(), nilai_admin.nev_id);
						renderArgs.put("syaratAdmin", syaratAdmin);
					}
				}
				EvaluasiPl teknis = EvaluasiPl.findTeknis(pl.lls_id);
				renderArgs.put("teknis", teknis);

				if (teknis != null) {
					NilaiEvaluasiPl nilai_teknis = NilaiEvaluasiPl.findBy(teknis.eva_id, peserta.psr_id);
					renderArgs.put("nilai_teknis", nilai_teknis);
					if (nilai_teknis != null) {
						List<ChecklistEvaluasiPl> syaratTeknis = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratTeknis(), nilai_teknis.nev_id);
						renderArgs.put("syaratTeknis", syaratTeknis);
					}
				}

				renderArgs.put("allow_admin_teknis", allow_admin_teknis && (active_user.isPP() || active_user.isPanitia()));
				EvaluasiPl harga = EvaluasiPl.findHarga(pl.lls_id);
				renderArgs.put("harga", harga);

				if (harga != null) {
					boolean allow_harga = tahapNow.isEvaluasiHarga();
					NilaiEvaluasiPl nilai_harga = NilaiEvaluasiPl.findBy(harga.eva_id, peserta.psr_id);
					if (nilai_harga != null) {
						List<ChecklistEvaluasiPl> syaratHarga = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratHarga(), nilai_harga.nev_id);
						renderArgs.put("syaratHarga", syaratHarga);
					}
					renderArgs.put("allow_harga", allow_harga && (active_user.isPP() || active_user.isPanitia()));
					renderArgs.put("nilai_harga", nilai_harga);
				}
			}

			String nama = peserta.getNamaPeserta();
			renderArgs.put("nama", nama);
			renderTemplate("nonlelang/evaluasi/evaluasi-detail.html");
		}
	}
	
	

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklist_kualifikasi(Long id, List<Long> checklist, List<Long> checklistIjin, List<Long> checklistAdmin, List<Long> checklistTeknis, List<Long> checklistKeuangan, @As(binder= DecimalBinder.class) Double skor,String uraian) {
		checkAuthenticity();
		boolean lulus = false;
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		boolean sudahUndangPembuktian = MailQueueDao.countByJenisAndRekanan(pl.lls_id, JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id, peserta.rkn_id) > 0;
		if (pl.getEvaluasi() != null && pl.getEvaluasi().isNilai())
			Validation.addError("kualifikasi.skor","Anda harus mengisi besar skor harga pada Evaluasi Kualifikasi.");
		EvaluasiPl evaluasi = EvaluasiPl.findKualifikasi(pl.lls_id);
		NilaiEvaluasiPl nilai = NilaiEvaluasiPl.findBy(evaluasi.eva_id, peserta.psr_id);
		JenisDokPl jenis = pl.isPenunjukanLangsungNew() ? JenisDokPl.DOKUMEN_LELANG_PRA: JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_lelang = Dok_pl.findBy(pl.lls_id, jenis);
		
		List<Long> _checklist = new ArrayList<>();
		List<ChecklistEvaluasiPl> list = new ArrayList<>();
		
		if(!dok_lelang.isSyaratKualifikasiBaru()) {
			if (checklist != null) _checklist.addAll(checklist);
			list = ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratIjinUsaha(), nilai.nev_id);
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasi(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for(Long check:_checklist){
	        		if(check != null)
	        			lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
		}
		else {
			if (checklistIjin != null) _checklist.addAll(checklistIjin);
			if (checklistAdmin != null) _checklist.addAll(checklistAdmin);
			if (checklistTeknis != null) _checklist.addAll(checklistTeknis);
			if (checklistKeuangan != null) _checklist.addAll(checklistKeuangan);
			list = ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratIjinUsahaBaru(), nilai.nev_id);
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasiAdministrasi(), nilai.nev_id));
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasiTeknis(), nilai.nev_id));
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasiKeuangan(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for (Long check:_checklist) {
	        		if(check != null)
	        			lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
		}					
		if ((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)){
			Validation.addError("kualifikasi.alasan", "Anda wajib mengisi alasan pada Evaluasi Administrasi. Alasan minimal 10 karakter");
		}
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = ChecklistEvaluasiPl.simpanChecklist(list, _checklist) ? NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			if(pl.isPenunjukanLangsungNew()) {
				evaluasi.simpanNilaiPesertaPl(peserta, nilai, true);
			}else {
				evaluasi.simpanNilaiPeserta(peserta, nilai, true);
			}
			if(pl.isPenunjukanLangsungNew()) {
				String sudahAdaUndangan="";
				if(!sudahUndangPembuktian && lulus){sudahAdaUndangan = ". Silakan kirim Undangan/Pemberitahuan Pembuktian Kualifikasi";}
				flash.success("Data Evaluasi Kualifikasi telah tersimpan"+sudahAdaUndangan);
			}
			else {
				flash.success("Data Evaluasi Kualifikasi telah tersimpan");
			}
		}
		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);
		redirect(Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url+"#kualifikasi");
	}

	public static boolean isLulus(List<Long> checklist, boolean lulus, int size) {
		if(!CommonUtil.isEmpty(checklist)) {
			List<Long> lulus_list = new ArrayList<>();
			for(Long check:checklist){
				if(check != null)
					lulus_list.add(check);
			}
			lulus = lulus_list.size() == size;
		}
		return lulus;
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklist_pembuktian(Long id, List<Long> checklist, List<Long> checklistIjin, List<Long> checklistAdmin, List<Long> checklistTeknis, List<Long> checklistKeuangan, @As(binder= DecimalBinder.class) Double skor, String uraian) {
		checkAuthenticity();
		boolean lulus = false;
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
		Pl_seleksi lelang = Pl_seleksi.findById(peserta.lls_id);
		EvaluasiPl evaluasi = EvaluasiPl.findNCreatePembuktian(lelang.lls_id);
		NilaiEvaluasiPl nilai = NilaiEvaluasiPl.findBy(evaluasi.eva_id, peserta.psr_id);
		JenisDokPl jenis = lelang.isPenunjukanLangsung() ? JenisDokPl.DOKUMEN_LELANG_PRA: JenisDokPl.DOKUMEN_LELANG;
		Dok_pl dok_lelang = Dok_pl.findBy(lelang.lls_id, jenis);
		
		List<Long> _checklist = new ArrayList<>();
		List<ChecklistEvaluasiPl> list = new ArrayList<>();		
		if(!dok_lelang.isSyaratKualifikasiBaru()) {
			if (checklist != null) _checklist.addAll(checklist);
			list = ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratIjinUsaha(), nilai.nev_id);
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasi(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for(Long check:_checklist){
	        		if(check != null)
	        			lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
		}
		else {
			if (checklistIjin != null) _checklist.addAll(checklistIjin);
			if (checklistAdmin != null) _checklist.addAll(checklistAdmin);
			if (checklistTeknis != null) _checklist.addAll(checklistTeknis);
			if (checklistKeuangan != null) _checklist.addAll(checklistKeuangan);
			list = ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratIjinUsahaBaru(), nilai.nev_id);
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasiAdministrasi(), nilai.nev_id));
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasiTeknis(), nilai.nev_id));
			list.addAll(ChecklistEvaluasiPl.addPersyaratan(dok_lelang.getSyaratKualifikasiKeuangan(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for (Long check:_checklist) {
	        		if(check != null)
	        			lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
		}

		if((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("bukti.alasan","Anda wajib mengisi alasan pada Pembuktian Evaluasi. Alasan minimal 10 karakter");
		}

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		}
		else {
			nilai.nev_lulus = ChecklistEvaluasiPl.simpanChecklist(list, _checklist) ?
			NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS: NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			evaluasi.simpanNilaiPesertaPl(peserta, nilai, true);
			flash.success("Data Pembuktian Kualifikasi telah tersimpan");	
		}
		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);
		redirect(Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url+"#pembuktian");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklist_admin(Long id, List<Long> checklist, @As(binder = DecimalBinder.class) Double skor,String uraian) {
		checkAuthenticity();
		boolean lulus = false;

		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		EvaluasiPl evaluasi = EvaluasiPl.findAdministrasi(pl.lls_id);
		NilaiEvaluasiPl nilai = NilaiEvaluasiPl.findBy(evaluasi.eva_id, peserta.psr_id);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
		List<ChecklistEvaluasiPl> list = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratAdministrasi(), nilai.nev_id);

		if (!CommonUtil.isEmpty(checklist)) {
			List<Long> lulus_list = new ArrayList<>();
			for (Long check : checklist) {
				if (check != null)
					lulus_list.add(check);
			}
			lulus = lulus_list.size() == list.size();
		}
		if ((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)){
			Validation.addError("admin.alasan", "Anda wajib mengisi alasan pada Evaluasi Administrasi. Alasan minimal 10 karakter");
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = ChecklistEvaluasiPl.simpanChecklist(list, checklist) ? NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor =skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, false);
			evaluasi.save();
			flash.success("Data Evaluasi Administrasi telah tersimpan");
		}
		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);

		redirect(Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url + "#administrasi");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklist_teknis(Long id, List<Long> checklist, @As(binder = DecimalBinder.class) Double skor, String uraian) {
		checkAuthenticity();

		boolean lulus = false;
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		EvaluasiPl evaluasi = EvaluasiPl.findTeknis(pl.lls_id);
		NilaiEvaluasiPl nilai = NilaiEvaluasiPl.findBy(evaluasi.eva_id, peserta.psr_id);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
		List<ChecklistEvaluasiPl> list = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratTeknis(), nilai.nev_id);

		if (!CommonUtil.isEmpty(checklist)) {
			List<Long> lulus_list = new ArrayList<Long>();
			for (Long check : checklist) {
				if (check != null)
					lulus_list.add(check);
			}
			lulus = lulus_list.size() == list.size();
		}

//		if (pl.getEvaluasi().isNilai() && skor == null)
//			Validation.addError("teknis.skor", "Anda harus mengisi besar skor teknis pada Evaluasi Teknis.");

		if ((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10))
			Validation.addError("teknis.alasan", "Anda wajib mengisi alasan pada Evaluasi Teknis. Alasan minimal 10 karakter");

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = ChecklistEvaluasiPl.simpanChecklist(list, checklist) ? NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS
					: NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor =skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, false);
			evaluasi.save();

			flash.success("Data Evaluasi Teknis telah tersimpan");
		}

		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);

		redirect(Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url+"#teknis");

	}

	/**
	 * submit evaluasi Harga
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void checklist_harga(Long id, @As(binder= DecimalBinder.class) Double skor, Double hargaTerkoreksi, boolean lulus, String uraian, List<Long> checklist) {
		checkAuthenticity();
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		EvaluasiPl evaluasi = EvaluasiPl.findHarga(peserta.lls_id);
		NilaiEvaluasiPl nilai = NilaiEvaluasiPl.findBy(evaluasi.eva_id, peserta.psr_id);
		Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
		List<ChecklistEvaluasiPl> list = ChecklistEvaluasiPl.addPersyaratan(dok_pl.getSyaratHarga(), nilai.nev_id);

		List<Long> lulus_list = new ArrayList<>();
		if (!CommonUtil.isEmpty(checklist)) {
			for (Long check : checklist) {
				if (check != null)
					lulus_list.add(check);
			}
			lulus = lulus_list.size() == list.size();
		}

		if ((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("harga.alasan","Anda wajib mengisi alasan pada Evaluasi Harga. Alasan minimal 10 karakter.");
			flash.error("Data gagal tersimpan, Anda wajib mengisi alasan pada Evaluasi Harga. Alasan minimal 10 karakter.");
		}
		if(hargaTerkoreksi == null || hargaTerkoreksi == 0.0){
			Validation.addError("hargaTerkoreksi", "Harga Terkoreksi tidak boleh kosong");
			flash.error("Data gagal tersimpan, Harga Terkoreksi tidak boleh kosong");
		}

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			
		}else {
			nilai.nev_harga = peserta.psr_harga;
			nilai.nev_harga_terkoreksi = hargaTerkoreksi;
			if(list != null && checklist != null){
				nilai.nev_lulus = ChecklistEvaluasiPl.simpanChecklist(list, checklist) ? NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
			}else{
				nilai.nev_lulus = lulus ? NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
			}
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			if(hargaTerkoreksi != null && hargaTerkoreksi > 0.0){
				peserta.psr_harga_terkoreksi= hargaTerkoreksi;
				peserta.save();
			}
			evaluasi.simpanNilaiPeserta(peserta, nilai, false);
			flash.success("Data Evaluasi Harga telah tersimpan");
			evaluasi.save();
			
		}

//		if(checklist == null){
//			flash.error("Persyaratan belum terchecklist");
//		}
		

		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);
		redirect(Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url+"#harga");

	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void penetapan(Long id) {
		otorisasiDataPl(id);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
		renderArgs.put("pl", pl);
		renderArgs.put("evaluasi", evaluasi);
		renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL+"/daftar-hitam");
		if(evaluasi != null) {
			List<Long> bisaMenangIdList = HasilEvaluasi.findByPl(pl.lls_id).stream().filter(o1 -> o1.isBisaMenang()).map(o -> o.psr_id).collect(Collectors.toList());
			List<NilaiEvaluasiPl> nilaiEvaluasiList = evaluasi.findPesertaList(true);
			List<NilaiEvaluasiPl> nilaiEvaluasiBisaMenangList = nilaiEvaluasiList.stream().filter(o->bisaMenangIdList.contains(o.psr_id)).collect(Collectors.toList());
			renderArgs.put("pesertaList", nilaiEvaluasiBisaMenangList);
		}

		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(id);
		TahapNowPl tahapNow = new TahapNowPl(pl.lls_id);
		boolean editable = tahapNow.isPenetapaPemenang() && evaluasi.eva_status == EvaluasiPl.StatusEvaluasi.SEDANG_EVALUASI;
		boolean allowPenetapan = tahapNow.isPenetapaPemenang();
		renderArgs.put("editable", true);
		renderArgs.put("allowPenetapan", allowPenetapan);
//		renderArgs.put("showHargaNego", nilaiEvaluasiList.size() == 1 || pl.getPaket().kgr_id.isConsultant());
		renderTemplate("nonlelang/evaluasi/evaluasi-pemenang-pl.html");

	}

	@AllowAccess({Group.PANITIA})
	public static void penetapanPanitia(Long id) {
		otorisasiDataPl(id);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
		renderArgs.put("pl", pl);
		renderArgs.put("evaluasi", evaluasi);
		renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL+"/daftar-hitam");
		if(evaluasi != null) {
			List<Long> bisaMenangIdList = HasilEvaluasi.findByPl(pl.lls_id).stream().filter(o1 -> o1.isBisaMenang()).map(o -> o.psr_id).collect(Collectors.toList());
			List<NilaiEvaluasiPl> nilaiEvaluasiList = evaluasi.findPesertaList(true);
			List<NilaiEvaluasiPl> nilaiEvaluasiBisaMenangList = nilaiEvaluasiList.stream().filter(o->bisaMenangIdList.contains(o.psr_id)).collect(Collectors.toList());
			renderArgs.put("pesertaList", nilaiEvaluasiBisaMenangList);
		}

		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(id);
		TahapNowPl tahapNow = new TahapNowPl(pl.lls_id);
		boolean editable = tahapNow.isPenetapanPemenangPra() && evaluasi.eva_status == EvaluasiPl.StatusEvaluasi.SEDANG_EVALUASI;
		boolean allowPenetapanPra = tahapNow.isPenetapanPemenangPra() && pl.mtd_kualifikasi == 0;
		renderArgs.put("editable", true);
		renderArgs.put("allowPenetapanPra", allowPenetapanPra);
//		renderArgs.put("showHargaNego", nilaiEvaluasiList.size() == 1 || pl.getPaket().kgr_id.isConsultant());
		renderTemplate("nonlelang/evaluasi/evaluasi-pemenang-penunjukan.html");

	}
	
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void submit_penetapanPra(Long id,  Long[] shortlist){
		checkAuthenticity();
		otorisasiDataPl(id); // check otorisasi data lelang
		Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
		Pl_seleksi lelang = Pl_seleksi.findById(id);

		if(dok_pl.isEmptyDokumen()) {
			flash.error("Dokumen Tender/Seleksi Belum dibuat, Anda tidak bisa melakukan Penetapan Pemenang Prakualifikasi");
		}

		if(!flash.contains("error")) {		
				EvaluasiPl evaluasi = EvaluasiPl.findKualifikasi(id);
				EvaluasiPl pembuktian = EvaluasiPl.findPembuktian(id);
				if (evaluasi == null) {
					flash.error("Anda belum melalukan Evaluasi Kualifikasi.");
				} else if (pembuktian == null) {
					flash.error("Anda belum melakukan Pembuktian Kualifikasi.");
				}
				
				if (!flash.contains("error")) {
					evaluasi.eva_status = EvaluasiPl.StatusEvaluasi.SELESAI;
					evaluasi.eva_tgl_setuju = newDate();
					evaluasi.save();
					pembuktian.eva_status = EvaluasiPl.StatusEvaluasi.SELESAI;
					pembuktian.eva_tgl_setuju = new Date();
					pembuktian.save();
					flash.success("Penetapan Pemenang Prakualifikasi berhasil dilakukan.");
				}
			}		
		indexpl(id);
	}
	
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void submit_penetapan(Long id, Long[] nilai, @As(binder = RupiahArrayBinder.class) Double[] harga, Integer[] urutan){
		checkAuthenticity();
		otorisasiDataPl(id);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(id);
		TahapNowPl tahapNow = pl.getTahapNow();
		boolean editable = tahapNow.isPenetapaPemenang();
		if(editable && (nilai != null)) {
			//validasi nilai harga
			Boolean validate = false;
			StringBuilder namaPeserta = new StringBuilder();
//			PesertaPl pesertaPemenang = null;
			for(int i=0; i< nilai.length; i++) {
				if(nilai[i] == null)
					continue;
				NilaiEvaluasiPl nilai_evaluasi = NilaiEvaluasiPl.findById(nilai[i]);
				PesertaPl peserta = PesertaPl.findById(nilai_evaluasi.psr_id);
				peserta.psr_harga_terkoreksi = peserta.psr_harga_terkoreksi != null ? peserta.psr_harga_terkoreksi : 0;

			}
				boolean arrayDuplicate = false;
				if(!pl.isItemized()) {
					List<Integer> listUrutan = new ArrayList<>();
					for (Integer nevUrut : urutan) {
						arrayDuplicate = listUrutan.stream().anyMatch(t -> t.equals(nevUrut));
						if (arrayDuplicate) {
							flash.error("Gagal menetapkan pemenang! <br>Nomor urut tidak valid, ditemukan angka duplikat: " + nevUrut);
							break;
						}
						listUrutan.add(nevUrut);
					}
				}
				if(!arrayDuplicate) {
					NilaiEvaluasiPl nilai_evaluasi;
					boolean pemenang = false;
					for (int i = 0; i < nilai.length; i++) {
						nilai_evaluasi = NilaiEvaluasiPl.findById(nilai[i]);
						if (harga != null && harga[i] != null) {
							nilai_evaluasi.nev_harga_negosiasi = harga[i];
						}
						nilai_evaluasi.nev_urutan = urutan[i];
						if (nilai_evaluasi.nev_urutan == 1) { // peserta dengan urutan pertama aka diset sebagai pemenang
							nilai_evaluasi.nev_lulus = NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS;
							pemenang = true;
//							pesertaPemenang = nilai_evaluasi.getPeserta();
						} else {
							nilai_evaluasi.nev_lulus = NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
						}
						nilai_evaluasi.save();

					}
					if (!pemenang) {
						flash.error("Pemenang Belum Ditetapkan!");
					}
					else {
						EvaluasiPl evaluasiPl = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
						evaluasiPl.eva_status = EvaluasiPl.StatusEvaluasi.SELESAI;
						evaluasiPl.save();
						NilaiEvaluasiPl nev = NilaiEvaluasiPl.findByLulusAndLelang(evaluasiPl.lls_id);
						PesertaPl pesertaPemenang = nev.getPeserta();
						if (pesertaPemenang != null) {
							pesertaPemenang.is_pemenang_verif = 1;
							pesertaPemenang.save();
						}
						flash.success("Pemenang Sudah Ditetapkan!");

					}
				}

//			}

		}
		if(pl.isPenunjukanLangsungNew()) {
			indexpl(id);
		}else {
			index(id);
		}
	}

	@AllowAccess({Group.PANITIA})
	public static void submitPenetapanPra(Long id, Long[] nilai, @As(binder = RupiahArrayBinder.class) Double[] harga, Integer[] urutan){
		checkAuthenticity();
		otorisasiDataPl(id);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		boolean persetujuan = PersetujuanPl.isSedangPersetujuanPemenang(id);
		TahapNowPl tahapNow = pl.getTahapNow();
		boolean editable = tahapNow.isPenetapanPemenangPra();
		if(editable && (nilai != null)) {
			//validasi nilai harga
			Boolean validate = false;
			StringBuilder namaPeserta = new StringBuilder();
			for(int i=0; i< nilai.length; i++) {
				if(nilai[i] == null)
					continue;
				NilaiEvaluasiPl nilai_evaluasi = NilaiEvaluasiPl.findById(nilai[i]);
				PesertaPl peserta = PesertaPl.findById(nilai_evaluasi.psr_id);
				peserta.psr_harga_terkoreksi = peserta.psr_harga_terkoreksi != null ? peserta.psr_harga_terkoreksi : 0;

			}
			boolean arrayDuplicate = false;
			if(!pl.isItemized()) {
				List<Integer> listUrutan = new ArrayList<>();
				for (Integer nevUrut : urutan) {
					arrayDuplicate = listUrutan.stream().anyMatch(t -> t.equals(nevUrut));
					if (arrayDuplicate) {
						flash.error("Gagal menetapkan pemenang! <br>Nomor urut tidak valid, ditemukan angka duplikat: " + nevUrut);
						break;
					}
					listUrutan.add(nevUrut);
				}
			}
			if(!arrayDuplicate) {
				NilaiEvaluasiPl nilai_evaluasi;
				boolean pemenang = false;
				for (int i = 0; i < nilai.length; i++) {
					nilai_evaluasi = NilaiEvaluasiPl.findById(nilai[i]);
					if (harga != null && harga[i] != null) {
						nilai_evaluasi.nev_harga_negosiasi = harga[i];
					}
					nilai_evaluasi.nev_urutan = urutan[i];
					if (nilai_evaluasi.nev_urutan == 1) { // peserta dengan urutan pertama aka diset sebagai pemenang
						nilai_evaluasi.nev_lulus = NilaiEvaluasiPl.StatusNilaiEvaluasi.LULUS;
						pemenang = true;
//							pesertaPemenang = nilai_evaluasi.getPeserta();
					} else {
						nilai_evaluasi.nev_lulus = NilaiEvaluasiPl.StatusNilaiEvaluasi.TDK_LULUS;
					}
					nilai_evaluasi.save();

				}
				if (!pemenang) {
					flash.error("Pemenang Belum Ditetapkan!");
				}
				else {
					EvaluasiPl evaluasiPl = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
					evaluasiPl.eva_status = EvaluasiPl.StatusEvaluasi.SELESAI;
					evaluasiPl.save();
					NilaiEvaluasiPl nev = NilaiEvaluasiPl.findByLulusAndLelang(evaluasiPl.lls_id);
					PesertaPl pesertaPemenang = nev.getPeserta();
					if (pesertaPemenang != null) {
						pesertaPemenang.is_pemenang_verif = 1;
						pesertaPemenang.save();
					}
					flash.success("Pemenang Sudah Ditetapkan!");

				}
			}

//			}

		}
		if(pl.isPenunjukanLangsungNew()) {
			indexpl(id);
		}else {
			index(id);
		}
	}

	public static Integer pengecekanBlacklist(Long nevId){

		NilaiEvaluasiPl nilai_evaluasi = NilaiEvaluasiPl.findByNevId(nevId);

		PesertaPl peserta = PesertaPl.findById(nilai_evaluasi.psr_id);

		Rekanan rekanan = Rekanan.findById(peserta.rkn_id);

		Active_user active_user = Active_user.current();

		return BlacklistCheckerUtil.checkBlacklistStatusPl(rekanan, active_user.pegawaiId, peserta.lls_id, 1);

	}

	public static void hasil(Long id) {
		Pl_seleksi pl = Pl_seleksi.findById(id);
		TahapStartedPl tahapStarted = new TahapStartedPl(id);
		boolean showKualifikasi = tahapStarted.isShowHasilEvaluasiKualifikasi();
		boolean showTeknis = tahapStarted.isShowHasilEvaluasiTeknis();
		boolean showEvaluasi = tahapStarted.isShowHasilEvaluasi();

//		boolean allow = tahapStarted.isShowHasilEvaluasi();

//		boolean nilai = pl.getEvaluasi().isNilai();
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("showKualifikasi", showKualifikasi);
		renderArgs.put("showTeknis", showTeknis);
		renderArgs.put("showEvaluasi", showEvaluasi);
		List<HasilEvaluasi> hasil = HasilEvaluasi.findByPl(id);
		boolean isSelesai = false;
		renderArgs.put("hasil", hasil);
		renderArgs.put("isSelesai", isSelesai);
		EvaluasiPl penetapan = EvaluasiPl.findPenetapanPemenang(id);
		if(penetapan != null)
			isSelesai = penetapan.eva_status.isSelesai();
		SppbjPl sppbj = SppbjPl.find("lls_id=?", id).first();
		renderArgs.put("sppbj", sppbj);
		renderTemplate("nonlelang/hasil-evaluasi-pl.html");

	}
	
	@AllowAccess({Group.PANITIA, Group.PP})
	public static void penetapanPra(Long id){	
		otorisasiDataPl(id); // check otorisasi data lelang
		Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		TahapNowPl tahapNow = pl.getTahapNow();
		EvaluasiPl evaluasi = EvaluasiPl.findPembuktian(pl.lls_id);
		renderArgs.put("dok_pl", dok_pl);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapNow", tahapNow);
		renderArgs.put("evaluasi", evaluasi);
		renderArgs.put("pesertaLulus", evaluasi.findPesertaLulusEvaluasiList());
		if(pl.isPenunjukanLangsungNew()) {
			Evaluasi pembuktian = Evaluasi.findPembuktian(id);
			if(pembuktian != null) {				
				boolean allow_penetapan_pra = tahapNow.isPenetapanHasilPra() && pembuktian.eva_status.isSedangEvaluasi();
				renderArgs.put("allow_penetapan_pra", allow_penetapan_pra);
				renderArgs.put("isEmptyDokPemilihan", dok_pl.isEmptyDokumen());
				if(pembuktian.eva_status.isSelesai() && !allow_penetapan_pra)
					renderArgs.put("status_penetapan_pra", "Penetapan Pemenang Prakualifikasi sudah dilakukan");
			}
		}				
		renderTemplate("nonlelang/evaluasi/evaluasi-pemenang-prakualifikasi-pl.html");
	}


	public static void pemenang(Long id) {
		Pl_detil pl = Pl_detil.findById(id);
		TahapStartedPl tahapStarted = new TahapStartedPl(id);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		if (tahapStarted.isShowHasilEvaluasi()) {
			EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
			if (evaluasi != null && evaluasi.eva_status.isSelesai()) {
				// ambil peserta dgn is_pemenang=1
				List<NilaiEvaluasiPl> calonPemenang = Query.find("SELECT * FROM ekontrak.nilai_evaluasi nev JOIN ekontrak.peserta_nonlelang psr ON nev.psr_id = psr.psr_id WHERE nev.eva_id = ? AND psr.is_pemenang_verif = ?", NilaiEvaluasiPl.class, evaluasi.eva_id, 1).fetch();
				renderArgs.put("calonPemenang", calonPemenang);
				if(calonPemenang!=null)
					renderArgs.put("pemenang", calonPemenang);
				if (calonPemenang != null) {
					NilaiEvaluasiPl nev = NilaiEvaluasiPl.find("eva_id=?", evaluasi.eva_id).first();
					if (nev != null)
						renderArgs.put("nilai", nev.nev_harga_negosiasi != null && nev.nev_harga_negosiasi > 0 ? nev.nev_harga_negosiasi : nev.nev_harga_terkoreksi);
				}
			}
		}
		renderTemplate("nonlelang/pemenang-pl.html");

	}

	@AllowAccess({Group.PANITIA})
	public static void persetujuan(Long id){
		otorisasiDataPl(id); //check otorisasi data lelang
		Pl_seleksi pl = Pl_seleksi.findById(id);
		TahapStartedPl tahapStarted = pl.getTahapStarted();
		Active_user active_user = Active_user.current();
		TahapNowPl tahapAktif = new TahapNowPl(id);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("evaluasi", EvaluasiPl.findPenetapanPemenang(id));
//		renderArgs.put("nego", DokPenawaranPl.c);
		boolean allow_penetapan = tahapAktif.isPengumuman_PemenangNew() && pl.mtd_kualifikasi == 0;
		renderArgs.put("allow_penetapan", allow_penetapan);
		renderArgs.put("editable", false);
		validation.keep();
		params.flash();
		renderTemplate("nonlelang/evaluasi/persetujuan-evaluasi.html");
	}

	/**
	 * Fungsi {@code mnonlelang.pemenangBerkontrak} digunakan untuk menampilkan detail pemenang berkontrak
	 * dalam halaman popup
	 * @param id parameter id lelang yang dilihat
	 */
	public static void pemenangBerkontrak(Long id) {
		Pl_detil pl = Pl_detil.findById(id);
		TahapStartedPl tahapStarted = new TahapStartedPl(id);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		if (tahapStarted.isShowHasilEvaluasi()) {
			EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(id);
			if (evaluasi != null && evaluasi.eva_status.isSelesai()) {
				SppbjPl sppbj = SppbjPl.find("lls_id=?", id).first();
				if(sppbj != null) {
					renderArgs.put("pemenang", sppbj.getRekanan());
					PesertaPl peserta = PesertaPl.findBy(id, sppbj.rkn_id);
					renderArgs.put("peserta", peserta);
					if (peserta != null && peserta.psr_id != null) {
						NilaiEvaluasiPl nev = NilaiEvaluasiPl.find("eva_id=? and psr_id=?", evaluasi.eva_id, peserta.psr_id).first();
						if (nev != null)
							renderArgs.put("nilai", nev.nev_harga_negosiasi);
					}
				}
			}
		}
		renderTemplate("nonlelang/pemenang-berkontrak-pl.html");

	}

	@AllowAccess({Group.PPK})
	public static void penilaian_sikap(Long id){

		otorisasiDataPl(id); // check otorisasi data pl

		int repoId = ConfigurationDao.getRepoId();

		Active_user active_user = Active_user.current();

		Group group = active_user.group;

		Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);

		JsonObject jsonObject = new JsonObject();

		jsonObject.addProperty("lls_id", id);

		jsonObject.addProperty("repo_id", repoId);

		jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);

		jsonObject.addProperty("repo_nama", pegawai.peg_nama);

		jsonObject.addProperty("peg_role", group.toString());

		jsonObject.addProperty("fmenu", "penilaian_kinerja");

		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));

		redirect(BasicCtr.SIKAP_URL+"/verifikasi/index?q="+param);

	}

	/*
 * Panitia kirim pesan kepada peserta terhadap kekurangan persyaratan kualifikasi yang dikirim
 * proses ini hanya terjadi pada pengadaan langsung dan penunjukan langsung
 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void kirim_pesan(Long id){
		PesertaPl pesertaPl = PesertaPl.findById(id);
		otorisasiDataPesertaPl(pesertaPl);// check otorisasi data peserta
		Pl_seleksi pl = Pl_seleksi.findById(pesertaPl.lls_id);
		renderArgs.put("pesertaPl", pesertaPl);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", pl.getTahapStarted());
		renderArgs.put("list", MailQueueDao.findPesanKualifikasi(pl.lls_id, pesertaPl.rkn_id));
		renderTemplate("nonlelang/evaluasi/kirim-pesan-kualifikasi.html");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void submit_kirim_pesan(Long id, @Required String pesan){
		checkAuthenticity();
		if(validation.hasErrors()){
			validation.keep();
			params.flash();
		} else {
			try{
				PesertaPl pesertaPl = PesertaPl.findById(id);
				otorisasiDataPl(pesertaPl.lls_id);
				String namaPaket = Paket_pl.getNamaPaketFromlelang(pesertaPl.lls_id);

				Panitia panitia = Panitia.findByPenunjukanLangsung(pesertaPl.lls_id);
				Pp pp = Pp.findByLelang(pesertaPl.lls_id);

				if( null != panitia && null == pp){
					String namaPanitia =  panitia.pnt_nama;
					MailQueueDao.kirimPesanKualifikasiPl(pesertaPl.getRekanan(), pesan, pesertaPl.lls_id, namaPaket, namaPanitia);
				}else if( null == panitia && null != pp){
					String namaPp = pp.getPegawai().peg_namauser;
					MailQueueDao.kirimPesanKualifikasiPl(pesertaPl.getRekanan(), pesan, pesertaPl.lls_id, namaPaket, namaPp);
				}

				DokPenawaranPl dok = pesertaPl.getDokSusulan();
				if(dok != null){
					dok.dok_disclaim = 0;
					dok.save();
				}
				pesertaPl.is_dikirim_pesan= true;
				pesertaPl.save();
			}catch (Exception e){
				flash.error("Terjadi kesalahan teknis pengiriman.");
				Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan Pembuktian Kualifikasi.");
			}
		}
		kirim_pesan(id);
	}



	@AllowAccess({Group.PP, Group.PANITIA})
	public static void kirim_undangan_pembuktian(Long id) {
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPesertaPl(peserta); // check otorisasi data peserta
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		TahapStartedPl tahapStarted = new TahapStartedPl(pl.lls_id);
		renderArgs.put("peserta", peserta);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		renderTemplate("nonlelang/evaluasi/kirim-undangan-pembuktian-pl.html");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void submit_kirim_und_pembuktian(Long id, @Required @As(binder=DatetimeBinder.class) Date waktu, @Required String tempat,
			   @Required String dibawa, @Required String hadir, @Required @As(binder=DatetimeBinder.class) Date sampai){
				checkAuthenticity();
				if(validation.hasErrors()){
				validation.keep();
				params.flash();
				kirimUndanganVerifikasi(id);
				}
				
				try {
				PesertaPl peserta = PesertaPl.findById(id);
				otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
				Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
				String namaPaket = Paket_pl.getNamaPaketFromlelang(peserta.lls_id);
				String namaPp = "";
				
				if (Active_user.current().isPP())
				namaPp = pl.getPp().getPegawai().peg_nama;
				else
				namaPp = pl.getNamaPanitia();
				
				MailQueueDao.kirimUndanganVerifikasiPl(peserta.getRekanan(), peserta.lls_id, namaPaket, namaPp, waktu, sampai, tempat, dibawa, hadir);
				flash.success("Undangan verifikasi berhasil terkirim");
				}catch(Exception e) {
				flash.error("Terjadi Kesalahan Teknis Pengiriman Undangan Verifikasi");
				Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Undangan Verifikasi");
				}
				
				kirimUndanganVerifikasi(id);
	}

	@AllowAccess({Group.PP, Group.PANITIA, Group.AUDITOR})
	public static void verifikasi_sikap(Long id){
		PesertaPl peserta = PesertaPl.findById(id);

		JsonObject jsonObject = new JsonObject();
		Active_user active_user = Active_user.current();
		Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);

		if (active_user.isPP()) {
			Pp pp = Pp.findByLelang(peserta.lls_id);
			otorisasiDataPp(pp.pp_id); // check otorisasi data panitia
			jsonObject.addProperty("pnt_id", pp.pp_id);
			jsonObject.addProperty("pnt_nama", pegawai.peg_nama);
		} else{
			Panitia panitia = Panitia.findByPenunjukanLangsung(peserta.lls_id);
			otorisasiDataPanitia(panitia.pnt_id);
			jsonObject.addProperty("pnt_id", panitia.pnt_id);
			jsonObject.addProperty("pnt_nama", pegawai.peg_nama);
		}

		int repoId = ConfigurationDao.getRepoId();

		jsonObject.addProperty("lls_id", peserta.lls_id);
		jsonObject.addProperty("rkn_id", peserta.rkn_id);
		jsonObject.addProperty("repo_id", repoId);
		jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);
		jsonObject.addProperty("repo_nama", pegawai.peg_nama);
		jsonObject.addProperty("peg_role", active_user.group.toString());
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
		String url = BasicCtr.SIKAP_URL;
		redirect(url+"/verifikasi/index?q="+param);
	}

	/**
	 * verifikasi SIKaP setiap peserta
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA, Group.PP})
	public static void konfirmasi_verifikasi_sikap(Long id) {
		PesertaPl peserta = PesertaPl.findById(id);
		Active_user active_user = Active_user.current();
		if(active_user.isPanitia()){
			Panitia panitia = Panitia.findByPenunjukanLangsung(peserta.lls_id);
			otorisasiDataPanitia(panitia.pnt_id); // check otorisasi data panitia
		}else if(active_user.isPP()){
			Pp pp = Pp.findByLelang(peserta.lls_id);
		}
		renderArgs.put("peserta", peserta);
		renderTemplate("nonlelang/evaluasi/konfirmasi-verifikasi.html");
	}

	@AllowAccess({Group.PANITIA, Group.PP})
	public static void submit_konfirmasi_verifikasi_sikap(Long id, Long psr_id) {
		PesertaPl peserta = PesertaPl.findById(psr_id);
		Pl_seleksi pl = Pl_seleksi.findById(id);
		peserta.sudah_verifikasi_sikap = 1;
		peserta.save();
		flash.success("Konfirmasi Verifikasi berhasil disimpan.");

		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);
		if(pl.mtd_kualifikasi == 0) {
			redirect(Router.reverse("nonlelang.EvaluasiPlCtr.indexpl", param).url);
		}
		else {
			redirect(Router.reverse("nonlelang.EvaluasiPlCtr.index", param).url);
		}
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void kirimUndanganVerifikasi(Long id) {
		PesertaPl peserta = PesertaPl.findById(id);
		otorisasiDataPesertaPl(peserta); // check otorisasi data peserta
		Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
		TahapStartedPl tahapStarted = new TahapStartedPl(pl.lls_id);
		List<MailQueue> mails = MailQueueDao.findPesanVerifikasi(peserta.lls_id, peserta.rkn_id);
		renderArgs.put("list", mails);
		renderArgs.put("peserta", peserta);
		renderArgs.put("pl", pl);
		renderArgs.put("tahapStarted", tahapStarted);
		renderTemplate("nonlelang/evaluasi/kirim-undangan-verifikasi-pl.html");
	}

	@AllowAccess({Group.PP, Group.PANITIA})
	public static void submitKirimUndanganVerifikasiPl(Long id, @Required @As(binder=DatetimeBinder.class) Date waktu, @Required String tempat,
												   @Required String dibawa, @Required String hadir, @Required @As(binder=DatetimeBinder.class) Date sampai){
		checkAuthenticity();
		if(validation.hasErrors()){
			validation.keep();
			params.flash();
			kirimUndanganVerifikasi(id);
		}

		try {
			PesertaPl peserta = PesertaPl.findById(id);
			otorisasiDataPl(peserta.lls_id); // check otorisasi data lelang
			Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
			String namaPaket = Paket_pl.getNamaPaketFromlelang(peserta.lls_id);
			String namaPp = "";

			if (Active_user.current().isPP())
				namaPp = pl.getPp().getPegawai().peg_nama;
			else
				namaPp = pl.getNamaPanitia();

			MailQueueDao.kirimUndanganVerifikasiPl(peserta.getRekanan(), peserta.lls_id, namaPaket, namaPp, waktu, sampai, tempat, dibawa, hadir);
			flash.success("Undangan verifikasi berhasil terkirim");
		}catch(Exception e) {
			flash.error("Terjadi Kesalahan Teknis Pengiriman Undangan Verifikasi");
			Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Undangan Verifikasi");
		}

		kirimUndanganVerifikasi(id);
	}

}
