package controllers.nonlelang;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.agency.*;
import models.agency.Paket_pl.StatusPaket;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Checklist_master.JenisChecklist;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.nonlelang.*;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.nonlelang.common.TahapNowPl;
import models.nonlelang.common.TahapStartedPl;
import models.nonlelang.workflow.ProcessInstancePl;
import models.nonlelang.workflow.WorkflowPlDao;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.sikap.RekananSikap;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.lang3.StringUtils;
import org.apache.ivy.util.CollectionUtils;

import play.Logger;
import play.Play;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jdbc.Query;
import play.i18n.Messages;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * definisikan semua terkait pengadaan
 *
 * @author wahid
 */
public class PengadaanLCtr extends BasicCtr {
    private static final String DCEkey = "Y95YrM1551CpRb1w";

    /**
     * Fungsi {@code index} digunakan untuk menampilkan halaman riwayat tender
     * e-procurement LPSE
     */

    public static void index(Integer kategoriId,String instansiId, Integer tahun, String rekanan) {
        Kategori kategori = null;
        renderArgs.put("kategoriId", kategoriId);
        renderArgs.put("instansiId", instansiId);
        renderArgs.put("tahun", tahun);
        renderArgs.put("rekanan", rekanan);
        renderArgs.put("kategoriList", Kategori.all);
        renderArgs.put("instansiList", LelangQueryPl.findInstansi());
        renderArgs.put("tahunList", LelangQueryPl.listTahunAnggaranAktif());
        renderTemplate("nonlelang/pl.html");
    }

    @AllowAccess({Group.REKANAN, Group.PP, Group.PPK, Group.AUDITOR, Group.PANITIA})
    public static void view(Long id) {
    	Active_user active_user = Active_user.current();
        otorisasiDataPl(id);
        Pl_seleksi pl = Pl_seleksi.findById(id);
        Paket_pl paket = pl.getPaket();
        renderArgs.put("pl", pl);
        renderArgs.put("paket", paket);
        boolean isPp = active_user.isPP();
        boolean isPanitia = active_user.isPanitia();
        renderArgs.put("isPp", isPp);
        renderArgs.put("isPanitia", isPanitia);
        renderArgs.put("isLelangAktif", pl.lls_status.isAktif());
        String tahapNow = Jadwal_pl.getJadwalSekarang(id,true,pl.getPemilihan().isLelangExpress(), pl.isLelangV3());
        renderArgs.put("tahapNow", tahapNow);
        TahapStartedPl tahapStarted = pl.getTahapStarted();
        renderArgs.put("tahapStarted", tahapStarted);
        TahapNowPl tahapAktif = pl.getTahapNow();
        renderArgs.put("tahapAktif", tahapAktif);

        if(pl.isPenunjukanLangsungNew()) {
        	renderArgs.put("isKirimKualifikasi", tahapAktif.isPemasukanDokPra());
        	Dok_pl dokplPra = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG_PRA);
        	renderArgs.put("allow_adendum_pra", (isPanitia || isPp) && tahapStarted.isAllowAdendumPra() && !pl.lls_status.isDraft());   	
            renderArgs.put("dok_kualifikasi", dokplPra);
        	if (tahapAktif.isPenetapanHasilPra()) {
                EvaluasiPl penetapan = EvaluasiPl.findPembuktian(id);
                boolean allowPengumumanPra = penetapan != null && penetapan.eva_status.isSelesai() && tahapStarted.isPengumumanPemenangPra();
                renderArgs.put("allow_pengumuman_pemenang_pra", isPp || isPanitia && allowPengumumanPra);
                Integer versiEvaluasi = 1;
                if(penetapan != null)
                    versiEvaluasi = penetapan.eva_versi;
                int jumlahEmail = MailQueueDao.countByJenis(pl.lls_id, JenisEmail.PENGUMUMAN_PEMENANG_PRA.id);
                renderArgs.put("sudahKirimPengumumanPra", jumlahEmail >= pl.getJumlahPeserta() * versiEvaluasi);
            }
        	if (dokplPra != null) {
                boolean sudahCetak = dokplPra != null && dokplPra.dll_id_attachment != null;
                renderArgs.put("allow_download_pra", sudahCetak && tahapStarted.isAllowDownloadDokPra());
                Dok_pl_content dok_pl_content_pra = Dok_pl_content.findBy(dokplPra.dll_id);
                if (dok_pl_content_pra != null) {
                    renderArgs.put("ldk_adendum_pra", dok_pl_content_pra.dll_ldk_updated
                            && dok_pl_content_pra.isAllowUpload());
                    renderArgs.put("allowUploadPra",dok_pl_content_pra.isAllowUpload()
                            && (isPp || isPanitia));
                }
            }
        }

        //checklist adendum
        Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
        renderArgs.put("dok_pl", dok_pl);
        if (!(pl.lls_status.isDraft()))
        renderArgs.put("isAnyDokPemilihan", !(dok_pl.isEmptyDokumen()));
        renderArgs.put("kategori", pl.getKategori());
        final boolean jenis_paket_isEditable = dok_pl.isEditable(pl, newDate());
        renderArgs.put("jenis_paket_isEditable", jenis_paket_isEditable);
        if(dok_pl != null) {
            boolean sudahCetak = dok_pl != null && dok_pl.dll_id_attachment != null;
            renderArgs.put("allow_download", sudahCetak && pl.lls_status.isAktif());
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            boolean allow_adendum = sudahCetak && (isPp || isPanitia || active_user.isPpk()) && tahapStarted.isAllowAdendum() ;
            renderArgs.put("allow_adendum", allow_adendum); // allow adendum
            if(dok_pl_content != null) {
                if(dok_pl_content.ldpcontent != null)
                    renderArgs.put("masaberlaku", dok_pl_content.ldpcontent.masa_berlaku_penawaran);
                boolean isAdendum = dok_pl_content.isAdendum();
                boolean modified = dok_pl_content.dll_modified;
                boolean allowUpload = isAdendum && modified && (active_user.isPP() || active_user.isPanitia());
                renderArgs.put("allowUpload", allowUpload);
                renderArgs.put("ldk_adendum", dok_pl_content.dll_ldk_updated && modified && isAdendum);
                renderArgs.put("syarat_adendum", dok_pl_content.dll_syarat_updated && modified && isAdendum);
                renderArgs.put("hps_adendum", dok_pl_content.dll_dkh != null && modified && isAdendum);
                renderArgs.put("ldp_adendum", dok_pl_content.dll_ldp != null && modified && isAdendum);
                renderArgs.put("sskk_adendum", dok_pl_content.dll_spek != null && modified && isAdendum);
                renderArgs.put("spek_adendum", dok_pl_content.dll_spek != null && modified && isAdendum);
                renderArgs.put("lainnya_adendum", dok_pl_content.dll_lainnya != null && modified && isAdendum);
                renderArgs.put("rancangankontrak_adendum", dok_pl_content.dll_sskk_attachment != null && modified && isAdendum);
                renderArgs.put("jeniskontrak_adendum", dok_pl_content.dll_kontrak_pembayaran != null && modified && isAdendum);
            }
        }

        renderArgs.put("baTambahan", Berita_acara_pl.findBy(pl.lls_id, Tahap.UPLOAD_BA_TAMBAHAN));

        if(pl.isPenunjukanLangsungNew()) {
	        if (tahapStarted.isPengumumanPemenangAkhirNew()) {
	            EvaluasiPl penetapan = EvaluasiPl.findPenetapanPemenang(id);
	            renderArgs.put("allow_pengumuman_pemenang",	isPp || isPanitia && penetapan != null && penetapan.eva_status.isSelesai() && pl.hasPemenangTerverifikasi() && PersetujuanPl.isApprove(id, PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG));
	            int jumlahEmail = MailQueueDao.countByJenis(pl.lls_id, JenisEmail.PENGUMUMAN_PEMENANG.id);
	            renderArgs.put("sudahKirimPengumuman", jumlahEmail == pl.getJumlahPeserta());
	        }
        }
        else {
        	if (tahapStarted.isPengumumanPemenangAkhir()) {
	            EvaluasiPl penetapan = EvaluasiPl.findPenetapanPemenang(id);
	            renderArgs.put("allow_pengumuman_pemenang",	isPp || isPanitia && penetapan != null && penetapan.eva_status.isSelesai() && pl.hasPemenangTerverifikasi() && PersetujuanPl.isApprove(id, PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG));
	            int jumlahEmail = MailQueueDao.countByJenis(pl.lls_id, JenisEmail.PENGUMUMAN_PEMENANG.id);
	            renderArgs.put("sudahKirimPengumuman", jumlahEmail == pl.getJumlahPeserta());
	        }
        }
        if (active_user.isRekanan()) { // untuk rekanan
            PesertaPl peserta = PesertaPl.find("lls_id=? and rkn_id=?", pl.lls_id, active_user.rekananId).first();
            renderArgs.put("peserta", peserta);
            DokPenawaranPl penawaran_kualifikasi = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id,
                    DokPenawaranPl.JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
            renderArgs.put("penawaran_kualifikasi", penawaran_kualifikasi);
            boolean kualifikasiUploaded = penawaran_kualifikasi != null || pl.getPemilihan().isLelangExpress();
            renderArgs.put("kualifikasiUploaded", kualifikasiUploaded);
            Rekanan pemenang = null;
            if (tahapStarted.isShowHasilEvaluasi()) {
                EvaluasiPl evaluasipl = EvaluasiPl.findPenetapanPemenang(id);
                if (evaluasipl != null) {
                    NilaiEvaluasiPl nev = NilaiEvaluasiPl.find("eva_id=? and nev_lulus=1", evaluasipl.eva_id).first();
                    if (nev != null) {
                        pemenang = Rekanan.findByPesertaPl(nev.psr_id);
                    }
                }
            }
            renderArgs.put("pemenang", pemenang);
        } else { // untuk selain rekanan
            boolean hide =  !tahapStarted.isShowPenyedia();
            int jumlah_evaluasi = EvaluasiPl.findCountEvaluasiUlang(pl.lls_id);
            renderArgs.put("hide", hide);
            renderArgs.put("allow_report",
                    (active_user.isPP() || active_user.isPanitia() || active_user.isAuditor() || active_user.isPpk()) && !hide);
            renderArgs.put("allow_pemekaan", (active_user.isPP() || active_user.isPanitia() || active_user.isAuditor())
                    && false && tahapStarted.isPembukaan());
            renderArgs.put("allow_history_evaluasi", jumlah_evaluasi > 1 && tahapStarted.isEvaluasi());
            renderArgs.put("jumlah_evaluasi", jumlah_evaluasi);
            if (active_user.isPanitia()) {
                renderArgs.put("allow_pemasukan_ulang", tahapStarted.isAllowPemasukanUlang());
                renderArgs.put("allowEvaluasiUlang", tahapStarted.isAllowEvaluasiUlang());
            }
            if (active_user.isAuditor()) {
                Jadwal_pl kontrak = Jadwal_pl.findByLelangNTahap(id, Tahap.TANDATANGAN_KONTRAK);
                renderArgs.put("allow_keterangan", kontrak != null && kontrak.isNow(newDate()));
            }
        }
        if (active_user.isRekanan()){
        	if(!(pl.mtd_kualifikasi == 0)) {
        		PesertaPl pesertaPl = PesertaPl.findBy(pl.lls_id, active_user.rekananId);
        		boolean allow_kirim_penawaran = pesertaPl != null ? pl.isAllowKirimPenawaran(pesertaPl, tahapAktif) : false;
        		renderArgs.put("allow_kirim_penawaran", allow_kirim_penawaran);
        		renderArgs.put("total_penawaran", FormatUtils.formatCurrencyRupiah(pesertaPl.psr_harga));
        	}
        	else {
        		PesertaPl pesertaPl = PesertaPl.findBy(pl.lls_id, active_user.rekananId);
        		boolean allow_kirim_penawaran = pesertaPl != null ? pl.isAllowKirimPenawaranPl(pesertaPl, tahapAktif) : false;
        		renderArgs.put("allow_kirim_penawaran", allow_kirim_penawaran);
        		renderArgs.put("total_penawaran", FormatUtils.formatCurrencyRupiah(pesertaPl.psr_harga));        	
        	}
        }

        renderArgs.put("allow_edit_jadwal", !tahapNow.equalsIgnoreCase("Paket Sudah Selesai"));

        Boolean allowUpdate = false;
        Boolean allowForensik = false;
        if ("true".equals(Play.configuration.get("allow.update.lelang"))) {
            allowUpdate = true;
        }
        renderArgs.put("allowUpdate", allowUpdate);
        renderArgs.put("allowForensik", allowForensik);
        if(paket.isFlag43()){
            renderArgs.put("paketPpk", PaketPpkPl.findByPaket(paket.pkt_id));
        }else{
            renderArgs.put("ppkList", Paket_anggaran_pl.findByPaket(pl.pkt_id));
        }
        renderArgs.put("historyPpkList", History_paket_pl_ppk.findByPaket(paket.pkt_id));
        DokPersiapanPl dokPersiapanPl = paket.getDokPersiapan();
        renderArgs.put("dokPersiapan", dokPersiapanPl);
        renderArgs.put("surveyHarga", SurveyHargaPl.findBy(pl.lls_id));
        renderArgs.put("isFlag43",paket.isFlag43());
        renderTemplate("nonlelang/pl-view.html");
    }

    @AllowAccess({Group.PP, Group.PANITIA})
    public static void edit(Long id) {// oldId diisi jika non lelang ulang.
    	otorisasiDataPl(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        Pl_seleksi pl = Pl_seleksi.findById(id);
        Paket_pl paket = pl.getPaket();
        Double nilaiHps = paket.pkt_hps;
        renderArgs.put("active_user", active_user);
        if(Dok_pl.count("lls_id=?", id) == 0) { // check jika Dok_lelang null
            DokPersiapanPl dokPersiapan = DokPersiapanPl.findLastByPaket(paket.pkt_id);
            if(dokPersiapan != null)
                dokPersiapan.transferDokPersiapanToDokLelang();
        }

        renderArgs.put("pl", pl);
        renderArgs.put("paket", paket);
        boolean allow_persetujuan = false;
        renderArgs.put("pengadaanlangsung", pl.getPemilihan().isPenunjukanLangsung());
        // Menambah validasi apabila ppk belum diisi di paket tersebut
        if (Paket_anggaran_pl.count("pkt_id=? and ppk_id is null", paket.pkt_id) > 0) {
            flash.error(Messages.get("flash.mabmdppk"));
            PaketPlCtr.index(null, null);
        }

        // Validasi jika paket belum ada data lokasi pekerjaan
        if (CommonUtil.isEmpty(paket.getPaketLokasi())) {
            flash.error(Messages.get("flash.mabmdlppdp"));
            PaketPlCtr.index(null,null);
        }
        
        Kategori kategori = pl.getKategori();
        renderArgs.put("konsultansi", kategori.isConsultant());
        renderArgs.put("JkKonstruksi", kategori.isJkKonstruksi() || kategori.isKonstruksi());
        boolean draft = !pl.isLelangV3() && pl.lls_status.isDraft() ;
        boolean penunjukanLangsung = (pl.mtd_kualifikasi != null && pl.mtd_kualifikasi == 0);

        Dok_pl dok_kualifikasi_pl = null;
        Dok_pl dok_pl = Dok_pl.findBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG);
        if(penunjukanLangsung) {
        	dok_kualifikasi_pl = Dok_pl.findNCreateBy(pl.lls_id, JenisDokPl.DOKUMEN_LELANG_PRA);
        }

        renderArgs.put("penunjukanLangsung", penunjukanLangsung);
        renderArgs.put("dok_pl", dok_pl);
        renderArgs.put("dok_kualifikasi_pl", dok_kualifikasi_pl);
        boolean ldk_terisi = false;
        if (dok_kualifikasi_pl != null) {
            Dok_pl_content dok_kualifikasi_pl_content = Dok_pl_content.findBy(dok_kualifikasi_pl.dll_id, 1);
            if(dok_kualifikasi_pl_content == null) {
            	dok_kualifikasi_pl_content = Dok_pl_content.findNCreateBy(dok_kualifikasi_pl.dll_id, pl);
            }

            ldk_terisi = !CommonUtil.isEmpty(dok_kualifikasi_pl.getSyaratIjinUsaha()) || !CommonUtil.isEmpty(dok_kualifikasi_pl.getSyaratKualifikasi())
            			|| !CommonUtil.isEmpty(dok_kualifikasi_pl.getSyaratIjinUsahaBaru()) || !CommonUtil.isEmpty(dok_kualifikasi_pl.getSyaratKualifikasiAdministrasi());
            renderArgs.put("ldk_terisi", ldk_terisi);
            
            if (dok_kualifikasi_pl_content != null) {
                renderArgs.put("dok_kualifikasi_pl_content", dok_kualifikasi_pl_content);
                if (penunjukanLangsung)
                    allow_persetujuan = !dok_kualifikasi_pl_content.dll_modified && dok_kualifikasi_pl.dll_id_attachment != null && ldk_terisi && paket.pkt_hps > 0;
            }
            boolean allow_upload_dok_kualifikasi = draft && dok_kualifikasi_pl_content.dll_modified && pl.isJadwalSudahTerisi() && ldk_terisi;
            renderArgs.put("allow_upload_dok_kualifikasi", allow_upload_dok_kualifikasi);
            renderArgs.put("dokKualifikasiList", dok_kualifikasi_pl.getDokumenList());
        }

        if (dok_pl != null) {
            Integer masaBerlaku = 0; // default masa berlaku penawaran
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id, 1);
            if(dok_pl_content == null){
                dok_pl_content = Dok_pl_content.findNCreateBy(dok_pl.dll_id, pl);
            }
            if(dok_pl_content != null ){
                nilaiHps = !StringUtils.isEmpty(dok_pl_content.dll_dkh) ? nilaiHps : 0d;
                if(dok_pl_content.ldpcontent != null){
                    masaBerlaku = dok_pl_content.ldpcontent.masa_berlaku_penawaran;
                }
                renderArgs.put("dok_pl_content", dok_pl_content);
            }
            renderArgs.put("masaberlaku", masaBerlaku);
            boolean persyaratan_terisi = !CommonUtil.isEmpty(dok_pl.getSyaratTeknis()) || !CommonUtil.isEmpty(dok_pl.getSyaratHarga());
            renderArgs.put("persyaratan_terisi", persyaratan_terisi);
            List<BlobTable> dok_sskk = dok_pl_content.getDokSskk();
            renderArgs.put("dok_sskk", dok_sskk);
            boolean rancangan_kontrak_terisi = !CommonUtil.isEmpty(dok_sskk);

            if(!(penunjukanLangsung)) {
            	renderArgs.put("dok_pl", dok_pl);
	            renderArgs.put("dok_pl_content", dok_pl_content);
	            ldk_terisi = !CommonUtil.isEmpty(dok_pl.getSyaratIjinUsaha()) || !CommonUtil.isEmpty(dok_pl.getSyaratKualifikasi())
            			|| !CommonUtil.isEmpty(dok_pl.getSyaratIjinUsahaBaru()) || !CommonUtil.isEmpty(dok_pl.getSyaratKualifikasiAdministrasi());;                	            
	            renderArgs.put("ldk_terisi", ldk_terisi);
	            allow_persetujuan = !StringUtils.isEmpty(dok_pl_content.dll_dkh) && dok_pl.dll_id_attachment != null;
	            renderArgs.put("allow_upload_dok", draft && dok_pl_content.dll_modified && pl.isJadwalSudahTerisi() && persyaratan_terisi && masaBerlaku > 0);
            }
            else {
            	 boolean allow_upload_dok = dok_pl_content.dll_modified && pl.isJadwalSudahTerisi() && persyaratan_terisi && masaBerlaku > 0 && rancangan_kontrak_terisi;
                 EvaluasiPl pembuktian = EvaluasiPl.findPembuktian(id);
                 EvaluasiPl evakualifikasi = EvaluasiPl.findKualifikasi(id);
                 allow_upload_dok = allow_upload_dok && (pembuktian == null || pembuktian.eva_status.isSedangEvaluasi()) && (evakualifikasi == null || evakualifikasi.eva_status.isSedangEvaluasi());
                 renderArgs.put("allow_upload_dok", allow_upload_dok);
            }
        }

        if (!penunjukanLangsung && dok_kualifikasi_pl != null) {
            ChecklistPl.delete("dll_id=?", dok_kualifikasi_pl.dll_id);
            Dok_pl.delete("dll_id=?", dok_kualifikasi_pl.dll_id);
            dok_kualifikasi_pl.delete();
        }


        List<DraftPesertaPl> draftPesertaPlList = DraftPesertaPl.findWithLelang(pl.lls_id);

        renderArgs.put("jumlah_peserta_terdaftar", draftPesertaPlList.size());

        renderArgs.put("peserta_terdaftar", draftPesertaPlList.size() > 0 ? true : false);
        renderArgs.put("kategori", kategori);
        renderArgs.put("draft", !pl.isLelangV3() && pl.lls_status.isDraft());
        renderArgs.put("ketJadwal", pl.getInfoJadwal());
        renderArgs.put("draftPesertaPlList", draftPesertaPlList);


        if (pl.getPemilihan().isPl()) {
            Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
            List<Long> listKab = Query.find("select kbp_id from ekontrak.paket_lokasi where pkt_id=?", Long.class, paket.pkt_id).fetch();
            Paket_satker_pl paket_satker = Paket_satker_pl.find("pkt_id=?", paket.pkt_id).first();
            Satuan_kerja satker = Satuan_kerja.findById(paket_satker.stk_id);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("userid", active_user.userid);
            if(active_user.isPP())
                jsonObject.addProperty("role", "PP");
            else
                jsonObject.addProperty("role", "PANITIA");
            jsonObject.addProperty("nama", pegawai.peg_nama);
            jsonObject.addProperty("repo_id", ConfigurationDao.getRepoId());
            jsonObject.addProperty("prod", ConfigurationDao.isProduction() ? "TRUE" : "FALSE");
            jsonObject.addProperty("lls_id", pl.lls_id);
            jsonObject.addProperty("pkt_nama", paket.pkt_nama);
            jsonObject.addProperty("kls_id", paket.kls_id);
            jsonObject.addProperty("kgr_id", paket.kgr_id.id);
            jsonObject.addProperty("pkt_hps", paket.pkt_hps);
            if (satker != null)
                jsonObject.addProperty("instansi_id", satker.instansi_id);
            if (!CommonUtil.isEmpty(listKab))
                jsonObject.addProperty("kbp_id", StringUtils.join(listKab, ","));
            try {
                String param = URLEncoder.encode(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)), "UTF-8");
                renderArgs.put("sikap_shorlist_url", BasicCtr.SIKAP_URL + "/services/getListRekanan?q=" + param);
                renderArgs.put("sikap_kriteria_url",
                        BasicCtr.SIKAP_URL + "/services/getListRekanan?q=" + pl.lls_id);

//                renderArgs.put("old_id", oldId);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if(Active_user.current().isPanitia() || Active_user.current().isPP()){
            PersetujuanPl persetujuan_pegawai = PersetujuanPl.findByPegawaiPengumuman(active_user.pegawaiId, pl.lls_id);
            if (persetujuan_pegawai != null && allow_persetujuan) {
                allow_persetujuan = persetujuan_pegawai.pst_status.isBelumSetuju()
                        || persetujuan_pegawai.pst_status.isTidakSetuju();
            }
        }

        if(pl.isLelangUlang()){
            if(pl.lls_diulang_karena == null || pl.lls_diulang_karena.isEmpty()){
                allow_persetujuan = false;
            }
        }
        renderArgs.put("allowBatalPersetujuan", pl.allowBatalPersetujuan());
        if(!kategori.isConsultant() && !kategori.isJkKonstruksi()){
            allow_persetujuan = allow_persetujuan && !StringUtils.isEmpty(paket.kls_id);
        }
        renderArgs.put("nilaiHps", nilaiHps);
        renderArgs.put("allow_persetujuan" ,allow_persetujuan && pl.isJadwalSudahTerisi());
//        List<SurveyHargaPl> surveyHargaPl = SurveyHargaPl.findAllBy(pl.lls_id);
//        renderArgs.put("surveyHargaPl", surveyHargaPl);
        DokPersiapanPl dokPersiapanPl = paket.getDokPersiapan();
        renderArgs.put("dokPersiapan", dokPersiapanPl);
        renderArgs.put("surveyHarga", SurveyHargaPl.findBy(pl.lls_id));
        renderArgs.put("nilaiHps", nilaiHps);
        renderTemplate("nonlelang/pl-edit.html");
    }

    private static boolean isMandatoryDokCompleted(Dok_pl_content dok_lelang_content, Pl_seleksi pl, boolean ldk_terisi,
                                                   boolean persyaratan_terisi){

        if(dok_lelang_content == null){
            return false;
        }

        boolean result = true;

        if(pl.mtd_pemilihan.equals(MetodePemilihan.PENUNJUKAN_LANGSUNG.id)){
            result = dok_lelang_content.dll_spek != null;
        }

        return ldk_terisi && persyaratan_terisi && dok_lelang_content.dll_ldp != null && result;

    }

    @AllowAccess({Group.PP, Group.PANITIA})
    public static void simpan(Long id, Pl_seleksi pl, Integer kategoriId, String kualifikasiId) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data lelang
        Pl_seleksi obj = Pl_seleksi.findById(id);

        if (kualifikasiId != null && kualifikasiId.isEmpty()) {
        		flash.error(Messages.get("flash.kuhdi"));
        		edit(obj.lls_id);
        } else {
        		if(pl == null)
                pl = new Pl_seleksi();

            if (pl.mtd_pemilihan != null) {
                obj.mtd_pemilihan = pl.mtd_pemilihan;// old metode to new metode
            }

            if (pl.lls_versi_lelang == null) {
                Paket_pl paket = Paket_pl.findBy(id);
                if (paket.pkt_status.equals(StatusPaket.ULANG_LELANG)) {
                    if (pl.lls_diulang_karena != null && !pl.lls_diulang_karena.isEmpty()) {
                        obj.lls_diulang_karena = pl.lls_diulang_karena;
                    } else {
                        flash.error(Messages.get("flash.amlhdi"));
                        edit(obj.lls_id);
                    }
                }
            }
            
            if (kualifikasiId != null) {
	            Paket_pl paket = Paket_pl.findById(obj.pkt_id);
	            paket.kls_id = kualifikasiId;
	            paket.save();
            }
            obj.save();
            if(obj.mtd_kualifikasi==null || obj.mtd_pemilihan==null)
                flash.error("Metode Pengadaan harus dipilih");
            edit(obj.lls_id);
        }

    }

    @AllowAccess({Group.PPK})
    public static void hapus(Long id) {
        otorisasiDataPl(id); // check otorisasi data Pl
        Pl_seleksi pl = Pl_seleksi.findById(id);

        if (!pl.lls_status.isDraft()) {
            flash.error(Messages.get("lash.mnlysatdhps"));
            PaketPlCtr.index(null, null);
        }

        try {
            pl.hapusPl(id);
        } catch (Exception ex) {
            Logger.info("Error delete " + ex.getLocalizedMessage());
            flash.error(Messages.get("flash.gmntdr") + id);
            PaketPlCtr.index(id, null);
        }

        PaketPlCtr.index(null, null);
    }

    //pilih penyedia
    @AllowAccess({Group.PP, Group.PANITIA})
    public static void viewPilihPenyedia(Long lelangId, Boolean search, String npwp, String nama, Long kabupatenId, Long propinsiId, String jenisIjin) throws ExecutionException, InterruptedException, UnsupportedEncodingException {
        otorisasiDataPl(lelangId); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(lelangId);
        List<Map<String, String>> rekananList = new ArrayList<>();
        String rekananListJson = null;
        Active_user active_user = Active_user.current();
        search = search == null ? false : search;
        List<DraftPesertaPl> draftPesertaPlList = null;
        if (pl != null) {
            if ((npwp != null && !npwp.isEmpty()) || (nama != null && !nama.isEmpty()) || (jenisIjin != null && !jenisIjin.isEmpty()) || kabupatenId != null) {
                rekananList = RekananSikap.getListPenyediaByNpwpFromSikap(npwp, nama, kabupatenId, jenisIjin, pl.lls_id, "pl");
                rekananListJson = CommonUtil.toJson(rekananList);

            } else if(search) {
                    flash.error(Messages.get("flash.smptdhsspp"));
            }

            if ((rekananList == null || rekananList.size() == 0) && (kabupatenId != null || (jenisIjin != null && !jenisIjin.isEmpty()) || (nama != null && !nama.isEmpty()) || (npwp != null && !npwp.isEmpty()))) {
                rekananList = new ArrayList<>();
                flash.error(Messages.get("flash.ptdmk"));
            }

            draftPesertaPlList = DraftPesertaPl.findWithLelang(pl.lls_id);
        }

        render("nonlelang/pl-pilih-penyedia.html", pl, rekananList, npwp, nama, kabupatenId, propinsiId, jenisIjin, rekananListJson, active_user, draftPesertaPlList);

    }



    @AllowAccess({Group.PP, Group.PANITIA})
    public static void simpanPilihPenyedia(Long lelangId, String npwp, String nama, Long kabupatenId, Long propinsiId,
                                           String jenisIjin,  List<Long> rekananId, List<String> rekananNama,
                                           List<String> rekananNpwp, List<String> rekananEmail, List<String> rekananTelepon,
                                           List<String> rekananAlamat)
            throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        otorisasiDataPl(lelangId); // check otorisasi data lelang

        rekananId = rekananId != null ? rekananId : new ArrayList<Long>();

        rekananId.removeAll(Collections.singleton(null));

        rekananNama = rekananNama != null ? rekananNama : new ArrayList<String>();

        rekananNama.removeAll(Collections.singleton(null));

        rekananNpwp = rekananNpwp != null ? rekananNpwp : new ArrayList<String>();

        rekananNpwp.removeAll(Collections.singleton(null));

        rekananEmail = rekananEmail != null ? rekananEmail : new ArrayList<String>();

        rekananEmail.removeAll(Collections.singleton(null));

        rekananTelepon = rekananTelepon != null ? rekananTelepon : new ArrayList<String>();

        rekananTelepon.removeAll(Collections.singleton(null));

        rekananAlamat = rekananAlamat != null ? rekananAlamat : new ArrayList<String>();

        rekananAlamat.removeAll(Collections.singleton(null));

        if (rekananId.size() != 0) {

            int i = 0;

            int limit = 0;

            boolean isAllowSimpanPengadaan = true;

            Pl_seleksi pl_seleksi = Pl_seleksi.findById(lelangId);

            if (pl_seleksi.getPemilihan().isPenunjukanLangsung()) {

                List<DraftPesertaPl> draftPesertaPlList = DraftPesertaPl.findWithLelang(pl_seleksi.lls_id);

                limit = 1 - draftPesertaPlList.size();

                isAllowSimpanPengadaan = rekananId.size() <= limit ? true : false;

            }

            if (isAllowSimpanPengadaan) {

                for (Long rkn_id : rekananId) {

                    DraftPesertaPl draftPesertaPl = new DraftPesertaPl();

                    draftPesertaPl.lls_id = lelangId;
                    draftPesertaPl.rkn_id = rkn_id;
                    draftPesertaPl.dp_nama_rekanan = rekananNama.get(i);
                    draftPesertaPl.dp_npwp_rekanan = rekananNpwp.get(i);
                    draftPesertaPl.dp_email_rekanan = rekananEmail.get(i);
                    draftPesertaPl.dp_telp_rekanan = rekananTelepon.get(i);
                    draftPesertaPl.dp_alamat_rekanan = rekananAlamat.get(i);
//                    draftPesertaPl.repo_id = repoId.get(i);
                    draftPesertaPl.save();

                    i++;

                }

                flash.success(Messages.get("flash.bsmpndrftp"));

            } else {

                if (pl_seleksi.getPemilihan().isPenunjukanLangsung() && limit == 0)
                    flash.error(Messages.get("flash.mjpydtntkn"));
                else
                    flash.error("<i class=\"fa fa-warning\"></i>&nbsp;&nbsp;"+Messages.get("flash.mjpyd") +
                           Messages.get("flash.ahdm") + limit + Messages.get("flash.penyedia"));

            }
        } else {
            flash.error(Messages.get("flash.abmp_spptd"));
        }

        viewPilihPenyedia(lelangId, true, npwp, nama, kabupatenId, propinsiId, jenisIjin);

    }


    @AllowAccess({Group.PP, Group.PANITIA})
    public static void hapusDraftPenyedia(Long draftPenyediaId) throws ExecutionException, InterruptedException {

        DraftPesertaPl draftPesertaPl = DraftPesertaPl.findById(draftPenyediaId);

        Long lelangId = null;

        if (draftPesertaPl != null) {

            lelangId = draftPesertaPl.lls_id;

            draftPesertaPl.delete();

            flash.success(Messages.get("flash.bhpspeserta"));
        }

        edit(lelangId);

    }

    @AllowAccess({Group.PANITIA, Group.PP})
    public static void pengumuman(Long id, String alasan, boolean setuju) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data lelang

        Pl_seleksi lelang = Pl_seleksi.findById(id);
        if (!lelang.isJadwalSudahTerisi()) {
            Validation.addError("jadwal", Messages.get("flash.jadwal_kosong"));
            flash.error(Messages.get("flash.jntndrmksg"));
        }
        if (Active_user.current().isPanitia() && !setuju && CommonUtil.isEmpty(alasan)) {
            Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
        }
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
        } else {
            Active_user active_user = Active_user.current();
            if(active_user.isPanitia()){
                PersetujuanPl persetujuan = PersetujuanPl.find("peg_id=? and lls_id=? and pst_jenis=?",
                        Active_user.current().pegawaiId, id, PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG).first();
                persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.SETUJU;
                if (!CommonUtil.isEmpty(alasan)) {
                    persetujuan.pst_alasan = alasan;
                    // jika ada alasan maka status tidak setuju
                    persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.TIDAK_SETUJU;
                }
                if (persetujuan.pst_status.isSetuju()) {
                    persetujuan.pst_alasan = null; //clear alasan
                }
                persetujuan.pst_tgl_setuju = BasicCtr.newDate();
                persetujuan.save();
                // tambahkan ke riwayat persetujuan
                Riwayat_PersetujuanPl.simpanRiwayatPersetujuan(persetujuan);
            }

            boolean publish = active_user.isPP() || PersetujuanPl.isApprove(id, PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG);
            if(publish){
                int sesi = 0;
                if (ConfigurationDao.isLatihan())
                    sesi = BasicCtr.getSesiPelatihan().id;
                if (lelang.lls_status.isDraft()) {
                    ProcessInstancePl proses = WorkflowPlDao.createProsesInstance(lelang.lls_id, lelang.lls_wf_id, lelang.getPemilihan());
                    lelang.lls_wf_id = proses.process_instance_id;
                    lelang.save();
                    WorkflowPlDao.updateState(proses);
                    // proses.startInstance();
                    proses.status = ProcessInstancePl.PROCESS_STATUS.RUNNING;
                    proses.save();
                    lelang.lls_sesi = sesi;
                    lelang.lls_status = StatusPl.AKTIF;
                    lelang.lls_tgl_setuju = newDate();
                    Paket_pl paket = Paket_pl.findById(lelang.pkt_id);
                    paket.pkt_status = Paket_pl.StatusPaket.SEDANG_LELANG;
                    paket.save();
                    lelang.save();
                    try {
                        String referer = getRequestHost();
                        MailQueueDao.kirimUndanganPl(lelang, referer);
                    } catch (Exception e) {
                        Logger.error("Gagal Kirim Undangan PL : " + e.getLocalizedMessage());
                    }
                    BerandaCtr.index();
                }
            }
        }

        edit(id);
    }

    @AllowAccess({Group.PP})
    public static void pengumumanPp(Long id, String alasan, boolean setuju) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data lelang

        Pl_seleksi lelang = Pl_seleksi.findById(id);
        Paket_pl paket = lelang.getPaket();
        if (!lelang.isJadwalSudahTerisi()) {
            Validation.addError("jadwal", Messages.get("flash.jadwal_kosong"));
            flash.error(Messages.get("flash.jntndrmksg"));
        }

        if (Active_user.current().isPP() && !setuju && CommonUtil.isEmpty(alasan)) {
            Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
        }

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();

        } else {
                PersetujuanPl persetujuan = PersetujuanPl.find("peg_id=? and lls_id=? and pst_jenis=?", Active_user.current().pegawaiId, id, PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG).first();
                persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.SETUJU;
                if (!CommonUtil.isEmpty(alasan)) {
                    persetujuan.pst_alasan = alasan;
                    // jika ada alasan maka status tidak setuju
                    persetujuan.pst_status = PersetujuanPl.StatusPersetujuan.TIDAK_SETUJU;
                }
                if (persetujuan.pst_status.isSetuju()) {
                    persetujuan.pst_alasan = null; //clear alasan
                }
                persetujuan.pst_tgl_setuju = BasicCtr.newDate();
                persetujuan.save();

                // tambahkan ke riwayat persetujuan
                Riwayat_PersetujuanPl.simpanRiwayatPersetujuan(persetujuan);

                if (PersetujuanPl.isApprove(id, PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG)) {
                    int sesi = 0;
                    if (ConfigurationDao.isLatihan())
                        sesi = BasicCtr.getSesiPelatihan().id;
                    if(lelang.lls_status.isDraft()) {
//                        if (!lelang.isExpress()) {
                            ProcessInstancePl proses = WorkflowPlDao.createProsesInstance(lelang.lls_id, lelang.lls_wf_id, lelang.getPemilihan());
                            lelang.lls_wf_id = proses.process_instance_id;
                            lelang.save();
                            WorkflowPlDao.updateState(proses);
                            // proses.startInstance();
                            proses.status = ProcessInstancePl.PROCESS_STATUS.RUNNING;
                            proses.save();
//                        }
//                        else
//                            {
                            //lelang cepat kirim undangan ke penyedia
//                            JsonObject jsonObject = new JsonObject();
//                            jsonObject.addProperty("lls_id", id);
//                            jsonObject.addProperty("repo_id", ConfigurationDao.getRepoId());
//                            String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
//                            try (WS.WSRequest request = WS.url(URL_SIKAP_KIRIM_UNDANGAN + param)) {
//                                request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
//                            } catch (Exception ex) {
//                                Logger.error(ex, "Gagal mengirim undangan/tidak dapat terhubung ke aplikasi SIKaP");
//                            }
//                            // jika paket 4.3 tender cepat , ada proses reverse auction
////							if(paket.isFlag43()) {
//                            ReverseAuction.create(lelang, Integer.valueOf(1));
//							}
//                        }
                        lelang.lls_sesi = sesi;
                        lelang.lls_status = StatusPl.AKTIF;
                        lelang.lls_tgl_setuju = newDate();
                        paket.pkt_status = Paket_pl.StatusPaket.SEDANG_LELANG;
                        paket.save();
                        lelang.save();
                        try {
                            String referer = getRequestHost();
                            MailQueueDao.kirimUndanganPl(lelang, referer);
                        } catch (Exception e) {
                            Logger.error("Gagal Kirim Undangan PL : " + e.getLocalizedMessage());
                        }
//                        Lelang_key.generateKey(lelang); // generate kunci lelang
                        // buat KPL jika OSD enabled
//                        if(isOSD) {
//                            OSDUtil.createKPL(paket, anggotaPanitia, lelang, getRequestHost());
//                        }
//                        LelangQuery.save(lelang, paket);
                    }
                }
        }

        edit(id);
    }

    public static void pengumumanPl(Long id) {
        Pl_detil pl = Pl_detil.findById(id);
        renderArgs.put("pl", pl);
        if (pl != null && !pl.lls_status.isAktif())
            forbidden(Messages.get("not-authorized"));// hanya lelang aktif yang muncul di pengumuman
        renderArgs.put("tahapStarted", new TahapStartedPl(id));
        renderArgs.put("tahapNow", Jadwal_pl.getJadwalSekarang(id,true,pl.getPemilihan().isLelangExpress(),false));
        renderArgs.put("anggaranList", Anggaran.findByPaketPl(pl.pkt_id));
        renderArgs.put("lokasiList", Paket_pl_lokasi.find("pkt_id=?", pl.pkt_id).fetch());
        Paket_pl paket = Paket_pl.findById(pl.pkt_id);
        renderArgs.put("paket", paket);
        Dok_pl dok_pl = Dok_pl.findBy(id, JenisDokPl.DOKUMEN_LELANG);
        renderArgs.put("rupList", Paket_pl.getRupList(pl.pkt_id));
        if (dok_pl != null) {
            Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
            if (dok_pl_content != null) {
                LDPContent ldpContent = dok_pl_content.ldpcontent;
                String lingkup = ldpContent != null ? ldpContent.lingkup_pekerjaan : "";
                renderArgs.put("lingkup", lingkup);
            }
        }
        renderTemplate("nonlelang/pl-pengumuman-pl.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void plBaru() {
        Date now = newDate();
        Blacklist blacklist = Blacklist.find("rkn_id=? and bll_tglawal <= ? AND bll_tglakhir >= ?", Active_user.current().rekananId, now, now).first();
        if (blacklist != null)
            BerandaCtr.nonLelang(); // jika rekanan terblacklist tidak dpt ikut lelang dan kembali home rekanan
        else
            render("nonlelang/pl-baru.html");
    }

    public static String getRequestHost() {
        if (request.secure)
            return "https://" + request.host;
        else
            return "http://" + request.host;
    }

    @AllowAccess({Group.PP, Group.PANITIA})
    public static void undanganPra(Long id) {
        otorisasiDataPl(id); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(id);
        boolean adaPemenang = NilaiEvaluasiPl.countLulus(pl.lls_id, EvaluasiPl.JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI) > 0;
        int jumlahEmail = MailQueueDao.countByJenis(pl.lls_id, JenisEmail.PENGUMUMAN_PEMENANG_PRA.id);
        boolean sudahKirim = jumlahEmail == pl.getJumlahPeserta();
        renderArgs.put("pl", pl);
        renderArgs.put("adaPemenang", adaPemenang);
        renderArgs.put("sudahKirim", sudahKirim);
        renderTemplate("nonlelang/undangan-pl-pra.html");
    }

    @AllowAccess({Group.PP, Group.PANITIA})
    public static void kirimUndanganPra(Long id) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data PL
        Pl_seleksi pl = Pl_seleksi.findById(id);
        TahapNowPl tahapNow = new TahapNowPl(id);
        if (tahapNow.isPenetapanHasilPra()) {
            try {
                MailQueueDao.kirimPengumumanPrakualifikasiPl(pl);
                flash.success(Messages.get("flash.ifpptk"));
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat Kirim Undangan");
                flash.error(Messages.get("flash.mtkslhtknsp"));
            }
        }
        else
            flash.error(Messages.get("flash.mwaupth"));
        view(id);
    }

    @AllowAccess({Group.PP, Group.PANITIA})
    public static void undangan(Long id) {
        otorisasiDataPl(id); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(id);
        boolean adaPemenang = NilaiEvaluasiPl.countLulus(pl.lls_id, EvaluasiPl.JenisEvaluasi.EVALUASI_AKHIR) > 0;
        int jumlahEmail = MailQueueDao.countByJenis(pl.lls_id, JenisEmail.PENGUMUMAN_PEMENANG.id);
        boolean sudahKirim = jumlahEmail == pl.getJumlahPeserta();
        renderArgs.put("pl", pl);
        renderArgs.put("adaPemenang", adaPemenang);
        renderArgs.put("sudahKirim", sudahKirim);
        renderTemplate("nonlelang/pl-undangan.html");

    }

    @AllowAccess({Group.PP, Group.PANITIA})
    public static void kirimUndangan(Long id) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data PL
        Pl_seleksi pl = Pl_seleksi.findById(id);
        TahapNowPl tahapNow = new TahapNowPl(id);

        if(pl.isPenunjukanLangsungNew()) {
	        if (tahapNow.isPengumuman_PemenangNew()) {
	            try {
	                MailQueueDao.kirimPengumumanPemenangPl(pl);
	                flash.success("Informasi Pengumuman Pemenang telah dikirim");
	            } catch (Exception e) {
	                Logger.error(e, "Kesalahan saat Kirim Undangan");
	                flash.error("Maaf terdapat kesalahan teknis pengiriman. Mohon Ulangi!");
	            }
	        } 
	        else
	            flash.error("Maaf Waktu Anda Untuk Mengirim Pengumuman telah habis");
        
        }else {
        	if (tahapNow.isPengumuman_Pemenang()) {
	            try {
	                MailQueueDao.kirimPengumumanPemenangPl(pl);
	                flash.success("Informasi Pengumuman Pemenang telah dikirim");
	            } catch (Exception e) {
	                Logger.error(e, "Kesalahan saat Kirim Undangan");
	                flash.error("Maaf terdapat kesalahan teknis pengiriman. Mohon Ulangi!");
	            }
	        } 
	        else
	            flash.error("Maaf Waktu Anda Untuk Mengirim Pengumuman telah habis");
        }
        view(id);
    }


    @AllowAccess({ Group.PP, Group.PANITIA })
    public static void tutupPl(Long id) {
        otorisasiDataPl(id);
        Pl_seleksi pl = Pl_seleksi.findById(id);
        Active_user activeUser = Active_user.current();
        renderArgs.put("pl", pl);
        renderArgs.put("tahapNow", Jadwal_pl.getJadwalSekarang(id, true, pl.getPemilihan().isLelangExpress(), pl.isLelangV3()));
        renderArgs.put("isPanitia",activeUser.isPanitia());

        if(activeUser.isPanitia()){
            PersetujuanPl persetujuanPl = PersetujuanPl.findByPegawaiLelangInJenisBatal(pl.lls_id);
            renderArgs.put("persetujuan",persetujuanPl);
            if(persetujuanPl != null){
                renderArgs.put("allowPersetujuan",persetujuanPl.allowPersetujuanBatal());
            }
        }

        renderTemplate("nonlelang/pl-tutup.html");
    }

    /**
     * Admin Lpse mengubah jadwal Fungsi {@code ubahJadwalPL} digunakan
     * untuk menampilkan halaman.
     *
     * @param lelangId
     */
    @AllowAccess({Group.ADM_PPE})
    public static void buka(Long lelangId) {
        if (lelangId != null) {
            Pl_seleksi pl = Pl_seleksi.findById(lelangId);
            if (pl != null) {
                renderArgs.put("pl", pl);
            } else if (lelangId != 0) {
                flash.error("Paket yang Anda cari belum terdaftar");
            }
        }
        renderTemplate("nonlelang/pl-buka.html");
    }

    @AllowAccess({ Group.PP, Group.PANITIA })
    public static void tutupPlSubmit(Long id,  String actionLelang, @Required String alasan, Boolean setuju, String alasanTidakSetuju) {
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data pl

        if(setuju == null){
            setuju = true;
        }

        if (!setuju && CommonUtil.isEmpty(alasanTidakSetuju)) {
            Validation.addError("alasanTidakSetuju", Messages.get("flash.alasan_harus_diis"));
        }else if (!setuju && !CommonUtil.isEmpty(alasanTidakSetuju) && alasan.length() <= 10) {
            Validation.addError("alasanTidakSetuju", Messages.get("flash.ahssc"));
        }

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            tutupPl(id);
        } else {
            try {
                Pl_seleksi pl = Pl_seleksi.findById(id);
                Active_user activeUser = Active_user.current();
                PersetujuanPl.JenisPersetujuanPl jenis = PersetujuanPl.JenisPersetujuanPl.BATAL_LELANG;
                if(actionLelang.equals("ulang")){
                    jenis = PersetujuanPl.JenisPersetujuanPl.ULANG_LELANG;
                }

                if(activeUser.isPanitia()){ //Jika panitia, harus kolektif kolegial terlebih dahulu
                    pl.lls_ditutup_karena = alasan;
                    pl.save();
                    PersetujuanPl.simpanPersetujuanBatalTender(pl,setuju,alasanTidakSetuju,jenis);
                    if(PersetujuanPl.isApprove(id, jenis)){
                        tutupProcess(jenis,pl,alasan);
                    }
                }else{
                	tutupProcess(jenis,pl,alasan);
                }
            }catch (Exception e){
                Logger.error("Kesalahan Tutup Paket",e.getStackTrace());
                flash.error(e.getMessage());
            }
        }
        tutupPl(id);
    }

    /**
     * submit penutupan paket atau paket diulang
     *
     * @param id
     * @param tutup
     * @param ulang
     * @param alasan
     */
    @AllowAccess({Group.ADM_PPE})
    public static void bukaSubmit(Long id, boolean tutup, boolean ulang, @Required String alasan) {
        checkAuthenticity();
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            buka(id);
        } else {
            try {
                Pl_seleksi pl = Pl_seleksi.findById(id);
                pl.bukaLelang(alasan);
                flash.success("Reaktivasi Non Tender telah berhasil dilakukan");
            } catch (Exception e) {
                Logger.error("Kesalahan Reaktivasi Non Tender %s", e.getLocalizedMessage());
                flash.error("Reaktivasi Non Tender tidak dapat dilakukan. Mohon ulangi");
            }
            buka(id);
        }
    }

    private static void tutupProcess(PersetujuanPl.JenisPersetujuanPl jenis, Pl_seleksi pl, String alasan){
        if (jenis.equals(PersetujuanPl.JenisPersetujuanPl.BATAL_LELANG)) {
            try {
                Pl_seleksi.tutupPl(pl, alasan, "BATAL");
                BerandaCtr.index();
            } catch (Exception e) {
                Logger.error("Kesalahan Non Tender Ulang %s", e.getLocalizedMessage());
                flash.error(Messages.get("flash.pbntndrtdpt"));
            }
        } else if (jenis.equals(PersetujuanPl.JenisPersetujuanPl.ULANG_LELANG)) {
            try {
                Pl_seleksi.tutupPl(pl, alasan, "ULANG");
                Long lelangIdBaru = pl.ulangPl(pl.lls_id);
                edit(lelangIdBaru);
            } catch (Exception e) {
                Logger.error("Kesalahan Non Tender Ulang %s", e.getStackTrace());
                flash.error(Messages.get("flash.pmnttddmu"));
            }
        }
    }

    @AllowAccess({Group.PANITIA, Group.PP})
    public static void metode(Long id){
        Active_user user = Active_user.current();
        otorisasiDataPl(id); // check otorisasi data non tender
        Pl_seleksi pl = Pl_seleksi.findById(id);
        renderArgs.put("user", user);
        renderArgs.put("draft", !pl.isLelangV3() && pl.lls_status.isDraft());
        renderArgs.put("pl", pl);
        renderArgs.put("kategori", pl.getPaket().kgr_id);
        renderArgs.put("kategoriList", Kategori.all);
        if(user.isPP()){
            renderArgs.put("metodeKualifikasiList", MetodeKualifikasi.kualifikasiPraPasca);
            renderArgs.put("metodePemilihanList", MetodePemilihanPenyedia.listMetodePemilihanNonTender);
        }else{
            renderArgs.put("metodeKualifikasiList", MetodeKualifikasi.kualifikasiPraPasca2);
            renderArgs.put("metodePemilihanList", MetodePemilihanPenyedia.listMetodePemilihanPokja);
        }

        renderTemplate("nonlelang/pilih-metode.html");
    }

    public static void updateHistoryLelangForPpk(Long lls_id, Long ppk_id) {

        Pl_seleksi pl = Pl_seleksi.findById(lls_id);
        JsonObject beforeJson = generateJsonObjectForUpdateLelang(pl, ppk_id);
        JsonObject afterJson = generateJsonObjectForUpdateLelang(pl, null);
        UpdateLelangHistory updateLelangHistory = new UpdateLelangHistory();
        updateLelangHistory.setLls_id(pl.lls_id);
        updateLelangHistory.setUlh_before(beforeJson.toString());
        updateLelangHistory.setUlh_after(afterJson.toString());
        updateLelangHistory.save();

        view(lls_id);

    }

    private static JsonObject generateJsonObjectForUpdateLelang(Pl_seleksi pl, Long ppk_id) {

        Paket_pl paket = pl.getPaket();
        JsonObject jsonObject = new JsonObject();

        //jsonObject.addProperty("pkt_nama", paket.pkt_nama);
        jsonObject.addProperty("ppk_id", Paket_anggaran_pl.findPPKByPaket(paket.pkt_id));
        jsonObject.addProperty("pkt_hps", paket.pkt_hps.longValue());
        jsonObject.addProperty("pkt_pagu", paket.pkt_pagu.longValue());
        jsonObject.addProperty("lls_kontrak_pembayaran", pl.lls_kontrak_pembayaran);
        if (paket.kls_id != null) {
            jsonObject.addProperty("kls_id", paket.getKualifikasi().id);
        }

        List<Paket_pl_lokasi> lokasiList = Paket_pl_lokasi.find("pkt_id=?", paket.pkt_id).fetch();
        if (lokasiList.size() > 0) {
            JsonArray jsonArray = new JsonArray();
            for (Paket_pl_lokasi lokasi : lokasiList) {
                JsonObject jsonObjectArray = new JsonObject();
                jsonObjectArray.addProperty("pkl_id", lokasi.pkl_id);
                jsonObjectArray.addProperty("kbp_id", lokasi.kbp_id);
                jsonObjectArray.addProperty("pkt_lokasi", lokasi.pkt_lokasi);
                jsonArray.add(jsonObjectArray);

            }
            jsonObject.add("lokasi", jsonArray);

        }

        return jsonObject;

    }

    public static void metodeSubmit(Long id, Integer kategoriId, Integer mtdPemilihan, Integer mtdKualifikasi){
        checkAuthenticity();
        otorisasiDataPl(id); // check otorisasi data non tender
        Pl_seleksi obj = Pl_seleksi.findById(id);
        obj.mtd_pemilihan = mtdPemilihan;
        Kategori kategori = Kategori.findById(kategoriId);
        MetodeKualifikasi metodeKualifikasi = MetodeKualifikasi.fromValue(mtdKualifikasi);
        if(obj.isPenunjukanLangsung()){
            if(metodeKualifikasi.isPasca()) {
            	Jadwal_pl kontrak = Jadwal_pl.findByLelangNTahap(obj.lls_id, Tahap.TANDATANGAN_KONTRAK);
            	obj.lls_flow_new = null;
            	obj.mtd_kualifikasi = mtdKualifikasi;
            	if(kontrak == null){
	                Jadwal_pl.delete("lls_id=?", obj.lls_id);
	            }
            }
            else {
            	Jadwal_pl kontrak = Jadwal_pl.findByLelangNTahap(obj.lls_id, Tahap.PENGUMUMAN_PEMENANG_AKHIR);
            	obj.lls_flow_new = 1;
            	obj.mtd_kualifikasi = mtdKualifikasi;
            	if(kontrak == null){
        	        Jadwal_pl.delete("lls_id=?", obj.lls_id);
        	    }
            }
        }else{
            Jadwal_pl kontrak = Jadwal_pl.findByLelangNTahap(obj.lls_id, Tahap.TANDATANGAN_KONTRAK);
            obj.lls_flow_new = null;
            obj.mtd_kualifikasi = mtdKualifikasi;
            if(kontrak == null){
                Jadwal_pl.delete("lls_id=?", obj.lls_id);
            }
        }
        Paket_pl paket = obj.getPaket();
        paket.kgr_id = Kategori.findById(kategoriId);
        if ((paket.kgr_id != null && !paket.kgr_id.equals(kategori))) {
    		List<Integer> syarat_kualifikasi = new ArrayList<Integer>(JenisChecklist.CHECKLIST_KUALIFIKASI_BARU);
    		syarat_kualifikasi.addAll(JenisChecklist.CHECKLIST_SYARAT);
    		String jenis_checklist = StringUtils.join(syarat_kualifikasi, ',');
			ChecklistPl.delete("dll_id in (select dll_id from ekontrak.dok_nonlelang where lls_id=?)  and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis_checklist+"))", obj.lls_id);
        }
        paket.save();
        obj.save();
        edit(id);
    }
}
