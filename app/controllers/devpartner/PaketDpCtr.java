package controllers.devpartner;

import ams.models.ServiceResult;
import ams.utils.AmsUtil;
import controllers.lelang.LelangCtr;
import controllers.security.AllowAccess;
import models.agency.*;
import models.common.Active_user;
import models.devpartner.*;
import models.devpartner.common.JenisKontrakDp;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.lelang.Lelang_seleksi;
import models.osd.Certificate;
import models.osd.JenisCertificate;
import models.secman.Group;
import models.sirup.PaketSirup;
import models.sso.common.Kabupaten;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.mvc.Http.Cookie;
import play.mvc.Router;
import utils.LogUtil;
import utils.osd.OSDUtil;

import java.util.*;

public class PaketDpCtr extends DevPartnerCtr {

	public static final String TAG = "PaketDpCtr";
	private static List<String> STEPPERS = Arrays.asList(Messages.get("paket.data_paket"), Messages.get("paket.dokumen_persiapan"));

	@AllowAccess({Group.PANITIA,Group.PPK,Group.KUPPBJ})
	public static void index(Long panitiaId, Integer status) {
		if(status == null)
			renderArgs.put("status", Paket.StatusPaket.SEDANG_LELANG); // default status paket
		Active_user user = Active_user.current();
		renderArgs.put("isAllowCreate", user.isPpk());
		renderArgs.put("panitiaId", panitiaId);
		String templateUrl = "devpartner/paket/paket-list.html";
		if (user.isPpk()) {
			renderArgs.put("url", Router.reverse("DataTableCtr.paketPpkDevP").add("ppkId", user.ppkId).add("status", status).url);
		} else if(user.isKuppbj()){
			renderArgs.put("url", Router.reverse("DataTableCtr.paketKuppbjDevP").add("ukpbjId",user.ukpbjId).add("status",status).url);
			templateUrl = "devpartner/paket/paket-list-kuppbj.html";
		} else {
			renderArgs.put("url", Router.reverse("DataTableCtr.paketPanitiaDevP").add("panitiaId", panitiaId).add("status",status).url);
		}
		renderTemplate(templateUrl);
	}

	@AllowAccess({Group.PANITIA,Group.PPK/*, Group.REKANAN*/}) // dikomentarin terkait ISSUE #326
	public static void view(Long id) {
		PaketDp paket = PaketDp.findById(id);
		otorisasiDataPaketDp(paket); // check otorisasi data paket
		renderArgs.put("paket", paket);
		renderArgs.put("panitia", paket.getPanitia());
		renderArgs.put("lokasiList", Paket_lokasi.find("pkt_id=?", paket.pkt_id).fetch());
		renderArgs.put("anggaranList", Anggaran.findByPaket(id));
		renderTemplate("devpartnre/paket/paket-view.html");
	}
	
	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void edit(Long id) {
		PaketDp paket = PaketDp.findById(id);
		Active_user user = Active_user.current();
		otorisasiDataPaketDp(paket); // check otorisasi data paket
		Integer currentStepper = params.get("step", Integer.class);
		if(currentStepper == null)
			currentStepper = 1;
		renderArgs.put("draft", paket.pkt_status.isDraft());
		renderArgs.put("currentStep", currentStepper);
		renderArgs.put("steppers", STEPPERS);
		renderArgs.put("kategori", paket.kgr_id);
		if(currentStepper != null && currentStepper == 2) {
			renderArgs.put("paket", paket);
			renderArgs.put("isPpk", user.isPpk());
			renderArgs.put("isKuppbj", user.isKuppbj());
			renderArgs.put("isPanitia", user.isPanitia());
			renderArgs.put("allowToShowSaveButton", paket.isEditable());
			renderArgs.put("historyPokja", History_paket_pokja.findByPaket(id));
			renderTemplate("/devpartner/paket/paket-edit-part2.html");
		} else{
			//cookie
			Cookie cookie=request.cookies.get("paket_edit_propinsi_id");
			Long propinsiId=null;
			if(cookie!=null && !CommonUtil.isEmpty(cookie.value))
				propinsiId=Long.parseLong(cookie.value);
			paket.getPanitia();
			paket.withLocations();
			if (CommonUtil.isEmpty(paket.getPaketLocations())) {
				PaketLokasiDp pl = new PaketLokasiDp();
				if (propinsiId != null) {
					// default propinsi
					List<Kabupaten> listKab = Kabupaten.findByPropinsi(propinsiId);
					if (!listKab.isEmpty())
						pl.kbp_id = listKab.get(0).kbp_id;
				}
				paket.setPaketLocations(new ArrayList<>(1));
				paket.getPaketLocations().add(pl);
			}
			paket.getPaketAnggarans();
			if (CollectionUtils.isEmpty(paket.getPaketAnggarans())) {
				paket.paketAnggarans = new ArrayList<>(1);
				paket.paketAnggarans.add(new PaketAnggaranDp());
			}
			renderArgs.put("paket", paket);
			LelangDp lelang_seleksi = LelangDp.findByPaketNewestVersi(paket.pkt_id);
			Boolean isTenderUlang = lelang_seleksi!=null && lelang_seleksi.lls_versi_lelang>1;
			renderArgs.put("isPpk", user.isPpk());
			renderArgs.put("isTenderUlang", isTenderUlang);
			renderTemplate("/devpartner/paket/paket-edit-part1.html");
		}
	}


	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void simpan(Long id, PaketDp paket, PaketLokasiDp[] lokasi, Integer step) {
		checkAuthenticity();
		Active_user user = Active_user.current();
		PaketDp obj = PaketDp.findById(id);
		otorisasiDataPaketDp(obj); // check otorisasi data paket
		if (!obj.isEditable() || user.isKuppbj()) {
			redirectToStepTwo(id);
		}
		if(StringUtils.isEmpty(paket.pkt_nama))
			validation.addError("paket.pkt_nama", Messages.get("flash.nphd"));
		if (PaketAnggaranDp.find("pkt_id=? and ppk_id is null", id).fetch().size() > 0) {
			//ppk harus diisi untuk setiap paket anggaran
			validation.addError("paket.ppk", Messages.get("flash.phd"));
		}
		for (int i = 0; i < lokasi.length; i++) {
			if (!validation.required(lokasi[i].kbp_id).ok) {
				validation.addError("paket_lokasi_" + i + ".kbp_id", Messages.get("flash.lhd"), "var");
			}
			if (!validation.required(lokasi[i].pkt_lokasi).ok) {
				validation.addError("paket_lokasi_" + i + ".pkt_lokasi", Messages.get("flash.dlhd"), "var");
			}
		}

		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			flash.error(Messages.get("flash.dpgs_ckia"));
			validation.keep();
			LogUtil.debug(TAG, validation.errorsMap());
			edit(id);
		}
		obj.pkt_no_spk = paket.pkt_no_spk;
		if (obj.pkt_status.isDraft()) {
			obj.pkt_nama = paket.pkt_nama.replace("'", "").replace("\"", "");
		}
		PaketDp.simpanBuatPaket(obj, lokasi);
		redirectToStepTwo(id);
	}

	private static void redirectToStepTwo(Long id) {
		redirect(Router.reverse("devpartner.PaketDpCtr.edit").add("id", id).add("step", 2).url);
	}

	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void simpanPartTwo(Long id, Double hps) {
		checkAuthenticity();
		PaketDp obj = PaketDp.findById(id);
		Active_user user = Active_user.current();
		LogUtil.debug(TAG, obj);
		if (user.isKuppbj()) {
			if (obj.getPanitia() == null) {
				flash.error(Messages.get("flash.bmpp"));
				redirectToStepTwo(id);
			}
			flash.success(Messages.get("flash.dptt"));
			edit(id);
		}
		if (obj.ukpbj_id == null) {
			validation.addError("paket.ukpbj_id", Messages.get("flash.spu"), "var");
		}
		if (validation.hasErrors()) {
			flash.error(Messages.get("flash.dpgs_ckia"));
			params.flash(); // add http parameters to the flash scope
			validation.keep();
			redirectToStepTwo(id);
		} else {
			obj.pkt_hps = hps;
			obj.save();
			LelangDp lelang_seleksi = LelangDp.findByPaket(obj.pkt_id);
			lelang_seleksi.lls_kontrak_pembayaran = JenisKontrakDp.LUMP_SUM.id;
			lelang_seleksi.save();
			flash.success(Messages.get("flash.dptt"));
			edit(id);
		}
	}

	/**
	 * tampilan informasi rencana pengadaan
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void rencana(String instansiId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		int tahunNow = DateUtil.getTahunSekarang();
		if(CommonUtil.isEmpty(instansiId)) {
			//dapatkan id Instansi dari cookies
			Cookie cookieInstansi=request.cookies.get("sirup_instansi_id");	
			if(cookieInstansi!=null && !CommonUtil.isEmpty(cookieInstansi.value))
				instansiId=cookieInstansi.value;
		}
		if(satkerId == null) {
			Cookie cookieSatker=request.cookies.get("sirup_satker_id");	
			if(cookieSatker != null && !CommonUtil.isEmpty(cookieSatker.value))
				satkerId=Long.parseLong(cookieSatker.value);			
		}
		if(tahun == null) {
			Cookie cookieTahun = request.cookies.get("sirup_tahun");
			if (cookieTahun != null && !cookieTahun.value.equals("")){
				tahun = Integer.parseInt(cookieTahun.value);
			}else{
				tahun = tahunNow;
			}
		}
		if (metodePemilihan == null) {
			Cookie cookieMetodePemilihan = request.cookies.get("sirup_metode_pemilihan");
			if (cookieMetodePemilihan != null)
				metodePemilihan = Integer.parseInt(cookieMetodePemilihan.value);
		}
		renderArgs.put("tahun", tahun);
		renderArgs.put("instansiId", instansiId);
		renderArgs.put("satkerId", satkerId);
		renderArgs.put("metodePemilihan", metodePemilihan);
		renderTemplate("devpartner/paket/rencana-paket.html");
	}

	/**
	 * tampilan popup informasi detail rencana paket tertentu berdasarkan kode rup
	 * @param id
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void rencanaView(Long id) {
		PaketSirup paket = PaketSirup.findById(id);
		renderArgs.put("paket", paket);
		if(!CollectionUtils.isEmpty(paket.paket_anggaran_json)) {
			renderArgs.put("anggaranList", paket.paket_anggaran_json);
		}
		renderArgs.put("satker", Satuan_kerja.find("rup_stk_id = ?", paket.rup_stk_id).first());
		List<Panitia> panitiaList = Panitia.findByPegawai(Active_user.current().pegawaiId);
		Map<Long,Boolean> mapBuatPaket = new HashMap<>();

		renderArgs.put("panitiaList", panitiaList);
		renderArgs.put("mapBuatPaket", mapBuatPaket);
		renderArgs.put("isPanitia",Active_user.current().isPanitia());
		renderTemplate("devpartner/paket/rencana-view.html");
	}
	
	/**
	 * Pembuatan paket dari rencana Pengadaan (RUP)
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void buatPaket(Long id, Long panitiaId) {
		checkAuthenticity();
		PaketDp paket = PaketDp.buatPaketFromSirup(id, panitiaId);
		edit(paket.pkt_id);
	}
	

	//========================= BEGIN IMPLEMENT EDIT RUP UNTUK LELANG ULANG =========================

	/**
	 * tampilan informasi rencana pengadaan
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void editRencana(Long paketId, String instansiId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		int tahunNow = DateUtil.getTahunSekarang();
		renderArgs.put("paketId", paketId);
		if(CommonUtil.isEmpty(instansiId)) {
			//dapatkan id Instansi dari cookies
			Cookie cookieInstansi=request.cookies.get("sirup_instansi_id");
			if(cookieInstansi!=null && !CommonUtil.isEmpty(cookieInstansi.value))
				instansiId=cookieInstansi.value;
		}
		if(satkerId == null) {
			Cookie cookieSatker=request.cookies.get("sirup_satker_id");
			if(cookieSatker != null && !CommonUtil.isEmpty(cookieSatker.value))
				satkerId=Long.parseLong(cookieSatker.value);
		}
		if(tahun == null) {
			Cookie cookieTahun = request.cookies.get("sirup_tahun");
			if (cookieTahun != null && !cookieTahun.value.equals("")){
				tahun = Integer.parseInt(cookieTahun.value);
			}else{
				tahun = tahunNow;
			}
		}
		if (metodePemilihan == null) {
			Cookie cookieMetodePemilihan = request.cookies.get("sirup_metode_pemilihan");
			if (cookieMetodePemilihan != null)
				metodePemilihan = Integer.parseInt(cookieMetodePemilihan.value);
		}
		renderArgs.put("tahun", tahun);
		renderArgs.put("instansiId", instansiId);
		renderArgs.put("satkerId", satkerId);
		renderArgs.put("metodePemilihan", metodePemilihan);
		renderTemplate("lelang/paket/edit-rup.html");
	}

	/**
	 * tampilan popup informasi detail rencana paket tertentu berdasarkan kode rup
	 * @param id
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void editRencanaView(Long id, Long paketId) {
		PaketSirup paket = PaketSirup.findById(id);
		renderArgs.put("paketId", paketId);
		renderArgs.put("paket", paket);
		if(!CollectionUtils.isEmpty(paket.paket_anggaran_json)) {
			renderArgs.put("anggaranList", paket.paket_anggaran_json);
		}
		renderArgs.put("satker", Satuan_kerja.find("rup_stk_id = ?", paket.rup_stk_id).first());
		List<Panitia> panitiaList = Panitia.findByPegawai(Active_user.current().pegawaiId);
		Map<Long,Boolean> mapBuatPaket = new HashMap<>();

		renderArgs.put("panitiaList", panitiaList);
		renderArgs.put("mapBuatPaket", mapBuatPaket);
		renderArgs.put("isPanitia",Active_user.current().isPanitia());
		renderTemplate("lelang/paket/edit-rencana-view.html");
	}

	// TODO: hapus ini
	/**
	 * Pembuatan paket dari rencana Pengadaan (RUP)
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void editRencanaBuatPaket(Long id, Long paketId, Long panitiaId) {
		checkAuthenticity();
		LogUtil.debug(TAG,"Update RUP");
		Paket paket = Paket.updatePaketRup(id, paketId);
		edit(paket.pkt_id);
	}

	//========================= END   IMPLEMENT EDIT RUP UNTUK LELANG ULANG =========================

	/**
	 * Tampilan detil paket pada beranda PPK dan Auditor
	 * @param id
	 */
    @AllowAccess({Group.AUDITOR,Group.PPK})
    public static void detil_paket(Long id){
    	otorisasiDataLelang(id); // check otorisasi data lelang
		renderArgs.put("lelang",Lelang_seleksi.findById(id));
        renderTemplate("admin/auditor/detilPaket.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void ppk(Long id, Long ppk_id, Long ang_id, Long llsId, String insId){
        Paket paket = Paket.findById(id);
        otorisasiDataPaket(paket); // check otorisasi data paket
		// List Instansi untuk filter ppk
		renderArgs.put("paket", paket);
		renderArgs.put("ang_id", ang_id);
		renderArgs.put("insId", insId);
		renderArgs.put("llsId", llsId);
		renderArgs.put("oldPpkId", ppk_id);
		if(paket.pkt_status.isSedangLelang()) {
			Lelang_seleksi lelang = Lelang_seleksi.findAktifByPaket(paket.pkt_id);
			renderArgs.put("lelang", lelang);
		}
        renderTemplate("lelang/paket/paket-ppk.html");
    }

	@AllowAccess({Group.PANITIA})
	public static void alasanGantiPpk(Long id, Long oldPpkId, Long llsId, String insId, Long ppkId, Long ang_id){
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket); // check otorisasi data paket
		Ppk oldPpk = Ppk.findById(oldPpkId);
		Ppk newPpk = Ppk.findById(ppkId);
		renderArgs.put("paket", paket);
		renderArgs.put("ppkId",ppkId);
		renderArgs.put("oldPpk",oldPpk);
		renderArgs.put("newPpk",newPpk);
		renderArgs.put("ang_id", ang_id);
		renderArgs.put("insId", insId);
		renderArgs.put("llsId", llsId);
		renderTemplate("lelang/paket/paket-ganti-ppk.html");
	}

	@AllowAccess({Group.KUPPBJ})
	public static void panitia(Long id){
		PaketDp paket = PaketDp.findById(id);
		otorisasiDataPaketDp(paket); // check otorisasi data paket
		LelangDp lelang = LelangDp.findByPaket(id);
		renderArgs.put("paket", paket);
		boolean sedangLelang = paket.pkt_status.isSedangLelang();
		renderArgs.put("sedangLelang", sedangLelang);
		renderArgs.put("oldPanitia", paket.getPanitia());
		renderArgs.put("llsId", lelang.lls_id);
		renderTemplate("devpartner/paket/paket-panitia.html");
	}

	@AllowAccess({Group.KUPPBJ})
	public static void submit_panitia(Long id, Long pntId, Long old_pnt_id, String alasan){
		checkAuthenticity();
    	PaketDp paket = PaketDp.findById(id);
    	otorisasiDataPaketDp(paket);

        LelangDp lelang = LelangDp.findByPaket(id);

		if(old_pnt_id != null){
			if(alasan == null){
				//validasi
                flash.error(Messages.get("flash.abmamp"));
                params.flash(); // add http parameters to the flash scope
                validation.keep();
                panitia(id);
			}

			//Riwayat
			HistoryPaketPokjaDp history = new HistoryPaketPokjaDp();
			history.pkt_id = paket.pkt_id;
			history.pnt_id = old_pnt_id;
			history.alasan = alasan;
			history.peg_id = Active_user.current().pegawaiId;
			history.tgl_perubahan = newDate();
			history.save();

			// Ganti Persetujuan
			if(!lelang.lls_status.isDraft()){
				if(!PersetujuanDp.isApprove(lelang.lls_id, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG)){
					//Jika sedang persetujuan pemenang, reset persetujuannya
					PersetujuanDp.deleteAllByLelangAndJenisPemenang(lelang.lls_id);
					//Jika sudah ada pemenang, reset pemenangnya
					EvaluasiDp penetapan = EvaluasiDp.findAkhir(lelang.lls_id);
					if(penetapan != null) {
						Query.update("UPDATE devpartner.nilai_evaluasi_dp SET nev_lulus=? WHERE eva_id=? AND nev_lulus=?"
								, NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS, penetapan.eva_id, NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS);
					}
				}
				PersetujuanDp persetujuan = PersetujuanDp.findByPegawaiLelangAndJenis(lelang.lls_id, PersetujuanDp.JenisPersetujuan.BATAL_LELANG);
				if(persetujuan == null){
					persetujuan = PersetujuanDp.findByPegawaiLelangAndJenis(lelang.lls_id, PersetujuanDp.JenisPersetujuan.ULANG_LELANG);
				}

				if (persetujuan != null && !persetujuan.isApprove()) {
					if(persetujuan.isSedangPersetujuan()){
						persetujuan.deleteAllByLelangAndJenis();
					}
				}
			}else{
				PersetujuanDp.deleteAllByLelangAndJenisPengumuman(lelang.lls_id);
			}
		}
		PaketPanitiaDp paket_panitia = PaketPanitiaDp.getByPktId(paket.pkt_id);
		if (paket_panitia == null) {
			paket_panitia = new PaketPanitiaDp();
			paket_panitia.pkt_id = paket.pkt_id;
		}
		paket_panitia.pnt_id = pntId;
		paket_panitia.save();
		paket.pnt_id = pntId;
		paket.save();
		redirectToStepTwo(id);
	}

	private static ServiceResult checkCertificatePanitia(Long pntId) {
		LogUtil.debug(TAG, "check certificate for each committee");
		List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?", pntId).fetch();
		StringBuilder sb = new StringBuilder();
		boolean canCreate = true;
		for (Anggota_panitia anggota:anggota_panitia) {
			boolean validCert = false;
			String info = Messages.get("flash.tmsa");
			Pegawai pegawai = Pegawai.findById(anggota.peg_id);
			if (pegawai.cer_id != null) {
				Certificate certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id, JenisCertificate.CERT_PANITIA);

				if (certificate!=null && certificate.cer_sn!=null) {
					if (certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
						if (!certificate.isCertExpired() && AmsUtil.checkOCSPCertificate(certificate.getX509()).isValid()) {
							info = certificate.getRangeCertificateValidDate();
							validCert = true;
						}
					}
				}
				if (certificate==null) {
					info = Messages.get("flash.tdmds");
				}
			}
			canCreate = canCreate && validCert;
			sb.append("<p>").append(pegawai.peg_nama + " <strong>(" + info + ")</strong>").append("</p>");
		}
		return new ServiceResult<>(canCreate, sb.toString());
	}

	@AllowAccess({Group.KUPPBJ})
	public static void alasanGantiPokja(Long id, Long oldPanitiaId, Long llsId, Long panitiaId){
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket); // check otorisasi data paket

		Panitia oldPanitia = Panitia.findById(oldPanitiaId);
		Panitia newPanitia = Panitia.findById(panitiaId);

		renderArgs.put("paket", paket);
		renderArgs.put("panitiaId",panitiaId);
		renderArgs.put("oldPanitia",oldPanitia);
		renderArgs.put("newPanitia",newPanitia);
		renderArgs.put("llsId", llsId);

		renderTemplate("lelang/paket/paket-ganti-pokja.html");
	}

	@AllowAccess({Group.PPK})
	public static void hapus(Long id){
    	PaketDp paket = PaketDp.findById(id);
    	otorisasiDataPaketDp(paket);

    	if(paket.ukpbj_id != null){
    		flash.error(Messages.get("flash.mptbhkk"));
    		index(null,null);
		}

		try {
			paket.delete();
		} catch (Exception ex) {
			Logger.info(ex,"Error delete " + ex.getLocalizedMessage());
			flash.error(Messages.get("flash.gmp") + paket.pkt_id);
			index(null, null);
		}

		index(null, null);
	}

	@AllowAccess({Group.PPK})
	public static void hapus_rup(Long id, Long rupId) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket); // check otorisasi data paket
		PaketSirup rup = PaketSirup.findById(rupId);
		Anggaran anggaran = null;
		paket.pkt_nama = paket.pkt_nama.replace(',' + rup.nama, "");
		Paket_satker.delete("pkt_id=? AND rup_id=?", id, rupId);
		List<Paket_anggaran> list = Paket_anggaran.find("pkt_id=? AND rup_id=?", id, rupId).fetch();
		if(!CollectionUtils.isEmpty(list)) {
			for(Paket_anggaran obj : list){
				obj.delete();
				anggaran = Anggaran.findByAnggaranId(obj.ang_id);
				if(anggaran != null) {
					paket.pkt_pagu = paket.pkt_pagu - anggaran.ang_nilai;
					anggaran.delete();
				}
			}
		}
		if(!CollectionUtils.isEmpty(rup.paket_lokasi_json)){
			for(PaketSirup.PaketSirupLokasi lokasi:rup.paket_lokasi_json) {
				if(lokasi.id_kabupaten != null) {
					Paket_lokasi paket_lokasi = Paket_lokasi.find("pkt_id=? AND kbp_id=?", paket.pkt_id, lokasi.id_kabupaten).first();
					if (paket_lokasi != null)
						paket_lokasi.delete();
				}
			}
		}
		paket.save();
		edit(id);
	}

    @AllowAccess({Group.PANITIA})
    public static void submit_ppk(Long id, Long llsId, Long ang_id, Long ppkId, Long old_ppk_id, String alasan) {
    	checkAuthenticity();
        Paket paket = Paket.findById(id);
        otorisasiDataPaket(paket); // check otorisasi data paket
        Ppk ppk = Ppk.findById(ppkId);
        if(ang_id != null){
			Paket_anggaran paket_anggaran = Paket_anggaran.find("pkt_id=? and ang_id=?", paket.pkt_id, ang_id).first();
			if (paket_anggaran != null && paket_anggaran.ppk_id != null) {
				old_ppk_id = paket_anggaran.ppk_id;
			}
			paket_anggaran.ppk_id = ppk.ppk_id;
			paket_anggaran.save();
		}else if (paket.isFlag43()){
        	//Karena di 4.3 semua Anggaran otomatis dipegang oleh PPK yang bertanggung jawab atas paketnya
			Paket_anggaran.updatePpkAllAnggaranByPaket(paket.pkt_id, ppkId);
		}

		History_paket_ppk history = new History_paket_ppk();
		history.pkt_id = paket.pkt_id;
		history.ppk_id = old_ppk_id;
		history.alasan = alasan;
		history.peg_id = Active_user.current().pegawaiId;
		history.tgl_perubahan = newDate();
		history.save();

		if(paket.isFlag43()){
			PaketPpk paketPpk = PaketPpk.findByPaketAndPpk(paket.pkt_id,old_ppk_id);
	    	paketPpk.ppk_id = ppkId;
			paketPpk.save();
			//hapus data paket ppk yang lama karena history ada di History_paket_ppk
			paketPpk.deleteOldPpkNew(old_ppk_id);
		}

		if(llsId != null){
			//update history lelang untuk perubahan ppk di fitur ubah lelang
			LelangCtr.updateHistoryLelangForPpk(llsId, old_ppk_id);
		} else {
			Lelang_seleksi lelang = Lelang_seleksi.findByPaket(paket.pkt_id);
			LelangCtr.view(lelang.lls_id);
		}
    }
    
    @AllowAccess({Group.PANITIA})
    public static void anggaran(Long id){
		renderArgs.put("anggaran",Anggaran.findById(id));
    	renderTemplate("lelang/paket/paketAnggaran.html");
    }
    
    @AllowAccess({Group.PANITIA})
    public static void simpan_anggaran(Long id, String kode_anggaran, Double nilai){
    	Anggaran anggaran = Anggaran.findById(id);
    	if(kode_anggaran != null)
    		anggaran.ang_koderekening = kode_anggaran;
    	if(nilai != null)
    		anggaran.ang_nilai = nilai;
    	anggaran.save();
    	boolean result = true;
    	renderJSON(result);
    }

	@AllowAccess({Group.PPK})
	public static void pilihUkpbj(Long id){
		PaketDp paket = PaketDp.findById(id);
		otorisasiDataPaketDp(paket);
		renderArgs.put("paket", paket);
		if(paket.pkt_status.isSelesaiLelang()){
			LelangDp lelang = LelangDp.findAktifByPaket(paket.pkt_id);
			renderArgs.put("lelang", lelang);
		}
		renderArgs.put("backUrl", Router.reverse("devpartner.PaketDpCtr.edit").add("id", paket.pkt_id).add("step", 2));
		renderTemplate("devpartner/paket/paket-ukpbj.html");
	}

	@AllowAccess({Group.PPK})
	public static void submit_ukpbj(Long id , Long ukpbj_id){
		checkAuthenticity();
		PaketDp paket = PaketDp.findById(id);
		otorisasiDataPaketDp(paket);
		paket.ukpbj_id = ukpbj_id;
		paket.save();

		redirectToStepTwo(id);
	}

	@AllowAccess({Group.PPK})
	public static void batalPilihUkpbj(Long id){
		PaketDp paket = PaketDp.findById(id);
		paket.ukpbj_id = null;
		paket.save();

		redirectToStepTwo(id);
	}

	@AllowAccess({Group.KUPPBJ})
	public static void ubahDaftarPaket(Long id) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		renderArgs.put("paket", paket);
		renderArgs.put("konsolidasiList", Paket.findByKonsolidasiId(paket.pkt_id));
		renderTemplate("lelang/paket/paket-konsolidasi.html");
	}

	@AllowAccess({Group.KUPPBJ})
	public static void ubahDaftarPaketSubmit(Long id, List<String> paketIds) throws Exception {
		checkAuthenticity();
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		if(!CollectionUtils.isEmpty(paketIds)) {
			List<Paket> paketList = Paket.findByIds(String.join(",", paketIds));
			Paket.buatPaketKonsolidasi(paketList, paket.pkt_id);
		}
		if(Paket.count("pkt_id_konsolidasi = ?", id) < 2 ) {
			flash.error(Messages.get("flash.khldp"));
		}
		edit(paket.pkt_id);
	}

	@AllowAccess({Group.PANITIA, Group.PPK})
	public static void hapus_lokasi(Long id, Long lokasiId){
		PaketDp paket = PaketDp.findById(id);
		notFoundIfNull(paket);
		otorisasiDataPaketDp(paket); // check otorisasi data paket
		PaketLokasiDp paket_lokasi = PaketLokasiDp.findById(lokasiId);
		if(paket_lokasi != null)
			paket_lokasi.delete();
	}
}
		
		 
	
		



