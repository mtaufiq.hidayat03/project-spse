package controllers.devpartner;

import controllers.security.AllowAccess;
import models.common.Active_user;
import models.devpartner.DokPenawaranDp;
import models.devpartner.LelangDp;
import models.devpartner.common.TahapDpStarted;
import models.secman.Group;

/**
 * definisi semua action terkait peserta lelang dan penawarannya
 * @author arief ardiyansah
 *
 */
public class PesertaDpCtr extends DevPartnerCtr {
	
	/**
     * Fungsi {@code PesertaCtr.index} digunakan untuk menampilkan detail
     * para peserta lelang beserta hasil evaluasinya dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
	public static void index(Long id) {
		LelangDp lelang = LelangDp.findById(id);
		notFoundIfNull(lelang);
        TahapDpStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("hide", !tahapStarted.isShowPenyedia());
        renderTemplate("devpartner/peserta/index.html");
	}
	
	/**
     * Fungsi {@code PesertaCtr.index} digunakan untuk menampilkan detail
     * para peserta lelang beserta penawarannya
     * @param id parameter id lelang yang dilihat
     */
	@AllowAccess({ Group.PANITIA, Group.PPK})
	public static void penawaran(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		Active_user active_user = Active_user.current();
		// peserta yang tidak menawar tidak bisa akses data penawaran peserta
		renderArgs.put("not_allowed", active_user.isRekanan() && !DokPenawaranDp.isAnyPenawaran(id, active_user.rekananId));
		renderArgs.put("group", active_user.group);
		LelangDp lelang = LelangDp.findById(id);
        TahapDpStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang",lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("hide", !tahapStarted.isShowPenyedia());
		renderTemplate("devpartner/peserta/penawaran.html");
	}
}
