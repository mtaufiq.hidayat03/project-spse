package controllers.devpartner;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.common.JadwalFlash;
import models.devpartner.HistoryJadwalDp;
import models.devpartner.JadwalDp;
import models.devpartner.JadwalDraftDp;
import models.devpartner.LelangDp;
import models.devpartner.pojo.TahapanLelangDp;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DoubleConverter;
import models.secman.Group;
import play.Logger;
import play.i18n.Messages;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class JadwalDpCtr extends DevPartnerCtr {

	@AllowAccess({ Group.PANITIA, Group.ADM_PPE })
	public static void list(Long id, Long lelangCopyId) {
		LelangDp lelang = LelangDp.findById(id);
		if(Active_user.current().isPanitia()) {
			otorisasiDataLelangDp(id); // check otorisasi data lelang
			boolean allowEditJadwal = lelang.lls_status.isDraft();
			if(!allowEditJadwal)
				forbidden(Messages.get("not-authorized"));
		}
		renderArgs.put("lelang", lelang);
		renderArgs.put("lelangCopyId", lelangCopyId);
		List<JadwalDp> jadwalList = JadwalDp.findByLelang(lelang.lls_id);
		if (flash.get("jadwalFlash") != null) {
			List<JadwalFlash> jadwalFlash = new GsonBuilder()
					.registerTypeAdapter(Date.class, new JadwalDp.DateConverter())
					.registerTypeAdapter(Double.class, new DoubleConverter()).create()
					.fromJson(flash.get("jadwalFlash"), new TypeToken<List<JadwalFlash>>(){}.getType());
			jadwalList = JadwalDp.getJadwalList(jadwalFlash, jadwalList);
		}
		if (CommonUtil.isEmpty(jadwalList)) {
			jadwalList = JadwalDp.getJadwalkosong(lelang.lls_id, lelang.getMetode());
		}

		String template = "devpartner/jadwal/jadwal-edit.html";
		renderArgs.put("jadwalList", jadwalList);
		renderTemplate(template);
	}

	@AllowAccess({ Group.PANITIA})
	public static void editAktif(Long id) {
		LelangDp lelang = LelangDp.findById(id);
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		boolean allowEditJadwal = lelang.lls_status.isAktif();
		if(!allowEditJadwal)
			forbidden(Messages.get("not-authorized"));
		renderArgs.put("lelang", lelang);
		List<JadwalDp> jadwalList = JadwalDp.findByLelang(lelang.lls_id);
		Date now = newDate();
		if (flash.get("jadwalFlash") != null) {
			List<JadwalFlash> jadwalFlash = new GsonBuilder()
					.registerTypeAdapter(Date.class, new JadwalDp.DateConverter())
					.registerTypeAdapter(Double.class, new DoubleConverter()).create()
					.fromJson(flash.get("jadwalFlash"), new TypeToken<List<JadwalFlash>>(){}.getType());
			jadwalList = JadwalDp.getJadwalList(jadwalFlash, jadwalList, lelang.isAllowReschedule(), now);
		}

		String template = "devpartner/jadwal/jadwal-edit-aktif.html";
		renderArgs.put("jadwalList", jadwalList);
		renderArgs.put("now", now);
		renderArgs.put("allowReschedule", lelang.isAllowReschedule());
		renderArgs.put("waitingConfirmation", JadwalDraftDp.isWaitingConfirmation(lelang.lls_id));
		renderTemplate(template);
	}
	
	@AllowAccess({Group.PANITIA})
	public static void copy(Long id, Long lelangCopyId) {  
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		JadwalDp.copyJadwal(id,lelangCopyId);
		list(id, null);
	}
	
	@AllowAccess({Group.PANITIA, Group.ADM_PPE})
    public static void simpan(Long id, List<JadwalDp> jadwalList, String keterangan) {
		checkAuthenticity();
		if (Active_user.current().isPanitia())
			otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		boolean harusBerubah = params._contains("harus_berubah");
		if (harusBerubah) {
			validation.min(keterangan.length(), 31).key("keterangan");
		}
		Collections.sort(jadwalList, Comparator.comparingInt(jadwal -> jadwal.akt_urut));
		JadwalDp.validate(lelang, jadwalList, newDate(), keterangan, 0, false);
		boolean valid = true;
		if (validation.hasErrors()) {
			validation.keep();
			valid = false;
		} else {
			try {
				JadwalDp.simpanJadwal(lelang, jadwalList, newDate(), keterangan);
				flash.success(Messages.get("flash.djttt"));
			} catch (Exception e) {
				valid = false;
				Logger.error(e, "Simpan Jadwal gagal");
				flash.error(Messages.get("flash.djtgt"));
			}
		}
		if (!valid) {
			flash.put("jadwalFlash", CommonUtil.toJson(JadwalDp.getJadwalFlash(jadwalList)));

		}
		if (Active_user.current().isPanitia()){
			list(id,null);
		} else{
    	   ubahJadwalLelang(id);
       }
    }

	@AllowAccess({Group.PANITIA, Group.ADM_PPE})
	public static void simpanAktif(Long id, Integer allowReschedule, List<JadwalDp> jadwalList, String keterangan) {
		checkAuthenticity();
		if (Active_user.current().isPanitia())
			otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		if (JadwalDraftDp.isWaitingConfirmation(lelang.lls_id)) {
			forbidden(Messages.get("not-authorized"));
		}
		if (allowReschedule != null) {
			lelang.allow_reschedule = allowReschedule == 1 ? 1 : 0;
			lelang.save();
			flashSuccess(lelang.isAllowReschedule() ? "lelang_jadwal.allow_reschedule_opened" : "lelang_jadwal.allow_reschedule_cancelled");
			editAktif(id);
		}

		boolean harusBerubah = params._contains("harus_berubah");
		if (harusBerubah) {
			validation.min(keterangan.length(), 31).key("keterangan");
		}
		Collections.sort(jadwalList, Comparator.comparingInt(jadwal -> jadwal.akt_urut));
		JadwalDp.validate(lelang, jadwalList, newDate(), keterangan, 0, false);
		boolean valid = true;
		if (validation.hasErrors()) {
			validation.keep();
			valid = false;
		} else {
			try {
				if (lelang.isAllowReschedule()) {
					JadwalDp.simpanJadwalDraft(lelang, jadwalList, keterangan);
					lelang.allow_reschedule = 0;
					lelang.save();
				} else {
					JadwalDp.simpanJadwal(lelang, jadwalList, newDate(), keterangan);
				}
				flashSuccess("lelang_jadwal.reschedule_submitted");
			} catch (Exception e) {
				valid = false;
				Logger.error(e, "Simpan Jadwal gagal");
				flash.error(Messages.get("flash.djtgt"));
			}
		}
		if (!valid) {
			flash.put("jadwalFlash", CommonUtil.toJson(JadwalDp.getJadwalFlash(jadwalList)));

		}
		if (Active_user.current().isPanitia()){
			editAktif(id);
		} else{
			ubahJadwalLelang(id);
		}
	}

	/**
	 * Admin Lpse mengubah jadwal
	 * Fungsi {@code ubahJadwalLelang} digunakan untuk menampilkan halaman.
	 * @param lelangId
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void ubahJadwalLelang(Long lelangId) {
		if(lelangId != null)
		{
			LelangDp lelang = LelangDp.findById(lelangId);
			if(lelang != null && JadwalDraftDp.isWaitingConfirmation(lelang.lls_id)){
				List<JadwalDp> jadwalList = JadwalDp.findByLelang(lelangId);

				renderArgs.put("lelang", lelang);
				renderArgs.put("jadwalList", jadwalList);
			}
			else if(lelangId!=0){
				flash.error(Messages.get("flash.tyacbt"));
			}
		}
		renderTemplate("devpartner/jadwal/jadwal-edit-ppe.html");
	}

	/**
	 * Admin Lpse mengubah jadwal
	 * Fungsi {@code ubahJadwalLelang} digunakan untuk menampilkan halaman.
	 * @param lelangId
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void approveUbahJadwal(Long lelangId, Integer reject) {
		if(lelangId != null) {
			LelangDp lelang = LelangDp.findById(lelangId);
			if(lelang != null && JadwalDraftDp.isWaitingConfirmation(lelang.lls_id)){
				try {
					if (reject != null && reject == -1) {
						JadwalDraftDp.deleteByLelang(lelang.lls_id);
						flashSuccess("lelang_jadwal.reschedule_ditolak");
					} else {
						JadwalDp.approveJadwalDraft(lelang, newDate());
						flashSuccess("lelang_jadwal.reschedule_diterima");
					}
				} catch (Exception e) {
					Logger.error(e, "Simpan Jadwal gagal");
					flash.error(Messages.get("flash.djtgt"));
				}
			} else if(lelangId != 0){
				flash.error(Messages.get("flash.tyacbt"));
			}
		}
		renderTemplate("devpartner/jadwal/jadwal-edit-ppe.html");
	}
    
    /**
     * Fungsi {@code tahap} digunakan untuk menampilkan detail tahapan lelang
     * dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
    public static void tahap(Long id) {
    	notFoundIfNull(id);
		renderArgs.put("lelang", LelangDp.findById(id));
		renderArgs.put("date", newDate()); // tanggal sekarang
		renderArgs.put("list", TahapanLelangDp.findByLelang(id));
        renderTemplate("/devpartner/jadwal/tahap.html");
	}
    
    public static void jadwalLelang(Long id) {
		renderArgs.put("lls", LelangDp.findById(id));
		renderArgs.put("date", newDate()); // tanggal sekarang
		renderArgs.put("list",TahapanLelangDp.findByLelang(id));
        renderTemplate("/devpartner/jadwal/jadwal-lelang.html");
	}
    
    /**
     * Fungsi {@code tahap} digunakan untuk menampilkan detail history tahapan lelang
     * dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
    public static void history(Long id) {
		renderArgs.put("historyList", HistoryJadwalDp.find("dtj_id=? order by hjd_tanggal_edit desc", id).fetch());
    	renderTemplate("devpartner/jadwal/history.html");
    }   
    
   
}
