package controllers.devpartner;

import controllers.security.AllowAccess;
import models.common.Active_user;
import models.devpartner.AnggaranDp;
import models.devpartner.PaketLokasiDp;
import models.devpartner.PesertaDp;
import models.devpartner.common.TahapDpNow;
import models.devpartner.panel.PanelUndanganLelang;
import models.devpartner.pojo.LelangDpDetil;
import models.secman.Group;

/**
 * aktivitas pendaftaran lelang
 */

public class PendaftaranLelangDpCtr extends DevPartnerCtr {

	public static final String TAG = "PendaftaranLelangCtr";

	/*
	* @param id
 	* @param setuju
 	* @throws IOException
	 */
	@AllowAccess({Group.REKANAN})
	public static void index(Long id) {
		LelangDpDetil lelang = LelangDpDetil.findById(id);
		renderArgs.put("lelang", lelang);
		TahapDpNow tahapNow = lelang.getTahapNow();
		boolean allowed = tahapNow.isPengumumanLelang();
		if (allowed && lelang.getMetode().isCallDown()) {
			allowed = PanelUndanganLelang.isInvited(lelang.lls_id, Active_user.current().rekananId);
		}
		renderArgs.put("anggaranList", AnggaranDp.findByPaket(lelang.pkt_id));
		renderArgs.put("lokasiList", PaketLokasiDp.find("pkt_id=?", lelang.pkt_id).fetch());
		renderArgs.put("allowed", allowed);
		renderTemplate("devpartner/lelang/pendaftaran-peserta.html");
	}

	public static void submit(Long id, boolean setuju) {
		checkAuthenticity();
		Long rekananId = Active_user.current().rekananId;
		PesertaDp pesertaCheck = PesertaDp.findByRekananAndLelang(rekananId, id); // check pserta terdaftar agar tidak doule
		if (setuju && pesertaCheck == null) {			
			PesertaDp.daftarLelang(id, rekananId);
		}
		BerandaDpCtr.index();
	}

	public static void herp() {
		for (long lelangId = 9640999; lelangId <= 9640999; lelangId += 1000 ) {
			for (long rekananId = 11999; rekananId <= 15999; rekananId += 1000) {
				PesertaDp pesertaCheck = PesertaDp.findByRekananAndLelang(rekananId, lelangId); // check pserta terdaftar agar tidak doule
				if (pesertaCheck == null) {
					PesertaDp.daftarLelang(lelangId, rekananId);
				}
			}
		}
		BerandaDpCtr.index();
	}
}
