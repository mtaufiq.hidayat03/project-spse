package controllers.devpartner;

import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.PdfType;
import models.devpartner.BeritaAcaraDp;
import models.devpartner.LelangDp;
import models.secman.Group;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * controller untuk aktifitas-aktifitas terkait berita acara 
 * aktifitas tersebut diantaranya Upload dan download BA
 * @author lkpp
 *
 */
public class BeritaAcaraDpCtr extends DevPartnerCtr {

	@AllowAccess({Group.PANITIA})
	public static void formBa(Long id, BeritaAcaraDp.JenisBA jenis) {
		BeritaAcaraDp berita_acara = BeritaAcaraDp.findBy(id, jenis);
		renderArgs.put("id",id);
		renderArgs.put("berita_acara",berita_acara);
		renderArgs.put("jenis",jenis);
		renderArgs.put("today",newDate());
		String labelDokumen = "";
		renderArgs.put("labelDokumen",labelDokumen);
		Map<String, Object> params=new HashMap();
		params.put("id", id);
		String ref = Router.getFullUrl("devpartner.LelangDpCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}
		renderArgs.put("referer",ref);
		renderTemplate("devpartner/berita_acara/upload-ba.html");
	}

    @AllowAccess({Group.PANITIA})
	public static void uploadBaSubmit(Long id, @Valid @PdfType File file, String no,
									  @As(binder= DatetimeBinder.class) Date tanggal,
									  String tempat, String info, String ref, BeritaAcaraDp.JenisBA jenis) {
		LelangDp lelang = LelangDp.findById(id);
		otorisasiDataLelangDp(lelang); // check otorisasi data lelang
        String namaDokumen = Messages.get("flash.ba");
		if (validation.hasErrors()) {
			flash.error(Messages.get("flash.udyduhd"), namaDokumen);
		} else {
			BeritaAcaraDp berita_acara = BeritaAcaraDp.findNCreateBy(lelang.lls_id, jenis);
			try {
				berita_acara.brt_no = no;
				berita_acara.brt_tgl_evaluasi = tanggal;
				berita_acara.brt_info = info;
				berita_acara.simpan(file);
				flash.success(Messages.get("flash.updh"), namaDokumen);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Berita Acara");
			}
		}
		redirect(ref);
	}    
}
