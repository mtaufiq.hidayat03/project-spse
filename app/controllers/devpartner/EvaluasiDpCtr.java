package controllers.devpartner;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import controllers.security.AllowAccess;
import ext.IndonesianDecimalBinder;
import models.agency.Anggota_panitia;
import models.common.Active_user;
import models.common.JadwalFlash;
import models.devpartner.*;
import models.devpartner.common.*;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DoubleConverter;
import models.secman.Group;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.mvc.Router;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 *
 * @author idoej
 */
public class EvaluasiDpCtr extends DevPartnerCtr {

	public static final String TAG = "EvaluasiDpCtr";

	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void index(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		PaketDp paket = PaketDp.findByLelang(id);
		boolean isPanitia = Active_user.current().isPanitia();
		renderArgs.put("isPanitia", isPanitia);
        TahapDpStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("paket", paket);
		renderArgs.put("tahapStarted", tahapStarted);
		Active_user active_user = Active_user.current();
		boolean isKetuaPanitia = Anggota_panitia.isKetuaPanitia(lelang.getPanitia().pnt_id, active_user.pegawaiId);
		renderArgs.put("isKetuaPanitia", isKetuaPanitia);
		TahapDpNow tahapAktif = lelang.getTahapNow();
		renderArgs.put("tahapNow", tahapAktif);
		if(lelang.isPreQualification()) {
			EvaluasiDp shortlist = EvaluasiDp.findNCreateShortlist(	id);
            boolean allow_penetapan_pra = tahapAktif.isPenetapanShortlist() && shortlist.eva_status.isSedangEvaluasi() && active_user.isPanitia();
            renderArgs.put("allow_penetapan_pra", allow_penetapan_pra);
            boolean allow_pengumuman_pra = tahapAktif.isPengumumanShortlist() && shortlist.eva_status.isSelesai() && active_user.isPanitia();
            renderArgs.put("allow_pengumuman_pra", allow_pengumuman_pra);
			if(shortlist.eva_status.isSelesai() && !allow_penetapan_pra)
				renderArgs.put("status_penetapan_pra", Messages.get("evaluasi.status_penetapan_shortlist"));
		}

		if(!lelang.isSatuFile()) {
			EvaluasiDp teknis = EvaluasiDp.findTeknis(id);
			if (teknis != null) {
				boolean allow_penetapan_teknis = tahapAktif.isEvaluasiTeknis() && teknis.eva_status.isSedangEvaluasi() && active_user.isPanitia();
				renderArgs.put("allow_penetapan_teknis", allow_penetapan_teknis);
				if(allow_penetapan_teknis)
					renderArgs.put("labelTetapkanTeknis", Messages.get("evaluasi.penetapan_teknis"));
				if (teknis.eva_status.isSelesai() && !allow_penetapan_teknis)
					renderArgs.put("status_penetapan_teknis", Messages.get("evaluasi.status_penetapan_teknis"));

				boolean allow_pengumuman_teknis = tahapAktif.isPengumumanTeknis() && teknis.eva_status.isSelesai() && active_user.isPanitia();
				renderArgs.put("allow_pengumuman_teknis", allow_pengumuman_teknis);
				if(allow_pengumuman_teknis)
					renderArgs.put("labelPengumumanTeknis", Messages.get("evaluasi.pengumuman_teknis"));
			}

			EvaluasiDp harga = EvaluasiDp.findHarga(id);
			if (harga != null) {
				boolean allow_penetapan_harga = tahapAktif.isEvaluasiHarga() && harga.eva_status.isSedangEvaluasi() && active_user.isPanitia();
				renderArgs.put("allow_penetapan_harga", allow_penetapan_harga);
				if(allow_penetapan_harga)
					renderArgs.put("labelTetapkanHarga", Messages.get("evaluasi.penetapan_harga"));
				if (harga.eva_status.isSelesai() && !allow_penetapan_harga)
					renderArgs.put("status_penetapan_harga", Messages.get("evaluasi.status_penetapan_harga"));
			}
		}

		// TODO: CQS dan QBS gmn?
		if (!lelang.getMetode().isEmpanelment()) {
			EvaluasiDp akhir = EvaluasiDp.findAkhir(id);
			if (akhir != null) {
				boolean allow_penetapan_rank = ((lelang.getMetode().evaluasi.isQcbs() && tahapAktif.isPenetapanPemenang()) || (!lelang.getMetode().evaluasi.isQcbs() && tahapAktif.isEvaluasiAkhir()))
						&& akhir.eva_status.isSedangEvaluasi() && active_user.isPanitia() && akhir.isExistsPesertaRank();
				renderArgs.put("allow_penetapan_rank", allow_penetapan_rank);
				if(allow_penetapan_rank)
					renderArgs.put("labelTetapkanRank", Messages.get("evaluasi.penetapan_rank"));
				if (akhir.eva_status.isSelesai() && !allow_penetapan_rank)
					renderArgs.put("status_penetapan_rank", Messages.get("evaluasi.status_penetapan_rank"));
			}
		}

		PersetujuanDp persetujuan_pegawai  = PersetujuanDp.findByPegawaiPenetapanAkhir(active_user.pegawaiId, id);
		boolean editable = tahapAktif.isContractNegotiation() && !tahapAktif.isSedangPersetujuanPemenang();
		EvaluasiDp evaluasi = lelang.getEvaluasiAkhir();
		if(evaluasi != null) {
			boolean adaPemenang = NilaiEvaluasiDp.countLulus(evaluasi.eva_id) > 0;
			boolean allow_persetujuan = persetujuan_pegawai != null && (persetujuan_pegawai.pst_status.isBelumSetuju() || persetujuan_pegawai.pst_status.isTidakSetuju())
					&& adaPemenang && tahapAktif.isPenetapanPemenang() && (lelang.getMetode().isEmpanelment() ? evaluasi.eva_status.isSedangEvaluasi() : evaluasi.eva_status.isSelesai());
			if(evaluasi != null) {
				editable = evaluasi.eva_status.isSelesai();
			}
			renderArgs.put("allow_persetujuan", allow_persetujuan);
		}
		renderArgs.put("editable", editable);

		boolean allowPengumumanPemenang = tahapAktif.isPengumumanPemenangAkhir() && !lelang.kirim_pengumuman_pemenang && lelang.hasPemenang();
		renderArgs.put("allow_pengumuman_pemenang", allowPengumumanPemenang);
		if (lelang.kirim_pengumuman_pemenang) {
			renderArgs.put("status_pengumuman_pemenang", Messages.get("evaluasi.status_pengumuman_pemenang"));
		}

		renderTemplate("devpartner/evaluasi/index.html");
	}

	/**
	 * detail evaluasi masing - masing peserta
	 * @param id (Kode Peserta)
	 */
	@AllowAccess({Group.PANITIA,Group.PPK})
	public static void detail(Long id) {
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = peserta.getLelang_seleksi();
		MetodeEvaluasiDp metodeEvaluasi = lelang.getEvaluasi();
		renderArgs.put("id", id);
		renderArgs.put("lelang", lelang);
		Active_user active_user = Active_user.current();
		TahapDpStarted tahapStarted = lelang.getTahapStarted();
		TahapDpNow tahapNow = lelang.getTahapNow();
		boolean persetujuan = lelang.isSedangPersetujuanPemenang();
		renderArgs.put("tahapNow", tahapNow);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("persetujuan", persetujuan);
		if (lelang.isPreQualification()) {
			renderArgs.put("dokumen_eoi", peserta.getFileEoi());
		}
		if (lelang.isSatuFile()) {
			renderArgs.put("dokumen_penawaran", peserta.getFileAdminTeknisHarga());
		} else {
			renderArgs.put("dokumen", peserta.getFileTeknis());
			renderArgs.put("dokumen_harga", peserta.getFileHarga());
		}
		renderArgs.put("peserta", peserta);

		// evaluasi kualifikasi
		if (lelang.isPreQualification()) {
			EvaluasiDp kualifikasi = EvaluasiDp.findNCreateShortlist(lelang.lls_id);
			renderArgs.put("kualifikasi", kualifikasi);
			NilaiEvaluasiDp nilai_kualifikasi = NilaiEvaluasiDp.findBy(kualifikasi.eva_id, peserta.psr_id);
			boolean allow_kualifikasi = (!persetujuan || lelang.isPreQualification()) &&
					tahapNow.isPenetapanShortlist() && kualifikasi.eva_status.isSedangEvaluasi() && active_user.isPanitia();
			renderArgs.put("allow_kualifikasi", allow_kualifikasi);
			renderArgs.put("nilai_kualifikasi", nilai_kualifikasi);
		}

		EvaluasiDp teknis = lelang.isPreQualification() ? EvaluasiDp.findTeknis(lelang.lls_id) : EvaluasiDp.findNCreateTeknis(lelang.lls_id);
		NilaiEvaluasiDp nilai_teknis = null;
		renderArgs.put("teknis", teknis);
		renderArgs.put("showSkorTeknis", metodeEvaluasi.isNilai());
		if (teknis != null) {
			boolean allow_admin_teknis = !persetujuan && tahapNow.isEvaluasiTeknis() && teknis.eva_status.isSedangEvaluasi();
			if (allow_admin_teknis && lelang.isSatuFile()) {
				allow_admin_teknis = lelang.isSudahPembukaanPenawaran();
			}
			nilai_teknis = lelang.isPreQualification() ? NilaiEvaluasiDp.findBy(teknis.eva_id, peserta.psr_id) : NilaiEvaluasiDp.findNCreateBy(teknis.eva_id, peserta, EvaluasiDp.JenisEvaluasi.EVALUASI_TEKNIS);
			renderArgs.put("nilai_teknis", nilai_teknis);
			renderArgs.put("allow_admin_teknis", allow_admin_teknis && active_user.isPanitia());
		}
		EvaluasiDp harga = EvaluasiDp.findHarga(lelang.lls_id);
		NilaiEvaluasiDp nilai_harga = null;
		renderArgs.put("harga", harga);
		if (harga != null) {
			boolean allow_harga = !persetujuan && tahapNow.isEvaluasiHarga()
					&& active_user.isPanitia()
					&& lelang.isSudahPembukaanPenawaran()
					&& (lelang.isSatuFile() || teknis.eva_status.isSelesai())
					&& harga.eva_status.isSedangEvaluasi();
			nilai_harga = NilaiEvaluasiDp.findBy(harga.eva_id, peserta.psr_id);
			renderArgs.put("allow_harga", allow_harga);
			renderArgs.put("nilai_harga", nilai_harga);

			if (lelang.getMetode().evaluasi.isQbs()) {
				NilaiEvaluasiDp highestTeknis = teknis.findSkorTertinggi();
				boolean highestScore = highestTeknis != null && highestTeknis.psr_id.equals(peserta.psr_id);
				renderArgs.put("highest_score", highestScore);
			}
		}

		// evaluasi akhir & empanelment
		EvaluasiDp akhir = lelang.getMetode().isEmpanelment() ? EvaluasiDp.findNCreateAkhir(lelang.lls_id) : EvaluasiDp.findAkhir(lelang.lls_id);
		if (akhir != null) {
			renderArgs.put("akhir", akhir);
			NilaiEvaluasiDp nilai_akhir = NilaiEvaluasiDp.findBy(akhir.eva_id, peserta.psr_id);
			boolean allow_akhir = !persetujuan && akhir.eva_status.isSedangEvaluasi()
					&& active_user.isPanitia()
					&& (lelang.getMetode().isEmpanelment() ? tahapNow.isPenetapanPemenang() : (tahapNow.isEvaluasiAkhir() || tahapNow.isFinalRanking()))
					&& (lelang.isSatuFile() || harga.eva_status.isSelesai());
			if (lelang.getMetode().evaluasi.isQcbs() && nilai_akhir != null && allow_akhir && nilai_akhir.nev_skor == null && nilai_harga != null && nilai_teknis != null) {
				Double skorAkhir = 0.01 * ((nilai_harga.nev_skor * lelang.lls_bobot_biaya) + (nilai_teknis.nev_skor * lelang.lls_bobot_teknis));
				nilai_akhir.nev_skor = skorAkhir;
			}
			renderArgs.put("allow_akhir", allow_akhir);
			renderArgs.put("nilai_akhir", nilai_akhir);
			// final ranking qcbs
			if (lelang.getMetode().evaluasi.isQcbs()) {
				renderArgs.put("allow_rank", tahapNow.isFinalRanking());
			}

			// contract negotiation
			if (!lelang.getMetode().isEmpanelment()) {
				NilaiEvaluasiDp highestRankEva = akhir.findRankTertinggi();
				if (highestRankEva != null) {
					boolean adaPemenang = NilaiEvaluasiDp.countLulus(akhir.eva_id) > 0;
					boolean highestRank = highestRankEva.psr_id.equals(peserta.psr_id);
					boolean allowNego = highestRank && !persetujuan && akhir.eva_status.isSelesai() && !adaPemenang
							&& active_user.isPanitia()
							&& tahapNow.isContractNegotiation();

					renderArgs.put("highest_rank", highestRank);
					renderArgs.put("allow_nego", allowNego);

					if (allowNego) {
						renderArgs.put("lastCandidate", !nilai_akhir.isHasOtherCandidate());
					}
				}
			}
		}

		renderArgs.put("nama",peserta.getNamaPeserta());

		renderTemplate("devpartner/evaluasi/detil.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void submitShortlist(Long id, Boolean lulus, @As(binder= IndonesianDecimalBinder.class) Double skor, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findNCreateShortlist(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findNCreateBy(evaluasi.eva_id, peserta, evaluasi.eva_jenis);

		if (lelang.getMetode().isCqs() && skor == null) {
			Validation.addError("kualifikasi.skor",Messages.get("flash.ahmskpek"));
		}
		if((lulus == null || !lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("kualifikasi.alasan",Messages.get("flash.ahmapekamk"));
		}
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = lulus != null && lulus ? NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode());
			peserta.sudah_evaluasi_kualifikasi = true;
			peserta.save();
			flash.success(Messages.get("flash.dektt"));

		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("kualifikasi").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void submitTeknis(Long id, Boolean lulus, @As(binder = IndonesianDecimalBinder.class) Double skor, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findTeknis(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findBy(evaluasi.eva_id, peserta.psr_id);
		if (lelang.getEvaluasi().isNilai() && skor == null) {
			Validation.addError("teknis.skor", Messages.get("flash.ahmstpet"));
		}
		boolean actualPass = lelang.isManualTechnicalPass() ? (lulus != null && lulus) : skor >= lelang.lls_passinggrade;
		if ((!actualPass) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("teknis.alasan", Messages.get("flash.ahmstpet"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = actualPass ? NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode());
			flash.success(Messages.get("flash.dataetts"));
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("teknis").url);
	}

	/**
	 * submit evaluasi Harga
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA})
	public static void submitHarga(Long id, @As(binder= IndonesianDecimalBinder.class) Double skor,
								   @As(binder= IndonesianDecimalBinder.class) Double harga,
								   @As(binder= IndonesianDecimalBinder.class) Double hargaTerkoreksi, String uraian) {
        checkAuthenticity();
        PesertaDp peserta = PesertaDp.findById(id);
        otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		if(peserta == null || (peserta != null && (peserta.getFileHarga() == null && peserta.getFileAdminTeknisHarga() == null)))
			forbidden("Anda tidak bisa melakukan evaluasi peserta ini.");
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
        if (lelang.isNilaiHarga() && skor == null) {
			Validation.addError("harga.skor", Messages.get("flash.ahmshpeh"));
		}

		if(harga == null || harga == 0.0){
			Validation.addError("harga", Messages.get("flash.htbk"));
		}
		if(hargaTerkoreksi == null || hargaTerkoreksi == 0.0){
			Validation.addError("hargaTerkoreksi", Messages.get("flash.httbk"));
		}

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		}
        else {
			EvaluasiDp evaluasi = EvaluasiDp.findHarga(peserta.lls_id);
			NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findBy(evaluasi.eva_id, peserta.psr_id);
			nilai.nev_harga = harga;
			nilai.nev_harga_terkoreksi = hargaTerkoreksi;
			nilai.nev_lulus = NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			if(harga != null && harga > 0.0) {
				peserta.psr_harga = harga;
				peserta.save();
			}
			if(hargaTerkoreksi != null && hargaTerkoreksi > 0.0) {
				peserta.psr_harga_terkoreksi = hargaTerkoreksi;
				peserta.save();
			}
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode());
			if (lelang.getMetode().evaluasi.isAutoCombinedEvaluation()) {
				EvaluasiDp akhir = EvaluasiDp.findAkhir(lelang.lls_id);
				akhir.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
				akhir.eva_tgl_setuju = new Date();
				akhir.save();
				NilaiEvaluasiDp nilaiAkhir = NilaiEvaluasiDp.findBy(akhir.eva_id, peserta.psr_id);
				nilaiAkhir.nev_rank = NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS;
				nilaiAkhir.nev_combined = NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS;
				nilaiAkhir.nev_urutan = 1;
				nilaiAkhir.save();
			}
	        flash.success(Messages.get("flash.data_evaluasi_htt"));
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("harga").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void submitCombined(Long id, @As(binder = IndonesianDecimalBinder.class) Double skor, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findBy(evaluasi.eva_id, peserta.psr_id);
		if (skor == null) {
			Validation.addError("akhir.skor", Messages.get("flash.ahmstpet"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_skor = skor;
			nilai.nev_combined = NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS;
			nilai.save();
			flash.success(Messages.get("flash.dataetts"));
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("combined").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void submitRank(Long id, Boolean lulus, Integer urutan, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findBy(evaluasi.eva_id, peserta.psr_id);
		if (urutan == null) {
			Validation.addError("akhir.urutan", Messages.get("flash.evaluasi_urutan_diisi"));
		}
		boolean actualPass = lulus != null && lulus;
		if ((!actualPass) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("akhir.alasan", Messages.get("flash.ahmstpet"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_rank = actualPass ? NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_urutan = urutan;
			if (nilai.nev_lulus != null && nilai.nev_lulus.isLulus()) {
				nilai.nev_uraian = null;
			}
			nilai.save();
			flash.success(Messages.get("flash.final_ranking_berhasil"));
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("akhir").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void submitAkhir(Long id, Boolean lulus, @As(binder = IndonesianDecimalBinder.class) Double skor, Integer urutan, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findBy(evaluasi.eva_id, peserta.psr_id);
		if (lelang.getEvaluasi().isNilaiHarga() && skor == null) {
			Validation.addError("teknis.skor", Messages.get("flash.ahmstpet"));
		}
		if (urutan == null) {
			Validation.addError("akhir.urutan", Messages.get("flash.evaluasi_urutan_diisi"));
		}
		boolean actualPass = lulus != null && lulus;
		if ((!actualPass) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("teknis.alasan", Messages.get("flash.ahmstpet"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_rank = actualPass ? NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			if (nilai.nev_rank != null && nilai.nev_rank.isLulus()) {
				nilai.nev_uraian = null;
			}
			nilai.nev_urutan = urutan;
			nilai.nev_skor = skor;
			nilai.save();
			flash.success(Messages.get("flash.dataetts"));
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("teknis").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void submitNegotiation(Long id, Boolean berhasil, @As(binder = IndonesianDecimalBinder.class) Double hargaNego, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findBy(evaluasi.eva_id, peserta.psr_id);
		boolean actualPass = berhasil != null && berhasil;
		if ((!actualPass) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("nego.alasan", Messages.get("flash.ahmstpet"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			if (!actualPass && nilai.isHasOtherCandidate() && (lelang.getMetode().evaluasi.isCqs() || lelang.getMetode().evaluasi.isQbs())) {
				nilai.nev_uraian = uraian;
				nilai.save();

				aturJadwalKandidatBaru(lelang.lls_id);
			}

			nilai.nev_lulus = actualPass ? NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_harga_negosiasi = hargaNego;
			if (nilai.isLulus()) {
				nilai.nev_uraian = null;
			} else {
				nilai.nev_gagal = 1;
			}
			nilai.save();
			if (!nilai.isLulus() && !nilai.isHasOtherCandidate()) {
				try {
					lelang.tutupLelang(uraian);
					flashSuccess("lelang.berhasil_tutup");
				} catch (Exception e) {
					Logger.error("Kesalahan lelangUlang %s", e.getLocalizedMessage());
					flash.error(Messages.get("lelang.gagal_tutup"));
				}
				LelangDpCtr.view(lelang.lls_id);
			}
			flash.success(Messages.get("flash.dataetts"));
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("contract").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void submitEmpanelment(Long id, Boolean lulus, String uraian) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
		EvaluasiDp evaluasi = EvaluasiDp.findNCreateAkhir(lelang.lls_id);
		NilaiEvaluasiDp nilai = NilaiEvaluasiDp.findNCreateBy(evaluasi.eva_id, peserta, evaluasi.eva_jenis);

		if((lulus == null || !lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("akhir.alasan",Messages.get("flash.ahmapekamk"));
		}
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = lulus != null && lulus ? NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS : NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode());
			peserta.save();
			flash.success(Messages.get("flash.deemptt"));

		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("empanelment").url);
	}

    @AllowAccess({Group.PANITIA})
    public static void penetapanShortlist(Long id){
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        DokLelangDp dok_lelang = DokLelangDp.findBy(id, DokLelangDp.JenisDokLelang.DOKUMEN_LELANG);
        LelangDp lelang = LelangDp.findById(id);
        TahapDpNow tahapNow = lelang.getTahapNow();
        EvaluasiDp evaluasi = EvaluasiDp.findShortlist(lelang.lls_id);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapNow", tahapNow);
        renderArgs.put("evaluasi", evaluasi);
        if(evaluasi != null) {
            renderArgs.put("pesertaLulus", evaluasi.findPesertaLulus());
            if (lelang.isPreQualification()) {
                boolean allow_penetapan_pra = tahapNow.isPenetapanShortlist() && evaluasi.eva_status.isSedangEvaluasi();
                renderArgs.put("allow_penetapan_pra", allow_penetapan_pra);
                if (evaluasi.eva_status.isSelesai() && !allow_penetapan_pra)
                    renderArgs.put("status_penetapan_pra", "Penetapan Pemenang Prakualifikasi sudah dilakukan");
            }
        }
        renderTemplate("devpartner/evaluasi/evaluasi-pemenang-prakualifikasi.html");
    }

    /**
     * Penetapan pemenang Prakualifikasi
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void submitPenetapanShortlist(Long id,  Long[] shortlist){
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        DokLelangDp dok_lelang = DokLelangDp.findBy(id, DokLelangDp.JenisDokLelang.DOKUMEN_LELANG);
        if(dok_lelang == null) {
            flash.error(Messages.get("flash.dtsbdatbmppp"));
        }
        // else
        if(!flash.contains("error")) {
            LelangDp lelang = LelangDp.findById(id);
            // TODO check minimum shortlist, benar hanya cqs atau konsultansi?
			if (lelang.getMetode().isCqs()) {
                if (CommonUtil.isEmpty(shortlist)) { // shortlist empty
                    flash.error(Messages.get("flash.dpdsmp"));
                } else { // shortlist sudah ada
                    int jumlahShorlist = shortlist.length;
                    if ((jumlahShorlist < 3 || jumlahShorlist > 6) && !lelang.isLelangUlang()) {
                        flash.error(Messages.get("flash.dpdsbsp"));
                    }
                    EvaluasiDp evaluasi = EvaluasiDp.findShortlist(id);
                    if (evaluasi == null) {
                        flash.error(Messages.get("flash.abmek"));
                    }
                    if (!flash.contains("error")) {
                        for (NilaiEvaluasiDp nilai : evaluasi.findPeserta()) {
                            if (ArrayUtils.contains(shortlist, nilai.nev_id))
                                nilai.nev_lulus = NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS;
                            else if (nilai.isLulus())
                                nilai.nev_lulus = NilaiEvaluasiDp.StatusNilaiEvaluasi.LULUS_TDK_SHORTLIST;
                            else
                                nilai.nev_lulus = NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
                            nilai.save();
                        }
                        evaluasi.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
                        evaluasi.eva_tgl_setuju = newDate();
                        evaluasi.save();
                        flash.success(Messages.get("flash.pp_prabd"));
                    }
                }
            } else {
                EvaluasiDp evaluasi = EvaluasiDp.findShortlist(id);
                if (evaluasi == null) {
                    flash.error(Messages.get("flash.abmek"));
                }
                if (!flash.contains("error")) {
                    evaluasi.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
                    evaluasi.eva_tgl_setuju = newDate();
                    evaluasi.save();
                    flash.success(Messages.get("flash.pp_prabd"));
                }
            }
        }
        index(id);
    }

    @AllowAccess({Group.PANITIA})
    public static void pengumumanShortlist(Long id){
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);
        TahapDpNow tahapNow = lelang.getTahapNow();
        EvaluasiDp evaluasi = EvaluasiDp.findShortlist(lelang.lls_id);
        renderArgs.put("lelang", lelang);
        if(evaluasi != null) {
            renderArgs.put("pesertaLulus", evaluasi.findPesertaLulus());
            if (lelang.isPreQualification()) {
                boolean allow_pengumuman_pra = tahapNow.isPengumumanShortlist() && evaluasi.eva_status.isSelesai();
                renderArgs.put("allow_pengumuman_pra", allow_pengumuman_pra);
            }
        }
        renderTemplate("devpartner/evaluasi/evaluasi-pengumuman-prakualifikasi.html");
    }

    /**
     * Pengumuman pemenang Prakualifikasi
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void submitPengumumanShortlist(Long id){
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);
        EvaluasiDp evaluasi = EvaluasiDp.findShortlist(id);
        TahapDpNow tahapNow = lelang.getTahapNow();
        boolean allow_pengumuman_pra = tahapNow.isPengumumanShortlist() && evaluasi.eva_status.isSelesai();
        if (allow_pengumuman_pra) {
        	try {
				MailDpDao.kirimPengumumanShortlist(lelang);
				flash.success(Messages.get("email.pengumuman_shortlist_dikirim"));
			} catch (Exception e) {
				flash.error(Messages.get("flash.tktpp"));
				Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
			}
        }
        index(id);
    }

	/**
	 * Penetapan pemenang Prakualifikasi
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void penetapanTeknis(Long id){
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		renderArgs.put("lelang", lelang);
		TahapDpNow tahapNow = lelang.getTahapNow();
		if(!lelang.isSatuFile()) {
			EvaluasiDp evaluasi = EvaluasiDp.findTeknis(id);
			renderArgs.put("evaluasi", evaluasi);
			if(evaluasi != null) {
				List<NilaiEvaluasiDp> pesertaList = lelang.getEvaluasi().isNilai() ? evaluasi.findPesertaWithUrutSkor() : evaluasi.findPeserta();
				renderArgs.put("peserta", pesertaList);
				boolean allow_penetapan_teknis = tahapNow.isEvaluasiTeknis() && evaluasi.eva_status.isSedangEvaluasi();
				if(allow_penetapan_teknis)
					renderArgs.put("labelTetapkanTeknis", tahapNow.isPengumumanTeknis() ? "Pengumuman Pemenang Teknis":"Penetapan Pemenang Teknis");
				renderArgs.put("allow_penetapan_teknis", allow_penetapan_teknis);
				if(evaluasi.eva_status.isSelesai() && !allow_penetapan_teknis)
					renderArgs.put("status_penetapan", "Penetapan Pemenang Teknis sudah dilakukan");
			}
		}
		renderTemplate("devpartner/evaluasi/evaluasi-pemenang-teknis.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void submitPenetapanTeknis(Long id) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		EvaluasiDp teknis = EvaluasiDp.findTeknis(id);
		if (teknis == null) {
			flash.error(Messages.get("flash.abmet"));
		}
		else {
			teknis.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
			teknis.eva_tgl_setuju = new Date();
			teknis.save();
			flash.success(Messages.get("flash.penetapan_evaluasi_berhasil"));
		}
		index(id);
	}

	@AllowAccess({Group.PANITIA})
	public static void pengumumanTeknis(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		try {
			MailDpDao.kirimPengumumanTeknis(id);
		} catch (Exception e) {
			Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
		}

		flash.success(Messages.get("lelang.berhasil_pengumuman_teknis"));

		index(id);
	}

	/**
	 * Penetapan pemenang Prakualifikasi
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void penetapanHarga(Long id){
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		renderArgs.put("lelang", lelang);
		TahapDpNow tahapNow = lelang.getTahapNow();
		if(!lelang.isSatuFile()) {
			EvaluasiDp evaluasi = EvaluasiDp.findHarga(id);
			renderArgs.put("evaluasi", evaluasi);
			if(evaluasi != null) {
				List<NilaiEvaluasiDp> pesertaList = lelang.getEvaluasi().isNilaiHarga() ? evaluasi.findPesertaWithUrutSkor() : evaluasi.findPeserta();
				renderArgs.put("peserta", pesertaList);
				boolean allow_penetapan_harga = tahapNow.isEvaluasiHarga() && evaluasi.eva_status.isSedangEvaluasi();
				if(allow_penetapan_harga)
					renderArgs.put("labelTetapkanHarga", "Penetapan Evaluasi Harga");
				renderArgs.put("allow_penetapan_harga", allow_penetapan_harga);
				if(evaluasi.eva_status.isSelesai() && !allow_penetapan_harga)
					renderArgs.put("status_penetapan", "Penetapan Pemenang Harga sudah dilakukan");
			}
		}
		renderTemplate("devpartner/evaluasi/evaluasi-pemenang-harga.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void submitPenetapanHarga(Long id) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		EvaluasiDp harga = EvaluasiDp.findHarga(id);
		LelangDp lelang = harga.getLelang_seleksi();
		if (harga == null) {
			flash.error(Messages.get("flash.abmet"));
		}
		else {
			harga.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
			harga.eva_tgl_setuju = new Date();
			harga.save();
			if (lelang.getMetode().evaluasi.isAutoCombinedEvaluation()) {
				EvaluasiDp akhir = lelang.getEvaluasiAkhir();
				akhir.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
				akhir.eva_tgl_setuju = new Date();
				akhir.save();
			}
			flash.success(Messages.get("flash.penetapan_evaluasi_berhasil"));
		}
		index(id);
	}

	/**
	 * Penetapan pemenang Prakualifikasi
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void penetapanFinalRanking(Long id){
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		renderArgs.put("lelang", lelang);
		TahapDpNow tahapNow = lelang.getTahapNow();
		if(!lelang.getMetode().isEmpanelment()) {
			EvaluasiDp evaluasi = EvaluasiDp.findAkhir(id);
			renderArgs.put("evaluasi", evaluasi);
			if(evaluasi != null) {
				List<NilaiEvaluasiDp> pesertaList = evaluasi.findPesertaWithUrutRank();
				renderArgs.put("peserta", pesertaList);
				boolean allow_penetapan_rank = ((lelang.getMetode().evaluasi.isQcbs() && tahapNow.isPenetapanPemenang()) || (!lelang.getMetode().evaluasi.isQcbs() && tahapNow.isEvaluasiAkhir()))
						&& evaluasi.eva_status.isSedangEvaluasi() && evaluasi.isExistsPesertaRank();
				if(allow_penetapan_rank)
					renderArgs.put("labelTetapkanRank", "Penetapan Peringkat Akhir");
				renderArgs.put("allow_penetapan_rank", allow_penetapan_rank);
				if(evaluasi.eva_status.isSelesai() && !allow_penetapan_rank)
					renderArgs.put("status_penetapan", "Penetapan Peringkat Akhir sudah dilakukan");
			}
		}
		renderTemplate("devpartner/evaluasi/evaluasi-penetapan-rank.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void submitPenetapanRank(Long id) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		EvaluasiDp akhir = EvaluasiDp.findAkhir(id);
		if (akhir == null) {
			flash.error(Messages.get("flash.abmet"));
		}
		else {
			akhir.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
			akhir.eva_tgl_setuju = new Date();
			akhir.save();

			EvaluasiDp teknis = akhir.getLelang_seleksi().getTeknis();
			if (teknis != null && !teknis.eva_status.isSelesai()) {
				teknis.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
				teknis.save();
			}
			EvaluasiDp harga = akhir.getLelang_seleksi().getHarga();
			if (harga != null && !harga.eva_status.isSelesai()) {
				harga.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
				harga.save();
			}
			flash.success(Messages.get("flash.penetapan_evaluasi_berhasil"));
		}
		index(id);
	}

	/**
	 * evaluasi Ulang
	 */
	@AllowAccess({Group.PANITIA})
	public static void persetujuan(Long id){
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		TahapDpStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("evaluasi", EvaluasiDp.findAkhir(id));
		renderArgs.put("editable", false);
		validation.keep();
		params.flash();
		renderTemplate("devpartner/evaluasi/persetujuan-evaluasi.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void editJvPq(Long id, String jv_pq) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		PesertaDp peserta = PesertaDp.findById(id);
		LelangDp lelang = peserta.getLelang_seleksi();
		peserta.psr_jv_pq = jv_pq;
		peserta.save();

		try {
			MailDpDao.kirimPerubahanJvPq(peserta);
		} catch (Exception e) {
			Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
		}
		flash.success(Messages.get("jv.berhasil_diubah"));

		String firstTab = lelang.isPreQualification() ? "kualifikasi":( lelang.getMetode().isEmpanelment() ? "empanelment" : "teknis"); // currentTab di detail evaluasi
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef(firstTab).url);
	}

	@AllowAccess({Group.PANITIA})
	public static void editJvBid(Long id, String jv_bid) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		PesertaDp peserta = PesertaDp.findById(id);
		LelangDp lelang = peserta.getLelang_seleksi();
		peserta.psr_jv_bid_pokja = jv_bid;
		peserta.psr_jv_bid_auto_confirm = DateTime.now().plusDays(2).toDate();
		peserta.save();

		try {
			MailDpDao.kirimPerubahanJvBid(peserta);
		} catch (Exception e) {
			Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
		}

		flash.success(Messages.get("jv.berhasil_diubah"));

		String firstTab = lelang.isPreQualification() ? "kualifikasi":( lelang.getMetode().isEmpanelment() ? "empanelment" : "teknis"); // currentTab di detail evaluasi
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef(firstTab).url);
	}

	@AllowAccess({Group.PANITIA})
	public static void pembukaan(Long id) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		if (!lelang.isSudahPembukaanPenawaran()) {
			lelang.sudah_pembukaan = 1;
			lelang.tanggal_pembukaan = newDate();
			lelang.peg_pembukaan = Active_user.current().pegawaiId;
			lelang.save();
			try {
				MailDpDao.kirimInfoPembukaan(lelang);
			} catch (Exception e) {
				Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
			}

			flash.success(Messages.get("lelang.berhasil_pembukaan"));
		}

		index(id);
	}

    @AllowAccess({Group.PANITIA})
    public static void pembukaanHarga(Long id) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);
        if (!lelang.isSudahPembukaanPenawaran()) {
            lelang.sudah_pembukaan = 1;
            lelang.tanggal_pembukaan = newDate();
            lelang.peg_pembukaan = Active_user.current().pegawaiId;
            lelang.save();
            try {
                MailDpDao.kirimInfoPembukaanHarga(lelang);
            } catch (Exception e) {
                Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
            }

            flash.success(Messages.get("lelang.berhasil_pembukaan_harga"));
        }

        index(id);
    }

    @AllowAccess({Group.PANITIA})
    public static void undanganHarga(Long id) {
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);
        EvaluasiDp teknis = EvaluasiDp.findTeknis(lelang.lls_id);
        boolean allowUndang = lelang.isDuaFile() && teknis != null && teknis.eva_status.isSelesai();
        String labelUndangHarga = Messages.get("evaluasi.btn_kirim");
        List<NilaiEvaluasiDp> pesertaList = teknis.findPesertaLulus();
        renderArgs.put("peserta", pesertaList);

        renderTemplate("devpartner/evaluasi/undangan-harga.html", lelang, allowUndang, labelUndangHarga);
    }

    @AllowAccess({Group.PANITIA})
    public static void submitUndanganHarga(Long id, String waktu, String lokasi, String catatan) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        if (StringUtils.isEmpty(waktu)) {
            Validation.addError("waktu",Messages.get("validation.required", Messages.get("waktu")));
        }
        if (StringUtils.isEmpty(lokasi)) {
            Validation.addError("lokasi",Messages.get("validation.required", Messages.get("lokasi")));
        }
        if(validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error(Messages.get("evaluasi.undangan_harga_gagal"));
        } else {
            LelangDp lelang = LelangDp.findById(id);
            try {
                MailDpDao.kirimUndanganPembukaanHarga(lelang, waktu, lokasi, catatan);
                flash.success(Messages.get("evaluasi.undangan_harga_terkirim"));
            } catch (Exception e) {
                flash.success(Messages.get("evaluasi.undangan_harga_gagal"));
                Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
            }
        }
        index(id);
    }

    @AllowAccess({Group.PANITIA})
    public static void undanganNegosiasi(Long id) {
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(peserta.lls_id);
        String kirim = Messages.get("evaluasi.btn_kirim");

        renderTemplate("devpartner/evaluasi/undangan-negosiasi.html", peserta, lelang, kirim);
    }

	@AllowAccess({Group.PANITIA})
	public static void submitUndanganNegosiasi(Long id, String content) {
		checkAuthenticity();
		PesertaDp peserta = PesertaDp.findById(id);
		otorisasiDataLelangDp(peserta.lls_id); // check otorisasi data lelang
		if (StringUtils.isEmpty(content)) {
			Validation.addError("content",Messages.get("validation.required", Messages.get("content")));
		}
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("evaluasi.undangan_negosiasi_gagal"));
		} else {
			LelangDp lelang = LelangDp.findById(peserta.lls_id);
			try {
				MailDpDao.kirimUndanganNegosiasi(lelang, content);
				flash.success(Messages.get("evaluasi.undangan_negosiasi_terkirim"));
			} catch (Exception e) {
				flash.success(Messages.get("evaluasi.undangan_negosiasi_gagal"));
				Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
			}
		}
		redirect(Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", id).addRef("contract").url);
	}

	@AllowAccess({Group.PANITIA})
	public static void pengumumanPemenang(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		if (!lelang.kirim_pengumuman_pemenang) {
			lelang.kirim_pengumuman_pemenang = true;
			lelang.save();
			try {
				if (lelang.getMetode().isEmpanelment()) {
					MailDpDao.kirimPengumumanPemenangEmpanelment(id);
				} else {
					MailDpDao.kirimPengumumanPemenang(id);
				}
			} catch (Exception e) {
				Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan.");
			}

			flash.success(Messages.get("lelang.berhasil_pengumuman_pemenang"));
		}

		index(id);
	}

	@AllowAccess({Group.PANITIA})
	public static void aturJadwalKandidatBaru(Long id) {
		LelangDp lelang = LelangDp.findById(id);
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		EvaluasiDp akhir = lelang.getEvaluasiAkhir();
		NilaiEvaluasiDp nilaiAkhir = akhir.findRankTertinggi();
		boolean allowEditJadwal = (lelang.getMetode().evaluasi.isCqs() || lelang.getMetode().evaluasi.isQbs()) && lelang.lls_status.isAktif()
				&& nilaiAkhir.isHasOtherCandidate();
		if(!allowEditJadwal)
			forbidden(Messages.get("not-authorized"));

		renderArgs.put("lelang", lelang);
		List<JadwalDp> jadwalList = JadwalDp.findByLelang(lelang.lls_id);
		Date now = newDate();
		if (flash.get("jadwalFlash") != null) {
			List<JadwalFlash> jadwalFlash = new GsonBuilder()
					.registerTypeAdapter(Date.class, new JadwalDp.DateConverter())
					.registerTypeAdapter(Double.class, new DoubleConverter()).create()
					.fromJson(flash.get("jadwalFlash"), new TypeToken<List<JadwalFlash>>(){}.getType());
			jadwalList = JadwalDp.getJadwalList(jadwalFlash, jadwalList, lelang.isAllowReschedule(), now);
		}
		int urutStart = 0;
		TahapDp tahap = lelang.getMetode().evaluasi.isCqs() ? lelang.getTahapLelang().getTahapPengumumanShortlist() : lelang.getTahapLelang().getTahapPembukaanHarga();
		JadwalDp jadwalStart = JadwalDp.findByLelangNTahap(lelang.lls_id, tahap);
		if(jadwalStart != null) {
			urutStart = jadwalStart.akt_urut;
		}

		String template = "devpartner/jadwal/jadwal-edit-kandidat-baru.html";
		renderArgs.put("jadwalList", jadwalList);
		renderArgs.put("now", now);
		renderArgs.put("urutStart", urutStart);
		renderTemplate(template);
	}

	@AllowAccess({Group.PANITIA})
	public static void simpanJadwalKandidatBaru(Long id, List<JadwalDp> jadwalList) {
		checkAuthenticity();
		LelangDp lelang = LelangDp.findById(id);
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		EvaluasiDp akhir = lelang.getEvaluasiAkhir();
		NilaiEvaluasiDp nilaiAkhir = akhir.findRankTertinggi();
		boolean allowEditJadwal = (lelang.getMetode().evaluasi.isCqs() || lelang.getMetode().evaluasi.isQbs()) && lelang.lls_status.isAktif()
				&& nilaiAkhir.isHasOtherCandidate();
		if(!allowEditJadwal)
			forbidden(Messages.get("not-authorized"));

		int urutStart = 0;
		TahapDp tahap = lelang.getMetode().evaluasi.isCqs() ? lelang.getTahapLelang().getTahapPengumumanShortlist() : lelang.getTahapLelang().getTahapPembukaanHarga();
		JadwalDp jadwalStart = JadwalDp.findByLelangNTahap(lelang.lls_id, tahap);
		if(jadwalStart != null) {
			urutStart = jadwalStart.akt_urut;
		}

		Collections.sort(jadwalList, Comparator.comparingInt(jadwal -> jadwal.akt_urut));
		JadwalDp.validate(lelang, jadwalList, newDate(), nilaiAkhir.nev_uraian, urutStart-1, true);
		if (validation.hasErrors()) {
			validation.keep();
		} else {
			try {
				JadwalDp.simpanJadwal(lelang, jadwalList, newDate(), nilaiAkhir.nev_uraian);

				nilaiAkhir.nev_lulus = NilaiEvaluasiDp.StatusNilaiEvaluasi.TDK_LULUS;
				nilaiAkhir.nev_gagal = 1;
				nilaiAkhir.save();

				EvaluasiDp harga = lelang.getHarga();
				NilaiEvaluasiDp nilaiHarga = NilaiEvaluasiDp.findBy(harga.eva_id, nilaiAkhir.psr_id);
				nilaiHarga.nev_gagal = 1;
				nilaiHarga.save();
				harga.eva_status = EvaluasiDp.StatusEvaluasi.SEDANG_EVALUASI;
				harga.save();

				EvaluasiDp teknis = lelang.getTeknis();
				NilaiEvaluasiDp nilaiTeknis = NilaiEvaluasiDp.findBy(teknis.eva_id, nilaiAkhir.psr_id);
				nilaiTeknis.nev_gagal = 1;
				nilaiTeknis.save();
				if (lelang.getMetode().evaluasi.isCqs()) {
					teknis.eva_status = EvaluasiDp.StatusEvaluasi.SEDANG_EVALUASI;
					teknis.save();

					EvaluasiDp shortlist = lelang.getShortlist();
					NilaiEvaluasiDp nilaiShortlist = NilaiEvaluasiDp.findBy(shortlist.eva_id, nilaiAkhir.psr_id);
					nilaiShortlist.nev_gagal = 1;
					nilaiShortlist.save();
				}

				lelang.sudah_pembukaan = 0;
				lelang.tanggal_pembukaan = null;
				lelang.peg_pembukaan = null;
				lelang.save();

				flashSuccess("lelang_jadwal.reschedule_submitted");
				index(id);
			} catch (Exception e) {
				Logger.error(e, "Simpan Jadwal gagal");
				flash.error(Messages.get("flash.djtgt"));
			}
		}

		flash.put("jadwalFlash", CommonUtil.toJson(JadwalDp.getJadwalFlash(jadwalList)));
		aturJadwalKandidatBaru(id);
	}
}