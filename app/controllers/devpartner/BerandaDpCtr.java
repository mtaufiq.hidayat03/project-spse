package controllers.devpartner;

import controllers.security.AllowAccess;
import models.common.Active_user;
import models.secman.Group;

public class BerandaDpCtr extends DevPartnerCtr {

    @AllowAccess({Group.PANITIA, Group.REKANAN})
    public static void index() {
        Active_user active_user = Active_user.current();
        switch (active_user.group) {
            case PANITIA:
                panitia();
                break;
            case REKANAN:
                rekanan();
                break;
        }
    }

    @AllowAccess({Group.PANITIA})
    private static void panitia() {
        Long panitiaId = params.get("panitiaId", Long.class);
        renderArgs.put("panitiaId", panitiaId);
        renderTemplate("devpartner/beranda/panitia.html");
    }

    @AllowAccess({Group.REKANAN})
    private static void rekanan() {
        renderTemplate("devpartner/beranda/rekanan.html");
    }
}
