package controllers.devpartner;

import controllers.BerandaCtr;
import controllers.lelang.PaketCtr;
import controllers.security.AllowAccess;
import models.agency.Paket.StatusPaket;
import models.common.Active_user;
import models.common.Kategori;
import models.devpartner.*;
import models.devpartner.common.*;
import models.devpartner.panel.PanelLelang;
import models.devpartner.panel.PanelUndanganLelang;
import models.jcommon.util.CommonUtil;
import models.lelang.Blacklist;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import play.Logger;
import play.data.validation.Required;
import play.i18n.Messages;

import java.util.*;

/**
 * definisikan semua terkait tender
 *
 * @author arief ardiyansah
 */
public class LelangDpCtr extends DevPartnerCtr {

    /**
     * Fungsi {@code index} digunakan untuk menampilkan halaman riwayat tender
     * e-procurement LPSE
     */
    public static void index(Integer selectKategori) {
        Kategori kategori = null;
        if (selectKategori != null)
            kategori = Kategori.findById(selectKategori);
        renderArgs.put("kategori", kategori);
        renderArgs.put("kategoriList", Kategori.all);
        renderTemplate("lelang/lelang.html");
    }

    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR, Group.PPK})
    public static void view(Long id) {
        Active_user activeUser = Active_user.current();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);
        renderArgs.put("lelang", lelang);
        boolean isPanitia = activeUser.isPanitia();
        boolean isRekanan = activeUser.isRekanan();
        PesertaDp peserta = isRekanan ? PesertaDp.findBy(lelang.lls_id, activeUser.rekananId) : null;
        boolean isPeserta = peserta != null;
        renderArgs.put("isPanitia", isPanitia);
        renderArgs.put("isLelangAktif", lelang.lls_status.isAktif());
        renderArgs.put("tahapNow", JadwalDp.getJadwalSekarang(id, true));
        TahapDpStarted tahapStarted = lelang.getTahapStarted();
        renderArgs.put("tahapStarted", tahapStarted);
        TahapDpNow tahapAktif = lelang.getTahapNow();
        renderArgs.put("tahapAktif", tahapAktif);
        TahapLelangDp tahapLelang = lelang.getTahapLelang();
        renderArgs.put("tahapLelang", tahapLelang);
        // reoi summary
        boolean showReoiSummary = lelang.getMetode().hasReoiSummary();
        if (showReoiSummary && lelang.getMetode().isCallDown() && activeUser.isRekanan()) {
            showReoiSummary = PanelUndanganLelang.isInvited(id, activeUser.rekananId);
        }
        if (showReoiSummary) {
            DokLelangDp reoiSummary = DokLelangDp.findBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY);
            renderArgs.put("dok_reoi_summary", reoiSummary);
        }
        renderArgs.put("showReoiSummary", showReoiSummary);
        // reoi
        boolean showReoi = lelang.getMetode().hasReoi() && (!isRekanan || isPeserta);
        if (showReoi) {
            DokLelangDp reoi = DokLelangDp.findBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_REOI);
            renderArgs.put("dok_reoi", reoi);
        }
        renderArgs.put("showReoi", showReoi);

        // all docs flag
        boolean showPrivateDoc = !isRekanan || isPeserta;
        renderArgs.put("showPrivateDoc", showPrivateDoc);

        boolean showDokLelang = showPrivateDoc && (isPanitia || (!lelang.isPreQualification() || (tahapStarted.isPengumumanShortlist() && peserta.isShortlisted())));
        renderArgs.put("showDokLelang", showDokLelang);

        DokLelangDp dokLelang = DokLelangDp.findBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("dok_lelang", dokLelang);
        renderArgs.put("dok_tambahan", DokLelangDp.findBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_TAMBAHAN));
        if (tahapStarted.isPengumumanPemenangAkhir()) {
            EvaluasiDp penetapan = EvaluasiDp.findAkhir(id);
            renderArgs.put("allow_pengumuman_pemenang", isPanitia && penetapan != null && penetapan.eva_status.isSelesai() 
                    && PersetujuanDp.isApprove(id, PersetujuanDp.JenisPersetujuan.PEMENANG_LELANG));
            renderArgs.put("sudahKirimPengumuman", lelang.kirim_pengumuman_pemenang);
        }
        if (activeUser.isRekanan()) { // untuk rekanan
            renderArgs.put("peserta", peserta);
            renderArgs.put("tahapPenawaran", tahapLelang.getTahapPenawaran());
            if (tahapLelang.isUploadEoi()) {
                renderArgs.put("dokEoi", DokPenawaranDp.findPenawaranPeserta(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_EOI));
                renderArgs.put("tahapEoi", tahapLelang.getTahapUploadEoi());
                renderArgs.put("dokEoiLabel", lelang.getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran.PENAWARAN_EOI));
            }

            if (lelang.getMetode().dokumen.isSatuFile()) {
                renderArgs.put("dokSatuFile", DokPenawaranDp.findPenawaranPeserta(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA));
                renderArgs.put("dokSatuFileLabel", lelang.getPenawaranSatuFileLabel());
            } else {
                renderArgs.put("dokTeknis", DokPenawaranDp.findPenawaranPeserta(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI));
                renderArgs.put("dokTeknisLabel", lelang.getPenawaranTeknisLabel());
                renderArgs.put("dokHarga", DokPenawaranDp.findPenawaranPeserta(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_HARGA));
                renderArgs.put("dokHargaLabel", lelang.getPenawaranHargaLabel());
            }
            List<NilaiEvaluasiDp> pemenangList = lelang.getPemenangList();
            if (!CollectionUtils.isEmpty(pemenangList)) {
                // check pemenang by evaluasi akhir
                for(NilaiEvaluasiDp o : pemenangList) {
                    if(o.psr_id.equals(peserta.psr_id)) {
                        renderArgs.put("pemenang", true);
                        break;
                    }
                }
            }

        } else { // untuk selain rekanan
            boolean hide = !tahapStarted.isShowPenyedia();
            boolean allow_pembukaan = activeUser.isPanitia() && tahapStarted.isPembukaan();
            renderArgs.put("hide", hide);
            renderArgs.put("allow_report",
                    (activeUser.isPanitia() || activeUser.isAuditor() || activeUser.isPpk()) && !hide);
            renderArgs.put("allow_pembukaan", allow_pembukaan);
            if (activeUser.isPanitia()) {
                renderArgs.put("allowEvaluasiUlang", false); // TODO
            }
        }
        renderArgs.put("allowUpdate", false); // fitur Ubah Lelang di-disable, terkait perubahan lelang harus melalui adendum
        PaketDp paket = PaketDp.findById(lelang.pkt_id);
        renderArgs.put("paketPpk", PaketPpkDp.findByPaket(paket.pkt_id));
        renderArgs.put("allowEditJadwal",lelang.lls_status.isAktif() && activeUser.isPanitia());

        renderArgs.put("sudahEvaluasiTeknis",NilaiEvaluasiDp.countLulus(id, EvaluasiDp.JenisEvaluasi.EVALUASI_TEKNIS) > 0);

        renderTemplate("devpartner/lelang/lelang-view.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void edit(Long id) { // oldId diisi jika lelang ulang.
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        LelangDp lelang = LelangDp.findById(id);
        notFoundIfNull(lelang);
        PaketDp paket = lelang.getPaket();
        Double nilaiHps = paket.pkt_hps;
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", paket);
        // Menambah validasi apabila ppk belum diisi di paket tersebut
        if (PaketAnggaranDp.count("pkt_id=? and ppk_id is null", paket.pkt_id) > 0) {
            flash.error(Messages.get("flash.mabmdppk"));
            PaketDpCtr.index(null, null);
        }
        // Validasi jika paket belum ada data lokasi pekerjaan
        if (CommonUtil.isEmpty(paket.getPaketLokasi())) {
            flash.error(Messages.get("flash.mabmdlppdp"));
            PaketDpCtr.index(null, null);
        }
        Kategori kategori = lelang.getKategori();
        renderArgs.put("konsultansi", kategori.isConsultant());
        renderArgs.put("metode", lelang.getMetode());
        boolean sedangPersetujuan = PersetujuanDp.isSedangPersetujuan(lelang.lls_id);
        boolean draft = lelang.lls_status.isDraft() && !sedangPersetujuan;
        renderArgs.put("kategori", kategori);
        renderArgs.put("draft", draft);
        renderArgs.put("ketJadwal", lelang.getInfoJadwal());
        DokLelangDp dok_reoi_summary = !lelang.getMetode().hasReoiSummary() ? null : DokLelangDp.findNCreateBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_REOI_SUMMARY);
        DokLelangDp dok_reoi = !lelang.getMetode().hasReoi() ? null : DokLelangDp.findNCreateBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_REOI);
        DokLelangDp dok_lelang = DokLelangDp.findNCreateBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_LELANG);
        DokLelangDp dok_tambahan = DokLelangDp.findNCreateBy(lelang.lls_id, DokLelangDp.JenisDokLelang.DOKUMEN_TAMBAHAN);

        renderArgs.put("dok_reoi_summary", dok_reoi_summary);
        renderArgs.put("dok_reoi", dok_reoi);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("dok_tambahan", dok_tambahan);
        boolean allow_persetujuan = lelang.isDataLengkap() && lelang.isJadwalLengkap();
        // check kelengkapan dokumen
        if (dok_reoi_summary != null) {
            allow_persetujuan = allow_persetujuan && (!lelang.getMetode().isMandatoryReoiSummaryPengumuman() || dok_reoi_summary.dll_id_attachment != null);
            boolean allow_upload_dok = draft;
            renderArgs.put("allow_upload_reoi_summary", allow_upload_dok);
        }
        if (dok_reoi != null) {
            allow_persetujuan = allow_persetujuan && (!lelang.getSumberDana().mandatoryReoi || !lelang.getMetode().isMandatoryReoiPengumuman() || dok_reoi.dll_id_attachment != null);
            boolean allow_upload_dok = draft;
            renderArgs.put("allow_upload_reoi", allow_upload_dok);
        }
        if (dok_lelang != null) {
            allow_persetujuan = allow_persetujuan && (!lelang.getMetode().isMandatoryDokLelang() || dok_lelang.dll_id_attachment != null);
            boolean allow_upload_dok = draft;
            renderArgs.put("allow_upload_dok", allow_upload_dok);
        }
        if (dok_tambahan != null) {
            boolean allow_upload_tambahan = draft;
            renderArgs.put("allow_upload_tambahan", allow_upload_tambahan);
        }
        PersetujuanDp persetujuan_pegawai = PersetujuanDp.findByPegawaiPengumuman(active_user.pegawaiId, lelang.lls_id);
        if (persetujuan_pegawai != null && allow_persetujuan) {
            allow_persetujuan = persetujuan_pegawai.pst_status.isBelumSetuju() || persetujuan_pegawai.pst_status.isTidakSetuju();
        }

        // pakta integritas tidak muncul bila alasan lelang ulang tidak diisi
        if (lelang.isLelangUlang()) {
            if (lelang.lls_diulang_karena == null || lelang.lls_diulang_karena.isEmpty()) {
                allow_persetujuan = false;
            }
        }

        // Jika dokumen lelang content modified=false
        allow_persetujuan = allow_persetujuan && dok_lelang.allowPersetujuan();

        renderArgs.put("allowBatalPersetujuan",lelang.allowBatalPersetujuan());
        renderArgs.put("allow_persetujuan", allow_persetujuan);
        renderArgs.put("nilaiHps", nilaiHps);

        // panel related
        if (lelang.getMetode().isPanel()) {
            renderArgs.put("panel", PanelLelang.find("lls_id=?", lelang.lls_id).first());
        }

        renderTemplate("devpartner/lelang/lelang-edit.html");
    }


    @AllowAccess({Group.PPK})
    public static void hapus(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);

        if (lelang == null || !lelang.lls_status.isDraft()) {
            flash.error("Maaf, Lelang yang sedang aktif tidak dapat dihapus!");
            PaketCtr.index(null, null);
        }
        try {
            lelang.delete();
        } catch (Exception ex) {
            Logger.info(ex,"Error delete " + ex.getLocalizedMessage());
            flash.error("Gagal menghapus paket " + lelang.pkt_id);
            PaketCtr.index(null, null);
        }

        PaketDpCtr.index(null, null);
    }

    /**
     * TODO: FUGLY
     * annoying glitch number conversion in play?
     * value = 10.5 (ten point 5)
     * render in HTML = 10.5 (using dot as decimal separator)
     * on submission = 105 (using comma as decimal separator)
     */
    @AllowAccess({Group.PANITIA})
    public static void simpan(Long id, LelangDp lelang, String bobot_teknis, String bobot_biaya, String passing_grade,
                              Long panelId) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp obj = LelangDp.findById(id);
        if(lelang != null) {
            if (obj.getMetode().evaluasi.isQcbs()) {

                lelang.lls_bobot_teknis = bobot_teknis == null ? null : Double.parseDouble(bobot_teknis);
                lelang.lls_bobot_biaya = bobot_biaya == null ? null : Double.parseDouble(bobot_biaya);

                if (lelang.lls_bobot_teknis == null || lelang.lls_bobot_teknis <= 0
                        || lelang.lls_bobot_biaya == null || lelang.lls_bobot_biaya <= 0) {
                    flash.error(Messages.get("qcbs.bobot.required"));
                    edit(obj.lls_id);
                }
                if ((int)Math.round(lelang.lls_bobot_teknis + lelang.lls_bobot_biaya) != 100) {
                    flash.error(Messages.get("qcbs.bobot.100"));
                    edit(obj.lls_id);
                }
                obj.lls_bobot_teknis = lelang.lls_bobot_teknis;
                obj.lls_bobot_biaya = lelang.lls_bobot_biaya;
            } else {
                obj.lls_bobot_teknis = 0d;
                obj.lls_bobot_biaya = 0d;
            }
            if (obj.getMetode().evaluasi.isNilai()) {

                lelang.lls_passinggrade = passing_grade == null ? null : Double.parseDouble(passing_grade);

                if (lelang.lls_passinggrade == null || lelang.lls_passinggrade <= 0) {
                    flash.error(Messages.get("qcbs.passinggrade.required"));
                    edit(obj.lls_id);
                }
                obj.lls_passinggrade = lelang.lls_passinggrade;
            } else {
                obj.lls_passinggrade = 0d;
            }
            obj.lls_kontrak_pembayaran = lelang.lls_kontrak_pembayaran;
            obj.lls_kontrak_tahun = lelang.lls_kontrak_tahun;
            obj.lls_kontrak_sbd = lelang.lls_kontrak_sbd;
            PaketDp paket = PaketDp.findByLelang(id);
            if (paket.pkt_status.equals(StatusPaket.ULANG_LELANG)) {
                if (lelang.lls_diulang_karena != null && !lelang.lls_diulang_karena.isEmpty()) {
                    obj.lls_diulang_karena = lelang.lls_diulang_karena;
                } else {
                    flash.error(Messages.get("flash.amlhdi"));
                    edit(obj.lls_id);
                }
            }
            // panel handling
            if (obj.getMetode().isPanel()) {
                if (panelId == null) {
                    flash.error(Messages.get("flash.panel.required"));
                    edit(obj.lls_id);
                }
                PanelLelang.createOrUpdate(panelId, obj.lls_id);
            } else {
                // delete panel related data
                PanelLelang.delete("lls_id = ?", obj.lls_id);
            }
            flash.success(Messages.get("flash.dbd"));
            obj.save();
        }
        edit(obj.lls_id);
    }
    
    /**
     * Fungsi {@code rekananLelangBaru} digunakan untuk menampilkan halaman
     * daftara lelang baru yang tersedia untuk rekanan
     */
    @AllowAccess({Group.REKANAN})
    public static void lelangBaru() {
        Date now = newDate();
        Blacklist blacklist = Blacklist.find("rkn_id=? and bll_tglawal <= ? AND bll_tglakhir >= ?", Active_user.current().rekananId, now, now).first();
        if (blacklist != null)
            BerandaCtr.index(); // jika rekanan terblacklist tidak dpt ikut lelang dan kembali home rekanan
        else
            renderTemplate("devpartner/lelang/lelang-baru.html");
    }

    /*
     * Penutupan Lelang
     * TODO
     */
    @AllowAccess({Group.PANITIA})
    public static void tutup(Long id) {
//        otorisasiDataLelang(id); // check otorisasi data lelang
//        LelangDp lelang = LelangDp.findById(id);
//        notFoundIfNull(lelang);
//        if(lelang.isDitutup())
//            forbidden("Maaf Tender sudah dibatalkan");
//        renderArgs.put("lelang", lelang);
//        renderArgs.put("tahapNow", JadwalDp.getJadwalSekarang(id, true));

//        PersetujuanDp persetujuan = PersetujuanDp.findByPegawaiLelangInJenisBatal(lelang.lls_id);
//        renderArgs.put("persetujuan",persetujuan);
//        if(persetujuan != null){
//            renderArgs.put("allowPersetujuan",persetujuan.pst_status.isBelumSetuju() || persetujuan.pst_status.isTidakSetuju());
//        }

//        renderTemplate("lelang/lelang-tutup.html");
    }

    /**
     * TODO
     * Admin Lpse mengubah jadwal Fungsi {@code ubahJadwalLelang} digunakan
     * untuk menampilkan halaman.
     *
     * @param lelangId
     */
    @AllowAccess({Group.ADM_PPE})
    public static void buka(Long lelangId) {
//        if (lelangId != null) {
//            LelangDp lelang = LelangDp.findById(lelangId);
//            if (lelang != null) {
//                if (lelang.lls_status.isDitutup()) {
//                    renderArgs.put("lelang", lelang);
//                } else {
//                    flash.error("Lelang tidak sedang dibatalkan");
//                }
//            } else if (lelangId != 0) {
//                flash.error("Lelang yang Anda cari belum terdaftar");
//            }
//        }
//        renderTemplate("lelang/lelang-buka.html");
    }

    /**
     * submit penutupan lelang atau lelang diulang
     *
     * @param id
     * @param actionLelang
     * @param alasan
     * @param alasan
     */
    @AllowAccess({Group.PANITIA})
    public static void tutupSubmit(Long id, String actionLelang, @Required String alasan, Boolean setuju, String alasanTidakSetuju) {
//        checkAuthenticity();
//        otorisasiDataLelang(id); // check otorisasi data lelang
//
//        if(setuju == null){
//            setuju = true;
//        }
//
//        if (!setuju && CommonUtil.isEmpty(alasanTidakSetuju)) {
//            Validation.addError("alasanTidakSetuju", "Alasan Harus diisi");
//        }else if (!setuju && !CommonUtil.isEmpty(alasanTidakSetuju) && alasanTidakSetuju.length() <= 30) {
//            Validation.addError("alasanTidakSetuju", "Alasan harus lebih dari 30 karakter");
//        }
//
//        if (validation.hasErrors()) {
//            validation.keep();
//            params.flash();
//            tutup(id);
//        } else {
//            try {
//                Lelang_seleksi lelang = Lelang_seleksi.findById(id);
//                notFoundIfNull(lelang);
//                if(lelang.isDitutup())
//                    forbidden("Maaf Tender sudah dibatalkan");
//                lelang.lls_ditutup_karena = alasan;
//                lelang.save();
//
//                JenisPersetujuan jenis = JenisPersetujuan.BATAL_LELANG;
//                if(actionLelang.equals("ulang")){
//                    jenis = JenisPersetujuan.ULANG_LELANG;
//                }
//
//                Persetujuan.simpanPersetujuanBatalTender(lelang,setuju,alasanTidakSetuju,jenis);
//
//                if (Persetujuan.isApprove(id, jenis)) {
//                    if (jenis.equals(JenisPersetujuan.BATAL_LELANG)) {
//                        try {
//                            Lelang_seleksi.tutupLelang(lelang, alasan);
//                            BerandaCtr.index();
//                        } catch (Exception e) {
//                            Logger.error("Kesalahan lelangUlang %s", e.getLocalizedMessage());
//                            flash.error("Proses batal lelang tidak dapat dilakukan. mohon ulangi");
//                        }
//                    } else if (jenis.equals(JenisPersetujuan.ULANG_LELANG)) {
//                        try {
//                            Lelang_seleksi.tutupLelang(lelang, alasan);
//                            Long lelangIdBaru = Lelang_seleksi.ulangLelang(id);
//                            if (lelang.getPemilihan().isLelangExpress())
//                                edit(lelangIdBaru);
//                            else
//                                edit(lelangIdBaru);
//                        } catch (Exception e) {
//                            Logger.error("Kesalahan lelangUlang %s", e.getLocalizedMessage());
//                            flash.error("Proses mengulang lelang tidak dapat dilakukan. mohon ulangi");
//                        }
//                    }
//                }
//            }catch (Exception e) {
//                e.printStackTrace();
//                flash.error(e.getMessage());
//            }
//        }
//
//        tutup(id);
    }

    /**
     * submit penutupan lelang atau lelang diulang
     *
     * @param id
     * @param tutup
     * @param ulang
     * @param alasan
     */
    @AllowAccess({Group.ADM_PPE})
    public static void bukaSubmit(Long id, boolean tutup, boolean ulang, @Required String alasan) {
//        checkAuthenticity();
//        if (validation.hasErrors()) {
//            validation.keep();
//            params.flash();
//            buka(id);
//        } else {
//            try {
//                LelangDp lelang = LelangDp.findById(id);
//                lelang.bukaLelang(alasan);
//                flash.error("Reaktivasi Tender telah berhasil dilakukan");
//            } catch (Exception e) {
//                Logger.error("Kesalahan Reaktivasi Tender %s", e.getLocalizedMessage());
//                flash.error("Reaktivasi Tender tidak dapat dilakukan. Mohon ulangi");
//            }
//            buka(id);
//        }
    }

    /**
     * submit lelang ulang
     *
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void ulang(Long id) {
//        checkAuthenticity();
//        otorisasiDataLelang(id); // check otorisasi data lelang
//        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
//        notFoundIfNull(lelang);
//        if(lelang.lls_status.isAktif())
//            forbidden("Maaf Tender sedang Berjalan, Anda tidak bisa melakukan Tender Ulang");
//        Long lelangIdBaru = Lelang_seleksi.ulangLelang(id);
//        edit(lelangIdBaru);
    }

    /**
     * aktivitas pengumuman lelang hak akses public
     *
     * @param id
     */
    public static void pengumumanLelang(Long id) {
        LelangDp lelang = LelangDp.findById(id);
        notFoundIfNull(lelang);
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", lelang.getPaket());
        boolean isAuditor = false;
        if(Active_user.current() != null){
            isAuditor = Active_user.current().isAuditor();
        }
        if (!isAuditor && lelang != null && !lelang.lls_status.isAktif())
            forbidden(Messages.get("not-authorized"));// hanya lelang aktif yang muncul di pengumuman
        renderArgs.put("tahapStarted", lelang.getTahapStarted());
        renderArgs.put("anggaranList", AnggaranDp.findByPaket(lelang.pkt_id));
        renderArgs.put("lokasiList", PaketLokasiDp.findByPaket(lelang.pkt_id));
        renderArgs.put("tahapNow", JadwalDp.getJadwalSekarang(id, true));
        renderTemplate("devpartner/pengumuman-lelang.html");
    }

    @AllowAccess({Group.PANITIA, Group.PPK})
    public static void viewDraft(Long id) {
        LelangDp lelang = LelangDp.findById(id);
        notFoundIfNull(lelang);
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", lelang.getPaket());
        renderArgs.put("tahapStarted", lelang.getTahapStarted());
        renderArgs.put("anggaranList", AnggaranDp.findByPaket(lelang.pkt_id));
        renderArgs.put("lokasiList", PaketLokasiDp.findByPaket(lelang.pkt_id));
        renderTemplate("devpartner/lelang-draft.html");
    }


    @AllowAccess({Group.PANITIA})
    public static void viewLisLelang(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        renderArgs.put("lls_id", id);
        renderTemplate("lelang/lelang-list.html");
    }

//    private static JsonObject generateJsonObjectForUpdateLelang(Lelang_seleksi lelang, Long ppk_id) {
//
//        Paket paket = lelang.getPaket();
//        JsonObject jsonObject = new JsonObject();
//
//        //jsonObject.addProperty("pkt_nama", paket.pkt_nama);
//        jsonObject.addProperty("ppk_id", Paket_anggaran.findPPKByPaket(paket.pkt_id));
//        jsonObject.addProperty("pkt_hps", paket.pkt_hps.longValue());
//        jsonObject.addProperty("pkt_pagu", paket.pkt_pagu.longValue());
//        jsonObject.addProperty("lls_kontrak_pembayaran", lelang.lls_kontrak_pembayaran);
//        if (paket.kls_id != null) {
//            jsonObject.addProperty("kls_id", paket.getKualifikasi().id);
//        }
//
//        List<Paket_lokasi> lokasiList = Paket_lokasi.find("pkt_id=?", paket.pkt_id).fetch();
//        if (lokasiList.size() > 0) {
//            JsonArray jsonArray = new JsonArray();
//            for (Paket_lokasi lokasi : lokasiList) {
//                JsonObject jsonObjectArray = new JsonObject();
//                jsonObjectArray.addProperty("pkl_id", lokasi.pkl_id);
//                jsonObjectArray.addProperty("kbp_id", lokasi.kbp_id);
//                jsonObjectArray.addProperty("pkt_lokasi", lokasi.pkt_lokasi);
//                jsonArray.add(jsonObjectArray);
//
//            }
//            jsonObject.add("lokasi", jsonArray);
//
//        }
//
//        return jsonObject;
//
//    }

    @AllowAccess({Group.REKANAN, Group.AUDITOR, Group.PPK})
    public static void penawaran(Long id, int jenis) {
//        otorisasiDataLelang(id); // check otorisasi data lelang
//        Active_user active_user = Active_user.current();
//        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
//        renderArgs.put("jenis", jenis);
//        renderArgs.put("lelang", lelang);
//        renderArgs.put("tahapAktif", lelang.getTahapNow());
//        boolean allowed = false;
//        if (active_user.isRekanan()) { // untuk rekanan
//            Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", lelang.lls_id, active_user.rekananId).first();
//            renderArgs.put("peserta", peserta);
//            Dok_penawaran penawaran_kualifikasi = Dok_penawaran.findPenawaranPeserta(peserta.psr_id,
//                    JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
//            boolean kualifikasiUploaded = penawaran_kualifikasi != null || lelang.getPemilihan().isLelangExpress();
//
//            allowed = active_user.isRekanan() && peserta != null && kualifikasiUploaded;
//            if (lelang.isSatuFile()) {
//                allowed = allowed && jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id;
//            } else {
//                allowed = allowed && (jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id || jenis == JenisDokPenawaran.PENAWARAN_HARGA.id);
//            }
//        }
//        renderArgs.put("allowed", allowed);
//        renderTemplate("lelang/lelang-penawaran.html");
    }

    @AllowAccess({Group.PANITIA, Group.AUDITOR, Group.PPK})
    public static void pembukaan(Long id, int jenis) {
//        otorisasiDataLelang(id); // check otorisasi data lelang
//        Active_user active_user = Active_user.current();
//        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
//        TahapStarted tahapStarted = lelang.getTahapStarted();
//        boolean allowed = (active_user.isPanitia() || active_user.isAuditor())
//                && !lelang.isLelangV3() && tahapStarted.isPembukaan();
//        if (lelang.isSatuFile()) {
//            allowed = allowed && jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id;
//        } else {
//            allowed = allowed && (jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id || jenis == JenisDokPenawaran.PENAWARAN_HARGA.id);
//        }
//        renderArgs.put("jenis", jenis);
//        renderArgs.put("lelang", lelang);
//        renderArgs.put("tahapStarted", tahapStarted);
//        renderArgs.put("allowed", allowed);
//        renderTemplate("lelang/lelang-pembukaan.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void metode(Long id) {
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp lelang = LelangDp.findById(id);
        Kategori kategori = lelang.getPaket().kgr_id;
        boolean sedangPersetujuan = PersetujuanDp.isSedangPersetujuan(lelang.lls_id);
        renderArgs.put("draft", lelang.lls_status.isDraft() && !sedangPersetujuan);
        renderArgs.put("lelang", lelang);
        renderArgs.put("kategori", kategori);
        renderArgs.put("kategoriList", KategoriDp.all);
        renderArgs.put("metodeList", MetodeDp.getByKategori(kategori));
        Map metodeMap = new TreeMap();
        for (Kategori kat: KategoriDp.all) {
            List metodeForKategori = new ArrayList();
            MetodeDp[] metodeList = MetodeDp.getByKategori(kat);
            for (MetodeDp mtd: metodeList) {
                Map metodeItem = new HashMap();
                metodeItem.put("id", mtd.id);
                metodeItem.put("label", mtd.getLabel());
                metodeForKategori.add(metodeItem);
            }
            metodeMap.put(kat.id, metodeForKategori);
        }
        renderArgs.put("allMetode", CommonUtil.toJson(metodeMap));
        renderTemplate("devpartner/lelang/pilih-metode.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void metodeSubmit(Long id, Integer kategoriId, Integer mtd) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        LelangDp obj = LelangDp.findById(id);
        Kategori kategori = Kategori.findById(kategoriId);
        MetodeDp metode = MetodeDp.findById(mtd);
        if ((obj.mtd_id != null && !obj.mtd_id.equals(metode.id))) {
            HistoryJadwalDp.delete("dtj_id in (select dtj_id from devpartner.jadwal_dp where lls_id=?)", obj.lls_id);
            JadwalDp.delete("lls_id=?", obj.lls_id);
        }
        obj.mtd_id = metode.id;
        obj.save();
        PaketDp paket = obj.getPaket();
        paket.kgr_id = Kategori.findById(kategoriId);
        paket.save();

        // panel handling
        if (!obj.getMetode().isPanel()) {
            // delete panel related data
            PanelLelang.delete("lls_id = ?", obj.lls_id);
        }

        edit(id);
    }

    @AllowAccess({Group.REKANAN})
    public static void editJvPq(Long id, String jv_pq) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        PesertaDp peserta = PesertaDp.findByRekananAndLelang(active_user.rekananId, id);
        peserta.psr_jv_pq = jv_pq;
        peserta.save();

        flash.success(Messages.get("jv.berhasil_diubah"));
        view(id);
    }

    @AllowAccess({Group.REKANAN})
    public static void editJvBid(Long id, String jv_bid) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        PesertaDp peserta = PesertaDp.findByRekananAndLelang(active_user.rekananId, id);
        peserta.psr_jv_bid = jv_bid;
        peserta.save();

        flash.success(Messages.get("jv.berhasil_diubah"));
        view(id);
    }

    @AllowAccess({Group.REKANAN})
    public static void confirmJvBid(Long id, Integer jv_confirm, String jv_bid) {
        checkAuthenticity();
        otorisasiDataLelangDp(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        PesertaDp peserta = PesertaDp.findByRekananAndLelang(active_user.rekananId, id);
        if (jv_confirm != null && jv_confirm == 1) {
            peserta.psr_jv_bid = peserta.psr_jv_bid_pokja;
        } else {
            peserta.psr_jv_bid = jv_bid;
        }
        peserta.psr_jv_bid_pokja = null;
        peserta.psr_jv_bid_confirm = 1;
        peserta.psr_jv_bid_auto_confirm = null;
        peserta.save();

        flash.success(Messages.get("jv.berhasil_diubah"));
        view(id);
    }
}
