package controllers.devpartner;

import controllers.security.AllowAccess;
import ext.DokumenType;
import models.common.Active_user;
import models.devpartner.LelangDp;
import models.devpartner.NilaiEvaluasiDp;
import models.devpartner.common.MetodeDp;
import models.devpartner.panel.Panel;
import models.devpartner.panel.PanelLelang;
import models.devpartner.panel.PanelRekanan;
import models.devpartner.panel.PanelUndanganLelang;
import models.jcommon.util.CommonUtil;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.lang3.ArrayUtils;
import play.i18n.Messages;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PanelCtr extends DevPartnerCtr {

    /**
     * Fungsi {@code index} menampilkan halaman daftar panel.
     *
     */
    @AllowAccess({Group.ADM_PANEL})
    public static void index() {
        renderTemplate("devpartner/panel/index.html");
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void edit(Long id) {
        Panel panel = id == null ? null : Panel.findById(id);
        if (panel != null && !panel.isRightfulOwner(Active_user.current().pegawaiId)) {
            flash.error(Messages.get("panel.invalid_access"));
            index();
        }
        renderTemplate("devpartner/panel/edit.html", panel);
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void simpan(Long id, Panel panel, @DokumenType File memberDetails, Long hapus, @DokumenType File contractAward, Long hapusAward) {
        checkAuthenticity(); // Setiap menjalankan fungsi ini perlu cek otentifikasi
        Panel newPanel = id == null ? null : Panel.findById(id);
        if (newPanel != null && !newPanel.isRightfulOwner(Active_user.current().pegawaiId)) {
            flash.error(Messages.get("panel.invalid_access"));
            index();
        }

        validation.valid(panel);
        if(validation.hasErrors()) {
            validation.keep();
            params.flash();
            edit(id);
        } else {
            if (newPanel == null) {
                newPanel = new Panel();
                newPanel.pnl_owner_id = Active_user.current().pegawaiId;
            }

            try {
                // actual save
                newPanel.simpan(panel, memberDetails, hapus, contractAward, hapusAward);
                flash.success(Messages.get("flash.dbd"));
                index();
            } catch (Exception e) {
                flash.error(Messages.get("flash.dgd"));
                validation.keep();
                params.flash();
                edit(id);
            }
        }
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void hapus(List<Long> chkPanel) {
        checkAuthenticity(); // Setiap menjalankan fungsi ini perlu cek otentifikasi (submit token dari form)
        if(!CommonUtil.isEmpty(chkPanel)) {
            Panel.hapus(chkPanel);
        }
        index();
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void view(Long id) {
        Panel panel = id == null ? null : Panel.findById(id);
        if (panel == null || !panel.isRightfulOwner(Active_user.current().pegawaiId)) {
            flash.error(Messages.get("panel.not_found"));
            index();
        }

        // actual view
        renderTemplate("devpartner/panel/view.html", panel);
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void vendor(Long id) {
        Panel panel = id == null ? null : Panel.findById(id);
        if (panel == null || !panel.isRightfulOwner(Active_user.current().pegawaiId)) {
            flash.error(Messages.get("panel.not_found"));
            index();
        }
        List<PanelRekanan> vendors = PanelRekanan.findByPanel(panel.pnl_id);

        // actual view
        renderTemplate("devpartner/panel/vendor-list.html", panel, vendors);
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void hapusVendor(Long panelId, List<Long> chkVendor) {
        checkAuthenticity(); // Setiap menjalankan fungsi ini perlu cek otentifikasi (submit token dari form)
        if(!CommonUtil.isEmpty(chkVendor)) {
            PanelRekanan.hapus(panelId, chkVendor);
        }
        index();
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void viewVendor(Long panelId, Long id) {
        PanelRekanan panelRekanan = PanelRekanan.findBy(panelId, id);
        if (panelRekanan == null) {
            flash.error(Messages.get("panel.vendor_not_found"));
            index();
        }
        // actual view
        renderTemplate("devpartner/panel/view-vendor.html", panelRekanan);
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void addVendor(Long id, Long lls_id) {
        Panel panel = id == null ? null : Panel.findById(id);
        if (panel == null || !panel.isRightfulOwner(Active_user.current().pegawaiId)) {
            flash.error(Messages.get("panel.not_found"));
            index();
        }
        if (lls_id != null) {
            LelangDp lelang = LelangDp.findById(lls_id);
            if (lelang.getPemenangList() != null) {
                long[] registeredRekanan = PanelRekanan.findByPanel(panel.pnl_id).stream().mapToLong(value -> value.getRekanan().rkn_id).toArray();
                List<Rekanan> vendors = new ArrayList<>();
                for (NilaiEvaluasiDp eva : lelang.getPemenangList()) {
                    if (!ArrayUtils.contains(registeredRekanan, eva.getPeserta().rkn_id)) {
                        vendors.add(Rekanan.findById(eva.getPeserta().rkn_id));
                    }
                }
                renderArgs.put("vendors", vendors);
            }
            renderArgs.put("lelang", lelang);
        }
        List<PanelLelang> lelangs = PanelLelang.findAllBy(panel, MetodeDp.EMPANELMENT);
        // actual view
        renderTemplate("devpartner/panel/add-vendor.html", panel, lelangs);
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void submitVendor(Long panelId, List<Long> chkVendor) {
        checkAuthenticity(); // Setiap menjalankan fungsi ini perlu cek otentifikasi (submit token dari form)
        if(!CommonUtil.isEmpty(chkVendor)) {
            PanelRekanan.tambah(panelId, chkVendor);
        }
        index();
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void callDown(Long id) {
        Panel panel = id == null ? null : Panel.findById(id);
        if (panel == null || !panel.isRightfulOwner(Active_user.current().pegawaiId)) {
            flash.error(Messages.get("panel.not_found"));
            index();
        }
        List<PanelRekanan> vendors = PanelRekanan.findByPanel(panel.pnl_id);
        List<PanelLelang> lelangs = PanelLelang.findAllBy(panel, MetodeDp.CALL_DOWN);

        // actual view
        renderTemplate("devpartner/panel/call-down.html", panel, vendors, lelangs);
    }

    @AllowAccess({Group.ADM_PANEL})
    public static void submitCallDown(Long panelId, Long lls_id, List<Long> chkVendor) {
        checkAuthenticity(); // Setiap menjalankan fungsi ini perlu cek otentifikasi (submit token dari form)
        if(!CommonUtil.isEmpty(chkVendor)) {
            PanelUndanganLelang.tambah(lls_id, chkVendor);
        }
        index();
    }
}
