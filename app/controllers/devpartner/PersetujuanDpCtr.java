package controllers.devpartner;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.StatusLelang;
import models.devpartner.*;
import models.devpartner.PersetujuanDp.JenisPersetujuan;
import models.devpartner.PersetujuanDp.StatusPersetujuan;
import models.jcommon.util.CommonUtil;
import models.secman.Group;
import play.data.validation.Validation;
import play.i18n.Messages;

import java.util.concurrent.ExecutionException;

public class PersetujuanDpCtr extends DevPartnerCtr {
	@AllowAccess({ Group.PANITIA})
	public static void riwayatPersetujuan(Long pst_id) throws InterruptedException, ExecutionException {
		PersetujuanDp persetujuan = PersetujuanDp.findById(pst_id);
		otorisasiDataLelangDp(persetujuan.lls_id);
		renderArgs.put("listRiwayatPersetujuan", RiwayatPersetujuanDp.RiwayatPersetujuanLelang.findList(persetujuan.peg_id, persetujuan.lls_id, persetujuan.pst_jenis));
		renderTemplate("lelang/riwayat-persetujuan.html");
	}

	/**
	 * Persetujuan Pengumuman lelang
	 * @param id
	 * @param setuju
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@AllowAccess({ Group.PANITIA})
	public static void pengumuman(Long id, String alasan, boolean setuju)  {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		PaketDp paket = lelang.getPaket();

		if (!setuju && CommonUtil.isEmpty(alasan)) {
			Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
		}

		if (!lelang.isJadwalLengkap()){
			Validation.addError("jadwal", Messages.get("flash.jadwal_kosong"));
			flash.error(Messages.get("flash.jadwal_tender_masih_kosong"));
		}

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		}
		else {
			PersetujuanDp persetujuan = PersetujuanDp.find("peg_id=? and lls_id=? and pst_jenis=?", Active_user.current().pegawaiId, id, JenisPersetujuan.PENGUMUMAN_LELANG).first();
			persetujuan.pst_status = StatusPersetujuan.SETUJU;
			if (!CommonUtil.isEmpty(alasan)) {
				persetujuan.pst_alasan = alasan;
				// jika ada alasan maka status tidak setuju
				persetujuan.pst_status = StatusPersetujuan.TIDAK_SETUJU;
			}
			if (persetujuan.pst_status.isSetuju()) {
				persetujuan.pst_alasan = null; //clear alasan
			}

			persetujuan.pst_tgl_setuju = newDate();
			persetujuan.save();

			// tambahkan ke riwayat persetujuan
			RiwayatPersetujuanDp.simpanRiwayatPersetujuan(persetujuan);

			if (PersetujuanDp.isApprove(id, JenisPersetujuan.PENGUMUMAN_LELANG)) {
				int sesi = 0;
				if (ConfigurationDao.isLatihan())
					sesi = BasicCtr.getSesiPelatihan().id;
				if(lelang.lls_status.isDraft()) {
					// TODO: workflow instance?
//					ProcessInstance proses = WorkflowDao.createProsesInstance(lelang.lls_id, lelang.lls_wf_id, lelang.getMetode());
//					lelang.lls_wf_id = proses.process_instance_id;
//					lelang.save();
//					WorkflowDao.updateState(proses);
					// proses.startInstance();
//					proses.status = ProcessInstance.PROCESS_STATUS.RUNNING;
//					proses.save();
					lelang.lls_sesi = sesi;
					lelang.lls_status = StatusLelang.AKTIF;
					lelang.lls_tgl_setuju = newDate();
					paket.pkt_status = PaketDp.StatusPaket.SEDANG_LELANG;
					paket.save();
					lelang.save();
				}
			}

		}
		LelangDpCtr.edit(id);
	}

	@AllowAccess({Group.PANITIA})
	public static void batalPersetujuanPengumuman(Long lelangId){
		Active_user user = Active_user.current();
		PersetujuanDp persetujuan = PersetujuanDp.findByPegawaiPengumuman(user.pegawaiId, lelangId);
		persetujuan.pst_status = PersetujuanDp.StatusPersetujuan.BELUM_SETUJU;
		persetujuan.pst_tgl_setuju = null;
		persetujuan.save();
		LelangDpCtr.edit(lelangId);
	}

	/**
	 * Persetujuan Pemenang lelang
	 * @param id
	 * @param alasan
	 * @param setuju
	 */
	@AllowAccess({ Group.PANITIA})
	public static void pemenang(Long id, String alasan, boolean setuju) {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang

		if (!setuju && CommonUtil.isEmpty(alasan)) {
			Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
		}else if (!setuju && !CommonUtil.isEmpty(alasan) && alasan.length() <= 10) {
			Validation.addError("alasan", Messages.get("flash.ahssc"));
		}

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			EvaluasiDpCtr.persetujuan(id);
		}
		else {
			try {
				simpanPersetujuanPemenang(id, alasan, setuju);
			}catch (Exception e) {
				flash.error(e.getMessage());
			}

			EvaluasiDpCtr.index(id);
		}

	}

	private static void simpanPersetujuanPemenang(Long id, String alasan, boolean setuju) {
		PersetujuanDp persetujuan = PersetujuanDp.findByPegawaiLelangAndJenis(id, JenisPersetujuan.PEMENANG_LELANG);

		if (!setuju && !CommonUtil.isEmpty(alasan)) {
			persetujuan.pst_alasan = alasan;
			persetujuan.pst_status = StatusPersetujuan.TIDAK_SETUJU; //jika ada alasan maka status tidak setuju
		}else if(setuju) {
			persetujuan.pst_alasan = null;
			persetujuan.pst_status = StatusPersetujuan.SETUJU;
		}

		persetujuan.pst_tgl_setuju = BasicCtr.newDate();
		persetujuan.save();

		// tambahkan ke riwayat persetujuan
		RiwayatPersetujuanDp.simpanRiwayatPersetujuan(persetujuan);

		if (PersetujuanDp.isApprove(id, JenisPersetujuan.PEMENANG_LELANG)) {
			EvaluasiDp evaluasi = EvaluasiDp.findAkhir(id);
			evaluasi.eva_status = EvaluasiDp.StatusEvaluasi.SELESAI;
			evaluasi.eva_tgl_setuju = newDate();
			evaluasi.save();
			for (NilaiEvaluasiDp nilai : NilaiEvaluasiDp.findBy(evaluasi.eva_id)) {
				if (nilai.isLulus()) {
					PesertaDp peserta = PesertaDp.findById(nilai.psr_id);
					peserta.is_pemenang = Integer.valueOf(1);
					peserta.save();
				}
			}
		}
	}
}
