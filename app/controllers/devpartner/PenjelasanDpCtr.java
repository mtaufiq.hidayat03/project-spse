package controllers.devpartner;

import controllers.security.AllowAccess;
import ext.DokumenType;
import models.common.Active_user;
import models.devpartner.DiskusiLelangDp;
import models.devpartner.LelangDp;
import models.devpartner.PesertaDp;
import models.devpartner.common.TahapDp;
import models.devpartner.common.TahapDpStarted;
import models.devpartner.common.TahapLelangDp;
import models.jcommon.util.CommonUtil;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Error;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * definisi semua action terkait penjelasan/aanwijzing 
 *  
 * @author idoej
 * 
 */
public class PenjelasanDpCtr extends DevPartnerCtr {

	/**
	 * tahapan penjelasan dokumen pengadaan
	 * @param id
	 */
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.PPK })
	public static void pengadaan(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		Active_user active_user = Active_user.current();
		TahapLelangDp tahapLelang = new TahapLelangDp(lelang);
		TahapDp tahap = tahapLelang.getTahapPenjelasan();
		Group group = active_user.group;
		boolean allow = DiskusiLelangDp.isWriteable(id, tahap, group, newDate()) && lelang.lls_status.isAktif();
		if(active_user.isRekanan()) {
			PesertaDp peserta = PesertaDp.find("lls_id=? and rkn_id=?", id, active_user.rekananId).first();
			if(lelang.isPreQualification() && peserta != null && !peserta.isShortlisted()) {
				forbidden(Messages.get("not-authorized"));
			}
			renderArgs.put("isPeserta", allow && peserta != null);
		}
		TahapDpStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("allow", allow);
		renderArgs.put("group", group);
		Map params = new HashMap<String, Object>(1);
		params.put("id", lelang.lls_id);
		renderArgs.put("simpanUrl", Router.reverse("devpartner.PenjelasanDpCtr.simpan", params));
		renderArgs.put("sisaUrl", Router.reverse("devpartner.PenjelasanDpCtr.sisa_waktu", params));
		renderArgs.put("listUrl", Router.reverse("devpartner.PenjelasanDpCtr.list", params));
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderTemplate("devpartner/penjelasan/index.html");
	}
	
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.PPK})
	public static void list(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		TahapLelangDp tahapLelangDp = new TahapLelangDp(lelang);
		TahapDp tahap = tahapLelangDp.getTahapPenjelasan();
		Group group = Active_user.current().group;
        TahapDpStarted tahapStarted = lelang.getTahapStarted();
		List<DiskusiLelangDp> list=DiskusiLelangDp.getDiskusiLelangTopik(id, tahap);
		renderArgs.put("tahap", tahap);
		renderArgs.put("list", list);
		renderArgs.put("group", group);
		renderArgs.put("allow", DiskusiLelangDp.isWriteable(id, tahap, group, newDate()) && lelang.lls_status.isAktif());
		renderArgs.put("hide", !tahapStarted.isShowPenyedia());
		renderTemplate("devpartner/penjelasan/list.html");
	}
	
	public static void sisa_waktu(Long id) {
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		TahapLelangDp tahapLelang = new TahapLelangDp(id);
		String sisa = DiskusiLelangDp.getSisaWaktu(id, tahapLelang.getTahapPenjelasan());
		renderText(sisa);
	}
	

	@AllowAccess({ Group.PANITIA, Group.REKANAN})
	public static void simpan(Long id, DiskusiLelangDp penjelasan, Long pertanyaan_id, @Valid @DokumenType File file) throws Exception {
		checkAuthenticity();
		otorisasiDataLelangDp(id); // check otorisasi data lelang

		validation.valid(penjelasan);

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			StringBuilder sb = new StringBuilder();
			for(Error err:validation.errors()) {
				if(!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}
			flash.error(Messages.get("flash.dgt")+ sb);
		} else {
			TahapDp tahap = new TahapLelangDp(id).getTahapPenjelasan();
			if (CommonUtil.isEmpty(penjelasan.dsl_uraian)) {
				validation.addError("penjelasan.dsl_uraian", Messages.get("flash.utbk"));
			} else {
				DiskusiLelangDp.simpan(id, penjelasan, tahap, file, pertanyaan_id);
			}
		}

		pengadaan(id);

	}
}
