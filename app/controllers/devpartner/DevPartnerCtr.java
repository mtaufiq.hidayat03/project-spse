package controllers.devpartner;

import controllers.BasicCtr;
import models.agency.Anggota_panitia;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.StatusLelang;
import models.devpartner.LelangDp;
import models.devpartner.PaketAnggaranDp;
import models.devpartner.PaketDp;
import models.devpartner.PesertaDp;
import play.i18n.Messages;
import play.mvc.Before;

public class DevPartnerCtr extends BasicCtr {

    @Before
    static void checkEnabled() {
        if (!ConfigurationDao.isEnableDevPartner()) {
            forbidden(Messages.get("Cannot access resource")); // tampilkan halaman error
        }
    }

    // chek otorisasi active user terhadap data paket yang diakses
    protected static void otorisasiDataPaketDp(PaketDp paket) {
        Active_user active_user = Active_user.current();
        if (active_user.isPanitia())
            otorisasiDataPanitia(paket.pnt_id);
        else if (active_user.isPpk()) {
            // chek otorisasi active user terhadap data ppk yang diakses

            if (!PaketAnggaranDp.isPpkInThisPaket(paket.pkt_id, active_user.ppkId)) {
                forbidden(Messages.get("not-authorized"));
            }
        } else if (active_user.isKuppbj()){
            if(!paket.ukpbj_id.equals(active_user.ukpbjId)){
                forbidden(Messages.get("not-authorized"));
            }
        }
    }

    // chek otorisasi active user terhadap data lelang yang diakses
    protected static void otorisasiDataLelangDp(Long lelangId) {
        Active_user active_user = Active_user.current();
        if (active_user == null)
            forbidden();
        if(active_user.isRekanan()) {
            StatusLelang statusLelang = LelangDp.findStatusLelang(lelangId);
            PesertaDp peserta = PesertaDp.findBy(lelangId, active_user.rekananId);
            if (peserta == null || !statusLelang.isAktif())
                forbidden(Messages.get("not-authorized"));

        } else if (active_user.isPanitia() || active_user.isPpk()) {
            PaketDp paket = PaketDp.findByLelang(lelangId);
            authByUserRole(active_user, paket);
        } else {
            forbidden(Messages.get("not-authorized"));
        }
    }

    protected static void otorisasiDataLelangDp(LelangDp lelang) {
        if(lelang == null) {
            badRequest("Tender not found");
        }
        Active_user active_user = Active_user.current();
        if (active_user == null)
            forbidden();
        if(active_user.isRekanan()) {
            StatusLelang statusLelang = lelang.lls_status;
            PesertaDp peserta = PesertaDp.findBy(lelang.lls_id, active_user.rekananId);
            if (peserta == null || !statusLelang.isAktif())
                forbidden(Messages.get("not-authorized"));

        } else if (active_user.isPanitia() || active_user.isPpk()) {
            PaketDp paket = lelang.getPaket();
            authByUserRole(active_user, paket);
        } else {
            forbidden(Messages.get("not-authorized"));
        }
    }

    protected static void authByUserRole(Active_user active_user, PaketDp paket) {
        if(paket != null) {
            switch (active_user.group) {
                case PANITIA:
                    boolean isPanitia = Anggota_panitia.isAnggotaPanitia(paket.pnt_id, active_user.pegawaiId);
                    if (!isPanitia)
                        forbidden(Messages.get("not-authorized"));
                    break;
                case PPK:
                    if (PaketAnggaranDp.count("pkt_id=? AND ppk_id=?", paket.pkt_id, active_user.ppkId) == 0L)
                        forbidden(Messages.get("not-authorized"));
                    break;
            }
        }
    }
}
