package controllers.devpartner;


import com.google.gson.JsonObject;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.PdfType;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.UploadInfo;
import models.devpartner.*;
import models.devpartner.common.TahapDp;
import models.devpartner.common.TahapDpNow;
import models.secman.Group;
import org.apache.commons.io.FilenameUtils;
import play.Logger;
import play.data.validation.Valid;
import play.mvc.Router;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author idoej
 * Controller untuk dokumen Dev Partner
 */
public class DokumenDpCtr extends DevPartnerCtr {

	@AllowAccess({Group.PANITIA})
	public static void upload(Long id, int jenisDok) {
		DokLelangDp dok_lelang = DokLelangDp.findById(id);
		renderArgs.put("id", dok_lelang.lls_id);
		renderArgs.put("dokId", id);
		renderArgs.put("dok_lelang", dok_lelang);
		renderArgs.put("today", newDate());
		renderArgs.put("jenisDok", jenisDok);

		// Dokumen Tender/Seleksi adalah label untuk Tender Prakualifikasi
		// Dokumen Pemilihan adalah label untuk Tender Pascakualifikasi
		String labelDokumen = "Tender/Seleksi";
		renderArgs.put("labelDokumen", labelDokumen);

		Map<String, Object> params=new HashMap();
		params.put("id",dok_lelang.lls_id);
		String ref = Router.getFullUrl("devpartner.LelangDpCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}

		renderArgs.put("referer",ref);
		renderTemplate("devpartner/dokumen/upload.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void submit(Long id, int jenisDok, @Valid @PdfType File file) {
		LelangDp lelang = LelangDp.findById(id);
		otorisasiDataLelangDp(lelang); // check otorisasi data lelang
		String namaDokumen = "Tender/Seleksi";
		if (validation.hasErrors()) {
			flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
		} else {
			DokLelangDp dok_lelang = DokLelangDp.findNCreateBy(lelang.lls_id, DokLelangDp.JenisDokLelang.fromValue(jenisDok));
			try {
				// cetak konten html
				dok_lelang.simpan(lelang, file);
				flash.success("Upload Dokumen %s berhasil.", namaDokumen);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Tender/Seleksi");
			}
		}
		LelangDpCtr.edit(id);
	}

	@AllowAccess({Group.REKANAN})
	public static void viewKirimPenawaran(Long id, Integer jenis){
		otorisasiDataLelangDp(id); // check otorisasi data lelang
		LelangDp lelang = LelangDp.findById(id);
		renderArgs.put("lelang", lelang);
		Active_user active_user = Active_user.current();
		PesertaDp peserta = PesertaDp.findBy(lelang.lls_id, active_user.rekananId);
		renderArgs.put("peserta", peserta);
		String namaLpse = ConfigurationDao.getNamaLpse();
		renderArgs.put("namaLpse", namaLpse);
		TahapDpNow tahapNow = new TahapDpNow(lelang.lls_id);
		TahapDp tahapSekarang = null;
		boolean editable = false;
		if (jenis == DokPenawaranDp.JenisDokPenawaran.PENAWARAN_EOI.id) {
			tahapSekarang = tahapNow.getTahapUploadEoi();
			editable = tahapNow.isUploadEoi();
		} else {
			tahapSekarang = tahapNow.getTahapPenawaran();
			editable = tahapNow.isPenawaran();
			if (editable && lelang.isPreQualification()) {
				editable = peserta.isShortlisted();
			}
		}
		JadwalDp jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, tahapSekarang);
		if(jadwal != null){
			renderArgs.put("tglPenutupan", jadwal.dtj_tglakhir);
		}
		renderArgs.put("doc_penawaran_list", DokPenawaranDp.findByPsrAndJenis(peserta.psr_id, jenis));
		renderArgs.put("editable", editable);
		renderArgs.put("jenis", jenis);
		renderArgs.put("dokLabel", lelang.getDokPenawaranLabel(DokPenawaranDp.JenisDokPenawaran.fromValue(jenis)));
		renderTemplate("devpartner/dokumen/penawaran.html");
	}

	@AllowAccess({Group.REKANAN})
	public static void uploadPenawaran(Long lls_id, Long psr_id, Integer jenis, @Valid @DokumenType File file) throws Exception {
		DokPenawaranDp.JenisDokPenawaran jenisDokPenawaran = DokPenawaranDp.JenisDokPenawaran.fromValue(jenis);
		Map<String, Object> result = new HashMap<>();
		try {
			if (file != null) {
				int filenameLength = FilenameUtils.removeExtension(file.getName()).length();
				if (filenameLength <= 80) {
					Map<String, Object> res = DokPenawaranDp.simpanDokPenawaran(psr_id, file, jenisDokPenawaran);
					UploadInfo info = (UploadInfo) res.get("file");
					List<UploadInfo> files = new ArrayList<UploadInfo>();
					files.add(info);

					result.put("files", files);
					result.put("dok_id", res.get("dok_id"));

				} else
					result.put("error", "Dokumen Harga Gagal di simpan: Judul dokumen maksimal 80 karakter.");
			}
		} catch (Exception e){
			result.put("error", "Dokumen Gagal di simpan: Judul dokumen maksimal 80 karakter.");
			Logger.error("Kesalahan saat upload penawaran : detail %s", e.getMessage());
			e.printStackTrace();
		}

		renderJSON(result);

	}

	@AllowAccess({Group.REKANAN})
	public static void hapusPenawaran(Long dok_id) {
		DokPenawaranDp dokPenawaran = DokPenawaranDp.findById(dok_id);
		JsonObject result = new JsonObject();
		try {
			dokPenawaran.deleteModelAndBlob();
			result.addProperty("status", "ok");
		} catch (Exception e) {
			Logger.error("Hapus Dokumen", e.getLocalizedMessage());
			result.addProperty("status", "error");
			result.addProperty("message", "Dokumen gagal dihapus!");
		}
		renderJSON(result);
	}
}
