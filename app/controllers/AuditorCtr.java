package controllers;

import controllers.security.AllowAccess;
import ext.DokumenType;
import models.agency.*;
import models.auditor.*;
import models.jcommon.blob.BlobTable;
import models.lelang.History_dok_penawaran;
import models.lelang.History_penawaran;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import models.nonlelang.Pl_seleksi;
import models.secman.Group;
import models.secman.Usrsession;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.Query;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Kelas {@code AuditorCtr} merupakan kelas yang mengatur alur aktivitas yang
 * berkaitan dengan objek {@code Auditor}. Created by IntelliJ IDEA. Date:
 * 27/08/12 Time: 15:16
 *
 * @author I Wayan Wiprayoga W
 */
public class AuditorCtr extends BasicCtr {

    /**
     * Fungsi {@code auditor} digunakan untuk menampilkan halaman daftar auditor
     * yang ada pada sistem SPSE.
     */
    @AllowAccess({Group.ADM_PPE})
    public static void index() {
        renderTemplate("admin/auditor/auditor.html");
    }

    /**
     * Fungsi {@code auditorEdit} digunakan untuk menampilkan halaman edit data
     * auditor.
     *
     * @param id id auditor
     */
    @AllowAccess({Group.ADM_PPE})
    public static void edit(Long id) {
        boolean peg_baru = false;
        if (id == null) { // tambah auditor baru
            renderArgs.put("peg_baru", true);
            renderTemplate("admin/auditor/auditorEdit.html");
        } else { // edit auditor
            Auditor auditor = Auditor.findById(id);
            renderArgs.put("auditor", auditor);
            renderArgs.put("pegawai", Pegawai.findById(auditor.peg_id)); // objek pegawai yang merupakan auditor
            renderTemplate("admin/auditor/auditorEdit.html");
        }
    }

    @AllowAccess({Group.ADM_PPE})
    public static void auditorSkView(Long id) {
        boolean peg_baru = false;
        if (id == null) { // tambah auditor baru
            renderArgs.put("peg_baru", true);
            renderTemplate("admin/auditor/auditorSkView.html");
        } else { // edit auditor
            Auditor auditor = Auditor.findById(id);
            renderArgs.put("pegawai", Pegawai.findById(auditor.peg_id)); // objek pegawai yang merupakan auditor
            Auditor_skauditor skid = Auditor_skauditor.find("auditorid=?", id).first();
            renderArgs.put("auditor", auditor);
            if (skid != null) {
                renderArgs.put("ska", Skauditor.findById(skid.skid));
                renderTemplate("admin/auditor/auditorSkView.html");
            } else {
                renderTemplate("admin/auditor/auditorSkView.html");
            }

        }

    }

    /**
     * Fungsi {@code auditorSimpanSubmit} digunakan untuk menyimpan model
     * {@code Pegawai} sekaligus juga model {@code Auditor} (karena auditor juga
     * merupakan pegawai LPSE) hasil submit dari form edit Pegawai.
     *
     * @param pegawai objek {@code Pegawai} yang telah di-binding
     */
    @AllowAccess({Group.ADM_PPE})
    public static void simpan(Pegawai pegawai, String group, boolean chkPassword, String newPassword, String ulangiPassword, String isActive) {
        checkAuthenticity(); // cek token
        pegawai.peg_isactive = isActive != null ? -1 : 0;

        if (pegawai.peg_id == null) {
            validation.required(newPassword).message("Password Baru wajib diisi");
            validation.minSize(newPassword, 8).message("Panjang Password minimal 8 karakter");
            if (pegawai.peg_namauser != null) {
                Pegawai checkUser = Pegawai.findBy(pegawai.peg_namauser);
                if (checkUser != null)
                    validation.addError("pegawai.peg_namauser", "validation.unique");
            }
        }

        if (chkPassword) {
            validation.required(newPassword).message("Password Baru wajib diisi");
            validation.minSize(newPassword, 8).message("Panjang Password minimal 8 karakter");
        }

        validation.valid(pegawai);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Data gagal tersimpan. Mohon periksa kembali inputan Anda");
            if (pegawai.peg_id != null) {
                Auditor auditor = Auditor.find("peg_id=?", pegawai.peg_id).first();
                edit(auditor.auditorid);
            } else
                edit(null);
        } else {
            try {
                Pegawai.simpan(pegawai, chkPassword, newPassword, group);
                Auditor.createAuditorFromPegawai(pegawai.peg_id); // buat objek auditor untuk
                flash.success("Data berhasil tersimpan");
                index(); // jika berhasil tampilkan halaman daftar auditor
            } catch (Exception e) {
                flash.error("Data gagal tersimpan");
                renderArgs.put("pegawai", pegawai);
                renderTemplate("admin/auditor/auditorEdit.html");
            }
        }
    }

    /**
     * Fungsi {@code auditorHapusSubmit} digunakan untuk menghapus objek
     * {@code Auditor}.
     */
    @AllowAccess({Group.ADM_PPE})
    public static void hapus(Long[] chkAuditor) {
        checkAuthenticity(); // cek token
        if (chkAuditor != null) {
            try {
                for (Long val : chkAuditor) {
                    Auditor.hapus(val); // hapus objek auditor juga berarti
                }
                flash.success("Data Berhasil dihapus");
            }catch (Exception e){
                Logger.error(e, "Hapus Auditor gagal");
                flash.error("Data tidak berhasil dihapus");
            }
        }
        index(); // tampilkan kembali halaman daftar auditor
    }

    // TODO: Belum implementasi
    @AllowAccess({Group.ADM_PPE})
    public static void sk() {
        renderTemplate("admin/skauditor/skauditor.html");
    }

    /**
     * Fungsi {@code skauditorEdit} digunakan untuk menampilkan halaman edit SK
     * auditor
     *
     * @param id id skauditor
     */
    @AllowAccess({Group.ADM_PPE})
    public static void editSk(Long id) {
        if (id != null) {
            Skauditor skauditor = Skauditor.findById(id);
            List<Auditor_skauditor> auditor_skauditor = Auditor_skauditor.find("skid=?", skauditor.skid).fetch();
            //List<Paket_skauditor> paket_skauditor = Paket_skauditor.find("skid=?", skauditor.skid).fetch();
            List<PaketSkAuditor> paket_skauditor = PaketSkAuditor.findAllBySkId(skauditor.skid);
            renderArgs.put("skauditor", skauditor);
            renderArgs.put("auditor_skauditor", auditor_skauditor);
            renderArgs.put("paket_skauditor", paket_skauditor);
        }
        renderTemplate("admin/skauditor/skauditor-edit.html");
    }

    @AllowAccess({Group.ADM_PPE})
    public static void viewsk(Long id) {
        if (id != null) { // tambah skauditor baru
            Skauditor ska = Skauditor.findById(id);
            List<Auditor_skauditor> auditor_skauditor = Auditor_skauditor.find("skid=?", ska.skid).fetch();
            List<Paket_skauditor> paket_skauditor = Paket_skauditor.find("skid=?", ska.skid).fetch();
            renderArgs.put("ska", ska);
            renderArgs.put("auditor_skauditor", auditor_skauditor);
            renderArgs.put("paket_skauditor", paket_skauditor);
        }
        renderTemplate("admin/skauditor/skauditor-view.html");
    }

    // TODO: Belum implementasi
    @AllowAccess({Group.ADM_PPE})
    public static void simpanSk(Skauditor skauditor, @DokumenType File attachment, List<Long> auditorId, String hapusAuditor, List<Long> chkAuditor, List<Long> paketId, String hapusPaket, List<Long> chkPaket, String hapus) {
        checkAuthenticity();
        validation.valid(skauditor);
        if (attachment == null && hapus == null) {
            validation.addError("attachment", "Dokumen Surat Tugas wajib dilampirkan");
        }
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
        } else {
            try {
                File file = attachment != null ? attachment : null;
                if (hapusAuditor != null) {
                    if (chkAuditor != null) {
                        String join = StringUtils.join(chkAuditor, ",");
                        Query.update("delete from Auditor_skauditor where skid=? and auditorid in (" + join + ')', skauditor.skid);
                    }
                    flash.success("%s berhasil dihapus", "Paket");
                }
                if (hapusPaket != null) {
                    if (chkPaket != null) {
                        String join = StringUtils.join(chkPaket, ",");
                        Query.update("delete from Paket_skauditor where skid=? and pkt_id in (" + join + ')', skauditor.skid);
                    }
                    flash.success("%s berhasil dihapus", "Paket");
                }
                Skauditor.simpanSk(skauditor, file, hapus);
                flash.success("%s berhasil disimpan", "Surat Tugas");
            } catch (Exception e) {
                flash.error("%s gagal disimpan", "Surat Tugas");
                Logger.error(e, "Surat Tugas gagal disimpan");
            }
        }
        editSk(skauditor.skid);
    }

    // TODO: Belum implementasi
    @AllowAccess({Group.ADM_PPE})
    public static void daftarAuditor(Long id) {
        renderArgs.put("id", id);
        renderTemplate("admin/skauditor/auditor-daftar.html");
    }

    @AllowAccess({Group.ADM_PPE})
    public static void simpanPilihAuditor(Long id, Long[] chkAuditor) {
        // cek otentikasi
        if (chkAuditor != null) {
            Auditor.createPilihAuditor(id, chkAuditor);
            flash.success("Auditor telah ditambahkan");
        }
        editSk(id);
    }

    // TODO: Belum implementasi
    @AllowAccess({Group.ADM_PPE})
    public static void pilihPaket(Long id,String instansiId, Long satkerId, Integer tahun) {
        renderArgs.put("id", id);
        renderArgs.put("instansiId", instansiId);
        renderArgs.put("satkerId", satkerId);
        renderArgs.put("tahun", tahun);
        renderArgs.put("skauditor", Skauditor.findById(id));
        renderArgs.put("tahunList", Anggaran.listTahunAnggaran());
        renderTemplate("admin/skauditor/auditor-pilih-paket.html");
    }

    public static void simpanPilihPaket(Long skId, Long satkerId, Integer tahun, Long[] chkPaket) {
        if (chkPaket != null) {
            for (Long lelangId : chkPaket) {
                Lelang_seleksi lelangSeleksi = Lelang_seleksi.findById(lelangId);
                Paket_skauditor temp = Paket_skauditor.find("pkt_id=? and skid=?", lelangSeleksi.pkt_id, skId).first();
                if (temp != null) {
                    Paket paket = Paket.findById(temp.pkt_id);
                    flash.error("Paket %s sudah dipilih", paket.pkt_nama);
                    editSk(skId);
                }
                Skauditor.createPaketAuditor(skId, lelangSeleksi.pkt_id);
            }
            flash.success("Paket telah ditambahkan");
            editSk(skId);
        }
    }

    // TODO: Belum implementasi
    @AllowAccess({Group.ADM_PPE})
    public static void hapusSk(Long[] chkAuditorSK) {
        checkAuthenticity(); // cek token
        if (!ArrayUtils.isEmpty(chkAuditorSK)) {
            try {
                for (Long val : chkAuditorSK) {
                    Skauditor.hapus(val);
                }
                flash.success("Data berhasil dihapus");
            }catch (Exception e) {
                Logger.error(e,"Data Gagal terhapus");
                flash.error("Data tidak berhasil dihapus");
            }
        }
        sk();
    }

    @AllowAccess({Group.PANITIA, Group.AUDITOR})
    public static void audit_kontrak(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("lelang", lelang);
        renderArgs.put("sppbj", Sppbj.find("lls_id=?", lelang.lls_id).first());
        renderArgs.put("kontrak",Kontrak.find("lls_id=?", lelang.lls_id).first());
        renderTemplate("lelang/audit-kontrak.html");
    }

    @AllowAccess({Group.AUDITOR})
    public static void audit_log(Long id, String tanggal) throws ParseException {
        Peserta peserta = Peserta.findById(id);
        renderArgs.put("peserta", peserta);
        String userid = Query.find("select rkn_namauser from rekanan where rkn_id=?", String.class, peserta.rkn_id).first();
        List<Usrsession> log = null;
        if (StringUtils.isEmpty(tanggal)) {
            log = Usrsession.findByUserid(userid);
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date tgl1 = formatter.parse(tanggal);
            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
            String tgl = formatter1.format(tgl1);
            log = Usrsession.find("userid=? and sessiontime > '" + tgl + " 00:00:00' and sessiontime  <'" + tgl + " 23:59:59'", userid).fetch();
        }
        renderArgs.put("log",log);
        renderTemplate("lelang/audit-userlog.html");
    }

    @AllowAccess({Group.AUDITOR})
    public static void audit_penawaran(Long id) throws ParseException {
        Peserta peserta = Peserta.findById(id);
        renderArgs.put("peserta", peserta);
        List<History_dok_penawaran> list = History_dok_penawaran.find("psr_id=?", id).fetch();
        renderArgs.put("list",list);
        renderTemplate("lelang/audit-penawaran.html");
    }

    @AllowAccess({Group.PANITIA, Group.AUDITOR})
    public static void audit_log_dokumen(Long id, Integer versi) {
        renderArgs.put("blob", BlobTable.findById(id, versi));
        renderTemplate("lelang/informasiDokumen.html");
    }

    @AllowAccess({Group.ADM_PPE})
    public static void skNonLelang() {
        renderTemplate("admin/skauditor/skauditor-nonlelang.html");
    }

    @AllowAccess({Group.ADM_PPE})
    public static void hapusSkNonLelang(Long[] chkAuditorSK) {
        checkAuthenticity(); // cek token
        if (!ArrayUtils.isEmpty(chkAuditorSK)) {
            try {
                for (Long val : chkAuditorSK) {
                    SkAuditorNonLelang.hapus(val);
                }
                flash.success("Data berhasil dihapus");
            }catch (Exception e){
                Logger.error(e, e.getMessage());
                flash.error("Data tidak berhasil dihapus");
            }
        }
        skNonLelang();
    }

    @AllowAccess({Group.ADM_PPE})
    public static void editSkNonLelang(Long id) {
        if (id != null) {
            SkAuditorNonLelang skauditor = SkAuditorNonLelang.findById(id);
            List<AuditorSkAuditorNonLelang> auditor_skauditor = AuditorSkAuditorNonLelang.find("skid=?", skauditor.skid).fetch();
            //List<Paket_skauditor> paket_skauditor = Paket_skauditor.find("skid=?", skauditor.skid).fetch();
            List<PaketSkAuditorNonLelangObj> paket_skauditor = PaketSkAuditorNonLelangObj.findAllBySkId(skauditor.skid);
            renderArgs.put("skauditor", skauditor);
            renderArgs.put("auditor_skauditor", auditor_skauditor);
            renderArgs.put("paket_skauditor", paket_skauditor);
        }
        renderTemplate("admin/skauditor/skauditor-nonlelang-edit.html");
    }

    @AllowAccess({Group.ADM_PPE})
    public static void simpanSkNonLelang(SkAuditorNonLelang skauditor, @DokumenType File attachment, List<Long> auditorId, String hapusAuditor, List<Long> chkAuditor, List<Long> paketId, String hapusPaket, List<Long> chkPaket, String hapus) {
        checkAuthenticity();
        validation.valid(skauditor);
        if (attachment == null && hapus == null) {
            validation.addError("attachment", "Dokumen Surat Tugas wajib dilampirkan");
        }
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
        } else {
            try {
                File file = attachment != null ? attachment : null;
                if (hapusAuditor != null) {
                    if (chkAuditor != null) {
                        String join = StringUtils.join(chkAuditor, ",");
                        Query.update("DELETE FROM ekontrak.auditor_skauditor WHERE skid=? AND auditorid IN (" + join + ')', skauditor.skid);
                    }
                    flash.success("%s berhasil dihapus", "Paket");
                }
                if (hapusPaket != null) {
                    if (chkPaket != null) {
                        String join = StringUtils.join(chkPaket, ",");
                        Query.update("DELETE FROM ekontrak.paket_skauditor WHERE skid=? AND pkt_id in (" + join + ')', skauditor.skid);
                    }
                    flash.success("%s berhasil dihapus", "Paket");
                }
                SkAuditorNonLelang.simpanSk(skauditor, file, hapus);
                flash.success("%s berhasil disimpan", "Surat Tugas");
            } catch (Exception e) {
                flash.error("%s gagal disimpan", "Surat Tugas");
                Logger.error(e, "Surat Tugas gagal disimpan");
            }
        }
        editSkNonLelang(skauditor.skid);
    }

    @AllowAccess({Group.ADM_PPE})
    public static void daftarAuditorNonLelang(Long id) {
        renderArgs.put("id", id);
        renderTemplate("admin/skauditor/auditor-nonlelang-daftar.html");
    }

    @AllowAccess({Group.ADM_PPE})
    public static void simpanPilihAuditorNonLelang(Long id, Long[] chkAuditor) {
        // cek otentikasi
        if (chkAuditor != null) {
            Auditor.createPilihAuditorNonLelang(id, chkAuditor);
            flash.success("Auditor telah ditambahkan");
        }
        editSkNonLelang(id);
    }

    @AllowAccess({Group.ADM_PPE})
    public static void pilihPaketNonLelang(Long id, String instansiId, Long satkerId, Integer tahun) {
        renderArgs.put("id", id);
        renderArgs.put("instansiId", instansiId);
        renderArgs.put("satkerId", satkerId);
        renderArgs.put("tahun", tahun);
        renderArgs.put("skauditor", SkAuditorNonLelang.findById(id));
        renderArgs.put("tahunList", Anggaran.listTahunAnggaran());
        renderTemplate("admin/skauditor/auditor-nonlelang-pilih-paket.html");
    }

    public static void simpanPilihPaketNonLelang(Long skId, Long satkerId, Integer tahun, Long[] chkPaket) {
        if (chkPaket != null) {
            for (Long lelangId : chkPaket) {
                Pl_seleksi pl = Pl_seleksi.findById(lelangId);
                PaketSkAuditorNonLelang temp = PaketSkAuditorNonLelang.find("pkt_id=? and skid=?", pl.pkt_id, skId).first();
                if (temp != null) {
                    Paket_pl paket = Paket_pl.findById(temp.pkt_id);
                    flash.error("Paket %s sudah dipilih", paket.pkt_nama);
                    editSkNonLelang(skId);
                }
                SkAuditorNonLelang.createPaketAuditor(skId, pl.pkt_id);
            }
            flash.success("Paket telah ditambahkan");
            editSkNonLelang(skId);
        }
    }

    @AllowAccess({Group.ADM_PPE})
    public static void viewSkNonLelang(Long id) {
        if (id != null) { // tambah skauditor baru
            SkAuditorNonLelang ska = SkAuditorNonLelang.findById(id);
            List<AuditorSkAuditorNonLelang> auditor_skauditor = AuditorSkAuditorNonLelang.find("skid=?", ska.skid).fetch();
            List<PaketSkAuditorNonLelang> paket_skauditor = PaketSkAuditorNonLelang.find("skid=?", ska.skid).fetch();
            renderArgs.put("ska", ska);
            renderArgs.put("auditor_skauditor", auditor_skauditor);
            renderArgs.put("paket_skauditor", paket_skauditor);
        }
        renderTemplate("admin/skauditor/skauditor-nonlelang-view.html");
    }

}
