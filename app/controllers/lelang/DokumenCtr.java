package controllers.lelang;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DateBinder;
import ext.DatetimeBinder;
import ext.PdfType;
import models.agency.DaftarKuantitas;
import models.agency.DokPersiapan;
import models.agency.Paket;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.TempFileManager;
import models.lelang.*;
import models.lelang.Checklist_master.JenisChecklist;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.LocalDate;
import play.Logger;
import play.cache.Cache;
import play.cache.CacheFor;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.mvc.Router;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;

import java.io.File;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author iwawiwi, arief ardiyansah
 * Controller untuk dokumen SBD
 */
//Export ke PDF perlu direview lagi karena memerlukan waktu komputasi 10x lipat daripada HTML
/**
 * Hasil JMeter pada Laptop dg spek - memory 4GB - intel 4 core 2,5GB - Windows7
 * 64 bit Data - Jumlah sample 150 request - URL:
 * /spse4/dokumen/673999/pengadaan ---------------------------------------------
 * Request/sec Size PDF 1.7 96Kb HTML 13.0 1.8Mb
 *
 * @author andik
 *
 */
public class DokumenCtr extends BasicCtr {

	//tambahan untuk evaluasi teknis
	public static final Template TEMPLATE_EVALUASI_TEKNIS = TemplateLoader.load("/lelang/dokumen/template/evaluasiTeknis.html");
	public static final Template TEMPLATE_DOK_KUALIFIKASI= TemplateLoader.load("/lelang/dokumen/template/dok-kualifikasi.html");
	public static final Template TEMPLATE_DOK_PEMILIHAN = TemplateLoader.load("/lelang/dokumen/template/dok-pemilihan.html");
	public static final Template TEMPLATE_DOK_PENGADAAN = TemplateLoader.load("/lelang/dokumen/template/dok-pengadaan.html");
	public static final Template TEMPLATE_DOK_PEENGADAAN_EXPRESS = TemplateLoader.load("/lelang/dokumen/template/dok-pengadaan-express.html");
	/**
	 * preview dokumen pengadaan/pemilihan
	 *
	 * @param id
	 *            (kode dokumen) //CATATAN oleh [AY]: Sebaiknya 'content'
	 *            disimpan ke database AY telah melakukan uji coba di laptop
	 *            Lenovo 4-core, 4-GB memory. Maksimum 200 user simultan bisa
	 *            dilayani oleh method ini; yaitu ketika velocity generate
	 *            real-time
	 *
	 *            Ketika diberi @CacheFor("1mn") maka bisa menerima hingga 2750
	 *            request (generate hanya 1 kali lalu di-cache)
	 * */
	@AllowAccess({ Group.PANITIA, Group.AUDITOR, Group.REKANAN, Group.PPK })
	@CacheFor("15s")
	public static void preview(Long id, Integer versi) {
		if (versi == null)
			versi = Integer.valueOf(1);
		Dok_lelang dok_lelang = Dok_lelang.findById(id);
		Lelang_seleksi lelang = Lelang_seleksi.findById(dok_lelang.lls_id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		renderArgs.put("dok_lelang", dok_lelang);
		Dok_lelang_content content = Dok_lelang_content.findBy(dok_lelang.dll_id, versi);
		renderArgs.put("prakualifikasi", lelang.isPrakualifikasi());
		renderArgs.put("content", content != null ? content.dll_content_html : "");
		renderArgs.put("dokContent", content);
		renderArgs.put("pemilihan", lelang.getPemilihan());
		renderArgs.put("is43", lelang.isLelangV43());
		renderTemplate("lelang/dokumen/dok_lelang.html");
	}

	@AllowAccess({Group.PPK})
	public static void hapusSpek(Long id, Integer versi) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
		Map<String, Object> result = new HashMap<String, Object>(1);
		if(dok_lelang.isEditable(lelang, newDate())) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_spek == null) {
                List<BlobTable> blobList = dok_lelang_content.getDokSpek();
                blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                try {
                    dok_lelang_content.dll_spek = dok_lelang_content.simpanBlobSpek(blobList);
                    result.put("success", true);
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                } catch (Exception e) {
                    Logger.error(e, "File Anda tidak dapat dihapus");
                    result.put("success", false);
                    result.put("message", "File Anda tidak dapat dihapus");
                }
            }  else {
                BlobTable blob = BlobTable.findById(dok_lelang_content.dll_spek, versi);
                if (blob != null)
                    blob.delete();
                dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
            }
        }else {
			result.put("success",false);
			result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void hapusSpekPpk(Long id, Integer versi) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapan dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null) {
			if(dokPersiapan.isEditable(paket, newDate())){
				if(dokPersiapan.isAdendum() && dokPersiapan.dp_spek == null){
					List<BlobTable> blobList = dokPersiapan.getDokSpek();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));

					try{
						dokPersiapan.dp_spek = dokPersiapan.simpanBlobSpek(blobList);
						dokPersiapan.dp_modified = true;
						dokPersiapan.save();
						if (paket.ukpbj_id != null || paket.isFlag42()) {
							dokPersiapan.transferDokPersiapanToDokLelang();
						}
						result.put("success",true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success",false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else {
					if (dokPersiapan.dp_spek != null) {
						BlobTable blob = BlobTable.findById(dokPersiapan.dp_spek, versi);
						if (blob != null) {
							blob.delete();
						}
						if (CollectionUtils.isEmpty(dokPersiapan.getDokSpek())) {
							dokPersiapan.dp_spek = null;
						}
					}
					dokPersiapan.dp_modified = true;
					dokPersiapan.save();
					if (paket.ukpbj_id != null || paket.isFlag42()) {
						dokPersiapan.transferDokPersiapanToDokLelang();
					}
					result.put("success",true);
				}
			}else {
				result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
			}
		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PANITIA, Group.PPK})
	public static void hapusLainnya(Long id, Integer versi) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Map<String, Object> result = new HashMap<String, Object>(1);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
		if(dok_lelang.isEditable(lelang, newDate())) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);;
			if (dok_lelang_content != null) {
				if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_lainnya == null) {
					Dok_lelang_content dok_lelang_content_prev = Dok_lelang_content.findBy(dok_lelang.dll_id, dok_lelang_content.dll_versi - 1);
					List<BlobTable> blobList = dok_lelang_content_prev.getDokLainnya();
					if(!CollectionUtils.isEmpty(blobList))
						blobList.removeIf(blob -> versi.equals(blob.blb_versi));
					try {
						dok_lelang_content.dll_lainnya = dok_lelang_content.simpanBlobSpek(blobList);
						result.put("success", true);
                        dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
					} catch (Exception e) {
						Logger.error(e, "File Anda tidak dapat dihapus");
						result.put("success", false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				} else {
					BlobTable blob = BlobTable.findById(dok_lelang_content.dll_lainnya, versi);
					if (blob != null)
						blob.delete();
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
				}
			}
		}else {
			result.put("success", false);
			result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK})
	public static void hapusLainnyaPpk(Long id, Integer versi) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);

		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapan dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null) {
			if(dokPersiapan.isEditable(paket, newDate())){
				if(dokPersiapan.isAdendum() && dokPersiapan.dp_lainnya == null){
					List<BlobTable> blobList = dokPersiapan.getDokLainnya();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));
					try{
						dokPersiapan.dp_lainnya = dokPersiapan.simpanBlobSpek(blobList);
						dokPersiapan.dp_modified = true;
						dokPersiapan.save();
						if (paket.ukpbj_id != null || paket.isFlag42()) {
							dokPersiapan.transferDokPersiapanToDokLelang();
						}
						result.put("success",true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success",false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else {
					if (dokPersiapan.dp_lainnya != null) {
						BlobTable blob = BlobTable.findById(dokPersiapan.dp_lainnya, versi);
						if (blob != null) {
							blob.delete();
						}
						if (CollectionUtils.isEmpty(dokPersiapan.getDokLainnya())) {
							dokPersiapan.dp_lainnya = null;
						}
					}
					dokPersiapan.dp_modified = true;
					dokPersiapan.save();

					if (paket.ukpbj_id != null || paket.isFlag42()) {
						dokPersiapan.transferDokPersiapanToDokLelang();
					}
					result.put("success",true);
				}
			}else {
				result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
			}
		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PANITIA })
	public static void editCopy(Long idDest, Long idSource) {
		Lelang_seleksi lelangDest = Lelang_seleksi.findById(idDest);
		otorisasiDataLelang(lelangDest); // check otorisasi data lelang
		StringBuilder message = new StringBuilder("Copy Dokumen Pemilihan : </br><ul>");
		// dest
		Dok_lelang dok_lelangDest = Dok_lelang.findNCreateBy(lelangDest.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		Dok_lelang_content dok_lelang_contentDest = Dok_lelang_content.findNCreateBy(dok_lelangDest.dll_id, lelangDest);
		// source
		Lelang_seleksi lelangSource = Lelang_seleksi.findById(idSource);
		Dok_lelang dok_lelangSource = Dok_lelang.findBy(lelangSource.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		MetodePemilihan mtd_pemilihan = MetodePemilihan.findById(lelangDest.mtd_pemilihan);
		if (dok_lelangSource != null) {
			Dok_lelang_content dok_lelang_contentSource = Dok_lelang_content.findBy(dok_lelangSource.dll_id);

			// copy checklist persyaratan
			Checklist.copyChecklist(dok_lelangSource.dll_id, dok_lelangDest.dll_id);
			// copy content SBD
			Paket paket = lelangDest.getPaket();
//			Panitia panitia = Panitia.findById(paket.pnt_id);
			dok_lelang_contentDest.dll_sbd_versi = dok_lelang_contentSource.dll_sbd_versi;
			dok_lelang_contentDest.dll_versi = 1;
			dok_lelang_contentDest.ldpcontent = dok_lelangSource.getLdpContent(); // LDP yang berlaku
			dok_lelang_contentDest.sskk_content = dok_lelangSource.getSskkContent(); // SSKK yang berlaku
			if (dok_lelangSource.getRincianHPS() != null && dok_lelangSource.getRincianHPS().total <= lelangDest.getPaket().pkt_pagu) {
				dok_lelang_contentDest.dkh = dok_lelangSource.getRincianHPS(); // Rincian HPS yang berlaku
				paket.pkt_hps = dok_lelang_contentDest.dkh.total;
				paket.save();				
				message.append("<li>Rincian HPS tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>");
			} else {
				dok_lelang_contentDest.dkh = null;
				message.append("<li>Rincian HPS tidak tercopy, kosong atau melebihi pagu <span class='glyphicon glyphicon-remove-circle' ></span></li>");
			}

			dok_lelang_contentDest.dll_modified = true;
			dok_lelang_contentDest.save();
			if (!CommonUtil.isEmpty(dok_lelang_contentDest.dll_ldp)) {
				message.append("<li>Dokumen LDP tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>");
			} else {
				message.append("<li>Dokumen LDP tidak tercopy (kosong) <span class='glyphicon glyphicon-remove-circle' ></span> </li>");
			}
			if (!CommonUtil.isEmpty(dok_lelang_contentDest.dll_sskk)) {
				message.append("<li>Rincian SSKK tercopy <span class='glyphicon glyphicon-ok-circle'></span> </li>");
			} else {
				message.append("<li>Dokumen SSKK tidak tercopy (kosong) <span class='glyphicon glyphicon-remove-circle' ></span> </li>");
			}
			List<Checklist> ijinList, syaratList;
			// simpan LDK
			if (lelangDest.isPrakualifikasi()) {
				Dok_lelang dok_kualifikasiSource = Dok_lelang.findBy(lelangSource.lls_id,JenisDokLelang.DOKUMEN_LELANG_PRA);
				Dok_lelang dok_kualifikasiDest = Dok_lelang.findNCreateBy(lelangDest.lls_id,JenisDokLelang.DOKUMEN_LELANG_PRA);
				Checklist.copyChecklist(dok_kualifikasiSource.dll_id, dok_kualifikasiDest.dll_id);
				Dok_lelang_content dok_kualifikasi_contentDest = Dok_lelang_content.findNCreateBy(dok_kualifikasiDest.dll_id, lelangDest);
				ijinList = dok_kualifikasiDest.getSyaratIjinUsaha();
				syaratList = dok_kualifikasiDest.getSyaratKualifikasi();
				dok_kualifikasiDest.dll_nama_dokumen = String.format("Dok Kualifikasi [%s] - %s.pdf", lelangDest.lls_id,StringUtils.abbreviate(lelangDest.getNamaPaket(), 200));
				dok_kualifikasi_contentDest.dll_modified = true;
				dok_kualifikasi_contentDest.save();
				dok_kualifikasiDest.save();
			} else {
				ijinList = dok_lelangDest.getSyaratIjinUsaha();
				syaratList = dok_lelangDest.getSyaratKualifikasi();
			}
			if (!mtd_pemilihan.isLelangExpress()) {
				if (!CommonUtil.isEmpty(ijinList) && !CommonUtil.isEmpty(syaratList)) {
					message.append("<li>Dokumen LDK ditemukan dan disimpan <span class='glyphicon glyphicon-ok-circle'></span> </li>");
				} else {
					message.append("<li>Dokumen LDK tidak ditemukan dan tidak disimpan <span class='glyphicon glyphicon-remove-circle'></span> </li>");
				}
			}

			if (!mtd_pemilihan.isLelangExpress()) {
				// simpan persyaratan teknis
				syaratList = dok_lelangDest.getSyaratTeknis();
				if (!CommonUtil.isEmpty(syaratList)) {
					message.append("<li>Dokumen Syarat Teknis ditemukan dan disimpan <span class='glyphicon glyphicon-ok-circle'></span></li>");
				} else {
					message.append("<li>Ceklist Syarat Teknis tidak ditemukan dan tidak disimpan <span class='glyphicon glyphicon-remove-circle'></span> </li>");
				}
			}

			if (!mtd_pemilihan.isLelangExpress()) {
				// simpan persyaratan admin
				syaratList = dok_lelangDest.getSyaratAdministrasi();
				if (!CommonUtil.isEmpty(syaratList)) {
					message.append("<li>Ceklist Syarat Admin ditemukan dan disimpan <span class='glyphicon glyphicon-ok-circle'></span> </li>");
				} else {
					message.append("<li>Ceklist Syarat Admin tidak ditemukan dan tidak disimpan <span class='glyphicon glyphicon-remove-circle'></span> </li>");
				}
				// simpan persyaratan harga
				if (lelangSource.getKategori().isKonstruksi()) {
					syaratList = dok_lelangDest.getSyaratHarga();
					if (CommonUtil.isEmpty(syaratList)) {
						message.append("<li>Dokumen Syarat Harga tidak ditemukan(Konstruksi) dan tidak disimpan <span class='glyphicon glyphicon-remove-circle'></span> </li>");
					} else {
						message.append("<li>Dokumen Syarat Harga ditemukan(Konstruksi) dan disimpan <span class='glyphicon glyphicon-ok-circle'></span> </li>");
					}
				}
			}
		} else {
			message.append("<li>Dokumen lelang sumber tidak ditemukan <span class='glyphicon glyphicon-remove-circle'></span></li>");
		}
		message.append("</ul>");
		flash.success(message.toString());
		LelangCtr.edit(idDest);
	}

	/**
	 * * Cetak Dokumen Pengadaan atau Pemilihan
	 * @param id
     */
	@AllowAccess({Group.PANITIA})
	public static void cetak(Long id, String nomorSDP, @As(binder= DatetimeBinder.class) Date tglSDP) throws Exception {
		checkAuthenticity();
		Dok_lelang dok_lelang = Dok_lelang.findById(id);
		Lelang_seleksi lelang = Lelang_seleksi.findById(dok_lelang.lls_id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		if(lelang.lls_status.isDraft()) {
			// cek kelengkapan dokumen dilakukan saat lelang masih draft
			boolean express  = lelang.getPemilihan().isLelangExpress();
			Kategori kategori = lelang.getKategori();
			StringBuilder complete = new StringBuilder();
			if (dok_lelang != null && dok_lelang.dll_id != null) {
				Dok_lelang_content dok_ll_con = Dok_lelang_content.findBy(dok_lelang.dll_id);
				if(!express && (lelang.isPascakualifikasi() || dok_lelang.dll_jenis.isKualifikasi() || dok_lelang.dll_jenis.isAdendumPra())) {
					List<Checklist> ijinList = dok_lelang.getSyaratIjinUsaha();
					List<Checklist> syaratList = dok_lelang.getSyaratKualifikasi();
					if (CommonUtil.isEmpty(ijinList)) // empty ijin usaha
						complete.append("- Persyaratan Kualifikasi (Izin Usaha).\n ");
					if (CommonUtil.isEmpty(syaratList))
						complete.append("- Persyaratan Kualifikasi (Syarat).\n ");
				}
				if (dok_lelang.dll_jenis.isDokLelang() || dok_lelang.dll_jenis.isAdendum()) {
					if(!express) {
						if (CommonUtil.isEmpty(dok_lelang.getSyaratTeknis()))
							complete.append("- Persyaratan Dokumen Teknis.\n ");
						if (kategori.isKonstruksi() && CommonUtil.isEmpty(dok_lelang.getSyaratHarga()))
							complete.append("- Persyaratan Dokumen Harga.\n ");
					}
					if(CommonUtil.isEmpty(dok_ll_con.dll_ldp))
						complete.append("- Lembar Data Pemilihan.\n ");
					if(CommonUtil.isEmpty(dok_ll_con.dll_dkh))
						complete.append("- Rincian HPS.\n ");
					if(CommonUtil.isEmpty(dok_ll_con.dll_sskk))
						complete.append("- Syarat-Syarat Khusus Kontrak (SSKK).\n ");
					if(dok_ll_con.dll_spek == null)
						complete.append("- Spesifikasi Teknis dan Gambar.\n ");
				}
			}
			if (complete.length() > 0)
				flash.error("Kelengkapan Yang Belum Disimpan: " + complete);
		}
		// jika ada error  tidak perlu disimpan
		if(!flash.contains("error")){
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			if(dok_lelang_content == null)
				return;
			else if(dok_lelang_content.dll_modified) {
				dok_lelang_content.dll_nomorSDP = nomorSDP;
				dok_lelang_content.dll_tglSDP = tglSDP;

				StopWatch sw = new StopWatch();
				sw.start();
				Template template = null;
				if(dok_lelang.dll_jenis.isKualifikasi()) {
					template = TEMPLATE_DOK_KUALIFIKASI;
				}else { // untuk dokumen pengadaan/pemilihan
					template = lelang.isPrakualifikasi() ? TEMPLATE_DOK_PEMILIHAN : TEMPLATE_DOK_PENGADAAN;
					if (lelang.getPemilihan().isLelangExpress())
						template = TEMPLATE_DOK_PEENGADAAN_EXPRESS;
				}
				String content = Dok_lelang.cetak_content(lelang, dok_lelang, dok_lelang_content, template);
				if (!StringUtils.isEmpty(content)) {
					dok_lelang_content.dll_content_html = content.replace("<br>", "<br/>");
					InputStream dok = HtmlUtil.generatePDF(template.name+"-"+id, "#{extends 'borderTemplate.html' /} "+dok_lelang_content.dll_content_html);
					// simpan ke dok_lelang
					sw.stop();
					if (dok == null) {
						dok = IOUtils.toInputStream(dok_lelang_content.dll_content_html, "UTF-8");
						File file = TempFileManager.createFileInTemporaryFolder("InvalidDocLelangTemplate-lls_id#" + lelang.lls_id + ".html");
						FileUtils.copyInputStreamToFile(dok, file);
						Logger.error("Gagal generate PDF untuk lls_id %s. Template dokumen yang error disimpan di: %s", lelang.lls_id, file);
					} else {
						Logger.debug("Generate PDF document: %s, , duration: %s", dok_lelang.dll_nama_dokumen, sw);
					}
					dok_lelang.simpanCetak(lelang, dok_lelang_content, dok);
				}	

				// kirim email notifikasi jika adendum
				if(dok_lelang_content.dll_versi > 1){
					TahapStarted tahapStarted = lelang.getTahapStarted();
					List<Peserta> pesertaList = lelang.getPesertaList();
					if (lelang.isPrakualifikasi()) {
						Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
						boolean allow_pengumuman_pra = pembuktian != null && pembuktian.eva_status.isSelesai() && tahapStarted.isPengumumanPemenangPra();
						if (allow_pengumuman_pra) {
							// hanya kirim ke pemenang prakualifikasi
							pesertaList = Nilai_evaluasi.findPemenangPrakualifikasi(lelang.lls_id);
						}
					}

					if(pesertaList != null) {
						try {
							MailQueueDao.kirimNotifikasiAdendum(lelang, pesertaList);
						} catch (Exception ex) {
							Logger.error("Gagal mengirim notifikasi adendum untuk lls_id %s.", lelang.lls_id);
						}
					}
				}
			}
		}
		if (lelang.lls_status.isDraft())
			LelangCtr.edit(dok_lelang.lls_id);
		else
			LelangCtr.view(dok_lelang.lls_id);
	}

	/**form nomor SDP
	 * @param id
	*/
	@AllowAccess({Group.PANITIA})
	public static void nomorSDP(Long id){
		Dok_lelang dok_lelang = Dok_lelang.findById(id);
		otorisasiDataLelang(dok_lelang.lls_id);
		renderArgs.put("id", id);
		renderArgs.put("dok_lelang", dok_lelang);
		renderArgs.put("dok_lelang_content", Dok_lelang_content.findBy(dok_lelang.dll_id));
		renderArgs.put("today", newDate());
		renderTemplate("lelang/dokumen/form-nomorSDP.html");
	}
	
	/**
	 * form LDK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void ldk(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Kategori kategori = lelang.getKategori();
		Kualifikasi kualifikasi = lelang.getPaket().getKualifikasi();
		renderArgs.put("lelang", lelang);
		renderArgs.put("kategori", kategori);
		renderArgs.put("kualifikasi", lelang.getPaket().getKualifikasi());
		JenisDokLelang jenis = lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA:JenisDokLelang.DOKUMEN_LELANG;
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, jenis);
		renderArgs.put("dok_lelang", dok_lelang);
		renderArgs.put("editable", dok_lelang.isEditable(lelang, newDate()));
		boolean isSyaratKualifikasiBaru = dok_lelang.isSyaratKualifikasiBaru();
		renderArgs.put("isNew", Checklist.isChecklistEmpty(dok_lelang.dll_id, JenisChecklist.CHECKLIST_LDK)); ///check apakah sudah terdapat checklist di tender ini atau tidak
		if(!kategori.isKonsultansiPerorangan()) {
			List<Checklist> ijinList = isSyaratKualifikasiBaru ? 
					(flash.get("flashError") != null ? Cache.get("ijinList_"+dok_lelang.dll_id, List.class) : Checklist.getListIjinUsahaBaru(dok_lelang))
					: Checklist.getListIjinUsaha(dok_lelang);
			if (flash.get("flashError") != null)
				Cache.safeDelete("ijinList_"+dok_lelang.dll_id);
			if (CommonUtil.isEmpty(ijinList)) { // dokumen lelang belum ada
				ijinList = new ArrayList<Checklist>(1);
				Checklist checklist = new Checklist();
				checklist.ckm_id = isSyaratKualifikasiBaru ? 50 : 1;
				ijinList.add(checklist);
			}
			renderArgs.put("ijinList", ijinList);
		}
		if (isSyaratKualifikasiBaru) {
    	 		List<Checklist> syaratAdmin = new ArrayList<Checklist>();
    	 		List<Checklist> syaratTeknis = new ArrayList<Checklist>();
    	 		List<Checklist> syaratKeuangan = new ArrayList<Checklist>();    	 		
    	 		if (flash.get("flashError") != null) {
    	 			syaratAdmin = Cache.get("syaratAdmin_"+dok_lelang.dll_id, List.class);
        	 		syaratTeknis = Cache.get("syaratTeknis_"+dok_lelang.dll_id, List.class);
        	 		syaratKeuangan = Cache.get("syaratKeuangan_"+dok_lelang.dll_id, List.class);
        	 		Cache.safeDelete("syaratAdmin_"+dok_lelang.dll_id);
        	 		Cache.safeDelete("syaratTeknis_"+dok_lelang.dll_id);
        	 		Cache.safeDelete("syaratKeuangan_"+dok_lelang.dll_id);
        	 		renderArgs.put("isOk", false);
    	 		} else {
    	 			syaratAdmin = Checklist.getListSyaratKualifikasi(dok_lelang, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, lelang.lls_status.isDraft());
        	 		syaratTeknis = Checklist.getListSyaratKualifikasi(dok_lelang, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, lelang.lls_status.isDraft());
        	 		syaratKeuangan = Checklist.getListSyaratKualifikasi(dok_lelang, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, lelang.lls_status.isDraft());
        	 		renderArgs.put("isOk", true);
    	 		}
    	 		renderArgs.put("syaratAdmin", syaratAdmin);
			renderArgs.put("syaratTeknis", syaratTeknis);
			renderArgs.put("syaratKeuangan", syaratKeuangan);
			renderTemplate("lelang/dokumen/form-ldk-baru.html");
		} else {
			renderArgs.put("syaratList", Checklist.getListSyaratKualifikasi(dok_lelang, kategori));
			renderTemplate("lelang/dokumen/form-ldk.html");
		}
	}
	
	/**
	 * simpan Form LDK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void ldkSubmit(Long id, List<Checklist> checklist, List<Checklist> ijin) throws Exception {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		if (!CommonUtil.isEmpty(checklist)) {
			boolean valid = true;
			for (Checklist check : checklist) {
				if (check.isBankSupportSpec() && !check.isValidForPercentage()) {
					valid = false;
					break;
				}
			}
			if (!valid) {
				flash.error("Nominal dukungan keuangan minimal 10%% maksimal 100%%!");
				ldk(id);
			}
		}
		JenisDokLelang jenis = lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA:JenisDokLelang.DOKUMEN_LELANG;
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, jenis);
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		if(!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			Checklist.simpanChecklist(dok_lelang, ijin, checklist, params, Integer.valueOf(1), lelang.getPaket().getKualifikasi());
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateByForLdk(dok_lelang.dll_id, lelang.lls_status);
			dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
			flash.success("Persyaratan Kualifikasi berhasil tersimpan");
		}catch (Exception e) {
			flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
			Logger.error(e, "Persyaratan Kualifikasi gagal tersimpan. %s", e.getLocalizedMessage());
		}
		ldk(id);
	}

	@AllowAccess({Group.PANITIA})
	public static void ldkSubmitBaru(Long id, List<Checklist> ijin, List<Checklist> syaratAdmin, List<Checklist> syaratTeknis, List<Checklist> syaratKeuangan)  {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Kategori kategori = lelang.getKategori();
        JenisDokLelang jenis = lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA:JenisDokLelang.DOKUMEN_LELANG;
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, jenis);
		Kualifikasi kualifikasi = lelang.getPaket().getKualifikasi();
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		if(!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			Checklist.simpanChecklist(dok_lelang, ijin, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, Integer.valueOf(1), kualifikasi);
			Checklist.simpanChecklist(dok_lelang, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, Integer.valueOf(1), kualifikasi);
			Checklist.simpanChecklist(dok_lelang, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, Integer.valueOf(1), kualifikasi);
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateByForLdk(dok_lelang.dll_id, lelang.lls_status);
			dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
			flash.success("Persyaratan Kualifikasi berhasil tersimpan");
		} catch (Exception e) {
			if(!kategori.isKonsultansiPerorangan())
				Cache.set("ijinList_"+dok_lelang.dll_id, Checklist.getListIjinUsahaBaruFlash(dok_lelang, ijin));
			Cache.set("syaratAdmin_"+dok_lelang.dll_id, Checklist.getListSyaratKualifikasiFlash(dok_lelang, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, kualifikasi));
			Cache.set("syaratTeknis_"+dok_lelang.dll_id, Checklist.getListSyaratKualifikasiFlash(dok_lelang, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, kualifikasi));
			Cache.set("syaratKeuangan_"+dok_lelang.dll_id, Checklist.getListSyaratKualifikasiFlash(dok_lelang, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, kualifikasi));
			flash.put("flashError", true);
			flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
			Logger.error(e, e.getMessage());
		}
		ldk(id);
	}

	/**
	 * form LDP
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void ldp(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Paket paket = Paket.findById(lelang.pkt_id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("paket", paket);
		renderArgs.put("kategori", lelang.getKategori());
		renderArgs.put("panitia", lelang.getPanitia());
		renderArgs.put("server", getRequestHost());
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("editable", dok_lelang.isEditable(lelang, newDate()));
		renderArgs.put("evaluasi",lelang.getEvaluasi());
		//list tenaga ahli
		String[][] tenagaAhli=null;
		LDPContent ldpcontent = null;
		if(dok_lelang != null) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			if (dok_lelang_content != null) {
				ldpcontent = dok_lelang_content.ldpcontent;
				renderArgs.put("adendum", dok_lelang_content.isAdendum());
			}
			if (ldpcontent == null && !lelang.lls_status.isDraft())
				ldpcontent = dok_lelang.getLdpContent();
			if (ldpcontent != null && ldpcontent.bobotTenagaAhli != null) {
				tenagaAhli = new String[ldpcontent.bobotTenagaAhli.size()][2];
				for (int i = 0; i < ldpcontent.bobotTenagaAhli.size(); i++) {
					JsonObject jsonObject = (JsonObject) ldpcontent.bobotTenagaAhli.get(i);
					tenagaAhli[i][0] = jsonObject.get("profesi").toString();
					tenagaAhli[i][1] = jsonObject.get("bobot").toString();
				}
			}
		}

		if(ldpcontent == null){
			ldpcontent = new LDPContent();
		}

		if (StringUtils.isEmpty(ldpcontent.evaluasiTeknis)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("kategori", lelang.getKategori());
			ldpcontent.evaluasiTeknis = TEMPLATE_EVALUASI_TEKNIS.render(params);
		}
		renderArgs.put("ldpcontent", ldpcontent);
		renderArgs.put("tenagaAhli", tenagaAhli);
		renderArgs.put("backUrl", getPaketLelangBackUrl(tahapStarted, lelang.pkt_id));
		renderTemplate("lelang/dokumen/form-ldp.html");
	}
	/**
	 * simpan Form LDP
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void ldpSubmit(Long id, @Valid LDPContent ldpcontent, String[][] tenagaAhli) {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			if(!CommonUtil.isEmpty(tenagaAhli)) {
				JsonArray jsonArray = new JsonArray();

				for(int i = 0; i < tenagaAhli.length; i++){
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("profesi", tenagaAhli[i][0]);
					jsonObject.addProperty("bobot", tenagaAhli[i][1]);
					jsonArray.add(jsonObject);
				}
				ldpcontent.bobotTenagaAhli = jsonArray;
			}
			Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
			boolean editable = dok_lelang.isEditable(lelang, newDate());
			if (!editable)
				forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
			if (ldpcontent != null) {
				try {
					dok_lelang_content.ldpcontent = ldpcontent; // simpan LDP
					if (lelang.getEvaluasi().isNilai()) {
						lelang.lls_bobot_biaya = ldpcontent.bobot_harga;
						lelang.lls_bobot_teknis = ldpcontent.bobot_teknis;
						lelang.save();
					}
					dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
					flash.success("Lembar Data Pemilihan berhasil tersimpan");
				} catch (Exception e) {
					Logger.error(e, "Gagal simpan LDK");
					flash.error("Lembar Data Pemilihan gagal tersimpan");
				}
			}
		}
		ldp(id);
	}

	/**
	 * form Checklist Persyaratan
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void checklist(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		TahapStarted tahapStarted = lelang.getTahapStarted();
		if(lelang.getPemilihan().isLelangExpress()) {
			forbidden("Tender Cepat Dan Seleksi Cepat Tidak Perlu Persyaratan Dokumen");
		}
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("lelang", lelang);
		renderArgs.put("dok_lelang", dok_lelang);
		Kategori kategori = lelang.getKategori();
		renderArgs.put("kategori", kategori);
		List<Checklist> syaratAdmin = Checklist
				.getListSyarat(dok_lelang, JenisChecklist.CHECKLIST_ADMINISTRASI, kategori, lelang.lls_status.isDraft())
				.stream()
				.sorted(Comparator.comparing(Checklist::getSortingRule))
				.collect(Collectors.toList());
		renderArgs.put("syaratAdmin",syaratAdmin);
		final boolean isConsultant = kategori.isConsultant();
		List<Checklist> syaratTeknis = Checklist
				.getListSyarat(dok_lelang,JenisChecklist.CHECKLIST_TEKNIS, kategori, lelang.lls_status.isDraft())
				.stream()
				.filter(c -> isConsultant == c.getChecklist_master().isKonsultansiPerorangan() || c.getChecklist_master().isSyaratLain())
				.sorted(Comparator.comparing(Checklist::getSortingRule))
				.collect(Collectors.toList());
		renderArgs.put("syaratTeknis", syaratTeknis);
//		if (kategori.isKonstruksi()) {
			renderArgs.put("syaratHarga", Checklist.getListSyarat(dok_lelang, JenisChecklist.CHECKLIST_HARGA, kategori, lelang.lls_status.isDraft()));
//		}
		if (dok_lelang != null) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			renderArgs.put("editable", dok_lelang.isEditable(lelang, newDate()) && !dok_lelang_content.isAdendum());
			if (dok_lelang_content != null) {
				renderArgs.put("adendum", dok_lelang_content.isAdendum());
			}
		}
		renderTemplate("lelang/dokumen/form-checklist-penawaran.html");
	}
	/**
	 * Simpan Form Checklist Persyaratan
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void checklistSubmit(Long id,  List<Checklist> syarat, List<Checklist> syaratAdmin, List<Checklist> syaratHarga) {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		if(lelang.getPemilihan().isLelangExpress()) {
			forbidden("Tender Cepat Dan Seleksi Cepat Tidak Perlu Persyaratan Dokumen");
		}
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		if (!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			boolean emptyTeknis = true;
			boolean emptyHarga = true;
			// do validation first
			if (syarat != null) {
				for (int i = 0; i < syarat.size(); i++) {
					if (syarat.get(i).ckm_id != null) {
						emptyTeknis= false;
						validation.valid(syarat.get(i));
					}
				}
			}
			if (syaratHarga != null) {
				for (int i = 0; i < syaratHarga.size(); i++) {
					if (syaratHarga.get(i).ckm_id != null) {
						emptyHarga = false;
						validation.valid(syaratHarga.get(i));
					}
				}
			}

			if(!validation.hasErrors()) {
				if (emptyTeknis)
					flash.error("Persyaratan Teknis pada Persyaratan Dokumen Penawaran wajib diisi!");
				else if (lelang.getKategori().isKonstruksi() && emptyHarga)
					flash.error("Persyaratan Harga pada Persyaratan Dokumen Penawaran wajib diisi!");
				else {
					// do the save
					if (!emptyTeknis)
						Checklist.simpanChecklist(dok_lelang, syarat, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS, Integer.valueOf(1)); // simpan checklist penawaran

					if (syaratAdmin != null)
						Checklist.simpanChecklist(dok_lelang, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI, Integer.valueOf(1)); // simpan checklist penawaran

					Checklist.simpanChecklist(dok_lelang, syaratHarga, Checklist_master.JenisChecklist.CHECKLIST_HARGA, Integer.valueOf(1)); // simpan checklist penawaran

					Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateByForSyarat(dok_lelang.dll_id, lelang);
					dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);

					flash.success("Persyaratan Dokumen berhasil tersimpan");
				}
			} else {
				StringBuilder sb = new StringBuilder();
				for(play.data.validation.Error err:validation.errors()) {
					if(!StringUtils.isEmpty(err.getKey())) {
						sb.append("<br/>").append(err.message());
					}
				}
				flash.error("Persyaratan Dokumen gagal tersimpan:"+ sb);
			}
		}catch (Exception e) {
			Logger.error(e, "Gagal simpan Persyaratan Dokumen");
			flash.error("Persyaratan Dokumen gagal tersimpan");
		}
		checklist(id);
	}

	@AllowAccess({Group.PPK, Group.PANITIA, Group.KUPPBJ, Group.AUDITOR})
	public static void hps(Long id){
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		boolean fixed = true;
		String data = "[]";
		DaftarKuantitas dk;
		boolean editable;
		Active_user activeUser = Active_user.current();
		Dok_lelang dokLelang = lelang.getDokumenLelang();
		Dok_lelang_content dokLelangContent = dokLelang.getDokLelangContent();
		dk = dokLelangContent.dkh;
		if(dk == null) {
			dk = dokLelang.getRincianHPS();
		}
		editable = lelang.getTahapStarted().isAllowAdendum() && activeUser.isPpk();
		if(dk != null){
			fixed = dk.fixed;
			if (dk.items != null) {
				data = CommonUtil.toJson(dk.items);
			}
			renderArgs.put("total", dk.total);
		}
		renderArgs.put("data", data);
		renderArgs.put("editable", editable);
		renderArgs.put("fixed", fixed);
		renderArgs.put("enableViewHps", lelang.getPaket().isEnableViewHps());
		renderArgs.put("draft", lelang.lls_status.isDraft());
		renderArgs.put("backUrl", activeUser.isPanitia() ? Router.reverse("lelang.LelangCtr.edit").add("id",id).url:Router.reverse("lelang.LelangCtr.view").add("id",id).url);
		renderTemplate("lelang/dokumen/form-hps.html");
	}

	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void hpsPpk(Long id){
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Active_user activeUser = Active_user.current();
		boolean fixed = true;
		String data = "[]";
		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket);
		DaftarKuantitas dk = dokPersiapan.dkh;
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		if(dk != null){
			fixed = dk.fixed;
			if (dk.items != null) {
				data = CommonUtil.toJson(dk.items);
			}
			renderArgs.put("total", dk.total);
		}
		renderArgs.put("paket", paket);
		renderArgs.put("data", data);
		renderArgs.put("editable", editable);
		renderArgs.put("fixed", fixed);
		renderArgs.put("enableViewHps", paket.isEnableViewHps());
		renderTemplate("lelang/dokumen/form-hps-persiapan.html");
	}

	private static Router.ActionDefinition getPaketLelangBackUrl(TahapStarted tahapStarted, Long paketId) {
		if (tahapStarted.isAllowAdendum()) {
			return Router.reverse("lelang.LelangCtr.view").add("id", tahapStarted.lelangId);
		} else if (Active_user.current().isPpk()) {
			return Router.reverse("lelang.PaketCtr.edit").add("id", paketId).add("step", 2);
		}else if(Active_user.current().isPanitia() && !tahapStarted.isAllowAdendum()){
			return Router.reverse("lelang.LelangCtr.edit").add("id", tahapStarted.lelangId);
		}
		return null;
	}

	/**
	 * Simpan Form Rincian HPS
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void hpsSubmit(Long id, String data, boolean fixed, boolean enableViewHps) {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		Map<String, Object> result = new HashMap<>(1);
		if(!editable)
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		else {
			try {
				dok_lelang.simpanHps(lelang,  data, fixed, enableViewHps);
				result.put("result", "ok");
			} catch (Exception e) {
				Logger.error("Gagal Menyimpan HPS : %s", e.getLocalizedMessage());
				result.put("result", e.getLocalizedMessage());
			}
		}
		renderJSON(result);
	}

	@AllowAccess({Group.PPK})
	public static void hpsSubmitPpk(Long id, String data, boolean fixed, boolean enableViewHps){
		checkAuthenticity();

		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Map<String, Object> result = new HashMap<>(1);

		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket);
		if(!dokPersiapan.isEditable(paket, newDate())){
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        }else{
            try{
                dokPersiapan.simpanHps(paket, data, fixed, enableViewHps);
                result.put("result", "ok");
            }catch (Exception e){
                Logger.error("Gagal Menyimpan HPS : %s", e.getLocalizedMessage());
                result.put("result", e.getLocalizedMessage());
            }
        }

		renderJSON(result);
	}

	/**
	 * form Spek/KAK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA,Group.KUPPBJ, Group.PPK})
	public static void spek(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Kategori kategori = lelang.getKategori();
		renderArgs.put("lelang", lelang);
		renderArgs.put("kategori", kategori);
		Active_user activeUser = Active_user.current();
		Dok_lelang dokLelang = Dok_lelang.findBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("editable", lelang.getTahapStarted().isAllowAdendum() && activeUser.isPpk());
		if(dokLelang != null) {
			Dok_lelang_content dokLelangContent = Dok_lelang_content.findBy(dokLelang.dll_id);
			if(dokLelangContent != null) {
				renderArgs.put("spek",dokLelangContent.getDokSpek());
				renderArgs.put("adendum", dokLelangContent.isAdendum());
			}
		}
		renderArgs.put("isPanitia",activeUser.isPanitia());
		renderTemplate("lelang/dokumen/form-spek.html");
	}
	/**
	 * Simpan Form Spek/KAK
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void spekSubmit(Long id, File file) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Map<String, Object> result = new HashMap<>(1);
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id,	JenisDokLelang.DOKUMEN_LELANG);
		Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		if(!editable)
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		else {
			try {
				if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_spek == null) {
					List<BlobTable> blobTableList = dok_lelang_content.getDokSpek();
					dok_lelang_content.dll_spek = dok_lelang_content.simpanBlobSpek(blobTableList);
				}
				UploadInfo model = dok_lelang_content.simpanSpek(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload spek");
				result.put("success", false);
				result.put("result", "Kesalahan saat upload spek");
			}
		}		
		renderJSON(result);
	}

	/**
	 * form Spek/KAK untuk PPP,
	 * Terkait Dokumen Persiapan (@models.agency.DokPersiapan)
	 * adendum tidak dilakukan ppk di halaman
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void spekPpk(Long id) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		renderArgs.put("paket", paket);
		Active_user activeUser = Active_user.current();
		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket);
		List<BlobTable> spek = dokPersiapan.getDokSpek();
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		renderArgs.put("editable", editable);
		renderArgs.put("spek",spek);
		renderArgs.put("isFlagV3",paket.isFlag43());
		renderTemplate("lelang/dokumen/form-spek-persiapan.html");
	}

	/**
	 * Simpan Form Spek/KAK PPK
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void spekPpkSubmit(Long id, File file) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket); // check otorisasi data lelang
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapan dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null && dokPersiapan.isEditable(paket, newDate())) {
			try {
				UploadInfo model = dokPersiapan.simpanSpek(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload spek");
				result.put("result", "Kesalahan saat upload spek");
			}
		}else {
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}

	/**
	 * form SSKK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA, Group.PPK})
	public static void sskk(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("isConsultant", lelang.getKategori().isConsultant() || lelang.getKategori().isJkKonstruksi());
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
		Active_user activeUser = Active_user.current();
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		boolean isFlag43 = lelang.getPaket().isFlag43();
		if(isFlag43){
			editable = editable && activeUser.isPpk();
		}
		renderArgs.put("editable", editable);
		renderArgs.put("isFlagV3",isFlag43);
		renderArgs.put("isPanitia", activeUser.isPanitia());
		Kategori kategori = lelang.getKategori();
		renderArgs.put("kategori", kategori);
		SskkContent sskk_content = null;
		if(dok_lelang != null) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			if(dok_lelang_content != null) {
				renderArgs.put("adendum", dok_lelang_content.isAdendum());
				sskk_content = dok_lelang_content.sskk_content;
			}
			if(sskk_content == null && !lelang.lls_status.isDraft()) {
				sskk_content = dok_lelang.getSskkContent();
			}
		}
		if(sskk_content == null) {
			sskk_content = new SskkContent();
			sskk_content.kontrak_pembayaran = lelang.lls_kontrak_pembayaran;
		}
		renderArgs.put("backUrl", getPaketLelangBackUrl(tahapStarted, lelang.pkt_id));
		renderArgs.put("urlSubmit",Router.reverse("lelang.DokumenCtr.sskkSubmit").add("id", lelang.lls_id));
		renderArgs.put("sskk_content", sskk_content);
		if(lelang.getPemilihan().isLelangExpress())
			renderTemplate("lelang/dokumen/form-sskk-express.html");
		else
			renderTemplate("lelang/dokumen/form-sskk.html");
	}

	/**
	 * form SSKK PPK
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void sskkPpk(Long id) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Lelang_seleksi lelang = Lelang_seleksi.findByPaket(id);
		renderArgs.put("lelang", lelang);
		Kategori kategori = lelang.getKategori();
		renderArgs.put("kategori", kategori);
		renderArgs.put("isConsultant", kategori.isConsultant() || kategori.isJkKonstruksi());
		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(id);
		renderArgs.put("editable", dokPersiapan.isEditable(paket, newDate()) && Active_user.current().isPpk());
		renderArgs.put("isFlagV3",paket.isFlag43());
		renderArgs.put("isPanitia", Active_user.current().isPanitia());
		renderArgs.put("adendum", dokPersiapan.isAdendum());
		SskkContent sskkContent = dokPersiapan.sskkContent;
		if(sskkContent == null) {
			if(!lelang.lls_status.isDraft()){
				sskkContent = dokPersiapan.getOldSskkContent();
			}else {
				sskkContent = new SskkContent();
				sskkContent.kontrak_pembayaran = lelang.lls_kontrak_pembayaran;
			}
		}
		renderArgs.put("backUrl", Router.reverse("lelang.PaketCtr.edit").add("id", paket.pkt_id));
		renderArgs.put("urlSubmit",Router.reverse("lelang.DokumenCtr.sskkPpkSubmit").add("id", lelang.pkt_id));
		renderArgs.put("sskk_content", sskkContent);
		if(lelang.getPemilihan().isLelangExpress())
			renderTemplate("lelang/dokumen/form-sskk-express.html");
		else
			renderTemplate("lelang/dokumen/form-sskk.html");
	}
	
	@AllowAccess({Group.PANITIA,Group.KUPPBJ, Group.PPK})
	public static void docSskk(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Kategori kategori = lelang.getKategori();
		renderArgs.put("lelang", lelang);
		renderArgs.put("kategori", kategori);
		renderArgs.put("isConsultant", kategori.isConsultant() || kategori.isJkKonstruksi());
		Active_user activeUser = Active_user.current();
		Dok_lelang dokLelang = Dok_lelang.findBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("editable", tahapStarted.isAllowAdendum() && activeUser.isPpk());
		if(dokLelang != null) {
			Dok_lelang_content dokLelangContent = Dok_lelang_content.findBy(dokLelang.dll_id);
			if(dokLelangContent != null) {
				renderArgs.put("sskk", dokLelangContent.getDokSskk());
				renderArgs.put("adendum", dokLelangContent.isAdendum());
			}
		}
		renderTemplate("lelang/dokumen/form-upload-sskk.html");
	}
	
	/**
	 * form SSKK PPK
	 * untuk dokumen persiapan tender bukan buat adendum
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ, Group.PANITIA})
	public static void uploadSskk(Long id) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Active_user activeUser = Active_user.current();
		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket);
		List<BlobTable>	sskk = dokPersiapan.getDokSskkAttachment();
		boolean	editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		renderArgs.put("paket", paket);
		renderArgs.put("editable", editable);
		renderArgs.put("isFlagV3",paket.isFlag43());
		renderArgs.put("sskk",sskk);
		renderArgs.put("editable", editable);
		renderTemplate("lelang/dokumen/form-upload-sskk-persiapan.html");
	}

	@AllowAccess({Group.PPK})
	public static void uploadSskkSubmit(Long id, File file) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
        Map<String, Object> result = new HashMap<>(1);
        DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket.pkt_id);

		String fileName=file.getName().toUpperCase();
		Pattern findExtention = Pattern.compile("PDF");
		Matcher isExtExist = findExtention.matcher(fileName);
		if(!isExtExist.find()){
			result.put("success", false);
			result.put("message", fileName+" - File dengan ekstensi ini tidak diijinkan");
			renderJSON(result);
		}
        if (dokPersiapan != null && dokPersiapan.isEditable(paket, newDate())) {
            try {
                UploadInfo model = dokPersiapan.simpanSskkAttachment(file);
                if (model == null) {
                    result.put("success", false);
                    result.put("message", "File dengan nama yang sama telah ada di server!");
                    renderJSON(result);
                }
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, e.getLocalizedMessage());
                result.put("result", "Kesalahan saat upload sskk");
            }
        }else{
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
		renderJSON(result);
    }

	@AllowAccess({Group.PPK})
    public static void docSskkSubmit(Long id, File file){
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang);
        Map<String, Object> result = new HashMap<>(1);
        Dok_lelang dok_lelang = lelang.getDokumenLelang();
        if (dok_lelang != null && lelang.getTahapStarted().isAllowAdendum()) {
            try {
                Dok_lelang_content dok_lelang_content = dok_lelang.getDokLelangContent();
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_sskk_attachment == null) {
                    List<BlobTable> blobTableList = dok_lelang_content.getDokSskk();
                    dok_lelang_content.dll_sskk_attachment = dok_lelang_content.simpanBlobSpek(blobTableList);
                    dok_lelang_content.dll_modified = true;
                }
                UploadInfo model = dok_lelang_content.simpanSskkAttachment(file);
                if (model == null) {
                    result.put("success", false);
                    result.put("message", "File dengan nama yang sama telah ada di server!");
                    renderJSON(result);
                }
                List<UploadInfo> files = new ArrayList<UploadInfo>();
                files.add(model);
				dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, e.getLocalizedMessage());
                result.put("result", "Kesalahan saat upload sskk");
            }
        }else{
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
		renderJSON(result);
    }

	@AllowAccess({ Group.PPK })
	public static void hapusSskkPpk(Long id, Integer versi) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapan dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null) {
			if(dokPersiapan.isEditable(paket, newDate())){
				if(dokPersiapan.isAdendum() && dokPersiapan.dp_sskk_attachment == null){
					List<BlobTable> blobList = dokPersiapan.getDokSskkAttachment();
					blobList.removeIf(blob -> versi.equals(blob.blb_versi));

					try{
						dokPersiapan.dp_sskk_attachment = dokPersiapan.simpanBlobSpek(blobList);
						dokPersiapan.dp_modified = true;
						dokPersiapan.save();
						if (paket.ukpbj_id != null || paket.isFlag42()) {
							dokPersiapan.transferSskkToDokLelang();
						}
						result.put("success",true);
					}catch (Exception e){
						e.printStackTrace();
						result.put("success",false);
						result.put("message", "File Anda tidak dapat dihapus");
					}
				}else{
					BlobTable blob = BlobTable.findById(dokPersiapan.dp_sskk_attachment,versi);
					if (blob != null){
						blob.delete();
					}
					if (dokPersiapan.dp_sskk_attachment != null && dokPersiapan.getDokSskkAttachment().isEmpty()) {
						dokPersiapan.dp_sskk_attachment = null;
					}
					dokPersiapan.dp_modified = true;
					dokPersiapan.save();

					if (paket.ukpbj_id != null || paket.isFlag42()) {
						dokPersiapan.transferSskkToDokLelang();
					}

					result.put("success",true);
				}
			}else{
				result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
			}

		}else{
			result.put("message","File Anda tidak dapat dihapus");
		}

		renderJSON(result);

	}

	/**
	 * Simpan SSKK PPK
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void sskkPpkSubmit(Long id, @Valid  SskkContent sskk_content) {
		checkAuthenticity();
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Lelang_seleksi lelang = Lelang_seleksi.findByPaket(id);
		boolean valid = false;
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			sskkError(id,sskk_content);
		} else {
			DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(id);
			boolean editable = dokPersiapan.isEditable(paket, newDate());
			if (!editable)
				forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
			if (sskk_content != null) {
				Date now = newDate();
				LocalDate nowLd = LocalDate.fromDateFields(now);
				LocalDate kontrakMulaiLd = sskk_content.kontrak_mulai != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_mulai) : null;
				LocalDate kontrakAkhirLd = sskk_content.kontrak_akhir != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_akhir) : null;

				if (kontrakMulaiLd != null && kontrakMulaiLd.isBefore(nowLd)
						|| kontrakAkhirLd != null && kontrakAkhirLd.isBefore(nowLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku yang Anda masukkan sudah lewat");
				} else if (kontrakAkhirLd != null && kontrakMulaiLd != null
						&& kontrakAkhirLd.isBefore(kontrakMulaiLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku akhir kontrak mendahului awal kontrak");
				} else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.after(sskk_content.kontrak_akhir)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi setelah akhir kontrak");
				} else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi mendahului mulai kontrak");
				} else if (sskk_content.tgl_serah_terima != null && sskk_content.tgl_serah_terima.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Serah Terima Pekerjaan mendahului mulai kontrak");
				}else {// simpan SSKK
					dokPersiapan.sskkContent = sskk_content;
					try {
						dokPersiapan.save();
					}catch (Exception e){
						e.printStackTrace();
					}
					if(paket.ukpbj_id != null){
						try{
							dokPersiapan.transferSskkToDokLelang();
						}catch (Exception e){
							flash.error(e.getLocalizedMessage());
						}
					}

					lelang.lls_kontrak_pembayaran = sskk_content.kontrak_pembayaran;
					lelang.save();


					flash.success("Syarat-Syarat Khusus Kontrak berhasil tersimpan ");
					valid = true;
				}
			}
		}
		if(valid)
			sskkPpk(id);
		else
			sskkError(id,sskk_content);

	}


	/**
	 * form SSKK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA, Group.PPK})
	public static void sskkError(Long id,SskkContent sskk_content) {
		if(Active_user.current().isPanitia()){
			otorisasiDataLelang(id); // check otorisasi data lelang
		}else{
			Paket paket = Paket.findById(id);
			otorisasiDataPaket(paket);
			id = Lelang_seleksi.findByPaket(id).lls_id;
		}
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("editable", dok_lelang.isEditable(lelang, newDate()));
		renderArgs.put("kategori", lelang.getKategori());
		if(sskk_content == null) {
			sskk_content = new SskkContent();
			sskk_content.kontrak_pembayaran = lelang.lls_kontrak_pembayaran;
		}
		renderArgs.put("sskk_content", sskk_content);
		Router.ActionDefinition urlSubmit = Active_user.current().isPanitia() ?
				Router.reverse("lelang.DokumenCtr.sskkSubmit").add("id", lelang.lls_id) :
				Router.reverse("lelang.DokumenCtr.sskkPpkSubmit").add("id", lelang.pkt_id);
		renderArgs.put("urlSubmit",urlSubmit);
		if(lelang.getPemilihan().isLelangExpress())
			renderTemplate("lelang/dokumen/form-sskk-express.html");
		else
			renderTemplate("lelang/dokumen/form-sskk.html");
	}
	/**
	 * Simpan SSKK
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void sskkSubmit(Long id, @Valid  SskkContent sskk_content) {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		boolean valid = false;
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			sskkError(id,sskk_content);
		} else {
			Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
			boolean editable = dok_lelang.isEditable(lelang, newDate());
			if (!editable)
				forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
			if (sskk_content != null) {
				Date now = newDate();
				LocalDate nowLd = LocalDate.fromDateFields(now);
				LocalDate kontrakMulaiLd = sskk_content.kontrak_mulai != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_mulai) : null;
				LocalDate kontrakAkhirLd = sskk_content.kontrak_akhir != null ?
						LocalDate.fromDateFields(sskk_content.kontrak_akhir) : null;

				if (kontrakMulaiLd != null && kontrakMulaiLd.isBefore(nowLd)
						|| kontrakAkhirLd != null && kontrakAkhirLd.isBefore(nowLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku yang Anda masukkan sudah lewat");
				} else if (kontrakAkhirLd != null && kontrakMulaiLd != null
						&& kontrakAkhirLd.isBefore(kontrakMulaiLd)) {
					flash.error("Data Gagal disimpan! Masa Berlaku akhir kontrak mendahului awal kontrak");
				}else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.after(sskk_content.kontrak_akhir)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi setelah akhir kontrak");
				} else if (sskk_content.inspeksi_tgl != null && sskk_content.inspeksi_tgl.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Inspeksi mendahului mulai kontrak");
				} else if (sskk_content.tgl_serah_terima != null && sskk_content.tgl_serah_terima.before(sskk_content.kontrak_mulai)) {
					flash.error("Data Gagal disimpan! Tanggal Serah Terima Pekerjaan mendahului mulai kontrak");
				}else {// simpan SSKK
					Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
					dok_lelang_content.sskk_content = sskk_content;
					lelang.lls_kontrak_pembayaran = sskk_content.kontrak_pembayaran;
					lelang.save();
					dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
					flash.success("Syarat-Syarat Khusus Kontrak berhasil tersimpan ");
					valid = true;
				}
			}
		}
		if(valid)
			sskk(id);
		else
			sskkError(id,sskk_content);

	}

	/**
	 * upload dok lainnya oleh PPK saat proses persiapan tender bukan buat adendum dokument
	 * @param id
	 */
	@AllowAccess({Group.PPK, Group.KUPPBJ})
	public static void lainnyaPpk(Long id) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket);
		Active_user activeUser = Active_user.current();
		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(paket);
		List<BlobTable> lainnya = dokPersiapan.getDokLainnya();
		boolean editable = dokPersiapan.isEditable(paket, newDate()) && activeUser.isPpk();
		renderArgs.put("paket", paket);
		renderArgs.put("lainnya",lainnya);
		renderArgs.put("editable", editable);
		renderArgs.put("isKuppbj", Active_user.current().isKuppbj());
		renderTemplate("lelang/dokumen/form-info-lainnya-persiapan.html");
	}

	/**
	 * form Dokumen Pengadaan Lainnya
	 * @param id
	 */
	@AllowAccess({Group.PANITIA, Group.KUPPBJ, Group.PPK})
	public static void lainnya(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("kategori", lelang.getKategori());
		renderArgs.put("lelang", lelang);
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id,JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("editable", dok_lelang.isEditable(lelang, newDate()));
		if(dok_lelang != null) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			if(dok_lelang_content != null) {
				renderArgs.put("lainnya",dok_lelang_content.getDokLainnya());
				renderArgs.put("adendum", dok_lelang_content.isAdendum());
			}
		}
		renderArgs.put("backUrl", getPaketLelangBackUrl(tahapStarted, lelang.pkt_id));
		renderArgs.put("isKuppbj", Active_user.current().isKuppbj());
		renderTemplate("lelang/dokumen/form-info-lainnya.html");
	}
	/**
	 * Simpan Form Dokumen Pengadaan Lainnya
	 * @param id
	 */
	@AllowAccess({Group.PANITIA, Group.PPK})
	public static void lainnyaSubmit(Long id, File file) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id,	JenisDokLelang.DOKUMEN_LELANG);
		Map<String, Object> result = new HashMap<String, Object>(1);
		if(dok_lelang.isEditable(lelang, newDate())) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
			try {
				if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_lainnya == null) {
					List<BlobTable> blobTableList = dok_lelang_content.getDokLainnya();
					if(!CollectionUtils.isEmpty(blobTableList))
						dok_lelang_content.dll_lainnya = dok_lelang_content.simpanBlobSpek(blobTableList);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(dok_lelang_content.simpanLainnya(file));
				dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload informasi lainnya");
				result.put("success", false);
				result.put("result", "Kesalahan saat upload informasi lainnya");
			}
		} else {
			result.put("success", false);
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}

	/**
	 * Simpan Form Spek/KAK PPK
	 * @param id
	 */
	@AllowAccess({Group.PPK})
	public static void lainnyaPpkSubmit(Long id, File file) {
		Paket paket = Paket.findById(id);
		otorisasiDataPaket(paket); // check otorisasi data lelang
		Map<String, Object> result = new HashMap<String, Object>(1);
		DokPersiapan dokPersiapan = paket.getDokPersiapan();
		if (dokPersiapan != null && dokPersiapan.isEditable(paket, newDate())) {
			try {
				UploadInfo model = dokPersiapan.simpanLainnya(file);
				if (model == null) {
					result.put("success", false);
					result.put("message", "File dengan nama yang sama telah ada di server!");
					renderJSON(result);
				}
				List<UploadInfo> files = new ArrayList<UploadInfo>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload spek");
				result.put("result", "Kesalahan saat upload spek");
			}
		}else {
			result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);
	}
	
	// fungsi cetak ulang , digunakan dalam kondisi darurat ketika ada permasalahan cetak dokumen
	// ekskutor adalah dit-spse
	public static void cetakUlang(@Required  Long id, @Required Integer versi) throws Exception {
		Dok_lelang dok_lelang = Dok_lelang.findById(id);
		Lelang_seleksi lelang = Lelang_seleksi.findById(dok_lelang.lls_id);
		// jika ada error tidak perlu disimpan
		Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id, versi);
		if (dok_lelang_content == null)
			return;
		// hanya untuk spse 4.2
//		BlobTable blob = BlobTable.findById(dok_lelang_content.dll_content_attachment,versi);
//		dok_lelang.simpan(dok_lelang_content, lelang, blob.getFile());

		// hanya untuk spse 4.3
		String content = dok_lelang.cetakPreview(lelang, dok_lelang_content);
		if (!StringUtils.isEmpty(content)) {
			dok_lelang_content.dll_content_html = content.replaceAll("<br>", "<br/>");
		}
		dok_lelang_content.save();
		response.writeChunk(dok_lelang.dll_jenis.label+" untuk Tender "+lelang.lls_id+" telah berhasil dicetak ulang.");
	}

	@AllowAccess({Group.PANITIA})
	public static void masaBerlakuPenawaran(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		renderArgs.put("id", id); // kode lelang
		Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
		if (dok_lelang != null) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			renderArgs.put("editable", dok_lelang.isEditable(lelang, newDate()) && !dok_lelang_content.isAdendum());
			if(dok_lelang_content != null && dok_lelang_content.ldpcontent != null)
				renderArgs.put("masaberlaku", dok_lelang_content.ldpcontent.masa_berlaku_penawaran);
		}
		renderTemplate("lelang/dokumen/masa-berlaku-penawaran.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void masaBerlakuPenawaranSubmit(Long id, Integer masaberlaku) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(id, JenisDokLelang.DOKUMEN_LELANG);
		boolean editable = dok_lelang.isEditable(lelang, newDate());
		if(!editable){
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		}else{
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
			if(dok_lelang_content != null) {
				LDPContent ldpContent = dok_lelang_content.ldpcontent != null ? dok_lelang_content.ldpcontent:new LDPContent();
				ldpContent.masa_berlaku_penawaran = masaberlaku;
				dok_lelang_content.ldpcontent = ldpContent;
				dok_lelang_content.dll_modified = true;
				dok_lelang_content.save();
			}
		}
		LelangCtr.edit(id);
	}

    @AllowAccess({Group.PANITIA})
    public static void uploadDokTender(Long id) {
		Dok_lelang dok_lelang = Dok_lelang.findById(id);
	    renderArgs.put("id", dok_lelang.lls_id);
	    renderArgs.put("dokId", id);
		renderArgs.put("dok_lelang", dok_lelang);
		renderArgs.put("dok_lelang_content", Dok_lelang_content.findBy(dok_lelang.dll_id));
		renderArgs.put("today", newDate());

		// Dokumen Tender/Seleksi adalah label untuk Tender Prakualifikasi
		// Dokumen Pemilihan adalah label untuk Tender Pascakualifikasi
		String labelDokumen = "Tender/Seleksi";
		if(dok_lelang.getLelang_seleksi().isPascakualifikasi()){
			labelDokumen = "Pemilihan";
		}
		renderArgs.put("labelDokumen", labelDokumen);

		Map<String, Object> params=new HashMap();
		params.put("id",dok_lelang.lls_id);
		String ref = Router.getFullUrl("lelang.LelangCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}

		renderArgs.put("referer",ref);
	    renderTemplate("lelang/dokumen/upload-dok-pemilihan.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void uploadDokKualifikasi(Long id) {
		Dok_lelang dok_lelang = Dok_lelang.findById(id);
		renderArgs.put("id", dok_lelang.lls_id);
		renderArgs.put("dokId", id);
		renderArgs.put("dok_lelang", dok_lelang);
		renderArgs.put("dok_lelang_content", Dok_lelang_content.findBy(dok_lelang.dll_id));
		renderArgs.put("today", newDate());

		Map<String, Object> params=new HashMap();
		params.put("id",dok_lelang.lls_id);
		String ref = Router.getFullUrl("lelang.LelangCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}

		renderArgs.put("referer",ref);
        renderTemplate("lelang/dokumen/upload-dok-kualifikasi.html");
    }
    
	/**
	 * Simpan Form Dokumen Pengadaan
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void doktenderSubmit(Long id, @Valid @PdfType File file, String nomorSDP,
									   @As(binder= DateBinder.class) Date tglSDP) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
        String namaDokumen = lelang.isPrakualifikasi() ? "Tender/Seleksi" : "Pemilihan";
		if (validation.hasErrors()) {
			flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
		} else {

			Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
			try {
				// cetak konten html
				dok_lelang_content.dll_nomorSDP = nomorSDP;
				dok_lelang_content.dll_tglSDP = tglSDP;
				dok_lelang.simpan(dok_lelang_content, lelang, file);
				flash.success("Upload Dokumen %s berhasil.", namaDokumen);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Tender/Seleksi");
			}
		}
		LelangCtr.edit(id);
	}

	@AllowAccess({ Group.PANITIA})
	public static void hapusDokTender(Long id, Integer versi) {
		if (versi == null)
			versi = Integer.valueOf(0);
		otorisasiDataLelang(id); // check otorisasi data lelang
//		Dok_lelang dok_lelang = Dok_lelang.findById(id);
		Dok_lelang dok_lelang = Dok_lelang.findBy(id,JenisDokLelang.DOKUMEN_LELANG);
		Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
		if (dok_lelang_content != null /*&& dok_lelang_content.dll_modified*/) {
			try {
				BlobTable blob = BlobTable.findById(dok_lelang_content.dll_content_attachment, versi);
				if (blob != null)
					blob.delete();
				if(dok_lelang_content.isEmptyDokumen())
				    dok_lelang_content.dll_content_attachment = null;
				if(dok_lelang.isEmptyDokumen())
				    dok_lelang.dll_id_attachment = null;
				dok_lelang_content.dll_modified = true;
				dok_lelang_content.save();
				dok_lelang.save();
				flash.success("Dokumen berhasil dibatalkan.");
			}catch (Exception e){
				Logger.error(e, "Gagal Hapus Dokumen tender");
				flash.error("Pembatalan Dokumen gagal.");
			}
		}
		LelangCtr.edit(dok_lelang.lls_id);
	}


    /**
     * Simpan Form Dokumen Kualifikasi
     * @param id
     */
	@AllowAccess({Group.PANITIA})
	public static void dokKualifikasiSubmit(Long id, @Valid @PdfType File file, String nomorSDP,
											@As(binder= DateBinder.class) Date tglSDP) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		if (validation.hasErrors()) {
			String namaDokumen = lelang.isPrakualifikasi() ? "Tender/Seleksi" : "Pemilihan";
			flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
		} else {
			Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG_PRA);
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
			try {
				dok_lelang_content.dll_nomorSDP = nomorSDP;
				dok_lelang_content.dll_tglSDP = tglSDP;
				dok_lelang.simpan(dok_lelang_content, lelang, file);
				flash.success("Upload Dokumen Kualifikasi berhasil.");
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Kualifikasi");
			}
		}
		LelangCtr.edit(id);
	}


    @AllowAccess({ Group.PANITIA})
    public static void hapusDokKualifikasi(Long id, Integer versi) {
		if (versi == null)
			versi = Integer.valueOf(0);
        otorisasiDataLelang(id); // check otorisasi data lelang
        Dok_lelang dok_lelang = Dok_lelang.findBy(id,JenisDokLelang.DOKUMEN_LELANG_PRA);
        Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
        if (dok_lelang_content != null /*&& dok_lelang_content.dll_modified*/) {
            try {
                BlobTable blob = BlobTable.findById(dok_lelang_content.dll_content_attachment, versi);
                if (blob != null)
                    blob.delete();
                if(dok_lelang_content.isEmptyDokumen())
                    dok_lelang_content.dll_content_attachment = null;
                if(dok_lelang.isEmptyDokumen())
                    dok_lelang.dll_id_attachment = null;
                dok_lelang_content.dll_modified = true;
                dok_lelang_content.save();
                dok_lelang.save();
				flash.success("Dokumen Kualifikasi berhasil dibatalkan.");
            }catch (Exception e){
                Logger.error(e, "Gagal Hapus Dokumen Kualifikasi");
                flash.error("Pembatalan Dokumen Kualifikasi gagal.");
            }
        }
		LelangCtr.edit(dok_lelang.lls_id);
    }

	@AllowAccess({ Group.PPK })
	public static void hapusSskk(Long id, Integer versi) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Map<String, Object> result = new HashMap<String, Object>(1);
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
		if(dok_lelang.isEditable(lelang, newDate())) {
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
			if (dok_lelang_content != null) {
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_sskk_attachment == null) {
                    List<BlobTable> blobList = dok_lelang_content.getDokSskk();
                    blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                    try {
                        dok_lelang_content.dll_sskk_attachment = dok_lelang_content.simpanBlobSpek(blobList);
                        result.put("success", true);
                        dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                    } catch (Exception e) {
                        Logger.error(e, "File Anda tidak dapat dihapus");
                        result.put("success", false);
                        result.put("message", "File Anda tidak dapat dihapus");
                    }

                }  else {
					BlobTable blob = BlobTable.findById(dok_lelang_content.dll_sskk_attachment, versi);
					if (blob != null)
						blob.delete();
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
				}
			}
		}else {
			result.put("success",false);
			result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
		}
		renderJSON(result);

	}
}
