package controllers.lelang;

import ams.models.ServiceResult;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import models.agency.*;
import models.agency.Paket.StatusPaket;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Checklist_master.JenisChecklist;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Persetujuan.JenisPersetujuan;
import models.lelang.rhs.RhsPermission;
import models.secman.Group;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.libs.MimeTypes;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;
import play.mvc.Http;
import models.sso.common.adp.util.DceSecurityV2;
import play.mvc.Router;
import utils.SikapUtil;
import utils.osd.KmsClient;
import utils.osd.OSDUtil;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.*;

/**
 * definisikan semua terkait tender
 *
 * @author arief ardiyansah
 */
public class LelangCtr extends BasicCtr {

    /**
     * Fungsi {@code index} digunakan untuk menampilkan halaman riwayat tender
     * e-procurement LPSE
     */
    public static void index(Integer kategoriId,String instansiId, Integer tahun, String rekanan) {
        renderArgs.put("kategoriId", kategoriId);
        renderArgs.put("instansiId", instansiId);
        renderArgs.put("tahun", tahun);
        renderArgs.put("rekanan", rekanan);
        renderArgs.put("kategoriList", Kategori.all);
        renderArgs.put("instansiList", Lelang_query.findInstansi());
        renderArgs.put("tahunList", Lelang_query.listTahunAnggaranAktif());
        renderTemplate("lelang/lelang.html");
    }

    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR, Group.PPK})
    public static void view(Long id) {
        Active_user activeUser = Active_user.current();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("lelang", lelang);
        boolean isPanitia = activeUser.isPanitia();
        renderArgs.put("isPra", lelang.isPrakualifikasi());
        renderArgs.put("isPasca", lelang.isPascakualifikasi());
        renderArgs.put("isPanitia", isPanitia);
        renderArgs.put("isLelangAktif", lelang.lls_status.isAktif());
        renderArgs.put("tahapNow", Jadwal.getJadwalSekarang(id, true, lelang.getPemilihan().isLelangExpress(), lelang.isLelangV3()));
        TahapStarted tahapStarted = lelang.getTahapStarted();
        renderArgs.put("tahapStarted", tahapStarted);
        TahapNow tahapAktif = lelang.getTahapNow();
        renderArgs.put("tahapAktif", tahapAktif);
        Dok_lelang dokLelangPra = null;
        if (lelang.isPrakualifikasi()) {
            renderArgs.put("isKirimKualifikasi",tahapAktif.isPemasukanDokPra());
            dokLelangPra = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG_PRA);
            renderArgs.put("dok_kualifikasi", dokLelangPra);
            renderArgs.put("allow_adendum_pra", isPanitia && tahapStarted.isAllowAdendumPra() && !lelang.lls_status.isDraft());
            Evaluasi pembuktian = Evaluasi.findPembuktian(id);
            boolean allowPengumumanPra = pembuktian != null && pembuktian.eva_status.isSelesai() && tahapStarted.isPengumumanPemenangPra();
            renderArgs.put("allow_pengumuman_pra", isPanitia && allowPengumumanPra);
            renderArgs.put("sudahKirimUndanganPra", lelang.kirim_pengumuman_pemenang_pra);
            if (dokLelangPra != null) {
                boolean sudahCetak = dokLelangPra != null && dokLelangPra.dll_id_attachment != null;
                renderArgs.put("allow_download_pra", sudahCetak && tahapStarted.isAllowDownloadDokPra());
                Dok_lelang_content dok_lelang_content_pra = Dok_lelang_content.findBy(dokLelangPra.dll_id);
                if (dok_lelang_content_pra != null) {
                    renderArgs.put("ldk_adendum_pra", dok_lelang_content_pra.dll_ldk_updated
                            && dok_lelang_content_pra.isAllowUpload());
                    renderArgs.put("allowUploadPra",dok_lelang_content_pra.isAllowUpload()
                            && activeUser.isPanitia());
                }
            }
        }
        //checklist adendum
        Dok_lelang dokLelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("dok_lelang", dokLelang);
        if (dokLelang != null) {
            boolean sudahCetak = dokLelang != null && dokLelang.dll_id_attachment != null;
            renderArgs.put("allow_download", sudahCetak && tahapStarted.isAllowDownloadDok(lelang.isPrakualifikasi(), lelang.isLelangV3())); // allow download dokumen lelang
            Dok_lelang_content dokLelangContent = Dok_lelang_content.findBy(dokLelang.dll_id);
            final boolean allow_adendum = sudahCetak && lelang.getTahapStarted().isAllowAdendum();
            renderArgs.put("allow_adendum", allow_adendum); // allow adendum
            if (dokLelangContent != null) {
                boolean isAdendum = dokLelangContent.isAdendum();
                boolean modified = dokLelangContent.dll_modified;
                boolean ldpAdendum = dokLelangContent.dll_ldp != null && modified && isAdendum;
                renderArgs.put("masaberlaku", ldpAdendum ? dokLelangContent.ldpcontent.masa_berlaku_penawaran: dokLelang.getMasaBerlakuPenawaran());
                renderArgs.put("allowUpload", isAdendum && modified && activeUser.isPanitia());
                renderArgs.put("ldk_adendum", dokLelangContent.dll_ldk_updated && modified && isAdendum);
                renderArgs.put("syarat_adendum", dokLelangContent.dll_syarat_updated && modified && isAdendum);
                renderArgs.put("hps_adendum", dokLelangContent.dll_dkh != null && modified && isAdendum);
                renderArgs.put("ldp_adendum", ldpAdendum);
                renderArgs.put("sskk_adendum", dokLelangContent.dll_sskk != null && modified && isAdendum);
                renderArgs.put("spek_adendum", dokLelangContent.dll_spek != null && modified && isAdendum);
                renderArgs.put("lainnya_adendum", dokLelangContent.dll_lainnya != null && modified && isAdendum);
                renderArgs.put("rancangankontrak_adendum", dokLelangContent.dll_sskk_attachment != null && modified && isAdendum);
                renderArgs.put("jeniskontrak_adendum", dokLelangContent.dll_kontrak_pembayaran != null && modified && isAdendum);
                renderArgs.put("jenis_kontrak", dokLelangContent.dll_kontrak_pembayaran != null ? JenisKontrak.findById(dokLelangContent.dll_kontrak_pembayaran).label : lelang.getKontrakPembayaran().label);
            }
        }
        renderArgs.put("baTambahan", Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_TAMBAHAN));
        if (tahapStarted.isPengumumanPemenangAkhir()) {
            Evaluasi penetapan = Evaluasi.findPenetapanPemenang(id);
            renderArgs.put("allow_pengumuman_pemenang", isPanitia && penetapan != null && penetapan.eva_status.isSelesai() 
                    && (!lelang.getPemilihan().isLelangExpress() || lelang.hasPemenangTerverifikasi()) && Persetujuan.isApprove(id, JenisPersetujuan.PEMENANG_LELANG));
            renderArgs.put("sudahKirimPengumuman", lelang.kirim_pengumuman_pemenang);
        }
        if (activeUser.isRekanan()) { // untuk rekanan
            Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", lelang.lls_id, activeUser.rekananId).first();
            renderArgs.put("peserta", peserta);
            Dok_penawaran penawaranKualifikasi = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
            renderArgs.put("penawaran_kualifikasi", penawaranKualifikasi);
            boolean kualifikasiUploaded = penawaranKualifikasi != null || lelang.getPemilihan().isLelangExpress();
            renderArgs.put("kualifikasiUploaded", kualifikasiUploaded);
            if(kualifikasiUploaded) {
                Date tgl_dokumen = lelang.isPrakualifikasi() ?  dokLelangPra.getTanggalDokumen() : dokLelang.getTanggalDokumen();
                Dok_penawaran penawaran_kualifikasi = peserta.getDokKualifikasi();
                if (penawaran_kualifikasi != null && tgl_dokumen != null) {
                    renderArgs.put("notifikasi_kualifikasi", tgl_dokumen.after(penawaran_kualifikasi.dok_tgljam));
                }
            }
            List<Nilai_evaluasi> pemenangList = lelang.getPemenangList();
            if (!CollectionUtils.isEmpty(pemenangList)) {
                // check pemenang by evaluasi akhir
                for(Nilai_evaluasi o : pemenangList) {
                    if(o.psr_id.equals(peserta.psr_id)) {
                        renderArgs.put("pemenang", true);
                        break;
                    }
                }
                // chek pemenang by sppbj
                Sppbj sppbj = Sppbj.findByLelang(id);
                if(sppbj != null) {
                    renderArgs.put("pemenang", sppbj.rkn_id.equals(activeUser.rekananId));
                }
            }
            if(dokLelang != null) {
                Date tgl_dokumen = dokLelang.getTanggalDokumen();
                Dok_penawaran penawaran_teknis = peserta.getFileTeknis();
                if(penawaran_teknis != null && tgl_dokumen != null ) {
                    renderArgs.put("notifikasi_admin_teknis", tgl_dokumen.after(penawaran_teknis.dok_tgljam));
                }
                Dok_penawaran penawaran_harga = peserta.getFileHarga();
                if(penawaran_harga != null && tgl_dokumen != null) {
                    renderArgs.put("notifikasi_harga", tgl_dokumen.after(penawaran_harga.dok_tgljam));
                }
            }

        } else { // untuk selain rekanan
            boolean hide = !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca());
            boolean allow_pembukaan = (activeUser.isPanitia() || activeUser.isAuditor()) && !lelang.isLelangV3() && tahapStarted.isPembukaan();
            renderArgs.put("hide", hide);
            renderArgs.put("kualifikasiPermission", lelang.isKualifikasiPermission());
            renderArgs.put("allow_report",
                    (activeUser.isPanitia() || activeUser.isAuditor() || activeUser.isPpk()) && !hide);
            renderArgs.put("allow_pembukaan", allow_pembukaan);
            if (activeUser.isPanitia()) {
                if (lelang.isPrakualifikasi()) {
                    renderArgs.put("allowEvaluasiUlangPra", lelang.isPrakualifikasi() && tahapStarted.isAllowEvaluasiUlangPra());
                }
                renderArgs.put("allowEvaluasiUlang", tahapStarted.isAllowEvaluasiUlang());
            }
            if (activeUser.isAuditor()) {
                Jadwal kontrak = Jadwal.findByLelangNTahap(id, Tahap.TANDATANGAN_KONTRAK);
                renderArgs.put("allow_keterangan", kontrak != null && kontrak.isNow(newDate()));
            }
        }
        renderArgs.put("allowUpdate", false); // fitur Ubah Lelang di-disable, terkait perubahan lelang harus melalui adendum
        renderArgs.put("allowForensik", "true".equals(Play.configuration.getProperty("allow.rhs.permission", "true")));
        Paket paket = Paket.findById(lelang.pkt_id);
        if(paket.isFlag43()){
        		renderArgs.put("paketPpk", PaketPpk.findByPaket(paket.pkt_id));
       	} else {
            renderArgs.put("ppkList", Paket_anggaran.findByPaket(lelang.pkt_id));
        }
        renderArgs.put("historyPpkList",History_paket_ppk.findByPaket(paket.pkt_id));
        renderArgs.put("allowEditJadwal",tahapAktif.isAllowEditJadwal() && !lelang.isShowNotifTambahJadwalTambahan());

        // memeriksa apakah sudah amanda atau belum
        boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
        renderArgs.put("isOSD", isOSD);
        renderArgs.put("isAuctionLelangCepat" , lelang.isExpress());
        renderArgs.put("isFlag43",paket.isFlag43());
        renderTemplate("lelang/lelang-view.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void edit(Long id) { // oldId diisi jika lelang ulang.
    		otorisasiDataLelang(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        notFoundIfNull(lelang);
        Paket paket = lelang.getPaket();
        Double nilaiHps = paket.pkt_hps;
        if(Dok_lelang.count("lls_id=?", id) == 0) { // check jika Dok_lelang null
            DokPersiapan dokPersiapan = DokPersiapan.findByPaket(paket.pkt_id);
            if(dokPersiapan != null)
                dokPersiapan.transferDokPersiapanToDokLelang();
        }
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", paket);
        // Menambah validasi apabila ppk belum diisi di paket tersebut
        if (Paket_anggaran.count("pkt_id=? and ppk_id is null", paket.pkt_id) > 0) {
            flash.error("Maaf Anda belum melengkapi data PPK pada detail paket");
            PaketCtr.index(null, null);
        }
        // Validasi jika paket belum ada data lokasi pekerjaan
        if (CommonUtil.isEmpty(paket.getPaketLokasi())) {
            flash.error(Messages.get("flash.mabmdlppdp"));
            PaketCtr.index(null, null);
        }
        Kategori kategori = lelang.getKategori();
        if(kategori.isKonstruksi() && lelang.lls_metode_penawaran == null)
            lelang.lls_metode_penawaran = Lelang_seleksi.MetodePenawaranHarga.AUCTION;

        renderArgs.put("konsultansi", kategori.isConsultant());
        renderArgs.put("konstruksi", kategori.isKonstruksi());
        renderArgs.put("JasaKonsultanKonstruksi", kategori.isJkKonstruksi());
        renderArgs.put("JkKonstruksi", kategori.isJkKonstruksi() || kategori.isKonstruksi());
        renderArgs.put("metode", lelang.getMetode());
        boolean sedangPersetujuan = Persetujuan.isSedangPersetujuan(lelang.lls_id);
        boolean draft = !lelang.isLelangV3() && lelang.lls_status.isDraft() && !sedangPersetujuan;
        renderArgs.put("kategori", kategori);
        renderArgs.put("draft", draft);
        renderArgs.put("ketJadwal", lelang.getInfoJadwal());
        boolean prakualifikasi = lelang.isPrakualifikasi();
        Dok_lelang dok_kualifikasi = null;
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
        if(prakualifikasi) {
            dok_kualifikasi = Dok_lelang.findNCreateBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG_PRA);
            renderArgs.put("dok_is_editable", dok_lelang.isEditable(lelang, newDate()));
        }

        renderArgs.put("prakualifikasi", prakualifikasi);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("dok_kualifikasi", dok_kualifikasi);
        boolean ldk_terisi = false;
        boolean allow_persetujuan = false;
        if (dok_kualifikasi != null) {
            Dok_lelang_content dok_kualifikasi_content = Dok_lelang_content.findBy(dok_kualifikasi.dll_id, 1);
            if(dok_kualifikasi_content == null) {
                dok_kualifikasi_content = Dok_lelang_content.findNCreateBy(dok_kualifikasi.dll_id, lelang);
            }
            ldk_terisi = !CommonUtil.isEmpty(dok_kualifikasi.getSyaratIjinUsaha()) || !CommonUtil.isEmpty(dok_kualifikasi.getSyaratKualifikasi()) || 
            				!CommonUtil.isEmpty(dok_kualifikasi.getSyaratIjinUsahaBaru()) || !CommonUtil.isEmpty(dok_kualifikasi.getSyaratKualifikasiAdministrasi());
            renderArgs.put("ldk_terisi", ldk_terisi);
            if (dok_kualifikasi_content != null) {
                renderArgs.put("dok_kualifikasi_content", dok_kualifikasi_content);
                if (prakualifikasi) {
                    allow_persetujuan = !dok_kualifikasi_content.dll_modified && dok_kualifikasi.dll_id_attachment != null && ldk_terisi && paket.pkt_hps > 0;
                    Persetujuan.createPersetujuan(lelang.lls_id, JenisPersetujuan.PENGUMUMAN_LELANG);
                }
            }
            boolean allow_upload_kualifikasi = draft && dok_kualifikasi_content.dll_modified && lelang.isJadwalSudahTerisi() && ldk_terisi;
            renderArgs.put("allow_upload_kualifikasi", allow_upload_kualifikasi);
            renderArgs.put("dokKualifikasiList", dok_kualifikasi.getDokumenList());
        }
        if (dok_lelang != null) {
            Integer masaBerlaku = 0; // default masa berlaku penawaran
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id, 1);
            if(dok_lelang_content == null) {
                dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            }
            if(dok_lelang_content != null){
                nilaiHps = !StringUtils.isEmpty(dok_lelang_content.dll_dkh) ? nilaiHps : 0d;
                if(dok_lelang_content.ldpcontent != null){
                    masaBerlaku =  dok_lelang_content.ldpcontent.masa_berlaku_penawaran;
                }
                renderArgs.put("dok_lelang_content", dok_lelang_content);
            }
            renderArgs.put("masaberlaku", masaBerlaku);
            boolean persyaratan_terisi = !CommonUtil.isEmpty(dok_lelang.getSyaratTeknis()) || !CommonUtil.isEmpty(dok_lelang.getSyaratHarga());
            renderArgs.put("persyaratan_terisi", persyaratan_terisi);
            List<BlobTable> dok_sskk = dok_lelang_content.getDokSskk();
            renderArgs.put("dok_sskk", dok_sskk);
            boolean rancangan_kontrak_terisi = !CollectionUtils.isEmpty(dok_sskk);
            if(lelang.isExpress()) {
                allow_persetujuan = !StringUtils.isEmpty(dok_lelang_content.dll_dkh) && dok_lelang.dll_id_attachment != null && masaBerlaku > 0; // allow persetujuan jika dokumen lelang sudah di cetak
                boolean allow_upload_dok = draft && dok_lelang_content.dll_modified && lelang.isJadwalSudahTerisi() && masaBerlaku > 0 && rancangan_kontrak_terisi;
                renderArgs.put("allow_upload_dok", allow_upload_dok);
            } else if (lelang.isPascakualifikasi()) {
                ldk_terisi = !CommonUtil.isEmpty(dok_lelang.getSyaratIjinUsaha()) || !CommonUtil.isEmpty(dok_lelang.getSyaratKualifikasi()) || 
                				!CommonUtil.isEmpty(dok_lelang.getSyaratIjinUsahaBaru()) || !CommonUtil.isEmpty(dok_lelang.getSyaratKualifikasiAdministrasi());
                renderArgs.put("ldk_terisi", ldk_terisi);
                allow_persetujuan = !StringUtils.isEmpty(dok_lelang_content.dll_dkh) && dok_lelang.dll_id_attachment != null && ldk_terisi && persyaratan_terisi && masaBerlaku > 0; // allow persetujuan jika dokumen lelang sudah di cetak
                boolean allow_upload_dok = draft && dok_lelang_content.dll_modified && lelang.isJadwalSudahTerisi() && ldk_terisi && persyaratan_terisi && masaBerlaku > 0 && rancangan_kontrak_terisi;
                renderArgs.put("allow_upload_dok", allow_upload_dok);
            } else {
                boolean allow_upload_dok = dok_lelang_content.dll_modified && lelang.isJadwalSudahTerisi() && persyaratan_terisi && masaBerlaku > 0 && rancangan_kontrak_terisi;
                Evaluasi pembuktian = Evaluasi.findPembuktian(id);
                Evaluasi evakualifikasi = Evaluasi.findKualifikasi(id);
                allow_upload_dok = allow_upload_dok && (pembuktian == null || pembuktian.eva_status.isSedangEvaluasi()) && (evakualifikasi == null || evakualifikasi.eva_status.isSedangEvaluasi());
                renderArgs.put("allow_upload_dok", allow_upload_dok);
            }

            if(!prakualifikasi) {
                Persetujuan.createPersetujuan(lelang.lls_id, JenisPersetujuan.PENGUMUMAN_LELANG);
            }

        }
        // jika proses pascakualifkasi maka dokumen kualifikasi dihapus
        if (!prakualifikasi && dok_kualifikasi != null) {
            Checklist.delete("dll_id=? and chk_versi=1", dok_kualifikasi.dll_id);
            Dok_lelang_content.delete("dll_id=?", dok_kualifikasi.dll_id);
            dok_kualifikasi.delete();
        }
        if (lelang.isExpress()) {
            Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
            List<Long> listKab = Query.find("select kbp_id from paket_lokasi where pkt_id=?", Long.class, paket.pkt_id).fetch();
            Paket_satker paket_satker = Paket_satker.find("pkt_id=?", paket.pkt_id).first();
            Satuan_kerja satker = Satuan_kerja.findById(paket_satker.stk_id);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("userid", active_user.userid);
            jsonObject.addProperty("role", "PANITIA");
            jsonObject.addProperty("nama", pegawai.peg_nama);
            jsonObject.addProperty("repo_id", ConfigurationDao.getRepoId());
            jsonObject.addProperty("prod", ConfigurationDao.isProduction() ? "TRUE" : "FALSE");
            jsonObject.addProperty("lls_id", lelang.lls_id);
            jsonObject.addProperty("pkt_nama", paket.pkt_nama);
            jsonObject.addProperty("kls_id", paket.kls_id);
            jsonObject.addProperty("kgr_id", paket.kgr_id.id);
            jsonObject.addProperty("pkt_hps", paket.pkt_hps);
            jsonObject.addProperty("pkt_flag", paket.pkt_flag);
            if (satker != null)
                jsonObject.addProperty("instansi_id", satker.instansi_id);
            if (!CommonUtil.isEmpty(listKab))
                jsonObject.addProperty("kbp_id", StringUtils.join(listKab, ","));
            try {
                String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
                renderArgs.put("sikap_shorlist_url", BasicCtr.SIKAP_URL + "/shortlist/create?q=" + param);
                renderArgs.put("sikap_kriteria_url", BasicCtr.SIKAP_URL + "/shortlist/showQualification?q=" + lelang.lls_id);
                // jika lelang express (cepat) lakukan pemeriksaan shortlist
                // jika belum membuat shortlist maka tidak diizinkan untuk melakukan persetujuan
                allow_persetujuan = allow_persetujuan && SikapUtil.isCriteriaCreated(lelang);
            } catch (Exception e) {
                Logger.error(e, "Gagal memeriksa status shortlist ke SIKaP");
            }
        }
        Persetujuan persetujuan_pegawai = Persetujuan.findByPegawaiPengumuman(active_user.pegawaiId, lelang.lls_id);
        if (persetujuan_pegawai != null && allow_persetujuan) {
            allow_persetujuan = persetujuan_pegawai.pst_status.isBelumSetuju() || persetujuan_pegawai.pst_status.isTidakSetuju();
        }

        // pakta integritas tidak muncul bila alasan lelang ulang tidak diisi
        if (lelang.isLelangUlang()) {
            if (lelang.lls_diulang_karena == null || lelang.lls_diulang_karena.isEmpty()) {
                allow_persetujuan = false;
            }
            renderArgs.put("checklist", Checklist_gagal.findBy(lelang.lls_id, JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL));
        }

        // Jika dokumen lelang content modified=false
        if(!prakualifikasi) {
            allow_persetujuan = allow_persetujuan && dok_lelang.allowPersetujuan();
        }

        renderArgs.put("allowBatalPersetujuan",lelang.allowBatalPersetujuan());
        if(!kategori.isConsultant() && !kategori.isJkKonstruksi()){
            allow_persetujuan = allow_persetujuan && !StringUtils.isEmpty(paket.kls_id);
        }
        renderArgs.put("allow_persetujuan", allow_persetujuan && lelang.isJadwalSudahTerisi());
        renderArgs.put("nilaiHps", nilaiHps);

        renderTemplate("lelang/lelang-edit.html");
    }


    @AllowAccess({Group.PPK})
    public static void hapus(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);

        if (lelang == null || !lelang.lls_status.isDraft()) {
            flash.error("Maaf, Lelang yang sedang aktif tidak dapat dihapus!");
            PaketCtr.index(null, null);
        }
        try {
            lelang.delete();
        } catch (Exception ex) {
            Logger.info(ex,"Error delete " + ex.getLocalizedMessage());
            flash.error("Gagal menghapus paket " + lelang.pkt_id);
            PaketCtr.index(null, null);
        }

        PaketCtr.index(null, null);
    }

    @AllowAccess({Group.PANITIA})
    public static Boolean copyShortlist(Long oldId, Long newId) {
        boolean retval = false;
        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("old_lls_id", oldId);
            jsonObject.addProperty("new_lls_id", newId);
            String param;

            param = URLEncoder.encode(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)), "UTF-8");
            String url = BasicCtr.SIKAP_URL + "/services/copyShortlist?q=" + param;
            WSRequest request = WS.url(url);
            String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
            if (!CommonUtil.isEmpty(content)) {
                retval = Boolean.parseBoolean(DceSecurityV2.decrypt(content));
            }
        } catch (Exception e) {
           Logger.error(e,"Gagal copy shortlist %s ke %s", oldId, newId);
        }
        return retval;
    }

    
    @AllowAccess({Group.PANITIA})
    public static void simpan(Long id, Lelang_seleksi lelang, String kualifikasiId) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi obj = Lelang_seleksi.findById(id);
        if (kualifikasiId != null && kualifikasiId.isEmpty()) {
        		flash.error("Kualifikasi Usaha wajib diisi");
        		edit(obj.lls_id);
        } else {
            if(lelang != null) {
                obj.lls_penetapan_pemenang = lelang.lls_penetapan_pemenang != null && !obj.getPemilihan().isLelangExpress() ? lelang.lls_penetapan_pemenang : Lelang_seleksi.JenisLelang.LELANG_NON_ITEMIZE;
                obj.lls_metode_penawaran = lelang.lls_metode_penawaran == null ? Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION : lelang.lls_metode_penawaran;
                if (lelang.lls_versi_lelang == null) {
                    Paket paket = Paket.findByLelang(id);
                    if (paket.pkt_status.equals(StatusPaket.ULANG_LELANG)) {
                        if (lelang.lls_diulang_karena != null && !lelang.lls_diulang_karena.isEmpty()) {
                            obj.lls_diulang_karena = lelang.lls_diulang_karena;
                        } else {
                            flash.error("Alasan mengulang Tender wajib diisi");
                            edit(obj.lls_id);
                        }
                    }
                }
                obj.save();
            }
            if (kualifikasiId != null) {
	            Paket paket = Paket.findById(obj.pkt_id);
	            if (paket.kls_id != null && !paket.kls_id.equals(kualifikasiId) && (paket.kgr_id.equals(Kategori.PEKERJAAN_KONSTRUKSI) || paket.kgr_id.equals(Kategori.KONSULTANSI_KONSTRUKSI))) {
	        			String jenis_checklist = StringUtils.join(JenisChecklist.CHECKLIST_KUALIFIKASI_BARU, ',');
	        			Checklist.delete("dll_id in (select dll_id from dok_lelang where lls_id=?)  and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis_checklist+"))", obj.lls_id);
	            }
	            paket.kls_id = kualifikasiId;
	            paket.save();
            }
            edit(obj.lls_id);
        }     
    }
    
    /**
     * Fungsi {@code rekananLelangBaru} digunakan untuk menampilkan halaman
     * daftara lelang baru yang tersedia untuk rekanan
     */
    @AllowAccess({Group.REKANAN})
    public static void lelangBaru() {
        Date now = newDate();
        Blacklist blacklist = Blacklist.find("rkn_id=? and bll_tglawal <= ? AND bll_tglakhir >= ?", Active_user.current().rekananId, now, now).first();
        if (blacklist != null)
            BerandaCtr.index(); // jika rekanan terblacklist tidak dpt ikut lelang dan kembali home rekanan
        else
            renderTemplate("lelang/lelang-baru.html");
    }

    /*
     * Penutupan Lelang
     */
    @AllowAccess({Group.PANITIA})
    public static void tutup(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        TahapStarted tahapStarted = lelang.getTahapStarted();
        notFoundIfNull(lelang);
        if(lelang.isDitutup())
            forbidden("Maaf Tender sudah dibatalkan");
        renderArgs.put("lelang", lelang);
        renderArgs.put("allowEvaluasiUlang", tahapStarted.isAllowEvaluasiUlang());
        renderArgs.put("tahapNow", Jadwal.getJadwalSekarang(id, true, lelang.getPemilihan().isLelangExpress(), lelang.isLelangV3()));
        boolean belumPersetujuan = !Persetujuan.isSedangPersetujuanBatalTender(id);
        renderArgs.put("belumPersetujuan", belumPersetujuan);
        Persetujuan persetujuan = Persetujuan.findByPegawaiLelangInJenisBatal(lelang.lls_id);
        renderArgs.put("persetujuan",persetujuan);
        if(belumPersetujuan)
            renderArgs.put("checklist", Checklist_gagal.getChecklist(JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL));
        else {
            renderArgs.put("checklist", Checklist_gagal.findBy(id, JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL));
        }
        if(persetujuan != null){
            renderArgs.put("alasanLabel", persetujuan.pst_jenis == JenisPersetujuan.BATAL_LELANG ? "Alasan Gagal":"Alasan Ulang");
            renderArgs.put("allowPersetujuan",persetujuan.pst_status.isBelumSetuju() || persetujuan.pst_status.isTidakSetuju());
            renderArgs.put("allowBatalPersetujuan", persetujuan.pst_status.isSetuju() && lelang.isSedangPersetujuanBatalTender() && lelang.lls_status.isAktif());
        }
        renderTemplate("lelang/lelang-tutup.html");
    }
    
    
    
    
    
    

    /**
     * Admin Lpse mengubah jadwal Fungsi {@code ubahJadwalLelang} digunakan
     * untuk menampilkan halaman.
     *
     * @param lelangId
     */
    @AllowAccess({Group.ADM_PPE})
    public static void buka(Long lelangId) {
        if (lelangId != null) {
            Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
            if (lelang != null) {
                renderArgs.put("lelang", lelang);
            } else if (lelangId != 0) {
                flash.error("tender yang Anda cari belum terdaftar");
            }
        }
        renderTemplate("lelang/lelang-buka.html");
    }

    /**
     * submit penutupan lelang atau lelang diulang
     *
     * @param id
     * @param actionLelang
     * @param alasan
     * @param alasan
     */
    @AllowAccess({Group.PANITIA})
    public static void tutupSubmit(Long id, String actionLelang, String alasan, Boolean setuju, String alasanTidakSetuju, List<Checklist_gagal> checklistKategori) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        boolean emptyChecklist = true;
        if (checklistKategori != null) {
            for (int i = 0; i < checklistKategori.size(); i++) {
                if (checklistKategori.get(i).ckm_id != null) {
                    emptyChecklist = false;
                    validation.valid(checklistKategori.get(i));
                }
            }
        }

        if(setuju == null){
            setuju = true;
        }

        if (!setuju && CommonUtil.isEmpty(alasanTidakSetuju)) {
            Validation.addError("alasanTidakSetuju", "Alasan wajib diisi");
        }else if (!setuju && !CommonUtil.isEmpty(alasanTidakSetuju) && alasanTidakSetuju.length() <= 30) {
            Validation.addError("alasanTidakSetuju", "Alasan minimal 30 karakter");
        }

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            tutup(id);
        } else {
            try {
                Lelang_seleksi lelang = Lelang_seleksi.findById(id);
                notFoundIfNull(lelang);
                if(lelang.isDitutup())
                    forbidden("Maaf Tender sudah dibatalkan");
                JenisPersetujuan jenis = JenisPersetujuan.BATAL_LELANG;
                lelang.lls_ditutup_karena = alasan;
                if(actionLelang.equals("ulang")){
                    jenis = JenisPersetujuan.ULANG_LELANG;
                    lelang.lls_diulang_karena = alasan;
                }
                lelang.save();
                Persetujuan.simpanPersetujuanBatalTender(lelang,setuju,alasanTidakSetuju,jenis);
                if(!Persetujuan.isSedangPersetujuanPraGagalTender(lelang.lls_id) && !CollectionUtils.isEmpty(checklistKategori))
                    Checklist_gagal.simpanChecklist(lelang, checklistKategori, JenisChecklist.CHECKLIST_PASCAKUALFIKASI_BATAL, Integer.valueOf(1));
                if (Persetujuan.isApprove(id, jenis)) {
                    if (jenis.equals(JenisPersetujuan.BATAL_LELANG)) {
                        try {
                            lelang.tutupLelang();
                            BerandaCtr.index();
                        } catch (Exception e) {
                            Logger.error("Kesalahan lelangUlang %s", e.getLocalizedMessage());
                            flash.error(Messages.get("lelang.gagal_tutup"));
                        }
                    } else if (jenis.equals(JenisPersetujuan.ULANG_LELANG)) {
                        try {
                            lelang.tutupLelang();
                            Long lelangIdBaru = Lelang_seleksi.ulangLelang(id);
                            if (lelang.getPemilihan().isLelangExpress())
                                edit(lelangIdBaru);
                            else
                                edit(lelangIdBaru);
                        } catch (Exception e) {
                            Logger.error("Kesalahan lelangUlang %s", e.getLocalizedMessage());
                            flash.error(Messages.get("lelang.gagal_ulang"));
                        }
                    }
                }
            }catch (Exception e) {
               Logger.error(e, "Kendala simpan tender gagal : %s", e.getMessage());
                flash.error(e.getMessage());
            }
        }

        tutup(id);
    }

    /**
     * submit penutupan lelang atau lelang diulang
     *
     * @param id
     * @param tutup
     * @param ulang
     * @param alasan
     */
    @AllowAccess({Group.ADM_PPE})
    public static void bukaSubmit(Long id, boolean tutup, boolean ulang, @Required String alasan) {
        checkAuthenticity();
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            buka(id);
        } else {
            try {
                Lelang_seleksi lelang = Lelang_seleksi.findById(id);
                lelang.bukaLelang(alasan);
                flash.success("Reaktivasi Tender telah berhasil dilakukan");
            } catch (Exception e) {
                Logger.error("Kesalahan Reaktivasi Tender %s", e.getLocalizedMessage());
                flash.error("Reaktivasi Tender tidak dapat dilakukan. Mohon ulangi");
            }
            buka(id);
        }
    }

    /**
     * submit lelang ulang
     *
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void ulang(Long id) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        notFoundIfNull(lelang);
        if(lelang.lls_status.isAktif())
            forbidden("Maaf Tender sedang Berjalan, Anda tidak bisa melakukan Tender Ulang");
        Long lelangIdBaru = Lelang_seleksi.ulangLelang(id);
        edit(lelangIdBaru);
    }

    /**
     * aktivitas pengumuman lelang hak akses public
     *
     * @param id
     */
    public static void pengumumanLelang(Long id) {
        Lelang_detil lelang = Lelang_detil.findById(id);
        notFoundIfNull(lelang);
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", lelang.getPaket());
        boolean isAuditor = false;
        if(Active_user.current() != null){
            isAuditor = Active_user.current().isAuditor();
        }
        if (!isAuditor && lelang != null && !lelang.lls_status.isAktif())
            forbidden(Messages.get("not-authorized"));// hanya lelang aktif yang muncul di pengumuman
        renderArgs.put("tahapStarted", new TahapStarted(id));
        renderArgs.put("anggaranList", Anggaran.findByPaket(lelang.pkt_id));
        renderArgs.put("lokasiList", Paket_lokasi.findByPaket(lelang.pkt_id));
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
        if (dok_lelang != null) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
            if (dok_lelang_content != null) {
                LDPContent ldpContent = dok_lelang_content.ldpcontent;
                String lingkup = ldpContent != null ? ldpContent.lingkup_pekerjaan : "";
                renderArgs.put("lingkup", lingkup);
            }
        }
        renderTemplate("lelang/pengumuman-lelang.html");
    }

    @AllowAccess({Group.PANITIA, Group.PPK})
    public static void viewDraft(Long id) {
        Lelang_detil lelang = Lelang_detil.findById(id);
        notFoundIfNull(lelang);
        otorisasiDataLelang(id); // check otorisasi data lelang
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", lelang.getPaket());
        renderArgs.put("tahapStarted", new TahapStarted(id));
        renderArgs.put("anggaranList", Anggaran.findByPaket(lelang.pkt_id));
        renderArgs.put("lokasiList", Paket_lokasi.findByPaket(lelang.pkt_id));
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
        if (dok_lelang != null) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
            if (dok_lelang_content != null) {
                LDPContent ldpContent = dok_lelang_content.ldpcontent;
                String lingkup = ldpContent != null ? ldpContent.lingkup_pekerjaan : "";
                renderArgs.put("lingkup", lingkup);
            }
        }
        renderTemplate("lelang/lelang-draft.html");
    }

    /**
     * aktivitas pemasukan penawaran ulang hak akses Panitia dan peserta
     *
     * @param id TODO
     */
    @AllowAccess({Group.PANITIA})
    public static void penawaranUlang(Long id) {
    	otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("lelang", lelang);
        renderArgs.put("list", Jadwal.findByLelang(id));
        TahapStarted tahapStarted = lelang.getTahapStarted();
        boolean editable = tahapStarted.isAllowEvaluasiUlang();
        Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.AMBIL_DOKUMEN_PEMILIHAN); // tender 1 file dan 2 file
        if(jadwal == null)
            jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.AMBIL_DOKUMEN); // tender 2 tahap
        renderArgs.put("urutStart", editable && jadwal != null ? jadwal.akt_urut : 0);
        renderArgs.put("editable", editable);
        renderArgs.put("backUrl", Router.reverse("lelang.LelangCtr.tutup").add("id", id));
        renderTemplate("lelang/penawaran-ulang.html");
    }

    /**
     * aktivitas pemasukan penawaran ulang hak akses Panitia dan peserta
     *
     */
    @AllowAccess({Group.PANITIA})
    public static void penawaranUlangPra(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("lelang", lelang);
        renderArgs.put("list", Jadwal.findByLelang(id));
        TahapStarted tahapStarted = lelang.getTahapStarted();
        boolean editable = tahapStarted.isKirimPersyaratKualifikasi() && lelang.isPrakualifikasi();
        Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.AMBIL_DOK_PRA);
        renderArgs.put("urutStart", editable && jadwal != null ? jadwal.akt_urut : 0);
        renderArgs.put("editable", editable);
        renderArgs.put("backUrl", Router.reverse("lelang.LelangCtr.praGagal").add("id", id));
        renderTemplate("lelang/penawaran-ulang.html");
    }

    /**
     * submit setting jadwal untuk pemasukan penawaran ulang
     *
     * @param id
     * @param jadwalList
     */
    @AllowAccess({Group.PANITIA})
    public static void submit_penawaran_ulang(Long id, List<Jadwal> jadwalList, String keterangan, boolean harus_berubah) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        boolean valid = true;
        Paket paket = Paket.findByLelang(id);
        boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
        List<Anggota_panitia> anggotaPanitia = null;
        boolean ulangPra = lelang.getTahapStarted().isAllowEvaluasiUlangPra() && lelang.isPrakualifikasi();
        if (isOSD) {
            anggotaPanitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
            ServiceResult result = OSDUtil.validCertPanitia(anggotaPanitia, lelang, jadwalList);
            if (!result.isSuccess()) {
                flash.error(result.getMessage());
                Jadwal.paramFlash(jadwalList);
                penawaranUlang(id);
            }
            valid = result.isSuccess();
        }
        if (valid) {
            if (harus_berubah) {
                validation.min(keterangan.length(), 31).key("keterangan").message("Alasan minimal 30 karakter");
            }
            Tahap tahap = Jadwal.findTahapPemasukanPenawaranUlang(lelang);
            Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, tahap);
            Jadwal.validate(lelang, jadwalList, newDate(), keterangan, jadwal.akt_urut);
            if (validation.hasErrors()) {
                validation.keep();
                Jadwal.paramFlash(jadwalList);
            }
            else {
                try {
                    Jadwal.simpanJadwal(lelang, jadwalList, newDate(), keterangan);
                    if (isOSD && lelang.lls_status.isAktif() && !lelang.isLelangV3()) {
                        OSDUtil.createKPL(Paket.findById(lelang.pkt_id), anggotaPanitia, lelang, getRequestHost());
                    }
                    lelang.lls_penawaran_ulang = 1;
                    lelang.lls_tanggal_pembukaan_admteknis = null;
                    lelang.lls_tanggal_pembukaan_harga = null;
                    lelang.save();
                    if(ulangPra) {
                        Evaluasi.evaluasiUlangKualifikasi(lelang);
                        List<Peserta> pesertaList = lelang.getPesertaList();
                        if (!CollectionUtils.isEmpty(pesertaList)) {
                            for (Peserta peserta : pesertaList) {
                                peserta.resetPenawaranPrakualifikasi();
                            }
                        }
                    } else {
                        Evaluasi.evaluasiUlang(lelang);
                        // reseting penawaran dan dok_penawaran peserta
                        List<Peserta> pesertaList = lelang.getPesertaList();
                        if (!CollectionUtils.isEmpty(pesertaList)) {
                            for (Peserta peserta : pesertaList) {
                                peserta.resetPenawaran(lelang.isPrakualifikasi());
                            }
                        }
                    }
                    flash.success("Data Jadwal Tender berhasil tersimpan");
                } catch (Exception e) {
                    Logger.error(e, "Simpan Jadwal gagal");
                    flash.error("Data Jadwal Tender gagal tersimpan");
                    Jadwal.paramFlash(jadwalList);
                }
            }
        } else { // sertifikat tidak valid
            Logger.error("Tidak bisa simpan jadwal, pastikan seluruh panitia memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
            flash.error("Tidak bisa simpan jadwal, pastikan seluruh panitia memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
            Jadwal.paramFlash(jadwalList);
        }
        if(ulangPra)
            penawaranUlangPra(id);
        else
            penawaranUlang(id);
    }

    @AllowAccess({Group.PANITIA})
    public static void viewLisLelang(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        renderArgs.put("lls_id", id);
        renderTemplate("lelang/lelang-list.html");
    }

    // fungsi untuk fitur ubah lelang
    @AllowAccess({Group.PANITIA})
    public static void updateLelang(Long id) {
        Lelang_detil lelang = Lelang_detil.findById(id);
        Paket paket = Paket.findByLelang(lelang.lls_id);
        renderArgs.put("lelang", lelang);
        renderArgs.put("paket", paket);
        Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);

        if (dok_lelang != null) {
            SskkContent sskkContent = dok_lelang.getSskkContent();
            renderArgs.put("sskkContent", sskkContent);
        }

        List<Paket_lokasi> lokasiList = Paket_lokasi.find("pkt_id=?", paket.pkt_id).fetch();
        renderArgs.put("lokasiList", lokasiList);

        // history perubahan
        UpdateLelangHistory updateLelangHistory = UpdateLelangHistory.getByLelang(lelang.lls_id);
        Map<String, Object> historyMap = new HashMap<String, Object>();
        List<Paket_lokasi> historyLokasiList = new ArrayList<Paket_lokasi>();
        Boolean isHistory = false;

        if (updateLelangHistory != null) {
            isHistory = true;
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(updateLelangHistory.getUlh_before()).getAsJsonObject();

            if (jsonObject != null) {
                Locale indo = new Locale("id", "ID");
                NumberFormat defaultFormat = NumberFormat.getCurrencyInstance(indo);
                historyMap.put("pkt_nama", jsonObject.get("pkt_nama"));
                Ppk ppk = Ppk.findById(jsonObject.get("ppk_id").getAsLong());
                historyMap.put("ppk_nama", ppk != null ? ppk.getPegawai().peg_nama : "");

                if (lelang.isLelangV3()) {
                    historyMap.put("pkt_hps", defaultFormat.format(jsonObject.get("pkt_hps").getAsDouble()));
                }

                historyMap.put("pkt_pagu", defaultFormat.format(jsonObject.get("pkt_pagu").getAsDouble()));
                Kualifikasi kualifikasi = jsonObject.get("kls_id") != null ? Kualifikasi
                        .findById(jsonObject.get("kls_id").getAsString()) : null;

                historyMap.put("kls_id", kualifikasi != null ? kualifikasi.label : "");

                JenisKontrak jenisKontrak = JenisKontrak.findById(jsonObject.get("lls_kontrak_pembayaran").getAsInt());
                historyMap.put("kontrak_pembayaran", jenisKontrak != null ? jenisKontrak.label : "");

                JsonArray jsonArray = jsonObject.get("lokasi").getAsJsonArray();
                if (jsonArray.size() > 0) {
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonObject jsonObject1 = jsonArray.get(i).getAsJsonObject();
                        Paket_lokasi paket_lokasi = new Paket_lokasi();
                        paket_lokasi.kbp_id = Long.parseLong(jsonObject1.get("kbp_id").getAsString());
                        paket_lokasi.pkt_lokasi = jsonObject1.get("pkt_lokasi").getAsString();
                        historyLokasiList.add(paket_lokasi);
                    }
                }
            }
        }

        renderArgs.put("historyMap", historyMap);
        renderArgs.put("historyLokasiList", historyLokasiList);
        renderArgs.put("isHistory", isHistory);
        renderTemplate("lelang/lelang-update.html");
    }

    // fungsi untuk menyimpan hasil ubah lelang
    @AllowAccess({Group.PANITIA})
    public static void simpanUpdateLelang(Long id, Lelang_seleksi lelangSeleksi, Paket paket, Paket_lokasi[] lokasi) {

        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        // set history update
        JsonObject beforeJson = generateJsonObjectForUpdateLelang(lelang, null);

        UpdateLelangHistory updateLelangHistory = new UpdateLelangHistory();
        updateLelangHistory.setLls_id(lelang.lls_id);
        updateLelangHistory.setUlh_before(beforeJson.toString());
        Paket paketUpdt = lelang.getPaket();

        // set data lelang
        // validasi data lelang
        if (validation.required(lelangSeleksi.lls_kontrak_pembayaran).ok) {
            JenisKontrak jenisKontrak = JenisKontrak.findById(lelangSeleksi.lls_kontrak_pembayaran);
            lelang.setKontrakPembayaran(jenisKontrak);
        } else {
            validation.addError("", "Cara pembayaran harus di pilih", "var");
            flash.error("Cara pembayaran harus di pilih");
        }

        // set data paket
        paketUpdt.pkt_pagu = paket.pkt_pagu;
        //paketUpdt.pkt_nama = paket.pkt_nama;
        if (lelang.isLelangV3()) {
            paketUpdt.pkt_hps = paket.pkt_hps;
        }

        // validasi
        if (!validation.required(paketUpdt.pkt_pagu).ok) {
            validation.addError("", "Paket Pagu wajib diisi", "var");
            flash.error("Paket Pagu wajib diisi");
        }

        if (!validation.required(paketUpdt.pkt_hps).ok) {
            validation.addError("", "Nilai HPS wajib diisi", "var");
            flash.error("Nilai HPS wajib diisi");
        }

        if (paket.kls_id != null && !paket.kls_id.equalsIgnoreCase("")) {
            Kualifikasi kualifikasi = Kualifikasi.findById(paket.getKualifikasi().id);
            paketUpdt.setKualifikasi(kualifikasi);
        }

        // check data lokasi
        for (int i = 0; i < lokasi.length; i++) {
            if (!validation.required(lokasi[i].kbp_id).ok) {
                validation.addError("paket_lokasi_" + i + ".kbp_id", "Lokasi wajib diisi", "var");
                flash.error("Lokasi wajib diisi");
            }
            if (!validation.required(lokasi[i].pkt_lokasi).ok) {
                validation.addError("paket_lokasi_" + i + ".pkt_lokasi", "Detail Lokasi wajib diisi", "var");
                flash.error("Detail Lokasi wajib diisi");
            }
        }

        // ----------------------------------------------------------------------

        if (validation.hasErrors()) {
            validation.keep();
        } else {
            lelang.save();
            JsonObject afterJson = generateJsonObjectForUpdateLelang(lelang, null);
            updateLelangHistory.setUlh_after(afterJson.toString());
            updateLelangHistory.save();
            flash.success("Data lelang berhasil di ubah");
        }
        updateLelang(id);
    }

    public static void updateHistoryLelangForPpk(Long lls_id, Long ppk_id) {

        Lelang_seleksi lelang = Lelang_seleksi.findById(lls_id);
        JsonObject beforeJson = generateJsonObjectForUpdateLelang(lelang, ppk_id);
        JsonObject afterJson = generateJsonObjectForUpdateLelang(lelang, null);
        UpdateLelangHistory updateLelangHistory = new UpdateLelangHistory();
        updateLelangHistory.setLls_id(lelang.lls_id);
        updateLelangHistory.setUlh_before(beforeJson.toString());
        updateLelangHistory.setUlh_after(afterJson.toString());
        updateLelangHistory.save();

        updateLelang(lls_id);

    }

    private static JsonObject generateJsonObjectForUpdateLelang(Lelang_seleksi lelang, Long ppk_id) {

        Paket paket = lelang.getPaket();
        JsonObject jsonObject = new JsonObject();

        //jsonObject.addProperty("pkt_nama", paket.pkt_nama);
        jsonObject.addProperty("ppk_id", Paket_anggaran.findPPKByPaket(paket.pkt_id));
        jsonObject.addProperty("pkt_hps", paket.pkt_hps.longValue());
        jsonObject.addProperty("pkt_pagu", paket.pkt_pagu.longValue());
        jsonObject.addProperty("lls_kontrak_pembayaran", lelang.lls_kontrak_pembayaran);
        if (paket.kls_id != null) {
            jsonObject.addProperty("kls_id", paket.getKualifikasi().id);
        }

        List<Paket_lokasi> lokasiList = Paket_lokasi.find("pkt_id=?", paket.pkt_id).fetch();
        if (lokasiList.size() > 0) {
            JsonArray jsonArray = new JsonArray();
            for (Paket_lokasi lokasi : lokasiList) {
                JsonObject jsonObjectArray = new JsonObject();
                jsonObjectArray.addProperty("pkl_id", lokasi.pkl_id);
                jsonObjectArray.addProperty("kbp_id", lokasi.kbp_id);
                jsonObjectArray.addProperty("pkt_lokasi", lokasi.pkt_lokasi);
                jsonArray.add(jsonObjectArray);

            }
            jsonObject.add("lokasi", jsonArray);

        }

        return jsonObject;

    }

    //Forensik RHS
    @AllowAccess({Group.PANITIA})
    public static void viewRhsPermission(Long lelangId) {
        notFoundIfNull(lelangId);
        Lelang_seleksi lelangSeleksi = Lelang_seleksi.findById(lelangId);
        notFoundIfNull(lelangSeleksi);
        renderArgs.put("lelangId", lelangId);
        RhsPermission.insertPermissionIfNull(lelangSeleksi);
        List<RhsPermission> rhsPermissionList = RhsPermission.getByLelang(lelangId);
        renderArgs.put("express", lelangSeleksi.getPemilihan().isLelangExpress());
        renderArgs.put("rhsPermissionList", rhsPermissionList);
        renderTemplate("lelang/viewRhsPermission.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void changeRhsPermission(Long lls_id, Long rpn_id, Long tipe) {
        notFoundIfNull(lls_id);
        notFoundIfNull(rpn_id);
        RhsPermission rhsPermission = RhsPermission.findById(rpn_id);
        notFoundIfNull(rpn_id);
        rhsPermission.setPermissionByType(tipe);
        rhsPermission.save();
        viewRhsPermission(lls_id);
    }

    @AllowAccess({Group.HELPDESK})
    public static void viewForensikRhs() {
        renderArgs.put("rhsPermissionList", RhsPermission.getByPermission());
        renderTemplate("lelang/viewRhsForensik.html");
    }

    @AllowAccess({Group.HELPDESK})
    public static void viewDetailForensikRhs(Long lelangId) {
        renderArgs.put("rhsPermissionList", RhsPermission.getByLelang(lelangId));
        renderTemplate("lelang/viewDetailForensikRhs.html");

    }

    @AllowAccess({Group.HELPDESK})
    public static void downloadRhs(Long pesertaId, Integer jenisDok) throws IOException {
        notFoundIfNull(pesertaId);
        Peserta peserta = Peserta.findBy(pesertaId);
        notFoundIfNull(peserta);
        Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(pesertaId, JenisDokPenawaran.fromValue(jenisDok));
        if (dok_penawaran == null) {
            flash.error("<strong>Terjadi Kesalahan : </strong> Peserta belum upload dok penawaran.");
            viewDetailForensikRhs(peserta.lls_id);
        }
        BlobTable blob = BlobTableDao.getLastById(dok_penawaran.dok_id_attachment);
        String namaFile = peserta.getAdmFileNameByDocumentType(JenisDokPenawaran.fromValue(jenisDok));
        renderRhsFile(blob, namaFile, peserta.lls_id);
    }

    private static void renderRhsFile(BlobTable blob, String namaFile, Long lelangId) {
        try {
            response.contentType = MimeTypes.getContentType(blob.getFileName(), "application/octet-stream");
            Http.Header header = new Http.Header("Content-Disposition", "attachment; filename=\"" + namaFile + "\"");
            response.headers.put("Content-Disposition", header);
            FileInputStream fis = new FileInputStream(blob.getFile());
            renderBinary(fis);
        } catch (Exception e) {
            Logger.error("<strong>Terjadi Kesalahan : </strong>", e.getLocalizedMessage());
            flash.error("<strong>Terjadi Kesalahan : </strong>" + e.getLocalizedMessage());
            viewDetailForensikRhs(lelangId);
        }
    }

    @AllowAccess({Group.HELPDESK})
    public static void downloadPrivateKey(Long pesertaId, Integer jenisDok) {
        Peserta peserta = Peserta.findById(pesertaId);
        Paket paket = Paket.findByLelang(peserta.lls_id);
        boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
        JenisDokPenawaran jenis = JenisDokPenawaran.fromValue(jenisDok);
        String key = null;
        if (isOSD) {
            try {
                key = KmsClient.getKPLXd(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id, paket.ttp_ticket_id);
            } catch (Exception e) {
            }
        } else {
            key = Lelang_key.getPrivateKey(jenis, peserta.lls_id);
        }
        if (key != null) {
            try {
                String namaFile = "";
                switch (jenisDok) {
                    case 1:
                        namaFile = peserta.lls_id + "_kunci-private_adm_teknis.txt";
                        break;
                    case 2:
                        namaFile = peserta.lls_id + "_kunci-private_harga.txt";
                        break;
                    case 3:
                        namaFile = peserta.lls_id + "_kunci-private_adm_teknis_harga.txt";
                        break;
                    default:
                        namaFile = "";
                        break;
                }

                response.contentType = MimeTypes.getContentType(namaFile, "application/octet-stream");
                Http.Header header = new Http.Header("Content-Disposition", "attachment; filename=" + namaFile);
                response.headers.put("Content-Disposition", header);
                InputStream is = new ByteArrayInputStream(key.getBytes());

                renderBinary(is);

            } catch (Exception e) {
                Logger.error("<strong>Terjadi Kesalahan : </strong>" + e.getLocalizedMessage(), e.getLocalizedMessage());
                flash.error("<strong>Terjadi Kesalahan : </strong>" + e.getLocalizedMessage());

                viewDetailForensikRhs(peserta.lls_id);
            }
        } else {
            flash.error("<strong>Terjadi Kesalahan : </strong> Key tidak ditemukan.");
            viewDetailForensikRhs(peserta.lls_id);
        }
    }

    @AllowAccess({Group.HELPDESK})
    public static void downloadStruk(Long pesertaId, Integer jenisDok) throws IOException {
        Peserta peserta = Peserta.findBy(pesertaId);
        Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(pesertaId, JenisDokPenawaran.fromValue(jenisDok));
        if (dok_penawaran != null && dok_penawaran.dok_struk_id != null) {
            BlobTable blob = BlobTableDao.getLastById(dok_penawaran.dok_struk_id);
            String namaFile = dok_penawaran.dok_struk_nama;
            renderRhsFile(blob, namaFile, peserta.lls_id);
        } else {
            flash.error("<strong>Terjadi Kesalahan : </strong> Struk belum dikirim ke server.");
            viewDetailForensikRhs(peserta.lls_id);
        }
    }

    @AllowAccess({Group.HELPDESK})
    public static void removeForensikRhs(List<Long> chkForensik) {
        if (!CommonUtil.isEmpty(chkForensik)) {
            for (Long lelangId : chkForensik) {
                List<RhsPermission> rhsPermissionList = RhsPermission.getByLelang(lelangId);
                for (RhsPermission rhsPermission : rhsPermissionList) {
                    rhsPermission.rpn_allow_harga = 0;
                    rhsPermission.rpn_allow_teknis = 0;
                    rhsPermission.save();
                }
            }
        }

        flash.success("%s berhasil dihapus", "Data");
        viewForensikRhs();
    }

    public static void sendRhsForensik(Long lelangId) throws IOException {
        notFoundIfNull(lelangId);
        try {
            final String result = RhsPermission.sendForensik(lelangId);
            if (result.equalsIgnoreCase("success"))
                flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil kirim forensik");
            else
                flash.error("<i class=\"fa fa-cross\"></i>&nbsp;&nbsp;<b>Gagal kirim forensik :</b>" + result);
        } catch (Exception e) {
            Logger.error("Terjadi Kesalahan Koneksi Ke Spse Helpdesk : " + e.getLocalizedMessage());
            flash.error("<i class=\"fa fa-cross\"></i>&nbsp;&nbsp;<b>Terjadi Kesalahan Koneksi Ke Spse Helpdesk : </b>" + e.getLocalizedMessage());
        }
        viewDetailForensikRhs(lelangId);
    }

    @AllowAccess({Group.REKANAN, Group.AUDITOR, Group.PPK})
    public static void penawaran(Long id, int jenis) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("jenis", jenis);
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapAktif", lelang.getTahapNow());
        boolean allowed = false;
        if (active_user.isRekanan()) { // untuk rekanan
            Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", lelang.lls_id, active_user.rekananId).first();
            renderArgs.put("peserta", peserta);
            Dok_penawaran penawaran_kualifikasi = Dok_penawaran.findPenawaranPeserta(peserta.psr_id,
                    JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
            boolean kualifikasiUploaded = penawaran_kualifikasi != null || lelang.getPemilihan().isLelangExpress();

            allowed = active_user.isRekanan() && peserta != null && kualifikasiUploaded;
            if (lelang.isSatuFile()) {
                allowed = allowed && jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id;
            } else {
                allowed = allowed && (jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id || jenis == JenisDokPenawaran.PENAWARAN_HARGA.id);
            }
        }
        renderArgs.put("allowed", allowed);
        renderTemplate("lelang/lelang-penawaran.html");
    }

    @AllowAccess({Group.PANITIA, Group.AUDITOR, Group.PPK})
    public static void pembukaan(Long id, int jenis) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Active_user active_user = Active_user.current();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        TahapStarted tahapStarted = lelang.getTahapStarted();
        boolean allowed = (active_user.isPanitia() || active_user.isAuditor())
                && !lelang.isLelangV3() && tahapStarted.isPembukaan();
        if (lelang.isSatuFile()) {
            allowed = allowed && jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id;
        } else {
            allowed = allowed && (jenis == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id || jenis == JenisDokPenawaran.PENAWARAN_HARGA.id);
        }
        renderArgs.put("jenis", jenis);
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapStarted", tahapStarted);
        renderArgs.put("allowed", allowed);
        renderTemplate("lelang/lelang-pembukaan.html");
    }

    /**
     * Tambah Jadwal Kualifikasi
     */
    @AllowAccess({Group.PANITIA})
    public static void tambahJadwalKualifikasi(Long id){
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        if(lelang.jumlahPenawarKualifikasi() != lelang.getJumlahPesertaTelahEvaluasiKualifikasi()){
            flash.error("Masih ada Peserta yang belum Evaluasi Kualifikasi. ");
            EvaluasiCtr.index(id);
        }
        renderArgs.put("editable", !lelang.is_kualifikasi_tambahan);
        renderArgs.put("lelang", lelang);
        renderArgs.put("list", Jadwal.findByLelang(id));

        Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_DOK_PRA);
        int urutStart = jadwal != null ? jadwal.akt_urut : 0;
        renderArgs.put("urutStart", urutStart);
        renderTemplate("lelang/tambah-jadwal-kualifikasi.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void submitTambahKualifikasi(Long id, List<Jadwal> jadwalList, String keterangan) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        boolean valid = true;
        Paket paket = Paket.findByLelang(id);
        boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);

        List<Anggota_panitia> anggotaPanitia = null;
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        if (isOSD) {
            anggotaPanitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
            ServiceResult result = OSDUtil.validCertPanitia(anggotaPanitia, lelang, jadwalList);
            if (!result.isSuccess()) {
                flash.error(result.getMessage());
                tambahJadwalKualifikasi(id);
            }
            valid = result.isSuccess();
        }

        if (valid) {
            boolean harusBerubah = params._contains("harus_berubah");
            if (harusBerubah) {
                validation.min(keterangan.length(), 31).key("keterangan").message("Alasan minimal 30 karakter");
            }
            Jadwal.validate(lelang, jadwalList, newDate(), keterangan, 1);
            if (validation.hasErrors()) {
                validation.keep();
            } else {
                try {
                    Jadwal.simpanJadwal(lelang, jadwalList, newDate(), keterangan);
                    if (isOSD && lelang.lls_status.isAktif() && !lelang.isLelangV3()) {
                        OSDUtil.createKPL(Paket.findById(lelang.pkt_id), anggotaPanitia, lelang, getRequestHost());
                    }
                    lelang.is_kualifikasi_tambahan = true;
                    lelang.save();
                    flash.success("Data Jadwal Tender berhasil tersimpan");
                } catch (Exception e) {
                    Logger.error(e, "Simpan Jadwal gagal");
                    flash.error("Data Jadwal Tender gagal tersimpan");
                }
            }
        } else { // sertifikat tidak valid
            Logger.error("Tidak bisa simpan jadwal, pastikan seluruh panitia memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
            flash.error("Tidak bisa simpan jadwal, pastikan seluruh panitia memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
        }

        tambahJadwalKualifikasi(id);
    }

    @AllowAccess({Group.PANITIA})
    public static void metode(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        Kategori kategori = lelang.getPaket().kgr_id;
        boolean sedangPersetujuan = Persetujuan.isSedangPersetujuan(lelang.lls_id);
        renderArgs.put("draft", !lelang.isLelangV3() && lelang.lls_status.isDraft() && !sedangPersetujuan);
        renderArgs.put("lelang", lelang);
        renderArgs.put("kategori", kategori);
        renderArgs.put("kategoriList", Kategori.all);
        renderArgs.put("metodePemilihanList", kategori.isConsultant() || kategori.isJkKonstruksi() ? MetodePemilihan.metodePemilihanKonsultan:MetodePemilihan.metodePemilihanTender);
        renderArgs.put("metodeKualifikasiList", lelang.getMetodeKualifikasiList());
        renderArgs.put("metodeDokumenList", lelang.getMetodeDokumenList());
        renderArgs.put("metodeEvaluasiList", MetodeEvaluasi.findListBy(kategori, lelang.getMetode()));
        renderTemplate("lelang/pilih-metode.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void metodeSubmit(Long id, Integer kategoriId, Integer mtdPemilihan,  Integer mtdKualifikasi, Integer mtdDokumen, Integer mtdEvaluasi) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi obj = Lelang_seleksi.findById(id);
        Kategori kategori = Kategori.findById(kategoriId);
        MetodeKualifikasi metodeKualifikasi = MetodeKualifikasi.fromValue(mtdKualifikasi);
        MetodeDokumen metodeDokumen = MetodeDokumen.fromValue(mtdDokumen);
        MetodeEvaluasi metodeEvaluasi = MetodeEvaluasi.findById(mtdEvaluasi);
        Metode metode = Metode.findById(kategori, metodeKualifikasi, metodeDokumen, metodeEvaluasi);
//        Logger.info("metode : %s, %s", metode.id, obj.mtd_id);
        // lelang ganti metode, seharusnya delete jadwal existing
        if ((obj.mtd_id != null && !obj.mtd_id.equals(metode.id))) {
            History_jadwal.delete("dtj_id in (select dtj_id from jadwal where lls_id=?)", obj.lls_id);
            Jadwal.delete("lls_id=?", obj.lls_id);
        }
        obj.mtd_pemilihan = mtdPemilihan;// old metode to new metode
        obj.mtd_id = metode.id;
        obj.mtd_evaluasi = mtdEvaluasi;
        if (obj.isExpress()) { // jika lelang cepat tak mungkin ada tahap kontrak, jika ada harus delete semua jadwal
            Jadwal kontrak = Jadwal.findByLelangNTahap(obj.lls_id, Tahap.TANDATANGAN_KONTRAK);
            if (kontrak != null) {
                Jadwal.delete("lls_id=?", obj.lls_id);
            }
            //jika lelang cepat metode lelang Pasca Satu File Sistem Gugur
            obj.mtd_id = Metode.PASCA_SATU_FILE.id;
        } else { // jika bukan lelang cepat pasti ada jadwal kontrak
            Jadwal kontrak = Jadwal.findByLelangNTahap(obj.lls_id, Tahap.TANDATANGAN_KONTRAK);
            if (kontrak == null) {
                Jadwal.delete("lls_id=?", obj.lls_id);
            }
        }
        Paket paket = obj.getPaket();
        if ((paket.kgr_id != null && !paket.kgr_id.equals(kategori))) {
        		List<Integer> syarat_kualifikasi = new ArrayList<Integer>(JenisChecklist.CHECKLIST_KUALIFIKASI_BARU);
        		syarat_kualifikasi.addAll(JenisChecklist.CHECKLIST_SYARAT);
        		String jenis_checklist = StringUtils.join(syarat_kualifikasi, ',');
    			Checklist.delete("dll_id in (select dll_id from dok_lelang where lls_id=?)  and ckm_id in (select ckm_id from checklist_master where ckm_jenis in ("+jenis_checklist+"))", obj.lls_id);
        }
        paket.kgr_id = Kategori.findById(kategoriId);
        paket.save();
        obj.lls_metode_penawaran = obj.isExpress() && !obj.isItemized()? Lelang_seleksi.MetodePenawaranHarga.AUCTION : Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION;
        obj.save();
        edit(id);
    }
    
    @AllowAccess({Group.PANITIA})
    public static void praGagal(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        notFoundIfNull(lelang);
        if(lelang.isDitutup())
            forbidden("Maaf Tender sudah dibatalkan");
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapNow", Jadwal.getJadwalSekarang(id, true, lelang.getPemilihan().isLelangExpress(), lelang.isLelangV3()));
        boolean belumPersetujuan = !Persetujuan.isSedangPersetujuanPraGagalTender(id);
        renderArgs.put("belumPersetujuan", belumPersetujuan);
        Persetujuan persetujuan = Persetujuan.findByPegawaiLelangInJenisBatalPra(lelang.lls_id);
        JenisChecklist jenis = JenisChecklist.CHECKLIST_PRAKUALFIKASI_BATAL;
        renderArgs.put("persetujuan",persetujuan);
        renderArgs.put("checklist" , belumPersetujuan ? Checklist_gagal.getChecklist(jenis) : Checklist_gagal.findBy(id, jenis));
        if(persetujuan != null) {
            renderArgs.put("alasanLabel", persetujuan.pst_jenis == JenisPersetujuan.PRAKUALIFIKASI_BATAL ? "Alasan Prakualifikasi Batal":"Alasan Prakualifikasi Ulang");
            renderArgs.put("allowPersetujuan",persetujuan.pst_status.isBelumSetuju() || persetujuan.pst_status.isTidakSetuju());
            renderArgs.put("allowBatalPersetujuan", persetujuan.pst_status.isSetuju() && !belumPersetujuan && lelang.lls_status.isAktif());
        }
        renderTemplate("lelang/lelang-gagal-pra.html");
    }
    
    @AllowAccess({Group.PANITIA})
    public static void praGagalSubmit(Long id, String alasan, String actionLelang, Boolean setuju, String alasanTidakSetuju, List<Checklist_gagal> checklistKategori) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        boolean emptyChecklist = true;
		if (checklistKategori != null) {
			for (int i = 0; i < checklistKategori.size(); i++) {
				if (checklistKategori.get(i).ckm_id != null) {
					emptyChecklist = false;
					validation.valid(checklistKategori.get(i));
				}
			}
		}
        if(setuju == null){
            setuju = true;
        }

        if (!setuju && CommonUtil.isEmpty(alasanTidakSetuju)) {
            Validation.addError("alasanTidakSetuju", "Alasan wajib diisi");
        }else if (!setuju && !CommonUtil.isEmpty(alasanTidakSetuju) && alasanTidakSetuju.length() <= 30) {
            Validation.addError("alasanTidakSetuju", "Alasan minimal 30 karakter");
        }

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            praGagal(id);
        } else {
            try {
                Lelang_seleksi lelang = Lelang_seleksi.findById(id);
                notFoundIfNull(lelang);
                if(lelang.isDitutup())
                    forbidden("Maaf Tender sudah dibatalkan");
                JenisPersetujuan jenis = JenisPersetujuan.PRAKUALIFIKASI_BATAL;
                lelang.lls_ditutup_karena = alasan;
                if(actionLelang.equals("pra_ulang")){
                    jenis = JenisPersetujuan.PRAKUALIFIKASI_ULANG;
                    lelang.lls_diulang_karena = alasan;
                }
                lelang.save();
                Persetujuan.simpanPersetujuanBatalTender(lelang,setuju,alasanTidakSetuju,jenis);
                Checklist_gagal.simpanChecklist(lelang, checklistKategori, Checklist_master.JenisChecklist.CHECKLIST_PRAKUALFIKASI_BATAL, Integer.valueOf(1));
                if (Persetujuan.isApprove(id, jenis)) {
                    if (jenis.equals(JenisPersetujuan.BATAL_LELANG)) {
                        try {
                            lelang.tutupLelang();
                            BerandaCtr.index();
                        } catch (Exception e) {
                            Logger.error(e, "Kesalahan Batal tender %s", e.getLocalizedMessage());
                            flash.error("Proses batal lelang tidak dapat dilakukan. mohon ulangi");
                        }
                    }
                    else if (jenis.equals(JenisPersetujuan.ULANG_LELANG)) {
                        try {
                            lelang.tutupLelang();
                            Long lelangIdBaru = Lelang_seleksi.ulangLelang(id);
                            if (lelang.getPemilihan().isLelangExpress())
                                edit(lelangIdBaru);
                            else
                                edit(lelangIdBaru);
                        } catch (Exception e) {
                            Logger.error(e,"Kesalahan lelangUlang %s", e.getLocalizedMessage());
                            flash.error("Proses mengulang lelang tidak dapat dilakukan. mohon ulangi");
                        }
                    }
                }
            }catch (Exception e) {
                Logger.error(e,"Kesalahan Batal tender %s", e.getLocalizedMessage());
                flash.error(e.getMessage());
            }
        }
        praGagal(id);
    }
    
}
