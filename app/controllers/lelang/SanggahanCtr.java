package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import models.common.Active_user;
import models.common.MailQueueDao;
import models.common.Tahap;
import models.common.TahapNow;
import models.common.TahapStarted;
import models.lelang.*;
import models.lelang.Checklist_master.JenisChecklist;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * definisikan semua action terkait sanggahan
 * 
 * @author arief ardiyansah
 *
 */
public class SanggahanCtr extends BasicCtr {

	@AllowAccess({Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK})
	public static void index(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Active_user active_user = Active_user.current();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
        TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("tahapStarted", tahapStarted);
		TahapNow tahapAktif = lelang.getTahapNow();
        JadwalSanggahBanding jdwl_banding  =JadwalSanggahBanding.findByLelang(id);
        if(jdwl_banding == null){
            Integer versi = Evaluasi.findCurrentVersi(id);
            jdwl_banding = JadwalSanggahBanding.create(lelang);
        }else{
            notFoundIfNull(jdwl_banding);
        }
        if(jdwl_banding != null) {
			renderArgs.put("jdwl_banding", jdwl_banding);
			renderArgs.put("sisaWaktu", jdwl_banding.sisaWaktu());
			renderArgs.put("status", jdwl_banding.sgh_status);
		}
        boolean hide = !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca());
		renderArgs.put("tahapEvaluasi",tahapStarted.isEvaluasi());
		renderArgs.put("tahapPenjelasan", tahapStarted.isPenjelasan());
		renderArgs.put("tahapPenjelasanPra", tahapStarted.isPenjelasanPra());
		renderArgs.put("tahapPengumuman", tahapStarted.isPengumuman());
		Tahap tahap = Tahap.SANGGAH;
		boolean sanggahPra = false;
		renderArgs.put("not_allowed", active_user.isRekanan() && !Dok_penawaran.isAnyPenawaran(id, active_user.rekananId));
		renderArgs.put("sanggahPra", sanggahPra);
		Group group = active_user.group;
		renderArgs.put("group", group);
		if(active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			boolean allow_kirim_sanggah = Sanggahan.isAllowKirimSanggah(lelang, group, tahap, peserta, newDate()) && lelang.lls_status.isAktif();
			boolean allow_kirim_sanggah_banding = Sanggahan.isAllowKirimSanggahBanding(lelang, group, tahap, peserta, newDate()) && lelang.lls_status.isAktif();
			boolean sudah_banding = Sanggah_banding.isSudahBanding(peserta, tahap);
			int count_jawaban = Sanggahan.countJawabanSanggahan(tahap, peserta.psr_id);
			renderArgs.put("allow_kirim_sanggah", allow_kirim_sanggah);
			renderArgs.put("allow_kirim_sanggah_banding", allow_kirim_sanggah_banding && !sudah_banding && count_jawaban > 0 && jdwl_banding.isSedangProses() && !(jdwl_banding.isSelesai()));			
			renderArgs.put("allow_sanggah_banding", !sanggahPra && count_jawaban > 0);
			if (allow_kirim_sanggah)
				renderArgs.put("checklist", Checklist_sanggah.getChecklist(JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN));
		}
		int count_sanggahan = Sanggahan.countSanggahan(lelang.lls_id, tahap);
		renderArgs.put("alllow_atur_jadwal_sanggah", count_sanggahan > 0);
		renderArgs.put("tahapAktif", tahapAktif.isSanggah());
		renderArgs.put("ket_jawab", !active_user.isRekanan() && hide);
		renderArgs.put("simpanUrl", Router.reverse("lelang.SanggahanCtr.simpan").add("id", lelang.lls_id).url);
		renderArgs.put("simpanBandingUrl", Router.reverse("lelang.SanggahanCtr.simpanBanding").add("id", lelang.lls_id).url);
		renderArgs.put("listUrl", Router.reverse("lelang.SanggahanCtr.list").add("id", lelang.lls_id).url);
		renderTemplate("lelang/sanggahan/sanggahan.html");
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void list(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.SANGGAH;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("panitia", lelang.getPanitia());
		Active_user active_user = Active_user.current();
		renderArgs.put("sanggahPra", tahap == Tahap.SANGGAH_PRA);
		renderArgs.put("allowSanggahBanding", lelang.getKategori().isKonstruksi());
		boolean allow_balas = Sanggahan.isAllowBalas(active_user.group, id, tahap, newDate())
				&& lelang.lls_status.isAktif();
		renderArgs.put("allow_balas", allow_balas);
		if (active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			renderArgs.put("sanggahanList", Sanggahan.findBy(id, tahap, peserta.psr_id));
			renderArgs.put("sanggah_banding_list", Sanggah_banding.findByPeserta(peserta.psr_id));
		} else {
			renderArgs.put("sanggahanList", Sanggahan.findBy(id, tahap, null));
			renderArgs.put("sanggah_banding_list", Sanggah_banding.findBy(id));
		}

		renderTemplate("lelang/sanggahan/sanggahan-list.html");
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void listPra(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.SANGGAH_PRA;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("panitia", lelang.getPanitia());
		Active_user active_user = Active_user.current();
		renderArgs.put("sanggahPra", tahap == Tahap.SANGGAH_PRA);
		renderArgs.put("allow_balas",
				Sanggahan.isAllowBalas(active_user.group, id, tahap, newDate()) && lelang.lls_status.isAktif());
		if (active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			renderArgs.put("sanggahanList", Sanggahan.findBy(id, tahap, peserta.psr_id));
		} else {
			renderArgs.put("sanggahanList", Sanggahan.findBy(id, tahap, null));
		}
		renderArgs.put("sanggah_banding_list", Sanggah_banding.findBy(id));
		renderTemplate("lelang/sanggahan/sanggahan-list.html");
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void listAdmTeknis(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.SANGGAH_ADM_TEKNIS;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("panitia", lelang.getPanitia());
		Active_user active_user = Active_user.current();
		renderArgs.put("sanggahPra", tahap == Tahap.SANGGAH_PRA);
		renderArgs.put("allow_balas",
				Sanggahan.isAllowBalas(active_user.group, id, tahap, newDate()) && lelang.lls_status.isAktif());
		if (active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			renderArgs.put("sanggahanList", Sanggahan.findBy(id, tahap, peserta.psr_id));
		} else {
			renderArgs.put("sanggahanList", Sanggahan.findBy(id, tahap, null));
		}
		renderArgs.put("sanggah_banding_list", Sanggah_banding.findBy(id));
		renderTemplate("lelang/sanggahan/sanggahan-list.html");
	}

	/**
	 * halaman sanggah prakualifikasi
	 * 
	 * @param id
	 */
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void kualifikasi(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Active_user active_user = Active_user.current();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		boolean hide = !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca());
		renderArgs.put("tahapEvaluasi", tahapStarted.isEvaluasi());
		renderArgs.put("tahapPenjelasan", tahapStarted.isPenjelasan());
		renderArgs.put("tahapPenjelasanPra", tahapStarted.isPenjelasanPra());
		renderArgs.put("tahapPengumuman", tahapStarted.isPengumuman());
		Tahap tahap = Tahap.SANGGAH_PRA;
		renderArgs.put("sanggahPra", true);
		Group group = active_user.group;
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("id", lelang.lls_id);
		if (active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			boolean allow_kirim_sanggah = Sanggahan.isAllowKirimSanggah(lelang, group, tahap, peserta, newDate()) && lelang.lls_status.isAktif();
			renderArgs.put("allow_kirim_sanggah", allow_kirim_sanggah);
			if (allow_kirim_sanggah)
				renderArgs.put("checklist", Checklist_sanggah.getChecklist(JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN));
		}
		renderArgs.put("ket_jawab", !active_user.isRekanan() && hide);
		renderArgs.put("simpanUrl", Router.reverse("lelang.SanggahanCtr.simpanPra", params));
		renderArgs.put("listUrl", Router.reverse("lelang.SanggahanCtr.listPra", params));
		renderArgs.put("group", group);
		renderTemplate("lelang/sanggahan/sanggahan.html");
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void admTeknis(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Active_user active_user = Active_user.current();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		boolean hide = !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca());
		renderArgs.put("tahapEvaluasi", tahapStarted.isEvaluasi());
		renderArgs.put("tahapPenjelasan", tahapStarted.isPenjelasan());
		renderArgs.put("tahapPenjelasanPra", tahapStarted.isPenjelasanPra());
		renderArgs.put("tahapPengumuman", tahapStarted.isPengumuman());
		Tahap tahap = Tahap.SANGGAH_ADM_TEKNIS;
		Group group = active_user.group;
		renderArgs.put("group", group);
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("id", lelang.lls_id);
		if (active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			boolean allow_kirim_sanggah = Sanggahan.isAllowKirimSanggah(lelang, group, tahap, peserta, newDate())
					&& lelang.lls_status.isAktif();
			renderArgs.put("allow_kirim_sanggah", allow_kirim_sanggah);
			if (allow_kirim_sanggah)
				renderArgs.put("checklist", Checklist_sanggah.getChecklist(JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN));
		}
		renderArgs.put("ket_jawab", !active_user.isRekanan() && hide);
		renderArgs.put("simpanUrl", Router.reverse("lelang.SanggahanCtr.simpanAdmTeknis", params));
		renderArgs.put("listUrl", Router.reverse("lelang.SanggahanCtr.listAdmTeknis", params));
		renderTemplate("lelang/sanggahan/sanggahan.html");
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN })
	public static void simpan(Long id, List<Checklist_sanggah> checklistKategori, String uraian, @Valid @DokumenType File file, Long sanggahan_id) {
		Active_user active_user = Active_user.current();
		otorisasiDataLelang(id); // check otorisasi data lelang
		boolean emptyChecklist = true;
		if (checklistKategori != null) {
			for (int i = 0; i < checklistKategori.size(); i++) {
				if (checklistKategori.get(i).ckm_id != null) {
					emptyChecklist = false;
					validation.valid(checklistKategori.get(i));
				}
			}
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			StringBuilder sb = new StringBuilder();
			for (play.data.validation.Error err : validation.errors()) {
				if (!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}
			flash.error("Data gagal tersimpan:" + sb);
		} else {
			if(active_user.isRekanan()) {
				if (emptyChecklist)
					flash.error("Kategori sanggahan wajib diisi!");
			}
			try {
				Sanggahan sanggahan = Sanggahan.simpan(id, Tahap.SANGGAH, uraian, file, sanggahan_id, newDate());
				if(active_user.isRekanan())
					Checklist_sanggah.simpanChecklist(sanggahan, checklistKategori, Checklist_master.JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN, Integer.valueOf(1)); 
				// notif
				Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(id);
				if (evaluasi != null) {
					List<Nilai_evaluasi> pesertaList = evaluasi.findPesertaList();
					MailQueueDao.kirimNotifikasiSanggahan(Lelang_seleksi.findById(id), pesertaList);
				}
			} catch (Exception e) {
				Logger.error(e, "@simpan sanggahan %s", e.getLocalizedMessage());
				flash.error(e.getLocalizedMessage());
			}
		}
		index(id);
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN })
	public static void simpanPra(Long id, List<Checklist_sanggah> checklistKategori, String uraian, @Valid @DokumenType File file, Long sanggahan_id) {
		Active_user active_user = Active_user.current();
		otorisasiDataLelang(id); // check otorisasi data lelang
		boolean emptyChecklist = true;
		if (checklistKategori != null) {
			for (int i = 0; i < checklistKategori.size(); i++) {
				if (checklistKategori.get(i).ckm_id != null) {
					emptyChecklist = false;
					validation.valid(checklistKategori.get(i));
				}
			}
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			StringBuilder sb = new StringBuilder();
			for (play.data.validation.Error err : validation.errors()) {
				if (!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}
			flash.error(Messages.get("flash.dgt") + sb);
		} else {
			if(active_user.isRekanan()) {
				if (emptyChecklist)
					flash.error("Kategori sanggahan wajib diisi!");
			}
			try {
				Sanggahan sanggahan = Sanggahan.simpan(id, Tahap.SANGGAH_PRA, uraian, file, sanggahan_id, newDate());
				if(active_user.isRekanan())
					Checklist_sanggah.simpanChecklist(sanggahan, checklistKategori, Checklist_master.JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN, Integer.valueOf(1));
				// notif
				Evaluasi evaluasi = Evaluasi.findPembuktian(id);
				List<Nilai_evaluasi> pesertaList = evaluasi.findPesertaLulusEvaluasiList();
				MailQueueDao.kirimNotifikasiSanggahan(Lelang_seleksi.findById(id), pesertaList);
			} catch (Exception e) {
				Logger.error("@simpan sanggahan prakualifikasi %s", e.getLocalizedMessage());
				flash.error(e.getLocalizedMessage());
			}
		}
		kualifikasi(id);
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN })
	public static void simpanAdmTeknis(Long id, String uraian, @Valid @DokumenType File file, Long sanggahan_id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			StringBuilder sb = new StringBuilder();
			for (play.data.validation.Error err : validation.errors()) {
				if (!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}
			flash.error(Messages.get("flash.dgt") + sb);
		} else {
			try {
				Sanggahan.simpan(id, Tahap.SANGGAH_ADM_TEKNIS, uraian, file, sanggahan_id, newDate());
				// notif
				Evaluasi evaluasi = Evaluasi.findTeknis(id);
				List<Nilai_evaluasi> pesertaList = evaluasi.findPesertaList();
				MailQueueDao.kirimNotifikasiSanggahan(Lelang_seleksi.findById(id), pesertaList);
			} catch (Exception e) {
				Logger.error("@simpan sanggahan administrasi dan teknis %s", e.getLocalizedMessage());
				flash.error(e.getLocalizedMessage());
			}
		}
		admTeknis(id);
	}

	@AllowAccess({ Group.PANITIA, Group.REKANAN })
	public static void simpanBanding(Long id, String uraian, @Valid @DokumenType File file, @DokumenType File datadukung) {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			StringBuilder sb = new StringBuilder();
			for (play.data.validation.Error err : validation.errors()) {
				if (!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}
			flash.error(Messages.get("flash.dgt") + sb);
		} else {
			try {
				Peserta peserta = Peserta.findBy(id, Active_user.current().rekananId);
				Sanggah_banding.simpan(uraian, file, datadukung, peserta.psr_id, newDate(), Tahap.SANGGAH);
				flash.success(Messages.get("flash.sbtt"));
			} catch (Exception e) {
				Logger.error("@lelang.SanggahanCtr.simpanBanding(%d) : %s", id, e.getMessage());
				flash.error(Messages.get("flash.sbgt"));
				e.printStackTrace();

			}
		}
		index(id);
	}
}
