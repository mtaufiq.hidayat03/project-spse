package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.agency.Paket;
import models.agency.Panitia;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.common.TahapNow;
import models.lelang.Evaluasi;
import models.lelang.Lelang_seleksi;
import models.lelang.Nilai_evaluasi;
import models.lelang.Peserta;
import models.secman.Group;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.i18n.Messages;
import play.mvc.Router;
import utils.LogUtil;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author HanusaCloud on 10/11/2018
 */
public class KlarifikasiCtr extends BasicCtr {

    public static final String TAG = "KlarifikasiCtr";
    public static final Map<String, String> MAP_KLARIFIKASI = new HashMap<String, String>(){
        {
            put("UNDANGAN_PEMBUKTIAN", "Pembuktian Kualifikasi");
            put("UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS_HARGA", "Klarifikasi Administrasi, Kualifikasi, Teknis dan Harga");
            put("UNDANGAN_HARGA", "Klarifikasi Harga");
            put("UNDANGAN_KUALIFIKASI", "Klarifikasi Kualifikasi");
            put("UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS", "Klarifikasi Administrasi, Kualifikasi dan Teknis");
            put("UNDANGAN_ADMIN_TEKNIS", "Klarifikasi Administrasi dan Teknis");
            put("UNDANGAN_TEKNISREVISI_HARGA", "Klarifikasi Teknis (Revisi) dan Harga");
            put("UNDANGAN_VERIFIKASI", "Verifikasi");
        }
    };

    /**
     * @param id peserta id
     * @param jenis jenis undangan/pemberitahuan*/
    @AllowAccess({Group.PANITIA})
    public static void kirimPesan(Long id, int jenis) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta); // check otorisasi data peserta
        Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        JenisEmail jenisEmail = JenisEmail.getById(jenis);
        renderArgs.put("peserta", peserta);
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapStarted", lelang.getTahapStarted());
        renderArgs.put("jenisUndangan", jenis);
        renderArgs.put("namaPanitia", lelang.getPanitia().pnt_nama);
        renderArgs.put("list", MailQueueDao.findUndanganEvaluasi(lelang.lls_id, peserta.rkn_id, jenis));
        renderArgs.put("messageTypes", JenisEmail.JenisPesan.values());
        renderArgs.put("key", jenisEmail.name());

        Router.ActionDefinition backUrl = Router.reverse("lelang.EvaluasiCtr.detail").add("id",peserta.psr_id).addRef("kualifikasi");
        if(lelang.isExpress()){
            backUrl = Router.reverse("lelang.EvaluasiCtr.index").add("id",peserta.lls_id);
        }

        renderArgs.put("backUrl", backUrl);
        renderTemplate("pesan/kirim-pesan.html");
    }

  /*  private static String getKeyMessage(Lelang_seleksi lelang, Peserta peserta) {
        TahapNow tahapNow = lelang.getTahapNow();
        Evaluasi harga = Evaluasi.findHarga(lelang.lls_id);
        Evaluasi admin = Evaluasi.findNCreateAdministrasi(lelang.lls_id);

        Nilai_evaluasi nilai_admin = Nilai_evaluasi
                .findNCreateBy(admin.eva_id, peserta,Evaluasi.JenisEvaluasi.EVALUASI_ADMINISTRASI);

        Evaluasi teknis = Evaluasi.findTeknis(lelang.lls_id);
        Nilai_evaluasi nilai_teknis = null;
        if (teknis != null) {
            nilai_teknis = Nilai_evaluasi.findBy(teknis.eva_id, peserta.psr_id);
        }
        Evaluasi kualifikasi = lelang.isPrakualifikasi()
                ? Evaluasi.findNCreateKualifikasi(lelang.lls_id)
                : Evaluasi.findKualifikasi(lelang.lls_id);
        Nilai_evaluasi nilai_kualifikasi = null;
        if (kualifikasi != null) {
            nilai_kualifikasi = Nilai_evaluasi.findBy(kualifikasi.eva_id, peserta.psr_id);
        }
        if (tahapNow.isPembuktian()) {
            return "PEMBUKTIAN_KUALIFIKASI";
        } else if (lelang.isPascaSatuFile()) {
            return "UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS_HARGA";
        } else if ((lelang.isPascaDuaFile() || lelang.isPraDuaFile())
                && tahapNow.isEvaluasiHarga()
                && Nilai_evaluasi.findBy(harga.eva_id, peserta.psr_id) != null) {
            return "UNDANGAN_HARGA";
        }  else if ((lelang.isPraDuaFile() || lelang.isPraDuaTahap()) && peserta.getDokKualifikasi() != null) {
            return "UNDANGAN_KUALIFIKASI";
        } else if ((lelang.isPraDuaFile() || lelang.isPraDuaTahap()) && peserta.getFileTeknis() != null) {
            return "UNDANGAN_ADMIN_TEKNIS";
        } else if (lelang.isPascaDuaFile()
                && (nilai_admin != null || nilai_teknis != null &&  nilai_kualifikasi != null)) {
            return "UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS";
        }
        return "";
    }*/

    @AllowAccess({Group.PANITIA})
    public static void submit(Long id, int jenisUndangan,
                              @Required @As(binder= DatetimeBinder.class) Date waktu,
                              @Required @As(binder=DatetimeBinder.class) Date sampai, @Required String tempat,
                              @Required String dibawa, @Required String hadir,
                              String key, Integer tipe_pesan) {
        checkAuthenticity();
        Date now = newDate();
        if (waktu != null && waktu.before(now)) {
            validation.addError("waktu", Messages.get("flash.wyamsl"));
        }
        if (waktu != null && sampai != null && sampai.before(waktu)) {
            validation.addError("sampai", Messages.get("flash.wsmwm"));
        }
        LogUtil.debug(TAG, "message type: " + tipe_pesan);
        if (tipe_pesan == null || tipe_pesan <= 0) {
            validation.addError("tipe_pesan","Gagal kirim, mohon pilih tipe pesan yang akan Anda kirim!");
        }
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error(Messages.get("flash.gkmcia"));
            kirimPesan(id, jenisUndangan);
        }
        try {
            Peserta peserta = Peserta.findById(id);
            otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
            String namaPaket = Paket.getNamaPaketFromlelang(peserta.lls_id);
            String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
            MailQueueDao.kirimPesanKlarifikasi(
                    peserta.getRekanan(), peserta.lls_id, namaPaket, namaPanitia,
                    waktu, sampai, tempat, dibawa,
                    hadir, jenisUndangan, tipe_pesan, MAP_KLARIFIKASI.get(key));
            flash.success(Messages.get("flash.pbt"));
        } catch(Exception e) {
            flash.error(Messages.get("flaash.tktpp"));
            Logger.error(e, Messages.get("flaash.tktpp"));
        }
        kirimPesan(id, jenisUndangan);
    }

    /**
     * @param id peserta id
     * @param jenis jenis undangan/pemberitahuan*/
    @AllowAccess({Group.PANITIA})
    public static void view(Long id, int jenis) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta); // check otorisasi data peserta
        Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        JenisEmail jenisEmail = JenisEmail.getById(jenis);
        renderArgs.put("peserta", peserta);
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapStarted", lelang.getTahapStarted());
        renderArgs.put("jenisEmail", jenisEmail);
        renderArgs.put("namaPanitia", lelang.getPanitia().pnt_nama);
        renderArgs.put("list", MailQueueDao.findUndanganEvaluasi(lelang.lls_id, peserta.rkn_id, jenis));
        renderTemplate("pesan/list-pesan.html");
    }

}
