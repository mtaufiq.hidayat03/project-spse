package controllers.lelang;

import com.google.gson.JsonObject;
import models.agency.Rincian_hps;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.lelang.AuctionRoom;
import models.lelang.Peserta;
import models.lelang.ReverseAuction;
import models.lelang.ReverseAuctionDetil;
import org.apache.commons.lang3.ArrayUtils;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.WebSocketController;

public class AuctionWebsocketCtr extends WebSocketController {

    public static void join(Long id) {
        Active_user active_user = Active_user.current();
        ReverseAuction auction = ReverseAuction.findByLelang(id);
        Peserta peserta = Peserta.findByRekananAndLelang(active_user.rekananId, id);
        AuctionRoom room = AuctionRoom.get(auction.ra_id);
        if (auction.isSelesai()) {
            disconnect();
            room.clear();
        }
        room.addPeserta(peserta.psr_id, outbound);
        Logger.info("room : %s",room);
        while (inbound.isOpen()) {
            Http.WebSocketEvent e = await(inbound.nextEvent());
            JsonObject result = null;
            if(e.TextFrame.match(e).isPresent()) {
                String msg = e.TextFrame.match(e).get();
                if (msg.equals("update time")) {
                    if (auction.isSelesai()) {
//                            auction.sendPenawaranPeserta();
                        disconnect();
                    } else if (outbound.isOpen()) {
                        result = new JsonObject();
                        result.addProperty("waktu", auction.sisaWaktu());
                        outbound.send(result.toString());
                    }
                } else {
                    JsonObject jsonObject = CommonUtil.fromJson(msg, JsonObject.class);
                    if (jsonObject != null) {
                        result = new JsonObject();
                        Rincian_hps[] data = jsonObject.get("data") != null ? Rincian_hps.fromJson(jsonObject.get("data").toString()) : null;
                        Double total = Rincian_hps.total(data);
                        if (total != null && total == 0.0) {
                            result.addProperty("message", Messages.get("flash.tnptb"));
                            outbound.send(result.toString());
                        } else if (ArrayUtils.isEmpty(data)) {
                            result.addProperty("message", Messages.get("flash.rptbk"));
                            outbound.send(result.toString());
                        } else {
                            ReverseAuctionDetil lastPenawaran = ReverseAuctionDetil.findLastPenawaran(auction.ra_id, peserta.psr_id);
                            ReverseAuctionDetil penawaranTermurah = ReverseAuctionDetil.findPenawaranTermurah(auction.ra_id);
                            if(auction.ra_min_penawaran != null && auction.ra_min_penawaran > 0.0 && auction.ra_min_penawaran <= total) {
                                result.addProperty("message", Messages.get("flash.pnwhlrnhps"));
                                outbound.send(result.toString());
                            } else if (lastPenawaran != null && lastPenawaran.rad_nev_harga > 0.0 && lastPenawaran.rad_nev_harga <= total) {
                                result.addProperty("message", Messages.get("flash.phlrdps"));
                                outbound.send(result.toString());
                            } else if (penawaranTermurah != null && penawaranTermurah.rad_nev_harga > 0.0 && penawaranTermurah.rad_nev_harga.equals(total)) {
                                result.addProperty("message", Messages.get("flash.ptbsdppl"));
                                outbound.send(result.toString());
                            } else {
                                room.simpan(data, total, peserta.psr_id);
                            }
                        }
                    }
                }
                if(e.SocketClosed.match(e).isPresent()) {
                    disconnect();
                }
            }
        }
    }
}
