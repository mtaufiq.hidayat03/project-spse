package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DateBinder;
import ext.DatetimeBinder;
import ext.FormatUtils;
import ext.PdfType;
import models.agency.DaftarKuantitas;
import models.agency.Rincian_hps;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.TempFileManager;
import models.kontrak.Rincian_Barang;
import models.lelang.*;
import models.lelang.Checklist_master.JenisChecklist;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Valid;
import play.templates.Template;
import utils.HtmlUtil;
import utils.JsonUtil;

import java.io.File;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import com.google.gson.reflect.TypeToken;

/**
 * @author Arief
 * Controller terkait proses Adendum
 */
public class AdendumCtr extends BasicCtr {

    /**
     * form Adendum LDK
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void ldk(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        TahapStarted tahapStarted = lelang.getTahapStarted();
        Kategori kategori = lelang.getKategori();
        Kualifikasi kualifikasi = lelang.getPaket().getKualifikasi();
        renderArgs.put("lelang", lelang);
        renderArgs.put("kategori", kategori);
        renderArgs.put("kualifikasi", lelang.getPaket().getKualifikasi());
        Dok_lelang.JenisDokLelang jenis = lelang.isPrakualifikasi() ? Dok_lelang.JenisDokLelang.DOKUMEN_LELANG_PRA: Dok_lelang.JenisDokLelang.DOKUMEN_LELANG;
        Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, jenis);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("editable", dok_lelang.isAllowAdendum(lelang));
        Checklist_tmp.copyChecklist(dok_lelang, JenisChecklist.CHECKLIST_LDK);
        	boolean isSyaratKualifikasiBaru = Checklist_tmp.isSyaratKualifikasiBaru(dok_lelang);
    		if(!kategori.isKonsultansiPerorangan()) {
            	List<Checklist_tmp> ijinList = isSyaratKualifikasiBaru ? 
        			(flash.get("flashError") != null ? Cache.get("ijinList_"+dok_lelang.dll_id, List.class) : Checklist_tmp.getListIjinUsahaBaru(dok_lelang)) 
        			: Checklist_tmp.getListIjinUsaha(dok_lelang);
        		if (flash.get("flashError") != null)
        			Cache.safeDelete("ijinList_"+dok_lelang.dll_id);
        		if (CommonUtil.isEmpty(ijinList)) { // dokumen lelang belum ada
                ijinList = new ArrayList<Checklist_tmp>(1);
                Checklist_tmp checklist = new Checklist_tmp();
                checklist.ckm_id = isSyaratKualifikasiBaru ? 50 : 1;
                ijinList.add(checklist);
            }
            renderArgs.put("ijinList", ijinList);
    		}
    		if (isSyaratKualifikasiBaru) {
        	 	List<Checklist_tmp> syaratAdmin = new ArrayList<Checklist_tmp>();
    	 		List<Checklist_tmp> syaratTeknis = new ArrayList<Checklist_tmp>();
    	 		List<Checklist_tmp> syaratKeuangan = new ArrayList<Checklist_tmp>();    	 		
    	 		if (flash.get("flashError") != null) {
    	 			syaratAdmin = Cache.get("syaratAdmin_"+dok_lelang.dll_id, List.class);
        	 		syaratTeknis = Cache.get("syaratTeknis_"+dok_lelang.dll_id, List.class);
        	 		syaratKeuangan = Cache.get("syaratKeuangan_"+dok_lelang.dll_id, List.class);
        	 		Cache.safeDelete("syaratAdmin_"+dok_lelang.dll_id);
        	 		Cache.safeDelete("syaratTeknis_"+dok_lelang.dll_id);
        	 		Cache.safeDelete("syaratKeuangan_"+dok_lelang.dll_id);
    	 		} else {
    	 			syaratAdmin = Checklist_tmp.getListSyaratKualifikasi(dok_lelang, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, kualifikasi);
        	 		syaratTeknis = Checklist_tmp.getListSyaratKualifikasi(dok_lelang, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, kualifikasi);
        	 		syaratKeuangan = Checklist_tmp.getListSyaratKualifikasi(dok_lelang, kategori, JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, kualifikasi);
    	 		}
    	 		renderArgs.put("syaratAdmin", syaratAdmin);
    			renderArgs.put("syaratTeknis", syaratTeknis);
    			renderArgs.put("syaratKeuangan", syaratKeuangan);
    			renderTemplate("lelang/dokumen/form-ldk-adendum-baru.html");
    		} else {
    			renderArgs.put("syaratList", Checklist_tmp.getListSyaratKualifikasi(dok_lelang, kategori));
    			renderTemplate("lelang/dokumen/form-ldk-adendum.html");
    		}
    }

    /**
     * simpan Form LDK
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void ldkSubmit(Long id, List<Checklist_tmp> checklist, List<Checklist_tmp> ijin) throws Exception {
        checkAuthenticity();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        if (!CommonUtil.isEmpty(checklist)) {
            boolean valid = true;
            for (Checklist_tmp check : checklist) {
                if (check.isBankSupportSpec() && !check.isValidForPercentage()) {
                    valid = false;
                    break;
                }
            }
            if (!valid) {
                flash.error("Nominal dukungan keuangan minimal 10%% maksimal 100%%!");
                ldk(id);
            }
        }
        Dok_lelang.JenisDokLelang jenis = lelang.isPrakualifikasi() ? Dok_lelang.JenisDokLelang.DOKUMEN_LELANG_PRA: Dok_lelang.JenisDokLelang.DOKUMEN_LELANG;
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, jenis);
        boolean editable = dok_lelang.isAllowAdendum(lelang);
        if(!editable)
            forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
        try {
            Checklist_tmp.simpanChecklist(dok_lelang, ijin, checklist, params, lelang.getPaket().getKualifikasi());
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateByForLdk(dok_lelang.dll_id, lelang.lls_status);
            dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
            flash.success("Persyaratan Kualifikasi telah tersimpan");
        }catch (Exception e) {
            flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
            Logger.error(e, e.getMessage());
        }
        ldk(id);
    }
    
    @AllowAccess({Group.PANITIA})
	public static void ldkSubmitBaru(Long id, List<Checklist_tmp> ijin, List<Checklist_tmp> syaratAdmin, List<Checklist_tmp> syaratTeknis, List<Checklist_tmp> syaratKeuangan) throws Exception {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		Kategori kategori = lelang.getKategori();
        JenisDokLelang jenis = lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA:JenisDokLelang.DOKUMEN_LELANG;
		Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, jenis);
		Kualifikasi kualifikasi = lelang.getPaket().getKualifikasi();
		boolean editable = dok_lelang.isAllowAdendum(lelang);
		if(!editable)
			forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
		try {
			Checklist_tmp.simpanChecklist(dok_lelang, ijin, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, kualifikasi);
			Checklist_tmp.simpanChecklist(dok_lelang, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params, kualifikasi);
			Checklist_tmp.simpanChecklist(dok_lelang, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, kualifikasi);
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateByForLdk(dok_lelang.dll_id, lelang.lls_status);
			dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
			flash.success("Persyaratan Kualifikasi telah tersimpan");
		} catch (Exception e) {
			if(!kategori.isKonsultansiPerorangan())
				Cache.set("ijinList_"+dok_lelang.dll_id, Checklist_tmp.getListIjinUsahaBaruFlash(dok_lelang, ijin));
			Cache.set("syaratAdmin_"+dok_lelang.dll_id, Checklist_tmp.getListSyaratKualifikasiFlash(dok_lelang, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_ADMINISTRASI, params, kualifikasi));
			Cache.set("syaratTeknis_"+dok_lelang.dll_id, Checklist_tmp.getListSyaratKualifikasiFlash(dok_lelang, syaratTeknis, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_TEKNIS, params,kualifikasi));
			Cache.set("syaratKeuangan_"+dok_lelang.dll_id, Checklist_tmp.getListSyaratKualifikasiFlash(dok_lelang, syaratKeuangan, Checklist_master.JenisChecklist.CHECKLIST_KUALIFIKASI_KEUANGAN, params, kualifikasi));
			flash.put("flashError", true);
			flash.error("Persyaratan Kualifikasi gagal tersimpan. " + e.getLocalizedMessage());
			Logger.error(e, e.getMessage());
		}
		ldk(id);
	}
    
    @AllowAccess({Group.PANITIA})
    public static void masaBerlakuPenawaran(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        renderArgs.put("id", id); // kode lelang
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("editable", dok_lelang.isAllowAdendum(lelang));
        if (dok_lelang != null) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
            if(dok_lelang_content != null && dok_lelang_content.ldpcontent != null)
                renderArgs.put("masaberlaku", dok_lelang_content.ldpcontent.masa_berlaku_penawaran);
        }
        renderTemplate("lelang/dokumen/masa-berlaku-penawaran-adendum.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void masaBerlakuPenawaranSubmit(Long id, Integer masaberlaku) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        boolean editable = dok_lelang.isAllowAdendum(lelang);
        if(!editable){
            forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
        }else{
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            if(dok_lelang_content != null) {
                LDPContent ldpContent = dok_lelang_content.ldpcontent != null ? dok_lelang_content.ldpcontent:new LDPContent();
                ldpContent.masa_berlaku_penawaran = masaberlaku;
                dok_lelang_content.ldpcontent = ldpContent;
                dok_lelang_content.dll_modified = true;
                dok_lelang_content.save();
            }
        }
        LelangCtr.view(id);
    }

    /**
     * form Checklist Persyaratan
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void checklist(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        if(lelang.getPemilihan().isLelangExpress()) {
            forbidden("Tender Cepat Dan Seleksi Cepat Tidak Perlu Persyaratan Dokumen");
        }
        Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("lelang", lelang);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("editable", dok_lelang.isAllowAdendum(lelang));
        Kategori kategori = lelang.getKategori();
        renderArgs.put("kategori", kategori);
        Checklist_tmp.copyChecklist(dok_lelang, JenisChecklist.CHECKLIST_SYARAT);
        List<Checklist_tmp> syaratAdmin = Checklist_tmp
                .getListSyarat(dok_lelang, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI, kategori)
                .stream()
                .sorted(Comparator.comparing(Checklist_tmp::getSortingRule))
                .collect(Collectors.toList());
        renderArgs.put("syaratAdmin",syaratAdmin);
        final boolean isConsultant = kategori.isConsultant();
        List<Checklist_tmp> syaratTeknis = Checklist_tmp
                .getListSyarat(dok_lelang, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS, kategori)
                .stream()
                .filter(c -> isConsultant == c.getChecklist_master().isConsultant() || c.getChecklist_master().isSyaratLain())
                .sorted(Comparator.comparing(Checklist_tmp::getSortingRule))
                .collect(Collectors.toList());
        renderArgs.put("syaratTeknis", syaratTeknis);
//    		if (kategori.isKonstruksi()) {
        renderArgs.put("syaratHarga", Checklist_tmp.getListSyarat(dok_lelang, Checklist_master.JenisChecklist.CHECKLIST_HARGA, kategori));
//    		}
        if (dok_lelang != null) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
            if (dok_lelang_content != null) {
                renderArgs.put("adendum", dok_lelang_content.isAdendum());
            }
        }
        renderTemplate("lelang/dokumen/form-checklist-penawaran-adendum.html");
    }

    /**
     * Simpan Form Checklist Persyaratan
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void checklistSubmit(Long id,  List<Checklist_tmp> syarat, List<Checklist_tmp> syaratAdmin, List<Checklist_tmp> syaratHarga) {
        checkAuthenticity();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        if(lelang.getPemilihan().isLelangExpress()) {
            forbidden("Tender Cepat Dan Seleksi Cepat Tidak Perlu Persyaratan Dokumen");
        }
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        boolean editable = dok_lelang.isAllowAdendum(lelang);
        if (!editable)
            forbidden("Anda tidak bisa melakukan Perubahan Dokumen");
        try {
            boolean emptyTeknis = true;
            boolean emptyHarga = true;
            // do validation first
            if (syarat != null) {
                for (int i = 0; i < syarat.size(); i++) {
                    if (syarat.get(i).ckm_id != null) {
                        emptyTeknis= false;
                        validation.valid(syarat.get(i));
                    }
                }
            }
            if (syaratHarga != null) {
                for (int i = 0; i < syaratHarga.size(); i++) {
                    if (syaratHarga.get(i).ckm_id != null) {
                        emptyHarga = false;
                        validation.valid(syaratHarga.get(i));
                    }
                }
            }

            if(!validation.hasErrors()) {
                if (emptyTeknis)
                    flash.error("Persyaratan Teknis pada Persyaratan Dokumen Penawaran wajib diisi!");
                else if (lelang.getKategori().isKonstruksi() && emptyHarga)
                    flash.error("Persyaratan Harga pada Persyaratan Dokumen Penawaran wajib diisi!");
                else {
                    // do the save
                    if (!emptyTeknis)
                    	Checklist_tmp.simpanChecklist(dok_lelang, syarat, Checklist_master.JenisChecklist.CHECKLIST_TEKNIS); // simpan checklist penawaran

                    if (syaratAdmin != null)
                    	Checklist_tmp.simpanChecklist(dok_lelang, syaratAdmin, Checklist_master.JenisChecklist.CHECKLIST_ADMINISTRASI); // simpan checklist penawaran

                    Checklist_tmp.simpanChecklist(dok_lelang, syaratHarga, Checklist_master.JenisChecklist.CHECKLIST_HARGA); // simpan checklist penawaran

                    Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateByForSyarat(dok_lelang.dll_id, lelang);
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);

                    flash.success("Persyaratan Dokumen telah tersimpan");
                }
            } else {
                StringBuilder sb = new StringBuilder();
                for(play.data.validation.Error err:validation.errors()) {
                    if(!StringUtils.isEmpty(err.getKey())) {
                        sb.append("<br/>").append(err.message());
                    }
                }
                flash.error("Persyaratan Dokumen gagal tersimpan:"+ sb);
            }
        }catch (Exception e) {
            Logger.error(e, "Gagal Simpan Persyaratan Dokumen");
            flash.error("Persyaratan Dokumen gagal tersimpan");
        }
        checklist(id);
    }

    @AllowAccess({Group.PPK, Group.PANITIA, Group.KUPPBJ})
    public static void hps(Long id){
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("lelang", lelang);
        boolean fixed = true;
        String data = "[]";
        Active_user activeUser = Active_user.current();
        Dok_lelang dokLelang = lelang.getDokumenLelang();
        Dok_lelang_content dokLelangContent = dokLelang.getDokLelangContent();
        DaftarKuantitas dk = dokLelangContent.dkh;
        if(dk == null) {
            dk = dokLelang.getRincianHPS();
        }
        boolean editable = lelang.getTahapStarted().isAllowAdendum() && activeUser.isPpk();
        if(dk != null){
            fixed = dk.fixed;
            if (dk.items != null) {
                List<Rincian_hps> rincianHpsList = new ArrayList<Rincian_hps>();
                for (int i = 0; i < dk.items.size() ; i++) {
                    if((!dk.items.get(i).item.isEmpty() || !dk.items.get(i).item.equalsIgnoreCase(""))
                            && dk.items.get(i).harga != 0
                            && dk.items.get(i).total_harga != 0){
                        rincianHpsList.add(dk.items.get(i));
                    }
                }
                data = CommonUtil.toJson(rincianHpsList);
            }
            renderArgs.put("total", dk.total);
        }
        renderArgs.put("paket", lelang.getPaket());
        renderArgs.put("data", data);
        renderArgs.put("editable", editable);
        renderArgs.put("fixed", fixed);
        renderArgs.put("enableViewHps", lelang.getPaket().isEnableViewHps());
        renderArgs.put("draft", lelang.lls_status.isDraft());
        renderTemplate("lelang/dokumen/form-hps-adendum.html");
    }

    /**
     * Simpan Form Rincian HPS
     * @param id
     */
    @AllowAccess({Group.PPK})
    public static void hpsSubmit(Long id, String data, boolean fixed, boolean enableViewHps) {
        checkAuthenticity();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        boolean editable = dok_lelang.isAllowAdendum(lelang);
        Map<String, Object> result = new HashMap<>(1);
        if(!editable)
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        else {
            try {
                dok_lelang.simpanHps(lelang,  data, fixed, enableViewHps);
                result.put("result", "ok");
            } catch (Exception e) {
                Logger.error("Gagal Menyimpan HPS : %s", e.getLocalizedMessage());
                result.put("result", e.getLocalizedMessage());
            }
        }
        renderJSON(result);
    }

    /**
     * form Spek/KAK
     * @param id
     */
    @AllowAccess({Group.PANITIA,Group.KUPPBJ, Group.PPK})
    public static void spek(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        Kategori kategori = lelang.getKategori();
        renderArgs.put("lelang", lelang);
        renderArgs.put("kategori", kategori);
        Active_user activeUser = Active_user.current();
        Dok_lelang dokLelang = Dok_lelang.findBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("editable", lelang.getTahapStarted().isAllowAdendum() && activeUser.isPpk());
        if(dokLelang != null) {
            Dok_lelang_content dokLelangContent = Dok_lelang_content.findBy(dokLelang.dll_id);
            if(dokLelangContent != null) {
                renderArgs.put("spek",dokLelangContent.getDokSpek());
                renderArgs.put("adendum", dokLelangContent.isAdendum());
            }
        }
        renderArgs.put("isPanitia",activeUser.isPanitia());
        renderTemplate("lelang/dokumen/form-spek-adendum.html");
    }

    /**
     * Simpan Form Spek/KAK
     * @param id
     */
    @AllowAccess({Group.PPK})
    public static void spekSubmit(Long id, File file) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        Map<String, Object> result = new HashMap<>(1);
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id,	Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
        boolean editable = dok_lelang.isAllowAdendum(lelang);
        if(!editable)
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        else {
            try {
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_spek == null) {
                    List<BlobTable> blobTableList = dok_lelang_content.getDokSpek();
                    dok_lelang_content.dll_spek = dok_lelang_content.simpanBlobSpek(blobTableList);
                }
                UploadInfo model = dok_lelang_content.simpanSpek(file);
                if (model == null) {
                    result.put("success", false);
                    result.put("message", "File dengan nama yang sama telah ada di server!");
                    renderJSON(result);
                }
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload spek");
                result.put("success", false);
                result.put("result", "Kesalahan saat upload spek");
            }
        }
        renderJSON(result);
    }


    @AllowAccess({Group.PPK })
    public static void hapusSpek(Long id, Integer versi) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        Map<String, Object> result = new HashMap<String, Object>(1);
        if(dok_lelang.isAllowAdendum(lelang)) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_spek == null) {
                List<BlobTable> blobList = dok_lelang_content.getDokSpek();
                blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                try {
                    dok_lelang_content.dll_spek = dok_lelang_content.simpanBlobSpek(blobList);
                    result.put("success", true);
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                } catch (Exception e) {
                    Logger.error(e, "File Anda tidak dapat dihapus");
                    result.put("success", false);
                    result.put("message", "File Anda tidak dapat dihapus");
                }
            }  else {
                BlobTable blob = BlobTable.findById(dok_lelang_content.dll_spek, versi);
                if (blob != null)
                    blob.delete();
                dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
            }
        }else {
            result.put("success",false);
            result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PANITIA,Group.KUPPBJ, Group.PPK})
    public static void docSskk(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        TahapStarted tahapStarted = lelang.getTahapStarted();
        Kategori kategori = lelang.getKategori();
        renderArgs.put("lelang", lelang);
        renderArgs.put("kategori", kategori);
        renderArgs.put("isConsultant", kategori.isConsultant() || kategori.isJkKonstruksi());
        Active_user activeUser = Active_user.current();
        Dok_lelang dokLelang = Dok_lelang.findBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("editable", tahapStarted.isAllowAdendum() && activeUser.isPpk());
        if(dokLelang != null) {
            Dok_lelang_content dokLelangContent = Dok_lelang_content.findBy(dokLelang.dll_id);
            if(dokLelangContent != null) {
                renderArgs.put("sskk", dokLelangContent.getDokSskk());
                renderArgs.put("adendum", dokLelangContent.isAdendum());
            }
        }
        renderTemplate("lelang/dokumen/form-upload-sskk-adendum.html");
    }


    @AllowAccess({Group.PPK})
    public static void docSskkSubmit(Long id, File file){
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang);
        Map<String, Object> result = new HashMap<>(1);
        Dok_lelang dok_lelang = lelang.getDokumenLelang();
        if (dok_lelang != null && lelang.getTahapStarted().isAllowAdendum()) {
            try {
                Dok_lelang_content dok_lelang_content = dok_lelang.getDokLelangContent();
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_sskk_attachment == null) {
                    List<BlobTable> blobTableList = dok_lelang_content.getDokSskk();
                    dok_lelang_content.dll_sskk_attachment = dok_lelang_content.simpanBlobSpek(blobTableList);
                    dok_lelang_content.dll_modified = true;
                }
                UploadInfo model = dok_lelang_content.simpanSskkAttachment(file);
                if (model == null) {
                    result.put("success", false);
                    result.put("message", "File dengan nama yang sama telah ada di server!");
                    renderJSON(result);
                }
                List<UploadInfo> files = new ArrayList<UploadInfo>();
                files.add(model);
                dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, e.getLocalizedMessage());
                result.put("result", "Kesalahan saat upload sskk");
            }
        }else{
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PPK })
    public static void hapusSskk(Long id, Integer versi) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Map<String, Object> result = new HashMap<String, Object>(1);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        if(dok_lelang.isAllowAdendum(lelang)) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            if (dok_lelang_content != null) {
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_sskk_attachment == null) {
                    List<BlobTable> blobList = dok_lelang_content.getDokSskk();
                    blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                    try {
                        dok_lelang_content.dll_sskk_attachment = dok_lelang_content.simpanBlobSpek(blobList);
                        result.put("success", true);
                        dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                    } catch (Exception e) {
                        Logger.error(e, "File Anda tidak dapat dihapus");
                        result.put("success", false);
                        result.put("message", "File Anda tidak dapat dihapus");
                    }

                }  else {
                    BlobTable blob = BlobTable.findById(dok_lelang_content.dll_sskk_attachment, versi);
                    if (blob != null)
                        blob.delete();
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                }
            }
        }else {
            result.put("success",false);
            result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);

    }

    /**
     * form Jenis Paket
     * @param id
     */
    @AllowAccess({Group.PPK})
    public static void jenisKontrak(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        Kategori kategori = lelang.getKategori();
        renderArgs.put("lelang", lelang);
        renderArgs.put("kategori", kategori);
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        final boolean isEditable = dok_lelang.isAllowAdendum(lelang);
        renderArgs.put("editable", isEditable);
        renderTemplate("lelang/dokumen/edit-jenis-kontrak.html");
    }

    /**
     * simpan Jenis Paket
     * @param id
     */
    @AllowAccess({Group.PPK})
    public static void jenisKontrakSubmit(Long id, Integer kontrak_pembayaran) throws Exception {
        Logger.info("jenisKontrakSubmit");
        checkAuthenticity();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        JenisKontrak kontrak = JenisKontrak.findById(kontrak_pembayaran);
        if(kontrak!=null) {
            Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            if(dok_lelang_content != null) {
                dok_lelang_content.dll_modified = true;
                dok_lelang_content.dll_kontrak_pembayaran = kontrak_pembayaran;
                dok_lelang_content.save();
            }
            flash.success("Update Jenis Kontrak berhasil.");
        }
        else {
            flash.error("kontrak pembayaran tidak dikenal");
        }
        LelangCtr.view(id);
    }

    /**
     * form Dokumen Pengadaan Lainnya
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.KUPPBJ, Group.PPK})
    public static void lainnya(Long id) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        TahapStarted tahapStarted = lelang.getTahapStarted();
        renderArgs.put("kategori", lelang.getKategori());
        renderArgs.put("lelang", lelang);
        Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        renderArgs.put("editable", dok_lelang.isAllowAdendum(lelang));
        if(dok_lelang != null) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
            if(dok_lelang_content != null) {
                renderArgs.put("lainnya",dok_lelang_content.getDokLainnya());
                renderArgs.put("adendum", dok_lelang_content.isAdendum());
            }
        }
        renderArgs.put("isKuppbj", Active_user.current().isKuppbj());
        renderTemplate("lelang/dokumen/form-info-lainnya-adendum.html");
    }

    /**
     * Simpan Form Dokumen Pengadaan Lainnya
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PPK})
    public static void lainnyaSubmit(Long id, File file) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id,	Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        Map<String, Object> result = new HashMap<String, Object>(1);
        if(dok_lelang.isAllowAdendum(lelang)) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            try {
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_lainnya == null) {
                    List<BlobTable> blobTableList = dok_lelang_content.getDokLainnya();
                    if(!CollectionUtils.isEmpty(blobTableList))
                        dok_lelang_content.dll_lainnya = dok_lelang_content.simpanBlobSpek(blobTableList);
                }
                List<UploadInfo> files = new ArrayList<UploadInfo>();
                files.add(dok_lelang_content.simpanLainnya(file));
                dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload informasi lainnya");
                result.put("success", false);
                result.put("result", "Kesalahan saat upload informasi lainnya");
            }
        } else {
            result.put("success", false);
            result.put("result", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PANITIA, Group.PPK})
    public static void hapusLainnya(Long id, Integer versi) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Map<String, Object> result = new HashMap<String, Object>(1);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        Dok_lelang dok_lelang = Dok_lelang.findBy(id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
        if(dok_lelang.isAllowAdendum(lelang)) {
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);;
            if (dok_lelang_content != null) {
                if (dok_lelang_content.isAdendum() && dok_lelang_content.dll_lainnya == null) {
                    Dok_lelang_content dok_lelang_content_prev = Dok_lelang_content.findBy(dok_lelang.dll_id, dok_lelang_content.dll_versi - 1);
                    List<BlobTable> blobList = dok_lelang_content_prev.getDokLainnya();
                    if(!CollectionUtils.isEmpty(blobList))
                        blobList.removeIf(blob -> versi.equals(blob.blb_versi));
                    try {
                        dok_lelang_content.dll_lainnya = dok_lelang_content.simpanBlobSpek(blobList);
                        result.put("success", true);
                        dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                    } catch (Exception e) {
                        Logger.error(e, "File Anda tidak dapat dihapus");
                        result.put("success", false);
                        result.put("message", "File Anda tidak dapat dihapus");
                    }
                } else {
                    BlobTable blob = BlobTable.findById(dok_lelang_content.dll_lainnya, versi);
                    if (blob != null)
                        blob.delete();
                    dok_lelang.simpan(lelang.lls_status, lelang.getNamaPaket(), dok_lelang_content);
                }
            }
        }else {
            result.put("success", false);
            result.put("message", "Anda tidak bisa melakukan Perubahan Dokumen");
        }
        renderJSON(result);
    }

    /**
     * * Cetak Dokumen Pengadaan atau Pemilihan
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void cetak(Long id, String nomorSDP, @As(binder= DatetimeBinder.class) Date tglSDP) throws Exception {
        checkAuthenticity();
        Dok_lelang dok_lelang = Dok_lelang.findById(id);
        Lelang_seleksi lelang = Lelang_seleksi.findById(dok_lelang.lls_id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        if(lelang.lls_status.isDraft()) {
            // cek kelengkapan dokumen dilakukan saat lelang masih draft
            boolean express  = lelang.getPemilihan().isLelangExpress();
            Kategori kategori = lelang.getKategori();
            StringBuilder complete = new StringBuilder();
            if (dok_lelang != null && dok_lelang.dll_id != null) {
                Dok_lelang_content dok_ll_con = Dok_lelang_content.findBy(dok_lelang.dll_id);
                if(!express && (lelang.isPascakualifikasi() || dok_lelang.dll_jenis.isKualifikasi() || dok_lelang.dll_jenis.isAdendumPra())) {
                    List<Checklist> ijinList = dok_lelang.getSyaratIjinUsaha();
                    List<Checklist> syaratList = dok_lelang.getSyaratKualifikasi();
                    if (CommonUtil.isEmpty(ijinList)) // empty ijin usaha
                        complete.append("- Persyaratan Kualifikasi (Izin Usaha).\n ");
                    if (CommonUtil.isEmpty(syaratList))
                        complete.append("- Persyaratan Kualifikasi (Syarat).\n ");
                }
                if (dok_lelang.dll_jenis.isDokLelang() || dok_lelang.dll_jenis.isAdendum()) {
                    if(!express) {
                        if (CommonUtil.isEmpty(dok_lelang.getSyaratTeknis()))
                            complete.append("- Persyaratan Dokumen Teknis.\n ");
                        if (kategori.isKonstruksi() && CommonUtil.isEmpty(dok_lelang.getSyaratHarga()))
                            complete.append("- Persyaratan Dokumen Harga.\n ");
                    }
                    if(CommonUtil.isEmpty(dok_ll_con.dll_ldp))
                        complete.append("- Lembar Data Pemilihan.\n ");
                    if(CommonUtil.isEmpty(dok_ll_con.dll_dkh))
                        complete.append("- Rincian HPS.\n ");
                    if(CommonUtil.isEmpty(dok_ll_con.dll_sskk))
                        complete.append("- Syarat-Syarat Khusus Kontrak (SSKK).\n ");
                    if(dok_ll_con.dll_spek == null)
                        complete.append("- Spesifikasi Teknis dan Gambar.\n ");
                }
            }
            if (complete.length() > 0)
                flash.error("Kelengkapan Yang Belum Disimpan: " + complete);
        }
        // jika ada error  tidak perlu disimpan
        if(!flash.contains("error")){
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
            if(dok_lelang_content == null)
                return;
            else if(dok_lelang_content.dll_modified) {
                dok_lelang_content.dll_nomorSDP = nomorSDP;
                dok_lelang_content.dll_tglSDP = tglSDP;

                StopWatch sw = new StopWatch();
                sw.start();
                Template template = null;
                if(dok_lelang.dll_jenis.isKualifikasi()) {
                    template = DokumenCtr.TEMPLATE_DOK_KUALIFIKASI;
                }else { // untuk dokumen pengadaan/pemilihan
                    template = lelang.isPrakualifikasi() ? DokumenCtr.TEMPLATE_DOK_PEMILIHAN : DokumenCtr.TEMPLATE_DOK_PENGADAAN;
                    if (lelang.getPemilihan().isLelangExpress())
                        template = DokumenCtr.TEMPLATE_DOK_PEENGADAAN_EXPRESS;
                }
                String content = Dok_lelang.cetak_content(lelang, dok_lelang, dok_lelang_content, template);
                if (!StringUtils.isEmpty(content)) {
                    dok_lelang_content.dll_content_html = content.replace("<br>", "<br/>");
                    InputStream dok = HtmlUtil.generatePDF(template.name+"-"+id, "#{extends 'borderTemplate.html' /} "+dok_lelang_content.dll_content_html);
                    // simpan ke dok_lelang
                    sw.stop();
                    if (dok == null) {
                        dok = IOUtils.toInputStream(dok_lelang_content.dll_content_html, "UTF-8");
                        File file = TempFileManager.createFileInTemporaryFolder("InvalidDocLelangTemplate-lls_id#" + lelang.lls_id + ".html");
                        FileUtils.copyInputStreamToFile(dok, file);
                        Logger.error("Gagal generate PDF untuk lls_id %s. Template dokumen yang error disimpan di: %s", lelang.lls_id, file);
                    } else {
                        Logger.debug("Generate PDF document: %s, , duration: %s", dok_lelang.dll_nama_dokumen, sw);
                    }
                    dok_lelang.simpanCetak(lelang, dok_lelang_content, dok);
                }

                // kirim email notifikasi jika adendum
                if(dok_lelang_content.dll_versi > 1){
                    TahapStarted tahapStarted = lelang.getTahapStarted();
                    List<Peserta> pesertaList = lelang.getPesertaList();
                    if (lelang.isPrakualifikasi()) {
                        Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
                        boolean allow_pengumuman_pra = pembuktian != null && pembuktian.eva_status.isSelesai() && tahapStarted.isPengumumanPemenangPra();
                        if (allow_pengumuman_pra) {
                            // hanya kirim ke pemenang prakualifikasi
                            pesertaList = Nilai_evaluasi.findPemenangPrakualifikasi(lelang.lls_id);
                        }
                    }

                    if(pesertaList != null) {
                        try {
                            MailQueueDao.kirimNotifikasiAdendum(lelang, pesertaList);
                        } catch (Exception ex) {
                            Logger.error("Gagal kirim notifikasi adendum untuk lls_id %s.", lelang.lls_id);
                        }
                    }
                }
            }
        }
        LelangCtr.view(dok_lelang.lls_id);
    }

    @AllowAccess({Group.PANITIA})
    public static void uploadDokTender(Long id) {
        Dok_lelang dok_lelang = Dok_lelang.findById(id);
        renderArgs.put("id", dok_lelang.lls_id);
        renderArgs.put("dokId", id);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("dok_lelang_content", Dok_lelang_content.findBy(dok_lelang.dll_id));
        renderArgs.put("today", newDate());

        // Dokumen Tender/Seleksi adalah label untuk Tender Prakualifikasi
        // Dokumen Pemilihan adalah label untuk Tender Pascakualifikasi
        String labelDokumen = "Tender/Seleksi";
        if(dok_lelang.getLelang_seleksi().isPascakualifikasi()){
            labelDokumen = "Pemilihan";
        }
        renderArgs.put("labelDokumen", labelDokumen);
        renderTemplate("lelang/dokumen/upload-dok-pemilihan-adendum.html");
    }

    /**
     * Simpan Form Dokumen Pengadaan
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void doktenderSubmit(Long id, @Valid @PdfType File file, String nomorSDP, @As(binder= DateBinder.class) Date tglSDP) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        String namaDokumen = lelang.isPrakualifikasi() ? "Tender/Seleksi" : "Pemilihan";
        if (validation.hasErrors()) {
            flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
        } else {
            Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            try {
            		if (dok_lelang_content.dll_ldk_updated)
            			Checklist.copyChecklist(dok_lelang, JenisChecklist.CHECKLIST_LDK);
            		if (dok_lelang_content.dll_syarat_updated)
            			Checklist.copyChecklist(dok_lelang, JenisChecklist.CHECKLIST_SYARAT);
            		// cetak konten html
                dok_lelang_content.dll_nomorSDP = nomorSDP;
                dok_lelang_content.dll_tglSDP = tglSDP;
                dok_lelang.simpan(dok_lelang_content, lelang, file);
                flash.success("Upload Dokumen %s berhasil.", namaDokumen);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Dokumen Tender/Seleksi");
            }
        }
        LelangCtr.view(id);
    }

    @AllowAccess({Group.PANITIA})
    public static void uploadDokKualifikasi(Long id) {
        Dok_lelang dok_lelang = Dok_lelang.findById(id);
        renderArgs.put("id", dok_lelang.lls_id);
        renderArgs.put("dokId", id);
        renderArgs.put("dok_lelang", dok_lelang);
        renderArgs.put("dok_lelang_content", Dok_lelang_content.findBy(dok_lelang.dll_id));
        renderArgs.put("today", newDate());
        renderTemplate("lelang/dokumen/upload-dok-kualifikasi-adendum.html");
    }

    /**
     * Simpan Form Dokumen Kualifikasi
     * @param id
     */
    @AllowAccess({Group.PANITIA})
    public static void dokKualifikasiSubmit(Long id, @Valid @PdfType File file, String nomorSDP, @As(binder= DateBinder.class) Date tglSDP) {
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        if (validation.hasErrors()) {
            String namaDokumen = lelang.isPrakualifikasi() ? "Tender/Seleksi" : "Pemilihan";
            flash.error("Untuk Dokumen %s yang dapat diupload hanya dokumen/file yang memiliki ekstensi <b>*.pdf</b>", namaDokumen);
        } else {
            Dok_lelang dok_lelang = Dok_lelang.findNCreateBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG_PRA);
            Dok_lelang_content dok_lelang_content = Dok_lelang_content.findNCreateBy(dok_lelang.dll_id, lelang);
            try {
            		if (dok_lelang_content.dll_ldk_updated)
            			Checklist.copyChecklist(dok_lelang, JenisChecklist.CHECKLIST_LDK);
            		dok_lelang_content.dll_nomorSDP = nomorSDP;
                dok_lelang_content.dll_tglSDP = tglSDP;
                dok_lelang.simpan(dok_lelang_content, lelang, file);
                // jika terdapat adendum kualifikasi , maka status kualifikasi tambahan di batalkan. dan peserta bisa kirim kualifikasi semua
                lelang.is_kualifikasi_tambahan = false;
                lelang.save();
                List<Peserta> pesertaList = lelang.getPesertaList();
                if (!CollectionUtils.isEmpty(pesertaList)) {
                    for (Peserta peserta : pesertaList) {
                        peserta.is_dikirim_pesan = false;
                        peserta.save();
                    }
                }
                flash.success("Upload Dokumen Kualifikasi berhasil.");
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Dokumen Kualifikasi");
            }
        }
        LelangCtr.view(id);
    }
}
