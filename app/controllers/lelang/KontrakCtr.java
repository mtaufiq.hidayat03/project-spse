package controllers.lelang;

import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.DokumenType;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.kontrak.*;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.rekanan.Landasan_hukum;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import play.Logger;
import play.data.FileUpload;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Valid;
import play.i18n.Messages;
import utils.BlacklistCheckerUtil;
import utils.LogUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;

/**
 * Definisikan aktivitas terkait kontrak seperti kontrak dan berita acara
 * terkait kontrak
 *
 * @author arief ardiyansah
 *
 */
public class KontrakCtr extends BasicCtr {

	public static void index(Long lls_id) {
		Lelang_seleksi lls = Lelang_seleksi.findById(lls_id);
		renderArgs.put("lls", lls);
		renderArgs.put("pesertaList", Peserta.find("lelang_seleksi.lls_id = ? and is_pemenang=?", lls_id, 1).fetch());
		renderArgs.put("kontrak", Kontrak.find("lelang_seleksi.lls_id=?", lls_id).first());
		renderArgs.put("harga", Peserta_harga_view.getListHarga(lls_id));
		Pegawai pegawai = Pegawai.findBy(Active_user.current().userid);
		renderArgs.put("ppk", Ppk.find("pegawai.peg_id=?", pegawai.peg_id).first());
		renderTemplate("lelang/kontrak.html");
	}

	/**
	 * View Kontrak
	 *
	 * @param id
	 */
	@AllowAccess({ Group.PPK })
	public static void kontrak(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		renderArgs.put("lls", Lelang_seleksi.findById(id));
		renderTemplate("lelang/kontrak.html");
	}

	@AllowAccess({ Group.PPK })
	public static void simpanKontrak1(Kontrak kontrak) {
		checkAuthenticity();
		otorisasiDataLelang(kontrak.lls_id); // check otorisasi data lelang
		kontrak.save();
		kontrak(kontrak.lls_id);
	}

	@AllowAccess({ Group.PPK })
	public static void sppbjPpk(Long lelangId) {
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		renderArgs.put("lelang", lelang);
		renderArgs.put("allowCreateSppbj", lelang.allowCreateSppbj());
		renderTemplate("kontrak/sppbjPpkList.html");
	}

	@AllowAccess({ Group.PPK })
	public static void sppbjPpkDetil(Long lelangId, Long sppbjId) {
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		// data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		renderArgs.put("lelang", lelang);
		boolean isKonsolidasi = lelang.isPaketKonsolidasi();
		renderArgs.put("isKonsolidasi", isKonsolidasi);
		renderArgs.put("konsultan", lelang.getKategori().isConsultant() || lelang.getKategori().isJkKonstruksi());
		Active_user currentUser = Active_user.current();
		Pegawai pegawai = Pegawai.findBy(currentUser.userid);
		Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
		Long ppk_id = ppk != null ? ppk.ppk_id : null;
		renderArgs.put("ppk_id", ppk_id);
		renderArgs.put("pegawai", pegawai);
		renderArgs.put("peserta", Nilai_evaluasi.findAllWinnerCandidate(lelang.getEvaluasiAkhir().eva_id,lelang));
		List<Peserta> pemenang = Peserta.find("lls_id=? and is_pemenang_klarifikasi=1", lelangId).fetch();
		if(CollectionUtils.isEmpty(pemenang))
			pemenang = Peserta.find("lls_id=? and is_Pemenang=1", lelangId).fetch();
		renderArgs.put("pemenang", pemenang);
		renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL + "/daftar-hitam");
		Paket paket = lelang.getPaket();
		renderArgs.put("rupList", paket.isFlag43() ? paket.getRupListByPpk(ppk_id) : paket.getRupListByPpk42(ppk_id));

		Double hargaFinal = 0d;
		Double jaminanPelaksanaan = 0d;

		// data sppbj
		if(sppbjId != null) {
			Sppbj sppbjx, sppbj;
			sppbjx = sppbj = Sppbj.find("sppbj_id=? and ppk_id=?", sppbjId, ppk_id).first();
			if (sppbjx != null) {
				String str = sppbjx.sppbj_tembusan;
				List<String> tembusan = null;
				if (str != null) {
					tembusan = Arrays.asList(str.split("\\s*,\\s*"));
				}
				hargaFinal = sppbj.harga_final;
				jaminanPelaksanaan = sppbj.jaminan_pelaksanaan;
				renderArgs.put("sppbj", sppbj);
				renderArgs.put("sppbjx", sppbjx);
				renderArgs.put("tembusan", tembusan);
			}
		}else{
			TempSppbjHps tmp = TempSppbjHps.findByLelangAndPpk(lelangId);
			if(isKonsolidasi && tmp != null){
				DaftarKuantitas dk = CommonUtil.fromJson(tmp.dkh,DaftarKuantitas.class);
				hargaFinal = dk.total;
			}else {
				hargaFinal = Peserta.getTotalHargaFinal(pemenang,lelang);
			}
		}

		renderArgs.put("hargaFinal", hargaFinal);
		renderArgs.put("jaminanPelaksanaan", jaminanPelaksanaan);
		renderTemplate("kontrak/sppbjPpk.html");
	}

	@AllowAccess({ Group.PPK })
	public static void simpanSppbj(Long lelangId, Sppbj sppbj,  List<String> nama, Long rekananId, String hapus,String catatan_ppk) {
		checkAuthenticity();
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
		// use old value
		Active_user currentUser = Active_user.current();
		Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
		Sppbj sppbjx = sppbj;

		if(sppbj.jaminan_pelaksanaan == null){
			sppbj.jaminan_pelaksanaan = 0d;
		}

		if(sppbj.sppbj_id != null) {
			sppbj = Sppbj.findById(sppbj.sppbj_id);
		} else {
			// sppbj harus unique setiap rekanan, ppk dan lelang
			Sppbj chkSppbj = Sppbj.find("ppk_id=? and rkn_id=? and lls_id=?", ppk.ppk_id, rekananId, lelangId).first();
			if (chkSppbj != null) {
				validation.addError("sppbj.rkn_id", Messages.get("flash.sppbj_urdlsa"));
			}

			sppbj = new Sppbj();
			sppbj.ppk_id = ppk.ppk_id;
			sppbj.lls_id = lelang.lls_id;
		}
		sppbj.harga_final = sppbjx.harga_final;
		sppbj.jaminan_pelaksanaan = sppbjx.jaminan_pelaksanaan;
		sppbj.sppbj_no = sppbjx.sppbj_no;
		sppbj.sppbj_lamp = sppbjx.sppbj_lamp;
		sppbj.sppbj_kota = sppbjx.sppbj_kota;

		TempSppbjHps temp = TempSppbjHps.findByLelangAndPpk(lelang.lls_id);
		if(temp != null){
			sppbj.sppbj_dkh = temp.dkh;
		}else{
			Peserta peserta = Peserta.findByRekananAndLelang(rekananId, lelang.lls_id);
			if(peserta != null) {
				sppbj.sppbj_dkh = peserta.psr_dkh;
			}
			if(peserta == null){
				peserta = Peserta.findByRekananAndLelang(sppbj.rkn_id, lelang.lls_id);
				sppbj.sppbj_dkh = peserta.psr_dkh;
			}
			ReverseAuction ra = ReverseAuction.findByLelang(lelang.lls_id);
			if(ra != null && peserta != null){
				ReverseAuctionDetil rad = ReverseAuctionDetil.findLastPenawaran(ra.ra_id, peserta.psr_id);
				if(rad != null)
					sppbj.sppbj_dkh = rad.rad_dkh;
			}
		}

		//disamakan dengan tgl kirim karena di form sudah dihide tapi kolom db tidak boleh null
		sppbj.sppbj_tgl_buat = sppbjx.sppbj_tgl_kirim;
		sppbj.sppbj_tgl_kirim = sppbjx.sppbj_tgl_kirim;

		if(sppbjx.catatan_ppk != null && !lelang.isItemized()){
			validation.min(sppbjx.catatan_ppk.trim().length(),10)
					.key("sppbj.catatan_ppk").message(Messages.get("flash.cmsk"));
		}
		sppbj.catatan_ppk = sppbjx.catatan_ppk;

		validation.required(sppbj.harga_final).key("sppbj.harga_final").message(Messages.get("flash.hfhd"));
		if (sppbj.harga_final != null && sppbj.harga_final <= 0) {
			validation.addError("sppbj.harga_final", Messages.get("flash.hfhldo"));
		}

		if (rekananId != null) {
			sppbj.rkn_id = rekananId;
		}

		StringBuilder param = new StringBuilder();
		if (nama != null) {
			for (String temb : nama) {
				if(temb == null)
					continue;// handle null

				if (param.length() > 0)
					param.append(',');
				param.append(temb);
			}
			sppbj.sppbj_tembusan = param.toString();
		} else {
			sppbj.sppbj_tembusan = null;
		}
		boolean success = true;
		sppbj.sppbj_status = sppbj.sppbj_id == null ? sppbj.STATUS_DRAFT : sppbj.STATUS_TERKIRIM;
		validation.valid(sppbj);

		if (validation.hasErrors()) {
			flash.error(Messages.get("flash.gm_sppbj"));
			validation.keep();
			params.flash();
			success = false;
			LogUtil.info("errors",CommonUtil.toJson(validation.errorsMap()));
		}  else {
			try {
				sppbj.save();
				flash.success("%s berhasil disimpan", "SPPBJ");
			} catch (Exception e) {
				e.printStackTrace();
				success = false;
				flash.error(Messages.get("flash.datagt_ckia"));
			}
		}

		if(success){
			if(temp != null)
				temp.delete();
			sppbjPpkDetil(lelangId, sppbj.sppbj_id);
		}else {
			//render kembali halaman edit sppbj dengan load data terbaru
			Paket paket = lelang.getPaket();
			renderArgs.put("ppk_id", ppk.ppk_id);
			renderArgs.put("rupList", paket.getRupListByPpk(ppk.ppk_id));
			renderArgs.put("sppbjx", sppbjx);
			renderArgs.put("sppbj", sppbj);
			renderArgs.put("lelang", lelang);
			renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
			renderArgs.put("peserta", Nilai_evaluasi.findAllWinnerCandidate(lelang.getEvaluasiAkhir().eva_id,lelang));
			List<Peserta> pemenang = Peserta.find("lls_id=? and is_Pemenang=1", lelangId).fetch();
			renderArgs.put("pemenang", pemenang);
			renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL + "/daftar-hitam");
			renderArgs.put("hargaFinal", Peserta.getTotalHargaFinal(pemenang, lelang));
			String str = sppbj.sppbj_tembusan;
			List<String> tembusan = null;
			if (str != null) {
				tembusan = Arrays.asList(str.split("\\s*,\\s*"));
			}
			renderArgs.put("jaminanPelaksanaan", sppbjx.jaminan_pelaksanaan);
			renderArgs.put("tembusan", tembusan);
			renderTemplate("kontrak/sppbjPpk.html");
		}
	}

	@AllowAccess({ Group.PPK })
	public static void sppbjHps(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
		otorisasiDataLelang(sppbj.lls_id); // check otorisasi data lelang
		renderArgs.put("sppbj", sppbj);
		Lelang_seleksi lelang = Lelang_seleksi.findById(sppbj.lls_id);
		DaftarKuantitas dk = sppbj.dkh;
		if (dk == null) {
			Peserta peserta = Peserta.findByRekananAndLelang(sppbj.rkn_id, lelang.lls_id);
			dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);
		}
		renderArgs.put("lelang", lelang);
		renderArgs.put("data", CommonUtil.toJson(dk.items));
		renderTemplate("kontrak/sppbjHps.html");
	}

	@AllowAccess({Group.PPK})
	public static void sppbjHpsSubmit(Long sppbjId, String data, String total) {
		checkAuthenticity();
		Sppbj sppbj = Sppbj.findById(sppbjId);
		otorisasiDataLelang(sppbj.lls_id); // check otorisasi data lelang
		double total_harga = total != null ? Double.parseDouble(total) : 0d;
		Map<String, Object> result = new HashMap<>(1);
		try {
			if(total_harga <= 0d){
				throw new Exception("Total harga harus lebih dari 0");
			}
			sppbj.simpanHps(data, total);
			result.put("result", "ok");
		} catch (Exception e) {
			result.put("result", e.getMessage());
		}
		renderJSON(result);
	}

	@AllowAccess({Group.PPK})
	public static void tempSppbjHpsSubmit(Long llsId, Long psrId, String data, String total) {
		checkAuthenticity();
		otorisasiDataLelang(llsId); // check otorisasi data lelang

		double total_harga = total != null ? Double.parseDouble(total) : 0d;
		Map<String, Object> result = new HashMap<>(1);
		try {
			if(total_harga <= 0d){
				throw new Exception("Total harga harus lebih dari 0");
			}
			TempSppbjHps.simpanHps(llsId, psrId, data, total);
			result.put("result", "ok");
		} catch (Exception e) {
			e.printStackTrace();
			result.put("result", e.getMessage());
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void cetakDokSppbj(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
		long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		if(sppbj.harga_final == 0.0) {
			flash.error(Messages.get("flash.mldhtd"));
			sppbjPpkDetil(lelangId, sppbj.sppbj_id);
		}
		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lelangId);
		response.contentType = "application/pdf";
		InputStream is = Sppbj.cetak(lelang_seleksi, sppbj);
		sppbj.sppbj_status = sppbj.STATUS_TERKIRIM;
		sppbj.save();
	    renderBinary(is);
	}

	@AllowAccess({ Group.PPK })
	public static void kirimPengumumanSppbj(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
		long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		if(sppbj.harga_final == 0.0) {
			flash.error(Messages.get("flash.mldhtd"));
			sppbjPpkDetil(lelangId, sppbj.sppbj_id);
		}
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		try {

			Active_user active_user = Active_user.current();
			MailQueueDao.kirimPengumumanPemenangKontrak(lelang, sppbj, active_user.name);

			sppbj.sudah_kirim_pengumuman = true;
			sppbj.save();

			flash.success(Messages.get("flash.epbd"));
		} catch (Exception e) {
			flash.error(Messages.get("flash.tktp"));
			Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Pengumuman");
		}
		sppbjPpkDetil(lelangId, sppbj.sppbj_id);
	}

	@AllowAccess({ Group.PPK })
	public static void suratPerjanjianPpk(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
		long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang

	    Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		renderArgs.put("lelang", lelang);
		renderArgs.put("sppbj", sppbj);
		renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
		renderArgs.put("ppk", Ppk.findByLelangAndPegawai(lelangId, Active_user.current().pegawaiId));
		renderArgs.put("anggaranList", Anggaran.findByPaket(lelang.pkt_id));
	    Kontrak kontrakx = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
	    if(kontrakx!=null){
	        kontrakx.kontrak_nilai = kontrakx.kontrak_nilai == 0 ? sppbj.harga_final : kontrakx.kontrak_nilai;
	    	if(kontrakx.kontrak_kso!=null){
		    	Kontrak_kso kso = kontrakx.getKontrakKSO();
				renderArgs.put("kso",kso);
	    	}
	    }
		renderArgs.put("kontrakx", kontrakx);
		renderArgs.put("akta", Landasan_hukum.findByRekanan(sppbj.rkn_id));
	    renderTemplate("kontrak/suratPerjanjianPpk.html");
	}

	@AllowAccess({ Group.PPK })
	public static void uploadAttachment2(Long id, File file) {
		Kontrak kontrak = Kontrak.find("kontrak_id = ?", id).first();
		Map<String, Object> result = new HashMap<>(1);
		if(kontrak != null) {
			try {
				UploadInfo model = kontrak.simpanAttachment2(file);
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload spek");
				result.put("result", Messages.get("flash.ksus"));
			}
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void hapusAttachment2(Long id, Integer versi) {
		Kontrak kontrak = Kontrak.find("kontrak_id = ?", id).first();
		if (kontrak != null) {
			BlobTable blob = BlobTable.findById(kontrak.kontrak_id_attacment2, versi);
			if (blob != null)
				blob.delete();
			if (kontrak.kontrak_id_attacment2 != null && kontrak.getDokAttachment2().isEmpty()) {
				kontrak.kontrak_id_attacment2 = null;
			}
			kontrak.save();
		}
	}

	@AllowAccess({ Group.PPK })
	public static void simpanSuratPerjanjian(Long sppbjId, Kontrak kontrak,	 Kontrak_kso kso, String pembuat_sk, String nomor_sk, String unit_kerja_sk, String alamat_satker) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
		checkAuthenticity();
		Long lelangId = sppbj.lls_id;
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelangId, JenisDokLelang.DOKUMEN_LELANG);
		Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
		if (dok_lelang_content != null)
			kontrak.kontrak_sskk = dok_lelang_content.dll_sskk;
		Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, Active_user.current().pegawaiId);
		kontrak.lls_id = lelangId;
		kontrak.ppk_id = ppk.ppk_id;
		kontrak.rkn_id = sppbj.rkn_id;
		if (kontrak.kontrak_tipe_penyedia.equals("Penyedia Perseorangan")) {
			kontrak.kontrak_wakil_penyedia = null;
			kontrak.kontrak_jabatan_wakil = null;
			kontrak.kontrak_kso = null;
		} else {
			if(kontrak.kontrak_tipe_penyedia.equals("Penyedia Badan Usaha Non KSO")) {
				validation.required(kontrak.kontrak_wakil_penyedia).key("kontrak.kontrak_wakil_penyedia");
				validation.required(kontrak.kontrak_jabatan_wakil).key("kontrak.kontrak_jabatan_wakil");
			}
			if (kontrak.kontrak_tipe_penyedia.equals("Penyedia Kemitraan/KSO")) {
				kso.tgl_surat_kso = FormatUtils.formatDateFromDatePicker(params.get("tgl_kso"));
				kontrak.kontrak_kso = CommonUtil.toJson(kso);
			}
		}
		validation.required(kontrak.kontrak_tanggal).key("kontrak.kontrak_tanggal").message("Tanggal wajib diisi");
		validation.required(nomor_sk).key("nomor_sk").message("No. SK PPK wajib diisi");
		validation.required(alamat_satker).key("alamat_satker").message("Alamat Satuan Kerja wajib diisi");
		validation.valid(kontrak);
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();
			flash.error(Messages.get("flash.gm_surat_perjanjian"));
		} else {
			try {
				ppk.ppk_pembuat_sk = pembuat_sk;
				ppk.ppk_nomor_sk = nomor_sk;
				ppk.ppk_unit_kerja_sk = unit_kerja_sk;
				ppk.save();
				if(kontrak.kontrak_mulai ==null)
					kontrak.kontrak_mulai = kontrak.kontrak_tanggal;
				if(kontrak.kontrak_akhir ==null)
					kontrak.kontrak_akhir = kontrak.kontrak_tanggal;
				if(kontrak.kontrak_lingkup ==null)
					kontrak.kontrak_lingkup = "";
				kontrak.save();
				Paket_satker ps = Paket_satker.find("pkt_id = ?", lelang.pkt_id).first();
				if(ps != null) {
					Satuan_kerja sk = Satuan_kerja.find("stk_id = ?", ps.stk_id).first();
					if(sk != null) {
						sk.stk_alamat = alamat_satker;
						sk.save();
					}
				}
				flash.success(Messages.get("flash.bds", "Surat Perjanjian"));
			} catch (Exception e) {
				flash.error(Messages.get("flash.gds" , "Surat Perjanjian"));
				Logger.error(e, "%s gagal disimpan", "Surat Perjanjian");
			}
		}
		suratPerjanjianPpk(sppbj.sppbj_id);
	}

	@AllowAccess({ Group.PPK })
	public static void cetakPerjanjian(Long kontrakId) {
		Kontrak kontrak = Kontrak.findById(kontrakId);
		Long lelangId = kontrak.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		response.contentType = "application/pdf";
		InputStream is = Sppbj.cetak_perjanjian(kontrak);
		renderBinary(is);
	}

	@AllowAccess({ Group.PPK })
	public static void suratPesananPpk(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
		Kontrak kontrakx = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
	    if (kontrakx == null) {
	    	flash.error(Messages.get("flash.mabmds"));
			sppbjPpk(sppbj.lls_id);
		}
	    Long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		renderArgs.put("lelangId", lelangId);
		renderArgs.put("kontrakx", kontrakx);
		renderArgs.put("sppbj", sppbj);
		renderArgs.put("pesanan", Pesanan.find("kontrak_id=?", kontrakx.kontrak_id).fetch());
		renderTemplate("kontrak/daftarSuratPesanan.html");
	}

	@AllowAccess({ Group.PPK })
	public static void editPesanan(Long sppbjId, Long kontrakId, Long pesananId) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
	    if(kontrakx == null){
	    	flash.error(Messages.get("flash.mabmds"));
	    	BerandaCtr.index();
	    }
	    Long lelangId = kontrakx.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		renderArgs.put("sppbj", Sppbj.findById(sppbjId));
		renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
		renderArgs.put("lelang", lelang);
		renderArgs.put("kontrakx", kontrakx);
		if (pesananId != null) {
			Pesanan pesananx = Pesanan.findById(pesananId);
			renderArgs.put("tgl_pes", pesananx.pes_tgl);
			renderArgs.put("content", pesananx.getPesananContent());
			renderArgs.put("pesananx", pesananx);
			renderArgs.put("barang", Pesanan_barang.find("pes_id=?", pesananx.pes_id).fetch());
		}
		renderTemplate("kontrak/suratPesananPpk.html");
	}

	@AllowAccess({ Group.PPK })
	public static void openRincianBarang(Long lelangId, Long pesananId) {
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		Pesanan pesananx = Pesanan.findById(pesananId);
		Paket paket = Paket.findById(lelang.pkt_id);
		Sppbj sppbj = Sppbj.findByLelang(lelang.lls_id);
		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
		boolean isSpse3 = paket.pkt_flag == 1;
		String data = "[]";
		if (pesananx != null) {
			DaftarBarang db = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
			if (paket.pkt_flag == 1 && pesananx.pes_barang != null) {
				data = db.items != null ? CommonUtil.toJson(db.items) : "[]";
			} else if (paket.pkt_flag == 2) { // jika lelang SPSE4 rincian
				// barang ambil dari rincian hps
				Peserta peserta = sppbj == null ? null : Peserta.findByRekananAndLelang(sppbj.rkn_id, lelangId);
				DaftarBarang db1 = CommonUtil.fromJson(peserta.psr_dkh, DaftarBarang.class);
				if (!CommonUtil.isEmpty(pesananx.pes_barang))
					db1 = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
				if (db1.items != null)
					data = CommonUtil.toJson(db1.items);
			} else if (paket.pkt_flag == 3) { // jika lelang SPSE 4.3
				DaftarBarang db1 = CommonUtil.fromJson(sppbj.sppbj_dkh, DaftarBarang.class);
				if (!CommonUtil.isEmpty(pesananx.pes_barang))
					db1 = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
				if (db1 != null && db1.items != null)
					data = CommonUtil.toJson(db1.items);
			}
		}
		renderArgs.put("kontrak", kontrak);
		renderArgs.put("pesananx", pesananx);
		renderArgs.put("isSpse3",isSpse3);
		renderArgs.put("lelang", lelang);
		renderArgs.put("sppbj", sppbj);
		renderArgs.put("data", data);
		renderTemplate("kontrak/rincianBarangEditable.html");
	}

	@AllowAccess({ Group.PPK })
	public static void openRincianBarangFixed(Long lelangId, Long pesananId){
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		Pesanan pesananx = Pesanan.findById(pesananId);
		Paket paket = Paket.findById(lelang.pkt_id);
		boolean isSpse3 = paket.pkt_flag == 1;
		DaftarBarang daftarBarang = new DaftarBarang();
		renderArgs.put("lelang", lelang);
		if (pesananx != null) {
			daftarBarang = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
			if (paket.pkt_flag == 2){ //jika lelang SPSE4 rincian barang ambil dari rincian hps
				Sppbj sppbj = Sppbj.findByLelang(lelangId);
				Peserta peserta = sppbj == null ? null : Peserta.findByRekananAndLelang(sppbj.rkn_id, lelangId);
				daftarBarang = CommonUtil.fromJson(peserta.psr_dkh, DaftarBarang.class);
			}
		}
		renderArgs.put("daftarBarang", daftarBarang);
		renderArgs.put("pesananx", pesananx);
		renderArgs.put("isSpse3", isSpse3);
		renderTemplate("kontrak/rincianBarangFixed.html");
	}

	@AllowAccess({Group.PPK})
	public static void simpanSpmk(Long sppbjId, Long kontrakId ,Pesanan pesanan ,PesananContent content, String hapus) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
		if (kontrakx == null) {
			flash.error(Messages.get("flash.mabmds"));
			BerandaCtr.index();
		}
		Long lelangId = kontrakx.lls_id;
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);

		checkAuthenticity();
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		if(lelang.getKategori().isBarang()){
			validation.required(content.alamat_kirim).key("content.alamat_kirim").message(Messages.get("flash.aphd"));
			if(lelang.getPemilihan().isLelangExpress()){
				validation.required(content.jaminan).key("content.jaminan").message(Messages.get("flash.jcbhd"));
			}
		}else{
			validation.required(content.lingkup_pekerjaan).key("content.lingkup_pekerjaan").message(Messages.get("flash.lphd"));
		}

		validation.required(params.get("tgl_diterima")).key("tgl_diterima").message(Messages.get("flash.tbdhd"));
		validation.required(params.get("tgl_selesai")).key("tgl_selesai").message(Messages.get("flash.tpshd"));
		validation.required(pesanan.pes_no).key("pesanan.pes_no").message(Messages.get("flash.nphd"));
		validation.required(pesanan.pes_tgl).key("pesanan.pes_tgl").message(Messages.get("flash.tphd"));

		Date diterima = FormatUtils.formatDateFromDatePicker(params.get("tgl_diterima"));
		Date selesai = FormatUtils.formatDateFromDatePicker(params.get("tgl_selesai"));

		pesanan.kontrak_id = kontrakx.kontrak_id;

		validation.valid(pesanan);
		validation.valid(content);
		content.tgl_brng_diterima = diterima;
		content.tgl_pekerjaan_selesai = selesai;
		pesanan.pes_content = CommonUtil.toJson(content);

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("flash.datagt_ckia"));
			if (!lelang.getKategori().isBarang()) {
				editSpmk(sppbjId, kontrakx.kontrak_id,pesanan.pes_id);
			} else {
				editPesanan(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);
			}
		}

		pesanan.kontrak_id = kontrakx.kontrak_id;
		if (pesanan.pes_id == null) {
			pesanan.kontrak_id = kontrakx.kontrak_id;
		} else {
			Pesanan pesan = Pesanan.findById(pesanan.pes_id);
			if (pesan.pes_barang != null) {
				pesanan.pes_barang = pesan.pes_barang;
			}
		}
		try {

			pesanan.save();
			if (!lelang.getKategori().isBarang()) {
				flash.success(Messages.get("flash.bds" , "SPMK"));
			} else {
				flash.success(Messages.get("flash.bds", "Surat Pesanan"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			flash.error(Messages.get("flash.datagt_ckia"));
		}
		if (!lelang.getKategori().isBarang()) {
			editSpmk(sppbjId, kontrakx.kontrak_id,pesanan.pes_id);
		} else {
			editPesanan(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);
		}

	}

	@AllowAccess({Group.PPK})
	public static void simpanSuratPesanan(Long sppbjId, Long kontrakId ,Pesanan pesanan ,PesananContent content, String hapus) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
		if (kontrakx == null) {
			flash.error(Messages.get("flash.mabmds"));
			BerandaCtr.index();
		}
		Long lelangId = kontrakx.lls_id;
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);

		checkAuthenticity();
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		if(lelang.getKategori().isBarang()){
			params.flash();
			validation.required(content.alamat_kirim).key("content.alamat_kirim").message(Messages.get("flash.aphd"));
			if(lelang.getPemilihan().isLelangExpress()){
				params.flash();
				validation.required(content.jaminan).key("content.jaminan").message(Messages.get("flash.jcbhd"));
			}
		}else{
			params.flash();
			validation.required(content.lingkup_pekerjaan).key("content.lingkup_pekerjaan").message(Messages.get("flash.lphd"));
		}
		params.flash();
		validation.required(params.get("tgl_diterima")).key("tgl_diterima").message("flash.tbdhd");
		validation.required(params.get("tgl_selesai")).key("tgl_selesai").message("Tanggal Pekerjaan Selesai harus diisi");
		validation.required(pesanan.pes_no).key("pesanan.pes_no").message(Messages.get("flash.nphd"));
		validation.required(pesanan.pes_tgl).key("pesanan.pes_tgl").message(Messages.get("flash.tphd"));

		Date diterima = FormatUtils.formatDateFromDatePicker(params.get("tgl_diterima"));
		Date selesai = FormatUtils.formatDateFromDatePicker(params.get("tgl_selesai"));

		pesanan.kontrak_id = kontrakx.kontrak_id;

		validation.valid(pesanan);
		validation.valid(content);
		content.tgl_brng_diterima = diterima;
		content.tgl_pekerjaan_selesai = selesai;
		pesanan.pes_content = CommonUtil.toJson(content);

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("flash.datagt_ckia"));
			if (!lelang.getKategori().isBarang()) {
				editSpmk(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);
			} else {
				editPesanan(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);
			}
		}
			try {

				pesanan.save();
				if (!lelang.getKategori().isBarang()) {
					flash.success(Messages.get("flash.bds", "SPMK"));
				} else {
					flash.success(Messages.get("flash.bds", "Surat Pesanan"));
				}
			} catch (Exception e) {
				e.printStackTrace();
				flash.error(Messages.get("flash.datagt_ckia"));
				params.flash();
			}

			if (!lelang.getKategori().isBarang()) {
				editSpmk(sppbjId, kontrakx.kontrak_id,pesanan.pes_id);
			} else {
				editPesanan(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);
			}

	}

	@AllowAccess({Group.PPK})
	public static void hapusDokumenSuratPesanan(Long id, Integer versi) {
		Pesanan pesanan = Pesanan.findById(id);
		otorisasiDataLelang(pesanan.getKontrak().lls_id); // check otorisasi data lelang
		if (pesanan != null) {
			BlobTable blob = BlobTable.findById(pesanan.pes_attachment,versi);
			if (blob != null)
				blob.delete();
			if(pesanan.pes_attachment != null && CollectionUtils.isEmpty(pesanan.getDokumen())){
				pesanan.pes_attachment = null;
			}
			pesanan.save();
		}
	}

	public static void simpanRincian(Long lelangId, Pesanan pesananx, Date tgl_pes, String data, Double total) {
		Map<String, Object> result = new HashMap<>(1);
		if (pesananx != null) {
			DaftarBarang db = new DaftarBarang();
			db.items = CommonUtil.fromJson(data, new TypeToken<List<Rincian_Barang>>(){}.getType());
			if (db.items != null) {
				for (int i = 0; i < db.items.size(); i++) {
					if (db.items.get(i).item == null) {
						db.items.remove(i); // jika ada item yang kosong hapus
											// dari list
					} else {
						double ppn = ((db.items.get(i).harga == null ? 0 : db.items.get(i).harga)
								* (db.items.get(i).pajak == null ? 0 : db.items.get(i).pajak) / 100)
								* ((db.items.get(i).vol == null ? 0 : db.items.get(i).vol)
										+ (db.items.get(i).vol2 == null ? 0 : db.items.get(i).vol2));

						db.total_ppn = (db.total_ppn == null ? 0 : db.total_ppn) + ppn;
						db.total = (db.total == null ? 0 : db.total)
								+ (db.items.get(i).total_harga == null ? 0 : db.items.get(i).total_harga);
					}
				}
				db.total_pembayaran = db.total + db.total_ppn;
			}
			pesananx.db = db;
			pesananx.pes_barang = CommonUtil.toJson(db);
			pesananx.pes_tgl = tgl_pes;
			pesananx.save();
			result.put("result", "ok");
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void cetakPesanan(Long sppbjId, Long kontrakId, Long pesananId) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
		if(kontrakx == null){
			flash.error(Messages.get("flash.mabdmds"));
			BerandaCtr.index();
		}
		Long lelangId = kontrakx.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lelangId);
		InputStream is = Pesanan.cetak(kontrakx, lelang_seleksi, pesananId);
		response.contentType = "application/pdf";
		renderBinary(is);
	}

	@AllowAccess({ Group.PPK })
	public static void hapusPesanan(Long sppbjId, Long kontrakId, Long pesananId){
		Kontrak kontrakx = Kontrak.findById(kontrakId);
		if(kontrakx == null){
			flash.error(Messages.get("flash.mabmds"));
			BerandaCtr.index();
		}
		Long lelangId = kontrakx.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		List <Pesanan_barang> barang = Pesanan_barang.find("pes_id=?", pesananId).fetch();
		if(barang!=null){
			for(Pesanan_barang pes: barang){
				pes.delete();
			}
		}

		Pesanan pesanan = Pesanan.findById(pesananId);
		pesanan.delete();
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		if(!lelang.getKategori().isBarang()){
			flash.success(Messages.get("flash.dspmk_bd"));
			sppbjPpk(lelangId);
		}else{
			flash.success(Messages.get("flash.dspbd"));
			suratPesananPpk(sppbjId);
		}
	}

	@AllowAccess({ Group.PPK })
	public static void sskk(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
	    if (kontrak == null) {
	    	flash.error(Messages.get("flash.mabmds"));
			sppbjPpk(sppbj.lls_id);
		}
	    Long lelangId = sppbj.lls_id;
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		SskkPpkContent sskk_content;
		if (kontrak != null) {
			renderArgs.put("lelang", lelang);
			if (kontrak.kontrak_sskk_perubahan != null) {
				sskk_content = kontrak.getSskkContentPerubahan();
			} else if (kontrak.kontrak_sskk == null) {
				//jika sskk kosong maka ambil dari sskk di dokumen lelang
				Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
				sskk_content = dok_lelang.getSskkContentToSkkkPpk();
			} else {
				sskk_content = kontrak.getSskkContent();
			}

			renderViewSskk(sppbj, lelang, sskk_content, kontrak);
		} else {
			flash.error(Messages.get("flash.mabmds"));
			BerandaCtr.index();
		}
	}

	private static void renderViewSskk(Sppbj sppbj, Lelang_seleksi lelang, SskkPpkContent sskk_content, Kontrak kontrak){
		Map<String,Object> params = new HashMap<>();
		params.put("lelang", lelang);
		params.put("sskk_content", sskk_content);
		params.put("kategori", lelang.getKategori());
		params.put("kontrak", kontrak);
		params.put("sskkBaru", kontrak.kontrak_sskk == null);
		params.put("sppbj", sppbj);
		params.put("allowedExt", DokumenTypeCheck.allowedExt);
		if (lelang.getPemilihan().isLelangExpress()) {
			renderTemplate("kontrak/sskk_express.html",params);
		} else {
			renderTemplate("kontrak/sskkBarang.html",params);
		}
	}

	@AllowAccess({ Group.PPK })
	public static void simpanSskk(Long sppbjId, @Valid SskkPpkContent sskk_content, Kontrak kontrak, @DokumenType File attachment) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
		checkAuthenticity();
		Long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		File file = attachment;
		validation.valid(sskk_content);
		if(validation.hasErrors()) {
			validation.keep();
			if (sskk_content.kontrak_akhir != null && sskk_content.kontrak_mulai != null
					&& sskk_content.kontrak_akhir.before(sskk_content.kontrak_mulai)) {
				flash.error(Messages.get("flash.mbakm"));
			} else if (sskk_content.kontrak_akhir == null || sskk_content.kontrak_mulai == null) {
				flash.error(Messages.get("flash.tbkbd"));
			}
			params.flash();
			renderViewSskk(sppbj,lelang,sskk_content,kontrak);
		} else if (sskk_content != null) {
			Kontrak temp = Kontrak.findById(kontrak.kontrak_id);
			kontrak = temp;
			kontrak.kontrak_mulai = FormatUtils.formatDateFromDatePicker(params.get("sskk_content.kontrak_mulai"));
			kontrak.kontrak_akhir = FormatUtils.formatDateFromDatePicker(params.get("sskk_content.kontrak_akhir"));
			kontrak.kontrak_sskk = CommonUtil.toJson(sskk_content);

			if (temp != null) {

				if (file != null) {
					try {
						BlobTable bt = null;
						if (kontrak.kontrak_id != null) {
							// sppbj.sppbj_attachment =
							// Long.parseLong("123");
							if (kontrak.kontrak_sskk_attacment == null) {
								// jika baru upload file, gunakan fungsi
								// save file dengan 2 parameter
								bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);

							} else if (kontrak.kontrak_sskk_attacment != null) {
								// jika update file yang di-upload gunakan
								// fungsi save file dengan 3 parameter
								bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file,
										kontrak.kontrak_sskk_attacment);

							}
						} else {

							bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
						}
						kontrak.kontrak_sskk_attacment = bt.blb_id_content;
						kontrak.save();
						flash.success(Messages.get("flash.bds", "SSKK"));

					} catch (IOException e) {
						Logger.error(e, "Terjadi IOException @KontrakCtr.simpanSskk ");
					} catch (NoSuchAlgorithmException e) {
						Logger.error(e, "Terjadi NoSuchAlgorithmException @KontrakCtr.simpanSskk");
					} catch (SQLException e) {
						Logger.error(e, "Terjadi SQLException @KontrakCtr.simpanSskk");
					} catch (Exception e) {
						Logger.error(e, "Terjadi Exception @KontrakCtr.simpanSskk");
					}
				} else {

					try {

						kontrak.save();
						flash.success(Messages.get("flash.bds", "SSKK"));
					} catch (Exception e) {
						e.printStackTrace();
						flash.error(Messages.get("flash.datagt_ckia"));
					}
				}

			}
		}
		sskk(sppbjId);
	}

	@AllowAccess({ Group.PPK })
	public static void cetakSskk(Long sppbjId) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
	    Long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		InputStream is = Kontrak.cetak(sppbj);
		response.contentType = "application/pdf";
		renderBinary(is);
	}

	@AllowAccess({ Group.PPK })
	public static Kontrak hapusSSKK(Long id) {
		Kontrak kontrak = Kontrak.findById(id);
		kontrak.kontrak_sskk = null;
		kontrak.save();
		return kontrak;
	}

	@AllowAccess({ Group.PPK })
	public static void daftarPembayaran(Long kontrakId) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
	    if (kontrakx == null) {
	    	flash.error(Messages.get("flash.mabmds"));
	    	BerandaCtr.index();
		}
	    Long lelangId = kontrakx.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		renderArgs.put("kontrakx", kontrakx);
		renderArgs.put("listBap", BA_Pembayaran.find("kontrak_id=? order by bap_id asc", kontrakx.kontrak_id).fetch());
		SskkPpkContent sskk = kontrakx.getSskkContent();
		switch (sskk.cara_pembayaran) {
			case "Termin":
				renderTemplate("kontrak/daftarPembayaranTermin.html");
				break;
			case "Sekaligus":
				viewBap(kontrakx.kontrak_id, null);
				break;
			default:
				renderTemplate("kontrak/daftarPembayaranBulan.html");
				break;
		}
	}

	@AllowAccess({ Group.PPK })
	public static void buatDokumenLainnya(Long lelangId){
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		renderArgs.put("lelang",Lelang_seleksi.findById(lelangId));
		renderTemplate("kontrak/dokumenLainnya.html");

	}

	@AllowAccess({ Group.PPK })
	public static void dokumenLainnya(Long lelangId){
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		renderArgs.put("lelangId", lelangId);
		renderArgs.put("dokLainList", DokLain.find("lls_id=?", lelangId).fetch());
		renderTemplate("kontrak/daftarDokLain.html");

	}

	@AllowAccess({ Group.PPK })
	public static void editDokumenLainnya(Long lelangId, Long dlpId){
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		renderArgs.put("lelang", Lelang_seleksi.findById(lelangId));
		renderArgs.put("dokLain",DokLain.find("lls_id=? AND dlp_id=?", lelangId, dlpId).first());
		renderTemplate("kontrak/dokumenLainnya.html");

	}

	@AllowAccess({ Group.PPK })
	public static void simpanDokumenLain(Long lelangId, DokLain dokLain, @DokumenType FileUpload dlpAttachment, String hapus) {

		checkAuthenticity();

		otorisasiDataLelang(lelangId); // check otorisasi data lelang

		File file=null;

		dokLain.lls_id = lelangId;

		if(dlpAttachment !=null)
			file=dlpAttachment.asFile();

		validation.valid(dokLain);

		if(validation.hasErrors()) {

			validation.keep();

			params.flash();

			if (dokLain.dlp_id != null)
				editDokumenLainnya(dokLain.lls_id, dokLain.dlp_id);
			else
				buatDokumenLainnya(dokLain.lls_id);

		} else {

			try {

				DokLain.simpanDokLain(dokLain, file, hapus);

				flash.success(Messages.get("flash.dbd"));

				dokumenLainnya(dokLain.lls_id);

			}catch (Exception e) {

				Logger.error(e, "Data gagal disimpan");

				flash.error(Messages.get("flash.dgd"));

				if (dokLain.dlp_id != null)
					editDokumenLainnya(dokLain.lls_id, dokLain.dlp_id);
				else
					buatDokumenLainnya(dokLain.lls_id);

			}

		}

	}


	@AllowAccess({ Group.PPK })
	public static void viewBap(Long kontrakId, Long bapId) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
		otorisasiDataLelang(kontrakx.lls_id); // check otorisasi data lelang
		Lelang_detil lelang_detil = Lelang_detil.findById(kontrakx.lls_id);
		renderArgs.put("lelang_detil", lelang_detil);
		renderArgs.put("kontrakx", kontrakx);
		SskkPpkContent sskk = kontrakx.getSskkContent();
		renderArgs.put("tipePembayaran", sskk.cara_pembayaran);
		renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
		BA_Pembayaran bapx;
		if (bapId != null) {
			bapx = BA_Pembayaran.findById(bapId);
			bapx.besar_pembayaran = bapx.besar_pembayaran == 0 ? kontrakx.kontrak_nilai : bapx.besar_pembayaran;
			renderArgs.put("bapx", bapx);
		} else if (sskk.cara_pembayaran.equals("Sekaligus")) {
			bapx = BA_Pembayaran.find("kontrak_id=?", kontrakId).first();
			renderArgs.put("bapx", bapx);
		}
		renderTemplate("kontrak/form-ba-pembayaran.html");
	}

	// Penambahan @Valid untuk validasi form
	@AllowAccess({ Group.PPK })
	public static void simpanBap(Long kontrak_id, BA_Pembayaran bap, String hapus) {
		checkAuthenticity();
		bap.kontrak_id = kontrak_id;
		Kontrak kontrak = Kontrak.findById(kontrak_id);
		otorisasiDataLelang(kontrak.lls_id); // check otorisasi data lelang
		if (bap.bast_tgl!=null && bap.bast_tgl.before(kontrak.kontrak_tanggal)) {
			params.flash();
			flash.error(Messages.get("flash.tbtmtk"));
			viewBap(bap.kontrak_id, bap.bap_id);
		}

		if (bap.bap_tgl != null && bap.bap_tgl.before(kontrak.kontrak_tanggal)) {
			params.flash();
			flash.error(Messages.get("flash.tbtmtk"));
			viewBap(bap.kontrak_id, bap.bap_id);
		}
		validation.valid(bap);

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			viewBap(bap.kontrak_id, bap.bap_id);
		} else {
			try {
				bap.save();
				flash.success("%s berhasil disimpan", "Berita Pembayaran");
			} catch (Exception e) {
				Logger.error(e, Messages.get("flash.bds" , "Berita Pembayaran"));
				flash.error(Messages.get("flash.gds", "Berita Pembayaran"));
			}
		}
		viewBap(bap.kontrak_id, bap.bap_id);
	}

	@AllowAccess({ Group.PPK })
	public static void cetakBap(Long bapId) {
		response.contentType = "application/pdf";
		InputStream is = BA_Pembayaran.cetak(bapId);
		renderBinary(is);
	}

	@AllowAccess({ Group.PPK })
	public static void cetakBast(Long bapId) {
		response.contentType = "application/pdf";
		InputStream is = BA_Pembayaran.cetak_bast(bapId);
		renderBinary(is);
	}

	@AllowAccess({ Group.PPK })
	public static void hapus(Long kontrakId, List<Long> bapId) {
		Kontrak kontrak = Kontrak.findById(kontrakId);
		Long lelangId = kontrak.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		StringBuilder param = new StringBuilder();
		// BA_Pembayaran obj = null;
		// int i=0;
		if (bapId == null) {
			BA_Pembayaran.delete("kontrak_id =? ", kontrak.kontrak_id);
		} else {
			for (Long id : bapId) {
				if (id != null) {
					if (param.length() > 0)
						param.append(',');
					param.append(id);
				}

				if (param.length() > 0) {
					BA_Pembayaran.delete("kontrak_id =? AND bap_id NOT IN (" + param + ')',
							kontrak.kontrak_id);
				}
			}
		}
		daftarPembayaran(kontrak.kontrak_id);
	}

	@AllowAccess({ Group.PPK })
	public static void spmk(Long sppbjId, Long pes_id) {
		Sppbj sppbj = Sppbj.findById(sppbjId);
		if(sppbj == null){
			flash.error(Messages.get("flash.mabmd_sppbj"));
			BerandaCtr.index();
		}
		Kontrak kontrakx = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
		if (kontrakx == null) {
			flash.error(Messages.get("flash.mabmds"));
			sppbjPpk(sppbj.lls_id);
		}

		Long lelangId = sppbj.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		Pesanan pesananx = Pesanan.find("kontrak_id=?", kontrakx.kontrak_id).first();
		renderArgs.put("lelangId", lelangId);
		renderArgs.put("lelang", lelang);
		renderArgs.put("kontrakx", kontrakx);
		renderArgs.put("sppbj", sppbj);
		renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
		renderArgs.put("pesananx", pesananx);
		if (pesananx != null) {
			PesananContent content = pesananx.getPesananContent();
			renderArgs.put("content", content);
			renderTemplate("kontrak/SPMK.html");
		} else {
			renderTemplate("kontrak/SPMK.html");
		}
	}

	@AllowAccess({ Group.PPK })
	public static void editSpmk(Long sppbjId, Long kontrakId, Long pesananId) {
		Kontrak kontrakx = Kontrak.findById(kontrakId);
		if(kontrakx == null){
			flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
			BerandaCtr.index();
		}
		Long lelangId = kontrakx.lls_id;
		otorisasiDataLelang(lelangId); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		renderArgs.put("sppbj", Sppbj.findById(sppbjId));
		renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
		renderArgs.put("lelang", lelang);
		renderArgs.put("kontrakx", kontrakx);
		if (pesananId != null) {
			Pesanan pesananx = Pesanan.findById(pesananId);
			renderArgs.put("tgl_pes", pesananx.pes_tgl);
			renderArgs.put("content", pesananx.getPesananContent());
			renderArgs.put("pesananx", pesananx);
		}
		renderTemplate("kontrak/SPMK.html");
	}


	public static Integer pengecekanBlacklist(Long rknId, Long llsId) {
		Rekanan rekanan = Rekanan.findById(rknId);
		Active_user active_user = Active_user.current();

		return BlacklistCheckerUtil.checkBlacklistStatus(rekanan, active_user.pegawaiId, llsId, 2);
	}

	@AllowAccess({ Group.PPK })
	public static void undanganKontrak(Long id, Long pst_id) {
		Sppbj sppbj = Sppbj.findById(id);
		Peserta peserta = Peserta.findById(pst_id);
   	 	otorisasiDataPeserta(peserta); // check otorisasi data peserta
		renderArgs.put("peserta", peserta);
		renderArgs.put("sppbj", sppbj);
		renderArgs.put("lelang", peserta.getLelang_seleksi());
		renderArgs.put("list", MailQueueDao.findUndanganKontrak(peserta.lls_id, peserta.rkn_id, JenisEmail.UNDANGAN_KONTRAK.id));
   	 	renderTemplate("kontrak/form-undangan.html");
	}

	@AllowAccess({ Group.PPK })
	public static void submitUndangan(Long id, @Required @As(binder=DatetimeBinder.class) Date waktu,
									  @Required @As(binder=DatetimeBinder.class) Date sampai, @Required String tempat,
									  @Required String dibawa, @Required String hadir) {
		checkAuthenticity();
		Peserta peserta = Peserta.findById(id);
   	 	otorisasiDataPeserta(peserta); // check otorisasi data peserta
   	 	Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
    	if(validation.hasErrors()){
    		validation.keep();
    		params.flash();
       	 	flash.error(Messages.get("flash.ugtmpkia"));
    	} else {
    		try {
	    		String namaPaket = Paket.getNamaPaketFromlelang(peserta.lls_id);
	    		String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
	    		MailQueueDao.kirimUndanganKontrak(peserta.getRekanan(), peserta.lls_id, namaPaket, namaPanitia, waktu, sampai, tempat, dibawa, hadir);
	    		flash.success(Messages.get("flash.ubt"));
    		}catch(Exception e) {
    			flash.error(Messages.get("flash.tktpu"));
    			Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Undangan Kontrak");
    		}
    	}
   	 	KontrakCtr.sppbjPpk(lelang.lls_id);
	}

	@AllowAccess({Group.PPK})
	public static void pilihCaraPembayaran(Long id){
		Sppbj sppbj = Sppbj.findById(id);
		renderArgs.put("sppbjId", id);

		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
		renderArgs.put("kontrak", kontrak);
		renderArgs.put("dokKontrak",kontrak.kontrak_sskk_attacment != null ?
				BlobTableDao.listById(kontrak.kontrak_sskk_attacment) : null);

		SskkPpkContent sskkContent = kontrak.getSskkContent();
		if(sskkContent == null)
			sskkContent = new SskkPpkContent();
		renderArgs.put("sskkContent",sskkContent);

		Lelang_seleksi lelang = Lelang_seleksi.findById(sppbj.lls_id);
		DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(lelang.pkt_id);
		renderArgs.put("sskk",dokPersiapan.getDokSskkAttachment());

		renderTemplate("kontrak/form-pilih-cara-pembayaran.html");
	}

	@AllowAccess({Group.PPK})
	public static void simpanCaraPembayaran(Long id, SskkPpkContent sskkContent){
		Sppbj sppbj = Sppbj.findById(id);
		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
		kontrak.kontrak_sskk = CommonUtil.toJson(sskkContent);
		kontrak.save();

		sppbjPpk(sppbj.lls_id);
	}

	@AllowAccess({Group.PPK})
	public static void uploadSskkSubmit(Long id, File file) {
//		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Sppbj sppbj = Sppbj.findByLelang(id);
		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();

		Map<String, Object> result = new HashMap<>(1);
		try {
			UploadInfo model = kontrak.simpanSskkAttachment(file);
			if (model == null) {
				result.put("success", false);
				result.put("message", "File dengan nama yang sama telah ada di server!");
				renderJSON(result);
			}
			List<UploadInfo> files = new ArrayList<>();
			files.add(model);
			result.put("success", true);
			result.put("files", files);
		} catch (Exception e) {
			Logger.error(e, e.getLocalizedMessage());
			result.put("result", "Kesalahan saat upload sskk");
		}

		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void hapusSskkAttachment(Long id, Integer versi) {
//		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Sppbj sppbj = Sppbj.findByLelang(id);
		Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();

		if (kontrak != null) {
			BlobTable blob = BlobTable.findById(kontrak.kontrak_sskk_attacment,versi);
			if (blob != null){
				blob.delete();
			}
			if (kontrak.kontrak_sskk_attacment != null && kontrak.getDokSskkAttachment().isEmpty()) {
				kontrak.kontrak_sskk_attacment = null;
			}
			kontrak.save();
		}

	}

	@AllowAccess({Group.PPK})
	public static void rincianHargaPemenang(Long id, Long rekananId){
		otorisasiDataLelang(id);
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);

		Long pesertaId = null;
		DaftarKuantitas dk = null;
		TempSppbjHps temp = TempSppbjHps.findByLelangAndPpk(id);
		if(temp == null){
			if(rekananId == null){
				List<Peserta> pesertaList = lelang.getPesertaList();
				for(Peserta peserta : pesertaList){
					if(peserta.isPemenang() || (lelang.isExpress() && peserta.isPemenangVerif())){
						dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);
						pesertaId = peserta.psr_id;
						break;
					}
				}
			}else{
				Peserta peserta = Peserta.findByRekananAndLelang(rekananId,id);
				dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);
				pesertaId = peserta.psr_id;
			}
		}else{
			dk = CommonUtil.fromJson(temp.dkh, DaftarKuantitas.class);
			pesertaId = temp.psr_id;
		}

		String data = "[]";
		if(dk != null){
			if (dk.items != null) {
				data = CommonUtil.toJson(dk.items);
			}
		}
		renderArgs.put("pesertaId", pesertaId);
		renderArgs.put("lelang", lelang);
		renderArgs.put("data", data);
		renderTemplate("kontrak/sppbjHpsKonsolidasi.html");
	}

	@AllowAccess({Group.PPK})
	public static void resetHargaFinalKonsolidasi(Long id, Long pesertaId){
		otorisasiDataLelang(id);

		Peserta peserta = Peserta.findBy(pesertaId);
		DaftarKuantitas dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);

		//Reset yang ada di tabel temporary
		TempSppbjHps temp = TempSppbjHps.find("ppk_id = ? and lls_id = ?",Active_user.current().ppkId,id).first();
		if(temp != null){
			temp.dkh = peserta.psr_dkh;
			temp.save();
		}

		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("pesertaId", pesertaId);
		renderArgs.put("lelang", lelang);
		renderArgs.put("data", CommonUtil.toJson(dk.items));
		renderTemplate("kontrak/sppbjHpsKonsolidasi.html");
	}

	@AllowAccess({Group.PPK})
	public static void hapusDokSppbj(Long id, Integer versi) {
		Sppbj sppbj = Sppbj.findById(id);
		if (sppbj != null) {
			BlobTable blob = BlobTable.findById(sppbj.sppbj_attachment, versi);
			if (blob != null)
				blob.delete();
			if (sppbj.sppbj_attachment != null && CollectionUtils.isEmpty(sppbj.getDokumen())) {
				sppbj.sppbj_attachment = null;
			}
			sppbj.save();
		}
	}

	@AllowAccess({ Group.PPK })
	public static void uploadDokSppbj(Long id, File file) {
		Sppbj sppbj = Sppbj.findById(id);
		Map<String, Object> result = new HashMap<>(1);
		if(sppbj != null) {
			otorisasiDataLelang(sppbj.getLelang_seleksi()); // check otorisasi data lelang
			try {
				UploadInfo model = sppbj.simpanDok(file);
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload SPPBJ");
				result.put("result", "Kesalahan saat upload SPPBJ");
			}
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void uploadDokPesanan(Long id, File file) {
		Pesanan pesanan = Pesanan.findById(id);
		Map<String, Object> result = new HashMap<>(1);
		if(pesanan != null) {
			otorisasiDataLelang(pesanan.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
			try {
				UploadInfo model = pesanan.simpanDok(file);
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Pesanan");
				result.put("result", "Kesalahan saat upload Pesanan");
			}
		}
		renderJSON(result);
	}

	@AllowAccess({ Group.PPK })
	public static void uploadDokBap(Long id, File file) {
		BA_Pembayaran bapx = BA_Pembayaran.findById(id);
		Map<String, Object> result = new HashMap<>(1);
		if(bapx != null) {
			otorisasiDataLelang(bapx.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
			try {
				UploadInfo model = bapx.simpanDokBap(file);
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload BAP");
				result.put("result", "Kesalahan saat upload BAP");
			}
		}
		renderJSON(result);
	}

	@AllowAccess({Group.PPK})
	public static void hapusDokBap(Long id, Integer versi) {
		BA_Pembayaran bapx = BA_Pembayaran.findById(id);
		otorisasiDataLelang(bapx.getKontrak().lls_id); // check otorisasi data lelang
		if (bapx != null) {
			BlobTable blob = BlobTable.findById(bapx.cetak_bap_attachment, versi);
			if (blob != null)
				blob.delete();
			if (bapx.cetak_bap_attachment != null && CollectionUtils.isEmpty(bapx.getDokumenBap())) {
				bapx.cetak_bap_attachment = null;
			}
			bapx.save();
		}
	}

	@AllowAccess({ Group.PPK })
	public static void uploadDokBast(Long id, File file) {
		BA_Pembayaran bapx = BA_Pembayaran.findById(id);
		Map<String, Object> result = new HashMap<>(1);
		if(bapx != null) {
			otorisasiDataLelang(bapx.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
			try {
				UploadInfo model = bapx.simpanDokBast(file);
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload BAST");
				result.put("result", Messages.get("flash.ksub"));
			}
		}
		renderJSON(result);
	}

	@AllowAccess({Group.PPK})
	public static void hapusDokBast(Long id, Integer versi) {
		BA_Pembayaran bapx = BA_Pembayaran.findById(id);
		otorisasiDataLelang(bapx.getKontrak().lls_id); // check otorisasi data lelang
		if (bapx != null) {
			BlobTable blob = BlobTable.findById(bapx.cetak_bast_attachment, versi);
			if (blob != null)
				blob.delete();
			if (bapx.cetak_bast_attachment != null && CollectionUtils.isEmpty(bapx.getDokumenBast())) {
				bapx.cetak_bast_attachment = null;
			}
			bapx.save();
		}
	}
}