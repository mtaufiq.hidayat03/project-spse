package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import models.common.Active_user;
import models.common.Tahap;
import models.common.TahapStarted;
import models.jcommon.util.CommonUtil;
import models.lelang.Diskusi_lelang;
import models.lelang.Diskusi_lelang.Jenis_diskusi;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * definisi semua action terkait penjelasan/aanwijzing 
 *  
 * @author arief ardiyansah
 * 
 */
public class PenjelasanCtr extends BasicCtr {

	/**
	 * tahapan penjelasan dokumen pengadaan
	 * @param id
	 */
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void pengadaan(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Active_user active_user = Active_user.current();
		Tahap tahap = Tahap.PENJELASAN;
		Group group = active_user.group;
		boolean allow = Diskusi_lelang.isWriteable(id, tahap, group, newDate()) && lelang.lls_status.isAktif();
		if(active_user.isRekanan()) {
			Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", id, active_user.rekananId).first();
			if(lelang.isPrakualifikasi() && peserta != null && !peserta.isLulusPembuktian()) {
				forbidden(Messages.get("not-authorized"));
			}
			renderArgs.put("isPeserta", allow && peserta != null);
		}
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("allow", allow);
		renderArgs.put("group", group);
		renderArgs.put("pembukaan", Diskusi_lelang.findPembukaan(id, tahap));
		Map params = new HashMap<String, Object>(1);
		params.put("id", lelang.lls_id);
		renderArgs.put("simpanUrl", Router.reverse("lelang.PenjelasanCtr.simpan", params));
		renderArgs.put("sisaUrl", Router.reverse("lelang.PenjelasanCtr.sisa_waktu", params));
		renderArgs.put("listUrl", Router.reverse("lelang.PenjelasanCtr.list_pengadaan", params));
		renderArgs.put("pembukaanUrl",Router.reverse("lelang.PenjelasanCtr.pembukaan_pengadaan" ,params));
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderTemplate("lelang/penjelasan/penjelasan.html");
	}
	
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void list_pengadaan(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.PENJELASAN;
		Group group = Active_user.current().group;
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        TahapStarted tahapStarted = lelang.getTahapStarted();
		List<Diskusi_lelang> list=Diskusi_lelang.getDiskusiLelangTopik(id, tahap);
		renderArgs.put("tahap", tahap);
		renderArgs.put("list", list);
		renderArgs.put("group", group);
		renderArgs.put("allow", Diskusi_lelang.isWriteable(id, tahap, group, newDate()) && lelang.lls_status.isAktif());
		renderArgs.put("hide", !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca()));
		renderTemplate("lelang/penjelasan/penjelasan-list.html");
	}
	
	public static void sisa_waktu(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		String sisa = Diskusi_lelang.getSisaWaktu(id, Tahap.PENJELASAN);
		renderText(sisa);
	}
	
	/**
	 * tahapan penjelasan prakualifikasi
	 * @param id
	 */
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void kualifikasi(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Tahap tahap = Tahap.PENJELASAN_PRA;
		Active_user active_user = Active_user.current();
		Group group = active_user.group;
		boolean allow = Diskusi_lelang.isWriteable(id, tahap, group, newDate()) && lelang.lls_status.isAktif() ;
		Diskusi_lelang pembukaan = Diskusi_lelang.findPembukaan(id, tahap);
		if(active_user.isRekanan()) {
			Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", id, active_user.rekananId).first();
			boolean isPeserta = allow && peserta != null;
			renderArgs.put("isPeserta", isPeserta);
		}
		Map params = new HashMap<String, Object>(1);
		params.put("id", lelang.lls_id);
		renderArgs.put("simpanUrl", Router.reverse("lelang.PenjelasanCtr.simpan_kualifikasi", params));
		renderArgs.put("sisaUrl", Router.reverse("lelang.PenjelasanCtr.sisa_waktu_kualifikasi", params));
		renderArgs.put("listUrl", Router.reverse("lelang.PenjelasanCtr.list_kualifikasi", params));
		renderArgs.put("pembukaanUrl",Router.reverse("lelang.PenjelasanCtr.pembukaan_kualifikasi" ,params));
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("allow", allow);
		renderArgs.put("tahap", tahap);
		renderArgs.put("pembukaan", pembukaan);
		renderArgs.put("group", group);
		renderTemplate("lelang/penjelasan/penjelasan.html");
	}	
	
	@AllowAccess({ Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK })
	public static void list_kualifikasi(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.PENJELASAN_PRA;
		Group group = Active_user.current().group;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		List<Diskusi_lelang> list = Diskusi_lelang.getDiskusiLelangTopik(id, tahap);
		renderArgs.put("tahap", tahap);
		renderArgs.put("list", list);
		renderArgs.put("group", group);
		renderArgs.put("allow", Diskusi_lelang.isWriteable(id, tahap, group, newDate()) && lelang.lls_status.isAktif() );
		renderArgs.put("hide", !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca()));
		renderTemplate("lelang/penjelasan/penjelasan-list.html");
	}
	
	public static void sisa_waktu_kualifikasi(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		String sisa = Diskusi_lelang.getSisaWaktu(id, Tahap.PENJELASAN_PRA);
		renderText(sisa);
	}
	
	@AllowAccess({ Group.PANITIA, Group.REKANAN})
	public static void simpan(Long id, Diskusi_lelang penjelasan, Long pertanyaan_id, @Valid @DokumenType File file) throws Exception {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang

		validation.valid(penjelasan);

		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			StringBuilder sb = new StringBuilder();
			for(play.data.validation.Error err:validation.errors()) {
				if(!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}
			flash.error(Messages.get("flash.dgt")+ sb);
		} else {
			Tahap tahap = Tahap.PENJELASAN;
			if (CommonUtil.isEmpty(penjelasan.dsl_uraian)) {
				validation.addError("penjelasan.dsl_uraian", Messages.get("flash.utbk"));
			} else {
				Diskusi_lelang.simpan(id, penjelasan, tahap, file, pertanyaan_id);
			}
		}

		pengadaan(id);

	}
	
	@AllowAccess({ Group.PANITIA, Group.REKANAN})
	public static void simpan_kualifikasi(Long id, Diskusi_lelang penjelasan, Long pertanyaan_id, File file) throws Exception {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.PENJELASAN_PRA;
		if (CommonUtil.isEmpty(penjelasan.dsl_uraian)) {
			validation.addError("penjelasan.dsl_uraian",Messages.get("flash.utbk"));
		}
		else {						
			Diskusi_lelang.simpan(id, penjelasan, tahap, file, pertanyaan_id);
		}
		kualifikasi(id);
	}
	
	@AllowAccess({ Group.PANITIA})
	public static void pembukaan_kualifikasi(Long id, String uraian) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.PENJELASAN_PRA;		
		Map<String, Object> result = new HashMap<String, Object>(1);
		if (CommonUtil.isEmpty(uraian)) {
			result.put("result",Messages.get("flash.utbk"));
		}
		else {						
			Diskusi_lelang pembukaan = Diskusi_lelang.findPembukaan(id, tahap);
			if(pembukaan == null) {
				pembukaan = new Diskusi_lelang();
				pembukaan.dsl_jenis = Jenis_diskusi.PEMBUKAAN;
			}
			pembukaan.dsl_uraian = uraian;
			Diskusi_lelang.simpan(id, pembukaan, tahap, null, null);
			result.put("result", "ok");
		}
		renderJSON(result);
	}
	
	@AllowAccess({ Group.PANITIA})
	public static void pembukaan_pengadaan(Long id, String uraian) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Tahap tahap = Tahap.PENJELASAN;
		Map<String, Object> result = new HashMap<String, Object>(1);
		if (CommonUtil.isEmpty(uraian)) {
			result.put("result",Messages.get("flash.utbk"));
		}
		else {					
			Diskusi_lelang pembukaan = Diskusi_lelang.findPembukaan(id, tahap);
			if(pembukaan == null) {
				pembukaan = new Diskusi_lelang();
				pembukaan.dsl_jenis = Jenis_diskusi.PEMBUKAAN;
			}
			pembukaan.dsl_uraian = uraian;
			Diskusi_lelang.simpan(id, pembukaan, tahap, null, null);
			result.put("result", "ok");
		}
		renderJSON(result);
	}

}
