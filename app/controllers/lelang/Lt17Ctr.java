package controllers.lelang;

import ams.utils.UserAgent;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import ext.FormatUtils;
import jobs.SendMailJobNow;
import models.agency.DaftarKuantitas;
import models.agency.Paket;
import models.agency.Pegawai;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.apendo.Datum;
import models.lelang.apendo.PenawaranHarga;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.Usergroup;
import models.tus.UuidStore;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.Logger;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http.Cookie;
import play.mvc.Router;
import utils.Encrypt;
import utils.Helpers;
import utils.JsonUtil;
import utils.LogUtil;
import utils.osd.KmsClient;
import utils.osd.OSDUtil;
import utils.tus.TusHandler;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author sgp
 */
public class Lt17Ctr extends Controller {

	public static final String ROOT_URL_PENAWARAN = BasicCtr.CONTEXT_PATH + "/lt17";
	private static final String k = "sk_apd_4fcb0e2c-165e-49da-a13f-628df708e722";
	public static String versiApendoBuildSistem = "5.1.2";
//	public static String versiApendoSistem = "4.2.0";
	public static final String SPAMKODOK_VERSI = "2.0.3";

	// issue #26 --> https://gitlab.lkpp.go.id/eproc/apendo-4/issues/26
	private static void checkUserAgent(boolean isOSD) {
//		String versiSpseSistem = ConfigurationDao.SPSE_VERSION; // x.y.z
		
		if (isOSD){
			versiApendoBuildSistem = SPAMKODOK_VERSI;
		}
		String invalidApendoMsg = versiApendoBuildSistem;
		
		if(request.headers.get("user-agent")==null)
			invalid_apendo(invalidApendoMsg, isOSD);
		
		String userAgent = request.headers.get("user-agent").value();

		// e.g. "Apendo/4.2.0 (23 Maret 2016; build 4.8.7) LT17/1.0 (23 Maret 2016) Qt/4.8.4 LKPPRI-SPSE/4.1"
		if(userAgent == null) {
			invalid_apendo(invalidApendoMsg, isOSD);
		}
		Pattern r = UserAgent.APENDO_PATTERN;
		if(isOSD)
			r = UserAgent.AMANDA_PATTERN;
	    Matcher m = r.matcher(userAgent);
	    if(!m.find()) {
			invalid_apendo(invalidApendoMsg, isOSD);
	    }
	    try {
	    	Logger.info("user agent : %s", userAgent);
			String versiApendoBuildHeader = m.group(1); // x.y.z
			if(!Helpers.validApendoVersion(versiApendoBuildSistem, versiApendoBuildHeader)) {
				invalid_apendo(invalidApendoMsg, isOSD);
			}
	    } catch (Exception e) {
	    	Logger.error(e, "Error parsing user-agent header:" + e.getMessage());
			invalid_apendo(invalidApendoMsg, isOSD);
	    }
	}

	private static void checkSignature() {		
		if(request.headers.get("apendo-signature")==null) {
			Logger.info("apendo-signature header is NULL");
			not_authorized();
		}
		String apendoSignature = request.headers.get("apendo-signature").value();
		// format: timestamp|randomstring|hash
		if(apendoSignature == null) {
			Logger.info("apendo-signature is NULL");
			not_authorized();
		}
		try {
			String[] values = apendoSignature.split("\\|");
			String timestamp = values[0];
			String randomstring = values[1];
			String hashSent = values[2];
			Logger.info("hashSent>>>"+hashSent);
			String toDigest = request.method + request.getBase() + request.url;
			String key = '{' + k + "}-{" + timestamp + "}-{" + randomstring + '}';
			String hashToCompare = Base64.encodeBase64String(HmacUtils.hmacSha1(key, toDigest));
			Logger.info("toDigest>>>"+toDigest);
			Logger.info("hashToCompare>>>"+hashToCompare);
			if(!hashSent.equals(hashToCompare)) {
				Logger.info("hashToCompare is DIFFERENT");
				// dimatiin dulu karena ada case di Serang hasil hash-nya berbeda
			}
		} catch (Exception e) {
			Logger.error(e, "Error parsing apendo-signature header:" + e.getMessage());
	    	not_authorized();
		}
	}

	@Before(unless = {"not_authorized","invalid_apendo","invalid_jadwal", "not_authorized_json","invalid_apendo_json","invalid_jadwal_json"})
	static void interuptor() {
		String access_token = params.get("access_token") != null ? params.get("access_token") : (request.headers.get("access_token") == null ? "" : request.headers.get("access_token").value());
		if (StringUtils.isEmpty(access_token)){ // forbidden access_token
			not_authorized();
			return;
		}
		
		Lelang_token lelang_token = Lelang_token.find("llt_key=?", access_token).first();
		if (lelang_token == null) {// forbidden : token tidak valid
			Logger.info("lelang_token is NULL");
			not_authorized();
			return;
		}
		boolean auditor = false;
		if(lelang_token.peg_id != null) {
			Pegawai pegawai = Pegawai.findById(lelang_token.peg_id);
			if(pegawai != null) {
				renderArgs.put("pegawai", pegawai);
				Group group = Usergroup.findGroupByUser(pegawai.peg_namauser);
				if (group != null) {
					auditor = group.isAuditor();
					renderArgs.put("auditor", group.isAuditor());
				}
			}
		}
		
		Lelang_seleksi lelang = Lelang_seleksi.findById(lelang_token.lls_id);
		if(!lelang.lls_status.isAktif() && !auditor)
			unauthorized();
		Paket paket = lelang.getPaket();
		boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
		checkUserAgent(isOSD);
		if(!isOSD)
			checkSignature();
		
		if (ConfigurationDao.isLatihan()) {
			int sesi = Query.find("select lls_sesi from lelang_seleksi where lls_id=?", Integer.class, lelang_token.lls_id).first();
			Cookie cookie = request.cookies.get("sesiPelatihan");
			if (cookie == null) {
				cookie = new Cookie("sesiPelatihan", String.valueOf(sesi));
			}
			request.cookies.put("sesiPelatihan", cookie);
		}
		if(lelang_token.isExpire()) {
			Logger.error("lelang_token is expire : %s", lelang_token.llt_expired);
			not_authorized();
		}
		renderArgs.put("access_token", access_token);
		renderArgs.put("lelang_token", lelang_token);
		renderArgs.put("lelang", lelang);
	}

	/**
	 * Dieksekusi saat penyedia drag ke Apendo
	 */
	public static void index() {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);		
		Lelang_seleksi lelang = renderArgs.get("lelang", Lelang_seleksi.class);
		Paket paket = lelang.getPaket();
		boolean express = lelang.getPemilihan().isLelangExpress();
		renderArgs.put("latihan",!ConfigurationDao.isProduction());
		renderArgs.put("express", express);
		renderArgs.put("lelang", lelang);
		renderArgs.put("paket", paket);
		renderArgs.put("instansi", paket.getNamaInstansi());
		renderArgs.put("namaLpse", ConfigurationDao.getNamaLpse());
		if (lelang_token.psr_id != null) {
			renderPeserta(lelang_token, lelang, paket);
		} else if (lelang_token.peg_id != null) {
			renderPanitia(lelang_token, lelang, paket);
		}
	}

	private static void renderPanitia(Lelang_token lelang_token, Lelang_seleksi lelang, Paket paket) {
		TahapStarted tahapStarted = lelang.getTahapStarted();
		if (tahapStarted.isPembukaan()) {
			Pegawai pegawai = renderArgs.get("pegawai", Pegawai.class);
			renderArgs.put("downloadUrl",BasicCtr.getRequestHost() + Router.reverse("lelang.Lt17Ctr.download").url);
			renderArgs.put("sortUrl", BasicCtr.getRequestHost() + Router.reverse("lelang.Lt17Ctr.sort").url);
			if (lelang.isSatuFile()) {
				renderArgs.put("keyAll", Lelang_key.getPrivateKey(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA, lelang, paket, pegawai.peg_namauser));
				renderArgs.put("dokHargaList",Dok_penawaran.findAllPenawaranHarga(lelang.lls_id, lelang.isSatuFile()));
			} else {
				List<Dok_penawaran> dokTeknisList = new ArrayList<>();
				List<Dok_penawaran> dokHargaList = new ArrayList<>();
				if (tahapStarted.isPembukaanTeknis() && lelang_token.llt_jenis.isAdmTeknis()) {
					renderArgs.put("keyAdmTeknis", Lelang_key.getPrivateKey(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI, lelang, paket, pegawai.peg_namauser));
					dokTeknisList = Dok_penawaran.findAllPenawaranTeknis(lelang.lls_id);
				}
				if (tahapStarted.isPembukaanHarga() && lelang_token.llt_jenis.isHarga()) {
					renderArgs.put("keyHarga", Lelang_key.getPrivateKey(JenisDokPenawaran.PENAWARAN_HARGA, lelang, paket, pegawai.peg_namauser));
					dokHargaList = Dok_penawaran.findAllPenawaranHarga(lelang.lls_id, lelang.isSatuFile());
				}
				renderArgs.put("dokTeknisList", dokTeknisList);
				renderArgs.put("dokHargaList", dokHargaList);
			}
			renderTemplate("lelang/lt17/panitia.html");
		} else {
			Logger.info("Bukan tahap pembukaan penawaran");
			invalid_jadwal(Messages.get("flash.matdmpp"));
		}
	}

	private static void renderPeserta(Lelang_token lelang_token, Lelang_seleksi lelang, Paket paket) {
		TahapNow tahapNow = lelang.getTahapNow();
		if (tahapNow.isPemasukanPenawaran() || tahapNow.isPemasukanPenawaranAdmTeknis() || tahapNow.isPemasukanPenawaranHarga()) {
			Peserta peserta = Peserta.findById(lelang_token.psr_id);
			renderArgs.put("peserta", peserta);
			DateTime dateTime = new DateTime(BasicCtr.newDate());
			Long versi = 0L;
			Dok_lelang dok_lelang = Dok_lelang.findBy(lelang_token.lls_id, JenisDokLelang.DOKUMEN_LELANG);
			Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
			Tahap tahapSekrang = null;
			if(tahapNow.isPemasukanPenawaran())
				tahapSekrang = Tahap.PEMASUKAN_PENAWARAN;
			if(tahapNow.isPemasukanPenawaranAdmTeknis())
				tahapSekrang = Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS;
			if(tahapNow.isPemasukanPenawaranHarga())
				tahapSekrang = Tahap.PEMASUKAN_PENAWARAN_BIAYA;
			Jadwal jadwal = Jadwal.findByLelangNTahap(lelang_token.lls_id, tahapSekrang);
			if(jadwal != null) {
				renderArgs.put("tglPenutupan", jadwal.dtj_tglakhir);
			}
			DaftarKuantitas daftarKuantitas = null;
			if (dok_lelang_content != null) {
				daftarKuantitas = dok_lelang.getRincianHPS();
				Integer berlaku = dok_lelang.getMasaBerlakuPenawaran();
				renderArgs.put("noSdp", dok_lelang_content.dll_nomorSDP);
				renderArgs.put("tanggalSdp", FormatUtils.formatDateInd(dok_lelang_content.dll_tglSDP));
				renderArgs.put("berlaku",berlaku);
				renderArgs.put("rincianFixed", daftarKuantitas.fixed);
			}
			if (lelang.isSatuFile()) {
				renderArgs.put("checklistAdm", dok_lelang.getSyaratAdministrasi());
				renderArgs.put("checklistTeknis",dok_lelang.getSyaratTeknis());
				renderArgs.put("checklistHarga",dok_lelang.getSyaratHarga());
				renderArgs.put("rincianList",daftarKuantitas.items);
				renderArgs.put("keyAll", Lelang_key.getPublicKey(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA, lelang, paket));
				versi = History_dok_penawaran.count("psr_id=? AND dok_jenis = 2", peserta.psr_id);
			} else if ((tahapNow.isPemasukanPenawaranAdmTeknis() || tahapNow.isPemasukanPenawaran()) && lelang_token.llt_jenis.isAdmTeknis()) {
				renderArgs.put("checklistAdm", dok_lelang.getSyaratAdministrasi());
				renderArgs.put("checklistTeknis",dok_lelang.getSyaratTeknis());
				renderArgs.put("checklistHarga",dok_lelang.getSyaratHarga());
				renderArgs.put("keyAdmTeknis", Lelang_key.getPublicKey(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI, lelang, paket));
				versi = History_dok_penawaran.count("psr_id=? AND dok_jenis  = 1", peserta.psr_id);
			}
			else if((tahapNow.isPemasukanPenawaranHarga() || tahapNow.isPemasukanPenawaran()) && lelang_token.llt_jenis.isHarga()) {
				renderArgs.put("rincianList",daftarKuantitas.items);
				renderArgs.put("checklistHarga",dok_lelang.getSyaratHarga());
				renderArgs.put("keyHarga", Lelang_key.getPublicKey(JenisDokPenawaran.PENAWARAN_HARGA, lelang, paket));
				versi = History_dok_penawaran.count("psr_id=? AND dok_jenis = 2", peserta.psr_id);
			}
			renderArgs.put("uploadUrl",BasicCtr.getRequestHost() + Router.reverse("lelang.Lt17Ctr.upload").url);
			String nomor_surat_penawaran = peserta.psr_id+"/"+(versi + 1)+"/"+dateTime.getYear()+"/"+dateTime.getMonthOfYear();
			renderArgs.put("no_surat_penawaran", nomor_surat_penawaran);
			renderTemplate("lelang/lt17/peserta.html");
		} else {
			Logger.info("Bukan tahap pemasukan penawaran");
			invalid_jadwal(Messages.get("flash.matdmpp"));
		}
	}

	public static void upload(File document, Integer berlaku, String surat, String hash) {
		LogUtil.debug("surat", surat);
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);
		Lelang_seleksi lelang = renderArgs.get("lelang", Lelang_seleksi.class);
		boolean allowed = false; // default false
		if (lelang_token != null && lelang_token.psr_id != null) {
			TahapNow tahapNow = lelang.getTahapNow();
			allowed = tahapNow.isPemasukanPenawaran() || tahapNow.isPemasukanPenawaranAdmTeknis() || tahapNow.isPemasukanPenawaranHarga();			
		}
		// perlu evaluasi, apakah menyimpan surat penawaran dengan cara ini
		// adalah yang terbaik
		Map<String, Object> result = new HashMap<>(1);
		if (lelang_token != null && lelang_token.psr_id != null && document != null && allowed) {
			Logger.debug("Uploading file penawaran token %s, namafile %s", lelang_token.llt_key, document.getName());
			result.put("name", document.getName());
			JenisDokPenawaran jenis = JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;
			result.put("type", "administrasi-dan-teknis");
			if (document.getName().contains("{harga}")) {
				result.put("type", "harga");
				jenis = JenisDokPenawaran.PENAWARAN_HARGA;
			}
			try {
				FileInputStream fis = new FileInputStream(document);
				String savedHash =DigestUtils.md5Hex(fis);
				fis.close();
				// get the saved doc and compare the hash
				if (savedHash.equals(hash)) {
					Dok_penawaran.simpanPenawaran(lelang_token.psr_id, jenis, document, berlaku, surat);
					result.put("status", "ok");
					Logger.debug("Sama, server vs. local val: %s vs. %s", savedHash, hash);
				} else {
					result.put("status", "error");
					result.put("code", "400");
					result.put("message", Messages.get("flash.dybdmhb"));
					Logger.error("Berbeda, server vs. local val: %s vs. %s", savedHash, hash);
					badRequest();
				}
			} catch (Exception e) {
				e.printStackTrace();
				Logger.error("Kesalahan simpan dok_penawaran %s", e);
				result.put("status", "error");
				result.put("code", "500");
				result.put("message", Messages.get("flash.dtdd"));
				error();
			}
		} else { // forbidden
			result.put("status", "error");
			result.put("code", "401");
			result.put("message", Messages.get("flash.mad"));
			forbidden();
		}
		// can we send other than 200?
		renderJSON(result);
	}
	
	public static void uploadResume(String uuid) {
		Logger.debug("Uploading file penawaran uuid %s", uuid);

		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);		

		Peserta peserta = Peserta.findById(lelang_token.psr_id);
		Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
		String directory = FileUtils.getTempDirectory().getAbsolutePath();// Configuration.getConfigurationValue(UuidStore.UPLOAD_DIR_CFG, "/tmp");
    	String path = "lt17.uploadResume";
    	TusHandler tusHandler = new TusHandler(directory, path, request, response);
		Map<String, Object> result = new HashMap<>(1);
    	try {
    		tusHandler.process(rekanan.rkn_namauser, peserta.rkn_id);
        	if(tusHandler.finishAll) {
        		result.put("uuid", uuid);
        		renderJSON(result);
        	}
    	} catch (Exception e) {
			result.put("error", e.getMessage());
    		renderJSON(result);
    	}
	}

	public static void uploadCommit(String uuid, Integer berlaku, String surat, String hash) {

		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);		

		// perlu evaluasi, apakah menyimpan surat penawaran dengan cara ini
		// adalah yang terbaik
		Map<String, Object> result = new HashMap<>(1);
		UuidStore uuidStore = UuidStore.findById(uuid);
		if(uuid == null) {
			result.put("status", "error");
			result.put("code", "401");
			result.put("message", "UUID not found");
			renderJSON(result);
			return;
		}
		if(lelang_token == null || lelang_token.psr_id == null) {
			 // forbidden
			result.put("status", "error");
			result.put("code", "401");
			result.put("message", Messages.get("flash.mad"));
			renderJSON(result);
			return;
		}
		
		Peserta peserta = Peserta.findById(lelang_token.psr_id);
		if(peserta == null || !peserta.rkn_id.equals(uuidStore.rkn_id)) {
			 // forbidden
			result.put("status", "error");
			result.put("code", "401");
			result.put("message", Messages.get("flash.mad"));
			renderJSON(result);
			return;
		}
		// save file
		String directory = FileUtils.getTempDirectory().getAbsolutePath();// Configuration.getConfigurationValue(UuidStore.UPLOAD_DIR_CFG, "/tmp");
		String path = "lt17.uploadResume";
    	String filepath = directory + (directory.endsWith(File.separator) ? "" : File.separator) 
    			+ uuidStore.uuid.replaceAll(path, "") + File.separator + uuidStore.filename; 
		File document = new File(filepath);
		
		Logger.debug("Uploading file penawaran token %s, namafile %s", lelang_token.llt_key, document.getName());
		result.put("name", document.getName());
		JenisDokPenawaran jenis = JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;
		result.put("type", "administrasi-dan-teknis");
		if (document.getName().contains("{harga}")) {
			result.put("type", "harga");
			jenis = JenisDokPenawaran.PENAWARAN_HARGA;
		}
		try {
			FileInputStream fis = new FileInputStream(document);
			String savedHash =DigestUtils.md5Hex(fis);
			fis.close();
			// get the saved doc and compare the hash
			if (savedHash.equals(hash)) {
				Dok_penawaran.simpanPenawaran(lelang_token.psr_id, jenis, document, berlaku, surat);
				result.put("status", "ok");
				Logger.debug("Sama, server vs. local val: %s vs. %s", savedHash, hash);
			} else {
				result.put("status", "error");
				result.put("code", "400");
				result.put("message", Messages.get("flash.dybdmpp"));
				Logger.error("Berbeda, server vs. local val: %s vs. %s", savedHash, hash);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logger.error("Kesalahan simpan dok_penawaran %s", e);
			result.put("status", "error");
			result.put("code", "500");
			result.put("message", Messages.get("flash.dtdd"));
		}
		document.delete();
		document.getParentFile().delete();
		uuidStore.delete();
		// can we send other than 200?
		renderJSON(result);
	}

	public static void download(Long id) {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);		

		if (lelang_token != null && lelang_token.peg_id != null) {
			BlobTable blob = BlobTableDao.getLastById(id);
			Logger.debug("downloading file penawaran token %s, namafile %s", lelang_token.llt_key, blob.getFileName());
			renderBinary(blob.getFile());
		} else {
			not_authorized();
		}
	}

	public static void downloadResume(Long id) {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);		

		Long offset = 0L;
		if(request.headers.get("offset") != null) {
			offset = Helpers.getLong(request.headers.get("offset").value(), 0L);
		}
		if (lelang_token != null && lelang_token.peg_id != null) {
			BlobTable blob = BlobTableDao.getLastById(id);
			Logger.debug("downloading file penawaran token %s, namafile %s", lelang_token.llt_key, blob.getFileName());
			try {
				FileInputStream fis = new FileInputStream(blob.getFile());
				fis.getChannel().position(offset);
				renderBinary(fis);
			} catch (Exception e) {
				e.printStackTrace();
				renderText(e.getMessage());
			}
		} else {
			not_authorized();
		}
	}

	public static void sort(String payload) {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);
		Lelang_seleksi lelang = renderArgs.get("lelang", Lelang_seleksi.class);
		boolean auditor = renderArgs.get("auditor", Boolean.class);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		if (lelang_token != null && lelang_token.peg_id != null) {
			JsonObject penawaranHarga1 = JsonUtil.formatJson(payload);
			PenawaranHarga penawaranHarga = JsonUtil.fromJson(penawaranHarga1.toString(),PenawaranHarga.class);
			penawaranHarga.recalculateVolume();
			List<Datum> datumList = penawaranHarga.data;
			Datum[] datums = datumList.toArray(new Datum[0]);
			String downloadUrl = BasicCtr.getRequestHost() + Router.reverse("lelang.Lt17Ctr.download").url;
			// should be put in separate method?
			if (tahapStarted.isPembukaanTeknis() && !tahapStarted.isPembukaanHarga()) {
				renderJSON(Datum.renderPembukaanTeknis(datums, lelang, downloadUrl, auditor));
			}
			Arrays.sort(datums, Datum.totalComparator);
			renderJSON(Datum.render(datums, lelang, lelang_token, downloadUrl, auditor));
		} else {
			not_authorized();
		}
	}
	
	public static void uploadStruk(File document, String hash) {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);
		Lelang_seleksi lelang = renderArgs.get("lelang", Lelang_seleksi.class);
		boolean allowed = false; // default false
		if (lelang_token != null && lelang_token.psr_id != null) {
			TahapNow tahapNow = lelang.getTahapNow();
			allowed = tahapNow.isPemasukanPenawaran() || tahapNow.isPemasukanPenawaranAdmTeknis() || tahapNow.isPemasukanPenawaranHarga();			
		}

		Map<String, Object> result = new HashMap<>(1);
		if (lelang_token != null && lelang_token.psr_id != null && document != null && allowed) {
			Logger.debug("Uploading struk token %s, namafile %s", lelang_token.llt_key, document.getName());
			result.put("name", document.getName());
			JenisDokPenawaran jenis = JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;
			result.put("type", "administrasi-dan-teknis");
			if (document.getName().contains("{harga}")) {
				result.put("type", "harga");
				jenis = JenisDokPenawaran.PENAWARAN_HARGA;
			}
			try {
				FileInputStream fis = new FileInputStream(document);
				String savedHash =DigestUtils.md5Hex(fis);
				fis.close();
				// get the saved doc and compare the hash
				if (savedHash.equals(hash)) {
					Dok_penawaran.simpanStruk(lelang_token.psr_id, jenis, document);
					result.put("status", "ok");
					Logger.debug("Sama, server vs. local val: %s vs. %s", savedHash, hash);
				} else {
					result.put("status", "error");
					result.put("code", "400");
					result.put("message", Messages.get("flash.sybdmhyb"));
//					response.status = 400;
					Logger.error("Berbeda, server vs. local val: %s vs. %s", savedHash, hash);
					badRequest();
				}
			} catch (Exception e) {
				e.printStackTrace();
				Logger.error("Kesalahan simpan struk %s", e);
				result.put("status", "error");
				result.put("code", "500");
				result.put("message", Messages.get("flash.stdd"));
//				response.status = 500;
				error();
			}
		} else { // forbidden
			result.put("status", "error");
			result.put("code", "401");
			result.put("message", Messages.get("flash.mad"));
//			response.status = 401;
			forbidden();
		}
		// can we send other than 200?
		renderJSON(result);
	}

	public static void invalid_jadwal(String message) {
		response.status = 403;
		renderArgs.put("message", message);
		renderTemplate("lelang/lt17/invalid_jadwal.html");
	}

	public static void invalid_jadwal_json(String message) {
		response.status = 403;
		JsonObject object = new JsonObject();
		object.addProperty("message", message);
		renderJSON(object);
	}

	public static void not_authorized() {
		response.status = 403;
		renderTemplate("lelang/lt17/forbidden.html");
	}

	public static void not_authorized_json() {
		response.status = 403;
		JsonObject object = new JsonObject();
		object.addProperty("message", "Anda tidak diizinkan untuk masuk.");
		renderJSON(object);
	}

	public static void invalid_apendo(String versiApendo, boolean isOSD) {
		response.status = 403;
		renderArgs.put("versiApendo", versiApendo);
		renderArgs.put("isOSD", isOSD);
		renderTemplate("lelang/lt17/invalid_apendo.html");
	}

	public static void invalid_apendo_json(String versiApendo, boolean isOSD) {
		response.status = 403;
		JsonObject object = new JsonObject();
		object.addProperty("versiApendo", versiApendo);
		object.addProperty("isOSD", isOSD);
		renderJSON(object);
	}

	/**
	 * send email dari apendo
	 * param content (content email dari apendo - struk)
	 * @param encContent
	 * @param encKey
	 */
	public static void send_email(String encContent, String encKey) {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);		

		if (lelang_token != null && lelang_token.psr_id != null) {
			String plainContent;
			byte[] key = Encrypt.apendoRSADecrypt(encKey);
			plainContent = new String(Encrypt.decryptApendoContent(Encrypt.decodeApendo(encContent), key));
			plainContent = plainContent.replace("\n", "<br />");
			Map<String, Object> result = new HashMap<>();
			Rekanan rekanan = Rekanan.findByPeserta(lelang_token.psr_id);
			try {
				MailQueue mq= EmailManager.createEmail(rekanan.rkn_email, plainContent, "Notifikasi Pengiriman Penawaran");
				mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
				mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
				mq.lls_id=lelang_token.lls_id;
				mq.jenis = JenisEmail.PENGIRIMAN_PENAWARAN.id;
				mq.rkn_id = rekanan.rkn_id;
				await(new SendMailJobNow(mq).now());
				result.put("status", "ok");
				result.put("message", Messages.get("flash.npbdke"));
				Logger.debug(Messages.get("flash.npbdke"));
			} catch (Exception e) {
				result.put("status", "error");
				result.put("code", "400");
				result.put("message", Messages.get("flash.npgdke"));
				Logger.debug(Messages.get("flash.npgdke"));
			}
			renderJSON(result);
		}else {
			not_authorized();
		}
	}

	/**
	 * informasi dokumen persyaratan teknis yang telah dikirim peserta lelang
	 * @param info (content informasi dokumen persyaratan teknis, content seperti struks yang sudah ada)
	 * @param  id (id peserta)
     */
	public static void submitInfoTeknis(Long id, String info) {
		Lelang_token lelang_token = renderArgs.get("lelang_token", Lelang_token.class);
		if (lelang_token != null && lelang_token.peg_id != null) { // hanya pokja yang di ijinkan
			Peserta peserta = Peserta.findBy(id);
			peserta.psr_uraian = info;
			peserta.save();
		}
	}

}
