package controllers.lelang;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import models.agency.Anggaran;
import models.agency.Paket_lokasi;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.TahapNow;
import models.jcommon.util.CommonUtil;
import models.lelang.Lelang_detil;
import models.lelang.Peserta;
import models.rekanan.Rekanan;
import models.secman.Group;
import play.Logger;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;
import utils.BlacklistCheckerUtil;
import models.sso.common.adp.util.DceSecurityV2;
import utils.LogUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * aktivitas pendaftaran lelang
 */

public class PendaftaranLelangCtr extends BasicCtr {

	public static final String TAG = "PendaftaranLelangCtr";

	public static final String URL_SIKAP_ISQUALIFIED = BasicCtr.SIKAP_URL+"/services/isRekananQualified?q=";
	public static final String URL_SIKAP_DAFTAR_LELANG = BasicCtr.SIKAP_URL+"/services/rekanandaftarlelang?q=";

	/*
	* @param id
 	* @param setuju
 	* @throws IOException
	 */
	@AllowAccess({Group.REKANAN})
	public static void index(Long id) {
		Long rekananId = Active_user.current().rekananId;
		Lelang_detil lelang = Lelang_detil.findById(id);
		renderArgs.put("lelang", lelang);
		Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", lelang.lls_id, rekananId).first();
		boolean allowed = TahapNow.isAllowPendaftaran(lelang.lls_id, lelang.getMetode().kualifikasi.isPra(), lelang.getPemilihan().isLelangExpress()) && (peserta == null) && !lelang.isLelangV3(); // 30 maret 2017, tidak boleh daftar lelang v3
		renderArgs.put("anggaranList", Anggaran.findByPaket(lelang.pkt_id));
		renderArgs.put("lokasiList", Paket_lokasi.find("pkt_id=?", lelang.pkt_id).fetch());
		if(lelang.getPemilihan().isLelangExpress())
		{
			Rekanan rekanan = Rekanan.findById(rekananId);
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("lls_id", id);
			jsonObject.addProperty("rkn_id", rekananId);
			jsonObject.addProperty("rkn_namauser", rekanan.rkn_namauser);
			allowed = false;
			String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
			try {
				WSRequest request = WS.url(URL_SIKAP_ISQUALIFIED+param);
				String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
				if(!CommonUtil.isEmpty(content)) {
					allowed = Boolean.parseBoolean(DceSecurityV2.decrypt(content));
				}
			}catch (Exception ex){
				Logger.error(ex,"Tidak bisa konek ke SIKaP");
			}
		}
		renderArgs.put("inaproc_url",BasicCtr.INAPROC_URL + "/daftar-hitam");
		renderArgs.put("allowed", allowed);
		renderArgs.put("isOsd", ConfigurationDao.isOSD(BasicCtr.newDate()));
		renderTemplate("lelang/pendaftaran-peserta.html");
	}

	public static void submit(Long id, boolean setuju) {
		checkAuthenticity();
		Lelang_detil lelang = Lelang_detil.findById(id);
		if(lelang.isLelangV3()) { // tidak boleh daftar lelang versi 3
			BerandaCtr.index();
		}
		Long rekananId = Active_user.current().rekananId;
		Peserta pesertaCheck = Peserta.findByRekananAndLelang(rekananId, id); // check pserta terdaftar agar tidak doule
		if (setuju && pesertaCheck == null) {			
			Peserta.daftarLelang(id, rekananId);
			if(lelang.getPemilihan().isLelangExpress())
			{
				Rekanan rekanan = Rekanan.findById(rekananId);
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("lls_id", id);
				jsonObject.addProperty("rkn_id", rekananId);
				jsonObject.addProperty("rkn_namauser", rekanan.rkn_namauser);
				String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
				try {
					WSRequest request = WS.url(URL_SIKAP_DAFTAR_LELANG+param);
					String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
					if (!CommonUtil.isEmpty(content)) {
						Logger.info("Berhasil simpan history kualifikasi ke SIKaP : " + content);
					}
				} catch (Exception ex) {
					Logger.error(ex, "Tidak bisa konek ke SIKaP");
				}
			}
		}
		BerandaCtr.index();
	}

	public static void checkRequirements(Long llsId) {
//		if (ConfigurationDao.isOSD(BasicCtr.newDate())) {
//			ServiceResult serviceResult = Active_user.current().isCertificateValid();
//			if (!serviceResult.isSuccess()) {
//				LogUtil.debug(TAG, "check certificate");
//				renderJSON(serviceResult.toJson());
//			}
//		}
		JsonObject jsonObject = new JsonObject();
		Active_user active_user = Active_user.current();
		Rekanan rekanan = Rekanan.findById(active_user.rekananId);
		Integer check = BlacklistCheckerUtil.checkBlacklistStatus(rekanan, null, llsId, 0);
		if (check == 1) {
			LogUtil.debug(TAG, "check blacklist");
			jsonObject.addProperty("status", false);
			jsonObject.addProperty("message", "<h5><strong><i class=\"fa fa-exclamation-triangle\" aria-hidden=\"true\"></i>\n" +
					"         Tidak diperkenankan daftar lelang, karena Anda ada di dalam daftar hitam Inaproc.</strong></h5>\n" +
					"            Silakan akses situs (<a href=\"" + BasicCtr.INAPROC_URL + "/daftar-hitam\" target=\"_blank\">INAPROC</a>) untuk informasi lebih detil");
			renderJSON(jsonObject.toString());
		}
		jsonObject.addProperty("status", true);
		jsonObject.addProperty("message", "success");
		renderJSON(jsonObject.toString());
	}

}
