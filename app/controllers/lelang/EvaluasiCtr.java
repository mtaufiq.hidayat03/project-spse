package controllers.lelang;

import ams.models.ServiceResult;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.admin.UtilityCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.DecimalBinder;
import ext.RupiahArrayBinder;
import models.agency.*;
import models.common.*;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Nilai_evaluasi.StatusNilaiEvaluasi;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.libs.URLs;
import play.mvc.Router;
import utils.BlacklistCheckerUtil;
import models.sso.common.adp.util.DceSecurityV2;
import utils.LogUtil;
import utils.osd.OSDUtil;

import java.util.*;

/**
 * Created by IntelliJ IDEA. Date: 12/09/12 Time: 16:27
 *
 * @author I Wayan Wiprayoga Wisesa
 */
public class EvaluasiCtr extends BasicCtr {

	public static final String TAG = "EvaluasiCtr";

	@AllowAccess({Group.PANITIA,Group.AUDITOR,Group.PPK})
	public static void index(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Paket paket = Paket.findByLelang(id);
		boolean isPanitia = Active_user.current().isPanitia();
		renderArgs.put("isPanitia", isPanitia);
        boolean express = lelang.getPemilihan().isLelangExpress();		
        TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("paket", paket);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("express", express);
		Active_user active_user = Active_user.current();	
		boolean isKetuaPanitia = Anggota_panitia.isKetuaPanitia(lelang.getPanitia().pnt_id, active_user.pegawaiId);
		renderArgs.put("isKetuaPanitia", isKetuaPanitia);
		TahapNow tahapAktif = lelang.getTahapNow();
		Evaluasi pembuktian = Evaluasi.findPembuktian(id);
		renderArgs.put("allow_batal_persetujuan",lelang.allowBatalPersetujuanPemenang());
		if(express) {
			renderArgs.put("list", HasilEvaluasiExpress.findByLelang(id));
			renderArgs.put("showDetilHarga", !Paket.isFlag43(lelang.lls_id));
//			Peserta peserta = Peserta.findPesertaPemenangVerifByIdLelang(lelang.lls_id);
			Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(id);
			renderArgs.put("evaluasi", evaluasi);
			if(evaluasi != null) {
				boolean sudah_ada_penetapan = Nilai_evaluasi.countLulus(evaluasi.eva_id) > 0;
				renderArgs.put("sudah_ada_penetapan", sudah_ada_penetapan);
				renderArgs.put("jmlPeserta", evaluasi.countPesertaList());
				renderArgs.put("allow_penetapan", lelang.sudahVerifikasiSikap() && active_user.isPanitia() && evaluasi.eva_status.isSedangEvaluasi());
				Persetujuan persetujuan_pegawai  = Persetujuan.findByPegawaiPenetapanAkhir(active_user.pegawaiId, id);
				renderArgs.put("allow_persetujuan", persetujuan_pegawai != null && (persetujuan_pegawai.pst_status.isBelumSetuju()
						|| persetujuan_pegawai.pst_status.isTidakSetuju()) && sudah_ada_penetapan);
			}
			renderArgs.put("jenis",JenisEmail.UNDANGAN_VERIFIKASI.id);
			renderTemplate("lelang/evaluasi/evaluasi-express.html");
		} else  {
			renderArgs.put("tahapNow", tahapAktif);
			if(lelang.isPrakualifikasi()) {
				if(pembuktian != null) {
					boolean allow_penetapan_pra = tahapAktif.isPenetapanHasilPra() && pembuktian.eva_status.isSedangEvaluasi() && active_user.isPanitia();
					renderArgs.put("allow_penetapan_pra", allow_penetapan_pra);
					if(pembuktian.eva_status.isSelesai() && !allow_penetapan_pra)
						renderArgs.put("status_penetapan_pra", "Penetapan Pemenang Prakualifikasi sudah dilakukan.");
				}
			}
			if(lelang.isDuaFile() || lelang.isDuaTahap()) {
				Evaluasi teknis = Evaluasi.findTeknis(id);
				if (teknis != null) {
					Evaluasi administrasi = Evaluasi.findAdministrasi(id);
					boolean allow_penetapan_teknis = tahapAktif.isAllowPenetapanPemenangTeknis() && teknis.eva_status.isSedangEvaluasi() && active_user.isPanitia()
							&& teknis.isSudahEvaluasi() && (administrasi != null && administrasi.isSudahEvaluasi());
					renderArgs.put("allow_penetapan_teknis", allow_penetapan_teknis);
					if(allow_penetapan_teknis)
						renderArgs.put("labelTetapkanTeknis", tahapAktif.isPengumumanPemenangTeknis() ? "Pengumuman Pemenang Teknis":"Penetapan Pemenang Teknis");
					if (teknis.eva_status.isSelesai() && !allow_penetapan_teknis)
						renderArgs.put("status_penetapan_teknis", "Penetapan Pemenang Teknis sudah dilakukan.");
				}
			}
	        Persetujuan persetujuan_pegawai  = Persetujuan.findByPegawaiPenetapanAkhir(active_user.pegawaiId, id);
			boolean editable = tahapAktif.isEvaluasi() && !tahapAktif.isSedangPersetujuanPemenang();
			Evaluasi evaluasi = lelang.getEvaluasiAkhir();
			if(evaluasi != null) {
				boolean sudah_ada_penetapan = Nilai_evaluasi.countLulus(evaluasi.eva_id) > 0;
				boolean allow_persetujuan = persetujuan_pegawai != null && (persetujuan_pegawai.pst_status.isBelumSetuju() || persetujuan_pegawai.pst_status.isTidakSetuju())
						&& sudah_ada_penetapan && tahapAktif.isPenetapaPemenang();
				if(evaluasi != null) {
		        	editable = evaluasi.eva_status.isSedangEvaluasi();
		        }
				// check status pembuktian
				boolean sudahdibuktikan  = false;
				if(pembuktian != null) {
					sudahdibuktikan = Nilai_evaluasi.count("nev_lulus in (0,1,2) AND eva_id =?", pembuktian.eva_id) > 0;
				}
				renderArgs.put("sudah_ada_penetapan", sudah_ada_penetapan);
				renderArgs.put("allow_persetujuan", allow_persetujuan);
				renderArgs.put("allow_penetapan", tahapAktif.isAllowPenetapanPemenangAkhir() && active_user.isPanitia() && sudahdibuktikan);

				// untuk konsultansi bisa input harga negosiasi di tahap klarifikasi dan negosiasi teknis dan biaya
				if(lelang.isAllowNego()){
					List<Nilai_evaluasi> pemenangList = evaluasi.findPesertaList(true);
					boolean allow_input_negosiasi = !CommonUtil.isEmpty(pemenangList) && active_user.isPanitia();
					renderArgs.put("allow_input_negosiasi", allow_input_negosiasi);
				}
			}
			renderArgs.put("editable", editable);
			renderTemplate("lelang/evaluasi/evaluasi.html");
		}
	}

    /**
     * detail evaluasi masing - masing peserta
     * @param id (Kode Peserta)
     */
    @AllowAccess({Group.PANITIA,Group.AUDITOR,Group.PPK})
    public static void detail(Long id) {    	
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
        Lelang_seleksi lelang = peserta.getLelang_seleksi();
		MetodeEvaluasi metodeEvaluasi = lelang.getEvaluasi();
		renderArgs.put("id", id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("pascaDuaFileKualitas", lelang.getMetode() == Metode.PASCA_DUA_FILE_KUALITAS);
        Active_user active_user = Active_user.current();
		if(lelang.getPemilihan().isLelangExpress()) {
			forbidden(Messages.get("evaluasi.express.tender.message"));
		} else {
        	TahapStarted tahapStarted = lelang.getTahapStarted();
        	TahapNow tahapNow = lelang.getTahapNow();
			boolean persetujuan = lelang.isSedangPersetujuanPemenang();
			Dok_penawaran dok_penawaran_kualifikasi = peserta.getDokKualifikasi();
			Dok_penawaran dok_penawaran_harga = peserta.getFileHarga();
			Dok_penawaran dok_penawaran_admin_teknis = peserta.getFileTeknis();
			renderArgs.put("tahapNow", tahapNow);
			renderArgs.put("tahapStarted", tahapStarted);
			renderArgs.put("persetujuan", persetujuan);
			renderArgs.put("dokumen", dok_penawaran_admin_teknis);
			renderArgs.put("dokumen_harga", dok_penawaran_harga);
			renderArgs.put("dokumen_kualifikasi", dok_penawaran_kualifikasi);
			renderArgs.put("peserta", peserta);

			Dok_lelang dok_kualifikasi = Dok_lelang.findBy(lelang.lls_id, lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA : JenisDokLelang.DOKUMEN_LELANG);
			if(dok_kualifikasi != null && dok_penawaran_kualifikasi != null) {
				// evaluasi kualifikasi
				Evaluasi kualifikasi = Evaluasi.findNCreateKualifikasi(lelang.lls_id);
				renderArgs.put("kualifikasi", kualifikasi);
				Nilai_evaluasi nilai_kualifikasi = Nilai_evaluasi.findBy(kualifikasi.eva_id, peserta.psr_id);
				boolean allow_kualifikasi = (!persetujuan || lelang.isPrakualifikasi())  &&
						tahapNow.isEvaluasiKualifikasi() && kualifikasi.eva_status.isSedangEvaluasi() && active_user.isPanitia();
//				renderArgs.put("allow_skor_kualifikasi", allow_kualifikasi);
				boolean kirimPesan = allow_kualifikasi && !lelang.is_kualifikasi_tambahan && !tahapNow.isEvaluasiKualifikasiBerakhir() && !lelang.getKategori().isJkKonstruksi();
//				if(!peserta.is_dikirim_pesan){
////					kirimPesan = kirimPesan && !peserta.sudah_evaluasi_kualifikasi;
////				}
				renderArgs.put("kirimPesan", kirimPesan);

				if(!lelang.isEvaluasiUlang()){
					if(lelang.is_kualifikasi_tambahan){ //Evaluasi Kualifikasi Tambahan hanya bisa dilakukan pada penyedia yang tidak lengkap kualifikasinya
						allow_kualifikasi = allow_kualifikasi && peserta.is_dikirim_pesan;
					}else{
						allow_kualifikasi = allow_kualifikasi && !peserta.is_dikirim_pesan;
					}
				}

				renderArgs.put("allow_kualifikasi", allow_kualifikasi);
				boolean sudahUndangPembuktian = MailQueueDao.countByJenisAndRekanan(lelang.lls_id, JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id, peserta.rkn_id) > 0;
				renderArgs.put("sudahUndangPembuktian", sudahUndangPembuktian);
				if (lelang.isPrakualifikasi())
					nilai_kualifikasi = Nilai_evaluasi.findNCreateBy(kualifikasi.eva_id, peserta, Evaluasi.JenisEvaluasi.EVALUASI_KUALIFIKASI);
				renderArgs.put("nilai_kualifikasi", nilai_kualifikasi);
				Nilai_evaluasi nilai_pembuktian = null;
				Evaluasi pembuktian = lelang.getPembuktian(false);
				if(pembuktian != null)
					nilai_pembuktian = peserta.getNilaiPembuktian();
				if(nilai_kualifikasi != null) {										
					List<Checklist_evaluasi> syaratKualifikasi = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratIjinUsaha(), nilai_kualifikasi.nev_id);
					syaratKualifikasi.addAll(Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasi(), nilai_kualifikasi.nev_id));
					List<Checklist_evaluasi> syaratIjinUsahaBaru = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratIjinUsahaBaru(), nilai_kualifikasi.nev_id);
					List<Checklist_evaluasi> syaratKualifikasiAdmin = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasiAdministrasi(), nilai_kualifikasi.nev_id);
					List<Checklist_evaluasi> syaratKualifikasiTeknis = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasiTeknis(), nilai_kualifikasi.nev_id);
					List<Checklist_evaluasi> syaratKualifikasiKeuangan = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasiKeuangan(), nilai_kualifikasi.nev_id);
					renderArgs.put("syaratKualifikasi", syaratKualifikasi);
					renderArgs.put("syaratIjinUsahaBaru", syaratIjinUsahaBaru);
					renderArgs.put("syaratKualifikasiAdmin", syaratKualifikasiAdmin);
					renderArgs.put("syaratKualifikasiTeknis", syaratKualifikasiTeknis);
					renderArgs.put("syaratKualifikasiKeuangan", syaratKualifikasiKeuangan);					
				}
				if (!lelang.isLelangV3()) { // khusus spse 4, ada proses pembuktian kualifikasi
					renderArgs.put("pembuktian", pembuktian);
					if (pembuktian != null) {
						renderArgs.put("nilai_pembuktian", nilai_pembuktian);
						if (nilai_pembuktian != null) {
							boolean allow_pembuktian = (!persetujuan || lelang.isPrakualifikasi()) && tahapNow.isPembuktian() && pembuktian.eva_status.isSedangEvaluasi();
							//renderArgs.put("allow_kirim_undangan", allow_pembuktian);
							renderArgs.put("allow_pembuktian", allow_pembuktian && active_user.isPanitia());
							MailQueue mail = MailQueueDao.getByJenisAndLelangAndRekanan(lelang.lls_id
									, peserta.rkn_id , JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);

							renderArgs.put("mail", mail);
							List<Checklist_evaluasi> buktiKualifikasi = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratIjinUsaha(), nilai_pembuktian.nev_id);
							buktiKualifikasi.addAll(Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasi(), nilai_pembuktian.nev_id));
							List<Checklist_evaluasi> buktiIjinUsahaBaru = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratIjinUsahaBaru(), nilai_pembuktian.nev_id);
							List<Checklist_evaluasi> buktiKualifikasiAdmin = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasiAdministrasi(), nilai_pembuktian.nev_id);						
							List<Checklist_evaluasi> buktiKualifikasiTeknis = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasiTeknis(), nilai_pembuktian.nev_id);
							List<Checklist_evaluasi> buktiKualifikasiKeuangan = Checklist_evaluasi.addPersyaratan(dok_kualifikasi.getSyaratKualifikasiKeuangan(), nilai_pembuktian.nev_id);														
							renderArgs.put("buktiKualifikasi", buktiKualifikasi);
							renderArgs.put("buktiIjinUsahaBaru", buktiIjinUsahaBaru);
							renderArgs.put("buktiKualifikasiTeknis", buktiKualifikasiTeknis);
							renderArgs.put("buktiKualifikasiAdmin", buktiKualifikasiAdmin);
							renderArgs.put("buktiKualifikasiTeknis", buktiKualifikasiTeknis);
							renderArgs.put("buktiKualifikasiKeuangan", buktiKualifikasiKeuangan);
						}
					}
				}
				Date tgl_dokumen = dok_kualifikasi.getTanggalDokumen();
				Dok_penawaran penawaran_kualifikasi = peserta.getDokKualifikasi();
				if (penawaran_kualifikasi != null && tgl_dokumen != null) {
					renderArgs.put("notifikasi_kualifikasi", tgl_dokumen.after(penawaran_kualifikasi.dok_tgljam));
				}
				renderArgs.put("skorLabel", lelang.isJkKonstruksi() ? "Skor Kualifikasi Teknis":"Skor Kualifikasi");
			}
			Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
			if (dok_lelang != null && (dok_penawaran_admin_teknis != null || dok_penawaran_harga != null)) {
				boolean allow_admin_teknis = !persetujuan && tahapNow.isEvaluasiTeknis();
				Evaluasi admin = Evaluasi.findNCreateAdministrasi(lelang.lls_id);
				renderArgs.put("admin", admin);
				boolean isKirimPenawaranAdminTeknis = peserta.getFileTeknis() != null;
				if (admin != null && isKirimPenawaranAdminTeknis) {
					Nilai_evaluasi nilai_admin = Nilai_evaluasi.findNCreateBy(admin.eva_id, peserta,Evaluasi.JenisEvaluasi.EVALUASI_ADMINISTRASI);
					renderArgs.put("nilai_admin", nilai_admin);
					if (nilai_admin != null) {
						List<Checklist_evaluasi> syaratAdmin = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratAdministrasi(), nilai_admin.nev_id);
						renderArgs.put("syaratAdmin", syaratAdmin);
						MailQueue mailAdministrasi = MailQueueDao.getByJenisAndLelangAndRekanan(lelang.lls_id, peserta.rkn_id , JenisEmail.UNDANGAN_ADMINISTRASI);
						renderArgs.put("mailAdministrasi", mailAdministrasi);
						boolean sudahUndangAdministrasi = MailQueueDao.countByJenisAndRekanan(lelang.lls_id, JenisEmail.UNDANGAN_ADMINISTRASI.id, peserta.rkn_id) > 0;
						renderArgs.put("sudahUndangAdministrasi", sudahUndangAdministrasi);
					}
				}
				Evaluasi teknis = Evaluasi.findTeknis(lelang.lls_id);
				renderArgs.put("teknis", teknis);
				//note: metodeEvaluasi.isNilai() ==> KUALITAS_BIAYA || NILAI || UMUR_EKONOMIS
				renderArgs.put("showSkorTeknis", metodeEvaluasi.isNilai() || metodeEvaluasi.isPaguAnggaran() || metodeEvaluasi.isBiayaTerendah() || metodeEvaluasi.isKualitas());
				String skorRequired="";
				if( !(metodeEvaluasi.isGugur()||metodeEvaluasi.isUmurEkonomis()||metodeEvaluasi.isHargaTerendahAmbangBatas())){
					skorRequired="*";
				}
				renderArgs.put("skorRequired", skorRequired);

				if (teknis != null && isKirimPenawaranAdminTeknis) {
					allow_admin_teknis = allow_admin_teknis && teknis.eva_status.isSedangEvaluasi();
					Nilai_evaluasi nilai_teknis = Nilai_evaluasi.findBy(teknis.eva_id, peserta.psr_id);
					renderArgs.put("nilai_teknis", nilai_teknis);
					if (nilai_teknis != null) {
						List<Checklist_evaluasi> syaratTeknis = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratTeknis(), nilai_teknis.nev_id);
						renderArgs.put("syaratTeknis", syaratTeknis);
						MailQueue mailTeknis = MailQueueDao.getByJenisAndLelangAndRekanan(lelang.lls_id, peserta.rkn_id , JenisEmail.UNDANGAN_TEKNIS);
						renderArgs.put("mailTeknis", mailTeknis);
						boolean sudahUndangTeknis = MailQueueDao.countByJenisAndRekanan(lelang.lls_id, JenisEmail.UNDANGAN_TEKNIS.id, peserta.rkn_id) > 0;
						renderArgs.put("sudahUndangTeknis", sudahUndangTeknis);
					}
				}
				Evaluasi harga = Evaluasi.findHarga(lelang.lls_id);
				renderArgs.put("harga", harga);
				boolean allow_harga = !persetujuan && tahapNow.isEvaluasiHarga();
				if (harga != null) {
					Nilai_evaluasi nilai_harga = Nilai_evaluasi.findBy(harga.eva_id, peserta.psr_id);
					renderArgs.put("nilai_harga", nilai_harga);
					MailQueue mailHarga = MailQueueDao.getByJenisAndLelangAndRekanan(lelang.lls_id, peserta.rkn_id , JenisEmail.UNDANGAN_HARGA);
					renderArgs.put("mailHarga", mailHarga);
					boolean sudahUndangHarga = MailQueueDao.countByJenisAndRekanan(lelang.lls_id, JenisEmail.UNDANGAN_HARGA.id, peserta.rkn_id) > 0;
					renderArgs.put("sudahUndangHarga", sudahUndangHarga);
				}
				Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lelang.lls_id);
				if(penetapan != null) {
					allow_admin_teknis = allow_admin_teknis && penetapan.eva_status.isSedangEvaluasi();
					allow_harga = allow_harga && penetapan.eva_status.isSedangEvaluasi();
				}
				renderArgs.put("allow_admin_teknis", allow_admin_teknis && active_user.isPanitia());
				renderArgs.put("allow_harga", allow_harga && active_user.isPanitia());
				Date tgl_dokumen = dok_lelang.getTanggalDokumen();
				Dok_penawaran penawaran_teknis = peserta.getFileTeknis();
				if(penawaran_teknis != null && tgl_dokumen != null ) {
					renderArgs.put("notifikasi_admin_teknis", tgl_dokumen.after(penawaran_teknis.dok_tgljam));
				}
				Dok_penawaran penawaran_harga = peserta.getFileHarga();
				if(penawaran_harga != null && tgl_dokumen != null) {
					renderArgs.put("notifikasi_harga", tgl_dokumen.after(penawaran_harga.dok_tgljam));
				}
			}
			renderArgs.put("nama",peserta.getNamaPeserta());
            renderTemplate("lelang/evaluasi/evaluasi-detil.html");
        }
    }

    /**
     * simpan data evaluasi Teknis peserta
     * @param id (kode peserta)
     */
	@AllowAccess({Group.PANITIA})
    public static void checklist_admin(Long id, List<Long> checklist, String uraian) {
        checkAuthenticity();        
        boolean lulus = false;
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        Evaluasi evaluasi = Evaluasi.findAdministrasi(lelang.lls_id);
        Nilai_evaluasi nilai = Nilai_evaluasi.findBy(evaluasi.eva_id, peserta.psr_id);
        Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
        List<Checklist_evaluasi> list = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratAdministrasi(), nilai.nev_id);
        List<Long> lulus_list = new ArrayList<>();
        if(!CommonUtil.isEmpty(checklist)) {
	        for(Long check:checklist){
	        	if(check != null)
	        		lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
        }
        if((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("admin.alasan", Messages.get("flash.ahmapea"));
        } if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		}else {
	        nilai.nev_lulus = Checklist_evaluasi.simpanChecklist(list, checklist) ? StatusNilaiEvaluasi.LULUS:StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
	        evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode(), lelang);
	        flash.success(Messages.get("flash.deatt"));
        }
		redirect(Router.reverse("lelang.EvaluasiCtr.detail").add("id", id).addRef("administrasi").url);
    }
	
	/**
	 * simpan data evaluasi Teknis peserta
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA})
	public static void checklist_teknis(Long id, List<Long> checklist, @As(binder = DecimalBinder.class) Double skor, String uraian) {
		checkAuthenticity();
		boolean lulus = false;
		Peserta peserta = Peserta.findById(id);
		otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		Evaluasi evaluasi = Evaluasi.findTeknis(lelang.lls_id);
		Nilai_evaluasi nilai = Nilai_evaluasi.findBy(evaluasi.eva_id, peserta.psr_id);
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		List<Checklist_evaluasi> list = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratTeknis(), nilai.nev_id);		
		if (!CommonUtil.isEmpty(checklist)) {
			List<Long> lulus_list = new ArrayList<>();
			for (Long check : checklist) {
				if (check != null)
					lulus_list.add(check);
			}
			lulus = lulus_list.size() == list.size();
		}
		MetodeEvaluasi mtdEvaluasi= lelang.getEvaluasi();
		if(skor == null && !(mtdEvaluasi.isGugur()||mtdEvaluasi.isUmurEkonomis()||mtdEvaluasi.isHargaTerendahAmbangBatas())){
			Validation.addError("teknis.skor", Messages.get("flash.ahmstpet"));
		}
		if ((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("teknis.alasan", Messages.get("flash.ahmstpet"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = Checklist_evaluasi.simpanChecklist(list, checklist) ? StatusNilaiEvaluasi.LULUS : StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor =skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode(), lelang);
			String sudahAdaUndangan = "";
			if(lelang.getMetode() == Metode.PASCA_DUA_FILE_KUALITAS) {
				boolean sudahUndangPembuktian = MailQueueDao.countByJenisAndRekanan(lelang.lls_id, JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id, peserta.rkn_id) > 0;
				if(lulus && !sudahUndangPembuktian)
					sudahAdaUndangan = Messages.get("flash.skuppk");

			}
			flash.success(Messages.get("flash.dataetts")+sudahAdaUndangan);
		}
		redirect(Router.reverse("lelang.EvaluasiCtr.detail").add("id", id).addRef("teknis").url);
	}

	/**
	 * submit evaluasi Harga
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA})
	public static void checklist_harga(Long id, @As(binder= DecimalBinder.class) Double skor, Double hargaTerkoreksi, boolean lulus, String uraian) {
        checkAuthenticity();
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
		if(peserta == null || (peserta != null && peserta.getFileHarga() == null))
			forbidden("Anda tidak bisa melakukan evaluasi peserta ini.");
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        if (lelang.getEvaluasi().isNilai() && skor == null) {
			Validation.addError("harga.skor", Messages.get("flash.ahmshpeh"));
		}
		if ((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("harga.alasan",Messages.get("flash.ahmapehamk"));
        }

		if(hargaTerkoreksi == null || hargaTerkoreksi == 0.0){
			Validation.addError("hargaTerkoreksi", Messages.get("flash.httbk"));
		}

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		}
        else {
			Evaluasi evaluasi = Evaluasi.findHarga(peserta.lls_id);
			Nilai_evaluasi nilai = Nilai_evaluasi.findBy(evaluasi.eva_id, peserta.psr_id);
			nilai.nev_harga = peserta.psr_harga;
			nilai.nev_harga_terkoreksi = hargaTerkoreksi;
			nilai.nev_lulus = lulus ? StatusNilaiEvaluasi.LULUS:StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			if(hargaTerkoreksi != null && hargaTerkoreksi > 0.0) {
				peserta.psr_harga_terkoreksi = hargaTerkoreksi;
				peserta.save();
			}
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode(), lelang);
	        flash.success(Messages.get("flash.data_evaluasi_htt"));
		}
		redirect(Router.reverse("lelang.EvaluasiCtr.detail").add("id", id).addRef("harga").url);
	}

	/**
	 * detail evaluasi peserta
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA})
	public static void checklist_kualifikasi(Long id, List<Long> checklist, List<Long> checklistIjin, List<Long> checklistAdmin, List<Long> checklistTeknis, List<Long> checklistKeuangan, @As(binder= DecimalBinder.class) Double skor, String uraian) {
        checkAuthenticity();
        boolean lulus = false;
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		if (lelang.isPrakualifikasi() && (lelang.getKategori().isKonsultansi() || lelang.getKategori().isJkKonstruksi()) && skor == null) {
			Validation.addError("kualifikasi.skor","Anda harus mengisi skor kualifikasi pada Evaluasi Kualifikasi.");
		}
		Evaluasi evaluasi = Evaluasi.findNCreateKualifikasi(lelang.lls_id);
		Nilai_evaluasi nilai = Nilai_evaluasi.findNCreateBy(evaluasi.eva_id, peserta, evaluasi.eva_jenis);
		JenisDokLelang jenis = lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA: JenisDokLelang.DOKUMEN_LELANG;
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, jenis);
		
		List<Long> _checklist = new ArrayList<>();
		List<Checklist_evaluasi> list = new ArrayList<>();
				
		if(!dok_lelang.isSyaratKualifikasiBaru()) {
			if (checklist != null) _checklist.addAll(checklist);
			list = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratIjinUsaha(), nilai.nev_id);
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasi(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for(Long check:_checklist){
	        		if(check != null)
	        			lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
        } else {
			if (checklistIjin != null) _checklist.addAll(checklistIjin);
			if (checklistAdmin != null) _checklist.addAll(checklistAdmin);
			if (checklistTeknis != null) _checklist.addAll(checklistTeknis);
			if (checklistKeuangan != null) _checklist.addAll(checklistKeuangan);
			list = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratIjinUsahaBaru(), nilai.nev_id);
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasiAdministrasi(), nilai.nev_id));
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasiTeknis(), nilai.nev_id));
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasiKeuangan(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for (Long check:_checklist) {
	        		if(check != null)
	        			lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
        }
		
		if((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("kualifikasi.alasan",Messages.get("flash.ahmapekamk"));
		}
        if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			nilai.nev_lulus = Checklist_evaluasi.simpanChecklist(list, _checklist) ? StatusNilaiEvaluasi.LULUS : StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode(), lelang);
			peserta.sudah_evaluasi_kualifikasi = true;
			peserta.save();
			String sudahAdaUndangan = "";
			if (lulus && lelang.getMetode() != Metode.PASCA_DUA_FILE_KUALITAS) {
				boolean sudahUndangPembuktian = MailQueueDao.countByJenisAndRekanan(lelang.lls_id, JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id, peserta.rkn_id) > 0;
				if(!sudahUndangPembuktian)
					sudahAdaUndangan = Messages.get("flash.skuppk");
			}
			flash.success(Messages.get("flash.dektt") + sudahAdaUndangan);
		}
		redirect(Router.reverse("lelang.EvaluasiCtr.detail").add("id", id).addRef("kualifikasi").url);
	}
	
	/**
	 * detail evaluasi peserta
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA})
	public static void checklist_pembuktian(Long id, List<Long> checklist, List<Long> checklistIjin, List<Long> checklistAdmin, List<Long> checklistTeknis, List<Long> checklistKeuangan, @As(binder= DecimalBinder.class) Double skor, String uraian) {
        checkAuthenticity();
        boolean lulus = false;
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		Evaluasi evaluasi = Evaluasi.findNCreatePembuktian(lelang.lls_id);
		Nilai_evaluasi nilai = Nilai_evaluasi.findNCreateBy(evaluasi.eva_id, peserta, evaluasi.eva_jenis);
		JenisDokLelang jenis = lelang.isPrakualifikasi() ? JenisDokLelang.DOKUMEN_LELANG_PRA: JenisDokLelang.DOKUMEN_LELANG;
		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, jenis);
//		boolean isSyaratKualifikasiBaru = Checklist.isSyaratKualifikasiBaru(dok_lelang);
		
		List<Long> _checklist = new ArrayList<>();
		List<Checklist_evaluasi> list = new ArrayList<>();
		
		if(!dok_lelang.isSyaratKualifikasiBaru()) {
			if (checklist != null) _checklist.addAll(checklist);
			list = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratIjinUsaha(), nilai.nev_id);
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasi(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for(Long check:_checklist){
	        	if(check != null)
	        		lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
        } else {
			if (checklistIjin != null) _checklist.addAll(checklistIjin);
			if (checklistAdmin != null) _checklist.addAll(checklistAdmin);
			if (checklistTeknis != null) _checklist.addAll(checklistTeknis);
			if (checklistKeuangan != null) _checklist.addAll(checklistKeuangan);
			list = Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratIjinUsahaBaru(), nilai.nev_id);
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasiAdministrasi(), nilai.nev_id));
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasiTeknis(), nilai.nev_id));
			list.addAll(Checklist_evaluasi.addPersyaratan(dok_lelang.getSyaratKualifikasiKeuangan(), nilai.nev_id));
			List<Long> lulus_list = new ArrayList<>();
	        for(Long check:_checklist){
	        	if(check != null)
	        		lulus_list.add(check);
	        }
	        lulus = lulus_list.size() == list.size();
        }
        
        if((!lulus) && (StringUtils.isEmpty(uraian) || uraian.trim().length() < 10)) {
			Validation.addError("bukti.alasan",Messages.get("flash.ahmapbeamk"));
        }
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		}
        else {
			nilai.nev_lulus = Checklist_evaluasi.simpanChecklist(list, _checklist) ? StatusNilaiEvaluasi.LULUS : StatusNilaiEvaluasi.TDK_LULUS;
			nilai.nev_uraian = uraian;
			nilai.nev_skor = skor;
			evaluasi.simpanNilaiPeserta(peserta, nilai, lelang.getMetode(), lelang);

			if(!(lelang.lls_metode_penawaran==Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION && lelang.isKonstruksi())) {
				if (lelang.isPascakualifikasi() && !lelang.isItemized() && !lelang.isKonsultansi() && !lelang.isJkKonstruksi()) {
					Evaluasi penetapan = Evaluasi.findPenetapanPemenang(lelang.lls_id);
					boolean useAuction = (lelang.getEvaluasi().isGugur() || lelang.getEvaluasi().isHargaTerendahAmbangBatas())
							&& (penetapan != null && Nilai_evaluasi.count("eva_id=?", penetapan.eva_id) == 2);
					lelang.lls_metode_penawaran = useAuction ? Lelang_seleksi.MetodePenawaranHarga.AUCTION : Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION;
					lelang.save();
				} else {
					lelang.lls_metode_penawaran = Lelang_seleksi.MetodePenawaranHarga.NON_AUCTION;
					lelang.save();
				}
				flash.success(Messages.get("flash.dpkts"));
			}
			flash.success(Messages.get("flash.dpkts"));
        }
		redirect(Router.reverse("lelang.EvaluasiCtr.detail").add("id", id).addRef("pembuktian").url);
	}
	
	/**
	 * Penetapan Pemenang
	 */
	@AllowAccess({Group.PANITIA})
	public static void penetapan(Long id){
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("evaluasi", evaluasi);
		renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL+"/daftar-hitam");
		List<Nilai_evaluasi> nilaiEvaluasiList = new ArrayList<>();
		if(evaluasi != null) {
			nilaiEvaluasiList = evaluasi.findPesertaList(true);
			renderArgs.put("pesertaList", nilaiEvaluasiList);
		}
		TahapNow tahapNow = lelang.getTahapNow();
		TahapStarted tahapStarted = lelang.getTahapStarted();
		boolean editable = tahapNow.isPenetapaPemenang() && !Persetujuan.isApprove(lelang.lls_id, Persetujuan.JenisPersetujuan.PEMENANG_LELANG);
		renderArgs.put("editable", editable);
		if(lelang.isItemized()) {
			renderTemplate("lelang/evaluasi/evaluasi-pemenang-itemized.html");
		} else if(lelang.getPemilihan().isLelangExpress()) {
			Persetujuan persetujuanPemenang = Persetujuan.findByPegawaiPenetapanAkhir(Active_user.current().pegawaiId,lelang.lls_id);
			Boolean allowPenetapan = false;
			Long psr_id = 0L;
			String alasan_menang = null;
			if(persetujuanPemenang != null) {
				List<Peserta> pesertaList = Peserta.findByIdLelang(persetujuanPemenang.lls_id);
				for (Peserta peserta : pesertaList) {
					psr_id = peserta.is_pemenang_verif == 1 ? peserta.psr_id : psr_id;
					alasan_menang = peserta.is_pemenang_verif == 1 ? peserta.psr_alasan_menang  : alasan_menang;
				}
				if (!editable && persetujuanPemenang != null) {
					allowPenetapan = true;
				}
			}
			renderArgs.put("psr_id", psr_id);
			renderArgs.put("alasan_menang", alasan_menang);
			renderArgs.put("allowPenetapan", allowPenetapan);
			renderTemplate("lelang/evaluasi/evaluasi-pemenang-express.html");
		}
		else {
			renderArgs.put("editable", true);
			if(tahapStarted.isAuction()) {
				renderArgs.put("isAuction", !lelang.isExpress());
				ReverseAuction auction = ReverseAuction.findByLelang(lelang.lls_id);
				if(auction != null)
					renderArgs.put(	"editable", auction.isSelesai()); // jika auction berlangsung pokja tidak bisa menetapkan
			} else {
				renderArgs.put("showHargaNego", evaluasi.isShowHargaNego());
			}
			renderTemplate("lelang/evaluasi/evaluasi-pemenang.html");
		}
	}

	/**
	 * Penetapan pemenang Prakualifikasi
	 * @param id
     */
	@AllowAccess({Group.PANITIA})
	public static void penetapanPra(Long id){	
		otorisasiDataLelang(id); // check otorisasi data lelang
		Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapNow tahapNow = lelang.getTahapNow();
		Evaluasi evaluasi = Evaluasi.findPembuktian(lelang.lls_id);
		renderArgs.put("dok_lelang", dok_lelang);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapNow", tahapNow);
		renderArgs.put("evaluasi", evaluasi);
		if(evaluasi != null) {
			renderArgs.put("showSkor", evaluasi.isAnySkor());
			renderArgs.put("pesertaLulus", evaluasi.findPesertaLulusEvaluasiList());
			if (lelang.isPrakualifikasi()) {
				boolean allow_penetapan_pra = tahapNow.isPenetapanHasilPra() && evaluasi.eva_status.isSedangEvaluasi();
				renderArgs.put("allow_penetapan_pra", allow_penetapan_pra);
				if (evaluasi.eva_status.isSelesai() && !allow_penetapan_pra)
					renderArgs.put("status_penetapan_pra", "Penetapan Pemenang Prakualifikasi sudah dilakukan");
			}
		}
		renderTemplate("lelang/evaluasi/evaluasi-pemenang-prakualifikasi.html");
	}

	/**
	 * Penetapan pemenang Prakualifikasi
	 * @param id
	 */
	@AllowAccess({Group.PANITIA})
	public static void penetapanTeknis(Long id){
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		TahapNow tahapNow = lelang.getTahapNow();
		if(lelang.isDuaFile() || lelang.isDuaTahap()) {
			Evaluasi evaluasi = Evaluasi.findTeknis(id);
			renderArgs.put("evaluasi", evaluasi);
			if(evaluasi != null) {
				List<Nilai_evaluasi> pesertaList = lelang.getEvaluasi().isNilai() ? evaluasi.findPesertaListWithUrutSkor() : evaluasi.findPesertaList();
				renderArgs.put("peserta", pesertaList);
				boolean allow_penetapan_teknis = tahapNow.isPenetapanPemenangTeknis() && evaluasi.eva_status.isSedangEvaluasi();
				if(allow_penetapan_teknis)
					renderArgs.put("labelTetapkanTeknis", tahapNow.isPengumumanPemenangTeknis() ? "Pengumuman Pemenang Teknis":"Penetapan Pemenang Teknis");
				renderArgs.put("allow_penetapan_teknis", allow_penetapan_teknis);
				if(evaluasi.eva_status.isSelesai() && !allow_penetapan_teknis)
					renderArgs.put("status_penetapan", "Penetapan Pemenang Teknis sudah dilakukan");
			}
		}			
		renderTemplate("lelang/evaluasi/evaluasi-pemenang-teknis.html");
	}
	
	/**
	 * Penetapan pemenang Prakualifikasi
	 * @param id
     */
	@AllowAccess({Group.PANITIA})
	public static void submit_penetapanPra(Long id,  Long[] shortlist){
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Dok_lelang dok_lelang = Dok_lelang.findBy(id, JenisDokLelang.DOKUMEN_LELANG);
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		if(dok_lelang == null) {
			flash.error(Messages.get("flash.dtsbdatbmppp"));
		} else if (dok_lelang != null && dok_lelang.dll_id != null) {
			// memeriksa kelengkapan dokumen lelang
			StringBuilder complete = new StringBuilder();

			/*// memeriksa versi spse for backward safety reason, bisa jadi tidak perlu
			boolean syaratKualifikasiEmpty = lelang.isLelangV43() ? Checklist.isChecklistEmpty(dok_lelang.dll_id) :
					Dok_lelang_content.isLdpEmpty(dok_lelang.dll_id);
			if (syaratKualifikasiEmpty)
				complete.append("- Persyaratan Kualifikasi.<br/> ");*/

			if (Dok_lelang_content.isDkhEmpty(dok_lelang.dll_id))
				complete.append(Messages.get("flash.rhps"));
			if (Dok_lelang_content.isSpekEmpty(dok_lelang.dll_id))
				complete.append(Messages.get("flash.kak_stg"));

			if (complete.length() > 0)
				flash.error(Messages.get("flash.atbmppp_mldts") + complete);
			else {
				Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
				if(dok_lelang_content.getDokumen() == null || dok_lelang_content.dll_modified)
					flash.error(Messages.get("flash.atbmppp_mudtstd"));
			}
		}

		if(!flash.contains("error")) {
			if (lelang.isPrakualifikasi() && (lelang.getKategori().isKonsultansi() || lelang.getKategori().isJkKonstruksi())) {
				if (CommonUtil.isEmpty(shortlist)) { // shortlist empty
					flash.error(Messages.get("flash.dpdsmp"));
				} else { // shortlist sudah ada
					int jumlahShorlist = shortlist.length;
					if (ConfigurationDao.isEnableICB() && (jumlahShorlist < 3 || jumlahShorlist > 6) && !lelang.isLelangUlang()) {
						flash.error(Messages.get("flash.dpdsbsp"));
					} else if ((jumlahShorlist < 3 || jumlahShorlist > 7) && !lelang.isLelangUlang()) {
						flash.error(Messages.get("flash.dpdsbspp"));
					}
					Evaluasi evaluasi = Evaluasi.findKualifikasi(id);
					Evaluasi pembuktian = Evaluasi.findPembuktian(id);
					if (evaluasi == null) {
						flash.error(Messages.get("flash.abmek"));
					} else if (pembuktian == null) {
						flash.error(Messages.get("flash.abmpk"));
					}
					if (!flash.contains("error")) {
						for (Nilai_evaluasi nilai : pembuktian.findPesertaList()) {
							if (ArrayUtils.contains(shortlist, nilai.nev_id))
								nilai.nev_lulus = StatusNilaiEvaluasi.LULUS;
							else if (nilai.isLulus())
								nilai.nev_lulus = StatusNilaiEvaluasi.LULUS_TDK_SHORTLIST;
							else
								nilai.nev_lulus = StatusNilaiEvaluasi.TDK_LULUS;
							nilai.save();
						}
						evaluasi.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
						evaluasi.eva_tgl_setuju = newDate();
						evaluasi.save();
						pembuktian.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
						pembuktian.eva_tgl_setuju = new Date();
						pembuktian.save();
						flash.success(Messages.get("flash.pp_prabd"));
					}
				}
			} else {
				Evaluasi evaluasi = Evaluasi.findKualifikasi(id);
				Evaluasi pembuktian = Evaluasi.findPembuktian(id);
				if (evaluasi == null) {
					flash.error(Messages.get("flash.abmek"));
				} else if (pembuktian == null) {
					flash.error(Messages.get("flash.abmpk"));
				}
				if (pembuktian != null && pembuktian.countLulusEvaluasi() < 3 && !lelang.isLelangUlang()) {
					flash.error(Messages.get("flash.dpdpramin"));
				}
				if (!flash.contains("error")) {
					evaluasi.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
					evaluasi.eva_tgl_setuju = newDate();
					evaluasi.save();
					pembuktian.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
					pembuktian.eva_tgl_setuju = new Date();
					pembuktian.save();
					flash.success(Messages.get("flash.pp_prabd"));
				}
			}
		}
		index(id);
	}
	
	@AllowAccess({Group.PANITIA})
	public static void submit_penetapan_teknis(Long id) {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Evaluasi admin = Evaluasi.findAdministrasi(id);
		Evaluasi teknis = Evaluasi.findTeknis(id);
		if(admin == null) {
			flash.error(Messages.get("flash.abmea"));
		} else if (teknis == null) {
			flash.error(Messages.get("flash.abmet"));
		}
		else {
			admin.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
			admin.eva_tgl_setuju = newDate();
			admin.save();
			teknis.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
			teknis.eva_tgl_setuju = new Date();
			teknis.save();
			flash.success(Messages.get("flash.ppptbd"));
		}
		index(id);
	}

	/**
	 * evaluasi Ulang
	 */
	@AllowAccess({Group.PANITIA})
	public static void submit_penetapan(Long id, Long[] nilai, Double[] skor, Integer[] urutan){
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapNow tahapNow = lelang.getTahapNow();
		boolean editable = tahapNow.isAllowPenetapanPemenangAkhir();
		if(editable && (nilai != null)) {
			for(int i=0; i< nilai.length; i++) {
				if(nilai[i] == null)
					continue;

				Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(nilai[i]);
				Peserta peserta = Peserta.findBy(nilai_evaluasi.psr_id);
				peserta.psr_harga_terkoreksi = peserta.psr_harga_terkoreksi != null ? peserta.psr_harga_terkoreksi : 0;
			}

				//check urutan hanya boleh 1
				boolean arrayDuplicate = false;
				if(!lelang.isItemized()) {
					List<Integer> listUrutan = new ArrayList<>();
					for (Integer nevUrut : urutan) {
						arrayDuplicate = listUrutan.stream().anyMatch(t -> t.equals(nevUrut));
						if (arrayDuplicate) {
							flash.error(Messages.get("flash.gmp_nutv")+ nevUrut);
							break;
						}
						listUrutan.add(nevUrut);
					}
				}

				if(!arrayDuplicate) {
					Nilai_evaluasi nilai_evaluasi;
					boolean pemenang = false;
					for (int i = 0; i < nilai.length; i++) {
						nilai_evaluasi = Nilai_evaluasi.findById(nilai[i]);
						if (skor != null && skor[i] != null)
							nilai_evaluasi.nev_skor = skor[i];
						nilai_evaluasi.nev_urutan = urutan[i];
						if (nilai_evaluasi.nev_urutan == 1) { // peserta dengan urutan pertama aka diset sebagai pemenang
							nilai_evaluasi.nev_lulus = StatusNilaiEvaluasi.LULUS;
							pemenang = true;
						} else {
							nilai_evaluasi.nev_lulus = StatusNilaiEvaluasi.TDK_LULUS;
						}
						nilai_evaluasi.save();
					}
					if (!pemenang) {
						flash.error("Pemenang belum ditetapkan!");
					} else {
						Persetujuan.createPersetujuan(id, Persetujuan.JenisPersetujuan.PEMENANG_LELANG);
					}
				}

		}
        index(id);
	}

	//submit penetapan untuk lelang cepat
	@AllowAccess({Group.PANITIA})
	public static void submit_penetapan_express(Long id, Long radio,String[] alasan, Long[] nilai, @As(binder = RupiahArrayBinder.class) Double[] harga){
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang		
		Active_user active_user = Active_user.current();
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Nilai_evaluasi nilaiEvaluasi = Nilai_evaluasi.findById(radio);
		TahapNow tahapNow = lelang.getTahapNow();
		boolean editable = tahapNow.isAllowPenetapanPemenangAkhir();
		if (editable && (nilai != null) && (harga != null)) {
			//validasi nilai harga
			Boolean validate = false;
			StringBuilder namaPeserta = new StringBuilder();
			for (int i = 0; i < nilai.length; i++) {
				Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(nilai[i]);
				HasilEvaluasiExpress hasilEvaluasiExpress = HasilEvaluasiExpress.findByPeserta(nilai_evaluasi.psr_id);
				if (hasilEvaluasiExpress.psr_harga_terkoreksi >= harga[i]) {
					validate = true;
				} else {
					validate = false;
					namaPeserta.append(", ").append(nilai_evaluasi.getPeserta().getNamaPeserta());
				}
			}
			if (validate) {
				Nilai_evaluasi nilai_evaluasi;
				for (int i = 0; i < nilai.length; i++) {
					nilai_evaluasi = Nilai_evaluasi.findById(nilai[i]);
					if (harga != null && harga[i] != null) {
						nilai_evaluasi.nev_harga = harga[i];
						nilai_evaluasi.nev_harga_terkoreksi = harga[i];
					}
					nilai_evaluasi.save();

				}
			} else {

				flash.error("<span class=\"glyphicon glyphicon-exclamation-sign\"> </span>" +
						Messages.get("flash.gmpp") + namaPeserta.substring(1) + Messages.get("flash.harga") +
						Messages.get("flash.tklbdha"));
				index(id);
			}

		}

		if (radio != null) {
			Persetujuan.deleteAllByLelangAndJenisPemenang(lelang.lls_id);
			//set persetujuan
			Persetujuan.createPersetujuan(lelang.lls_id, Persetujuan.JenisPersetujuan.PEMENANG_LELANG);
			Persetujuan persetujuanPemenang = Persetujuan.findByPegawaiPenetapanAkhir(active_user.pegawaiId, lelang.lls_id);
			persetujuanPemenang.pst_status = Persetujuan.StatusPersetujuan.SETUJU;
			persetujuanPemenang.pst_tgl_setuju = newDate();
			persetujuanPemenang.save();
			Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(id);
			List<Nilai_evaluasi> nilai_evaluasis = evaluasi.findPesertaList(true);
			int i=0;
			for(Nilai_evaluasi nev : nilai_evaluasis){
				Peserta  peserta = nev.getPeserta();
				if(nilaiEvaluasi.psr_id.equals(peserta.psr_id)){
					peserta.is_pemenang_verif = 1;
				} else {
					peserta.is_pemenang_verif = 0;
					peserta.psr_alasan_menang = alasan[i];
				}
				peserta.save();
				i++;
			}
		}

		index(id);

	}
	/**
     * Persetujuan Penetapan Pemenang
     */
    @AllowAccess({Group.PANITIA})
    public static void persetujuan(Long id){
    	otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Evaluasi penetapan = Evaluasi.findPenetapanPemenang(id);
		if(penetapan == null)
			forbidden("Proses Evaluasi belum selesai. Penetapan pemenang tidak dapat dilakukan.");
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("evaluasi", penetapan);
		renderArgs.put("nego", penetapan.isShowHargaNego());
		renderArgs.put("editable", false);
        renderTemplate("lelang/evaluasi/persetujuan-evaluasi.html");
    }

	/**
	 * evaluasi Ulang
	 */
	@AllowAccess({Group.PANITIA})
	public static void persetujuanExpress(Long id){
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("evaluasi", Evaluasi.findPenetapanPemenang(id));
		renderArgs.put("editable", false);
		validation.keep();
		params.flash();
		renderTemplate("lelang/evaluasi/persetujuan-evaluasi-express.html");
	}

	/**
	 * evaluasi Ulang Prakualifikasi
	 */
	@AllowAccess({Group.PANITIA})
	public static void ulangpra(Long id){
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("list", Jadwal.findByLelang(id));
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Boolean editable = tahapStarted.isEvaluasiUlangPra() && lelang.isPrakualifikasi();
		if(flash.contains("success"))
			editable = false;
		renderArgs.put("editable", editable);
		Jadwal jadwal = Jadwal.findTahapMulaiEvaluasiUlang(lelang);
		int urutStart = jadwal != null && editable ? jadwal.akt_urut : 0;
		renderArgs.put("urutStart", urutStart);
		renderArgs.put("backUrl", Router.reverse("lelang.LelangCtr.praGagal").add("id", id));
		renderTemplate("lelang/evaluasi/evaluasi-ulang.html");
	}

	/**
	 * evaluasi Ulang
	 */
	@AllowAccess({Group.PANITIA})
	public static void ulang(Long id){
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("list", Jadwal.findByLelang(id));
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Boolean editable = tahapStarted.isEvaluasiUlang();
		if(flash.contains("success"))
			editable = false;
		renderArgs.put("editable", editable);
		Jadwal jadwal = Jadwal.findTahapMulaiEvaluasiUlang(lelang);
		int urutStart = jadwal != null && editable ? jadwal.akt_urut : 0;
		renderArgs.put("urutStart", urutStart);
		renderArgs.put("backUrl", Router.reverse("lelang.LelangCtr.tutup").add("id", id));
		renderTemplate("lelang/evaluasi/evaluasi-ulang.html");
	}
	
	@AllowAccess({Group.PANITIA})
	public static void submit_ulang(Long id, List<Jadwal> jadwalList, String keterangan) {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		boolean valid = true;
		Paket paket = Paket.findByLelang(id);
		boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
		List<Anggota_panitia> anggotaPanitia = null;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		boolean isUlangPra = lelang.getTahapStarted().isEvaluasiUlangPra() && lelang.isPrakualifikasi();
		if (isOSD) {
			anggotaPanitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
			ServiceResult result = OSDUtil.validCertPanitia(anggotaPanitia, lelang, jadwalList);
			if (!result.isSuccess()) {
				flash.error(result.getMessage());
				Jadwal.paramFlash(jadwalList);
				ulang(id);
			}
			valid = result.isSuccess();
		}
		if (valid) {
			boolean harusBerubah = params._contains("harus_berubah");
			if(harusBerubah) {
				validation.min(keterangan.length(), 31).key("keterangan").message(Messages.get("flash.ahldk"));
			}
			Jadwal jadwal = Jadwal.findTahapMulaiEvaluasiUlang(lelang);
			Jadwal.validate(lelang, jadwalList, newDate(), keterangan, jadwal.akt_urut);
			if (validation.hasErrors()) {
				validation.keep();
				Jadwal.paramFlash(jadwalList);
			} else {
				try {
					Jadwal.simpanJadwal(lelang, jadwalList, newDate(), keterangan);
					if (isOSD && lelang.lls_status.isAktif() && !lelang.isLelangV3()) {
						OSDUtil.createKPL(Paket.findById(lelang.pkt_id), anggotaPanitia, lelang, getRequestHost());
					}
					lelang.lls_evaluasi_ulang = Integer.valueOf(1);
					lelang.save();
					if(isUlangPra)
						Evaluasi.evaluasiUlangKualifikasi(lelang);
					else
						Evaluasi.evaluasiUlang(lelang);
					flash.success(Messages.get("flash.djttt"));
				} catch (Exception e) {
					Logger.error(e, "Simpan Jadwal gagal");
					flash.error(Messages.get("flash.djtgt"));
					Jadwal.paramFlash(jadwalList);
				}
			}
		} else { // sertifikat tidak valid
			Logger.error("Tidak bisa menyimpan Jadwal, pastikan seluruh Pokja Pemilihan memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
			flash.error(Messages.get("flash.tbmj_pspmsyah"));
			Jadwal.paramFlash(jadwalList);
		}
		if(isUlangPra)
			ulangpra(id);
		else
			ulang(id);
	}

	 /**
     * Fungsi {@code pesertaLelang} digunakan untuk menampilkan detail
     * para peserta lelang beserta hasil evaluasinya dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
    public static void hasil(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		notFoundIfNull(lelang);
		TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("isAuction", ReverseAuction.findByLelang(id) != null);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("showKualifikasi", tahapStarted.isShowHasilEvaluasiKualifikasi());
		renderArgs.put("showTeknis", tahapStarted.isShowHasilEvaluasiTeknis());
		renderArgs.put("showEvaluasi", tahapStarted.isShowHasilEvaluasi());
		renderArgs.put("hasil", HasilEvaluasi.findByLelang(id));
		boolean isSelesai = false;
		Evaluasi teknis = Evaluasi.findTeknis(id);
		if(teknis != null)
			renderArgs.put("showSkorTeknis", teknis.isAnySkor());
		Evaluasi harga = Evaluasi.findHarga(id);
		if(harga != null)
			renderArgs.put("showSkorHarga", harga.isAnySkor());
		Evaluasi penetapan = Evaluasi.findPenetapanPemenang(id);
		if(penetapan != null) {
			isSelesai = penetapan.eva_status.isSelesai();
			renderArgs.put("showSkorAkhir", penetapan.isAnySkor());
            renderArgs.put("showHargaNego", penetapan.isShowHargaNego());
		}
		renderArgs.put("isSelesai", isSelesai);
		renderArgs.put("sppbj", Sppbj.find("lls_id=?", id).first());
		if(lelang.isExpress())
			renderTemplate("lelang/evaluasi/hasil-evaluasi-express.html");
		else
			renderTemplate("lelang/evaluasi/hasil-evaluasi.html");
	}
    
    
    @AllowAccess({Group.PANITIA, Group.PPK, Group.AUDITOR})
	public static void checklist(Long id) {
    	Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(id);
    	Evaluasi evaluasi = Nilai_evaluasi.findById(nilai_evaluasi.eva_id);
    	Peserta peserta = Peserta.findById(nilai_evaluasi.psr_id);
    	otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
    	Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("peserta", peserta);
		renderArgs.put("nilai_evaluasi", nilai_evaluasi);
		renderArgs.put("group", Active_user.current().group);
		renderArgs.put("tahapStarted",  lelang.getTahapStarted());
    	if(lelang.isPrakualifikasi()) {
    		Integer jml = MailQueueDao.countPesanKualifikasi(lelang.lls_id, peserta.rkn_id);
			renderArgs.put("jmlPesan", jml);
    	}
		renderArgs.put("editable", true);
    	if(lelang.isLelangV3()) {
    		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_PRAKUALIFIKASI);
    		List<Checklist> syaratList = dok_lelang.getSyaratIjinUsaha();
    		syaratList.addAll(dok_lelang.getSyaratKualifikasi());
			renderArgs.put("syaratList", syaratList);
			renderArgs.put("syaratPaketList", Syarat_paket.findByPaket(lelang.pkt_id));
    		renderTemplate("lelang/evaluasi/checklistEvaluasiV3.html");
    	} else {
	    	if(evaluasi.eva_jenis.isKualifikasi() || evaluasi.eva_jenis.isPembuktian()) {	    		
	    		JenisDokLelang jenis = JenisDokLelang.DOKUMEN_LELANG;
				if(lelang.isPrakualifikasi())
					jenis = JenisDokLelang.DOKUMEN_LELANG_PRA;
				Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id,jenis);
	    		List<Checklist> syaratList = dok_lelang.getSyaratIjinUsaha();
	    		syaratList.addAll(dok_lelang.getSyaratKualifikasi());
				renderArgs.put("syaratList", syaratList);
	    	}
	    	else if(evaluasi.eva_jenis.isTeknis()) {
	    		Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
				renderArgs.put("syaratList", dok_lelang.getSyaratTeknis());
	    	}    	
	    	renderTemplate("lelang/evaluasi/checklistEvaluasi.html");
    	}		
	}

    /**
     * Fungsi {@code lelang.pemenang} digunakan untuk menampilkan detail pemenang
     * dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
	public static void pemenang(Long id) {
		Lelang_detil lelang = Lelang_detil.findById(id);
		notFoundIfNull(lelang);
		TahapStarted tahapStarted = new TahapStarted(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("isV3", lelang.isLelangV3());
		renderArgs.put("itemized", lelang.isItemized());
		if (tahapStarted.isShowHasilEvaluasi()) {
			renderArgs.put("isAuction", ReverseAuction.findByLelang(id) != null);
			Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(id);
			List<Nilai_evaluasi> pemenang = lelang_seleksi.getPemenangList();
			if(CollectionUtils.isNotEmpty(pemenang)) {
				renderArgs.put("nev", pemenang);
				Double d = new Double(0);
				for(Nilai_evaluasi a:pemenang)
					d = Double.valueOf(d.doubleValue() + a.hargaFinal().doubleValue());
				renderArgs.put("OverPagu", d > lelang.pkt_pagu);
			}
		}
		renderTemplate("lelang/pemenang.html");
	}

	/**
	 * Fungsi {@code lelang.pemenangBerkontrak} digunakan untuk menampilkan detail pemenang berkontrak
	 * dalam halaman popup
	 * @param id parameter id lelang yang dilihat
	 */
	public static void pemenangBerkontrak(Long id) {
		Lelang_detil lelang = Lelang_detil.findById(id);
		TahapStarted tahapStarted = new TahapStarted(id);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("itemized", lelang.isItemized());
		if (tahapStarted.isShowHasilEvaluasi()) {
			Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(id);
			if (evaluasi != null && evaluasi.eva_status.isSelesai()) {
				renderArgs.put("isAuction", ReverseAuction.findByLelang(id) != null);
				if (lelang.isLelangV3()) {
					List<Nilai_evaluasi> nev = Nilai_evaluasi.find("eva_id=? and nev_lulus=1", evaluasi.eva_id).fetch();
					if (nev != null) {
						renderArgs.put("nev", nev);
						Double d = new Double(0);
						for(Nilai_evaluasi a:nev)
							d = Double.valueOf(d.doubleValue() + a.hargaFinal().doubleValue());
							renderArgs.put("OverPagu", d > lelang.pkt_pagu);
					}
				} else {
					List<Nilai_evaluasi> nev = Nilai_evaluasi.find("eva_id=? and psr_id in (select psr_id from peserta p, kontrak k WHERE p.rkn_id=k.rkn_id and k.lls_id=?)", evaluasi.eva_id, id).fetch();
					if(nev != null) {
						renderArgs.put("nev", nev);
						Double d = new Double(0);
						for(Nilai_evaluasi a:nev)
							d = Double.valueOf(d.doubleValue() + a.hargaFinal().doubleValue());
							renderArgs.put("OverPagu", d > lelang.pkt_pagu);
					} else {
						flash.error("Pemenang Berkontrak belum ditentukan.");
					}
				}
			}
		}
		renderTemplate("lelang/pemenang-berkontrak.html");
	}
    
    /*
     * Panitia kirim pesan kepada peserta terhadap kekurangan persyaratan kualifikasi yang dikirim
     * proses ini hanya terjadi pada prakualifikasi
     */
	@AllowAccess({Group.PANITIA})
	public static void kirim_pesan(Long id) {
		Peserta peserta = Peserta.findById(id);
		otorisasiDataPeserta(peserta); // check otorisasi data peserta
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		renderArgs.put("peserta", peserta);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", lelang.getTahapStarted());
		renderArgs.put("list", MailQueueDao.findPesanKualifikasi(lelang.lls_id, peserta.rkn_id));
		renderTemplate("lelang/evaluasi/kirim-pesan-kualifikasi.html");
	}
    
    /**
     * submit kirim pesan kualifikasi saat panitia minta dokumen tambahan
     * @param id
     * @param pesan
     */
    @AllowAccess({Group.PANITIA})
    public static void submit_kirim_pesan(Long id, @Required String pesan){
    	checkAuthenticity();
    	if(validation.hasErrors()){
    		validation.keep();
    		params.flash();
    	} else {
    		try {
	    		Peserta peserta = Peserta.findById(id);
	    		otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
	    		String namaPaket = Paket.getNamaPaketFromlelang(peserta.lls_id);
	    		String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
	    		MailQueueDao.kirimPesanKualifikasi(peserta.getRekanan(), pesan, peserta.lls_id, namaPaket, namaPanitia);
				Dok_penawaran dok = peserta.getDokSusulan();
				if(dok != null) {
					dok.dok_disclaim = 0;		
					dok.save();
				}

				peserta.is_dikirim_pesan = true;
				peserta.sudah_evaluasi_kualifikasi = true;
				peserta.save();
    		}catch(Exception e) {
    			flash.error(Messages.get("flash.tjktp"));
    			Logger.error(e, "Terjadi kesalahan teknis pengiriman pesan Pembuktian Kualifikasi.");
    		}
		}
    	kirim_pesan(id);
    }
    
    /*
     * Panitia kirim undangan pembuktian kepada penyedia
     */
	@AllowAccess({Group.PANITIA})
	public static void kirim_undangan_evaluasi(Long id, int jenisUndangan) {
		Peserta peserta = Peserta.findById(id);
		otorisasiDataPeserta(peserta); // check otorisasi data peserta
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		renderArgs.put("peserta", peserta);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", lelang.getTahapStarted());
		renderArgs.put("jenisUndangan", jenisUndangan);
		renderArgs.put("namaPanitia", lelang.getPanitia().pnt_nama);
		renderArgs.put("list", MailQueueDao.findUndanganEvaluasi(lelang.lls_id, peserta.rkn_id, jenisUndangan));
		if(jenisUndangan == JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id) {
			renderTemplate("lelang/evaluasi/kirim-undangan-pembuktian.html");
		} else if(jenisUndangan == JenisEmail.UNDANGAN_ADMINISTRASI.id) {
			renderTemplate("lelang/evaluasi/kirim-undangan-administrasi.html");
		} else if(jenisUndangan == JenisEmail.UNDANGAN_TEKNIS.id) {
			renderTemplate("lelang/evaluasi/kirim-undangan-teknis.html");
		} else if(jenisUndangan == JenisEmail.UNDANGAN_HARGA.id) {
			renderTemplate("lelang/evaluasi/kirim-undangan-harga.html");
		}
	}
    
    /**
     * submit kirim kirim undangan pembuktian kepada penyedia
     * @param id
	 * @param waktu
	 * @param tempat
	 * @param dibawa
	 * @param hadir
     */
    @AllowAccess({Group.PANITIA})
    public static void submit_kirim_und_evaluasi(Long id, int jenisUndangan,
			@Required @As(binder=DatetimeBinder.class) Date waktu,
			@Required @As(binder=DatetimeBinder.class) Date sampai, @Required String tempat,
    		@Required String dibawa, @Required String hadir){
    	checkAuthenticity();
	    Date now = newDate();
		if (waktu != null && waktu.before(now)) {
			validation.addError("waktu", Messages.get("flash.wyamsl"));
		}
		if (waktu != null && sampai != null && sampai.before(waktu)) {
			validation.addError("sampai", Messages.get("flash.wsmwm"));
		}
		Integer tipePesan = null;
		if (jenisUndangan == JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id) {
			tipePesan = params.get("tipe_pesan", Integer.class);
			LogUtil.debug(TAG, "message type: " + tipePesan);
			if (tipePesan == null || tipePesan <= 0) {
				flash.error(Messages.get("flash.gku_mptpyaak"));
				kirim_undangan_evaluasi(id, jenisUndangan);
			}
		}
    	if(validation.hasErrors()){
    		validation.keep();
    		params.flash();
    		flash.error(Messages.get("flash.gku_mcia"));
    	} else {
    		try {
	    		Peserta peserta = Peserta.findById(id);
	    		otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
	    		String namaPaket = Paket.getNamaPaketFromlelang(peserta.lls_id);
	    		String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
	    		MailQueueDao.kirimUndanganEvaluasi(
	    				peserta.getRekanan(), peserta.lls_id, namaPaket, namaPanitia,
						waktu, sampai, tempat, dibawa,
						hadir, jenisUndangan, tipePesan);
	    		flash.success(Messages.get("flash.upkbt"));
    		}catch(Exception e) {
    			flash.error(Messages.get("flash.tktpupk"));
    			Logger.error(e, "Terjadi kesalahan teknis pengiriman Undangan Pembuktian Kualifikasi.");
    		}
		}
    	kirim_undangan_evaluasi(id, jenisUndangan);
    }

	/*
	 * Panitia kirim undangan verifikasi kepada penyedia
	 */
	@AllowAccess({Group.PANITIA})
	public static void kirim_undangan_verifikasi(Long id) {
		Peserta peserta = Peserta.findById(id);
		otorisasiDataPeserta(peserta); // check otorisasi data peserta
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		renderArgs.put("peserta", peserta);
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", lelang.getTahapStarted());
		renderTemplate("lelang/evaluasi/kirim-undangan-verifikasi.html");
	}

	/*
	 * Panitia cetak undangan verifikasi
	 */
	@AllowAccess({Group.PANITIA})
	public static void cetak_undangan_verifikasi(Long id) throws Exception {
		Peserta peserta = Peserta.findById(id);
		otorisasiDataPeserta(peserta); // check otorisasi data peserta
		UtilityCtr.viewUdanganPdf(peserta.lls_id, peserta.rkn_id, JenisEmail.UNDANGAN_VERIFIKASI);
	}

	/**
     * submit kirim kirim undangan pembuktian kepada penyedia
     * @param id
     * @param waktu
	 * @param tempat
	 * @param dibawa
	 * @param hadir
     */
    @AllowAccess({Group.PANITIA})
    public static void submit_kirim_und_verifikasi(Long id, @Required @As(binder=DatetimeBinder.class) Date waktu, @Required String tempat, 
    		@Required String dibawa, @Required String hadir){
    	checkAuthenticity();
    	if(validation.hasErrors()){
    		validation.keep();
    		params.flash();
    	} else {
    		try {
	    		Peserta peserta = Peserta.findById(id);
	    		otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
	    		String namaPaket = Paket.getNamaPaketFromlelang(peserta.lls_id);
	    		String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
	    		MailQueueDao.kirimUndanganVerifikasi(peserta.getRekanan(), peserta.lls_id, namaPaket, namaPanitia, waktu, tempat, dibawa, hadir);
	    		flash.success(Messages.get("flash.uvbt"));
    		}catch(Exception e) {
    			flash.error(Messages.get("flash.tktpuv"));
    			Logger.error(e, "Terjadi kesalahan teknis pengiriman Undangan Verifikasi.");
    		}
		}
    	kirim_undangan_verifikasi(id);
    }
    

	/**
	 * verifikasi SIKaP setiap peserta
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA, Group.AUDITOR})
	public static void verifikasi_sikap(Long id){
		Peserta peserta = Peserta.findById(id);
		Panitia panitia = Panitia.findByLelang(peserta.lls_id);
		otorisasiDataPanitia(panitia.pnt_id); // check otorisasi data panitia
		Active_user active_user = Active_user.current();
		int repoId = ConfigurationDao.getRepoId();
		Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("lls_id", peserta.lls_id);
		jsonObject.addProperty("rkn_id", peserta.rkn_id);
		jsonObject.addProperty("repo_id", repoId);
		jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);
		jsonObject.addProperty("repo_nama", pegawai.peg_nama);
		jsonObject.addProperty("peg_role", active_user.group.toString());
		jsonObject.addProperty("pnt_id", panitia.pnt_id);
		jsonObject.addProperty("pnt_nama", panitia.pnt_nama);
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
		redirect(BasicCtr.SIKAP_URL+"/verifikasi/index?q="+param);
	}

	/**
	 * verifikasi SIKaP setiap peserta
	 * @param id (kode peserta)
	 */
	@AllowAccess({Group.PANITIA})
	public static void konfirmasi_verifikasi_sikap(Long id) {
		Peserta peserta = Peserta.findById(id);
		Panitia panitia = Panitia.findByLelang(peserta.lls_id);
		otorisasiDataPanitia(panitia.pnt_id); // check otorisasi data panitia
		renderArgs.put("peserta", peserta);
		renderTemplate("lelang/evaluasi/konfirmasi-verifikasi.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void submit_konfirmasi_verifikasi_sikap(Long id, Long psr_id) {
//		Lelang_seleksi lelang = Lelang_seleksi.findById(id);

		Peserta peserta = Peserta.findById(psr_id);
		peserta.sudah_verifikasi_sikap = 1;
		peserta.save();
		flash.success("Konfirmasi Verifikasi berhasil disimpan.");

		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);
		redirect(Router.reverse("lelang.EvaluasiCtr.index", param).url);
	}

	@AllowAccess({Group.PANITIA})
	public static void lihat_kualifikasi_sikap(Long id) {
		Peserta peserta = Peserta.findById(id);
		Panitia panitia = Panitia.findByLelang(peserta.lls_id);
		otorisasiDataPeserta(peserta); // check otorisasi data peserta

		Active_user active_user = Active_user.current();
		int repoId = ConfigurationDao.getRepoId();
		Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("lls_id", peserta.lls_id);
		jsonObject.addProperty("rkn_id", peserta.rkn_id);
		jsonObject.addProperty("repo_id", repoId);
		jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);
		jsonObject.addProperty("repo_nama", pegawai.peg_nama);
		jsonObject.addProperty("peg_role", active_user.group.toString());
		jsonObject.addProperty("pnt_id", panitia.pnt_id);
		jsonObject.addProperty("pnt_nama", panitia.pnt_nama);
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
		redirect(BasicCtr.SIKAP_URL + "/historikualifikasi?q=" + param);
	}

	/**
	 * Penilaian SIKaP setiap peserta
	 * @param sppbjId
	 */
	@AllowAccess({Group.PPK})	
	public static void penilaian_sikap(Long sppbjId){
		Sppbj sppbj = Sppbj.findById(sppbjId);
	    if(sppbj == null){
	    	flash.error(Messages.get("flash.mabmd_sppbj"));
	    	BerandaCtr.index();
	    }
	    Long id = sppbj.lls_id;
		otorisasiDataLelang(id); // check otorisasi data lelang
		int repoId = ConfigurationDao.getRepoId();
		Active_user active_user = Active_user.current();
		Group group = active_user.group;
		Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("lls_id", id);
		jsonObject.addProperty("repo_id", repoId);
		jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);
		jsonObject.addProperty("repo_nama", pegawai.peg_nama);
		jsonObject.addProperty("peg_role", group.toString());
		jsonObject.addProperty("fmenu", "penilaian_kinerja");
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
		redirect(BasicCtr.SIKAP_URL+"/verifikasi/index?q="+param);
	}

	public static Integer pengecekanBlacklist(Long nevId){

		Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(nevId);

		Peserta peserta = Peserta.findById(nilai_evaluasi.psr_id);

		Rekanan rekanan = Rekanan.findById(peserta.rkn_id);

		Active_user active_user = Active_user.current();

		return BlacklistCheckerUtil.checkBlacklistStatus(rekanan, active_user.pegawaiId, peserta.lls_id, 1);

	}
	
	@AllowAccess({Group.PANITIA,Group.AUDITOR,Group.PPK})
	public static void history(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        boolean express = lelang.getPemilihan().isLelangExpress();
		renderArgs.put("lelang", lelang);
		renderArgs.put("express", express);
		renderArgs.put("jumlah", Evaluasi.findCountEvaluasiUlang(id));
		renderArgs.put("tahapStarted", lelang.getTahapStarted());
		renderArgs.put("list", HasilEvaluasi.findByLelang(id));
		renderTemplate("lelang/evaluasi/evaluasi-history.html");
	}

	/**
	 * Input Hasil Negosiasi
	 * untuk konsultansi dan tahap klarifikasi dan negosiasi teknis dan biaya
	 */
	@AllowAccess({Group.PANITIA})
	public static void inputHasilNegosiasi(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Evaluasi evaluasi = lelang.getEvaluasiAkhir();
		renderArgs.put("lelang", lelang);
		renderArgs.put("editable", lelang.isAllowNego());
		if (evaluasi != null) {
			renderArgs.put("pesertaList", evaluasi.findPesertaList(true));
			renderArgs.put("sudahDiTetapkan", evaluasi.eva_status.isSelesai());
		}
//		renderArgs.put("sudahDitetapkan", Peserta.count("lls_id=? AND is_pemenang_klarifikasi = 1", id) > 0);
		renderTemplate("lelang/evaluasi/klarifikasi-negosiasi-teknis-biaya.html");
	}


	/**
	 * Submit Hasil Negosiasi
	 * untuk konsultansi dan tahap klarifikasi dan negosiasi teknis dan biaya
	 */
	@AllowAccess({Group.PANITIA})
	public static void submitHasilNegosiasi(Long id, Long[] nilai, @As(binder = RupiahArrayBinder.class) Double[] harga, Long pemenang, String[] alasan){
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang

		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		boolean editable = lelang.isAllowNego();
		if(editable && (nilai != null)) {
			if(harga==null){
				flash.error("harga tidak boleh null");
			}
			StringBuilder namaPeserta = new StringBuilder();
			for(int i=0; i< nilai.length; i++) {
				Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(nilai[i]);
				if (nilai_evaluasi.nev_harga_terkoreksi < harga[i]) {
					if(namaPeserta.length() > 1)
						namaPeserta.append(", ");
					namaPeserta.append(nilai_evaluasi.getPeserta().getNamaPeserta());
				}
			}
			if (namaPeserta.length() == 0) {
				for (int i = 0; i < nilai.length; i++) {
					Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(nilai[i]);
					Peserta peserta = nilai_evaluasi.getPeserta();
					if (harga != null && harga[i] != null){
						nilai_evaluasi.nev_harga_negosiasi = harga[i];
					}
					if(pemenang != null && nilai_evaluasi.nev_id.equals(pemenang)) {
						nilai_evaluasi.setLulus(true);
						peserta.is_pemenang_klarifikasi = Integer.valueOf(1);
						peserta.is_pemenang = Integer.valueOf(1);
					} else {
						peserta.is_pemenang_klarifikasi = Integer.valueOf(0);
						peserta.is_pemenang = Integer.valueOf(0);
						nilai_evaluasi.setLulus(false);
						peserta.psr_alasan_menang = alasan[i];
					}
					nilai_evaluasi.save();
					peserta.save();
				}
				flash.success(Messages.get("flash.bshn"));
			}
			else {
				flash.error("Harga Negosiasi %s tidak boleh lebih dari Harga Terkoreksi", namaPeserta.toString());
			}
		}
		index(id);
	}

	@AllowAccess({Group.PANITIA})
	public static void submit_penetapan_itemized(Long id, List<Nilai_evaluasi> nilai){
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		TahapNow tahapNow = lelang.getTahapNow();
		boolean editable = tahapNow.isAllowPenetapanPemenangAkhir();
		if(editable && !CollectionUtils.isEmpty(nilai)) {
			List<Long> saveId = new ArrayList<>();
			nilai.forEach(o -> {
				Nilai_evaluasi nilai_evaluasi = Nilai_evaluasi.findById(o.nev_id);
				nilai_evaluasi.nev_skor = o.nev_skor;
				nilai_evaluasi.nev_lulus = o.nev_lulus == null ? StatusNilaiEvaluasi.TDK_LULUS : o.nev_lulus;
				nilai_evaluasi.nev_urutan = 1;
				nilai_evaluasi.nev_harga_negosiasi = o.nev_harga_negosiasi;
				nilai_evaluasi.save();
				saveId.add(o.nev_id);
			});
			// yg masuk dalam nilai[i] berarti tidak lulus
			boolean pemenang = false;
			if(saveId.size() > 0) {
				Nilai_evaluasi nevFirst = Nilai_evaluasi.findById(saveId.get(0));
				String notIn = "('" + StringUtils.join(saveId, "','") + "')";
				Query.update("UPDATE nilai_evaluasi SET nev_lulus = 0 WHERE eva_id=? AND nev_id not in " + notIn, nevFirst.eva_id);
				pemenang = Nilai_evaluasi.count("eva_id=? AND nev_lulus = 1", nevFirst.eva_id) > 0;
			}
			if (!pemenang) {
				flash.error("Pemenang belum ditetapkan!");
			}else {
				Persetujuan.createPersetujuan(id, Persetujuan.JenisPersetujuan.PEMENANG_LELANG);
			}
		}
		index(id);
	}

	// shortcut jika proses evaluasi ulang/pemasukan pemasukan ulang gagal oleh pokja
	public static void ulang_create(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		notFoundIfNull(lelang);
		boolean isUlangPra = lelang.getTahapStarted().isEvaluasiUlangPra() && lelang.isPrakualifikasi();
		if(isUlangPra)
			Evaluasi.evaluasiUlangKualifikasi(lelang);
		else
			Evaluasi.evaluasiUlang(lelang);
	}
}