package controllers.lelang;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.AesDecryptBinder;
import models.agency.DaftarKuantitas;
import models.agency.Panitia;
import models.agency.Rincian_hps;
import models.common.Active_user;
import models.common.MailQueueDao;
import models.common.TahapStarted;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Required;
import play.i18n.Messages;
import play.libs.MimeTypes;
import play.mvc.Http;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.AesUtil;
import utils.HtmlUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class AuctionCtr extends BasicCtr {

//    public static final int DEFAULT_SELESAI_AUCTION = Integer.valueOf(Play.configuration.getProperty("default.auction.waktu", "2")); // satuan jam

    @AllowAccess({Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK})
    public static void index(Long id) {
        notFoundIfNull(id);
        otorisasiDataLelang(id);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        TahapStarted tahapStarted = lelang.getTahapStarted();
        ReverseAuction auction = ReverseAuction.findByLelang(id);
        
        
        System.out.println("ID"+id+"auction"+auction.ra_id);
        
        
        if(auction == null && lelang.isAuction()){
            Integer versi = Evaluasi.findCurrentVersi(id);
            auction = ReverseAuction.create(lelang, versi);
        }else{
            notFoundIfNull(auction);
        }

        renderArgs.put("lelang", lelang);
        renderArgs.put("tahapStarted", tahapStarted);
        renderArgs.put("auction", auction);
        renderArgs.put("editable", auction.isEditable());
        renderArgs.put("hps", lelang.getPaket().pkt_hps);
        Active_user active_user = Active_user.current();
        if(active_user.isRekanan()) {
            Peserta peserta = Peserta.findByRekananAndLelang(active_user.rekananId, id);
            ReverseAuctionDetil lastPenawaran = ReverseAuctionDetil.findLastPenawaran(auction.ra_id, peserta.psr_id);
            
            System.out.println("PESERTA 1"+ peserta.psr_id);
            
            renderArgs.put("peserta", peserta);
            if (lastPenawaran.dkh != null) {
                if (lastPenawaran.dkh.items != null)
                    renderArgs.put("data",  CommonUtil.toJson(lastPenawaran.dkh.items));
            }
            ReverseAuctionDetil penawaranTermurah = ReverseAuctionDetil.findPenawaranTermurah(auction.ra_id);
            
            
            System.out.println("auction.ra_id"+auction.ra_id);
            
            
            if(penawaranTermurah != null)
                renderArgs.put("penawaranTermurah", penawaranTermurah.rad_id);
            
            renderArgs.put("penawaranlist", ReverseAuctionDetil.findByPeserta(peserta.psr_id));
            
            renderArgs.put("lastPenawaran", lastPenawaran);
            AesUtil aesUtil = new AesUtil(256, 1000);
            AesUtil.AesKey aesKey = aesUtil.getKey(session.getId());
            renderArgs.put("token", aesKey.token);
            renderArgs.put("iv", aesKey.iv);
            renderArgs.put("salt", aesKey.salt);
            renderTemplate("lelang/auction/auction-rekanan-sse.html");
        } else if (active_user.isPanitia()  || active_user.isAuditor() || active_user.isPpk()) {
            if(auction.isSelesai()) {
                renderArgs.put("hasil", auction.hasilAkhir());
                renderArgs.put("termurah", auction.termurah());
            }
            renderTemplate("lelang/auction/auction-panitia.html");
        }
    }

    @AllowAccess({Group.PANITIA})
    public static void ubah(Long id) {
        otorisasiDataLelang(id);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        ReverseAuction auction = ReverseAuction.findByLelang(id);
        if(auction == null || !auction.isEditable()) {
            String message = Messages.get("not-authorized");
            forbidden(message);
        }
        renderArgs.put("showAlasan", auction.isNeedAlasan());
        renderArgs.put("lelang", lelang);
        renderArgs.put("auction", auction);
        renderTemplate("lelang/auction/auction-ubah-waktu.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void submitPengaturan(Long id, @Required ReverseAuction auction, String alasan) {
        checkAuthenticity();
        otorisasiDataLelang(id);
        if(validation.hasErrors()){
            validation.keep();
            params.flash();
        } else {
            Lelang_seleksi lelang = Lelang_seleksi.findById(id);
            auction.lls_id = lelang.lls_id;
            if(!lelang.isExpress()){
            	try {
            	    auction.validate(newDate());
                }catch (Exception e) {
                    flash.error(e.getMessage());
                    index(id);
                }
            }
            try {
                ReverseAuction obj = ReverseAuction.findByLelang(id);
                obj.ra_waktu_mulai = auction.ra_waktu_mulai;
                obj.ra_waktu_selesai = auction.ra_waktu_selesai;
//                obj.ra_min_penawaran = auction.ra_min_penawaran;
                obj.ra_status = ReverseAuction.StatusReverseAuction.NON_EDITABLE;
                if(!StringUtils.isEmpty(alasan))
                    obj.ra_alasan += "\n\n"+alasan;
                obj.save();
                if (!lelang.isExpress()) {// selain tender cepat, sistem akan kirim undangan
                    Panitia panitia = Panitia.findByLelang(id);
                    MailQueueDao.kirimUndanganReverseAuction(id, lelang.getNamaPaket(), panitia.pnt_nama, obj.ra_waktu_mulai, obj.ra_waktu_selesai, obj.getPeserta());
                }
                flash.success(Messages.get("flash.pts"));
            }catch (Exception e) {
                Logger.error(e, "Pengaturan Gagal Tersimpan");
                flash.error(Messages.get("flash.pgt"));
            }
        }
        index(id);
    }

    @AllowAccess({Group.REKANAN})
    public static void submitPenawaran(Long id, Long file_id, @As(binder = AesDecryptBinder.class) String data) {
        checkAuthenticity();
        otorisasiDataLelang(id);
        JsonObject result = new JsonObject();
        Double nilai_total = 0.0;
        Rincian_hps[] items = null;
        if (!StringUtils.isEmpty(data)) {
            items = Rincian_hps.fromJson(data);
            nilai_total = Rincian_hps.total(items);
        }
        if (nilai_total != null && nilai_total == 0.0) {
            result.addProperty("message", Messages.get("flash.tnptb"));
        } else if (items == null) {
            result.addProperty("message", Messages.get("flash.rptbk"));
        } else {
            ReverseAuction auction = ReverseAuction.findByLelang(id);
            if(auction.isSelesai()) {
                result.addProperty("message", Messages.get("flash.wrash"));
            } else {
                try{
                    Peserta peserta = Peserta.findByRekananAndLelang(Active_user.current().rekananId, id);
                    auction.validatePenawaran(peserta, nilai_total);
                    DaftarKuantitas dk = new DaftarKuantitas();
                    dk.items = Arrays.asList(items);
                    dk.fixed = true;
                    if (nilai_total > 0) {
                        dk.total = nilai_total;
                    }
                    ReverseAuctionDetil auctionDetil = new ReverseAuctionDetil();
                    auctionDetil.ra_id = auction.ra_id;
                    auctionDetil.psr_id = peserta.psr_id;
                    auctionDetil.rad_nev_harga = nilai_total;
                    auctionDetil.dkh = dk;
                    if(file_id != null){
                        auctionDetil.rad_id_attachment = file_id;
                    }
                    auctionDetil.save();
                    result.addProperty("message", "ok");
                    Cache.delete("peserta_auction_"+auction.ra_id+"_"+peserta.psr_id);
                    Cache.delete("penawar_termurah_"+auction.ra_id);
                }catch (Exception e) {
                    Logger.error(e, Messages.get("flash.gkp"), e.getMessage());
                    result.addProperty("message", e.getMessage());
                    //result.addProperty("message", "Gagal Kirim Penawaran, mohon periksa inputan Anda!");
                }
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PANITIA})
    public static void pembukaan(Long id) {
        otorisasiDataLelang(id);
        ReverseAuction auction = ReverseAuction.findByLelang(id);
        auction.sendPenawaranPeserta();
        auction.ra_status = ReverseAuction.StatusReverseAuction.SELESAI;
        auction.save();
        index(id);
    }

    @AllowAccess({Group.PANITIA, Group.PPK})
    public static void penawaran(Long id) {
        Peserta peserta = Peserta.findById(id);
        notFoundIfNull(peserta);
        otorisasiDataLelang(peserta.lls_id);
        ReverseAuction auction = ReverseAuction.findByLelang(peserta.lls_id);
        if(auction != null && auction.isEditable())
            forbidden(Messages.get("flash.forbidden"));
        Lelang_seleksi lelang = Lelang_seleksi.findById(auction.lls_id);
        renderArgs.put("id", id);
        renderArgs.put("auction", auction);
        renderArgs.put("lelang", lelang);
        renderArgs.put("namaPeserta", peserta.getNamaPeserta());
        renderTemplate("/lelang/auction/auction-penawaran.html");
    }

    @AllowAccess({Group.PANITIA, Group.PPK})
    public static void cetak_penawaran(Long id) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id);
        ReverseAuction auction = ReverseAuction.findByLelang(peserta.lls_id);
        if(auction != null && auction.isEditable())
            forbidden(Messages.get("flash.forbidden"));
        String namafile = "rincian-penawaran-"+peserta.getNamaPeserta()+".pdf";
        response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
        Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
        response.headers.put("Content-Disposition", header);
        Template template = TemplateLoader.load("/lelang/auction/auction-penawaran-print.html");
        Lelang_seleksi lelang = Lelang_seleksi.findById(auction.lls_id);
        Map<String, Object> param = new HashMap<>();
        param.put("id", id);
        param.put("auction", auction);
        param.put("lelang", lelang);
        param.put("namaPeserta", peserta.getNamaPeserta());
        renderBinary(HtmlUtil.generatePDF(template, param));
    }

    // shortcut jika ada kendala Reverse auction tidak bisa di create, executor lkpp
    public static void create(Long id) {
        notFoundIfNull(id);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        notFoundIfNull(lelang);
        ReverseAuction auction = ReverseAuction.findByLelang(id);
        if(auction != null)
            response.writeChunk("Reverse auction sudah pernah dibuat");
        else if(lelang.isAuction()){
            Evaluasi penetapan = Evaluasi.findPenetapanPemenang(id);
            notFoundIfNull(penetapan);
            if (penetapan != null) {
                if (Nilai_evaluasi.count("eva_id=?", penetapan.eva_id) == 2) { // negosiasi
                    ReverseAuction.create(lelang, penetapan.eva_versi);
                    response.writeChunk("Reverse auction dibuat");
                }
            }
        }
    }
}
