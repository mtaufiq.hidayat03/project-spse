package controllers.lelang;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.lelang.Peserta;
import models.lelang.ReverseAuction;
import models.lelang.ReverseAuctionDetil;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.Json;
import utils.AesUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arief Ardiyansah
 * Auction Menggunakan Server Sent Event (SSE)
 */

public class AuctionSSECtr extends BasicCtr {

    @AllowAccess({Group.REKANAN})
    public static void join(Long id) {
        otorisasiDataLelang(id);
        Active_user active_user = Active_user.current();
        ReverseAuction auction = ReverseAuction.findByLelang(id);
        Peserta peserta = Peserta.findByRekananAndLelang(active_user.rekananId, id);
        response.encoding = "UTF-8";
        response.contentType = "text/event-stream";
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Access-Control-Allow-Origin", "*");
        JsonObject result = null;
        AesUtil aesUtil = new AesUtil(256, 1000);
        while (!auction.isSelesai()) {
            await(500);
            String jsonPenawaran = Cache.get("peserta_auction_"+auction.ra_id+"_"+peserta.psr_id, String.class);
            if(StringUtils.isEmpty(jsonPenawaran)) {
                ReverseAuctionDetil lastPenawaran = ReverseAuctionDetil.findLastPenawaran(auction.ra_id, peserta.psr_id);
                if(lastPenawaran != null && lastPenawaran.rad_nev_harga > 0.0) {
                    result = new JsonObject();
                    result.addProperty("id", lastPenawaran.rad_id);
                    result.addProperty("penawaran", FormatUtils.formatCurrencyRupiah(lastPenawaran.rad_nev_harga));
                    result.addProperty("waktu_pengiriman", FormatUtils.formatDateTimeInd(lastPenawaran.auditupdate));
                    if(lastPenawaran.getDokumen()!=null) {
                        result.addProperty("nama_file", lastPenawaran.getDokumen().blb_nama_file);
                        result.addProperty("file_url", lastPenawaran.getDownloadUrl());
                    }
                    else{
                        result.addProperty("nama_file", "");
                        result.addProperty("file_url", "");
                    }
                    jsonPenawaran = result.toString();
                    Cache.set("peserta_auction_" + auction.ra_id + "_" + peserta.psr_id, jsonPenawaran);
                }
            }
            result = Json.fromJson(jsonPenawaran, JsonObject.class);
            if(result == null)
                result = new JsonObject();
            result.addProperty("waktu", auction.sisaWaktu());
            Long penawaranTermurah = Cache.get("penawar_termurah_"+auction.ra_id, Long.class);
            if(penawaranTermurah == null) {
                ReverseAuctionDetil termurah = ReverseAuctionDetil.findPenawaranTermurah(auction.ra_id);
                if(termurah != null) {
                    penawaranTermurah = termurah.rad_id;
                    Cache.set("penawar_termurah_"+auction.ra_id, penawaranTermurah);
                }
            }
            String keterangan = Messages.get("flash.pabt");
            if(penawaranTermurah != null && result.get("id") != null && result.get("id").getAsLong() == penawaranTermurah.longValue())
                keterangan = Messages.get("flash.pat");
            result.addProperty("keterangan",  keterangan);
            response.writeChunk("event: message\n"+ "data:" + aesUtil.encrypt(session.getId(), result.toString())+ "\n\n");
        }
        await(500);
        result = new JsonObject();
        result.addProperty("waktu", Messages.get("flash.wah"));
        response.writeChunk("event: message\n" + "data: "+result.toString()+"\r\n\n");
    }
    
	 public static void RaHpsExcel(Long lls_id,Peserta peserta,ReverseAuction auction , File file) {
			Map<String, Object> result = new HashMap<>(1);
			try {
                BlobTable blob;
                blob = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
				result.put("success", true);
				result.put("file_id", blob.blb_id_content);
			} catch (Exception e) {
				Logger.error(e,"Kesalahan saat upload informasi lainnya");
				result.put("result", "Kesalahan saat upload informasi lainnya");
				result.put("success", false);
			}

			renderJSON(result);
	}

}
