package controllers.lelang;

import com.google.gson.Gson;
import ext.StringFormat;
import models.handler.DiskusiLelangHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.lelang.Diskusi_lelang;
import models.lelang.Peserta;
import play.Logger;
import play.cache.Cache;
import play.mvc.Http;
import play.mvc.WebSocketController;

import java.util.*;
import java.util.Map.Entry;

public class PenjelasanWSCtr extends WebSocketController {
	
	public enum ACTION
	{
		INIT,
		UPLOAD,
		TANYA,
		JAWAB,
		WAKTU
	}

	/**Class untuk menyimpan OpenConnection */
	public static class WSConnection
	{
		Http.Inbound inbound;
		Http.Outbound outbound;
		boolean isPanitia;//yang konek panitia apa bukan?
		Long rkn_id;//ID rekanan yang konek (jika rkanan)
		public WSConnection(Http.Inbound inbound, Http.Outbound outbound, boolean isPanitia, Long rkn_id)
		{
			this.inbound=inbound;
			this.outbound=outbound;
			this.isPanitia=isPanitia;
			this.rkn_id=rkn_id;
		}
	}
	
	
	
	private static Map<String, WSConnection>  getConnectionMap()
	{
		String KEY="controllers.lelang.PenjelasanWSCtr.getConnectionMap";
		//dapatkan dari Cache
		Map<String, WSConnection> map = Cache.get(KEY, Map.class);
		if(map==null)
		{
			map=new HashMap<String, WSConnection>();
			Cache.set(KEY, map);
		}
		return map;
	}
	
	/*public static void pengadaan(Long id) {
		penjelasanSession(id, Tahap.PENJELASAN);
	}
	
	public static void kualifikasi(Long id) {
		penjelasanSession(id, Tahap.PENJELASAN_PRA);
	}
	
	*//**Web socket sesi penjelasan.
	 * Digunakan oleh panitia maupun peserta lelang yang dibedakan oleh Session
	 * 
	*//*
	public static void penjelasanSession(Long id, Tahap tahap)
	{
		Long panitiaId=BasicCtr.getPanitiaIdFromSession();
		Long rekananId=BasicCtr.active_user.get().rekananId;
		Long ppkId=null; //TODO isikan PPK id dari session & paket
		String userNameRekanan=null;
		if(rekananId!=null)
			userNameRekanan=BasicCtr.active_user.get().userid;
		boolean isPanitia=rekananId==null;
		//new socket
		WSConnection conn=new PenjelasanWSCtr.WSConnection(inbound, outbound, isPanitia, rekananId);
		Map map=getConnectionMap();
		map.put(session.getId(), conn);
		String attachmentfileName="";
		Gson gson=new Gson();
		Diskusi_lelang dsl = null;
		Logger.debug("[Penjelasan, connect] connected: %s", map.size());
		while(inbound.isOpen()) {
	         WebSocketEvent e = await(inbound.nextEvent());	  
	         boolean allow_jawab = Diskusi_lelangDao.isWriteable(id, tahap, BasicCtr.getGroupSession());   
	         for(String msg: e.TextFrame.match(e)) {	        	 
	        	 //saat pertama kali halaman di-load
	        	 if(ACTION.INIT.name().equals(msg))
	        	 {
	        		 *//**Isi penjelasan dikirim semua via WebSocket, tidak ada yang pakai render()
	        		  * Di-send ketika browser mengirim teks 'init' 
	        		  *//*        		 
	        		 List<Diskusi_lelang> list=Diskusi_lelangDao.getDiskusiLelangTopik(id);	        
	        		 for(Diskusi_lelang diskusi: list)
	        		 {
	        			 String[] data=new String[]{"", getJsonPertanyaan(diskusi, allow_jawab, rekananId)};
	        			 outbound.send(gson.toJson(data));
	        			 String[] jawab = new String[]{""+diskusi.dsl_id_topik, getJsonJawaban(diskusi)};
	        			 outbound.send(gson.toJson(jawab));
	        		 }
	        	 } else if(ACTION.WAKTU.name().equals(msg)) {
	        		 String[] data=new String[]{"WAKTU", Diskusi_lelangDao.getSisaWaktu(id, tahap)};
	        		 outbound.send(gson.toJson(data));
	        	 } else	 {
	        		 String[] data=new Gson().fromJson(msg, String[].class);
	         		 if(data[0].equals(ACTION.TANYA.name()) || data[0].equals(ACTION.JAWAB.name()))
	        		 {
	        			 //di sini kita dapatkan data dari Web Browser
			        	 dsl= Diskusi_lelangDao.insertFromJson(id, panitiaId, ppkId, userNameRekanan, data, tahap);	
			        	 sendNewDiskusiLelangToAllSockets(dsl);
	        		 };
	        		 //**Informasi nama file ada di sini. Sementara itu untuk data filenya sendiri  lihat block *//*
	        		 if(data[0].equals(ACTION.UPLOAD.name()))
	        		 {
	        			 attachmentfileName=data[1];	        			
	        		 };		        	 	        		 
	        	 }
	         } 
	         
	         for(byte[] objdata: e.BinaryFrame.match(e))
	         {
	        	 Logger.debug("[Upload Don]: %,d ", objdata.length);	        
	        	 try {
					Diskusi_lelangDao.attachFile(dsl.dsl_id_topik, objdata, attachmentfileName);
					sendNewDiskusiLelangToAllSockets(dsl);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
	         }	         
	         
	         for(WebSocketClose closed: e.SocketClosed.match(e)) {
	             map.remove(session.getId());
	             Logger.debug("[Penjelasan, disconnect] connected: %s", map.size());
	         }
	    }
	}*/
	
	/*private static void sendAttachmentToAllSockets(Long dsl_id) {
		Gson gson=new Gson();
		//send attacment info ke pertanyaannya
		Diskusi_lelang dsl=Diskusi_lelangDao.findById(dsl_id);
		String html=getHtmlAttachment(dsl);
		Object[] jsonData=new Object[] {"ATTACHMENT",dsl_id, html};
		String json=gson.toJson(jsonData);

		Map map=getConnectionMap();
		Iterator<Entry<String, WSConnection>> set=map.entrySet().iterator();
		Logger.debug("Send attachment info to %s sockets", map.size());
		while(set.hasNext())
		{
			Entry<String, WSConnection> socket=set.next();
			WSConnection ws=socket.getValue();
			if(ws.outbound.isOpen())
				ws.outbound.send(json);
		}
		
	}*/

	/**Kirim pertanyaan ke semua client yang lagi connect
	 * 
	 * @param dsl
	 * @param isPanitia
	 */
	private static void sendNewDiskusiLelangToAllSockets(Diskusi_lelang dsl) {
		Map map=getConnectionMap();
		Iterator<Entry<String, WSConnection>> set=map.entrySet().iterator();
		Logger.debug("sendNewDiskusiLelangToAllSockets");
		while(set.hasNext())
		{
			WSConnection ws=set.next().getValue();
			if(ws.outbound.isOpen())
			{
				String msg;
				Object data[]=new String[2];
				/* [0]: berisi jawabIDxxxx artinya ini jawaban
				   [0]: jika kosong ini pertanyaan
				   [1]: data HTML
				*/
				if(dsl.isPertanyaan())
				{
					msg=getJsonPertanyaan(dsl, ws.isPanitia, ws.rkn_id);
					data[0]="";
				}
				else
				{
					Diskusi_lelang tanya = Diskusi_lelang.findById(dsl.dsl_pertanyaan_id);
					msg = getJsonJawaban(tanya);
					data[0]=String.valueOf(tanya.dsl_id_topik); 				
				}				
				data[1]=msg;
				msg=new Gson().toJson(data);
				Logger.debug("ws.outbound.send : %s", msg);
				ws.outbound.send(msg);
			}
		}
	}
	
	private static String getJsonPertanyaan(Diskusi_lelang tanya, boolean allow_jawab, Long rkn_id) {
		Diskusi_content content = new Diskusi_content();
		content.id = tanya.dsl_id_topik;
		content.dokumen = tanya.dsl_dokumen+" - Bab "+tanya.dsl_bab;
//		content.no = tanya.dsl_nomor;
		content.uraian = tanya.dsl_uraian;		
		content.allow_jawab = allow_jawab && rkn_id == null;
		content.isPertanyaan = tanya.isPertanyaan();
		content.tanggal = StringFormat.formatDateTime(tanya.dsl_tanggal);
		Peserta peserta = Peserta.findById(tanya.psr_id);
		if(tanya.psr_id != null && peserta.rkn_id==rkn_id)
			content.pengirim="Anda";
		else
			content.pengirim = tanya.getPengirim();
		if(tanya.dsl_id_attachment != null){
			BlobTable blob= BlobTableDao.getLastById(tanya.dsl_id_attachment);
			content.filename = blob.getFileName();
			content.downloadUrl = blob.getDownloadUrl(DiskusiLelangHandler.class);
		}
		return CommonUtil.toJson(content);
	}
	
	
	
	private static String getJsonJawaban(Diskusi_lelang tanya) {
		List<Diskusi_content> jawabanList = new ArrayList<PenjelasanWSCtr.Diskusi_content>();
		List<Diskusi_lelang> list=tanya.getJawabanList();
		for(Diskusi_lelang jawaban: list)
		{
			Diskusi_content jwb = new Diskusi_content();
			jwb.id = jawaban.dsl_id_topik;
			jwb.dokumen = jawaban.dsl_dokumen+" - Bab "+tanya.dsl_bab;
//			jwb.no = tanya.dsl_nomor;
			jwb.uraian = StringFormat.convertNewLine(jawaban.dsl_uraian);
			jwb.pengirim = jawaban.getPengirim();
			jwb.isJawaban = !jawaban.isPertanyaan();
			jwb.tanggal = StringFormat.formatDateTime(tanya.dsl_tanggal);
			if(jawaban.dsl_id_attachment != null){
				BlobTable blob= BlobTableDao.getLastById(jawaban.dsl_id_attachment);
				jwb.filename = blob.getFileName();
				jwb.downloadUrl = blob.getDownloadUrl(DiskusiLelangHandler.class);
			}
			jawabanList.add(jwb);
		}
		return CommonUtil.toJson(jawabanList);
	}
	
	public static class Diskusi_content {
		public Long id;
		public Integer no;
		public String dokumen;
		public String uraian;
		public String pengirim;
		public String tanggal;
		public String filename;
		public String downloadUrl;
		public boolean isPertanyaan;
		public boolean isJawaban;
		public boolean allow_jawab;
	}
}
