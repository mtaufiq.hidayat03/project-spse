package controllers.lelang;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.AesDecryptBinder;
import models.agency.DaftarKuantitas;
import models.agency.Panitia;
import models.agency.Rincian_hps;
import models.common.Active_user;
import models.common.MailQueueDao;
import models.common.Tahap;
import models.common.TahapNow;
import models.common.TahapStarted;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Checklist_master.JenisChecklist;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Required;
import play.i18n.Messages;
import play.libs.MimeTypes;
import play.mvc.Http;
import play.mvc.Router;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.AesUtil;
import utils.HtmlUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class JadwalSanggahBandingCtr extends BasicCtr {
	
	@AllowAccess({Group.PANITIA, Group.REKANAN, Group.AUDITOR, Group.PPK})
    public static void index(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Active_user active_user = Active_user.current();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		renderArgs.put("lelang", lelang);
        TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("tahapStarted", tahapStarted);
		TahapNow tahapAktif = lelang.getTahapNow();
        JadwalSanggahBanding jdwl_banding  =JadwalSanggahBanding.findByLelang(id);
        if(jdwl_banding == null){
            Integer versi = Evaluasi.findCurrentVersi(id);
            jdwl_banding = JadwalSanggahBanding.create(lelang);
        }else{
            notFoundIfNull(jdwl_banding);
        }
        if(jdwl_banding != null) {
			renderArgs.put("jdwl_banding", jdwl_banding);
			renderArgs.put("sisaWaktu", jdwl_banding.sisaWaktu());
			renderArgs.put("status", jdwl_banding.sgh_status);
		}
        boolean hide = !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca());
		renderArgs.put("tahapEvaluasi",tahapStarted.isEvaluasi());
		renderArgs.put("tahapPenjelasan", tahapStarted.isPenjelasan());
		renderArgs.put("tahapPenjelasanPra", tahapStarted.isPenjelasanPra());
		renderArgs.put("tahapPengumuman", tahapStarted.isPengumuman());
		Tahap tahap = Tahap.SANGGAH;
		boolean sanggahPra = false;
		renderArgs.put("not_allowed", active_user.isRekanan() && !Dok_penawaran.isAnyPenawaran(id, active_user.rekananId));
		renderArgs.put("sanggahPra", sanggahPra);
		Group group = active_user.group;
		renderArgs.put("group", group);
		if(active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			boolean allow_kirim_sanggah = Sanggahan.isAllowKirimSanggah(lelang, group, tahap, peserta, newDate()) && lelang.lls_status.isAktif();
			boolean allow_kirim_sanggah_banding = Sanggahan.isAllowKirimSanggahBanding(lelang, group, tahap, peserta, newDate()) && lelang.lls_status.isAktif();
			boolean sudah_banding = Sanggah_banding.isSudahBanding(peserta, tahap);
			int count_jawaban = Sanggahan.countJawabanSanggahan(tahap, peserta.psr_id);
			renderArgs.put("allow_kirim_sanggah", allow_kirim_sanggah);
			renderArgs.put("allow_kirim_sanggah_banding", allow_kirim_sanggah_banding && !sudah_banding && count_jawaban > 0 && jdwl_banding.isSedangProses() && !(jdwl_banding.isSelesai()));			
			renderArgs.put("allow_sanggah_banding", !sanggahPra && count_jawaban > 0);
			if (allow_kirim_sanggah)
				renderArgs.put("checklist", Checklist_sanggah.getChecklist(JenisChecklist.CHECKLIST_KATEGORI_SANGGAHAN));
		}
		int count_sanggahan = Sanggahan.countSanggahan(lelang.lls_id, tahap);
		renderArgs.put("alllow_atur_jadwal_sanggah", count_sanggahan > 0);
		renderArgs.put("tahapAktif", tahapAktif.isSanggah());
		renderArgs.put("ket_jawab", !active_user.isRekanan() && hide);
		renderArgs.put("simpanUrl", Router.reverse("lelang.SanggahanCtr.simpan").add("id", lelang.lls_id).url);
		renderArgs.put("simpanBandingUrl", Router.reverse("lelang.SanggahanCtr.simpanBanding").add("id", lelang.lls_id).url);
		renderArgs.put("listUrl", Router.reverse("lelang.SanggahanCtr.list").add("id", lelang.lls_id).url);
		renderTemplate("lelang/sanggahan/sanggahan.html");
	}

    @AllowAccess({Group.PANITIA})
    public static void ubah(Long id) {
        otorisasiDataLelang(id);
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        JadwalSanggahBanding jadwal = JadwalSanggahBanding.findByLelang(id);
        renderArgs.put("lelang", lelang);
        renderArgs.put("jadwal", jadwal);
        renderTemplate("lelang/sanggahan/jadwal-sanggah-banding.html");
    }
    
    @AllowAccess({Group.PANITIA})
    public static void submitPengaturan(Long id, @Required JadwalSanggahBanding jadwal) {
        checkAuthenticity();
        otorisasiDataLelang(id);
        if(validation.hasErrors()){
            validation.keep();
            params.flash();
        } else {
            Lelang_seleksi lelang = Lelang_seleksi.findById(id);
            jadwal.lls_id = lelang.lls_id;
            if(!lelang.isExpress()){
            	try {
            		jadwal.validate(newDate());
                }catch (Exception e) {
                    flash.error(e.getMessage());
                    index(id);
                }
            }
            try {
                JadwalSanggahBanding obj = JadwalSanggahBanding.findByLelang(id);
                obj.sgh_waktu_mulai = jadwal.sgh_waktu_mulai;
                obj.sgh_waktu_selesai = jadwal.sgh_waktu_selesai;
                obj.sgh_status = JadwalSanggahBanding.StatusSanggahBanding.EDITABLE;
                obj.save();
                if(!lelang.isExpress()){
                    Panitia panitia = Panitia.findByLelang(id);
                    MailQueueDao.kirimUndanganSanggahBanding(id, lelang.getNamaPaket(), panitia.pnt_nama, obj.sgh_waktu_mulai, obj.sgh_waktu_selesai, obj.getPeserta());
                }

                flash.success(Messages.get("flash.pts"));
            }catch (Exception e) {
                Logger.error(e, "Pengaturan Gagal Tersimpan");
                flash.error(Messages.get("Pengaturan Gagal Tersimpan"));
            }
        }
        index(id);
    }
    
}
