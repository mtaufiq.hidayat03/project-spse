package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.agency.DaftarKuantitas;
import models.common.Active_user;
import models.common.Tahap;
import models.common.TahapStarted;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.text.StringEscapeUtils;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.HtmlUtil;
import utils.LogUtil;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * definisi semua action terkait peserta lelang dan penawarannya
 * @author arief ardiyansah
 *
 */
public class PesertaCtr extends BasicCtr {


	/**
     * Fungsi {@code PesertaCtr.index} digunakan untuk menampilkan detail
     * para peserta lelang beserta hasil evaluasinya dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
	public static void index(Long id) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		notFoundIfNull(lelang);
        TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang", lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("hide", !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca()));
        renderTemplate("lelang/peserta.html");
	}
	
	/**
     * Fungsi {@code PesertaCtr.index} digunakan untuk menampilkan detail
     * para peserta lelang beserta penawarannya
     * @param id parameter id lelang yang dilihat
     */
	@AllowAccess({ Group.PANITIA, Group.AUDITOR, Group.PPK , Group.REKANAN})
	public static void penawaran(Long id) {
		otorisasiDataLelang(id); // check otorisasi data lelang
		Active_user active_user = Active_user.current();
		// peserta yang tidak menawar tidak bisa akses data penawaran peserta
		renderArgs.put("not_allowed", active_user.isRekanan() && !Dok_penawaran.isAnyPenawaran(id, active_user.rekananId));
		renderArgs.put("group", active_user.group);
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        TahapStarted tahapStarted = lelang.getTahapStarted();
		renderArgs.put("lelang",lelang);
		renderArgs.put("tahapStarted", tahapStarted);
		renderArgs.put("hide", !tahapStarted.isShowPenyedia(lelang.getMetode().kualifikasi.isPasca()));
		renderTemplate("lelang/penawaran-peserta.html");
	}
	
	 /**
     * cetak rincian Penawaran Peserta Tender
     */
    @AllowAccess({Group.PANITIA, Group.PPK, Group.AUDITOR})
    public static void cetak_rincian_penawaran(Long id) {
    	Peserta peserta = Peserta.findById(id);
    	notFoundIfNull(peserta);
    	otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
		Map<String, Object> model = new HashMap<>();
		model.put("format", new FormatUtils());
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		String namaPeserta = peserta.getNamaPeserta();
		String namaPaket = lelang.getNamaPaket();
		DaftarKuantitas dkh = peserta.dkh != null ? peserta.dkh:null;
		model.put("dkh", dkh);
		model.put("namaPeserta", namaPeserta);
		model.put("namaPaket", namaPaket);
		model.put("peserta", peserta);
  		response.contentType = "application/pdf";
  		Template template = TemplateLoader.load("/kontrak/template/RincianPenawaran.html");
  		InputStream is = HtmlUtil.generatePDF(template, model);
  		renderBinary(is);
    }
    
    
    /**
     * rincian Penawaran Peserta Tender
     */
    @AllowAccess({Group.PANITIA, Group.PPK, Group.AUDITOR, Group.REKANAN})
    public static void rincian_penawaran(Long id) {
    	Peserta peserta = Peserta.findById(id);
    	notFoundIfNull(peserta);
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
    	otorisasiDataLelang(lelang); // check otorisasi data lelang
		renderArgs.put("peserta", peserta);
		DaftarKuantitas dkh = peserta.dkh != null ? peserta.dkh : null;
		renderArgs.put("namaPeserta", peserta.getNamaPeserta());
		renderArgs.put("namaPaket", lelang.getNamaPaket());
		renderArgs.put("allowDetilRincian", !Active_user.current().isRekanan());
		renderArgs.put("dkh",  dkh);
	    Dok_penawaran penawaran_harga = peserta.getFileHarga();
	    if(penawaran_harga != null) {
		    String tglKirim = penawaran_harga.getTglKirim();
			renderArgs.put("tglKirim", tglKirim);
	    }
    	renderTemplate("lelang/evaluasi/rincian-penawaran.html");
    }
    
    
    /**
     * rincian dokumen administrasi dan teknis
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PPK, Group.AUDITOR, Group.REKANAN})
    public static void rincian_adminteknis(Long id) {
    	Peserta peserta = Peserta.findById(id);
    	otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
    	Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		renderArgs.put("peserta", peserta);
    	Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		renderArgs.put("namaPeserta", peserta.getNamaPeserta());
		renderArgs.put("namaPaket", lelang.getNamaPaket());
		renderArgs.put("syaratAdmin", dok_lelang.getSyaratAdministrasi());
		renderArgs.put("syaratTeknis", dok_lelang.getSyaratTeknis());
	    Dok_penawaran penawaran_teknis = lelang.getPemilihan().isLelangExpress() ? peserta.getFileAdminTeknisHarga() : peserta.getFileTeknis();
	    if(penawaran_teknis != null){
		    String tglKirim = penawaran_teknis.getTglKirim();
			renderArgs.put("tglKirim", tglKirim);
	    }
    	renderTemplate("lelang/evaluasi/rincian-admin-teknis.html");
    }

	/**
	 * cetak surat penawaran penyedia
	 * @param id (id peserta)
	 */
	@AllowAccess({Group.PANITIA, Group.PPK, Group.AUDITOR, Group.REKANAN})
	public static void cetakSuratPenawaran(Long id) {
		Peserta peserta = Peserta.findById(id);
		otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
		Dok_penawaran.JenisDokPenawaran jenisDokPenawaran = lelang.getPemilihan().isLelangExpress() ?
				Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA : Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;
		Dok_penawaran penawaranPeserta = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, jenisDokPenawaran);
		Map<String, Object> model = new HashMap<>();
		model.put("peserta", peserta);
		SuratPenawaranContent surat = penawaranPeserta.getSuratPenawaranContent();
		final String harga = FormatUtils.formatCurrencyRupiah(peserta.psr_harga);
		Dok_lelang dok_lelang = Dok_lelang.findBy(peserta.lls_id, JenisDokLelang.DOKUMEN_LELANG);
		List<Checklist> checklists = new ArrayList<>();
		checklists.addAll(dok_lelang.getSyaratTeknis());
		checklists.addAll(dok_lelang.getSyaratHarga());
		LogUtil.debug("Cetak", checklists);
		if(surat != null) {
			model.put("title", surat.getTitle());
			if(!CollectionUtils.isEmpty(surat.getBody())) {
				StringBuilder body = new StringBuilder();
				for (String content : surat.getBody()) {
					if(content.contains("batas akhir pemasukan dokumen penawaran")) {
						Tahap tahapSekrang = lelang.isSatuFile() ? Tahap.PEMASUKAN_PENAWARAN:Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS;
						Jadwal jadwal = Jadwal.findByLelangNTahap(peserta.lls_id, tahapSekrang);
						if(jadwal != null) {
							content += "("+(FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir))+")";
						}
					} else if (content.contains("sebesar Surat Penawaran") || content.contains("Pemilihan Nomor:")) {
						String date = dok_lelang.getDokLelangContent().dll_tglSDP != null
								? FormatUtils.formatDateInd(dok_lelang.getDokLelangContent().dll_tglSDP)
								: "";
						content = content.replaceAll("sebesar Surat Penawaran", "sebesar " + harga)
								.replaceAll("Nomor: tanggal",
								"Nomor: " + dok_lelang.getDokLelangContent().dll_nomorSDP
										+ " tanggal " + date);
					}
					content = StringEscapeUtils.escapeHtml4(content);
					if (content.contains("kami lampirkan")) {
						StringBuilder sb = new StringBuilder();
						sb.append("<ol>");
						for (Checklist checklist : dok_lelang.getSyaratAdministrasi()) {
							if (checklist.getChecklist_master().ckm_id != 16
									&& checklist.getChecklist_master().ckm_id != 18) {
								sb.append("<li>")
										.append(StringEscapeUtils.escapeHtml4(checklist.getChkNameContent()))
										.append(".")
										.append("</li>");
							}
						}
						for (Checklist checklist : checklists) {
							sb.append("<li>")
									.append(StringEscapeUtils.escapeHtml4(checklist.getChkNameContent()))
									.append(".")
									.append("</li>");
						}
						sb.append("<li>")
								.append("FData Kualifikasi (Form isian elektronik atau data SIKaP).")
								.append("</li>")
								.append("</ol>");
						content = content.substring(0, content.indexOf("lampirkan") + 9);
						if (!content.contains(":")) {
							content += ":";
						}
						content += sb.toString();
					}
					//escape html symbols

					body.append("<p style=\"text-align: justify; font-size: 12px;\">").append(content).append("</p>");
				}
				model.put("body", body.toString());
			}
//			batas akhir pemasukan dokumen penawaran
		}
		model.put("surat", penawaranPeserta.getSuratPenawaranContent());
		model.put("tglKirim", penawaranPeserta.getTglKirimWithoutTime());
		Template template = TemplateLoader.load("/lelang/template-surat-penawaran-peserta.html");
		InputStream is = HtmlUtil.generatePDF(template, model);
		if(is == null) {
			flash.error("Gagal cetak Dokumen Surat Penawaran.");
			penawaran(peserta.lls_id);
		}
		response.contentType = "application/pdf";
		renderBinary(is, "Cetak-Penawaran.pdf");
	}
}
