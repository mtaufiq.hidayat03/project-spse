package controllers.lelang;

import ams.models.ServiceResult;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Anggota_panitia;
import models.agency.Paket;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import models.lelang.History_jadwal;
import models.lelang.Jadwal;
import models.lelang.Lelang_seleksi;
import models.lelang.TahapanLelang;
import models.secman.Group;
import play.Logger;
import play.i18n.Messages;
import utils.LogUtil;
import utils.osd.OSDUtil;

import java.util.List;

public class JadwalCtr extends BasicCtr {

	@AllowAccess({ Group.PANITIA, Group.ADM_PPE })
	public static void list(Long id, Long lelangCopyId) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		if(Active_user.current().isPanitia()) {
			otorisasiDataLelang(id); // check otorisasi data lelang
			boolean allowEditJadwal = !lelang.lls_status.isDitutup();
			if(lelang.isExpress()){ // validasi edit jadwal hnya aktif untuk jumlah penawaran 0 dan 1
				allowEditJadwal = allowEditJadwal && lelang.getJumlahPenawar() <= 1 ;
			}
			if(!allowEditJadwal)
				forbidden(Messages.get("not-authorized"));
		}
		renderArgs.put("lelang", lelang);
		renderArgs.put("lelangCopyId", lelangCopyId);
		List<Jadwal> jadwalList = Jadwal.findByLelang(lelang.lls_id);
		if (CommonUtil.isEmpty(jadwalList)) {
			jadwalList = Jadwal.getJadwalkosong(lelang.lls_id, lelang.getMetode(), lelang.getPemilihan().isLelangExpress());
		}
		int urutStart = 0;
		int i = 0;
		//if pembukaan penawaran has done, then pemasukan penawaran is denied.
		if(lelang.sudahPembukaanPenawaran() && !lelang.isExpress()){
			Tahap tahap = Tahap.PEMBUKAAN_PENAWARAN;
			if(lelang.isDuaFile()) {
				tahap = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS;
			} else if(lelang.isDuaTahap()) {
				tahap = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA;
			}
			Jadwal jadwalStart = Jadwal.findByLelangNTahap(lelang.lls_id, tahap);
			if(jadwalStart != null)
				urutStart = jadwalStart.akt_urut;
		}
		if(lelang.getPemilihan().isLelangExpress()) {
			for(Jadwal jadwal:jadwalList) {
				i++;
				if(i==1)
			   		jadwal.keterangan = "disesuaikan dengan kebutuhan";
			   	else
			   		jadwal.keterangan = "paling lama 3 (tiga) hari kerja setelah undangan Tender";
		   }
	   	}
		renderArgs.put("jadwalList", jadwalList);
		renderArgs.put("urutStart", urutStart);
		renderTemplate("lelang/jadwal/jadwal-edit.html");
	}
	
	@AllowAccess({Group.PANITIA})
	public static void copy(Long id, Long lelangCopyId) {  
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Jadwal.copyJadwal(id,lelangCopyId);
		list(id, null);
	}
	
	@AllowAccess({Group.PANITIA, Group.ADM_PPE})
    public static void simpan(Long id, List<Jadwal> jadwalList, String keterangan) {
		checkAuthenticity();
		if (Active_user.current().isPanitia())
			otorisasiDataLelang(id); // check otorisasi data lelang
		boolean valid = true;
		Paket paket = Paket.findByLelang(id);
		boolean isOSD = ConfigurationDao.isOSD(paket.pkt_tgl_buat);
		List<Anggota_panitia> anggotaPanitia = null;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		if (isOSD) {
			LogUtil.debug("JadwalCtr", jadwalList);
			anggotaPanitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
			ServiceResult result = OSDUtil.validCertPanitia(anggotaPanitia, lelang, jadwalList);
			if (!result.isSuccess()) {
				flash.error(result.getMessage());
				Jadwal.paramFlash(jadwalList);
				if(Active_user.current().isPanitia()){
					list(id,null);
				}else{
					ubahJadwalLelang(id);
				}
			}
			valid = result.isSuccess();
		}
		if (valid) {
			boolean harusBerubah = params._contains("harus_berubah");
			if (harusBerubah) {
				validation.min(keterangan.length(), 31).key("keterangan");
			}
			Jadwal.validate(lelang, jadwalList, newDate(), keterangan, 1);
			if (validation.hasErrors()) {
				validation.keep();
				valid = false;
			} else {
				try {
					Jadwal.simpanJadwal(lelang, jadwalList, newDate(), keterangan);
					if (isOSD && lelang.lls_status.isAktif() && !lelang.isLelangV3()) {
						OSDUtil.createKPL(Paket.findById(lelang.pkt_id), anggotaPanitia, lelang, getRequestHost());
					}
					flash.success(Messages.get("flash.djttt"));
				} catch (Exception e) {
					Logger.error(e, "Simpan Jadwal gagal");
                    flash.error(Messages.get("flash.djtgt"));
					valid = false;

				}
			}
		}
		else { // sertifikat tidak valid
			Logger.error("Tidak bisa simpan jadwal, pastikan seluruh panitia memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
            flash.error(Messages.get("flash.tbsj_psm"));
		}
		if (!valid) {
			Jadwal.paramFlash(jadwalList);
		}
		if(Active_user.current().isPanitia()){
		/*Ada bug untuk kasus spt ini.
		 * 1. Jadwal telah terisi dengan baik tanpa error
		 * 2. User mengedit jadwal pada baris ke-x dari nilai A menjadi B
		 * 3. Saat disimpan, baris tsb error
		 * 4. yang ditampilkan adalah nilai A; harusnya adalah nilai B (yg error). 
		 * 		Ini membuat user bingung; katanya error tapi kok bener (yg lama)
		 * 
		 *  Tidak boleh di-render() di sini karena kalau di-refresh berarti submit()
		 *  Jadi harus di-redirect ke edit.
		 *  
		 *  Action edit() harus menggunakan jadwalList dari action simpan() BUKAN load dari database.
		 *  Sebenarnya bisa pakai AJAX tapi modifikasi ini jd lebih banyak
		 */
//			Cache.set("jadwalList" + lelang.lls_id + session.getId(), jadwalList, "30s");
			list(id,null);
       }else{
    	   ubahJadwalLelang(id);
       }
    }

    /**
     * Admin Lpse mengubah jadwal 
     * Fungsi {@code ubahJadwalLelang} digunakan untuk menampilkan halaman.
     * @param lelangId
     */
    @AllowAccess({Group.ADM_PPE})
    public static void ubahJadwalLelang(Long lelangId) {
    	if(lelangId != null)
    	{  
    		Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
    		if(lelang!=null){
    			List<Jadwal> jadwalList = Jadwal.findByLelang(lelangId);
    	        if(CommonUtil.isEmpty(jadwalList)) {
    	        	jadwalList = Jadwal.getJadwalkosong(lelangId, lelang.getMetode(), lelang.getPemilihan().isLelangExpress());
    	        }
    	        renderArgs.put("lelang", lelang);
				renderArgs.put("jadwalList", jadwalList);
    		}
    		else if(lelangId!=0){
    			flash.error(Messages.get("flash.tyacbt"));
    		}
    	}    
    	renderTemplate("lelang/jadwal/jadwal-edit-ppe.html");
    }
    
    /**
     * Fungsi {@code tahap} digunakan untuk menampilkan detail tahapan lelang
     * dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
    public static void tahap(Long id) {
    	notFoundIfNull(id);
		renderArgs.put("lelang", Lelang_seleksi.findById(id));
		renderArgs.put("date", newDate()); // tanggal sekarang
		renderArgs.put("list", TahapanLelang.findByLelang(id));
        renderTemplate("/lelang/jadwal/tahap.html");
	}
    
    public static void jadwalLelang(Long id) {
		renderArgs.put("lls", Lelang_seleksi.findById(id));
		renderArgs.put("date", newDate()); // tanggal sekarang
		renderArgs.put("list",TahapanLelang.findByLelang(id));
        renderTemplate("/lelang/jadwal/jadwal-lelang.html");
	}
    
    /**
     * Fungsi {@code tahap} digunakan untuk menampilkan detail history tahapan lelang
     * dalam halaman popup
     * @param id parameter id lelang yang dilihat
     */
    public static void history(Long id) {
		renderArgs.put("historyList", History_jadwal.find("dtj_id=? order by hjd_tanggal_edit desc", id).fetch());
    	renderTemplate("lelang/jadwal/history.html");
    }   
    
   
}
