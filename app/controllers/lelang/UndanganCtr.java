package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.MailQueueDao;
import models.common.Tahap;
import models.common.TahapNow;
import models.lelang.Evaluasi;
import models.lelang.Lelang_seleksi;
import models.lelang.Nilai_evaluasi;
import models.lelang.UndangaPeserta;
import models.secman.Group;
import play.Logger;

/**
 * Controller terkait undangan di tender
 * @author Arief Ardiyansah
 */
public class UndanganCtr extends BasicCtr {

    /**
     * aktivitas pengumuman pemenang hak akses Panitia dan peserta
     *
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void undangan(Long id) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        notFoundIfNull(lelang);
        TahapNow tahapNow = lelang.getTahapNow();
        boolean undanganPra = tahapNow.isUndanganPra();
        boolean adaPemenang = lelang.getPemilihan().isLelangExpress() ? lelang.hasPemenangTerverifikasi(): Nilai_evaluasi.countLulus(lelang.lls_id, (undanganPra ? Evaluasi.JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI : Evaluasi.JenisEvaluasi.EVALUASI_AKHIR)) > 0;
        boolean sudahKirim = undanganPra ? lelang.kirim_pengumuman_pemenang_pra : lelang.kirim_pengumuman_pemenang;
        Tahap tahap = undanganPra ? Tahap.PENGUMUMAN_HASIL_PRA : Tahap.PENGUMUMAN_PEMENANG_AKHIR;
        renderArgs.put("pesertaList", UndangaPeserta.findAll(lelang.lls_id, tahap, lelang.isLelangV3()));
        renderArgs.put("lelang", lelang);
        renderArgs.put("tahap", tahap);
        renderArgs.put("undanganPra", undanganPra);
        renderArgs.put("adaPemenang", adaPemenang);
        renderArgs.put("sudahKirim", sudahKirim);
        renderTemplate("lelang/lelang-undangan.html");
    }



    /**
     * aktivitas pengumuman pemenang hak akses Panitia dan peserta
     *
     * @param id
     */
    @AllowAccess({Group.PANITIA, Group.PP})
    public static void kirimUndangan(Long id) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        TahapNow tahapNow = lelang.getTahapNow();
        if (tahapNow.isUndanganPra() || tahapNow.isPengumuman_Pemenang()) {
            try {
                if (tahapNow.isUndanganPra()) {
                    MailQueueDao.kirimPengumumanPrakualifikasi(lelang);
                    lelang.kirim_pengumuman_pemenang_pra = true;
                    lelang.save();
                    flash.success("Undangan Pemenang Prakualifikasi telah dikirim");
                } else {
                    MailQueueDao.kirimPengumumanPemenang(lelang);
                    lelang.kirim_pengumuman_pemenang = true;
                    lelang.save();
                    flash.success("Informasi Pengumuman Pemenang telah dikirim");
                }
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat Kirim Undangan");
                flash.error("Maaf terdapat kesalahan teknis pengiriman. Mohon Ulangi!");
            }
        } else {
            flash.error("Maaf Waktu Anda Untuk Mengirim Pengumuman telah habis");
        }
        LelangCtr.view(id);
    }

}
