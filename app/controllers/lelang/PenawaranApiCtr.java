package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import utils.PenawaranUtil;

import static models.secman.Group.*;

/**
 * @author HanusaCloud on 8/10/2018
 */
public class PenawaranApiCtr extends BasicCtr {

    @AllowAccess({PANITIA, REKANAN, PPK})
    public static void getInfo(Long id, int jenis) {
        otorisasiDataLelang(id); // check otorisasi data lelang
        renderJSON(PenawaranUtil.toJson(Active_user.current(), id, jenis));
    }

}
