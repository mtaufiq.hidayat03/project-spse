package controllers.lelang;

import controllers.BasicCtr;
import models.agency.DaftarKuantitas;
import models.agency.Pegawai;
import models.agency.Rincian_hps;
import models.common.ConfigurationDao;
import models.common.Metode;
import models.handler.DokPenawaranDownloadHandler;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Router;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Kelas {@code PenawaranCtr} merupakan kelas yang mengontrol proses pemasukan
 * penawaran oleh peserta lelang. Created by IntelliJ IDEA. Date: 07/09/12 Time:
 * 17:50
 *
 * @author I Wayan Wiprayoga W
 */
public class PenawaranCtr extends Controller {
	
	public static final String ROOT_URL_PENAWARAN=BasicCtr.getRequestHost()+BasicCtr.CONTEXT_PATH+"/penawaran";
	
	/**
	 * Dieksekusi saat penyedia drag ke Apendo
	 * @param access_token
	 */	

	public static void index(String access_token) {
		Lelang_token lelang_token  = Lelang_token.find("llt_key=?", access_token).first();
		renderArgs.put("access_token", access_token);
		renderArgs.put("namaLpse", ConfigurationDao.getNamaLpse());
		if(lelang_token != null) {
			Lelang_seleksi lelang = Lelang_seleksi.findById(lelang_token.lls_id);
			renderArgs.put("url", BasicCtr.getRequestHost()+Router.reverse("lelang.PenawaranCtr.upload").url);
			renderArgs.put("lelang", lelang);
			Metode metode= lelang.getMetode();
			if(metode.dokumen.isSatuFile()) {
				renderArgs.put("key", Lelang_key.find("lls_id=? and dok_jenis=?", lelang_token.lls_id, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA).first());
				
			} else {
				renderArgs.put("adminKey", Lelang_key.find("lls_id=? and dok_jenis=?", lelang_token.lls_id, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI).first());
				renderArgs.put("hargaKey", Lelang_key.find("lls_id=? and dok_jenis=?", lelang_token.lls_id, JenisDokPenawaran.PENAWARAN_HARGA).first());
			}	
			if(lelang_token.psr_id != null) {
				renderArgs.put("peserta", Peserta.findById(lelang_token.psr_id));
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("access_token", access_token);
				renderArgs.put("url", Router.reverse("lelang.PenawaranCtr.upload", params).url);
				Dok_lelang dok_lelang = Dok_lelang.findBy(lelang_token.lls_id, JenisDokLelang.DOKUMEN_LELANG);
				Dok_lelang_content dok_lelang_content = Dok_lelang_content.findBy(dok_lelang.dll_id);
				renderArgs.put("checklists", dok_lelang.getSyaratTeknis());
				DaftarKuantitas dkh = dok_lelang_content.dkh;
				if(dkh != null) {
					List<Rincian_hps> rincianList = dkh.items;
					renderArgs.put("rincianList", rincianList);
				}
				renderArgs.put("uploadUrl",BasicCtr.getRequestHost()+Router.reverse("lelang.PenawaranCtr.upload").url);
				renderTemplate("lelang/penawaran/vendor.html");
			}
			else if(lelang_token.peg_id != null) { // pembukaan penawaran harus sudah dalam kondisi tersorting
				renderArgs.put("pegawai", Pegawai.findById(lelang_token.peg_id));
				renderArgs.put("pesertaList", Peserta.findBylelang(lelang_token.lls_id));
				renderArgs.put("url", BasicCtr.getRequestHost()+Router.reverse("lelang.PenawaranCtr.download").url);
				renderTemplate("lelang/penawaran/committee.html");
			}
		}
		else {			
			forbidden(Messages.get("flash.atdum"));
		}
	}
	
	public static void upload(String access_token, File document, String hash, Integer periode) {
		Lelang_token lelang_token  = Lelang_token.find("llt_key=?", access_token).first();
		Logger.debug("Uploading file penawaran token %s, namafile %s", access_token, document.getName());
		Map<String, Object> result = new HashMap<String, Object>(1);		
		if(lelang_token != null && lelang_token.psr_id != null && document != null) {	
//			Peserta peserta= lelang_token.peserta;
			JenisDokPenawaran jenis = JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA;
			if(document.getName().contains("{pricing}"))
				jenis = JenisDokPenawaran.PENAWARAN_HARGA;
			else if(document.getName().contains("{technical}"))
				jenis = JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI;			
			try {
//				Dok_penawaran.uploadPenawaran(peserta, jenis, dokumen); // ini akan digunakan jika sudah ada ketentuan pengecekan header file
				Dok_penawaran.simpanPenawaran(lelang_token.psr_id, jenis, document, periode);
				result.put("result", "ok"); //perlu pembahasan
			}catch(Exception e) {
				e.printStackTrace();
				Logger.error("Kesalahan simpan dok_penawaran %s", e.getLocalizedMessage());
			}				
		}
		else {	// forbidden	
			result.put("result", Messages.get("flash.atdum"));
		}
		renderJSON(result);
	}
	
	public static void download(String access_token, Long id) {
		Lelang_token lelang_token  = Lelang_token.find("llt_key=?", access_token).first();		
		if(lelang_token != null && lelang_token.peg_id != null) {
			BlobTable blob = BlobTableDao.getLastById(id);
			Logger.debug("downloading file penawaran token %s, namafile %s", access_token, blob.getFileName());
			String downloadUrl = blob.getDownloadUrl(DokPenawaranDownloadHandler.class);
			redirect(downloadUrl);
		}
		else {			
			forbidden(Messages.get("flash.atdum"));
		}
	}
	
	/**
	 * fungsi urutkan harga penawaran peserta lelang setelah pembukaan
	 * @param access_token
	 * @param id
	 * @param dkh
	 */
	public static void simpan_rincian(String access_token, Long id, DaftarKuantitas dkh) {
		Logger.debug("simpan rincian penawaran token %s", access_token);
		Lelang_token lelang_token  = Lelang_token.find("llt_key=?", access_token).first();		
		if(lelang_token != null && lelang_token.peg_id != null) {
			Dok_penawaran dok_penawaran = Dok_penawaran.findById(id);
			Peserta peserta = Peserta.findById(dok_penawaran.psr_id);
			peserta.psr_dkh = CommonUtil.toJson(dkh);
			peserta.psr_harga = dkh.total;
			peserta.psr_harga_terkoreksi = dkh.total;
			peserta.save();
		}
		else {			
			forbidden(Messages.get("flash.atdum"));
		}
	}
	
	/**
	 * fungsi urutkan harga penawaran peserta lelang setelah pembukaan
	 * @param access_token
	 */
	public static void sorting(String access_token) {
		Logger.debug("sorting harga penawaran token %s", access_token);
		Lelang_token lelang_token  = Lelang_token.find("llt_key=?", access_token).first();		
		if(lelang_token != null && lelang_token.peg_id != null) {
			
		}
		else {			
			forbidden(Messages.get("flash.atdum"));
		}
	}
}
