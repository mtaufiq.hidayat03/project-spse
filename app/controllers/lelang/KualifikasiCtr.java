package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.common.TahapNow;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.rekanan.*;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Rekanan mengirimkan kualifikasi
 * TODO : perlu diperhatikan otorisasi data yang disimpan terhadap waktu pelaksanaan lelang
 * rekanan hanya boleh mengirimkan pada waktu yang sudah ditentukan
 *
 * @author arief ardiyansah
 */
public class KualifikasiCtr extends BasicCtr {

    /**
     * Render halaman pemasukan data kualifikasi Susulan
     *
     * @param id id lelang
     */
    @AllowAccess({Group.REKANAN, Group.PANITIA})
    public static void susulan(Long id) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
        renderArgs.put("peserta", peserta);
        Active_user active_user = Active_user.current();
        renderArgs.put("lelang", Lelang_seleksi.findById(peserta.lls_id));
        Dok_penawaran dok_penawaran = peserta.getDokSusulan();
        renderArgs.put("allow_download", active_user.isAuditor() || active_user.isPanitia() || (peserta.rkn_id == active_user.rekananId));
        if (dok_penawaran != null) {
            renderArgs.put("list", dok_penawaran.getDokumenList());
        }
        renderTemplate("lelang/kualifikasi/kualifikasi-susulan-rekanan.html");
    }

    public static void kirimSusulan(Long id) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta);
        renderArgs.put("peserta", peserta);
        Active_user active_user = Active_user.current();
        renderArgs.put("lelang", Lelang_seleksi.findById(peserta.lls_id));
        renderArgs.put("allow_download", active_user.isAuditor() || active_user.isPanitia() || (peserta.rkn_id == active_user.rekananId));
        Dok_penawaran dok_penawaran = peserta.getDokSusulan();
        boolean editable = peserta.rkn_id == active_user.rekananId;
        if (dok_penawaran != null)
            renderArgs.put("list", dok_penawaran.getDokumenList());
        renderArgs.put("editable", editable);
        renderTemplate("lelang/kualifikasi/kualifikasi-kirim-susulan.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void submitSusulan(Long id, File file) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta);
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            List<UploadInfo> files = new ArrayList<UploadInfo>();
            files.add(Dok_penawaran.simpanSusulan(peserta, file, newDate()));
            result.put("files", files);
        } catch (Exception e) {
            Logger.error(e, "Kesalahan saat upload susulan");
        }
        renderJSON(result);
    }

    /**
     * Render halaman pengiriman data kualifikasi yang telah dimasukkan
     *
     * @param id id lelang
     */
    @AllowAccess({Group.REKANAN})
    public static void kirim(Long id) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelangPeserta(peserta);
    		Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        TahapNow tahapAktif = lelang.getTahapNow();
    		boolean allow_kualifikasi = lelang.isPrakualifikasi() ? tahapAktif.isPemasukanDokPra() : tahapAktif.isPemasukanPenawaran();
        if (!allow_kualifikasi) forbidden(Messages.get("not-authorized")); 
        renderArgs.put("lelang",lelang);
        renderArgs.put("peserta", peserta);
        renderArgs.put("allow_kualifikasi", allow_kualifikasi);
        renderArgs.put("dukunganBank", Dukungan_bank.find("psr_id = ?", peserta.psr_id).first());
        renderArgs.put("dok_lain",Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN));
        renderArgs.put("dok_lain_draft",Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT));
        renderTemplate("lelang/kualifikasi/kualifikasi-kirim.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void simpan(Long id, Long[] ijin, Long[] pajak, Dukungan_bank dukunganBank, File fileDukungan,
                              Long[] staf, Long[] pengalaman, Long[] alat) throws Exception {
        checkAuthenticity();
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta);
        Rekanan rekanan = Rekanan.findById(peserta.rkn_id);
        if (!(rekanan.getBentuk_usaha().iskonsultanIndividu() || !rekanan.getBentuk_usaha().isPerorangan()) && ijin == null)
            flash.error(Messages.get("flash.diuhayd"));
        else {
            String join = StringUtils.join(ijin, ",");
            String joinPajak = StringUtils.join(pajak, ",");
            List<Ijin_usaha> ijinList = Ijin_usaha.find("ius_id in (" + join + ')').fetch();
            List<Pajak> pajakList = Pajak.find("pjk_id in (" + joinPajak + ')').fetch();
            Ijin_usaha_peserta.simpanIjinUsahaPeserta(ijinList, peserta.psr_id);
            Bukti_pajak.simpanPajakPeserta(pajakList, peserta.psr_id);
            if (fileDukungan != null || dukunganBank.dkb_id != null)
                Dukungan_bank.simpanDukunganBankPeserta(dukunganBank, peserta.psr_id, fileDukungan);
            Landasan_hukum_peserta.simpanAkta(peserta.psr_id, peserta.rkn_id);
            String joinStaf = StringUtils.join(staf, ",");
            List<Staf_ahli> staf_ahliList = Staf_ahli.find("sta_id in (" + joinStaf + ')').fetch();
            Staf_ahli_psr.simpanStafAhliPeserta(peserta.psr_id, staf_ahliList);
            String joinPengalaman = StringUtils.join(pengalaman, ",");
            List<Pengalaman> pengalamanList = Pengalaman.find("pgn_id in (" + joinPengalaman + ')').fetch();
            Pengalaman_peserta.simpanPengalamanPeserta(pengalamanList, peserta.psr_id);
            String joinAlat = StringUtils.join(alat, ",");
            List<Peralatan> peralatanList = Peralatan.find("alt_id in (" + joinAlat + ')').fetch();
            Peralatan_peserta.simpanPeralatanPeserta(peralatanList, peserta.psr_id);
            Dok_penawaran.simpanKualifikasi(peserta);
            Dok_penawaran.simpanKualifikasiLainnya(peserta);
            flash.success(Messages.get("flash.data_ktt"));
        }
        LelangCtr.view(peserta.lls_id);
    }

    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR, Group.PPK})
    public static void preview(Long id) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelangPeserta(peserta);
        renderArgs.put("peserta", peserta);
        if (!Active_user.current().isRekanan()) {
            renderArgs.put("rekanan", CommonUtil.fromJson(peserta.psr_identitas, Rekanan.class));
            renderArgs.put("listPengurus", CommonUtil.fromJson(peserta.psr_pengurus, Pengurus[].class));
            renderArgs.put("listPemilik", CommonUtil.fromJson(peserta.psr_pemilik, Pemilik[].class));
        }
        renderArgs.put("listIjinUsaha", Ijin_usaha_peserta.find("psr_id=?", id).fetch());
        renderArgs.put("dukunganBank", Dukungan_bank.find("psr_id=?", id).first());
        renderArgs.put("aktaList", Landasan_hukum_peserta.findBy(id));
        renderArgs.put("dok_lain", Dok_penawaran.findPenawaranPeserta(id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN));
        renderArgs.put("listPajak", Bukti_pajak.find("psr_id=?", id).fetch());
        renderArgs.put("listStafAhli", Staf_ahli_psr.find("psr_id=?", id).fetch());
        renderArgs.put("listPengalaman", Pengalaman_peserta.find("psr_id=?", id).fetch());
        renderArgs.put("listPeralatan", Peralatan_peserta.find("psr_id=?", id).fetch());
        renderArgs.put("allowDownloadDok", (!Active_user.current().isRekanan()));
        renderArgs.put("pesertaId", id);
        renderTemplate("lelang/kualifikasi/kualifikasi-all.html");
    }


    @AllowAccess({Group.REKANAN})
    public static void uploadPersyaratanLain(Long id, File file) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta);
        Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            BlobTable blob = null;
            if (dok_penawaran != null && dok_penawaran.dok_id_attachment != null) {
                blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file, dok_penawaran.dok_id_attachment);
            } else {
                blob = BlobTableDao.saveFile(ARCHIEVE_MODE.ARCHIEVE, file);
            }
            Dok_penawaran.simpanKualifikasiLainnyaDraft(peserta.psr_id, blob, newDate());
            List<UploadInfo> files = new ArrayList<UploadInfo>();
            UploadInfo info = UploadInfo.findBy(blob);
            files.add(info);
            result.put("files", files);
        } catch (Exception e) {
            Logger.error("Kesalahan saat upload dokumen persyaratan lainnya: detail %s", e.getMessage());
            e.printStackTrace();
        }
        renderJSON(result);
    }

    @AllowAccess({Group.REKANAN})
    public static void hapusKualifikasiLainnyaDraft(Long id, Integer versi) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta);
        Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN_DRAFT);
        if (dok_penawaran == null)
            return;
        BlobTable blob = BlobTable.findById(dok_penawaran.dok_id_attachment, versi);
        if (blob != null)
            blob.delete();
    }
    
    @AllowAccess({Group.REKANAN})
    public static void hapusKualifikasiLainnya(Long id, Integer versi) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataPeserta(peserta);
        Dok_penawaran dok_penawaran = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI_TAMBAHAN);
        if (dok_penawaran == null)
            return;
        BlobTable blob = BlobTable.findById(dok_penawaran.dok_id_attachment, versi);
        if (blob != null)
            blob.delete();
    }

    // staf ahli peserta
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR})
    public static void staf_ahli(Long id, Long pesertaId) {
        Peserta peserta = Peserta.findById(pesertaId);
        otorisasiDataLelang(peserta.lls_id);
        renderArgs.put("staf_ahli_psr", Staf_ahli_psr.find("stp_id=? and psr_id=?", id, pesertaId).first());
        renderTemplate("lelang/kualifikasi/kualifikasi-view-ahli.html");
    }

    // pengalaman peserta
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR})
    public static void pengalaman(Long id, Long pesertaId) {
        Peserta peserta = Peserta.findById(pesertaId);
        otorisasiDataLelang(peserta.lls_id);
        renderArgs.put("psr_pengalaman",Pengalaman_peserta.find("pen_id=? AND psr_id=?", id, pesertaId).first());
        renderTemplate("lelang/kualifikasi/kualifikasi-view-pengalaman.html");
    }

    // izin usaha
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR})
    public static void ijin_usaha(Long id, Long pesertaId) {
        Peserta peserta = Peserta.findById(pesertaId);
        otorisasiDataLelang(peserta.lls_id);
        renderArgs.put("psr_ijin_usaha", Ijin_usaha_peserta.find("ius_id=? AND psr_id=?", id, pesertaId).first());
        renderArgs.put("allowDownloadDok", (!Active_user.current().isRekanan()));
        renderTemplate("lelang/kualifikasi/kualifikasi-view-ijin-usaha.html");
    }

    // peralatan
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR})
    public static void peralatan(Long id, Long pesertaId) {
        Peserta peserta = Peserta.findById(pesertaId);
        otorisasiDataLelang(peserta.lls_id);
        renderArgs.put("psr_peralatan",Peralatan_peserta.find("id_alat_peserta=? AND psr_id=?", id, pesertaId).first());
        renderTemplate("lelang/kualifikasi/kualifikasi-view-peralatan.html");
    }

    // bukti pajak
    @AllowAccess({Group.REKANAN, Group.PANITIA, Group.AUDITOR})
    public static void bukti_pajak(Long id, Long pesertaId) {
        Peserta peserta = Peserta.findById(pesertaId);
        otorisasiDataLelang(peserta.lls_id);
        renderArgs.put("psr_bukti_pajak", Bukti_pajak.find("pjk_id=? AND psr_id=?", id, pesertaId).first());
        renderArgs.put("allowDownloadDok", (!Active_user.current().isRekanan()));
        renderTemplate("lelang/kualifikasi/kualifikasi-view-bukti-pajak.html");
    }

}
