package controllers.lelang;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.PdfType;
import models.agency.Sppbj;
import models.common.Instansi;
import models.common.Tahap;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.lelang.Berita_acara;
import models.lelang.Diskusi_lelang;
import models.lelang.Evaluasi;
import models.lelang.Lelang_seleksi;
import models.lelang.cast.NilaiEvaluasiView;
import models.lelang.cast.PesertaView;
import models.secman.Group;
import play.Logger;
import play.cache.CacheFor;
import play.data.binding.As;
import play.data.validation.Valid;
import play.i18n.Messages;
import play.mvc.Router;

import java.io.File;
import java.io.InputStream;
import java.util.*;
/**
 * controller untuk aktifitas-aktifitas terkait berita acara 
 * aktifitas tersebut diantaranya Upload dan download BA
 * @author lkpp
 *
 */
public class BeritaAcaraCtr extends BasicCtr {
	
	@AllowAccess({Group.PANITIA})
	public static void uploadTambahan(Long id, File file) {		
		otorisasiDataLelang(id); // check otorisasi data lelang
		Map<String, Object> result = new HashMap<>(1);
		try {
			UploadInfo info = Berita_acara.simpan(id, Tahap.UPLOAD_BA_TAMBAHAN, file);
			List<UploadInfo> files = new ArrayList<>();
			files.add(info);
			result.put("success", true);
			result.put("files", files);
		}catch(Exception e) {
			Logger.error(e,"Kesalahan saat upload informasi lainnya");
			result.put("result", Messages.get("flash.ksuil"));
			result.put("success", false);
		}	
		renderJSON(result);
	}
	
	@AllowAccess({Group.PANITIA})
	public static void cetak(Long id, Tahap jenis, String no, @As(binder= DatetimeBinder.class) Date tanggal, String info, String tempat) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang
        Berita_acara berita_acara = Berita_acara.findBy(id, jenis);
        if(berita_acara == null) {
            berita_acara = new Berita_acara();
            berita_acara.lls_id = id;
            berita_acara.brc_jenis_ba = jenis;
        }
        berita_acara.brt_no = no;
        berita_acara.brt_tgl_evaluasi = tanggal;
        berita_acara.brt_info = info;
		if (tempat != null && !tempat.equalsIgnoreCase("")){
			berita_acara.brt_tempat = tempat;
		}
        try {
        		InputStream is = Berita_acara.cetak(jenis, id, berita_acara);
            if(is != null) {
            		renderBinary(is, berita_acara.getJudul() + '-' + id + ".pdf");
    				flash.success(berita_acara.getJudul() + " berhasil dicetak");
				Logger.info(berita_acara.getJudul() + " berhasil dicetak");
			}
        }catch (Exception e){
            Logger.error(e,"@lelang.BeritaAcaraCtr.cetak : %s", e.getMessage());
            flash.error(berita_acara.getJudul() + Messages.get("flash.gdc_mu"));
        }
	}

	@AllowAccess({Group.PANITIA})
	@CacheFor("15s")
	public static void preview(Long id, Tahap jenis, String no, @As(binder= DatetimeBinder.class) Date tanggal, String info){
		Berita_acara berita_acara = new Berita_acara();
		berita_acara.brt_no = no;
		berita_acara.brt_tgl_evaluasi = tanggal;
		berita_acara.brt_info = info;
		berita_acara.brc_jenis_ba = jenis;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		berita_acara.lls_id = lelang.lls_id;
		renderArgs.put("jumlahPeserta", lelang.getJumlahPeserta());
		renderArgs.put("jumlahPenawar", lelang.getJumlahPenawar());
		renderArgs.put("isPreview", true);
		renderArgs.put("pnt_nama", lelang.getPaket().getPanitia().pnt_nama);
		renderArgs.put("berita_acara", berita_acara);
		renderArgs.put("lelang", lelang);
		renderArgs.put("instansi", Instansi.findNamaInstansiPaket(lelang.pkt_id));
		renderArgs.put("sppbj", Sppbj.findByLelang(lelang.lls_id));
		if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran()) {
			renderArgs.put("pesertaList", PesertaView.findByLelang(lelang.lls_id));
		}
		if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran() || berita_acara.isBAPenawaranTeknis()) {
			Evaluasi administrasi = Evaluasi.findAdministrasi(lelang.lls_id);
			if(administrasi != null)
				renderArgs.put("pesertaAdm", administrasi.findAllNilaiEvaluasi());
			Evaluasi teknis = Evaluasi.findTeknis(lelang.lls_id);
			if(teknis != null)
				renderArgs.put("pesertaTeknis", teknis.findAllNilaiEvaluasi());
		}
		if (berita_acara.isBAHasil() || berita_acara.isBAPenawaran() || berita_acara.isBAPenawaranHarga()) {
			Evaluasi harga = Evaluasi.findHarga(lelang.lls_id);
			if(harga != null) {
				renderArgs.put("pesertaHarga", harga.findAllNilaiEvaluasi());
				renderArgs.put("pesertaHargaSorted", harga.findAllNilaiEvaluasiWithUrutHarga());
			}
			if (berita_acara.isBAHasil()) {
				renderArgs.put("hasil_lelang", true);
			} else
				renderArgs.put("hasil_lelang", false);
		}
		if (berita_acara.isBAHasil()) {
			Evaluasi kualifikasi = Evaluasi.findKualifikasi(lelang.lls_id);
			if(kualifikasi != null)
				renderArgs.put("pesertaKualifikasi", kualifikasi.findAllNilaiEvaluasi());
			Evaluasi pembuktian = Evaluasi.findPembuktian(lelang.lls_id);
			if(pembuktian != null)
				renderArgs.put("pesertaPembuktian", pembuktian.findAllNilaiEvaluasi());
		}
		if (berita_acara.isBAPenjelasanPra() || berita_acara.isBAPenjelasan()) {
			Tahap tahap = berita_acara.isBAPenjelasanPra() ? Tahap.PENJELASAN_PRA : Tahap.PENJELASAN;
			Diskusi_lelang pembukaan = Diskusi_lelang.findPembukaan(lelang.lls_id, tahap);
			List<Diskusi_lelang> list = Diskusi_lelang.getDiskusiLelangTopik(lelang.lls_id, tahap);
			renderArgs.put("pembukaan", pembukaan);
			renderArgs.put("list", list);
			renderTemplate("lelang/berita_acara/BA-Penjelasan-Template.html");
		} else if (lelang.isPrakualifikasi() && berita_acara.isBAHasil()) {
			renderArgs.put("jumlahShortlist", lelang.getJumlahShortlist());
			renderTemplate("lelang/berita_acara/BAHP-prakualifikasi.html");
		} else {
			renderTemplate("lelang/berita_acara/BA-template.html");
		}
	}

	@AllowAccess({Group.PANITIA})
	@CacheFor("15s")
	public static void previewHasilNegosiasi(Long id, Tahap jenis, String no, @As(binder= DatetimeBinder.class) Date tanggal
			, String info, String tempat){
		Berita_acara berita_acara = new Berita_acara();
		berita_acara.brt_no = no;
		berita_acara.brt_tgl_evaluasi = tanggal;
		berita_acara.brt_info = info;
		berita_acara.brc_jenis_ba = jenis;
		berita_acara.brt_tempat = tempat;
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		berita_acara.lls_id = lelang.lls_id;
		renderArgs.put("pnt_nama", lelang.getPaket().getPanitia().pnt_nama);
		renderArgs.put("isPreview", true);
		renderArgs.put("paket", lelang.getPaket());
		renderArgs.put("mtd_pemilihan", lelang.getPemilihan());
		renderArgs.put("lelang", lelang);
		renderArgs.put("berita_acara", berita_acara);
		renderArgs.put("instansi", Instansi.findNamaInstansiPaket(lelang.pkt_id));
		Evaluasi akhir = Evaluasi.findPenetapanPemenang(lelang.lls_id);
		renderArgs.put("akhir", akhir);
		List<NilaiEvaluasiView> pesertaHargaSorted = new ArrayList<>();
		if (akhir != null) {
			pesertaHargaSorted = akhir.findAllNilaiEvaluasiWithUrutHarga();
		}
		renderArgs.put("pesertaHargaSorted", pesertaHargaSorted);
		renderTemplate("lelang/berita_acara/BA-Negosiasi-Template.html");
	}

	@AllowAccess({ Group.PANITIA })
	public static void hapusBaTambahan(Long id, Integer versi) {
		Berita_acara ba = Berita_acara.find("lls_id=? and brc_jenis_ba=?", id,Tahap.UPLOAD_BA_TAMBAHAN).first();
		if(ba != null && ba.brc_id_attachment != null) {
			BlobTable blob = BlobTable.findById(ba.brc_id_attachment, versi);
			if (blob != null)
				blob.delete();
			if (ba.brc_id_attachment != null && ba.getDokumen().isEmpty()) {
				ba.brc_id_attachment = null;
			}
			ba.save();
		}

	}
	
	@AllowAccess({Group.PANITIA})
    public static void formBa(Long id, Tahap jenis, String aksi) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
        Berita_acara berita_acara = Berita_acara.findBy(id, jenis);
        renderArgs.put("lelang",lelang);
		renderArgs.put("id",id);
		renderArgs.put("berita_acara",berita_acara);
		renderArgs.put("jenis",jenis);
		renderArgs.put("today",newDate());
		String labelDokumen = "";
		renderArgs.put("labelDokumen",labelDokumen);
		Map<String, Object> params=new HashMap();
		params.put("id", id);
		String ref = Router.getFullUrl("lelang.LelangCtr.view",params);
		if(request.headers != null){
			ref = request.headers.get("referer").value();
		}
		renderArgs.put("referer",ref);
		if ("upload".equals(aksi)) {
			renderTemplate("lelang/berita_acara/upload-ba.html");	
		} else if ("cetak".equals(aksi)) {
			renderTemplate("lelang/berita_acara/cetak-ba.html");
		}
    }
    
    @AllowAccess({Group.PANITIA})
	public static void uploadBaSubmit(Long id, @Valid @PdfType File file, String no,
									   @As(binder= DatetimeBinder.class) Date tanggal, 
									   String tempat, String info, String ref, Tahap jenis) {
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		otorisasiDataLelang(lelang); // check otorisasi data lelang
        String namaDokumen = Messages.get("flash.ba");
		if (validation.hasErrors()) {
			flash.error(Messages.get("flash.udyduhd"), namaDokumen);
		} else {
			Berita_acara berita_acara = Berita_acara.findNCreateBy(lelang.lls_id, jenis);
			try {
				berita_acara.brt_no = no;
				berita_acara.brt_tgl_evaluasi = tanggal;
				if (tempat != null && !tempat.equalsIgnoreCase("")) {
					berita_acara.brt_tempat = tempat;	
				}
				berita_acara.brt_info = info;
				berita_acara.simpan(file);
				flash.success(Messages.get("flash.updh"), namaDokumen);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Dokumen Berita Acara");
			}
		}
		redirect(ref);
	}    
}
