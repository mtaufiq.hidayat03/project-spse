package controllers.lelang;

import ams.models.ServiceResult;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Anggota_panitia;
import models.agency.Paket;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.StatusLelang;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Persetujuan.JenisPersetujuan;
import models.lelang.Persetujuan.StatusPersetujuan;
import models.secman.Group;
import models.workflow.instance.ProcessInstance;
import play.Logger;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.libs.URLs;
import play.libs.WS;
import models.sso.common.adp.util.DceSecurityV2;
import play.libs.WS.WSRequest;
import utils.osd.OSDUtil;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class PersetujuanCtr extends BasicCtr {

	public static final String URL_SIKAP_ISCRITERIA_CREATED = BasicCtr.SIKAP_URL + "/services/isCriteriaCreated?q=";
	public static final String URL_SIKAP_KIRIM_UNDANGAN = BasicCtr.SIKAP_URL + "/services/kirimUndangan?q=";

	/**
	 * Persetujuan Pengumuman lelang
	 * @param id
	 * @param setuju
	 * @throws ExecutionException
	 * @throws InterruptedException
	 */
	@AllowAccess({ Group.PANITIA})
	public static void pengumuman(Long id, String alasan, boolean setuju)  {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		Lelang_seleksi lelang = Lelang_seleksi.findById(id);
		Paket paket = lelang.getPaket();

		// cek OSD
		boolean valid = true;
		boolean isOSD = ConfigurationDao.isOSD(lelang.getPaket().pkt_tgl_buat);
		List<Anggota_panitia> anggotaPanitia = null;
		if(isOSD) {
			anggotaPanitia = Anggota_panitia.find("pnt_id=?", lelang.getPaket().pnt_id).fetch();
			List<Jadwal> jadwalList = Jadwal.findByLelang(lelang.lls_id);
			ServiceResult result = OSDUtil.validCertPanitia(anggotaPanitia, lelang, jadwalList);
			if (!result.isSuccess()) {
				flash.error(result.getMessage());
				LelangCtr.edit(id);
			}
			valid = result.isSuccess();
		}

		if(valid) {
			if (!setuju && CommonUtil.isEmpty(alasan)) {
				Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
			}

			if (!lelang.isJadwalSudahTerisi()){
				Validation.addError("jadwal", Messages.get("flash.jadwal_kosong"));
				flash.error(Messages.get("flash.jadwal_tender_masih_kosong"));
			}

			// validate shortlist ke SIKaP jika belum ada approval
			if (lelang.isExpress() && Persetujuan.allowSendMailSikap(lelang.lls_id, JenisPersetujuan.PENGUMUMAN_LELANG) && CommonUtil.isEmpty(alasan)) {
				// jika lelang express(cepat) maka ada pengecekan status undangan ke SIKaP.
				// jika belum terundangan lelang tidak boleh diumumkan
				boolean allowed = true;
				JsonObject jsonObject = new JsonObject();
				jsonObject.addProperty("lls_id", id);
				jsonObject.addProperty("repo_id", ConfigurationDao.getRepoId());
				Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_PENAWARAN);
				if(jadwal != null){
					jsonObject.addProperty("dtj_tglawal",jadwal.dtj_tglawal.toString());
					jsonObject.addProperty("dtj_tglakhir",jadwal.dtj_tglakhir.toString());
				}
				String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
				try  {
					WSRequest request = WS.url(URL_SIKAP_ISCRITERIA_CREATED + param);
					String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
					if (!CommonUtil.isEmpty(content)) {
						allowed = Boolean.parseBoolean(DceSecurityV2.decrypt(content));
					}
				}catch (Exception ex){
					Logger.error(ex,Messages.get("flash.tdptkas"));
					Validation.addError("kriteria", Messages.get("flash.tdptkas"));// don't get confuse, just write a sign to avoid approval process
					flash.error(Messages.get("flash.tdptkas"));
				}
				if (!allowed) {
					Validation.addError("kriteria", Messages.get("flash.ksbd"));
					flash.error(Messages.get("flash.ksbd_mbkstd"));
				}
			}

			if (validation.hasErrors()) {
				validation.keep();
				params.flash();
			}
			else {
				Persetujuan persetujuan = Persetujuan.find("peg_id=? and lls_id=? and pst_jenis=?", Active_user.current().pegawaiId, id, JenisPersetujuan.PENGUMUMAN_LELANG).first();
				persetujuan.pst_status = StatusPersetujuan.SETUJU;
				if (!CommonUtil.isEmpty(alasan)) {
					persetujuan.pst_alasan = alasan;
					// jika ada alasan maka status tidak setuju
					persetujuan.pst_status = StatusPersetujuan.TIDAK_SETUJU;
				}
				if (persetujuan.pst_status.isSetuju()) {
					persetujuan.pst_alasan = null; //clear alasan
				}

				persetujuan.pst_tgl_setuju = newDate();
				persetujuan.save();

				// tambahkan ke riwayat persetujuan
				Riwayat_Persetujuan.simpanRiwayatPersetujuan(persetujuan);

				if (Persetujuan.isApprove(id, JenisPersetujuan.PENGUMUMAN_LELANG)) {
					int sesi = 0;
					if (ConfigurationDao.isLatihan())
						sesi = BasicCtr.getSesiPelatihan().id;
					if(lelang.lls_status.isDraft()) {
						if (!lelang.isExpress()) {
							ProcessInstance proses = WorkflowDao.createProsesInstance(lelang.lls_id, lelang.lls_wf_id, lelang.getMetode());
							lelang.lls_wf_id = proses.process_instance_id;
							lelang.save();
							WorkflowDao.updateState(proses);
							// proses.startInstance();
							proses.status = ProcessInstance.PROCESS_STATUS.RUNNING;
							proses.save();
						} else {
							//lelang cepat kirim undangan ke penyedia
							JsonObject jsonObject = new JsonObject();
							jsonObject.addProperty("lls_id", id);
							jsonObject.addProperty("repo_id", ConfigurationDao.getRepoId());
							String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
							try {
								WSRequest request = WS.url(URL_SIKAP_KIRIM_UNDANGAN + param);
								request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
							} catch (Exception ex) {
								Logger.error(ex, "Gagal mengirim Undangan/tidak dapat terhubung ke aplikasi SIKaP");
							}
							// jika paket 4.3 tender cepat , ada proses reverse auction
//							if(paket.isFlag43()) {
								ReverseAuction.create(lelang, Integer.valueOf(1));
//							}
						}
						lelang.lls_sesi = sesi;
						lelang.lls_status = StatusLelang.AKTIF;
						lelang.lls_tgl_setuju = newDate();
						paket.pkt_status = Paket.StatusPaket.SEDANG_LELANG;
						paket.save();
						lelang.save();
						Lelang_key.generateKey(lelang); // generate kunci lelang
						// buat KPL jika OSD enabled
						if(isOSD) {
							OSDUtil.createKPL(paket, anggotaPanitia, lelang, getRequestHost());
						}
					}
				}

			}
		} else { // sertifikat tidak valid
			Logger.error("Tidak bisa simpan jadwal, pastikan seluruh panitia memiliki sertifikat yang aktif hingga tanggal pembukaan penawaran!");
			flash.error(Messages.get("flash.tbsj_psm"));
		}
		LelangCtr.edit(id);
	}

	/**
	 * Persetujuan Pemenang lelang
	 * @param id
	 * @param alasan
	 * @param setuju
	 */
    @AllowAccess({ Group.PANITIA})
    public static void pemenang(Long id, String alasan, boolean setuju) {
        checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data lelang

		if (!setuju && CommonUtil.isEmpty(alasan)) {
			Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
		}else if (!setuju && !CommonUtil.isEmpty(alasan) && alasan.length() <= 10) {
			Validation.addError("alasan", Messages.get("flash.ahssc"));
		}

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			EvaluasiCtr.persetujuan(id);
		}
        else {
			try {
				simpanPersetujuanPemenang(id, alasan, setuju);
			}catch (Exception e) {
				flash.error(e.getMessage());
			}

			EvaluasiCtr.index(id);
        }

    }

	private static void simpanPersetujuanPemenang(Long id, String alasan, boolean setuju) {
		Persetujuan persetujuan = Persetujuan.findByPegawaiLelangAndJenis(id, JenisPersetujuan.PEMENANG_LELANG);

		if (!setuju && !CommonUtil.isEmpty(alasan)) {
			persetujuan.pst_alasan = alasan;
			persetujuan.pst_status = StatusPersetujuan.TIDAK_SETUJU; //jika ada alasan maka status tidak setuju
		}else if(setuju) {
			persetujuan.pst_alasan = null;
			persetujuan.pst_status = StatusPersetujuan.SETUJU;
		}

		persetujuan.pst_tgl_setuju = BasicCtr.newDate();
		persetujuan.save();

		// tambahkan ke riwayat persetujuan
		Riwayat_Persetujuan.simpanRiwayatPersetujuan(persetujuan);

		if (Persetujuan.isApprove(id, JenisPersetujuan.PEMENANG_LELANG)) {
			Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(id);
			evaluasi.eva_status = Evaluasi.StatusEvaluasi.SELESAI;
			evaluasi.eva_tgl_setuju = newDate();
			evaluasi.save();
			for (Nilai_evaluasi nilai : Nilai_evaluasi.findBy(evaluasi.eva_id)) {
				if (nilai.isLulus()) {
					Peserta peserta = Peserta.findById(nilai.psr_id);
					peserta.is_pemenang = Integer.valueOf(1);
					peserta.save();
				}
			}
		}
	}

	@AllowAccess({ Group.PANITIA})
	public static void pemenangExpress(Long id, String alasan, boolean setuju) {
		checkAuthenticity();
		otorisasiDataLelang(id); // check otorisasi data lelang
		if (!setuju && CommonUtil.isEmpty(alasan)) {
			Validation.addError("alasan", Messages.get("flash.alasan_harus_diisi"));
		}

		if (!setuju && !CommonUtil.isEmpty(alasan) && alasan.length() < 10)
			Validation.addError("alasan", Messages.get("flash.ahssc"));

		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			EvaluasiCtr.persetujuanExpress(id);
		}
		else {
			simpanPersetujuanPemenang(id, alasan, setuju);
			EvaluasiCtr.index(id);
		}

	}

	@AllowAccess({ Group.PANITIA})
	public static void riwayatPersetujuan(Long pst_id) throws InterruptedException, ExecutionException {
		Persetujuan persetujuan = Persetujuan.findById(pst_id);
		otorisasiDataLelang(persetujuan.lls_id);
		renderArgs.put("listRiwayatPersetujuan",Riwayat_Persetujuan.RiwayatPersetujuanLelang.findList(persetujuan.peg_id, persetujuan.lls_id, persetujuan.pst_jenis));
		renderTemplate("lelang/riwayat-persetujuan.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void batalPersetujuanPengumuman(Long lelangId){
    	Active_user user = Active_user.current();
		Persetujuan persetujuan = Persetujuan.findByPegawaiPengumuman(user.pegawaiId, lelangId);
		persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
		persetujuan.pst_tgl_setuju = null;
		persetujuan.save();
		LelangCtr.edit(lelangId);
	}

	@AllowAccess({Group.PANITIA})
	public static void batalPersetujuanPemenang(Long lelangId){
		Persetujuan persetujuan = Persetujuan.findByPegawaiPenetapanAkhir(Active_user.current().pegawaiId, lelangId);
		persetujuan.pst_status = Persetujuan.StatusPersetujuan.BELUM_SETUJU;
		persetujuan.pst_tgl_setuju = null;
		persetujuan.save();
		EvaluasiCtr.index(lelangId);
	}

	@AllowAccess({Group.PANITIA})
	public static void batalPersetujuanTutupTender(Long lelangId){
		Persetujuan persetujuan = Persetujuan.findByPegawaiLelangInJenisBatal(lelangId);
		persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
		persetujuan.pst_tgl_setuju = null;
		persetujuan.save();
		LelangCtr.tutup(lelangId);
	}

	@AllowAccess({Group.PANITIA})
	public static void batalPersetujuanPraGagalTender(Long lelangId){
		Persetujuan persetujuan = Persetujuan.findByPegawaiLelangInJenisBatalPra(lelangId);
		persetujuan.pst_status = StatusPersetujuan.BELUM_SETUJU;
		persetujuan.pst_tgl_setuju = null;
		persetujuan.save();
		LelangCtr.praGagal(lelangId);
	}
}
