package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.DokumenTypeCheck;
import models.common.Active_user;
import models.common.Berita;
import models.common.Sub_tag.JenisSubtag;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;

import java.io.File;
import java.util.List;


public class BeritaCtr extends BasicCtr {

	/**
	 * Fungsi {@code berita} menampilkan halaman daftar berita.
	 *
	 * @param jenis
	 */
	@AllowAccess({Group.ADM_PPE, Group.PANITIA, Group.HELPDESK,Group.PP})
	public static void index(String jenis) {
		renderArgs.put("jenisSubtag",JenisSubtag.valueOf(jenis.toUpperCase()));
		renderTemplate("admin/berita/berita.html");
	}

	/**
	 * Fungsi {@code beritaEdit} menampilkan halaman edit model berita.
	 *
	 * @param id          id berita, jika {@code null} berarti berita baru
	 * @param jenis
	 */
	
	@AllowAccess({Group.ADM_PPE, Group.PANITIA, Group.HELPDESK,Group.PP})
	public static void edit(Long id, String jenis) {
		JenisSubtag jenisSubtag = JenisSubtag.valueOf(jenis.toUpperCase());
		Berita berita = null;
		if (id != null) {
			berita = Berita.findById(id);
			jenisSubtag = JenisSubtag.valueOf(berita.stg_id); // jenis subtag tidak mungkin diubah
		} else {
			berita = new Berita();
			berita.flash();
		}
		renderArgs.put("berita", berita);
		renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
		renderArgs.put("jenisSubtag", jenisSubtag);
		renderTemplate("admin/berita/berita-edit.html");
	}

	/**
	 * Fungsi {@code beritaView} menampilkan halaman detail berita.
	 *
	 * @param id          id berita, jika {@code null} berarti berita baru
	 */
	
	@AllowAccess({Group.ADM_PPE, Group.PANITIA, Group.HELPDESK,Group.PP})
	public static void view(String jenis, String id) {
		if (id != null) {
            //berita = BeritaDao.findBeritaByPegawaiAndID(Long.parseLong(id), session.get(SESSION_USER));
			Berita berita = Berita.findById(Long.parseLong(id));
			JenisSubtag jenisSubtag = JenisSubtag.valueOf(berita.stg_id); // jenis subtag tidak mungkin diubah
			renderArgs.put("berita", berita);
			renderArgs.put("jenisSubtag", jenisSubtag);
            renderTemplate("admin/berita/berita-view.html");
		}
	}

	/**
	 * Fungsi {@code beritaHapusSubmit} untuk menghapus daftar berita yang
	 * di-checklist.
	 *
	 * @param jenis
	 */
	

	@AllowAccess({Group.ADM_PPE, Group.PANITIA, Group.HELPDESK,Group.PP})
	public static void hapus(String jenis, List<Long> chkBerita) {
		checkAuthenticity(); // Setiap menjalankan fungsi ini perlu memeriksa otentifikasi (submit token dari form)
		if(!CommonUtil.isEmpty(chkBerita)) {
			String join = StringUtils.join(chkBerita, ",");
			Berita.delete("brt_id in ("+join+ ')');
		}
		Berita.refreshCache(jenis.toUpperCase());
		if(Active_user.current().isPanitia()) {
			flash.success(Messages.get("flash.bhsldhps"), "Data");
			panitia();
		} else if(Active_user.current().isPP()) {
			flash.success("%s berhasil dihapus", "Data");
			pp();
		} else {
			flash.success(Messages.get("flash.bhsldhps"), "Data");
		    index(jenis); // tampilkan lagi halaman daftar berita setelah menghapus berita
		}
	}

	/**
	 * Fungsi {@code beritaSimpanSubmit} untuk menyimpan model berita hasil
	 * submit dari form edit berita.
	 *
	 * @param berita      objek berita dari form yang telah di-binding
	 * @param jenis
	 */
	
	@AllowAccess({Group.ADM_PPE, Group.PANITIA, Group.HELPDESK, Group.PP})
	public static void simpan(String jenis, Berita berita, @DokumenType File brtAttachment, Long hapus) {
		checkAuthenticity(); // Setiap menjalankan fungsi ini perlu memeriksa otentifikasi
		File file=null;
		JenisSubtag jenisSubtag = JenisSubtag.valueOf(jenis.toUpperCase());
		berita.stg_id = jenisSubtag.toString();
		berita.brt_tanggal = controllers.BasicCtr.newDate();
		if(brtAttachment !=null){
			file=brtAttachment;
		}
		if (berita.brt_isi.equals("<br>")) {
			berita.brt_isi="";
		}
		
		if(jenisSubtag.isMandatoryExpiredDate()){
			if(berita.brt_expires ==null){
				validation.addError("berita.brt_expires", Messages.get("flash.tgl_tayang"));
			}
		}
		validation.valid(berita);
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			berita.paramFlash();
			edit(berita.brt_id, jenisSubtag.toString().toLowerCase());
		} else {
			try {
				Berita.simpanBerita(berita, file, hapus);
				Berita.refreshCache(berita.stg_id);
				flash.success(Messages.get("flash.dbd"));
	            view(jenisSubtag.toString().toLowerCase(), berita.brt_id.toString()); // render halaman daftar berita jika sukses validasi dan simpan data
			}catch (Exception e) {
				Logger.error(e, Messages.get("flash.dgd"));
				flash.error(Messages.get("flash.dgd"));
				edit(berita.brt_id, jenisSubtag.toString().toLowerCase());
			}
		}
	}

    public static String tampilSystemMessage() {
        Berita berita = new Berita();
        berita = Berita.find("stg_id = ?", "SYSTEM_MESSAGE").first();
        return berita.brt_isi;
    }

    /**
     * untuk diakses public
     */
    public static void pengumuman() {
        renderTemplate("admin/berita/publik-pengumuman.html");
    }
    
    /**
     * Fungsi {@code pengumuman} digunakan untuk menampilkan detail pengumuman
     * dalam halaman popup
     * @param id parameter id berita yang akan dilihat
     */
    public static void pengumuman_detil(Long id) {
        Berita berita = Berita.findById(id);
		renderArgs.put("berita", berita);
        if(berita.brt_id_attachment != null) {
			renderArgs.put("blob", BlobTableDao.getLastById(berita.brt_id_attachment));
		}
		renderTemplate("admin/berita/popup-detil-pengumuman.html");
	}
    
    /**
	 * Fungsi {@code berita} menampilkan halaman daftar berita di Panitia
	 *
	 */
	@AllowAccess({Group.PANITIA})
	public static void panitia() {
		renderArgs.put("jenisSubtag", JenisSubtag.LELANG);
		renderTemplate("admin/berita/panitia-berita.html");
	}
	
	/**
	 * Fungsi {@code berita} menampilkan halaman daftar berita di Panitia
	 *
	 */
	@AllowAccess({Group.PP})
	public static void pp() {
		renderArgs.put("jenisSubtag", JenisSubtag.PL);
		renderTemplate("admin/berita/pp-berita.html");
	}
	
	@AllowAccess({Group.PANITIA})
	public static void panitia_edit(Long id){
		Berita berita = new Berita();
		if (id != null){
			berita = Berita.findById(id);
		}else{
			berita.brt_judul = flash.get("berita.brt_judul");
			berita.brt_isi = flash.get("berita.brt_isi");
		}
		renderArgs.put("berita", berita);
		renderTemplate("admin/berita/panitia-berita-edit.html");
	}
	
	@AllowAccess({Group.PP})
	public static void pp_edit(Long id){
		Berita berita = new Berita();
		if(id != null)
			berita = Berita.findById(id);
		renderArgs.put("berita", berita);
		renderTemplate("admin/berita/pp-berita-edit.html");
	}

	@AllowAccess({Group.PANITIA})
	public static void panitia_simpan(Berita berita, @DokumenType File brtAttachment, Long hapus){
		checkAuthenticity();
		berita.peg_id = Active_user.current().pegawaiId;
		berita.stg_id = JenisSubtag.LELANG.toString();
		berita.brt_tanggal = newDate();
		validation.valid(berita);
		if(validation.hasErrors()){
			validation.keep();
			params.flash();
			panitia_edit(berita.brt_id);
		}
		else {
			try {
				File file=brtAttachment != null ? brtAttachment:null;
				Berita.simpanBerita(berita, file, hapus);
				flash.success(Messages.get("flash.berita_bds"));
				panitia();
			}catch (Exception e) {
				Logger.error(e, Messages.get("flash.berita_gds"));
				flash.error(Messages.get("flash.berita_gds"));

			}
		}

	}
	
	@AllowAccess({Group.PP})
	public static void pp_simpan(Berita berita, @DokumenType File brtAttachment, Long hapus){
		checkAuthenticity();
		berita.peg_id = Active_user.current().pegawaiId;
		berita.stg_id = JenisSubtag.PL.toString();
		berita.brt_tanggal = newDate();
		validation.valid(berita);
		if(validation.hasErrors()){
			validation.keep();
			params.flash();
			pp_edit(berita.brt_id);
		}
		else {
			try {
				File file=brtAttachment != null ? brtAttachment:null;
				Berita.simpanBerita(berita, file, hapus);
				flash.success(Messages.get("flash.berita_bds"));
				pp();
			}catch (Exception e) {
				Logger.error(e, "Berita gagal disimpan");
				flash.error(Messages.get("flash.berita_gds"));

			}
		}

	}
}
