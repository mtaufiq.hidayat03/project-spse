package controllers.admin;

import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.PublikCtr;
import controllers.UserCtr;
import ext.AesDecryptBinder;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.UserLoginStep;
import models.secman.Group;
import models.secman.Usergroup;
import models.secman.Usrsession;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Validation;
import play.libs.Codec;
import utils.AesUtil;
import utils.Encrypt;
import utils.JsonUtil;

public class Login2Ctr extends BasicCtr {

    public static void loginAdmin() {
        renderArgs.put("randomID", Codec.UUID());
        AesUtil aesUtil = new AesUtil(256, 1000);
        AesUtil.AesKey aesKey = aesUtil.getKey(session.getId());
        renderArgs.put("token", aesKey.token);
        renderArgs.put("iv", aesKey.iv);
        renderArgs.put("salt", aesKey.salt);
        renderTemplate("admin/login/user.html");
    }
    public static void loginAdminSubmit(@As(binder = AesDecryptBinder.class) String txtUserId) {
        checkAuthenticity();
        validation.required(txtUserId).message("User ID tidak boleh kosong");

        if (validation.hasErrors()) {
            validation.keep();
            Login2Ctr.loginAdmin();
        }

        txtUserId = txtUserId.toUpperCase();
        UserCtr.LoginResult login_result = Pegawai.doLogin(txtUserId, request.remoteAddress); // aksi
        // login
        Group group = Usergroup.findGroupByUser(txtUserId);
        // untuk kompatibilitas SPSE 3.x maka grup ADMIN disamakan ADM_PPE
        if (login_result == UserCtr.LoginResult.SUCCESS && group == Group.ADM_PPE) {
            UserLoginStep step = new UserLoginStep(txtUserId,session.getId());
            String encrypt = Encrypt.encrypt(JsonUtil.gson.toJson(step));
            String ids = session.getId()+UserLoginStep.uniqStr;
            Cache.set(ids,encrypt ,"1mn");
            Login2Ctr.loginPassword();
        } else if (login_result == UserCtr.LoginResult.SUCCESS && group != Group.ADM_PPE) {
            txtUserId="";
            validation.required(txtUserId).message(login_result.INVALID_USER_ID.pesan);
            validation.keep();
            Login2Ctr.loginAdmin();
        } else {
            txtUserId = "";
            validation.required(txtUserId).message(login_result.pesan);
            validation.keep();
            Login2Ctr.loginAdmin();
        }
    }

    public static void loginPassword(){
        String txtUserId = null;
        String ids = session.getId()+UserLoginStep.uniqStr;
        String encrypt = (String) Cache.get(ids);
        UserLoginStep user = JsonUtil.fromJson(Encrypt.decrypt(encrypt),UserLoginStep.class);
        if(user!= null) {
            txtUserId = user.username;
        }else{
            Cache.delete(ids);
            PublikCtr.index();
        }
        if(null != txtUserId){
            renderArgs.put("userid", txtUserId);
            renderArgs.put("uniqid", user.uniqid);
            renderArgs.put("randomID", Codec.UUID());
            AesUtil aesUtil = new AesUtil(256, 1000);
            AesUtil.AesKey aesKey = aesUtil.getKey(session.getId());
            renderArgs.put("token", aesKey.token);
            renderArgs.put("iv", aesKey.iv);
            renderArgs.put("salt", aesKey.salt);
            renderTemplate("admin/login/userpass.html");
        }else{
            validation.required(txtUserId).message("User ID tidak boleh kosong");
            validation.keep();
            Login2Ctr.loginAdmin();
        }
    }

    /**
     * otorisasi khusus untuk admin lpse (admin PPE)
     *
     * @param txtUserId
     * @param txtPassword
     * @param hidRandomID
     * @param txtCode
     */
    public static void login2Admin(@As(binder = AesDecryptBinder.class) String txtUserId, @As(binder = AesDecryptBinder.class) String txtPassword, String hidRandomID, String txtCode, String uniqid) {
        checkAuthenticity();

        //check validity before All
        String ids = session.getId()+UserLoginStep.uniqStr;//idsesi
        String encrypt = (String) Cache.get(ids);
        UserLoginStep user = JsonUtil.fromJson(Encrypt.decrypt(encrypt),UserLoginStep.class);
        if(null != user){
            if(user.uniqid.equals(uniqid) && txtUserId.equals(user.username)){
                txtUserId = user.username;
            }
        }else{
            txtUserId = "";
            txtPassword = "";
            Cache.delete(ids);
            PublikCtr.index();
        }
        validation.required(txtUserId).message("User ID tidak boleh kosong");
        validation.required(txtPassword).message("Password tidak boleh kosong");
        validation.required(txtCode).message("Kode Keamanan wajib diisi");
        Validation.ValidationResult result = null;
        if (!StringUtils.isNotEmpty(txtUserId)) {
            result = validation.equals(txtUserId, "-").key("txtUserId").message("Perhatikan pesan error di bawah !");
        }

        if (!StringUtils.isNotEmpty(txtPassword)) {
            result = validation.equals(txtPassword, "-").key("txtPassword")
                    .message("Perhatikan pesan error di bawah !");
        }

        if (!StringUtils.isNotEmpty(txtCode)) {
            result = validation.equals(txtCode, "-").key("txtCode").message("Perhatikan pesan error di bawah !");
        } else if (hidRandomID != null && StringUtils.isNotEmpty(txtCode)) {
            // WARN: Capctha case sensitive
            result = validation.equals(txtCode, Cache.get(hidRandomID)).key("invalid.code")
                    .message("Kode yang Anda masukkan salah. Silakan masukkan Kode kembali!");
        }

        if (validation.hasErrors()) {
            if(result != null && result.error != null)
                flash.error(result.error.toString());
            params.flash();
            validation.keep();
            loginPassword();
        }

        txtUserId = txtUserId.toUpperCase();
        txtPassword = DigestUtils.md5Hex(txtPassword);
        UserCtr.LoginResult login_result = Pegawai.doLogin(txtUserId, txtPassword, request.remoteAddress); // aksi
        // login
        Group group = Usergroup.findGroupByUser(txtUserId);
        // untuk kompatibilitas SPSE 3.x maka grup ADMIN disamakan ADM_PPE
        if (login_result == UserCtr.LoginResult.SUCCESS && group == Group.ADM_PPE) {
//			session.put(SESSION_USERID, txtUserId);
            Usrsession usrsession = Usrsession.setWaktuLogin(txtUserId, request.remoteAddress, false); // rekam data session pengguna
            Active_user active_user = new Active_user();
            active_user.id = usrsession.sessionid;
            active_user.sessionid = session.getId();
            active_user.userid = txtUserId;
            active_user.name = txtUserId;
            active_user.logintime = usrsession.sessiontime.getTime();
            Pegawai pegawai = Pegawai.findBy(txtUserId);
            if (pegawai != null) {
                active_user.pegawaiId = pegawai.peg_id;
                active_user.name = pegawai.peg_nama;
            }
            active_user.remoteaddress = request.remoteAddress;
            active_user.group = Group.ADM_PPE;
            active_user.save();
            Cache.delete(ids);//clear cache auth username
            BerandaCtr.index(); // redirect ke halaman beranda Admin PPE
        } else if (login_result == UserCtr.LoginResult.SUCCESS && group != Group.ADM_PPE) {
            //bukan admin ppe
            Cache.delete(ids);
            flash.error(login_result.INVALID_USER_PASSWORD.pesan);
            validation.keep();
            params.flash();
            loginPassword();
        } else {
            //max password wrong 3x
            //Logger.info("salah password:"+user.wrongPassword);
            user.wrongPassword = user.wrongPassword+1;
            if(user.wrongPassword < user.maxWrongPass){
                encrypt = Encrypt.encrypt(JsonUtil.gson.toJson(user));
                Cache.set(ids,encrypt ,"1mn");
            }else{
                Cache.delete(ids);
            }
            flash.error(login_result.pesan);
            validation.keep();
            params.flash();
            loginPassword();
        }
    }
}
