package controllers.admin;

import businesslogic.ServicePackAcceptance;
import businesslogic.ServicePackChangelog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.devpartner.PaketDpCtr;
import controllers.lelang.PaketCtr;
import controllers.nonlelang.PaketPlCtr;
import controllers.security.AllowAccess;
import controllers.swakelola.PaketSwakelolaCtr;
import ext.DatetimeBinder;
import jaim.agent.JaimConfig;
import jaim.common.model.ChangelogItem;
import jaim.common.model.ErrorMessage;
import jaim.common.model.ResponseCheck;
import jaim.common.model.UpdateSchedule;
import models.agency.Satker_rup;
import models.agency.Satuan_kerja;
import models.common.*;
import models.jcommon.config.Configuration;
import models.jcommon.mail.MailQueue;
import models.jcommon.secure.encrypt.CipherEngineException;
import models.jcommon.secure.encrypt2.CachedKeyCipherEngine;
import models.jcommon.secure.encrypt2.CachedRSAKey;
import models.jcommon.secure.encrypt2.CipherEngine2;
import models.jcommon.secure.encrypt2.EncryptCipherEngine;
import models.lelang.SummaryLelang;
import models.nonlelang.SummaryNonSpk;
import models.nonlelang.SummaryPl;
import models.nonlelang.SummarySwakelola;
import models.secman.Group;
import models.sirup.PaketSirup;
import models.sirup.PaketSwakelolaSirup;
import models.sso.common.Propinsi;
import org.apache.commons.codec.binary.Base64;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Required;
import play.i18n.Messages;
import play.libs.Json;
import play.libs.MimeTypes;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import play.mvc.Http;
import play.mvc.Router;
import utils.JsonUtil;
import utils.PasswordGenerator;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Kelas {@code UtilityCtr} digunakan untuk mengatur halaman fitur-fitur sistem yang bersifat administrasi sistem lainnya
 * dan tidak behubungan langsung dengan LPSE. Contohnya seperti manajemen email, manajemen konten multimedia, dll.
 *
 * @author I Wayan Wiprayoga W
 */
public class UtilityCtr extends BasicCtr {

//	private static final String URL_API_PAKET_SIRUP = SIRUP_URL+"/service/paketPenyediaPerSatkerTampil"; // API Sirup 2.2

	/**
	 * Fungsi {@code konfigurasiAll} digunakan untuk menampilkan halaman edit konfigurasi sistem lainnya
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void konfigurasiAll() {
		//boolean role_ppk = Boolean.valueOf(Configuration.getConfigurationValue(Configuration.CONFIG, "ppk.enabled"));
		renderArgs.put("fitur_itemize", Configuration.getBoolean(CONFIG.ITEMIZED_ENABLED.toString()));
		//boolean paket_lintas_satker = Boolean.valueOf(Configuration.getConfigurationValue(Configuration.CONFIG, "paket.lintas-satker"));
		renderArgs.put("non_eproc", Configuration.getBoolean(CONFIG.NON_EPROC_ENABLED.toString()));
		renderTemplate("admin/utility/konfigurasiAll.html");

	}

	/**
	 * Fungsi {@code setKonfigurasi} digunakan untuk set nilai konfigurasi sistem yang ada di database.
	 *
	 * @param config sub kategori konfigurasi
	 * @param value nilai yang di-set
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void setKonfigurasi(CONFIG config, String value) {
		Configuration.updateConfigurationValue(config.toString(), value);
		konfigurasiAll();
	}

	/**
	 * Fungsi {@code viewAccessLog} digunakan untuk menampilkan halaman log akses seluruh pengguna LPSE
	 */
	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void accessLog() {
		renderTemplate("admin/utility/accessLog.html");
	}

	@AllowAccess({Group.ADM_PPE})
	public static void summaryLelang() {
		renderTemplate("admin/utility/summaryLelang.html");
	}

	@AllowAccess({Group.ADM_PPE})
	public static void summaryNonTender() {
		renderTemplate("admin/utility/summaryNonTender.html");
	}

	@AllowAccess ({Group.ADM_PPE,Group.AUDITOR,Group.PANITIA, Group.PPK})
	public static void viewPDF(Long id) {
		String namafile = "Summary-Report-"+id+".pdf";
		response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
		Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
		response.headers.put("Content-Disposition", header);
		try {
			renderBinary(SummaryLelang.cetak(id));
		} catch (Exception e) {
			Logger.error(e, "Kesalahan cetak Summary report kode lelang %s", id);
			forbidden(Messages.get("flash.tdp_kendala_ctk_summary"));
		}
	}

	@AllowAccess ({Group.ADM_PPE,Group.AUDITOR, Group.PPK, Group.PP, Group.PANITIA})
	public static void viewPDFPl(Long id) {
		String namafile = "Summary-Report-"+id+".pdf";
		response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
		Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
		response.headers.put("Content-Disposition", header);
		try {
			renderBinary(SummaryPl.cetak(id));
		} catch (Exception e) {
			Logger.error(e, "Kesalahan cetak Summary report kode non tender %s", id);
			forbidden(Messages.get("flash.ctk_kdl_non_tender"));
		}
	}

	@AllowAccess ({Group.ADM_PPE,Group.AUDITOR, Group.PPK, Group.PP})
	public static void viewPDFSwakelola(Long id) {
		String namafile = "Summary-Report-"+id+".pdf";
		response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
		Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
		response.headers.put("Content-Disposition", header);
		try {
			renderBinary(SummarySwakelola.cetak(id));
		} catch (Exception e) {
			Logger.error(e, "Kesalahan cetak Summary report kode lelang %s", id);
			forbidden(Messages.get("flash.tdp_kendala_ctk_summary"));
		}
	}

	@AllowAccess ({Group.ADM_PPE,Group.AUDITOR, Group.PPK, Group.PP})
	public static void viewPDFNonSpk(Long id) {
		String namafile = "Summary-Report-"+id+".pdf";
		response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
		Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
		response.headers.put("Content-Disposition", header);
		try {
			renderBinary(SummaryNonSpk.cetak(id));
		} catch (Exception e) {
			Logger.error(e, "Kesalahan cetak Summary report kode lelang %s", id);
			forbidden(Messages.get("flash.tdp_kendala_ctk_summary"));
		}
	}

	@AllowAccess ({Group.ADM_PPE,Group.AUDITOR, Group.PPK, Group.PP, Group.PANITIA, Group.REKANAN})
	public static void viewUdanganPdf(Long id, Long rkn_id, JenisEmail jenis) {

		String namafile = jenis.name()+ '_' +id+ '_' +rkn_id+".pdf";

		response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
		Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
		response.headers.put("Content-Disposition", header);
		try {
			MailQueue mailQueue = MailQueueDao.getByJenisAndLelangAndRekanan(id, rkn_id, jenis);
			renderBinary(MailQueueDao.cetak(mailQueue));
		} catch (Exception e) {
			Logger.error(e, "Kesalahan cetak Udangan kode lelang/PL %s", id);
			forbidden(Messages.get("flash.tdp_kendala_cetak_undangan"));
		}
	}

	@AllowAccess ({Group.ADM_PPE,Group.AUDITOR, Group.PPK, Group.PP, Group.PANITIA, Group.REKANAN})
	public static void viewUdanganPdfPl(Long id, Long rkn_id, JenisEmail jenis) {

		String namafile = jenis.name()+ '_' +id+ '_' +rkn_id+".pdf";

		response.contentType= MimeTypes.getContentType(namafile, "application/octet-stream");
		Http.Header header=new Http.Header("Content-Disposition", "attachment; filename="+namafile);
		response.headers.put("Content-Disposition", header);
		try {
			MailQueue mailQueue = MailQueueDao.getByJenisAndLelangAndRekanan(id, rkn_id, jenis);
			renderBinary(MailQueueDao.cetak(mailQueue));
		} catch (Exception e) {
			Logger.error(e, "Kesalahan cetak Udangan kode non tender %s", id);
			forbidden(Messages.get("flash.tdp_kendala_cetak_undangan"));
		}
	}

	@AllowAccess ({Group.ADM_PPE})
	public static void sesiPelatihan()	{
		/* sistem menyiapkan ada 20 sesi Pelatihan yang bisa digunakan
		 * Admin PPE bisa mengeset aktif
		 */
		renderArgs.put("list", SesiPelatihan.findAll());
		renderArgs.put("simpanUrl", Router.reverse("admin.UtilityCtr.sesiPelatihanSubmit").url);
		renderTemplate("admin/utility/sesiPelatihan.html");
	}

	@AllowAccess ({Group.ADM_PPE})
	public static void sesiPelatihanSubmit(SesiPelatihan sesi) throws IOException
	{
		SesiPelatihan.save(sesi);
		sesiPelatihan();
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK})
	public static void datamaster() {
		renderArgs.put("propinsiList", Propinsi_kabupaten.findAll());
		renderArgs.put("instansiList", JenisInstansi.findAll());
		renderTemplate("admin/utility/datamaster.html");
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK})
	public static void update_datamaster() {
		try {
			Logger.debug("Get data Provinsi dan Kabupaten/Kota");
			Propinsi.updateFromSirup();
			Logger.debug("Get data Instansi");
			Instansi.updateFromSirup();
			flash.success(Messages.get("flash.dm_st"));
		}catch (Exception e) {
			flash.error(Messages.get("flash.dm_gt"));
		}
		datamaster();
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.PP, Group.PPK, Group.KUPPBJ})
	public static void update_instansi(Long paketId) {
		Instansi.updateFromSirup();
		Active_user active_user = Active_user.current();

//		if (active_user.isPP())
//			PaketPlCtr.rencana_nonlelang(null, null, null,null,null);
//		if (active_user.isPpk())
//			PaketPlCtr.rencanaNonSpk(null, null, null,null,null);
		if (active_user.isKuppbj())
			PanitiaCtr.edit(null);

		PaketCtr.rencana(paketId,null, null, null, null);
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.PPK, Group.KUPPBJ})
	public static void update_instansi_nontender(Long paketId) {
		Instansi.updateFromSirup();
		Active_user active_user = Active_user.current();

		if(active_user.isAdminAgency() || active_user.isKuppbj())
			PanitiaCtr.edit(null);
		else
			PaketPlCtr.rencana_nonlelang(paketId, null, null, null, null);
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.PP, Group.PPK, Group.KUPPBJ})
	public static void update_instansi_dp() {
		Instansi.updateFromSirup();
		Active_user active_user = Active_user.current();

//		if (active_user.isPP())
//			PaketPlCtr.rencana_nonlelang(null, null, null,null,null);
//		if (active_user.isPpk())
//			PaketPlCtr.rencanaNonSpk(null, null, null,null,null);
		if (active_user.isKuppbj())
			PanitiaCtr.edit(null);

		PaketDpCtr.rencana(null, null, null, null);
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.PPK, Group.KUPPBJ})
	public static void update_instansi_pencatatan(Long paketId) {
		Instansi.updateFromSirup();
		Active_user active_user = Active_user.current();
		if(active_user.isAdminAgency() || active_user.isKuppbj())
			PanitiaCtr.edit(null);
		else
			PaketPlCtr.rencanaNonSpk(paketId, null, null, null, null);
	}

	// update satker controller dari SIRUP
	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY, Group.PP, Group.PPK, Group.KUPPBJ})
	public static void update_satker(Long paketId, String id, Integer tahun) {
		Satker_rup.updateFromSirup(id, tahun);
		Active_user active_user = Active_user.current();
		if(active_user.isAdminAgency() || active_user.isKuppbj())
			PanitiaCtr.edit(null);
		else
			PaketCtr.rencana(paketId, id, null, tahun, null);
//		if (active_user.isPpk())
//			PaketPlCtr.rencanaNonSpk(paketId, id, null, null,null);
	}

	// update satker controller dari SIRUP
	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY,  Group.PPK, Group.KUPPBJ})
	public static void update_satker_nontender(Long paketId, String id, Integer tahun) {
		Satker_rup.updateFromSirup(id, tahun);
		Active_user active_user = Active_user.current();
		if(active_user.isAdminAgency() || active_user.isKuppbj())
			PanitiaCtr.edit(null);
		else
			PaketPlCtr.rencana_nonlelang(paketId, id, null, tahun, null);
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY,  Group.PPK, Group.KUPPBJ})
	public static void update_satker_pencatatan(Long paketId, String id, Integer tahun) {
		Satker_rup.updateFromSirup(id, tahun);
		Active_user active_user = Active_user.current();
		if(active_user.isAdminAgency() || active_user.isKuppbj())
			PanitiaCtr.edit(null);
		else
			PaketPlCtr.rencanaNonSpk(paketId, id, null, tahun, null);
	}

	@AllowAccess ({Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY, Group.PP, Group.PPK, Group.KUPPBJ})
	public static void update_satker_dp(String id, Integer tahun) {
		Satker_rup.updateFromSirup(id, tahun);
		Active_user active_user = Active_user.current();
		PaketDpCtr.rencana(id, null, tahun, null);
	}

	@AllowAccess({ Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY, Group.PPK})
	public static void update_paket_sirup(Long paketId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketCtr.rencana(paketId, satker.instansi_id, satkerId, tahun, metodePemilihan);
	}

	@AllowAccess({ Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY, Group.PPK})
	public static void edit_update_paket_sirup(Long paketId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketCtr.editRencana(paketId, satker.instansi_id, satkerId, tahun, metodePemilihan);
	}


	@AllowAccess({ Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY, Group.PPK})
	public static void edit_update_paket_pl_sirup(Long paketId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketPlCtr.editRencana(paketId, satker.instansi_id, satkerId, tahun, metodePemilihan);
	}

	@AllowAccess({ Group.ADM_PPE, Group.HELPDESK, Group.PANITIA, Group.ADM_AGENCY, Group.PPK})
	public static void update_paket_sirup_dp(Long satkerId, Integer tahun, Integer metodePemilihan) {
		Logger.debug("get data paket sirup");
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketDpCtr.rencana(satker.instansi_id, satkerId, tahun, metodePemilihan);
	}

	public static void reload_config() {
		ConfigurationDao.setupConfig();
		renderText("konfigurasi reloaded");
	}

	@Deprecated //tidak dipakai, hanya untuk development
	public static void internalServerErrorBeta(String bug_id) throws CipherEngineException
	{
		String publikKey=request.params.get("publicKey");
		CachedRSAKey key=new CachedRSAKey();
		CipherEngine2 ce= new EncryptCipherEngine(publikKey);
		byte[] ary=ce.doCrypto("ABC".getBytes());
		renderBinary(new ByteArrayInputStream(ary));


	}
	/**Mencari detil dari error di logs
	 * @param sessionId
	 * @throws CipherEngineException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 *
	 * Contoh penggunaan bisa dilihat di log.LogFileErrorMessageTest
	 *
	 *Step 1
	 * Server mengirim sessionId serta publicKey dari milik server -> PubServer
	 * SPSE merespon dengan mengirim publicKey dari SPSE (pubSpse) yg di-encrypt dengan pubServer
	 *
	 *Step 2
	 * Request:
	 * a. Server mendapatkan pubSpse dan digunakan untuk melakukan enkripsi terdapat bugId -> encryptedBugId
	 * b. Server mengirimkan encryptedBugId serta PubServer
	 * SPSE Response:
	 * a. SPSE melakukan dekripsi terhadap bugId
	 * b. SPSE mencari bug dari semua file di folder logs -> log_content
	 *    Dilakuan terhadap file terbaru hingga file terlama sampai ditemukan
	 * c. SPSE melakukan enkripsi log_content menggunakan PubServer
	 * d. Server melakukan dekripsi log_content
	 *
	 * Uji coba
	 * Dengan jumlah 50 file log (total 641 MB) memerlukan waktu 1,3 detik.
	 *
	 */
	public static void getLogFileErrorMessageStep1(String sessionId) throws CipherEngineException
	{
		String publikKey=request.params.get("publicKey");
		CachedRSAKey key=new CachedRSAKey();
		CipherEngine2 ce= new EncryptCipherEngine(publikKey); //gunakan publikKey dari Server untuk melakukan enkripsi
		byte[] result=ce.doCrypto(key.getPublicKeyEncoded().getBytes());
		response.setHeader("X-session", key.sessionKey);//tambahkan ini untuk session id
		Logger.debug("[SPSE#1]  spseSessionId: %s -> spsePubkey: %s", key.sessionKey, key.getPublicKeyEncoded());
		renderBinary(new ByteArrayInputStream(result));
	}

	public static void getLogFileErrorMessageStep2(String spseSessionId) throws CipherEngineException
	{
		String bugId=params.get("bugId");
		String publicKeyServer=params.get("publicKey");
		byte[] ary=Base64.decodeBase64(bugId);
		CachedKeyCipherEngine ce=new CachedKeyCipherEngine(Cipher.DECRYPT_MODE, spseSessionId, false);
		bugId=new String(ce.doCrypto(ary));

		Logger.debug("[SPSE]  spseSessionId: %s -> bug_id: %s", spseSessionId, bugId);

		String key="controllers.admin.UtilityCtr.internalServerErrorStep2(String)" + bugId;
		String logMessage=Cache.get(key,String.class);
		if(logMessage==null)
		{
			logMessage=new LogFile().findLogMessage(bugId);
			if(logMessage==null)
				logMessage="NotFound";
			Cache.set(key, logMessage, "24h");//simpan di cache selama 24 jam
		}

		CipherEngine2 ecryptor= new EncryptCipherEngine(publicKeyServer); //gunakan publikKey dari Server untuk melakukan enkripsi
		byte[] result=ecryptor.doCrypto(logMessage.getBytes());

		renderBinary(new ByteArrayInputStream(result));
	}

	@AllowAccess({ Group.ADM_PPE, Group.PP, Group.PANITIA,Group.PPK })
	public static void update_paket_pl_sirup(Long paketId, Long satkerId, Integer tahun, Integer metodePemilihan)  {
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketPlCtr.rencana_nonlelang(paketId, satker.instansi_id, satkerId, tahun, metodePemilihan);
	}

	@AllowAccess({ Group.ADM_PPE, Group.PP, Group.PANITIA,Group.PPK })
	public static void updatePaketNonSpkFromSirup(Long paketId, Long satkerId, Integer tahun, Integer metodePemilihan)  {
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketPlCtr.rencanaNonSpk(paketId, satker.instansi_id, satkerId, tahun, metodePemilihan);
	}

	// update satker controller dari SIRUP
	@AllowAccess ({Group.ADM_PPE,Group.PPK})
	public static void update_instansi_swakelola(Long paketId) {
		Instansi.updateFromSirup();
		Active_user active_user = Active_user.current();
		if (active_user.isPpk())
			PaketSwakelolaCtr.rencana_swakelola(paketId, null, null,null, null);

	}

	// update satker controller dari SIRUP
	@AllowAccess ({Group.ADM_PPE,Group.PPK})
	public static void update_satker_swakelola(Long paketId, String id, Integer tahun) {
		Satker_rup.updateFromSirup(id, tahun);
		Active_user active_user = Active_user.current();
		if(active_user.isAdminAgency())
			PanitiaCtr.edit(null);
		if (active_user.isPpk())
			PaketSwakelolaCtr.rencana_swakelola(paketId, id, null, tahun, null);
	}

	@AllowAccess({ Group.ADM_PPE, Group.PPK })
	public static void update_paket_swk_sirup(Long paketId, final Long satkerId, final Integer tahun, Integer tipeSwakelola) {
		Satuan_kerja satker = Satuan_kerja.findById(satkerId);
		PaketSwakelolaSirup.updatePaketFromSirup(satker.rup_stk_id, tahun);
		PaketSwakelolaCtr.rencana_swakelola(paketId, satker.instansi_id, satkerId, tahun, tipeSwakelola);
	}

	@AllowAccess(Group.ADM_PPE)
	public static void configuration() {
		renderArgs.put("namalpse", ConfigurationDao.getNamaLpse());
		renderTemplate("admin/utility/localConfiguration.html");
	}

	@AllowAccess(Group.ADM_PPE)
	public static void updateConfiguration(String namalpse) {
		checkAuthenticity();
		Configuration.updateConfigurationValue(CONFIG.PPE_NAMA.toString(), namalpse);
		Cache.safeDelete("ext.CustomTag._title()");
		flash.success("data telah tersimpan");
		configuration();
	}

	@AllowAccess(Group.ADM_PPE)
	public static void tanggalUpateLpse(){
		UpdateSchedule updateSchedule = UpdateSchedule.findNew();
		renderArgs.put("us", updateSchedule);
		renderTemplate("admin/utility/tanggalUpdateLpse.html");
	}

	@AllowAccess(Group.ADM_PPE)
	public static void updateTanggalUpdateLpse(Long id, @Required @As(binder= DatetimeBinder.class) Date waktu){
		checkAuthenticity();
		UpdateSchedule updateSchedule = UpdateSchedule.findById(id);
		if(updateSchedule == null){
			flash.error("Data gagal tersimpan, Jadwal update tidak ditemukan.");
			renderArgs.put("us", updateSchedule);
			renderTemplate("admin/utility/tanggalUpdateLpse.html");
		}
		updateSchedule.update_date = waktu;
		updateSchedule.do_backup = params.get("do_backup") != null;
		updateSchedule.save();
		Cache.set(UpdateSchedule.CACHE_KEY, updateSchedule);

		final ChangelogItem[] changeLogs = ServicePackChangelog.getChangeLogs(updateSchedule);
		final Optional<Date> lastDate = ServicePackChangelog.getLastDate(changeLogs);
		final String msg = ServicePackAcceptance.setujuiTolakUpdate(
				new SimpleDateFormat("yyyyMMdd").format(lastDate),
				updateSchedule.id,
				"SETUJU");
		flash.success("Jadwal update LPSE berhasil disimpan.");
		renderArgs.put("us", updateSchedule);
		renderTemplate("admin/utility/tanggalUpdateLpse.html");
	}

	@AllowAccess(Group.ADM_PPE)
	public static void checkUpdate() throws IOException {
		JsonObject result = new JsonObject();
		UpdateSchedule updateSchedule = UpdateSchedule.findNew();
		if (updateSchedule != null) {
			Logger.info("[JaimCtr] checkUpdate - There is pending update. No need to check to JaIM Server");
			flash.success(Messages.get("flash.patch_update"));
			tanggalUpateLpse();
		}

		int lpse_id = Configuration.getInt("ppe.id");
		String lpse_versi = Configuration.getConfigurationValue("ppe.versi");
		String url = String.format("%s/v2/servicePack/%s/check", JaimConfig.jaimServer, lpse_id);
		JaimConfig.baseApp.prepareFilesAttribute();
		String json = Json.toJson(JaimConfig.baseApp.fileAttributes);
		String uuid = Play.configuration.getProperty("spse.uuid");
		try {
			WSRequest req = WS.url(url);
			req.timeout(JaimConfig.JAIM_CONNECTION_TIMEOUT).setParameter("fileAttributes", json)
					.setParameter("lpse_versi", lpse_versi).setParameter("uuid", uuid);
			HttpResponse resp = req.postAsync().get();
			Logger.info("[JaimCtr] checkUpdate - code: " + resp.getStatus() + " message: " + resp.getStatusText());
			if (!resp.success()) {
				Logger.error("[JaimCtr] checkUpdate - %s", resp.getString());
				ErrorMessage errorMessage = JsonUtil.fromJson(resp.getString(), ErrorMessage.class);
				String msg = String.format("Terdapat kendala pada proses cek update ke JAIM SERVER. %s.", errorMessage.message);
				if (errorMessage.errAttributes != null) {
					msg = String.format(msg.concat(" Error Parameter: %s"), String.join(", ", errorMessage.errAttributes));
				}
				flash.error(msg);
				tanggalUpateLpse();
			}
			Cache.set(JaimConfig.JAIM_CONNECTION_FAILED, false);
			final String responseJson = resp.getString();
			Logger.info("[JaimCtr] checkUpdate - response: " + responseJson);
			ResponseCheck responseCheck = new Gson().fromJson(responseJson, ResponseCheck.class);
			if (!responseCheck.needUpdate) {
				Logger.info("[JaimCtr] checkUpdate - Application already up to date!");
				flash.success("SPSE sudah up to date.");
				tanggalUpateLpse();
			}
			updateSchedule = new UpdateSchedule();
			updateSchedule.id = responseCheck.spId;
			updateSchedule.sp_id = responseCheck.spId;
			updateSchedule.spse_version = responseCheck.spseVersion;
			updateSchedule.save();
			Logger.info("[JaimCtr] checkUpdate - " + new Gson().toJson(updateSchedule));
			Logger.info("[JaimCtr] checkUpdate - new Update inserted. Waiting for PPE to set schedule");
			flash.success("Patch baru berhasil didaftarkan: %s", updateSchedule.sp_id);
		}catch (Exception e) {
			Logger.error(e, "Patch baru gagal didaftarkan");
//			Logger.info(e, "Patch baru gagal didaftarkan: %s", updateSchedule.sp_id);
			if(updateSchedule != null)
				flash.error("Patch baru gagal didaftarkan: "+updateSchedule.sp_id);
			else
				flash.success("tidak ada patch baru");
		}
		tanggalUpateLpse();
	}

	@AllowAccess(Group.ADM_PPE)
	public static void setujuiUpdate(String last_date, Long up_id) throws IOException {
		Logger.info("[JaimCtr] setujuiUpdate");
		Logger.debug("last_date = %s", last_date);
		JsonObject result = new JsonObject();
		final String msg = ServicePackAcceptance.setujuiTolakUpdate(last_date, up_id, "SETUJU");
		if(ServicePackAcceptance.OK.equals(msg)) {
			boolean do_backup = params.get("do_backup") != null && params.get("do_backup").equals("true");
			UpdateSchedule updateSchedule = UpdateSchedule.findById(up_id);
			updateSchedule.update_date = new Date();
			updateSchedule.do_backup = do_backup;
			updateSchedule.save();
		}
		result.addProperty("message", msg);
		renderJSON(result);
	}

	@AllowAccess(Group.ADM_PPE)
	public static void tolakUpdate(String last_date, Long up_id) throws IOException {
		Logger.info("[JaimCtr] tolakUpdate");
		Logger.debug("last_date = %s", last_date);
		JsonObject result = new JsonObject();
		final String msg = ServicePackAcceptance.setujuiTolakUpdate(last_date, up_id, "TOLAK");
		result.addProperty("message", msg);
		renderJSON(result);
	}


	public static void generatePassword() {
		renderText(PasswordGenerator.generateCommonLangPassword());
	}
}

