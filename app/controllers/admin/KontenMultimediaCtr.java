package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import models.common.KontenMultimedia;
import models.common.KontenMultimedia.JenisKonten;
import models.secman.Group;
import play.Play;
import play.data.validation.Valid;
import play.i18n.Messages;

import java.io.File;

/**
 * Controller untuk manajemen kontent multimedia
 * @author arief ardiyansah
 *
 */
public class KontenMultimediaCtr extends BasicCtr {

	@AllowAccess({Group.ADM_PPE})
	public static void index() {
		renderArgs.put("daftarSlide", KontenMultimedia.find("jenis = ?", JenisKonten.SLIDE_SHOW).fetch());
		renderArgs.put("daftarLink", KontenMultimedia.find("jenis = ?", JenisKonten.LINK).fetch());
		renderTemplate("admin/kontenMultimedia/list.html");
	}

	@AllowAccess({Group.ADM_PPE})
	public static void tambah_slide() {
		KontenMultimedia konten = new KontenMultimedia();
		konten.jenis = JenisKonten.SLIDE_SHOW;
		renderArgs.put("konten", konten);
		renderTemplate("admin/kontenMultimedia/edit.html");
	}

	@AllowAccess({Group.ADM_PPE})
	public static void tambah_link() {
		KontenMultimedia konten = new KontenMultimedia();
		konten.jenis = JenisKonten.LINK;
		renderArgs.put("konten", konten);
		renderTemplate("admin/kontenMultimedia/edit.html");
	}

	@AllowAccess({Group.ADM_PPE})
	public static void edit(Long id) {
		renderArgs.put("konten", KontenMultimedia.findById(id));
		renderTemplate("admin/kontenMultimedia/edit.html");
	}

	@AllowAccess({Group.ADM_PPE})
	public static void submit(Long id, @Valid KontenMultimedia konten, @DokumenType File blobTable) throws Exception {
		checkAuthenticity();
		String message = "";
		boolean isError = false;
//		if(konten.blb_id==null){
//			validation.required(blobTable).key("konten.gambar").message("Berkas wajib diisi");
//		}
//		boolean changeImage = true;

		if(konten.description.length() > 140) {
			validation.maxSize(konten.description, 140).message(Messages.get("flash.max_140"));
		}

		if(id==null && blobTable == null){
			validation.required(blobTable).key("konten.file_name").message(Messages.get("flash.bkd"));
		}
		if(blobTable != null){						
			boolean permitDimension = KontenMultimedia.isPermittedDimension(blobTable);
			if(!permitDimension){
				//not Permit
				message +=Messages.get("flast.duts");
				validation.required(blobTable).key("konten.file_name").message(message);
				isError = true;
			}			
		}

		if (isError){
			validation.keep();
			params.flash();
			flash.error(Messages.get("flast.dgs")+message);
			renderArgs.put("konten", konten);
			renderTemplate("admin/kontenMultimedia/edit.html");
		}
		else{
			id = KontenMultimedia.simpan(id, konten, blobTable);
			flash.success(Messages.get("flast.dt"));
		}
		index();
	}

	@AllowAccess({Group.ADM_PPE})
	public static void hapus(Long id){
		KontenMultimedia.delete("media_id=?", id);
		index();
	}

	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void header() {		
		renderTemplate("admin/kontenMultimedia/header.html");
	}

	/** MEngganti image Logo instansi dan nama LPSE
	 * @throws Exception*/
	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void headerSubmit(File namaLPSE, File logoInstansi, Integer aktif) throws Exception {
		KontenMultimedia header = KontenMultimedia.find(JenisKonten.NAMA_LPSE);
		KontenMultimedia logo = KontenMultimedia.find(JenisKonten.LOGO_INSTANSI);
//		//jika set aktif
		if(aktif == 0) {
			header.setBlobTable(new File(Play.applicationPath + "/public/images/imgng/lpse-nama-default.png"));
			logo.setBlobTable(new File(Play.applicationPath + "/public/images/imgng/instansi-logo-default.png"));
		}else if (aktif == 2) {	
			if(namaLPSE != null) {					
				header.setBlobTable(namaLPSE);
			}
			if(logoInstansi != null) {					
				logo.setBlobTable(logoInstansi);
			}		
		}
		header();
	}


}
