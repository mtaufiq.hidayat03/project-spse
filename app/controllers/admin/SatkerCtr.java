package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Satuan_kerja;
import models.common.Active_user;
import models.secman.Group;
import play.data.validation.Valid;
import play.i18n.Messages;

/**
 * Kelas controller untuk CRUD model {@code Satuan_kerja}. Created by IntelliJ
 * IDEA, Date: 30/07/12 Time: 14:46
 *
 * @author I Wayan Wiprayoga W
 */
public class SatkerCtr extends BasicCtr {

	/**
	 * Fungsi {@code satker} digunakan untuk menampilkan halaman daftar satuan
	 * kerja
	 */
	@AllowAccess({Group.ADM_AGENCY})
	public static void index() {
		renderTemplate("admin/satker/satker.html");
	}

	/**
	 * Fungsi {@code satkerEdit} digunakan untuk menampilkan halaman edit satuan
	 * kerja
	 *
	 * @param stk_id id satuan kerja
	 */
	@AllowAccess({Group.ADM_AGENCY})
	public static void edit(Long stk_id) {
		if (stk_id != null) { // edit satker
			Satuan_kerja satker = Satuan_kerja.findBy(stk_id, Active_user.current().agencyId); // admin agency hanya dapat akses satuan kerja yang bernaung dibawahnya
			if (satker == null) {
				forbidden(Messages.get("flash.fbdnn"));
			}
			renderArgs.put("satker", satker);
		}	
		renderTemplate("admin/satker/satkerEdit.html");
	}

	// TODO: Belum dites
	@AllowAccess({Group.ADM_AGENCY})
	public static void hapus(Long stk_id) {
		checkAuthenticity(); // memeriksa token
		String[] chk_val = params.getAll("chkSatker");
		if (chk_val == null) {
			// do nothing
		} else {
			for (String val : chk_val) {
				Satuan_kerja.hapus(Long.parseLong(val)); // hapus masing-masing data satuan kerja
			}
		}
		index();
	}

	// TODO: Belum dites
	@AllowAccess({Group.ADM_AGENCY})
	public static void simpan(@Valid Satuan_kerja satker) {
		checkAuthenticity(); // memeriksa token
		try {
			satker.save();
			flash.success(Messages.get("flash.dt"));
			index();
		}catch (Exception e) {
			flash.error(Messages.get("flash.dgt"));
			renderArgs.put("satker", satker);
			renderTemplate("admin/satker/satkerEdit.html"); // error input satuan kerja
		}
		
	}


}
