package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Ppe_site;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;

/**
 * Controller untuk Manajemen Daftar LPSE
 * @author areif ardiyansah
 *
 */
public class LpseCtr extends BasicCtr {

	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void index() {
		renderTemplate("admin/lpse/list.html");
	}

	
	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void edit(String id) {
		boolean lpse_baru = true;
		Ppe_site ppe_site = null;
		if(!StringUtils.isEmpty(id)) {
			ppe_site = Ppe_site.findById(id);
			lpse_baru = false;
		}
		if(ppe_site == null){
			ppe_site = new Ppe_site();
		}
		ppe_site.flash();
		renderArgs.put("ppe_site", ppe_site);
		renderArgs.put("lpse_baru", lpse_baru);
		renderTemplate("admin/lpse/edit.html");
	}
	
	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void hapus(String id) {
		Ppe_site.delete("pps_id=?", id);
		flash.success(Messages.get("flast.bhsldhps"), "LPSE");
		Cache.safeDelete("ppe_siteList");
		index();
	}
	
	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void submit(Ppe_site ppe_site) {
		checkAuthenticity();
		String lpse_baru = params.get("lpse_baru");
		String stg_id = params.get("ppe_site.sub_tag");
		if (StringUtils.isNotEmpty(stg_id)) {
			ppe_site.stg_id = stg_id;		
		}
		if (StringUtils.isNotEmpty(lpse_baru) && lpse_baru!= null) { // input LPSE baru
			Ppe_site temp = Ppe_site.findById(ppe_site.pps_id);
			// jika ID LPSE sudah ada
			if (temp != null)  // error duplikasi ID LPSE
				validation.required("").key("ppe_site.pps_id.duplicate").message(Messages.get("flash.id_ada"));
		}
		validation.valid(ppe_site);
		if(validation.hasErrors()){
			validation.keep();
			params.flash();
			ppe_site.paramFlash();
			edit(ppe_site.pps_id);
		} else {
			try {				
				ppe_site.save();
				flash.success(Messages.get("flash.bds"), "Data");
				Cache.safeDelete("ppe_siteList");
				index();
			}catch(Exception e) {
				Logger.error(e, "%s gagal disimpan", "Data");
				flash.error(Messages.get("flash.gds", "Data"));
				edit(ppe_site.pps_id);
			}
		}
	}
}
