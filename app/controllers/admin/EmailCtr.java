package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import jobs.SendMailJobNow;
import models.common.ConfigurationDao;
import models.jcommon.config.Configuration;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.DateUtil;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import play.Logger;
import play.libs.Mail;
import play.i18n.Messages;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.util.HashMap;
import java.util.Map;


public class EmailCtr extends BasicCtr {

    private static final Template templates = TemplateLoader.load("/email/test-email.html");

    /**
     * Fungsi {@code emailStatusPengiriman} digunakan untuk menampilkan halaman daftar status pengiriman email LPSE
     */
    @AllowAccess({Group.ADM_PPE, Group.HELPDESK})
    public static void index() {
        renderArgs.put("mail_delay", Configuration.getConfigurationValue(MailQueue.SMTP_DELAY));
        renderArgs.put("mail_retry",Configuration.getConfigurationValue(MailQueue.SMTP_RETRY));
        renderArgs.put("mail_sender", Configuration.getConfigurationValue(MailQueue.SMTP_SENDER));
        renderArgs.put("mail_server",Configuration.getConfigurationValue(MailQueue.SMTP_HOST));
        renderArgs.put("mail_port", Configuration.getConfigurationValue(MailQueue.SMTP_PORT));
        renderTemplate("admin/email/list.html");
    }


    /**
     * Fungsi {@code emailInfoServer} digunakan untuk menampilkan halaman edit info server email LPSE
     */
    @AllowAccess({Group.ADM_PPE})
    public static void infoServer() {
        renderArgs.put("mail_delay", Configuration.getConfigurationValue(MailQueue.SMTP_DELAY));
        renderArgs.put("mail_retry", Configuration.getConfigurationValue(MailQueue.SMTP_RETRY));
        renderArgs.put("mail_sender",Configuration.getConfigurationValue(MailQueue.SMTP_SENDER));
        renderArgs.put("mail_server", Configuration.getConfigurationValue(MailQueue.SMTP_HOST));
        renderArgs.put("mail_port",Configuration.getConfigurationValue(MailQueue.SMTP_PORT));
        renderArgs.put("mail_password", Configuration.getConfigurationValue(MailQueue.SMTP_SENDER_PASWORD));
        renderTemplate("admin/email/infoServer.html");
    }

    /**
     * Fungsi {@code emailInfoServerSimpanSubmit} digunakan untuk menyimpan informasi mail server yang dimasukan oleh
     * Admin PPE
     */
    @AllowAccess({Group.ADM_PPE})
    public static void InfoServerSimpanSubmit() {
        checkAuthenticity(); // periksa authentikasi
        // Dapatkan parameter HTTP
        String mail_delay = params.get("epns.mail-delay");
        String mail_retry = params.get("epns.mail-retry");
        String mail_sender = params.get("epns.mail-sender");
        String mail_server = params.get("epns.mail-server");
        String mail_port = params.get("epns.mail-port");
        String mail_password = params.get("epns.mail-password");
        // validasi input
        validation.required(mail_server).key("epns.mail-server").message("SMTP Server wajib diisi");
        validation.required(mail_port).key("epns.mail-port").message("SMTP Port wajib diisi");
        if (validation.hasErrors()) {
            flash.error("Data gagal tersimpan");
            renderArgs.put("mail_delay", mail_delay);
            renderArgs.put("mail_port", mail_port);
            renderArgs.put("mail_retry", mail_retry);
            renderArgs.put("mail_sender", mail_sender);
            renderArgs.put("mail_server", mail_server);
            renderArgs.put("mail_password", mail_password);
            renderTemplate("admin/utility/emailInfoServer.html");
        } else { // berhasil validasi, lalu update data
            Configuration.updateConfigurationValue(MailQueue.SMTP_DELAY, mail_delay);
            Configuration.updateConfigurationValue(MailQueue.SMTP_RETRY, mail_retry);
            Configuration.updateConfigurationValue(MailQueue.SMTP_SENDER, mail_sender);
            Configuration.updateConfigurationValue(MailQueue.SMTP_HOST, mail_server);
            Configuration.updateConfigurationValue(MailQueue.SMTP_PORT, mail_port);
            Configuration.updateConfigurationValue(MailQueue.SMTP_SENDER_PASWORD, mail_password);
            flash.success("Data berhasil tersimpan");
            infoServer(); // render halaman edit lagi
        }
    }

    public static void tesKirim(String mailTo, String mailSubject, String mailValue)  {
        checkAuthenticity();
        Map vars = new HashMap<String, String>();
        vars.put("subject", mailSubject);
        vars.put("isi", mailValue);
        vars.put("nama_lpse", ConfigurationDao.getNamaLpse());
        try {
            MailQueue mq = EmailManager.createEmail(mailTo, templates, vars);
            mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
            mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
            mq.lls_id = Long.parseLong("0");
            mq.rkn_id = Long.parseLong("0");
            await(new SendMailJobNow(mq).now());
            flash.success(Messages.get("flash.test_email_ptt"));
        }catch (Exception e){
            Logger.error(e, "Test Email Gagal, pesan Anda gagal terkirim");
            flash.error(Messages.get("flash.test_email_pgt"));
        }
        infoServer();
    }


    @AllowAccess({Group.ADM_PPE, Group.HELPDESK})
    public static void kirimUlang(Long[] chkEmail) throws Exception {
        if (ArrayUtils.isEmpty(chkEmail)) {
            index();
        }
        MailQueue[] mails = new MailQueue[chkEmail.length];
        for(Long id : chkEmail) {
            MailQueue mq = MailQueue.findById(id);
            mq.enqueue_date = DateUtil.newDate();
            mq.status = MailQueue.MAIL_STATUS.INBOX; //status awal
            mq.retry = 0;
            mq.engine_version = MailQueue.ENGINE_VERSION;
            mq.mime = 1;
            mq.from_address = Configuration.getConfigurationValue(MailQueue.SMTP_SENDER);
            if (mq.from_address == null)
                throw new NullPointerException("[CONFIGURATION] mail.sender belum diisi");
            ArrayUtils.add(mails, mq);
        }
        try {
            await(new SendMailJobNow(mails).now());
            flash.success("Sistem ");
        }catch (Exception e){
            Logger.error(e, "Sistem Gagal Kirim ulang");
            flash.error(Messages.get("flash.sgku"));
        }
        index();
    }

    @AllowAccess({Group.ADM_PPE, Group.HELPDESK})
    public static void view(Long id) {
        renderArgs.put("mq", MailQueue.findById(id));
        renderTemplate("admin/email/viewEmail.html");
    }
}
