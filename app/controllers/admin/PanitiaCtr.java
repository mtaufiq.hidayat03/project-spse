package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.*;
import models.common.Active_user;
import models.jcommon.util.DateUtil;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Valid;
import play.db.jdbc.Query;
import play.i18n.Messages;

import java.util.List;

/**
 * Kelas {@code PanitiaCtr} digunakan untuk mengatur proses CRUD Kepanitiaan.
 * Created by IntelliJ IDEA. Date: 07/08/12 Time: 15:39
 *
 * @author I Wayan Wiprayoga W
 */
public class PanitiaCtr extends BasicCtr {

	/**
	 * Fungsi {@code panitia} digunakan untuk menampilkan halaman daftar
	 * kepanitiaan dalam sebuah agency
	 *
	 * @param tahun menampilkan daftar kepanitiaan berdasarkan tahun
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.UKPBJ, Group.KUPPBJ})
	public static void index(int tahun) {
		if (tahun == 0) {
			tahun = DateUtil.getTahunSekarang(); // tahun kepanitiaan
		}
		renderArgs.put("tahun", tahun);
		renderTemplate("admin/panitia/panitia.html");
	}

	/**
	 * Fungsi {@code panitiaIdentitas} digunakan untuk menampilkan halaman edit identitas pokja/kepanitiaan
	 */
	@AllowAccess({Group.PANITIA})
	public static void identitas(Long panitiaId) {
		Panitia panitia = Panitia.findById(panitiaId); // dapatkan data pantia dari session ID panitia
		if (panitia != null) {
			renderArgs.put("panitia", panitia);
			renderArgs.put("anggotaList", Anggota_panitia.find("pnt_id=?", panitia).fetch());
			renderTemplate("admin/panitia/panitiaIdentitas.html");
		} else {
			forbidden("Anda bukan merupakan anggota untuk pokja/kepanitiaan ini! Silakan login ulang!");
		}
	}

	/**
	 * Fungsi {@code panitiaIdentitasSimpanSubmit} digunakan untuk menyimpan data identitas pokja.
	 *
	 * @param panitia objek panitia yang sudah di-bind dari hasil submit form
	 */
	@AllowAccess({Group.PANITIA})
	public static void simpanIdentitas(@Valid Panitia panitia) {
		checkAuthenticity();
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else {
			Panitia obj = Panitia.findById(panitia.pnt_id);
			obj.pnt_alamat = panitia.pnt_alamat;
			obj.kbp_id = panitia.kbp_id;
			obj.pnt_email = panitia.pnt_email;
			obj.pnt_website = panitia.pnt_website;
			obj.pnt_telp = panitia.pnt_telp;
			obj.pnt_fax = panitia.pnt_fax;
			obj.save();
			flash.success(Messages.get("flash.dt"));
		}
		identitas(panitia.pnt_id); // render kembali halaman identitas pantia
	}

	/**
	 * Fungsi {@code panitiaEdit} digunakan untuk menampilkan halaman edit
	 * kepanitiaan
	 *
	 * @param id     id kepanitiaan
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.KUPPBJ})
	public static void edit(Long id) {
		Panitia panitia = new Panitia();
		panitia.pnt_tahun = DateUtil.getTahunSekarang();
		List<Anggota_panitia> daftar_panitia = null;
		// TODO: apakah admin agency hanya bisa edit panitia yang ada pada satker dibawahnya?
		if (id != null) {
//			Active_user activeUser = Active_user.current();
//			if (activeUser != null && activeUser.agencyId != null) {
//				List<Pegawai> leaders = Pegawai.findMyUkpbjs(activeUser.agencyId);
//				param.put("leaders", leaders);
//			}
			panitia = Panitia.findById(id); // ambil data kepanitiaan yang sesuai
			renderArgs.put("daftar_panitia",panitia.getAnggota_panitiaList()); // cari semua anggota kepanitiaan
			renderArgs.put("instansiId", panitia.getSatuan_kerja().instansi_id);
		}
		renderArgs.put("ukpbj_id",Active_user.current().ukpbjId);
		renderArgs.put("panitia", panitia);
		renderTemplate("admin/panitia/panitiaEdit.html");
	}
	
	/**
	 * Fungsi {@code editError} digunakan untuk menampilkan halaman edit
	 * kepanitiaan
	 *
	 * @param panitia 
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.KUPPBJ})
	public static void editError(Panitia panitia) {
		renderArgs.put("panitia", panitia);
		//TODO : ambil list daftar_panitia
		List<Anggota_panitia> daftar_panitia = null;
		// TODO: apakah admin agency hanya bisa edit panitia yang ada pada satker dibawahnya?
//		Active_user activeUser = Active_user.current();
//		if (activeUser != null && activeUser.agencyId != null) {
//			List<Pegawai> leaders = Pegawai.findMyUkpbjs(activeUser.agencyId);
//			param.put("leaders", leaders);
//		}
		renderTemplate("admin/panitia/panitiaEdit.html");
	}

	/**
	 * Fungsi {@code panitiaHapusSubmit} digunakan untuk menghapus entri
	 * kepanitiaan
	 */
	// TODO: Belum Implementasi
	@AllowAccess({Group.ADM_AGENCY, Group.KUPPBJ})
	public static void hapus() {
		checkAuthenticity();
		String[] selected_val = params.getAll("chkPanitia"); // dapatkan id pegawai dari parameter HTTP
		if(selected_val != null && selected_val.length > 0) {
			String join = StringUtils.join(selected_val, ",");
			long pktJml = Paket_panitia.count("pnt_id in ("+join+ ")");
			long pktPlJml = Paket_pl_panitia.count("pnt_id in ("+join+ ")");
			if(pktJml > 0 || pktPlJml > 0) {
				flash.error(Messages.get("flash.kepanitian_tdk_dpt_dhps"));
				index(0);
			}
			Anggota_panitia.delete("pnt_id in ("+join+ ')');
			Panitia.delete("pnt_id in ("+join+ ')');
		}
		flash.success(Messages.get("flash.kepanitian_bhps"));
		index(DateUtil.getTahunSekarang());
	}

	/**
	 * Fungsi {@code panitiaSimpanSubmit} digunakan untuk menyimpan data
	 * kepanitiaan
	 *
	 * @param panitia objek kepanitiaan yang di-<i>bind</i> dari form
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.KUPPBJ})
	public static void simpan(Panitia panitia, List<Long> pegawaiId, List<String> jabatan, String hapus, List<Long> chk_Anggota) {
		checkAuthenticity();
		if(panitia.hadPaket()) {
			panitia = Panitia.findById(panitia.pnt_id);
		}
		panitia.is_active = params.get("isActive") == null ? 0 : -1;
		validation.valid(panitia);
		if (hapus != null) {
			if (chk_Anggota != null) {
				long jumlah = Anggota_panitia.count("pnt_id=?", panitia.pnt_id) - chk_Anggota.size();
				if (jumlah == 1 || jumlah % 2 == 0) {
					flash.error(Messages.get("flash.pokja_min_3"));
					edit(panitia.pnt_id);
				}

				String join = StringUtils.join(chk_Anggota, ",");
				Query.update("delete from Anggota_panitia where pnt_id=? and peg_id in (" + join + ')', panitia.pnt_id);
			
			}
			flash.success(Messages.get("flash.data_bhps")); // berhasil validasi dan simpan data
		} else if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("flash.dgt"));
			editError(panitia);
		} else {
			panitia.agc_id = Active_user.current().agencyId;
			panitia.save();
			flash.success(Messages.get("flash.dt")); // berhasil validasi dan simpan data
		}

		edit(panitia.pnt_id);
	}

	/**
	 * Fungsi {@code panitiaTambahAnggota} digunakan untuk menampilkan halaman
	 * menambahkan anggota kepanitiaan
	 *
	 * @param id id kepanitiaan
	 */
	@AllowAccess({Group.KUPPBJ})
	public static void tambahAnggota(Long id) {
		if(id == null)			
			index(0);
		else {
			renderArgs.put("panitia", Panitia.findById(id));
//			renderArgs.put("agency", Agency.findById(Active_user.current().agencyId));
			renderArgs.put("ukpbj", Ukpbj.findById(Active_user.current().ukpbjId));
			renderArgs.put("jumlahAnggota",Anggota_panitia.count("pnt_id=?", id));
			renderTemplate("admin/panitia/panitiaTambahAnggota.html");
		}
	}

	/**
	 * TODO: Belum Implementasi!
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.KUPPBJ})
	public static void hapusAnggota(Long id) {
		checkAuthenticity();
		String[] selected_val = params.getAll("chkAnggota"); // dapatkan id pegawai dari parameter HTTP

		if(selected_val!=null && selected_val.length > 0){
			long jumlahSekarang = Anggota_panitia.count("pnt_id=?", id);
			long jumlah = jumlahSekarang - selected_val.length;
			if (jumlah == 1 || jumlah % 2 == 0) {
				flash.error(Messages.get("flash.data_bhps"));
				tambahAnggota(id);
			}

			String join = StringUtils.join(selected_val, ",");
			Anggota_panitia.delete("pnt_id=? and peg_id in ("+join+ ')', id);
		}
		edit(id);
	}
	
	/**
	 * Fungsi {@code panitiaSimpanAnggotaPanitiaSubmit} digunakan untuk menyimpan anggota kepanitiaan terpilih
	 *
	 * @param id id kepanitiaan
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.KUPPBJ})
	public static void simpanAnggota(Long id) {
		// memeriksa otentikasi
		Panitia panitia = Panitia.findById(id);
		String[] selected_val = params != null ? params.getAll("peg_id") : null; // dapatkan id pegawai dari parameter HTTP
		if (selected_val != null && selected_val.length > 0) {
			for (String val : selected_val) {
				Long pegawaiId = Long.parseLong(val);
				Anggota_panitia.createAnggotaPanitia(pegawaiId, id); // buat objek anggota kepanitiaan untuk panitia yang telah didapatkan id-nya
			}
			long jumlahAnggota = Anggota_panitia.count("pnt_id=?", id);
			if(jumlahAnggota == 1 || jumlahAnggota % 2 == 0){
				flash.error(Messages.get("flash.pokja_min_3"));
				tambahAnggota(id);
			}
			flash.success(Messages.get("flash.pokja_dtmbh"));
		}
		edit(panitia.pnt_id); // kembali ke halaman edit kepanitiaan
	}

	private static boolean cekJumlahAnggotaPanitia(int jumlahBaru, long panitiaId){
		long jumlahSekarang = Anggota_panitia.count("pnt_id=?", panitiaId);
		long jumlah = jumlahSekarang - jumlahBaru;

		return jumlah > 1 && jumlah % 2 == 0;

	}

	@AllowAccess({Group.PANITIA, Group.KUPPBJ})
	public static void anggota(Long id) {
		otorisasiDataPanitia(id);
		notFoundIfNull(id);
		Panitia panitia = Panitia.findById(id);
		renderArgs.put("panitia", panitia);
		renderArgs.put("anggotaList", panitia.getAnggota_panitiaList());
		renderTemplate("admin/panitia/anggota-panitia.html");
	}
	
}