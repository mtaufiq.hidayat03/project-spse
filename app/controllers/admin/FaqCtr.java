package controllers.admin;


import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Faq;
import models.agency.Faq.FaqStatus;
import models.agency.Pegawai;
import models.common.Active_user;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.Codec;

/**
 * Kelas controller untuk CRUD model {@code Faq}.
 *
 * @author I Wayan Wiprayoga W
 */
// TODO: Controller FAQ belum dites
public class FaqCtr extends BasicCtr {

    /**
     * Fungsi {@code faq} digunakan untuk menampilkan halaman daftar FAQ
     */
    @AllowAccess({Group.HELPDESK, Group.ADM_PPE})
    public static void index() {
        renderTemplate("admin/faq/faq.html");
    }

    /**
     * Fungsi {@code faqView} digunakan untuk menampilkan halaman lihat detil
     * FAQ (<i>read only</i>)
     *
     * @param id id FAQ
     */
    @AllowAccess({Group.HELPDESK, Group.ADM_PPE})
    public static void view(Long id) {
        if (id == null) {
            index(); // jika id faq null, kembali ke halaman daftar FAQ
        } else {
            Faq faq = Faq.findById(id);
            renderArgs.put("faq", faq);
            renderTemplate("admin/faq/faqView.html");
        }
    }

    /**
     * Fungsi {@code faqJawab} digunakan untuk menampilkan halaman menjawab
     * pertanyaan (FAQ)
     *
     * @param id id FAQ
     */
    @AllowAccess({Group.HELPDESK, Group.ADM_PPE})
    public static void jawab(Long id) {
        boolean jawabFaq = true; // flag menjawab FAQ
        if (id == null) {
            index(); // jika id faq null, kembali ke halaman daftar FAQ
        } else {
            // Cek apakah FAQ ini berasal dari publik. contoh ciri : pegawai null -> belum ada yang menjawab
            Faq faq = Faq.findFaqByID(id);
            Active_user active_user = Active_user.current();
            if (faq.peg_id != null) {
                if (active_user.isHelpdesk()) { // TODO: helpdesk hanya bisa akses data yang dibuatnya?
                    faq = Faq.findFaqByPegawaiAndID(id, active_user.userid);
                    Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
                    renderArgs.put("pegawai", pegawai);
                } else { // Admin PPE bisa akses semua data FAQ
                    faq = Faq.findById(id);
                }
            }
            renderArgs.put("faq", faq);
            renderArgs.put("jawabFaq", jawabFaq);
            renderTemplate("admin/faq/faqEdit.html");
        }
    }

    /**
     * Fungsi {@code faqHapusSubmit} digunakan untuk menghapus FAQ yang
     * ada pada halaman daftar FAQ
     */
    @AllowAccess({Group.HELPDESK, Group.ADM_PPE})
    public static void hapus() {
        checkAuthenticity(); // cek otentikasi
        String[] chk_val = params.getAll("chkFaq");
        if (chk_val == null) { // jika tidak ada faq yang di-ceklist, maka tampilkan kembali halaman daftar FAQ
            index();
        }
        for (String val : chk_val) {
            Faq.hapus(Long.parseLong(val)); // hapus faq satu-persatu
        }
        index();
    }

    /**
     * Fungsi {@code faqSimpanSubmit} digunakan untuk menyimpan objek
     * {@code Faq} hasil submit dari form.
     *
     * @param faq input Faq yang sudah ter-<i>binding</i> dari form
     */
    // TODO: Belum handling kesalahan input
    @AllowAccess({Group.HELPDESK, Group.ADM_PPE})
    public static void simpan(Faq faq) {
        checkAuthenticity();
        if (faq.faq_id == null) {
            faq.faq_status = FaqStatus.BELUM_TERJAWAB;
        }
        validation.valid(faq);
        if (validation.hasErrors()) {
            params.flash();
            validation.keep();
            flash.error(Messages.get("flash.dgs"));
            if (faq.faq_id == null) {
                tambah();
            } else {
                renderArgs.put("faq", faq);
                renderTemplate("admin/faq/faqEdit.html");
            }
        } else {

            String sudahDijawab = params.get("chkSudahDijawab");
            if (StringUtils.isNotEmpty(faq.faq_jawaban)) { // jika jawaban ada berarti sudah pernah dijawab
                faq.faq_tgljawab = controllers.BasicCtr.newDate(); // set tanggal jawab sekarang
                if (sudahDijawab != null) {
                    faq.faq_status = FaqStatus.SUDAH_DIJAWAB;
                    faq.save();
                }
                //faq.faq_status = FaqStatus.SUDAH_DIJAWAB.getValue();} // set status FAQ sudah pernah dijawab

                else {
                    faq.faq_status = FaqStatus.TERJAWAB;
                    faq.save();
                }
                flash.success(Messages.get("flash.dbs"));
                view(faq.faq_id);
            }
            //faq.faq_status = FaqStatus.TERJAWAB.getValue();}
            else { // jika belum ada jawaban berarti baru mengirimkan FAQ
                faq.faq_status = FaqStatus.BELUM_TERJAWAB;
                faq.save();
                flash.success(Messages.get("flash.dbs"));
                index();
            }

        }
    }

    /**
     * Fungsi {@code faqTambah} digunakan untuk menampilkan halaman edit FAQ
     * tetapi untuk kasus membuat FAQ baru. Hanya HELPDESK yang dapat menambah FAQ
     */
    @AllowAccess({Group.HELPDESK})
    public static void tambah() {
        renderArgs.put("tambah",  true);
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderTemplate("admin/faq/faqEdit.html"); // tampilkan halaman edit FAQ
    }

    /**
     * menampilkan faq di halaman publik
     */
    public static void list() {
        renderArgs.put("randomID", Codec.UUID());
        renderTemplate("admin/faq/list.html");
    }

    public static void tambahFaq(Faq faq, String randomID, String code) {

        checkAuthenticity();
        faq.faq_status = FaqStatus.BELUM_TERJAWAB;
        validation.valid(faq);
        validation.required(code).message(Messages.get("flash.kkhd"));
        if (randomID != null && code != null) {
            validation.equals(code, Cache.get(randomID)).message(Messages.get("flas.kks"));
        }
        if (validation.hasErrors()) {
            params.flash();
            validation.keep();
        } else {
            faq.faq_tglkirim = (controllers.BasicCtr.newDate());
            faq.save();
            flash.success(Messages.get("flash.pertanyaan_berhasil"));
        }
        list();
    }
}
