package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Admin_agency;
import models.agency.Agency;
import models.agency.Pegawai;
import models.agency.Satuan_kerja;
import models.common.Active_user;
import models.form.FormAgency;
import models.form.FormSelectAgency;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.secman.Group;
import play.Logger;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Kelas controller untuk CRUD Model {@code Agency}.
 *
 * @author I Wayan Wiprayoga W
 */
public class AgencyCtr extends BasicCtr {

	/**
     * Fungsi {@code agency} digunakan untuk menampilkan halaman daftar agency
     */
    @AllowAccess({Group.ADM_AGENCY, Group.ADM_PPE})
    public static void index() {       
        renderTemplate("admin/agency/agency.html");
    }

	/**
	 * Fungsi {@code agencyDetil} digunakan untuk menampilkan halaman detil
	 * agency (<i>read-only</i>)
	 *
	 * @param id id agency
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.ADM_PPE})
	public static void detil(Long id) {
		if (id == null) {
			index(); // jika id null, kembali ke halaman daftar agency
		} else {
			Agency agency = Agency.findById(id);
			renderArgs.put("agency", agency);
			if (Active_user.current().isAdminAgency()) {
				List<Admin_agency> admin = Admin_agency.find("agc_id=?", id).fetch();
				if (admin != null) {
					renderArgs.put("admin", admin);
				}
			} else { // admin agency dapat melihat detil semua agency yang ada
				List<Admin_agency> admin = Admin_agency.find("agc_id=?", id).fetch();
				if (admin != null) {
					renderArgs.put("admin", admin);
				}
			}
			renderTemplate("admin/agency/agencyDetil.html");
		}
	}

	/**
	 * Fungsi {@code agencyEdit} digunakan untuk menampilkan halaman edit model
	 * {@code Agency}
	 *
	 * @param id id berita, jika tidak {@code null} berarti edit berita dan jika
	 *           {@code null} maka berita baru
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void edit(Long id) {
		Active_user activeUser = Active_user.current();
		FormAgency form = new FormAgency();
		if(flash.contains("body")){
			try{
				form.convert(flash,activeUser);
			}catch (ParseException e){
				flash.error(e.getLocalizedMessage());
				if(id != null){
					detil(id);
				}
				index();
			}
		}else{
			if(id != null){
				form.agency = Agency.findById(id);
				form.adminAgencies = FormSelectAgency.findByAgency(form.agency.agc_id);

				if(form.adminAgencies.isEmpty()){
					form.adminAgencies = new ArrayList<>(1);
					form.adminAgencies.add(new FormSelectAgency());
				}
			}

			if (activeUser.isAdminAgency()) {
				form.agencyParent = Agency.findAgencyByPenanggungJawab(activeUser.userid);
				// baik Admin PPE maupun Admin Agency dapat menambah agency
			}
		}
		renderArgs.put("form",form);
		renderTemplate("admin/agency/agencyEdit.html");
	}

	/**
	 * Fungsi {@code agencyHapus} digunakan untuk melakukan aksi penghapusan
	 * terhadap sebuah objek {@code Agency}
	 *
	 * @param id id agency yang akan dihapus
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.ADM_PPE})
	public static void hapus(Long id) {
		checkAuthenticity(); // memeriksa otentikasi
		Agency agency;
		if (id == null) {
			index(); // kalau tidak ada yang dihapus, langsung redirect ke halaman daftar agency
		}
		long jumlahSatker = Satuan_kerja.count("agc_id=?", id);
		long jumlahAdmin = Admin_agency.count("agc_id=?", id);
		long jumlahPegawai = Pegawai.count("agc_id=?", id);
		long jumlahSubAgency = Agency.count("sub_agc_id=?", id);
		if(jumlahAdmin == 0L && jumlahSatker == 0L && jumlahPegawai == 0L && jumlahSubAgency == 0L) {
			Agency.delete("agc_id=?", id);
			flash.success("Data berhasil dihapus");
			index(); // redirect ke halaman daftar agency
		} else {
			flash.error("Data tidak bisa dihapus");
			detil(id);
		}
	}

	/**
	 * Fungsi {@code agencySimpanSubmit} digunakan untuk menyimpan model
	 * {@code Agency} hasil submit dari form edit agency.
	 *
	 * @param form objek FormAgency dari form yang telah di-binding
	 */
	@AllowAccess({Group.ADM_AGENCY, Group.ADM_PPE})
	public static void simpan(FormAgency form) {
		checkAuthenticity(); // memeriksa otentikasi
		form.agency.is_lpse = form.agency.isLpse ? 1 : 0;

		validation.valid(form);
		if (validation.hasErrors()){
			validation.keep();
			params.flash();
			flash.error("Data gagal tersimpan. Mohon periksa kembali Data Agency yang Anda Masukkan.");
		} else {
			if(!form.isAdminAgenciesFilled()){
				flash.error("Data gagal tersimpan. Mohon lengkapi data Penanggung Jawab dan Nomor SK Penunjukkan.");
				params.flash();
				edit(form.agency.agc_id);
			}

			try {
				Agency.simpanAgency(form.agency, form.adminAgencies);
				flash.success("%s berhasil disimpan", "Data");
	            detil(form.agency.agc_id);
			}catch (Exception e) {
				flash.error("%s gagal tersimpan", "Data");
				e.printStackTrace();
			}
		}

		edit(form.agency.agc_id);

	}

	
	/**
	 * Fungsi {@code agencyIdentitas} digunakan untuk menampilkan halaman
	 * identitas agency untuk pengguna dengan role sebagai Admin Agency.
	 */
	@AllowAccess({Group.ADM_AGENCY})
	public static void identitas() {
		Agency agency = Agency.findById(Active_user.current().agencyId); // dapatkan data agency sesuai dengan session pengguna
		Admin_agency admin = Admin_agency.find("agc_id=?", agency.agc_id).first();
		renderArgs.put("agency", agency);
		renderArgs.put("admin", admin);
		renderArgs.put("pegawai", Pegawai.find("peg_id=?", admin.peg_id).first());
		renderTemplate("admin/agency/agencyIdentitas.html");
	}

	/**
	 * Fungsi {@code agencyIdentitasSimpanSubmit} digunakan untuk menyimpan data
	 * entri identitas sebuah agency.
	 *
	 * @param agency objek {@code Agency} yang sudah di-binding
	 */
	@AllowAccess({Group.ADM_AGENCY})
	public static void simpanIdentitas(Agency agency) {
		checkAuthenticity(); // memeriksa token
		validation.valid(agency);
		try {
			if(!validation.hasErrors()){
				agency.save();
				flash.success("%s berhasil disimpan", "Identitas");
			}
			else {
				System.out.println(CommonUtil.toJson(validation.errorsMap()));
				for (play.data.validation.Error error : validation.errors()) {
					Logger.debug("Error = %s", error.message());
				}
				validation.keep();
				params.flash();
				flash.error("Data gagal tersimpan.");
			}
			identitas(); // render kembali halaman identitas
			
		}catch (Exception e) {
			flash.error("%s gagal tersimpan", "Identitas");
			identitas();
		}
	} 

    /**
     * paket yang dimiliki oleh agency
     * @param tahun
     */
    @AllowAccess({Group.ADM_AGENCY})
    public static void paket(int tahun){
		if (tahun == 0) {
			tahun = DateUtil.getTahunSekarang(); // tahun kepanitiaan
		}
		renderArgs.put("tahun", tahun);
    	renderTemplate("admin/agency/paketAgency.html");
    }    
}
