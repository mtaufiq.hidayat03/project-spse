package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Admin_agency;
import models.agency.Agency;
import models.agency.Paket;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;
import models.osd.Certificate;
import models.osd.ErrorCodeOSD;
import models.secman.Group;
import models.secman.Usergroup;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;
import utils.osd.OSDUtil;

import java.util.List;

/**
 * Kelas controller untuk CRUD Model {@code Pegawai}. Created by IntelliJ IDEA,
 * Date: 30/07/12 Time: 9:53.
 *
 * @author I Wayan Wiprayoga W
 */
public class PegawaiCtr extends BasicCtr {

    /**
     * Fungsi {@code pegawai} digunakan untuk menampilkan halaman daftar pegawai
     */
    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void index() {
        renderArgs.put("isOSD", ConfigurationDao.isOSD(BasicCtr.newDate()));
        renderTemplate("admin/pegawai/pegawai.html");
    }

    /**
     * Fungsi {@code pegawaiEdit} digunakan untuk menampilkan halaman edit
     * pegawai
     *
     * @param id nip pegawai
     */
    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void edit(Long id) {
        Agency agency = null;
        Active_user active_user = Active_user.current();
        if (active_user.isAdminAgency()) {
            agency = Agency.findById(active_user.agencyId);
        }
        if (id != null) {
            Pegawai pegawai = Pegawai.findById(id); // Admin PPE dapat edit semua data Pegawai dalam sistem
            if(pegawai != null) {
                Group group = Usergroup.findGroupByUser(pegawai.peg_namauser);
                renderArgs.put("pegawai", pegawai);
                renderArgs.put("group", group);
                if (agency != null && !pegawai.agc_id.equals(agency.agc_id)) // memastikan bahwa yang edit Admin Agency
                    forbidden("Anda tidak Berhak Melakukan Perubahan Data Pegawai " + pegawai.peg_nama);
            }
        }
        renderArgs.put("agency", agency);
        renderTemplate("admin/pegawai/pegawaiEdit.html");
    }

    /**
     * Fungsi {@code pegawaiEditEror} digunakan untuk menampilkan halaman edit ketika validasi gagal
     * pegawai
     *
     * @param pegawai
     */
    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void editError(Pegawai pegawai) {
        Agency agency = null;
        Active_user active_user = Active_user.current();
        if (active_user.isAdminAgency()) {
            agency = Agency.findById(active_user.agencyId);
        }
        if (pegawai.peg_namauser != null) {
            Group group = Usergroup.findGroupByUser(pegawai.peg_namauser);
            renderArgs.put("group", group);
        }
        if (pegawai.agc_id != null)
            if (agency != null && !pegawai.agc_id.equals(agency.agc_id)) // memastikan bahwa yang edit Admin Agency
                forbidden(Messages.get("flash.tdk_dpt_merubah") + pegawai.peg_nama);
        renderArgs.put("agency", agency);
        renderArgs.put("pegawai", pegawai);
        renderTemplate("admin/pegawai/pegawaiEdit.html");
    }

    /**
     * Fungsi {@code pegawaiView} digunakan untuk menampilkan detail
     * pegawai
     *
     * @param id nip pegawai
     */
    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void view(Long id) {
        Active_user active_user = Active_user.current();
        if (id != null) {
            Pegawai pegawai = null;
            if (active_user.isAdminAgency()) {
                pegawai = Pegawai.findPegawaiByAgency(id, active_user.agencyId); // menghindari request yang tidak semestinya
                Logger.debug("Pegawai = %s", pegawai == null ? "null" : pegawai.peg_id.toString());
            } else {
                pegawai = Pegawai.findById(id);// Admin PPE dapat edit semua data Pegawai dalam sistem
            }
            renderArgs.put("pegawai", pegawai);
            if(pegawai != null)
                renderArgs.put("group", Usergroup.findGroupByUser(pegawai.peg_namauser));
            renderTemplate("admin/pegawai/pegawaiView.html");
        }
	}

	/**
	 * Fungsi {@code pegawaiSimpanSubmit} digunakan untuk menyimpan model
	 * {@code Pegawai} hasil submit dari form edit pegawai.
	 *
	 * @param pegawai objek pegawai dari form yang telah di-binding
	 */
	// TODO: Belum handling kesalahan input. Handling Duplikasi USERNAME dan NIP sudah diimplementasikan
	@AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
	public static void simpan(Pegawai pegawai, boolean chkPassword, String newPassword, String group, String isActive) {
		checkAuthenticity();
		pegawai.peg_isactive = isActive != null ? -1 : 0;
		if(pegawai.peg_id != null) {
            Pegawai pegawaiExist = Pegawai.findById(pegawai.peg_id);
            pegawai.cer_id = pegawaiExist.cer_id;
            pegawai.ams_id = pegawaiExist.ams_id;
        }
		if (pegawai.peg_id == null || chkPassword) {
			validation.required(newPassword).message(Messages.get("flash.pwb_bru"));
			validation.minSize(newPassword, 6).message(Messages.get("flash.pwd_min"));
		}

        if (pegawai.peg_id == null && pegawai.peg_namauser != null) {
            Pegawai checkUser = Pegawai.findBy(pegawai.peg_namauser);
            if (checkUser != null)
                validation.addError("pegawai.peg_namauser", "validation.unique");
        }

		if ((group.equalsIgnoreCase("PP") || group.equalsIgnoreCase("PANITIA"))) {
			if(CommonUtil.isEmpty(pegawai.peg_no_pbj)){
				validation.addError("pegawai.peg_no_pbj", "validation.required", "pegawai.peg_no_pbj");
			} else if (pegawai.peg_no_pbj.length() != 15){
				validation.addError("pegawai.peg_no_pbj", Messages.get("flash.pjng_15"));
			}
		}

		validation.required(group).message(Messages.get("flash.penganggkatan_req"));
		validation.valid(pegawai);
		
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();
			for (play.data.validation.Error err : validation.errors()) {
				if (!StringUtils.isEmpty(err.getKey())) {
					Logger.error("simpan pegawai >>> " + err.message());
				}
			}
			flash.error(Messages.get("flash.datagt_ckia"));
            renderArgs.put("pegawai", pegawai);
            renderArgs.put("group", group);
            renderArgs.put("chkPassword", chkPassword);
            renderTemplate("admin/pegawai/pegawaiEdit.html");
		} else {
			try {
				Active_user active_user = Active_user.current();
				if (active_user.isAdminAgency()) {
					Agency agency = Agency.findById(active_user.agencyId);
					pegawai.agc_id = agency.agc_id;
				}
				Pegawai.simpan(pegawai, chkPassword, newPassword, group);
				// Update sub-agency jika pegawai adalah admin agency yg di non aktifkan
				if(pegawai.peg_isactive == 0) {
					List<Admin_agency> adminAgencies = Admin_agency.find("peg_id = ?", pegawai.peg_id).fetch();
					Admin_agency.nonActiveAdmAgc(adminAgencies);
				}
				if (chkPassword) {
					flash.success(Messages.get("flash.perubahan_pwd_dt"));
				} else {
					flash.success(Messages.get("flash.dt"));
				}
				view(pegawai.peg_id); // jika berhasil tampilkan halaman daftar pegawai
			} catch (Exception e) {
				Logger.error(e, "Terjadi Kesalah Gagal simpan Pegawai");
				flash.error(Messages.get("flash.dgt"));
				edit(pegawai.peg_id);
			}
		}		
	}
	
	// TODO: Belum Implementasi
	@AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
	public static void hapus(Long[] chkPegawai) {
		checkAuthenticity();
		Pegawai.hapus(chkPegawai);
		flash.success(Messages.get("flash.pgw_dhps"));
		index(); // tampilkan lagi halaman daftar pegawai setelah menghapus objek pegawai
	}

    //FUNGSI MELIHAT DETAIL SERTIFIKAT ELEKTRONIK REKANAN  
    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void viewCert(Long id) {
        List<Certificate> certificates = OSDUtil.getCertificatesFromInaproc(id);
        renderArgs.put("certificates", certificates);
        boolean emptyData = certificates == null || certificates.size() <= 0;
        renderArgs.put("emptyData", emptyData);
        int lastVersi = 0;
        Certificate certificateLast;
        if (!emptyData && (certificateLast = certificates.get(certificates.size() - 1)) != null) {
            renderArgs.put("certificateLast", certificateLast);
            renderArgs.put("lastVersi", certificateLast.cer_versi);
        }
        renderTemplate("admin/pegawai/pegawaiViewCert.html");
    }

    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void revoke(Long id) {
        Certificate certificate = OSDUtil.getLastCertificateFromInaproc(id);
        Active_user active_user = Active_user.current();
        ErrorCodeOSD errorCode = OSDUtil.revokeCertificate(certificate, id, false, BasicCtr.newDate(), active_user.userid);
        if (errorCode != null) {
            flash.error(Messages.get("flash.sgd_de") + errorCode.getLabel());
        } else {
            flash.success(Messages.get("flash.sbdbtl"));
        }
        viewCert(id);
    }

    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void cancelRevoke(Long id) {
        Certificate certificate = OSDUtil.getLastCertificateFromInaproc(id);
        ErrorCodeOSD errorCode = OSDUtil.cancelRevokeCertificate(certificate);
        if (errorCode != null) {
            flash.error(Messages.get("flash.pbtl_cerf") + errorCode.getLabel());
        } else {
            flash.success(Messages.get("flash.permintaan_pbtl_berhasil"));
        }
        viewCert(id);
    }

    @AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
    public static void hapus(Long peg_id) {
		try {
			Pegawai pegawai = Pegawai.findByPegId(peg_id);
			if(Paket.countPaketOwnedByPegawai(pegawai) == 0) {
				pegawai.delete();
				flash.success(Messages.get("flash.pgw_dhps"));
				index();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		flash.error(Messages.get("flash.tdk_hps_peg"));
		view(peg_id);
	}
}
