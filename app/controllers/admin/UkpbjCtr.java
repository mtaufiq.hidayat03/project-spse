package controllers.admin;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.*;
import models.common.Active_user;
import models.jcommon.util.DateUtil;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 5/18/2018
 */
public class UkpbjCtr extends BasicCtr {

    @AllowAccess({Group.ADM_AGENCY})
    public static void index() {
        renderTemplate("admin/ukpbj/ukpbj.html");
    }

    @AllowAccess({Group.ADM_AGENCY, Group.UKPBJ})
    public static void edit(Long id) {
        Active_user user = Active_user.current();
        Ukpbj model = Ukpbj.findById(id);
        if (model != null) {
            if(!model.agc_id.equals(user.agencyId)){
                forbidden(Messages.get("flash.pbd_hakakss"));
            }
            renderArgs.put("daftarPegawai", model.getPegawaiList()); // cari semua pegawai
        } else {
            model = new Ukpbj();
            model.tgl_daftar = BasicCtr.newDate();
        }

        renderArgs.put("ukpbj", model);
        renderTemplate("admin/ukpbj/ukpbjEdit.html");
    }

    @AllowAccess({Group.ADM_AGENCY, Group.UKPBJ})
    public static void submit(Ukpbj ukpbj, String hapusPegawai, List<Long> chk_pegawai) {
        checkAuthenticity();
        ukpbj.agc_id = Active_user.current().agencyId;
        validation.valid(ukpbj);
        if (hapusPegawai != null) {
            if (chk_pegawai != null) {
                String join = StringUtils.join(chk_pegawai, ",");
                Pegawai_ukpbj.delete("ukpbj_id=? and peg_id in ("+join+ ')', ukpbj.ukpbj_id);

            }
            flash.success(Messages.get("flash.data_bhps")); // berhasil validasi dan simpan data
            edit(ukpbj.ukpbj_id);
        } else if (validation.hasErrors()) {
            if (ukpbj.ukpbj_id != null) {
                validation.keep();
                params.flash();
                edit(ukpbj.ukpbj_id);
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put("ukpbij", ukpbj);
                renderTemplate("admin/ukpbj/ukpbjEdit.html", map);
            }
        } else {
            try {
                ukpbj.save();
                flash.success(Messages.get("flash.bds", "Data"));
                edit(ukpbj.ukpbj_id);
            } catch (Exception e) {
                Logger.error(e, "Gagal simpan UKPBJ");
                flash.error(Messages.get("flash.gds", "Data"));
                edit(ukpbj.ukpbj_id);
            }
        }
    }

    /**
     * Fungsi {@code ukpbjTambahPegawai} digunakan untuk menampilkan halaman
     * menambahkan pegawai ukpbj
     *
     * @param id id ukpbj
     */
    @AllowAccess({Group.ADM_AGENCY})
    public static void tambahPegawai(Long id) {
        if(id == null)
            index();
        else {
            Ukpbj ukpbj = Ukpbj.findById(id);
            renderArgs.put("ukpbj", ukpbj);
            renderArgs.put("agency", Agency.findById(ukpbj.agc_id));
            renderTemplate("admin/ukpbj/ukpbjTambahPegawai.html");
        }
    }

    /**
     * Fungsi {@code ukpbjSimpanPegawai} digunakan untuk menyimpan pegawai ukpbj terpilih
     *
     * @param id id ukpbj
     */
    @AllowAccess({Group.ADM_AGENCY})
    public static void simpanPegawai(Long id) {
        // periksa otentikasi
        Ukpbj ukpbj = Ukpbj.findById(id);
        String[] selected_val = params != null ? params.getAll("peg_id") : null; // dapatkan id pegawai dari parameter HTTP
        if (selected_val != null) {
            for (String val : selected_val) {
                Long pegawaiId = Long.parseLong(val);
                Pegawai_ukpbj.createPegawai(pegawaiId, id); // buat objek pegawai ukpbj untuk pegawai yang telah didapatkan id-nya
            }
            flash.success(Messages.get("flash.pgw_ukpbj_dtmbhk"));
        }
        edit(ukpbj.ukpbj_id); // kembali ke halaman edit ukpbj
    }

    /**
     * Fungsi {@code UkpbjIdentitas} digunakan untuk menampilkan halaman
     * identitas ukpbj untuk pengguna dengan role sebagai UKPBJ/KUPPBJ.
     */
    @AllowAccess({Group.UKPBJ, Group.KUPPBJ})
    public static void identitas() {
        Ukpbj ukpbj = Ukpbj.findById(Active_user.current().ukpbjId); // dapatkan data agency sesuai dengan session pengguna
        renderArgs.put("ukpbj", ukpbj);
        renderArgs.put("pegawai", Pegawai.find("peg_id=?", ukpbj.kpl_unit_pemilihan_id).first());
        renderTemplate("admin/ukpbj/ukpbjIdentitas.html");
    }

    @AllowAccess({Group.UKPBJ, Group.KUPPBJ})
    public static void simpanIdentitas(Ukpbj ukpbj) {
        checkAuthenticity();
        Ukpbj obj =  Ukpbj.findById(Active_user.current().ukpbjId);
        obj.nama = ukpbj.nama;
        obj.alamat = ukpbj.alamat;
        obj.telepon = ukpbj.telepon;
        obj.fax = ukpbj.fax;
        obj.tgl_daftar = ukpbj.tgl_daftar;
        validation.valid(obj);
        if (validation.hasErrors()){
            validation.keep();
            params.flash();
        } else {
            try {
                obj.save();
                flash.success(Messages.get("flash.bds", "Data"));
            }catch (Exception e) {
                Logger.error(e, "Gagal simpan UKPBJ");
                flash.error(Messages.get("flash.gds", "Data"));
            }
        }
        identitas();
    }

    @AllowAccess({Group.UKPBJ, Group.KUPPBJ})
    public static void panitia() {

        renderTemplate("admin/ukpbj/ukpbjIdentitas.html");
    }


    @AllowAccess({Group.UKPBJ, Group.KUPPBJ})
    public static void paket(int tahun) {
        if (tahun == 0) {
            tahun = DateUtil.getTahunSekarang(); // tahun kepanitiaan
        }
        renderArgs.put("tahun", tahun);
        renderTemplate("admin/ukpbj/paketUkpbj.html");
    }

    @AllowAccess({Group.ADM_AGENCY})
    public static void hapus(Long[] chkUkpbj) {
        try {
            if(chkUkpbj != null && chkUkpbj.length > 0) {
                // validasi ukpbj
                boolean allow_delete = true;
                for(Long id: chkUkpbj) {
                    if(Paket.count("ukpbj_id=?", id) > 0 || Paket_pl.count("ukpbj_id=?", id) > 0
                            || Pegawai.count("ukpbj_id=?", id) > 0 || Pegawai_ukpbj.count("ukpbj_id=?", id) > 0) {
                        allow_delete = false;
                        flash.error("Data UKPBJ tidak berhasil dihapus");
                        break;
                    }
                }
                if(allow_delete) {
                    Ukpbj.delete("ukpbj_id IN (" + StringUtils.join(chkUkpbj, ",") + ")");
                    flash.success("Data berhasil dihapus");
                }
            }
        }   catch (Exception e) {
            flash.error("Data UKPBJ tidak bisa dihapus");
            Logger.error(e, "kendala hapus UKPBJ: %s", e.getMessage());
        }
        index();
    }
}
