package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.Play;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.util.List;

public class TableCtr extends BasicCtr {

    static final Gson gson = new Gson();

    /**
     * support response datatable via ajax
     * Ref : https://datatables.net/manual/server-side
     * @param select
     * @param from
     * @param column
     * @param resultset
     * @return
     */
    static JsonObject datatable(QueryBuilder select, QueryBuilder from, final String[] column, final ResultSetHandler<String[]> resultset) {
        // PAGING
        final Integer start = params.get("start", Integer.class);
        final Integer length = params.get("length", Integer.class);
        QueryBuilder queryCount = new QueryBuilder("SELECT count("+column[0]+") ").append(from);
        long iTotal = Query.count(queryCount);
        // FILTERING
        String search = params.get("search[value]");
        if (!CommonUtil.isEmpty(search)) {
            search = '%' +StringEscapeUtils.escapeHtml4(search.trim().toLowerCase())+ '%';
            QueryBuilder filter = new QueryBuilder();
            for (int i = 0; i < column.length; i++) {
                boolean searchable = column[i].equals("lls_id") ? true : Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
                if (searchable) {
                    if (!filter.isEmpty())
                        filter.append(" OR ");
                    filter.append(" LOWER(").append(column[i]).append("::varchar) LIKE ?", search);
                }
            }
            if (!filter.isEmpty()) {
                from.append(from.query().toLowerCase().contains("where") ? " AND " : " WHERE ");
                from.append("(").append(filter).append(")");
            }
        }
        // INDIVIDUAL COLUMN FILTERING
        boolean searchable = false;
        String column_search = null;
        QueryBuilder filter = new QueryBuilder();
        for (int i = 0; i < column.length; i++) {
            searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
            column_search = StringEscapeUtils.escapeHtml4(params.get("columns[" + i + "][search][value]"));
            if (searchable && !StringUtils.isEmpty(column_search)) {
                if (!filter.isEmpty()) {
                    filter.append(" OR ");
                }
                column_search = '%' +column_search.toLowerCase()+ '%';
                filter.append(" LOWER(").append(column[i]).append("::varchar) LIKE ?", column_search);
            }
        }
        if (!filter.isEmpty()) {
            from.append(from.query().toLowerCase().contains("where") ? " AND " : " WHERE ");
            from.append("(").append(filter).append(")");
        }
        queryCount.reset();
        queryCount.append("SELECT count("+column[0]+") ").append(from);
//        Logger.info("query count :"+ queryCount.query());
        long iFilteredTotal = Query.count(queryCount);
        // ORDERING
        String requestColumn = null;
        int columnIdx = 0;
        boolean firstOrder = true;
        for (int i = 0; i < column.length; i++) {
            requestColumn = StringEscapeUtils.escapeHtml4(params.get("order[" + i + "][column]"));
            if (StringUtils.isEmpty(requestColumn))
                continue;
           columnIdx = Integer.parseInt(requestColumn);
            boolean orderable = Boolean.parseBoolean(params.get("columns[" + columnIdx + "][orderable]"));
            if (orderable) {
                String order_status = params.get("order[" + i + "][dir]") != null ? params.get("order[" + i + "][dir]").toLowerCase():"";
                if (firstOrder) {
                    from.append(" ORDER BY ").append(column[columnIdx]).append(order_status.contains("desc")? " desc ":" asc ");
                    firstOrder = false;
                } else {
                    from.append(",").append(column[columnIdx]).append(order_status.contains("desc")? " desc ":" asc ");
                }
            }
        }

        if (start != null && length >= 0) {
            from.append(" OFFSET ? LIMIT ?", start, length);
        }
        select.append(from);
        List<String[]> data = Query.find(select, resultset).fetch();
        JsonObject output = new JsonObject();
        output.addProperty("draw", params.get("draw"));
        output.addProperty("recordsTotal", iTotal);
        output.addProperty("recordsFiltered", iFilteredTotal);
        output.add("data", gson.toJsonTree(data));
        return output;
    }

    static JsonObject datatableForensik(QueryBuilder select, QueryBuilder from_sub, final  QueryBuilder from_sub2, final String[] column, final ResultSetHandler<String[]> resultset) {
        final Integer start = params.get("start", Integer.class);
        final Integer length = params.get("length", Integer.class);
        QueryBuilder queryCount = new QueryBuilder("SELECT count("+column[0]+") ").append(from_sub).append(from_sub2);
        long iTotal = Query.count(queryCount);
        // FILTERING
//        StringBuilder filter = new StringBuilder();
        if (!CommonUtil.isEmpty(params.get("search[value]"))) {
            String search = '%' +params.get("search[value]").trim().toLowerCase()+ '%';
            from_sub.append(from_sub.query().toLowerCase().contains("where") ? " AND " : " WHERE ");
            from_sub.append("(");
            from_sub.append(" LOWER(").append("l.lls_id").append("::varchar) LIKE ?", search);
            from_sub.append(" OR ");
            from_sub.append(" LOWER(").append("p.pkt_nama").append("::varchar) LIKE ?", search);
            from_sub.append(")");
        }
        queryCount.reset();
        queryCount.append("SELECT count("+column[0]+") ").append(from_sub).append(from_sub2);
        long iFilteredTotal = Query.count(queryCount);

        from_sub.append(from_sub2);
        // ORDERING
//        StringBuilder sOrder = new StringBuilder();
        String requestColumn = null;
        boolean firstOrder = true;
        int columnIdx = 0;
        for (int i = 0; i < column.length; i++) {
            requestColumn = params.get("order[" + i + "][column]");
            if (StringUtils.isEmpty(requestColumn))
                continue;
            columnIdx = Integer.parseInt(requestColumn);
            boolean orderable = Boolean.parseBoolean(params.get("columns[" + i + "][orderable]"));
            if (orderable) {
                String order_status = column[columnIdx]+ ' ' +params.get("order[" + i + "][dir]");
                if (firstOrder) {
                    from_sub.append(" ORDER BY ").append(column[columnIdx]).append(order_status.contains("desc")? " desc ":" asc ");
                    firstOrder = false;
                } else {
                    from_sub.append(",").append(column[columnIdx]).append(order_status.contains("desc")? " desc ":" asc ");
                }
            }
        }
        if (start != null && length >= 0) {
            from_sub.append(" OFFSET ? LIMIT ?", start, length);
        }
        select.append(from_sub);
        if ("true".equals(Play.configuration.get("jpa.debugSQL")))
            Logger.info("[DataTableCtr] SQL: %s", select.query());
        List<String[]> data = Query.find(select, resultset).fetch();
        JsonObject output = new JsonObject();
        output.addProperty("draw", params.get("draw"));
        output.addProperty("recordsTotal", iTotal);
        output.addProperty("recordsFiltered", iFilteredTotal);
        output.add("data", gson.toJsonTree(data));
        return output;
    }

    /**Create by Andik
     * Improvement
     * ada tambahan columLike[] untuk flag apakah kolom dicari pakai LIKE atau pakai =
     * Untuk kolom yg numerik maka JANGAN pakai LIKE supaya speed cepet
     *
     * @param select
     * @param from
     * @param column
     * @param columnLike
     * @param resultset
     * @return
     */
    static JsonObject datatable2(QueryBuilder select, QueryBuilder from, final String[] column, final boolean[] columnLike,
                                 final ResultSetHandler<String[]> resultset) {
        // PAGING
        final Integer start = params.get("start", Integer.class);
        final Integer length = params.get("length", Integer.class);
        QueryBuilder queryCount = new QueryBuilder("SELECT count("+column[0]+") ").append(from);
        long iTotal = Query.count(queryCount);
        // FILTERING
        String search = params.get("search[value]");
        if (!CommonUtil.isEmpty(search)) {
            String searchNumber=null; //jika search berupa angka
            search= StringEscapeUtils.escapeHtml4(search.trim().toLowerCase());
            searchNumber=search.replaceAll(",", "");
            Double doubleValue=null;
            try
            {
                doubleValue=Double.parseDouble(searchNumber);
            }
            catch(Exception e)
            {
                searchNumber=null; //bukan angka
            }

            from.append(from.query().toLowerCase().contains("where")?"\n AND " : " WHERE ");
            from.append("( false ");
            for (int i = 0; i < column.length; i++) {
                boolean searchable = column[i].equals("lls_id") ? true : Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
                if (searchable) {
                    if(columnLike[i] && doubleValue==null)//jika angka, jangan pakai LIKE
                        from.append("\n OR to_tsvector('english'," + column[i] + ")")
                                .append("  @@ plainto_tsquery('english',?) ",
                                        search);
                    else
                    {
                        //pakai = dan untuk angka
                        if(!columnLike[i] && doubleValue!=null)
                            from.append("\n OR " + column[i]).append(" =? ",doubleValue);
                    }
                }
            }
            from.append(")");
        }
        // INDIVIDUAL COLUMN FILTERING
        boolean searchable = false;
        String column_search = null;
        QueryBuilder filter = new QueryBuilder();
        for (int i = 0; i < column.length; i++) {
            searchable = Boolean.parseBoolean(params.get("columns[" + i + "][searchable]"));
            column_search = params.get("columns[" + i + "][search][value]");
            if (searchable && !StringUtils.isEmpty(column_search)) {
                column_search = '%' +column_search.toLowerCase()+ '%';
                filter.append(" LOWER(").append(column[i]).append("::varchar) LIKE ?", column_search);
                if (i < (column.length - 1))
                    filter.append(" OR \n");
            }
        }
        if (!filter.isEmpty()) {
            from.append(from.toString().toLowerCase().contains("where") ? " AND " : " WHERE ");
            from.append("(").append(filter).append(")");
        }
        queryCount.reset();
        queryCount.append("SELECT count("+column[0]+") ").append(from);
        long iFilteredTotal = Query.count(queryCount);
        // ORDERING
        String requestColumn = null;
        int columnIdx = 0;
        boolean firstOrder = true;
        for (int i = 0; i < column.length; i++) {
            requestColumn = params.get("order[" + i + "][column]");
            if (StringUtils.isEmpty(requestColumn))
                continue;
            columnIdx = Integer.parseInt(requestColumn);
            boolean orderable = Boolean.parseBoolean(params.get("columns[" + i + "][orderable]"));
            if (orderable) {
                String order_status = params.get("order[" + i + "][dir]") != null ? params.get("order[" + i + "][dir]").toLowerCase():"";
                if (firstOrder) {
                    from.append(" ORDER BY ").append(column[columnIdx]).append(order_status.contains("desc")? " desc ":" asc ");
                    firstOrder = false;
                } else {
                    from.append(",").append(column[columnIdx]).append(order_status.contains("desc")? " desc ":" asc ");
                }
            }
        }

        if (start != null && length >= 0) {
            from.append("\n OFFSET ? LIMIT ?", start, length);
        }
        select.append(from);
        if ("true".equals(Play.configuration.get("jpa.debugSQL"))) {
            Logger.info("[DataTableCtr] SQL: %s", select.query());
        }
        List<String[]> data = Query.find(select, resultset).fetch();
        JsonObject output = new JsonObject();
        output.addProperty("draw", params.get("draw"));
        output.addProperty("recordsTotal", iTotal);
        output.addProperty("recordsFiltered", iFilteredTotal);
        output.add("data", gson.toJsonTree(data));
        return output;
    }
}
