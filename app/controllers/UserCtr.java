package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import controllers.security.AllowAccess;
import ext.StringFormat;
import jobs.SendMailJobNow;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.MailQueueDao;
import models.common.SesiPelatihan;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.LupaPasswordToken;
import models.sso.common.RekananSSO;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.Codec;
import play.libs.Json;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import play.mvc.Router;
import play.templates.Template;
import play.templates.TemplateLoader;
import models.sso.common.adp.util.DceSecurityV2;

import java.util.HashMap;
import java.util.Map;

/**
 * Kelas {@code UserCtr} merupakan kelas yang mengatur aksi login dan logout
 * pengguna, alur penggantian password pengguna, alur lupa password pengguna,
 * alur tampilan log akses pengguna, alur kotak masuk email pengguna, dan
 * aktifitas-aktifitas umum pengguna aplikasi
 *
 * @author Oscar Kurniawan
 * @author I Wayan Wiprayoga W
 */
public class UserCtr extends BasicCtr {       

	private static final Template templates = TemplateLoader.load("/email/lupa-password.html");
	
    /**
     * Fungsi {@code gantiPassword} menampilkan halaman ganti passowrd
     */
    @AllowAccess({Group.ALL_AUTHORIZED_USERS})
    public static void gantiPassword() {
        renderTemplate("user/user-ganti-password.html");
    }

  
    /**
     * Fungsi {@code logUser} menampilkan halaman log pengguna
     */
    @AllowAccess({Group.ADM_AGENCY, Group.ADM_PPE, Group.PANITIA, Group.REKANAN, Group.PPK, Group.HELPDESK,
			Group.VERIFIKATOR, Group.TRAINER, Group.AUDITOR, Group.PP, Group.UKPBJ, Group.KUPPBJ})
    public static void logUser() {
        renderArgs.put("userid", Active_user.current().userid); // dapatkan userid
		renderTemplate("user/user-log.html");
    }

	@AllowAccess({Group.ALL_AUTHORIZED_USERS})
    public static void SimpanPass(String pass, String pass_baru, String pass_baru2) throws Exception {
        checkAuthenticity();
        validation.required(pass).message(Messages.get("user.password_lama"));
        validation.required(pass_baru).message(Messages.get("user.password_baru"));
        validation.required(pass_baru2).message(Messages.get("user.password_ulangi"));
        validation.minSize(pass_baru, 8).message(Messages.get("user.panjang_password"));
       
        if (validation.hasErrors()) {
        	validation.keep();   
        	params.flash(); 
        } else if(!pass_baru.equals(pass_baru2)) {
        	params.flash(); 
        	flash.error("%s dan %s tidak sama", "Password Baru","Ulangi Password");
        }else {
        	Active_user active_user = Active_user.current();
        	String hashpass = DigestUtils.md5Hex(pass);
        	if(active_user.isRekanan()) {
					Rekanan rekanan = Rekanan.findByNamaUser(active_user.userid);
	        		boolean saved = true;
					//update password dilakukan jika Enable-inaproc set TRUE walaupun ia belum aktivasi, agar data reliable
					if(ConfigurationDao.isEnableInaproc() && !rekanan.isPerusahaanAsing()){
						JsonObject paramObj = new JsonObject();
						paramObj.addProperty("rekananId", active_user.rekananId);
						paramObj.addProperty("newpassword", DigestUtils.md5Hex(pass_baru));
						paramObj.addProperty("oldpasword", hashpass);
						paramObj.addProperty("repoId",ConfigurationDao.getRepoId());
						String param = URLs.encodePart(DceSecurityV2.encrypt(Json.toJson(paramObj)));
						String url_service = ConfigurationDao.getRestCentralService() + "/update-pass?q=" + param;
						try {
							WSRequest request = WS.url(url_service);
							request.setHeader("authentication", Play.secretKey);
							String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
							String response = DceSecurityV2.decrypt(content);
							if (!StringUtils.isEmpty(response)) { // jika ada error
								saved = false;
								flash.error("%s tidak berhasil diganti.", "Password");
							}
						} catch (Exception e) {
                            saved = false;
                            Logger.error(e, "%s tidak berhasil diganti.", "Password");
                        }
                    }
					else { // update lokal
						String passw = rekanan.passw;
						if(!passw.equals(hashpass)) {
							params.flash();
							flash.error("%s tidak sama", "Password Lama");
							saved = false;
						} else {
							rekanan.passw = DigestUtils.md5Hex(pass_baru);
							rekanan.save();
						}
					}
					if(saved) {
						// kirim email pemberitahuan
						try {
							MailQueueDao.kirimNotifikasiGantiPassword(rekanan.rkn_namauser, rekanan.rkn_email, newDate(), rekanan.rkn_id);
						} catch (Exception e) {
							Logger.error(e, "kirim notifikasi Ganti Password gagal");
						}
						flash.success("%s berhasil diubah.", "Password");
					}
        	}else {
        		Pegawai pegawai = Pegawai.findBy(active_user.userid);
        		String passw = pegawai.passw;
            	if(!passw.equals(hashpass)) {
            		params.flash(); 
            		flash.error("%s tidak sama", "Password Lama");
            	} else {
            		pegawai.passw = DigestUtils.md5Hex(pass_baru);
					pegawai.save();
					flash.success("%s berhasil diganti.", "Password");
            	}
        	}           	
        }        
        flash.keep();
        gantiPassword();
    }

    /**
     * Lupa password ini  merupakan proses yang cukup kritis.
     * Beberapa skenario harus diantisipasi. Seringkali orang dengan mudah klik 'lupa password'.
     * Untuk itu, proses Lupa Password ini dibuat lebih ketat dengan melibatkan email si pemilik akun.
     * Prosedur:
     * 1. Klik lupa password
     * 2. Isi: User Id, Email, Captcha
     * 3. SPSE mengirim link untuk reset password ke email ybs. Link mengandung 'token untuk reset password'
     * 4. Token tersebut disimpan pada tabel Usrtab dengan masa berlaku selama 1x24 jam
     * 5. Token tersebut unik terhadap user id. Artinya, jika dikirim email beberapa kali maka semua link pada email bisa diklik
     * 	  dengan masa berlaku sesuai token terakhir. Poin #5 ini penting karena orang sering klik 'lupa password' berulang-ulang
     * 		karena email belum sampai.
     * 
     * Fungsi {@code lupaPassword} menampilkan halaman lupa password
     */
    
    public static void lupaPassword() {
    	renderArgs.put("kapcaID",Codec.UUID());
		renderTemplate("user/password-lupa.html");
    }

	public static void gantiKode() {
		Map<String, Object> result = new HashMap<>(1);
		try {
			result.put("success", true);
			result.put("kapcaID", Codec.UUID());
		}catch(Exception e) {
			result.put("result", "Kesalahan saat meminta kode baru!");
			result.put("success", false);
		}
		renderJSON(result);
	}

	public static void lupaPasswordSubmit(String userid, String email, String kapcaID, String kapca, boolean isRekanan) throws Exception {
		checkAuthenticity();
		userid = userid.toUpperCase();

		validation.required(email).message("Email wajib diisi");
		validation.required(userid).message("User ID wajib diisi");

		if (validation.hasErrors()) {
			flash.error("Email dan User ID wajib diisi");
		}
		// cek kapca
		String kapcaValueFromCache = Cache.get(kapcaID, String.class);
		if (!kapca.equals(kapcaValueFromCache)) {
			flash.error("Kode Keamanan tidak sesuai.");
		} else {
			Cache.delete(kapcaID);
			if (isRekanan) {
				Rekanan user = Rekanan.findByNamaUser(userid);
				if (user != null) {//userid ditemukan
					if (user.rkn_email.equalsIgnoreCase(email)) {
						kirimLupaPassword(user);
						renderTemplate("user/password-lupa-terkirim.html");
					} else {
						flash.error("User ID tersebut tidak memiliki email yang dimaksud.");
					}
				} else {
					flash.error("User ID tidak ditemukan.");
				}
			} else {
				Pegawai user = Pegawai.findBy(userid);
				if (user != null) {//userid ditemukan
					if (user.peg_email.equalsIgnoreCase(email)) {
						kirimLupaPassword(user);
						flash.success("Email untuk penggantian password sudah terkirim ke: %s", email);
						renderTemplate("user/password-lupa-terkirim.html");
					} else {
						flash.error("User ID tersebut tidak memiliki email yang dimaksud.");
					}
				} else {
					flash.error("User ID tidak ditemukan.");
				}
			}
		}
		lupaPassword();
	}


	//mengirim email untuk ganti password
    private static void kirimLupaPassword(Rekanan user) throws Exception{
    	LupaPasswordToken token= user.requestLupaPasswordToken();    	
    	user.save();
    	Logger.debug("[LupaPassword] userId: %s, token: %s", user.rkn_namauser, user.reset_password);
		Map vars=new HashMap<String, String>(); 
		vars.put("email", user.rkn_email);
		vars.put("user", user);
		vars.put("tanggal", StringFormat.formatDateTime(BasicCtr.newDate()));
		//ini sebagai metode agar link yag di-generate memiliki expires selama 7 x 24 jam
		//pada url ditambahkan tanggal generate
		String signature = token.token;
		user.reset_password=signature;
		String url=getRequestHost()+Router.reverse("UserCtr.gantiPasswordViaEmail").add("signature", signature).add("isRekanan", 1).url;
		vars.put("link", url);
		vars.put("nama_lpse",ConfigurationDao.getNamaLpse());
		vars.put("subject", "Konfirmasi Penggantian Password");
		MailQueue mq=EmailManager.createEmail("\"Pengguna SPSE\" <" + user.rkn_email + '>',templates , vars);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id=Long.parseLong("0");
		try {
			await(new SendMailJobNow(mq).now());
			flash.success("Email untuk penggantian password sudah terkirim ke: %s", user.rkn_email);
		}catch (Exception e) {
			Logger.error(e, "Email untuk penggantian password gagal terkirim");
			flash.error("Email untuk penggantian password gagal dikirim");
		}
    }

	//mengirim email untuk ganti password
	private static void kirimLupaPassword(Pegawai user) throws Exception {
		LupaPasswordToken token = user.requestLupaPasswordToken();
		user.save();
		Logger.debug("[LupaPassword] userId: %s, token: %s", user.peg_namauser, user.reset_password);
		Map vars = new HashMap<String, String>();
		vars.put("email", user.peg_email);
		vars.put("user", user);
		vars.put("tanggal", StringFormat.formatDateTime(BasicCtr.newDate()));
		//ini sebagai metode agar link yag di-generate memiliki expires selama 7 x 24 jam
		//pada url ditambahkan tanggal generate
		String signature = token.token;
		user.reset_password = signature;
		String url = getRequestHost() + Router.reverse("UserCtr.gantiPasswordViaEmail").add("signature", signature).add("isRekanan", 0).url;
		vars.put("link", url);
		vars.put("nama_lpse", ConfigurationDao.getNamaLpse());
		vars.put("subject", "Konfirmasi Penggantian Password");
		MailQueue mq = EmailManager.createEmail("\"Pengguna SPSE\" <" + user.peg_email + '>', templates, vars);
		mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
		mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
		mq.lls_id = Long.parseLong("0");
		try {
			await(new SendMailJobNow(mq).now());
			flash.success("Email sudah dikirim");
		}catch (Exception e){
			Logger.error(e, "Email ganti password gagal terkirim");
			flash.error("Email gagal dikirim");
		}
	}

	/**
     * 
     * @param signature berisi date.getTime()-UUID dalam di-base-64 2kali
     */ 
    public static void gantiPasswordViaEmail(String signature, Boolean isRekanan){
		// validasi token signature
		String tokenformat = "%\"token\":\""+signature+"\"%";
		long count = Rekanan.count("reset_password ilike ?", tokenformat);
		long countPegawai = Pegawai.count("reset_password ilike ?", tokenformat);
		if(count == 0 && countPegawai == 0)
			forbidden("Anda tidak Berhak Akses Halaman ini");
	    renderArgs.put("isRekanan", isRekanan == null ? true : isRekanan);
	    renderArgs.put("signature", signature);
    	Logger.debug("[GANTI PASSWORD: signature %s", signature);
		renderTemplate("user/user-lupa-ganti-pass.html");
    }
    
    public static void gantiLupaPassword(){
		renderTemplate("user/user-lupa-ganti-pass.html");
    }

	public static void simpanLupaPass(String userid, String pass_baru, String pass_baru2, String signature, Boolean isRekanan) throws Exception {
    	checkAuthenticity();
		//signature digunakan jika halaman ini di-forward dari email
		validation.required(userid).message("User ID wajib diisi.");
		validation.required(pass_baru).message("Password Baru wajib diisi.");
		validation.required(pass_baru2).message("Ulangi Password wajib diisi.");
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
		} else if (!pass_baru.equals(pass_baru2)) {
			params.flash();
			flash.error("%s dan %s tidak sama", "Password baru", "Ulangi password");
		} else {
			boolean rekanan = isRekanan == null ? true : isRekanan;//fitur baru reset password utk pegawai, jadi default adalah rekanan
			Pegawai peg = null;
			Rekanan rkn = null;
			LupaPasswordToken token;
			String rkn_id = "";
			if (rekanan) {
				rkn = Rekanan.findByNamaUser(userid);
				token = rkn.getLupaPasswordToken();
				rkn_id = rkn.rkn_id.toString();
			} else {
				peg = Pegawai.findBy(userid);
				token = peg.getLupaPasswordToken();
			}
			String userIDSession = null;
			if (Active_user.current() != null) {
				userIDSession = Active_user.current().userid;
			}
			//user telah mengajukan reset password,maka cek link dengan signature di database, khusus jika orang ini tidak login
			if (userIDSession == null) {
				renderArgs.put("signature", signature);
				renderArgs.put("isRekanan", isRekanan == null ? true : isRekanan);
				if (token == null) {
					flash.error("Link untuk Ganti Password telah habis masa berlakunya");
					params.flash();
				} else if (token.isExpires()) {
					params.flash();
					flash.error("Link untuk Ganti Password telah habis masa berlakunya");
				} else if (!token.token.equals(signature)) {//apakah token sama
					params.flash();
					flash.error("Link untuk Ganti Password error atau telah habis masa berlakunya");
				}
			}
			if (ConfigurationDao.isEnableInaproc() && rekanan && !rkn.isPerusahaanAsing()) {
				try {
					HttpResponse httpResponse = WS.url(ConfigurationDao.getRestCentralService() + "/rekanan").setHeader("authentication", Play.secretKey)
							.setParameter("q", DceSecurityV2.encrypt(rkn_id))
							.timeout(ConfigurationDao.CONNECTION_TIMEOUT).getAsync().get();
					if (httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
						String response = DceSecurityV2.decrypt(httpResponse.getString());
						RekananSSO objRekanan = Json.fromJson(response, RekananSSO.class);
						if(objRekanan.disableuser != null && objRekanan.disableuser != 0)
							flash.error("Proses Lupa Password tidak dapat dilakukan karena status akun tersebut tidak aktif");
						else {
							String strOldpassw = objRekanan.passw;
							String strNewpassw = DigestUtils.md5Hex(pass_baru);
							JsonObject paramObj = new JsonObject();
							paramObj.addProperty("rekananId", rkn_id);
							paramObj.addProperty("newpassword", strNewpassw);
							paramObj.addProperty("oldpasword", strOldpassw);
							paramObj.addProperty("repoId", ConfigurationDao.getRepoId());
							httpResponse = WS.url(ConfigurationDao.getRestCentralService() + "/update-pass").setHeader("authentication", Play.secretKey)
									.setParameter("q", DceSecurityV2.encrypt(CommonUtil.toJson(paramObj)))
									.timeout(ConfigurationDao.CONNECTION_TIMEOUT).postAsync().get();
							if (httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
								String content = DceSecurityV2.decrypt(httpResponse.getString());
								flash.error(content);
							}
						}
					}
				} catch (Exception e) {
					Logger.error(e, e.getMessage());
					flash.error("Proses ganti %s gagal.", "Password");
				}
			}
			if (!flash.contains("error")) {
				if (rekanan) {
					// jika latihan tidak perlu update ke ADP, cukup ke lokal, jika prod harus update ke ADP tanpa ke db lokal
					rkn.passw = ConfigurationDao.isLatihan() ? DigestUtils.md5Hex(pass_baru):"";
					rkn.reset_password = null;
					rkn.save();
				} else { // pegawai
					peg.reset_password = null;
					peg.passw = DigestUtils.md5Hex(pass_baru);
					peg.save();
				}
				flash.success("%s berhasil diganti.", "Password");
			}
		}
		gantiLupaPassword();
	}


	/**
	 * Kelas {@code LoginResult} merupakan enumerasi status login pengguna
	 */
	public enum LoginResult {
	    INVALID_USER_PASSWORD("user.id_password_salah"),
		INVALID_USER_ID("user.id_salah"),
		INVALID_PASSWORD("user.password_salah"),
		SUCCESS("ser.log_berhasil"),
		DISABLED_USER("user.tdk_aktif"),
		FAILED("user.login_gagal!");

		public final String pesan; // pesan login

		// Constructor
		LoginResult(String pesan) {
			this.pesan = pesan;
		}

		public String getCaption()
		{
			return Messages.get(pesan);
		}
	}
	
	
	public static void sesiPelatihanPilih()
	{
		renderArgs.put("listSesi", SesiPelatihan.getActive());
		renderArgs.put("sesi", getSesiPelatihan());
		renderTemplate("user/sesi-pelatihan-pilih.html");
	}
	
	public static void sesiPelatihanPilihSubmit(SesiPelatihan sesi)
	{
		checkAuthenticity();
		response.removeCookie("sesiPelatihan");
		response.setCookie("sesiPelatihan", String.valueOf(sesi.id));
		SesiPelatihan sesiAktif = SesiPelatihan.findById(sesi.id);
		if(sesiAktif != null)
			Logger.info("[SetSesiPelatihan] %s : %s",  sesiAktif.nama, sesiAktif.getWaktuPelatihan());		;
		redirect(request.headers.get("referer").value());
	}
	
	/**
	 * Fungsi {@code viewActiveUser} digunakan untuk menampilkan halaman lihat daftar pengguna yang sedang aktif saat ini.
	 */
	@AllowAccess({Group.ADM_PPE,Group.HELPDESK})
	public static void activeUser() {
		renderArgs.put("list", Active_user.findAll());
		renderArgs.put("ppe", Active_user.current().isAdminPPE());
		renderTemplate("admin/utility/activeUser.html");
	}	
	
}