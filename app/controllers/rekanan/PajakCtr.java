package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenTypeCheck;
import models.common.Active_user;
import models.rekanan.Pajak;
import models.secman.Group;
import play.Logger;
import play.i18n.Messages;

public class PajakCtr extends BasicCtr {

 /**
     * Render halaman Data Penyedia - Pajak
     */
    
    @AllowAccess({Group.REKANAN})   
    public static void list() {   
        renderTemplate("rekanan/pajak.html");
    }

    /**
     * Render halaman edit Data Penyedia - Pajak Jika parameter id null, merujuk
     * kepada pembuatan pajak baru
     *
     * @param id
     */
    @AllowAccess({Group.REKANAN})
    public static void edit(Long id) {
        Pajak pajak;
		renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
        if (id == null) {
            pajak = new Pajak();
        } else {
            pajak = Pajak.findById(id);
            otorisasiDataPenyedia(pajak.rkn_id);// check otorisasi terhadap data pajak
        }
		renderArgs.put("pajak", pajak);
        renderTemplate("rekanan/pajak-edit.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void simpan(Pajak pajak){
    	checkAuthenticity();
		checkStatusMigrasi();
    	if(pajak.pjk_id != null) { // check untuk memastikan submit pajak adalah pemilik data
			Pajak obj = Pajak.findById(pajak.pjk_id);
			otorisasiDataPenyedia(obj.rkn_id);
		}
    	pajak.rkn_id = Active_user.current().rekananId;
    	pajak.pjk_periode = "Y"; // default tahunan
    	pajak.pjk_bulan = null;
		validation.valid(pajak);		
		if (validation.hasErrors()) {			
			validation.keep();
			params.flash();
			flash.error(Messages.get("rkn.data_gagal_tersimpan"));
			edit(pajak.pjk_id);
		} else {
			try {
				pajak.save();
				flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
				list();				
			}catch (Exception e) {
				Logger.error(e, "Data gagal tersimpan");
				flash.error(Messages.get("rkn.data_gagal_tersimpan"));
				edit(pajak.pjk_id);
			}
		}
		
     }

    /**
     * Fungsi {@code pajakHapusSubmit} digunakan untuk menghapus pajak yang
     * ada pada halaman daftar pajak
     */
    @AllowAccess({Group.REKANAN})
    public static void hapus(Long[] chkPjk) {
    	checkAuthenticity();
		checkStatusMigrasi();
        if (chkPjk != null && chkPjk.length > 0) { // jika tidak ada faq yang di-ceklist, maka tampilkan kembali halaman daftar FAQ
        	 for (Long val : chkPjk) {
        		 Pajak obj = Pajak.findById(val);
        		 otorisasiDataPenyedia(obj.rkn_id);
                 obj.delete();
             }
             flash.success("Data Terhapus");
        }       
        list();
    }
}