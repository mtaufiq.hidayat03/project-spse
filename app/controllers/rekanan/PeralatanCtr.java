package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.common.Status_kepemilikan;
import models.rekanan.Peralatan;
import models.secman.Group;
import play.data.validation.Valid;
import play.i18n.Messages;

public class PeralatanCtr extends BasicCtr {

	 /**
     * Render halaman Data Penyedia - Peralatan
     */
    @AllowAccess({Group.REKANAN})
    public static void list() {        
        renderTemplate("rekanan/peralatan.html");
    }

    /**
     * Render halaman edit Data Penyedia - Peralatan Jika parameter id null,
     * merujuk kepada pembuatan peralatan baru
     *
     * @param id id peralatan
     */
    @AllowAccess({Group.REKANAN})
    public static void edit(Long id) {
    	Peralatan peralatan;
        if (id == null) {
            peralatan = new Peralatan();
            peralatan.skp_id = Status_kepemilikan.MILIK_SENDIRI;
        } else {
            peralatan = Peralatan.findById(id);
            otorisasiDataPenyedia(peralatan.rkn_id); // check otorisasi terhadap data peralatan
        }
		renderArgs.put("peralatan", peralatan);
        renderTemplate("rekanan/peralatan-edit.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void simpan(@Valid Peralatan peralatan, Long status){
    	checkAuthenticity();
    	checkStatusMigrasi();
    	if(peralatan.alt_id != null) {
    		Peralatan obj = Peralatan.findById(peralatan.alt_id);
    		otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data peralatan
    	}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("rkn.data_gagal_tersimpan"));
			edit(peralatan.alt_id);
		} else {
			Status_kepemilikan skp = Status_kepemilikan.findById(status);    	
	    	peralatan.skp_id=skp;
	    	peralatan.rkn_id=Active_user.current().rekananId;
	    	peralatan.save();
			flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
			list();
		}
	} 

    
    @AllowAccess({Group.REKANAN})
    public static void hapus(Long[] alatChk) {
    	checkAuthenticity();
    	checkStatusMigrasi();
		if (alatChk != null && alatChk.length > 0) {
			for(Long val:alatChk) {
				Peralatan obj = Peralatan.findById(val);
				otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data peralatan
				obj.delete();
			}
			flash.success(Messages.get("rkn.data_berhasil_dihapus"));
		}
        list();
    }
}