package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.rekanan.MigrasiCV;
import models.rekanan.Staf_ahli;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import play.data.validation.Valid;
import play.i18n.Messages;

import java.util.List;

public class Staf_AhliCtr extends BasicCtr {
	/**
	 * Render halaman Data Penyedia - Staf Ahli
	 */
	@AllowAccess({Group.REKANAN})
	public static void list() {
		renderTemplate("rekanan/staf-ahli.html");
	}

	/**
	 * Render halaman edit Data Penyedia - Staf Ahli Jika parameter id null,
	 * merujuk kepada pembuatan staf ahli baru
	 *
	 * @param id id staf ahli
	 */
	@AllowAccess({Group.REKANAN})
	public static void edit(Long id) {
		Staf_ahli staf;
		if (id == null) {
			staf = new Staf_ahli();
		} else {
			staf = Staf_ahli.findById(id);
			otorisasiDataPenyedia(staf.rkn_id);// check otorisasi terhadap data tenaga ahli
		}
		if (CollectionUtils.isEmpty(staf.pengalaman))
			staf.pengalaman.add(new Staf_ahli.Cv_staf());
		if (CollectionUtils.isEmpty(staf.pendidikan))
			staf.pendidikan.add(new Staf_ahli.Cv_staf());
		if (CollectionUtils.isEmpty(staf.sertifikasi))
			staf.sertifikasi.add(new Staf_ahli.Cv_staf());
		if (CollectionUtils.isEmpty(staf.bahasa)){
			staf.bahasa.add(new Staf_ahli.Cv_staf());
		}
		renderArgs.put("staf", staf);
		renderArgs.put("popup", false);
		renderArgs.put("cvA", staf.pengalaman);
		renderArgs.put("cvB", staf.bahasa);
		renderArgs.put("cvD", staf.pendidikan);
		renderArgs.put("cvL", staf.sertifikasi);
		renderTemplate("rekanan/staf-ahli-edit.html");
	}

	@AllowAccess({ Group.REKANAN })
	public static void simpan( @Valid Staf_ahli staf, List<Long>cvId,List<String> cvWaktu,List <String> cvDetil, List <String> cvKat,
							   List<Long>cvId2,List<String> cvWaktu2,List <String> cvDetil2, List <String> cvKat2,
							   List<Long>cvId3,List<String> cvWaktu3,List <String> cvDetil3, List <String> cvKat3,
							   List<Long>cvId4,List <String> cvDetil4, List <String> cvKat4) {
		checkAuthenticity();
		checkStatusMigrasi();
		if(staf.sta_id != null) {
			Staf_ahli obj = Staf_ahli.findById(staf.sta_id);
			otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data tenaga ahli
		}
		if (staf.sta_tgllahir != null) {
			DateTime datetime = new DateTime();
			DateTime tgl_lahir = new DateTime(staf.sta_tgllahir.getTime());
			int now = datetime.getYear();
			int lahir = tgl_lahir.getYear();
			int selisih = now - lahir;
			validation.min("staf.sta_tgllahir", selisih, 17).message(Messages.get("rkn.tanggal_lahir_tidak_valid"));
		}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("rkn.data_gagal_tersimpan"));
			edit(staf.sta_id);
		} else {
			staf.rkn_id = Active_user.current().rekananId;
			staf.validate(cvWaktu, cvId, cvDetil, cvKat, "a");
			staf.validate(cvWaktu2, cvId2, cvDetil2, cvKat2,"d");
			staf.validate(cvWaktu3, cvId3, cvDetil3, cvKat3,"l");
			staf.validate(null, cvId4, cvDetil4, cvKat4, "b");
			staf.save();
			flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
			list();
		}
	}


	@AllowAccess({ Group.REKANAN })
	public static void hapus(List<Long> chkStaf) {
		checkAuthenticity();
		checkStatusMigrasi();
		if(!CommonUtil.isEmpty(chkStaf)) {
			for(Long val:chkStaf) {
				Staf_ahli obj = Staf_ahli.findById(val);
				otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data tenaga ahli
				obj.delete();
			}
			flash.success(Messages.get("rkn.data_berhasil_dihapus"));
		}
		list();
	}

	public static void migrasiCv() {
		response.writeChunk(Messages.get("rkn.migrasi_cv_sedang_dilakukan")+"\n");
		MigrasiCV.update();
		response.writeChunk(Messages.get("rkn.migrasi_cv_sudah_dilakukan")+"\n");
	}

}
