package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenTypeCheck;
import models.common.Active_user;
import models.common.Kualifikasi;
import models.rekanan.Ijin_usaha;
import models.secman.Group;
import play.Logger;
import play.data.validation.Validation;
import play.i18n.Messages;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IjinUsahaCtr extends BasicCtr {

	  /**
     * Render halaman Data Penyedia - Izin Usaha
     */
    @AllowAccess({Group.REKANAN})    
    public static void list() {
        renderTemplate("rekanan/ijin-usaha.html");
    }



    /**
     * Render halaman edit Data Penyedia - Izin Usaha Jika parameter id null,
     * merujuk kepada pembuatan izin usaha baru
     *
     * @param id id izin usaha
     */
    @AllowAccess({Group.REKANAN})
    public static void edit(Long id) {
		Ijin_usaha ijin;
		renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
		if (id == null) {
			ijin = new Ijin_usaha();
			ijin.kls_id = Kualifikasi.NON_KECIL.id;
		} else {			
			ijin = Ijin_usaha.findById(id);
			otorisasiDataPenyedia(ijin.rkn_id);// check otorisasi terhadap data izin usaha
		}
		renderArgs.put("ijin", ijin);
        renderTemplate("rekanan/ijin-usaha-edit.html");
    }


    /**
     * Simpan izin usaha
     */
    @AllowAccess({Group.REKANAN})
    public static void simpan(Ijin_usaha ijin_usaha) {
		checkAuthenticity();
		checkStatusMigrasi();
		if(ijin_usaha.status_berlaku != 1 && ijin_usaha.ius_berlaku == null){
			Validation.addError("ijin_usaha.ius_berlaku", Messages.get("rkn.batas_berlaku"));
		}
		validation.valid(ijin_usaha);
		if(validation.hasErrors()) {
			validation.keep();
			params.flash();
			edit(ijin_usaha.ius_id);
		} else {
			ijin_usaha.rkn_id= Active_user.current().rekananId;
			if(ijin_usaha.ius_id != null) { // check untuk memastikan submit izin usaha adalah pemilik data
				Ijin_usaha obj = Ijin_usaha.findById(ijin_usaha.ius_id);
				otorisasiDataPenyedia(obj.rkn_id);
			}

			if(ijin_usaha.status_berlaku==1){
				// don't get confuse, this is SIKaP standard
				String date = "31-12-9999";
				DateFormat format = new SimpleDateFormat("dd-mm-yyyy");
				try {
					Date maxDate = (Date) format.parse(date);
					ijin_usaha.ius_berlaku = maxDate;
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				ijin_usaha.save();
				flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
				list();
			}catch (Exception e) {
				Logger.error(e,  "Data Gagal Tersimpan");
				flash.error(Messages.get("rkn.data_gagal_tersimpan"));
				edit(ijin_usaha.ius_id);
			}
		}


		
    }

   
    /**
     * Hapus izin Usaha
     *
     */
    @AllowAccess({Group.REKANAN})
	public static void hapus(Long[] chkush) {
		checkAuthenticity();
		checkStatusMigrasi();
		String errmsg = null;
		if (chkush != null && chkush.length > 0) { 
			for (Long val : chkush) {
				try {
					Ijin_usaha obj = Ijin_usaha.findById(val);
					otorisasiDataPenyedia(obj.rkn_id);
					obj.delete();
				} catch (Exception e) {
					Logger.error(e, "Hapus Izin Usaha gagal");
					errmsg = e.getMessage();
				}
			}
			if (errmsg == null)
				flash.success("Data Terhapus");
			else if (errmsg.toLowerCase().contains("foreign key")) {
				flash.error("Gagal hapus data <br> Data sudah digunakan");
			} else
				flash.error("<h3>Maaf ada error</h3><br>" + errmsg);
		}
		list();
	}
}
