package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.rekanan.Pengurus;
import models.secman.Group;
import play.data.validation.Valid;
import play.i18n.Messages;

public class PengurusCtr extends BasicCtr {
	 /**
     * Render halaman Data Penyedia - pengurus
     */
    @AllowAccess({Group.REKANAN})
    public static void list() {
        renderTemplate("rekanan/pengurus.html");
    }
    
        /**
     * Render halaman edit Data Penyedia - Pengurus Jika parameter id null,
     * merujuk kepada pembuatan pengurus baru
     *
     * @param id id pengurus
     */
    @AllowAccess({Group.REKANAN})
    public static void edit(Long id) {
        Pengurus pengurus;
        if (id == null) {
            pengurus = new Pengurus();
        } else {
            pengurus = Pengurus.findById(id);
            otorisasiDataPenyedia(pengurus.rkn_id);// check otorisasi terhadap data pengurus
        }
		renderArgs.put("pengurus", pengurus);
        renderTemplate("rekanan/pengurus-edit.html");
    }
    
    @AllowAccess({Group.REKANAN})
    public static void simpan(@Valid Pengurus pengurus){
    	checkAuthenticity();
    	checkStatusMigrasi();
    	if(pengurus.pgr_id != null) {
    		Pengurus obj= Pengurus.findById(pengurus.pgr_id);
    		otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data pengurus
    	}
    	if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("rkn.data_gagal_tersimpan"));
			edit(pengurus.pgr_id);
		} else {
			pengurus.rkn_id = Active_user.current().rekananId;
			pengurus.save();
			flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
			list();
		}
	}
 
    
    @AllowAccess({Group.REKANAN})
    public static void hapus(Long[] chkPgr) {
    	checkAuthenticity();
    	checkStatusMigrasi();
    	if(chkPgr != null && chkPgr.length > 0) {
    		for(Long val:chkPgr) {
    			Pengurus obj = Pengurus.findById(val);
    			otorisasiDataPenyedia(obj.rkn_id); // check otorisasi terhadap data pengurus
    			obj.delete();
    		}
			flash.success(Messages.get("rkn.data_berhasil_dihapus"));
    	}
        list();
    }
}
