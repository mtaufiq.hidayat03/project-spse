package controllers.rekanan;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.jcommon.secure.encrypt2.InMemoryKeyCipherEngine;
import models.jcommon.secure.encrypt2.InMemoryRSAKey;
import models.jcommon.util.CommonUtil;
import models.rekanan.HistoryMigrasiSikap;
import models.rekanan.MigrasiSikap;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.codec.binary.Base64;
import play.Logger;
import play.i18n.Messages;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;

import java.util.Date;
import java.util.UUID;

public class MigrasiSikapCtr extends BasicCtr {

	private static final String URL_EDIT_SIKAP = BasicCtr.SIKAP_URL + "/application/spseLogin";
	private static final String URL_SIKAP_CERT = BasicCtr.SIKAP_URL + "/migrasidatapenyedia/getServerCert";

    /**
     * Render halaman Data Penyedia - Identitas Perusahaan Default halaman yang
     * ditampilkan saat user REKANAN mengklik menu Data Penyedia
     */
    @AllowAccess({Group.REKANAN})
    public static void index() {
        Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
		renderArgs.put("rekanan", rekanan);
        HistoryMigrasiSikap history = null;
        if(!rekanan.isBelumMigrasiSikap()) {
        	history = HistoryMigrasiSikap.find("rkn_id=?", rekanan.rkn_id).first();
			renderArgs.put("history", history);
        }
        renderTemplate("rekanan/migrasi-sikap.html");
    }

    /**
     * Render Integrasi SIKaP
     * hanya tarik dari SIKaP, tidak ada proses kirim
     */
    @AllowAccess({Group.REKANAN})
    public static void submit() throws Exception {
	    Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
	    checkAuthenticity();
	    if(rekanan.isSelesaiMigrasiSikap()) {
		    flash.error(Messages.get("rkn.sudah_migrasi_sikap"));
	    } else {
		    if(rekanan.isBelumMigrasiSikap()) { // tarik data data
//			    try {
				    Logger.info("Tarik data dari SIKaP started");
				    String status = MigrasiSikap.tarikDataPenyedia(rekanan, new Date());
				    if(status.equals("1") || status.equals("2")) {
					    Active_user active_user = Active_user.current();
					    active_user.statusMigrasi = Rekanan.MIGRASI_SIKAP_SELESAI;
					    if(status.equals("1")) { // bedakan message supaya sedikit mempermudah forensik
						    Logger.info("Tarik data data SIKaP success");
						    flash.success(Messages.get("rkn.data_berhasil_integrasi_sikap"));
					    } else {
						    Logger.info("Data sudah pernah ditarik ke SIKaP");
						    flash.success(Messages.get("rkn.data_sudah_berhasil_integrasi_sikap"));
					    }
				    } else if(status.equals("0")) {
					    flash.error(Messages.get("rkn.data_gagal_update_sikap"));
				    } else {
					    // TODO: buat dan ambil pesan gagal dari SIKaP
					    flash.error(Messages.get("rkn.data_gagal_update_sikap_karena")+ status);
				    }
//			    } catch (Exception e) {
//				    flash.error("Ada kesalahan sistem. Mohon hubungi administrator sistem.");
//				    Logger.error(e, "Gagal tarik data dari SIKaP");
//			    }
		    }
	    }
	    index();
    }
   /* @AllowAccess({Group.REKANAN})
    public static void submit() {
        Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
    	checkAuthenticity();
    	if(rekanan.isSelesaiMigrasiSikap()) {
    		flash.error("Anda sudah selesai melakukan migrasi ke SIKaP");
    	} else {
    		if(rekanan.isBelumMigrasiSikap()) { // kirim data
	    		try {
	    			Logger.info("Kirim data ke SIKaP started");
	    			int status = MigrasiSikap.kirimDataPenyedia(rekanan, new Date());
	    			if(status == 1 || status == 2) { // sukses
	    				Active_user active_user = Active_user.current();
	    				active_user.statusMigrasi = Rekanan.MIGRASI_SIKAP_PROSES;
	    				if(status == 1) { // bedakan message supaya sedikit mempermudah forensik
		    				Logger.info("Kirim data ke SIKaP success");
		    				flash.success("Data berhasil terkirim ke SIKaP.");
	    				} else {
	    					Logger.info("Data sudah pernah dikirim ke SIKaP");
		    				flash.success("Data sudah terkirim ke SIKaP.");
	    				}
	    			} else {
	    				// TODO: buat dan ambil pesan gagal dari SIKaP
	    				flash.error("Gagal mengirim data ke SIKaP. Silakan coba beberapa menit lagi.");
	    			}
	    		} catch (Exception e) {
	    			flash.error("Ada kesalahan sistem. Mohon hubungi administrator sistem.");
	    			Logger.error(e, "Gagal kirim data ke SIKaP");
	    		}
    		} else { // tarik data
    			try {
	    			Logger.info("Tarik data dari SIKaP started");
	    			String status = MigrasiSikap.tarikDataPenyedia(rekanan, new Date());
				    if(status.equals("1") || status.equals("2")) {
	    				Active_user active_user = Active_user.current();
	    				active_user.statusMigrasi = Rekanan.MIGRASI_SIKAP_SELESAI;
	    				if(status.equals("1")) { // bedakan message supaya sedikit mempermudah forensik
		    				Logger.info("Tarik data data SIKaP success");
		    				flash.success("Data berhasil ditarik dari SIKaP.");
	    				} else {
	    					Logger.info("Data sudah pernah ditarik ke SIKaP");
		    				flash.success("Data sudah ditarik dari SIKaP.");
	    				}
	    			} else if(status.equals("0")) {
	    				flash.error("Gagal mengupdate data dari SIKaP. Silakan coba beberapa menit lagi.");
    				} else {
	    				// TODO: buat dan ambil pesan gagal dari SIKaP
	    				flash.error("Gagal mengupdate data dari SIKaP. Mohon koreksi data Anda di SIKaP terlebih dahulu: "+ status);
	    			}
	    		} catch (Exception e) {
	    			flash.error("Ada kesalahan sistem. Mohon hubungi administrator sistem.");
	    			Logger.error(e, "Gagal tarik data dari SIKaP");
	    		}
    		}

    	}
        index();
    }*/

    /**
     * Redirect penyedia ke aplikasi SIKaP dalam kondisi ter-logged in
     */
    @AllowAccess({Group.REKANAN})
    public static void editData() {
    	Active_user active_user = Active_user.current();
    	if(!active_user.isSelesaiMigrasi()) {
    		flash.error("Anda belum melakukan migrasi ke SIKaP");
    		index();
    	} else {
			try {
				WSRequest request = WS.url(URL_SIKAP_CERT);
	    		int repoId = ConfigurationDao.getRepoId();
	    		// generate key pair
	    		InMemoryRSAKey key = InMemoryRSAKey.getKey();
	    		// ambil certificate server
	    		String response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).setParameter("cert", key.getPublicKeyEncoded())
						.setParameter("repoId", repoId).post().getString();
	    		Logger.info("response = %s", response);
	    		InMemoryKeyCipherEngine decryptEngine = InMemoryKeyCipherEngine.getDecyptEngine(key.idxKey, true);
	    		String decodedResponse = new String(decryptEngine.doCrypto(Base64.decodeBase64(response)));
	    		Logger.info("decodedResponse = %s ", decodedResponse);
	    		JsonParser parser = new JsonParser();
	    		JsonObject jsonResponse = (JsonObject) parser.parse(decodedResponse);
	            if(jsonResponse.get("status") == null || jsonResponse.get("status").getAsInt() == 0) {
	            	throw new Exception(decodedResponse);
	            }
	            String serverCert = jsonResponse.get("cert").getAsString();
	            String sessionKey = jsonResponse.get("k").getAsString();

	            // prepare parameters
	    		Long rekananId = active_user.rekananId;
	    		String randomString = UUID.randomUUID().toString();
	    		long timestamp = System.currentTimeMillis(); // tidak pakai basic ctr.newDate karena SIKaP tidak kenal sesi training
	    		JsonObject jsonParam = new JsonObject();
	    		jsonParam.addProperty("userId", rekananId);
	    		jsonParam.addProperty("randomString", randomString);
	    		jsonParam.addProperty("timestamp", timestamp);
	    		// generate url
	    		InMemoryKeyCipherEngine encryptEngine = InMemoryKeyCipherEngine.getEncyptEngine(serverCert, true);
	    		String param = URLs.encodePart(Base64.encodeBase64String(encryptEngine.doCrypto(CommonUtil.toJson(jsonParam).getBytes())));
	    		String fullUrl = URL_EDIT_SIKAP + "?q=" + param + "&k=" + sessionKey;
	    		Logger.debug("URL EDIT SIKaP %s", fullUrl);
	    		redirect(fullUrl);
    		} catch (Exception e) {
    			Logger.error(e, "Gagal generate url login ke SIKaP");
        		flash.error("Gagal menghubungi SIKaP. Mohon coba beberapa saat lagi.");
        		index();
    		}
    	}
    }

    /**
     * Proses sinkronisasi data Penyedia dari SIKaP
     */
    @AllowAccess({Group.REKANAN})
    public static void update() throws Exception {
        Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
    	checkAuthenticity();
    	if(!rekanan.isSelesaiMigrasiSikap()) {
    		flash.error("Anda belum melakukan migrasi ke SIKaP");
    	} else {
//			try {
    			Logger.info("Update data dari SIKaP started");
    			String status = MigrasiSikap.updateDataPenyedia(rekanan);
    			if(status.equals("1")) {
    				Logger.info("Update data data SIKaP success");
    				flash.success("Data berhasil diperbaharui dari SIKaP");
    			} else if(status.equals("0")) {
    				// TODO: buat dan ambil pesan gagal dari SIKaP
    				flash.error("Gagal update data dari SIKaP. Silakan coba beberapa menit lagi.");
    			} else {
    				// TODO: buat dan ambil pesan gagal dari SIKaP
    				flash.error("Gagal mengupdate data dari SIKaP. Mohon koreksi data Anda di SIKaP terlebih dahulu: "+ status);
    			}
//    		} catch (Exception e) {
//    			flash.error("Ada kesalahan sistem. Mohon hubungi administrator sistem.");
//    			Logger.error(e, "Gagal update data dari SIKaP");
//    		}
			
    	}
        index();
    }

}
