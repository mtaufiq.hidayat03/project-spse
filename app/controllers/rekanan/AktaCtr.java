package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenTypeCheck;
import models.common.Active_user;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.rekanan.Landasan_hukum;
import models.secman.Group;
import play.Logger;
import play.data.validation.Valid;
import play.i18n.Messages;

import java.util.List;

public class AktaCtr extends BasicCtr {
	
	 /**
     * Render halaman Data Penyedia - Akta
     */
    @AllowAccess({Group.REKANAN})   
    public static void list() {
    	List<Landasan_hukum> landasan_hukumList = Landasan_hukum.find("rkn_id = ? order by lhk_tanggal asc ", Active_user.current().rekananId).fetch(2);
		renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
		if(!CommonUtil.isEmpty(landasan_hukumList)) {
			renderArgs.put("akta",landasan_hukumList.get(0));
	        if(landasan_hukumList.size() > 1)
				renderArgs.put("akta_akhir",landasan_hukumList.get(1));
    	}     
    	renderTemplate("rekanan/akta.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void simpan(@Valid Landasan_hukum akta,Landasan_hukum akta_akhir) {
    	checkAuthenticity();
		checkStatusMigrasi();   	
    	if (validation.hasErrors()) {
    		validation.keep();
    		params.flash();
    		flash.error(Messages.get("rkn.data_gagal_tersimpan"));
    		list();
    	}
    	try {
    		Landasan_hukum.simpan(akta, akta_akhir, Active_user.current().rekananId);
			flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
    	}catch (Exception e) {
    		Logger.error("@simpan akta : %s", e.getMessage());
    		e.printStackTrace();
    		flash.error(Messages.get("rkn.data_gagal_tersimpan"));
    	}	
		list(); 
    }
    
    @AllowAccess({Group.REKANAN})
    public static void delete_file(Long id, Integer versi) {
		checkStatusMigrasi();
    	BlobTable.delete("blb_id_content=? and blb_versi=?", id, versi);
    }
}