package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.rekanan.Pemilik;
import models.secman.Group;
import play.data.validation.Valid;
import play.i18n.Messages;

public class PemilikCtr extends BasicCtr {
	/**
     * Render halaman Data Penyedia - Pemilik
     */
    @AllowAccess({Group.REKANAN})
    public static void list() {
        renderTemplate("rekanan/pemilik.html");
    }

    /**
     * Render halaman edit Data Penyedia - Pemilik Jika parameter id null,
     * merujuk kepada pembuatan pemilik baru
     *
     * @param id id pemilik
     */
    @AllowAccess({Group.REKANAN})
    public static void edit(Long id) {
        Pemilik pemilik;       
        if (id == null) {
            pemilik = new Pemilik();
			pemilik.pml_satuan = 1;
        } else {
            pemilik = Pemilik.findById(id);
            otorisasiDataPenyedia(pemilik.rkn_id);// check otorisasi terhadap data pemilik
        }
		renderArgs.put("pemilik", pemilik);
        renderTemplate("rekanan/pemilik-edit.html");
    }
    
    @AllowAccess({Group.REKANAN})
    public static void simpan(@Valid Pemilik pemilik){
    	checkAuthenticity();
		checkStatusMigrasi();
    	if(pemilik.pml_id != null) {
    		Pemilik obj = Pemilik.findById(pemilik.pml_id);
    		otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data pemilik
    	}
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			flash.error(Messages.get("rkn.data_gagal_tersimpan"));
			edit(pemilik.pml_id);
		} else {
            pemilik.rkn_id=Active_user.current().rekananId;
			pemilik.save();
			flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
			list();
		}
	}

    @AllowAccess({Group.REKANAN})
    public static void hapus(Long[] chkpml){
    	checkAuthenticity();
		checkStatusMigrasi();
    	if(chkpml != null && chkpml.length > 0) {
    		for(Long val:chkpml) {
    			Pemilik obj = Pemilik.findById(val);
    			otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data pemilik
    			obj.delete();
    		}
    	}
        list();
    }
}