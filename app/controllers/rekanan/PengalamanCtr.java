package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.rekanan.Pengalaman;
import models.secman.Group;
import play.data.validation.Valid;
import play.i18n.Messages;

public class PengalamanCtr extends BasicCtr {
	/**
     * Render halaman Data Penyedia - Pengalaman
     */
    @AllowAccess({Group.REKANAN})
    public static void list() {
        renderTemplate("rekanan/pengalaman.html");
    }

    /**
     * Render halaman edit Data Penyedia - Pengalaman Jika parameter id null,
     * merujuk kepada pembuatan pengalaman baru
     *
     * @param id id pengalaman
     */
    @AllowAccess({Group.REKANAN})
    public static void edit(Long id) {
    	Pengalaman pengalaman;
        if (id == null) {
            pengalaman = new Pengalaman();
        } else {
            pengalaman = Pengalaman.findById(id);
            otorisasiDataPenyedia(pengalaman.rkn_id);// check otorisasi terhadap data pengalaman
        }
		renderArgs.put("pengalaman", pengalaman);
        renderTemplate("rekanan/pengalaman-edit.html");
    }

    /**
     * Submit edit Data Penyedia - Pengalaman
     *
     */
  

    @AllowAccess({Group.REKANAN})
    public static void simpan(@Valid Pengalaman pengalaman){
    	checkAuthenticity();
    	checkStatusMigrasi();
    	if(pengalaman.pgn_id != null) {
    		Pengalaman obj = Pengalaman.findById(pengalaman.pgn_id);
    		otorisasiDataPenyedia(obj.rkn_id);// check otorisasi terhadap data pengalaman
    	}
    	if(pengalaman.pgl_persenprogress != null && (pengalaman.pgl_persenprogress > 100 || pengalaman.pgl_persenprogress < 0)){
		    validation.addError("pengalaman.pgl_persenprogress", Messages.get("rkn.persentase_pelaksanaan_tidak_valid"));
	    }
	    if (pengalaman.pgl_slsba != null && pengalaman.pgl_tglkontrak != null && pengalaman.pgl_slsba.before(pengalaman.pgl_tglkontrak)) {
		    validation.addError("pengalaman.pgl_slsba", Messages.get("rkn.tanggal_serah_terima"));
	    }
	    validation.valid(pengalaman);
		if (validation.hasErrors()) {
			validation.keep();
			params.flash();
			/*StringBuilder sb = new StringBuilder();
			for (play.data.validation.Error err : validation.errors()) {
				if (!StringUtils.isEmpty(err.getKey())) {
					sb.append("<br/>").append(err.message());
				}
			}*/
			flash.error(Messages.get("rkn.data_gagal_tersimpan"));
			edit(pengalaman.pgn_id);
		} else {
			pengalaman.rkn_id = Active_user.current().rekananId;	
			pengalaman.pgl_tglprogress = pengalaman.pgl_persenprogress.intValue() != 100 ? BasicCtr.newDate():null;        	
			pengalaman.save();
			flash.success(Messages.get("rkn.data_berhasil_tersimpan"));
            list();
		}
   }

    @AllowAccess({Group.REKANAN})
    public static void hapus(Long[] pengalamanChk) {
    	checkAuthenticity();
    	checkStatusMigrasi();
    	if(pengalamanChk != null && pengalamanChk.length > 0) {
    		for(Long val:pengalamanChk) {
    			Pengalaman obj = Pengalaman.findById(val);
    			otorisasiDataPenyedia(obj.rkn_id); // check otorisasi terhadap data pengalaman
    			obj.delete();
    		}
            flash.success(Messages.get("rkn.data_berhasil_dihapus"));
    	}
        list();
    }
}