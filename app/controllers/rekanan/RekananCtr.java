package controllers.rekanan;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.common.Active_user;
import models.jcommon.mail.MailQueue;
import models.jcommon.mail.MailQueue.BACA_STATUS;
import models.jcommon.util.CommonUtil;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;

public class RekananCtr extends BasicCtr {

    /**
     * Render halaman Data Penyedia - Identitas Perusahaan Default halaman yang
     * ditampilkan saat user REKANAN mengklik menu Data Penyedia
     */
    @AllowAccess({Group.REKANAN})
    public static void identitas() {
        Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
        renderArgs.put("rekanan", rekanan);
        if(!StringUtils.isEmpty(rekanan.edited_data)) {
            renderArgs.put("rekananEdit", CommonUtil.fromJson(rekanan.edited_data, Rekanan.class));
        }
        renderTemplate("rekanan/identitas.html");
    }

    /**
     * Render halaman Data Penyedia - Identitas Perusahaan Default halaman yang
     * ditampilkan saat user REKANAN mengklik menu Data Penyedia
     */
    @AllowAccess({Group.REKANAN})
    public static void identitasSubmit(Rekanan rekanan, Long propinsi) {
    	checkAuthenticity();
    	validation.required("rekanan.rkn_telepon", rekanan.rkn_telepon);
    	if(rekanan.isPerusahaanAsing()) {
            validation.required("rekanan.ngr_id", rekanan.ngr_id);
            validation.required("rekanan.kota", rekanan.kota);
        } else {
            validation.required("rekanan.kbp_id", rekanan.kbp_id);
            validation.required("propinsi", propinsi);
        }
    	validation.url("rekanan.rkn_website", rekanan.rkn_website);
        validation.email("rekanan.rkn_emailpusat", rekanan.rkn_emailpusat);
        validation.phone("rekanan.rkn_mobile_phone", rekanan.rkn_mobile_phone);
        validation.phone("rekanan.rkn_telepon", rekanan.rkn_telepon);
        validation.phone("rekanan.rkn_telppusat", rekanan.rkn_telppusat);
        validation.phone("rekanan.rkn_fax", rekanan.rkn_fax);
        validation.phone("rekanan.rkn_faxpusat", rekanan.rkn_faxpusat);
        validation.match("rekanan.rkn_kodepos", rekanan.rkn_kodepos, "[0-9]{5}");
        if (rekanan.rkn_statcabang != null ) {
            if(rekanan.rkn_statcabang.equals("P")) {
                rekanan.rkn_almtpusat = null;
                rekanan.rkn_emailpusat = null;
                rekanan.rkn_telppusat = null;
                rekanan.rkn_faxpusat = null;
            } else if(rekanan.rkn_statcabang.equals("C")) {
                validation.required("rekanan.rkn_almtpusat", rekanan.rkn_almtpusat);
                validation.required("rekanan.rkn_emailpusat", rekanan.rkn_emailpusat);
            }
        }
        if(validation.hasErrors()) {
    		validation.keep();
    		params.flash();
    		flash.error(Messages.get("rkn.data_gagal_tersimpan"));
    	}
    	else {
    		try {
                Rekanan obj = Rekanan.findById(Active_user.current().rekananId);
                String editedData = CommonUtil.toJson(rekanan);
                obj.edited_data = editedData;
                obj.save();
    			flash.success(Messages.get("rkn.perubahan_data_disimpan"));
    		}catch (Exception e) {
    			flash.error(Messages.get("rkn.simpan_data_identitas_gagal"));
    			Logger.error(e, "Simpan Data Identitas Gagal");
    		}
    	}
        identitas();
    }

    /**
     * Batalkan permintaan perubahan data rekanan
     */
    @AllowAccess({Group.REKANAN})
    public static void identitasBatal() {
        Rekanan obj = Rekanan.findById(Active_user.current().rekananId);
        obj.edited_data = null;
        obj.save();
        flash.success(Messages.get("rkn.perubahan_data_dibatalkan"));
        identitas();
    }

    /**
     * Render halaman Inbox
     */
    @AllowAccess({Group.REKANAN})
    public static void inbox(BACA_STATUS status) {
        renderArgs.put("status", status);
        renderTemplate("rekanan/inbox.html");
    }

    @AllowAccess({Group.REKANAN})
    public static void inboxDetil(Long id) {
        MailQueue mail = MailQueue.findById(id);
        otorisasiDataPenyedia(mail.rkn_id);
        //set status = dari UNREAD menjadi READ
		String message = mail.getRawBody();
//        Logger.debug(message);
		String subject = mail.subject;
        if(mail.bcstat==BACA_STATUS.UNREAD)
        {
        	mail.bcstat=MailQueue.BACA_STATUS.READ;
        	mail.save();
        }
        renderArgs.put("message",message);
        renderArgs.put("subject", subject);
        renderTemplate("rekanan/inbox-detil.html");
    }
}
