package controllers.nonSpk;

import controllers.BasicCtr;
import controllers.nonlelang.PaketPlCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.agency.Anggaran;
import models.agency.Paket_pl;
import models.agency.Paket_pl_lokasi;
import models.common.Active_user;
import models.common.Kategori;
import models.jcommon.util.CommonUtil;
import models.nonlelang.DraftPesertaNonSpk;
import models.nonlelang.LelangQueryPl;
import models.nonlelang.nonSpk.Dok_non_spk;
import models.nonlelang.nonSpk.HistoryUbahTanggalNonSpk;
import models.nonlelang.nonSpk.JenisRealisasiNonSpk;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import models.secman.Group;
import models.sikap.RekananSikap;
import models.sso.common.Kabupaten;
import play.Logger;
import play.i18n.Messages;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by Lambang on 5/4/2017.
 */
public class NonSpkCtr extends BasicCtr {

    public static void index(Integer kategoriId, Integer tahun) {
        renderArgs.put("kategoriId", kategoriId);
        renderArgs.put("tahun", tahun);
        renderArgs.put("kategoriList", Kategori.all);
        renderArgs.put("tahunList", LelangQueryPl.listTahunAnggaranAktif());
        renderTemplate("nonSpk/non-spk.html");
    }

    @AllowAccess({Group.PPK})
    public static void edit(Long id) {
//        Map<String, Object>  params = new HashMap<>();
//        params.put("nonSpk", nonSpk);
//        if (nonSpk == null) {
//            flash.error("Maaf Anda belum melengkapi data paket, mohon lengkapi terlebih dahulu");
//            PaketPlCtr.indexNonSpk(null, null);
//        }
        Active_user active_user = Active_user.current();
        otorisasiDataNonSpk(id); // check otorisasi data non tender
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(id);
        renderArgs.put("nonSpk", nonSpk);
//        boolean draft = nonSpk.lls_status.isDraft();
        Paket_pl paket = nonSpk.getPaket();
        renderArgs.put("active_user", active_user);
        renderArgs.put("draft",nonSpk.lls_status.isDraft());
        renderArgs.put("paket", paket);
        // Validasi jika paket belum ada data lokasi pekerjaan
        if (CommonUtil.isEmpty(paket.getPaketLokasi())) {
            flash.error(Messages.get("flash.mabmdlppdp"));
            PaketPlCtr.indexNonSpk(null, null);
        }
        Kategori kategori = nonSpk.getKategori();
        Dok_non_spk dok_non_spk = Dok_non_spk.findNCreateBy(nonSpk.lls_id);
        List<JenisRealisasiNonSpk> jenisRealisasiList = new ArrayList<>();
        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah((JenisRealisasiNonSpk.getJumlahNilaiRealisasi(id,null)));
        renderArgs.put("kategori", kategori);
        renderArgs.put("dok_non_spk", dok_non_spk);
        renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);
        if (dok_non_spk != null)
            jenisRealisasiList = nonSpk.getJenisRealisasi();
        renderArgs.put("jenisRealisasiList", jenisRealisasiList);
        List<DraftPesertaNonSpk> draftPesertaNonSpkList = DraftPesertaNonSpk.findWithLelang(nonSpk.lls_id);
        int jumlahPerubahanTanggal = nonSpk.getHistoryUbahTanggalNonSpkList().size();
        renderArgs.put("draftPesertaNonSpkList", draftPesertaNonSpkList);
        renderArgs.put("jumlahPerubahanTanggal", jumlahPerubahanTanggal);
        Date lastTanggalUbah=null ;
        Date dateNow = new Date();
        renderArgs.put("dateNow", dateNow);
        renderTemplate("nonSpk/non-spk-edit.html");

    }

    private static void setPaketLokasi(Paket_pl paket, Long propinsiId) {
        if (CommonUtil.isEmpty(paket.getPaketLocations())) {
            Paket_pl_lokasi pl_lokasi = new Paket_pl_lokasi();
            if (propinsiId != null) {
                // default propinsi
                List<Kabupaten> listKab = Kabupaten.findByPropinsi(propinsiId);
                if (!listKab.isEmpty())
                    pl_lokasi.kbp_id = listKab.get(0).kbp_id;
            }
            paket.setPaketLocations(new ArrayList<>(1));
            paket.getPaketLocations().add(pl_lokasi);
        }
    }

    @AllowAccess({Group.PPK})
    public static void simpan(Non_spk_seleksi nonSpk, Integer kategoriId, boolean ubahTanggalSelesai, String alasan) throws Exception {
        checkAuthenticity();
        otorisasiDataNonSpk(nonSpk.lls_id); // check otorisasi data lelang
        Date oldTanggalSelesai = null;
        if (nonSpk.lls_id != null){
            Non_spk_seleksi non_spk_seleksi = Non_spk_seleksi.findById(nonSpk.lls_id);
            if(!non_spk_seleksi.getPaket().pkt_status.isSelesaiLelang()) {
                validation.valid(nonSpk);
                if (nonSpk.lls_nilai_gabungan != null)
                    validation.max(Double.valueOf(nonSpk.lls_nilai_gabungan), 100).key("lls_nilai_gabungan").message("Nilai Gabungan tidak boleh lebih dari 100 persen");
            }
            if((ubahTanggalSelesai && nonSpk.lls_tanggal_paket_selesai == null)
                    || (non_spk_seleksi.lls_tanggal_paket_selesai == null && nonSpk.lls_tanggal_paket_selesai == null)) {
                validation.addError("nonSpk.lls_tanggal_paket_selesai", "Tanggal Paket Selesai wajib diisi.", "var");
            } else {
                if (ubahTanggalSelesai && CommonUtil.isEmpty(alasan))
                    validation.addError("nonSpk.lls_tanggal_paket_selesai", "Alasan Ubah Tanggal Paket Selesai wajib diisi.", "var");
            }
            if (validation.hasErrors()) {
                validation.keep();
                params.flash();
                flash.error("Data Gagal Tersimpan, silakan cek kembali inputan Anda.");
                edit(nonSpk.lls_id);

            }
            oldTanggalSelesai = non_spk_seleksi.lls_tanggal_paket_selesai;
            if (!non_spk_seleksi.getPaket().pkt_status.isSelesaiLelang()) {
                non_spk_seleksi.lls_jangka_waktu_pelaksanaan = nonSpk.lls_jangka_waktu_pelaksanaan;
                non_spk_seleksi.lls_nilai_gabungan = nonSpk.lls_nilai_gabungan;
                non_spk_seleksi.lls_uraian_pekerjaan = nonSpk.lls_uraian_pekerjaan;
                if (non_spk_seleksi.lls_tanggal_paket_mulai == null)
                    non_spk_seleksi.lls_tanggal_paket_mulai = newDate();
                if (nonSpk.lls_tanggal_paket_selesai != null)
                    non_spk_seleksi.lls_tanggal_paket_selesai = nonSpk.lls_tanggal_paket_selesai;
                non_spk_seleksi.mtd_pemilihan = nonSpk.mtd_pemilihan;
                non_spk_seleksi.lls_nama_penyedia = nonSpk.lls_nama_penyedia;
                non_spk_seleksi.lls_npwp_penyedia = nonSpk.lls_npwp_penyedia;
                non_spk_seleksi.lls_telp_penyedia = nonSpk.lls_telp_penyedia;
                non_spk_seleksi.lls_alamat_penyedia = nonSpk.lls_alamat_penyedia;
                non_spk_seleksi.lls_bukti_pembayaran = Integer.valueOf(0);

            } else {
                if (nonSpk.lls_tanggal_paket_selesai != null)
                    non_spk_seleksi.lls_tanggal_paket_selesai = nonSpk.lls_tanggal_paket_selesai;
            }

            nonSpk = non_spk_seleksi;

        }

        /*File file = attachment != null ? attachment : null;

        if(dok_non_spk == null)
            dok_non_spk = Dok_non_spk.findBy(nonSpk.lls_id);

        if (file == null) {
            if (dok_non_spk.dns_bast_attachment != null && hapus1 == null) {
                BlobTable.delete("blb_id_content=?", dok_non_spk.dns_bast_attachment);
                dok_non_spk.dns_bast_attachment = null;
            }
        } else {
            if (file != null) {
                BlobTable bt = null;
                if(dok_non_spk.dns_bast_attachment == null)
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
                else
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dok_non_spk.dns_bast_attachment);

                dok_non_spk.dns_bast_attachment = bt.blb_id_content;
            }

        }*/

        nonSpk.save();

        //dok_non_spk.save();

        if(!CommonUtil.isEmpty(alasan)) {
            HistoryUbahTanggalNonSpk historyUbahTanggalNonSpk = new HistoryUbahTanggalNonSpk();

            historyUbahTanggalNonSpk.lls_id = nonSpk.lls_id;
            historyUbahTanggalNonSpk.uts_tanggal_edit = newDate();
            historyUbahTanggalNonSpk.uts_tanggal_asli = oldTanggalSelesai;
            historyUbahTanggalNonSpk.uts_tanggal_ubah = nonSpk.lls_tanggal_paket_selesai;
            historyUbahTanggalNonSpk.uts_keterangan = alasan;
            historyUbahTanggalNonSpk.save();
        }

        Paket_pl paket = Paket_pl.findById(nonSpk.pkt_id);
        if(paket.pkt_status.isDraft())
            paket.pkt_status = Paket_pl.StatusPaket.SEDANG_LELANG;
        if(nonSpk.lls_id != null) {
            if (ubahTanggalSelesai && (nonSpk.lls_tanggal_paket_selesai != oldTanggalSelesai) && paket.pkt_status.isSelesaiLelang())
                paket.pkt_status = Paket_pl.StatusPaket.SEDANG_LELANG;
            else if(ubahTanggalSelesai && nonSpk.lls_tanggal_paket_selesai.before(newDate()) && paket.pkt_status.isSedangLelang())
                paket.pkt_status = Paket_pl.StatusPaket.SELESAI_LELANG;
            else if (!ubahTanggalSelesai && nonSpk.lls_tanggal_paket_selesai.before(newDate()))
                paket.pkt_status = Paket_pl.StatusPaket.SELESAI_LELANG;
        }

        if (nonSpk.getPaket().pkt_status.isSelesaiLelang())
            kategoriId = nonSpk.getPaket().kgr_id.id;

        paket.kgr_id = Kategori.findById(kategoriId);

        paket.save();

        flash.success("Paket berhasil di simpan");

        edit(nonSpk.lls_id);

    }

    @AllowAccess({Group.PPK})
    public static void hapus(Long id) {
        Paket_pl paket = Paket_pl.findById(id);
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(paket.pkt_id);
        otorisasiDataPaketPl(paket); // check otorisasi data lelang
        if (paket.getPaketPpk() != null && !paket.pkt_status.isDraft()) {
            flash.error("Maaf, Paket yang sedang aktif tidak dapat dihapus!");
            PaketPlCtr.indexNonSpk(null, null);
        }
        try {
            nonSpk.hapusNonSpk(paket.pkt_id);
//            paket.delete();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.info("Error delete " + ex.getLocalizedMessage());
            flash.error("Gagal menghapus paket pencatatan " + paket.pkt_id);
            PaketPlCtr.indexNonSpk(null, null);
        }
        PaketPlCtr.indexNonSpk(null, null);
    }

    @AllowAccess({Group.PP, Group.PPK})
    public static void view(Long id) {
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(id);
        otorisasiDataNonSpk(nonSpk.lls_id);
        renderArgs.put("nonSpk", nonSpk);
        renderArgs.put("list_jenis_realisasi", nonSpk.getJenisRealisasi());
        Dok_non_spk dok_non_spk = Dok_non_spk.findBy(nonSpk.lls_id);
        renderArgs.put("dok_non_spk", dok_non_spk);
        renderArgs.put("informasi_lainnya", nonSpk.getInformasiLainnya());
        renderArgs.put("disabled", true);
        Kategori kategori = nonSpk.getKategori();
        List<DraftPesertaNonSpk> draftPesertaNonSpkList = DraftPesertaNonSpk.findWithLelang(nonSpk.lls_id);
        renderArgs.put("kategori", kategori);
        renderArgs.put("draftPesertaNonSpkList", draftPesertaNonSpkList);
        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah((JenisRealisasiNonSpk.getJumlahNilaiRealisasi(id,null)));
        renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);
        int jumlahPerubahanTanggal = nonSpk.getHistoryUbahTanggalNonSpkList().size();
        renderArgs.put("jumlahPerubahanTanggal", jumlahPerubahanTanggal);
        renderTemplate("nonSpk/non-spk-view.html" );
    }

    //pilih penyedia
    @AllowAccess({Group.PP})
    public static void viewPilihPenyediaNonSpk(Long lelangId, Boolean search, String npwp, String nama, Long kabupatenId, Long propinsiId, String jenisIjin, String repoId) throws ExecutionException, InterruptedException, UnsupportedEncodingException {
        otorisasiDataNonSpk(lelangId); // check otorisasi data lelang
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(lelangId);
        List<Map<String, String>> rekananList = new ArrayList<>();
        String rekananListJson = null;
        search = search == null ? false : search;
        renderArgs.put("nonSpk", nonSpk);
        if (nonSpk != null) {
            if ((npwp != null && !npwp.isEmpty()) || (nama != null && !nama.isEmpty()) || (jenisIjin != null && !jenisIjin.isEmpty()) || kabupatenId != null) {
                rekananList = RekananSikap.getListPenyediaByNpwpFromSikap(npwp, nama, kabupatenId, jenisIjin, nonSpk.lls_id, "non_spk");
                rekananListJson = CommonUtil.toJson(rekananList);
            } else{
                if(search)
                    flash.error("Silakan masukkan/pilih terlebih dahulu salah satu parameter pencarian");
            }
            renderArgs.put("rekananListJson", rekananListJson);

            if ((rekananList == null || rekananList.size() == 0) && (kabupatenId != null || (jenisIjin != null && !jenisIjin.isEmpty())
                    || (nama != null && !nama.isEmpty()) || (npwp != null && !npwp.isEmpty()))) {
                rekananList = new ArrayList<>();
                flash.error("<i class=\"fa fa-close\"></i>&nbsp;&nbsp;Penyedia tidak di temukan/koneksi ke SIKaP bermasalah.");
            }
            renderArgs.put("rekananList", rekananList);
        }
        renderArgs.put("npwp", npwp);
        renderArgs.put("nama", nama);
        renderArgs.put("kabupatenId", kabupatenId);
        renderArgs.put("propinsiId", propinsiId);
        renderArgs.put("jenisIjin", jenisIjin);
        renderTemplate("nonSpk/non-spk-pilih-penyedia.html");
    }



    @AllowAccess({Group.PP})
    public static void simpanPilihPenyedia(Long lelangId, String npwp, String nama, Long kabupatenId, Long propinsiId,
                                           String jenisIjin, String repoId,  List<Long> rekananId, List<String> rekananNama,
                                           List<String> rekananNpwp, List<String> rekananEmail, List<String> rekananTelepon,
                                           List<String> rekananAlamat)
            throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        otorisasiDataNonSpk(lelangId); // check otorisasi data lelang

        rekananId = rekananId != null ? rekananId : new ArrayList<Long>();

        rekananId.removeAll(Collections.singleton(null));

        rekananNama = rekananNama != null ? rekananNama : new ArrayList<String>();

        rekananNama.removeAll(Collections.singleton(null));

        rekananNpwp = rekananNpwp != null ? rekananNpwp : new ArrayList<String>();

        rekananNpwp.removeAll(Collections.singleton(null));

        rekananEmail = rekananEmail != null ? rekananEmail : new ArrayList<String>();

        rekananEmail.removeAll(Collections.singleton(null));

        rekananTelepon = rekananEmail != null ? rekananTelepon : new ArrayList<String>();

        rekananTelepon.removeAll(Collections.singleton(null));

        rekananAlamat = rekananAlamat != null ? rekananAlamat : new ArrayList<String>();

        rekananAlamat.removeAll(Collections.singleton(null));

        if (rekananId.size() != 0) {

            int i = 0;

            for (Long rkn_id : rekananId) {

                DraftPesertaNonSpk draftPesertaNonSpk = new DraftPesertaNonSpk();

                draftPesertaNonSpk.lls_id = lelangId;
                draftPesertaNonSpk.rkn_id = rkn_id;
                draftPesertaNonSpk.dpn_nama_rekanan = rekananNama.get(i);
                draftPesertaNonSpk.dpn_npwp_rekanan = rekananNpwp.get(i);
                draftPesertaNonSpk.dpn_email_rekanan = rekananEmail.get(i);
                draftPesertaNonSpk.dpn_telp_rekanan = rekananTelepon.get(i);
                draftPesertaNonSpk.dpn_alamat_rekanan = rekananAlamat.get(i);
                draftPesertaNonSpk.save();

                i++;

            }

            flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");

        } else {
            flash.error("<i class=\"fa fa-warning\"></i>&nbsp;&nbsp;Anda belum memilih Penyedia. Silakan pilih Penyedia terlebih dahulu !");
        }

        viewPilihPenyediaNonSpk(lelangId, true, npwp, nama, kabupatenId, propinsiId, jenisIjin, repoId);

    }

    @AllowAccess({Group.PP})
    public static void hapusDraftPenyedia(Long draftPenyediaId) throws ExecutionException, InterruptedException {
        DraftPesertaNonSpk draftPesertaNonSpk = DraftPesertaNonSpk.findById(draftPenyediaId);
        Long lelangId = null;
        if (draftPesertaNonSpk != null) {
            lelangId = draftPesertaNonSpk.lls_id;
            draftPesertaNonSpk.delete();
            flash.success("Berhasil hapus peserta");
        }
        edit(lelangId);
    }

    public static void pengumumanNonSpk(Long id) {
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(id);
        List<Anggaran> anggaranList = Anggaran.findByPaketPl(nonSpk.pkt_id);
        List<Paket_pl_lokasi> lokasiList = Paket_pl_lokasi.find("pkt_id=?", nonSpk.pkt_id).fetch();
        renderArgs.put("nonSpk", nonSpk);
        renderArgs.put("anggaranList", anggaranList);
        renderArgs.put("lokasiList", lokasiList);
        renderTemplate("nonSpk/pengumuman-non-spk.html");
    }

    @AllowAccess({Group.PPK})
    public static void viewHistoryUbahTanggalNonSpk(Long id){
        Non_spk_seleksi nonSpkSeleksi = Non_spk_seleksi.findById(id);
        List<HistoryUbahTanggalNonSpk> historyUbahTanggalNonSpkList = nonSpkSeleksi.getHistoryUbahTanggalNonSpkList();
        renderArgs.put("nonSpkSeleksi", nonSpkSeleksi);
        renderArgs.put("historyUbahTanggalNonSpkList", historyUbahTanggalNonSpkList);
        renderTemplate("nonSpk/view-history-ubah-tanggal-nonspk.html");

    }

}
