package controllers.nonSpk;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.FormatUtils;
import models.agency.DaftarKuantitas;
import models.agency.Paket_pl;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.UploadInfo;
import models.form.FormTambahPenyediaNonSikap;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.nonlelang.nonSpk.*;
import models.secman.Group;
import models.sikap.NonRekananSikap;
import models.sikap.RekananSikap;
import play.Logger;
import play.data.validation.Valid;
import play.libs.URLs;
import models.sso.common.adp.util.DceSecurityV2;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by Lambang on 5/4/2017.
 */
public class RealisasiNonSpkCtr extends BasicCtr {


    @AllowAccess({Group.PPK})
    public static void hps(Long id){

        otorisasiDataNonSpk(id); // check otorisasi data lelang

        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(id);

        Dok_non_spk dok_non_spk = Dok_non_spk.findBy(nonSpk.lls_id);

        boolean editable = true;

        String data = "[]";

        boolean fixed = true;

        DaftarKuantitas dk = null;

        if (dok_non_spk != null) {

            dk = dok_non_spk.dkh;

            if(dk == null && !nonSpk.lls_status.isDraft())
                dk = dok_non_spk.getRincianHPS();

        }

        if(dk == null)
            dk = new DaftarKuantitas();

        if (dk != null) {

            fixed = dk.fixed;

            if (dk.items != null)
                data = CommonUtil.toJson(dk.items);

        }

        render("nonSpk/realisasi/form-non-spk-hps.html" , nonSpk, editable, data, dok_non_spk, fixed);

    }

    @AllowAccess({Group.PPK})
    public static void hpsSubmit(Long id, String data, boolean fixed){

        checkAuthenticity();

        otorisasiDataNonSpk(id); // check otorisasi data lelang

        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(id);

        Dok_non_spk dok_non_spk = Dok_non_spk.findBy(nonSpk.lls_id);

        Map<String, Object> result = new HashMap<String, Object>(1);

        try {

            if(dok_non_spk == null)
                dok_non_spk = Dok_non_spk.findNCreateBy(nonSpk.lls_id);

            dok_non_spk.simpanHps(nonSpk, data, fixed);

            result.put("result", "ok");

        } catch (Exception e) {

            result.put("result", e.getMessage());

        }

        renderJSON(result);

    }

    @AllowAccess({ Group.PPK })
    public static void editJenisRealisasi(Long nonSpkId, Long jenisRealisasiId) {
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(nonSpkId);
        otorisasiDataNonSpk(nonSpkId); // check otorisasi data lelang
        Map<String,Object> params = new HashMap<>();
//        if (nonSpk.getPaket().pkt_status.isSelesaiLelang())
//            forbidden();

        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah(JenisRealisasiNonSpk.getJumlahNilaiRealisasi(nonSpkId,null));
        params.put("nonSpk",nonSpk);
        params.put("totalNilaiRealisasi",totalNilaiRealisasi);

        if (jenisRealisasiId != null) {
            JenisRealisasiNonSpk jenisRealisasiNonSpk, jenisRealisasi;
            jenisRealisasiNonSpk = jenisRealisasi = JenisRealisasiNonSpk.find("rsk_id=?", jenisRealisasiId).first();
//            JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(jenisRealisasiId);
            List<RealisasiPenyediaNonSpk> realisasiPenyediaNonSpkList = new ArrayList<>();
            List<RealisasiNonPenyediaNonSpk> realisasiNonPenyediaNonSpkList = new ArrayList<>();

            if (jenisRealisasiNonSpk.is_penyedia)
                realisasiPenyediaNonSpkList = RealisasiPenyediaNonSpk.findWithRealisasi(jenisRealisasiNonSpk.rsk_id);
            else
                realisasiNonPenyediaNonSpkList = RealisasiNonPenyediaNonSpk.findWithRealisasi(jenisRealisasiNonSpk.rsk_id);

            params.put("jenisRealisasiNonSpk",jenisRealisasiNonSpk);
            params.put("jenisRealisasi",jenisRealisasi);
            params.put("realisasiPenyediaNonSpkList",realisasiPenyediaNonSpkList);
            params.put("realisasiNonPenyediaNonSpkList",realisasiNonPenyediaNonSpkList);

        }
        Date dateNow = new Date();
        Boolean isPaketSelesai=false;
        if(nonSpk.lls_tanggal_paket_selesai!=null && nonSpk.lls_tanggal_paket_selesai.before(dateNow)) {
            isPaketSelesai=true;
        }
        params.put("isPaketSelesai",isPaketSelesai);
        renderTemplate("nonSpk/realisasi/edit-jenis-dokumen-non-spk.html", params);
    }

    @AllowAccess({ Group.PPK })
    public static void viewJenisRealisasi(Long nonSpkId, Long jenisDokumenId) {

        otorisasiDataNonSpk(nonSpkId); // check otorisasi data lelang

        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(nonSpkId);

        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(jenisDokumenId);

        List<RealisasiPenyediaNonSpk> realisasiPenyediaNonSpkList = new ArrayList<>();

        List<RealisasiNonPenyediaNonSpk> realisasiNonPenyediaNonSpkList = new ArrayList<>();

        if (jenisRealisasiNonSpk.is_penyedia)
            realisasiPenyediaNonSpkList = RealisasiPenyediaNonSpk.findWithRealisasi(jenisRealisasiNonSpk.rsk_id);
        else
            realisasiNonPenyediaNonSpkList = RealisasiNonPenyediaNonSpk.findWithRealisasi(jenisRealisasiNonSpk.rsk_id);

        render("nonSpk/realisasi/view-jenis-dokumen-non-spk.html", nonSpk, jenisRealisasiNonSpk, realisasiPenyediaNonSpkList
                , realisasiNonPenyediaNonSpkList);

    }


    @AllowAccess({ Group.PPK })
    public static void simpanJenisRealisasi(Long nonSpkId, JenisRealisasiNonSpk jenisRealisasiNonSpk) throws Exception {

        checkAuthenticity();
        otorisasiDataNonSpk(nonSpkId); // check otorisasi data lelang
        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah(JenisRealisasiNonSpk.getJumlahNilaiRealisasi(nonSpkId,jenisRealisasiNonSpk.rsk_id));
        if(!validation.required(jenisRealisasiNonSpk.rsk_jenis).ok)
            validation.addError("jenisRealisasiNonSpk.rsk_jenis", "Jenis Realisasi wajib diisi", "var");
        if(!validation.required(jenisRealisasiNonSpk.rsk_nilai).ok)
            validation.addError("jenisRealisasiNonSpk.rsk_nilai", "Nilai Realisasi wajib diisi", "var");
        if(!validation.required(jenisRealisasiNonSpk.rsk_tanggal).ok)
            validation.addError("jenisRealisasiNonSpk.rsk_tanggal", "Tanggal Realisasi wajib diisi", "var");
        Non_spk_seleksi nonSpk = Non_spk_seleksi.findById(nonSpkId);

        Date dateNow = new Date();
        if(nonSpk.lls_tanggal_paket_selesai!=null && nonSpk.lls_tanggal_paket_selesai.before(dateNow)) {
            flash.error("Proses ditolak, Tanggal Paket sudah lewat");
            editJenisRealisasi(nonSpkId, jenisRealisasiNonSpk.rsk_id);
        }

        if(validation.required(jenisRealisasiNonSpk.rsk_nilai).ok) {
            Paket_pl paket_pl = nonSpk.getPaket();
            Double jumlahNilaiRealisasiAwal=JenisRealisasiNonSpk.getJumlahNilaiRealisasi(nonSpkId,jenisRealisasiNonSpk.rsk_id);
            Double totalJumlahNilaiRealisasi = jumlahNilaiRealisasiAwal+ jenisRealisasiNonSpk.rsk_nilai;

            if (totalJumlahNilaiRealisasi > paket_pl.pkt_pagu) {
                validation.addError("Total Nilai Realisasi melebihi Pagu", "var");
                final String message = String.format("Total Nilai Realisasi %s (%s + %s) melebihi Pagu %s, silakan sesuaikan kembali Nilai Realisasi.",
                        FormatUtils.formatCurrencyRupiah(totalJumlahNilaiRealisasi),
                        FormatUtils.formatCurrencyRupiah(jumlahNilaiRealisasiAwal),
                        FormatUtils.formatCurrencyRupiah(jenisRealisasiNonSpk.rsk_nilai),
                        FormatUtils.formatCurrencyRupiah(paket_pl.pkt_pagu) );
                flash.error(message);
            }
        }

        if(jenisRealisasiNonSpk.rsk_jenis == 7 && CommonUtil.isEmpty(jenisRealisasiNonSpk.rsk_nama_dok))
            validation.addError("jenisRealisasiNonSpk.rsk_nama_dok", "Nama Dokumen wajib diisi", "var");
        if(jenisRealisasiNonSpk.rsk_id != null && !validation.required(jenisRealisasiNonSpk.rsk_id_attachment).ok)
            validation.addError("jenisRealisasiNonSpk.rsk_id_attachment", "Dokumen wajib dilampirkan", "var");

        if (validation.hasErrors()) {
            validation.keep();
            flash.error("Gagal simpan Realisasi");
            renderArgs.put("nonSpk", nonSpk);
            renderArgs.put("jenisRealisasiNonSpk", jenisRealisasiNonSpk);
            renderArgs.put("jenisRealisasi", jenisRealisasiNonSpk);
            renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);

            List<RealisasiPenyediaNonSpk> realisasiPenyediaNonSpkList = new ArrayList<>();
            List<RealisasiNonPenyediaNonSpk> realisasiNonPenyediaNonSpkList = new ArrayList<>();
            if (jenisRealisasiNonSpk.is_penyedia)
                realisasiPenyediaNonSpkList = RealisasiPenyediaNonSpk.findWithRealisasi(jenisRealisasiNonSpk.rsk_id);
            else
                realisasiNonPenyediaNonSpkList = RealisasiNonPenyediaNonSpk.findWithRealisasi(jenisRealisasiNonSpk.rsk_id);
            renderArgs.put("realisasiPenyediaNonSpkList", realisasiPenyediaNonSpkList);
            renderArgs.put("realisasiNonPenyediaNonSpkList", realisasiNonPenyediaNonSpkList);

            renderTemplate("nonSpk/realisasi/edit-jenis-dokumen-non-spk.html");
        }

        jenisRealisasiNonSpk.lls_id = nonSpkId;
        if (jenisRealisasiNonSpk.rsk_id != null){
            if (jenisRealisasiNonSpk.rsk_jenis != 7)
                jenisRealisasiNonSpk.rsk_nama_dok = null;
            JenisRealisasiNonSpk jenisRealisasi = JenisRealisasiNonSpk.findById(jenisRealisasiNonSpk.rsk_id);
            jenisRealisasi.rsk_jenis = jenisRealisasiNonSpk.rsk_jenis;
            jenisRealisasi.rsk_nomor = jenisRealisasiNonSpk.rsk_nomor;
            jenisRealisasi.rsk_nama = jenisRealisasiNonSpk.rsk_nama;
            jenisRealisasi.rsk_nama_dok = jenisRealisasiNonSpk.rsk_nama_dok;
            jenisRealisasi.rsk_tanggal = jenisRealisasiNonSpk.rsk_tanggal;
            jenisRealisasi.rsk_nilai = jenisRealisasiNonSpk.rsk_nilai;
            jenisRealisasi.rsk_npwp = jenisRealisasiNonSpk.rsk_npwp;
            jenisRealisasi.rsk_keterangan = jenisRealisasiNonSpk.rsk_keterangan;
            jenisRealisasi.rsk_bukti_pembayaran = jenisRealisasiNonSpk.rsk_bukti_pembayaran;
            jenisRealisasiNonSpk = jenisRealisasi;
        }
        jenisRealisasiNonSpk.save();
        flash.success("Berhasil simpan Realisasi");
//        NonSpkCtr.edit(jenisRealisasiNonSpk.lls_id);
        editJenisRealisasi(nonSpkId, jenisRealisasiNonSpk.rsk_id);

    }

    @AllowAccess({Group.PPK})
    public static void uploadDokRealisasi(Long id, File file){
        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(jenisRealisasiNonSpk != null){
            otorisasiDataNonSpk(jenisRealisasiNonSpk.getSpkSeleksi()); // check otorisasi data pencatatan
            try{
                UploadInfo model = jenisRealisasiNonSpk.simpanDokRealisasi(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload dokumen realisasi");
                result.put("result", "Kesalahan saat upload dokumen realisasi");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PPK })
    public static void hapusDokRealisasi(Long id, Integer versi){
        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.find("rsk_id=?", id).first();
        if(jenisRealisasiNonSpk != null){
            BlobTable blob = BlobTable.findById(jenisRealisasiNonSpk.rsk_id_attachment, versi);
            if (blob != null)
                blob.delete();
            if (jenisRealisasiNonSpk.rsk_id_attachment != null && jenisRealisasiNonSpk.getAttachment().isEmpty()) {
                jenisRealisasiNonSpk.rsk_id_attachment = null;
            }
            jenisRealisasiNonSpk.save();
        }
    }

    @AllowAccess({ Group.PPK })
    public static void hapusJenisDokumen(Long nonSpkId, Long jenisDokumenId) {
        otorisasiDataNonSpk(nonSpkId); // check otorisasi data lelang
        if (jenisDokumenId != null) {
            JenisRealisasiNonSpk jenisDokumenNonSpk = JenisRealisasiNonSpk.findById(jenisDokumenId);
            if (jenisDokumenNonSpk.is_penyedia)
                RealisasiPenyediaNonSpk.deleteByRealisasi(jenisDokumenNonSpk.rsk_id);
            else
                RealisasiNonPenyediaNonSpk.deleteByRealisasi(jenisDokumenNonSpk.rsk_id);
            jenisDokumenNonSpk.delete();
            flash.success("Berhasil Hapus JenisDokumen");
        }
        NonSpkCtr.edit(nonSpkId);
    }

    @AllowAccess({Group.PPK})
    public static void uploadInformasiLainnya(Long id, File file) {
        otorisasiDataNonSpk(id);
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            UploadInfo info = DokumenLainNonSpk.simpan(id, DokumenLainNonSpk.JenisDokLainNonSpk.INFORMASI_LAINNYA.value, file);
            List<UploadInfo> files = new ArrayList<UploadInfo>();
            files.add(info);
            result.put("files", files);
        }catch(Exception e) {
            Logger.error("Kesalahan saat upload informasi lainnya: detail %s", e.getMessage());
            e.printStackTrace();
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PPK })
    public static void hapusInformasiLainnya(Long id) {

        DokumenLainNonSpk dokumenLainNonSpk = DokumenLainNonSpk.findById(id);

        if (dokumenLainNonSpk != null) {

            BlobTable blob = BlobTableDao.getLastById(dokumenLainNonSpk.dlk_id_attachment);

            if (blob != null)
                blob.delete();

            dokumenLainNonSpk.delete();
        }

    }


    @AllowAccess({ Group.PPK })
    public static void viewRincianHps(Long nonSpkId){

        Dok_non_spk dokNonSpk = Dok_non_spk.findBy(nonSpkId);

        JsonParser parser = new JsonParser();

        Double total = 0d;

        List<Map<String, Object>> items = new ArrayList<>();

        if(dokNonSpk.dns_dkh != null) {

            JsonObject jsonObject = parser.parse(dokNonSpk.dns_dkh).getAsJsonObject();

            total = jsonObject.get("total").getAsDouble();

            items = formatRincianHps(nonSpkId);

        }

        render("nonSpk/realisasi/viewRincianHpsNonSpk.html", items, total);

    }

    public static  List<Map<String, Object>> formatRincianHps(Long nonSpkId){

        Dok_non_spk dokNonSpk = Dok_non_spk.findBy(nonSpkId);

        JsonParser parser = new JsonParser();

        JsonObject jsonObject = parser.parse(dokNonSpk.dns_dkh).getAsJsonObject();

        JsonArray jsonArray = jsonObject.get("items").getAsJsonArray();

        List<Map<String, Object>> items = new ArrayList<>();

        for (int i = 0; i < jsonArray.size(); i++){

            Map<String, Object> item = new HashMap<>();

            JsonObject json = jsonArray.get(i).getAsJsonObject();

            item.put("item", json.get("item").getAsString());
            item.put("unit", json.get("vol").getAsDouble());
            item.put("vol", json.get("unit").getAsString());
            item.put("unit2", json.get("vol2").getAsDouble());
            item.put("vol2", json.get("unit2").getAsString());
            item.put("harga", FormatUtils.formatDesimal2(json.get("harga").getAsDouble()));
            item.put("total_harga", FormatUtils.formatDesimal2(json.get("total_harga").getAsDouble()));
            item.put("keterangan", json.get("keterangan").getAsString());
            item.put("pajak", json.get("pajak").getAsDouble());

            items.add(item);

        }

        return items;

    }

    @AllowAccess({ Group.PPK })
    public static void tambahPenyediaSikap(){

        Active_user active_user = Active_user.current();

        int repoId = ConfigurationDao.getRepoId();

        Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("repo_id", repoId);
        jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);
        jsonObject.addProperty("repo_nama", pegawai.peg_nama);
        jsonObject.addProperty("peg_role", active_user.group.toString());
        jsonObject.addProperty("pnt_id", active_user.ppkId);
        jsonObject.addProperty("pnt_nama", pegawai.peg_nama);

        String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));

        String url = BasicCtr.SIKAP_URL;

        redirect(url+"/nonrekanan/index?q="+param);

    }

    //pilih penyedia
    @AllowAccess({Group.PPK})
    public static void viewPilihPenyedia(Long realisasiId, Boolean search, String npwp, String nama, Long kabupatenId, Long propinsiId,
                                         String jenisIjin) throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiId);

        List<Map<String, String>> rekananList = new ArrayList<>();

        String rekananListJson = null;

        search = search == null ? false : search;

        if (realisasiId != null) {

            if ((npwp != null && !npwp.isEmpty()) || (nama != null && !nama.isEmpty())
                    || (jenisIjin != null && !jenisIjin.isEmpty()) || kabupatenId != null) {
                rekananList = RekananSikap.getListPenyediaByNpwpFromSikap(npwp, nama, kabupatenId, jenisIjin, jenisRealisasiNonSpk.rsk_id, "realisasi_non_spk");

                rekananListJson = CommonUtil.toJson(rekananList);

            } else{
                if(search)
                    flash.error("Silakan masukkan/pilih terlebih dahulu salah satu parameter pencarian");
            }

            //else if (kabupatenId == null && ((npwp != null && !npwp.equals("")) || (nama != null && !nama.equals(""))))
            //    flash.error("Silakan Pilih Kabupaten Terlebih Dahulu");
            //rekananList = Rekanan.getRekananByNpwp(npwp);

            if ((rekananList == null || rekananList.size() == 0) && (kabupatenId != null || (jenisIjin != null && !jenisIjin.isEmpty())
                    || (nama != null && !nama.isEmpty()) || (npwp != null && !npwp.isEmpty()))) {
                rekananList = new ArrayList<>();
                flash.error("<i class=\"fa fa-close\"></i>&nbsp;&nbsp;Penyedia tidak di temukan/koneksi ke SIKaP bermasalah.");
            }

        }

        render("nonSpk/realisasi/realisasi-nonspk-pilih-penyedia.html", jenisRealisasiNonSpk, rekananList, npwp, nama, kabupatenId, propinsiId, jenisIjin, rekananListJson);

    }

    @AllowAccess({Group.PPK})
    public static void simpanPilihPenyedia(Long realisasiId, String npwp, String nama, Long kabupatenId, Long propinsiId,
                                           String jenisIjin,  List<Long> rekananId, List<String> rekananNama,
                                           List<String> rekananNpwp, List<String> rekananEmail, List<String> rekananTelepon,
                                           List<String> rekananAlamat)
            throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiId);

        if(jenisRealisasiNonSpk != null) {

            jenisRealisasiNonSpk.is_penyedia = true;

            jenisRealisasiNonSpk.save();

            //hapus data non penyedia
            RealisasiNonPenyediaNonSpk.deleteByRealisasi(jenisRealisasiNonSpk.rsk_id);

            rekananId = rekananId != null ? rekananId : new ArrayList<Long>();

            rekananId.removeAll(Collections.singleton(null));

            rekananNama = rekananNama != null ? rekananNama : new ArrayList<String>();

            rekananNama.removeAll(Collections.singleton(null));

            rekananNpwp = rekananNpwp != null ? rekananNpwp : new ArrayList<String>();

            rekananNpwp.removeAll(Collections.singleton(null));

            rekananEmail = rekananEmail != null ? rekananEmail : new ArrayList<String>();

            rekananEmail.removeAll(Collections.singleton(null));

            rekananTelepon = rekananTelepon != null ? rekananTelepon : new ArrayList<String>();

            rekananTelepon.removeAll(Collections.singleton(null));

            rekananAlamat = rekananAlamat != null ? rekananAlamat : new ArrayList<String>();

            rekananAlamat.removeAll(Collections.singleton(null));

            if (rekananId.size() != 0) {

                int i = 0;

                for (Long rkn_id : rekananId) {

                    RealisasiPenyediaNonSpk realisasiPenyediaNonSpk = new RealisasiPenyediaNonSpk();

                    realisasiPenyediaNonSpk.rsk_id = realisasiId;
                    realisasiPenyediaNonSpk.rkn_id = rkn_id;
                    realisasiPenyediaNonSpk.rp_nama_rekanan = rekananNama.get(i);
                    realisasiPenyediaNonSpk.rp_npwp_rekanan = rekananNpwp.get(i);
                    realisasiPenyediaNonSpk.rp_email_rekanan = rekananEmail.get(i);
                    realisasiPenyediaNonSpk.rp_telp_rekanan = rekananTelepon.get(i);
                    realisasiPenyediaNonSpk.rp_alamat_rekanan = rekananAlamat.get(i);
                    realisasiPenyediaNonSpk.save();

                    i++;

                }

                flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");

            } else {
                flash.error("<i class=\"fa fa-warning\"></i>&nbsp;&nbsp;Anda belum memilih Penyedia. Silakan pilih Penyedia terlebih dahulu!");
            }

        }

        viewPilihPenyedia(realisasiId, true, npwp, nama, kabupatenId, propinsiId, jenisIjin);

    }

    @AllowAccess({Group.PPK})
    public static void hapusPenyedia(Long realiasiPenyediaId) throws ExecutionException, InterruptedException {
        RealisasiPenyediaNonSpk realisasiPenyediaNonSpk = RealisasiPenyediaNonSpk.findById(realiasiPenyediaId);
        JenisRealisasiNonSpk jenisRealisasiNonSpk = null;
        if (realisasiPenyediaNonSpk != null) {
            jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiPenyediaNonSpk.rsk_id);
            realisasiPenyediaNonSpk.delete();
            flash.success("Berhasil hapus peserta");
        }
        editJenisRealisasi(jenisRealisasiNonSpk.lls_id, jenisRealisasiNonSpk.rsk_id);
    }


    //pilih penyedia
    @AllowAccess({Group.PPK})
    public static void viewPilihNonPenyedia(Long realisasiId, Boolean search, String npwp, String nama) throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiId);

        List<Map<String, String>> rekananList = new ArrayList<>();

        String rekananListJson = null;

        search = search == null ? false : search;

        if (realisasiId != null) {
            if ((npwp != null && !npwp.isEmpty()) || (nama != null && !nama.isEmpty())) {
                rekananList = NonRekananSikap.getListNonPenyediaFromSikap(npwp, nama, jenisRealisasiNonSpk.rsk_id);
                rekananListJson = CommonUtil.toJson(rekananList);
            } else{
                if(search)
                    flash.error("Silakan masukkan/pilih terlebih dahulu salah satu parameter pencarian");
            }

            //else if (kabupatenId == null && ((npwp != null && !npwp.equals("")) || (nama != null && !nama.equals(""))))
            //    flash.error("Silakan Pilih Kabupaten Terlebih Dahulu");
            //rekananList = Rekanan.getRekananByNpwp(npwp);

            if ((rekananList == null || rekananList.size() == 0) && ((nama != null && !nama.isEmpty()) || (npwp != null && !npwp.isEmpty()))) {
                rekananList = new ArrayList<>();
                flash.error("<i class=\"fa fa-close\"></i>&nbsp;&nbsp;Penyedia tidak di temukan/koneksi ke SIKaP bermasalah.");
            }

        }

        render("nonSpk/realisasi/realisasi-nonspk-pilih-nonpenyedia.html", jenisRealisasiNonSpk, rekananList, npwp, nama, rekananListJson);

    }

    @AllowAccess({Group.PPK})
    public static void simpanPilihNonPenyedia(Long realisasiId, String npwp, String nama, List<Long> rekananId, List<String> rekananNama,
                                           List<String> rekananNpwp, List<String> rekananEmail, List<String> rekananTelepon,
                                           List<String> rekananAlamat)
            throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiId);

        if(jenisRealisasiNonSpk != null) {

            jenisRealisasiNonSpk.is_penyedia = false;

            jenisRealisasiNonSpk.save();

            //hapus data penyedia
            RealisasiPenyediaNonSpk.deleteByRealisasi(jenisRealisasiNonSpk.rsk_id);

            rekananId = rekananId != null ? rekananId : new ArrayList<Long>();

            rekananId.removeAll(Collections.singleton(null));

            rekananNama = rekananNama != null ? rekananNama : new ArrayList<String>();

            rekananNama.removeAll(Collections.singleton(null));

            rekananNpwp = rekananNpwp != null ? rekananNpwp : new ArrayList<String>();

            rekananNpwp.removeAll(Collections.singleton(null));

            rekananEmail = rekananEmail != null ? rekananEmail : new ArrayList<String>();

            rekananEmail.removeAll(Collections.singleton(null));

            rekananTelepon = rekananTelepon != null ? rekananTelepon : new ArrayList<String>();

            rekananTelepon.removeAll(Collections.singleton(null));

            rekananAlamat = rekananAlamat != null ? rekananAlamat : new ArrayList<String>();

            rekananAlamat.removeAll(Collections.singleton(null));

            if (rekananId.size() != 0) {

                int i = 0;

                for (Long rkn_id : rekananId) {

                    RealisasiNonPenyediaNonSpk realisasiNonPenyediaNonSpk = new RealisasiNonPenyediaNonSpk();

                    realisasiNonPenyediaNonSpk.rsk_id = realisasiId;
                    realisasiNonPenyediaNonSpk.rkn_id = rkn_id;
                    realisasiNonPenyediaNonSpk.rn_nama_rekanan = rekananNama.get(i);
                    realisasiNonPenyediaNonSpk.rn_npwp_rekanan = rekananNpwp.get(i);
                    realisasiNonPenyediaNonSpk.rn_email_rekanan = rekananEmail.get(i);
                    realisasiNonPenyediaNonSpk.rn_telp_rekanan = rekananTelepon.get(i);
                    realisasiNonPenyediaNonSpk.rn_alamat_rekanan = rekananAlamat.get(i);
                    realisasiNonPenyediaNonSpk.save();

                    i++;

                }

                flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");

            } else {
                flash.error("<i class=\"fa fa-warning\"></i>&nbsp;&nbsp;Anda belum memilih Penyedia. Silakan pilih Penyedia terlebih dahulu!");
            }

        }

        viewPilihNonPenyedia(realisasiId, true, npwp, nama);

    }

    @AllowAccess({Group.PPK})
    public static void viewTambahPenyediaNonSikap(Long realisasiId, Long nonSpkId){
        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiId);
        Map<String, Object> params = new HashMap<>();
        params.put("jenisRealisasiNonSpk", jenisRealisasiNonSpk);
        params.put("nonSpkId",nonSpkId);
        renderTemplate("nonSpk/realisasi/realisasi-nonspk-tambah-nonsikap.html", params);

    }

    @AllowAccess({Group.PPK})
    public static void simpanTambahNonSikap(Long realisasiId, Long nonSpkId, FormTambahPenyediaNonSikap form){

        JenisRealisasiNonSpk jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiId);

        if(jenisRealisasiNonSpk != null) {
            jenisRealisasiNonSpk.is_penyedia = false;
            jenisRealisasiNonSpk.save();

            validation.valid(form);
            if(validation.hasErrors()){
                flash.error("Gagal menambahkan penyedia.");

                Map<String, Object> params = new HashMap<>();
                params.put("nonSpkId",nonSpkId);
                params.put("jenisRealisasiNonSpk",jenisRealisasiNonSpk);
                params.put("realisasiId",realisasiId);
                params.put("form",form);

                renderTemplate("nonSpk/realisasi/realisasi-nonspk-tambah-nonsikap.html", params);
            }

            RealisasiNonPenyediaNonSpk realisasiNonPenyediaNonSpk = new RealisasiNonPenyediaNonSpk();

            realisasiNonPenyediaNonSpk.rsk_id = realisasiId;
            realisasiNonPenyediaNonSpk.rkn_id = 0l;
            realisasiNonPenyediaNonSpk.rn_nama_rekanan = form.nama;
            realisasiNonPenyediaNonSpk.rn_npwp_rekanan = form.npwp;
            realisasiNonPenyediaNonSpk.rn_email_rekanan = form.email;
            realisasiNonPenyediaNonSpk.rn_telp_rekanan = form.telp;
            realisasiNonPenyediaNonSpk.rn_alamat_rekanan = form.alamat;
            realisasiNonPenyediaNonSpk.save();

            flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");

        }

        editJenisRealisasi(nonSpkId, realisasiId);

    }


    @AllowAccess({Group.PPK})
    public static void hapusNonPenyedia(Long realiasiNonPenyediaId) throws ExecutionException, InterruptedException {
        RealisasiNonPenyediaNonSpk realisasiNonPenyediaNonSpk = RealisasiNonPenyediaNonSpk.findById(realiasiNonPenyediaId);
        JenisRealisasiNonSpk jenisRealisasiNonSpk = null;
        if (realisasiNonPenyediaNonSpk != null) {
            jenisRealisasiNonSpk = JenisRealisasiNonSpk.findById(realisasiNonPenyediaNonSpk.rsk_id);
            realisasiNonPenyediaNonSpk.delete();
            flash.success("Berhasil hapus peserta");
        }
        editJenisRealisasi(jenisRealisasiNonSpk.lls_id, jenisRealisasiNonSpk.rsk_id);
    }

}
