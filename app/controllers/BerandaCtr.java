package controllers;

import businesslogic.ServicePackChangelog;
import controllers.devpartner.PanelCtr;
import controllers.security.AllowAccess;
import jaim.agent.JaimConfig;
import jaim.common.model.ChangelogItem;
import jaim.common.model.UpdateSchedule;
import models.agency.*;
import models.auditor.*;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.SesiPelatihan;
import models.common.StatusLelang;
import models.jcommon.sysinfo.SystemInformation;
import models.lelang.Blacklist;
import models.nonlelang.Pl_seleksi;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.Usergroup;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import play.Logger;
import play.cache.Cache;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Kelas {@code BerandaCtr} merupakan kelas yang mengontrol alur tampilan
 * halaman beranda untuk tiap role dalam aplikasi SPSE.
 *
 * @author I Wayan Wiprayoga W
 */
public class BerandaCtr extends BasicCtr {

    @AllowAccess({Group.ADM_PPE, Group.VERIFIKATOR, Group.PANITIA, Group.REKANAN, Group.PPK, Group.ADM_AGENCY,
            Group.HELPDESK, Group.AUDITOR, Group.TRAINER, Group.PP, Group.PPHP, Group.KUPPBJ, Group.UKPBJ, Group.ADM_PANEL})
    public static void index() {
        Active_user active_user = Active_user.current();
        switch (active_user.group) {
            case PANITIA:
                panitia();
                break;
            case REKANAN:
                rekanan();
                break;
            case PPK:
                ppk();
                break;
            case ADM_AGENCY:
                adminAgency();
                break;
            case ADM_PPE:
                adminPpe();
                break;
            case HELPDESK:
                helpdesk();
                break;
            case VERIFIKATOR:
                verifikator();
                break;
            case AUDITOR:
                auditor();
                break;
            case TRAINER:
                trainer();
                break;
            case PP:
                PP();
                break;
            case PPHP:
                PPHP();
                break;
            case UKPBJ:
            case KUPPBJ:
                ukpbj();
                break;
            case ADM_PANEL:
                PanelCtr.index();
                break;
        }
    }

    /**
     * Fungsi {@code berandaAdminPPE} digunakan untuk menampilkan halaman
     * beranda pengguna dengan role ADMIN_PPE
     */
    @AllowAccess({Group.ADM_PPE})
    private static void adminPpe() {
        UpdateSchedule updateSchedule = UpdateSchedule.findNew();
        final ChangelogItem[] changeLogs = ServicePackChangelog.getChangeLogs(updateSchedule);
        final Optional<Date> lastDate = ServicePackChangelog.getLastDate(changeLogs);
        final DateFormat df = new SimpleDateFormat("yyyyMMdd");

        renderArgs.put("sys",SystemInformation.getSystemInformation());
        renderArgs.put("sesiAktif", SesiPelatihan.getActiveCount());
        renderArgs.put("latihan", ConfigurationDao.isLatihan());
        renderArgs.put("update", updateSchedule);
        renderArgs.put("changeLogs", changeLogs);
        renderArgs.put("lastDate", lastDate.isPresent()? df.format(lastDate.get()):null);
        renderArgs.put("jaimFailed", Cache.get(JaimConfig.JAIM_CONNECTION_FAILED, Boolean.class));
        renderTemplate("beranda/beranda-admin-ppe.html");
    }

    /**
     * Fungsi {@code berandaAdminAgency} digunakan untuk menampilkan halaman
     * beranda pengguna dengan role ADMIN_AGENCY
     */
    @AllowAccess({Group.ADM_AGENCY})
    private static void adminAgency() {
        Active_user active_user = Active_user.current();
        List<Agency> listAgency = Agency.findAllAgencyByPegawai(active_user.pegawaiId);
        Agency agency = Agency.findById(active_user.agencyId);
        renderArgs.put("listAgency", listAgency);
        renderArgs.put("agency", agency);
        renderArgs.put("jumlahSatker", Satuan_kerja.count("agc_id=?", agency.agc_id));
        renderArgs.put("jumlahPegawai",Pegawai.count("agc_id=?", agency.agc_id));
        renderTemplate("beranda/beranda-admin-agency.html");
    }

    /**
     * Fungsi {@code berandaPP} digunakan untuk menampilkan halaman
     * beranda pengguna dengan role PP
     */
    @AllowAccess({Group.PP})
    private static void PP() {
        renderTemplate("beranda/beranda-pejabat-pengadaan.html");
    }

    @AllowAccess({Group.PPK})
    public static void PpkNonTransaksional() {
        renderTemplate("beranda/beranda-ppk-non-transaksional.html");
    }

    @AllowAccess({Group.PPK})
    public static void PpkSwakelola() {
        renderTemplate("beranda/beranda-ppk-swakelola.html");
    }

    /**
     * Fungsi {@code berandaPPHP} digunakan untuk menampilkan halaman
     * beranda pengguna dengan role PPHP
     */
    @AllowAccess({Group.PPHP})
    private static void PPHP() {
        renderTemplate("beranda/beranda-pphp.html");
    }


    /**
     * Fungsi {@code berandaPanita} digunakan untuk menampilkan halaman beranda
     * pengguna dengan role PANITIA
     */
    @AllowAccess({Group.PANITIA})
    private static void panitia() {
        Long panitiaId = params.get("panitiaId", Long.class);
        Integer status = params.get("status", Integer.class);
        if(status == null)
            status = StatusLelang.AKTIF.id;
        renderArgs.put("panitiaId", panitiaId);
        renderArgs.put("status", status);
        renderTemplate("beranda/beranda-panitia.html");
    }

    @AllowAccess({Group.PANITIA})
    private static void panitiaPenunjukanLangsung() {
        Long panitiaId = params.get("panitiaId", Long.class);
        renderArgs.put("panitiaId", panitiaId);
        renderTemplate("beranda/beranda-panitia-penunjukan-langsung.html");
    }

    /**
     * Fungsi {@code berandaRekanan} digunakan untuk menampilkan halaman beranda
     * pengguna dengan role REKANAN
     */
    @AllowAccess({Group.REKANAN})
    private static void rekanan() {
        Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
        Integer status = params.get("status", Integer.class);
        if(status == null)
            status = StatusLelang.AKTIF.id; // default tender aktif
        renderArgs.put("status", status);
        Blacklist blacklist = Blacklist.find("rkn_id=? and ? between bll_tglawal and bll_tglakhir", rekanan.rkn_id, LocalDate.fromDateFields(BasicCtr.newDate()).toDate()).first();
        if (blacklist != null) {
            renderArgs.put("blacklist", blacklist);
        }
        if (ConfigurationDao.isEnableInaproc()) {
            renderArgs.put("status_aktivasi", session.get("status_aktivasi"));
        }
        renderTemplate("beranda/beranda-rekanan.html");
    }

    @AllowAccess({Group.REKANAN})
    private static void rekananPl() {
        Rekanan rekanan = Rekanan.findById(Active_user.current().rekananId);
        Blacklist blacklist = Blacklist.find("rkn_id=? and ? between bll_tglawal and bll_tglakhir", rekanan.rkn_id, LocalDate.fromDateFields(BasicCtr.newDate()).toDate()).first();
        if (blacklist != null) {
            renderArgs.put("blacklist", blacklist);
        }
        renderTemplate("beranda/beranda-rekanan-pl.html");
    }

    /**
     * Fungsi {@code berandaPpk} digunakan untuk menampilkan halaman beranda
     * pengguna dengan role PPK
     */
    @AllowAccess({Group.PPK})
    private static void ppk() {
        renderTemplate("beranda/beranda-ppk.html");
    }

    /**
     * Fungsi {@code berandaPpk} digunakan untuk menampilkan halaman beranda epl
     * pengguna dengan role PPK
     */
    @AllowAccess({Group.PPK})
    private static void ppkPl() {
        renderTemplate("beranda/beranda-ppk-pl.html");
    }


    /**
     * Fungsi {@code berandaHelpdesk} digunakan untuk menampilkan halaman
     * beranda pengguna dengan role HELPDESK
     */
    @AllowAccess({Group.HELPDESK})
    private static void helpdesk() {
        renderArgs.put("sys",SystemInformation.getSystemInformation());
        renderTemplate("beranda/beranda-helpdesk.html");
    }

    /**
     * Fungsi {@code berandaTrainer} digunakan untuk menampilkan halaman beranda
     * pengguna dengan role TRAINER
     */
    @AllowAccess({Group.TRAINER})
    private static void trainer() {
        String username = Active_user.current().userid;
        String userNumber = username.replace("TRAINER","");
        if(userNumber.isEmpty() || !StringUtils.isNumeric(userNumber)){
            renderArgs.put("list", SesiPelatihan.getActive());
        }else{
            int noSesi = Integer.parseInt(userNumber);
            List<SesiPelatihan> list = new ArrayList<>();

            if(noSesi > SesiPelatihan.getActiveCount()){
                list = SesiPelatihan.getActive();
            }else{
                SesiPelatihan sesi = SesiPelatihan.findById(noSesi);
                list.add(sesi);
            }
            renderArgs.put("list", list);
        }
        renderTemplate("beranda/beranda-trainer.html");
    }

    /**
     * Fungsi {@code berandaVerifikator} digunakan untuk menampilkan halaman
     * beranda pengguna dengan role VERIFIKATOR
     */
    @AllowAccess({Group.VERIFIKATOR})
    private static void verifikator() {
        renderTemplate("beranda/beranda-verifikator.html");
    }

    /**
     * Fungsi {@code berandaAuditor} digunakan untuk menampilkan halaman beranda
     * pengguna dengan role AUDITOR
     */
    @AllowAccess({Group.AUDITOR})
    private static void auditor() {
        Long skId = params.get("skId", Long.class);
        Boolean kadaluarsa = false;
        Auditor auditor = Auditor.find("peg_id=?", Active_user.current().pegawaiId).first();
        if (auditor == null) {
            params.flash();
            flash.error("Tidak ada data Auditor untuk user ini");
        } else {
            List<Auditor_skauditor> skauditorList = Auditor_skauditor.find("auditorid=?", auditor.auditorid).fetch();
            renderArgs.put("skauditorList", skauditorList);
        }
        if (skId != null) {
            Skauditor suratTugas = Skauditor.findById(skId);

            Date now = newDate();
            Calendar c = Calendar.getInstance();
            c.setTime(now);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            now = c.getTime();

            if (suratTugas.skakhir.before(now)) {
                kadaluarsa = true;
            }
            renderArgs.put("skId", skId);
            renderArgs.put("suratTugas", suratTugas);
        }
        renderArgs.put("kadaluarsa", kadaluarsa);
        renderTemplate("beranda/beranda-auditor.html");
    }

    @AllowAccess({Group.AUDITOR})
    private static void auditorNonLelang() {
        Boolean kadaluarsa = false;
        Auditor auditor = Auditor.find("peg_id=?", Active_user.current().pegawaiId).first();
        List<Pl_seleksi> plList = new ArrayList<>();
        if (auditor == null) {
            params.flash();
            flash.error("Tidak ada data Auditor untuk user ini");
        } else {
            List<AuditorSkAuditorNonLelang> skauditorList = AuditorSkAuditorNonLelang.find("auditorid=?", auditor.auditorid).fetch();
            renderArgs.put("skauditorList", skauditorList);
        }
        if (params.get("skId") != null) {
            Long skId = params.get("skId", Long.class);
            SkAuditorNonLelang suratTugas = SkAuditorNonLelang.findById(skId);

            Date now = newDate();
            Calendar c = Calendar.getInstance();
            c.setTime(now);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            now = c.getTime();

            if (suratTugas.skakhir.before(now)) {
                kadaluarsa = true;
            }
            renderArgs.put("skId", skId);
            renderArgs.put("suratTugas", suratTugas);
            plList = Pl_seleksi.getPlBySk(skId);

        }
        renderArgs.put("kadaluarsa", kadaluarsa);
        renderArgs.put("plList", plList);
        renderTemplate("beranda/beranda-auditor-nonlelang.html");
    }

    /**
     * Fungsi {@code panitiaPilihPokja} digunakan untuk menampilkan halaman
     * beranda pilih kepanitiaan bagi pengguna dengan role PANITIA dan telah
     * terdaftar pada beberapa kepanitiaan1
     */
    @AllowAccess({Group.PANITIA, Group.ADM_AGENCY, Group.PPK})
    public static void pilihRoles() {
        Active_user active_user = Active_user.current();
        Pegawai pegawai = Pegawai.findBy(active_user.userid); // dapatkan objek pegawai dari session
        List<Group> groups = Usergroup.findListGroupByUser(active_user.userid);
        for (Group group : groups) {
//			if(group == Group.PANITIA) {
//				renderArgs.put("daftar_kepanitiaan", Anggota_panitia.findKepanitiaanPegawai(pegawai.peg_id));
//				renderArgs.put("pnt_id",active_user.panitiaId);
//			}
            if (group == Group.ADM_AGENCY) {
                renderArgs.put("daftar_agency", Agency.findAllAgencyByPegawai(pegawai.peg_id));
                renderArgs.put("agc_id", active_user.agencyId);
            }
            if (group == Group.PPK) {
                renderArgs.put("daftar_ppk", Ppk.findBy(pegawai.peg_id));
                renderArgs.put("ppk_id", active_user.ppkId);
            }
        }
        renderTemplate("beranda/pilih-roles.html");
    }

    /**
     * Fungsi {@code panitiaSetPokjaAndRedirectHome} digunakan untuk set session
     * kepanitiaan terpilih sekaligus menampilkan halaman beranda panitia
     *
     * @param panitiaId id kepanitiaan
     */
    @AllowAccess({Group.PANITIA, Group.ADM_AGENCY, Group.PPK, Group.KUPPBJ})
    public static void setRoles(Long panitiaId, Long agencyId, Long ppkId, Long ukpbjId) {
        Active_user active_user = Active_user.current();
//		if(active_user.isPanitia()) {
//			active_user.panitiaId =panitiaId;
//		}		
        if (active_user.isAdminAgency()) {
            active_user.agencyId = agencyId;
        } else if (active_user.isPpk()) {
            active_user.ppkId = ppkId;
        }else if(active_user.isKuppbj()){
            active_user.ukpbjId = ukpbjId;
        }
        active_user.save();
        index(); // redirect ke halaman beranda panitia
    }

    // Beranda Halaman Non Tender
    @AllowAccess({Group.PPK, Group.REKANAN, Group.AUDITOR, Group.PANITIA})
    public static void nonLelang() {
        Active_user active_user = Active_user.current();
        switch (active_user.group) {
            case PPK:
                ppkPl();
                break;
            case REKANAN:
                rekananPl();
                break;
            case AUDITOR:
                auditorNonLelang();
                break;
            case PANITIA:
                panitiaPenunjukanLangsung();
                break;
        }
    }

    @AllowAccess({Group.UKPBJ, Group.KUPPBJ})
    private static void ukpbj() {
        Active_user activeUser = Active_user.current();
        List<Ukpbj> listUkpbj = Ukpbj.findAllUkpbjByKepala(activeUser.pegawaiId);
        renderArgs.put("listUkpbj", listUkpbj);
        renderArgs.put("ukpbj", Ukpbj.findById(activeUser.ukpbjId));
        renderTemplate("beranda/beranda-ukpbj.html");
    }

}
