package controllers;

import controllers.security.AllowAccess;
import models.Datatable;
import models.agency.*;
import models.agency.resulthandler.LelangSeleksiResultHandler;
import models.agency.resulthandler.NonPaketResultHandler;
import models.agency.resulthandler.PaketResultHandler;
import models.agency.resulthandler.PaketSwaResultHandler;
import models.announce.Pengumuman_lelang;
import models.auditor.Auditor;
import models.auditor.SkAuditorNonLelang;
import models.auditor.Skauditor;
import models.common.*;
import models.common.Sub_tag.JenisSubtag;
import models.devpartner.LelangDp;
import models.devpartner.common.TahapDp;
import models.devpartner.handler.LelangDpResultHandler;
import models.devpartner.panel.Panel;
import models.devpartner.handler.PaketDpResultHandler;
import models.jcommon.mail.MailQueue.BACA_STATUS;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.lelang.Lelang_seleksi;
import models.lelang.Unspsc;
import models.nonlelang.Swakelola_seleksi;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import models.nonlelang.resulthandlers.PlSeleksiResultHandler;
import models.rekanan.*;
import models.secman.Group;
import models.secman.Usrsession;
import models.sirup.PaketSirup;
import models.sirup.PaketSwakelolaSirup;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.sql2o.ResultSetHandler;
import play.Logger;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**NOTE: Pada setiap request, DataTable mengirimkan timestamp pada URL-nya
 *
 * Contoh: /spse4/dt/lelang?kategori=PENGADAAN_BARANG&....&_=1410341578267
 * Ini menyebabkan tidak bisa dilakukan CacheFor() karena URL-nya selalu berbeda meskipun
 * request terhadap data yang sama.
 * QueryString _ itu sebagai timestamp
 *
 *
 * Kelas untuk implementasi <i>server side processing</i> JQUERY DataTable  .
 * Created with IntelliJ IDEA. Date: 25/05/12 Time: 12:07
 *
 * @author I Wayan Wiprayoga Wisesa
 */
public class DataTableCtr extends TableCtr {
	/**
	 * get JSON for table Berita for subtag = BERITA
	 */
	public static void berita(JenisSubtag jenisSubtag) {
		String[] column = new String[] {"brt_id", "brt_judul", "brt_tanggal"};
		QueryBuilder select = new QueryBuilder("SELECT brt_id,brt_judul,brt_tanggal");
		QueryBuilder from = new QueryBuilder("FROM berita WHERE stg_id = ?", jenisSubtag);
		renderJSON(datatable(select, from, column, Berita.resultsetBerita));
	}

	/**
	 * get JSON for table Berita for subtag = BERITA
	 */
	@AllowAccess({Group.PANITIA, Group.PP})
	public static void berita_panitia() {
		Long pegawaiId = Active_user.current().pegawaiId;
		String[] column = new String[] { "brt_id", "brt_judul", "brt_tanggal" };
		QueryBuilder select = new QueryBuilder("SELECT brt_id,brt_judul,brt_tanggal");
		QueryBuilder from = new QueryBuilder("FROM berita WHERE peg_id = ?", pegawaiId);
		renderJSON(datatable(select, from, column, Berita.resultsetBerita));
	}

	/**
	 * list berita untuk halaman publik
	 */
	public static void pengumuman() {
		String[] column = new String[]{"brt_id", "brt_judul", "brt_tanggal"};
		QueryBuilder select = new QueryBuilder("SELECT brt_id,brt_judul,brt_tanggal");
		QueryBuilder from = new QueryBuilder("FROM berita WHERE STG_ID in (?,?,?,?)", JenisSubtag.LELANG, JenisSubtag.LOWONGAN, JenisSubtag.PL, JenisSubtag.BERITA);
		renderJSON(datatable(select, from, column, Berita.resultsetBerita));
	}

	/**
	 * get JSON for table FAQ
	 */
	@AllowAccess({Group.HELPDESK, Group.ADM_PPE})
	public static void faq() {
		String[] column = new String[]{"faq_id", "faq_pertanyaan", "faq_tglkirim", "faq_status"};
		QueryBuilder select =new QueryBuilder("SELECT faq_id, faq_pertanyaan, (CASE WHEN faq_tgljawab IS NOT NULL THEN faq_tgljawab ELSE faq_tglkirim END) AS faq_tglkirim, faq_status");
		QueryBuilder from = new QueryBuilder("FROM faq");
		renderJSON(datatable(select, from, column, Faq.resultsetFaqAdmin));
	}

	public static void publicFAQ() {
		String[] column = new String[]{"faq_id", "faq_pertanyaan", "faq_jawaban"};
		QueryBuilder select = new QueryBuilder("SELECT faq_id, faq_pertanyaan, faq_jawaban");
		QueryBuilder from = new QueryBuilder("FROM faq WHERE faq_status=1");
		renderJSON(datatable(select, from, column, Faq.resultsetFaq));
	}

	/**
	 * get JSON for table Agency
	 */
	@AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
	public static void agency() {
		String[] column = new String[]{"agc_id", "agc_nama", "agc_alamat", "agc_telepon", "audituser", "is_lpse"};
		QueryBuilder select = new QueryBuilder("SELECT agc_id, agc_nama, agc_alamat, agc_telepon, audituser, is_lpse");
		QueryBuilder from = new QueryBuilder("FROM agency");
		Long agencyId = Active_user.current().agencyId;
		if(agencyId != null) {
			from.append(" WHERE sub_agc_id = ?", agencyId);
		}
		renderJSON(datatable(select, from, column, Agency.resultsetAgency));
	}

	/**
	 * get JSON for table Pegawai
	 */
	@AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
	public static void pegawai() {
		Long agencyId = Active_user.current().agencyId;
		String[] column = new String[]{"peg_id","peg_nip", "peg_nama","peg_namauser", "idgroup"};
		QueryBuilder select = new QueryBuilder("SELECT peg_id, peg_nip, peg_nama, peg_namauser, idgroup");
		if(ConfigurationDao.isOSD(BasicCtr.newDate())) {
			column = (String[]) ArrayUtils.add(column, "cer_id");
			select.append(",cer_id ");
		}
		QueryBuilder from = new QueryBuilder("FROM pegawai p, usergroup u WHERE peg_id <> '1'");
		if(agencyId != null) {
			from.append(" AND p.agc_id= ? AND p.peg_namauser=u.userid AND u.idgroup IN ('ADM_AGENCY', 'PANITIA','PPK','PP','PPHP', 'UKPBJ', 'KUPPBJ')",agencyId);
		}
		else {
			from.append(" AND p.peg_namauser=u.userid AND u.idgroup IN ('ADM_PPE', 'ADM_AGENCY', 'HELPDESK','VERIFIKATOR')");
		}
		renderJSON(datatable(select, from, column, Pegawai.resultsetPegawai));
	}

	@AllowAccess({Group.ADM_AGENCY, Group.UKPBJ, Group.KUPPBJ})
	public static void panitia(Integer tahun) {
		Active_user user = Active_user.current();
		String[] column = new String[]{"pnt_id", "pnt_nama", "stk_nama", "is_active"};
		QueryBuilder select = new QueryBuilder("SELECT pnt_id, pnt_nama, stk_nama, is_active, (select count(peg_id) FROM anggota_panitia where pnt_id=p.pnt_id) AS jumlah");
		QueryBuilder from = new QueryBuilder("FROM panitia p LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id ");
		if(user.isAdminAgency()) {
			from.append("WHERE p.agc_id = ?", user.agencyId);
		}
		if(user.isUkpbj() || user.isKuppbj()) {
			from.append("WHERE p.ukpbj_id = ?", user.ukpbjId);
		}
		if(tahun != null) {
			from.append(" AND p.pnt_tahun= ?", tahun);
		}
		renderJSON(datatable(select, from, column, Panitia.resultsetPanitia));
	}

	@AllowAccess({Group.KUPPBJ})
	public static void pilihPanitia(Long oldPanitia){
		Active_user user = Active_user.current();
		String[] column = new String[]{"pnt_id","pnt_no_sk","pnt_nama","pnt_tahun"};
		QueryBuilder select = new QueryBuilder("SELECT pnt_id, pnt_no_sk, pnt_nama, pnt_tahun, ");
		select.append("(SELECT string_agg(peg_nama, ', ') as peg_nama from pegawai peg join anggota_panitia a on a.peg_id = peg.peg_id where a.pnt_id = p.pnt_id) as peg_nama ");
		QueryBuilder from = new QueryBuilder("FROM panitia p WHERE ukpbj_id = ? and (select count(*) from anggota_panitia ap where ap.pnt_id = p.pnt_id) > 0 and p.is_active = -1",user.ukpbjId);

		if(oldPanitia != null){
			from.append(" and pnt_id != ?",oldPanitia);
		}

		renderJSON(datatable(select,from,column,Panitia.resultsetPilihPanitia));
	}

	@AllowAccess({Group.KUPPBJ, Group.PPK})
	public static void pilihPp(Long oldPp){
		Active_user user = Active_user.current();
		String[] column = new String[]{"pp_id","peg_nip", "peg_nama","peg_namauser"};
		QueryBuilder select = new QueryBuilder("SELECT pp_id, peg_nip, peg_nama, peg_namauser");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.pp , pegawai WHERE pp.peg_id=pegawai.peg_id and pegawai.disableuser=0");
//		if(!StringUtils.isEmpty(insId)) {
//			from = new QueryBuilder ("FROM pp INNER JOIN pegawai ON pp.peg_id = pegawai.peg_id INNER JOIN agency ON pegawai.agc_id = agency.agc_id " +
//					"INNER JOIN instansi ON agency.kbp_id = instansi.kbp_id WHERE instansi.id = ? AND pegawai.disableuser=0 ", insId);
//		}
		if(oldPp != null){
			from.append("and pp_id != ?", oldPp);
		}
		renderJSON(datatable(select,from,column,Pp.resultsetPilihPp));
	}

	@AllowAccess({Group.KUPPBJ, Group.PPK})
	public static void pilihUkpbj(){
		String[] column = new String[]{"nama","alamat", "kpl_unit_pemilihan_id","u.ukpbj_id", "peg_nama"};
		QueryBuilder select = new QueryBuilder("SELECT u.nama, u.alamat, u.kpl_unit_pemilihan_id, u.ukpbj_id, p.peg_nama");
		QueryBuilder from = new QueryBuilder("FROM ukpbj u join pegawai p on u.kpl_unit_pemilihan_id = p.peg_id");
		renderJSON(datatable(select, from, column, Ukpbj.resultsetPilihUkpbj));
	}

	/**
	 * get JSON for table Anggota Panitia
	 *
	 * @param id id kepanitiaan
	 */
	@AllowAccess({Group.ADM_AGENCY})
	public static void anggotaPanitia(Long id) {
		String[] column = new String[]{"peg_id", "peg_nama", "peg_namauser", "agp_jabatan"};
		QueryBuilder select = new QueryBuilder("select peg_nama, peg_namauser, agp_jabatan, p.peg_id");
		QueryBuilder from = new QueryBuilder("FROM anggota_panitia a, pegawai p where a.peg_id=p.peg_id and pnt_id = ?", id);
		renderJSON(datatable(select, from, column, Anggota_panitia.resultsetAnggotaPanitia));
	}

	@AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY,Group.KUPPBJ})
	public static void calonAnggotaPanitia(Long ukpbjId, Long id) {
		String[] column = new String[]{"peg_nip", "peg_nama", "peg_namauser", "peg_id"};
		QueryBuilder select = new QueryBuilder("SELECT peg_nip, peg_nama, peg_namauser, peg_id");
		QueryBuilder from = new QueryBuilder("FROM pegawai WHERE peg_id IN (SELECT peg_id from pegawai_ukpbj where ukpbj_id=?) AND peg_id NOT IN (SELECT peg_id FROM anggota_panitia WHERE pnt_id=?) " +
				"AND peg_namauser IN (SELECT userid FROM usergroup WHERE idgroup='PANITIA')", ukpbjId, id);
		renderJSON(datatable(select, from, column, Anggota_panitia.resultsetAnggotaPanitia));
	}

	/**
	 * get JSON for table SK Auditor
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void skAuditor() {
		String[] column = new String[]{"skid", "skno", "skmulai", "skakhir"};
		QueryBuilder select = new QueryBuilder("SELECT skid, skno, skmulai, skakhir");
		QueryBuilder from = new QueryBuilder("FROM skauditor");
		renderJSON(datatable(select, from, column, Skauditor.resultsetAuditor));
	}

	@AllowAccess({Group.ADM_PPE})
	public static void skAuditorNonLelang() {
		String[] column = new String[]{"skid", "skno", "skmulai", "skakhir"};
		QueryBuilder select = new QueryBuilder("SELECT skid, skno, skmulai, skakhir");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.skauditor");
		renderJSON(datatable(select, from, column, SkAuditorNonLelang.resultsetAuditor));
	}

	/**
	 * get JSON for table MailQueue
	 *
	 * @param status email status
	 */
	@AllowAccess({Group.ADM_PPE, Group.HELPDESK})
	public static void email(int status) {
		if (status!=1 && status!=2)
			status = 0;
		String[] column = new String[]{"id", "to_addresses", "subject", "enqueue_date", "retry"};
		QueryBuilder select = new QueryBuilder("SELECT id,to_addresses, subject,enqueue_date,retry");
		QueryBuilder from = new QueryBuilder("FROM mail_queue WHERE status=?", status);
		renderJSON(datatable(select, from, column, MailQueueDao.resultsetMail));
	}

	/**
	 * get JSON for table Auditor using INNER JOIN statement
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void auditor() {
		String[] column = new String[]{"auditorid", "peg_nama", "p.peg_nip", "peg_namauser"};
		QueryBuilder select = new QueryBuilder("SELECT auditorid, peg_nama, p.peg_nip, peg_namauser");
		QueryBuilder from = new QueryBuilder("FROM auditor a, pegawai p WHERE p.peg_id = a.peg_id");
		renderJSON(datatable(select, from, column, Auditor.resultsetAuditor));
	}

	/**
	 * get JSON for table Auditor using INNER JOIN statement
	 */
	@AllowAccess({Group.ADM_PPE})
	public static void paketAuditor(Long skId, Long instansiId, Long satkerId, Integer tahun) {
		String[] column = new String[]{"pkt_nama", "pkt_pagu", "l.lls_id"};
		QueryBuilder select= new QueryBuilder("SELECT DISTINCT pkt_nama, pkt_pagu, l.lls_id, lls_status ");
		QueryBuilder from = new QueryBuilder("FROM paket p INNER JOIN lelang_seleksi l ON l.pkt_id = p.pkt_id AND l.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=l.pkt_id) " +
				"WHERE l.lls_status <> 0 AND p.pkt_id NOT IN (SELECT pkt_id FROM PAKET_SKAUDITOR WHERE skid = ?)", skId);
		if(tahun != null) {
			from.append(" AND p.pkt_id IN (SELECT pkt_id FROM paket_anggaran pa, anggaran a WHERE a.ang_id=pa.ang_id AND ang_tahun=?)", tahun);
		}
		if(instansiId != null) {
			from.append(" AND p.pkt_id IN (SELECT DISTINCT p.pkt_id FROM paket p INNER JOIN paket_satker ps ON p.pkt_id=ps.pkt_id INNER JOIN satuan_kerja s ON s.stk_id=ps.stk_id WHERE s.instansi_id=?)", instansiId);
		}
		if(satkerId != null) {
			from.append(" AND p.pkt_id IN (SELECT DISTINCT p.pkt_id FROM paket p INNER JOIN paket_satker ps ON p.pkt_id=ps.pkt_id INNER JOIN satuan_kerja s ON s.stk_id=ps.stk_id WHERE s.stk_id=?)", satkerId);
		}
		renderJSON(datatable(select, from, column, Auditor.resultsetPaketAuditor));
	}

	@AllowAccess({Group.ADM_PPE})
	public static void paketAuditorNonLelang(Long skId, Long instansiId, Long satkerId, Integer tahun) {
		String[] column = new String[]{"pkt_nama", "pkt_pagu", "l.lls_id"};
		QueryBuilder select= new QueryBuilder("SELECT DISTINCT pkt_nama, pkt_pagu, l.lls_id, lls_status ");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p " +
				"INNER JOIN ekontrak.nonlelang_seleksi l ON l.pkt_id = p.pkt_id AND l.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM ekontrak.nonlelang_seleksi WHERE pkt_id=l.pkt_id) " +
				"WHERE l.lls_status=1 AND l.pkt_id NOT IN (SELECT pkt_id FROM ekontrak.paket_skauditor WHERE skid = ?)", skId);
		if(tahun != null) {
			from.append(" AND p.pkt_id IN (SELECT pkt_id FROM ekontrak.paket_anggaran pa, anggaran a WHERE a.ang_id=pa.ang_id AND ang_tahun=?)", tahun);
		}
		if(instansiId != null) {
			from.append(" AND p.pkt_id IN (SELECT DISTINCT p.pkt_id FROM ekontrak.paket p INNER JOIN ekontrak.paket_satker ps ON p.pkt_id=ps.pkt_id INNER JOIN satuan_kerja s ON s.stk_id=ps.stk_id WHERE s.instansi_id=?)", instansiId);
		}
		if(satkerId != null) {
			from.append(" AND p.pkt_id IN (SELECT DISTINCT p.pkt_id FROM ekontrak.paket p INNER JOIN ekontrak.paket_satker ps ON p.pkt_id=ps.pkt_id INNER JOIN satuan_kerja s ON s.stk_id=ps.stk_id WHERE s.stk_id=?)", satkerId);
		}

		renderJSON(datatable(select, from, column, Auditor.resultsetNonPaketAuditor));
	}

	/**
	 * get JSON for table Rekanan using INNER JOIN statement
	 *
	 * @param status rekanan status
	 */
	@AllowAccess({Group.ADM_PPE, Group.VERIFIKATOR})
	public static void rekanan(String status)  {
		Date now = newDate();
		String[] column = new String[]{"r.rkn_id", "rkn_nama", "rkn_telepon", "rkn_alamat", "rkn_npwp", "rkn_email", "btu_id", "rkn_tgl_daftar", "disableuser", "b.bll_id","rkn_isactive","bll_tglakhir"};
		QueryBuilder select = new QueryBuilder("SELECT r.rkn_id, rkn_nama, rkn_telepon, rkn_alamat, rkn_npwp, rkn_email, btu_id, rkn_tgl_daftar, r.disableuser,b.bll_id,rkn_isactive,b.bll_tglakhir");
		if(ConfigurationDao.isOSD(now)) {
			column = (String[])ArrayUtils.add(column, "cer_id");
			select.append(",cer_id");
		}
		QueryBuilder from = new QueryBuilder("FROM rekanan r LEFT JOIN blacklist b on r.rkn_id=b.rkn_id and bll_tglawal< ? and bll_tglakhir > ?", now, now);
		if(!CommonUtil.isEmpty(status)) {
			if(status.equals("R")){
				from.append(" where repo_id <>  ?", ConfigurationDao.getRepoId());
			}
			else{
				from.append(" where rkn_status= ? AND repo_id = ?", Integer.valueOf(status), ConfigurationDao.getRepoId());
			}
		}
//		Logger.info("SQL : %s", from.toString());
		renderJSON(datatable(select, from, column, Rekanan.resultsetRekanan));
	}

	/**
	 * get JSON for table Satker
	 */
	@AllowAccess({Group.ADM_AGENCY})
	public static void satker() {
		Long agencyId = Active_user.current().agencyId;
		String[] column = new String[]{"stk_id", "stk_nama", "stk_alamat", "stk_contact_person", "stk_telepon", "stk_fax"};
		QueryBuilder select = new QueryBuilder("SELECT stk_id, stk_nama, stk_alamat, stk_contact_person, stk_telepon, stk_fax");
		QueryBuilder from = new QueryBuilder("FROM satuan_kerja");
		if(agencyId != null) {
			from.append(" where agc_id= ? ", agencyId);
		}
		renderJSON(datatable(select, from, column, Satuan_kerja.resultsetSatker));
	}

	/**
	 * get JSON for table Paket
	 *
	 * @param tahun tahun anggaran paket
	 */
	@AllowAccess({Group.ADM_PPE, Group.ADM_AGENCY})
	public static void paketAgency(Integer tahun) {
		if (tahun == null)
			tahun =DateUtil.getTahunSekarang();
		Long agencyId = Active_user.current().agencyId;
		String[] column = new String[]{"pkt_id","pkt_nama", "pkt_status", "pkt_pagu", "stk_nama"};
		QueryBuilder select = new QueryBuilder("SELECT pkt_nama, pkt_status, pkt_pagu, s.stk_nama, pp.ppk_id, pe.peg_id");
		QueryBuilder from = new QueryBuilder("FROM paket p , satuan_kerja s , ppk pp , pegawai pe where p.ppk_id =pp.ppk_id and pp.peg_id = pe.peg_id "+
				"and p.stk_id=s.stk_id AND EXTRACT(YEAR FROM pkt_tgl_buat) = ?", tahun);
		if(agencyId != null)
			from.append(" AND pe.agc_id= ? ", agencyId);
		renderJSON(datatable(select, from, column, Agency.resultsetPaketAgency));
	}

	/**
	 * get JSON for table Paket
	 *
	 * @param tahun tahun anggaran paket
	 */
	@AllowAccess({Group.UKPBJ, Group.KUPPBJ})
	public static void paketUkpbj(Integer tahun) {
		if (tahun == null)
			tahun =DateUtil.getTahunSekarang();
		String[] column = new String[]{"pkt_id","pkt_nama", "pkt_status", "pkt_pagu", "stk_nama"};
		QueryBuilder select = new QueryBuilder("SELECT pkt_nama, pkt_status, pkt_pagu, s.stk_nama, pnt_nama");
		QueryBuilder from = new QueryBuilder("FROM paket p , satuan_kerja s , panitia pn where p.pnt_id=pn.pnt_id AND p.stk_id=s.stk_id AND EXTRACT(YEAR FROM pkt_tgl_buat) = ? AND p.ukpbj_id=?", tahun, Active_user.current().ukpbjId);
		renderJSON(datatable(select, from, column, Ukpbj.resultsetPaket));
	}

	/**
	 * Get JSON for table Lelang_seleksi, especially for newly-fresh tender
	 */
	@AllowAccess({Group.TRAINER, Group.REKANAN})
	public static void lelangBaru() {
		Long rekananId = Active_user.current().rekananId;
		String id_metode_pasca = StringUtils.join(Metode.METODE_PASCA_ID, ",");
		String id_metode_pra = StringUtils.join(Metode.METODE_PRA_ID, ",");
		final Date now = BasicCtr.newDate();
		String[] column = new String[]{"lls_id", "pkt_nama", "pkt_hps"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, mtd_pemilihan, pkt_nama, pkt_hps, pkt_flag, p.is_pkt_konsolidasi, lls_versi_lelang");
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l LEFT JOIN paket p ON p.pkt_id=l.pkt_id WHERE l.lls_status=1")
				.append(" AND l.lls_id IN (SELECT distinct j.lls_id FROM jadwal j, aktivitas a, lelang_seleksi l WHERE j.lls_id=l.lls_id and j.akt_id=a.akt_id")
				.append(" AND ((l.mtd_pemilihan not in (9,10) AND l.mtd_id IN ("+id_metode_pasca+") AND akt_jenis IN ('PENGUMUMAN_LELANG','AMBIL_DOKUMEN') AND dtj_tglawal <= ? AND dtj_tglakhir >= ?)")
				.append(" OR (l.mtd_pemilihan not in (9,10) AND l.mtd_id IN ("+id_metode_pra+") AND akt_jenis IN ('UMUM_PRAKUALIFIKASI','PEMASUKAN_DOK_PRA','AMBIL_DOK_PRA') AND dtj_tglawal <= ? AND dtj_tglakhir >= ?)")
				.append(" OR (l.mtd_pemilihan in (9,10) AND akt_jenis = 'PEMASUKAN_PENAWARAN' AND dtj_tglakhir >= ?))) ", now, now, now, now, now);
		if(rekananId != null)
			from.append(" AND l.lls_id NOT IN (SELECT lls_id FROM peserta WHERE rkn_id=?)", rekananId);
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetLelangBaru));
	}

	@AllowAccess({Group.TRAINER, Group.REKANAN})
	public static void plBaru() {
		Long rekananId = Active_user.current().rekananId;
		Rekanan rekanan = Rekanan.findById(rekananId);
		final Date now = BasicCtr.newDate();
		String[] column = new String[]{"lls_id", "pkt_nama", "pkt_hps"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, mtd_pemilihan, pkt_nama, pkt_hps, pkt_flag");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON p.pkt_id=l.pkt_id WHERE l.lls_status=1")
				.append(" AND l.lls_id IN (SELECT distinct j.lls_id FROM ekontrak.jadwal j, ekontrak.aktivitas_pl a, ekontrak.nonlelang_seleksi l WHERE j.lls_id=l.lls_id and j.akt_id=a.akt_id")
				.append(" AND a.akt_jenis IN ('PEMASUKAN_PENAWARAN','UMUM_PRAKUALIFIKASI') AND j.dtj_tglakhir >= ?)", now);
		if(rekananId != null) {
			from.append(" AND l.lls_id IN (SELECT lls_id FROM ekontrak.draft_peserta_nonlelang WHERE dp_npwp_rekanan=?)", rekanan.rkn_npwp);
			from.append(" AND l.lls_id NOT IN (SELECT lls_id FROM ekontrak.peserta_nonlelang WHERE rkn_id=?)", rekananId);
			from.append(" AND p.pkt_jenis = 0");
		}
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetPlBaru));
	}

	/**
	 * Daftar paket berdasarkan panitia
	 *
	 * @param status kode status lelang
	 */
	@AllowAccess({Group.PANITIA})
	public static void paketPanitia(Long panitiaId, Integer status) {
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "pkt_tgl_buat", "paket_satker(p.pkt_id)", "lls_id", "lls_penawaran_ulang"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, paket_satker(p.pkt_id) as stk_nama, pkt_pagu, " +
				"pkt_status, pkt_flag, lls_id, mtd_pemilihan, tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status, lls_penawaran_ulang, lls_evaluasi_ulang," +
				"ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi", newDate());
		QueryBuilder from = new QueryBuilder("FROM paket p LEFT JOIN lelang_seleksi lls ON lls.pkt_id=p.pkt_id AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM lelang_seleksi WHERE pkt_id=p.pkt_id AND p.pkt_id_konsolidasi is null)");
		Active_user active_user = Active_user.current();
		if (panitiaId != null) {
			from.append("WHERE p.pnt_id=? ", panitiaId);
		} else {
			from.append("WHERE p.pnt_id IN (SELECT pnt_id FROM anggota_panitia WHERE peg_id=?) ", active_user.pegawaiId);
		}
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
		renderJSON(datatable(select, from, column, PaketResultHandler.resultsetPaketList));
	}

	@AllowAccess({Group.PPK})
	public static void paketPpkNew(Long ppkId, Integer status) {
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "paket_satker(p.pkt_id)", "pkt_flag", "lls_id", "lls_penawaran_ulang"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, paket_satker(p.pkt_id) as stk_nama, " +
				"lls_penawaran_ulang, lls_evaluasi_ulang, ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi, "
				+"pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status", newDate());
		QueryBuilder from = new QueryBuilder("FROM paket p JOIN (SELECT DISTINCT(ppk_id), pkt_id FROM paket_anggaran) ppk ON ppk.pkt_id=p.pkt_id " +
				"LEFT JOIN lelang_seleksi lls ON lls.pkt_id=p.pkt_id " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM lelang_seleksi WHERE pkt_id=p.pkt_id) WHERE ppk.ppk_id=? AND p.pkt_id_konsolidasi is null", ppkId);
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
		renderJSON(datatable(select, from, column, PaketResultHandler.resultsetPaketList));
	}

	@AllowAccess({Group.KUPPBJ})
	public static void paketKuppbj(Long ukpbjId, Integer status) {
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "stk_nama", "pkt_flag", "lls_id", "pn.pnt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, pn.pnt_nama, is_pkt_konsolidasi, " +
				"(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				"paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, " +
				"pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, tahap_now(lls_id, ?) AS tahaps, " +
				"lls_versi_lelang, lls_status", newDate());
		QueryBuilder from = new QueryBuilder("FROM paket p " +
				"LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN lelang_seleksi lls ON lls.pkt_id=p.pkt_id " +
				"LEFT JOIN panitia pn ON pn.pnt_id = p.pnt_id  " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM lelang_seleksi WHERE pkt_id=p.pkt_id)");
		from.append("WHERE p.ukpbj_id=? AND pkt_id_konsolidasi is null", ukpbjId);
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
//		from.append(" AND pkt_flag =?", 3);
		renderJSON(datatable(select, from, column, PaketResultHandler.resultsetPaketKuppbj));
	}


	/**
	 * Daftar paket berdasarkan pejabat pengadaan
	 *
	 * @param status kode status non tender
	 */
	@AllowAccess({Group.PP})
	public static void paketPp(Long ppId, Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "pkt_tgl_buat", "stk_nama", "lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				"ekontrak.paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) AS stk_nama, " +
				"pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, ekontrak.tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status, " +
				"ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi",newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p " +
				"LEFT JOIN public.satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN ekontrak.nonlelang_seleksi lls ON lls.pkt_id=p.pkt_id " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM ekontrak.nonlelang_seleksi WHERE pkt_id=p.pkt_id AND p.pkt_id_konsolidasi is null)");
		Active_user active_user = Active_user.current();
		if (ppId != null) {
			from.append("WHERE p.pp_id=?", ppId);
			if (status != null)
				from.append(" AND pkt_status=?", status);
		}
//		else if (active_user.isPanitia() != null) {
//			from.append("WHERE p.pnt_id=? ", panitiaId);
//		}
		else
		{
			from.append("WHERE p.pp_id IN (SELECT pp_id FROM ekontrak.pp WHERE peg_id=?)", active_user.pegawaiId);
			if (status != null)
				from.append("AND pkt_status=?", status);
		}
		renderJSON(datatable(select, from, column, NonPaketResultHandler.resultsetPaketPp));
	}

	@AllowAccess({Group.PANITIA})
	public static void paketPenunjukanLangsung(Long panitiaId, Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "pkt_tgl_buat", "stk_nama", "lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				" ekontrak.paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, " +
				"pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, ekontrak.tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status, ukpbj_id ,p.pkt_id_konsolidasi, p.is_pkt_konsolidasi", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p " +
				"LEFT JOIN public.satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN ekontrak.nonlelang_seleksi lls ON lls.pkt_id=p.pkt_id AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM ekontrak.nonlelang_seleksi WHERE pkt_id=p.pkt_id) AND p.pkt_id_konsolidasi is null");
		Active_user active_user = Active_user.current();
		if (panitiaId != null) {
			from.append("WHERE p.pnt_id=?", panitiaId);
			if (status != null)
				from.append(" AND pkt_status=?", status);
		}
		else {
			from.append(" WHERE p.pnt_id IN (SELECT pnt_id FROM public.anggota_panitia WHERE peg_id=?)", active_user.pegawaiId);
			if (status != null)
				from.append(" AND pkt_status=?", status);
//			from.append(" AND p.pkt_jenis = 0");
		}
		renderJSON(datatable(select, from, column, NonPaketResultHandler.resultsetPaketPp));
	}

	@AllowAccess({Group.PPK})
	public static void paketPpkNonTender(Long ppkId , Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "pkt_flag", "lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, (SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				" ekontrak.paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, " +
				"ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi, pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, ekontrak.tahap_now(lls_id, ?) AS tahaps, " +
				"lls_versi_lelang, lls_status ", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p JOIN (SELECT DISTINCT(ppk_id), pkt_id FROM ekontrak.paket_anggaran) ppk ON ppk.pkt_id=p.pkt_id LEFT JOIN ekontrak.nonlelang_seleksi lls ON lls.pkt_id=p.pkt_id " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM ekontrak.nonlelang_seleksi WHERE pkt_id=p.pkt_id) WHERE ppk.ppk_id=? AND p.pkt_id_konsolidasi is null", ppkId);
			if (status != null) {
				from.append("AND pkt_status=?", status);
			}
		from.append(" AND p.pkt_jenis=?", 0);
		from.append(" AND pkt_flag =?", 3);
		renderJSON(datatable(select, from, column, NonPaketResultHandler.resultsetPaketPp));

	}

	@AllowAccess({Group.KUPPBJ})
	public static void paketNonKuppbj(Long ukpbjId, Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "stk_nama", "pkt_flag", "lls_id", "pn.pnt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, pn.pnt_nama, is_pkt_konsolidasi, " +
				"(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM public.satuan_kerja s, " +
				"ekontrak.paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, " +
				"pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, ekontrak.tahap_now(lls_id, ?) AS tahaps, " +
				"lls_versi_lelang, lls_status", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p " +
				"LEFT JOIN public.satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN ekontrak.nonlelang_seleksi lls ON lls.pkt_id=p.pkt_id " +
				"LEFT JOIN panitia pn ON pn.pnt_id = p.pnt_id  " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM ekontrak.nonlelang_seleksi WHERE pkt_id=p.pkt_id)");
		from.append("WHERE p.ukpbj_id=? AND pkt_id_konsolidasi is null", ukpbjId);
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
//		from.append(" AND pkt_flag =?", 3);
		renderJSON(datatable(select, from, column, NonPaketResultHandler.resultsetPaketKuppbj));

	}

	/**
	 * get JSON for all LPSE list on DB
	 */
	@AllowAccess({Group.PP, Group.PANITIA})
	public static void PengadaanLPp(Long ppId, Long metodeId) {
		String[] column = new String[]{"lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, mtd_pemilihan, pkt_nama, pkt_flag, lls_versi_lelang, (select count(psr_id) from ekontrak.peserta_nonlelang where lls_id=l.lls_id) as peserta, ekontrak.tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l, ekontrak.paket p WHERE l.pkt_id=p.pkt_id AND lls_status=1");
		if(ppId != null) {
			from.append("AND p.pp_id=? ", ppId);
		} else {
			from.append("AND p.pp_id IN (SELECT pp_id FROM ekontrak.pp WHERE peg_id=?)", Active_user.current().pegawaiId);
		}
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetPLPanitia));
	}

	@AllowAccess({Group.PANITIA})
	public static void PengadaanLPanitia(Long panitiaId) {
		String[] column = new String[]{"lls_id", "pkt_nama"};
		QueryBuilder select =new QueryBuilder("SELECT lls_id, mtd_pemilihan, pkt_nama, pkt_flag, lls_versi_lelang, (select count(psr_id) from ekontrak.peserta_nonlelang where lls_id=l.lls_id) as peserta, ekontrak.tahap_now(lls_id, ?) AS tahaps", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l, ekontrak.paket p WHERE l.pkt_id=p.pkt_id AND lls_status=1");
		if(panitiaId != null) {
			from.append("AND p.pnt_id=?", panitiaId);
		} else {
			from.append("AND p.pnt_id IN (SELECT pnt_id FROM anggota_panitia WHERE peg_id=?)", Active_user.current().pegawaiId);
		}
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetPLPanitia));
	}

	@AllowAccess({Group.PPK})
	public static void SwakelolaPpk() {
		Long ppkId = Active_user.current().ppkId;
		String[] column = new String[]{"lls_id","lls_dibuat_tanggal", "pkt_nama", "pkt_status"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, lls_dibuat_tanggal, pkt_nama, p.pkt_status, pkt_flag");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.swakelola_seleksi l, ekontrak.paket_swakelola p ");
		from.append("WHERE l.swk_id=p.swk_id AND lls_status=1 AND p.ppk_id=? AND p.pkt_status != 0", ppkId);
		renderJSON(datatable(select, from, column, Swakelola_seleksi.resultsetSwakelolaPpk));

	}

	@AllowAccess({Group.PPK})
	public static void NonSpkPpk() {
		Long ppkId = Active_user.current().ppkId;
		String[] column = new String[]{"lls_id","lls_dibuat_tanggal","pkt_nama","mtd_pemilihan","pkt_status"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id,lls_dibuat_tanggal,pkt_nama,mtd_pemilihan, p.pkt_status, pkt_flag");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.non_spk_seleksi l, ekontrak.paket p ");
		from.append("WHERE l.pkt_id=p.pkt_id AND lls_status=1 AND p.ppk_id=? AND p.pkt_status != 0", ppkId);
		renderJSON(datatable(select, from, column, Non_spk_seleksi.resultsetNonSpkPp));

	}

	@AllowAccess({Group.PANITIA})
	public static void paketPanitiaForCopy(Long lls_id) {
		Lelang_seleksi ls = Lelang_seleksi.findById(lls_id);
		Panitia panitia = ls.getPanitia();
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status","pkt_tgl_buat", "stk_nama", "pkt_flag", "lls_id", "lls_versi_lelang", "lls_penawaran_ulang"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, p.pkt_nama, p.pkt_tgl_buat, s.stk_nama, p.pkt_pagu, p.pkt_status, p.pkt_flag, ls.lls_id, " +
				"ls.mtd_pemilihan, tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status, lls_penawaran_ulang, lls_evaluasi_ulang, ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi", newDate());
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi ls LEFT JOIN paket p ON ls.pkt_id=p.pkt_id LEFT JOIN satuan_kerja s on s.stk_id = p.stk_id ")
				.append("WHERE ls.lls_id <> ? AND ls.mtd_pemilihan = ? AND ls.mtd_id = ? AND ls.mtd_evaluasi = ? AND p.kgr_id=? AND p.pnt_id= ? AND p.pkt_flag > 1 AND p.pkt_id_konsolidasi is null",
						lls_id, ls.mtd_pemilihan, ls.mtd_id, ls.mtd_evaluasi, ls.getKategori().id, panitia.pnt_id);
		renderJSON(datatable(select, from, column, PaketResultHandler.resultsetPaketList));
	}
	/**
	 * Datfar lelang by status
	 */
	@AllowAccess(Group.REKANAN)
	public static void dataTableStatusLelang(Integer status) {
		Long rekananId = Active_user.current().rekananId;
		String[] column = new String[]{"l.lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT l.lls_id,mtd_pemilihan, p.kgr_id, p.is_pkt_konsolidasi, lls_versi_lelang, pkt_nama, pkt_flag, tahap_now(l.lls_id, ?) AS tahaps ", newDate());
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l, paket p, peserta psr WHERE p.pkt_id=l.pkt_id AND l.lls_id=psr.lls_id AND psr.rkn_id=?", rekananId);
		if(status != null && status == 2) {
			from.append("AND lls_status=? AND l.pkt_id NOT IN (SELECT pkt_id FROM lelang_seleksi WHERE lls_status=1)", status);
		}
		else
			from.append("AND l.lls_status = 1 ");
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetLelangRekanan));
	}

	@AllowAccess(Group.REKANAN)
	public static void dataTableStatusPl() {
		Long rekananId = Active_user.current().rekananId;
		String[] column = new String[]{"l.lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT l.lls_id,mtd_pemilihan, p.kgr_id, p.is_pkt_konsolidasi,l.lls_versi_lelang, pkt_nama, pkt_flag, ekontrak.tahap_now(l.lls_id, ?) AS tahaps ", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l, ekontrak.paket p, ekontrak.peserta_nonlelang psr WHERE l.lls_status = 1 AND p.pkt_id=l.pkt_id AND l.lls_id=psr.lls_id AND psr.rkn_id=?", rekananId);
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetPlRekanan));
	}

	/**
	 * get JSON for all user log
	 */
	@AllowAccess(Group.ADM_PPE)
	public static void accessLog() {
		String[] column = new String[]{"userid", "sessiontime", "logouttime", "station"};
		QueryBuilder select = new QueryBuilder("SELECT userid, sessiontime, logouttime, station");
		QueryBuilder from = new QueryBuilder("FROM usrsession");
		renderJSON(datatable(select, from, column, Usrsession.resultsetAccessLog));
	}

	/**
	 * get JSON for all user log
	 */
	@AllowAccess(Group.ALL_AUTHORIZED_USERS)
	public static void accessLogUser() {
		String[] column = new String[]{"sessiontime", "logouttime", "station"};
		QueryBuilder select = new QueryBuilder("SELECT userid, sessiontime, logouttime, station");
		QueryBuilder from = new QueryBuilder("FROM usrsession");
		String userid= Active_user.current().userid;
		if(!StringUtils.isEmpty(userid))
		{
			from.append(" WHERE userid=?", userid);
		}
		renderJSON(datatable(select, from, column, Usrsession.resultsetUserlog));
	}

	/**
	 * get JSON for all LPSE list on DB
	 */
	public static void lpse() {
		String[] column = new String[]{"pps_id", "pps_nama", "pps_alamat", "pps_display"};
		QueryBuilder select = new QueryBuilder("SELECT pps_id, pps_nama, pps_alamat, pps_display");
		QueryBuilder from = new QueryBuilder("FROM ppe_site");
		renderJSON(datatable(select, from, column, Ppe_site.resultsetLpse));
	}

	/**Digunakan pada halaman publik */
	public static void lelang(Kategori kategori, String rkn_nama) {
		checkAuthenticity();
		String[] column = new String[]{"l.lls_id","pkt_nama", "paket_instansi(p.pkt_id)", "tahap","pkt_hps"};
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id LEFT JOIN kontrak k ON l.lls_id=k.lls_id WHERE l.lls_status=1 " +
				"AND ((l.mtd_pemilihan NOT IN (9,10) AND tahap_now(l.lls_id, ?) <> '" + Tahap.BELUM_DILAKSANAKAN + "') OR l.mtd_pemilihan IN (9,10))", newDate());

		QueryBuilder select = new QueryBuilder("SELECT l.lls_id, p.kgr_id, p.pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_id, mtd_pemilihan, mtd_evaluasi, kgr_id, "
				+ "paket_instansi(p.pkt_id) as nama_instansi, lls_penawaran_ulang, kontrak_id, kontrak_nilai, p.is_pkt_konsolidasi, "
				+ "(SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') FROM Anggaran a, Paket_anggaran pa WHERE pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, "
				+ "tahap_now(l.lls_id, ?) AS tahaps, lls_versi_lelang, lls_evaluasi_ulang", newDate());

		if(kategori != null) {
			from.append(" AND p.kgr_id=?", kategori.id);
		}
		if (StringUtils.isNotEmpty(rkn_nama)) {// hanya tahap lelang sudah kontrak
			from.append(" AND l.lls_id IN (SELECT lls_id FROM peserta p JOIN rekanan ON p.rkn_id=rekanan.rkn_id WHERE lower(rkn_nama) LIKE ?) AND tahap_now(l.lls_id, ?) = 'SUDAH_SELESAI'",
					"%"+rkn_nama.trim().toLowerCase()+"%",newDate());
		}
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetLelang));
	}

	/**Digunakan pada halaman publik */
	public static void pl(Kategori kategori, String rkn_nama) {
		checkAuthenticity();
		String[] column = new String[]{"l.lls_id","pkt_nama", "ekontrak.paket_instansi(p.pkt_id)", "tahap","pkt_hps"};
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l LEFT JOIN ekontrak.paket p ON l.pkt_id=p.pkt_id LEFT JOIN ekontrak.kontrak k ON k.lls_id=l.lls_id WHERE l.lls_status=1 " +
				"AND ((l.mtd_pemilihan NOT IN (9,10) AND ekontrak.tahap_now(l.lls_id, ?) <> '" + Tahap.BELUM_DILAKSANAKAN + "') OR l.mtd_pemilihan IN (7,8))", newDate());

		QueryBuilder select = new QueryBuilder("SELECT l.lls_id, p.kgr_id, p.pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_pemilihan, mtd_evaluasi, kgr_id, "
				+ "(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct nama), ', ') FROM instansi WHERE id in (SELECT instansi_id FROM satuan_kerja sk, ekontrak.paket_satker ps WHERE ps.stk_id=sk.stk_id AND ps.pkt_id=p.pkt_id)) AS nama_instansi, kontrak_id, kontrak_nilai,p.is_pkt_konsolidasi,"
				+ "(SELECT array_to_string(array_agg(distinct a.ang_tahun), ',') from Anggaran a, ekontrak.Paket_anggaran pa where pa.ang_id=a.ang_id and pa.pkt_id=p.pkt_id) AS anggaran, "
				+ "ekontrak.tahap_now(l.lls_id, ?) AS tahaps, lls_versi_lelang , (SELECT MAX(eva_versi) FROM ekontrak.evaluasi WHERE lls_id=l.lls_id) as eva_versi", newDate());
		if(kategori != null) {
			from.append(" AND p.kgr_id=?", kategori.id);
		}
		if (StringUtils.isNotEmpty(rkn_nama)) {// hanya tahap lelang sudah kontrak
			from.append(" AND l.lls_id IN (SELECT lls_id FROM ekontrak.peserta_nonlelang p JOIN rekanan ON p.rkn_id=rekanan.rkn_id WHERE lower(rkn_nama) LIKE ?) AND ekontrak.tahap_now(l.lls_id, ?) = 'SUDAH_SELESAI'",
					"%"+rkn_nama.trim().toLowerCase()+"%",newDate());
		}
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetPL));
	}

	/**Digunakan pada halaman helpdesk */
	@AllowAccess({Group.HELPDESK})
	public static void lelangForensik() {
		String[] column = new String[]{"lelang.lls_id","lelang.pkt_nama", "lelang.auditupdate"};
		QueryBuilder from_sub1 = new QueryBuilder("FROM (SELECT DISTINCT ON(l.lls_id) l.lls_id, p.pkt_nama, rp.auditupdate FROM lelang_seleksi l INNER JOIN paket p ON l.pkt_id=p.pkt_id " +
				"INNER JOIN peserta ps ON l.lls_id = ps.lls_id INNER JOIN rhs_permission rp ON ps.psr_id = rp.psr_id WHERE (rp.rpn_allow_harga = 1 OR rp.rpn_allow_teknis = 1)");
		QueryBuilder from_sub2 = new QueryBuilder(") AS lelang");
		QueryBuilder select = new QueryBuilder("SELECT * ");
		renderJSON(datatableForensik(select, from_sub1, from_sub2, column, LelangSeleksiResultHandler.resultsetForensik));
	}

	public static void lelangKontrak(Kategori kategori) {
		String[] column = new String[]{"l.lls_id","pkt_nama", "agc_nama","pkt_hps", "brc_id"};
		QueryBuilder select = new QueryBuilder("SELECT l.lls_id, l.mtd_pemilihan, p.pkt_id, pkt_nama, pkt_hps, p.pkt_flag, mtd_id, mtd_pemilihan, mtd_evaluasi, kgr_id, agc_nama, b.brc_id, i.nama, tahap_now(lls_id, ?) AS tahaps", newDate());
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l LEFT JOIN paket p ON l.pkt_id=p.pkt_id LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id LEFT JOIN agency a ON s.agc_id=a.agc_id LEFT JOIN instansi i ON s.instansi_id=i.id LEFT JOIN berita_acara b ON l.lls_id=b.lls_id WHERE l.lls_status=1");
		if(kategori != null) {
			from.append(" AND p.kgr_id=?", kategori.id);
		}
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetkontrak));
	}
	/**
	 * get JSON for all LPSE list on DB
	 */
	@AllowAccess({Group.PANITIA})
	public static void lelangPanitia(Long panitiaId, Integer status) {
		String[] column = new String[]{"lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, mtd_pemilihan, lls_versi_lelang, p.kgr_id, pkt_nama, pkt_flag, p.is_pkt_konsolidasi, (select count(psr_id) from peserta where lls_id=l.lls_id) as peserta, tahap_now(lls_id, ?) AS tahaps", newDate());
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l, paket p WHERE l.pkt_id=p.pkt_id AND lls_status=?", status != null ? status: StatusLelang.AKTIF);
		if(panitiaId != null) {
			from.append("AND p.pnt_id=?", panitiaId);
		} else {
			from.append("AND p.pnt_id IN (SELECT pnt_id FROM anggota_panitia WHERE peg_id=?)", Active_user.current().pegawaiId);
		}
		if(status != null && status == 2) {
			from.append("AND lls_status=? AND l.pkt_id NOT IN (SELECT pkt_id FROM lelang_seleksi WHERE lls_status=1)", status);
		}else {
			from.append("AND lls_status=1");
		}
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetLelangPanitia));
	}

	/**
	 * get data lelang manual (non eproc)+
	 * format JSON
	 */
	public static void nonEproc() {
		String[] column = new String[]{"pl.pll_id","plp_nama_paket","stk_nama","plp_hps"};
		QueryBuilder select=new QueryBuilder("SELECT pl.pll_id, s.stk_nama, pp.plp_nama_paket, pp.plp_pagu ,pp.plp_hps, pt.pll_tgl_awal, pt.pll_tgl_akhir, pl.pll_status");
		QueryBuilder from = new QueryBuilder("FROM pengumuman_lelang pl, pll_paket pp, pll_tahap pt, satuan_kerja s WHERE pl.pll_id = pp.pll_id AND pl.pll_id=pt.pll_id AND pl.stk_id=s.stk_id AND pt.nama_tahap='Waktu & Tempat Pengambilan Dokumen'");
		renderJSON(datatable(select, from, column, Pengumuman_lelang.resultsetNonEproc));
	}

	/**
	 * get data lelang manual (non eproc) yang dibuat oleh panitia
	 * format JSON
	 */
	@AllowAccess({Group.PANITIA})
	public static void nonEprocPanitia() {
//    	Long panitiaId = Active_user.current().panitiaId;
		String[] column = new String[]{"pll_id","pll_no_peng","pll_tgl_buat","pll_tahun","pll_status"};
		QueryBuilder select=new QueryBuilder("SELECT pll_id, pll_no_peng, pll_status, pll_tgl_buat, pll_tahun, (SELECT COUNT(plp_paket) FROM pll_paket p WHERE pll_id=pengumuman_lelang.pll_id) as jumlah_paket");
		QueryBuilder from = new QueryBuilder("FROM pengumuman_lelang WHERE pnt_id=?");
		renderJSON(datatable(select, from, column, Pengumuman_lelang.resultsetNonEprocPanitia));
	}

	@AllowAccess({Group.PPK})
	public static void rencanaPaket(Long pkt_id, Long satkerId, Integer tahun, Integer metodePemilihan) {
		if(satkerId != null && tahun != null)
		{
			String[] column = new String[]{"id", "nama", "paket_anggaran -> 0 ->> 'sumber_dana'", "metode_pengadaan", "pagu","tahun"};
			QueryBuilder select = new QueryBuilder("SELECT id, nama, pagu, tahun, paket_anggaran -> 0 ->> 'sumber_dana' as sumberdana, metode_pengadaan, (select pkt_id from paket where rup_id=p.id limit 1) as paket ");
			QueryBuilder from = new QueryBuilder("FROM public.paket_sirup p, satuan_kerja s WHERE p.rup_stk_id=s.rup_stk_id AND p.tahun= ?",tahun);
			if(satkerId != null) {
				from.append(" AND s.stk_id = ?", satkerId);
			}
			if (pkt_id != null) {
				from.append(" AND p.id NOT IN (SELECT rup_id FROM paket_satker WHERE pkt_id=?)", pkt_id);
			}
			if (metodePemilihan != null && metodePemilihan != 0) {
				from.append(" AND p.metode_pengadaan=?", metodePemilihan);
			}
			renderJSON(datatable(select, from, column, PaketSirup.resultset));
		}
		else
			renderJSON(ssp_empty());
	}

	public static void rencanaPaketNonPl(Long pkt_id, Long satkerId, Integer tahun, Integer metodePemilihan) {
		if(satkerId != null && tahun != null)
		{
			String[] column = new String[]{"id", "nama", "paket_anggaran -> 0 ->> 'sumber_dana'", "metode_pengadaan", "pagu", "tahun"};
			QueryBuilder select =new QueryBuilder("SELECT id, nama, pagu, tahun, paket_anggaran -> 0 ->> 'sumber_dana' as sumberdana, metode_pengadaan, (select pkt_id from ekontrak.paket where rup_id=p.id limit 1) as paket ");
			QueryBuilder from = new QueryBuilder("FROM public.paket_sirup p, satuan_kerja s WHERE p.rup_stk_id=s.rup_stk_id AND p.tahun=?",tahun);
			if(satkerId != null) {
				from.append(" AND s.stk_id=?", satkerId);
			}
			if (pkt_id != null) {
				from.append(" AND p.id NOT IN (SELECT rup_id FROM ekontrak.paket_satker WHERE pkt_id=?)", pkt_id);
			}
			if(metodePemilihan != null && metodePemilihan != 0) {
				from.append(" AND p.metode_pengadaan=?", metodePemilihan);
			}

			renderJSON(datatable(select, from, column, PaketSirup.resultset));
		}
		else
			renderJSON(ssp_empty());
	}

	public static void rencanaPaketPl(Long paketId, Long satkerId, Integer tahun, Integer metodePemilihan) {
		if(satkerId != null && tahun != null)
		{
			String[] column = new String[]{"id", "nama", "paket_anggaran -> 0 ->> 'sumber_dana'", "metode_pengadaan", "pagu","tahun"};
			QueryBuilder select =new QueryBuilder("SELECT id, nama, pagu, tahun, paket_anggaran -> 0 ->> 'sumber_dana' as sumberdana, metode_pengadaan, (select pkt_id from ekontrak.paket where rup_id=p.id limit 1) as paket ");
			QueryBuilder from = new QueryBuilder("FROM public.paket_sirup p, satuan_kerja s WHERE p.rup_stk_id=s.rup_stk_id AND p.tahun= ?",tahun);
			if(satkerId != null) {
				from.append(" AND s.stk_id=?", satkerId);
			}
			if (paketId != null) {
				from.append(" AND p.id NOT IN (SELECT rup_id FROM ekontrak.paket_satker WHERE pkt_id=?)", paketId);
			}
			if(metodePemilihan != null && metodePemilihan != 0) {
				from.append(" AND p.metode_pengadaan=?", metodePemilihan);
			}

			renderJSON(datatable(select, from, column, PaketSirup.resultset));
		}
		else
			renderJSON(ssp_empty());
	}

	public static void rencanaPaketSwakelola(Long paketId, Long satkerId, Integer tahun, Integer tipeSwakelola) {
		if(satkerId != null)
		{
			String[] column = new String[]{"id", "nama", "jumlah_pagu", "tahun"};
			QueryBuilder select = new QueryBuilder("SELECT id, nama, jumlah_pagu, tahun, paket_anggaran -> 0 ->> 'sumber_dana' as sumberdana," +
					"(select swk_id from ekontrak.paket_swakelola where rup_id=p.id limit 1) as paket ");
			QueryBuilder from = new QueryBuilder("FROM ekontrak.paket_swakelola_sirup p, satuan_kerja s WHERE p.rup_stk_id=s.rup_stk_id AND p.tahun=?", tahun);
			if(satkerId != null) {
				from.append(" AND s.stk_id=?", satkerId);
			}
			if (paketId != null) {
				from.append(" AND p.id NOT IN (SELECT rup_id FROM ekontrak.paket_satker WHERE pkt_id=?)", paketId);
			}
			if(tipeSwakelola != null && tipeSwakelola != 0) {
				from.append(" AND p.tipe_swakelola=?", tipeSwakelola);
			}
			renderJSON(datatable(select, from, column, PaketSwakelolaSirup.resultset));
		}
		else
			renderJSON(ssp_empty());
	}

	@AllowAccess(Group.REKANAN)
	public static void inbox(BACA_STATUS status) {
		Long rekananId = Active_user.current().rekananId;
		String[] column = new String[]{"id", "enqueue_date", "subject", "m.lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT id, enqueue_date, subject, bcstat, m.lls_id, l.mtd_pemilihan as l_mtd, nl.mtd_pemilihan as nl_mtd, pl.pkt_flag as pl_flag");
		QueryBuilder from = new QueryBuilder("FROM mail_queue m LEFT JOIN lelang_seleksi l on l.lls_id = m.lls_id LEFT JOIN ekontrak.nonlelang_seleksi nl ON nl.lls_id = m.lls_id " +
				"LEFT JOIN ekontrak.paket pl ON nl.pkt_id =  pl.pkt_id WHERE rkn_id = ?", rekananId);
		if(status != null) {
			from.append(" AND bcstat=?", status.ordinal());
		}
		renderJSON(datatable(select, from, column, Rekanan.resultsetInbox));
	}

	public static void rekap_auditor(Long skId,Boolean kadaluarsa){
		String[] column = new String[]{"lls_id","pkt_nama", "pkt_pagu", "stk_nama", "pkt_flag", "l.pkt_id"};
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l join paket p on l.pkt_id=p.pkt_id LEFT JOIN paket_satker pst ON pst.pkt_id=p.pkt_id LEFT JOIN satuan_kerja s ON s.stk_id=pst.stk_id join paket_skauditor ps on ps.pkt_id=p.pkt_id ");
		QueryBuilder select = new QueryBuilder("SELECT l.lls_id, l.mtd_pemilihan, pkt_nama, pkt_pagu, p.pkt_flag ,p.pkt_id, stk_nama, tahap_now(l.lls_id, ?) AS tahaps, l.lls_status, lls_versi_lelang ", newDate());
		if(skId != null && !kadaluarsa) {
			from.append(" AND ps.skid=?", skId);
		}else{
			from.append(" AND ps.skid=null");
		}
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetRekapAuditor));
	}

	public static void rekap_auditor_nonlelang(Long skId,Boolean kadaluarsa){
		String[] column = new String[]{"lls_id","pkt_nama", "pkt_pagu", "ekontrak.paket_satker(p.pkt_id)", "tahap","pkt_flag","pkt_id"};
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l join ekontrak.paket p on l.pkt_id=p.pkt_id join ekontrak.paket_skauditor ps on ps.pkt_id=p.pkt_id ");
		QueryBuilder select = new QueryBuilder("SELECT l.lls_id, l.mtd_pemilihan, pkt_nama, pkt_pagu, p.pkt_flag ,p.pkt_id, ekontrak.tahap_now(l.lls_id, ?) AS tahaps, l.lls_status, lls_versi_lelang , ekontrak.paket_satker(p.pkt_id) AS stk_nama ", newDate());
		if(skId != null && !kadaluarsa) {
			from.append(" AND ps.skid=?", skId);
		}else{
			from.append(" AND ps.skid=null");
		}
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetRekapAuditor));
	}

	@AllowAccess({Group.PPK})
	public static void paketPpk() {
		Long ppkId = Active_user.current().ppkId;
		String[] column = new String[]{"l.lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("select lls_id, mtd_pemilihan, lls_versi_lelang, p.pkt_nama, p.is_pkt_konsolidasi, p.pkt_flag, p.kgr_id , (select eva_status from evaluasi where lls_id=l.lls_id and eva_jenis=4 order by eva_versi desc limit 1) as status, tahap_now(lls_id, ?) AS tahaps ", newDate());
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l, paket p WHERE l.pkt_id=p.pkt_id AND l.lls_status= 1 AND l.pkt_id in (select pkt_id from paket_anggaran where ppk_id=?) AND p.pkt_id_konsolidasi is null ", ppkId);
		renderJSON(datatable(select, from, column, Ppk.resultsetPaketPPK));
	}

	@AllowAccess({Group.PPK})
	public static void ppkSppbj(long llsId) {
		Long ppkId = Active_user.current().ppkId;
		String[] column = new String[]{"sppbj_no", "sppbj_tgl_kirim", "rkn_nama"};
		QueryBuilder select = new QueryBuilder("SELECT sppbj_id, sppbj_no, sppbj_tgl_kirim, rkn_nama, harga_final, kontrak_nilai, kgr_id, kontrak_id, kontrak_sskk, (SELECT pes_id FROM pesanan WHERE kontrak_id=k.kontrak_id LIMIT 1) pes_id ");
		QueryBuilder from = new QueryBuilder("FROM sppbj s INNER JOIN  rekanan rkn ON s.rkn_id=rkn.rkn_id INNER JOIN lelang_seleksi l ON l.lls_id=s.lls_id")
				.append(" INNER JOIN paket p ON p.pkt_id=l.pkt_id LEFT JOIN kontrak k ON k.lls_id=s.lls_id AND k.rkn_id=s.rkn_id AND k.ppk_id=s.ppk_id")
				.append(" WHERE s.ppk_id=? and s.lls_id=?", ppkId, llsId);
		renderJSON(datatable(select, from, column, Ppk.resultsetPpkSppjb));
	}

	@AllowAccess({Group.PPK})
	public static void ppkSppbjPl(long llsId) {
		Long ppkId = Active_user.current().ppkId;
		String[] column = new String[]{"sppbj_no", "sppbj_tgl_kirim", "rkn_nama"};
		QueryBuilder select = new QueryBuilder("select sppbj_id, sppbj_no, sppbj_tgl_kirim, rkn_nama, harga_final, kontrak_nilai, kgr_id, kontrak_id, kontrak_sskk, (SELECT pes_id FROM ekontrak.pesanan WHERE kontrak_id=k.kontrak_id LIMIT 1) pes_id");
		QueryBuilder from = new QueryBuilder("from ekontrak.sppbj s inner join rekanan rkn on s.rkn_id=rkn.rkn_id inner join ekontrak.nonlelang_seleksi l on l.lls_id=s.lls_id")
				.append(" inner join ekontrak.paket p on p.pkt_id=l.pkt_id left join ekontrak.kontrak k on k.lls_id=s.lls_id and k.rkn_id=s.rkn_id and k.ppk_id=s.ppk_id")
				.append(" where s.ppk_id=? and s.lls_id=?", ppkId, llsId);
		renderJSON(datatable(select, from, column, Ppk.resultsetPpkSppjb));
	}

	@AllowAccess({Group.PPK})
	public static void paketPpkPl() {
		Long ppkId = Active_user.current().ppkId;
		String[] column = new String[]{"l.lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("select lls_id, mtd_pemilihan, lls_versi_lelang, p.pkt_nama, p.is_pkt_konsolidasi,p.pkt_flag, p.kgr_id , (select eva_status from ekontrak.evaluasi where lls_id=l.lls_id and eva_jenis=4 order by eva_versi desc limit 1) as status, ekontrak.tahap_now(lls_id, ?) AS tahaps ,(select sudah_verifikasi_sikap FROM ekontrak.peserta_nonlelang p join ekontrak.nilai_evaluasi n on n.psr_id =p.psr_id join ekontrak.evaluasi e on  e.eva_id =n.eva_id and e.eva_jenis =4 where p.lls_id =l.lls_id order by nev_urutan limit 1) sudah_verifikasi_sikap", newDate());
		QueryBuilder from = new QueryBuilder("from ekontrak.nonlelang_seleksi l, ekontrak.paket p where l.pkt_id=p.pkt_id AND l.lls_status= 1 AND p.pkt_id in (select pp.pkt_id FROM ekontrak.paket_anggaran pp where pp.ppk_id=?) AND p.pkt_id_konsolidasi is null", ppkId);
		renderJSON(datatable(select, from, column, Ppk.resultsetNonPaketPPK));
	}

	/**
	 * Daftar paket berdasarkan pejabat pengadaan
	 *
	 * @param status kode status epl lainnya
	 */
	@AllowAccess({Group.PP})
	public static void paketLainnya(Long ppId, Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "stk_nama", "pkt_flag", "lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT p.pkt_id, pkt_nama, pkt_tgl_buat, stk_nama, pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, tahap_now(lls_id, ?) AS tahaps", newDate());
		Active_user active_user = Active_user.current();
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id LEFT JOIN ekontrak.nonlelang_seleksi lls ON lls.pkt_id=p.pkt_id "
				+ "AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM ekontrak.nonlelang_seleksi WHERE lls_id=lls.lls_id)");
		if (ppId != null) {
			from.append("WHERE p.pp_id=?", ppId);
			if (status != null) {
				from.append(" AND pkt_status=?", status);
			}
		} else {
			from.append("WHERE p.pp_id IN (SELECT pp_id FROM ekontrak.pp WHERE peg_id=?)", active_user.pegawaiId);
			if (status != null) {
				from.append(" AND pkt_status=?", status);
			}
		}
		renderJSON(datatable(select, from, column, Paket_pl.resultsetPaketpp));
	}

	@AllowAccess({Group.PPK})
	public static void paketPpkNonSpk(Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "stk_nama", "pkt_flag", "lls_id", "mtd_pemilihan"};
		QueryBuilder select = new QueryBuilder("SELECT p.pkt_id, pkt_nama, pkt_tgl_buat, pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, " +
				"(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				"ekontrak.paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) AS stk_nama");

		Active_user active_user = Active_user.current();

		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p LEFT JOIN public.satuan_kerja s ON p.stk_id=s.stk_id LEFT JOIN ekontrak.non_spk_seleksi lls ON lls.pkt_id=p.pkt_id " +
				"WHERE p.ppk_id=?", active_user.ppkId);
		if (status != null) {
			from.append(" AND pkt_status = ? ", status);
		}
		from.append(" AND p.pkt_jenis = 1");

		renderJSON(datatable(select, from, column, Paket_pl.resultsetPaketNonSpkPp));


	}

	@AllowAccess({Group.PPK})
	public static void paketPpkPencatatan(Long ppkId, Integer status){
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "stk_nama", "pkt_flag", "lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT p.pkt_id, pkt_nama, pkt_tgl_buat, " +
				"(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				"ekontrak.paket_satker ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, " +
				"pkt_pagu, pkt_status, pkt_flag, lls_id, mtd_pemilihan, tahap_now(lls_id, ?) AS tahaps, " +
				"lls_status, ppk.ppk_id, ukpbj_id", newDate());
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket p " +
				"JOIN ekontrak.paket_ppk ppk ON ppk.pkt_id=p.pkt_id " +
				"LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN ekontrak.non_spk_seleksi lls ON lls.pkt_id=p.pkt_id ");
//				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
//				"LEFT JOIN ekontrak.non_spk_seleksi l ON l.lls_id=lls.lls_id");
		Active_user active_user = Active_user.current();
		if (ppkId != null) {
			from.append("WHERE ppk.ppk_id=?", ppkId);
			if (status != null) {
				from.append("AND pkt_status=?", status);
			}
		} else {
			from.append("WHERE p.ppk_id IN (SELECT ppk_id FROM ppk WHERE peg_id=?)", active_user.pegawaiId);
			if (status != null) {
				from.append("AND pkt_status=?", status);
			}
		}
		from.append(" AND p.pkt_jenis =?", 1);
		from.append(" AND pkt_flag =?", 3);
		renderJSON(datatable(select, from, column, Paket_pl.resultsetNonTenderPpk));

	}

	/**
	 * Daftar paket berdasarkan pejabat pengadaan
	 *
	 * @param status kode status swakelola
	 */
	@AllowAccess({Group.PPK})
	public static void paketPpkSwakelola(Long ppkId,Integer status){
		String[] column = new String[]{"p.swk_id", "pkt_nama", "pkt_status", "stk_nama", "pkt_flag", "lls_id"};
		QueryBuilder select = new QueryBuilder("SELECT p.swk_id, pkt_nama, pkt_tgl_buat, " +
				"(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s," +
				"ekontrak.paket_satker_swakelola ps WHERE ps.stk_id=s.stk_id AND ps.swk_id=p.swk_id) as stk_nama," +
				"pkt_pagu, pkt_status, pkt_flag, lls_id");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.paket_swakelola p " +
				"JOIN ekontrak.paket_ppk_swakelola ppk ON ppk.swk_id=p.swk_id " +
				"LEFT JOIN public.satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN ekontrak.swakelola_seleksi lls ON lls.swk_id=p.swk_id " +
				"WHERE ppk.ppk_id=?", ppkId);
		if (status != null) {
			from.append(" AND pkt_status = ? ", status);
		}
		renderJSON(datatable(select, from, column, PaketSwaResultHandler.resultsetPaketList));
	}

	@AllowAccess({Group.ADM_PPE})
	public static void summaryLelang() {
		String[] column = new String[] { "l.lls_id", "lls_dibuat_tanggal", "pkt_nama", "paket_instansi(p.pkt_id)", "paket_satker(p.pkt_id)", "pkt_hps", "pkt_pagu" };
		QueryBuilder from = new QueryBuilder("FROM lelang_seleksi l, paket p WHERE l.lls_status > 0 AND l.pkt_id=p.pkt_id ");
		QueryBuilder select = new QueryBuilder("SELECT  l.lls_id, l.mtd_pemilihan, l.lls_status, l.lls_dibuat_tanggal, p.pkt_nama, p.pkt_pagu, pkt_hps, paket_satker(p.pkt_id) as stk_nama, paket_instansi(p.pkt_id) as nama, p.pkt_id, p.pkt_flag, tahap_now(l.lls_id, ?) AS tahaps ", newDate());
		renderJSON(datatable(select, from, column, LelangSeleksiResultHandler.resultsetSummary));
	}

	@AllowAccess({Group.ADM_PPE})
	public static void summaryNonTender() {
		String[] column = new String[] { "l.lls_id", "lls_dibuat_tanggal", "pkt_nama", "ekontrak.paket_instansi(p.pkt_id)", "ekontrak.paket_satker(p.pkt_id)", "pkt_hps", "pkt_pagu" };
		QueryBuilder from = new QueryBuilder("FROM ekontrak.nonlelang_seleksi l, ekontrak.paket p WHERE l.lls_status > 0 AND l.pkt_id=p.pkt_id ");
		QueryBuilder select = new QueryBuilder("SELECT  l.lls_id, l.mtd_pemilihan, l.lls_status, l.lls_dibuat_tanggal, p.pkt_nama, p.pkt_pagu, pkt_hps, ekontrak.paket_satker(p.pkt_id) as stk_nama, ekontrak.paket_instansi(p.pkt_id) as nama, p.pkt_id, p.pkt_flag, ekontrak.tahap_now(l.lls_id, ?) AS tahaps ", newDate());
		renderJSON(datatable(select, from, column, PlSeleksiResultHandler.resultsetSummary));
	}

	@AllowAccess({Group.PANITIA})
	public static void unspsc() {
		String[] column = new String[]{"commodity_id", "commodity_title", "class_title", "family_title", "segment_title"};
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM unspsc");
		renderJSON(datatable(select, from, column, Unspsc.resultset));
	}

	public static void ppk(Long id, String insId) {
		String[] column = new String[]{"ppk_id","peg_nip", "peg_nama", "peg_namauser"};
		QueryBuilder select = new QueryBuilder("select ppk_id, peg_nama, peg_nip, peg_namauser");
		QueryBuilder from = new QueryBuilder ("from ppk, pegawai where ppk.peg_id=pegawai.peg_id and pegawai.disableuser=0");
		if(!StringUtils.isEmpty(insId)) {
			from = new QueryBuilder("FROM ppk INNER JOIN pegawai ON ppk.peg_id = pegawai.peg_id INNER JOIN agency ON pegawai.agc_id = agency.agc_id " +
					"INNER JOIN instansi ON agency.kbp_id = instansi.kbp_id WHERE instansi.id = ? and pegawai.disableuser=0 ", insId);
		}
		renderJSON(datatable(select, from, column, Ppk.resultsetPPK));
	}

	public static void ppkPl(Long id, String insId) {
		String[] column = new String[]{"ppk_id","peg_nip", "peg_nama", "peg_namauser"};
		QueryBuilder select = new QueryBuilder("select ppk_id, peg_nama, peg_nip, peg_namauser");
		QueryBuilder from = new QueryBuilder ("FROM ppk, pegawai WHERE ppk.peg_id=pegawai.peg_id and pegawai.disableuser=0");
		if(!StringUtils.isEmpty(insId)) {
			from = new QueryBuilder ("FROM ppk INNER JOIN pegawai ON ppk.peg_id = pegawai.peg_id INNER JOIN agency ON pegawai.agc_id = agency.agc_id " +
					"INNER JOIN instansi ON agency.kbp_id = instansi.kbp_id WHERE instansi.id = ? AND pegawai.disableuser=0 ", insId);
		}
		renderJSON(datatable(select, from, column, Ppk.resultsetPPK));
	}

	private static String ssp_empty() {
		Map<String, Object> output = new HashMap<String, Object>();
		output.put("draw", params.get("draw"));
		output.put("recordsTotal", 0);
		output.put("recordsFiltered",0);
		output.put("data", new ArrayList<String>());
		return gson.toJson(output);
	}

	/**
	 * get data Propinsi
	 * format JSON
	 */
	public static void propinsi() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM propinsi");
		String[] column = new String[]{"prp_id", "prp_nama"};
		renderJSON(datatable(select, from, column, resultsetPropinsi));
	}

	/**
	 * get data Kabupaten
	 * format JSON
	 */
	public static void kabupaten() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM kabupaten");
		String[] column = new String[]{"kbp_id", "kbp_nama", "prp_id"};
		renderJSON(datatable(select, from, column, resulsetKabupaten));
	}

	private static final ResultSetHandler<String[]> resulsetKabupaten = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[3];
			obj[0] = rs.getString("kbp_id");
			obj[1] = rs.getString("kbp_nama");
			obj[2] = rs.getString("prp_id");
			return obj;
		}
	};

	private static final ResultSetHandler<String[]> resultsetPropinsi = new ResultSetHandler<String[]>() {

		@Override
		public String[] handle(ResultSet rs) throws SQLException {
			String[] obj = new String[3];
			obj[0] = rs.getString("prp_id");
			obj[1] = rs.getString("prp_nama");
			return obj;
		}
	};

	@AllowAccess({Group.REKANAN})
	public static void ijinUsaha() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM ijin_usaha WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"ius_id", "jni_nama", "ius_no", "ius_berlaku", "ius_instansi", "kls_id"};
		renderJSON(datatable(select, from, column, Ijin_usaha.resulset));
	}

	@AllowAccess({Group.REKANAN})
	public static void pajak() {
		QueryBuilder select =new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM pajak WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"pjk_id", "pjk_jenis", "pjk_tanggal", "pjk_no"};
		renderJSON(datatable(select, from, column, Pajak.resulset));
	}

	@AllowAccess({Group.REKANAN})
	public static void pemilik() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM pemilik WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"pml_id", "pml_nama", "pml_ktp", "pml_alamat", "pml_saham"};
		renderJSON(datatable(select, from, column, Pemilik.resulset));
	}

	@AllowAccess({Group.REKANAN})
	public static void pengurus() {
		QueryBuilder select =new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM pengurus WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"pgr_id", "pgr_nama", "pgr_ktp", "pgr_alamat", "pgr_jabatan", "pgr_valid_start", "pgr_valid_end"};
		renderJSON(datatable(select, from, column, Pengurus.resulset));
	}

	@AllowAccess({Group.REKANAN})
	public static void stafAhli() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM staf_ahli WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"sta_id", "sta_nama", "sta_tgllahir", "sta_pendidikan", "sta_pengalaman", "sta_keahlian", "sta_status"};
		renderJSON(datatable(select, from, column, Staf_ahli.resulset));
	}

	@AllowAccess({Group.REKANAN})
	public static void peralatan() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM peralatan WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"alt_id", "alt_jenis", "alt_jumlah", "alt_kapasitas", "alt_merktipe", "alt_kondisi", "alt_thpembuatan", "alt_lokasi","alt_kepemilikan"};
		renderJSON(datatable(select, from, column, Peralatan.resulset));
	}

	@AllowAccess({Group.REKANAN})
	public static void pengalaman() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM pengalaman WHERE rkn_id = ?", Active_user.current().rekananId);
		String[] column = new String[]{"pgn_id", "pgl_kegiatan", "pgl_lokasi", "pgl_pembtgs", "pgl_almtpembtgs", "pgl_tglkontrak", "pgl_slskontrak"};
		renderJSON(datatable(select, from, column, Pengalaman.resulset));
	}

	@AllowAccess({Group.ADM_AGENCY})
	public static void ukpbj() {
		QueryBuilder select = new QueryBuilder("SELECT *");
		QueryBuilder from = new QueryBuilder("FROM ukpbj WHERE agc_id = ?", Active_user.current().agencyId);
		String[] column = new String[]{"ukpbj_id", "nama", "alamat", "telepon", "fax", "tgl_daftar"};
		renderJSON(datatable(select, from, column, Ukpbj.resultset));
	}

	@AllowAccess({Group.ADM_AGENCY})
	public static void calonPegawaiUkpbj(Long agcId, Long id) {
		String[] column = new String[]{"peg_nip", "peg_nama", "peg_namauser", "peg_id", "peg_email"};
		QueryBuilder select = new QueryBuilder("SELECT peg_nip, peg_nama, peg_namauser, peg_id, peg_email");
		QueryBuilder from = new QueryBuilder("FROM pegawai WHERE agc_id=? AND peg_id NOT IN (SELECT peg_id FROM pegawai_ukpbj WHERE ukpbj_id=?) " +
				"AND peg_namauser IN (SELECT userid FROM usergroup WHERE idgroup='PANITIA')", agcId, id);
		renderJSON(datatable(select, from, column, Pegawai_ukpbj.resultsetPegawaiUkpbj));
	}

	@AllowAccess({Group.KUPPBJ})
	public static void paketKonsolidasiList() {
		String[] column = new String[]{"pkt_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT pkt_id, pkt_nama, pkt_tgl_buat, pkt_id_konsolidasi");
		QueryBuilder from = new QueryBuilder("FROM paket WHERE is_pkt_konsolidasi = false AND pkt_id_konsolidasi is null  AND ukpbj_id = ?", Active_user.current().ukpbjId);
		renderJSON(datatable(select, from, column, PaketResultHandler.resultsetPaketKonsolidasi));
	}

	/**Improved version by Andikyulianto@yahoo.com
	 *
	 * pakai tabel antara -> lelang_query
	 *
	 * @param kategoriId
	 * @param instansiId
	 * @param tahun
	 * @param rekanan
	 */
	public static void lelang2(Integer kategoriId, String instansiId, String tahun, String rekanan) {
		/**JANGAN LUPA pakai full_text_index pada
		 * pkt_nama, nama_instansi
		 */
		checkAuthenticity();
		Datatable datatable = Datatable.create().columnNumber("lls_id").column("pkt_nama",  "nama_instansi", "tahap_sekarang").columnNumber("pkt_hps");
		datatable.select("SELECT lls_id, kgr_id, pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_id, mtd_pemilihan, mtd_evaluasi, "
				+ " nama_instansi, lls_penawaran_ulang, kontrak_id, kontrak_nilai, is_pkt_konsolidasi, anggaran, "
				+ " tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang,  eva_versi, lls_evaluasi_ulang ", newDate());
		datatable.from("FROM lelang_query WHERE jadwal_awal_pengumuman < ?", newDate());
		if(kategoriId != null) {
			datatable.filterAnd("kgr_id=?", kategoriId);
		}
		if(tahun != null) {
			datatable.filterAnd("anggaran ilike ?", '%'+tahun+'%');
		}
		if(!StringUtils.isEmpty(instansiId)) {
			Instansi instansi = Instansi.findById(instansiId);
			if(instansi != null)
				datatable.filterAnd("nama_instansi ilike ?", '%'+instansi.nama+'%');
		}
		if (!StringUtils.isEmpty(rekanan)) {// hanya tahap lelang sudah kontrak
			datatable.filterAnd("lls_id IN (SELECT lls_id FROM peserta p , rekanan r WHERE p.rkn_id=r.rkn_id AND p.is_pemenang = 1 AND to_tsvector('english',rkn_nama) "
							+ "@@plainto_tsquery('english',?)) AND jadwal_akhir < ?",rekanan.trim().toLowerCase(), newDate());
		}
		renderJSON(datatable.render(LelangSeleksiResultHandler.resultsetLelang));
	}

	/**Improved version by Andikyulianto@yahoo.com
	 *
	 * pakai tabel antara -> ekontrak lelang_query
	 *
	 * @param kategoriId
	 * @param tahun
	 * @param instansiId
	 * @param rekanan
	 */
	public static void pl2(Integer kategoriId, String instansiId, String tahun, String rekanan) {
		checkAuthenticity();
		Datatable datatable = Datatable.create().columnNumber("lls_id").column("pkt_nama",  "nama_instansi", "tahap_sekarang").columnNumber("pkt_hps");
		datatable.select("SELECT lls_id, kgr_id, pkt_id, pkt_nama, pkt_flag, pkt_hps, mtd_pemilihan, mtd_evaluasi, "
				+ " nama_instansi, kontrak_id, kontrak_nilai, is_pkt_konsolidasi, anggaran, "
				+ " ekontrak.tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang,  eva_versi ", newDate());
		datatable.from("FROM ekontrak.lelang_query WHERE jadwal_awal_pengumuman < ?", newDate());
		if(kategoriId != null) {
			datatable.filterAnd("kgr_id=?", kategoriId);
		}
		if(!StringUtils.isEmpty(tahun)) {
			datatable.filterAnd("anggaran like ?", '%'+tahun+'%');
		}
		if(!StringUtils.isEmpty(instansiId)) {
			Instansi instansi = Instansi.findById(instansiId);
			if(instansi != null)
				datatable.filterAnd("nama_instansi ilike ?", '%'+instansi.nama+'%');
		}
		if(!StringUtils.isEmpty(rekanan)) {// hanya tahap lelang sudah kontrak
			datatable.filterAnd("lls_id IN (SELECT lls_id FROM ekontrak.peserta_nonlelang p JOIN rekanan ON p.rkn_id=rekanan.rkn_id WHERE to_tsvector('english',rkn_nama) "
					+ "@@plainto_tsquery('english',?)) AND jadwal_akhir < ?",rekanan.trim().toLowerCase(), newDate());
		}
		renderJSON(datatable.render(PlSeleksiResultHandler.resultsetPL));
	}

	// list instansi yg punya paket aktif/berjalan
	public static void instansiaktif() {
		List<Instansi> list =  new ArrayList<>();
		if(CollectionUtils.isEmpty(list)) {
			list = Instansi.find("id in (SELECT distinct s.instansi_id FROM satuan_kerja s, paket_satker ps, paket p WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id AND pkt_status = 1)").fetch();
			Cache.add("instansiAktif", list, "5mn");
		}
		renderJSON(list);
	}

	public static void tahunAnggaranaktif() {
		List<Integer> list =  new ArrayList<>();
		if(CollectionUtils.isEmpty(list)) {
			list = Query.find("SELECT DISTINCT ang_tahun FROM paket p, paket_anggaran pa, anggaran a WHERE p.pkt_id=pa.pkt_id AND pa.ang_id=a.ang_id AND pkt_status = 1 ORDER BY ang_tahun", Integer.class).fetch();
			Cache.add("tahunAnggaranAktif", list, "5mn");
		}
		renderJSON(list);
	}

	public static void NonSpk(Integer kategoriId, Integer tahun) {
		String[] column = new String[]{"lls_id","pkt_nama","ekontrak.paket_instansi(p.pkt_id)","pkt_pagu"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, pkt_nama, ekontrak.paket_instansi(p.pkt_id) as instansi, mtd_pemilihan, p.pkt_status, pkt_flag, pkt_pagu, kgr_id, ang_tahun");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.non_spk_seleksi l, ekontrak.paket p, ekontrak.paket_anggaran pa, anggaran a ");
		from.append("WHERE l.pkt_id=p.pkt_id AND p.pkt_id=pa.pkt_id AND pa.ang_id=a.ang_id AND lls_status=1 AND p.pkt_status != 0");
		if(kategoriId != null)
			from.append("AND p.kgr_id= ?", kategoriId);
		if(tahun != null )
			from.append("AND ang_tahun =?", tahun);
		renderJSON(datatable(select, from, column, Non_spk_seleksi.resultsetNonSpkPublic));
	}

	public static void Swakelola(Integer kategoriId, String tahun) {
		String[] column = new String[]{"lls_id","pkt_nama", "ekontrak.paket_instansi_swakelola(p.swk_id)", "pkt_pagu"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, pkt_nama, p.pkt_status, pkt_flag, pkt_pagu, ekontrak.paket_instansi_swakelola(p.swk_id) as instansi , ang_tahun");
		QueryBuilder from = new QueryBuilder("FROM ekontrak.swakelola_seleksi l, ekontrak.paket_swakelola p, ekontrak.paket_anggaran_swakelola pa, ekontrak.anggaran_swakelola a");
		from.append("WHERE l.swk_id=p.swk_id AND p.swk_id=pa.swk_id AND pa.ang_id=a.ang_id AND lls_status=1 AND p.pkt_status != 0");
		renderJSON(datatable(select, from, column, Swakelola_seleksi.resultsetSwakelolaPublic));
	}

	/**
	 * get JSON for table Panel
	 */
	public static void panel() {
		String[] column = new String[] {"pnl_id", "nama_panel", "keterangan", "jumlah_vendor", "pnl_size"};
		QueryBuilder select = new QueryBuilder("SELECT pnl_id, nama_panel, keterangan, " +
				"(select count(1) from devpartner.panel_rekanan pr where pr.pnl_id = p.pnl_id) as jumlah_vendor, pnl_size");
		QueryBuilder from = new QueryBuilder("FROM devpartner.panel p WHERE pnl_owner_id = ?", Active_user.current().pegawaiId);
		renderJSON(datatable(select, from, column, Panel.resultset));
	}

	@AllowAccess({Group.PPK})
	public static void paketPpkDevP(Long ppkId, Integer status) {
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "paket_satker(p.pkt_id)", "pkt_flag", "lls_id", "lls_penawaran_ulang"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, paket_satker(p.pkt_id) as stk_nama, " +
				"lls_penawaran_ulang, ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi, "
				+"pkt_pagu, pkt_status, lls_id, mtd_id, devpartner.tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status", newDate());
		QueryBuilder from = new QueryBuilder("FROM devpartner.paket_dp p JOIN (SELECT DISTINCT(ppk_id), pkt_id FROM devpartner.paket_anggaran_dp) ppk ON ppk.pkt_id=p.pkt_id " +
				"LEFT JOIN devpartner.lelang_dp lls ON lls.pkt_id=p.pkt_id " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) FROM devpartner.lelang_dp WHERE pkt_id=p.pkt_id) WHERE ppk.ppk_id=? ", ppkId);
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
		renderJSON(datatable(select, from, column, PaketDpResultHandler.resultsetPaketList));
	}

	@AllowAccess({Group.KUPPBJ})
	public static void paketKuppbjDevP(Long ukpbjId, Integer status) {
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "stk_nama", "lls_id", "pn.pnt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, pn.pnt_nama, " +
				"(SELECT ARRAY_TO_STRING(ARRAY_AGG(distinct stk_nama), ', ') FROM satuan_kerja s, " +
				"devpartner.paket_satker_dp ps WHERE ps.stk_id=s.stk_id AND ps.pkt_id=p.pkt_id) as stk_nama, " +
				"pkt_pagu, pkt_status, lls_id, mtd_id, devpartner.tahap_now(lls_id, ?) AS tahaps, " +
				"lls_versi_lelang, lls_status", newDate());
		QueryBuilder from = new QueryBuilder("FROM devpartner.paket_dp p " +
				"LEFT JOIN satuan_kerja s ON p.stk_id=s.stk_id " +
				"LEFT JOIN devpartner.lelang_dp lls ON lls.pkt_id=p.pkt_id " +
				"LEFT JOIN panitia pn ON pn.pnt_id = p.pnt_id  " +
				"AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM devpartner.lelang_dp WHERE pkt_id=p.pkt_id)");
		from.append("WHERE p.ukpbj_id=?", ukpbjId);
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
//		from.append(" AND pkt_flag =?", 3);
		renderJSON(datatable(select, from, column, PaketDpResultHandler.resultsetPaketKuppbj));
	}

	/**
	 * Daftar paket berdasarkan panitia
	 *
	 * @param status kode status lelang
	 */
	@AllowAccess({Group.PANITIA})
	public static void paketPanitiaDevP(Long panitiaId, Integer status) {
		String[] column = new String[]{"p.pkt_id", "pkt_nama", "pkt_status", "pkt_tgl_buat", "paket_satker(p.pkt_id)", "lls_id", "lls_penawaran_ulang"};
		QueryBuilder select = new QueryBuilder("SELECT p.kgr_id, p.pkt_id, pkt_nama, pkt_tgl_buat, paket_satker(p.pkt_id) as stk_nama, pkt_pagu, " +
				"pkt_status, pkt_flag, lls_id, mtd_id, tahap_now(lls_id, ?) AS tahaps, lls_versi_lelang, lls_status, lls_penawaran_ulang, " +
				"ukpbj_id, p.pkt_id_konsolidasi, p.is_pkt_konsolidasi", newDate());
		QueryBuilder from = new QueryBuilder("FROM devpartner.paket_dp p LEFT JOIN devpartner.lelang_dp lls ON lls.pkt_id=p.pkt_id AND lls.lls_versi_lelang=(SELECT MAX(lls_versi_lelang) " +
				"FROM devpartner.lelang_dp WHERE pkt_id=p.pkt_id)");
		Active_user active_user = Active_user.current();
		if (panitiaId != null) {
			from.append("WHERE p.pnt_id=? ", panitiaId);
		} else {
			from.append("WHERE p.pnt_id IN (SELECT pnt_id FROM anggota_panitia WHERE peg_id=?) ", active_user.pegawaiId);
		}
		if (status != null) {
			from.append("AND pkt_status=?", status);
		}
		renderJSON(datatable(select, from, column, PaketDpResultHandler.resultsetPaketList));
	}

	@AllowAccess({Group.PPK})
	public static void rencanaPaketDp(Long satkerId, Integer tahun, Integer metodePemilihan) {
		if(satkerId != null && tahun != null)
		{
			String[] column = new String[]{"id", "nama", "paket_anggaran -> 0 ->> 'sumber_dana'", "metode_pengadaan", "pagu","tahun"};
			QueryBuilder select = new QueryBuilder("SELECT id, nama, pagu, tahun, paket_anggaran -> 0 ->> 'sumber_dana' as sumberdana, metode_pengadaan, (select pkt_id from devpartner.paket_dp where rup_id=p.id limit 1) as paket ");
			QueryBuilder from = new QueryBuilder("FROM public.paket_sirup p, satuan_kerja s WHERE p.rup_stk_id=s.rup_stk_id AND p.tahun= ?",tahun);
			if(satkerId != null) {
				from.append(" AND s.stk_id = ?", satkerId);
			}
			if (metodePemilihan != null && metodePemilihan != 0) {
				from.append(" AND p.metode_pengadaan=?", metodePemilihan);
			}
			renderJSON(datatable(select, from, column, PaketSirup.resultset));
		}
		else
			renderJSON(ssp_empty());
	}

	@AllowAccess({Group.PANITIA})
	public static void lelangDpPanitia(Long panitiaId) {
		String[] column = new String[]{"lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, lls_versi_lelang, mtd_id, p.kgr_id, pkt_nama, " +
				"(select count(psr_id) from devpartner.peserta_dp where lls_id=l.lls_id) as peserta, devpartner.tahap_now(lls_id, ?) AS tahaps", newDate());
		QueryBuilder from = new QueryBuilder("FROM devpartner.lelang_dp l, devpartner.paket_dp p WHERE l.pkt_id=p.pkt_id AND lls_status=1");
		if(panitiaId != null) {
			from.append("AND p.pnt_id=?", panitiaId);
		} else {
			from.append("AND p.pnt_id IN (SELECT pnt_id FROM anggota_panitia WHERE peg_id=?)", Active_user.current().pegawaiId);
		}
		renderJSON(datatable(select, from, column, LelangDpResultHandler.resultsetLelangPanitia));
	}

	@AllowAccess({Group.TRAINER, Group.REKANAN})
	public static void dpBaru() {
		Long rekananId = Active_user.current().rekananId;
		final Date now = BasicCtr.newDate();
		List<TahapDp> tahapPengumumanList = TahapDp.listPengumumanLelang();
		String paramKey = "";
		for (TahapDp tahap: tahapPengumumanList) {
			paramKey += ",?";
		}
		if (!paramKey.isEmpty()) {
			paramKey = paramKey.substring(1);
		}
		String[] column = new String[]{"lls_id", "pkt_nama", "pkt_hps"};
		QueryBuilder select = new QueryBuilder("SELECT lls_id, mtd_id, pkt_nama, pkt_hps, pkt_flag, lls_versi_lelang");
		QueryBuilder from = new QueryBuilder()
				.append("FROM devpartner.lelang_dp l LEFT JOIN devpartner.paket_dp p ON p.pkt_id=l.pkt_id ")
				.append("WHERE l.lls_status=1 ")
				.append("AND l.lls_id IN ( ")
				.append("	SELECT distinct j.lls_id FROM devpartner.jadwal_dp j, devpartner.aktivitas_dp a, devpartner.lelang_dp l ")
				.append("	WHERE j.lls_id=l.lls_id and j.akt_id=a.akt_id ")
				.append("	AND akt_jenis IN (").append(paramKey).append(") AND dtj_tglawal <= ? AND dtj_tglakhir >= ?")
				.append(")");
		from.params().addAll(tahapPengumumanList);
		from.params().add(now);
		from.params().add(now);

		if(rekananId != null)
			from.append(" AND l.lls_id NOT IN (SELECT lls_id FROM devpartner.peserta_dp WHERE rkn_id=?)", rekananId);
		renderJSON(datatable(select, from, column, LelangDpResultHandler.resultsetLelangBaru));
	}

	/**
	 * Datfar lelang by status
	 */
	@AllowAccess(Group.REKANAN)
	public static void dataTableStatusLelangDp() {
		Long rekananId = Active_user.current().rekananId;
		String[] column = new String[]{"l.lls_id", "pkt_nama"};
		QueryBuilder select = new QueryBuilder("SELECT l.lls_id,p.kgr_id, lls_versi_lelang, pkt_nama, devpartner.tahap_now(l.lls_id, ?) AS tahaps ", newDate());
		QueryBuilder from = new QueryBuilder("FROM devpartner.lelang_dp l, devpartner.paket_dp p, devpartner.peserta_dp psr WHERE l.lls_status = 1 AND p.pkt_id=l.pkt_id AND l.lls_id=psr.lls_id AND psr.rkn_id=?", rekananId);
		renderJSON(datatable(select, from, column, LelangDpResultHandler.resultsetLelangRekanan));
	}
}
