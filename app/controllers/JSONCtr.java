package controllers;

import com.google.gson.Gson;
import models.LOVItem;
import models.agency.Satuan_kerja;
import models.common.Instansi;
import models.jcommon.cache.AjaxCache;
import models.jcommon.cache.AjaxCacheProcessor;
import org.apache.commons.collections4.CollectionUtils;
import play.cache.Cache;
import play.mvc.Controller;

import java.util.Date;
import java.util.List;

/**
 * Kelas yang mengatur request JSON pada aplikasi (pengecualian untuk request
 * DataTable). Created by IntelliJ IDEA. Date: 29/08/12 Time: 13:33
 *
 * @author I Wayan Wiprayoga Wisesa
 */
public class JSONCtr extends Controller {

	private static final Gson gson = new Gson();

	public static void daftar_propinsi()
	{
		List<LOVItem> list = (List<LOVItem>)Cache.get("propinsi");
		if(CollectionUtils.isEmpty(list)) {
			list = LOVItem.getPropinsi();
			Cache.add("propinsi", list, "1h");
		}
		renderJSON(list);
	}

	public static void daftar_bentuk_usaha()
	{
		renderJSON(LOVItem.getBentukUsaha());
	}

	public static void daftar_negara()
	{
		List<LOVItem> list = (List<LOVItem>)Cache.get("negara");
		if(CollectionUtils.isEmpty(list)) {
			list = LOVItem.getNegara();
			Cache.add("negara", list, "1h");
		}
		renderJSON(list);
	}
	
	public static void daftar_kabupaten(Long id)
	{
		List<LOVItem> list = (List<LOVItem>)Cache.get("kabupaten-prp-"+id);
		if(CollectionUtils.isEmpty(list)) {
			list = LOVItem.getKabupaten(id);
			Cache.add("kabupaten-prp-"+id, list, "1h");
		}
		renderJSON(list);
	}

	
    public static void daftar_instansi(){
    	AjaxCacheProcessor processor=() -> {
            List<LOVItem> list = LOVItem.getInstansi();
            return gson.toJson(list);
    	};
    	AjaxCache.getInstance().execute(processor ,request, response, Instansi.class.getName());
    }

    public static void daftar_satker(String id, Integer tahun) {
    	AjaxCacheProcessor processor=() -> {
    		List<LOVItem> list = LOVItem.getSatkerPusat(id, tahun);
			return gson.toJson(list);
    	};

    	AjaxCache.getInstance().execute(processor ,request, response, Satuan_kerja.class.getName());
    }

	public String daftar_satker_nocache(String id, Integer tahun) {

			List<LOVItem> list = LOVItem.getSatkerPusat(id, tahun);
			return gson.toJson(list);

	}
    
    public static void daftar_satkerAgency(Long id) {
    	
    	AjaxCacheProcessor processor=() -> {
    		  List<LOVItem> list = LOVItem.getSatkerAgency(id);        
  		  return new Gson().toJson(list);
    	};
    	AjaxCache.getInstance().execute(processor ,request, response,Satuan_kerja.class.getName());
    }

	/**
	 * Fungsi {@code getJSONSemuaInstansiByJenisInstansi} digunakan untuk
	 * mendapatkan JSON daftar instansi dengan jenis tertentu.
	 *
	 * @param jenis jenis instansi
	 */
	public static void getJSONSemuaInstansiByJenisInstansi(String jenis) {
		if (jenis == null) { // jenis agency tidak didefinisikan
			renderJSON(""); // render JSON Kosong
		}
		Instansi.JenisInstansi jni = Instansi.JenisInstansi.valueOf(jenis);
		List<Instansi> daftar_instansi = Instansi.findSemuaByJenis(jni);
		if (daftar_instansi == null || daftar_instansi.isEmpty()) { // tidak ada daftar instansi yang sesuai dengan jenis ini
			renderJSON(""); // render JSON Kosong
		}
		renderJSON(daftar_instansi, Instansi.serializer); 
	}

	/**
	 * Fungsi {@code getServerTime} digunakan untuk mendapatkan informasi jam server
	 * format waktu dalam milisecond (milis).
	 * Modified at 17-10-2012, yang dirender adalah objek date.
	 */
	public static void getServerTime() {
		Date now = controllers.BasicCtr.newDate();
		renderJSON(now.getTime());
	}
	
}
