package controllers.security;

import org.bouncycastle.util.encoders.Base64;
import play.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class KeyUtilities {
	
//	private static final Logger logger = Logger.getLogger(KeyUtilities.class);

	public static byte[] getMD5Hash(byte[] input) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {			
			e.printStackTrace();
		}
		return messageDigest.digest(input);
	}
	
	public static byte[] encryptKey(byte[] bytePlain){
		int length = bytePlain.length + (16-(bytePlain.length%16));
		byte[] plain = new byte[length];
        for(int i=0;i<length;i++){
        	if(i<bytePlain.length)
        		plain[i]=bytePlain[i];
        	else
        		plain[i]='|';
        }

        SecretKeySpec skeySpec = new SecretKeySpec(getKey(), 0, 32, "AES");
 		IvParameterSpec ivSpec = new IvParameterSpec(getIv());
 		Cipher c;
 		
 		byte[] encrypted = new byte[length];
 		
		try {	
		  //c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			c = Cipher.getInstance("AES/CBC/NoPadding");
			c.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
			encrypted = c.doFinal(plain);
			
		} catch (NoSuchAlgorithmException e) {
			Logger.error(e, e.getMessage());
		} catch (NoSuchPaddingException e) {
			Logger.error(e, e.getMessage());
		} catch (InvalidKeyException e) {
			Logger.error(e, e.getMessage());
		} catch (InvalidAlgorithmParameterException e) {
			Logger.error(e, e.getMessage());
		} catch (IllegalBlockSizeException e) {
			Logger.error(e, e.getMessage());
		} catch (BadPaddingException e) {
			Logger.error(e, e.getMessage());
		}
		return encrypted;
	}
	
	public static byte[] decryptKey(String cipher){
		byte[] key = Base64.decode(cipher);
		return decryptKey(key); 
	}
	
	public static byte[] decryptKey(byte[] cipher){
       		
		SecretKeySpec skeySpec = new SecretKeySpec(getKey(), 0, 32, "AES");
 		IvParameterSpec ivSpec = new IvParameterSpec(getIv());
 		Cipher c;
 		
 		byte[] deCipher = new byte[cipher.length];
 		
			try {
//				c = Cipher.getInstance("AES/CBC/PKCS5Padding");
				c = Cipher.getInstance("AES/CBC/NoPadding");
				c.init(Cipher.DECRYPT_MODE, skeySpec, ivSpec);
				deCipher = c.doFinal(cipher);
			} catch (NoSuchAlgorithmException e) {
				Logger.error(e, e.getMessage());
			} catch (NoSuchPaddingException e) {
				Logger.error(e, e.getMessage());
			} catch (InvalidKeyException e) {
				Logger.error(e, e.getMessage());
			} catch (InvalidAlgorithmParameterException e) {
				Logger.error(e, e.getMessage());
			} catch (IllegalBlockSizeException e) {
				Logger.error(e, e.getMessage());
			} catch (BadPaddingException e) {
				Logger.error(e, e.getMessage());
			}

			int padLength = 0;
	        for(int i=deCipher.length-1;;i--){
	        	if(deCipher[i]=='|')
	        		padLength++;
	        	else
	        		break;
	        }
	        byte[] decrypted = new byte[deCipher.length-padLength];
		System.arraycopy(deCipher, 0, decrypted, 0, decrypted.length);
	        
		return decrypted;
	}
	
	private static byte[] getKey(){
		byte[] key1 = getMD5Hash("S3meN74RA".getBytes());
		byte[] key2 = getMD5Hash(key1);
		byte[] key3 = new byte[key1.length+key2.length];
		for(int i=0;i<key1.length;i++){
			key3[i]=key1[i];
			key3[i+key1.length]=key2[i];
		}
		return key3;
	}
	
	private static byte[] getIv(){
		return new byte[] {12,16,19,5,32,14,1,19,9,15,14,1,12,124,124,124};
	}
}
