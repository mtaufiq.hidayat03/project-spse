package controllers.security;

import models.common.MetodeDokumen;
import models.jcommon.util.RSAKeyPair;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Lelang_key;
import models.lelang.Lelang_seleksi;
import org.bouncycastle.util.encoders.Base64;
import play.Logger;

import java.math.BigInteger;
import java.security.SecureRandom;


public class KeyGeneratorApendoV1{
	
//	private static final Logger logger = Logger.getLogger(KeyGeneratorApendoV1.class);
	
	public static void generateKey(Lelang_seleksi lelang){
		boolean failed = true;
		int count =0;
		MetodeDokumen mtd_dokumen = lelang.getMetode().dokumen;
		if(mtd_dokumen.isSatuFile())
			while(failed){
				failed = generateKeyDok(lelang, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA);
				if(count<10)
					count++;
				else{
                    Logger.error("GENERATE KEY FAILED, OVER MAX LOOP");
					break;
				}
			}
		else {
			while(failed){
				failed = generateKeyDok(lelang, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
				if(count<10)
					count++;
				else{
                    Logger.error("GENERATE KEY FAILED, OVER MAX LOOP");
					break;
				}
			}
			failed = true;
			while(failed){
				failed = generateKeyDok(lelang, JenisDokPenawaran.PENAWARAN_HARGA);
				if(count<10)
					count++;
				else{
                    Logger.error("GENERATE KEY FAILED, OVER MAX LOOP");
					break;
				}
			}
		}
	}
	
	private static boolean generateKeyDok(Lelang_seleksi lelang, JenisDokPenawaran dok_jenis){
		SecureRandom randomly = new SecureRandom();
        RSAKeyPair generateKunci = new RSAKeyPair(randomly);
        BigInteger nilaiP = generateKunci.getP();
        BigInteger nilaiQ = generateKunci.getQ();
        BigInteger nilaiN = generateKunci.getN();
        BigInteger nilaiE = generateKunci.getE();
        BigInteger nilaiD = generateKunci.getD();
    
        byte concatPublic[] = concatPublicKey(nilaiN, nilaiE);
        byte concatPrivate[] = concatPrivateKey(concatPublic, nilaiD, nilaiP, nilaiQ);
        Lelang_key lelangKey = Lelang_key.find("lls_id=? and dok_jenis=?", lelang.lls_id, dok_jenis).first();
        if(lelangKey != null){
        	lelangKey.delete();
        }
        lelangKey = new Lelang_key();
		lelangKey.lls_id = lelang.lls_id;
		lelangKey.dok_jenis = dok_jenis;
		lelangKey.modulus = new String(Base64.encode(nilaiN.toByteArray()));
		lelangKey.public_key = new String(Base64.encode(concatPublic));
		byte[] encryptedKey = KeyUtilities.encryptKey(concatPrivate);
		boolean test = new String(Base64.encode(concatPrivate)).equals(new String(Base64.encode(KeyUtilities.decryptKey(encryptedKey))));
		if(test){
			lelangKey.private_key = new String(Base64.encode(encryptedKey));
			lelangKey.save();
			return false;
		}
		return true;
	}
	
	private static byte[] concatPublicKey(BigInteger nilaiN, BigInteger nilaiE){
        byte[] tempByte = new byte[10];
        tempByte[0]=35;tempByte[1]=12;tempByte[2]=5;tempByte[3]=13;tempByte[4]=19;
        tempByte[5]=1;tempByte[6]=14;tempByte[7]=5;tempByte[8]=7;tempByte[9]=35;
        //1. format publik (client) = n del e;
        byte[] ModPublic = nilaiN.toByteArray();
        int pjgByteModPublic = ModPublic.length - 1;
//        int pjgBitModPublic = nilaiN.bitLength();
        
        byte[] ExpPublicE = nilaiE.toByteArray();
        int pjgByteExpPublicE = ExpPublicE.length ;
//        int pjgBitExpPublicE = nilaiE.bitLength();
        
        int pjgAll = (tempByte.length+pjgByteModPublic+pjgByteExpPublicE);
        
        byte[] allPublic = new byte[pjgAll];
        int index = 0;
        
        for(int k=1;k<(pjgByteModPublic+1);k++){
            allPublic[index]=ModPublic[k];
            index=index+1;
        }
         for(int k=0;k<tempByte.length;k++){
            allPublic[index]=tempByte[k];
            index=index+1;
        }   
         for(int k=0;k<pjgByteExpPublicE;k++){
            allPublic[index]=ExpPublicE[k];
            index=index+1;
        }
         return allPublic;
	}
	
	private static byte[] concatPrivateKey(byte allPublic[], BigInteger nilaiD, BigInteger nilaiP, BigInteger nilaiQ){
		byte[] tempByte = new byte[10];
        tempByte[0]=35;tempByte[1]=12;tempByte[2]=5;tempByte[3]=13;tempByte[4]=19;
        tempByte[5]=1;tempByte[6]=14;tempByte[7]=5;tempByte[8]=7;tempByte[9]=35;
        //2. format private (server) = n del e del d del p del q
        byte[] ExpPrivateD = nilaiD.toByteArray();
        int pjgByteExpPrivateD = ExpPrivateD.length;
        
        byte[] pPrivate = nilaiP.toByteArray();
        int pjgBytepPrivate = pPrivate.length-1;
        
        byte[] qPrivate = nilaiQ.toByteArray();
        int pjgByteqPrivate = qPrivate.length-1;
        

        byte[] ExpPrivateD2 = new byte[128];
        
        if(pjgByteExpPrivateD < 128){
            int reg = 128-pjgByteExpPrivateD;
            byte[] kongja = new byte[reg];
            int index = 0;
            for(int m=0;m<reg;m++){
                kongja[m]=0;
                ExpPrivateD2[index]=kongja[m];
                index=index+1;
            }
            for(int n=0;n<pjgByteExpPrivateD;n++){
                ExpPrivateD2[index]=ExpPrivateD[n];
                index=index+1;
            }
            index=0;
        }
        else if(pjgByteExpPrivateD > 128){
            int reg = pjgByteExpPrivateD-128;
            int index = 0;
            for(int n=reg;n<pjgByteExpPrivateD;n++){
                ExpPrivateD2[index]=ExpPrivateD[n];
                index=index+1;
            }
            index=0;
        }
        else{
            int index = 0;
            for(int n=0;n<pjgByteExpPrivateD;n++){
                ExpPrivateD2[index]=ExpPrivateD[n];
                index=index+1;
            }
            index = 0;
        }
        int pjgByteExpPrivateD2 = ExpPrivateD2.length;                
        
        int pjgAll = allPublic.length;
        int pjgAll2 = (pjgAll+(3*tempByte.length)+pjgByteExpPrivateD2+pjgBytepPrivate+pjgByteqPrivate);
        
        byte[] allPrivate = new byte[pjgAll2];
        int index = 0;
        for(int k=0;k<pjgAll;k++){
             allPrivate[index]=allPublic[k];
            index=index+1;
        }
        for(int k=0;k<tempByte.length;k++){
            allPrivate[index]=tempByte[k];
            index=index+1;
        }  
        for(int k=0;k<pjgByteExpPrivateD2;k++){
            allPrivate[index]=ExpPrivateD2[k];
            index=index+1;
        }   
        for(int k=0;k<tempByte.length;k++){
            allPrivate[index]=tempByte[k];
            index=index+1;
        }  
        // untuk nilai primaP ... karena ditambahkan 1 byte
        // maka nilai byte sebenarnya diambil dari index ke 1 bukan ke 0
        for(int k=1;k<(pjgBytepPrivate+1);k++){
            allPrivate[index]=pPrivate[k];
            index=index+1;
        }   
        for(int k=0;k<tempByte.length;k++){
            allPrivate[index]=tempByte[k];
            index=index+1;
        }  
        // untuk nilai primaQ ... karena ditambahkan 1 byte
        // maka nilai byte sebenarnya diambil dari index ke 1 bukan ke 0
        for(int k=1;k<(pjgByteqPrivate+1);k++){
            allPrivate[index]=qPrivate[k];
            index=index+1;
        }
        return allPrivate;
	}
	
}
