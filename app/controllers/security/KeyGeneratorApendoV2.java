package controllers.security;

import models.common.ConfigurationDao;
import models.common.MetodeDokumen;
import models.jcommon.util.RSAKeyPair;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Lelang_key;
import models.lelang.Lelang_seleksi;
import org.bouncycastle.util.encoders.Base64;
import play.Logger;

import java.math.BigInteger;
import java.security.SecureRandom;


public class KeyGeneratorApendoV2{
	
//	private static final Logger logger = Logger.getLogger(KeyGeneratorApendoV2.class);
	
	public static void generateKey(Lelang_seleksi lelang){
		boolean failed = true;
		int count =0;
		MetodeDokumen mtd_dokumen = lelang.getMetode().dokumen;
		if(mtd_dokumen.isSatuFile())
			while(failed){
				failed = generateKeyDok(lelang, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA);
				if(count<10)
					count++;
				else{
					Logger.error("GENERATE KEY FAILED, OVER MAX LOOP");
					break;
				}
			}
		else {
			while(failed){
				failed = generateKeyDok(lelang, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
				if(count<10)
					count++;
				else{
                    Logger.error("GENERATE KEY FAILED, OVER MAX LOOP");
					break;
				}
			}
			failed = true;
			count = 0;
			while(failed){
				failed = generateKeyDok(lelang, JenisDokPenawaran.PENAWARAN_HARGA);
				if(count<10)
					count++;
				else{
                    Logger.error("GENERATE KEY FAILED, OVER MAX LOOP");
					break;
				}
			}
		}
	}	
	
	private static boolean generateKeyDok(Lelang_seleksi lelang, JenisDokPenawaran dok_jenis){
		SecureRandom randomly = new SecureRandom();
        RSAKeyPair generateKunci = new RSAKeyPair(randomly);
        BigInteger nilaiP = generateKunci.getP();
        BigInteger nilaiQ = generateKunci.getQ();
        BigInteger nilaiN = generateKunci.getN();
        BigInteger nilaiE = generateKunci.getE();
        BigInteger nilaiD = generateKunci.getD();
//        long lelangId = lelangId;
        byte[] allPublic = concatPublicKey(nilaiN, nilaiE);
        byte[] allPrivate = concatPrivateKey(allPublic, nilaiD, nilaiP, nilaiQ);
        byte[] concatPublic = concatInfo(allPublic, lelang.lls_id, lelang.getNamaPaket(), dok_jenis, "Informasi Kunci Publik");
        byte[] concatPrivate = concatInfo(allPrivate, lelang.lls_id,lelang.getNamaPaket(), dok_jenis, "Informasi Kunci Private");
        Lelang_key lelangKey = Lelang_key.find("lls_id=? and dok_jenis=?", lelang.lls_id, dok_jenis).first();
        if(lelangKey != null){
        	lelangKey.delete();
        }
        lelangKey = new Lelang_key();
		lelangKey.lls_id = lelang.lls_id;
		lelangKey.dok_jenis = dok_jenis;
		if(!ConfigurationDao.isProduction()){
			lelangKey.modulus = "AJZ38lazAI3agYmOjTXroHEU0dbzHfchL6c8/k6uDS9gdqZ9NrwD0uUJKn5BgNiD0HNneCkPv4KUUbGgx3cUhxiRGr8F5zlxzDTKisWEYj1+4NYPc7UqMhQqMrwb6gKSqJVfY/ylxUZCbnTGw5vkCHBfPy0MqC9WDaSrekV1Tqir";
			lelangKey.public_key = "SW5mb3JtYXNpIEt1bmNpIFB1YmxpawoKSUQgTGVsYW5nIDogMDAwMDAwCk5hbWEgUGFrZXQgOiBQZWxhdGloYW4gRnVuZ3Npb25hbCBQZW5nYWRhYW4gU2VjYXJhIEVsZWt0cm9uaWsKSmVuaXMgRG9rdW1lbiA6IERva3VtZW4gUGVuYXdhcmFuIEFkbWluaXN0cmFzaSwgVGVrbmlzIGRhbiBIYXJnYQoKUGFzdGlrYW4gSW5mb3JtYXNpIEt1bmNpIFB1YmxpayBkaWF0YXMgc2VzdWFpIGRlbmdhbiB0dWp1YW4gcGVuZ2lyaW1hbgpQZW5nZ3VuYSBiZXJ0YW5nZ3VuZyBqYXdhYiBhdGFzIGtlYW1hbmFuL2tlcnVzYWthbiBkYXRhIHlhbmcgZGlzZWJhYmthbgprZWxhbGFpYW4gcGVuZ2d1bmFhbiBhcGxpa2FzaSBBUEVORE8KCiMMBQ0TAQ4FByOWd/JWswCN2oGJjo0166BxFNHW8x33IS+nPP5Org0vYHamfTa8A9LlCSp+QYDYg9BzZ3gpD7+ClFGxoMd3FIcYkRq/Bec5ccw0yorFhGI9fuDWD3O1KjIUKjK8G+oCkqiVX2P8pcVGQm50xsOb5AhwXz8tDKgvVg2kq3pFdU6oqyMMBQ0TAQ4FByMBAAEjDAUNEwEOBQcjhwj9N7i/9DAEU7Bw1GBgTQ==";
			lelangKey.private_key = "ed5eBPpJfjTSszqRd2IEzex7lb/tSo5j48/YthHKsN6dxRtbQIC4Mdh+hOrh4bWLKgFQZFlXxZ7c6AQbXCxLl3ipnnP9l2QOIUmdgdwVMKIV3JCGJSBgS+3/RAeZjx0sNIseMG8rDSuqAyZsTtfb5Rr+MNDKRJ9ZmV8qQbiZtUwaw98p01yt/qAl/3BgJETeGSvls9Glxy0NbnWRgSzMzuruoTpk88cGyPjpioyPR2SQoafU5XH3z++MGT+RxAObvvguHXmkEq8JTS0gC0OJ6/JArH6WB38pvKgW8Ga0hfK4SEmNB9cGYzQRx2sFNNlchAym9Wrp2yGXhPDC+++lRoAIijc/lm2rZYEQOMPjTRuT2w1eONiFImF0l2RyGOZ9omDx1ENWvrMlfbWyL9jGvzLNsQYIVSW5BoqVg9L70dPQPsCMprdyK1keyVwkrV9WjT2Etv706t3Wh4NEydSwBG5mVzBBD0zcKUVwXKWzNqabBxu1dcsYoowWvj5Rj0KAIuS4WxzDXhmq+ADdJNawbvWvH2m7oBohvJPvc71E6oXQvCdJNtj89f5VZgkfeUsGECMXZ0ItS8v6PJeV7UZWiyvc6RI71glzkRAolWC3z8ZjLyYeRZDNdRJqSwtszVBbFMFFtkc4fZTm9C8YRxH6m7+xTlDRGUTiEwHIRBcZMQ3LLl5Wsl3CgeSSp1MkLycdyvLV2oO0E6NWMkxWUf5jGfd2js6PYjtXPi0bnUE2Cyvbm1+5k6ikGjqg8yIQOai01FW0NG4d1mlE5t3xsIsF3RM0xdH3jMggpKoqJiZc/uI6sKsZ3Rg2npnXNScpbjb3MavRU1st00mwxky+6/s9/6JOizMyR4+raOBoa3RTZACMhzFpmHsHZALTEcMF7z8PyBrpJMAF01UFj9IHjVttNHr252TyqPkXga1wBpeEImS5RT4UfT0FKoItTbWfYW7Dsr/me+TQ2HWqOlvHt3/ecCdkeJDYuCXvNxLVeluxCK+1bWKfzHbIdKvzjfICqiK5JgbG/fMs/Pn7dhcdDLgTk+6sP15sgDdBZRhTQ6G2KbwF70cAPXFOp6HN/+zM6OurLr+IIzTh8kQLBHVUOwDUtw==";
			lelangKey.save();
			return false;
		} else {
			lelangKey.modulus = new String(Base64.encode(nilaiN.toByteArray()));
			lelangKey.public_key = new String(Base64.encode(concatPublic));
			byte[] encryptedKey = KeyUtilities.encryptKey(concatPrivate);
			boolean test = new String(Base64.encode(concatPrivate)).equals(new String(Base64.encode(KeyUtilities.decryptKey(encryptedKey))));
			if(test){
				lelangKey.private_key = new String(Base64.encode(encryptedKey));
				lelangKey.save();
				return false;
			}
 		}
		return true;
	}
	
	private static byte[] concatPublicKey(BigInteger nilaiN, BigInteger nilaiE){
        byte[] tempByte = new byte[10];
        tempByte[0]=35;tempByte[1]=12;tempByte[2]=5;tempByte[3]=13;tempByte[4]=19;
        tempByte[5]=1;tempByte[6]=14;tempByte[7]=5;tempByte[8]=7;tempByte[9]=35;
        //1. format publik (client) = n del e;
        byte[] ModPublic = nilaiN.toByteArray();
        int pjgByteModPublic = ModPublic.length - 1;
//        int pjgBitModPublic = nilaiN.bitLength();
        
        byte[] ExpPublicE = nilaiE.toByteArray();
        int pjgByteExpPublicE = ExpPublicE.length ;
//        int pjgBitExpPublicE = nilaiE.bitLength();
        
        int pjgAll = (tempByte.length+pjgByteModPublic+pjgByteExpPublicE);
        
        byte[] allPublic = new byte[pjgAll];
        int index = 0;
        
        for(int k=1;k<(pjgByteModPublic+1);k++){
            allPublic[index]=ModPublic[k];
            index=index+1;
        }
         for(int k=0;k<tempByte.length;k++){
            allPublic[index]=tempByte[k];
            index=index+1;
        }   
         for(int k=0;k<pjgByteExpPublicE;k++){
            allPublic[index]=ExpPublicE[k];
            index=index+1;
        }

         return allPublic;
	}
	
	private static byte[] concatPrivateKey(byte allPublic[], BigInteger nilaiD, BigInteger nilaiP, BigInteger nilaiQ){
		byte[] tempByte = new byte[10];
        tempByte[0]=35;tempByte[1]=12;tempByte[2]=5;tempByte[3]=13;tempByte[4]=19;
        tempByte[5]=1;tempByte[6]=14;tempByte[7]=5;tempByte[8]=7;tempByte[9]=35;
        //2. format private (server) = n del e del d del p del q
        byte[] ExpPrivateD = nilaiD.toByteArray();
        int pjgByteExpPrivateD = ExpPrivateD.length;
        
        byte[] pPrivate = nilaiP.toByteArray();
        int pjgBytepPrivate = pPrivate.length-1;
        
        byte[] qPrivate = nilaiQ.toByteArray();
        int pjgByteqPrivate = qPrivate.length-1;
        

        byte[] ExpPrivateD2 = new byte[128];
        
        if(pjgByteExpPrivateD < 128){
            int reg = 128-pjgByteExpPrivateD;
            byte[] kongja = new byte[reg];
            int index = 0;
            for(int m=0;m<reg;m++){
                kongja[m]=0;
                ExpPrivateD2[index]=kongja[m];
                index=index+1;
            }
            for(int n=0;n<pjgByteExpPrivateD;n++){
                ExpPrivateD2[index]=ExpPrivateD[n];
                index=index+1;
            }
            index=0;
        }
        else if(pjgByteExpPrivateD > 128){
            int reg = pjgByteExpPrivateD-128;
            int index = 0;
            for(int n=reg;n<pjgByteExpPrivateD;n++){
                ExpPrivateD2[index]=ExpPrivateD[n];
                index=index+1;
            }
            index=0;
        }
        else{
            int index = 0;
            for(int n=0;n<pjgByteExpPrivateD;n++){
                ExpPrivateD2[index]=ExpPrivateD[n];
                index=index+1;
            }
            index = 0;
        }
        int pjgByteExpPrivateD2 = ExpPrivateD2.length;                
        
        int pjgAll = allPublic.length;
        int pjgAll2 = (pjgAll+(3*tempByte.length)+pjgByteExpPrivateD2+pjgBytepPrivate+pjgByteqPrivate);
        
        byte[] allPrivate = new byte[pjgAll2];
        int index = 0;
        for(int k=0;k<pjgAll;k++){
             allPrivate[index]=allPublic[k];
            index=index+1;
        }
        for(int k=0;k<tempByte.length;k++){
            allPrivate[index]=tempByte[k];
            index=index+1;
        }  
        for(int k=0;k<pjgByteExpPrivateD2;k++){
            allPrivate[index]=ExpPrivateD2[k];
            index=index+1;
        }   
        for(int k=0;k<tempByte.length;k++){
            allPrivate[index]=tempByte[k];
            index=index+1;
        }  
        // untuk nilai primaP ... karena ditambahkan 1 byte
        // maka nilai byte sebenarnya diambil dari index ke 1 bukan ke 0
        for(int k=1;k<(pjgBytepPrivate+1);k++){
            allPrivate[index]=pPrivate[k];
            index=index+1;
        }   
        for(int k=0;k<tempByte.length;k++){
            allPrivate[index]=tempByte[k];
            index=index+1;
        }  
        // untuk nilai primaQ ... karena ditambahkan 1 byte
        // maka nilai byte sebenarnya diambil dari index ke 1 bukan ke 0
        for(int k=1;k<(pjgByteqPrivate+1);k++){
            allPrivate[index]=qPrivate[k];
            index=index+1;
        }
        return allPrivate;
	}
	
	private static byte[] concatInfo(byte[] key, Long lelangId, String namaPaket, JenisDokPenawaran dokJenis, String keyInfo) {
//		format info del key del hash
		byte[] tempByte = new byte[10];
        tempByte[0]=35;tempByte[1]=12;tempByte[2]=5;tempByte[3]=13;tempByte[4]=19;
        tempByte[5]=1;tempByte[6]=14;tempByte[7]=5;tempByte[8]=7;tempByte[9]=35;
//		Lelang_seleksi lelang_seleksi = (Lelang_seleksi) lelang_seleksiDao.findByPrimaryKey(lls_id);
		String info = keyInfo + "\n\n" +
                "Kode Tender : " + lelangId + '\n' +
                "Nama Paket : " + namaPaket + '\n' +
                "Jenis Dokumen : " + dokJenis.label + "\n\n" +
                "Pastikan " + keyInfo + " diatas sesuai dengan tujuan pengiriman\n" +
                "Pengguna bertanggung jawab atas keamanan/kerusakan data yang disebabkan\nkelalaian penggunaan aplikasi APENDO\n\n";

		byte[] byteInfo = info.getBytes();		
		int pjgAll = ((2*tempByte.length)+byteInfo.length+key.length);
		byte[] allKeyInfo = new byte[pjgAll];
        int index = 0;
        
        for(int k=0;k<(byteInfo.length);k++){
            allKeyInfo[index]=byteInfo[k];
            index=index+1;
        }
         for(int k=0;k<tempByte.length;k++){
            allKeyInfo[index]=tempByte[k];
            index=index+1;
        }   
         for(int k=0;k<key.length;k++){
            allKeyInfo[index]=key[k];
            index=index+1;
        }
         for(int k=0;k<tempByte.length;k++){
             allKeyInfo[index]=tempByte[k];
             index=index+1;
         }
        
         byte[] hash = KeyUtilities.getMD5Hash(allKeyInfo);
         
         byte[] result = new byte[allKeyInfo.length+hash.length];
         index = 0;
         for(int k=0;k<(allKeyInfo.length);k++){
             result[index]=allKeyInfo[k];
             index=index+1;
         }
          for(int k=0;k<hash.length;k++){
        	 result[index]=hash[k];
             index=index+1;
         }
         return result;
	}

}
