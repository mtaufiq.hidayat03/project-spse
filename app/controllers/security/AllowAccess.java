package controllers.security;

import models.secman.Group;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * Annotation for Controller: security purpose
 * @author Andik Yulianto
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AllowAccess {

	// Which group can access the controller?
	public Group[] value();
}
