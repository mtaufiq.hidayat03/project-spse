package controllers.kontrak;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.JenisEmail;
import models.common.MailQueueDao;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.kontrak.Pesanan;
import models.kontrak.TempSppbjHps;
import models.lelang.*;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import play.data.validation.Validation;
import utils.BlacklistCheckerUtil;
import utils.LogUtil;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Arief
 * controller terkait sspbj tender
 */
public class SppbjCtr  extends BasicCtr {

    @AllowAccess({ Group.PPK })
    public static void sppbjPpk(Long lelangId) {
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        renderArgs.put("lelang", lelang);
        renderArgs.put("allowCreateSppbj", lelang.allowCreateSppbj());
        renderTemplate("kontrak/sppbj-ppk-list.html");
    }

    @AllowAccess({ Group.PPK })
    public static void sppbjPpkDetil(Long lelangId, Long sppbjId) {
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        // data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        renderArgs.put("lelang", lelang);
        boolean isKonsolidasi = lelang.isPaketKonsolidasi();
        renderArgs.put("isKonsolidasi", isKonsolidasi);
        renderArgs.put("konsultan", lelang.getKategori().isConsultant() || lelang.getKategori().isJkKonstruksi());
        Active_user currentUser = Active_user.current();
        Pegawai pegawai = Pegawai.findBy(currentUser.userid);
        Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
        Long ppk_id = ppk != null ? ppk.ppk_id : null;
        renderArgs.put("ppk", ppk);
        renderArgs.put("pegawai", pegawai);
        renderArgs.put("peserta", lelang.findAllWinnerCandidate());
        List<Peserta> pemenang = Peserta.find("lls_id=? and is_pemenang_klarifikasi=1", lelangId).fetch();
        if(CollectionUtils.isEmpty(pemenang))
            pemenang = Peserta.find("lls_id=? and is_Pemenang=1", lelangId).fetch();
        renderArgs.put("pemenang", pemenang);
        renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL + "/daftar-hitam");
        Paket paket = lelang.getPaket();
        renderArgs.put("rupList", paket.isFlag43() ? paket.getRupListByPpk(ppk_id) : paket.getRupListByPpk42(ppk_id));
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);

        Double hargaFinal = 0d;
        Double jaminanPelaksanaan = 0d;

        // data sppbj
        if(sppbjId != null) {
            Sppbj sppbjx, sppbj;
            sppbjx = sppbj = Sppbj.find("sppbj_id=? and ppk_id=?", sppbjId, ppk_id).first();
            if (sppbjx != null) {
                String tembusan[] = null;
                if (!StringUtils.isEmpty(sppbjx.sppbj_tembusan)) {
                    tembusan = sppbjx.sppbj_tembusan.split("\\s*,\\s*");
                    renderArgs.put("tembusan", tembusan);
                }
                hargaFinal = sppbj.harga_final;
                jaminanPelaksanaan = sppbj.jaminan_pelaksanaan;
                renderArgs.put("sppbj", sppbj);
                renderArgs.put("sppbjx", sppbjx);
            }
        }else{
            TempSppbjHps tmp = TempSppbjHps.findByLelangAndPpk(lelangId);
            if(isKonsolidasi && tmp != null){
                DaftarKuantitas dk = CommonUtil.fromJson(tmp.dkh,DaftarKuantitas.class);
                hargaFinal = dk.total;
            }else {
                hargaFinal = Peserta.getTotalHargaFinal(pemenang,lelang);
            }
        }

        renderArgs.put("hargaFinal", hargaFinal);
        renderArgs.put("jaminanPelaksanaan", jaminanPelaksanaan);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderTemplate("kontrak/sppbj-ppk.html");
    }

    @AllowAccess({ Group.PPK })
    public static void simpanSppbj(Long lelangId, Sppbj sppbj,  List<String> nama, Long rekananId, String hapus,String catatan_ppk, String pembuat_sk) {
        checkAuthenticity();
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        // use old value
        Active_user currentUser = Active_user.current();
        Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
        Sppbj sppbjx = sppbj;

        if(sppbj.jaminan_pelaksanaan == null){
            sppbj.jaminan_pelaksanaan = 0d;
        }

        if(sppbj.sppbj_id != null) {
            sppbj = Sppbj.findById(sppbj.sppbj_id);
        } else {
            // sppbj harus unique setiap rekanan, ppk dan lelang
            Sppbj chkSppbj = Sppbj.find("ppk_id=? and rkn_id=? and lls_id=?", ppk.ppk_id, rekananId, lelangId).first();
            if (chkSppbj != null) {
                validation.addError("sppbj.rkn_id", "SPPBJ untuk rekanan dan lelang ini sudah ada!");
            }

            sppbj = new Sppbj();
            sppbj.ppk_id = ppk.ppk_id;
            sppbj.lls_id = lelang.lls_id;
        }
//		sppbj.harga_final = sppbjx.harga_final;
        sppbj.jaminan_pelaksanaan = sppbjx.jaminan_pelaksanaan;
        sppbj.sppbj_no = sppbjx.sppbj_no;
        sppbj.sppbj_lamp = sppbjx.sppbj_lamp;
        sppbj.sppbj_kota = sppbjx.sppbj_kota;
        sppbj.alamat_satker = sppbjx.alamat_satker;
        sppbj.jabatan_ppk_sppbj = sppbjx.jabatan_ppk_sppbj;
        TempSppbjHps temp = TempSppbjHps.findByLelangAndPpk(lelang.lls_id);
        if(temp != null){
            sppbj.sppbj_dkh = temp.dkh;
        }else{
            Peserta peserta = Peserta.findByRekananAndLelang(rekananId, lelang.lls_id);
            if(peserta != null) {
                sppbj.sppbj_dkh = peserta.psr_dkh;
            }
            if(peserta == null){
                peserta = Peserta.findByRekananAndLelang(sppbj.rkn_id, lelang.lls_id);
                sppbj.sppbj_dkh = peserta.psr_dkh;
            }
            ReverseAuction ra = ReverseAuction.findByLelang(lelang.lls_id);
            if(ra != null && peserta != null){
                ReverseAuctionDetil rad = ReverseAuctionDetil.findLastPenawaran(ra.ra_id, peserta.psr_id);
                if(rad != null)
                    sppbj.sppbj_dkh = rad.rad_dkh;
            }
        }

        //disamakan dengan tgl kirim karena di form sudah dihide tapi kolom db tidak boleh null
        sppbj.sppbj_tgl_buat = sppbjx.sppbj_tgl_kirim;
        sppbj.sppbj_tgl_kirim = sppbjx.sppbj_tgl_kirim;

        if(sppbjx.catatan_ppk != null && !lelang.isItemized()){
            validation.min(sppbjx.catatan_ppk.trim().length(),10)
                    .key("sppbj.catatan_ppk").message("Catatan minimal 10 karakter");
        }
        sppbj.catatan_ppk = sppbjx.catatan_ppk;

        if(sppbjx.catatan_hargakontrak != null && !lelang.isItemized()){
            validation.min(sppbjx.catatan_hargakontrak.trim().length(),10)
                    .key("sppbj.catatan_hargakontrak").message("Catatan minimal 10 karakter");
        }
        sppbj.catatan_hargakontrak = sppbjx.catatan_hargakontrak;

/*		validation.required(sppbj.harga_final).key("sppbj.harga_final").message("Harga Kontrak wajib diisi");
		if (sppbj.harga_final != null && sppbj.harga_final <= 0) {
			validation.addError("sppbj.harga_final", "Harga Kontrak harus lebih dari 0");
		}*/

        if (rekananId != null) {
            sppbj.rkn_id = rekananId;
            Peserta peserta = Peserta.findBy(lelang.lls_id, rekananId);
            List<Nilai_evaluasi> list  = lelang.findAllWinnerCandidate();
            if(!CollectionUtils.isEmpty(list)) {
                for(Nilai_evaluasi nilai : list) {
                    if (nilai.psr_id.equals(peserta.psr_id)) {
                        sppbj.harga_final = nilai.hargaFinal();
                        break;
                    }
                }
            }
        }

        //if(StringUtils.isEmpty(pembuat_sk))
        //    Validation.addError("ppk.ppk_pembuat_sk", "validation.required");
        if (nama != null) {
            sppbj.sppbj_tembusan = StringUtils.join(nama, ',');
        } else {
            sppbj.sppbj_tembusan = null;
        }
        boolean success = true;
        sppbj.sppbj_status = sppbj.sppbj_id == null ? sppbj.STATUS_DRAFT : sppbj.STATUS_TERKIRIM;
        validation.valid(sppbj);

        if (validation.hasErrors()) {
            flash.error("Gagal menyimpan SPPBJ. Mohon periksa kembali inputan Anda.");
            validation.keep();
            params.flash();
            success = false;
            LogUtil.info("errors",CommonUtil.toJson(validation.errorsMap()));
        }  else {
            try {
                sppbj.save();
                flash.success("%s berhasil disimpan", "SPPBJ");
            } catch (Exception e) {
                Logger.error(e,"Data gagal tersimpan : %s", e.getLocalizedMessage());
                success = false;
                flash.error("Data gagal tersimpan. Mohon periksa kembali inputan Anda");
            }
        }

        if(success){
            if(temp != null)
                temp.delete();
            sppbjPpkDetil(lelangId, sppbj.sppbj_id);
        }
        sppbjPpkDetil(lelangId, sppbj.sppbj_id);
    }

    @AllowAccess({ Group.PPK })
    public static void sppbjHps(Long sppbjId) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        otorisasiDataLelang(sppbj.lls_id); // check otorisasi data lelang
        renderArgs.put("sppbj", sppbj);
        Lelang_seleksi lelang = Lelang_seleksi.findById(sppbj.lls_id);
        DaftarKuantitas dk = sppbj.dkh;
        if (dk == null) {
            Peserta peserta = Peserta.findByRekananAndLelang(sppbj.rkn_id, lelang.lls_id);
            dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);
        }
        renderArgs.put("lelang", lelang);
        renderArgs.put("data", CommonUtil.toJson(dk.items));
        renderTemplate("kontrak/sppbj-hps.html");
    }

    @AllowAccess({ Group.PPK })
    public static void sppbjHpsPemenang(Long id) {
        Peserta peserta = Peserta.findById(id);
        otorisasiDataLelang(peserta.lls_id); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        DaftarKuantitas dk = peserta.dkh;
        renderArgs.put("lelang", lelang);
        renderArgs.put("data", CommonUtil.toJson(dk.items));
        renderTemplate("kontrak/sppbj-hps-pemenang.html");
    }

    @AllowAccess({Group.PPK})
    public static void sppbjHpsSubmit(Long sppbjId, String data) {
        checkAuthenticity();
        Sppbj sppbj = Sppbj.findById(sppbjId);
        otorisasiDataLelang(sppbj.lls_id); // check otorisasi data lelang
        Map<String, Object> result = new HashMap<>(1);
        try {
            sppbj.simpanHps(data);
            result.put("result", "ok");
        } catch (Exception e) {
            result.put("result", e.getMessage());
        }
        renderJSON(result);
    }


    @AllowAccess({Group.PPK})
    public static void tempSppbjHpsSubmit(Long llsId, Long psrId, String data) {
        checkAuthenticity();
        otorisasiDataLelang(llsId); // check otorisasi data lelang

        Map<String, Object> result = new HashMap<>(1);
        try {
            TempSppbjHps.simpanHps(llsId, psrId, data);
            result.put("result", "ok");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("result", e.getMessage());
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PPK })
    public static void cetakDokSppbj(Long sppbjId) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        long lelangId = sppbj.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lelangId);
        response.contentType = "application/pdf";
        InputStream is = Sppbj.cetak(lelang_seleksi, sppbj);
        sppbj.sppbj_status = sppbj.STATUS_TERKIRIM;
        sppbj.save();
        renderBinary(is);
    }

    @AllowAccess({ Group.PPK })
    public static Integer pengecekanBlacklist(Long rknId, Long llsId) {
        Rekanan rekanan = Rekanan.findById(rknId);
        Active_user active_user = Active_user.current();
        return BlacklistCheckerUtil.checkBlacklistStatus(rekanan, active_user.pegawaiId, llsId, 2);
    }

    @AllowAccess({ Group.PPK })
    public static void kirimPengumumanSppbj(Long sppbjId) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        long lelangId = sppbj.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        try {
            Active_user active_user = Active_user.current();
            MailQueueDao.kirimPengumumanPemenangKontrak(lelang, sppbj, active_user.name);
            sppbj.sudah_kirim_pengumuman = true;
            sppbj.save();
            flash.success("Email pengumuman berhasil dikirim");
        } catch (Exception e) {
            flash.error("Terjadi Kesalahan Teknis Pengiriman");
            Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Pengumuman");
        }
        sppbjPpkDetil(lelangId, sppbj.sppbj_id);
    }

    @AllowAccess({ Group.PPK })
    public static void undanganKontrak(Long id, Long pst_id) {
        Sppbj sppbj = Sppbj.findById(id);
        Peserta peserta = Peserta.findById(pst_id);
        otorisasiDataPeserta(peserta); // check otorisasi data peserta
        renderArgs.put("peserta", peserta);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("lelang", peserta.getLelang_seleksi());
        renderArgs.put("list", MailQueueDao.findUndanganKontrak(peserta.lls_id, peserta.rkn_id, JenisEmail.UNDANGAN_KONTRAK.id));
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderTemplate("kontrak/form-undangan.html");
    }

    @AllowAccess({ Group.PPK })
    public static void submitUndangan(Long id, Long pst_id, @Required @As(binder= DatetimeBinder.class) Date waktu,
                                      @Required @As(binder=DatetimeBinder.class) Date sampai, @Required String tempat,
                                      @Required String dibawa, @Required String hadir) {
        checkAuthenticity();
        Peserta peserta = Peserta.findById(pst_id);
        otorisasiDataPeserta(peserta); // check otorisasi data peserta
        if(waktu == null || sampai == null || StringUtils.isEmpty(tempat) || StringUtils.isEmpty(dibawa) || StringUtils.isEmpty(hadir)) {
            flash.error("Mohon mengisi seluruh isian");
            undanganKontrak(id, pst_id);
        }
        Lelang_seleksi lelang = Lelang_seleksi.findById(peserta.lls_id);
        if(validation.hasErrors()){
            validation.keep();
            params.flash();
            flash.error("Undangan gagal terkirim. Mohon periksa kembali input Anda");
            undanganKontrak(id, pst_id);
        } else {
            try {
                String namaPaket = Paket.getNamaPaketFromlelang(peserta.lls_id);
                String namaPanitia = Panitia.findByLelang(peserta.lls_id).pnt_nama;
                MailQueueDao.kirimUndanganKontrak(peserta.getRekanan(), peserta.lls_id, namaPaket, namaPanitia, waktu, sampai, tempat, dibawa, hadir);
                flash.success("Undangan berhasil terkirim.");
            }catch(Exception e) {
                flash.error("Terjadi Kesalahan Teknis Pengiriman Undangan");
                Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Undangan Kontrak");
            }
        }
        sppbjPpkDetil(lelang.lls_id, id);
    }

    @AllowAccess({ Group.PPK })
    public static void hapusTembusanSppbj(Long id, String nama) {
        notFoundIfNull(id);
        Sppbj sppbj = Sppbj.findById(id);
        Lelang_seleksi lelang = sppbj.getLelang_seleksi();
        otorisasiDataLelang(lelang); // check otorisasi data lelang
        if (!StringUtils.isEmpty(sppbj.sppbj_tembusan)) {
            List<String> tembusan = new ArrayList<>();
            String[] split =  sppbj.sppbj_tembusan.split("\\s*,\\s*");
            for(String o : split){
                if(o.equals(nama))
                    continue;
                tembusan.add(o);
            }
            sppbj.sppbj_tembusan = !CollectionUtils.isEmpty(tembusan) ? StringUtils.join(tembusan) : null;
            sppbj.save();
        }
        sppbjPpkDetil(sppbj.lls_id, id);
    }

    @AllowAccess ({Group.PPK})
    public static void viewUdanganKontrakPdf(Long id, Long rkn_id) {
        MailQueue mailQueue = MailQueueDao.getByJenisAndLelangAndRekanan(id, rkn_id, models.common.JenisEmail.UNDANGAN_KONTRAK);
        renderHtml(MailQueueDao.metaReplacement(mailQueue.getRawBody()));
    }


    @AllowAccess({Group.PPK})
    public static void resetHargaFinalKonsolidasi(Long id, Long pesertaId){
        otorisasiDataLelang(id);
        Peserta peserta = Peserta.findBy(pesertaId);
        DaftarKuantitas dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);
        //Reset yang ada di tabel temporary
        TempSppbjHps temp = TempSppbjHps.find("ppk_id = ? and lls_id = ?",Active_user.current().ppkId,id).first();
        if(temp != null){
            temp.dkh = peserta.psr_dkh;
            temp.save();
        }
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        renderArgs.put("pesertaId", pesertaId);
        renderArgs.put("lelang", lelang);
        renderArgs.put("data", CommonUtil.toJson(dk.items));
        renderTemplate("kontrak/sppbjHpsKonsolidasi.html");
    }


    @AllowAccess({Group.PPK})
    public static void hapusDokSppbj(Long id, Integer versi) {
        Sppbj sppbj = Sppbj.findById(id);
        if (sppbj != null) {
            BlobTable blob = BlobTable.findById(sppbj.sppbj_attachment, versi);
            if (blob != null)
                blob.delete();
            if (sppbj.sppbj_attachment != null && CollectionUtils.isEmpty(sppbj.getDokumen())) {
                sppbj.sppbj_attachment = null;
            }
            sppbj.save();
        }
    }


    @AllowAccess({ Group.PPK })
    public static void uploadDokSppbj(Long id, File file) {
        Sppbj sppbj = Sppbj.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(sppbj != null) {
            otorisasiDataLelang(sppbj.getLelang_seleksi()); // check otorisasi data lelang
            try {
                UploadInfo model = sppbj.simpanDok(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload SPPBJ");
                result.put("result", "Kesalahan saat upload SPPBJ");
            }
        }
        renderJSON(result);
    }
    
	@AllowAccess({ Group.PPK })
	public static void uploadDokPesanan(Long id, File file) {
		Pesanan pesanan = Pesanan.findById(id);
		Map<String, Object> result = new HashMap<>(1);
		if(pesanan != null) {
			otorisasiDataLelang(pesanan.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
			try {
				UploadInfo model = pesanan.simpanDok(file);
				List<UploadInfo> files = new ArrayList<>();
				files.add(model);
				result.put("success", true);
				result.put("files", files);
			} catch (Exception e) {
				Logger.error(e, "Kesalahan saat upload Pesanan");
				result.put("result", "Kesalahan saat upload Pesanan");
			}
		}
		renderJSON(result);
	}
    
}
