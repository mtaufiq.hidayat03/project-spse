package controllers.kontrak;

import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.Kontrak;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.kontrak.BA_Pembayaran;
import models.kontrak.DokLain;
import models.kontrak.SskkPpkContent;
import models.lelang.Lelang_detil;
import models.lelang.Lelang_seleksi;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.FileUpload;
import play.data.validation.Validation;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Arief
 * controller terkait Pembayaran tender
 */
public class PembayaranCtr extends BasicCtr {


    @AllowAccess({ Group.PPK })
    public static void daftarPembayaran(Long kontrakId) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if (kontrakx == null) {
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        otorisasiDataLelang(kontrakx.lls_id); // check otorisasi data lelang
        SskkPpkContent sskk = kontrakx.getSskkContent();
        renderArgs.put("kontrakx", kontrakx);
        if(sskk != null) {
            List<BA_Pembayaran> listBap = BA_Pembayaran.find("kontrak_id=? order by bap_id asc", kontrakx.kontrak_id).fetch();
            if(!StringUtils.isEmpty(sskk.cara_pembayaran) && sskk.cara_pembayaran.equals("Sekaligus") && CollectionUtils.isEmpty(listBap)) {
                BA_Pembayaran ba_new = new BA_Pembayaran();
                ba_new.kontrak_id = kontrakId;
                listBap.add(ba_new);
            }
            renderArgs.put("listBap", listBap);
        }
        renderArgs.put("progressMin", kontrakx.progressMax());
        switch (sskk.cara_pembayaran) {
            case "Termin":
                renderTemplate("kontrak/daftar-pembayaran-termin.html");
                break;
            case "Sekaligus":
                renderTemplate("kontrak/daftar-pembayaran.html");
                break;
            default:
                renderTemplate("kontrak/daftar-pembayaran-bulan.html");
                break;
        }
    }


    @AllowAccess({ Group.PPK })
    public static void buatDokumenLainnya(Long lelangId){
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        renderArgs.put("lelang", Lelang_seleksi.findById(lelangId));
        renderTemplate("kontrak/dokumenLainnya.html");

    }

    @AllowAccess({ Group.PPK })
    public static void dokumenLainnya(Long lelangId){
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        renderArgs.put("lelangId", lelangId);
        renderArgs.put("dokLainList", DokLain.find("lls_id=?", lelangId).fetch());
        renderTemplate("kontrak/daftarDokLain.html");

    }

    @AllowAccess({ Group.PPK })
    public static void editDokumenLainnya(Long lelangId, Long dlpId){
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        renderArgs.put("lelang", Lelang_seleksi.findById(lelangId));
        renderArgs.put("dokLain",DokLain.find("lls_id=? AND dlp_id=?", lelangId, dlpId).first());
        renderTemplate("kontrak/dokumenLainnya.html");
    }

    @AllowAccess({ Group.PPK })
    public static void simpanDokumenLain(Long lelangId, DokLain dokLain, @DokumenType FileUpload dlpAttachment, String hapus) {

        checkAuthenticity();

        otorisasiDataLelang(lelangId); // check otorisasi data lelang

        File file=null;

        dokLain.lls_id = lelangId;

        if(dlpAttachment !=null)
            file=dlpAttachment.asFile();

        validation.valid(dokLain);

        if(validation.hasErrors()) {

            validation.keep();

            params.flash();

            if (dokLain.dlp_id != null)
                editDokumenLainnya(dokLain.lls_id, dokLain.dlp_id);
            else
                buatDokumenLainnya(dokLain.lls_id);

        } else {

            try {

                DokLain.simpanDokLain(dokLain, file, hapus);

                flash.success("Data berhasil disimpan");

                dokumenLainnya(dokLain.lls_id);

            }catch (Exception e) {

                Logger.error(e, "Data gagal disimpan");

                flash.error("Data gagal disimpan");

                if (dokLain.dlp_id != null)
                    editDokumenLainnya(dokLain.lls_id, dokLain.dlp_id);
                else
                    buatDokumenLainnya(dokLain.lls_id);

            }
        }
    }


    @AllowAccess({ Group.PPK })
    public static void viewBap(Long kontrakId, Long bapId) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        otorisasiDataLelang(kontrakx.lls_id); // check otorisasi data lelang
        Lelang_detil lelang_detil = Lelang_detil.findById(kontrakx.lls_id);
        renderArgs.put("lelang_detil", lelang_detil);
        renderArgs.put("kontrakx", kontrakx);
        SskkPpkContent sskk = kontrakx.getSskkContent();
        renderArgs.put("sskk", sskk);
        renderArgs.put("tipePembayaran", sskk.cara_pembayaran);
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
        BA_Pembayaran bapx = null;
        if(bapId != null)
            bapx = BA_Pembayaran.findById(bapId);
        else {
            bapx = new BA_Pembayaran();
        }
        bapx.kontrak_id = kontrakId;
        bapx.besar_pembayaran = bapx.besar_pembayaran == null  || bapx.besar_pembayaran == 0 ? kontrakx.kontrak_nilai : bapx.besar_pembayaran;
        if(StringUtils.isEmpty(bapx.kontrak_wakil_penyedia))
            bapx.kontrak_wakil_penyedia = kontrakx.kontrak_wakil_penyedia;
        if(StringUtils.isEmpty(bapx.kontrak_jabatan_wakil))
            bapx.kontrak_jabatan_wakil = kontrakx.kontrak_jabatan_wakil;
        renderArgs.put("bapx", bapx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderTemplate("kontrak/form-ba-pembayaran.html");
    }

    // Penambahan @Valid untuk validasi form
    @AllowAccess({ Group.PPK })
    public static void simpanBap(Long kontrak_id, BA_Pembayaran bap, String hapus) {
        checkAuthenticity();
        bap.kontrak_id = kontrak_id;
        Kontrak kontrak = Kontrak.findById(kontrak_id);
        otorisasiDataLelang(kontrak.lls_id); // check otorisasi data lelang
        if (bap.bast_tgl!=null && bap.bast_tgl.before(kontrak.kontrak_tanggal)) {
            params.flash();
            flash.error("Tanggal BAST tidak boleh mendahului tanggal kontrak!");
            viewBap(bap.kontrak_id, bap.bap_id);
        }

        if (bap.bap_tgl != null && bap.bap_tgl.before(kontrak.kontrak_tanggal)) {
            params.flash();
            flash.error("Tanggal BAP tidak boleh mendahului tanggal kontrak!");
            viewBap(bap.kontrak_id, bap.bap_id);
        }
        if(StringUtils.isEmpty(bap.jabatan_penandatangan_sk)) {
            Validation.addError("bap.jabatan_penandatangan_sk", "validation.required");
        }
        if(StringUtils.isEmpty(bap.kontrak_wakil_penyedia)) {
            Validation.addError("bap.kontrak_wakil_penyedia", "validation.required");
        }
        if(StringUtils.isEmpty(bap.kontrak_jabatan_wakil)) {
            Validation.addError("bap.kontrak_jabatan_wakil", "validation.required");
        }
        QueryBuilder builder = new QueryBuilder("SELECT SUM(besar_pembayaran) FROM ba_pembayaran WHERE kontrak_id=?", kontrak.kontrak_id);
        if(bap.bap_id != null)
            builder.append("AND bap_id <> ?", bap.bap_id);
        Double sudahBayar = Query.find(builder, Double.class).first();
        if(sudahBayar == null)
            sudahBayar = Double.valueOf(0);
        sudahBayar += bap.besar_pembayaran;
        if(sudahBayar > kontrak.kontrak_nilai) {
            Validation.addError("bap.besar_pembayaran", "Total Besar Pembayaran melebihi Nilai Kontrak");
        }
        if(bap.bap_id != null) {
            BA_Pembayaran bax = BA_Pembayaran.findById(bap.bap_id);
            bap.progres_fisik = bax.progres_fisik;
        }
        validation.valid(bap);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            viewBap(bap.kontrak_id, bap.bap_id);
        } else {
            try {
                bap.save();
                flash.success("%s berhasil disimpan", "Berita Pembayaran");
            } catch (Exception e) {
                Logger.error(e, "%s gagal disimpan", "Berita Pembayaran");
                flash.error("%s gagal disimpan", "Berita Pembayaran");
            }
        }
        viewBap(bap.kontrak_id, bap.bap_id);
    }


    @AllowAccess({ Group.PPK })
    public static void cetakBap(Long bapId) {
        response.contentType = "application/pdf";
        InputStream is = BA_Pembayaran.cetak(bapId);
        renderBinary(is);
    }

    @AllowAccess({ Group.PPK })
    public static void cetakBast(Long bapId) {
        response.contentType = "application/pdf";
        InputStream is = BA_Pembayaran.cetak_bast(bapId);
        renderBinary(is);
    }


    @AllowAccess({ Group.PPK })
    public static void hapus(Long kontrakId, List<Long> bapId) {
        Kontrak kontrak = Kontrak.findById(kontrakId);
        Long lelangId = kontrak.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        StringBuilder param = new StringBuilder();
        // BA_Pembayaran obj = null;
        // int i=0;
        if (bapId == null) {
            BA_Pembayaran.delete("kontrak_id =? ", kontrak.kontrak_id);
        } else {
            for (Long id : bapId) {
                if (id != null) {
                    if (param.length() > 0)
                        param.append(',');
                    param.append(id);
                }

                if (param.length() > 0) {
                    BA_Pembayaran.delete("kontrak_id =? AND bap_id NOT IN (" + param + ')',
                            kontrak.kontrak_id);
                }
            }
        }
        daftarPembayaran(kontrak.kontrak_id);
    }


    @AllowAccess({ Group.PPK })
    public static void uploadDokBap(Long id, File file) {
        BA_Pembayaran bapx = BA_Pembayaran.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(bapx != null) {
            otorisasiDataLelang(bapx.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
            try {
                UploadInfo model = bapx.simpanDokBap(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload BAP");
                result.put("result", "Kesalahan saat upload BAP");
            }
        }
        renderJSON(result);
    }


    @AllowAccess({Group.PPK})
    public static void hapusDokBap(Long id, Integer versi) {
        BA_Pembayaran bapx = BA_Pembayaran.findById(id);
        otorisasiDataLelang(bapx.getKontrak().lls_id); // check otorisasi data lelang
        if (bapx != null) {
            BlobTable blob = BlobTable.findById(bapx.cetak_bap_attachment, versi);
            if (blob != null)
                blob.delete();
            if (bapx.cetak_bap_attachment != null && CollectionUtils.isEmpty(bapx.getDokumenBap())) {
                bapx.cetak_bap_attachment = null;
            }
            bapx.save();
        }
    }


    @AllowAccess({ Group.PPK })
    public static void uploadDokBast(Long id, File file) {
        BA_Pembayaran bapx = BA_Pembayaran.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(bapx != null) {
            otorisasiDataLelang(bapx.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
            try {
                UploadInfo model = bapx.simpanDokBast(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload BAST");
                result.put("result", "Kesalahan saat upload BAST");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusDokBast(Long id, Integer versi) {
        BA_Pembayaran bapx = BA_Pembayaran.findById(id);
        otorisasiDataLelang(bapx.getKontrak().lls_id); // check otorisasi data lelang
        if (bapx != null) {
            BlobTable blob = BlobTable.findById(bapx.cetak_bast_attachment, versi);
            if (blob != null)
                blob.delete();
            if (bapx.cetak_bast_attachment != null && CollectionUtils.isEmpty(bapx.getDokumenBast())) {
                bapx.cetak_bast_attachment = null;
            }
            bapx.save();
        }
    }


    @AllowAccess({Group.PPK})
    public static void hapus_ba_pembayaran(Long kontrakId, Long id) {
        Kontrak kontrak = Kontrak.findById(kontrakId);
        notFoundIfNull(kontrak);
        Long lelangId = kontrak.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        if(id != null)
            BA_Pembayaran.delete("kontrak_id =? AND bap_id=?", kontrakId, id);
        daftarPembayaran(kontrak.kontrak_id);
    }

    @AllowAccess({Group.PPK})
    public static void simpanProgressFisik(Long id, Long progress) {
        notFoundIfNull(id);
        BA_Pembayaran bap = BA_Pembayaran.findById(id);
        Kontrak kontrak = bap.getKontrak();
        otorisasiDataLelang(kontrak.lls_id); // check otorisasi data lelang
        if(progress <= 100L) { // progress fisik maksimal 100%
            if(kontrak.progressMax() < progress) {
                bap.progres_fisik = progress;
                bap.save();
            }
        }
        daftarPembayaran(bap.kontrak_id);
    }

}
