package controllers.kontrak;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.DokPersiapanPl;
import models.agency.KontrakPl;
import models.agency.SppbjPl;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.kontrak.SskkPpkPlContent;
import models.nonlelang.Pl_seleksi;
import models.secman.Group;
import play.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arief
 * controller terkait SSKK non-tender
 */
public class SskkPlCtr extends BasicCtr {

    @AllowAccess({Group.PPK})
    public static void pilihCaraPembayaran(Long id) {
        SppbjPl sppbj = SppbjPl.findById(id);
        renderArgs.put("sppbjId", id);

        KontrakPl kontrak = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        renderArgs.put("kontrak", kontrak);
        renderArgs.put("dokKontrak", kontrak.kontrak_sskk_attacment != null ?
                BlobTableDao.listById(kontrak.kontrak_sskk_attacment) : null);

        SskkPpkPlContent sskkContent = kontrak.getSskkContent();
        if (sskkContent == null)
            sskkContent = new SskkPpkPlContent();
        renderArgs.put("sskkContent", sskkContent);

        Pl_seleksi pl = Pl_seleksi.findById(sppbj.lls_id);
        DokPersiapanPl dokPersiapan = DokPersiapanPl.findOrCreateByPaket(pl.pkt_id);
        renderArgs.put("sskk", dokPersiapan.getDokSskkAttachment());
        renderTemplate("kontrakpl/form-pilih-cara-pembayaran.html");
    }

    @AllowAccess({Group.PPK})
    public static void simpanCaraPembayaran(Long id, SskkPpkPlContent sskkContent) {
        SppbjPl sppbj = SppbjPl.findById(id);
        KontrakPl kontrak = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        kontrak.kontrak_sskk = CommonUtil.toJson(sskkContent);
        kontrak.save();
        SppbjPlCtr.listSppbjPl(sppbj.lls_id);
    }

    @AllowAccess({Group.PPK})
    public static void uploadSskkSubmit(Long id, File file) {
//		checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data non tender
        SppbjPl sppbj = SppbjPl.findByPl(id);
        KontrakPl kontrak = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();

        Map<String, Object> result = new HashMap<>(1);
        try {
            UploadInfo model = kontrak.simpanSskkAttachment(file);
            if (model == null) {
                result.put("success", false);
                result.put("message", "File dengan nama yang sama telah ada di server!");
                renderJSON(result);
            }
            List<UploadInfo> files = new ArrayList<>();
            files.add(model);
            result.put("success", true);
            result.put("files", files);
        } catch (Exception e) {
            Logger.error(e, e.getLocalizedMessage());
            result.put("result", "Kesalahan saat upload sskk");
        }

        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusSskkAttachment(Long id, Integer versi) {
//		checkAuthenticity();
        otorisasiDataLelang(id); // check otorisasi data non tender
        SppbjPl sppbj = SppbjPl.findByPl(id);
        KontrakPl kontrak = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if (kontrak != null) {
            BlobTable blob = BlobTable.findById(kontrak.kontrak_sskk_attacment, versi);
            if (blob != null) {
                blob.delete();
            }
            if (kontrak.kontrak_sskk_attacment != null && kontrak.getDokSskkAttachment().isEmpty()) {
                kontrak.kontrak_sskk_attacment = null;
            }
            kontrak.save();
        }
    }
}
