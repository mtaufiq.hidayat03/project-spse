package controllers.kontrak;

import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.KontrakPl;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.kontrak.BA_Pembayaran;
import models.kontrak.BaPembayaranPl;
import models.kontrak.SskkPpkPlContent;
import models.nonlelang.Pl_detil;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Arief
 * controller terkait Pembayaran non-tender
 */

public class PembayaranPlCtr extends BasicCtr {


    @AllowAccess({Group.PPK})
    public static void viewBapPl(Long kontrakId, Long bapId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        otorisasiDataPl(kontrakx.lls_id); // check otorisasi data non tender
        Pl_detil pl_detil = Pl_detil.findById(kontrakx.lls_id);
        renderArgs.put("pl_detil", pl_detil);
        renderArgs.put("kontrakx", kontrakx);
        SskkPpkPlContent sskk = kontrakx.getSskkContent();
        renderArgs.put("sskk", sskk);
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
        renderArgs.put("tipePembayaran", sskk.cara_pembayaran);
        BaPembayaranPl bapx = null;
        if (bapId != null)
            bapx = BaPembayaranPl.findById(bapId);
        else{
            bapx = new BaPembayaranPl();
        }
        bapx.kontrak_id = kontrakId;
        bapx.besar_pembayaran = bapx.besar_pembayaran == null || bapx.besar_pembayaran == 0 ? kontrakx.kontrak_nilai : bapx.besar_pembayaran;
        if(StringUtils.isEmpty(bapx.kontrak_wakil_penyedia))
            bapx.kontrak_wakil_penyedia = kontrakx.kontrak_wakil_penyedia;
        if(StringUtils.isEmpty(bapx.kontrak_jabatan_wakil))
            bapx.kontrak_jabatan_wakil = kontrakx.kontrak_jabatan_wakil;
        renderArgs.put("bapx", bapx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderTemplate("kontrakpl/form-ba-pembayaran-pl.html");
    }

    @AllowAccess({Group.PPK})
    public static void simpanBapPl(Long kontrak_id, BaPembayaranPl bap, String hapus) {
        checkAuthenticity();
        bap.kontrak_id = kontrak_id;
        KontrakPl kontrak = KontrakPl.findById(kontrak_id);
        otorisasiDataPl(kontrak.lls_id); // check otorisasi data non tender
        if (bap.bast_tgl != null && bap.bast_tgl.before(kontrak.kontrak_tanggal)) {
            flash.error("Tanggal BAST tidak boleh mendahului tanggal kontrak!");
            params.flash();
            viewBapPl(bap.kontrak_id, bap.bap_id);
        }

        if (bap.bap_tgl != null && bap.bap_tgl.before(kontrak.kontrak_tanggal)) {
            params.flash();
            flash.error("Tanggal BAP tidak boleh mendahului tanggal kontrak!");
            viewBapPl(bap.kontrak_id, bap.bap_id);
        }
        validation.valid(bap);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
        } else {
            try {
//                BaPembayaranPl.simpan(bap, file, hapus, file_bast, file_bap);
                bap.save();
                flash.success("%s berhasil disimpan", "Berita Pembayaran");
            } catch (Exception e) {
                Logger.error(e, "%s gagal disimpan", "Berita Pembayaran");
                flash.error("%s gagal disimpan", "Berita Pembayaran");
            }
        }
        viewBapPl(bap.kontrak_id, bap.bap_id);
    }


    @AllowAccess({Group.PPK})
    public static void daftarPembayaranPl(Long kontrakId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if (kontrakx == null) {
            flash.error("Maaf Anda belum membuat dokumen kontrak untuk non tender tersebut");
            BerandaCtr.index();
//            listSppbjPl(kontrakId);
        }
        otorisasiDataPl(kontrakx.lls_id); // check otorisasi data non tender
        SskkPpkPlContent sskk = kontrakx.getSskkContent();
        renderArgs.put("kontrakx", kontrakx);
        if(sskk != null) {
            List<BaPembayaranPl> listBap = BaPembayaranPl.find("kontrak_id=? order by bap_id asc", kontrakx.kontrak_id).fetch();
            if(!StringUtils.isEmpty(sskk.cara_pembayaran) && sskk.cara_pembayaran.equals("Sekaligus") && CollectionUtils.isEmpty(listBap)){
                BaPembayaranPl ba_new = new BaPembayaranPl();
                ba_new.kontrak_id = kontrakId;
                listBap.add(ba_new);
            }
            renderArgs.put("listBap", listBap);
        }
        switch (sskk.cara_pembayaran) {
            case "Termin":
                renderTemplate("kontrakpl/daftarPembayaranTerminPl.html");
                break;
            case "Sekaligus":
                renderTemplate("kontrakpl/daftarPembayaran-pl.html");
                break;
            default:
                renderTemplate("kontrakpl/daftarPembayaranBulanPl.html");
                break;
        }
    }

    @AllowAccess({Group.PPK})
    public static void hapusPembayaranPl(Long plId, List<Long> bapId) {

        otorisasiDataPl(plId); // check otorisasi data pl

        KontrakPl kontrak = KontrakPl.find("lls_id=?", plId).first();

        StringBuilder param = new StringBuilder();
        // BA_Pembayaran obj = null;
        // int i=0;
        if (bapId == null) {
            BaPembayaranPl.delete("kontrak_id =? ", kontrak.kontrak_id);
        } else {

            for (Long id : bapId) {

                if (id != null) {

                    if (param.length() > 0)
                        param.append(',');

                    param.append(id);

                }

                if (param.length() > 0)
                    BA_Pembayaran.delete("kontrak_id =? AND bap_id NOT IN (" + param + ')', kontrak.kontrak_id);

            }

        }

        daftarPembayaranPl(plId);
    }

    @AllowAccess({Group.PPK})
    public static void cetakBapPl(Long bapId) {
        response.contentType = "application/pdf";
        InputStream is = BaPembayaranPl.cetak(bapId);
        renderBinary(is);
    }

    @AllowAccess({Group.PPK})
    public static void cetakBastPl(Long bapId) {
        response.contentType = "application/pdf";
        InputStream is = BaPembayaranPl.cetak_bast(bapId);
        renderBinary(is);
    }

    @AllowAccess({ Group.PPK })
    public static void uploadDokBap(Long id, File file) {
        BaPembayaranPl bapx = BaPembayaranPl.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(bapx != null) {
            otorisasiDataPl(bapx.getKontrak().getPl_seleksi()); // check otorisasi data lelang
            try {
                UploadInfo model = bapx.simpanDokBap(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload BAP");
                result.put("result", "Kesalahan saat upload BAP");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusDokBap(Long id, Integer versi) {
        BaPembayaranPl bapx = BaPembayaranPl.findById(id);
        if (bapx != null) {
            otorisasiDataPl(bapx.getKontrak().getPl_seleksi()); // check otorisasi data lelang
            BlobTable blob = BlobTable.findById(bapx.cetak_bap_attachment, versi);
            if (blob != null)
                blob.delete();
            if (bapx.cetak_bap_attachment != null && CollectionUtils.isEmpty(bapx.getDokumenBap())) {
                bapx.cetak_bap_attachment = null;
            }
            bapx.save();
        }
    }

    @AllowAccess({ Group.PPK })
    public static void uploadDokBast(Long id, File file) {
        BaPembayaranPl bapx = BaPembayaranPl.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(bapx != null) {
            otorisasiDataPl(bapx.getKontrak().getPl_seleksi()); // check otorisasi data lelang
            try {
                UploadInfo model = bapx.simpanDokBast(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload BAST");
                result.put("result", "Kesalahan saat upload BAST");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusDokBast(Long id, Integer versi) {
        BaPembayaranPl bapx = BaPembayaranPl.findById(id);
        if (bapx != null) {
            otorisasiDataPl(bapx.getKontrak().getPl_seleksi()); // check otorisasi data lelang
            BlobTable blob = BlobTable.findById(bapx.cetak_bast_attachment, versi);
            if (blob != null)
                blob.delete();
            if (bapx.cetak_bast_attachment != null && CollectionUtils.isEmpty(bapx.getDokumenBast())) {
                bapx.cetak_bast_attachment = null;
            }
            bapx.save();
        }
    }

    @AllowAccess({Group.PPK})
    public static void simpanProgressFisik(Long id, Long progress) {
        notFoundIfNull(id);
        BaPembayaranPl bap = BaPembayaranPl.findById(id);
        KontrakPl kontrak = bap.getKontrak();
        otorisasiDataLelang(kontrak.lls_id); // check otorisasi data lelang
        if(progress <= 100L) { // progress fisik maksimal 100%
            if(kontrak.progressMax() < progress) {
                bap.progres_fisik = progress;
                bap.save();
            }
        }
        daftarPembayaranPl(bap.kontrak_id);
    }
}
