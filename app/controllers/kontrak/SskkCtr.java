package controllers.kontrak;

import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.DokPersiapan;
import models.agency.Kontrak;
import models.agency.Sppbj;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.kontrak.SskkPpkContent;
import models.lelang.Dok_lelang;
import models.lelang.Lelang_seleksi;
import models.secman.Group;
import play.Logger;
import play.data.validation.Valid;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Arief
 * controller terkait SSKK tender
 */
public class SskkCtr extends BasicCtr {

    @AllowAccess({ Group.PPK })
    public static void sskk(Long sppbjId) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk lelang tersebut");
            BerandaCtr.index();
        }
        Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if (kontrak == null) {
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            SppbjCtr.sppbjPpk(sppbj.lls_id);
        }
        Long lelangId = sppbj.lls_id;
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        SskkPpkContent sskk_content;
        if (kontrak != null) {
            renderArgs.put("lelang", lelang);
            if (kontrak.kontrak_sskk_perubahan != null) {
                sskk_content = kontrak.getSskkContentPerubahan();
            } else if (kontrak.kontrak_sskk == null) {
                //jika sskk kosong maka ambil dari sskk di dokumen lelang
                Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, Dok_lelang.JenisDokLelang.DOKUMEN_LELANG);
                sskk_content = dok_lelang.getSskkContentToSkkkPpk();
            } else {
                sskk_content = kontrak.getSskkContent();
            }
            renderViewSskk(sppbj, lelang, sskk_content, kontrak);
        } else {
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
    }


    private static void renderViewSskk(Sppbj sppbj, Lelang_seleksi lelang, SskkPpkContent sskk_content, Kontrak kontrak){
        Map<String,Object> params = new HashMap<>();
        params.put("lelang", lelang);
        params.put("sskk_content", sskk_content);
        params.put("kategori", lelang.getKategori());
        params.put("kontrak", kontrak);
        params.put("sskkBaru", kontrak.kontrak_sskk == null);
        params.put("sppbj", sppbj);
        params.put("allowedExt", DokumenTypeCheck.allowedExt);
        if (lelang.getPemilihan().isLelangExpress()) {
            renderTemplate("kontrak/sskk_express.html",params);
        } else {
            renderTemplate("kontrak/sskkBarang.html",params);
        }
    }


    @AllowAccess({ Group.PPK })
    public static void simpanSskk(Long sppbjId, @Valid SskkPpkContent sskk_content, Kontrak kontrak, @DokumenType File attachment) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk lelang tersebut");
            BerandaCtr.index();
        }
        checkAuthenticity();
        Long lelangId = sppbj.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        File file = attachment;
        validation.valid(sskk_content);
        if(validation.hasErrors()) {
            validation.keep();
            if (sskk_content.kontrak_akhir != null && sskk_content.kontrak_mulai != null
                    && sskk_content.kontrak_akhir.before(sskk_content.kontrak_mulai)) {
                flash.error("Masa Berlaku akhir kontrak mendahului");
            } else if (sskk_content.kontrak_akhir == null || sskk_content.kontrak_mulai == null) {
                flash.error("Tanggal Berlaku Kontrak belum diisi");
            }
            params.flash();
            renderViewSskk(sppbj,lelang,sskk_content,kontrak);
        } else if (sskk_content != null) {
            Kontrak temp = Kontrak.findById(kontrak.kontrak_id);
            kontrak = temp;
            kontrak.kontrak_mulai = FormatUtils.formatDateFromDatePicker(params.get("sskk_content.kontrak_mulai"));
            kontrak.kontrak_akhir = FormatUtils.formatDateFromDatePicker(params.get("sskk_content.kontrak_akhir"));
            kontrak.kontrak_sskk = CommonUtil.toJson(sskk_content);

            if (temp != null) {

                if (file != null) {
                    try {
                        BlobTable bt = null;
                        if (kontrak.kontrak_id != null) {
                            // sppbj.sppbj_attachment =
                            // Long.parseLong("123");
                            if (kontrak.kontrak_sskk_attacment == null) {
                                // jika baru upload file, gunakan fungsi
                                // save file dengan 2 parameter
                                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);

                            } else if (kontrak.kontrak_sskk_attacment != null) {
                                // jika update file yang di-upload gunakan
                                // fungsi save file dengan 3 parameter
                                bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file,
                                        kontrak.kontrak_sskk_attacment);

                            }
                        } else {

                            bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
                        }
                        kontrak.kontrak_sskk_attacment = bt.blb_id_content;
                        kontrak.save();
                        flash.success("%s berhasil disimpan", "SSKK");

                    } catch (IOException e) {
                        Logger.error(e, "Terjadi IOException @SskkCtr.simpanSskk ");
                    } catch (NoSuchAlgorithmException e) {
                        Logger.error(e, "Terjadi NoSuchAlgorithmException @SskkCtr.simpanSskk");
                    } catch (SQLException e) {
                        Logger.error(e, "Terjadi SQLException @SskkCtr.simpanSskk");
                    } catch (Exception e) {
                        Logger.error(e, "Terjadi Exception @SskkCtr.simpanSskk");
                    }
                } else {

                    try {

                        kontrak.save();
                        flash.success("%s berhasil disimpan", "SSKK");
                    } catch (Exception e) {
                        e.printStackTrace();
                        flash.error("Data gagal tersimpan. Mohon periksa kembali inputan Anda");
                    }
                }

            }
        }
        sskk(sppbjId);
    }

    @AllowAccess({ Group.PPK })
    public static void cetakSskk(Long sppbjId) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long lelangId = sppbj.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        InputStream is = Kontrak.cetak(sppbj);
        response.contentType = "application/pdf";
        renderBinary(is);
    }


    @AllowAccess({ Group.PPK })
    public static Kontrak hapusSSKK(Long id) {
        Kontrak kontrak = Kontrak.findById(id);
        kontrak.kontrak_sskk = null;
        kontrak.save();
        return kontrak;
    }


    @AllowAccess({Group.PPK})
    public static void pilihCaraPembayaran(Long id){
        Sppbj sppbj = Sppbj.findById(id);
        renderArgs.put("sppbjId", id);

        Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        renderArgs.put("kontrak", kontrak);
        renderArgs.put("dokKontrak",kontrak.kontrak_sskk_attacment != null ?
                BlobTableDao.listById(kontrak.kontrak_sskk_attacment) : null);

        SskkPpkContent sskkContent = kontrak.getSskkContent();
        if(sskkContent == null)
            sskkContent = new SskkPpkContent();
        renderArgs.put("sskkContent",sskkContent);

        Lelang_seleksi lelang = Lelang_seleksi.findById(sppbj.lls_id);
        DokPersiapan dokPersiapan = DokPersiapan.findOrCreateByPaket(lelang.pkt_id);
        renderArgs.put("sskk",dokPersiapan.getDokSskkAttachment());

        renderTemplate("kontrak/form-pilih-cara-pembayaran.html");
    }


    @AllowAccess({Group.PPK})
    public static void simpanCaraPembayaran(Long id, SskkPpkContent sskkContent){
        Sppbj sppbj = Sppbj.findById(id);
        Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        kontrak.kontrak_sskk = CommonUtil.toJson(sskkContent);
        kontrak.save();
        SppbjCtr.sppbjPpk(sppbj.lls_id);
    }

    @AllowAccess({Group.PPK})
    public static void uploadSskkSubmit(Long id, File file) {
//		checkAuthenticity();
        Sppbj sppbj = Sppbj.findById(id);
        otorisasiDataLelang(sppbj.lls_id); // check otorisasi data lelang
        Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        Map<String, Object> result = new HashMap<>(1);
        try {
            UploadInfo model = kontrak.simpanSskkAttachment(file);
            if (model == null) {
                result.put("success", false);
                result.put("message", "File dengan nama yang sama telah ada di server!");
                renderJSON(result);
            }
            List<UploadInfo> files = new ArrayList<>();
            files.add(model);
            result.put("success", true);
            result.put("files", files);
        } catch (Exception e) {
            Logger.error(e, e.getLocalizedMessage());
            result.put("result", "Kesalahan saat upload sskk");
        }

        renderJSON(result);
    }

    @AllowAccess({ Group.PPK })
    public static void hapusSskkAttachment(Long id, Integer versi) {
//		checkAuthenticity();
        Sppbj sppbj = Sppbj.findById(id);
        otorisasiDataLelang(sppbj.lls_id); // check otorisasi data lelang
        Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if (kontrak != null) {
            BlobTable blob = BlobTable.findById(kontrak.kontrak_sskk_attacment,versi);
            if (blob != null){
                blob.delete();
            }
            if (kontrak.kontrak_sskk_attacment != null && kontrak.getDokSskkAttachment().isEmpty()) {
                kontrak.kontrak_sskk_attacment = null;
            }
            kontrak.save();
        }
    }
}
