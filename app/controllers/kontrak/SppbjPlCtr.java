package controllers.kontrak;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.*;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.kontrak.TempSppbjPlHps;
import models.nonlelang.EvaluasiPl;
import models.nonlelang.NilaiEvaluasiPl;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.binding.As;
import play.data.validation.Required;
import utils.BlacklistCheckerUtil;
import utils.LogUtil;

import java.io.File;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

/**
 * @author Arief
 * controller terkait sspbj non-tender
 */
public class SppbjPlCtr extends BasicCtr {

    @AllowAccess({Group.PPK})
    public static void listSppbjPl(Long plId) {
        otorisasiDataPl(plId); // check otorisasi data pl
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        MetodePemilihanPenyedia metode = MetodePemilihanPenyedia.findById(pl.mtd_pemilihan);
        renderArgs.put("metode", metode);
        renderArgs.put("pl", pl);
        renderArgs.put("allowCreateSppbj", pl.allowCreateSppbj());
        renderTemplate("kontrakpl/sppbj-ppk-pl-list.html");
    }

    @AllowAccess({Group.PPK})
    public static void sppbjPpkPl(Long plId, Long sppbjId) {
        otorisasiDataPl(plId); // check otorisasi data pl
        //data non tender
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        renderArgs.put("pl", pl);
        boolean isKonsolidasi = pl.isPaketKonsolidasi();
        renderArgs.put("isKonsolidasi", isKonsolidasi);
        renderArgs.put("konsultan", pl.getKategori().isConsultant() || pl.getKategori().isJkKonstruksi());
        Active_user currentUser = Active_user.current();
        Pegawai pegawai = Pegawai.findBy(currentUser.userid);
        Ppk ppk = Ppk.findByPlAndPegawai(plId, currentUser.pegawaiId);
        Long ppk_id = ppk != null ? ppk.ppk_id : null;
        EvaluasiPl evalpl = EvaluasiPl.findPenetapanPemenang(pl.lls_id);

        renderArgs.put("ppk_id", ppk_id);
        renderArgs.put("pegawai", pegawai);
        renderArgs.put("peserta", NilaiEvaluasiPl.findAllWinnerCandidate(pl.getEvaluasiAkhir().eva_id, pl));
//        renderArgs.put("peserta", NilaiEvaluasiPl.find("eva_id=? order by nev_urutan", evalpl.eva_id).fetch());
        List<PesertaPl> pemenang = PesertaPl.find("lls_id=?", plId).fetch();
        if(CollectionUtils.isEmpty(pemenang))
            pemenang = PesertaPl.find("lls_id=? and is_Pemenang=1", plId).fetch();
        renderArgs.put("pemenang", pemenang);
        renderArgs.put("inaproc_url", BasicCtr.INAPROC_URL + "/daftar-hitam");
        Paket_pl paket = pl.getPaket();
        renderArgs.put("rupList", paket.isFlag43() ? paket.getRupList() : paket.getRupList42());
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);

        Double hargaFinal = 0d;
        Double jaminanPelaksanaan = 0d;

        // data sppbj
        if (sppbjId != null) {
            SppbjPl sppbjx, sppbj;
            sppbjx = sppbj = SppbjPl.find("sppbj_id=? and ppk_id=?", sppbjId, ppk_id).first();
            if (sppbjx != null) {
                String str = sppbjx.sppbj_tembusan;
                List<String> tembusan = null;
                if (str != null) {
                    tembusan = Arrays.asList(str.split("\\s*,\\s*"));
                }
                hargaFinal = sppbj.harga_final;
                jaminanPelaksanaan = sppbj.jaminan_pelaksanaan;
                renderArgs.put("sppbj", sppbj);
                renderArgs.put("sppbjx", sppbjx);
                renderArgs.put("tembusan", tembusan);

            }
        } else {
            TempSppbjPlHps tmp = TempSppbjPlHps.findByPlAndPpk(plId);
            if (isKonsolidasi && tmp != null) {
                DaftarKuantitas dk = CommonUtil.fromJson(tmp.dkh, DaftarKuantitas.class);
                hargaFinal = dk.total;
            } else {
                hargaFinal = PesertaPl.getTotalHargaFinal(pemenang, pl);
            }
        }
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderArgs.put("hargaFinal", hargaFinal);
        renderArgs.put("jaminanPelaksanaan", jaminanPelaksanaan);
        renderTemplate("kontrakpl/sppbjPpk-pl.html");
    }


    @AllowAccess({Group.PPK})
    public static void simpanSppbjPl(Long plId, SppbjPl sppbj, List<String> nama, Long rekananId, String hapus, String catatan_ppk) {
        checkAuthenticity();
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        otorisasiDataPl(pl);// check otorisasi data pl
        // use old value
        Active_user currentUser = Active_user.current();
        Ppk ppk = Ppk.findByPlAndPegawai(plId, currentUser.pegawaiId);
        Long ppk_id = ppk != null ? ppk.ppk_id : null;
        SppbjPl sppbjx = sppbj;

        if (sppbj.jaminan_pelaksanaan == null) {
            sppbj.jaminan_pelaksanaan = 0d;
        }

        if (sppbj.sppbj_id != null) {
            sppbj = SppbjPl.findById(sppbj.sppbj_id);
        } else {
            // sppbj harus unique setiap rekanan, ppk dan lelang
            SppbjPl chkSppbj = SppbjPl.find("ppk_id=? and rkn_id=? and lls_id=?", ppk.ppk_id, rekananId, plId).first();
            if (chkSppbj != null) {
                validation.addError("sppbj.rkn_id", "SPPBJ untuk rekanan dan  ini non tender udah ada!");
            }

            sppbj = new SppbjPl();
            sppbj.ppk_id = ppk.ppk_id;
            sppbj.lls_id = pl.lls_id;
        }
//        sppbj.harga_final = sppbjx.harga_final;
        sppbj.jaminan_pelaksanaan = sppbjx.jaminan_pelaksanaan;
        sppbj.sppbj_no = sppbjx.sppbj_no;
        sppbj.sppbj_lamp = sppbjx.sppbj_lamp;
        sppbj.sppbj_kota = sppbjx.sppbj_kota;
        sppbj.alamat_satker=sppbjx.alamat_satker;
        sppbj.jabatan_ppk_sppbj=sppbjx.jabatan_ppk_sppbj;
        validation.required(sppbj.alamat_satker).key("sppbj.alamat_satker").message("Alamat Satker wajib diisi");
        validation.required(sppbj.jabatan_ppk_sppbj).key("sppbj.jabatan_ppk_sppbj").message("Jabatan PPK wajib diisi");

        TempSppbjPlHps temp = TempSppbjPlHps.findByPlAndPpk(pl.lls_id);
        if (temp != null) {
            sppbj.sppbj_dkh = temp.dkh;
        } else {
            PesertaPl peserta = PesertaPl.findByRekananAndPl(rekananId, pl.lls_id);
            if (peserta != null) {
                sppbj.sppbj_dkh = peserta.psr_dkh;
            }
            if (peserta == null) {
                peserta = PesertaPl.findByRekananAndPl(sppbj.rkn_id, pl.lls_id);
                sppbj.sppbj_dkh = peserta.psr_dkh;
            }
        }

        //disamakan dengan tgl kirim karena di form sudah dihide tapi kolom db tidak boleh null
        sppbj.sppbj_tgl_buat = sppbjx.sppbj_tgl_kirim;
        sppbj.sppbj_tgl_kirim = sppbjx.sppbj_tgl_kirim;

        if (sppbjx.catatan_ppk != null) {
            validation.min(sppbjx.catatan_ppk.trim().length(), 10)
                    .key("sppbj.catatan_ppk").message("Catatan minimal 10 karakter");
        }

        sppbj.catatan_ppk = sppbjx.catatan_ppk;

        if (rekananId != null) {
            sppbj.rkn_id = rekananId;
            PesertaPl peserta = PesertaPl.findBy(pl.lls_id, rekananId);
            List<NilaiEvaluasiPl> list = pl.findAllWinnerCandidate();
            if(!CollectionUtils.isEmpty(list)){
                for(NilaiEvaluasiPl nilai : list){
                    if(nilai.psr_id.equals(peserta.psr_id)){
                        sppbj.harga_final = nilai.hargaFinal();
                        break;
                    }
                }
            }
        }

        if (nama != null) {
            sppbj.sppbj_tembusan = StringUtils.join(nama, ',');
        } else {
            sppbj.sppbj_tembusan = null;
        }
        boolean success = true;
        sppbj.sppbj_status = sppbj.sppbj_id == null ? sppbj.STATUS_DRAFT : sppbj.STATUS_TERKIRIM;
        validation.valid(sppbj);

        if (validation.hasErrors()) {
            flash.error("Gagal menyimpan SPPBJ. Periksa kembali inputan Anda.");
            validation.keep();
            params.flash();
            success = false;
            LogUtil.info("errors", CommonUtil.toJson(validation.errorsMap()));
        } else {
            try {
                sppbj.save();
                flash.success("%s berhasil disimpan", "SPPBJ");
            } catch (Exception e) {
                e.printStackTrace();
                success = false;
                flash.error("Data Gagal Tersimpan. Cek kembali inputan Anda");
            }
        }

        if (success) {
            if(temp != null)
                temp.delete();
            sppbjPpkPl(plId, sppbj.sppbj_id);
        }
            sppbjPpkPl(plId, sppbj.sppbj_id);
    }


    @AllowAccess({Group.PPK})
    public static void uploadSppbj(Long id, File file) {
        SppbjPl sppbj = SppbjPl.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if (sppbj != null) {
            otorisasiDataPl(sppbj.getPl());// check otorisasi data non tender
            try {
                UploadInfo model = sppbj.simpanSppbj(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload sppbj");
                result.put("result", "Kesalahan saat upload sppbj");
            }
        }
        renderJSON(result);
    }


    @AllowAccess({Group.PPK})
    public static void hapusSppbj(Long id, Integer versi) {
        SppbjPl sppbj = SppbjPl.find("sppbj_id=?", id).first();
        if (sppbj != null) {
            BlobTable blob = BlobTable.findById(sppbj.sppbj_attachment, versi);
            if (blob != null)
                blob.delete();
            if (sppbj.sppbj_attachment != null && sppbj.getAttachment().isEmpty()) {
                sppbj.sppbj_attachment = null;
            }
            sppbj.save();
        }
    }

    @AllowAccess({Group.PPK})
    public static void sppbjHpsSubmitPl(Long sppbjId, String data) {
        checkAuthenticity();
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        otorisasiDataPl(sppbj.lls_id); // check otorisasi data lelang
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            sppbj.simpanHps(data);
            result.put("result", "ok");
        } catch (Exception e) {
            result.put("result", e.getMessage());
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void cetakDokSppbjPl(Long sppbjId) throws ParseException {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        otorisasiDataPl(sppbj.lls_id); // check otorisasi data lelang
        if (sppbj.harga_final == 0.0) {
            flash.error("Mohon lengkapi detil harga terlebih dahulu");
            sppbjPpkPl(sppbj.lls_id, sppbj.sppbj_id);
        }

        Pl_seleksi pl = sppbj.getPl();
        response.contentType = "application/pdf";
        InputStream is = SppbjPl.cetak(pl, sppbj);
        sppbj.sppbj_status = sppbj.STATUS_TERKIRIM;
        sppbj.save();
        renderBinary(is);
    }


    public static Integer pengecekanBlacklist(Long rknId, Long llsId) {
        Rekanan rekanan = Rekanan.findById(rknId);
        Active_user active_user = Active_user.current();

        return BlacklistCheckerUtil.checkBlacklistStatusPl(rekanan, active_user.pegawaiId, llsId, 2);

    }

    @AllowAccess({Group.PPK})
    public static void kirimPengumumanSppbj(Long sppbjId) {

        SppbjPl sppbj = SppbjPl.findById(sppbjId);

        otorisasiDataPl(sppbj.lls_id); // check otorisasi data lelang

        if (sppbj.harga_final == 0.0) {

            flash.error("Mohon lengkapi detil harga terlebih dahulu");

            sppbjPpkPl(sppbj.lls_id, sppbj.sppbj_id);

        }

        Pl_seleksi pl = sppbj.getPl();

        try {
            MailQueueDao.kirimPengumumanPemenangKontrakPl(pl, sppbj);

            sppbj.sudah_kirim_pengumuman = true;
            sppbj.save();

            flash.success("Email pengumuman berhasil dikirim");
        } catch (Exception e) {
            flash.error("Terjadi Kesalahan Teknis Pengiriman");
            Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Pengumuman");
        }

        sppbjPpkPl(pl.lls_id, sppbj.sppbj_id);

    }


    @AllowAccess({Group.PPK})
    public static void sppbjHpsPl(Long sppbjId) {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        otorisasiDataPl(sppbj.lls_id); // check otorisasi data lelang
        renderArgs.put("sppbj", sppbj);
        Pl_seleksi pl = Pl_seleksi.findById(sppbj.lls_id);
        String data = "[]";
        DaftarKuantitas dk = sppbj.dkh;
        if (dk == null) {
            PesertaPl peserta = PesertaPl.findByRekananAndPl(sppbj.rkn_id, pl.lls_id);
            dk = CommonUtil.fromJson(peserta.psr_dkh, DaftarKuantitas.class);
        }
        renderArgs.put("pl", pl);
        renderArgs.put("data", CommonUtil.toJson(dk.items));
        renderTemplate("kontrakpl/sppbjHpsPl.html");

    }

    @AllowAccess({Group.PPK})
    public static void undanganKontrakPL(Long id, Long pst_id) {
        SppbjPl sppbj = SppbjPl.findById(id);
        PesertaPl peserta = PesertaPl.findById(pst_id);
        otorisasiDataPesertaPl(peserta); // check otorisasi data peserta
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("peserta", peserta);
        renderArgs.put("pl", peserta.getPl_seleksi());
        renderArgs.put("list", MailQueueDao.findUndanganKontrak(peserta.lls_id, peserta.rkn_id, JenisEmail.UNDANGAN_KONTRAK.id));
        renderTemplate("kontrakpl/form-undangan-pl.html");

    }

    @AllowAccess({Group.PPK})
    public static void submitUndanganPl(Long id, @Required @As(binder = DatetimeBinder.class) Date waktu,
                                        @Required @As(binder=DatetimeBinder.class) Date sampai, @Required String tempat,
                                        @Required String dibawa, @Required String hadir) {
        checkAuthenticity();
        PesertaPl peserta = PesertaPl.findById(id);
        otorisasiDataPesertaPl(peserta); // check otorisasi data peserta
        Pl_seleksi pl = Pl_seleksi.findById(peserta.lls_id);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Undangan gagal terkirim. Mohon periksa kembali input Anda");
        } else {
            try {
                String namaPaket = Paket_pl.getNamaPaketFromlelang(peserta.lls_id);
                String namaPp = pl.getPp().getPegawai().peg_nama;
                if(namaPp==null) namaPp="";
                MailQueueDao.kirimUndanganKontrakPl(peserta.getRekanan(), peserta.lls_id, namaPaket, namaPp, waktu, sampai
                        , tempat, dibawa, hadir);
                flash.success("Undangan berhasil terkirim.");
            } catch (Exception e) {
                flash.error("Terjadi Kesalahan Teknis Pengiriman Undangan");
                Logger.error(e, "Terjadi Kesalahan Teknis Pengiriman Undangan Kontrak");
            }
        }
        listSppbjPl(pl.lls_id);
    }

}

