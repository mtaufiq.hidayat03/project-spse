package controllers.kontrak;

import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.kontrak.*;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.validation.Valid;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Arief
 * controller terkait SPP/SPMK non-tender
 */
public class SppPlCtr extends BasicCtr {

    @AllowAccess({Group.PPK})
    public static void suratPesananPpkPl(Long sppbjId) {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk non tender tersebut");
            BerandaCtr.index();
        }
        KontrakPl kontrakx = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            SppbjPlCtr.listSppbjPl(sppbj.lls_id);
        }
        Long plId = kontrakx.lls_id;
        otorisasiDataPl(plId); // check otorisasi data non tender
        renderArgs.put("plId", plId);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("pesanan", PesananPl.find("kontrak_id=?", kontrakx.kontrak_id).fetch());
        renderTemplate("kontrakpl/daftarSuratPesanan-pl.html");
    }

    @AllowAccess({Group.PPK})
    public static void editPesanan(Long sppbjId,  Long kontrakId,Long pesananId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            BerandaCtr.index();
        }
        Long plId = kontrakx.lls_id;
        otorisasiDataPl(plId); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        Active_user currentUser = Active_user.current();
        Ppk ppk = Ppk.findByPlAndPegawai(plId, currentUser.pegawaiId);
        renderArgs.put("ppk", ppk);
        renderArgs.put("sppbj", SppbjPl.findById(sppbjId));
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("pl", pl);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
        if (pesananId != null) {
            PesananPl pesananx = PesananPl.findById(pesananId);
            renderArgs.put("tgl_pes", pesananx.pes_tgl);
            renderArgs.put("content", pesananx.getPesananContent());
            renderArgs.put("pesananx", pesananx);
//            renderArgs.put("barang",);
        }else {
        	PesananContent contentJW = new PesananContent();
			contentJW.jabatan_wakil_rekanan = kontrakx.kontrak_jabatan_wakil;
			contentJW.wakil_sah_rekanan = kontrakx.kontrak_wakil_penyedia;
			renderArgs.put("content", contentJW);
        }
        renderTemplate("kontrakpl/suratPesananPpk-pl.html");
    }

    @AllowAccess({Group.PPK})
    public static void simpanSuratPesananPl(Long sppbjId, Long kontrakId, PesananPl pesanan, PesananContent content, String hapus) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long plId = kontrakx.lls_id;
        Pl_seleksi pl = Pl_seleksi.findById(plId);

        checkAuthenticity();
        otorisasiDataPl(plId);
        if(pl.getKategori().isBarang()){
            params.flash();
            validation.required(content.alamat_kirim).key("content.alamat_kirim").message("Alamat SPP wajib diisi");
            validation.required(params.get("tgl_diterima")).key("tgl_diterima").message("Tanggal Barang Diterima wajib diisi");
            validation.required(params.get("tgl_selesai")).key("tgl_selesai").message("Tanggal Selesai Pekerjaan wajib diisi");
            validation.required(pesanan.pes_no).key("pesanan.pes_no").message("No. SPP wajib diisi");
            validation.required(pesanan.pes_tgl).key("pesanan.pes_tgl").message("Tanggal SPP wajib diisi");
        }
        else{
        	params.flash();
            validation.required(content.kota_pesanan).key("content.kota_pesanan").message("Kota SPMK wajib diisi");
            validation.required(params.get("tgl_diterima")).key("tgl_diterima").message("Tanggal Mulai Kerja wajib diisi");
            validation.required(params.get("tgl_selesai")).key("tgl_selesai").message("Tanggal Selesai Pekerjaan wajib diisi");
            validation.required(pesanan.pes_no).key("pesanan.pes_no").message("No SPMK wajib diisi");
            validation.required(pesanan.pes_tgl).key("pesanan.pes_tgl").message("Tanggal SPMK wajib diisi");
            
            
        }
       
        Date diterima = FormatUtils.formatDateFromDatePicker(params.get("tgl_diterima"));
        Date selesai = FormatUtils.formatDateFromDatePicker(params.get("tgl_selesai"));
        pesanan.kontrak_id = kontrakx.kontrak_id;
        validation.valid(pesanan);
        validation.valid(content);

        content.tgl_brng_diterima = diterima;
        content.tgl_pekerjaan_selesai = selesai;
        pesanan.pes_content = CommonUtil.toJson(content);

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Data Gagal Tersimpan. Cek kembali inputan Anda");
            if (!pl.getKategori().isBarang()) {
                editSpmkPl(sppbjId,kontrakx.kontrak_id, pesanan.pes_id);
            } else {
                editPesanan(sppbjId,kontrakx.kontrak_id, pesanan.pes_id);
            }
        }

        try {
            pesanan.save();
            if (!pl.getKategori().isBarang())
                flash.success("%s berhasil disimpan", "SPMK");
            else
                flash.success("%s berhasil disimpan", "Surat Pesanan");
        } catch (Exception e) {
            e.printStackTrace();
            params.flash();
            flash.error("Data Gagal Tersimpan. Cek kembali inputan Anda");
        }

        if (!pl.getKategori().isBarang()) {
            editSpmkPl(sppbjId,kontrakx.kontrak_id, pesanan.pes_id);
        } else {
            editPesanan(sppbjId,kontrakx.kontrak_id, pesanan.pes_id);
        }
    }

    @AllowAccess({Group.PPK})
    public static void spmkPl(Long sppbjId) {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk non tender tersebut");
            BerandaCtr.index();
        }
        KontrakPl kontrakx = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            SppbjPlCtr.listSppbjPl(sppbj.lls_id);
        }
        Long plId = sppbj.lls_id;
        otorisasiDataPl(plId); // check otorisasi data non tender
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        PesananPl pesananx = PesananPl.find("kontrak_id=?", kontrakx.kontrak_id).first();
        
        Active_user currentUser = Active_user.current();
        Ppk ppk = Ppk.findByPlAndPegawai(plId, currentUser.pegawaiId);
        renderArgs.put("ppk", ppk);
        
        renderArgs.put("plId", plId);
        renderArgs.put("pl", pl);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("pesananx", pesananx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        if (pesananx != null) {
            PesananContent content = pesananx.getPesananContent();
            renderArgs.put("content", content);
            renderTemplate("kontrakpl/SPMK-pl.html");
        } else {
            PesananContent contentJW = new PesananContent();
            contentJW.jabatan_wakil_rekanan = kontrakx.kontrak_jabatan_wakil;
            contentJW.wakil_sah_rekanan = kontrakx.kontrak_wakil_penyedia;
            renderArgs.put("content", contentJW);
            renderTemplate("kontrakpl/SPMK-pl.html");
        }
    }

    @AllowAccess({ Group.PPK })
    public static void editSpmkPl(Long sppbjId, Long kontrakId, Long pesananId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long plId = kontrakx.lls_id;
        otorisasiDataPl(plId); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        
        Active_user currentUser = Active_user.current();
        Ppk ppk = Ppk.findByPlAndPegawai(plId, currentUser.pegawaiId);
        renderArgs.put("ppk", ppk);
        
        renderArgs.put("sppbj", SppbjPl.findById(sppbjId));
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("pl", pl);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        if (pesananId != null) {
            PesananPl pesananx = PesananPl.findById(pesananId);
            renderArgs.put("tgl_pes", pesananx.pes_tgl);
            renderArgs.put("content", pesananx.getPesananContent());
            renderArgs.put("pesananx", pesananx);
        }
        renderTemplate("kontrakpl/SPMK-pl.html");
    }


    @AllowAccess({Group.PPK})
    public static void simpanSpk(Long sppbjId, Long kontrakId, Spk spk, PesananContent content) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            BerandaCtr.index();
        }
        checkAuthenticity();
        SppbjPl sppbjPl = SppbjPl.findById(sppbjId);
        long plId = sppbjPl.lls_id;
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        otorisasiDataPl(plId);
        Date diterima = FormatUtils.formatDateFromDatePicker(params.get("tgl_diterima"));
        Date selesai = FormatUtils.formatDateFromDatePicker(params.get("tgl_selesai"));
        renderArgs.put("sppbjPl", sppbjPl);
        renderArgs.put("pl", pl);
        renderArgs.put("spk", spk);
        renderArgs.put("content", content);

//        KontrakPl kontrakx = KontrakPl.find("lls_id=?", plId).first();
        renderArgs.put("kontrakx", kontrakx);
        validation.required(spk.spk_no).key("spk.pes_no").message("No. Pesanan wajib diisi");
        validation.required(spk.spk_tgl).key("spk.pes_tgl").message("Tanggal Pesanan wajib diisi");
        spk.kontrak_id = kontrakId;
        validation.valid(spk);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Data Gagal Tersimpan. Cek kembali inputan Anda");
            Pegawai pegawai = Pegawai.findBy(Active_user.current().userid);
            renderArgs.put("pegawai", pegawai);
            editSpkPl(sppbjId, kontrakx.kontrak_id, spk.spk_id);
//            renderTemplate("kontrakpl/SPK-pl.html");
        } else {
            try {
                if (spk.spk_id == null) {
                    spk.kontrak_id = kontrakx.kontrak_id;
                } else {
                    Spk spkpl = Spk.findById(spk.spk_id);
                    if (spkpl.spk_barang != null)
                        spk.spk_barang = spkpl.spk_barang;
                }
                content.tgl_brng_diterima = diterima;
                content.tgl_pekerjaan_selesai = selesai;
                spk.spk_content = CommonUtil.toJson(content);
                spk.kontrak_id = kontrakx.kontrak_id;
                spk.save();
                flash.success("%s berhasil disimpan", "Surat Surat Perintah Kerja");
            } catch (Exception e) {
                flash.error("%s gagal disimpan", "Surat KontPerintah Kerjarak");
                Logger.error(e, "%s gagal disimpan", "Surat Perintah Kerja");
            }
        }

        editSpkPl(sppbjId, kontrakx.kontrak_id, spk.spk_id);
    }


    @AllowAccess({Group.PPK})
    public static void cetakPesananPl(Long sppbjId, Long kontrakId, Long pesananId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null ){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            BerandaCtr.index();
        }
        Long plId = kontrakx.lls_id;
        otorisasiDataPl(plId); // check otorisasi data non tender
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        InputStream is = PesananPl.cetak(kontrakx, pl, pesananId);
        response.contentType = "application/pdf";
        renderBinary(is);
    }

    @AllowAccess({Group.PPK})
    public static void cetakSpkPl(Long plId, Long id) {
        otorisasiDataPl(plId); // check otorisasi data lelang
        response.contentType = "application/pdf";
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        SppbjPl sppbjPl = SppbjPl.findByPl(pl.lls_id);
        InputStream is = Spk.cetak(pl, sppbjPl, id);
        renderBinary(is);
    }

    @AllowAccess({Group.PPK})
    public static void hapusPesananPl(Long sppbjId, Long kontrakId, Long pesananId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            BerandaCtr.index();
        }
        Long plId = kontrakx.lls_id;
        otorisasiDataPl(plId); // check otorisasi data non tender
//        List<Pesanan_barang> barang = Pesanan_barang.find("pes_id=?", pesananId).fetch();
//        if (barang != null) {
//            for (Pesanan_barang pes : barang) {
//                pes.delete();
//            }
//        }

        PesananPl pesanan = PesananPl.findById(pesananId);
        pesanan.delete();
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        if (!pl.getKategori().isBarang()) {
            flash.success("Data SPMK Berhasil Dihapus");
            SppbjPlCtr.listSppbjPl(plId);
        } else {
            flash.success("Data Surat Pesanan Berhasil Dihapus");
            suratPesananPpkPl(plId);
        }
    }

    @AllowAccess({Group.PPK})
    public static void hapusSpkPl(Long pesananId, Long plId) {
        otorisasiDataPl(plId); // check otorisasi data lelang
        List<Pesanan_barang> barang = Pesanan_barang.find("pes_id=?", pesananId).fetch();
        if (barang != null) {
            for (Pesanan_barang pes : barang) {
                pes.delete();
            }
        }

        Spk spk = Spk.findById(pesananId);
        spk.delete();
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        if (!pl.getKategori().isBarang()) {
            flash.success("Data SPK Berhasil Dihapus");
            BerandaCtr.index();
        } else {
            flash.success("Data Surat Pesanan Berhasil Dihapus");
            //spkPl(spk.spk_id,plId);
        }
    }

    @AllowAccess({Group.PPK})
    public static void spkPl(Long sppbjId) {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk lelang tersebut");
            BerandaCtr.index();
        }
        KontrakPl kontrakx = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk lelang tersebut");
            SppbjPlCtr.listSppbjPl(sppbj.lls_id);
        }
        Long plId = sppbj.lls_id;
        otorisasiDataPl(plId); // check otorisasi data non tender
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        Spk spkx = Spk.find("kontrak_id=?", kontrakx.kontrak_id).first();
        renderArgs.put("plId", plId);
        renderArgs.put("pl", pl);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("ppk", Ppk.findByPlAndPegawai(plId, Active_user.current().pegawaiId));
        renderArgs.put("spk", spkx);
        if (spkx != null) {
            PesananContent content = spkx.getPesananContent();
            renderArgs.put("content", content);
        }
        renderTemplate("kontrakpl/SPK-pl.html");
    }

    @AllowAccess({ Group.PPK })
    public static void editSpkPl(Long sppbjId, Long kontrakId, Long spkId) {
        KontrakPl kontrakx = KontrakPl.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat kontrak untuk non tender tersebut");
            BerandaCtr.index();
        }
        Long plId = kontrakx.lls_id;
        otorisasiDataPl(plId); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        renderArgs.put("sppbj", SppbjPl.findById(sppbjId));
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("ppk", Ppk.findByPlAndPegawai(plId, Active_user.current().pegawaiId));
        renderArgs.put("pl", pl);
        renderArgs.put("kontrakx", kontrakx);
        if (spkId != null) {
            Spk spkx = Spk.findById(spkId);
            renderArgs.put("spk_tgl", spkx.spk_tgl);
            renderArgs.put("content", spkx.getPesananContent());
            renderArgs.put("spk", spkx);
        }
        renderTemplate("kontrakpl/SPK-pl.html");
    }


    @AllowAccess({Group.PPK})
    public static void openRincianBarang(Long lelangId, Long pesananId) {
        otorisasiDataPl(lelangId); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(lelangId);
        PesananPl pesananx = PesananPl.findById(pesananId);
        Paket_pl paket = Paket_pl.findById(pl.pkt_id);
        SppbjPl sppbj = SppbjPl.findByPl(pl.lls_id);
        KontrakPl kontrak = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id =?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        boolean isSpse3 = paket.pkt_flag == 1;
        String data = "[]";
        if (pesananx != null) {
            DaftarBarang db = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
            if (paket.pkt_flag == 1 && pesananx.pes_barang != null) {
                data = db.items != null ? CommonUtil.toJson(db.items) : "[]";
            } else if (paket.pkt_flag == 2) { // jika lelang SPSE4 rincian
                // barang ambil dari rincian hps
//				SppbjPl sppbj = SppbjPl.findByPl(lelangId);
                PesertaPl peserta = sppbj == null ? null : (PesertaPl) PesertaPl.findByRekananAndPl(sppbj.rkn_id, lelangId);
                DaftarBarang db1 = CommonUtil.fromJson(peserta.psr_dkh, DaftarBarang.class);
                if (!CommonUtil.isEmpty(pesananx.pes_barang))
                    db1 = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
                ;
                if (db1.items != null)
                    data = CommonUtil.toJson(db1.items);
            } else if (paket.pkt_flag == 3) {// jika lelang SPSE 4.3
                DaftarBarang db1 = CommonUtil.fromJson(sppbj.sppbj_dkh, DaftarBarang.class);
                if (!CommonUtil.isEmpty(pesananx.pes_barang))
                    db1 = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
                if (db1 != null && db1.items != null)
                    data = CommonUtil.toJson(db1.items);
            }
        }
        renderArgs.put("kontrak", kontrak);
        renderArgs.put("pesananx", pesananx);
        renderArgs.put("isSpse3", isSpse3);
        renderArgs.put("pl", pl);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("data", data);
        renderTemplate("kontrakpl/rincianBarangEditablePl.html");
    }

    @AllowAccess({Group.PPK})
    public static void openRincianBarangSpk(Long lelangId, Long pesananId) {
        otorisasiDataPl(lelangId); // check otorisasi data lelang
        Pl_seleksi pl = Pl_seleksi.findById(lelangId);
        Spk spk = Spk.findById(pesananId);
        Paket_pl paket = Paket_pl.findById(pl.pkt_id);
        boolean isSpse3 = paket.pkt_flag == 1 ? true : false;
        String data = "[]";
        if (spk != null) {
            DaftarBarang db = CommonUtil.fromJson(spk.spk_barang, DaftarBarang.class);

            if (paket.pkt_flag == 1 && spk.spk_barang != null) {
                data = db.items != null ? CommonUtil.toJson(db.items) : "[]";
            } else if (paket.pkt_flag == 2) { // jika lelang SPSE4 rincian
                // barang ambil dari rincian hps
                SppbjPl sppbj = SppbjPl.findByPl(lelangId);
                PesertaPl peserta = sppbj == null ? null
                        : (PesertaPl) PesertaPl.findByRekananAndPl(sppbj.rkn_id, lelangId);
                DaftarBarang db1 = CommonUtil.fromJson(peserta.psr_dkh, DaftarBarang.class);
                if (db1.items != null)
                    data = CommonUtil.toJson(db1.items);
            }
        }

        render("kontrakpl/rincianBarangSpkPl.html", data, pl, spk, isSpse3);
    }

    public static void simpanRincian(Long lelangId, PesananPl pesananx, Date tgl_pes, String data) {

        Map<String, Object> result = new HashMap<String, Object>(1);
        if (pesananx != null) {
            DaftarBarang db = new DaftarBarang();
            db.items = CommonUtil.fromJson(data, new TypeToken<List<Rincian_Barang>>() {
            }.getType());

            if (db.items != null) {
                for (int i = 0; i < db.items.size(); i++) {
                    if (db.items.get(i).item == null) {
                        db.items.remove(i); // jika ada item yang kosong hapus
                        // dari list
                    } else {
                        Double ppn = ((db.items.get(i).harga == null ? 0 : db.items.get(i).harga)
                                * (db.items.get(i).pajak == null ? 0 : db.items.get(i).pajak) / 100)
                                * ((db.items.get(i).vol == null ? 0 : db.items.get(i).vol)
                                + (db.items.get(i).vol2 == null ? 0 : db.items.get(i).vol2));

                        db.total_ppn = (db.total_ppn == null ? 0 : db.total_ppn) + ppn;
                        db.total = (db.total == null ? 0 : db.total)
                                + (db.items.get(i).total_harga == null ? 0 : db.items.get(i).total_harga);
                    }
                }
                db.total_pembayaran = db.total + db.total_ppn;
            }
            pesananx.db = db;
            if (db != null) {
                pesananx.pes_barang = CommonUtil.toJson(db);
            }
            pesananx.pes_tgl = tgl_pes;
            pesananx.save();
            result.put("result", "ok");
        }
        renderJSON(result);
    }

    public static void simpanRincianSpk(Long lelangId, Spk spk, Date tgl_pes, String data) {

        Map<String, Object> result = new HashMap<String, Object>(1);
        if (spk != null) {
            DaftarBarang db = new DaftarBarang();
            db.items = CommonUtil.fromJson(data, new TypeToken<List<Rincian_Barang>>() {
            }.getType());
            if (db.items != null) {
                for (int i = 0; i < db.items.size(); i++) {
                    if (db.items.get(i).item == null) {
                        db.items.remove(i); // jika ada item yang kosong hapus
                        // dari list
                    } else {
                        Double ppn = ((db.items.get(i).harga == null ? 0 : db.items.get(i).harga)
                                * (db.items.get(i).pajak == null ? 0 : db.items.get(i).pajak) / 100)
                                * ((db.items.get(i).vol == null ? 0 : db.items.get(i).vol)
                                + (db.items.get(i).vol2 == null ? 0 : db.items.get(i).vol2));

                        db.total_ppn = (db.total_ppn == null ? 0 : db.total_ppn) + ppn;
                        db.total = (db.total == null ? 0 : db.total)
                                + (db.items.get(i).total_harga == null ? 0 : db.items.get(i).total_harga);
                    }
                }
                db.total_pembayaran = db.total + db.total_ppn;
            }
            spk.db = db;
            if (db != null) {
                spk.spk_barang = CommonUtil.toJson(db);
            }
            spk.spk_tgl = tgl_pes;
            spk.save();
            result.put("result", "ok");
        }
        renderJSON(result);
    }


    @AllowAccess({Group.PPK})
    public static void uploadDokSpk(Long id, File file) {
        Spk spk = Spk.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if (spk != null) {
            otorisasiDataPl(spk.getKontrak().getPl_seleksi()); // check otorisasi data non tender
            try {
                UploadInfo model = spk.simpanDok(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Pesanan");
                result.put("result", "Kesalahan saat upload Pesanan");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusDokSpk(Long id, Integer versi) {
        Spk spk = Spk.findById(id);
        if (spk != null) {
            otorisasiDataPl(spk.getKontrak().getPl_seleksi()); // check otorisasi data non tender
            BlobTable blob = BlobTable.findById(spk.spk_attachment, versi);
            if (blob != null)
                blob.delete();
            if (spk.spk_attachment != null && CollectionUtils.isEmpty(spk.getDokumen())) {
                spk.spk_attachment = null;
            }
            spk.save();
        }
    }

    @AllowAccess({Group.PPK})
    public static void uploadDokPesanan(Long id, File file) {
        PesananPl pesananPl = PesananPl.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if (pesananPl != null) {
            otorisasiDataPl(pesananPl.getKontrak().getPl_seleksi()); // check otorisasi data non tender
            try {
                UploadInfo model = pesananPl.simpanDok(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Pesanan");
                result.put("result", "Kesalahan saat upload Pesanan");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusDokPesanan(Long id, Integer versi) {
        PesananPl pesananPl = PesananPl.findById(id);
        if (pesananPl != null) {
            otorisasiDataPl(pesananPl.getKontrak().getPl_seleksi()); // check otorisasi data non tender
            BlobTable blob = BlobTable.findById(pesananPl.pes_attachment, versi);
            if (blob != null)
                blob.delete();
            if (pesananPl.pes_attachment != null && CollectionUtils.isEmpty(pesananPl.getDokumen())) {
                pesananPl.pes_attachment = null;
            }
            pesananPl.save();
        }
    }

    @AllowAccess({Group.PPK})
    public static void dokumenLainnyaPl(Long plId) {

        otorisasiDataPl(plId); // check otorisasi data PL

        List<DokLainPl> dokLainPlList = DokLainPl.find("lls_id=?", plId).fetch();

        render("kontrakpl/daftarDokLainPl.html", dokLainPlList, plId);

    }

    @AllowAccess({Group.PPK})
    public static void buatDokumenLainnyaPl(Long plId) {

        otorisasiDataPl(plId); // check otorisasi data pl

        Pl_seleksi pl = Pl_seleksi.findById(plId);

        render("kontrakpl/dokumenLainnya-pl.html", pl);

    }

    @AllowAccess({Group.PPK})
    public static void editDokumenLainnyaPl(Long plId, Long dlpId) {

        otorisasiDataPl(plId); // check otorisasi data pl

        Pl_seleksi pl = Pl_seleksi.findById(plId);

        DokLainPl dokLainPl = DokLainPl.find("lls_id=? AND dlp_id=?", plId, dlpId).first();

        render("kontrakpl/dokumenLainnya-pl.html", pl, dokLainPl);

    }

    @AllowAccess({Group.PPK})
    public static void simpanDokumenLainPl(Long plId, DokLainPl dokLainPl, @Valid @DokumenType File dlpAttachment, String hapus) {

        checkAuthenticity();

        otorisasiDataPl(plId); // check otorisasi data PL

        File file = dlpAttachment != null ? dlpAttachment : null;

        dokLainPl.lls_id = plId;

        if (!validation.required(dokLainPl.dlp_nama_dokumen).ok)
            validation.addError("dokLainPl.dlp_nama_dokumen", "Nama Dokumen wajib diisi", "var");

        if (!validation.required(dokLainPl.dlp_nomor_dokumen).ok)
            validation.addError("dokLainPl.dlp_nomor_dokumen", "Nomor Dokumen wajib diisi", "var");

        Pl_seleksi pl = Pl_seleksi.findById(plId);

        validation.valid(dokLainPl);

        if (validation.hasErrors()) {

            validation.keep();

            params.flash();

            render("kontrakpl/dokumenLainnya-pl.html", pl, dokLainPl);

        } else {

            try {

                DokLainPl.simpanDokLain(dokLainPl, file, hapus);

                flash.success("Data berhasil disimpan");

                dokumenLainnyaPl(dokLainPl.lls_id);

            } catch (Exception e) {

                Logger.error(e, "Data gagal disimpan");

                flash.error("Data gagal disimpan");

                if (dokLainPl.dlp_id != null)
                    editDokumenLainnyaPl(dokLainPl.lls_id, dokLainPl.dlp_id);
                else
                    buatDokumenLainnyaPl(dokLainPl.lls_id);

            }

        }
    }

    @AllowAccess({Group.PPK})
    public static void hapusDokLain(Long plId, List<Long> chkDok) {
        checkAuthenticity();
        if (!CommonUtil.isEmpty(chkDok)) {
            String join = StringUtils.join(chkDok, ",");
            DokLainPl.delete("dlp_id in (" + join + ')');
        }
        flash.success("%s berhasil dihapus", "Data");
        dokumenLainnyaPl(plId);
    }

}
