package controllers.kontrak;

import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.agency.*;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.kontrak.AnggotaKSO;
import models.kontrak.Kontrak_kso;
import models.nonlelang.Dok_pl;
import models.nonlelang.Dok_pl_content;
import models.nonlelang.Pl_seleksi;
import models.rekanan.Landasan_hukum;
import models.secman.Group;
import play.Logger;
import utils.LogUtil;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Arief
 * controller terkait Surat Perjanjian non-tender
 */
public class SuratPerjanjianPlCtr extends BasicCtr {

    @AllowAccess({Group.PPK})
    public static void suratPerjanjianPpkPl(Long sppbjId) {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        if (sppbj == null) {
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk paket tersebut");
            BerandaCtr.index();
        }
        long plId = sppbj.lls_id;
        otorisasiDataPl(plId); // check otorisasi data lelang

        Pl_seleksi pl = Pl_seleksi.findById(plId);
        renderArgs.put("pl", pl);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
        //
        //renderArgs.put("ppk", Ppk.findByPlAndPegawai(plId, Active_user.current().pegawaiId));
        renderArgs.put("ppk", sppbj.getPpk());
        renderArgs.put("anggaranList", Anggaran.findByPaketPl(pl.pkt_id));
        KontrakPl kontrakx = KontrakPl.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if (kontrakx != null) {
            kontrakx.kontrak_nilai = kontrakx.kontrak_nilai == 0 ? sppbj.harga_final : kontrakx.kontrak_nilai;
            List<AnggotaKSO> listAnggota = new ArrayList<>();
            if (kontrakx.kontrak_kso != null) {
                Kontrak_kso kso = kontrakx.getKontrakKSO();
                renderArgs.put("kso", kso);
                listAnggota = kontrakx.getListAnggotaKSO();
            }
            if(null != flash.get("kontrak.anggotakso")){
                listAnggota = CommonUtil.fromJson(flash.get("kontrak.anggotakso"),new TypeToken<List<AnggotaKSO>>(){}.getType());
            }
            renderArgs.put("anggotakso", listAnggota);
        }
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("akta", Landasan_hukum.findByRekanan(sppbj.rkn_id));
        renderTemplate("kontrakpl/surat-perjanjian-ppk-pl.html");
    }

    @AllowAccess({Group.PPK})
    public static void uploadAttachment2(Long id, File file) {
        KontrakPl kontrak = KontrakPl.find("kontrak_id = ?", id).first();
        Map<String, Object> result = new HashMap<>(1);
        if (kontrak != null) {
            try {
                UploadInfo model = kontrak.simpanAttachment2(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload spek");
                result.put("result", "Kesalahan saat upload spek");
            }
        }
        renderJSON(result);
    }

    @AllowAccess({Group.PPK})
    public static void hapusAttachment2(Long id, Integer versi) {
        KontrakPl kontrak = KontrakPl.find("kontrak_id = ?", id).first();
        if (kontrak != null) {
            BlobTable blob = BlobTable.findById(kontrak.kontrak_id_attacment2, versi);
            if (blob != null)
                blob.delete();
            if (kontrak.kontrak_id_attacment2 != null && kontrak.getDokAttachment2().isEmpty()) {
                kontrak.kontrak_id_attacment2 = null;
            }
            kontrak.save();
        }
    }

    @AllowAccess({Group.PPK})
    public static void simpanSuratPerjanjianPl(Long sppbjId, KontrakPl kontrak, Kontrak_kso kso, String pembuat_sk, String nomor_sk, String unit_kerja_sk, String alamat_satker, boolean ubahnilai) {
        SppbjPl sppbj = SppbjPl.findById(sppbjId);
        if (sppbj == null) {
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk paket tersebut");
            BerandaCtr.index();
        }
        checkAuthenticity();
        long plId = sppbj.lls_id;
        Pl_seleksi pl = Pl_seleksi.findById(plId);
        otorisasiDataPl(plId); // check otorisasi data lelang
        Dok_pl dok_pl = Dok_pl.findBy(plId, Dok_pl.JenisDokPl.DOKUMEN_LELANG);
        Dok_pl_content dok_pl_content = Dok_pl_content.findBy(dok_pl.dll_id);
        if (dok_pl_content != null)
            kontrak.kontrak_sskk = dok_pl_content.dll_sskk;
        // 22 maret 2020, ppk diambil dari sppjb
        //Ppk ppk = Ppk.findByPlAndPegawai(plId, Active_user.current().pegawaiId);
        Ppk ppk = sppbj.getPpk();
        kontrak.lls_id = plId;
        kontrak.ppk_id = ppk.ppk_id;
        kontrak.rkn_id = sppbj.rkn_id;
        if (kontrak.kontrak_tipe_penyedia.equals("Penyedia Perseorangan")) {
            kontrak.kontrak_wakil_penyedia = null;
            kontrak.kontrak_jabatan_wakil = null;
            kontrak.kontrak_kso = null;
        } else {
            validation.required(kontrak.kontrak_wakil_penyedia).key("kontrak.kontrak_wakil_penyedia");
            validation.required(kontrak.kontrak_jabatan_wakil).key("kontrak.kontrak_jabatan_wakil");
            if (kontrak.kontrak_tipe_penyedia.equals("Penyedia Kemitraan/KSO")) {
                kso.tgl_surat_kso = FormatUtils.formatDateFromDatePicker(params.get("tgl_kso"));
                kontrak.kontrak_kso = CommonUtil.toJson(kso);
                if(null == kontrak.anggotakso || kontrak.anggotakso.size()<=0){
                    validation.required(kontrak.anggota_kso).key("kontrak.anggota_kso");
                }else{
                    int i=0;
                    for ( AnggotaKSO ag : kontrak.anggotakso) {
                        validation.valid(ag);
                        if(ag.nama_anggota.isEmpty()){
                            validation.required(kontrak.anggota_kso).key("kontrak.anggotakso["+i+"].nama_anggota").message("Nama Anggota wajib di isi");
                        }
                        i++;
                    }
                    if (!validation.hasErrors()) {
                        kontrak.anggota_kso = CommonUtil.toJson(kontrak.anggotakso);
                    }else{
                        flash.put("kontrak.anggotakso", CommonUtil.toJson(kontrak.anggotakso));
                    }
                }
            }
        }
        validation.required(kontrak.kontrak_tanggal).key("kontrak.kontrak_tanggal").message("Tanggal wajib diisi");
        //penyesuaian simpan, 17 maret 2020
        //alamat_satker diambil dari spbj sebelumnya input
        //validation.required(nomor_sk).key("nomor_sk").message("No. SK PPK wajib diisi");
        //validation.required(alamat_satker).key("alamat_satker").message("Alamat Satuan Kerja wajib diisi");
        validation.required(kontrak.kontrak_lingkup_pekerjaan).key("kontrak.kontrak_lingkup_pekerjaan").message("Lingkup Pekerjaan wajib diisi");
        if(ubahnilai==true && kontrak.alasanubah_kontrak_nilai.trim().length() < 10) {
            validation.required(kontrak.alasanubah_kontrak_nilai).key("kontrak.alasanubah_kontrak_nilai").message("Alasan wajib diisi");
            validation.min(kontrak.alasanubah_kontrak_nilai,10).key("kontrak.alasanubah_kontrak_nilai").message("Alasan min 10 karakter");
        }
        validation.valid(kontrak);
        if (validation.hasErrors()) {
            params.flash();
            validation.keep();
            LogUtil.info("errors", CommonUtil.toJson(validation.errorsMap()));
            flash.error("Gagal Menyimpan Surat Kontrak.");
        } else {
            try {
                ppk.ppk_pembuat_sk = pembuat_sk;
                //17 maret 2020, ppk_nomor_sk sudah ambil dari pegawai gak perlu save ulang
                //ppk.ppk_nomor_sk = nomor_sk;
                //23 maret 2020 ppk diambil dari sppbj, yang bisa di edit hanya jabatan_ppk_kontrak nya, dan disimpan di kontrak
                //ppk.ppk_unit_kerja_sk = unit_kerja_sk;
                //ppk.save();
                if(kontrak.kontrak_mulai ==null)
                    kontrak.kontrak_mulai = kontrak.kontrak_tanggal;
                if(kontrak.kontrak_akhir ==null)
                    kontrak.kontrak_akhir = kontrak.kontrak_tanggal;
                if(kontrak.kontrak_lingkup ==null)
                    kontrak.kontrak_lingkup = "";
                kontrak.save();
                Paket_satker_pl ps = Paket_satker_pl.find("pkt_id = ?", pl.pkt_id).first();
                if (ps != null) {
                    Satuan_kerja sk = Satuan_kerja.find("stk_id = ?", ps.stk_id).first();
                    if (sk != null) {
                        //10 maret 2020, alamat_satker diambil dari spbj sebelumnya input
                        sk.stk_alamat = sppbj.alamat_satker;
                        sk.save();
                    }
                }
                flash.success("%s berhasil disimpan", "Surat Kontrak");
            } catch (Exception e) {
                flash.error("%s gagal disimpan", "Surat Kontrak");
                Logger.error(e, "%s gagal disimpan", "Surat Kontrak");
            }
        }
        suratPerjanjianPpkPl(sppbj.sppbj_id);
    }


    @AllowAccess({Group.PPK})
    public static void cetakPerjanjianPl(Long kontrakId) {

        KontrakPl kontrak = KontrakPl.findById(kontrakId);

        Long plId = kontrak.lls_id;

        otorisasiDataPl(plId); // check otorisasi data lelang

        response.contentType = "application/pdf";

        InputStream is = SppbjPl.cetak_perjanjian(kontrak);

        renderBinary(is);

    }

}
