package controllers.kontrak;

import com.google.gson.reflect.TypeToken;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.DokumenTypeCheck;
import ext.FormatUtils;
import models.common.Active_user;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import models.kontrak.*;
import models.agency.*;
import models.lelang.*;
import models.common.Active_user;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import play.Logger;

import java.io.File;
import java.io.InputStream;
import java.util.*;

/**
 * @author Arief
 * controller terkait SPP/SPMK tender
 */
public class SppCtr extends BasicCtr {


    @AllowAccess({ Group.PPK })
    public static void suratPesananPpk(Long sppbjId) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk lelang tersebut");
            BerandaCtr.index();
        }
        Kontrak kontrakx = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        
        if (kontrakx == null) {
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            SppbjCtr.sppbjPpk(sppbj.lls_id);
        }
       
        
        Long lelangId = sppbj.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        renderArgs.put("lelangId", lelangId);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("pesanan", Pesanan.find("kontrak_id=?", kontrakx.kontrak_id).fetch());
        renderTemplate("kontrak/daftarSuratPesanan.html");
    }
    
    @AllowAccess({ Group.PPK })
    public static void editPesanan(Long sppbjId, Long kontrakId, Long pesananId) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long lelangId = kontrakx.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        
        Active_user currentUser = Active_user.current();
		Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
	    renderArgs.put("ppk", ppk);

        renderArgs.put("sppbj", Sppbj.findById(sppbjId));
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("lelang", lelang);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);
        if (pesananId != null) {
            Pesanan pesananx = Pesanan.findById(pesananId);
            renderArgs.put("tgl_pes", pesananx.pes_tgl);
            renderArgs.put("content", pesananx.getPesananContent());
            renderArgs.put("pesananx", pesananx);
            renderArgs.put("barang", Pesanan_barang.find("pes_id=?", pesananx.pes_id).fetch());
        }else {
        	PesananContent contentJW = new PesananContent();
			contentJW.jabatan_wakil_rekanan = kontrakx.kontrak_jabatan_wakil;
			contentJW.wakil_sah_rekanan = kontrakx.kontrak_wakil_penyedia;
			renderArgs.put("content", contentJW);
        }
        renderTemplate("kontrak/surat-pesanan-ppk.html");
    }


    @AllowAccess({ Group.PPK })
    public static void openRincianBarang(Long lelangId, Long pesananId) {
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        Pesanan pesananx = Pesanan.findById(pesananId);
        Paket paket = Paket.findById(lelang.pkt_id);
        Sppbj sppbj = Sppbj.findByLelang(lelang.lls_id);
        Kontrak kontrak = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        boolean isSpse3 = paket.pkt_flag == 1;
        String data = "[]";
        if (pesananx != null) {
            DaftarBarang db = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
            if (paket.pkt_flag == 1 && pesananx.pes_barang != null) {
                data = db.items != null ? CommonUtil.toJson(db.items) : "[]";
            } else if (paket.pkt_flag == 2) { // jika lelang SPSE4 rincian
                // barang ambil dari rincian hps
                Peserta peserta = sppbj == null ? null : Peserta.findByRekananAndLelang(sppbj.rkn_id, lelangId);
                DaftarBarang db1 = CommonUtil.fromJson(peserta.psr_dkh, DaftarBarang.class);
                if (!CommonUtil.isEmpty(pesananx.pes_barang))
                    db1 = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
                if (db1.items != null)
                    data = CommonUtil.toJson(db1.items);
            } else if (paket.pkt_flag == 3) { // jika lelang SPSE 4.3
                DaftarBarang db1 = CommonUtil.fromJson(sppbj.sppbj_dkh, DaftarBarang.class);
                if (!CommonUtil.isEmpty(pesananx.pes_barang))
                    db1 = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
                if (db1 != null && db1.items != null)
                    data = CommonUtil.toJson(db1.items);
            }
        }
        renderArgs.put("kontrak", kontrak);
        renderArgs.put("pesananx", pesananx);
        renderArgs.put("isSpse3",isSpse3);
        renderArgs.put("lelang", lelang);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("data", data);
        renderTemplate("kontrak/rincianBarangEditable.html");
    }

    @AllowAccess({ Group.PPK })
    public static void openRincianBarangFixed(Long lelangId, Long pesananId){
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        Pesanan pesananx = Pesanan.findById(pesananId);
        Paket paket = Paket.findById(lelang.pkt_id);
        boolean isSpse3 = paket.pkt_flag == 1;
        DaftarBarang daftarBarang = new DaftarBarang();
        renderArgs.put("lelang", lelang);
        if (pesananx != null) {
            daftarBarang = CommonUtil.fromJson(pesananx.pes_barang, DaftarBarang.class);
            if (paket.pkt_flag == 2){ //jika lelang SPSE4 rincian barang ambil dari rincian hps
                Sppbj sppbj = Sppbj.findByLelang(lelangId);
                Peserta peserta = sppbj == null ? null : Peserta.findByRekananAndLelang(sppbj.rkn_id, lelangId);
                daftarBarang = CommonUtil.fromJson(peserta.psr_dkh, DaftarBarang.class);
            }
        }
        renderArgs.put("daftarBarang", daftarBarang);
        renderArgs.put("pesananx", pesananx);
        renderArgs.put("isSpse3", isSpse3);
        renderTemplate("kontrak/rincianBarangFixed.html");
    }

    @AllowAccess({ Group.PPK })
    public static void spmk(Long sppbjId, Long pes_id) {
        Sppbj sppbj = Sppbj.findById(sppbjId);
        if(sppbj == null){
            flash.error("Maaf Anda belum membuat dokumen SPPBJ untuk lelang tersebut");
            BerandaCtr.index();
        }
        Kontrak kontrakx = Kontrak.find("lls_id = ? and rkn_id = ? and ppk_id = ?", sppbj.lls_id, sppbj.rkn_id, sppbj.ppk_id).first();
        if (kontrakx == null) {
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            SppbjCtr.sppbjPpk(sppbj.lls_id);
        }
        Long lelangId = sppbj.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        Pesanan pesananx = Pesanan.find("kontrak_id=?", kontrakx.kontrak_id).first();
        
        Active_user currentUser = Active_user.current();
		Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
	    renderArgs.put("ppk", ppk);
        
        renderArgs.put("lelangId", lelangId);
        renderArgs.put("lelang", lelang);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("sppbj", sppbj);
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("pesananx", pesananx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        renderArgs.put("allowedExt", DokumenTypeCheck.allowedExt);

        if (pesananx != null) {
            PesananContent content = pesananx.getPesananContent();

            renderArgs.put("content", content);
            renderTemplate("kontrak/spmk.html");
        } else {
            PesananContent contentJW = new PesananContent();
            contentJW.jabatan_wakil_rekanan = kontrakx.kontrak_jabatan_wakil;
            contentJW.wakil_sah_rekanan = kontrakx.kontrak_wakil_penyedia;
            renderArgs.put("content", contentJW);
            renderTemplate("kontrak/spmk.html");
        }

    }

    @AllowAccess({Group.PPK})
    public static void simpanSpmk(Long sppbjId, Long kontrakId ,Pesanan pesanan ,PesananContent content, String hapus) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if (kontrakx == null) {
            flash.error("Maaf Anda belum membuat dokumen Surat Perjanjian untuk Tender tersebut");
            BerandaCtr.index();
        }
        Long lelangId = kontrakx.lls_id;
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);

        checkAuthenticity();
        otorisasiDataLelang(lelangId); // check otorisasi data Tender

        validation.required(params.get("tgl_diterima")).key("tgl_diterima").message("Tanggal Barang Diterima wajib diisi");
        validation.required(params.get("tgl_selesai")).key("tgl_selesai").message("Tanggal Selesai Pekerjaan wajib diisi");
        validation.required(params.get("pesanan.pes_tgl")).key("pesanan.pes_tgl").message("Tanggal Pesanan wajib diisi");
        validation.required(params.get("pesanan.pes_no")).key("pesanan.pes_no").message("No. Pesanan wajib diisi");

        Date diterima = FormatUtils.formatDateFromDatePicker(params.get("tgl_diterima"));
        Date selesai = FormatUtils.formatDateFromDatePicker(params.get("tgl_selesai"));
        pesanan.kontrak_id = kontrakx.kontrak_id;
        validation.valid(pesanan);
        validation.valid(content);
        content.tgl_brng_diterima = diterima;
        content.tgl_pekerjaan_selesai = selesai;
        pesanan.pes_content = CommonUtil.toJson(content);

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Data gagal tersimpan. Mohon periksa kembali isian Anda");
            if (!lelang.getKategori().isBarang()) {
                editSpmk(sppbjId, kontrakx.kontrak_id,pesanan.pes_id);
            }

        }

        pesanan.kontrak_id = kontrakx.kontrak_id;
        if (pesanan.pes_id == null) {
            pesanan.kontrak_id = kontrakx.kontrak_id;
        } else {
            Pesanan pesan = Pesanan.findById(pesanan.pes_id);
            if (pesan.pes_barang != null) {
                pesanan.pes_barang = pesan.pes_barang;
            }
        }
        try {
            pesanan.save();
            if (!lelang.getKategori().isBarang()) {
                flash.success("%s berhasil disimpan", "SPMK");
            }
        } catch (Exception e) {
            e.printStackTrace();
            flash.error("Data gagal tersimpan. Mohon periksa kembali inputan Anda");
        }
        if (!lelang.getKategori().isBarang()) {
            System.out.println("pesanan.pes_id 1"+ pesanan);
            editSpmk(sppbjId, kontrakx.kontrak_id,pesanan.pes_id);
        }
    }


    @AllowAccess({ Group.PPK })
    public static void editSpmk(Long sppbjId, Long kontrakId, Long pesananId) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long lelangId = kontrakx.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        Active_user currentUser = Active_user.current();
		Ppk ppk = Ppk.findByLelangAndPegawai(lelangId, currentUser.pegawaiId);
	    renderArgs.put("ppk", ppk);
        renderArgs.put("sppbj", Sppbj.findById(sppbjId));
        renderArgs.put("pegawai", Pegawai.findBy(Active_user.current().userid));
        renderArgs.put("lelang", lelang);
        renderArgs.put("kontrakx", kontrakx);
        renderArgs.put("todayDate", FormatUtils.formatDateToDatePickerView(new Date()));
        System.out.println("PESANID1"+pesananId);
        if (pesananId != null) {
            System.out.println("PESANID2");
            Pesanan pesananx = Pesanan.findById(pesananId);
            renderArgs.put("tgl_pes", pesananx.pes_tgl);
            renderArgs.put("content", pesananx.getPesananContent());
            renderArgs.put("pesananx", pesananx);
        }
        renderTemplate("kontrak/spmk.html");
    }




    @AllowAccess({Group.PPK})
    public static void simpanSuratPesanan(Long sppbjId, Long kontrakId ,Pesanan pesanan ,PesananContent content, String hapus) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if (kontrakx == null) {
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long lelangId = kontrakx.lls_id;
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);

        checkAuthenticity();
        otorisasiDataLelang(lelangId); // check otorisasi data lelang

        if(lelang.getPemilihan().isLelangExpress()){
            validation.required(content.jaminan).key("content.jaminan").message("Jaminan Cacat (bulan) wajib diisi");
        }

        params.flash();
        validation.required(content.alamat_kirim).key("content.alamat_kirim").message("Alamat Pengiriman wajib diisi");
        validation.required(params.get("tgl_diterima")).key("tgl_diterima").message("Tanggal Barang Diterima wajib diisi");
        validation.required(params.get("tgl_selesai")).key("tgl_selesai").message("Tanggal Selesai Pekerjaan wajib diisi");
        validation.required(pesanan.pes_no).key("pesanan.pes_no").message("No. SPP wajib diisi");
        validation.required(pesanan.pes_tgl).key("pesanan.pes_tgl").message("Tanggal SPP wajib diisi");
        validation.required(pesanan.pes_tgl).key("content.kota_pesanan").message("Kota SPP wajib diisi");
        Date diterima = FormatUtils.formatDateFromDatePicker(params.get("tgl_diterima"));
        Date selesai = FormatUtils.formatDateFromDatePicker(params.get("tgl_selesai"));

        pesanan.kontrak_id = kontrakx.kontrak_id;

        validation.valid(pesanan);
        validation.valid(content);
        content.tgl_brng_diterima = diterima;
        content.tgl_pekerjaan_selesai = selesai;
        pesanan.pes_content = CommonUtil.toJson(content);

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.error("Data gagal tersimpan. Mohon periksa kembali inputan Anda");

                editPesanan(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);

        }
        try {

            pesanan.save();

                flash.success("%s berhasil disimpan", "Surat Pesanan");

        } catch (Exception e) {
            e.printStackTrace();
            flash.error("Data gagal tersimpan. Mohon periksa kembali inputan Anda");
            params.flash();
        }
            editPesanan(sppbjId, kontrakx.kontrak_id, pesanan.pes_id);
    }


    @AllowAccess({ Group.PPK })
    public static void hapusPesanan(Long sppbjId, Long kontrakId, Long pesananId){
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long lelangId = kontrakx.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        List<Pesanan_barang> barang = Pesanan_barang.find("pes_id=?", pesananId).fetch();
        if(barang!=null){
            for(Pesanan_barang pes: barang){
                pes.delete();
            }
        }

        Pesanan pesanan = Pesanan.findById(pesananId);
        pesanan.delete();
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
        if(!lelang.getKategori().isBarang()){
            flash.success("Data SPMK Berhasil Dihapus");
            SppbjCtr.sppbjPpk(lelangId);
        }else{
            flash.success("Data Surat Pesanan Berhasil Dihapus");
            suratPesananPpk(sppbjId);
        }
    }

    @AllowAccess({ Group.PPK })
    public static void simpanRincian(Long lelangId, Pesanan pesananx, Date tgl_pes, String data) {
        Map<String, Object> result = new HashMap<>(1);
        if (pesananx != null) {
            DaftarBarang db = new DaftarBarang();
            db.items = CommonUtil.fromJson(data, new TypeToken<List<Rincian_Barang>>(){}.getType());
            if (db.items != null) {
                for (int i = 0; i < db.items.size(); i++) {
                    if (db.items.get(i).item == null) {
                        db.items.remove(i); // jika ada item yang kosong hapus
                        // dari list
                    } else {
                        double ppn = ((db.items.get(i).harga == null ? 0 : db.items.get(i).harga)
                                * (db.items.get(i).pajak == null ? 0 : db.items.get(i).pajak) / 100)
                                * ((db.items.get(i).vol == null ? 0 : db.items.get(i).vol)
                                + (db.items.get(i).vol2 == null ? 0 : db.items.get(i).vol2));

                        db.total_ppn = (db.total_ppn == null ? 0 : db.total_ppn) + ppn;
                        db.total = (db.total == null ? 0 : db.total)
                                + (db.items.get(i).total_harga == null ? 0 : db.items.get(i).total_harga);
                    }
                }
                db.total_pembayaran = db.total + db.total_ppn;
            }
            pesananx.db = db;
            pesananx.pes_barang = CommonUtil.toJson(db);
            pesananx.pes_tgl = tgl_pes;
            pesananx.save();
            result.put("result", "ok");
        }
        renderJSON(result);
    }


    @AllowAccess({ Group.PPK })
    public static void cetakPesanan(Long sppbjId, Long kontrakId, Long pesananId) {
        Kontrak kontrakx = Kontrak.findById(kontrakId);
        if(kontrakx == null){
            flash.error("Maaf Anda belum membuat dokumen surat perjanjian untuk lelang tersebut");
            BerandaCtr.index();
        }
        Long lelangId = kontrakx.lls_id;
        otorisasiDataLelang(lelangId); // check otorisasi data lelang
        Lelang_seleksi lelang_seleksi = Lelang_seleksi.findById(lelangId);
        InputStream is = Pesanan.cetak(kontrakx, lelang_seleksi, pesananId);
        response.contentType = "application/pdf";
        renderBinary(is);
    }


    @AllowAccess({Group.PPK})
    public static void hapusDokumenSuratPesanan(Long id, Integer versi) {
        Pesanan pesanan = Pesanan.findById(id);
        otorisasiDataLelang(pesanan.getKontrak().lls_id); // check otorisasi data lelang
        if (pesanan != null) {
            BlobTable blob = BlobTable.findById(pesanan.pes_attachment,versi);
            if (blob != null)
                blob.delete();
            if(pesanan.pes_attachment != null && CollectionUtils.isEmpty(pesanan.getDokumen())){
                pesanan.pes_attachment = null;
            }
            pesanan.save();
        }
    }
 

    @AllowAccess({ Group.PPK })
    public static void uploadDokPesanan(Long id, File file) {
        Pesanan pesanan = Pesanan.findById(id);
        Map<String, Object> result = new HashMap<>(1);
        if(pesanan != null) {
            otorisasiDataLelang(pesanan.getKontrak().getLelang_seleksi()); // check otorisasi data lelang
            try {
                UploadInfo model = pesanan.simpanDok(file);
                List<UploadInfo> files = new ArrayList<>();
                files.add(model);
                result.put("success", true);
                result.put("files", files);
            } catch (Exception e) {
                Logger.error(e, "Kesalahan saat upload Pesanan");
                result.put("result", "Kesalahan saat upload Pesanan");
            }
        }
        renderJSON(result);
    }
}
