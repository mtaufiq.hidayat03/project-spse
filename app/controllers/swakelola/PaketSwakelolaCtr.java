package controllers.swakelola;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.AnggaranSwakelola;
import models.agency.*;
import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.secman.Group;
import models.sirup.PaketSwakelolaSirup;
import models.sso.common.Kabupaten;
import org.apache.commons.collections4.CollectionUtils;
import play.mvc.Http.Cookie;
import play.mvc.Router;

import java.util.*;

/**
 * Created by Lambang on 4/18/2017.
 */
public class PaketSwakelolaCtr extends BasicCtr {

    @AllowAccess({Group.PPK})
    public static void index(Long ppkId, Integer status){
//        renderArgs.put("status", status);
//        Active_user active_user = Active_user.current();
//        Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);
//        renderArgs.put("disableUser",pegawai.isDisableUser());
        if(status == null)
            renderArgs.put("status", Paket_swakelola.StatusPaketSwakelola.SEDANG_LELANG); // default status paket
        Active_user user = Active_user.current();
        Map<String, Object> params = new HashMap<>();
        String url;
        String templateUrl = "swakelola/paket/paket-swakelola-list.html";

            params.put("ppkId", user.ppkId);
            params.put("status", status);
            url = Router.reverse("DataTableCtr.paketPpkSwakelola", params).url;

        renderArgs.put("ppkId", ppkId);
        renderArgs.put("url", url);
        renderArgs.put("isAllowCreate", user.isPpk());
        renderTemplate(templateUrl);
    }
    
    @AllowAccess({Group.PPK})
	public static void view(Long id){
		Paket_swakelola paket = Paket_swakelola.findById(id);
//		otorisasiDataPaketPl(paket); // check otorisasi data paket
        otorisasiDataPaketSwakelola(paket);// check otorisasi data paket
//		Pp pp = paket.getPp();
        Ppk ppk = paket.getPpk();
        List<Paket_swakelola_lokasi> lokasiList = Paket_swakelola_lokasi.find("pkt_id=?", paket.swk_id).fetch();
        List<Paket_anggaran_swakelola> anggaranList = Paket_anggaran_swakelola.findByPaket(id);
        renderArgs.put("paket", paket);
        renderArgs.put("ppk", ppk);
        renderArgs.put("lokasiList", lokasiList);
        renderArgs.put("anggaranList", anggaranList);
		renderTemplate("swakelola/paket/paket-swakelola-view.html");
	}

    @AllowAccess({Group.PPK})
    public static void edit(Long id){
        //cookie
        Paket_swakelola paket = Paket_swakelola.findById(id);
        Active_user active_user = Active_user.current();
        otorisasiDataPaketSwakelola(paket); // check otorisasi data paket
        Cookie cookie=request.cookies.get("paket_edit_propinsi_id");
        Long propinsiId=null;
        if(cookie!=null && !CommonUtil.isEmpty(cookie.value))
            propinsiId=Long.parseLong(cookie.value);
        paket.getPpk();
        setPaketLokasi(paket, propinsiId);
        paket.withLocations();
        if (CommonUtil.isEmpty(paket.getPaketLocations())) {
            Paket_swakelola_lokasi pl = new Paket_swakelola_lokasi();
            if (propinsiId != null) {
                // default propinsi
                List<Kabupaten> listKab = Kabupaten.findByPropinsi(propinsiId);
                if (!listKab.isEmpty())
                    pl.kbp_id = listKab.get(0).kbp_id;
            }
            paket.setPaketLocations(new ArrayList<>(1));
            paket.getPaketLocations().add(pl);
        }
//        if(CommonUtil.isEmpty(paket.getAnggarans())) {
//            paket.setAnggarans(new ArrayList<>(1));
//            paket.getAnggarans().add(new AnggaranSwakelola());
//        }
        paket.getPaketAnggarans();
        if (CollectionUtils.isEmpty(paket.getPaketAnggarans())) {
            paket.paketAnggarans = new ArrayList<>(1);
            paket.paketAnggarans.add(new Paket_anggaran_swakelola());
        }
//        String instansiId = null;
//        if(paket.getNamaSatker() != null) {
//            Satuan_kerja satker = Satuan_kerja.findById(paket.stk_id);
//            instansiId = satker.instansi_id;
//        }
        renderArgs.put("paket", paket);
//        renderArgs.put("instansiId", instansiId);
        Boolean draft = paket.pkt_status.isDraft();
        renderArgs.put("draft", paket.pkt_status.isDraft());
        renderTemplate("swakelola/paket/paket-swakelola-edit.html");
    }

    private static void setPaketLokasi(Paket_swakelola paket, Long propinsiId) {
        if (CommonUtil.isEmpty(paket.getPaketLocations())) {
            Paket_swakelola_lokasi pl_lokasi = new Paket_swakelola_lokasi();
            if (propinsiId != null) {
                // default propinsi
                List<Kabupaten> listKab = Kabupaten.findByPropinsi(propinsiId);
                if (!listKab.isEmpty())
                    pl_lokasi.kbp_id = listKab.get(0).kbp_id;
            }
            paket.setPaketLocations(new ArrayList<>(1));
            paket.getPaketLocations().add(pl_lokasi);
        }
    }

    @AllowAccess({Group.PPK})
    public static void rencana_swakelola(Long paketId, String instansiId, Long satkerId, Integer tahun, Integer tipeSwakelola){
        int tahunNow = DateUtil.getTahunSekarang();
        renderArgs.put("paketId", paketId);
        if(CommonUtil.isEmpty(instansiId)) {
            //dapatkan id Instansi dari cookies
            Cookie cookieInstansi=request.cookies.get("sirup_instansi_id");
            if(cookieInstansi!=null && !CommonUtil.isEmpty(cookieInstansi.value))
                instansiId=cookieInstansi.value;
        }
        if(satkerId == null) {
            Cookie cookieSatker=request.cookies.get("sirup_satker_id");
            if(cookieSatker != null && !CommonUtil.isEmpty(cookieSatker.value))
                satkerId=Long.parseLong(cookieSatker.value);
        }
        if(tahun == null) {
            Cookie cookieTahun=request.cookies.get("sirup_tahun");
            if(cookieTahun!=null && !cookieTahun.value.equals("")){
                tahun=Integer.parseInt(cookieTahun.value);
            }else{
                tahun = tahunNow;
            }
        }

        renderArgs.put("tahun", tahun);
        renderArgs.put("instansiId", instansiId);
        renderArgs.put("satkerId", satkerId);
        renderTemplate("swakelola/paket/rencana-paket-swakelola.html");

    }

    @AllowAccess({Group.PPK})
    public static void buatPaketSwakelola(Long id, Long paketId, Long ppkId){
        checkAuthenticity();
        Paket_swakelola paket = Paket_swakelola.buatPaketSwakelolaFromSirup(id, paketId, ppkId);
        edit(paket.swk_id);

    }


    @AllowAccess({Group.PPK})
    public static void hapus_rup_swakelola(Long id, Long rupId){
        Paket_swakelola paket = Paket_swakelola.findById(id);
        otorisasiDataPaketSwakelola(paket); // check otorisasi data paket
        Paket_satker_swakelola paket_satker = Paket_satker_swakelola.find("swk_id=? AND rup_id=? ", id, rupId).first();
        RupPaketSwakelola rup = RupPaketSwakelola.findById(rupId);
        Rup_anggaran[] list_rup_anggaran = rup.getListPaketAnggaran();
        Anggaran anggaran;
        paket.pkt_nama = paket.pkt_nama.replace(',' +rup.nama,"");
        for(Rup_anggaran rup_anggaran:list_rup_anggaran) {
            anggaran = Anggaran.find("ang_koderekening=? AND ang_tahun=? AND stk_id=? AND ang_id in (SELECT ang_id FROM ekontrak.paket_anggaran_swakelola WHERE swk_id=?)",
                    rup_anggaran.mak, rup.tahunAnggaran, paket_satker.stk_id, paket.swk_id).first();
            if(anggaran != null) {
                paket.pkt_pagu = paket.pkt_pagu - anggaran.ang_nilai;
                Paket_anggaran_swakelola.delete("swk_id=? AND ang_id=?", paket.swk_id, anggaran.ang_id);
                anggaran.delete();
            }
        }
        paket_satker.delete();
        paket.save();
        edit(id);
    }

    @AllowAccess({Group.PP, Group.PPK})
    public static void simpan (Long id, Paket_swakelola paket, Paket_swakelola_lokasi[] lokasi){
        checkAuthenticity();
        Paket_swakelola obj = Paket_swakelola.findById(id);
        otorisasiDataPaketSwakelola(obj); // check otorisasi data paket
        List<Paket_swakelola_lokasi> paketSwakelolaList = new ArrayList<>(Arrays.asList(lokasi));

        paketSwakelolaList.removeIf(current -> current.pkt_lokasi == null && current.kbp_id == null && current.pkl_id != null);

        lokasi = paketSwakelolaList.toArray(new Paket_swakelola_lokasi[0]);

        if (validation.hasErrors()) {
            params.flash(); // add http parameters to the flash scope
            validation.keep();
            List<Paket_swakelola_lokasi> lokasiList = new ArrayList<>();
            for(int i = 0; i < lokasi.length; i++){
                Paket_swakelola_lokasi paketSwakelolaLokasi = new Paket_swakelola_lokasi();
                paketSwakelolaLokasi.kbp_id = lokasi[i].kbp_id;
                paketSwakelolaLokasi.pkt_lokasi = lokasi[i].pkt_lokasi;
                paketSwakelolaLokasi.pkt_lokasi_versi = 1;
                lokasiList.add(paketSwakelolaLokasi);
            }

            List<AnggaranSwakelola> anggaranList = AnggaranSwakelola.findByPaket(id);
            if(CommonUtil.isEmpty(anggaranList)) {
                anggaranList = new ArrayList<>(1);
                anggaranList.add(new AnggaranSwakelola());
            }

            String instansiId = null;
            if(paket.getSatuan_kerja() != null) {
                Satuan_kerja satker = Satuan_kerja.findById(paket.stk_id);
                instansiId = satker.instansi_id;
            }

//            Boolean draft = paket.pkt_status.isDraft();
            renderArgs.put("paket", paket);
            renderArgs.put("lokasiList", lokasiList);
            renderArgs.put("anggaranList", anggaranList);
            renderArgs.put("instansiId", instansiId);
//            renderArgs.put("draft", draft);
            renderTemplate("swakelola/paket/paket-swakelola-edit.html");

        } else {
            if(obj.pkt_status.isDraft()){
                obj.pkt_nama = paket.pkt_nama.replace("", "").replace("\"", "");
            }
            Paket_swakelola.simpanBuatPaket(obj, lokasi);
            flash.success("&{'flash.data_paket_telah_tersimpan'}");
        }

        edit(id);
    }

    public static void simpanPpk(Long id){
        Paket_swakelola paket = Paket_swakelola.findById(id);
        Paket_anggaran_swakelola anggaran = Paket_anggaran_swakelola.find("swk_id =?", paket.swk_id).first();
        if(paket.ppk_id == null){
            paket.ppk_id = anggaran.ppk_id;
            paket.save();
        }
        edit(id);
    }

    @AllowAccess({Group.PPK})
    public static void rencanaViewSwakelola(Long id, Long paketId){
        PaketSwakelolaSirup paket = PaketSwakelolaSirup.findById(id);
        renderArgs.put("paketId", paketId);
        renderArgs.put("paket", paket);
        if(!CollectionUtils.isEmpty(paket.paket_anggaran_json)){
            renderArgs.put("anggaranList", paket.paket_anggaran_json);
        }
        renderArgs.put("satker", Satuan_kerja.find("rup_stk_id=?", paket.rup_stk_id).first());
        Map<Long,List<String>> mapKepanitiaan = new HashMap<>();
        Map<Long,Boolean> mapBuatPaket = new HashMap<>();
        renderArgs.put("mapKepanitiaan", mapKepanitiaan);
        renderArgs.put("mapBuatPaket", mapBuatPaket);
        renderArgs.put("isPpk", Active_user.current().isPpk());
        renderTemplate("swakelola/paket/rencana-view-swakelola.html");
    }

}
