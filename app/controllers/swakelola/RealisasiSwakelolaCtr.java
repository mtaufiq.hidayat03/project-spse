package controllers.swakelola;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.FormatUtils;
import models.agency.Paket_swakelola;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.form.FormTambahPenyediaNonSikap;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.nonlelang.JenisRealisasiSwakelola;
import models.nonlelang.Swakelola_seleksi;
import models.nonlelang.nonSpk.RealisasiNonPenyediaSwakelola;
import models.nonlelang.nonSpk.RealisasiPenyediaSwakelola;
import models.secman.Group;
import models.sikap.NonRekananSikap;
import models.sikap.RekananSikap;
import play.data.validation.Valid;
import play.libs.URLs;
import models.sso.common.adp.util.DceSecurityV2;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by Lambang on 10/24/2017.
 */
public class RealisasiSwakelolaCtr extends BasicCtr {


    @AllowAccess({ Group.PPK })
    public static void editJenisRealisasi(Long swakelolaId, Long jenisRealisasiId) {
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swakelolaId);
        otorisasiDataSwakelola(swakelolaId); // check otorisasi data swakelola
//        if (swakelola.getSwakelola().pkt_status.isSelesaiLelang())
//            forbidden();

        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah(JenisRealisasiSwakelola.getJumlahNilaiRealisasi(swakelolaId));
        renderArgs.put("swakelola",swakelola);
        renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);
        if (jenisRealisasiId != null) {
            JenisRealisasiSwakelola jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(jenisRealisasiId);
            List<RealisasiPenyediaSwakelola> realisasiPenyediaSwakelolaList = new ArrayList<>();
            List<RealisasiNonPenyediaSwakelola> realisasiNonPenyediaSwakelolaList = new ArrayList<>();
            if (jenisRealisasiSwakelola.is_penyedia)
                realisasiPenyediaSwakelolaList = RealisasiPenyediaSwakelola.findWithRealisasi(jenisRealisasiSwakelola.rsk_id);
            else
                realisasiNonPenyediaSwakelolaList = RealisasiNonPenyediaSwakelola.findWithRealisasi(jenisRealisasiSwakelola.rsk_id);

            renderArgs.put("jenisRealisasiSwakelola", jenisRealisasiSwakelola);
            renderArgs.put("realisasiPenyediaSwakelolaList", realisasiPenyediaSwakelolaList);
            renderArgs.put("realisasiNonPenyediaSwakelolaList", realisasiNonPenyediaSwakelolaList);
            renderTemplate("swakelola/realisasi/edit-jenis-dokumen.html");

        }
        renderTemplate("swakelola/realisasi/edit-jenis-dokumen.html");
    }

    @AllowAccess({ Group.PPK })
    public static void viewJenisRealisasi(Long swakelolaId, Long jenisDokumenId) {
//        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swakelolaId);
        JenisRealisasiSwakelola jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(jenisDokumenId);
        List<RealisasiPenyediaSwakelola> realisasiPenyediaSwakelolaList = new ArrayList<>();
        List<RealisasiNonPenyediaSwakelola> realisasiNonPenyediaSwakelolaList = new ArrayList<>();
        renderArgs.put("swakelola", swakelola);
        renderArgs.put("jenisRealisasiSwakelola", jenisRealisasiSwakelola);
        if (jenisRealisasiSwakelola.is_penyedia)
            realisasiPenyediaSwakelolaList = RealisasiPenyediaSwakelola.findWithRealisasi(jenisRealisasiSwakelola.rsk_id);
        else
            realisasiNonPenyediaSwakelolaList = RealisasiNonPenyediaSwakelola.findWithRealisasi(jenisRealisasiSwakelola.rsk_id);
        renderArgs.put("realisasiPenyediaSwakelolaList", realisasiPenyediaSwakelolaList);
        renderArgs.put("realisasiNonPenyediaSwakelolaList", realisasiNonPenyediaSwakelolaList);
        renderTemplate("swakelola/realisasi/view-jenis-dokumen.html");

    }


    @AllowAccess({ Group.PPK })
    public static void simpanJenisRealisasi(Long swakelolaId, JenisRealisasiSwakelola jenisRealisasiSwakelola,
                                            @Valid @DokumenType File attachment, String hapus1) throws Exception {

        checkAuthenticity();
//        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang
        File file = attachment != null ? attachment : null;
        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah(JenisRealisasiSwakelola.getJumlahNilaiRealisasi(swakelolaId));
        if(!validation.required(jenisRealisasiSwakelola.rsk_jenis).ok)
            validation.addError("jenisRealisasiSwakelola.rsk_jenis", "Jenis Realisasi wajib diisi", "var");

        if(!validation.required(jenisRealisasiSwakelola.rsk_nilai).ok)
            validation.addError("jenisRealisasiSwakelola.rsk_nilai", "Nilai Realisasi wajib diisi", "var");

        if(!validation.required(jenisRealisasiSwakelola.rsk_tanggal).ok)
            validation.addError("jenisRealisasiSwakelola.rsk_tanggal", "Tanggal Realisasi wajib diisi", "var");

        if(validation.required(jenisRealisasiSwakelola.rsk_nilai).ok) {
            Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swakelolaId);
            Paket_swakelola paket_swakelola = swakelola.getSwakelola();
            Double totalJumlahNilaiRealisasi;
            if(jenisRealisasiSwakelola.rsk_id != null) {
                totalJumlahNilaiRealisasi = JenisRealisasiSwakelola.getJumlahNilaiRealisasi(swakelolaId, jenisRealisasiSwakelola.rsk_id);
            } else {
                totalJumlahNilaiRealisasi = JenisRealisasiSwakelola.getJumlahNilaiRealisasi(swakelolaId);
            }
            totalJumlahNilaiRealisasi += + jenisRealisasiSwakelola.rsk_nilai;
            renderArgs.put("swakelola", swakelola);
            if (totalJumlahNilaiRealisasi > paket_swakelola.pkt_pagu) {
                validation.addError("Total Nilai Realisasi melebihi Pagu", "var");
                flash.error("Total Nilai Realisasi melebihi Pagu, silakan sesuaikan kembali Nilai Realisasi.");
            }
        }

        if(jenisRealisasiSwakelola.rsk_jenis == 7 && CommonUtil.isEmpty(jenisRealisasiSwakelola.rsk_nama_dok))
            validation.addError("jenisRealisasiSwakelola.rsk_nama_dok", "Nama Dokumen wajib diisi", "var");

        if (validation.hasErrors()) {
            validation.keep();
            Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swakelolaId);
            renderArgs.put("swakelola", swakelola);
            renderArgs.put("jenisRealisasiSwakelola", jenisRealisasiSwakelola);
            renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);
            renderTemplate("swakelola/realisasi/edit-jenis-dokumen.html");
        }

        jenisRealisasiSwakelola.lls_id = swakelolaId;

        if (jenisRealisasiSwakelola.rsk_id != null){
            if (jenisRealisasiSwakelola.rsk_jenis != 7)
                jenisRealisasiSwakelola.rsk_nama_dok = null;

            JenisRealisasiSwakelola jenisRealisasi = JenisRealisasiSwakelola.findById(jenisRealisasiSwakelola.rsk_id);

            jenisRealisasi.rsk_jenis = jenisRealisasiSwakelola.rsk_jenis;
            jenisRealisasi.rsk_nomor = jenisRealisasiSwakelola.rsk_nomor;
            jenisRealisasi.rsk_nama = jenisRealisasiSwakelola.rsk_nama;
            jenisRealisasi.rsk_nama_dok = jenisRealisasiSwakelola.rsk_nama_dok;
            jenisRealisasi.rsk_tanggal = jenisRealisasiSwakelola.rsk_tanggal;
            jenisRealisasi.rsk_nilai = jenisRealisasiSwakelola.rsk_nilai;
            jenisRealisasi.rsk_npwp = jenisRealisasiSwakelola.rsk_npwp;
            jenisRealisasi.rsk_keterangan = jenisRealisasiSwakelola.rsk_keterangan;

            jenisRealisasiSwakelola = jenisRealisasi;

        }

        if (file == null) {
            if (jenisRealisasiSwakelola.rsk_id_attachment != null && hapus1 == null) {
                BlobTable.delete("blb_id_content=?", jenisRealisasiSwakelola.rsk_id_attachment);
                jenisRealisasiSwakelola.rsk_id_attachment = null;
            }
        } else {
            if (file != null) {
                BlobTable bt = null;
                if(jenisRealisasiSwakelola.rsk_id_attachment == null)
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
                else
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, jenisRealisasiSwakelola.rsk_id_attachment);

                jenisRealisasiSwakelola.rsk_id_attachment = bt.blb_id_content;
            }

        }

        jenisRealisasiSwakelola.save();

        flash.success("Berhasil simpan Realisasi");

        editJenisRealisasi(swakelolaId, jenisRealisasiSwakelola.rsk_id);

    }

    @AllowAccess({ Group.PPK })
    public static void hapusJenisDokumen(Long swakelolaId, Long jenisDokumenId) {

//        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang

        if (jenisDokumenId != null) {

            JenisRealisasiSwakelola jenisDokumenSwakelola = JenisRealisasiSwakelola.findById(jenisDokumenId);

            if (jenisDokumenSwakelola.is_penyedia)
                RealisasiPenyediaSwakelola.deleteByRealisasi(jenisDokumenSwakelola.rsk_id);
            else
                RealisasiNonPenyediaSwakelola.deleteByRealisasi(jenisDokumenSwakelola.rsk_id);

            jenisDokumenSwakelola.delete();

            flash.success("Berhasil Hapus JenisDokumen");
        }

        SwakelolaCtr.edit(swakelolaId);

    }

   /* @AllowAccess({Group.PPK})
    public static void uploadInformasiLainnya(Long id, File file) {
        otorisasiDataNonSpk(id);
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            UploadInfo info = DokumenLainNonSpk.simpan(id, DokumenLainNonSpk.JenisDokLainNonSpk.INFORMASI_LAINNYA.value, file);
            List<UploadInfo> files = new ArrayList<UploadInfo>();
            files.add(info);
            result.put("files", files);
        }catch(Exception e) {
            Logger.error("Kesalahan saat upload informasi lainnya: detail %s", e.getMessage());
            e.printStackTrace();
        }
        renderJSON(result);
    }*/

    /*@AllowAccess({ Group.PPK })
    public static void hapusInformasiLainnya(Long id) {

        DokumenLainNonSpk dokumenLainNonSpk = DokumenLainNonSpk.findById(id);

        if (dokumenLainNonSpk != null) {

            BlobTable blob = BlobTableDao.getLastById(dokumenLainNonSpk.dlk_id_attachment);

            if (blob != null)
                blob.delete();

            dokumenLainNonSpk.delete();
        }

    }


    @AllowAccess({ Group.PPK })
    public static void viewRincianHps(Long nonSpkId){

        Dok_non_spk dokNonSpk = Dok_non_spk.findBy(nonSpkId);

        JsonParser parser = new JsonParser();

        Double total = 0d;

        List<Map<String, Object>> items = new ArrayList<>();

        if(dokNonSpk.dns_dkh != null) {

            JsonObject jsonObject = parser.parse(dokNonSpk.dns_dkh).getAsJsonObject();

            total = jsonObject.get("total").getAsDouble();

            items = formatRincianHps(nonSpkId);

        }

        render("nonSpk/realisasi/viewRincianHpsNonSpk.html", items, total);

    }

    public static  List<Map<String, Object>> formatRincianHps(Long nonSpkId){

        Dok_non_spk dokNonSpk = Dok_non_spk.findBy(nonSpkId);

        JsonParser parser = new JsonParser();

        JsonObject jsonObject = parser.parse(dokNonSpk.dns_dkh).getAsJsonObject();

        JsonArray jsonArray = jsonObject.get("items").getAsJsonArray();

        List<Map<String, Object>> items = new ArrayList<>();

        for (int i = 0; i < jsonArray.size(); i++){

            Map<String, Object> item = new HashMap<>();

            JsonObject json = jsonArray.get(i).getAsJsonObject();

            item.put("item", json.get("item").getAsString());
            item.put("unit", json.get("vol").getAsDouble());
            item.put("vol", json.get("unit").getAsString());
            item.put("unit2", json.get("vol2").getAsDouble());
            item.put("vol2", json.get("unit2").getAsString());
            item.put("harga", FormatUtils.formatDesimal2(json.get("harga").getAsDouble()));
            item.put("total_harga", FormatUtils.formatDesimal2(json.get("total_harga").getAsDouble()));
            item.put("keterangan", json.get("keterangan").getAsString());
            item.put("pajak", json.get("pajak").getAsDouble());

            items.add(item);

        }

        return items;

    }*/

    @AllowAccess({ Group.PPK })
    public static void tambahPenyediaSikap(){

        Active_user active_user = Active_user.current();

        int repoId = ConfigurationDao.getRepoId();

        Pegawai pegawai = Pegawai.findById(active_user.pegawaiId);

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("repo_id", repoId);
        jsonObject.addProperty("peg_namauser", pegawai.peg_namauser);
        jsonObject.addProperty("repo_nama", pegawai.peg_nama);
        jsonObject.addProperty("peg_role", active_user.group.toString());
        jsonObject.addProperty("pnt_id", active_user.ppkId);
        jsonObject.addProperty("pnt_nama", pegawai.peg_nama);

        String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));

        String url = BasicCtr.SIKAP_URL;

        redirect(url+"/nonrekanan/index?q="+param);

    }

    //pilih penyedia
    @AllowAccess({Group.PPK})
    public static void viewPilihPenyedia(Long realisasiId, Boolean search, String npwp, String nama, Long kabupatenId, Long propinsiId,
                                         String jenisIjin, String repoId) throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiSwakelola jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiId);

        List<Map<String, String>> rekananList = new ArrayList<>();

        String rekananListJson = null;

        search = search == null ? false : search;

        if (realisasiId != null) {

            if ((npwp != null && !npwp.isEmpty()) || (nama != null && !nama.isEmpty())
                    || (jenisIjin != null && !jenisIjin.isEmpty()) || kabupatenId != null) {
                rekananList = RekananSikap.getListPenyediaByNpwpFromSikap(npwp, nama, kabupatenId, jenisIjin, jenisRealisasiSwakelola.rsk_id, "realisasi_swakelola");

                rekananListJson = CommonUtil.toJson(rekananList);

            } else{
                if(search)
                    flash.error("Silakan masukkan/pilih terlebih dahulu salah satu parameter pencarian");
            }

            //else if (kabupatenId == null && ((npwp != null && !npwp.equals("")) || (nama != null && !nama.equals(""))))
            //    flash.error("Silakan Pilih Kabupaten Terlebih Dahulu");
            //rekananList = Rekanan.getRekananByNpwp(npwp);

            if ((rekananList == null || rekananList.size() == 0) && (kabupatenId != null || (jenisIjin != null && !jenisIjin.isEmpty())
                    || (nama != null && !nama.isEmpty()) || (npwp != null && !npwp.isEmpty()))) {
                rekananList = new ArrayList<>();
                flash.error("<i class=\"fa fa-close\"></i>&nbsp;&nbsp;Penyedia tidak di temukan/koneksi ke SIKaP bermasalah.");
            }

        }

        render("swakelola/realisasi/realisasi-swakelola-pilih-penyedia.html", jenisRealisasiSwakelola, rekananList, npwp, nama, kabupatenId, propinsiId, jenisIjin, rekananListJson);

    }

    @AllowAccess({Group.PPK})
    public static void simpanPilihPenyedia(Long realisasiId, String npwp, String nama, Long kabupatenId, Long propinsiId,
                                           String jenisIjin, String repoId,  List<Long> rekananId, List<String> rekananNama,
                                           List<String> rekananNpwp, List<String> rekananEmail, List<String> rekananTelepon,
                                           List<String> rekananAlamat)
            throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiSwakelola jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiId);

        if(jenisRealisasiSwakelola != null) {

            jenisRealisasiSwakelola.is_penyedia = true;

            jenisRealisasiSwakelola.save();

            //hapus data non penyedia
            RealisasiNonPenyediaSwakelola.deleteByRealisasi(jenisRealisasiSwakelola.rsk_id);

            rekananId = rekananId != null ? rekananId : new ArrayList<Long>();

            rekananId.removeAll(Collections.singleton(null));

            rekananNama = rekananNama != null ? rekananNama : new ArrayList<String>();

            rekananNama.removeAll(Collections.singleton(null));

            rekananNpwp = rekananNpwp != null ? rekananNpwp : new ArrayList<String>();

            rekananNpwp.removeAll(Collections.singleton(null));

            rekananEmail = rekananEmail != null ? rekananEmail : new ArrayList<String>();

            rekananEmail.removeAll(Collections.singleton(null));

            rekananTelepon = rekananEmail != null ? rekananTelepon : new ArrayList<String>();

            rekananTelepon.removeAll(Collections.singleton(null));

            rekananAlamat = rekananAlamat != null ? rekananAlamat : new ArrayList<String>();

            rekananAlamat.removeAll(Collections.singleton(null));

            if (rekananId.size() != 0) {

                int i = 0;

                for (Long rkn_id : rekananId) {

                    RealisasiPenyediaSwakelola realisasiPenyediaSwakelola = new RealisasiPenyediaSwakelola();

                    realisasiPenyediaSwakelola.rsk_id = realisasiId;
                    realisasiPenyediaSwakelola.rkn_id = rkn_id;
                    realisasiPenyediaSwakelola.rp_nama_rekanan = rekananNama.get(i);
                    realisasiPenyediaSwakelola.rp_npwp_rekanan = rekananNpwp.get(i);
                    realisasiPenyediaSwakelola.rp_email_rekanan = rekananEmail.get(i);
                    realisasiPenyediaSwakelola.rp_telp_rekanan = rekananTelepon.get(i);
                    realisasiPenyediaSwakelola.rp_alamat_rekanan = rekananAlamat.get(i);
                    realisasiPenyediaSwakelola.save();

                    i++;

                }

                flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");

            } else {
                flash.error("<i class=\"fa fa-warning\"></i>&nbsp;&nbsp;Anda belum memilih Penyedia. Silakan pilih Penyedia terlebih dahulu !");
            }

        }

        viewPilihPenyedia(realisasiId, true, npwp, nama, kabupatenId, propinsiId, jenisIjin, repoId);

    }

    @AllowAccess({Group.PPK})
    public static void hapusPenyedia(Long realiasiPenyediaId) throws ExecutionException, InterruptedException {

        RealisasiPenyediaSwakelola realisasiPenyediaSwakelola = RealisasiPenyediaSwakelola.findById(realiasiPenyediaId);

        JenisRealisasiSwakelola jenisRealisasiSwakelola = null;

        if (realisasiPenyediaSwakelola != null) {

            jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiPenyediaSwakelola.rsk_id);

            realisasiPenyediaSwakelola.delete();

            flash.success("Berhasil hapus peserta");
        }

        editJenisRealisasi(jenisRealisasiSwakelola.lls_id, jenisRealisasiSwakelola.rsk_id);

    }


    //pilih penyedia
    @AllowAccess({Group.PPK})
    public static void viewPilihNonPenyedia(Long realisasiId, Boolean search, String npwp, String nama) throws ExecutionException, InterruptedException, UnsupportedEncodingException {
        JenisRealisasiSwakelola jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiId);
        List<Map<String, String>> rekananList = new ArrayList<>();

        String rekananListJson = null;

        search = search == null ? false : search;

        if (realisasiId != null) {
            if ((npwp != null && !npwp.isEmpty()) || (nama != null && !nama.isEmpty())) {
                rekananList = NonRekananSikap.getListNonPenyediaFromSikap(npwp, nama, jenisRealisasiSwakelola.rsk_id);
                rekananListJson = CommonUtil.toJson(rekananList);
            } else{
                if(search)
                    flash.error("Silakan masukkan/pilih terlebih dahulu salah satu parameter pencarian");
            }
            renderArgs.put("rekananListJson", rekananListJson);
            //else if (kabupatenId == null && ((npwp != null && !npwp.equals("")) || (nama != null && !nama.equals(""))))
            //    flash.error("Silakan Pilih Kabupaten Terlebih Dahulu");
            //rekananList = Rekanan.getRekananByNpwp(npwp);

            if ((rekananList == null || rekananList.size() == 0) && ((nama != null && !nama.isEmpty()) || (npwp != null && !npwp.isEmpty()))) {
                rekananList = new ArrayList<>();
                flash.error("<i class=\"fa fa-close\"></i>&nbsp;&nbsp;Penyedia tidak di temukan/koneksi ke SIKaP bermasalah.");
            }

        }
        renderArgs.put("jenisRealisasiSwakelola", jenisRealisasiSwakelola);
        renderArgs.put("rekananList", rekananList);
        renderArgs.put("npwp", npwp);
        renderArgs.put("nama", nama);
        renderTemplate("swakelola/realisasi/realisasi-swakelola-pilih-nonpenyedia.html");

    }



    @AllowAccess({Group.PPK})
    public static void simpanPilihNonPenyedia(Long realisasiId, String npwp, String nama, List<Long> rekananId, List<String> rekananNama,
                                              List<String> rekananNpwp, List<String> rekananEmail, List<String> rekananTelepon,
                                              List<String> rekananAlamat)
            throws ExecutionException, InterruptedException, UnsupportedEncodingException {

        JenisRealisasiSwakelola jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiId);

        if(jenisRealisasiSwakelola != null) {

            jenisRealisasiSwakelola.is_penyedia = false;

            jenisRealisasiSwakelola.save();

            //hapus data penyedia
            RealisasiPenyediaSwakelola.deleteByRealisasi(jenisRealisasiSwakelola.rsk_id);

            rekananId = rekananId != null ? rekananId : new ArrayList<Long>();

            rekananId.removeAll(Collections.singleton(null));

            rekananNama = rekananNama != null ? rekananNama : new ArrayList<String>();

            rekananNama.removeAll(Collections.singleton(null));

            rekananNpwp = rekananNpwp != null ? rekananNpwp : new ArrayList<String>();

            rekananNpwp.removeAll(Collections.singleton(null));

            rekananEmail = rekananEmail != null ? rekananEmail : new ArrayList<String>();

            rekananEmail.removeAll(Collections.singleton(null));

            rekananTelepon = rekananEmail != null ? rekananTelepon : new ArrayList<String>();

            rekananTelepon.removeAll(Collections.singleton(null));

            rekananAlamat = rekananAlamat != null ? rekananAlamat : new ArrayList<String>();

            rekananAlamat.removeAll(Collections.singleton(null));

            if (rekananId.size() != 0) {

                int i = 0;

                for (Long rkn_id : rekananId) {

                    RealisasiNonPenyediaSwakelola realisasiNonPenyediaSwakelola = new RealisasiNonPenyediaSwakelola();

                    realisasiNonPenyediaSwakelola.rsk_id = realisasiId;
                    realisasiNonPenyediaSwakelola.rkn_id = rkn_id;
                    realisasiNonPenyediaSwakelola.rn_nama_rekanan = rekananNama.get(i);
                    realisasiNonPenyediaSwakelola.rn_npwp_rekanan = rekananNpwp.get(i);
                    realisasiNonPenyediaSwakelola.rn_email_rekanan = rekananEmail.get(i);
                    realisasiNonPenyediaSwakelola.rn_telp_rekanan = rekananTelepon.get(i);
                    realisasiNonPenyediaSwakelola.rn_alamat_rekanan = rekananAlamat.get(i);
                    realisasiNonPenyediaSwakelola.save();

                    i++;

                }

                flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");

            } else {
                flash.error("<i class=\"fa fa-warning\"></i>&nbsp;&nbsp;Anda belum memilih Penyedia. Silakan pilih Penyedia terlebih dahulu !");
            }

        }

        viewPilihNonPenyedia(realisasiId, true, npwp, nama);

    }

    @AllowAccess({Group.PPK})
    public static void hapusNonPenyedia(Long realiasiNonPenyediaId) throws ExecutionException, InterruptedException {
        RealisasiNonPenyediaSwakelola realisasiNonPenyediaSwakelola = RealisasiNonPenyediaSwakelola.findById(realiasiNonPenyediaId);
        JenisRealisasiSwakelola jenisRealisasiSwakelola = null;
        if (realisasiNonPenyediaSwakelola != null) {
            jenisRealisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiNonPenyediaSwakelola.rsk_id);
            realisasiNonPenyediaSwakelola.delete();
            flash.success("Berhasil hapus peserta");
        }
        editJenisRealisasi(jenisRealisasiSwakelola.lls_id, jenisRealisasiSwakelola.rsk_id);
    }

    @AllowAccess({Group.PPK})
    public static void viewTambahPenyediaNonSikap(Long realisasiId, Long swkId){
        JenisRealisasiSwakelola realiasasiSwakelola = JenisRealisasiSwakelola.findById(realisasiId);
        renderArgs.put("realiasasiSwakelola", realiasasiSwakelola);
        renderArgs.put("swkId", swkId);
        renderTemplate("swakelola/realisasi/realisasi-swakelola-tambah-nonsikap.html");
    }

    @AllowAccess({Group.PPK})
    public static void simpanTambahNonSikap(Long swakelolaId, Long realisasiId, FormTambahPenyediaNonSikap form){
        JenisRealisasiSwakelola realisasiSwakelola = JenisRealisasiSwakelola.findById(realisasiId);
        if(realisasiSwakelola != null){
            realisasiSwakelola.is_penyedia = false;
            realisasiSwakelola.save();
            validation.valid(form);
            if(validation.hasErrors()){
                flash.error("Gagal menambahkan penyedia.");
                renderArgs.put("swakelolaId", swakelolaId);
                renderArgs.put("realisasiId", realisasiId);
                renderArgs.put("realisasiSwakelola", realisasiSwakelola);
                renderArgs.put("form", form);
                renderTemplate("swakelola/realisasi/realisasi-swakelola-tambah-nonsikap.html");
            }
            RealisasiNonPenyediaSwakelola realisasiNonPenyediaSwakelola = new RealisasiNonPenyediaSwakelola();

            realisasiNonPenyediaSwakelola.rsk_id = realisasiId;
            realisasiNonPenyediaSwakelola.rkn_id = 0l;
            realisasiNonPenyediaSwakelola.rn_nama_rekanan = form.nama;
            realisasiNonPenyediaSwakelola.rn_npwp_rekanan = form.npwp;
            realisasiNonPenyediaSwakelola.rn_email_rekanan = form.email;
            realisasiNonPenyediaSwakelola.rn_telp_rekanan = form.telp;
            realisasiNonPenyediaSwakelola.rn_alamat_rekanan = form.alamat;
            realisasiNonPenyediaSwakelola.save();
            flash.success("<i class=\"fa fa-check\"></i>&nbsp;&nbsp;Berhasil simpan draft penyedia");
        }
        editJenisRealisasi(swakelolaId, realisasiId);
    }


}
