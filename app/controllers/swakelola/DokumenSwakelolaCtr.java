package controllers.swakelola;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import ext.DokumenType;
import ext.FormatUtils;
import models.agency.DaftarKuantitas;
import models.common.UploadInfo;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.nonlelang.Dok_swakelola;
import models.nonlelang.DokumenLainSwakelola;
import models.nonlelang.JenisDokumenSwakelola;
import models.nonlelang.Swakelola_seleksi;
import models.secman.Group;
import play.Logger;
import play.data.validation.Valid;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Lambang on 4/26/2017.
 */
public class DokumenSwakelolaCtr extends BasicCtr {

    @AllowAccess({Group.PP})
    public static void hps(Long id){
        otorisasiDataSwakelola(id); // check otorisasi data lelang
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(id);
        Dok_swakelola dok_swakelola = Dok_swakelola.findBy(swakelola.lls_id);

        boolean editable = true;

        String data = "[]";

        boolean fixed = true;

        DaftarKuantitas dk = null;

        if (dok_swakelola != null) {
            dk = dok_swakelola.dkh;
            if(dk == null && !swakelola.lls_status.isDraft())
                dk = dok_swakelola.getRincianHPS();
        }
        if(dk == null)
            dk = new DaftarKuantitas();
        if (dk != null) {
            fixed = dk.fixed;
            if (dk.items != null)
                data = CommonUtil.toJson(dk.items);
        }
        renderArgs.put("swakelola", swakelola);
        renderArgs.put("dok_swakelola", dok_swakelola);
        renderArgs.put("editable", editable);
        renderArgs.put("data", data);
        renderArgs.put("fixed", fixed);

        renderTemplate("swakelola/dokumen/form-swakelola-hps.html");

    }

    @AllowAccess({Group.PP})
    public static void hpsSubmit(Long id, String data, boolean fixed){
        checkAuthenticity();
        otorisasiDataSwakelola(id); // check otorisasi data lelang
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(id);
        Dok_swakelola dok_swakelola = Dok_swakelola.findBy(swakelola.lls_id);
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            if(dok_swakelola == null)
                dok_swakelola = Dok_swakelola.findNCreateBy(swakelola.lls_id);
            dok_swakelola.simpanHps(swakelola, data, fixed);
            result.put("result", "ok");
        } catch (Exception e) {
            result.put("result", e.getMessage());
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PP })
    public static void editJenisDokumen(Long swakelolaId, Long jenisDokumenId) {
        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swakelolaId);
        renderArgs.put("swakelola", swakelola);
        if (jenisDokumenId != null) {
            JenisDokumenSwakelola jenisDokumenSwakelola = JenisDokumenSwakelola.findById(jenisDokumenId);
            renderArgs.put("JenisDokumenSwakelola", jenisDokumenSwakelola);
            renderTemplate("swakelola/dokumen/edit-jenis-dokumen.html");
        }
        render("swakelola/dokumen/edit-jenis-dokumen.html");
    }

    @AllowAccess({ Group.PP })
    public static void viewJenisDokumen(Long swakelolaId, Long jenisDokumenId) {
        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(swakelolaId);
        JenisDokumenSwakelola jenisDokumenSwakelola = JenisDokumenSwakelola.findById(jenisDokumenId);
        renderArgs.put("swakelola", swakelola);
        renderArgs.put("jenisDokumenSwakelola", jenisDokumenSwakelola);
        renderTemplate("swakelola/dokumen/view-jenis-dokumen.html", swakelola, jenisDokumenSwakelola);

    }


    @AllowAccess({ Group.PP })
    public static void simpanJenisDokumen(Long swakelolaId, JenisDokumenSwakelola jenisDokumenSwakelola,
                                          @Valid @DokumenType File attachment, String hapus1) throws Exception {

        checkAuthenticity();

        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang

        File file = attachment != null ? attachment : null;

        jenisDokumenSwakelola.lls_id = swakelolaId;

        if (jenisDokumenSwakelola.jds_id != null){

            JenisDokumenSwakelola jenisDok = JenisDokumenSwakelola.findById(jenisDokumenSwakelola.jds_id);

            jenisDok.jds_jenis = jenisDokumenSwakelola.jds_jenis;
            jenisDok.jds_nomor_dokumen = jenisDokumenSwakelola.jds_nomor_dokumen;
            jenisDok.jds_nama_dokumen = jenisDokumenSwakelola.jds_nama_dokumen;
            jenisDok.jds_tanggal_dokumen = jenisDokumenSwakelola.jds_tanggal_dokumen;

            jenisDokumenSwakelola = jenisDok;

        }

        if (file == null) {
            if (jenisDokumenSwakelola.jds_id_attachment != null && hapus1 == null) {
                BlobTable.delete("blb_id_content=?", jenisDokumenSwakelola.jds_id_attachment);
                jenisDokumenSwakelola.jds_id_attachment = null;
            }
        } else {
            if (file != null) {
                BlobTable bt = null;
                if(jenisDokumenSwakelola.jds_id_attachment == null)
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
                else
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, jenisDokumenSwakelola.jds_id_attachment);

                jenisDokumenSwakelola.jds_id_attachment = bt.blb_id_content;
            }

        }

        jenisDokumenSwakelola.save();

        flash.success("Berhasil simpan Jenis Dokumen");

        editJenisDokumen(swakelolaId, jenisDokumenSwakelola.jds_id);

    }

    @AllowAccess({ Group.PP })
    public static void hapusJenisDokumen(Long swakelolaId, Long jenisDokumenId) {

        otorisasiDataSwakelola(swakelolaId); // check otorisasi data lelang

        if (jenisDokumenId != null) {

            JenisDokumenSwakelola jenisDokumenSwakelola = JenisDokumenSwakelola.findById(jenisDokumenId);

            jenisDokumenSwakelola.delete();

            flash.success("Berhasil Hapus JenisDokumen");
        }

        SwakelolaCtr.edit(swakelolaId);

    }

    @AllowAccess({Group.REKANAN, Group.PP, Group.PPK})
    public static void uploadInformasiLainnya(Long id, File file) {
        otorisasiDataSwakelola(id);
        Map<String, Object> result = new HashMap<String, Object>(1);
        try {
            UploadInfo info = DokumenLainSwakelola.simpan(id, DokumenLainSwakelola.JenisDokLainSwakelola.INFORMASI_LAINNYA.value, file);
            List<UploadInfo> files = new ArrayList<UploadInfo>();
            files.add(info);
            result.put("files", files);
        }catch(Exception e) {
            Logger.error("Kesalahan saat upload informasi lainnya: detail %s", e.getMessage());
            e.printStackTrace();
        }
        renderJSON(result);
    }

    @AllowAccess({ Group.PP })
    public static void hapusInformasiLainnya(Long id) {

        DokumenLainSwakelola dokumenLainSwakelola = DokumenLainSwakelola.findById(id);

        if (dokumenLainSwakelola != null) {

            BlobTable blob = BlobTableDao.getLastById(dokumenLainSwakelola.dls_id_attachment);

            if (blob != null)
                blob.delete();

            dokumenLainSwakelola.delete();
        }

    }

    @AllowAccess({ Group.PP })
    public static void viewRincianHps(Long swakelolaId){
        Dok_swakelola dokSwakelola = Dok_swakelola.findBy(swakelolaId);
        JsonParser parser = new JsonParser();
        Double total = 0d;
        List<Map<String, Object>> items = new ArrayList<>();
        if(dokSwakelola.dsw_dkh != null) {
            JsonObject jsonObject = parser.parse(dokSwakelola.dsw_dkh).getAsJsonObject();
            total = jsonObject.get("total").getAsDouble();
            items = formatRincianHps(swakelolaId);
        }
        renderArgs.put("items", items);
        renderArgs.put("total", total);
        render("swakelola/dokumen/viewRincianHps.html");

    }

    public static  List<Map<String, Object>> formatRincianHps(Long swakelolaId){

        Dok_swakelola dokSwakelola = Dok_swakelola.findBy(swakelolaId);

        JsonParser parser = new JsonParser();

        JsonObject jsonObject = parser.parse(dokSwakelola.dsw_dkh).getAsJsonObject();

        JsonArray jsonArray = jsonObject.get("items").getAsJsonArray();

        List<Map<String, Object>> items = new ArrayList<>();

        for (int i = 0; i < jsonArray.size(); i++){

            Map<String, Object> item = new HashMap<>();

            JsonObject json = jsonArray.get(i).getAsJsonObject();

            item.put("item", json.get("item").getAsString());
            item.put("unit", json.get("vol").getAsDouble());
            item.put("vol", json.get("unit").getAsString());
            item.put("unit2", json.get("vol2").getAsDouble());
            item.put("vol2", json.get("unit2").getAsString());
            item.put("harga", FormatUtils.formatDesimal2(json.get("harga").getAsDouble()));
            item.put("total_harga", FormatUtils.formatDesimal2(json.get("total_harga").getAsDouble()));
            item.put("keterangan", json.get("keterangan").getAsString());
            item.put("pajak", json.get("pajak").getAsDouble());

            items.add(item);

        }

        return items;

    }

}
