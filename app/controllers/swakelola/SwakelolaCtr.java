package controllers.swakelola;

import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.AnggaranSwakelola;
import models.agency.Paket_swakelola;
import models.agency.Paket_swakelola_lokasi;
import models.agency.Satuan_kerja;
import models.common.Active_user;
import models.common.Kategori;
import models.jcommon.util.CommonUtil;
import models.lelang.Lelang_seleksi;
import models.nonlelang.Dok_swakelola;
import models.nonlelang.JenisRealisasiSwakelola;
import models.nonlelang.Swakelola_seleksi;
import models.nonlelang.nonSpk.HistoryUbahTanggalSwakelola;
import models.secman.Group;
import play.Logger;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SwakelolaCtr extends BasicCtr {
	
	/**
     * Fungsi {@code index} digunakan untuk menampilkan halaman riwayat tender
     * e-procurement LPSE
     */
    public static void index(Integer selectKategori) {
        Kategori kategori = null;
        if (selectKategori != null)
            kategori = Kategori.findById(selectKategori);
        Kategori[] kategoriList = Kategori.all;
        renderArgs.put("kategori", kategori);
        renderArgs.put("kategoriList", kategoriList);
        renderTemplate("swakelola/swakelola.html");
    }
    
    @AllowAccess({Group.PP, Group.PPK})
    public static void view(Long id) {
    	otorisasiDataSwakelola(id);
		Swakelola_seleksi swakelola = Swakelola_seleksi.findById(id);
		renderArgs.put("swakelola", swakelola);
        renderArgs.put("list_jenis_realisasi", swakelola.getJenisRealisasi());
        Dok_swakelola dok_swakelola = Dok_swakelola.findBy(swakelola.lls_id);
        renderArgs.put("dok_swakelola", dok_swakelola);
        renderArgs.put("informasi_lainnya", swakelola.getInformasiLainnya());
        renderArgs.put("disabled", true);

        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah((JenisRealisasiSwakelola.getJumlahNilaiRealisasi(id)));

        int jumlahPerubahanTanggal = swakelola.getHistoryUbahTanggalSwakelolaList().size();
        renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);
        renderArgs.put("jumlahPerubahanTanggal", jumlahPerubahanTanggal);
    	renderTemplate("swakelola/swakelola-view.html");
    }
    
    @AllowAccess({Group.PPK})
    public static void edit(Long id) {
        Active_user active_user = Active_user.current();
        otorisasiDataSwakelola(id); // check otorisasi data swakelola
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(id);
//        boolean draft = swakelola.lls_status.isDraft();
        Paket_swakelola paket = swakelola.getSwakelola();
        renderArgs.put("active_user", active_user);
        renderArgs.put("swakelola", swakelola);
        renderArgs.put("draft", swakelola.lls_status.isDraft());
        renderArgs.put("paket", paket);
        // Validasi jika paket belum ada data lokasi pekerjaan
        if (CommonUtil.isEmpty(paket.getPaketLokasi())) {
            flash.error(Messages.get("flash.mabmdlppdp"));
            BerandaCtr.PpkSwakelola();
        }
        Dok_swakelola dok_swakelola = Dok_swakelola.findNCreateBy(swakelola.lls_id);
        List<JenisRealisasiSwakelola> jenisRealisasiList = new ArrayList<>();
        String totalNilaiRealisasi = FormatUtils.formatCurrencyRupiah((JenisRealisasiSwakelola.getJumlahNilaiRealisasi(id)));
        renderArgs.put("dok_swakelola", dok_swakelola);
        if (dok_swakelola != null)
            jenisRealisasiList = swakelola.getJenisRealisasi();
        renderArgs.put("jenisRealisasiList", jenisRealisasiList);
        int jumlahPerubahanTanggal = swakelola.getHistoryUbahTanggalSwakelolaList().size();
        renderArgs.put("totalNilaiRealisasi", totalNilaiRealisasi);
        renderArgs.put("jumlahPerubahanTanggal", jumlahPerubahanTanggal);
        renderTemplate("swakelola/swakelola-edit.html");

    }

    @AllowAccess({Group.PPK})
    public static void hapus(Long id) {
        Paket_swakelola paket = Paket_swakelola.findById(id);
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(paket.swk_id);
        otorisasiDataPaketSwakelola(paket);// check otorisasi data swakelola
        if (paket.getPaketPpk() != null && !paket.pkt_status.isDraft()) {
            flash.error("Maaf, Paket yang sedang aktif tidak dapat dihapus!");
            PaketSwakelolaCtr.index(null, null);
        }
//        if (!swakelola.getSwakelola().pkt_status.isDraft()) {
//            flash.error("Maaf, Paket yang sedang aktif tidak dapat dihapus!");
//            PaketSwakelolaCtr.index(null, null);
//        }

        try {
            Swakelola_seleksi.hapusSwakelola(paket.swk_id);
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.info("Error delete " + ex.getLocalizedMessage());
            flash.error("Gagal menghapus paket " + id);
            edit(paket.swk_id);
        }

        PaketSwakelolaCtr.index(null, null);
    }

    @AllowAccess({Group.PPK})
    public static void simpan(Swakelola_seleksi swakelola, Integer swakelolaId, boolean ubahTanggalSelesai, String alasan) throws Exception {

        checkAuthenticity();

//        otorisasiDataSwakelola(swakelola.lls_id); // check otorisasi data lelang

        Date oldTanggalSelesai = null;

        if (swakelola.lls_id != null){

           // validation.max(Double.valueOf(swakelola.lls_nilai_gabungan), 100).key("lls_nilai_gabungan").message("Nilai Gabungan tidak boleh lebih dari 100 persen");

            Swakelola_seleksi swakelola_seleksi = Swakelola_seleksi.findById(swakelola.lls_id);

            if(!swakelola_seleksi.getSwakelola().pkt_status.isSelesaiLelang())
                validation.valid(swakelola);

            if((ubahTanggalSelesai && swakelola.lls_tanggal_paket_selesai == null)
                    || (swakelola_seleksi.lls_tanggal_paket_selesai == null && swakelola.lls_tanggal_paket_selesai == null)) {
                validation.addError("swakelola.lls_tanggal_paket_selesai", "Tanggal Paket Selesai wajib diisi.", "var");
            } else {
                if (ubahTanggalSelesai && CommonUtil.isEmpty(alasan))
                    validation.addError("swakelola.lls_tanggal_paket_selesai", "Alasan Ubah Tanggal Paket Selesai wajib diisi.", "var");
            }

            if (validation.hasErrors()) {

                validation.keep();

                params.flash();

                flash.error("Data gagal tersimpan, silakan periksa kembali inputan Anda.");

                edit(swakelola.lls_id);

            }

            oldTanggalSelesai = swakelola_seleksi.lls_tanggal_paket_selesai;

            if (!swakelola_seleksi.getSwakelola().pkt_status.isSelesaiLelang()) {
                swakelola_seleksi.lls_jangka_waktu_pelaksanaan = swakelola.lls_jangka_waktu_pelaksanaan;
                swakelola_seleksi.lls_nilai_gabungan = swakelola.lls_nilai_gabungan;
                swakelola_seleksi.lls_uraian_pekerjaan = swakelola.lls_uraian_pekerjaan;
                if (swakelola_seleksi.lls_tanggal_paket_mulai == null)
                    swakelola_seleksi.lls_tanggal_paket_mulai = newDate();
                if (swakelola.lls_tanggal_paket_selesai != null)
                    swakelola_seleksi.lls_tanggal_paket_selesai = swakelola.lls_tanggal_paket_selesai;
            } else {
                if (swakelola.lls_tanggal_paket_selesai != null)
                    swakelola_seleksi.lls_tanggal_paket_selesai = swakelola.lls_tanggal_paket_selesai;
            }


            swakelola = swakelola_seleksi;

        }

       /* File file = attachment != null ? attachment : null;

        if (file == null) {
            if (dok_swakelola.dsw_bast_attachment != null && hapus1 == null) {
                BlobTable.delete("blb_id_content=?", dok_swakelola.dsw_bast_attachment);
                dok_swakelola.dsw_bast_attachment = null;
            }
        } else {
            if (file != null) {
                BlobTable bt = null;
                if(dok_swakelola.dsw_bast_attachment == null)
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file);
                else
                    bt = BlobTableDao.saveFile(BlobTable.ARCHIEVE_MODE.ARCHIEVE, file, dok_swakelola.dsw_bast_attachment);

                dok_swakelola.dsw_bast_attachment = bt.blb_id_content;
            }

        }*/
        swakelola.tipe_swakelola = swakelolaId;
        swakelola.save();


        if(!CommonUtil.isEmpty(alasan)) {
            HistoryUbahTanggalSwakelola historyUbahTanggalSwakelola = new HistoryUbahTanggalSwakelola();

            historyUbahTanggalSwakelola.lls_id = swakelola.lls_id;
            historyUbahTanggalSwakelola.uts_tanggal_edit = newDate();
            historyUbahTanggalSwakelola.uts_tanggal_asli = oldTanggalSelesai;
            historyUbahTanggalSwakelola.uts_tanggal_ubah = swakelola.lls_tanggal_paket_selesai;
            historyUbahTanggalSwakelola.uts_keterangan = alasan;

            historyUbahTanggalSwakelola.save();
        }

        Paket_swakelola paket = swakelola.getSwakelola();

        if(paket.pkt_status.isDraft())
            paket.pkt_status = Paket_swakelola.StatusPaketSwakelola.SEDANG_LELANG;

        if(swakelola.lls_id != null) {
            if (ubahTanggalSelesai && (swakelola.lls_tanggal_paket_selesai != oldTanggalSelesai) && paket.pkt_status.isSelesaiLelang())
                paket.pkt_status = Paket_swakelola.StatusPaketSwakelola.SEDANG_LELANG;
            else if(ubahTanggalSelesai && swakelola.lls_tanggal_paket_selesai.before(newDate()) && paket.pkt_status.isSedangLelang())
                paket.pkt_status = Paket_swakelola.StatusPaketSwakelola.SELESAI_LELANG;
            else if (!ubahTanggalSelesai && swakelola.lls_tanggal_paket_selesai.before(newDate()))
                paket.pkt_status = Paket_swakelola.StatusPaketSwakelola.SELESAI_LELANG;
        }


        paket.save();

        flash.success("Paket berhasil di simpan");

        edit(swakelola.lls_id);

    }

    public static void pengumumanSwakelola(Long id) {
        Swakelola_seleksi swakelola = Swakelola_seleksi.findById(id);
        Lelang_seleksi lelang = Lelang_seleksi.findByPaket(id);
        Paket_swakelola paket = Paket_swakelola.findBy(id);
        List<AnggaranSwakelola> anggaranList = AnggaranSwakelola.findByPaket(swakelola.swk_id);
        List<Paket_swakelola_lokasi> lokasiList = Paket_swakelola_lokasi.find("swk_id=?", swakelola.swk_id).fetch();
        renderArgs.put("lelang", lelang);
        renderArgs.put("swakelola", swakelola);
        renderArgs.put("paket", paket);
        renderArgs.put("anggaranList", anggaranList);
        renderArgs.put("lokasiList", lokasiList);
        renderTemplate("swakelola/pengumuman-swakelola.html");
    }

    @AllowAccess({Group.PPK})
    public static void viewHistoryUbahTanggalSwakelola(Long id){
        Swakelola_seleksi swakelolaSeleksi = Swakelola_seleksi.findById(id);
        List<HistoryUbahTanggalSwakelola> historyUbahTanggalSwakelolaList = swakelolaSeleksi.getHistoryUbahTanggalSwakelolaList();
        renderArgs.put("swakelolaSeleksi", swakelolaSeleksi);
        renderArgs.put("historyUbahTanggalSwakelolaList", historyUbahTanggalSwakelolaList);
        renderTemplate("swakelola/view-history-ubah-tanggal-swakelola.html");
    }
}
