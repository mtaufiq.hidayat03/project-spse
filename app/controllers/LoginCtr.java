package controllers;

import controllers.UserCtr.LoginResult;
import controllers.security.AllowAccess;
import ext.AesDecryptBinder;
import models.agency.*;
import models.common.Active_user;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.Usergroup;
import models.secman.Usrsession;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.data.binding.As;
import play.data.validation.Validation.ValidationResult;
import play.i18n.Messages;
import play.mvc.Http.Cookie;

import java.util.concurrent.ExecutionException;

/**
 * Controler untuk handle terkait user login dan logout
 * @author Arief Ardiyansah
 *
 */
public class LoginCtr extends BasicCtr {
//	
//
//	/**Override Controller.checkAuthenticity.Untuk login*/
//	protected static void checkAuthenticityLogin() {
//        if(Scope.Params.current().get("authenticityToken") == null || !Scope.Params.current().get("authenticityToken").equals(Scope.Session.current().getAuthenticityToken())) {
//            PublikCtr.index();
//        }
//    }

	public static void loginSubmit(@As(binder = AesDecryptBinder.class) String txtUserId, @As(binder = AesDecryptBinder.class) String txtPassword, boolean isRekanan, String txtKodeKeamanan, String randomID) throws InterruptedException, ExecutionException {
		// simpan di cookies isRekanan
		Cookie cookie = new Cookie("isRekanan",  String.valueOf(isRekanan));
		cookie.path = Play.ctxPath+ '/';
		response.cookies.put("isRekanan", cookie);

//		String authenticityToken = Scope.Params.current().get("authenticityToken");
		// jika pakai JMeter maka tidak perlu cek authenticityToken
//		if (authenticityToken != null && !authenticityToken.equals("Apache JMeter"))
			/**
			 * Tidak bisa menggunakan Controller.checkAuthenticity karena pesan
			 * errornya kurang friendly maka buatkan method baru untuk UserCtr
			 */
		checkAuthenticity();
		validation.required(txtUserId).message("Wajib diisi");
		validation.required(txtPassword).message("Wajib diisi");
		if (validation.hasErrors()) {
			Logger.debug("error validasi : ");
			validation.keep();
			params.flash();
			PublikCtr.index();
		} else {
			txtUserId = txtUserId.trim().toUpperCase();
			txtPassword = DigestUtils.md5Hex(txtPassword);
			// check capca
			if (randomID != null && txtKodeKeamanan != null) {
				String kapca = Cache.get(randomID, String.class);
				Cache.delete(randomID);
				if (kapca != null)// mungkin null karena expires dari cache
				{
					if (!kapca.equals(txtKodeKeamanan)) {
						flash.error("Kode Keamanan yang dimasukkan salah atau tidak berlaku.");
						PublikCtr.index();
					}

				}
			}
			LoginResult login_result  = null;
			if(isRekanan) {
				login_result = Rekanan.doLogin(txtUserId, txtPassword, request.remoteAddress); // aksi login
			} else{
				login_result = Pegawai.doLogin(txtUserId, txtPassword, request.remoteAddress); // aksi login
			}

			if (login_result == LoginResult.SUCCESS) {
				// login sukses, hapus login counter
//				session.put(SESSION_USERID, txtUserId);
				Usrsession usrsession = Usrsession.setWaktuLogin(txtUserId, request.remoteAddress, isRekanan); // rekam data session pengguna
				response.removeCookie(PublikCtr.LOGIN_COUNTER_KEY, "/");
				Group group = isRekanan ? Group.REKANAN : Usergroup.findGroupByUser(txtUserId); // role bisa lebih dari satu
				if (group == null) // ada kalanya tidak memiliki role
				{
					flash.error("User ID Anda tidak memiliki hak akses apapun.");
					PublikCtr.index();
				}
				/**
				 * Login Dipisahkan antara rekanan/bukan karena ada kemungkinan
				 * userId yang sama antara rekanan/non rekanan setelah penerapan
				 * ADP Jika tidak ada ADP maka userId pasti unik di satu LPSE
				 * baik rekanan maupun non rekanan
				 */
				Active_user active_user = new Active_user();
				active_user.id = usrsession.sessionid;
				active_user.sessionid = session.getId();
				active_user.userid = txtUserId;
				active_user.name = txtUserId;
				active_user.remoteaddress = request.remoteAddress;
				active_user.logintime = usrsession.sessiontime.getTime();
				active_user.group = group; // set grup di session
				//active_user.setAmsRelatedData();
				if (isRekanan) {
					Rekanan rekanan = Rekanan.findByNamaUser(txtUserId);
					if (rekanan == null) { // user login bukan merupakan rekanan
						// Hapus session pengguna yang sudah terlanjur ada pada database
						Usrsession.hapusUsrsessionTerakhir(txtUserId);
						clearUserSession();
						flash.put("error", "User ID tidak terdaftar sebagai Penyedia.");
						PublikCtr.index();
					} else {
						// otorisasi rekanan terkait ADP
						active_user.rekananId = rekanan.rkn_id;
						active_user.name = rekanan.rkn_nama;
						// 4.1.2 integrasi data penyedia
						// mitigasi ketika status_migrasi belum diupdate via sql
						if(rekanan.status_migrasi == null) {
							rekanan.status_migrasi = 0;
							rekanan.save();
						}
						active_user.statusMigrasi = rekanan.status_migrasi;
						active_user.setAmsRelatedData();
						active_user.save();

						BerandaCtr.index(); // redirect ke halaman beranda Rekanan
					}
				} else {
					if (group == Group.REKANAN) { // jika ternyata user memiliki role REKANAN
						Usrsession.hapusUsrsessionTerakhir(txtUserId); // Hapus session pengguna yang sudah terlanjur ada pada database
						clearUserSession();
						flash.put("error", "User ID tidak terdaftar sebagai Non-Penyedia.");
						PublikCtr.index();

					} else if (group != Group.ADM_PPE) { // ADM_PPE tidak boleh login lewat halaman login umum
						redirectBerandaPegawai(active_user); // redirect ke halaman beranda masing-masing pegawai LPSE sesuai role-nya
					} else {
						Usrsession.hapusUsrsessionTerakhir(txtUserId); // Hapus session pengguna yang sudah terlanjur ada database
						session.clear();
						forbidden("Admin PPE harus login melalui halaman khusus.");
					}
				}
			}
			if (login_result == LoginResult.DISABLED_USER) {
				forbidden(login_result.getCaption());
			} else {
				/**
				 * JIka error maka counter untuk login di-increase
				 *
				 */
				Cookie loginCookie = request.cookies.get(PublikCtr.LOGIN_COUNTER_KEY);
				if (loginCookie == null) {
					loginCookie = new Cookie(PublikCtr.LOGIN_COUNTER_KEY, String.valueOf(0));
				}
				loginCookie.maxAge = 60*60;
				loginCookie.path = Play.ctxPath+ '/';
				int value = Integer.parseInt(loginCookie.value) + 1;
				loginCookie.value = String.valueOf(value);
				response.cookies.put(PublikCtr.LOGIN_COUNTER_KEY, loginCookie);

				if(login_result != null) {
					Logger.debug("Error otentikasi :%s %s", txtUserId, login_result);
					flash.error(login_result.getCaption());
				}
				PublikCtr.index();
			}
		}
	}

	/**
	 * otorisasi khusus untuk admin lpse (admin PPE)
	 *
	 * @param txtUserId
	 * @param txtPassword
	 * @param hidRandomID
	 * @param txtCode
	 */
	public static void loginAdmin(@As(binder = AesDecryptBinder.class) String txtUserId, @As(binder = AesDecryptBinder.class) String txtPassword, String hidRandomID, String txtCode) {
		checkAuthenticity();
		validation.required(txtUserId).message(Messages.get("login.uid_tdk_ksng"));
		validation.required(txtPassword).message(Messages.get("login.pwd_tblh_ksng"));
		validation.required(txtCode).message(Messages.get("login.kd_hrs"));
		ValidationResult result = null;
		if (!StringUtils.isNotEmpty(txtUserId)) {
			result = validation.equals(txtUserId, "-").key("txtUserId").message(Messages.get("login.prhtn_psn_rr"));
		}

		if (!StringUtils.isNotEmpty(txtPassword)) {
			result = validation.equals(txtPassword, "-").key("txtPassword")
					.message(Messages.get("login.prhtn_psn_rr")+"!");
		}

		if (!StringUtils.isNotEmpty(txtCode)) {
			result = validation.equals(txtCode, "-").key("txtCode").message(Messages.get("login.prhtn_psn_rr"));
		} else if (hidRandomID != null && StringUtils.isNotEmpty(txtCode)) {
			// WARN: Capctha case sensitive
			result = validation.equals(txtCode, Cache.get(hidRandomID)).key("invalid.code")
					.message(Messages.get("login.kd_mskn_slh"));
		}

		if (validation.hasErrors()) {
			if(result != null && result.error != null)
				flash.error(result.error.toString());
			params.flash();
			validation.keep();
			PublikCtr.loginAdmin();
		}

		txtUserId = txtUserId.toUpperCase();
		txtPassword = DigestUtils.md5Hex(txtPassword);
		LoginResult login_result = Pegawai.doLogin(txtUserId, txtPassword, request.remoteAddress); // aksi
																								// login
		Group group = Usergroup.findGroupByUser(txtUserId);
		// untuk kompatibilitas SPSE 3.x maka grup ADMIN disamakan ADM_PPE
		if (login_result == LoginResult.SUCCESS && group == Group.ADM_PPE) {
//			session.put(SESSION_USERID, txtUserId);
			Usrsession usrsession = Usrsession.setWaktuLogin(txtUserId, request.remoteAddress, false); // rekam data session pengguna
			Active_user active_user = new Active_user();
			active_user.id = usrsession.sessionid;
			active_user.sessionid = session.getId();
			active_user.userid = txtUserId;
			active_user.name = txtUserId;
			active_user.logintime = usrsession.sessiontime.getTime();
			Pegawai pegawai = Pegawai.findBy(txtUserId);
			if (pegawai != null) {
				active_user.pegawaiId = pegawai.peg_id;
				active_user.name = pegawai.peg_nama;
			}
			active_user.remoteaddress = request.remoteAddress;
			active_user.group = Group.ADM_PPE;
			active_user.save();
			BerandaCtr.index(); // redirect ke halaman beranda Admin PPE
		} else if (login_result == LoginResult.SUCCESS && group != Group.ADM_PPE) {
			flash.error(login_result.INVALID_USER_PASSWORD.getCaption());
			validation.keep();
			params.flash();
			PublikCtr.loginAdmin();
		} else {
			flash.error(login_result.getCaption());
			validation.keep();
			params.flash();
			PublikCtr.loginAdmin();
		}
	}

	 /**
     * Fungsi {@code redirectBerandaPegawai} merupakan fungsi yang digunakan
     * untuk menampilkan halaman beranda masing-masing pegawai LPSE sesuai
     * dengan <i>role</i>-nya dalam sistem.
     *
     * @param active_user    Active_user (<i>role</i>)
     */
    private static void redirectBerandaPegawai(Active_user active_user) {
        Pegawai pegawai = Pegawai.findBy(active_user.userid);
        if (pegawai != null) { // username ada di tabel pegawai
            active_user.pegawaiId = pegawai.peg_id;
            active_user.name  = pegawai.peg_nama;
            switch (active_user.group) {
                case ADM_AGENCY:
                    Agency agency = Agency.findAgencyByPegawai(pegawai.peg_id);
                    if (agency != null) {
                    	active_user.agencyId = agency.agc_id;
                     } else {
                        clearUserSession(); // bersihkan session pengguna
                        forbidden(Messages.get("login.agncy_tdk_dtmkn"));
                    }
                    break;
                case PANITIA:
					active_user.setAmsRelatedData();
					Anggota_panitia anggota_panitia = Anggota_panitia.find("peg_id = ? order by pnt_id desc", pegawai.peg_id).first(); // ambil kepanitian tahun saat ini, jika ada lebih dari satu kepanitiaan
                    if(anggota_panitia == null){
                        clearUserSession(); // bersihkan session pengguna
                        forbidden(Messages.get("login.tdk_dpt_aks")); // kalau pegawai yang merupakan panitia belum terdaftar di suatu kepanitiaan, tampilkan halaman penolakan akses
                    }
                    break;
                case PPK:
                	Ppk ppk = Ppk.find("peg_id=? ORDER BY ppk_id DESC", pegawai.peg_id).first();
                	if(ppk != null)
                		active_user.ppkId = ppk.ppk_id;
                    break;
                case HELPDESK:
                    break;
                case TRAINER:
                    break;
                case VERIFIKATOR:
                    break;
                case AUDITOR:
                    break;
                case PP:
                	Pp pp = Pp.find("peg_id=? ORDER BY pp_id DESC", pegawai.peg_id).first();
                	if(pp != null)
                		active_user.ppId = pp.pp_id;
                    break;
                case PPHP:
                    break;
				case UKPBJ:
				case KUPPBJ:
					Ukpbj ukpbj = Ukpbj.find("kpl_unit_pemilihan_id=?", pegawai.peg_id).first();
					if (ukpbj != null) {
						active_user.ukpbjId = ukpbj.ukpbj_id;
						active_user.agencyId = ukpbj.agc_id;
					} else {
						clearUserSession(); // bersihkan session pengguna
						forbidden(Messages.get("login.ukpbj_tdk_dtmkn"));
					}
					break;
				case ADM_PANEL:
					break;
            }
            active_user.save();
	        BerandaCtr.index();
        }
        else {
           	Logger.debug("error otentikasi : Tidak ada data pegawai dengan userid "+active_user.userid);
           	PublikCtr.index();
        }
    }

    /**
     * Fungsi {@code logout} digunakan sebagai aksi saat pengguna logout dari
     * aplikasi
     */
    @AllowAccess({Group.ALL_AUTHORIZED_USERS})
    public static void logout() {
        clearUserSession(); // bersihkan session pengguna
        PublikCtr.index(); // kembali ke halaman muka aplikasi
    }
}
