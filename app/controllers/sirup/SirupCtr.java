package controllers.sirup;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Paket_pl;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import models.secman.Group;
import models.sirup.Sirup;
import models.sirup.form.SirupSyncDataForm;
import models.sirup.presenter.SirupComparisonPresenter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 4/2/2018
 */
public class SirupCtr extends BasicCtr {

    @AllowAccess({Group.PP, Group.PPK})
    public static void comparison(Long pid, Long sid) {
        notFoundIfNull(pid);
        notFoundIfNull(sid);
        Non_spk_seleksi non_spk_seleksi = Non_spk_seleksi.findById(pid);
        notFoundIfNull(non_spk_seleksi);
        Paket_pl paket_pl = Paket_pl.findById(non_spk_seleksi.pkt_id);
        notFoundIfNull(paket_pl);
        Sirup model = Sirup.getSirupById(sid);
        notFoundIfNull(model);
        Map<String, Object> map = new HashMap<>();
        map.put("presenter", new SirupComparisonPresenter(non_spk_seleksi, paket_pl, model));
        renderTemplate("sirup/comparison.html", map);
    }

    @AllowAccess({Group.PP, Group.PPK})
    public static void syncRup(SirupSyncDataForm model) {

    }

}
