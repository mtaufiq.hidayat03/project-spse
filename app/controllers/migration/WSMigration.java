package controllers.migration;

import jobs.migration.DatabaseFKFixing;
import jobs.migration.LargeObjectRemoverLauncher;
import play.mvc.WebSocketController;

import java.io.IOException;

public class WSMigration extends WebSocketController {
	
	/**WebSocket untuk large object migration */
	public static void largeObjectMover() throws InterruptedException, IOException
	{
		while(inbound.isOpen())
		{
			//wait untuk object available
			synchronized (LargeObjectRemoverLauncher.MONITOR) {
				//keluar jika job tidak  running
				if(!LargeObjectRemoverLauncher.MONITOR.running)
				{
					if(outbound.isOpen())
						outbound.send(LargeObjectRemoverLauncher.MONITOR.status);
					break;
				}
				LargeObjectRemoverLauncher.MONITOR.wait();	
				if(outbound.isOpen())
					outbound.send(LargeObjectRemoverLauncher.MONITOR.status);
				else
					break;
				if(LargeObjectRemoverLauncher.MONITOR.status.startsWith("SELESAI"))
					break;
			}			
		}
	}
	
	/**WebSocket untuk large object migration */
	public static void databaseMigration() throws InterruptedException, IOException
	{
		while(inbound.isOpen())
		{
			//wait untuk object available
			synchronized (DatabaseFKFixing.MONITOR) {
				//keluar jika job tidak  running
				if(!DatabaseFKFixing.MONITOR.running)
				{
					if(outbound.isOpen())
						outbound.send(DatabaseFKFixing.MONITOR.status);
					break;
				}
				DatabaseFKFixing.MONITOR.wait();	
				if(outbound.isOpen())
					outbound.send(DatabaseFKFixing.MONITOR.status);
				else
					break;
				if(DatabaseFKFixing.MONITOR.status.startsWith("SELESAI"))
					break;
			}			
		}
	}
}
