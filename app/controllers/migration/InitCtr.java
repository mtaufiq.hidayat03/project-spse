package controllers.migration;

import models.UserMigration;
import models.common.ConfigurationDao;
import models.common.SesiPelatihan;
import models.jcommon.util.CommonUtil;
import play.Play;
import play.mvc.Controller;
import utils.LogUtil;

import java.util.Date;

/**
 * Created by rayhanfrds on 8/14/17.
 */
public class InitCtr extends Controller {

    public static void user(Integer max, String type, String mode, Integer start) {
        if (!ConfigurationDao.isLatihan()) {
            response.writeChunk("This request only allowed for LATIHAN mode");
        } else {
            if (CommonUtil.isEmpty(type)) {
                type = "ALL";
            }

            if (CommonUtil.isEmpty(mode)) {
                mode = "update";
            }
            String initRekananConf = Play.configuration.getProperty("init.rekanan");
            LogUtil.debug("InitCtr", "max: " + max + " " + type + " " + mode + " " + start);
            if (type.equals("ALL")) {
				UserMigration.insertAgency(max, mode, start);
				UserMigration.insertVerifikator(max);
				UserMigration.insertHelpdesk(max);
				UserMigration.insertRekanan(initRekananConf, mode, start, max);
            } else if (type.equals("pegawai")) {
				UserMigration.insertAgency(max, mode, start);
            } else if (type.equals("rekanan")) {
				UserMigration.insertRekanan(initRekananConf, mode, start, max);
            }
            Date now = new Date();
            response.writeChunk("DONE : " + now.toString());
        }
    }

	public static void trainer() {
		if (!ConfigurationDao.isLatihan()) {
			response.writeChunk("This request only allowed for LATIHAN mode");
		} else {
            UserMigration.insertTrainer();
            response.writeChunk("DONE : " + new Date());
		}
	}
}
