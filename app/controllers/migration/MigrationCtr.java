package controllers.migration;

import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.SQLException;


/**
 * migration Controller
 * @author Arief
 * deprecated karena sudah digantikan dengan proses di shell (command:spse4 migration)
 */
@Deprecated
public class MigrationCtr extends Controller {

	@Before
	static void interuptor() {
		forbidden(Messages.get("not-authorized"));
	}

	public static void index() throws Exception
	{
//        DatabaseMigration.doMigration();
		renderTemplate("migration/index.html");
	}

	/**Perform migration
	 * @throws SQLException
	 * @throws FileNotFoundException
	 *

	public static void doMigration() throws SQLException, FileNotFoundException
	{
		String logFile=Play.applicationPath+"/logs/DB.migration." + new SimpleDateFormat("yyyyMMdd-hhmmss").format(new Date()) + ".log";
		PrintWriter logWriter=new PrintWriter(new File(logFile));
		DatabaseMigration dm=new DatabaseMigration();
		response.contentType="text/html";
		response.writeChunk("<html><body>");
		response.writeChunk("<h1>Migration in progress - Don't Refresh</h1>");
		response.writeChunk("<i class=\"fa fa-spinner fa-pulse\"></i>");
		response.writeChunk("Preparing Database Metadata....<pre>");
		response.writeChunk("\nLog file: "+logFile);
		dm.doMigration();
		response.writeChunk("\n*********** Executing SQL.... ***********\n");
		int i=0;
		Connection conn=DB.getConnection();
		Statement st=conn.createStatement();
		List<String>[] arys=new List[]{dm.ddlCommands};
		for(List<String> ddlList: arys)
			for (String sql: ddlList)
			{
				String str=String.format("\n#### %s %s\n", ++i, sql);
				write(str, logWriter);
				try
				{
					st.executeUpdate(sql);
					write("[OK]\n", logWriter);
				}
				catch(Exception e)
				{
					str=String.format("[ERROR] %s\n", e.getMessage());
					write(str, logWriter);
				}
			}
		logWriter.close();
		st.close();
		response.writeChunk("</pre><h3>Migration DONE</h3></body><html>");
	} */

	private static void write(String str, PrintWriter file)
	{
		response.writeChunk(str);
		file.print(str);
	}
}
