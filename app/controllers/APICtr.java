package controllers;

import models.common.ConfigurationDao;
import models.jcommon.blob.BlobTable;
import play.Logger;
import play.libs.MimeTypes;
import play.mvc.Controller;
import play.mvc.Http;
import utils.APIUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Compact API agar penarikan data lebih efisien
 *
 * @author Mohamad Irvan Faradian
 */
public class APICtr extends Controller {
  /**
   * @param tahun : Tahun Anggaran
   */
  public static void writeToFile(int tahun) throws UnsupportedEncodingException, IOException {
    InputStream in = new ByteArrayInputStream(APIUtil.lelangLengkapJSON(tahun).getBytes(StandardCharsets.UTF_8.name()));

    String baseDir = BlobTable.getFileStorageDir();
    if (baseDir == null)
      throw new IOException(BlobTable.FILE_STORAGE_DIR + " is not defined");

    File parentDir = new File(baseDir + "/API");

    if (!parentDir.exists())
      parentDir.mkdirs();

    File file = new File(parentDir + "/" + tahun + "-" + ConfigurationDao.getRepoId() + ".json");

    // Selalu hapus existing file sebelum generate yang baru
    if (file.exists())
      file.delete();

    FileOutputStream out = new FileOutputStream(file);

    byte buffer[] = new byte[1024 * 10];
    int size;
    while((size = in.read(buffer)) != -1)
    {
      out.write(buffer, 0, size);
    }

    out.close();
    in.close();

    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    Logger.info("File API JSON " + tahun + " berhasil dibuat: " + df.format(date));
    renderText("File API JSON " + tahun + " berhasil dibuat");
  }

  /**
   * @param tahun : Tahun Anggaran
   */
  public static void downloadFile(int tahun) {
    File file = new File(BlobTable.getFileStorageDir() + "/API/" + tahun + "-" + ConfigurationDao.getRepoId() + ".json");

    if (file.exists()) {
      response.contentType = MimeTypes.getContentType(tahun + "-" + ConfigurationDao.getRepoId() + ".json", "application/octet-stream");
      Http.Header header = new Http.Header("Content-Disposition", "attachment; filename=\"" + tahun + "-" + ConfigurationDao.getRepoId() + ".json" + "\"");
      response.headers.put("Content-Disposition", header);
      renderBinary(file);
    } else {
      renderText("File tidak ada");
    }
  }
}
