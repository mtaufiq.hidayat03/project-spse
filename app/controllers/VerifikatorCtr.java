package controllers;

import controllers.security.AllowAccess;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.common.MailQueueDao;
import models.common.Sub_tag;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.lelang.Blacklist;
import models.osd.Certificate;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.sso.common.RekananSSO;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.data.validation.Validation;
import play.libs.URLs;
import play.libs.WS;
import models.sso.common.adp.util.DceSecurityV2;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import utils.osd.OSDUtil;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class VerifikatorCtr extends BasicCtr {

    @AllowAccess({Group.VERIFIKATOR})
    public static void rekanan() {
        renderArgs.put("isOSD", ConfigurationDao.isOSD(newDate()));
        renderTemplate("verifikator/verifikator-rekanan.html");
    }

    @AllowAccess({Group.VERIFIKATOR})
    public static void rekananBaru() {        
        renderArgs.put("status", String.valueOf(Rekanan.REKANAN_BARU));
        renderTemplate("verifikator/verifikator-rekanan.html");
    }

    @AllowAccess({Group.VERIFIKATOR})
    public static void rekananRoaming() {
        renderArgs.put("status","R");
        renderTemplate("verifikator/verifikator-rekanan.html");
    }

    @AllowAccess({Group.VERIFIKATOR})
    public static void rekananDitolak() {
        renderArgs.put("status", String.valueOf(Rekanan.REKANAN_DITOLAK));
        renderTemplate("verifikator/verifikator-rekanan.html");
    }

    @AllowAccess({Group.VERIFIKATOR})
    public static void rekananTerverifikasi() {
        renderArgs.put("status", String.valueOf(Rekanan.REKANAN_DISETUJUI));
        renderTemplate("verifikator/verifikator-rekanan.html");
    }

    //FUNGSI MELIHAT DETAIL DATA REKANAN  
    @AllowAccess({Group.VERIFIKATOR})
    public static void view(Long id) {
        Rekanan rekanan = Rekanan.findById(id);
        notFoundIfNull(rekanan);
        Date date = newDate();
        renderArgs.put("rekanan", rekanan);
        renderArgs.put("namalpse", ConfigurationDao.getNamaRepo(rekanan.repo_id));
        renderArgs.put("allowEdit", rekanan.repo_id.intValue() == ConfigurationDao.getRepoId());
        Blacklist blacklist = Blacklist.find("rkn_id=? order by bll_id desc", id).first();
        if (!(blacklist != null && (date == blacklist.bll_tglakhir || date.after(blacklist.bll_tglakhir)))) {
            renderArgs.put("blacklist", blacklist);
        }
        if(!StringUtils.isEmpty(rekanan.edited_data)) {
            renderArgs.put("rekananEdit", CommonUtil.fromJson(rekanan.edited_data, Rekanan.class));
        }
        renderTemplate("verifikator/verifikator-rekanan-view.html");
    }

    //FUNGSI MENAMPILKAN HALAMAN EDIT REKANAN
    @AllowAccess({Group.VERIFIKATOR})
    public static void edit(Long id) {
        Rekanan rekanan = Rekanan.findById(id);
        notFoundIfNull(rekanan);
        if(rekanan.repo_id.intValue() != ConfigurationDao.getRepoId())
            forbidden("Anda Tidak Diizinkan Melakukan Perubahan Data "+rekanan.rkn_nama);
        renderArgs.put("rekanan", rekanan);
        Date date = controllers.BasicCtr.newDate();
        Blacklist blacklist = Blacklist.find("rkn_id=? order by bll_id desc", id).first();
        if ((blacklist != null && date == blacklist.bll_tglakhir) || (blacklist != null && date.after(blacklist.bll_tglakhir))) {
            renderTemplate("verifikator/verifikator-rekanan-edit.html");
        } else {
            renderArgs.put("blacklist", blacklist);
            renderTemplate("verifikator/verifikator-rekanan-edit.html");
        }
    }

    //FUNGSI MENGUBAH (EDIT) DATA REKANAN
    @AllowAccess({Group.VERIFIKATOR})
    public static void editRekananSubmit(Rekanan rekanan) {
        checkAuthenticity();
        validation.required("rekanan.rkn_nama", rekanan.rkn_nama);
        validation.required("rekanan.rkn_alamat", rekanan.rkn_alamat);
        validation.required("rekanan.rkn_npwp", rekanan.rkn_npwp);
        validation.required("rekanan.rkn_email", rekanan.rkn_email);
        validation.required("rekanan.btu_id", rekanan.btu_id);
        validation.email("rekanan.rkn_email", rekanan.rkn_email);

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            edit(rekanan.rkn_id);
        }

        //Cek email yang disubmit sudah dipakai atau belum
        if (Rekanan.emailExist(rekanan.rkn_email, rekanan.rkn_id)) {
            validation.addError("rekanan.rkn_email", "Email sudah dipakai.");
            validation.keep();
            edit(rekanan.rkn_id);
        }

        Rekanan obj = Rekanan.findById(rekanan.rkn_id);
        obj.rkn_nama = rekanan.rkn_nama;
        obj.rkn_alamat = rekanan.rkn_alamat;
        obj.rkn_npwp = rekanan.rkn_npwp;
        obj.rkn_email = rekanan.rkn_email;
        obj.btu_id = rekanan.btu_id;

        if (obj.rkn_status == 0) {
            obj.rkn_telepon = rekanan.rkn_telepon;
            obj.rkn_mobile_phone = rekanan.rkn_mobile_phone;
        }

        try {
            obj.saveToADPCentral();
            obj.save();
            flash.success("Data berhasil tersimpan");
        } catch (Exception e) {
            flash.error("Data gagal tersimpan");
            Logger.error(e, "Data gagal tersimpan");
        }

        view(rekanan.rkn_id);

    }


    //FUNGSI MENAMPILKAN HALAMAN FORM BLACKLIST REKANAN
    @AllowAccess({Group.VERIFIKATOR})
    public static void blacklist(Long rekananId, Long bll_id) {
        Rekanan rekanan = Rekanan.findById(rekananId);
        renderArgs.put("rekanan", rekanan);
        Date date = controllers.BasicCtr.newDate();
        Blacklist blacklist = Blacklist.find("rkn_id=? order by bll_id desc", rekananId).first();
        if (blacklist == null) {
            blacklist = new Blacklist(); //untuk tanggal usulan wajib diisi oleh siapa?
        } else if ((date == blacklist.bll_tglakhir) || (date.after(blacklist.bll_tglakhir))) {
            rekanan = Rekanan.findById(rekananId);
            blacklist = new Blacklist();
        }
        renderArgs.put("blacklist", blacklist);
        renderTemplate("verifikator/verifikator-rekanan-edit-blacklist.html");
    }


    //FUNGSI MENAMPILKAN HALAMAN EDIT REKANAN
    @AllowAccess({Group.VERIFIKATOR})
    public static void editBlacklist(Long rekananId, Long id) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, DateUtil.getTahunSekarang());
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
//        Date date = cal.getTime();
        Rekanan rekanan = Rekanan.findById(rekananId);
        renderArgs.put("rekanan", rekanan);
        Blacklist blacklist = Blacklist.findById(id);
        renderArgs.put("blacklist", blacklist);
        if (blacklist == null) {
            renderArgs.put("blob", null);
            renderTemplate("verifikator/verifikator-rekanan-view.html");
        } else {
            if (blacklist.bll_pcattachment != null)
                renderArgs.put("blob", BlobTableDao.getLastById(blacklist.bll_pcattachment));
            renderTemplate("verifikator/verifikator-rekanan-edit-blacklist.html");
        }
    }


    //FUNGSI MEMASUKKAN REKANAN KE BLACKLIST
    @AllowAccess({Group.VERIFIKATOR})
    public static void simpanBlacklist(Blacklist blacklist, File attachment, File attachment2) {
        if (blacklist.bll_tglusul == null)
            blacklist.bll_tglusul = controllers.BasicCtr.newDate();
        if (blacklist.bll_tglsetuju == null)
            blacklist.bll_tglsetuju = controllers.BasicCtr.newDate();
        blacklist.bll_disetujui = Long.parseLong("1");
        validation.valid(blacklist);
        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
        } else {
            try {
                Blacklist.simpan(blacklist, attachment, attachment2);
                flash.success("%s berhasil disimpan", "Data Blacklist");
            } catch (Exception e) {
                Logger.error(e, "%s gagal create tersimpan", "Data Blacklist");
                flash.error("%s gagal create tersimpan", "Data Blacklist");
            }
        }
        editBlacklist(blacklist.rkn_id, blacklist.bll_id);
    }


    //fungsi belum digunakan
    @AllowAccess({Group.VERIFIKATOR})
    public static boolean isSuspend(Long rekananId) {
        Rekanan rekanan = Rekanan.findById(rekananId);
        if (rekanan.rkn_status.equals(Rekanan.REKANAN_DISETUJUI)) {
            return rekanan.isDisableUser();
        }
        return false;
    }

    //FUNGSI MENONAKTIFKAN REKANAN
    @AllowAccess({Group.VERIFIKATOR})
    public static void toggleSuspendRekanan(Long rekananId) {
        Rekanan rekanan = Rekanan.findById(rekananId);
        if (rekanan.rkn_status.equals(Rekanan.REKANAN_DISETUJUI)) {
            rekanan.rkn_isactive = (-1 * rekanan.rkn_isactive) - 1;
            rekanan.disableuser = (-1 * rekanan.disableuser) - 1;
            rekanan.save();

            if (rekanan.rkn_isactive == -1)
                flash.success("Penyedia diaktifkan");
            else
                flash.success("Penyedia dinon-aktifkan");
        }
        view(rekananId);
    }


    //FUNGSI MENYETUJUI REKANAN BARU
    @AllowAccess({Group.VERIFIKATOR})
    public static void setujuRekananSubmit(Long rekananId, boolean setuju) throws Exception {
    	checkAuthenticity();
        Rekanan rekanan = Rekanan.findById(rekananId);
        rekanan.rkn_keterangan = params.get("rekanan.rkn_keterangan");
        validation.required("rekanan.rkn_keterangan", rekanan.rkn_keterangan);
        if(Validation.hasErrors()) {
            validation.keep();
            params.flash();
        } else if(setuju){
            List<Sub_tag> prasyaratList = Sub_tag.findSubTagSyaratVerifikasi(rekanan.getBentuk_usaha().iskonsultanIndividu());
            boolean syaratValid = params.getAll("cekPrasyarat") != null && params.getAll("cekPrasyarat").length == prasyaratList.size();
            if(rekanan.isPerusahaanAsing())
                syaratValid = true;
            if (rekanan.rkn_status.equals(Rekanan.REKANAN_BARU) && syaratValid) {
                try {
                    rekanan.rkn_status = Rekanan.REKANAN_DISETUJUI;
                    rekanan.rkn_tgl_setuju = DateUtil.newDate();
                    Pegawai pegawai = Pegawai.find("peg_namauser=?", Active_user.current().userid).first();
                    rekanan.ver_namauser = pegawai.peg_nama;
                    StringBuilder verifikasi = new StringBuilder();
                    for (int i = 0; i < prasyaratList.size(); i++)
                        verifikasi.append('1');
                    rekanan.rkn_status_verifikasi = verifikasi.toString();
                    rekanan.rkn_isactive = -1;
                    rekanan.disableuser = 0;
                    rekanan.isbuiltin = 0;
                    	
                    rekanan.save();
                    if (ConfigurationDao.isEnableInaproc()) { // send data rekanan to Inaproc (central repository)
                        RekananSSO userRekanan = rekanan.toRekananSSO();
                        if (userRekanan != null) {
                            String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(userRekanan)));
                            String url_service = ConfigurationDao.getRestCentralService() + "/add-rekanan?q=" + param;
                            try {
                                WSRequest request = WS.url(url_service);
                                request.setHeader("authentication", Play.secretKey);
                                HttpResponse response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
                                if (response.success() && !StringUtils.isEmpty(response.getString())) {
                                    throw new Exception(DceSecurityV2.decrypt(response.getString()));
                                }
                            }catch (Exception e) {
                                throw e;
                            }
                        }
                    }
                    MailQueueDao.KonfirmasiPersetujuan(rekanan.rkn_id, rekanan.rkn_email, rekanan.rkn_namauser, rekanan.rkn_nama, rekanan.rkn_alamat);
                    flash.success("Penyedia berhasil disetujui");
                } catch (Exception e) {
                    Logger.error(e, e.getMessage());
                    flash.error("Penyedia gagal disetujui");
                }
            } else {
            	flash.error("Penyedia gagal disetujui");
            }
        } else {
            if (rekanan.rkn_status.equals(Rekanan.REKANAN_BARU)) {
                rekanan.rkn_status = Rekanan.REKANAN_DITOLAK;
                rekanan.save();
                flash.success("Penyedia tidak disetujui");
            }
        }
        view(rekanan.rkn_id);
    }


    //FUNGSI MENGHAPUS REKANAN
    @AllowAccess({Group.VERIFIKATOR})
    public static void hapusRekananSubmit(Long rekananId) {
        Rekanan rekanan = Rekanan.findById(rekananId);
        rekanan.delete();
        rekanan();
    }

    @AllowAccess({Group.VERIFIKATOR})
    public static void approvePerubahan(Long rekananId) {
        try {
            Pegawai pegawai = Pegawai.find("peg_namauser=?", Active_user.current().userid).first();
            Rekanan rekanan = Rekanan.findById(rekananId);
            Rekanan rekananEdit = CommonUtil.fromJson(rekanan.edited_data, Rekanan.class);
            rekanan.rkn_kodepos = rekananEdit.rkn_kodepos;
            rekanan.rkn_pkp = rekananEdit.rkn_pkp;
            rekanan.kbp_id = rekananEdit.kbp_id;
            rekanan.rkn_telepon = rekananEdit.rkn_telepon;
            rekanan.rkn_fax = rekananEdit.rkn_fax;
            rekanan.rkn_website = rekananEdit.rkn_website;
            rekanan.rkn_mobile_phone = rekananEdit.rkn_mobile_phone;
            rekanan.rkn_statcabang = rekananEdit.rkn_statcabang;
            if(rekananEdit.rkn_statcabang.equals("C")){
                rekanan.rkn_almtpusat = rekananEdit.rkn_almtpusat;
                rekanan.rkn_emailpusat = rekananEdit.rkn_emailpusat;
                rekanan.rkn_telppusat = rekananEdit.rkn_telppusat;
                rekanan.rkn_faxpusat = rekananEdit.rkn_faxpusat;
            }
            else{
                rekanan.rkn_almtpusat = null;
                rekanan.rkn_emailpusat = null;
                rekanan.rkn_telppusat = null;
                rekanan.rkn_faxpusat = null;
            }
            rekanan.edited_data = null;
            rekanan.ver_namauser = pegawai.peg_nama;
            rekanan.saveToADPCentral();
            rekanan.save();
            flash.success("Data Penyedia berhasil diubah");
        } catch (Exception e) {
            flash.error("Simpan Data Identitas gagal");
            Logger.error(e, "Simpan Data Identitas gagal");
        }

        view(rekananId);
    }

    /**
     * Batalkan permintaan perubahan data rekanan
     */
    @AllowAccess({Group.VERIFIKATOR})
    public static void cancelPerubahan(Long rekananId) {
        Rekanan obj = Rekanan.findById(rekananId);
        obj.edited_data = null;
        obj.save();
        flash.success("Permintaan perubahan data berhasil dibatalkan.");
        view(rekananId);
    }

    @AllowAccess({Group.VERIFIKATOR})
    public static void viewCert(Long id) {
        Rekanan rekanan = Rekanan.findById(id);
        notFoundIfNull(rekanan);
        List<Certificate> certificates = OSDUtil.getCertificatesFromInaproc(rekanan.cer_id);
        renderArgs.put("certificates", certificates);
        boolean emptyData = certificates == null || certificates.size() <= 0;
        renderArgs.put("emptyData", emptyData);
        int lastVersi = 0;
        Certificate certificateLast;
        if (!emptyData && (certificateLast = certificates.get(certificates.size() - 1)) != null) {
            renderArgs.put("certificateLast", certificateLast);
            renderArgs.put("lastVersi", certificateLast.cer_versi);
        }
        renderTemplate("admin/pegawai/pegawaiViewCert.html");
    }

    public static void sendToAdp(Long id) throws Exception {
        otorisasiSecretKey();
        Rekanan rekanan = Rekanan.findById(id);
        RekananSSO userRekanan = rekanan.toRekananSSO();
        if (userRekanan != null) {
            String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(userRekanan)));
            String url_service = ConfigurationDao.getRestCentralService() + "/add-rekanan?q=" + param;
            try {
                WSRequest request = WS.url(url_service);
                request.setHeader("authentication", Play.secretKey);
                HttpResponse response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
                if (response.success() && !StringUtils.isEmpty(response.getString())) {
                    throw new Exception(DceSecurityV2.decrypt(response.getString()));
                }
            } catch (Exception e) {
                Logger.error(e, "sendToAdp");
            }
        }
    }
}
