package controllers;

import controllers.security.AllowAccess;
import ext.DatetimeBinder;
import models.agency.Anggota_panitia;
import models.agency.Paket;
import models.common.SesiPelatihan;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DateUtil;
import models.lelang.*;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.lang3.ArrayUtils;
import play.Logger;
import play.data.binding.As;

import java.util.Date;
import java.util.List;

@AllowAccess({ Group.TRAINER })
public class TrainerCtr extends BasicCtr {
		
	@AllowAccess({Group.TRAINER})
	public static void setDate(@As(binder=DatetimeBinder.class) Date date, int sesiId) throws Exception {
		SesiPelatihan sesi=SesiPelatihan.findById(sesiId);
		DateUtil.setDate(sesi.id,date);
		Logger.info("[Trainer] Set Current Date to %s: %s", sesi.nama, date);		
//		WorkflowDao.updateProcess();
//		WorkflowPlDao.updateProcess();
		BerandaCtr.index();
	}
	
	@AllowAccess({Group.TRAINER})
	public static void ubahWaktu(int id) {
		SesiPelatihan sesi = SesiPelatihan.findById(id);
		renderArgs.put("sesi", sesi);
		renderArgs.put("date",DateUtil.newDate(sesi.id));
		renderTemplate("trainer/ubah-waktu.html");
	}
	
	@AllowAccess({Group.TRAINER})
	public static void pendaftaranPenyedia() {
		renderArgs.put("rekananList", Rekanan.order("rkn_id").fetch(25));
		renderTemplate("trainer/pendaftaran-penyedia.html");
	}
	
	@AllowAccess({Group.TRAINER})
	public static void daftarkan(List<Long> rekananId, List<Long> lelangId) {
		checkAuthenticity();
		if(lelangId != null && !lelangId.isEmpty()) {
			Peserta peserta = null;
			for (Long id : lelangId) {
				for (Long rekId : rekananId) {
					peserta = Peserta.find("lls_id=? and rkn_id=?", id, rekId).first();
					if(peserta == null)
						Peserta.daftarLelang(id, rekId);
				}
			}
			flash.success("Penyedia terpilih telah didaftarkan");
		}
		pendaftaranPenyedia();
	}

	@AllowAccess({Group.TRAINER})
	public static void penjelasanDokumen() {
		String aktivitas = "akt_jenis in ('"+Tahap.PENJELASAN+"','"+Tahap.PENJELASAN_PRA+"')";
		Date currDate = controllers.BasicCtr.newDate();
		renderArgs.put("lelang",Lelang_seleksi.find("lls_status=1 and lls_id in (select distinct lls_id from Jadwal j, aktivitas a where j.akt_id=a.akt_id and dtj_tglawal <= ? and dtj_tglakhir >= ? and "+aktivitas+ ')', currDate, currDate).fetch());
		renderTemplate("trainer/penjelasan-dokumen.html");
	}
	
	@AllowAccess({Group.TRAINER})
	public static void kirimPenjelasan(Long[] id, boolean pertanyaan, boolean penjelasan, boolean pertanyaan_pra, boolean penjelasan_pra, String text) {
		checkAuthenticity();
		if(!ArrayUtils.isEmpty(id)) {
			List<Peserta> pesertaList = null;
			for(Long lelangId:id) {
				pesertaList = Peserta.findBylelang(lelangId);
				if(pertanyaan) {
					Diskusi_lelang.kirimPertanyaan(lelangId, text, Tahap.PENJELASAN, pesertaList, newDate());
					flash.success("pengiriman pertanyaan berhasil dilakukan");
				}
				else if(pertanyaan_pra) {
					Diskusi_lelang.kirimPertanyaan(lelangId, text, Tahap.PENJELASAN_PRA, pesertaList, newDate());
					flash.success("pengiriman pertanyaan berhasil dilakukan");
				}else if(penjelasan) {
					Diskusi_lelang.kirimPenjelasan(lelangId, text, Tahap.PENJELASAN, newDate());
					flash.success("pengiriman penjelasan berhasil dilakukan");
				}else if(penjelasan_pra) {
					Diskusi_lelang.kirimPenjelasan(lelangId, text, Tahap.PENJELASAN_PRA, newDate());
					flash.success("pengiriman penjelasan berhasil dilakukan");
				}
			}
			
		}
		penjelasanDokumen();
	}

	@AllowAccess({Group.TRAINER})
	public static void pengirimanDokumenKualifikasi() {
		String aktivitas = "akt_jenis in ('"+Tahap.PEMASUKAN_DOK_PRA+"','"+Tahap.PEMASUKAN_PENAWARAN+"','"+Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS+"','"+Tahap.PEMASUKAN_PENAWARAN_BIAYA+"')";
		Date currDate = controllers.BasicCtr.newDate();
		renderArgs.put("lelang", Lelang_seleksi.find("lls_status=1 and lls_id in (select distinct lls_id from Jadwal j, aktivitas a where j.akt_id=a.akt_id and dtj_tglawal <= ? and dtj_tglakhir >= ? and "+aktivitas+ ')', currDate, currDate).fetch());
		renderTemplate("trainer/pengiriman-dokumen-kualifikasi.html");
	}
	
	public static void kirimkKualifikasi(Long[] id) {
		checkAuthenticity();
		if(!ArrayUtils.isEmpty(id)) {
			List<Peserta> pesertaList = null;
			Dok_penawaran dok = null;
			for(Long lelangId:id) {
				pesertaList = Peserta.findBylelang(lelangId);
				if(!CommonUtil.isEmpty(pesertaList))
				{
					for(Peserta peserta : pesertaList)
					{
						dok = Dok_penawaran.findPenawaranPeserta(peserta.psr_id, JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
						if (dok==null) {
							dok = new Dok_penawaran();
							dok.dok_jenis = JenisDokPenawaran.PENAWARAN_KUALIFIKASI;
							dok.psr_id = peserta.psr_id;
							dok.dok_tgljam = controllers.BasicCtr.newDate();
							dok.dok_judul = "Dok Prakualifikasi.xls";
							dok.dok_id_attachment =Long.valueOf(0);
							dok.save();
						}
					}
				}
			}
			flash.success("Pengiriman Dokumen Kualifikasi berhasil");
		}		
		pengirimanDokumenKualifikasi();
	}

	@AllowAccess({Group.TRAINER})
	public static void pengirimanDokumenPenawaran() {
		String aktivitas = "akt_jenis in ('"+Tahap.PEMASUKAN_PENAWARAN+"','"+Tahap.PEMASUKAN_PENAWARAN_ADM_TEKNIS+"','"+Tahap.PEMASUKAN_PENAWARAN_BIAYA+"')";
		Date currDate = controllers.BasicCtr.newDate();
		renderArgs.put("lelang",Lelang_seleksi.find("lls_status=1 and lls_id in (select distinct lls_id from Jadwal j, aktivitas a where j.akt_id=a.akt_id and dtj_tglawal <= ? and dtj_tglakhir >= ? and "+aktivitas+ ')', currDate, currDate).fetch());
		renderTemplate("trainer/pengiriman-dokumen-penawaran.html");
	}
	
	public static void kirimPenawaran(Long[] id) {
		checkAuthenticity();
		if(!ArrayUtils.isEmpty(id)) {
				/*if(Dok_penawaran.getFileLatihanUpload()!=null)
				{
					List<Peserta> pesertaList = null;
					Dok_penawaran dok = null;
					for(Long lelangId: id)
					{
						pesertaList = Peserta.findBylelang(lelangId);
						if(!CommonUtil.isEmpty(pesertaList))
						{
							for(Peserta peserta: pesertaList){
								if(lelang_seleksiDao.getMetodeLelang(lelangId).getDokumen().isSatuFile()){
									Dok_penawaran.simpanPenawaranLatihan(peserta.psr_id, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA);
								} else {
									dok_penawaranDao.simpanPenawaranLatihan(peserta.psr_id, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI, CommonUtil.getCurrentDate());
									dok_penawaranDao.simpanPenawaranLatihan(peserta.psr_id, JenisDokPenawaran.PENAWARAN_HARGA, CommonUtil.getCurrentDate());
								}
							}
						}
					}
					flash.success("pengiriman dokumen penawaran berhasil");
				} else
					flash.error("simulasi pengiriman penawaran tidak dapat dilakukan");*/
		}
	}

	@AllowAccess({Group.TRAINER})
	public static void sanggahanLelang() {
		String aktivitas = "akt_jenis in ('"+Tahap.SANGGAH+"','"+Tahap.SANGGAH_PRA+"')";
		Date currDate = controllers.BasicCtr.newDate();
		renderArgs.put("lelang", Lelang_seleksi.find("lls_status=1 and lls_id in (select distinct lls_id from Jadwal j, aktivitas a where j.akt_id=a.akt_id and dtj_tglawal <= ? and dtj_tglakhir >= ? and "+aktivitas+ ')', currDate, currDate).fetch());
		renderTemplate("trainer/sanggahan-lelang.html");
	}
	
	@AllowAccess({Group.TRAINER})
	public static void kirimSanggahan(Long[] id, boolean sanggah, boolean balas, boolean sanggah_pra, boolean balas_pra, String text) {
		checkAuthenticity();
		Tahap tahap = Tahap.SANGGAH;
		if(sanggah_pra || balas_pra)
			tahap = Tahap.SANGGAH_PRA;
		if(!ArrayUtils.isEmpty(id)) {
			Sanggahan sanggahan = null;
			if(sanggah || sanggah_pra) {
				for(Long lelangId:id) {
					List<Peserta> pesertaList = Peserta.findBylelang(lelangId);
					for (Peserta peserta : pesertaList) {
						sanggahan = Sanggahan.findBy(peserta.psr_id, tahap);
						if(sanggahan == null) {
							sanggahan = new Sanggahan();
							sanggahan.thp_id = tahap.id;
							sanggahan.san_sgh_id = null;
							sanggahan.sgh_tanggal = controllers.BasicCtr.newDate();
							sanggahan.psr_id = peserta.psr_id;
							sanggahan.sgh_pengirim = "R";
							sanggahan.sgh_isi = CommonUtil.isEmpty(text)?"contoh sanggahan "+tahap.label:text;
							sanggahan.save();	
						}
					}			
				}
				flash.success("pengiriman sanggahan berhasil");
			}
			if(balas || balas_pra) {
				for(Long lelangId:id) {
					Paket paket = Paket.findByLelang(lelangId);
					Anggota_panitia anggota = Anggota_panitia.findKetuaPanitia(paket.pnt_id);
					List<Sanggahan> sanggahanlist = Sanggahan.findBy(lelangId, tahap, null);
					for (Sanggahan obj : sanggahanlist) {
						sanggahan = new Sanggahan();
						sanggahan.thp_id = tahap.id;
						sanggahan.san_sgh_id = obj.sgh_id;
						sanggahan.psr_id = obj.psr_id;
						sanggahan.sgh_tanggal = newDate();
						sanggahan.peg_id = anggota.peg_id;
						sanggahan.sgh_pengirim = "P";
						sanggahan.sgh_isi = CommonUtil.isEmpty(text)?"contoh balasan sanggahan "+tahap.label:text;
						sanggahan.save();		
					}					
				}
				flash.success("pengiriman Balasan sanggahan berhasil");
			}
		}
		sanggahanLelang();
	}
}
