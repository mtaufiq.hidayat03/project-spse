package controllers.dce;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import controllers.BasicCtr;
import models.agency.Anggota_panitia;
import models.agency.Paket;
import models.common.CONFIG;
import models.common.ConfigurationDao;
import models.common.Metode;
import models.jcommon.blob.BlobTable;
import models.jcommon.config.Configuration;
import models.lelang.Dok_penawaran;
import models.lelang.Lelang_key;
import models.lelang.Lelang_seleksi;
import models.lelang.rhs.BlobMetadata;
import models.lelang.rhs.RhsFileValidation;
import org.apache.commons.collections4.CollectionUtils;
import org.bouncycastle.util.encoders.Base64;
import play.Logger;
import play.i18n.Messages;
import play.mvc.Controller;
import utils.osd.KmsClient;
import utils.osd.OSDUtil;

import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;


public class UtilityController extends Controller {


    public static void datetime() {
        renderText(BasicCtr.newDate().getTime());
    }

    public static void generateKey() {
        StringBuilder result = new StringBuilder();
        List<Lelang_seleksi> list = Lelang_seleksi.find("lls_status = 1 and lls_id not in (select lls_id from lelang_key) and pkt_id not in (select pkt_id from paket where pkt_flag >= 2) ORDER BY lls_id asc").fetch();
        if(!CollectionUtils.isEmpty(list)) {
            result.append(Messages.get("utiltiy.generate_key"));
            Metode metode = null;
            for(Lelang_seleksi lelang: list) {
                result.append(Messages.get("utility.gkl")).append(lelang.lls_id);
                Lelang_key.generateKey(lelang);
            }
        }else {
            result.append(Messages.get("utility.tidak_ada_kunci"));
        }
        renderText(result);
    }

    public static void generateKeyKms() {
        StringBuilder result = new StringBuilder();
        Date ttpDate = Configuration.getDate(CONFIG.AMANDA_START_DATE.category);
        List<Paket> list = Paket.find("ttp_ticket_id is NULL AND pkt_tgl_buat >= ? and pkt_flag >= 2 ORDER BY pkt_id asc", ttpDate).fetch();
        if(!CollectionUtils.isEmpty(list)) {
            result.append(Messages.get("utiltiy.generate_key"));
            for(Paket paket: list) {
                List<Lelang_seleksi> listLelang = Lelang_seleksi.find("pkt_id =? AND lls_status = 1 ORDER BY lls_versi_lelang DESC", paket.pkt_id).fetch();
                if(!CollectionUtils.isEmpty(listLelang)) {
                    Lelang_seleksi lelangItem = listLelang.get(0);
                    result.append(Messages.get("utility.generate_kms_kode_paket")).append(paket.pkt_id);
                    List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
                    OSDUtil.createKPL(paket, anggota_panitia, lelangItem, BasicCtr.getRequestHost());
                    paket = Paket.findById(paket.pkt_id);
                    result.append("<br />TTP Ticket Id: ").append(paket.ttp_ticket_id);
                }
            }
        }else {
            result.append(Messages.get("utility.tdk_ada_kms"));
        }
        renderText(result);
    }

    public static void generateKeyKmsPerLelang(Long kodeLelang) {
        StringBuilder result = new StringBuilder();
        List<Lelang_seleksi> listLelang = Lelang_seleksi.find("lls_id = ? AND lls_status = 1 AND pkt_id in (select pkt_id from paket where pkt_flag >= 2) ORDER BY lls_id DESC", kodeLelang).fetch();
        if(!CollectionUtils.isEmpty(listLelang)) {
            Lelang_seleksi lelang = listLelang.get(0);
            Paket paket = Paket.findByLelang(kodeLelang);
            if(paket != null && ConfigurationDao.isOSD(paket.pkt_tgl_buat)){
                if(paket.ttp_ticket_id != null){
                    result.append(Messages.get("utility.update_kunci_kms")).append(kodeLelang);
//					KmsClient.updateTTP(lelang, keyKPL, paket.getTtp_ticket_id(), paket);
                    List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
                    OSDUtil.createKPL(paket, anggota_panitia, lelang, BasicCtr.getRequestHost());
                } else
                    result.append(Messages.get("utility.takkyu"));
            }
        } else {
            result.append(Messages.get("utility.takkyu"));
        }
        renderText(result);
    }

    public static void getKplx(Long kodeLelang, Integer docId) {
        StringBuilder result = new StringBuilder();
        try {
            Paket paket = Paket.findByLelang(kodeLelang);
            Dok_penawaran.JenisDokPenawaran jenisDok = Dok_penawaran.JenisDokPenawaran.fromValue(docId);
            String kplx = KmsClient.getKPLX(paket.ttp_ticket_id, jenisDok.id);
            String decodedKplx = new String(Base64.decode(kplx));
            String ttpTicketId = paket.ttp_ticket_id;
            String pktId = decodedKplx.substring(decodedKplx.indexOf("<id_paket>") + 10, decodedKplx.indexOf("</id_paket>"));
            String namaPaket = decodedKplx.substring(decodedKplx.indexOf("<nama_paket>") + 12, decodedKplx.indexOf("</nama_paket>"));
            result.append("Ttp Ticket Id: ").append(ttpTicketId);
            result.append("<br/>Kode Paket: ").append(pktId);
            result.append("<br/>Nama Paket: ").append(namaPaket);

        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            result.append(errors.toString());
        }
       renderText(result);
    }

    public static void getKplxd(Long kodeLelang, Integer docId) {
        StringBuilder result = new StringBuilder();
        try {
            Paket paket = Paket.findByLelang(kodeLelang);
            Dok_penawaran.JenisDokPenawaran jenisDok = Dok_penawaran.JenisDokPenawaran.fromValue(docId);
            String kplxd = KmsClient.getKPLXd(jenisDok.id, paket.ttp_ticket_id);
            String decodedKplx = new String(Base64.decode(kplxd));
            String ttpTicketId = paket.ttp_ticket_id;
            String pktId = decodedKplx.substring(decodedKplx.indexOf("<id_paket>") + 10, decodedKplx.indexOf("</id_paket>"));
            String namaPaket = decodedKplx.substring(decodedKplx.indexOf("<nama_paket>") + 12, decodedKplx.indexOf("</nama_paket>"));
            result.append("Ttp Ticket Id: ").append(ttpTicketId);
            result.append(Messages.get("utility.kp")).append(pktId);
            result.append(Messages.get("utility.np")).append(namaPaket);

        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            result.append(errors.toString());
        }
        renderText(result);
    }

    public static void regenerateKeyKmsPerLelang(Long kodeLelang) {
        StringBuilder result = new StringBuilder();
        List<Lelang_seleksi> listLelang = Lelang_seleksi.find("lls_id = ? AND lls_status = 1 AND pkt_id in (select pkt_id from paket where pkt_flag >= 2) ORDER BY lls_id DESC").fetch();
        if(!CollectionUtils.isEmpty(listLelang)) {
            Lelang_seleksi lelang = listLelang.get(0);
            Paket paket = Paket.findByLelang(kodeLelang);
            if(paket != null && ConfigurationDao.isOSD(paket.pkt_tgl_buat)){
//				String keyKPL = kplDao.getKpl(paket.getPkt_id());
                if(paket.ttp_ticket_id !=null ) {
                    paket.ttp_ticket_id = null;
                    paket.save();
                    result.append(Messages.get("utility.update_kunci_kms")).append(kodeLelang);
//					epnsSecureFacade.updateTTP(lelang, keyKPL, paket.getTtp_ticket_id(), paket);
                    List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?",paket.pnt_id).fetch();
                    OSDUtil.createKPL(paket, anggota_panitia, lelang, BasicCtr.getRequestHost());
                } else
                    result.append(Messages.get("utility.takkyu"));
            }
        } else {
            result.append(Messages.get("utility.takkyu"));
        }
        renderText(result);
    }


    public static void rhsValidation() {
        String path = RhsFileValidation.class.getSimpleName() + ".class";
        InputStream is = RhsFileValidation.class.getResourceAsStream(path);
        renderBinary(is);
    }

    public static final String CLOUD="CLOUD-1.0";


    public static void inaprocCloudNotifier(String blob, Long psr_id){
        // parameter bisa dilihat di jobs.mirror.HomeLpseNotifierJob .doJob()
        Gson gson=new GsonBuilder().setDateFormat(DateFormat.FULL).create();
        BlobMetadata bmd=gson.fromJson(blob, BlobMetadata.class);
        /**Insert informasi blob
         *
         */
        BlobTable blobtable=new BlobTable();
        blobtable.blb_versi = -1; //minus 1 artinya berasal dari Cloud
        blobtable.blb_nama_file = bmd.blb_nama_file;
        blobtable.blb_hash = bmd.blb_hash;
        blobtable.blb_engine = CLOUD;
        blobtable.blb_ukuran = bmd.blb_ukuran;
        blobtable.blb_date_time = bmd.blb_date_time;
        String str=String.format("%s/%s/%s/%s", bmd.blb_cloud_url,  bmd.upload_token, bmd.blb_id_content, bmd.blb_versi);
        blobtable.blb_path = str;
        blobtable.save();


        Logger.debug(String.format("Insert blob_table from Cloud: blb_id_content=%s", blobtable.blb_id_content));

        //Insert dok penawaran
        //NOTE hanya boleh ada 1 dokumen per peserta per tipe
        Dok_penawaran.JenisDokPenawaran jenis= Dok_penawaran.JenisDokPenawaran.valueOf(bmd.blb_tipe);
        Dok_penawaran dok= Dok_penawaran.findPenawaranPeserta(psr_id, jenis);
    //		boolean isInsert=false;
        if(dok==null)
        {
            dok=new Dok_penawaran();
    //			isInsert=true;
            dok.dok_jenis = jenis;
        }
        dok.dok_disclaim = 1;
        dok.dok_hash = bmd.blb_hash;
        dok.dok_id_attachment = blobtable.blb_id_content;
        dok.dok_judul = bmd.blb_nama_file;
        dok.psr_id = psr_id;
        dok.dok_tgljam = bmd.blb_date_time;
        dok.save();

        Logger.debug(String.format("SPSE has been notified by %s,\n%s", psr_id, blob));
        renderText("OK");
    }

    public static void inaprocCloudDownload(String param) {
       /* String servletParam=req.getParameter("param");
        if(servletParam==null)
            return;
        servletParam=EncryptString.decrypt(servletParam);
        String[] ary=servletParam.split(";");
        Long blb_id_content=Long.parseLong(ary[0]);
        int blb_versi=Integer.parseInt(ary[1]);
        Blob_table blob_table=blob_tableDao.getBlobByPK(blb_id_content, blb_versi);
        HttpSession session = req.getSession();
        SessionInfo info= (SessionInfo) session.getAttribute(SessionVar.SESSION_INFO);
        if(info==null)
        {
            resp.setStatus(HttpStatus.FORBIDDEN.value());
            return;
        }
        Group[] grpArray=info.getGroups();
        StringBuilder strGroup= null;
        if(grpArray!=null)
            for(Group grp: grpArray)
            {
                if(strGroup==null)
                    strGroup = new StringBuilder(grp.name());
                else
                    strGroup.append(',').append(grp.name());
            }
        String blb_path=blob_table.getBlb_path();
        Matcher m= Pattern.compile("(.+mirror/download/)(.+)").matcher(blb_path);
        m.matches();
        String uploadTokenPlus=m.group(2);//upload_token/blb_id_content/blb_versi
        Long now=new Date().getTime();
        Long lpse_id=configurationDao.getRepoId();
        String cloudParam=String.format("%s/%s/%s/%s", uploadTokenPlus, now, lpse_id, strGroup.toString(),info.getUsername());
        String hash=DigestUtils.sha512Hex(servletParam);

        cloudParam=EncryptString.encryptTTS(cloudParam,"dp1InnyD2skXBMcS");
        String rhs=UUID.randomUUID().toString() + ".rhs";
        String finalUrl=String.format("%s%s/%s/%s", m.group(1), cloudParam, hash, rhs);
        try (SimpleHttpRequest simple = new SimpleHttpRequest(ContentPusat.CONNECTION_TIMEOUT)) {
            String urlForPrepare = m.group(1);
            urlForPrepare = urlForPrepare.substring(0, urlForPrepare.length() - 1) + "Prepare/" + cloudParam; // remove last slash
            simple.get(urlForPrepare);
            if (simple.getStatus() != 200) {
                throw new IOException(simple.getStatusText());
            } else {
                resp.sendRedirect(finalUrl);
            }
        }*/
    }
}

