package controllers.dce;

import jobs.adp.InisiasiJob;
import models.common.ConfigurationDao;
import models.sso.common.Repository;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.mvc.Controller;

/**
 * service REST untuk ADP , menggantikan WS-SOAP dengan XFire
 * @author arief ardiyansah
 *Ini hanya coding ulang apa yg di SPSE-3 dan belum dipakai di SPSE-4
 */
@Deprecated 
public class AdpServiceController extends Controller {
	
	public static void repository() {
		Logger.info("/repository ==> @AdpServiceController.repository");
		Repository repository = new Repository();
		repository.repo_id = ConfigurationDao.getRepoId();
		repository.repo_nama = ConfigurationDao.getNamaRepo();
		repository.repo_versi = ConfigurationDao.SPSE_VERSION;
		repository.repo_enable_adp = ConfigurationDao.isEnableInaproc()? 1:0;
		if(request.secure) 
			repository.repo_url = "https://"+request.host+Play.ctxPath;
		else
			repository.repo_url = "http://"+request.host+Play.ctxPath;
		String result = Json.toJson(repository);
		renderText(result);
	}
	public static void rekanan() throws Exception {
		new InisiasiJob().now();
		response.writeChunk("Inisiasi data sedang dilakukan");
	}
	public static void versi() {
		renderText(ConfigurationDao.SPSE_VERSION);
	}
}
