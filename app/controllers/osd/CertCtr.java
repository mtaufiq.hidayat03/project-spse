package controllers.osd;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.CONFIG;
import models.common.ConfigurationDao;
import models.common.Propinsi_kabupaten;
import models.jcommon.config.Configuration;
import models.jcommon.util.CommonUtil;
import models.osd.Certificate;
import models.osd.ErrorCodeOSD;
import models.osd.JenisCertificate;
import models.osd.UserInfo;
import models.rekanan.Rekanan;
import models.sso.common.Aktivasi;
import models.sso.common.Kabupaten;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import utils.osd.CertificateUtil;
import utils.osd.OSDUtil;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CertCtr extends BasicCtr {
	
	/**
	 * Service untuk mengirimkan informasi terkait user untuk kebutuhan certificate management
	 * Response parameters dalam bentuk JSON:
	 * - username
	 * - email
	 * - nama_pengguna
	 * - prov
	 * - kabkota
	 * - keyLength
	 * - sn
	 * - role
	 * - isOP
	 * - canRequestCSR
	 * - canRequestCSRRenew
	 * - lpseIdCertOrigin
	 * - lpseUrlCertOrigin
	 * - lpseNameCertOrigin
	 * - isRoaming
	 * - endDateCert
	 * - description
	 * @throws Exception
	 */
	/*@AllowAccess({Group.REKANAN, Group.PANITIA, Group.ADM_PPE, Group.ADM_AGENCY,
			Group.VERIFIKATOR, Group.AUDITOR, Group.PP, Group.PPK, Group.TRAINER,
			Group.KUPPBJ
	})*/
	public void userInfo() throws Exception {
		Active_user active_user = Active_user.current();
		if (active_user == null) {
			renderJSON("{\"status\":false,\"message\":\"Unauthorized user!\"}");
		}
		Date todayDate = new Date();
		UserInfo userInfo = new UserInfo();
		
		userInfo.lpseRepoId = String.valueOf(ConfigurationDao.getRepoId());		
		Map<String, Object> response = new HashMap<>();
		
		//isOP
		setIsOP(userInfo, active_user);

		//nama pengguna, email, kab/kota, prov, description
		setUserBasicInfo(userInfo, active_user);
		try {
			if(userInfo.error==null) {
				
				if (userInfo.cer_id != null){
					//lpseIdCertOrigin, lpseNameCertOrigin, lpseUrlCertOrigin, isRoaming
					Certificate certLast = OSDUtil.getLastCertificateFromInaproc(userInfo.cer_id);
					setCertBasicInfo(userInfo, certLast);
					if(certLast!=null){
						//sn
						userInfo.sn = certLast.getSerialNumberFromPEM();
						
						if (Arrays.asList(JenisCertificate.groupCert).contains(certLast.cer_jenis)){
							X509Certificate x509Certificate;
							try {
								x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certLast.cer_pem);
								if(x509Certificate!=null){
									Date endDateCert = x509Certificate.getNotAfter();
									Date dateNow = new Date();
									DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
									if (dateNow.before(endDateCert)){
										long diff = endDateCert.getTime() - dateNow.getTime();
										long daysLeft = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
										if (daysLeft > 30){
											userInfo.endDateCert = dateFormat.format(endDateCert);
										}
									}					
								}
							} catch (Exception e) {						
							}
						}
						
						if(!userInfo.isRoaming){
							//canRequestCSR, canRequestCSRRenew
							if (Arrays.asList(JenisCertificate.groupRevoked).contains(certLast.cer_jenis) 
									|| Arrays.asList(JenisCertificate.groupRejected).contains(certLast.cer_jenis)) {
								userInfo.canRequestCSR = true;
								userInfo.canRequestCSRRenew = false;
							} else if (Arrays.asList(JenisCertificate.groupCert).contains(certLast.cer_jenis)) {
								Date endValid = certLast.getExpiredDate();
								if(endValid != null){
									if(todayDate.after(endValid)){
										userInfo.canRequestCSR = true;
										userInfo.canRequestCSRRenew = false;
									} else {
										userInfo.canRequestCSR = false;
										userInfo.canRequestCSRRenew = true;
									}
								}
							}
						}
					} else {
						userInfo.error= ErrorCodeOSD.CERT_NOT_FOUND.getNotif();
					}
	
				} else {
					userInfo.canRequestCSR = true;
					userInfo.canRequestCSRRenew = false;
				}
			}
		} catch (Exception e) {
			Logger.error(e, "Error ambil user info dari ADP");
		}
		
		// TODO: isi sesuai data aktual
		response.put("username", active_user.userid);
		response.put("email", userInfo.email);
		response.put("nama_pengguna", userInfo.namaPengguna);
		response.put("prov", userInfo.provinsi);
		response.put("kabkota", userInfo.kabkota);
		response.put("keyLength", userInfo.keyLength);
		response.put("sn", userInfo.sn);
		response.put("role", active_user.group.getLabel());
		response.put("isOP", userInfo.op);
		response.put("canRequestCSR", userInfo.canRequestCSR);
		response.put("canRequestCSRRenew", userInfo.canRequestCSRRenew);
		response.put("lpseIdCertOrigin", userInfo.lpseIdCertOrigin);
		response.put("lpseUrlCertOrigin", userInfo.lpseUrlCertOrigin);
		response.put("lpseNameCertOrigin", userInfo.lpseNameCertOrigin);
		response.put("isRoaming", userInfo.isRoaming);
		response.put("endDateCert", userInfo.endDateCert);
		response.put("description", userInfo.description);

		if (active_user.isAmsExist()) {
			if (active_user.getAms().getId() != null) {
				response.put("ams_id", active_user.getAms().getId());
			}
			if (!CommonUtil.isEmpty(active_user.getAms().getOcspStatus())) {
				response.put("ocspStatus", active_user.getAms().getOcspStatus());
			}
		}

		if(userInfo.error!=null) {
			response.put("error", userInfo.error);
		}
		renderJSON(response);
	} 
	
	private void setIsOP(UserInfo userInfo, Active_user active_user){
		if(active_user.isAdminPPE() || active_user.isAdminAgency() || active_user.isVerifikator()) {
			userInfo.op = true;
		}
	}
	
	private void setUserBasicInfo(UserInfo userInfo, Active_user active_user) throws Exception{
		if (active_user.isRekanan()) {
			//Jika versi prod, data rekanan harus ambil dari ADP
			//Jika belum ADP atau tidak dapat koneksi ke ADP maka return null
			Rekanan rekanan = null;
			rekanan = Rekanan.findById(active_user.rekananId);
			if(rekanan!=null){
				userInfo.namaPengguna = rekanan.rkn_nama;
				userInfo.email = rekanan.rkn_email;
				Kabupaten kab = rekanan.getKabupaten();
				userInfo.kabkota = Propinsi_kabupaten.transformKabupatenName(kab.kbp_nama);
				userInfo.provinsi = kab.getPropinsi().prp_nama;
				userInfo.cer_id = rekanan.cer_id;
				if(rekanan.rkn_statcabang != null)
					userInfo.description = rekanan.rkn_statcabang.equals(Rekanan.PUSAT) ? "Pusat":"Cabang";
				if (ConfigurationDao.isProduction()) {
					Aktivasi aktivasi = Aktivasi.find("rkn_id=?", rekanan.rkn_id).first();
					if(!aktivasi.aktivasi_status.equalsIgnoreCase("ACTIVATED"))
						userInfo.error = ErrorCodeOSD.USER_NOT_ADP.getNotif();
						
				}
			}
//			if (ConfigurationDao.isEnableInaproc()) {
//				String param = URLs.encodePart(DceSecurityV2.encrypt(active_user.rekananId.toString()));
//				String url_service = ConfigurationDao.getRestCentralService() + "/rekanan?q=" + param;
//				String content = WS.url(url_service).timeout("5min").get().getString();
//				String response = DceSecurityV2.decrypt(content);
//				if (!StringUtils.isEmpty(response)) { // jika tidak ada error
//					RekananSSO userRekanan = CommonUtil.fromJson(response, RekananSSO.class);
//					userInfo.namaPengguna = userRekanan.rkn_nama;
//					userInfo.email = userRekanan.rkn_email;
//					Kabupaten kab = Kabupaten.findById(userRekanan.kbp_id);
//					userInfo.kabkota = transformKabupaten(kab.kbp_nama);
//					userInfo.provinsi = kab.getPropinsi().prp_nama;
//					userInfo.cer_id = userRekanan.cer_id;
//					if (userRekanan.rkn_statcabang.equals(Rekanan.PUSAT))
//						userInfo.description = "Pusat";
//					else 
//						userInfo.description = "Cabang";
//					
//				}
//			} else {
//				rekanan = Rekanan.findByNamaUser(active_user.userid);
//				if(rekanan!=null){
//					userInfo.namaPengguna = rekanan.rkn_nama;
//					userInfo.email = rekanan.rkn_email;
//					userInfo.kabkota = transformKabupaten(rekanan.getKabupaten().kbp_nama);
//					userInfo.provinsi = rekanan.getKabupaten().getPropinsi().prp_nama;
//					userInfo.cer_id = rekanan.cer_id;
//					if (rekanan.rkn_statcabang.equals(Rekanan.PUSAT))
//						userInfo.description = "Pusat";
//					else 
//						userInfo.description = "Cabang";
//				}
//			}
			//namaPengguna = rekanan.rkn_nama.replaceAll(",", ".");
		} else {
			if(!active_user.isAdminPPE()){
				Pegawai pegawai = Pegawai.findBy(active_user.userid);
				//namaPengguna = ConfigurationDao.getNamaRepo();
				//email = pegawai.peg_email == null ? "-" : pegawai.peg_email;
				if(pegawai.agc_id != null) {
					Kabupaten kabupaten = pegawai.getAgency().getKabupaten();
					userInfo.kabkota = Propinsi_kabupaten.transformKabupatenName(kabupaten.kbp_nama);
					userInfo.provinsi = kabupaten.getPropinsi().prp_nama;
				} else {
					long kbp_id = Configuration.getLong(CONFIG.KABUPATEN.category);
					Kabupaten kab = Kabupaten.findById(kbp_id);
					userInfo.kabkota = Propinsi_kabupaten.transformKabupatenName(kab.kbp_nama);
					userInfo.provinsi = kab.getPropinsi().prp_nama;
				}
				//kabkota = pegawai.agc_id == null ? "-" : pegawai.getAgency().getKabupaten().kbp_nama;
				//provinsi = pegawai.agc_id == null ? "-" : pegawai.getAgency().getKabupaten().getPropinsi().prp_nama;
				userInfo.namaPengguna = pegawai.peg_nama;
				userInfo.email = pegawai.peg_email;
				userInfo.cer_id = pegawai.cer_id;
			}
		}
	}
	
	private void setCertBasicInfo(UserInfo userInfo, Certificate certificate) throws IOException {
		if(certificate != null) {
			userInfo.lpseIdCertOrigin = String.valueOf(certificate.repoId);
			JsonObject json = OSDUtil.getServiceUrl(userInfo.lpseIdCertOrigin);
			if(json!=null) {
				userInfo.lpseNameCertOrigin = json.get("repo_nama").toString().replace("\"", "");
				userInfo.lpseUrlCertOrigin = json.get("repo_url").toString().replace("\"", "");
			}
			if(!StringUtils.isEmpty(userInfo.lpseIdCertOrigin)) {
				if (!userInfo.lpseIdCertOrigin.equals(userInfo.lpseRepoId))
					userInfo.isRoaming = true;
				else
					userInfo.isRoaming = false;
			}
		}
	}
}