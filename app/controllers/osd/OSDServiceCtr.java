package controllers.osd;

import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.*;
import models.common.Active_user;
import models.common.CONFIG;
import models.common.ConfigurationDao;
import models.jcommon.config.Configuration;
import models.lelang.Lelang_seleksi;
import models.osd.Certificate;
import models.osd.ErrorCodeOSD;
import models.osd.JenisCertificate;
import models.rekanan.Rekanan;
import models.secman.Group;
import org.apache.commons.collections4.CollectionUtils;
import org.bouncycastle.util.encoders.Hex;
import play.Logger;
import play.mvc.Controller;
import utils.osd.CertificateUtil;
import utils.osd.OSDUtil;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

public class OSDServiceCtr extends Controller{

	public static Long DEFAULT_SIGNATURE = new Long(0);

	public static String OPT_DOK_LELANG_PRA = "1";
	public static String OPT_DOK_LELANG_PASCA = "2";
	public static String OPT_DOK_LELANG_ADENDUM = "3";
	
	
	private static ErrorCodeOSD certMutualVerification() {
//		TODO disable sementara, untuk testing menggunakan fake sertifikat
//		if(UserContext.getCertificate()!=null)
//			return CertificateFacade.checkCertificate(UserContext.getCertificate());
//		return ErrorCodeOSD.CERT_UNTRUSTED;
//		return null;
		String verifiedHeader = request.headers.get("x-tls-client-verified").value();
		if(verifiedHeader != null && verifiedHeader.equalsIgnoreCase("success")) {
			return null;
		} else {
			return ErrorCodeOSD.CERT_UNTRUSTED;
		}
	}

	/**
	 * Service untuk mendapatkan list CSRR. Tidak membutuhkan parameter request.
	 * Response dalam bentu plain:
	 * {"csrrs":[{"userid":1,"username":"john","group":"Penyedia","identity":"1111111","type":"Baru"}]} atau
	 * {"error":"error code"}
	 */
	@AllowAccess({ Group.VERIFIKATOR, Group.ADM_PPE, Group.ADM_AGENCY})
	public static void getListCSRR() {
		Logger.trace("getListCSRR");
		Map<String, Object> responseObj = new HashMap<>();
		ErrorCodeOSD err = certMutualVerification(); 
		if(err!=null) {
			responseObj.put("error", err.getCode());
			renderJSON(responseObj);
		}
		Active_user user = Active_user.current();
		Group group = user.group;
		Pegawai op = Pegawai.findBy(user.userid);
		if(group.isVerifikator()||group.isAdminAgency()){
			if(!OSDUtil.isOpAllowed(op)) {
				responseObj.put("error", ErrorCodeOSD.CERT_UNTRUSTED.getCode());
				renderJSON(responseObj);
			}
		}

		List ret = new ArrayList<>();
		if(group.isVerifikator()){			
			String lsCerid = null;
			List<Rekanan> listEEPenyedia = new ArrayList<Rekanan>();
			List<Rekanan> listEEPenyediaRenew = new ArrayList<Rekanan>();
			
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_PENYEDIA);
			if(lsCerid != null)
				listEEPenyedia = Rekanan.find("cer_id in ("+lsCerid+ ')').fetch();
			
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_PENYEDIA_RENEW);
			if(lsCerid != null)
				listEEPenyediaRenew = Rekanan.find("cer_id in ("+lsCerid+ ')').fetch();
			
			
			int totalList = listEEPenyedia.size()+listEEPenyediaRenew.size();
			
			if(totalList==0){
				responseObj.put("error", ErrorCodeOSD.CSRR_NOT_FOUND.getCode());
			} else {
				if(listEEPenyedia.size()>0){						
					for(Iterator<Rekanan> i = listEEPenyedia.listIterator();i.hasNext();){
						Rekanan rekanan = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", rekanan.rkn_id.toString());
						obj.put("username", rekanan.rkn_nama);
						obj.put("group", Group.REKANAN.getLabel());
						obj.put("identity", rekanan.rkn_npwp);
						obj.put("type", JenisCertificate.NEW_VALUE);
						obj.put("description", rekanan.rkn_statcabang == null || rekanan.rkn_statcabang.equals("P") ? "Pusat" : "Cabang");
						ret.add(obj);
					}
				}
				
				if(listEEPenyediaRenew.size()>0){
					for(Iterator<Rekanan> i = listEEPenyediaRenew.listIterator();i.hasNext();){
						Rekanan rekanan = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", rekanan.rkn_id.toString());
						obj.put("username", rekanan.rkn_nama);
						obj.put("group", Group.REKANAN.getLabel());
						obj.put("identity", rekanan.rkn_npwp);
						obj.put("type", JenisCertificate.RENEW_VALUE);
						obj.put("description", rekanan.rkn_statcabang == null || rekanan.rkn_statcabang.equals("P") ? "Pusat" : "Cabang");
						ret.add(obj);
					}
				}
				
				responseObj.put("csrrs", ret);
			}

		} else if(group.isAdminAgency()) {
			List<Admin_agency> agcIdList = Admin_agency.find("peg_id=?", op.peg_id).fetch();
			if(agcIdList.size()==0){
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			} else {
				List<Pegawai> listEEPanitiaAll = new ArrayList<Pegawai>();
				List<String> listEEPanitiaCertJenisAll = new ArrayList<String>();
				
				for(Iterator<Admin_agency> i = agcIdList.listIterator();i.hasNext();){
					Agency agency = Agency.findById(i.next().agc_id);
					if(agency!=null){
						String lsCerid = null;
						List<Pegawai> listEEPanitia = new ArrayList<Pegawai>();
						lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_PANITIA);
						if(lsCerid != null)
							listEEPanitia = Pegawai.getPegawaiByCertIds(lsCerid, agency.agc_id);
						
						if(listEEPanitia.size()>0){
							for(Iterator<Pegawai> p = listEEPanitia.listIterator();p.hasNext();){
								Pegawai pegawai = p.next();
								listEEPanitiaAll.add(pegawai);
								listEEPanitiaCertJenisAll.add(JenisCertificate.NEW_VALUE);
							}
						}
						
						List<Pegawai> listEEPanitiaRenew = new ArrayList<Pegawai>();							
						lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_PANITIA_RENEW);
						if(lsCerid != null){
							listEEPanitiaRenew = Pegawai.getPegawaiByCertIds(lsCerid, agency.agc_id);
						}
						if(listEEPanitiaRenew.size()>0){
							for(Iterator<Pegawai> p = listEEPanitiaRenew.listIterator();p.hasNext();){
								Pegawai pegawai = p.next();
								listEEPanitiaAll.add(pegawai);
								listEEPanitiaCertJenisAll.add(JenisCertificate.RENEW_VALUE);
							}
						}
					}
				}
				if(listEEPanitiaAll.size()>0){
					int index = 0;
					for(Iterator<Pegawai> i = listEEPanitiaAll.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.PANITIA.getLabel());
						obj.put("identity", pegawai.peg_nip);
						obj.put("type", listEEPanitiaCertJenisAll.get(index));
						obj.put("description", "");
						ret.add(obj);
						index++;
					}
					
					responseObj.put("csrrs", ret);
				} else {
					responseObj.put("error", ErrorCodeOSD.CSRR_NOT_FOUND.getCode());
				}
			}
		} else if(group.isAdminPPE()) {
			List<Pegawai> listEEVerifikator = new ArrayList<Pegawai>();
			List<Pegawai> listEEAdminAgency = new ArrayList<Pegawai>();
			List<Pegawai> listEEVerifikatorRenew = new ArrayList<Pegawai>();
			List<Pegawai> listEEAdminAgencyRenew = new ArrayList<Pegawai>();				
			String lsCerid = null;

			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_VERIFIKATOR);
			if(lsCerid != null)
				listEEVerifikator = Pegawai.getPegawaiByCertIds(lsCerid, null);
			
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_VERIFIKATOR_RENEW);
			if(lsCerid != null)
				listEEVerifikatorRenew = Pegawai.getPegawaiByCertIds(lsCerid, null);
			
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_ADMIN_AGENCY);
			if(lsCerid != null)
				listEEAdminAgency = Pegawai.getPegawaiByCertIds(lsCerid, null);

			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.CSRR_ADMIN_AGENCY_RENEW);
			if(lsCerid != null)
				listEEAdminAgencyRenew = Pegawai.getPegawaiByCertIds(lsCerid, null);
			
			
			int totalList = listEEVerifikator.size()+listEEAdminAgency.size()
					+listEEVerifikatorRenew.size()+listEEAdminAgencyRenew.size();
			if(totalList>0){
				if(listEEVerifikator.size()>0){
					for(Iterator<Pegawai> i = listEEVerifikator.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.VERIFIKATOR.getLabel());
						obj.put("identity", pegawai.peg_nip);
						obj.put("type", JenisCertificate.NEW_VALUE);
						obj.put("description", "");
						ret.add(obj);
					}
				}
				if(listEEAdminAgency.size()>0){
					for(Iterator<Pegawai> i = listEEAdminAgency.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.ADM_AGENCY.getLabel());
						obj.put("identity", pegawai.peg_nip);
						obj.put("type", JenisCertificate.NEW_VALUE);
						obj.put("description", "");
						ret.add(obj);
					}
				}
				
				if(listEEVerifikatorRenew.size()>0){
					for(Iterator<Pegawai> i = listEEVerifikatorRenew.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.VERIFIKATOR.getLabel());
						obj.put("identity", pegawai.peg_nip);
						obj.put("type", JenisCertificate.RENEW_VALUE);
						obj.put("description", "");
						ret.add(obj);
					}
				}
				if(listEEAdminAgencyRenew.size()>0){
					for(Iterator<Pegawai> i = listEEAdminAgencyRenew.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.ADM_AGENCY.getLabel());
						obj.put("identity", pegawai.peg_nip);
						obj.put("type", JenisCertificate.RENEW_VALUE);
						obj.put("description", "");
						ret.add(obj);
					}
				}
				responseObj.put("csrrs", ret);
			} else {
				responseObj.put("error", ErrorCodeOSD.CSRR_NOT_FOUND.getCode());
			}
		}
		renderJSON(responseObj);
	}

	/**
	 * Service untuk menarik detil record sebuah permintaan CSR. Butuh request parameter "id".
	 * Response dalam bentu plain:
	 * {"csrr":{"pem":"AAAAAAAAAAAAAAA","username":"john","profileName":"LPSE-PENYEDIA-PROFILE"}} atau
	 * {"error":"error code"}  
	 */
	@AllowAccess({ Group.VERIFIKATOR, Group.ADM_PPE, Group.ADM_AGENCY})
	public static void getCSRR() {
		Logger.trace("getCSRR");
		Map<String, Object> responseObj = new HashMap<>();
		ErrorCodeOSD err = certMutualVerification(); 
		if(err!=null) {
			responseObj.put("error", err.getCode());
			renderJSON(responseObj);
		}
		String idEE = params.get("id");
		Active_user user = Active_user.current();
		Group group = user.group;
		Pegawai op = Pegawai.findBy(user.userid);
//			check validitas --> sementara
		if(group.isVerifikator()||group.isAdminAgency()){
			if(!OSDUtil.isOpAllowed(op)) {
				responseObj.put("error", ErrorCodeOSD.CERT_UNTRUSTED.getCode());
				renderJSON(responseObj);
			}
		}
		
		Certificate certificate;
		Map<String, Object> ret = new HashMap<>();
		if(group.isVerifikator()){
			Rekanan rekanan = (Rekanan) Rekanan.findById(Long.valueOf(idEE));
			if(rekanan!=null && rekanan.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(rekanan.cer_id);
				if(certificate!=null) {
					if(certificate.cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA) || certificate.cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA_RENEW)){
						//jika setelah diupgrade masih ada request sertifikat yg pending
						if(certificate.csr_pem==null)
							ret.put("pem", certificate.cer_pem);
						else
							ret.put("pem", certificate.csr_pem);
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_PENYEDIA));
						ret.put("username", certificate.cer_username);
						responseObj.put("csrr", ret);
					}
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminAgency()) {
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null) {
					//jika setelah diupgrade masih ada request sertifikat yg pending
					if(certificate.csr_pem==null)
						ret.put("pem", certificate.cer_pem);
					else
						ret.put("pem", certificate.csr_pem);
					ret.put("username", certificate.cer_username);
					if(certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA) || certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA_RENEW)){
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_PANITIA));
					}
					responseObj.put("csrr", ret);
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		}
		else if(group.isAdminPPE()) {
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null) {
					//jika setelah diupgrade masih ada request sertifikat yg pending
					if(certificate.csr_pem==null)
						ret.put("pem", certificate.cer_pem);
					else
						ret.put("pem", certificate.csr_pem);
					ret.put("username", certificate.cer_username);
					if(certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA) || certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA_RENEW)){
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_PANITIA));
					}
					if(certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY) || certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY_RENEW)){
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_ADMIN_AGENCY));
					} else if(certificate.cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR) || certificate.cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR_RENEW)){
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_VERIFIKATOR));
					}
					responseObj.put("csrr", ret);
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		}
		renderJSON(responseObj);
	}

//	gunakan sertifikat beneran untuk testing method ini
	/**
	 * Service untuk mengubah status permintaan CSR. Parameter request yang dibutuhkan:
	 * - id: string
	 * - pem: string
	 * - status: integer
	 * Response dalam bentu plain
	 * {"status":"error/success code"}  
	 */
	@AllowAccess({ Group.VERIFIKATOR, Group.ADM_PPE, Group.ADM_AGENCY})
	public static void setCSRStatus() throws Exception {
		Logger.trace("setCSRStatus");
		Map<String, Object> responseObj = new HashMap<>();
		ErrorCodeOSD err = certMutualVerification(); 
		if(err!=null) {
			responseObj.put("status", err.getCode());
			renderJSON(responseObj);
		}
		String idEE = params.get("id");
		int status = Integer.parseInt(params.get("status"));
		String pem = params.get("pem");
    	int APPROVED = 0;
    	Active_user user = Active_user.current();
		Group group = user.group;
		Pegawai op = Pegawai.findBy(user.userid);
//			check validitas --> sementara
		if(group.isVerifikator()||group.isAdminAgency()){
			if(!OSDUtil.isOpAllowed(op)) {
				responseObj.put("status", ErrorCodeOSD.CERT_UNTRUSTED.getCode());
				renderJSON(responseObj);
			}
		}

		Certificate certificate = null;
		if(group.isVerifikator()){
			Rekanan rekanan = (Rekanan) Rekanan.findById(Long.valueOf(idEE));
			if(rekanan!=null && rekanan.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(rekanan.cer_id);
				if(certificate!=null){
					if (certificate.cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA)
							|| certificate.cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA_RENEW)){
						if(status==APPROVED){
							certificate.cer_jenis = JenisCertificate.CERT_PENYEDIA;
							certificate.cer_pem = pem;
						} else
							certificate.cer_jenis = JenisCertificate.CSR_PENYEDIA_REJECTED;
					}	else {
						Logger.info("Update CSRR Status failed, no CSRR OR CSR-Renew for rekanan with ID : " + idEE + '(' + rekanan.rkn_nama + ')');
						responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
					}
				} else {
					Logger.info("Update CSRR Status failed, no CSRR OR CSR-Renew for rekanan with ID : " + idEE + '(' + rekanan.rkn_nama + ')');
					responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
				}
			} else {
				responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminAgency()){
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null){
					if (certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA)
							|| certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA_RENEW)){
						if(status==APPROVED){
							certificate.cer_jenis = JenisCertificate.CERT_PANITIA;
							certificate.cer_pem = pem;
						} else
							certificate.cer_jenis = JenisCertificate.CSR_PANITIA_REJECTED;
					}	else {
						Logger.info("Update CSRR Status failed, no CSRR OR CSR-Renew for panitia with ID : " + idEE + '(' + pegawai.peg_nama + ')');
						responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
					}
				} else {
					Logger.info("Update CSRR Status failed, no CSRR OR CSR-Renew for panitia with ID : " + idEE + '(' + pegawai.peg_nama + ')');
					responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
				}
			} else {
				responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminPPE()){
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null){
					if(certificate.cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR)
							|| certificate.cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR_RENEW)){
						if(status==APPROVED){
							certificate.cer_jenis = JenisCertificate.CERT_VERIFIKATOR;
							certificate.cer_pem = pem;
						} else
							certificate.cer_jenis = JenisCertificate.CSR_VERIFIKATOR_REJECTED;
					} else if(certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY)
							|| certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY_RENEW)){
						if(status==APPROVED){
							certificate.cer_jenis = JenisCertificate.CERT_ADMIN_AGENCY;
							certificate.cer_pem = pem;
						} else
							certificate.cer_jenis = JenisCertificate.CSR_ADMIN_AGENCY_REJECTED;
					} else {
						Logger.info("Update CSRR Status failed, no CSRR OR CSR-Renew for Verifikator/Admin Agency with ID : " + idEE + '(' + pegawai.peg_nama + ')');
						responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
					}
				} else
					responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			} else
				responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
		}
		if(responseObj.isEmpty()) { // hanya lanjut jika pengecekan di atas sukses (belum ada error status)
			Long cer_id = OSDUtil.updateCertificatePemToInaproc(certificate, user.userid, BasicCtr.newDate());
			if(cer_id==null)
				responseObj.put("status", ErrorCodeOSD.FAILED.getCode());
			else
				responseObj.put("status", ErrorCodeOSD.SUCCEEDED.getCode());
		}
		renderJSON(responseObj);
	}

	/**
	 * Service untuk mendapatkan list CRR (revoke). Tidak membutuhkan parameter request.
	 * Response dalam bentu plain:
	 * {"crrs":[{"userid":1,"username":"john","group":"Penyedia","identity":"1111111"}]} atau
	 * {"error":"error code"}
	 */
	@AllowAccess({ Group.VERIFIKATOR, Group.ADM_PPE, Group.ADM_AGENCY})
	public static void getListCRR() throws Exception {
		Logger.trace("getListCRR");
		Map<String, Object> responseObj = new HashMap<>();
		ErrorCodeOSD err = certMutualVerification(); 
		if(err!=null) {
			responseObj.put("status", err.getCode());
			renderJSON(responseObj);
		}
		Active_user user = Active_user.current();
		Group group = user.group;
		Pegawai op = Pegawai.findBy(user.userid);
		if(group.isVerifikator()||group.isAdminAgency()){
			if(!OSDUtil.isOpAllowed(op)) {
				responseObj.put("error", ErrorCodeOSD.CERT_UNTRUSTED.getCode());
				renderJSON(responseObj);
			}
		}

		List ret = new ArrayList<>();
		
		if(group.isVerifikator()){
			List<Rekanan> listEEPenyedia = new ArrayList<Rekanan>();
			String lsCerid = null;
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.REVOKE_REQ_PENYEDIA);
			if(lsCerid != null)
				listEEPenyedia = Rekanan.find("cer_id in ("+lsCerid+ ')').fetch();
			
			if(listEEPenyedia.size()>0){
				for(Iterator<Rekanan> i = listEEPenyedia.listIterator();i.hasNext();){
					Rekanan rekanan = i.next();
					Map obj = new HashMap<>();
					obj.put("userid", rekanan.rkn_id.toString());
					obj.put("username", rekanan.rkn_nama);
					obj.put("group", Group.REKANAN.getLabel());
					obj.put("identity", rekanan.rkn_npwp);
					ret.add(obj);
				}
				responseObj.put("crrs", ret);
			} else {
				responseObj.put("error", ErrorCodeOSD.CRR_NOT_FOUND.getCode());
			}
		} 
		else if(group.isAdminAgency()) {
			List<Admin_agency> agcIdList = Admin_agency.find("peg_id=?", op.peg_id).fetch();
			if(agcIdList.size()>0) {
				List<Pegawai> listEEPanitiaAll = new ArrayList<Pegawai>();
				for(Iterator<Admin_agency> i = agcIdList.listIterator();i.hasNext();){
					Agency agency = Agency.findById(i.next().agc_id);
					if(agency!=null){
//							List<Pegawai> listEEPanitia = Pegawai.getPegawaiByCertJenis(JenisCertificate.REVOKE_REQ_PANITIA, agency.agc_id);
						List<Pegawai> listEEPanitia = new ArrayList<Pegawai>();
						String lsCerid = null;
						lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.REVOKE_REQ_PANITIA);
						if(lsCerid != null)
							listEEPanitia = Pegawai.getPegawaiByCertIds(lsCerid, agency.agc_id);
						
						if(listEEPanitia.size()>0){
							listEEPanitiaAll.addAll(listEEPanitia);
						}
					}
				}
				if(listEEPanitiaAll.size()>0){
					for(Iterator<Pegawai> i = listEEPanitiaAll.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.PANITIA.getLabel());
						obj.put("identity", pegawai.peg_nip);
						ret.add(obj);
					}
					responseObj.put("crrs", ret);
				} else {
					responseObj.put("error", ErrorCodeOSD.CRR_NOT_FOUND.getCode());
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminPPE()) {
			List<Pegawai> listEEVerifikator = new ArrayList<Pegawai>();
			List<Pegawai> listEEAdminAgency = new ArrayList<Pegawai>();
			
			String lsCerid = null;
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.REVOKE_REQ_VERIFIKATOR);
			if(lsCerid != null)
				listEEVerifikator = Pegawai.getPegawaiByCertIds(lsCerid, null);
			
			lsCerid = OSDUtil.getCertIdFromInaproc(JenisCertificate.REVOKE_REQ_ADMIN_AGENCY);
			if(lsCerid != null)
				listEEAdminAgency = Pegawai.getPegawaiByCertIds(lsCerid, null);
			
			if(listEEVerifikator.size()+listEEAdminAgency.size()>0){
				if(listEEVerifikator.size()>0){
					for(Iterator<Pegawai> i = listEEVerifikator.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.VERIFIKATOR.getLabel());
						obj.put("identity", pegawai.peg_nip);
						ret.add(obj);
					}
				}
				if(listEEAdminAgency.size()>0){
					for(Iterator<Pegawai> i = listEEAdminAgency.listIterator();i.hasNext();){
						Pegawai pegawai = i.next();
						Map obj = new HashMap<>();
						obj.put("userid", pegawai.peg_id.toString());
						obj.put("username", pegawai.peg_nama);
						obj.put("group", Group.ADM_AGENCY.getLabel());
						obj.put("identity", pegawai.peg_nip);
						ret.add(obj);
					}
				}
				responseObj.put("crrs", ret);
			} else {
				responseObj.put("error", ErrorCodeOSD.CRR_NOT_FOUND.getCode());
			}
		}
	}

	/**
	 * Service untuk menarik detil record sebuah permintaan CRR (revoke). Butuh request parameter "id".
	 * Response dalam bentu plain:
	 * {"crr":{"pem":"AAAAAAAAAAAAAAA","username":"john","profileName":"LPSE-PENYEDIA-PROFILE"}} atau
	 * {"error":"error code"}  
	 */
	@AllowAccess({ Group.VERIFIKATOR, Group.ADM_PPE, Group.ADM_AGENCY})
	public static void getCRR() throws Exception {
		Logger.trace("getCRR");
		Map<String, Object> responseObj = new HashMap<>();
		ErrorCodeOSD err = certMutualVerification(); 
		if(err!=null) {
			responseObj.put("status", err.getCode());
			renderJSON(responseObj);
		}

		String idEE = params.get("id");
		Active_user user = Active_user.current();
		Group group = user.group;
		Pegawai op = Pegawai.findBy(user.userid);
//			check validitas --> sementara
		if(group.isVerifikator()||group.isAdminAgency()){
			if(!OSDUtil.isOpAllowed(op)) {
				responseObj.put("error", ErrorCodeOSD.CERT_UNTRUSTED.getCode());
				renderJSON(responseObj);
			}
		}
		Certificate certificate;
		Map<String, Object> ret = new HashMap<>();
		if(group.isVerifikator()){
			Rekanan rekanan = (Rekanan) Rekanan.findById(Long.valueOf(idEE));
			if(rekanan!=null && rekanan.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(rekanan.cer_id);
				if (certificate!=null){
					if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PENYEDIA) 
							|| certificate.cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA_RENEW)){
						ret.put("sn", certificate.cer_sn);
						if(certificate.cer_pem!=null){
							try {
								X509Certificate x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
								ret.put("sn", new String(Hex.encode(x509Certificate.getSerialNumber().toByteArray())).toUpperCase());
							} catch (CertificateException e) {
								e.printStackTrace();
							}
						}
						ret.put("username", certificate.cer_username);
						ret.put("issuerDn", certificate.cer_issuer_dn);
						ret.put("issuerCn", certificate.getCer_issuer_cn());
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_PENYEDIA));
						ret.put("subjectDn", certificate.getCer_subject_dn());
						responseObj.put("crr", ret);
					}
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminAgency()) {
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate != null) {
					if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PANITIA)
							 || certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA_RENEW)){
						ret.put("sn", certificate.cer_sn);
						if(certificate.cer_pem!=null){
							try {
								X509Certificate x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
								ret.put("sn", new String(Hex.encode(x509Certificate.getSerialNumber().toByteArray())).toUpperCase());
							} catch (CertificateException e) {
								e.printStackTrace();
							}
						}
						ret.put("username", certificate.cer_username);
						ret.put("issuerDn", certificate.cer_issuer_dn);
						ret.put("issuerCn", certificate.getCer_issuer_cn());
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_PANITIA));
						ret.put("subjectDn", certificate.getCer_subject_dn());
						responseObj.put("crr", ret);
					}
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminPPE()) {
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate != null) {
					ret.put("sn", certificate.cer_sn);
					if(certificate.cer_pem!=null){
						try {
							X509Certificate x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
							ret.put("sn", new String(Hex.encode(x509Certificate.getSerialNumber().toByteArray())).toUpperCase());
						} catch (CertificateException e) {
							e.printStackTrace();
						}
					}
					ret.put("username", certificate.cer_username);
					ret.put("issuerDn", certificate.cer_issuer_dn);
					ret.put("issuerCn", certificate.getCer_issuer_cn());
					if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_AGENCY)
							 || certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY_RENEW)){
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_ADMIN_AGENCY));
					} else if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_VERIFIKATOR)
							 || certificate.cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR_RENEW)){
						ret.put("profileName", OSDUtil.getEjbcaProfileName(JenisCertificate.PROFILE_VERIFIKATOR));
					}
					ret.put("subjectDn", certificate.getCer_subject_dn());
					responseObj.put("crr", ret);
				}
			} else {
				responseObj.put("error", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		}
	}

	/**
	 * Service untuk mengubah status permintaan CRR (revoke). Parameter request yang dibutuhkan:
	 * - id: string
	 * - status: integer
	 * Response dalam bentu plain
	 * {"status":"error/success code"}  
	 */
	@AllowAccess({ Group.VERIFIKATOR, Group.ADM_PPE, Group.ADM_AGENCY})
	public static void setCRRStatus() throws Exception {
		Logger.trace("setCRRStatus");
		Map<String, Object> responseObj = new HashMap<>();
		ErrorCodeOSD err = certMutualVerification(); 
		if(err!=null) {
			responseObj.put("status", err.getCode());
			renderJSON(responseObj);
		}
		String idEE = params.get("id");
		int status = Integer.parseInt(params.get("status"));
    	int APPROVED = 0;
    	Active_user user = Active_user.current();
		Group group = user.group;
		Pegawai op = Pegawai.findBy(user.userid);
//			check validitas --> sementara
		if(group.isVerifikator()||group.isAdminAgency()){
			if(!OSDUtil.isOpAllowed(op)) {
				responseObj.put("status", ErrorCodeOSD.CERT_UNTRUSTED.getCode());
				renderJSON(responseObj);
			}
		}
			
		Certificate certificate = null;
		if(group.isVerifikator()){
			Rekanan rekanan = (Rekanan) Rekanan.findById(Long.valueOf(idEE));
			if(rekanan!=null && rekanan.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(rekanan.cer_id);
				if(certificate!=null && certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PENYEDIA)){
					if(status==APPROVED)
						certificate.cer_jenis = JenisCertificate.REVOKED_PENYEDIA;
				} else {
					responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
				}
			} else {
				responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminAgency()){
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null && certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PANITIA)){
					if(status==APPROVED)
						certificate.cer_jenis = JenisCertificate.REVOKED_PANITIA;
				} else {
					responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
				}
			} else {
				responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else if(group.isAdminPPE()){
			Pegawai pegawai = (Pegawai) Pegawai.findById(Long.valueOf(idEE));
			if(pegawai!=null && pegawai.cer_id!=null){
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null){
					if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_VERIFIKATOR)){
						if(status==APPROVED)
							certificate.cer_jenis = JenisCertificate.REVOKED_VERIFIKATOR;
					} else if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_AGENCY)){
						if(status==APPROVED)
							certificate.cer_jenis = JenisCertificate.REVOKED_ADMIN_AGENCY;
					} else {
						responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
					}
				} else {
					responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
				}
			} else {
				responseObj.put("status", ErrorCodeOSD.DATA_NOT_FOUND.getCode());
			}
		} else {
			responseObj.put("status", ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode());
		}
		if(responseObj.isEmpty()) { // hanya lanjut jika pengecekan di atas sukses (belum ada error status)
			Long cer_id = OSDUtil.updateCertificateToInaproc(certificate, user.userid, BasicCtr.newDate());
			if(cer_id==null) {
				responseObj.put("status", ErrorCodeOSD.FAILED.getCode());
			} else {
				responseObj.put("status", ErrorCodeOSD.SUCCEEDED.getCode());
			}
		}
		renderJSON(responseObj);
	}
	
	public String generateKeyKms() {
		StringBuilder result = new StringBuilder();
		Date ttpDate = Configuration.getDate(CONFIG.AMANDA_START_DATE.category);	
		List<Paket> list = Paket.find("ttp_ticket_id is NULL AND pkt_tgl_buat >= ? and pkt_flag > 2 order by pkt_id asc", ttpDate).fetch();
		if(!CollectionUtils.isEmpty(list)) {
			result.append("Generate Key Tender Sedang Berjalan");
			for(Paket paket: list) {
				List<Lelang_seleksi> listLelang = Lelang_seleksi.find("pkt_id =? AND lls_status = 1 order by lls_versi_lelang desc", paket.pkt_id).fetch();
				if(!CollectionUtils.isEmpty(listLelang)) {
					Lelang_seleksi lelangItem = listLelang.get(0);
					result.append("<br />Genereate Kunci KMS, Kode Paket: ").append(paket.pkt_id);
					List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?",paket.pnt_id).fetch();
					OSDUtil.createKPL(paket, anggota_panitia, lelangItem, BasicCtr.LPSE_URL);
					paket = Paket.findById(paket.pkt_id);
					result.append("<br />TTP Ticket Id: ").append(paket.ttp_ticket_id);
				}
				
			}
		}else {
			result.append("Tidak Ada Kunci KMS yang di Generate");
		}
//		logger.info(result);
		return result.toString();
	}

	public static void updateKeyKms() {
		Long kodeLelang = Long.parseLong(params.get("id"));
		StringBuilder result = new StringBuilder();
		Lelang_seleksi lelang = Lelang_seleksi.find("lls_id = ? AND lls_status = 1 and pkt_id in (select pkt_id from paket where pkt_flag > 2) order by lls_id desc", kodeLelang).first();
		if(lelang != null) {
			Paket paket = Paket.findById(lelang.pkt_id);
			if(paket != null && ConfigurationDao.isOSD(paket.pkt_tgl_buat)){
				if(paket.ttp_ticket_id!=null){
					result.append("<br />Update Kunci KMS, Kode Tender: ").append(kodeLelang);
//					epnsSecureFacade.updateTTP(lelang, keyKPL, paket.getTtp_ticket_id(), paket);
					List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?", paket.pnt_id).fetch();
					OSDUtil.createKPL(paket, anggota_panitia, lelang, BasicCtr.LPSE_URL);
				} else 
					result.append("Tidak Ada Kunci KMS yang di Update");
			}
		} else {
			result.append("Tidak Ada Kunci KMS yang di Update");
		}
		renderText(result.toString());
	}
	
}
