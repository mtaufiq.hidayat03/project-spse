package controllers.osd;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.agency.Pegawai;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.osd.ErrorCodeOSD;
import models.rekanan.Rekanan;
import models.secman.Group;
import play.Logger;
import utils.LogUtil;
import utils.osd.CertificateUtil;
import utils.osd.OSDUtil;

import java.util.HashMap;
import java.util.Map;

public class OSDRegServiceCtr extends BasicCtr {

	public static final String TAG = "OSDRegServiceCtr";

	public static Long DEFAULT_SIGNATURE = new Long(0);

	public static String OPT_DOK_LELANG_PRA = "1";
	public static String OPT_DOK_LELANG_PASCA = "2";
	public static String OPT_DOK_LELANG_ADENDUM = "3";

	/**
	 * Service untuk meminta certificate server
	 * Response dalam bentu plain:
	 * {"cert":"cert content"} atau
	 * {"error":"error code"}
	 */
	public static void getCertServer() {
		Logger.trace("getCertServer");
		String pem=CertificateUtil.getCertificatePEM();
		Map<String, Object> responseObj = new HashMap<>();
		if(pem!=null) {
			responseObj.put("cert", pem);
		} else {
			responseObj.put("error",ErrorCodeOSD.GET_CERTIFICATE_FAILED.getCode());
		}
		renderJSON(responseObj);
	}
	
	/**
	 * Service untuk meminta versi aplikasi
	 * Response dalam bentu plain:
	 * {"version":"4.2"}
	 */
	public static void getEpnsVersion() {
		Logger.trace("getEpnsVersion");
		Map<String, Object> responseObj = new HashMap<>();
		responseObj.put("version", ConfigurationDao.getAppVersion());
		renderJSON(responseObj);
	}
	
	/**
	 * Service untuk menerima permintaan CSR
	 * Parameter yang dibutuhkan untuk service ini antara lain
	 * a) params = encrypted parameter dalam format JSON dengan atribut-atribute sbb:
	 * 		- subjectDN
	 * 		- csr
	 * b) random = random AES key yang digunakan untuk mengenkrip params (key ini sendiri dienkrip menggunakan public key server)
	 * Response yang dikirim dienkrip menggunakan AES key dari parameter request. Plain responsenya sendiri berbentuk : {"status":"responseCode"}
	 */
	@AllowAccess({ Group.REKANAN, Group.PANITIA, Group.ADM_AGENCY, Group.VERIFIKATOR })
	public static void setCSRR() {
		LogUtil.debug(TAG, "setcsrr");
		LogUtil.debug(TAG, params);
		LogUtil.debug(TAG, request.headers);
		String random = params.get("random");
		String p = params.get("params");
		LogUtil.debug(TAG, random);
		LogUtil.debug(TAG, p);
		byte[] key = CertificateUtil.RSADecryption(random);
		JsonObject jsonParam = OSDUtil.getParams(p, key);
		if(jsonParam != null){
			String subjectDN = jsonParam.get("subjectDN").getAsString();
			String eeCSRR = jsonParam.get("csr").getAsString();
			Active_user user = Active_user.current();
			if(user.isRekanan()){
				Rekanan rekanan = Rekanan.findById(user.rekananId);
				String respCode = OSDUtil.updateCSRRRekanan(rekanan, subjectDN, eeCSRR, BasicCtr.newDate());
				if(respCode.equals(ErrorCodeOSD.SUCCEEDED.getCode())) {
					try {
						rekanan.saveToADPCentral();
						user.setCsrExists();
					} catch (Exception e) {
						Logger.error(e, "cert id belum terupdate ke adp");
					} finally {
						rekanan.save();
					}
				}
				LogUtil.debug(TAG, respCode);
				renderJSON(OSDUtil.getReturn(respCode, key));
			} else {
				Pegawai pegawai = Pegawai.findBy(user.userid);
				user.setCsrExists();
				renderJSON(OSDUtil.getReturn(OSDUtil.updateCSRRPegawai(pegawai, user.group, subjectDN, eeCSRR, BasicCtr.newDate()), key));
			}
		} else {
			Logger.info(ErrorCodeOSD.PARAMS_ERROR.getLabel());
			LogUtil.debug(TAG, ErrorCodeOSD.PARAMS_ERROR.getLabel());
			renderJSON(OSDUtil.getReturn(ErrorCodeOSD.PARAMS_ERROR.getCode(), key));
		}
	}
	
	/**
	 * Service untuk set mengaktivasi certificate
	 * Parameter yang dibutuhkan untuk service ini antara lain
	 * a) random = random AES key yang digunakan untuk mengenkrip params (key ini sendiri dienkrip menggunakan public key server)
	 * Response yang dikirim dienkrip menggunakan AES key dari parameter request. Plain response dalam bentuk : {"status":"responseCode"} atau {"value":"PEMPEMPEM"}
	 */
	public static void certActivate() throws Exception {
		Logger.trace("certActivate");
		String random = params.get("random");
		byte[] key = CertificateUtil.RSADecryption(random);
		if(key != null){
			Active_user user = Active_user.current();
			Group group = user.group;
			Rekanan rekanan=null;
			Pegawai pegawai=null;
			if(group.isRekanan())
				rekanan = Rekanan.findById(user.rekananId);
			else 
				pegawai = Pegawai.findBy(user.userid);
			renderJSON(OSDUtil.getReturn(OSDUtil.certActivateWrapper(rekanan, pegawai, group, BasicCtr.newDate()), key));
		} else {
			renderJSON(OSDUtil.getReturn(ErrorCodeOSD.PARAMS_ERROR.getCode(), key));
		}
	}

	/**
	 * Service untuk menerima permintaan pembaharuan CSR
	 * Parameter yang dibutuhkan untuk service ini antara lain
	 * a) params = encrypted parameter dalam format JSON dengan atribut-atribute sbb:
	 * 		- subjectDN
	 * 		- csr
	 * 		- existingSN
	 * b) random = random AES key yang digunakan untuk mengenkrip params (key ini sendiri dienkrip menggunakan public key server)
	 * Response yang dikirim dienkrip menggunakan AES key dari parameter request. Plain response dalam bentuk : {"status":"responseCode"}
	 */
	@AllowAccess({ Group.REKANAN, Group.PANITIA, Group.ADM_AGENCY, Group.VERIFIKATOR })
	public static void setCSRRenew() throws Exception {
		Logger.trace("setCSR-Renew");
		String random = params.get("random");
		byte[] key = CertificateUtil.RSADecryption(random);
		JsonObject jsonParam = OSDUtil.getParams(params.get("params"), key);
		if(jsonParam != null){
			String subjectDN = jsonParam.get("subjectDN").getAsString();
			String eeCSRR = jsonParam.get("csr").getAsString();
			String existingSN = jsonParam.get("existingSN").getAsString();
			Active_user user = Active_user.current();
			Group group = user.group;
			if(group.isRekanan()){
				Rekanan rekanan = Rekanan.findById(user.rekananId);
				if(rekanan!=null){
					String respCode = OSDUtil.renewCSRRekanan(rekanan, existingSN, subjectDN, eeCSRR, BasicCtr.newDate());
					if(respCode.equals(ErrorCodeOSD.SUCCEEDED.getCode())) {
						try {
							rekanan.saveToADPCentral();
						} catch (Exception e) {
							Logger.error(e, "cert id belum terupdate ke adp");
						} finally {
							rekanan.save();
						}
					}
					renderJSON(OSDUtil.getReturn(respCode, key));
				}
			} else {
				Pegawai pegawai = Pegawai.findBy(user.userid);
				renderJSON(OSDUtil.getReturn(OSDUtil.renewCSRPegawai(pegawai, group, existingSN, subjectDN, eeCSRR, BasicCtr.newDate()), key));
			}
		} else {
			Logger.info(ErrorCodeOSD.PARAMS_ERROR.getLabel());
			renderJSON(OSDUtil.getReturn(ErrorCodeOSD.PARAMS_ERROR.getCode(), key));
		}
	}
}
