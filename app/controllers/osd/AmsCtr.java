package controllers.osd;

import ams.models.ServiceResult;
import ams.models.form.FormRegisterAms;
import ams.models.response.ams.AmsStatusResponse;
import ams.repositories.AmsRepository;
import ams.service.AmsService;
import ams.utils.UserAgent;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.BerandaCtr;
import controllers.security.AllowAccess;
import models.LOVItem;
import models.common.Active_user;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;
import play.i18n.Messages;
import play.mvc.Before;
import utils.LogUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.secman.Group.PANITIA;
import static models.secman.Group.REKANAN;

public class AmsCtr extends BasicCtr {

    public static final String TAG = "AmsCtr";
    public static final String FLASH_KEY = "ams-flash";

    @Before
    public static void checkUserAgent() {
        if (!ConfigurationDao.isOSD(BasicCtr.newDate()) || !UserAgent.allowUserAgent(request)) {
            forbidden("Tidak dapat mengakses halaman ini");
        }
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void register() {
        Active_user user = Active_user.current();
        if (!user.mustRegisterAms()) {
            forbidden(Messages.get("ams.user-has-been-registered"));
        }
        Map<String, Object> params = new HashMap<>();

        FormRegisterAms form = new FormRegisterAms(user);
        params.put("form", form);
        params.put("isPanitia",user.isPanitia());
        params.put("input", true);
        params.put("unitKerjaLabel", user.isPanitia() ? "Instansi" : "Nama Perusahaan");
        renderTemplate("ams/register.html",params);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void simpan(FormRegisterAms form) {
        checkAuthenticity();
        LogUtil.debug(TAG, form);
        validation.required(form);
        if (validation.hasErrors()) {
            flash.error(Messages.get("ams.form-validation-error"));
            register();
        }
        if (form.file == null) {
            flash.error(Messages.get("ams.photo-id-file-empty"));
            register();
        }
        if (!form.isValidFile()) {
            flash.error(Messages.get("ams.photo-id-file-size-exceed"));
            register();
        }
        if (CommonUtil.isEmpty(form.nomor_telepon)) {
            flash.error("Harap isi nomor telepon di akun Anda! Gunakan fitur update Data Penyedia jika Anda Penyedia, atau hubungi LPSE jika Anda Non Penyedia.");
            register();
        }
        Active_user user = Active_user.current();
        form.rebind(new FormRegisterAms(user));
        ServiceResult result = AmsService.register(user, form);
        if (result.isSuccess()) {
            amsFlash(result.getMessage());
            BerandaCtr.index();
        } else {
            flash.error(result.getMessage());
            renderTemplate("ams/register.html",params);
        }
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void enrollmentDocument() {
        Active_user user = Active_user.current();
        AmsStatusResponse response = AmsRepository.getStatus(user.ams);
        if (!response.isUserVerified()) {
            forbidden("User belum diverifikasi!");
        }
        if (response.isUserVerified() && !user.ams.isAmsEnrollTokenEmpty()) {
            forbidden(Messages.get("ams.enroll-document-has-been-sent"));
        }
        Map<String, Object> params = new HashMap<>();
        FormRegisterAms form = new FormRegisterAms(user);
        params.put("form", form);
        params.put("input", response.isUserVerified());
        renderTemplate("ams/enrollment-document.html", params);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void submitEnrollmentDocument(FormRegisterAms form) {
        checkAuthenticity();
        if (form.file == null) {
            flash.error(Messages.get("ams.enroll-document-empty"));
            enrollmentDocument();
        }
        if (!form.isValidFile()) {
            flash.error(Messages.get("ams.enroll-file-note"));
            enrollmentDocument();
        }
        if (!form.isValidDocument()) {
            flash.error(Messages.get("ams.wrong-format-enroll-document"));
            enrollmentDocument();
        }
        ServiceResult<String> serviceResult = AmsService.submitEnrollmentDocument(Active_user.current(), form, false);
        Map<String, Object> params = new HashMap<>();
        params.put("input", false);
        if (serviceResult.isSuccess() && serviceResult.getPayload() != null) {
            amsFlash(serviceResult.getMessage());
            BerandaCtr.index();
        } else {
            flash.error(serviceResult.getMessage());
            renderTemplate("ams/enrollment-document.html", params);
        }
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void uploadPhoto() {
        Active_user user = Active_user.current();
        if (!user.ams.isWaitingForPhoto()) {
            forbidden(Messages.get("ams.photo-id-has-been-sent"));
        }
        Map<String, Object> params = new HashMap<>();
        FormRegisterAms form = new FormRegisterAms(user);
        params.put("form", form);
        renderTemplate("ams/upload-photo.html",params);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void savePhoto(FormRegisterAms form) {
        checkAuthenticity();
        if (form.file == null) {
            flash.error("ams.photo-id-file-empty");
            uploadPhoto();
        }
        if (!form.isValidFile()) {
            flash.error("ams.photo-id-file-size-exceed");
            uploadPhoto();
        }
        if (!form.isValidPicture()) {
            flash.error("ams.photo-id-file-extension");
            uploadPhoto();
        }
        ServiceResult serviceResult = AmsService.uploadIdCard(Active_user.current(), form);
        if (!serviceResult.isSuccess()) {
            flash.error(serviceResult.getMessage());
            uploadPhoto();
        }
        amsFlash(serviceResult.getMessage());
        BerandaCtr.index();
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void downloadCertificate() throws IOException {
        Active_user user = Active_user.current();
        ServiceResult<JsonObject> result = AmsService.downloadCertificate(user);
        if (!result.isSuccess()) {
            renderJSON("{\"status\":false, \"message\":\"" + result.getMessage() + "\"}");
        }
        if (result.getPayload() == null) {
            renderJSON("{\"status\":false, \"message\":\"Error payload empty!\"}");
        }
        renderJSON(result.getPayload());
       // renderTemplate("ams/download-certificate.html");
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void getSatker() {
        String instansi = params.get("instansi");
        Map<String,Object> param = new HashMap<>();

        List<LOVItem> satkerList = LOVItem.getSatkerPusat(instansi, null);
        param.put("list",satkerList);

        param.put("status","ok");
        renderJSON(param);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void csr() {
        Active_user user = Active_user.current();
        notFoundIfNull(user);
        if (user.ams.isWaitingForCertificate()) {
            forbidden(Messages.get("ams.enroll-document-has-been-sent"));
        }
        /*if (!user.mustSendCsr()) {
            forbidden("Anda belum mengirimkan surat rekomendasi!");
        }*/
        Map<String, Object> params = new HashMap<>();
        FormRegisterAms form = new FormRegisterAms(Active_user.current());
        params.put("form", form);
        params.put("input", true);
        params.put("enrollmentToken", user.getAms().getAmsEnrollmentToken());
        params.put("amsId", user.getAms().getId());
        if (user.ams.isAmsEnrollTokenEmpty()) {
            params.put("showEnrollDocumentField", true);
        }
        renderTemplate("ams/csr.html", params);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void submitCsr(FormRegisterAms form) {
        Active_user user = Active_user.current();
        LogUtil.debug("AmsCtr", form);
        checkAuthenticity();
        if (user.ams.isAmsEnrollTokenEmpty() && form.file == null) {
            flash.error(Messages.get("ams.enroll-document-empty"));
            csr();
        }
        if (!form.isValidFile()) {
            flash.error(Messages.get("ams.enroll-file-note"));
            csr();
        }
        if (!form.isValidDocument()) {
            flash.error(Messages.get("ams.wrong-format-enroll-document"));
            csr();
        }
        ServiceResult result = AmsService.sendEnrollAndCsr(user, form);
        if (result.isSuccess()) {
            amsFlash(result.getMessage());
            BerandaCtr.index();
        }
        flash.error(result.getMessage());
        BerandaCtr.index();
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void revoke(String serial) {
        LogUtil.debug(TAG, "revoke");
        LogUtil.debug(TAG, request);
        LogUtil.debug(TAG, serial);
        if (CommonUtil.isEmpty(serial)) {
            LogUtil.debug(TAG, "serial empty!");
            renderJSON("{\"status\":false,\"message\":\"missing params!\"}");
        }
        ServiceResult result = AmsService.revokeCertificate(Active_user.current(), serial.toUpperCase());
        renderJSON(result.toJson());
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void requestCertificate() {
        LogUtil.debug(TAG, "request certificate");
        Active_user user = Active_user.current();
        ServiceResult<String> result = AmsService.sendRequestCertificate(user, params);
        LogUtil.debug(TAG, result);
        renderJSON(result.getPayload());
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void renewal() {
        LogUtil.debug(TAG, "renewal");
        Active_user user = Active_user.current();
        ServiceResult<String> result = AmsService.renewCertificate(user, params);
        if (result.getPayload() != null) {
            LogUtil.debug(TAG, result);
            renderJSON(result.getPayload());
        }
        LogUtil.debug(TAG, result.toJson());
        renderJSON(result.toJson());
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void checkRevoke() {
        LogUtil.debug(TAG, "check revocation status through OCSP");
        LogUtil.debug(TAG, request.headers);
        final String ref = request.headers != null ? request.headers.get("referer").value() : "";
        Active_user user = Active_user.current();
        if (!user.getAms().isCertificateRevocationRequested()) {
            BerandaCtr.index();
        }
        user.setAmsRelatedData();
        if (!CommonUtil.isEmpty(ref)) {
            redirect(ref);
        }
        BerandaCtr.index();
    }

    private static void amsFlash(String message) {
        flash.put(FLASH_KEY, message);
    }

}
