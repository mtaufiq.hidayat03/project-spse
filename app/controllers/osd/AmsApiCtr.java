package controllers.osd;

import ams.models.ServiceResult;
import ams.models.form.FormRegisterAms;
import ams.service.AmsService;
import ams.utils.UserAgent;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import controllers.security.AllowAccess;
import models.LOVItem;
import models.common.Active_user;
import models.common.ConfigurationDao;
import play.i18n.Messages;
import play.mvc.Before;
import utils.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.secman.Group.PANITIA;
import static models.secman.Group.REKANAN;

/**
 * @author HanusaCloud on 8/27/2018
 */
public class AmsApiCtr extends BasicCtr {

    @Before
    public static void checkUserAgent() {
        if (!ConfigurationDao.isOSD(BasicCtr.newDate()) || !UserAgent.allowUserAgent(request)) {
            forbidden("Tidak dapat mengakses halaman ini");
        }
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void userData() {
        Active_user user = Active_user.current();
        if (!user.mustRegisterAms()) {
            returnError("ams.user-has-been-registered");
        }
        Map<String, Object> params = new HashMap<>();
        FormRegisterAms form = new FormRegisterAms(user);
        params.put("form", form);
        params.put("isPanitia",user.isPanitia());
        params.put("input", true);
        params.put("unitKerjaLabel", user.isPanitia() ? "Instansi" : "Nama Perusahaan");
        renderJSON(new ServiceResult<>(true, "success", params));
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void saveUser(FormRegisterAms form) {
        validation.required(form);
        if (validation.hasErrors()) {
            returnError("ams.form-validation-error");
        }
        if (form.file == null) {
            returnError("ams.photo-id-file-empty");
        }
        if (!form.isValidFile()) {
            returnError("ams.photo-id-file-size-exceed");
        }
        ServiceResult result = AmsService.register(Active_user.current(), form);
        renderJSON(result);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void submitEnrollmentDocument(FormRegisterAms form) {
        if (form.file == null) {
            returnError("ams.enroll-document-empty");
        }
        if (!form.isValidDocument()) {
            returnError("ams.wrong-format-enroll-document");
        }
        if (!form.isValidFile()) {
            returnError("ams.enroll-file-note");
        }
        ServiceResult<String> serviceResult = AmsService.submitEnrollmentDocument(Active_user.current(), form, false);
        renderJSON(serviceResult);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void savePhoto(FormRegisterAms form) {
        if (form.file == null) {
            returnError("ams.photo-id-file-empty");
        }
        if (!form.isValidFile()) {
            returnError("ams.photo-id-file-size-exceed");
        }
        if (!form.isValidPicture()) {
            returnError("ams.photo-id-file-extension");
        }
        ServiceResult serviceResult = AmsService.uploadIdCard(Active_user.current(), form);
        renderJSON(serviceResult);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void submitCsr(FormRegisterAms form) {
        Active_user user = Active_user.current();
        LogUtil.debug("AmsCtr", form);
        if (user.ams.isAmsEnrollTokenEmpty() && form.file == null) {
            returnError("ams.enroll-document-empty");
        }
        if (!form.isValidFile()) {
            returnError("ams.enroll-file-note");
        }
        if (!form.isValidDocument()) {
            returnError("ams.enroll-file-note");
        }

        ServiceResult result = AmsService.sendEnrollAndCsr(user, form);
        renderJSON(result);
    }

    @AllowAccess({REKANAN, PANITIA})
    public static void checkStatus() {

    }

    @AllowAccess({REKANAN, PANITIA})
    public static void getSatker() {
        String instansi = params.get("instansi");
        Map<String,Object> param = new HashMap<>();

        List<LOVItem> satkerList = LOVItem.getSatkerPusat(instansi, null);
        param.put("list",satkerList);

        param.put("status","ok");
        renderJSON(param);
    }

    public static void getInstansi() {
        String kbpId = params.get("kbpId");
        Map<String,Object> param = new HashMap<>();
        List<LOVItem> instansi = LOVItem.getInstansiByKabupaten(Long.valueOf(kbpId));
        param.put("list", instansi);
        param.put("status","ok");
        renderJSON(param);
    }

    private static void returnError(String key) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", false);
        jsonObject.addProperty("message", Messages.get(key));
        renderJSON(jsonObject);
    }

}
