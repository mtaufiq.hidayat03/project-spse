package controllers.dce3client;

import com.google.gson.Gson;
import models.dce3client.DceKey;
import models.dce3client.KeyManager;
import models.dce3client.QueryEngine;
import models.dce3client.QueryEngine.SqlCommands;
import models.jcommon.secure.encrypt.CipherEngineException;
import models.jcommon.secure.encrypt2.CachedRSAKey;
import models.jcommon.secure.encrypt2.CipherEngine2;
import models.jcommon.secure.encrypt2.DecryptCipherEngine;
import models.jcommon.secure.encrypt2.EncryptCipherEngine;
import models.jcommon.sysinfo.SystemInformation;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Http.StatusCode;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**Class ini digunakan untuk DCE-3
 * 
 * Untuk mengetes class ini, gunakan test/dce3client/*
 * 
 * @author Andik
 *
 */
public class QueryCtr extends Controller {

	@play.mvc.Before
	protected void before()
	{
		Logger.debug("[SPSE] QueryCtr.before: %s", request.url);
		Logger.info("[QueryCtr] : %s", request.headers);
		Http.Header header = request.headers.get("x-forwarded-for");
		if(header == null  || (header != null && !header.value().contains("103.13.181.16"))
				|| (header != null &&!header.value().contains("103.13.181.142"))) {
			forbidden("Anda tidak berhak Akses");
		}
	}
	
	/**Output dari Query ini adalah .csv.gz
	 * @param dceSessionId 
	 * @param md5: dari file yang pernah di-request sebelumnya untuk SQL yang sama. Dihitung dari MD5 file setelah dekripsi (file dalam format *.csv.gz) 
	 *  
	 *  Ini akan mencegah pengirim kembali hasil SQL yang sama dengan pengiriman sebelumnya terutama tabel yang SELECT tanpa WHERE karena tdk punya auditupdate.
	 *  
	 *  Contoh: data Rekanan request tgl 1 masih sama dengan request data rekanan tanggal 2. Pada request tanggal 2, SPSE tidak perlu mengirimkan 
	 *  response data, namun cukup response.status -> SC_NOT_MODIFIED
	 *  Ini khusus untuk SQL: SELECT
	 * @param statement: 
	 *   berisi SQL yang dikirim (SELECT, INSERT, UPDATE, DELETE, ALTER, DROP)
	 *   
	 * @param disableBackup //TODO belum diimplementasikan
	 *   jika true maka LPSE tidak perlu membuat backup terhadap data yg kena UPDATE atau DELETE
	 *   jika kosong atau false maka semua row yg terkena UPDATE atau DELETE akan diexport ke gile csv.gz 
	 *   dan disimpan di ${FILE_STORAGE}/dce3client/  
	 *   
	 *  ============= ResponseStatus ====================
	 *  1. SC_BAD_REQUEST ->
	 *  	jika dceSessionId tidak valid atau sudah expires
	 *  2. SC_OK
	 *  	Query berhasil mendapatkan data dan dikirim dalam format csv.gz
	 *  3. SC_NOT_MODIFIED
	 *  	Query berhasil mendapatkan data namun tidak ada perubahan data sejak Query terakhir
	 *      File tidak dikirim
	 *  =============== ResponseBody =====================
	 *  File csv.gz yg di-enkripsi menggunakan publikKey dari DCE
	 */
	public static void sql(String dceSessionId, String md5)
	{
		//check sessionId expiry
		DceKey dceKey= KeyManager.getKey(dceSessionId);
		String error=null;
		if(dceKey==null)
			error="DCE session is not property initialized or expires";
		else
			if(dceKey.isKeyExpires())
				error="DCE session expires";
		if(error!=null)
		{
			Logger.error("[SPSE] from %s : dce3/sql/%s -> %s",request.remoteAddress,  dceSessionId, error);
			response.status=StatusCode.BAD_REQUEST;
			renderText(error);
		}
		
		String statement=params.get("statement");
		if(statement==null)
		{
			response.status=StatusCode.BAD_REQUEST;
			renderText("Statement is null");
		}
		//decrypt statement
		try {
			CipherEngine2 decrypt= new DecryptCipherEngine(dceKey.spsePrivateKey);
			byte[] ary = decrypt.doCrypto(Hex.decodeHex(statement.toCharArray()));
			statement=new String(ary);
		} catch (Exception e) {
			response.status=StatusCode.BAD_REQUEST;
			renderText("Invalid statement: " + e.getClass() + '.' +  e.getMessage());
		}

		Logger.debug("[SPSE] dce/sql/%s -> %s ", dceSessionId, statement);
		QueryEngine engine=new QueryEngine();
		File file=null;
		Pattern pattern=Pattern.compile("\\s*(select|insert|update|delete|alter|drop)\\s.*$", Pattern.CASE_INSENSITIVE);
		Matcher matcher=pattern.matcher(statement);
		SqlCommands command=null;
		if(matcher.matches())
		{
			command=SqlCommands.valueOf(matcher.group(1).toUpperCase());
			switch(command)
			{
				case SELECT:
				file=engine.select(statement);
				//apakah MD5 masih valid?
				//jika ya, maka jangan kirim datanya, cukup resposeStatus
				if(engine.md5Value!=null && md5!=null)
					if(engine.md5Value.equals(md5))
					{
						response.setHeader("X-MD5Value", engine.md5Value);
						response.status=StatusCode.NOT_MODIFIED;
						return;
					}
				break;
				case UPDATE:
				case DELETE:
					boolean createBackup=!"true".equals(params.get("disabledBackup"));
					engine.updateDelete(statement, createBackup);
				break;
				
			}
		}
		else
		{
			response.status=StatusCode.BAD_REQUEST;
			renderText("Invalid SQL Commands");
		}
		if(engine.error!=null)
		{
			response.status=StatusCode.BAD_REQUEST;
			renderText(engine.error);
			return;
		}
		else
		{
			response.status=StatusCode.OK;
			if(command.equals(SqlCommands.SELECT))
			{
				//tambahkan di header
				response.setHeader("X-MD5Value", engine.md5Value);
				//lakukan enkripsi sebelum dikirim
				CipherEngine2 encrypt;
				try {
					encrypt = new EncryptCipherEngine(dceKey.dcePublicKey);
					BufferedInputStream is=new BufferedInputStream(new FileInputStream(file));
					File encryptedFile=new File(file.getPath() + ".enc");
					BufferedOutputStream out=new BufferedOutputStream(new FileOutputStream(encryptedFile));
					encrypt.doCrypto(is, out);
					out.close();
					is.close();
					file.delete();
					renderBinary(encryptedFile);
					return;
				} catch (CipherEngineException  | IllegalBlockSizeException | BadPaddingException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**Dapatkan ssystem information
	 * @param dceSessionId
	 */
	public static void systemInformation(String dceSessionId)
	{
		DceKey dceKey= KeyManager.getKey(dceSessionId);
		String error=null;
		if(dceKey==null)
			error="DCE session is not property initialized or expires";
		else
			if(dceKey.isKeyExpires())
				error="DCE session expires";
		if(error!=null)
		{
			Logger.error("[SPSE] from %s : dce3/sql/%s -> %s",request.remoteAddress,  dceSessionId, error);
			response.status=StatusCode.BAD_REQUEST;
			renderText(error);
		}
		else
		{
			SystemInformation sys=SystemInformation.getSystemInformation();
			renderJSON(sys.informations);
		}
	}
	
	/** ====== Step 1 ======
	 * 1. DCE server mengirimkan publicKeyDCE
	 * 2. SPSE membalas dengan mengirimkan publicKeySPSE dan dceSessionId
	 * 		yg sudah di-encrypt dengan publicKeyDCE
	 * @param publicKeyDCE
	 */
	public static void init1()
	{
		try {
			String publicKeyDCE=params.get("publicKeyDCE");
			CachedRSAKey key=new CachedRSAKey();
			Gson gson=new Gson();
			String[] ary=new String[]{session.getId(),key.getPublicKeyEncoded()};
			String strResp=gson.toJson(ary);
			//encrypt with publickey
			CipherEngine2 ce=new EncryptCipherEngine(publicKeyDCE);
			byte[] encrypted= ce.doCrypto(strResp.getBytes());
			
			DceKey dceKey=new DceKey();
			dceKey.dceSessionId=session.getId();
			dceKey.spsePrivateKey=key.getPrivateKeyEncoded();
			dceKey.spsePubliKey=key.getPublicKeyEncoded();
			dceKey.dcePublicKey=publicKeyDCE;
			KeyManager.addKey(session.getId(), dceKey);
			
			renderText(Hex.encodeHexString(encrypted));
		} catch (CipherEngineException e) {
			Logger.error(e, "Error QueryController.init1-> URL: %s", request.url);
			response.status=StatusCode.BAD_REQUEST;
			renderText(e.getMessage());
		}
	}
	
	/** ====== Step 2 ====== 
	 * 1. DCE server mengirimkan 1 token, beserta dceSessionId yang sudah diencrypt dengan publicKeySPSE
	 * 2. SPSE men-decrypt token menggunakan privateKeySPSE
	 * 3. SPSE mengecek validitas token
	 *		jika valid, maka return OK dan dceSessionId tetap valid
	 *		jika invalid, return ERROR dan dceSessionId menjadi invalid
	 *
	 *  =========================
	 *  Validitas token:
	 *  =========================
	 *  Diambil dari MD5 dari row pertama pada tabel peserta, kolom auditupdate 
	 *  Mengapa validitas token di-create dg cara ini?
	 *   
	 *  Untuk memastikan bahwa hanya DCE-LKPP yang bisa menarik data ke dce3client maka harus ada 'kunci'
	 *  yang hanya diketahui oleh aplikasi DCE dan SPSE. Jika ada orang yg tahu algoritma encripsi ini
	 *  (mantan developer SPSE misalnya), dia bisa membuat duplikat DCE. Namun demikian, dia tidak bisa
	 *  menarik dari SPSE kecuali dia punya isi dari database DCE.
	 * 
	 */
	public static void init2(String dceSessionId)
	{
		String token=params.get("token");
		/**Decrypt pakai privateKey
		 */
		DceKey dceKey=KeyManager.getKey(dceSessionId);
		try {
			CipherEngine2 decrypt=new DecryptCipherEngine(dceKey.spsePrivateKey);
			byte[] ary=decrypt.doCrypto(Hex.decodeHex(token.toCharArray()));
			token=new String(ary);
			boolean valid=KeyManager.isTokenValid(token);
			Logger.debug("[SPSE] Token: %s, valid: %s", token, valid);
			if(!valid)
			{
				session.clear();
				KeyManager.removeKey(dceSessionId);
				response.status=StatusCode.BAD_REQUEST;
				renderText("Invalid DCE Token");
			}			
			else
				renderText("OK");
		} catch (CipherEngineException | DecoderException e) {
			e.printStackTrace();
		}
	}
	
}
