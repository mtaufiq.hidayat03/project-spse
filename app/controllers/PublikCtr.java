package controllers;

import jobs.SendMailJobNow;
import models.common.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.mail.EmailManager;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.lelang.Blacklist;
import models.rekanan.Rekanan;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.cache.CacheFor;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.i18n.Lang;
import play.libs.*;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import play.mvc.Http.Cookie;
import play.mvc.Router;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.AesUtil;

import java.util.*;

/**
 * Kelas ini merepresentasikan object yang mengontrol halaman utama dimana pengguna tidak
 * melakukan autentikasi (<i>login</i>).
 *
 * @author Oscar Kurniawan (oscar@lkpp.go.id)
 * @author I Wayan Wiprayoga W
 * @author Andik Yulianto
 * @author Arief Ardiyansah
 */
public class PublikCtr extends BasicCtr {

    private static final Template TEMPLATE = TemplateLoader.load("/email/notifikasi-email-confirm.html");
    public static final String LOGIN_COUNTER_KEY = "login-try";

    /**
     * Fungsi {@code index} digunakan untuk menampilkan halaman muka SPSE
     */
    @CacheFor("10s")//cache for 10 seconds
    public static void index() {
        if (Active_user.isLoggedIn())
            BerandaCtr.index();
        // Rekam data counter pengunjung tiap 1 hari -> Hanya direkam jika akses
        // halaman muka
        Cookie cookie = request.cookies.get("Counter");
        if (cookie == null) {
            Logger.debug("add counter");
            Counter.update();
            int expires = 24 * 60 * 60 - new DateTime().getHourOfDay(); //expires jam 24:00
            response.setCookie("Counter", "1", null, "/", expires, false); //nilai cookies tidak penting
        }
        Cookie isRekanan = request.cookies.get("isRekanan");
        if (isRekanan != null)
            renderArgs.put("isRekanan", Boolean.parseBoolean(isRekanan.value));
        renderArgs.put("beritaList", Berita.getPengumumanAndBeritaTerakhir(8));
        renderTemplate("publik/publik-index.html");
    }

    public static void formLogin() {
        //set counter
        Cookie loginCookie = request.cookies.get(LOGIN_COUNTER_KEY);
        if (loginCookie != null && !StringUtils.isEmpty(loginCookie.value)) {
            int loginCounter = Integer.parseInt(loginCookie.value);
            if (loginCounter >= ConfigurationDao.getTotalkaptcha()) {
                renderArgs.put("randomID", Codec.UUID());
            }
        }
        AesUtil aesUtil = new AesUtil(256, 1000);
        AesUtil.AesKey aesKey = aesUtil.getKey(session.getId());
        renderArgs.put("token", aesKey.token);
        renderArgs.put("iv", aesKey.iv);
        renderArgs.put("salt", aesKey.salt);
        renderTemplate("form-login.html");
    }

    /**
     * Fungsi {@code tentangKami} digunakan untuk menampilkan halaman tentang
     * kami
     */
    public static void tentangKami() {
        renderArgs.put("lpseId", ConfigurationDao.getRepoId());
        renderArgs.put("javaVersion", String.format("%s %sbit", System.getProperty("java.runtime.version"), System.getProperty("sun.arch.data.model")));
        renderArgs.put("playVersion", Play.version);
        renderArgs.put("statusAdp", ConfigurationDao.informasiAgregasi);
        renderTemplate("publik/publik-tentang-kami.html");
    }

    /**
     * Fungsi {@code regulasi} digunakan untuk menampilkan halaman yang berisi
     * regulasi atau peraturan seputar pengadaan barang dan jasa pemerintah yang
     * menaunginya.
     */
    public static void regulasi() {
        renderArgs.put("daftarRegulasi", Berita.findAllValidRegulasi());
        renderTemplate("publik/publik-regulasi.html");
    }

    /**
     * Fungsi {@code special} digunakan untuk menampilkan halaman yang berisi
     * konten spesial seputar pengadaan barang dan jasa pemerintah yang
     * menaunginya.
     */
    public static void special() {
        renderArgs.put("beritaList", Berita.findAllValidSpecialContent());
        renderTemplate("publik/publik-special-content.html");
    }

    /**
     * Fungsi {@code special} digunakan untuk menampilkan halaman yang berisi
     * konten spesial seputar pengadaan barang dan jasa pemerintah yang
     * menaunginya.
     */
    public static void detil_special(Long beritaId) {
        Berita berita = Berita.findById(beritaId);
        notFoundIfNull(berita);
        BlobTable blob = null;
        if (berita.brt_id_attachment != null)
            blob = BlobTableDao.getLastById(berita.brt_id_attachment);
        renderArgs.put("berita", berita);
        renderArgs.put("blob", blob);
        renderTemplate("publik/popup-detil-konten-khusus.html");
    }

    /**
     * Fungsi {@code sitemap} digunakan untuk menampilkan halaman yang berisi
     * peta situs dalam bentuk pohon. Termasuk tampilan <i>parent</i> dan
     * tampilan <i>child</i>.
     */
    public static void sitemap() {
        renderTemplate("publik/publikSiteMap.html");
    }

    /**
     * Fungsi {@code paktaIntegritas} digunakan untuk menampilkan halaman pakta
     * integritas.
     */
    public static void paktaIntegritas() {
        renderTemplate("publik/publikPaktaIntegritas.html");
    }

    /**
     * Fungsi {@code blacklist} digunakan untuk menampilkan halaman penyedia
     * yang masuk daftar hitam SPSE.
     */
    public static void blacklist() {
        redirect(INAPROC_URL + "/daftar-hitam");
    }

    /**
     * Fungsi {@code kontakKami} digunakan untuk menampilkan halaman yang berisi
     * semua kontak cabang LPSE yang terdaftar pada aplikasi SPSE LPSE yang
     * bersangkutan
     */
    @CacheFor("1d")
    public static void kontakKami() {
        List<Ppe_site> list = Cache.get("ppe_siteList");
        if(CollectionUtils.isEmpty(list)) {
            list = Ppe_site.findAll();
            Cache.set("ppe_siteList", list, "1d");
        }
        renderArgs.put("ppe_site", list);  // Versi detail kebawah
        renderTemplate("publik/publik-kontak-kami2.html");
    }

    public static void loginAdmin() {
        renderArgs.put("randomID", Codec.UUID());
        AesUtil aesUtil = new AesUtil(256, 1000);
        AesUtil.AesKey aesKey = aesUtil.getKey(session.getId());
        renderArgs.put("token", aesKey.token);
        renderArgs.put("iv", aesKey.iv);
        renderArgs.put("salt", aesKey.salt);
        renderTemplate("publik/admin.html");
    }

    /**
     * Fungsi {@code showCaptcha} digunakan untuk menampilkan text captcha
     *
     * @param id random ID yang disematkan pada captcha image
     */
    public static void showCaptcha(String id) {
        Images.Captcha captcha = Images.captcha();
        captcha.addNoise();
//        String code = captcha.getText("#073642"); // #based02 solarized color captcha
        String code = captcha.getText(6, "abcdefghijkmnopqrstuvwxyz23456789");
//		Logger.info("GET %s", code);
        Cache.set(id, code.toLowerCase(), "10mn"); // store this unique id for 10mn to validate captcha
        renderBinary(captcha);
    }

    // ================================ BELUM DICEK ==================================
    // ===============================================================================

    /**
     * Fungsi {@code loginPPE} digunakan untuk menampilkan halaman login admin
     * PPE
     *
     * @param userid  userid admin
     * @param message pesan login admin
     */
    @Deprecated
    public static void loginPPE(String userid, String message) {
        renderArgs.put("randomID", Codec.UUID());
        renderTemplate("publik/adminLogin.html");
    }

    /**
     * Fungsi {@code strukturOrganisasi} digunakan untuk menampilkan halaman
     * struktur organisasi spse. Gambar yang ditampilkan adalah
     * struktur_lpse.jpg
     */
    public static void strukturOrganisasi() {
        renderTemplate("publik/publik-struktur-organisasi.html");
    }

    /**
     * Fungsi {@code mendaftar} digunakan untuk menampilkan halaman pendaftaran
     * penyedia
     */
    public static void mendaftarEmail() {
        renderArgs.put("randomID",UUID.randomUUID());
        renderTemplate("publik/publik-mendaftar-email.html");
    }

    /**
     * Mendaftar berupa email oleh publik
     */
    public static void mendaftarEmailSubmit(@Required String email, @Required String kode, String randomID) throws Exception {
        checkAuthenticity();
        validation.email(email).message("Format Email salah");

        if (validation.hasErrors()) {
            validation.keep();
            params.flash();
            mendaftarEmail();
            return;
        }

        //ceck kode keamanan/kapca
        String kapca = Cache.get(randomID, String.class);
        if (kapca == null || !kapca.equals(kode)) {
            flash.error("Kode Keamanan salah");
            validation.keep();
            params.flash();
            mendaftarEmail();
            return;
        }

        Rekanan temp = Rekanan.findByEmail(email);
        if (temp != null) {
            flash.error("Email yang Anda masukkan sudah digunakan. Silakan mendaftar dengan email lain"); // set notifikasi error
            params.flash();
            mendaftarEmail();
            return;
        }

        Map vars = new HashMap<String, String>();
        Map parameter = new HashMap<String, String>();
        parameter.put("email", Crypto.encryptAES(email));
        String url = getRequestHost() + Router.reverse("PublikCtr.mendaftarEmailConfirmed", parameter).url;
        vars.put("link", url);
        vars.put("emailRekanan", email);
        vars.put("nama_lpse", ConfigurationDao.getNamaLpse());
        vars.put("subject", "Konfirmasi Pendaftaran LPSE");
        MailQueue mq = EmailManager.createEmail("\"Pendaftar LPSE - " + ConfigurationDao.getNamaLpse() + " \" <" + email + '>', TEMPLATE, vars);
        mq.prioritas = MailQueue.MAIL_PRIORITAS.HIGH; //Email ini mendapat prioritas utama
        mq.ref_a = "PUBLIK"; //tambahan informasi tentang siapa yang dituju
        mq.lls_id = 0L;
        try {
            await(new SendMailJobNow(mq).now());
            renderArgs.put("email", email);
            renderTemplate("publik/publik-mendaftar-email-ok.html");
        } catch (Exception e) {
            flash.error("Email Konfirmasi Pendaftaran gagal terkirim");
            mendaftarEmail();
        }
    }

    //shortuct development, testing form pendaftaran
    public static void daftarAkun(String email) {
        if(Play.mode.isProd())
            forbidden();
        mendaftarEmailConfirmed(Crypto.encryptAES(email));
    }

    public static void mendaftarEmailConfirmed(String email) {
        if (CommonUtil.isEmpty(email))
            mendaftarEmail();
        else {
            Rekanan rekanan = new Rekanan();
            rekanan.rkn_email = Crypto.decryptAES(email);
            renderArgs.put("rekanan", rekanan);
            renderTemplate("publik/publik-rekanan-mendaftar-form.html");
        }
    }

    public static void rekananMendaftarSave(Rekanan rekanan, @Required(message = "Password wajib diisi") String password,
                                            @Required(message = "Ulangi Password wajib diisi") String passwordVerifikasi, Long propinsi) {
        checkAuthenticity();
        if(rekanan.isPerusahaanAsing()) {
            if(StringUtils.isEmpty(rekanan.rkn_npwp))
                rekanan.rkn_npwp="-";
            validation.required("rekanan.ngr_id", rekanan.ngr_id);
            validation.required("rekanan.kota", rekanan.kota);
        }
        else {
            validation.required("propinsi", propinsi);
            validation.required("rekanan.kbp_id", rekanan.kbp_id);
        }
        Rekanan temp = rekanan.findByEmail(rekanan.rkn_email);
        if (temp != null) {
            flash.error("Email yang Anda masukkan sudah digunakan. Silakan mendaftar dengan email lain"); // set notifikasi error
            params.flash();
            mendaftarEmailConfirmed(Crypto.encryptAES(rekanan.rkn_email));
        } else {
            // validasi entri password
            validation.equals(password, passwordVerifikasi).key("passwordVerifikasi").message("Password dan Ulangi Password tidak sesuai");
            if (rekanan.rkn_statcabang != null ) {
                if(rekanan.rkn_statcabang.equals("P")) {
                    rekanan.rkn_almtpusat = null;
                    rekanan.rkn_emailpusat = null;
                    rekanan.rkn_telppusat = null;
                    rekanan.rkn_faxpusat = null;
                } else if(rekanan.rkn_statcabang.equals("C")) {
                    validation.required("rekanan.rkn_almtpusat", rekanan.rkn_almtpusat);
                    validation.required("rekanan.rkn_emailpusat", rekanan.rkn_emailpusat);
                    validation.required("rekanan.rkn_telppusat", rekanan.rkn_telppusat);
                    validation.required("rekanan.rkn_faxpusat", rekanan.rkn_faxpusat);
                }
            }
            rekanan.passw = DigestUtils.md5Hex(password);
            if (ConfigurationDao.isEnableInaproc()) {
                String param = URLs.encodePart(DceSecurityV2.encrypt(rekanan.rkn_namauser));
                String url_service = ConfigurationDao.getRestCentralService() + "/is-exist?q=" + param;
                try {
                    WSRequest request = WS.url(url_service);
                    request.setHeader("authentication", Play.secretKey);
                    HttpResponse response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
                    if (response.success() && !StringUtils.isEmpty(response.getString()) && response.getString().equals("T")) {
                        Validation.addError("rekanan.rkn_namauser", "Harap Ganti User ID");
                    }
                } catch (Exception e) {
                    Logger.error(e, "Kendala Checking Username ke Sistem ADP");
                    Validation.addError("rekanan.rkn_namauser", "Kendala koneksi ke Sistem Agregasi Data Penyedia");
                }
            }
            validation.valid(rekanan); // tetap cek validasi
            if (validation.hasErrors()) {
                validation.keep();
                params.flash();
                mendaftarEmailConfirmed(Crypto.encryptAES(rekanan.rkn_email));
            } else if (password.equals(passwordVerifikasi)) {
                try {
                    List<Sub_tag> listVerifikasi = Sub_tag.find("tag_id=?", "SYARAT_VERIFIKASI").fetch();
                    StringBuilder verifikasi = new StringBuilder();

                    for (int i = 0; i < listVerifikasi.size(); i++) {
                        verifikasi.append('0');
                    }
                    rekanan.isbuiltin = 0;
                    rekanan.rkn_status_verifikasi = verifikasi.toString();
                    rekanan.rkn_isactive = 0;
                    rekanan.rkn_namauser = rekanan.rkn_namauser.toUpperCase();
                    rekanan.repo_id = Long.valueOf(ConfigurationDao.getRepoId());
                    rekanan.rkn_tgl_daftar = controllers.BasicCtr.newDate();
                    // user harus dalam keadaan disable karena belum dilakukan verifikasi oleh Verifikator LPSE
                    String referer = getKontrakKamiUrl();
                    rekanan.disableuser = Integer.valueOf(1);
                    rekanan.save();
                    MailQueueDao.kirimKonfirmasiPendaftaran(rekanan.rkn_id, rekanan.rkn_email, rekanan.rkn_namauser,
                            rekanan.rkn_nama, rekanan.rkn_alamat, referer);
                    flash.success("Data berhasil tersimpan");
                    previewData(rekanan.rkn_id);
                } catch (Exception e) {
                    Logger.error(e, "Pendaftaran Penyedia gagal tersimpan");
                    params.flash();
                    flash.error("Data gagal tersimpan"); // set notifikasi error
                    mendaftarEmailConfirmed(Crypto.encryptAES(rekanan.rkn_email));
                }
            }
        }
    }

    public static void previewData(Long rkn_id) {
        renderArgs.put("rekanan", Rekanan.findById(rkn_id));
        renderTemplate("publik/preview-form-mendaftar.html");
    }

    public static void detilPesanBerjalanPopup(Long beritaId) {
        Berita berita = Berita.findById(beritaId);
        BlobTable blob = null;
        if (berita.brt_id_attachment != null)
            blob = BlobTableDao.getLastById(berita.brt_id_attachment);
        renderArgs.put("berita", berita);
        renderArgs.put("blob", blob);
        renderTemplate("/publik/popup-detil-pesan-berjalan.html");
    }

    public static void detilRekananBlacklist(Long Id) {
        notFound("Halaman Tidak ditemukan");
        Date now = newDate();
        Blacklist blacklist = Blacklist.find("bll_tglawal <= ? AND bll_tglakhir >= ? AND rkn_id = ?", now, now, Id).first();
        renderArgs.put("blacklist", blacklist);
        renderTemplate("/publik/detil-rekanan-blacklist.html");
    }

    public static void paktaIntegritasPopup() {
        renderArgs.put("active_user", Active_user.current());
        renderTemplate("/publik/pakta-integritas.html");
    }

    public static void syaratKetentuanPopup() {
        renderTemplate("/publik/syaratKetentuan.html");
    }

    /**
     * ini hidden url untuk cek apakah mutual berhasil disetting
     */
    public static void cobaCert() {
        renderArgs.put("lpseId", ConfigurationDao.getRepoId());
        renderArgs.put("spseVersion",request.headers.get("x-tls-client-certificate").value());
        renderArgs.put("statusAdp", request.headers.get("x-tls-client-verified").value());
        renderTemplate("publik/publik-tentang-kami.html");
    }

    public static String getKontrakKamiUrl() {
        String defaultPath = request.host + Play.configuration.getProperty("http.path");
        String url = defaultPath + "/publik/kontakkami";

        if (request.secure) {
            return "https://" + url;
        }

        return "http://" + url;
    }
    
    public static void persetujuanPopup() {
        renderTemplate("/publik/publik-rekanan-mendaftar-persetujuan.html");
    }

    public static void setLanguage(String lang)
    {
        if(StringUtils.isEmpty(lang))
            lang = "id";
       Lang.change(lang);
       index();
    }
}
