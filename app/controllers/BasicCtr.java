package controllers;

import ams.contracts.ActiveUserAms;
import ams.utils.UserAgent;
import controllers.jcommon.logger.LoggerController;
import controllers.security.AllowAccess;
import ext.FormatUtils;
import jaim.common.model.UpdateSchedule;
import models.agency.*;
import models.auditor.Auditor;
import models.common.*;
import models.jcommon.util.DateUtil;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import models.nonlelang.PesertaPl;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.nonSpk.Non_spk_seleksi;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.Http.Cookie;
import play.mvc.Http.Header;
import play.mvc.With;
import utils.Encrypt;
import utils.LogUtil;

import java.util.Collection;
import java.util.Date;

/**
 * Kelas {@code BasicCtr} pada dasarnya mirip dengan {@code BaseCtr} yang dibuat
 * sebelumnya yaitu sebagai superclass untuk semua controller
 * 
 * @author I Wayan Wiprayoga Wisesa
 */
@With({ LoggerController.class })
public class BasicCtr extends Controller {

	public static final String CONTEXT_PATH = Play.configuration.getProperty("http.path");

	public static final String SIKAP_URL = Play.configuration.getProperty("sikap.url", "https://sikap.lkpp.go.id");

	public static final String SIRUP_URL = Play.configuration.getProperty("sirup.url", "https://sirup.lkpp.go.id/sirup");
	
	public static final String INAPROC_URL = Play.configuration.getProperty("inaproc.url", "http://inaproc.id");

	public static final String DEFAULT_CACHE = Play.configuration.getProperty("cache.expires", "5mn");

	private static long accessCount = 0;
	
	public static String LPSE_URL = getRequestHost();
	
	/**
	 * Fungsi {@code interuptor} merupakan fungsi interuptor yang dijalankan
	 * sebelum sebuah fungsi pada kelas yang meng-<i>extend</i> kelas ini di-
	 * <i>invoke</i>.
	 */
	@Before(unless = { "LoginCtr.logout", "clearUserSession"})
	static void interuptor() {
		if (Cache.get("STARTUP_ERROR") != null)
			renderTemplate("errors/configNotComplete.html");
		if(StringUtils.isEmpty(Lang.get()))
			Lang.change("id");
		Logger.info("current lang is %s", Lang.get());
		/**
		 * Untuk akses pertama kali, tampilkan informasi request
		 */
		accessCount++;
		if (accessCount == 1) {
			Logger.info("[STARTUP] First request information\t URL %s, remoteAddress: %s, host: %s", request.url,request.remoteAddress, request.host);
			Collection<Header> coll = request.headers.values();
			StringBuilder str = new StringBuilder(1024);
			for (Header header : coll)
				str.append(header.name).append(": ").append(header.value()).append(", ");
			Logger.info("[STARTUP] First request information\t headers: %s", str);			
		}
		/*
		 * Jika ada AllowAccess maka wajib memeriksa antara Group dan User yang
		 * sedang login. Jika user tidak masuk dalam grup maka forbidden().
		 * 
		 * Jika method tidak memiliki @AllowAccess artinya tidak ada security
		 * dan asumsinya DEFAULT SEMUA BOLEH AKSES Programmer wajib
		 * memperhatikan hal ini.
		 */
		AllowAccess allow = (AllowAccess) getActionAnnotation(AllowAccess.class);
		if (allow != null) { // cek otoritas terhadap fungsi yang dijalankan
			boolean otorisasi = false;
			String user_session = "UNAUTHORIZED_USER";
			Active_user active_user = Active_user.current();
			if (active_user != null) {
				user_session = active_user.userid;
				otorisasi = active_user.isAuthorized(allow.value());
			}

			/**
			 * [ANDIK} Pada mode development, pengecekan activeUser ditiadakan
			 * supaya tidak selalu login
			 * 
			 */
			if (!otorisasi) {//
				/**
				 * Untuk Controller DataTableCtr yang memberikan return berupa
				 * json maka wajib diberikan pesan error yang berbeda karena
				 * forbidden() tidak akan menampilkan apa-apa kecuali table yang
				 * kosong. Ini akan membuat bingung karena tidak jelas errornya.
				 * Oleh karena itu wajib dibuat pesan error di sisi Server
				 * 
				 */
				if (request.action.contains("DataTableCtr"))
					throw new IllegalAccessError(String.format("FORBIDDEN User: %s tidak diijinkan mengakses action: %s", user_session, request.action));
				Logger.error("FORBIDDEN User: %s tidak diijinkan mengakses action: %s", user_session, request.action);
				String message = Messages.get("not-authorized");
				if (active_user == null)
					message += " karena <b>belum login atau session telah habis</b>.";
				forbidden(message); // tampilkan halaman error
			} else if(active_user != null) {
				active_user.lastRequest = newDate().getTime();
				active_user.updateLoggedIn();
				// set variable active_user.get() agar bisa diakses disemua template
				renderArgs.put("active_user", active_user);
			}
		}
		renderArgs.put("namaLpse", ConfigurationDao.getNamaLpse());
		renderArgs.put("serverTime", controllers.BasicCtr.newDate());
		renderArgs.put("utc_offset", FormatUtils.TIMEZONE_OFFSET);
		renderArgs.put("production", ConfigurationDao.isProduction());
		renderArgs.put("enableMultiUserInfo", ConfigurationDao.isEnableMultiUserInfo());
		renderArgs.put("zona", FormatUtils.TIMEZONE_LOKAL);
		renderArgs.put("tahun", "2006-"+DateUtil.getTahunSekarang());
		renderArgs.put("counter", FormatUtils.formatDesimal(Counter.getTotalCounter()));
		renderArgs.put("versi", ConfigurationDao.getVersi());
		renderArgs.put("enableDevPartner", ConfigurationDao.isEnableDevPartner());
		renderArgs.put("i18nMsg", play.i18n.Messages.class);
		if(Cache.get(UpdateSchedule.CACHE_KEY) != null){
			renderArgs.put("updateSchedule", Cache.get(UpdateSchedule.CACHE_KEY, UpdateSchedule.class));
		}
		if (Play.mode.isDev()) { 
			// untuk mempercepat development , action yg dieksekusi perlu diinfo ke developer
			Logger.debug("action yang diakses : %s(%s)", request.action, request.url);
		}
	}

	/**
	 * Fungsi {@code clearUserSession} digunakan untuk membersihkan session
	 * pengguna
	 */
	protected static void clearUserSession() {
		Active_user.clear();
		Cache.safeDelete(session.getId()); // pastikan objek session sudah dihapus jika ada di Cache		
		session.clear(); // bersihkan session pengguna		
		response.setHeader("Pragma-directive", "no-cache");
		response.setHeader("Cache-directive", "no-cache");
		response.setHeader("Cache-control", "no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "0");
	}

	private static final String SECRET_KEY_INAPROC = "mysecretkey";

	/**
	 * Fungsi {@code getURLUserManual} digunakan sebagai generator URL untuk
	 * akses manual SPSE bagi setiap user pada aplikasi INAPROC
	 * 
	 * @return URL ter-enkripsi
	 */
	public static String getURLUserManual() {
		return BasicCtr.INAPROC_URL+"/unduh?q=Petunjuk%20Penggunaan";
	}

	/**
	 * Fungsi {@code getURLAplikasiEprocLain} digunakan sebagai generator URL
	 * untuk akses link aplikasi pendukung e-procurement lainnya pada aplikasi
	 * INAPROC
	 * 
	 * @return URL ter-enkripsi
	 */
	public static String getURLAplikasiEprocLain() {
		Active_user active_user = Active_user.current();
		String params = active_user.userid + '|' + active_user.name + '|' + active_user.group + '|' + ConfigurationDao.getRepoId() + '|' +ConfigurationDao.isProduction() + '|' + FormatUtils.ttsDateFormat.format(newDate());
		return BasicCtr.INAPROC_URL+"/spse/callback/"+ Encrypt.encryptInaprocURL(params, SECRET_KEY_INAPROC);
	}

	/**
	 * Fungsi {@code getURLApendo} digunakan sebagai generator URL untuk akses
	 * apendo bagi user PANITIA dan REKANAN pada aplikasi INAPROC
	 * 
	 * @return URL ter-enkripsi
	 */
	public static String getURLApendo() {
		return BasicCtr.INAPROC_URL+"/unduh?q=Aplikasi";
	}

	/**
	 * Untuk mendapatkan hasil yang benar ketika Play running di belakang
	 * mod_proxy apache, pastikan ditambahkan XForwardedSupport=ALL di
	 * conf/application.conf
	 * 
	 */
	public static String getRequestHost() {
		if(request != null) {
			if (request.secure)
				return "https://" + request.host;
			else
				return "http://" + request.host;
		}
		return "";
	}

	// dapatkan sesi pelatihan berdasarkan cookies
	public static SesiPelatihan getSesiPelatihan() {
		if (request == null)
			throw new RuntimeException("Variable 'request' is null. This method is not allowed for Job");
		Cookie cookie = request.cookies.get("sesiPelatihan");
		int id = 0;
		if (cookie != null)// jika tidak ada maka deffault
			id = Integer.parseInt(cookie.value);
		SesiPelatihan sesi = Cache.get("sesiPelatihan$" + id, SesiPelatihan.class);
		if (sesi == null) {
			sesi = SesiPelatihan.findById(id);
			if(sesi != null)
				Cache.set("sesiPelatihan$" + id, sesi);
		}
		return sesi;
	}

	/**
	 * NewDate di semua class harus mengarah ke sini karena akan di-create
	 * sesuai dengan sesiPelatihan yang dipilih
	 * 
	 * @return
	 */
	public static Date newDate() {
		if (ConfigurationDao.isProduction()) {
			return DateUtil.newDate();
		} else {
			SesiPelatihan sp = getSesiPelatihan();
			if (sp == null)
				return DateUtil.newDate(0);
			else
				return DateUtil.newDate(sp.id);
		}
	}
	// chek otorisasi active user terhadap data penyedia yang diakses
	protected static void otorisasiDataPenyedia(Long rekananId) {
		Active_user active_user = Active_user.current();
		if (active_user != null && !rekananId.equals(active_user.rekananId)) {
			forbidden(Messages.get("not-authorized"));
		}
	}

	// chek otorisasi active user terhadap data panitia yang diakses
	protected static void otorisasiDataPanitia(Long panitiaId) {
		boolean allow = false;
		Active_user active_user = Active_user.current();
		if(active_user != null && active_user.isKuppbj())
			allow = Panitia.count("pnt_id=? AND ukpbj_id =? ", panitiaId, active_user.ukpbjId) > 0;
		if(active_user != null && active_user.isPanitia())
			allow = Anggota_panitia.isAnggotaPanitia(panitiaId, active_user.pegawaiId);
		if (!allow) {
			forbidden(Messages.get("not-authorized"));
		}
	}
	
	
	//chek otorisasi active user terhadap data pp yang di akses
	protected static void otorisasiDataPp(Long ppId){
		Active_user active_user = Active_user.current();
		boolean isPp = Pp.isPp(ppId, active_user.pegawaiId);
		if (active_user != null && !isPp){
			forbidden(Messages.get("not-authorized"));
		}
	}

	//chek otorisasi active user terhadap data pp yang di akses
	protected static void otorisasiDataPpk(Long ppkId){
		Active_user active_user = Active_user.current();
		if (active_user != null && !ppkId.equals(active_user.ppkId)){
			forbidden(Messages.get("not-authorized"));
		}
	}

	// chek otorisasi active user terhadap data auditor yang diakses
	protected static void otorisasiDataAuditor(Long auditorId) {
		Active_user active_user = Active_user.current();
		Auditor auditor = Auditor.find("peg_id=?", active_user.pegawaiId).first();
		if (active_user != null && !auditorId.equals(auditor.auditorid)) {
			forbidden(Messages.get("not-authorized"));
		}
	}

	// chek otorisasi active user terhadap data lelang yang diakses
	protected static void otorisasiDataLelang(Long lelangId) {
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {
			StatusLelang statusLelang = Lelang_seleksi.findStatusLelang(lelangId);
			Peserta peserta = Peserta.findBy(lelangId, active_user.rekananId);
			if (peserta == null || !(statusLelang.isAktif() || statusLelang.isDitutup()))
				forbidden(Messages.get("not-authorized"));

		} else if (active_user.isPanitia() || active_user.isPpk() || active_user.isAuditor()) {
			Paket paket = Paket.findByLelang(lelangId);
			authByUserRole(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}

	// chek otorisasi active user terhadap data non tender yang diakses
	protected static void otorisasiDataNonTender(Long lelangId) {
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {
			StatusPl statusPl = Pl_seleksi.findStatusLelang(lelangId);
			PesertaPl peserta = PesertaPl.findBy(lelangId, active_user.rekananId);
			if (peserta == null || !statusPl.isAktif())
				forbidden(Messages.get("not-authorized"));

		} else if (active_user.isPanitia() || active_user.isPP() || active_user.isPpk() || active_user.isAuditor()) {
			Paket_pl paket = Paket_pl.findBy(lelangId);
			authByUserRolePl(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}

	protected static void otorisasiDataLelang(Lelang_seleksi lelang) {
		if(lelang == null) {
			badRequest("Tender not found");
		}
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {
			StatusLelang statusLelang = lelang.lls_status;
			Peserta peserta = Peserta.findBy(lelang.lls_id, active_user.rekananId);
			if (peserta == null || !statusLelang.isAktif())
				forbidden(Messages.get("not-authorized"));

		} else if (active_user.isPanitia() || active_user.isPpk() || active_user.isAuditor()) {
			Paket paket = lelang.getPaket();
			authByUserRole(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}

	private static void authByUserRole(Active_user active_user, Paket paket) {
		if(paket != null) {
			switch (active_user.group) {
				case PANITIA:
					boolean isPanitia = Anggota_panitia.isAnggotaPanitia(paket.pnt_id, active_user.pegawaiId);
					if (!isPanitia)
						forbidden(Messages.get("not-authorized"));
					break;
				case PPK:
					if (Paket_anggaran.count("pkt_id=? AND ppk_id=?", paket.pkt_id, active_user.ppkId) == 0L)
						forbidden(Messages.get("not-authorized"));
					break;
				case AUDITOR:
					Auditor auditor = Auditor.find("peg_id=?", active_user.pegawaiId).first();
					if (!Auditor.isAllowAuditorAksesPaket(auditor.auditorid, paket.pkt_id))
						forbidden(Messages.get("not-authorized"));
					break;
			}
		}
	}

	// chek otorisasi active user terhadap data pengadaan yang diakses
		protected static void otorisasiDataPl(Long plId) {
			Active_user active_user = Active_user.current();
			if (active_user == null)
				forbidden();
			Paket_pl paket_pl = Paket_pl.findBy(plId);
			if (!active_user.isAdminPPE()) {
				switch (active_user.group) {
				case PP:
					if (!paket_pl.pp_id.equals(active_user.ppId))
						forbidden(Messages.get("not-authorized"));
					break;
				case PANITIA:
					boolean isPanitia = Anggota_panitia.isAnggotaPanitia(paket_pl.pnt_id, active_user.pegawaiId);
					if (!isPanitia)
						forbidden(Messages.get("not-authorized"));
					break;
				case REKANAN:
					PesertaPl peserta = PesertaPl.findBy(plId, active_user.rekananId);
					if (peserta == null)
						forbidden(Messages.get("not-authorized"));
					break;
				case PPK:
					//if (!paket_pl.ppk_id.equals(active_user.ppkId))
					if (Paket_anggaran_pl.count("pkt_id=? AND ppk_id=?", paket_pl.pkt_id, active_user.ppkId) == 0L)
						forbidden(Messages.get("not-authorized"));
					break;
				case AUDITOR:
					Auditor auditor = Auditor.find("peg_id=?", active_user.pegawaiId).first();
					if (!Auditor.isAllowAuditorAksesPaketNonLelang(auditor.auditorid, paket_pl.pkt_id))
						forbidden(Messages.get("not-authorized"));
					break;
				default:
					forbidden(Messages.get("not-authorized"));
					break;
				}
			}
		}

		// chek otorisasi active user terhadap data pengadaan yang diakses
		protected static void otorisasiDataNonSpk(Long plId) {
			Active_user active_user = Active_user.current();
			if (active_user == null)
				forbidden();
			if (!active_user.isAdminPPE()) {
				switch (active_user.group) {
					case REKANAN:
						PesertaPl peserta = PesertaPl.findBy(plId, active_user.rekananId);
						if (peserta == null)
							forbidden(Messages.get("not-authorized"));
						break;
					case PPK:
//						if (!paket_pl.ppk_id.equals(active_user.ppkId))
						if (Paket_anggaran_pl.count("ppk_id=?", active_user.ppkId) == 0L)
							forbidden(Messages.get("not-authorized"));
						break;
					default:
						forbidden(Messages.get("not-authorized"));
						break;
				}
			}
		}

	protected static void otorisasiDataNonSpk(Non_spk_seleksi spk_seleksi) {
		if(spk_seleksi == null) {
			badRequest("Pencatatan not found");
		}
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {

			StatusPl statusLelang = spk_seleksi.lls_status;
			PesertaPl peserta = PesertaPl.findBy(spk_seleksi.lls_id, active_user.rekananId);
			if (peserta == null || !statusLelang.isAktif())
				forbidden(Messages.get("not-authorized"));

		} else if (active_user.isPanitia() || active_user.isPpk() || active_user.isPP() || active_user.isAuditor()) {
			Paket_pl paket = spk_seleksi.getPaket();
			authByUserRolePl(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}
		
	// chek otorisasi active user terhadap data pengadaan yang diakses
	protected static void otorisasiDataSwakelola(Long swaId) {
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if (!active_user.isAdminPPE()) {
			switch (active_user.group) {
				case REKANAN:
					PesertaPl peserta = PesertaPl.findBy(swaId, active_user.rekananId);
					if (peserta == null)
						forbidden(Messages.get("not-authorized"));
					break;
				case PPK:
					//if (!paket_swakelola.ppk_id.equals(active_user.ppkId))
					if(Paket_anggaran_swakelola.count("ppk_id=?", active_user.ppkId) == 0L)
						forbidden(Messages.get("not-authorized"));
					break;
				default:
					forbidden(Messages.get("not-authorized"));
					break;
			}
		}
	}

	// chek otorisasi active user terhadap data peserta yang diakses
	protected static void otorisasiDataPeserta(Peserta peserta) {
		Active_user active_user = Active_user.current();
		if (active_user.isRekanan())
			otorisasiDataPenyedia(peserta.rkn_id);
		else {
			Paket paket = Paket.findByLelang(peserta.lls_id);
			otorisasiDataPaket(paket);
		}
	}

	// chek otorisasi active user terhadap data peserta yang diakses
	protected static void otorisasiDataPesertaPl(PesertaPl peserta) {
		Active_user active_user = Active_user.current();
		if (active_user.isRekanan())
			otorisasiDataPenyedia(peserta.rkn_id);
		else {
			Paket_pl paket = Paket_pl.findBy(peserta.lls_id);
			otorisasiDataPaketPl(paket);
		}
	}

	// chek otorisasi active user terhadap data paket yang diakses
	protected static void otorisasiDataPaket(Paket paket) {
		if(paket == null)
			forbidden(Messages.get("not-authorized"));
		Active_user active_user = Active_user.current();
		if (active_user.isPanitia())
			otorisasiDataPanitia(paket.pnt_id);
		else if (active_user.isPpk()) {
			// chek otorisasi active user terhadap data ppk yang diakses
			if (!Paket_anggaran.isPkkInThisPaket(paket.pkt_id, active_user.ppkId)) {
				forbidden(Messages.get("not-authorized"));
			}
		}
		else if (active_user.isAuditor()) {
			Auditor auditor = Auditor.find("peg_id=?", active_user.pegawaiId).first();
			if (!Auditor.isAllowAuditorAksesPaket(auditor.auditorid, paket.pkt_id))
				forbidden(Messages.get("not-authorized"));
		}
		else if (active_user.isKuppbj()){
			if(paket.ukpbj_id != null && !paket.ukpbj_id.equals(active_user.ukpbjId)){
				forbidden(Messages.get("not-authorized"));
			}
		}
	}
	
	// chek otorisasi active user terhadap data paket yang diakses
	protected static void otorisasiDataPaketPl(Paket_pl paket) {
		Active_user active_user = Active_user.current();
		if (active_user.isPanitia())
			otorisasiDataPanitia(paket.pnt_id);
		else if (active_user.isPP()){
			otorisasiDataPp(paket.pp_id);
		}
		else if (active_user.isPpk()) {
			//otorisasiDataPpk(paket.ppk_id);
			// chek otorisasi active user terhadap data ppk yang diakses
			if (active_user != null && Paket_anggaran_pl.count("pkt_id=? AND ppk_id=?", paket.pkt_id, active_user.ppkId) == 0L) {
				forbidden(Messages.get("not-authorized"));
			}
		}
		else if(active_user.isAuditor()){
			Auditor auditor = Auditor.find("peg_id=?", active_user.pegawaiId).first();
			if (!Auditor.isAllowAuditorAksesPaketNonLelang(auditor.auditorid, paket.pkt_id))
				forbidden(Messages.get("not-authorized"));
		}
	}

	protected static void otorisasiDataPl(Pl_seleksi pl) {
		if(pl == null) {
			badRequest("Non Tender not found");
		}
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {

			StatusPl statusLelang = pl.lls_status;
			PesertaPl peserta = PesertaPl.findBy(pl.lls_id, active_user.rekananId);
			if (peserta == null || !statusLelang.isAktif())
				forbidden(Messages.get("not-authorized"));

		} else if (active_user.isPanitia() || active_user.isPpk() || active_user.isPP() || active_user.isAuditor()) {
			Paket_pl paket = pl.getPaket();
			authByUserRolePl(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}

	private static void authByUserRolePl(Active_user active_user, Paket_pl paket) {
		if(paket != null) {
			switch (active_user.group) {
				case PP:
					if (!paket.pp_id.equals(active_user.ppId))
						forbidden(Messages.get("not-authorized"));
					break;
				case PANITIA:
					boolean isPanitia = Anggota_panitia.isAnggotaPanitia(paket.pnt_id, active_user.pegawaiId);
					if (!isPanitia)
						forbidden(Messages.get("not-authorized"));
					break;
				case PPK:
					if (Paket_anggaran_pl.count("pkt_id=? AND ppk_id=?", paket.pkt_id, active_user.ppkId) == 0L)
						forbidden(Messages.get("not-authorized"));
					break;
				case AUDITOR:
					Auditor auditor = Auditor.find("peg_id=?", active_user.pegawaiId).first();
					if (!Auditor.isAllowAuditorAksesPaket(auditor.auditorid, paket.pkt_id))
						forbidden(Messages.get("not-authorized"));
					break;
			}
		}
	}

	// chek otorisasi active user terhadap data paket yang diakses
	protected static void otorisasiDataPaketNonSpk(Paket_pl paket) {
		Active_user active_user = Active_user.current();
		if (active_user.isPpk())
			otorisasiDataPpk(active_user.ppkId);
	}


	// chek otorisasi active user terhadap data paket yang diakses
	protected static void otorisasiDataPaketSwakelola(Paket_swakelola paket) {
		Active_user active_user = Active_user.current();
		if (active_user.isPpk())
			otorisasiDataPpk(active_user.ppkId);
	}
	/**
     * Check that the current user can edit vendor data
     */
    protected static void checkStatusMigrasi() {
        if (!Active_user.isCurrentBelumMigrasi()) {
            forbidden(Messages.get("sedang.migrasi.tidak.boleh.edit"));
        }
    }

	@Before(unless = {"PublikCtr.index", "CertCtr.userInfo", "OSDRegServiceCtr.getCertServer"})
    protected static void setAmsNotifications() {
    	Active_user active_user = Active_user.current();
    	if (active_user == null || (ConfigurationDao.isOSD(newDate()) && !UserAgent.allowUserAgent(request))
				|| (!active_user.isPanitia())) { // sementara AMS buat penyedia di disabled, hanya aktif pegawai
			LogUtil.debug("BasicCtr", "not allow to show ams notification!");
    		return;
		}
		renderArgs.put("showAmsNotification", true);
		ActiveUserAms.AmsNotification notification = active_user.generateAmsNotification(request.action);
		if (notification != null) {
			renderArgs.put(notification.getKey(), notification.isValue());
			if (notification.getPayload() != null) {
				renderArgs.put(notification.getKey()+"Message", notification.getPayload());
			}
		}
	}
	
	protected static void otorisasiDataLelangPeserta(Peserta peserta) {
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {
			StatusLelang statusLelang = Lelang_seleksi.findStatusLelang(peserta.lls_id);
			Peserta _peserta = Peserta.findBy(peserta.lls_id, active_user.rekananId, peserta.psr_id);
			if (_peserta == null || !statusLelang.isAktif())
				forbidden(Messages.get("not-authorized"));
		} else if (active_user.isPanitia() || active_user.isPpk() || active_user.isAuditor()) {
			Paket paket = Paket.findByLelang(peserta.lls_id);
			authByUserRole(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}
	
	protected static void otorisasiDataPlPeserta(PesertaPl peserta) {
		Active_user active_user = Active_user.current();
		if (active_user == null)
			forbidden();
		if(active_user.isRekanan()) {
			StatusPl statusLelang = Pl_seleksi.findStatusLelang(peserta.lls_id);
			PesertaPl _peserta = PesertaPl.findBy(peserta.lls_id, active_user.rekananId, peserta.psr_id);			
			if (_peserta == null || !statusLelang.isAktif())
				forbidden(Messages.get("not-authorized"));
		} else if (active_user.isPanitia() || active_user.isPpk() || active_user.isPP() || active_user.isAuditor()) {
			Paket_pl paket = Paket_pl.findBy(peserta.lls_id);
			authByUserRolePl(active_user, paket);
		} else {
			forbidden(Messages.get("not-authorized"));
		}
	}

	// otorisasi header authorization, digunakn untuk shortcut
	protected static void otorisasiSecretKey() {
		Header header = request.headers.get("authorization");
		if(header == null)
			forbidden(Messages.get("not-authorized"));
		if(StringUtils.isEmpty(header.value()) || !header.value().equals(Play.secretKey))
			forbidden(Messages.get("not-authorized"));
	}

	/**
	 * Shortcode of flash.success(Messages.get(i18nKey, params));
	 * @param i18nKey key use in translation files
	 * @param params translation parameters
	 */
	protected static void flashSuccess(String i18nKey, Object... params) {
		flash.success(Messages.get(i18nKey, params));
	}

	/**
	 * Shortcode of flash.error(Messages.get(i18nKey, params));
	 * @param i18nKey key use in translation files
	 * @param params translation parameters
	 */
	protected static void flashError(String i18nKey, Object... params) {
		flash.error(Messages.get(i18nKey, params));
	}
}
