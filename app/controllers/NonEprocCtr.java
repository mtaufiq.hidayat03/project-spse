package controllers;

import controllers.security.AllowAccess;
import ext.FormatUtils;
import models.agency.Satuan_kerja;
import models.announce.Pengumuman_lelang;
import models.announce.Pll_paket;
import models.announce.Pll_tahap;
import models.common.ConfigurationDao;
import models.jcommon.util.DateUtil;
import models.secman.Group;
import play.data.validation.Valid;
import play.mvc.Before;

import java.util.ArrayList;
import java.util.List;

/**
 * controller untuk handle modul Tender NON EPROC
 * @author Arief Ardiyansah
 *
 */
public class NonEprocCtr extends BasicCtr {
	
	@Before (unless={"lelang_umum", "view"})
	public static void check() {
		if (!ConfigurationDao.isNonEprocEnabled()) { // jika fitur pembuatan paket non eprocurement tidak aktif
			forbidden("Fitur pembuatan lelang non-eProcurement tidak aktif!");
		}
	}

	/**
	 * Fungsi {@code nonEproc} digunakan untuk menampilkan halaman utama fitur lelang non-eProcurement.
	 */
	@AllowAccess({Group.PANITIA})
	public static void index() {
		renderTemplate("nonEproc/pengumuman.html");
	}
	
    @AllowAccess({Group.PANITIA})
    public static void tambah(boolean multipaket) {
         renderArgs.put("multipaket",multipaket);
    	 Pengumuman_lelang pgn_lelang = new Pengumuman_lelang();
         pgn_lelang.pll_tahun = DateUtil.getTahunSekarang();
         pgn_lelang.pll_tgl_buat = controllers.BasicCtr.newDate();       
         pgn_lelang.pll_status = Pengumuman_lelang.Status.DRAFT;
         renderArgs.put("pgn_lelang", pgn_lelang);
         List<Pll_paket> paketList = new ArrayList<Pll_paket>(1);
         paketList.add(new Pll_paket());
         renderArgs.put("paketList",paketList);
         renderArgs.put("tahapAmbil",new Pll_tahap());
         renderTemplate("nonEproc/pengumumanEdit.html");
    }
    
    @AllowAccess({Group.PANITIA})
    public static void edit(Long id) {
        Pengumuman_lelang pgn_lelang = Pengumuman_lelang.findById(id);
        renderArgs.put("pgn_lelang",pgn_lelang);
        String instansiId = null;
        if(pgn_lelang.stk_id != null) {
        	Satuan_kerja satker = Satuan_kerja.findById(pgn_lelang.stk_id);
            renderArgs.put("instansiId", satker.instansi_id);
        }
        List<Pll_paket> paketList = Pll_paket.find("pll_id=?", pgn_lelang.pll_id).fetch();
        renderArgs.put("paketList", paketList);
        renderArgs.put("tahapAmbil",Pll_tahap.find("pll_id=? order by nama_tahap", pgn_lelang.pll_id).first());
        renderArgs.put("multipaket",paketList.size() > 1);
        renderTemplate("nonEproc/pengumumanEdit.html");
    }

    @AllowAccess({Group.PANITIA})
    public static void simpan(Pengumuman_lelang pgn_lelang, @Valid List<Pll_paket> paketList, @Valid Pll_tahap tahapAmbil, Long panitiaId) {
        checkAuthenticity();
        if(pgn_lelang.pll_id != null) {
        	Pengumuman_lelang obj = Pengumuman_lelang.findById(pgn_lelang.pll_id);
        	pgn_lelang.pll_tgl_buat = obj.pll_tgl_buat;
        	pgn_lelang.pll_status = obj.pll_status;
        }
        else if(pgn_lelang.pll_tgl_buat == null)
            pgn_lelang.pll_tgl_buat = controllers.BasicCtr.newDate();
        pgn_lelang.pnt_id = panitiaId;
        if(params.get("simpanDraft") != null)
            pgn_lelang.pll_status = Pengumuman_lelang.Status.DRAFT;
        else if (params.get("simpanUmumkan") != null) {
            pgn_lelang.pll_status = Pengumuman_lelang.Status.PUBLISH;
            pgn_lelang.pll_tgl_diumumkan = controllers.BasicCtr.newDate();
        }
        validation.valid(pgn_lelang);
        if(validation.hasErrors()) {
            validation.keep();
            params.flash();
            flash.keep();
            if(pgn_lelang.pll_id != null)
                edit(pgn_lelang.pll_id);
            else {
                boolean multiple = (!paketList.isEmpty() && paketList.size() > 1);
                tambah(multiple);
            }
        } else if (params.get("hapus") != null) {           
           pgn_lelang.delete();
        } else {          
        	Pengumuman_lelang.save(pgn_lelang, paketList, tahapAmbil);
            index();
        }
    }

    public static void view(Long id) {
        Pengumuman_lelang pgn_lelang = Pengumuman_lelang.findById(id);
        renderArgs.put("pgn_lelang", pgn_lelang);
        renderArgs.put("paketList",Pll_paket.find("pll_id=?", pgn_lelang.pll_id).fetch());
        Pll_tahap tahapAmbil = Pll_tahap.find("pll_id=? order by nama_tahap", pgn_lelang.pll_id).first();
        renderArgs.put("tahapAmbil",tahapAmbil);
        renderArgs.put("tglPengumuman",FormatUtils.formatDateInd(pgn_lelang.pll_tgl_diumumkan));
        renderArgs.put("tglAmbil", FormatUtils.formatDateInd(tahapAmbil.pll_tgl_awal) + " s.d. " + FormatUtils.formatDateInd(tahapAmbil.pll_tgl_akhir));
        renderTemplate("nonEproc/pengumumanView.html");
    }

    public static void lelang_umum(){
        renderTemplate("nonEproc/pengumumanPublik.html");
    }
}
