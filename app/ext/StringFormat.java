package ext;

import models.agency.Anggota_panitia;
import models.agency.Panitia;
import models.agency.Pegawai;
import models.common.*;
import models.lelang.Peserta;
import models.secman.Usergroup;
import org.apache.commons.lang3.StringUtils;
import play.i18n.Messages;
import play.templates.JavaExtensions;
import utils.HtmlUtil;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

public class StringFormat extends JavaExtensions {	

	/**
	 * This ext is usefull for checking URL
	 */
	public static boolean startsWith(String str1, String str2) {
		return str1.startsWith(str2);
	}
	
	/**
	 * format tanggal untuk penggunaan datepicker
	 * @param date
	 * @return
	 */
	public static String toDatepicker(Date date) {
		if(date == null)
			return "";
		return DateBinder.format.format(date);
	}
	
	/**
	 * format tanggal untuk penggunaan datetimepicker
	 * @param date
	 * @return
	 */
	public static String toDatetimepicker(Date date) {
		if(date == null)
			return "";
		return DatetimeBinder.format.format(date);
	}


	/**
	 * Format date/time should be Indonesian
	 */
	public static String formatDateTime(Date dt) {
		return FormatUtils.formatDateTimeInd(dt);
	}

	/**
	 * Fungsi {@code rupiah} merupakan ekstensi template Play!Framework untuk format
	 * tanggal ke dalam format tanggal untuk regional Indonesia. Fungsinya dapat digunakan sebagai
	 * pengganti fungsi {@link FormatUtils#formatDateInd(java.util.Date) formatDateInd}
	 * Cara penggunaan di Template : ${some_variable_type_date.formatDate()}
	 *
	 * @param dt tanggal yang diformat
	 * @return string terformat
	 */
	public static String formatDate(Date dt) {
		if (dt != null) {
			return FormatUtils.formatDateInd(dt);
		}
		return "";
	}

	public static String formatDouble(Double number) {
		String hasil = "";
		if (number != null) {
			hasil =  new DecimalFormat("#0.00").format(number);
			return hasil.replace(".",",");
		}
		return hasil;
	}

	/**
	 * Fungsi {@code rupiah} merupakan ekstensi template Play!Framework untuk format
	 * nilai ke dalam format rupiah. Fungsinya dapat digunakan sebagai
	 * pengganti fungsi {@link FormatUtils#formatCurrencyRupiah(Number) formatCurrencyRupiah}
	 * Cara penggunaan di Template : ${some_variable_type_double.rupiah()}
	 *
	 * @param harga nilai yang diformat
	 * @return string terformat
	 */
	public static String rupiah(Double harga) {
		return FormatUtils.formatCurrencyRupiah(harga);
	}

	public static String rupiahDalamJuta(Double harga) {
		return FormatUtils.formatCurrenciesJutaRupiah(harga);
	}

	public static long jumlahAnggota(Panitia panitia) {
		long count = Anggota_panitia.count("pnt_id", panitia.pnt_id);
		return count;
	}

	public static String allgroup(String username) {
		Pegawai user = Pegawai.findBy(username);
		StringBuilder grups = new StringBuilder();
		if (user != null) {
			List<Usergroup> lGroup = Usergroup.find("userid", user.peg_namauser).fetch();
			if (lGroup != null && !lGroup.isEmpty()) {
				for (int a = 0; a < lGroup.size(); a++) {
					Usergroup ug = lGroup.get(a);
					if (a == 0) {
						grups.append(ug.idgroup.getLabel());
						// grup = grup + ug.getGroup().getGroupname();
					} else {
						grups.append(',').append(ug.idgroup.getLabel());
						// grup = grup + "," + ug.getGroup().getGroupname();
					}
				}
			}
		} else {
			grups.append(Messages.get("ct.tpau"));
		}
		return grups.toString();
	}

	public static long peserta(Long lelangId) {
		return Peserta.count("lls_id", lelangId);
	}

	public static String mtdPemilihan(Integer id) {
		return MetodePemilihan.findById(id).getLabel();
	}

	public static String mtdKualifikasi(Integer id) {
		return MetodeKualifikasi.fromValue(id).label;
	}

	public static String mtdDokumen(Integer id) {
		return MetodeDokumen.fromValue(id).label;
	}

	public static String mtdEvaluasi(Integer id) {
		return MetodeEvaluasi.findById(id).label;
	}

	public static boolean isExpire(Date date) {
		return date.before(controllers.BasicCtr.newDate());
	}
	
	
	public static String formatInteger(Double data)
	{
		if(data==null)
			return "";
		DecimalFormat format=new DecimalFormat("###,###,###,###,###,##0");
		return format.format(data).replaceAll(",",".");
	}
	
	
	/**Convert CRLF to <br>
	 * 
	 * @param str
	 * @return
	 */
	public static String convertNewLine(String str)
	{
		if(str==null)
			return "";
		return str.replaceAll("\n", "<br>");
	}	
		
	public static String splitToCol(String text, int row) {
		if(StringUtils.isEmpty(text))
			return "";
		return HtmlUtil.textAreaView(text, row);
	}
	
	public static String namaRepo(Long repoId) {
		return ConfigurationDao.getNamaRepo(repoId);
	}	
	
	static final DecimalFormat dfkalkulasi = new DecimalFormat("#0.00");
	
	public static String calculatePrecentage(Double harga, Double total){	
		return dfkalkulasi.format((harga * 100) / total);
	}

	public static String statuslulusIcon(Integer status) {
		if(status != null ) {
			if (status.intValue() == 0)
				return "<i class=\"fa fa-close\" />";
			else if (status.intValue() == 1)
				return "<i class=\"fa fa-check\" style=\"color:#5cb85c\"/>";
			else if (status.intValue() == 2)
				return "<i class=\"fa fa-check\" style=\"color:#c60b0b\"/>";
		}
		return "<i class=\"fa fa-minus\" />";
	}
}



