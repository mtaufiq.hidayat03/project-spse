package ext;

import org.apache.commons.lang3.StringUtils;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateBinder implements TypeBinder<String> {

	 public static final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	 @Override
	 public Date bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
		 if(StringUtils.isEmpty(value))
	        return null;
	     return format.parse(value);
	 }
}
