package ext;

import play.data.binding.TypeBinder;
import play.mvc.Http;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Mengubah params string[] menjadi double[], string boleh dalam format angka saja
 * atau format rupiah misal: Rp 1.500.000,00
 *
 * @author Arief Ardiyansah
 */

public class RupiahArrayBinder implements TypeBinder<Double[]> {

    public Object bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        Http.Request req = Http.Request.current();
        Double[] result = null;
        if (req != null && req.params != null) {
            String[] res = req.params.get(name, String[].class);
            if(res != null) {
                result = new Double[res.length];
                int i = 0;
                for(String obj : res) {
                    result[i] = FormatUtils.parseRupiah(obj);
                    i++;
                }
            }
        }
        return result;
    }
}
