package ext;

import groovy.lang.Closure;
import models.agency.Paket;
import models.agency.contracts.AnggaranContract;
import models.agency.contracts.PaketAnggaranContract;
import models.agency.contracts.SirupContract;
import models.common.Active_user;
import models.devpartner.AnggaranDp;
import models.devpartner.PaketAnggaranDp;
import models.devpartner.contracts.AnggaranDpContract;
import models.devpartner.contracts.PaketAnggaranDpContract;
import org.apache.commons.collections4.CollectionUtils;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 8/15/2018
 * class ini ada untuk mengurangi duplicated code antara paket-edit dengan paket-pl-edit,
 * terutama table rup dan table anggaran karena 2 table tersebut memiliki kesamaan,
 */
public class PaketDpTag extends FastTags {

    public static void _rupTableDp(Map<?, ?> args, Closure body,  PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        List<SirupContract> sirups = (List<SirupContract>) args.get("sirups");
        if(CollectionUtils.isEmpty(sirups))
            return;
        Boolean isDraft = (Boolean) args.get("isDraft");
        if(isDraft == null)
            isDraft = Boolean.FALSE;
        Long paketId = (Long) args.get("paketId");
        StringBuilder content = new StringBuilder();
        content.append("<table class=\"table table-condensed\"><tr><th width=\"80\">"+Messages.get("ct.kdrup")+"</th>" +
                "<th>"+ Messages.get("ct.np") +"</th><th width=\"200\">"+Messages.get("ct.sbd")+"</th><th></th></tr>");
        int index = 0;
        for (SirupContract sirup : sirups) {
            content.append("<tr>");
            content.append("<td>").append(sirup.getId()).append("</td>");
            content.append("<td>").append(sirup.getNama()).append("</td>");
            content.append("<td>").append(sirup.getSumberDanaString()).append("</td>");
            content.append("<td>");
            content.append("</td>");
            content.append("</tr>");
            index++;
        }
        content.append("</table>");
        out.print(content);
    }

    public static void _anggaranTableDp(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        List<? extends PaketAnggaranDpContract> contracts = (List<? extends PaketAnggaranDpContract>) args.get("anggarans");
        StringBuilder content = new StringBuilder();
        content.append("<table id=\"tblSumberAnggaran\" class=\"table table-condensed\"><tr><th width=\"80\">"+Messages.get("ct.tahun")+"</th>" +
                "<th width=\"200\">"+Messages.get("ct.sbd")+"</th><th>"+Messages.get("ct.ka")+"</th><th width=\"150\">"+Messages.get("ct.nilai")+"</th>" +
                "<th width=\"150\">"+Messages.get("tag.ppk")+"</th></tr>");
        for (PaketAnggaranDpContract contract : contracts) {
            AnggaranDpContract anggaran = contract.getAnggaranContract();
            boolean isAnggaranExist = anggaran != null;
            content.append("<tr>");
            content.append("<td>").append(isAnggaranExist ? anggaran.getAngTahun() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist ? anggaran.getSbdLabel() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist
                    ? anggaran.getKodeRekening() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist
                    ? anggaran.getAngNilaiFormatted() : "").append("</td>");
            content.append("<td>").append(contract.getPpkNama());
            content.append("</td>");
            content.append("</tr>");
        }
        content.append("</table>");
        out.print(content);
    }
}
