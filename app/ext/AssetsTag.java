package ext;

import groovy.lang.Closure;
import models.common.ConfigurationDao;
import play.data.validation.Validation;
import play.exceptions.TagInternalException;
import play.exceptions.TemplateExecutionException;
import play.templates.FastTags;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.Map;

/**
 * @author HanusaCloud on 8/29/2018
 * gunakan tag ini untuk mempermudah pemanggilan assets,
 * jika terdapat perubahan konfigurasi hanya akan terjadi di dalam satu tempat yaitu class ini.
 * Contoh penggunaan #{assets '/js/jquery.maskedinput.min.js' /}
 */
public class AssetsTag extends FastTags {

    public static void _assets(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        final String filepath = (String) args.get("arg");
        out.print(ConfigurationDao.CDN_URL + filepath);
    }

    // custom validation class bootstrap 4
    public static void _validate(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        if (args.get("arg") == null) {
            throw new TemplateExecutionException(template.template, fromLine, "Please specify the error key", new TagInternalException(
                    "Please specify the error key"));
        }
        if (Validation.hasError(args.get("arg").toString())) {
            out.print("is-invalid");
        }
    }
}
