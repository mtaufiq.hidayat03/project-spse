package ext;

import groovy.lang.Closure;
import models.lelang.Peserta;
import models.rekanan.*;
import org.apache.commons.collections4.CollectionUtils;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/** tag terkait pengiriman data kualifikasi
 * 
 * @author arief ardiyansah
 *
 */
public class KualifikasiTag extends FastTags {

	  public static void _kirimKualifikasiAkta(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		  Peserta peserta = (Peserta)args.get("arg");
		  List<Landasan_hukum> aktaList = Landasan_hukum.findBy(peserta.rkn_id);
		  if(CollectionUtils.isEmpty(aktaList)) {
			  out.print("<div class=\"alert alert-danger\">"+ Messages.get("ct.akta_pendiri_perubahan")+"</div>");
		  } else {
			  out.print("<table class=\"table table-sm\">");
			  Landasan_hukum akta = null;
			  if(aktaList.get(0) != null) {
				  akta = aktaList.get(0);
				  out.print("<tr><th colspan=\"2\">"+Messages.get("ct.ap")+"</th></tr>");
				  out.print("<tr><th width=\"20%\" class=\"bg-warning\">"+Messages.get("ct.no")+"</th><td>"+akta.lhk_no+"</td></tr>");
				  out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.ts")+"</th><td>"+FormatUtils.formatDateInd(akta.lhk_tanggal)+"</td></tr>");
				  out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.notaris")+"</th><td>"+akta.lhk_notaris+"</td></tr>");
			  }
			  if(aktaList.size() > 1 && aktaList.get(1) != null) {
				  akta = aktaList.get(1);
				  out.print("<tr><th colspan=\"2\">"+Messages.get("ct.apt")+"</th></tr>");
				  out.print("<tr><th width=\"20%\" class=\"bg-warning\">"+Messages.get("ct.no")+"</th><td>"+akta.lhk_no+"</td></tr>");
				  out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.ts")+"</th><td>"+FormatUtils.formatDateInd(akta.lhk_tanggal)+"</td></tr>");
				  out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.notaris")+"</th><td>"+akta.lhk_notaris+"</td></tr>");
			  }			  
			  out.print("</table>");
		  }		  
		  out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
		  out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		  out.print("<div class=\" clearfix\"></div>");
		  out.print("");
	  }
	  
	  public static void _kirimKualifikasiIjin(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		  Peserta peserta = (Peserta)args.get("arg");
		  List<Long> ijinId = Query.find("select ius_id from Ijin_usaha_peserta where psr_id=?", Long.class, peserta.psr_id).fetch();
		  List<Ijin_usaha> ijinList = Ijin_usaha.find("rkn_id=?", peserta.rkn_id).fetch();
		  out.print("<table class=\"table table-condensed\">");
		  out.print("<tr><th width=\"10\"><input class=\"checkboxAll\" type=\"checkbox\" /> </th><th>"+Messages.get("ct.izin_usaha")+" </th><th>"+Messages.get("ct.nomor_surat")+"</th><th>"+Messages.get("ct.ip")+"</th></tr>");
		  for(Ijin_usaha ijin : ijinList) {
			  out.print("<tr>");
			  out.print("<td><input name=\"ijin\" class=\"checkbox\" type=\"checkbox\" value=\""+ijin.ius_id+"\" "+(ijinId != null && ijinId.contains(ijin.ius_id)? "checked":"")+"/></td>");
			  out.print("<td>"+ijin.jni_nama+"</td>");
			  out.print("<td>"+ijin.ius_no+"</td>");
			  out.print("<td>"+ijin.ius_instansi+"</td>");
			  out.print("</tr>");
		  }
		  out.print("</table>");
		  out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		  out.print("<div class=\"clearfix\"></div>");
		  out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.data_izin_usaha")+"</div>");
	  }
	  
	public static void _kirimKualifikasiPajak(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Peserta peserta = (Peserta) args.get("arg");
		List<Long> pajakId = Query.find("select pjk_id from bukti_pajak where psr_id=?", Long.class, peserta.psr_id).fetch();
		List<Pajak> pajakList = Pajak.find("rkn_id=?", peserta.rkn_id).fetch();
		out.print("<table class=\"table table-sm\">");
		out.print("<tr><th width=\"10\"><input class=\"checkboxAll\" type=\"checkbox\" /></th><th>Pajak</th><th>Tanggal</th><th>No Bukti </th></tr>");
		for (Pajak pajak : pajakList) {
			out.print("<tr>");
			out.print("<td><input class=\"checkbox\" type=\"checkbox\" name=\"pajak\" value=\""+pajak.pjk_id+"\" "+(pajakId != null && pajakId.contains(pajak.pjk_id)? "checked":"")+" /></td>");
			out.print("<td>"+pajak.pjk_jenis+" - "+(pajak.pjk_bulan != null ? pajak.pjk_bulan : "")+Messages.get("ct.tahun")+pajak.pjk_tahun+"</td>");
			out.print("<td>"+FormatUtils.formatDateInd(pajak.pjk_tanggal)+"</td>");
			out.print("<td>"+pajak.pjk_no+"</td>");
			out.print("</tr>");
		}
		out.print("</table>");
		out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
		out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		out.print("<div class=\"clearfix\"></div>");
		out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.pdpyd")+"</div>");
	}
	  
	  public static void _kirimKualifikasiStafAhli(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		  Peserta peserta = (Peserta) args.get("arg");
		  List<Long> stafId = Query.find("select stp_id from Staf_ahli_psr where psr_id=?",Long.class, peserta.psr_id).fetch();
		  List<Staf_ahli> stafAhliList = Staf_ahli.find("rkn_id=?", peserta.rkn_id).fetch();
		  out.print("<table class=\"table table-condensed\">");
		  out.print("<tr><th><input class=\"checkboxAll\" type=\"checkbox\" /></th><th>"+Messages.get("ct.nama")+"</th> <th>"+Messages.get("ct.tgl_lahir")+"</th><th>"+Messages.get("ct.pendidikan")+"</th> <th>"+Messages.get("ct.pengalaman_kerja")+"</th><th>"+Messages.get("ct.profesi")+"</th></tr>");
		  for(Staf_ahli staf : stafAhliList) {
			  out.print("<tr>");
			  out.print("<td><input type=\"checkbox\" name=\"staf\" class=\"checkbox\" value=\""+staf.sta_id+"\" "+(stafId != null && stafId.contains(staf.sta_id)? "checked":"")+"/></td>");
			  out.print("<td>"+staf.sta_nama+"</td>");
			  out.print("<td>"+FormatUtils.formatDateInd(staf.sta_tgllahir)+"</td>");
			  out.print("<td>"+staf.sta_pendidikan+"</td>");
			  out.print("<td>"+staf.sta_pengalaman+" "+Messages.get("ct.tahun")+"</td>");
			  out.print("<td>"+staf.sta_keahlian+"</td>");
			  out.print("<td>"+(staf.sta_status == 0 ? "Pegawai Tetap" : "Pegawai Tidak Tetap")+"</td>");
			  out.print("</tr>");
		  }		  
		  out.print("</table>");
		  out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
		  out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		  out.print("<div class=\"clearfix\"></div>");
		  out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.pdpyd")+"</div>");
	  }
	  
	  public static void _kirimKualifikasiPeralatan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		  Peserta peserta = (Peserta) args.get("arg");
		  List<Long> alatId = Query.find("select id_alat_peserta from Peralatan_peserta where psr_id=?",Long.class, peserta.psr_id).fetch();
		  List<Peralatan> peralatanList = Peralatan.find("rkn_id=?", peserta.rkn_id).fetch();
		  out.print("<table class=\"table table-condensed\">");
		  out.print("<tr><th><input class=\"checkboxAll\" type=\"checkbox\" /> </th><th>"+Messages.get("ct.jenis")+"</th><th>"+Messages.get("ct.jumlah")+"</th><th>"+Messages.get("ct.kapasitas")+"</th> <th>"+Messages.get("ct.merk")+"</th><th>"+Messages.get("ct.tp")+"</th><th>"+Messages.get("ct.kondisi")+"</th><th>"+Messages.get("ct.ls")+" </th><th>"+Messages.get("ct.bk")+" </th></tr>");
		  for(Peralatan alat:peralatanList) {
			  out.print("<tr>");
			  out.print("<td><input type=\"checkbox\" class=\"checkbox\" name=\"alat\" value=\""+alat.alt_id+"\" "+(alatId != null && alatId.contains(alat.alt_id)? "checked":"")+"/></td>");
			  out.print("<td>"+alat.alt_jenis+"</td>");
			  out.print("<td>"+alat.alt_jumlah+"</td>");
			  out.print("<td>"+alat.alt_kapasitas+"</td>");
			  out.print("<td>"+alat.alt_merktipe+"</td>");
			  out.print("<td>"+alat.alt_thpembuatan+"</td>");
			  out.print("<td>"+alat.alt_kondisi+"</td>");
			  out.print("<td>"+alat.alt_lokasi+"</td>");
			  out.print("<td>"+alat.alt_kepemilikan+"</td>");
			  out.print("</tr>");
		  }
		  out.print("</table>");
		  out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
		  out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		  out.print("<div class=\"clearfix\"></div>");
		  out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.pilih_data_peralatan")+"</div>");
	  }
	  
	  public static void _kirimKualifikasiPengalaman(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		  Peserta peserta = (Peserta) args.get("arg");
		  List<Long> pengalamanId = Query.find("select pen_id from Pengalaman_peserta where psr_id=?",Long.class, peserta.psr_id).fetch();
		  List<Pengalaman> pengalamanList = Pengalaman.find("rkn_id=? AND pgl_persenprogress = '100'", peserta.rkn_id).fetch();
		  out.print("<table class=\"table table-condensed\">");
		  out.print("<tr><th><input class=\"checkboxAll\" type=\"checkbox\" /></th><th>"+Messages.get("ct.pekerjaan")+"</th> <th>"+Messages.get("ct.loc")+"</th><th>"+Messages.get("ct.ipt")+" </th><th>"+Messages.get("ct.alamat")+"</th> <th>"+Messages.get("ct.tgl_kontrak")+" </th><th>"+Messages.get("ct.nk")+"</th></tr>");
		  for(Pengalaman pengalaman:pengalamanList) {
				  out.print("<tr>");
				  out.print("<td><input type=\"checkbox\" name=\"pengalaman\" class=\"checkbox\" value=\""+pengalaman.pgn_id+"\" "+(pengalamanId!= null && pengalamanId.contains(pengalaman.pgn_id)? "checked":"")+"/></td>");
				  out.print("<td>"+pengalaman.pgl_kegiatan+"</td>");
				  out.print("<td>"+pengalaman.pgl_lokasi+"</td>");
				  out.print("<td>"+pengalaman.pgl_pembtgs+"</td>");
				  out.print("<td>"+pengalaman.pgl_almtpembtgs+"</td>");
				  out.print("<td>"+FormatUtils.formatDateInd(pengalaman.pgl_tglkontrak)+"</td>");
				  out.print("<td>"+pengalaman.pgl_nokontrak+"</td>");
				  out.print("</tr>");
		  }
		  out.print("</table>");
		  out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
		  out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		  out.print("<div class=\"clearfix\"></div>");
		  out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.data_pengalamman")+"</div>");
	  }
	  
	  public static void _kirimKualifikasiPekerjaan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		  Peserta peserta = (Peserta) args.get("arg");
		  List<Long> pengalamanId = Query.find("select pen_id from Pengalaman_peserta where psr_id=?",Long.class, peserta.psr_id).fetch();
		  List<Pengalaman> pengalamanList = Pengalaman.find("rkn_id=? AND pgl_persenprogress < '100'", peserta.rkn_id).fetch();
		  out.print("<table class=\"table table-condensed\">");
		  out.print("<tr><th width=\"5\"><input class=\"checkboxAll\" type=\"checkbox\" /> </th><th>"+Messages.get("ct.pekerjaan")+"</th> <th>"+Messages.get("ct.loc")+"</th><th>"+Messages.get("ct.ipt")+" </th><th>"+Messages.get("ct.alamat")+"</th> <th>"+Messages.get("ct.tgl_kontrak")+" </th><th>"+Messages.get("ct.nk")+"</th></tr>");
		  for(Pengalaman pengalaman:pengalamanList) {			  
				  out.print("<tr>");
				  out.print("<td><input type=\"checkbox\" name=\"pengalaman\" class=\"checkbox\" value=\""+pengalaman.pgn_id+"\" "+(pengalamanId!= null && pengalamanId.contains(pengalaman.pgn_id)? "checked":"")+"/></td>");
				  out.print("<td>"+pengalaman.pgl_kegiatan+"</td>");
				  out.print("<td>"+pengalaman.pgl_lokasi+"</td>");
				  out.print("<td>"+pengalaman.pgl_pembtgs+"</td>");
				  out.print("<td>"+pengalaman.pgl_almtpembtgs+"</td>");
				  out.print("<td>"+FormatUtils.formatDateInd(pengalaman.pgl_tglkontrak)+"</td>");
				  out.print("<td>"+pengalaman.pgl_nokontrak+"</td>");
				  out.print("</tr>");
		  }		  
		  out.print("</table>");
		  out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
		  out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
		  out.print("<div class=\"clearfix\"></div>");
		  out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.data_pekerjaan")+"</div>");
	  }
}
	  
