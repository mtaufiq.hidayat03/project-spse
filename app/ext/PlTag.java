package ext;

import groovy.lang.Closure;
import models.agency.KontrakPl;
import models.agency.SppbjPl;
import models.common.*;
import models.handler.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.kontrak.BaPembayaranPl;
import models.kontrak.PesananPl;
import models.lelang.Checklist_master;
import models.lelang.HasilEvaluasi;
import models.nonlelang.*;
import models.nonlelang.Dok_pl.JenisDokPl;
import models.nonlelang.common.TahapNowPl;
import models.nonlelang.common.TahapStartedPl;
import models.rekanan.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.db.jdbc.Query;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rayhanfrds on 2/21/17.
 */
public class PlTag extends FastTags {
    /**
     * untuk penanganan flow berita acara
     */
    public static void _beritaAcaraPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_seleksi lelang = (Pl_seleksi) args.get("arg");
        TahapStartedPl tahapStarted = (TahapStartedPl) args.get("tahapStarted");
        Active_user active_user = Active_user.current();
        boolean allow_upload_ba = lelang.lls_status.isAktif() && (active_user.isPP() || active_user.isPanitia());
        Map<String, Object> params = new HashMap<String, Object>(1);
        params.put("id", lelang.lls_id);
        boolean allow_download_ba = lelang.isAllowDownloadBA();
        List<Tahap> balist = Berita_acara_pl.TAHAP_BERITA_ACARA;

        Berita_acara_pl berita_acara = null;
        StringBuilder content = new StringBuilder();

        if (allow_download_ba) {

            if (balist.contains(Tahap.EVALUASI_PENAWARAN) && !active_user.isRekanan()) {

                berita_acara = Berita_acara_pl.findBy(lelang.lls_id, Tahap.UPLOAD_BA_EVALUASI_PENAWARAN);
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("ct.ba_ep"));
                if (allow_upload_ba && !lelang.isLelangV3()) {
	                	content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
	                			.append(" title=\""+Messages.get("ct.ctk_ba_evaluasi")+"\"")
	                			.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_PENAWARAN).append("\">"+Messages.get("ct.cetak")+"</a>");
	                	content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
							.append(" title=\""+Messages.get("ct.ubaep")+"\"")
							.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_PENAWARAN).append("\">"+Messages.get("ct.upload")+"</a>");
                }
                content.append("</div>");
                if (berita_acara != null && !active_user.isRekanan()) {
                    content.append("<div class=\"list-group\">");
                    for (BlobTable blob : berita_acara.getDokumen()) {
                        content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraPlDownloadHandler.class))
                                .append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
                                .append(blob.getFileName()).append("<abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\"> (")
                                .append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
                    }
                    content.append("</div>");
                }

                content.append("</div>");
            }

            if (balist.contains(Tahap.EVALUASI_PENAWARAN)){
                berita_acara = Berita_acara_pl.findBy(lelang.lls_id, Tahap.UPLOAD_BA_HASIL_LELANG);
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("ct.ba_hnt"));
                if (allow_upload_ba && !lelang.isLelangV3()) {
	                	content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
	        					.append(" title=\""+Messages.get("ct.cbahnt")+"\"")
	        					.append(" value=\"").append(Tahap.UPLOAD_BA_HASIL_LELANG).append("\">"+Messages.get("ct.ctak")+"</a>");
	                	content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
							.append(" title=\""+Messages.get("ct.ba_upld_ntdr")+"\"")
							.append(" value=\"").append(Tahap.UPLOAD_BA_HASIL_LELANG).append("\">"+Messages.get("ct.upload")+"</a>");
	        		}
                content.append("</div>");
                if (berita_acara != null && (!active_user.isRekanan() || active_user.isRekanan())) {
                    content.append("<div class=\"list-group\">");
                    for (BlobTable blob : berita_acara.getDokumen()) {
                        content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraPlDownloadHandler.class))
                                .append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
                                .append(blob.getFileName()).append("<abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\"> (")
                                .append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
                    }
                    content.append("</div>");
                }

                content.append("</div>");
            }

            EvaluasiPl akhir = EvaluasiPl.findPenetapanPemenang(lelang.lls_id);

        }

        if (active_user.isRekanan())
            content.append("<div class=\"bs-callout bs-callout-info\">")
                    .append(Berita_acara_pl.getDownloadInfo(lelang.getKategori())).append("</div>");
        if (active_user.isPP() || active_user.isPanitia())
            content.append("<div class=\"bs-callout bs-callout-info\">"+Messages.get("ct.pngrm_ba")+"</div>");

        out.print(content.toString());
    }

    public static void _tabmenuPenawaranPL(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Http.Request request = Http.Request.current();
        Long plId = (Long)args.get("arg");
        final Boolean hasSubmit = args.containsKey("hasSubmittedLetter")? (Boolean) args.get("hasSubmittedLetter"):true;
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("id", plId);
        String content = "<ul class=\"nav nav-tabs\" role=\"tablist\">"
                    + "<li " + (request.action.equals("nonlelang.DokumenPlCtr.viewKirimSuratPenawaran")
                                ? "class=\"active\"" : "") + ">" +
                        "<a href=\"" + Router.reverse("nonlelang.DokumenPlCtr.viewKirimSuratPenawaran", param).url + "\" >" +
                            ""+Messages.get("ct.sp")+"</a>" +
                    "</li>" +
                    "<li class=\"" + (request.action.equals("nonlelang.DokumenPlCtr.viewKirimPenawaranTeknis")
                                ? "active": "") + getDisabledClass(hasSubmit) + "\"" +
                    "><a href=\"" + (!hasSubmit
                                        ? "#"
                                        : Router.reverse("nonlelang.DokumenPlCtr.viewKirimPenawaranTeknis", param).url) +
                    "\" >"+Messages.get("ct.pt")+"</a>" +
                    "</li>" +
                    "<li class=\"" + (request.action.equals("nonlelang.DokumenPlCtr.viewKirimPenawaranPeserta")
                                ? "active" : "") + getDisabledClass(hasSubmit) + "\"" +
                    "><a href=\"" + (!hasSubmit
                                        ? "#"
                                        : Router.reverse("nonlelang.DokumenPlCtr.viewKirimPenawaranPeserta", param).url) +
                    "\" >"+Messages.get("ct.ph")+"</a>" +
                    "</li>" +
                "</ul>";
        out.print(content);
    }

    private static String getDisabledClass(boolean status) {
        return status ? "" : " disabled";
    }

    public static void _kirimKualifikasiAktaPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Landasan_hukum> aktaList = Landasan_hukum.findBy(peserta.rkn_id);
        if(CollectionUtils.isEmpty(aktaList)) {
            out.print("<div class=\"alert alert-danger\">"+Messages.get("ct.akta_pendiri_perubahan")+"</div>");
        } else {
            out.print("<table class=\"table table-sm\">");
            Landasan_hukum akta = null;
            if(aktaList.get(0) != null) {
                akta = aktaList.get(0);
                out.print("<tr><th colspan=\"2\">"+Messages.get("ct.ap")+"</th></tr>");
                out.print("<tr><th width=\"20%\" class=\"bg-warning\">"+Messages.get("ct.no")+"</th><td>"+akta.lhk_no+"</td></tr>");
                out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.ts")+"</th><td>"+FormatUtils.formatDateInd(akta.lhk_tanggal)+"</td></tr>");
                out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.notaris")+"</th><td>"+akta.lhk_notaris+"</td></tr>");
            }
            if(aktaList.size() > 1 && aktaList.get(1) != null) {
                akta = aktaList.get(1);
                out.print("<tr><th colspan=\"2\">"+Messages.get("ct.apt")+"</th></tr>");
                out.print("<tr><th width=\"20%\" class=\"bg-warning\">"+Messages.get("ct.no")+"</th><td>"+akta.lhk_no+"</td></tr>");
                out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.ts")+"</th><td>"+FormatUtils.formatDateInd(akta.lhk_tanggal)+"</td></tr>");
                out.print("<tr><th class=\"bg-warning\">"+Messages.get("ct.notaris")+"</th><td>"+akta.lhk_notaris+"</td></tr>");
            }
            out.print("</table>");
        }
        out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
        out.print("<div class=\" clearfix\"></div>");
        out.print("");
    }

    public static void _kirimKualifikasiIjinPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Long> ijinId = Query.find("select ius_id from ekontrak.Ijin_usaha_peserta where psr_id=?", Long.class, peserta.psr_id).fetch();
        List<Ijin_usaha> ijinList = Ijin_usaha.find("rkn_id=?", peserta.rkn_id).fetch();
        out.print("<table class=\"table table-condensed\">");
        out.print("<tr><th width=\"10\"> </th><th>"+Messages.get("ct.izin_usaha")+" </th><th>"+Messages.get("ct.nomor_surat")+"</th><th>"+Messages.get("ct.ip")+"</th></tr>");
        for(Ijin_usaha ijin : ijinList) {
            out.print("<tr>");
            out.print("<td><input name=\"ijin\" class=\"checkbox\" type=\"checkbox\" value=\""+ijin.ius_id+"\" "+(ijinId != null && ijinId.contains(ijin.ius_id)? "checked":"")+"/></td>");
            out.print("<td>"+ijin.jni_nama+"</td>");
            out.print("<td>"+ijin.ius_no+"</td>");
            out.print("<td>"+ijin.ius_instansi+"</td>");
            out.print("</tr>");
        }
        out.print("</table>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
        out.print("<div class=\"clearfix\"></div>");
        out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.pdiu")+"</div>");
    }

    public static void _kirimKualifikasiPajakPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Long> pajakId = Query.find("select pjk_id from ekontrak.bukti_pajak where psr_id=?", Long.class, peserta.psr_id).fetch();
        List<Pajak> pajakList = Pajak.find("rkn_id=?", peserta.rkn_id).fetch();
        out.print("<table class=\"table table-condensed\">");
        out.print("<tr><th width=\"10\"> </th><th>"+Messages.get("ct.pjk")+"</th><th>"+Messages.get("ct.tgl")+"</th><th>"+Messages.get("ct.nbkt")+" </th></tr>");
        for (Pajak pajak : pajakList) {
            out.print("<tr>");
            out.print("<td><input class=\"checkbox\" type=\"checkbox\" name=\"pajak\" value=\""+pajak.pjk_id+"\" "+(pajakId != null && pajakId.contains(pajak.pjk_id)? "checked":"")+" /></td>");
            out.print("<td>"+pajak.pjk_jenis+" - "+pajak.pjk_bulan+" Tahun "+pajak.pjk_tahun+"</td>");
            out.print("<td>"+FormatUtils.formatDateInd(pajak.pjk_tanggal)+"</td>");
            out.print("<td>"+pajak.pjk_no+"</td>");
            out.print("</tr>");
        }
        out.print("</table>");
        out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
        out.print("<div class=\"clearfix\"></div>");
        out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.pbbpydprlkn")+"</div>");
    }

    public static void _kirimKualifikasiStafAhliPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Long> stafId = Query.find("select stp_id from ekontrak.Staf_ahli_psr where psr_id=?",Long.class, peserta.psr_id).fetch();
        List<Staf_ahli> stafAhliList = Staf_ahli.find("rkn_id=?", peserta.rkn_id).fetch();
        out.print("<table class=\"table table-condensed\">");
        out.print("<tr><th> </th><th>"+Messages.get("ct.nama")+"</th> <th>"+Messages.get("ct.tgl_lahir")+"</th><th>"+Messages.get("ct.pendidikan")+"</th> <th>"+Messages.get("ct.pengalaman_kerja")+"</th><th>"+Messages.get("ct.profesi")+"</th></tr>");
        for(Staf_ahli staf : stafAhliList) {
            out.print("<tr>");
            out.print("<td><input type=\"checkbox\" name=\"staf\" class=\"checkbox\" value=\""+staf.sta_id+"\" "+(stafId != null && stafId.contains(staf.sta_id)? "checked":"")+"/></td>");
            out.print("<td>"+staf.sta_nama+"</td>");
            out.print("<td>"+FormatUtils.formatDateInd(staf.sta_tgllahir)+"</td>");
            out.print("<td>"+staf.sta_pendidikan+"</td>");
            out.print("<td>"+staf.sta_pengalaman+" Tahun</td>");
            out.print("<td>"+staf.sta_keahlian+"</td>");
            out.print("<td>"+(staf.sta_status == 0 ? "Pegawai Tetap" : "Pegawai Tidak Tetap")+"</td>");
            out.print("</tr>");
        }
        out.print("</table>");
        out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjtnya")+" &raquo;</button>");
        out.print("<div class=\"clearfix\"></div>");
        out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.ptaddsrtkn")+"</div>");
    }

    public static void _kirimKualifikasiPeralatanPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Long> alatId = Query.find("select id_alat_peserta from ekontrak.Peralatan_peserta where psr_id=?",Long.class, peserta.psr_id).fetch();
        List<Peralatan> peralatanList = Peralatan.find("rkn_id=?", peserta.rkn_id).fetch();
        out.print("<table class=\"table table-condensed\">");
        out.print("<tr><th> </th><th>"+Messages.get("ct.jenis")+"</th><th>"+Messages.get("ct.jumlah")+"</th><th>"+Messages.get("ct.kapasitas")+"</th> <th>"+Messages.get("merk")+"</th><th>"+Messages.get("ct.tp")+"</th><th>"+Messages.get("ct.kondisi")+"</th><th>"+Messages.get("ct.ls")+" </th><th>"+Messages.get("ct.bk")+" </th></tr>");
        for(Peralatan alat:peralatanList) {
            out.print("<tr>");
            out.print("<td><input type=\"checkbox\" class=\"checkbox\" name=\"alat\" value=\""+alat.alt_id+"\" "+(alatId != null && alatId.contains(alat.alt_id)? "checked":"")+"/></td>");
            out.print("<td>"+alat.alt_jenis+"</td>");
            out.print("<td>"+alat.alt_jumlah+"</td>");
            out.print("<td>"+alat.alt_kapasitas+"</td>");
            out.print("<td>"+alat.alt_merktipe+"</td>");
            out.print("<td>"+alat.alt_thpembuatan+"</td>");
            out.print("<td>"+alat.alt_kondisi+"</td>");
            out.print("<td>"+alat.alt_lokasi+"</td>");
            out.print("<td>"+alat.alt_kepemilikan+"</td>");
            out.print("</tr>");
        }
        out.print("</table>");
        out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
        out.print("<div class=\"clearfix\"></div>");
        out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.pryrdgn")+"</div>");
    }

    public static void _kirimKualifikasiPengalamanPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Long> pengalamanId = Query.find("select pen_id from ekontrak.Pengalaman_peserta where psr_id=?",Long.class, peserta.psr_id).fetch();
        List<Pengalaman> pengalamanList = Pengalaman.find("rkn_id=? AND pgl_persenprogress = '100'", peserta.rkn_id).fetch();
        out.print("<table class=\"table table-condensed\">");
        out.print("<tr><th></th><th>"+Messages.get("ct.pekerjaan")+"</th> <th>"+Messages.get("ct.loc")+"</th><th>"+Messages.get("ct.ipt")+" </th><th>"+Messages.get("ct.alamat")+"</th> <th>"+Messages.get("ct.tgl_kontrak")+" </th></tr>");
        for(Pengalaman pengalaman:pengalamanList) {
            out.print("<tr>");
            out.print("<td><input type=\"checkbox\" name=\"pengalaman\" class=\"checkbox\" value=\""+pengalaman.pgn_id+"\" "+(pengalamanId!= null && pengalamanId.contains(pengalaman.pgn_id)? "checked":"")+"/></td>");
            out.print("<td>"+pengalaman.pgl_kegiatan+"</td>");
            out.print("<td>"+pengalaman.pgl_lokasi+"</td>");
            out.print("<td>"+pengalaman.pgl_pembtgs+"</td>");
            out.print("<td>"+pengalaman.pgl_almtpembtgs+"</td>");
            out.print("<td>"+FormatUtils.formatDateInd(pengalaman.pgl_tglkontrak)+"</td>");
            out.print("</tr>");
        }
        out.print("</table>");
        out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
        out.print("<div class=\"clearfix\"></div>");
        out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.ppngmn_prshn")+"</div>");
    }

    public static void _kirimKualifikasiPekerjaanPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl peserta = (PesertaPl) args.get("arg");
        List<Long> pengalamanId = Query.find("select pen_id from ekontrak.Pengalaman_peserta where psr_id=?",Long.class, peserta.psr_id).fetch();
        List<Pengalaman> pengalamanList = Pengalaman.find("rkn_id=? AND pgl_persenprogress < '100'", peserta.rkn_id).fetch();
        out.print("<table class=\"table table-condensed\">");
        out.print("<tr><th width=\"5\"> </th><th >"+Messages.get("ct.pekerjaan")+"</th><th >"+Messages.get("ct.lokasi")+"</th><th >"+Messages.get("ct.ipt")+" </th><th >"+Messages.get("ct.alamat")+"</th><th >"+Messages.get("ct.tgl_kontrak")+" </th></tr>");
        for(Pengalaman pengalaman:pengalamanList) {
            out.print("<tr>");
            out.print("<td><input type=\"checkbox\" name=\"pengalaman\" class=\"checkbox\" value=\""+pengalaman.pgn_id+"\" "+(pengalamanId!= null && pengalamanId.contains(pengalaman.pgn_id)? "checked":"")+"/></td>");
            out.print("<td>"+pengalaman.pgl_kegiatan+"</td>");
            out.print("<td>"+pengalaman.pgl_lokasi+"</td>");
            out.print("<td>"+pengalaman.pgl_pembtgs+"</td>");
            out.print("<td>"+pengalaman.pgl_almtpembtgs+"</td>");
            out.print("<td>"+FormatUtils.formatDateInd(pengalaman.pgl_tglkontrak)+"</td>");
            out.print("</tr>");
        }
        out.print("</table>");
        out.print("<button type=\"button\" class=\"btn btn-default prev\"> &laquo; "+Messages.get("ct.sebelumnya")+"</button>");
        out.print("<button type=\"button\" class=\"btn btn-default pull-right next\">"+Messages.get("ct.selanjutnya")+" &raquo;</button>");
        out.print("<div class=\"clearfix\"></div>");
        out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.data_pekerjaan")+"</div>");
    }

    public static void _surveyHarga(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Long lls_id = (Long)args.get("arg");
        List<SurveyHargaPl> surveyHargaPlList = SurveyHargaPl.findAllBy(lls_id);
        out.print("<ul class=\"list-group files\" style=\"width: 500px;\">");
        for(SurveyHargaPl surveyHargaPl : surveyHargaPlList) {
            out.print("<li class=\"list-group-item\">");
            out.print("<a href=\""+surveyHargaPl.getDownloadUrl(surveyHargaPl.getBlob())+"\">");
            out.print("<i class=\"fa fa-download\">&nbsp;</i>");
            out.print(surveyHargaPl.getNameFromBlobTable(surveyHargaPl.getBlob()));
            out.print("</li>");
        }
        out.print("</ul>");
    }

    // informasi Evaluasi disisi PP
    public static void _informasiEvaluasiPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_seleksi pl = (Pl_seleksi)args.get("arg");
        TahapStartedPl tahapStarted = (TahapStartedPl) args.get("tahapStarted");
//        PesertaPl peserta = (PesertaPl) args.get("peserta");
        boolean hasilEvaKualifikasi = EvaluasiPl.isShowHasilEvaluasiKualifikasi(pl.lls_id);
        Map<String, Object> param = new HashMap<String, Object>(1);
        param.put("id", pl.lls_id);
        StringBuilder content = new StringBuilder();
        if(tahapStarted.isPengumuman()) {
            content.append("<a href=\"").append(Router.reverse("nonlelang.EvaluasiPlCtr.hasil", param).url).append("\" class=\"badge jpopup pull-right\" width=\"700\" height=\"600\">"+Messages.get("ct.he")+"</a>");
        }
        if(hasilEvaKualifikasi) {
            int jmlLulusKualifikasi = NilaiEvaluasiPl.countLulus(pl.lls_id, EvaluasiPl.JenisEvaluasi.JENIS_EVALUASI_KUALIFIKASI);
            content.append("<div><b>"+Messages.get("ct.ek")+"</b><br />"+Messages.get("ct.lls")+" <span class=\"badge\">").append(jmlLulusKualifikasi).append("</span> "+Messages.get("ct.pbrg")+"</div>");
        }
        if(tahapStarted.isEvaluasiTeknis()) {
            int jmlLulusAdmTeknis = NilaiEvaluasiPl.countLulus(pl.lls_id, EvaluasiPl.JenisEvaluasi.EVALUASI_TEKNIS);
            content.append("<div><b>"+Messages.get("ct.epadt")+"</b><br />"+Messages.get("ct.lls")+": <span class=\"badge\">").append(jmlLulusAdmTeknis).append(" "+Messages.get("ct.pbrg")+"</span></div>");
        }
        if(tahapStarted.isEvaluasiHarga()) {
            int jmlLulusBiaya = NilaiEvaluasiPl.countLulus(pl.lls_id, EvaluasiPl.JenisEvaluasi.EVALUASI_HARGA);
            content.append("<div><b>"+Messages.get("ct.evls_pnwrn")+"</b>"+Messages.get("ct.lls")+": <span class=\"badge\">").append(jmlLulusBiaya).append("  "+Messages.get("ct.pbrg")+"</span></div>");
        }
        if (tahapStarted.isPengumuman()) {
            content.append("<div class=\"highlight\"><b>"+Messages.get("tag.pengumuman_pemenang")+"</b></div>");
            EvaluasiPl evaluasi = EvaluasiPl.findPenetapanPemenang(pl.lls_id);
            if(evaluasi != null) {
                List<NilaiEvaluasiPl> pemenangList = evaluasi.findPemenangList();
                if (!CommonUtil.isEmpty(pemenangList)) {
                    content.append("<table class=\"table table-sm\">");
                    for (NilaiEvaluasiPl pemenang : pemenangList) {
                        content.append("<tr><td>"+Messages.get("tag.urutan")+" ").append(pemenang.nev_urutan).append(' ');
                        if (pemenang.isLulus()){
                            if (!pl.getPemilihan().isLelangExpress() && pemenang.nev_urutan == 1 && pemenang.getPeserta().isPemenangVerif());
                            content.append("<i class=\"fa fa-star\" style=\"color:#f0ad4e\"></i>");
                                content.append("<br>* "+Messages.get("ct.phe"));
                        }

                        SppbjPl sppbj = SppbjPl.find("lls_id=?", pl.lls_id).first();

                        if (sppbj != null && pemenang.getPeserta().getRekanan().rkn_id.equals(sppbj.getRekanan().rkn_id)) {
                            content.append("<i class=\"fa fa-star\" style=\"color:#f0ad4e\"></i>");
                        }

//                        if (pemenang.isLulus()){
//                            if (!pl.getPemilihan().isLelangExpress() && pemenang.nev_urutan == 1)
//                                content.append("<br>* Pemenang Hasil Evaluasi");
//                        }

                        if (pl.getPemilihan().isLelangExpress() && pemenang.getPeserta().isPemenangVerif())
                            content.append("<br>* "+Messages.get("ct.pbvp"));

                        if (sppbj != null && pemenang.getPeserta().getRekanan().rkn_id.equals(sppbj.getRekanan().rkn_id))
                            content.append("<br>* "+Messages.get("ct.pbkntrk"));
                        content.append("</td><td>").append(pemenang.getPeserta().getNamaPeserta()).append("</td></tr>");
                    }
                    content.append("</table>");
                }
            }
            else {
                content.append(Messages.get("ct.bap"));
            }
        }
        out.print(content.toString());
    }

    public static void _dokPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Dok_pl dok_lelang =  (Dok_pl) args.get("arg");
        boolean lelangV3 = (Boolean) args.get("lelangV3");
        if (dok_lelang != null) {
            StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
            content.append("<div class=\"panel-heading\">").append(dok_lelang.dll_jenis.getLabel()).append("</div>");
            if(lelangV3 && dok_lelang != null) { // untuk handle dok_lelang dan dok_kualifikasi lelang versi 3.x
                content.append("<div class=\"list-group\">");
                List<BlobTable> list = dok_lelang.getDokumenList();
                if (!CommonUtil.isEmpty(list)) {
                    for (BlobTable dok : list) {
                        if (dok != null) {
                            content.append("<a href=\"").append(dok.getDownloadUrl(DokLelangDownloadHandler.class))
                                    .append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
                                    .append(dok.getFileName())
                                    .append(" <abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\">(")
                                    .append(FormatUtils.formatDateTimeInd(dok.blb_date_time)).append(")</abbr></a>");
                        }
                    }
                }
                content.append("</div>");
                // dokumen adendum v3
                Dok_pl.JenisDokPl jenis_adendum = Dok_pl.JenisDokPl.DOKUMEN_ADENDUM;
                Dok_pl dok_adendum = Dok_pl.findBy(dok_lelang.lls_id, jenis_adendum);
                if(dok_adendum != null) {
                    content.append("<div class=\"panel-heading\">").append(dok_adendum.dll_jenis.getLabel()).append("</div>");
                    content.append("<div class=\"list-group\">");
                    list = dok_lelang.getDokumenList();
                    if (!CommonUtil.isEmpty(list)) {
                        for (BlobTable dok : list) {
                            if (dok != null) {
                                content.append("<a href=\"").append(dok.getDownloadUrl(DokLelangDownloadHandler.class))
                                        .append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
                                        .append(dok.getFileName())
                                        .append(" <abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\">(")
                                        .append(FormatUtils.formatDateTimeInd(dok.blb_date_time))
                                        .append(")</abbr></a>");
                            }
                        }
                    }
                    content.append("</div>");
                }
            } else {
                String preview = "nonlelang.DokumenPlCtr.preview";
                Map<String, Object> params = new HashMap<String, Object>(1);
                content.append("<div class=\"list-group\">");
                List<Dok_pl_content> contentList = Dok_pl_content.find("dll_id=? order by dll_versi", dok_lelang.dll_id).fetch();
                if (dok_lelang != null && dok_lelang.dll_id_attachment != null) {
                    params.put("id", dok_lelang.dll_id);
                    content.append("<a href=\"").append(Router.reverse(preview, params).url).append("\" class=\"list-group-item jpopup\" title=\""+Messages.get("ct.dt")+"\">").append(dok_lelang.dll_nama_dokumen).append("</a>");
                }
                if (!CommonUtil.isEmpty(contentList)) {
                    for (Dok_pl_content dok_content : contentList) {
                        if (dok_content.dll_versi == 1 || dok_content.dll_modified) // versi 1 atau dll_modified=true tidak boleh muncul sebagai adendum
                            continue;
                        params.put("id", dok_content.dll_id);
                        params.put("versi", dok_content.dll_versi);
                        content.append("<a href=\"").append(Router.reverse(preview, params).url).append("\" class=\"list-group-item jpopup\" title=\"Perubahan\">").append(""+Messages.get("ct.prbhn")+" ").append(dok_content.dll_versi - 1).append("</a>");
                    }
                }
                content.append("</div>");
            }
            content.append("</div>");
            out.print(content.toString());
        }
    }

    public static void _hasilEvaluasiPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_seleksi lelang = (Pl_seleksi) args.get("arg");
        TahapNowPl tahapNowPl = (TahapNowPl)args.get("tahapNow");
        TahapStartedPl tahapStarted = new TahapStartedPl(lelang.lls_id);
        out.print("<table class=\"table table-sm \">");
        out.print("<tr>");
        out.print("<th width=\"10\">No.</th>");
        out.print("<th>"+Messages.get("ct.np")+"</th>");
        out.print("<th>"+Messages.get("ct.hp")+"</th>");
        out.print("<th>"+Messages.get("ct.ht")+"</th>");
        out.print("<th>"+Messages.get("ct.hn")+"</th>");
        out.print("<th width=\"130\">"+Messages.get("ct.uv")+"</th>");
        out.print("<th width=\"20\"><span class=\"label label-info\">A</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-danger\">K</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-info\">T</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-success\">H</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-warning\">P</span></th>");

        if(!lelang.isLelangV3())
            out.print("<th width=\"160\"><span class=\"badge  badge-danger\">B</span></th>");

        List<HasilEvaluasi> list = HasilEvaluasi.findByPl(lelang.lls_id);
        if(!CollectionUtils.isEmpty(list)) {
            Map<String, Object> param = new HashMap<>(1);
            int indeks = 1;
            Double nilai_hps = lelang.getPaket().pkt_hps;
            for(HasilEvaluasi peserta:list) {
                if(peserta == null)
                    continue;
                boolean isOverHps = nilai_hps != null && peserta.fileHarga != null && peserta.nev_harga_terkoreksi != null ? peserta.nev_harga_terkoreksi > nilai_hps : false;
                param.put("id", peserta.psr_id);

                PesertaPl pesertaPl = PesertaPl.findById(peserta.psr_id);
                boolean sudahUndangVerifikasi = MailQueueDao.countByJenisAndRekanan(pesertaPl.lls_id, JenisEmail.UNDANGAN_VERIFIKASI.id, peserta.rkn_id) > 0;

                out.print("<tr>");
                out.print("<td>"+indeks+"</td>");

                if(!tahapStarted.isShowPenyedia())
                    out.print("<td>"+Messages.get("ct.pnyd")+" "+indeks+"</td>");
                else if(tahapNowPl.isPembukaan())
                    out.print("<td>"+peserta.rkn_nama+"</td>");
                else
                    out.print("<td><a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url+"#administrasi"+"\" value=\""+peserta.psr_id+"\">"+peserta.rkn_nama+"</a></td>");

                out.print("<td>"+(peserta.psr_harga != null ? ("<div>"+ (isOverHps ? "<span class=\"label label-danger\">"+Messages.get("tag.melebihi_hps")+"</span> " : "") + FormatUtils.formatCurrencyRupiah(peserta.psr_harga) +"</div>") : ""+Messages.get("ct.tap")+"")+"</td>");
//                out.print("<td>" + (peserta.fileHarga != null ? "<div>" + peserta.getPenawaranTerkoreksi() + "</div>" : "Tidak Ada Penawaran") + "</td>");
                out.print("<td>"+ (peserta.psr_harga != null ? ("<div>" + peserta.getPenawaranTerkoreksi() +"</div>") : ""+Messages.get("ct.tap")+"")+"</td>");
                out.print("<td>"+ (peserta.nev_harga_negosiasi != null ? ("<div>" + peserta.getHargaNegosiasi() +"</div>") : "")+"</td>");
                out.print("<td>" );
                if(peserta.isUploadPenawaranHargaPl()) {
                    if(sudahUndangVerifikasi) {
                        PesertaPl pesertaObj = PesertaPl.findById(peserta.psr_id);
                        Map<String, Object> paramUndangan = new HashMap<String, Object>(1);
                        paramUndangan.put("id", pesertaObj.lls_id);
                        paramUndangan.put("rkn_id", pesertaObj.rkn_id);
                        paramUndangan.put("jenis", JenisEmail.UNDANGAN_VERIFIKASI);

                        out.print("<div class=\"btn-group\">");
                        out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.kirimUndanganVerifikasi", param).url+"\" type=\"button\" class=\"btn btn-success btn-xs\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i>&nbsp;&nbsp;Kirim</a>");
                        out.print("<a href=\""+Router.reverse("admin.UtilityCtr.viewUdanganPdfPl", paramUndangan).url+"\" type=\"button\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-download\" aria-hidden=\"true\"></i>&nbsp;&nbsp;"+Messages.get("ct.cetak")+"</a>");
                        out.print("</div>");
                    } else {
                        out.print("<div class=\"btn-group\">");
                        out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.kirimUndanganVerifikasi", param).url+"\" type=\"button\" class=\"btn btn-default btn-xs\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i>&nbsp;&nbsp;"+Messages.get("ct.krm")+"</a>");
                        out.print("</div>");
                    }

                }
                out.print("</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusAdmin()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusKualifikasi()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusTeknis()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\"/>")+"</td>");
//                if(lelang.getEvaluasi().isNilai())
//                    out.print("<td>"+(peserta.skor_teknis != null? peserta.skor_teknis:0)+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusHarga()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
//                if(lelang.getEvaluasi().isNilai())
//                    out.print("<td>"+(peserta.skor_harga != null? peserta.skor_harga:0)+"</td>");
//                if(lelang.getEvaluasi().isNilai())
//                    out.print("<td>"+(peserta.skor != null? peserta.skor:0)+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isPemenang()?"<i class=\"fa fa-star\" style=\"color:#f0ad4e\"/>":"")+"</td>");

                out.print("<td class=\"text-left\">");
                if(tahapNowPl.isEvaluasiHarga() || tahapNowPl.isPenetapaPemenang()){
                    out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.verifikasi_sikap", param).url+"\" class=\"label label-warning\" target=\"_blank\">"+Messages.get("ct.vrfks")+"</a>");
                }
                if(pesertaPl.sudah_verifikasi_sikap != null){
                    out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.konfirmasi_verifikasi_sikap", param).url+"\" class=\"label label-success\" target=\"_blank\"><i class=\"fa fa-check\"></i> "+Messages.get("ct.trvrfks")+"</a>");
                }else{
                    out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.konfirmasi_verifikasi_sikap", param).url+"\" class=\"label label-primary\" target=\"_blank\"><i class=\"fa fa-close\"></i> "+Messages.get("ct.knfrm")+"</a>");
                }
                out.print("</td>");
                out.print("</tr>");
                indeks++;
            }
        }
        out.print("</table>");
    }

    public static void _hasilEvaluasiPlNew(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Pl_seleksi lelang = (Pl_seleksi) args.get("arg");
        TahapNowPl tahapNowPl = (TahapNowPl)args.get("tahapNow");
        TahapStartedPl tahapStarted = new TahapStartedPl(lelang.lls_id);
        out.print("<table class=\"table table-sm \">");
        out.print("<tr>");
        out.print("<th width=\"10\">"+Messages.get("tag.no")+"</th>");
        out.print("<th>"+Messages.get("tag.nama_peserta")+"</th>");
        out.print("<th>"+Messages.get("tag.harga_penawaran")+"</th>");
        out.print("<th>"+Messages.get("tag.harga_terkoreksi")+"</th>");
        out.print("<th>"+Messages.get("tag.harga_negosiasi")+"</th>");
        out.print("<th width=\"20\"><span class=\"label label-danger\">K</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-danger\">"+Messages.get("tag.skor")+"</span></th>");
        if(!lelang.isLelangV3())
            out.print("<th width=\"20\"><span class=\"label label-info\">B</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-success\">A</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-info\">T</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-success\">H</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-success\">"+Messages.get("tag.skor")+"</span></th>");
        out.print("<th width=\"20\"><span class=\"label label-warning\">P</span></th>");
        
        out.print("<th class=\"text-center\">Verifikasi</span></th>");


        List<HasilEvaluasi> list = HasilEvaluasi.findByPl(lelang.lls_id);
        if(!CollectionUtils.isEmpty(list)) {
            Map<String, Object> param = new HashMap<>(1);
            int indeks = 1;
            Double nilai_hps = lelang.getPaket().pkt_hps;
            for(HasilEvaluasi peserta:list) {
                if(peserta == null)
                    continue;
                boolean isOverHps = nilai_hps != null && peserta.fileHarga != null && peserta.nev_harga_terkoreksi != null ? peserta.nev_harga_terkoreksi > nilai_hps : false;
                param.put("id", peserta.psr_id);

                PesertaPl pesertaPl = PesertaPl.findById(peserta.psr_id);
                boolean sudahUndangVerifikasi = MailQueueDao.countByJenisAndRekanan(pesertaPl.lls_id, JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI.id, peserta.rkn_id) > 0;

                out.print("<tr>");
                out.print("<td>"+indeks+"</td>");

                if(!tahapStarted.isShowPenyedia())
                    out.print("<td>"+Messages.get("tag.penyedia")+" "+indeks+"</td>");
                else if(tahapNowPl.isPembukaan())
                    out.print("<td>"+peserta.rkn_nama+"</td>");
                else
                    out.print("<td><a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.detail", param).url+"#kualifikasi"+"\" value=\""+peserta.psr_id+"\">"+peserta.rkn_nama+"</a></td>");

                out.print("<td>"+(peserta.psr_harga != null ? ("<div>"+ (isOverHps ? "<span class=\"label label-danger\">"+Messages.get("tag.melebihi_hps")+"</span> " : "") + FormatUtils.formatCurrencyRupiah(peserta.psr_harga) +"</div>") : Messages.get("tag.tidak_ada_penawaran"))+"</td>");
                out.print("<td>"+ (peserta.psr_harga != null ? ("<div>" + peserta.getPenawaranTerkoreksi() +"</div>") : Messages.get("tag.tidak_ada_penawaran"))+"</td>");
                out.print("<td>"+ (peserta.nev_harga_negosiasi != null ? ("<div>" + peserta.getHargaNegosiasi() +"</div>") : "")+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusKualifikasi()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
                out.print("<td>" +peserta.getSkorKualifikasi() +"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusPembuktian()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusAdmin()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusTeknis()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\"/>")+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isLulusHarga()? "<i class=\"fa fa-check\"/>":"<i class=\"fa fa-close\" />")+"</td>");
                out.print("<td/>"+(peserta.skor_harga != null? peserta.skor_harga:0)+"</td>");
                out.print("<td class=\"text-center\">"+(peserta.isPemenang()?"<i class=\"fa fa-star\" style=\"color:#f0ad4e\"/>":"")+"</td>");

                out.print("<td class=\"text-center\">");
                if(tahapNowPl.isPembuktianKualifikasi() || tahapNowPl.isPenetapaPemenang()){
                    out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.verifikasi_sikap", param).url+"\" class=\"badge  badge-warning\" target=\"_blank\">Verifikasi</a>");
                }
                if(pesertaPl.sudah_verifikasi_sikap != null){
                    out.print("<a href=\""+Router.reverse("nonlelang.EvaluasiPlCtr.konfirmasi_verifikasi_sikap", param).url+"\" class=\"badge  badge-success\" target=\"_blank\"><i class=\"fa fa-check\"></i> Terverifikasi</a>");
                }
                out.print("</td>");
                out.print("</tr>");
                indeks++;
            }
        }
        out.print("</table>");
    }

    public static void _kirimKualifikasiPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_seleksi pl = (Pl_seleksi)args.get("arg");
        DokPenawaranPl dok_kualifikasi = (DokPenawaranPl) args.get("dok_kualifikasi");
        PesertaPl peserta = (PesertaPl) args.get("peserta");
        TahapNowPl tahapAktif = (TahapNowPl) args.get("tahapAktif");
        boolean allow_kualifikasi = tahapAktif.isPemasukanPenawaran();
        Map<String, Object> param = new HashMap<String, Object>(1);
        param.put("id", peserta.psr_id);

        String ket_kualifikasi = "";
        if (dok_kualifikasi != null){
            ket_kualifikasi = ""+Messages.get("ct.sdp")+" : "+ FormatUtils.formatDateTimeInd(dok_kualifikasi.dok_tgljam);

        } else {
            Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(pl.lls_id, Tahap.PEMASUKAN_PENAWARAN);
            if (jadwal != null) {
                String tglAwal = "";
                String tglAkhir = "";
                if (jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
                    tglAwal = FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal);
                    tglAkhir = FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir);
                }
                ket_kualifikasi = ""+Messages.get("ct.bdk_jp")+" : " + tglAwal + " s.d. "+ tglAkhir;
            }
        }
        DokPenawaranPl dok_susulan = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_SUSULAN);
        String ket_susulan = Messages.get("ct.bdk_smk");
        if (dok_susulan != null && dok_susulan.dok_disclaim == 1) {
            ket_susulan = ""+Messages.get("ct.sdp")+" : " + FormatUtils.formatDateTimeInd(dok_susulan.auditupdate);
        }
        int jumlahEmail = MailQueueDao.countPesanKualifikasi(pl.lls_id, Active_user.current().rekananId);
        boolean allow_susulan = jumlahEmail != 0 && peserta.is_dikirim_pesan;
        StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
        content.append("<div class=\"panel-heading\">"+Messages.get("tag.data_kualifikasi")+"</div>");
        content.append("<ul class=\"list-group\">");
        content.append("<li class=\"list-group-item\">"+Messages.get("tag.dokumen_kualifikasi")+" <span class=\"badge\">Status : ").append(ket_kualifikasi).append("</span>");
        if(dok_kualifikasi != null)
            content.append("<a class=\"badge jpopup\" href=\"").append(Router.reverse("nonlelang.KualifikasiPlCtr.preview", param).url).append("\" width=\"900\">"+Messages.get("ct.ld")+"</a>");
        if(allow_kualifikasi || allow_susulan)
            content.append("<a class=\"badge\" href=\"").append(Router.reverse("nonlelang.KualifikasiPlCtr.kirim", param).url).append("\">"+Messages.get("ct.kd")+"</a>");
        content.append("</li>");

        if (allow_susulan) {
            content.append("<li class=\"list-group-item\">"+Messages.get("tag.persyaratan_kualifikasi_tambahan")+" <span class=\"badge\">Status : ").append(ket_susulan).append("</span>");
            if(dok_susulan != null && dok_susulan.dok_disclaim == 1)
                content.append("<a class=\"badge \" target=\"_blank\" href=\"").append(Router.reverse("nonlelang.KualifikasiPlCtr.susulan", param).url).append("\">"+Messages.get("ct.ld")+"</a>");

            content.append("<a class=\"badge dialog\" title=\""+Messages.get("tag.kirim_persyaratan_kualifikasi_tambahan")+"\" href=\"")
                    .append(Router.reverse("nonlelang.KualifikasiPlCtr.kirimSusulan", param).url);

            content.append(dok_susulan == null || dok_susulan.dok_disclaim == 0 ? "\">"+Messages.get("ct.kd")+"</a>" : "\">Update Data</a>") ;
            content.append("</li>");
        }

        content.append("</ul>");
        content.append("</div>");
        out.print(content.toString());
    }

    public static void _kirimKualifikasiPlNew(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Pl_seleksi pl = (Pl_seleksi)args.get("arg");
        DokPenawaranPl dok_kualifikasi = (DokPenawaranPl) args.get("dok_kualifikasi");
        PesertaPl peserta = (PesertaPl) args.get("peserta");
        TahapNowPl tahapAktif = (TahapNowPl) args.get("tahapAktif");
        boolean allow_kualifikasi = tahapAktif.isKirimPersyaratanKualifikasi();
        Map<String, Object> param = new HashMap<String, Object>(1);
        param.put("id", peserta.psr_id);

        String ket_kualifikasi = "";
        if (dok_kualifikasi != null){
            ket_kualifikasi = Messages.get("ct.sdp")+" : "+ FormatUtils.formatDateTimeInd(dok_kualifikasi.dok_tgljam);

        } else {

            Jadwal_pl jadwal = Jadwal_pl.findByLelangNTahap(pl.lls_id, Tahap.PEMASUKAN_DOK_PRA);

            if (jadwal != null) {
                String tglAwal = "";
                String tglAkhir = "";
                if (jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
                    tglAwal = FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal);
                    tglAkhir = FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir);
                }
                ket_kualifikasi = Messages.get("ct.bdk_jp")+" : " + tglAwal + " s.d. "+ tglAkhir;
            }
        }
        DokPenawaranPl dok_susulan = DokPenawaranPl.findPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_SUSULAN);
        String ket_susulan = Messages.get("ct.bdk_smk");
        if (dok_susulan != null && dok_susulan.dok_disclaim == 1) {
            ket_susulan = Messages.get("ct.sdp")+" : " + FormatUtils.formatDateTimeInd(dok_susulan.auditupdate);
        }
        int jumlahEmail = MailQueueDao.countPesanKualifikasi(pl.lls_id, Active_user.current().rekananId);
        boolean allow_susulan = jumlahEmail != 0 && peserta.is_dikirim_pesan;
        StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
        content.append("<div class=\"panel-heading\">"+Messages.get("ct.dk")+"</div>");
        content.append("<ul class=\"list-group\">");
        content.append("<li class=\"list-group-item\">"+Messages.get("ct.dpklfk")+" <span class=\"badge\">Status : ").append(ket_kualifikasi).append("</span>");
        if(dok_kualifikasi != null)
            content.append("<a class=\"badge jpopup\" href=\"").append(Router.reverse("nonlelang.KualifikasiPlCtr.preview", param).url).append("\" width=\"900\">"+Messages.get("ct.ld")+"</a>");
        if(allow_kualifikasi || allow_susulan)
            content.append("<a class=\"badge\" href=\"").append(Router.reverse("nonlelang.KualifikasiPlCtr.kirim", param).url).append("\">"+Messages.get("ct.kd")+"</a>");
        content.append("</li>");

        if (allow_susulan) {
            content.append("<li class=\"list-group-item\">"+Messages.get("ct.pkt")+" <span class=\"badge\">Status : ").append(ket_susulan).append("</span>");
            if(dok_susulan != null && dok_susulan.dok_disclaim == 1)
                content.append("<a class=\"badge \" target=\"_blank\" href=\"").append(Router.reverse("nonlelang.KualifikasiPlCtr.susulan", param).url).append("\">"+Messages.get("ct.ld")+"</a>");

            content.append("<a class=\"badge dialog\" title=\""+Messages.get("ct.kpkt")+"\" href=\"")
                    .append(Router.reverse("nonlelang.KualifikasiPlCtr.kirimSusulan", param).url);

            content.append(dok_susulan == null || dok_susulan.dok_disclaim == 0 ? "\">"+Messages.get("ct.kd")+"</a>" : "\">"+Messages.get("ct.ud")+"</a>") ;
            content.append("</li>");
        }

        content.append("</ul>");
        content.append("</div>");
        out.print(content.toString());
    }

    public static void _kirimPenawaranPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_seleksi pl = (Pl_seleksi)args.get("arg");
        PesertaPl peserta = (PesertaPl) args.get("peserta");
        List<DokPenawaranPl> dokPenawaranTeknis = DokPenawaranPl.findListPenawaranPeserta(peserta.psr_id, DokPenawaranPl.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
        Map<String, Object> param = new HashMap<String, Object>(1);
        boolean allow_kirim_penawaran = (Boolean) args.get("allow_kirim_penawaran");
        boolean kualifikasiUploaded = (Boolean) args.get("kualifikasiUploaded");
        param.put("id", peserta.lls_id);

        String hargaLabel = peserta.psr_dkh != null && !peserta.psr_dkh.isEmpty() ? "<span class=\"label label-success\">" +
                ""+Messages.get("ct.sd")+" <i>("+FormatUtils.formatDateTimeInd(peserta.tgl_penawaran_harga)+")</i></span>" :
                "<span class=\"label label-danger\">"+Messages.get("ct.bk")+"</span>";

        String suratPenawaranLabel = peserta.masa_berlaku_penawaran != null ? "<span class=\"label label-success\">" +
                ""+Messages.get("ct.sd")+" <i>("+FormatUtils.formatDateTimeInd(peserta.tgl_surat_penawaran)+")</i></span>" :
                "<span class=\"label label-danger\">"+Messages.get("ct.bk")+"</span>";

        String teknisLabel = dokPenawaranTeknis.size() > 0 ? "<span class=\"label label-success\">"+Messages.get("ct.sd")+" <i>("
                +FormatUtils.formatDateTimeInd(dokPenawaranTeknis.get(0).dok_tgljam)+")</i></span>" :
                "<span class=\"label label-danger\">"+Messages.get("ct.bk")+" </span>";

        StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
        content.append("<div class=\"panel-heading\">"+Messages.get("ct.dpth")+"</div>");
        content.append("<div class=\"panel-body\">");
        content.append("<strong><i>"+Messages.get("ct.spd")+"</i></strong><br /><br />");
        content.append("<ul class=\"list-inline clearfix\">");
        content.append("<li  style=\"padding-bottom : 15px;\">"+Messages.get("ct.sp")+" : ").append(suratPenawaranLabel).append("</li>");
        content.append("<li>"+Messages.get("ct.dt")+" : ").append(teknisLabel).append("</li>");
        content.append("<li>"+Messages.get("ct.dh")+" : ").append(hargaLabel).append("</li>");
        content.append("</ul><hr />");
            content.append("<strong><i>"+Messages.get("ct.tp")+" : </i></strong><span class=\"label label-primary\">").append(FormatUtils.formatCurrencyRupiah(peserta.psr_harga)).append("</span>");
        if(allow_kirim_penawaran) {
            if (kualifikasiUploaded)
                content.append("<a class=\"badge\" style=\"float: right;\" href=\"").append(Router.reverse("nonlelang.DokumenPlCtr.viewKirimSuratPenawaran", param).url).append("\">"+Messages.get("ct.kp")+"</a>");
            else
                content.append("<i style=\"float: right;\">**"+Messages.get("ct.ahmdktd")+"</i>");
        }
        content.append("</div>");
        out.print(content.toString());
    }

    public static void _syaratKualifikasiPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_detil pl = (Pl_detil)args.get("arg");
        Dok_pl dok_pl = null;
        StringBuilder content = new StringBuilder();
        JenisDokPl jenis = JenisDokPl.DOKUMEN_LELANG;
		if(pl.isPrakualifikasi())
			jenis = JenisDokPl.DOKUMEN_LELANG_PRA;
		dok_pl = Dok_pl.findBy(pl.lls_id, jenis);
        List<ChecklistPl> ijinList =  dok_pl.getSyaratIjinUsaha();
        List<ChecklistPl> syaratList =  dok_pl.getSyaratKualifikasi();
        List<ChecklistPl> ijinBaruList = dok_pl.getSyaratIjinUsahaBaru();
		List<ChecklistPl> syaratAdminList = dok_pl.getSyaratKualifikasiAdministrasi();
		List<ChecklistPl> syaratTeknisList = dok_pl.getSyaratKualifikasiTeknis();
		List<ChecklistPl> syaratKeuanganList =  dok_pl.getSyaratKualifikasiKeuangan();
		if(!CollectionUtils.isEmpty(ijinList) || !CollectionUtils.isEmpty(syaratList)) {
			content.append("<table class=\"table table-sm\">");
			if(!CollectionUtils.isEmpty(ijinList)){
				content.append("<tr><th>Izin Usaha</th></tr>");
				content.append("<tr><td><table class=\"table table-sm\">").append("<tr><td>Jenis Izin</td><td>Klasifikasi</td></tr>");
				for(ChecklistPl ijin : ijinList) {
					content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
				}
				content.append("</table></td></tr>");
			}
			for(ChecklistPl syarat:syaratList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if(syarat.getJsonName() != null) {
					content.append("<b>").append(cm.ckm_nama).append("</b>");
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("<b>").append(syarat.chk_nama).append("</b>");					
				} else {
					content.append("<b>").append(cm.ckm_nama).append("</b>");
					if(!CommonUtil.isEmpty(syarat.chk_nama))
						content.append("<p>").append(syarat.chk_nama).append("</p>");
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		if (!CollectionUtils.isEmpty(ijinBaruList) || !CollectionUtils.isEmpty(syaratAdminList)) {
			content.append("<h5>Persyaratan Kualifikasi Administrasi/Legalitas</h5>");
			content.append("<table class=\"table table-sm\">");
			if(!CollectionUtils.isEmpty(ijinBaruList)){
				content.append("<tr><th>Izin Usaha</th></tr>");
				content.append("<tr><td><table class=\"table table-sm\">").append("<tr><td>Jenis Izin</td><td>Bidang Usaha/Sub Bidang Usaha/Klasifikasi/Sub Klasifikasi</td></tr>");
				for(ChecklistPl ijin : ijinBaruList) {
					content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
				}
				content.append("</table></td></tr>");
			}
			for(ChecklistPl syarat:syaratAdminList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if(syarat.getJsonName() != null) {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("Syarat Kualifikasi Administrasi/Legalitas Lain<br/>");
					String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(syarat.chk_nama);
				} else {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);														
					if(!CommonUtil.isEmpty(syarat.chk_nama)) {
						String[] s = syarat.chk_nama.split("[\\r\\n]+");
						content.append("<i>");
						if (s != null) {
							for (String newline: s) {
								content.append(newline);
								content.append("<br/>");
							}
						} else 
							content.append(syarat.chk_nama);
						content.append("</i>");
					}
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		if (!CollectionUtils.isEmpty(syaratTeknisList)) {
			content.append("<h5>Persyaratan Kualifikasi Teknis</h5>");
			content.append("<table class=\"table table-sm\">");
			for(ChecklistPl syarat:syaratTeknisList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if (cm.isInputNumber()) {
					int i = 0;
					for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
						if (newline.split("number") == null || newline.split("number").length < 2) {
							content.append(newline);
						} else {
							int j = 0;
							for (String number: newline.split("number")) {
								content.append(number);
								for(Map<String, String> val : syarat.getJsonName()) {
									for(String key : table.key) {
										if (key.equals("number"+i) && j == 0) 
											content.append(val.get(key));
									}
								}
								j++;
							}
						}
						content.append("<br/>");
						i++;
					}
				}  else if (cm.isInputTextMulti()) {
					for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
						int j = 1;
						for (String text: newline.split("text")) {
							content.append(text);
							for(Map<String, String> val : syarat.getJsonName()) {
								for(String key : table.key) {
									if (key.equals("text"+j)) 
										content.append(val.get(key));
								}
							}
							j++;
						}
						content.append("<br/>");
					}
				} else if (syarat.getJsonName() != null) {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("Syarat Kualifikasi Teknis Lain<br/>");
					String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(syarat.chk_nama);
				} else {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					if(!CommonUtil.isEmpty(syarat.chk_nama)) {
						String[] s = syarat.chk_nama.split("[\\r\\n]+");
						content.append("<i>");
						if (s != null) {
							for (String newline: s) {
								content.append(newline);
								content.append("<br/>");
							}
						} else 
							content.append(syarat.chk_nama);
						content.append("</i>");
					}
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}
		if (!CollectionUtils.isEmpty(syaratKeuanganList)) {
			content.append("<h5>Persyaratan Kualifikasi Kemampuan Keuangan</h5>");
			content.append("<table class=\"table table-sm\">");
			for(ChecklistPl syarat:syaratKeuanganList) {
				Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
				content.append("<tr><td>");
				KeyLabel table = cm.getTable();
				if(syarat.getJsonName() != null) {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
					content.append("<thead>");
					for (String label : table.label) {
						content.append("<th>").append(label).append("</th>");
					}
					content.append("</thead>");
					content.append("<tbody>");
					for(Map<String, String> val : syarat.getJsonName()) {
						content.append("<tr>");
						for(String key : table.key) {
							content.append("<td>").append(val.get(key)).append("</td>");
						}
						content.append("</tr>");
					}
					content.append("</tbody>");
					content.append("</table>");
				} else if(cm.isSyaratLain()) {
					content.append("Syarat Kualifikasi Kemampuan Keuangan Lain<br/>");
					String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(syarat.chk_nama);
				} else {
					String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
					if (newlines != null) {
						for (String newline: newlines) {
							content.append(newline);
							content.append("<br/>");
						}
					} else 
						content.append(cm.ckm_nama);
					if(!CommonUtil.isEmpty(syarat.chk_nama)) {
						String[] s = syarat.chk_nama.split("[\\r\\n]+");
						content.append("<i>");
						if (s != null) {
							for (String newline: s) {
								content.append(newline);
								content.append("<br/>");
							}
						} else 
							content.append(syarat.chk_nama);
						content.append("</i>");
					}
				}					
				content.append("</td></tr>");
			}
			content.append("</table>");
		}   
        out.print(content.toString());
    }


    public static void _cetakUdanganPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        PesertaPl pesertaPl = (PesertaPl) args.get("arg");
        StringBuilder content = new StringBuilder();

        MailQueue mailPembuktianKualifikasi = MailQueueDao
                .getByJenisAndLelangAndRekanan(pesertaPl.lls_id, pesertaPl.rkn_id,JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);

        MailQueue mailKontrak =  MailQueueDao
                .getByJenisAndLelangAndRekanan(pesertaPl.lls_id, pesertaPl.rkn_id,JenisEmail.UNDANGAN_KONTRAK);

        SppbjPl sppbj = SppbjPl.findByPl(pesertaPl.lls_id);

        if (mailPembuktianKualifikasi != null){

            Map<String, Object> param = new HashMap<String, Object>(1);
            param.put("id", pesertaPl.lls_id);
            param.put("rkn_id", pesertaPl.rkn_id);
            param.put("jenis", JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);

            content.append("<a href=\"").append(Router.reverse("admin.UtilityCtr.viewUdanganPdfPl", param).url).append("\" class=\"badge\">"+Messages.get("ct.cupk")+"</a>&nbsp;");

        }

        if (sppbj != null && mailKontrak != null){

            if (sppbj.rkn_id == pesertaPl.rkn_id){

                Map<String, Object> param = new HashMap<String, Object>(1);
                param.put("id", pesertaPl.lls_id);
                param.put("rkn_id", pesertaPl.rkn_id);
                param.put("jenis", JenisEmail.UNDANGAN_KONTRAK);

                content.append("<a href=\"").append(Router.reverse("admin.UtilityCtr.viewUdanganPdfPl", param).url).append("\" class=\"badge\">"+Messages.get("ct.cuk")+"</a>&nbsp;");

            }

        }

        out.print(content.toString());
    }

    public static void _penawaranPesertaPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Pl_seleksi pl = (Pl_seleksi) args.get("arg");
        boolean hide = (Boolean) args.get("hide");
        Active_user active_user = Active_user.current();
        TahapNowPl tahapAktif = pl.getTahapNow();
        TahapStartedPl tahapStarted = pl.getTahapStarted();
        out.print("<table class=\"table table-striped table-condensed\">");
        out.print("<tr><th width=\"20\">#</th><th>"+Messages.get("tag.nama_penyedia_barang_jasa")+"</th><th width=\"150\">"+Messages.get("ct.tgl_dftr")+"</th>");
        out.print("<th width=\"150\" class=\"text-center\">"+Messages.get("ct.dpklfk")+"</th>");
        out.print("<th colspan=5 class=\"text-center\">"+Messages.get("ct.dc_p")+"</th>");
        out.print("</tr>");

        out.print("<tr>");
        out.print("<th></th><th></th><th></th>");
        out.print("<th></th>");
        out.print("<th width=\"150\" class=\"text-center\">"+Messages.get("ct.sp")+"</th>");
        out.print("<th width=\"150\" class=\"text-center\">"+Messages.get("ct.admt")+"</th>");
        out.print("<th width=\"150\" class=\"text-center\">"+Messages.get("ct.hrg")+"</th>");
        out.print("<th width=\"150\" class=\"text-center\">"+Messages.get("ct.mbl")+"</th>");
        out.print("<tr>");

        List<PesertaPl> list = pl.getPesertaList();
        if (!CollectionUtils.isEmpty(list)) {
            int idx = 1;
            DokPenawaranPl dok_teknis=null, dok_harga=null;
            Map<String, Object> param = new HashMap<String, Object>(1);
            BlobTable blob = null;
            for (PesertaPl peserta : list) {
                param.put("id", peserta.psr_id);
                out.print("<tr>");
                out.print("<td align=\"right\">"+idx+"</td>");
                if(hide)
                    out.print("<td>"+Messages.get("ct.pst")+" "+idx);
                else {
                    out.print("<td>"+peserta.getNamaPeserta());
                    if(active_user.isAuditor()) {
                        out.print("<a href=\""+Router.reverse("AuditorCtr.audit_log", param).url+"\" class=\"btn btn-success jpopup\" width=\"600\" height=\"400\" style=\"margin-top:3px;\" title=\""+Messages.get("ct.la")+"\"><i class=\"fa fa-edit\"></i></a>");
                    }
                    out.print("</td>");
                }
                out.print("<td>"+StringFormat.formatDate(peserta.psr_tanggal)+"</td>");
                out.print("<td class=\"text-center\">"+("<a href=\""+Router.reverse("nonlelang.KualifikasiPlCtr.preview", param).url+"\" class=\"badge jpopup\">"+Messages.get("tag.kualifikasi")+"</a>")+"</td>");
	            if(pl.isPenunjukanLangsungNew()) {    
	            		dok_teknis = peserta.getFileTeknis();
	            		if(tahapStarted.isEvaluasiPenawaranPl()) {
	            			out.print("<td class=\"text-center\">"+("<a href=\""+Router.reverse("nonlelang.PesertaPlCtr.cetakSuratPenawaranPeserta", param).url+"\" class=\"badge jpopup\">"+Messages.get("ct.cetak")+"</a>")+"</td>");
			                out.print("<td class=\"text-center\">"+(dok_teknis != null ? "<a href=\""+Router.reverse("nonlelang.PesertaPlCtr.rincian_adminteknis", param).url+"\" class=\"badge jpopup\">"+Messages.get("ct.dtl")+"</a>":"")+"</td>");
			                out.print("<td class=\"text-center\">"+(dok_teknis != null ? "<a href=\""+Router.reverse("nonlelang.PesertaPlCtr.rincian_penawaran", param).url+"\" class=\"badge jpopup\">"+Messages.get("ct.dtl")+"</a>":"")+"</td>");
			                out.print("<td class=\"text-center\">");
				                if(dok_teknis != null) {
			                    out.print(dok_teknis.dok_waktu + " "+Messages.get("ct.hr"));
		                }
	            	}
	            		else {
	            			out.print("<td class=\"text-center\">"+"<a href=#></a>"+"</td>");
	                    	out.print("<td class=\"text-center\">"+"<a href=#></a>"+"</td>");
	                    	out.print("<td class=\"text-center\">"+"<a href=#></a>"+"</td>");
	                    	out.print("<td class=\"text-center\">"+"<a href=#></a>"+"</td>");
	                    	out.print("<td class=\"text-center\">"+"<a href=#></a>"+"</td>");
	            		}
	            }
	            else {
	            	dok_teknis = peserta.getFileTeknis();	            	
	                out.print("<td class=\"text-center\">"+("<a href=\""+Router.reverse("nonlelang.PesertaPlCtr.cetakSuratPenawaranPeserta", param).url+"\" class=\"badge jpopup\">"+Messages.get("ct.cetak")+"</a>")+"</td>");
	                out.print("<td class=\"text-center\">"+(dok_teknis != null ? "<a href=\""+Router.reverse("nonlelang.PesertaPlCtr.rincian_adminteknis", param).url+"\" class=\"badge jpopup\">"+Messages.get("ct.dtl")+"</a>":"")+"</td>");
	                out.print("<td class=\"text-center\">"+(dok_teknis != null ? "<a href=\""+Router.reverse("nonlelang.PesertaPlCtr.rincian_penawaran", param).url+"\" class=\"badge jpopup\">"+Messages.get("ct.dtl")+"</a>":"")+"</td>");
	                out.print("<td class=\"text-center\">");
	                if(dok_teknis != null) {
	                    out.print(dok_teknis.dok_waktu + " "+Messages.get("ct.hr"));
	                }
	            }
                out.print("</td>");
                out.print("</tr>");
                idx++;
            }
        }
        out.print("</table>");
    }

    public static void _dokumenKontrakPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Long lelangId = (Long)args.get("arg");
        KontrakPl kontrakx = KontrakPl.find("lls_id=? ", lelangId).first();
        StringBuilder content = new StringBuilder();
        if (kontrakx != null) {
            BlobTable blob = null;
            content.append("<table>");
            if(kontrakx.kontrak_id_attacment2 != null) {
                blob = kontrakx.getBlob2();
                content.append("<tr><td>"+Messages.get("ct.sp")+" </td><td>:</td>");
                content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakPlHendler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
                content.append("</tr>");
            }
            if(kontrakx.kontrak_sskk_attacment != null) {
                blob = kontrakx.getBlobSskk();
                content.append("<tr><td>SSKK </td><td>: </td>");
                content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakPlHendler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
                content.append("</tr>");
            }
            PesananPl pesananx = PesananPl.find("kontrak_id=?",kontrakx.kontrak_id).first();
            if(pesananx != null && pesananx.pes_attachment != null) {
                blob = pesananx.getBlob();
                content.append("<tr><td>"+Messages.get("spmk")+" </td><td>: </td>");
                content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakPlHendler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
                content.append("</tr>");
            }
            List<BaPembayaranPl> listBap = BaPembayaranPl.find("kontrak_id=? order by bap_id asc", kontrakx.kontrak_id).fetch();
            if(!CommonUtil.isEmpty(listBap)) {
                int i = 1;
                for(BaPembayaranPl bapx : listBap) {
                    if(bapx.cetak_bap_attachment != null) {
                        blob = bapx.getBlobBap();
                        if(blob != null) {
                            content.append("<tr><td>Pembayaran ").append(i).append(" </td><td>: </td>");
                            content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakPlHendler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
                            content.append("</tr>");
                        }
                    }
                    if(bapx.cetak_bast_attachment != null) {
                        blob = bapx.getBlobBast();
                        if(blob != null) {
                            content.append("<tr><td>"+Messages.get("ct.pbyrn")+" ").append(i).append(" </td><td>: </td>");
                            content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakPlHendler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
                            content.append("</tr>");
                        }
                    }
                    i++;
                }
            }
            content.append("</table>");
        }
        out.print(content.toString());
    }

    public static void _persetujuanListPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Long lelangId = (Long)args.get("arg");
        boolean pemenang = (Boolean)args.get("pemenang");
        String jenisKey = (String)args.get("jenisKey");

        Active_user active_user = Active_user.current();
        List<PersetujuanPl.PersetujuanLelangPl> persetujuanList = null;
        PersetujuanPl.JenisPersetujuanPl jenis = null;

        if(jenisKey != null){
            jenis = PersetujuanPl.JenisPersetujuanPl.getByKey(jenisKey);
            PersetujuanPl.checkPersetujuanBatalLelang(lelangId, jenis);
            persetujuanList = PersetujuanPl.PersetujuanLelangPl.findBatalList(lelangId);
        }else{
            if(pemenang)  {
                PersetujuanPl.checkPersetujuanPemenang(lelangId);
                jenis = PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG;
            }else {
                PersetujuanPl.checkPersetujuanLelang(lelangId);
                jenis = PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG;
            }
            persetujuanList = PersetujuanPl.PersetujuanLelangPl.findList(lelangId, jenis);
        }

        PersetujuanPl persetujuan = PersetujuanPl.find("lls_id=? and peg_id=?", lelangId, active_user.pegawaiId).first();
        persetujuan = persetujuan != null ? persetujuan : new PersetujuanPl();

        StringBuilder content = new StringBuilder();
        content.append("<table class=\"table table-condensed\">");
        content.append("<tr><th width=\"350\">"+Messages.get("ct.apkj")+"</th><th>Status</th><th>"+Messages.get("ct.tgl")+"</th><th>"+Messages.get("ct.ats")+"</th></tr>");
        if(!CommonUtil.isEmpty(persetujuanList)) {
            for(PersetujuanPl.PersetujuanLelangPl item:persetujuanList) {
                Map<String, Object> param = new HashMap<String, Object>(1);
                param.put("pst_id", item.pst_id);
                content.append("<tr><td>");
                content.append("<a href=\"").append(Router.reverse("nonlelang.PersetujuanPlCtr.riwayatPersetujuan", param).url).append("\" class=\"dialog\" title=\""+Messages.get("ct.rp")+"\">");
                content.append(persetujuan.pst_id == item.pst_id? "Anda" : item.peg_nama);
                content.append("</a></td>");
                if(item.pst_status.isSetuju())
                    content.append("<td>").append("<i class=\"fa fa-check\"></i>").append("</td>");
                else if (item.pst_status.isTidakSetuju())
                    content.append("<td>").append("<i class=\"fa fa-times\"></i>").append("</td>");
                else
                    content.append("<td></td>");
                content.append("<td>").append(FormatUtils.formatDateTimeInd(item.pst_tgl_setuju)).append("</td>");
                content.append("<td>").append(JavaExtensions.raw(item.getAlasan())).append("</td>");
                content.append("</tr>");
            }
        }
        content.append("</table>");
        out.print(content.toString());
    }

    public static void _persetujuanListPp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Long lelangId = (Long)args.get("arg");
        boolean pemenang = (Boolean)args.get("pemenang");
        String jenisKey = (String)args.get("jenisKey");

        Active_user active_user = Active_user.current();
        List<PersetujuanPl.PersetujuanLelangPl> persetujuanList = null;
        PersetujuanPl.JenisPersetujuanPl jenis = null;

        if(jenisKey != null){
            jenis = PersetujuanPl.JenisPersetujuanPl.getByKey(jenisKey);
            PersetujuanPl.checkPersetujuanBatalPp(lelangId, jenis);
            persetujuanList = PersetujuanPl.PersetujuanLelangPl.findBatalList(lelangId);
        }else{
            if(pemenang)  {
                PersetujuanPl.checkPersetujuanPemenangPp(lelangId);
                jenis = PersetujuanPl.JenisPersetujuanPl.PEMENANG_LELANG;
            }else {
                PersetujuanPl.checkPersetujuanPp(lelangId);
                jenis = PersetujuanPl.JenisPersetujuanPl.PENGUMUMAN_LELANG;
            }
            persetujuanList = PersetujuanPl.PersetujuanLelangPl.findListPp(lelangId, jenis);
        }

        PersetujuanPl persetujuan = PersetujuanPl.find("lls_id=? and peg_id=?", lelangId, active_user.pegawaiId).first();
        persetujuan = persetujuan != null ? persetujuan : new PersetujuanPl();
        
        StringBuilder content = new StringBuilder();
        content.append("<table class=\"table table-condensed\">");
        content.append("<tr><th width=\"350\">"+Messages.get("ct.pjb_pngdn")+"</th><th>"+Messages.get("ct.status")+"</th><th>"+Messages.get("ct.tgl")+"</th><th>"+Messages.get("ct.ats")+"</th></tr>");
        if(!CommonUtil.isEmpty(persetujuanList)) {
            for(PersetujuanPl.PersetujuanLelangPl item:persetujuanList) {
                Map<String, Object> param = new HashMap<String, Object>(1);
                param.put("pst_id", item.pst_id);
                content.append("<tr><td>");
                content.append("<a href=\"").append(Router.reverse("nonlelang.PersetujuanPlCtr.riwayatPersetujuanPp", param).url).append("\" class=\"dialog\" title=\""+Messages.get("ct.rp")+"\">");
                content.append(persetujuan.pst_id == item.pst_id? "Anda" : item.peg_nama);
                content.append("</a></td>");
                if(item.pst_status.isSetuju())
                    content.append("<td>").append("<i class=\"fa fa-check\"></i>").append("</td>");
                else if (item.pst_status.isTidakSetuju())
                    content.append("<td>").append("<i class=\"fa fa-times\"></i>").append("</td>");
                else
                    content.append("<td></td>");
                content.append("<td>").append(FormatUtils.formatDateTimeInd(item.pst_tgl_setuju)).append("</td>");
                content.append("<td>").append(JavaExtensions.raw(item.getAlasan())).append("</td>");
                content.append("</tr>");
            }
        }
        content.append("</table>");
        out.print(content.toString());
    }

    public static void _dokNonTenderPokja(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        List<BlobTable> list = (List<BlobTable>) args.get("dok");
        String label = (String) args.get("arg");
        if(StringUtils.isEmpty(label))
            return;
        StringBuilder content = new StringBuilder();
        content.append("<div class=\"card card-info\">");
        content.append("<div class=\"card-header\">").append(Messages.get(label)).append("</div>");
        if (!CollectionUtils.isEmpty(list)) {
            content.append("<div class=\"list-group\">");
            for (BlobTable blob : list) {
                content.append("<a href=\"").append(blob.getDownloadUrl(DokLelangDownloadHandler.class))
                        .append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
                        .append(blob.getFileName()).append("<abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\"> (")
                        .append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
            }
            content.append("</div>");
        }
        content.append("</div>");
        out.print(content.toString());
    }

    public static void _dokPenawaranPlLain(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        BlobTable blob = (BlobTable)args.get("arg");
        if(blob == null)
            return;
        try {
            out.println(new StringBuilder("<a href=\"")
                    .append(blob.getDownloadUrl(DokPenawaranPlDownloadHandler.class))
                    .append("\"><span class=\"fa fa-download\"></span> ").append(blob.blb_nama_file).append("</a> - ").append(FormatUtils.formatBytes(blob.getFile().length())));
        }catch (Exception e) {
            e.printStackTrace();
            Logger.error("downloadSecurityHandler wajib diisi");
        }
    }

    public static void _listUndanganPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		PesertaPl peserta = (PesertaPl)args.get("arg");
		Pl_seleksi lelang = peserta.getPl_seleksi();
		TahapNowPl tahapNow = lelang.getTahapNow();
		TahapStartedPl tahapStarted = lelang.getTahapStarted();
		EvaluasiPl kualifikasi = lelang.getEvaluasiKualifikasi(true);
		NilaiEvaluasiPl nilaiPembuktian = peserta.getNilaiPembuktian();
		boolean isTahapKirimUndangan = false;
		isTahapKirimUndangan = tahapNow.isEvaluasiKualifikasiNew();
		Boolean allowKirimUndangan =
						(!lelang.isSedangPersetujuanPemenang() && kualifikasi.eva_status.isSedangEvaluasi() &&
						(Active_user.current().isPanitia() || Active_user.current().isPP()) &&
						nilaiPembuktian != null && isTahapKirimUndangan);
		boolean showKolomPembuktian = tahapStarted.isEvaluasiPl() && nilaiPembuktian != null;
		StringBuilder content = new StringBuilder();
		//Kolom Pembuktian Kualifikasi
		content.append(renderKolomPembuktian(lelang.lls_id,peserta.psr_id,allowKirimUndangan, showKolomPembuktian));
		out.print(content.toString());
	}

    private static String renderKolomPembuktian(Long lls_id, Long pesertaId, boolean showKirim, boolean showKolomPembuktian){
		if(showKolomPembuktian){
			JenisEmail jenisEmail = JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI;
			return renderKolomUndangan(lls_id,jenisEmail,pesertaId, showKirim);
		}
		return "";
    }

    private static String renderKolomUndangan(Long lls_id, JenisEmail jenis, Long pesertaId, boolean showKirim){
		PesertaPl peserta = PesertaPl.findBy(pesertaId);
		MailQueue mail = MailQueueDao.getByJenisAndLelangAndRekanan(lls_id, peserta.rkn_id , jenis.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);
		StringBuilder content = new StringBuilder();
		content.append("<tr>");
		content.append("<th class=\"bgwarning\">"+jenis.label);
		if(jenis.equals(JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI)){
			content.append(" <span class='warning'>*</span>");
		}
		content.append("</th>");
		content.append("<td>");
		String labelKirim = Messages.get("ct.krm");
		if(mail != null){
			labelKirim = labelKirim + " "+Messages.get("ct.ulg")+"";
			content.append(Messages.get("ct.sd")+" <i>("+FormatUtils.formatDateTimeInd(mail.enqueue_date)+")</i>");
		}else{
			content.append(Messages.get("ct.bd"));
		}
		String url = Router.reverse("nonlelang.KlarifikasiPlCtr.kirimPesan")
				.add("id",pesertaId)
				.add("jenis",jenis.id)
				.url;
		if(showKirim){
			content.append(" &nbsp<a href=\""+url+"\" class=\"badge  badge-secondary\">"+labelKirim+"</a>");
		}
		content.append("</tr>");
		return content.toString();
	}
    
    public static void _dokDukunganBankPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		BlobTable blob = (BlobTable)args.get("arg");
		if(blob == null)
			return;
		try {
			out.println(new StringBuilder("<a href=\"")
					.append(blob.getDownloadUrl(DokDukunganBankPlDownloadHandler.class))
					.append("\"><span class=\"fa fa-download\"></span> ").append(blob.blb_nama_file).append("</a> - ").append(FormatUtils.formatBytes(blob.getFile().length())));
		}catch (Exception e) {
			e.printStackTrace();
			Logger.error("downloadSecurityHandler wajib diisi");
		}
	}
}

