package ext;

import groovy.lang.Closure;
import models.agency.Panitia;
import models.agency.Sppbj;
import models.common.Kategori;
import models.common.Metode;
import models.common.Tahap;
import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.lelang.Evaluasi;
import models.lelang.History_jadwal;
import models.lelang.Jadwal;
import models.lelang.Lelang_seleksi;
import models.lelang.cast.*;
import models.lelang.Sanggahan;
import models.lelang.Checklist_sanggah;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import play.templates.JavaExtensions;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * tag2 terkait summary proses lelang
 *
 */
public class SummaryTags extends FastTags {

    public static void _summaryDokLelangPra(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Boolean lelangV3 = (Boolean) args.get("lelangV3");
        if(lelangV3) {
            List<BlobTable> listDokLelang = BlobTableDao.find("blb_id_content IN (SELECT dll_id_attachment FROM dok_lelang WHERE lls_id = ? AND dll_jenis = 0)",id).fetch();
            if(!CollectionUtils.isEmpty(listDokLelang)) {
                out.print("<div class=\"card card-primary\">");
                out.print("<div class=\"card-header\">Dokumen Kualifikasi</div>");
                out.print("<table class=\"table table-sm table-bordered\">");
                out.print("<thead><tr><th>Nama Dokumen</th><th>Tanggal Upload</th><th>Audituser</th></tr></thead>");
                out.print("<tbody>");
                if (!CollectionUtils.isEmpty(listDokLelang)) {
                    for (BlobTable dok_lelang : listDokLelang) {
                        out.print("<tr><td>" + JavaExtensions.raw(dok_lelang.blb_nama_file) + "</td><td>" + StringFormat.formatDateTime(dok_lelang.blb_date_time) + "</td><td>" + dok_lelang.audituser + "</td></tr>");
                    }
                }
                out.print("</tbody>");
                out.print("</table>");
                out.print("</div>");
            }
        } else {
            List<BlobTable> listDokLelang = BlobTableDao.find("blb_id_content IN (SELECT dll_id_attachment FROM dok_lelang WHERE lls_id = ? AND dll_jenis = 2)",id).fetch();
            if(!CollectionUtils.isEmpty(listDokLelang)) {
                out.print("<div class=\"card card-primary\">");
                out.print("<div class=\"card-header\">Dokumen Kualifikasi</div>");
                out.print("<table class=\"table table-sm table-bordered\">");
                out.print("<thead><tr><th>Nama Dokumen</th><th>Tanggal Upload</th><th>Pengirim</th></tr></thead>");
                out.print("<tbody>");
                if (!CollectionUtils.isEmpty(listDokLelang)) {
                    for (BlobTable dok_lelang : listDokLelang) {
                        out.print("<tr><td>" + JavaExtensions.raw(dok_lelang.blb_nama_file) + "</td><td>" + StringFormat.formatDateTime(dok_lelang.blb_date_time) + "</td><td>" + dok_lelang.audituser + "</td></tr>");
                    }
                }
                out.print("</tbody>");
                out.print("</table>");
                out.print("</div>");
            }
        }        
    }
    
    public static void _summaryDokLelang(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Boolean lelangV3 = (Boolean) args.get("lelangV3");
        List<BlobTable> listDokLelang = BlobTableDao.find("blb_id_content IN (SELECT dll_id_attachment FROM dok_lelang WHERE lls_id = ? AND dll_jenis = 1)",id).fetch();
        if(!CollectionUtils.isEmpty(listDokLelang)) {
            out.print("<div class=\"card card-primary\">");
            out.print("<div class=\"card-header\">Dokumen Pemilihan</div>");
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th>Nama Dokumen</th><th>Tanggal Upload</th><th>Pengirim</th></tr></thead>");
            out.print("<tbody>");
            if (!CollectionUtils.isEmpty(listDokLelang)) {
                for (BlobTable dok_lelang : listDokLelang) {
                    out.print("<tr><td>" + JavaExtensions.raw(dok_lelang.blb_nama_file) + "</td><td>" + StringFormat.formatDateTime(dok_lelang.blb_date_time) + "</td><td>" + dok_lelang.audituser + "</td></tr>");
                }
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("</div>");
        }
    }

    public static void _summaryJadwal(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Boolean lelangV3 = (Boolean) args.get("lelangV3");
        List<Jadwal> jadwalList = Jadwal.findByLelang(id);
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Jadwal</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th>Tahapan</th><th>Mulai</th><th>Akhir</th></tr></thead>");
        out.print("<tbody>");
        List<History_jadwal> historyList = null;
        if(!CollectionUtils.isEmpty(jadwalList)) {
            for(Jadwal jadwal : jadwalList) {
                out.print("<tr><td>"+jadwal.namaTahap(lelangV3)+"</td><td>"+StringFormat.formatDateTime(jadwal.dtj_tglawal)+"</td><td>"+StringFormat.formatDateTime(jadwal.dtj_tglakhir)+"</td></tr>");
                historyList = jadwal.getHistoryList();
                if(!CollectionUtils.isEmpty(historyList)) {
                    for(History_jadwal history : historyList) {
                        out.print("<tr><td><div align=\"right\">Perubahan Oleh "+history.getNamaPegawai()+"</div></td>");
                        out.print("<td>"+StringFormat.formatDateTime(history.hjd_tanggal_awal)+"</td>");
                        out.print("<td>"+StringFormat.formatDateTime(history.hjd_tanggal_akhir)+"</td></tr>");
                        out.print("<tr><td><div align=\"right\">Alasan</div></td><td colspan=\"2\">"+JavaExtensions.raw(history.hjd_keterangan)+"</td></tr>");
                    }
                }
            }
        }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }

    public static void _summaryPanitia(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Panitia panitia = (Panitia) args.get("arg");
        out.print("");
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Kelompok Kerja Pemilihan</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<tbody><tr><td class=\"bg-info\">Nama Kelompok Kerja</td><td>"+JavaExtensions.raw(panitia.pnt_nama)+"</td></tr>");
        out.print("<tr><td class=\"bg-info\">Nomor SK</td><td>"+JavaExtensions.raw(panitia.pnt_no_sk)+"</td></tr></tbody>");
        out.print("</table>");
        List<AnggotaPanitiaView> list = AnggotaPanitiaView.findByPantia(panitia.pnt_id);
        if(!CollectionUtils.isEmpty(list)) {
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th>Nama</th><th>NIP</th></tr></thead>");
            out.print("<tbody>");
            for(AnggotaPanitiaView anggota: list) {
                out.print("<tr><td>"+JavaExtensions.raw(anggota.peg_nama)+"</td><td>"+JavaExtensions.raw(anggota.peg_nip)+"</td></tr>");
            }
            out.print("</tbody></table>");
        }
        out.print("</div>");
    }

    public static void _summaryPeserta(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        List<PesertaView> pesertList = PesertaView.findByLelang(id);
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Peserta</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th>Peserta</th> <th>Tanggal Daftar</th></tr></thead>");
        out.print("<tbody>");
         if(!CollectionUtils.isEmpty(pesertList)) {
             for (PesertaView peserta : pesertList) {
                 out.print("<tr><td>" + JavaExtensions.raw(peserta.rkn_nama) + "</td><td>" + StringFormat.formatDateTime(peserta.psr_tanggal) + "</td></tr>");
             }
         }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }

    public static void _summaryPenjelasan(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Tahap tahap = Tahap.PENJELASAN;
        if(args.get("prakualifikasi") != null) {
            Boolean prakualifikasi = (Boolean) args.get("prakualifikasi");
            tahap = prakualifikasi ? Tahap.PENJELASAN_PRA : Tahap.PENJELASAN;
        }
        List<PenjelasanView> pertanyaanList = PenjelasanView.listTanggapanPeserta(id, tahap);
        List<PenjelasanView> penjelasanList = PenjelasanView.listTanggapanPanitia(id, tahap);
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">"+(tahap == Tahap.PENJELASAN_PRA?"Penjelasan Dokumen Prakualifikasi":"Penjelasan Tender")+"</div>");
        out.print("<div class=\"card card-default\">");
        out.print("<div class=\"card-header\">Pertanyaan Peserta</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th>Uraian</th><th>Tanggal</th><th>Pengirim</th></tr></thead>");
        out.print("<tbody>");
         if(!CollectionUtils.isEmpty(pertanyaanList)) {
             for (PenjelasanView pertanyaan : pertanyaanList) {
                 out.print("<tr><td>" + JavaExtensions.raw(pertanyaan.dsl_uraian) + "</td><td>" + StringFormat.formatDateTime(pertanyaan.dsl_tanggal) + "</td><td>" +
                         "<div class=\"icon-rekanan\"></div>" + JavaExtensions.raw(pertanyaan.rkn_nama) + "</td></tr>");
             }
         }
        out.print("");
        out.print("</tbody></table>");
        out.print("</div>");
        out.print("<div class=\"card card-default\">");
        out.print("<div class=\"card-header\">Penjelasan Panitia</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<tr><th>Uraian</th><th>Tanggal</th><th>Pengirim</th></tr>");
        out.print("<tbody>");
         if(!CollectionUtils.isEmpty(penjelasanList)) {
             for (PenjelasanView penjelasan : penjelasanList) {
                 out.print("<tr><td>" + JavaExtensions.raw(penjelasan.dsl_uraian) + "</td><td>" + StringFormat.formatDateTime(penjelasan.dsl_tanggal) + "</td><td>" +
                         "<div class=\"icon-panitia\"></div>" + JavaExtensions.raw(penjelasan.pnt_nama) + "</td></tr>");
             }
         }
        out.print("</tbody></table>");
        out.print("</div>");
        out.print("</div>");
    }

    public static void _summaryPenawaran(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        List<DokPenawaranView> penawaranList = DokPenawaranView.findAllByLelang(id);
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Penawaran Peserta</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th>Nama Peserta</th><th>Nama File</th><th>Tanggal Kirim</th><th>Hash Key</th></tr></thead>");
        out.print("<tbody>");
         if(!CollectionUtils.isEmpty(penawaranList)) {
             for (DokPenawaranView penawaran : penawaranList) {
                 out.print("<tr><td>" + JavaExtensions.raw(penawaran.rkn_nama) + "</td><td>" + penawaran.dok_judul + "</td><td>" + StringFormat.formatDateTime(penawaran.dok_tgljam) +
                         "</td><td>" + penawaran.dok_hash + "</td></tr>");
             }
         }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }


    public static void _summaryPemenang(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Boolean express = (Boolean) args.get("express");
        Evaluasi evaAkhir = Evaluasi.findPenetapanPemenang(id);
        if(evaAkhir == null || (express && evaAkhir.countPesertaList() < 2))
            return ;
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Pengumuman Pemenang</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th width=\"300\">Peserta</th><th>Pemenang</th></tr></thead>");
        out.print("<tbody>");
        List<NilaiEvaluasiView> pemenangList =  NilaiEvaluasiView.findAllPemenangByEvaluasi(evaAkhir.eva_id);
        for(NilaiEvaluasiView pemenang:pemenangList) {
            Integer nev_urutan = pemenang.nev_urutan != null ? pemenang.nev_urutan : 0;
            out.print("<tr><td>"+ nev_urutan + ' '+JavaExtensions.raw(pemenang.rkn_nama)+" ");
            if(express) {
                out.print((nev_urutan == 1 ? "(Harga Terendah)" : "") + ' ' + (pemenang.is_pemenang_verif == 1 ? "(Pemenang Terverifikasi)" : ""));
            }else {
                out.print(nev_urutan == 1 ? "(Pemenang)" : "");
            }
            out.print("</td><td>"+StringFormat.rupiah(pemenang.getHargaFinal())+"</td>");
            out.print("</tr>");
        }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }
    
    public static void _summaryPemenangPra(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Evaluasi evaPra = Evaluasi.findPembuktian(id);
        if(evaPra == null || evaPra.countPesertaList() < 2)
            return ;
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Pengumuman Pemenang Prakualifikasi</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th width=\"300\">Nama Peserta</th><th>Pemenang</th></tr></thead>");
        out.print("<tbody>");
        List<NilaiEvaluasiView> pemenangList =  NilaiEvaluasiView.findAllPemenangByEvaluasi(evaPra.eva_id);
        for(NilaiEvaluasiView pemenang:pemenangList) {
            out.print("<tr><td>"+JavaExtensions.raw(pemenang.rkn_nama)+"</td>");
            Integer nev_urutan = pemenang.nev_urutan != null ? pemenang.nev_urutan : 0;
            out.print("<td>" + nev_urutan + ' ' + (nev_urutan == 1 ? "(Pemenang Prakualifikasi)" : "") + "</td>");
            out.print("</tr>");
        }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }

    public static void _summarySanggahan(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Panitia panitia = (Panitia) args.get("panitia");
        Tahap tahap = Tahap.SANGGAH;
        if(args.get("prakualifikasi") != null) {
            Boolean prakualifikasi = (Boolean) args.get("prakualifikasi");
            tahap = prakualifikasi ? Tahap.SANGGAH_PRA : Tahap.SANGGAH;
        }
        List<SanggahanView> sanggahList = SanggahanView.findAllByLelangAndTahap(id, tahap, true);
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">"+(tahap == Tahap.SANGGAH_PRA?"Sanggah Prakualifikasi":"Sanggah Pemilihan")+"</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th width=\"60%\" colspan=\"2\">Sanggahan</th><th>Tanggal</th><th width=\"300\">Pengirim</th></tr></thead>");
        out.print("<tbody>");
         if(!CollectionUtils.isEmpty(sanggahList)) {
             for (SanggahanView sanggahan : sanggahList) {
            	 
            	 Sanggahan sanggah = Sanggahan.findById(sanggahan.sgh_id);
            	 List<Checklist_sanggah> checklist = sanggah.getChecklistKategori();
            	 StringBuilder kategori = new StringBuilder("");
            	 if (checklist != null && checklist.size() > 0) {
 					kategori.append("Kategori <br/> <table class=\"table table-sm\">");
 					for (Checklist_sanggah k : checklist)
 	 					kategori.append("<tr><td>-</td><td>").append(k.getChecklist_master().ckm_nama).append("</td></tr>");
 	 				kategori.append("</table>");
 				}
 				
            	 out.print("<tr><td colspan=\"2\">" + kategori + JavaExtensions.raw(sanggahan.sgh_isi) + "</td><td>" +FormatUtils.formatDateTimeInd(sanggahan.sgh_tanggal)+ "</td><td colspan=\"2\"><div class=\"icon-rekanan\"></div>" + JavaExtensions.raw(sanggahan.rkn_nama) + "</td></tr>");
                 
            	 
            	 for (SanggahanView jawaban : sanggahan.balasan) {
                	 out.print("<tr><td colspan=\"2\">" + JavaExtensions.raw(jawaban.sgh_isi) + "</td><td>" +FormatUtils.formatDateTimeInd(jawaban.sgh_tanggal)+ "</td><td colspan=\"2\"><div class=\"icon-rekanan\"></div>" + JavaExtensions.raw(panitia.pnt_nama) + "</td></tr>");
                 }
             }
         }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }

    public static void _summarySppbj(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        List<Sppbj> sppbjList = Sppbj.find("lls_id=?", id).fetch();
        if(CollectionUtils.isEmpty(sppbjList))
            return;
        out.print("<div class=\"card card-primary\">");
        out.print("<div class=\"card-header\">Daftar Penunjukan Penyedia Barang Jasa</div>");
        out.print("<table class=\"table table-sm table-bordered\">");
        out.print("<thead><tr><th>Nomor SPPBJ</th><th>Dokumen</th><th>Tanggal Kirim</th><th>Status</th></tr></thead>");
        out.print("<tbody>");
        BlobTable dokumen = null;
         if(!CollectionUtils.isEmpty(sppbjList)) {
             for (Sppbj sppbj : sppbjList) {
                 if (sppbj.sppbj_attachment != null) {
                     dokumen = BlobTableDao.getLastById(sppbj.sppbj_attachment);
                     out.print("<tr><td>" + JavaExtensions.raw(sppbj.sppbj_no) + "</td><td>" + JavaExtensions.raw(dokumen != null ? dokumen.blb_nama_file : "") + "</td><td>" + StringFormat.formatDateTime(sppbj.sppbj_tgl_kirim) + "</td><td>" + sppbj.getStatus() + "</td></tr>");
                 }
             }
         }
        out.print("</tbody>");
        out.print("</table>");
        out.print("</div>");
    }

    public static void _summaryEvaluasi(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Kategori kategori = (Kategori) args.get("kategori");
        Metode metode = Lelang_seleksi.getMetodeLelang(id);
        out.print("<div class=\"card card-primary\"><div class=\"card-header\">Hasil Evaluasi</div>");
        Integer lastVersi = Evaluasi.findCurrentVersi(id);
        for(int i=1;i<=lastVersi;i++) {
             if(metode.kualifikasi.isPra()){
                 _summaryEvaluasiKualifikasi(id, i, out);
             }
             _summaryEvaluasiPenawaran(id, i, out);
            if(metode.kualifikasi.isPasca()){
                _summaryEvaluasiKualifikasi(id, i, out);
            }
            Evaluasi penetapan = Evaluasi.findBy(id, Evaluasi.JenisEvaluasi.EVALUASI_AKHIR, i);
            if(penetapan != null) {
                List<NilaiEvaluasiView> list = null;
                    String title = (kategori.isKonsultansi() || kategori.isJkKonstruksi()) ? "Klarifikasi Teknis Dan Biaya" :"Harga Negosiasi";
                    out.print("<div class=\"card card-default\">");
                    out.print("<div class=\"card-header\">"+title+"</div>");
                    out.print("<div class=\"card card-default\">");
                    out.print("<table class=\"table table-sm table-bordered\">");
                    out.print("<thead><tr><th width=\"300\">Peserta</th><th width=\"120\">Harga Negosiasi</th>");
                    out.print("</tr></thead><tbody>");
                    list = NilaiEvaluasiView.findAllByEvaluasi(penetapan.eva_id);
                    for (NilaiEvaluasiView nilai : list) {
                        out.print("<tr><td>" + JavaExtensions.raw(nilai.rkn_nama) + "</td><td>" + StringFormat.rupiah(nilai.getHargaFinal()) + "</td></tr>");
                    }
                    out.print("</tbody>");
                    out.print("</table>");
                    out.print("</div>");
                    out.print("</div>");

            }
        }
        out.print("</div>");
    }
    
    public static void _summaryEvaluasiPra(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Kategori kategori = (Kategori) args.get("kategori");
        Metode metode = Lelang_seleksi.getMetodeLelang(id);
        out.print("<div class=\"card card-primary\"><div class=\"card-header\">Hasil Evaluasi</div>");
        Integer lastVersi = Evaluasi.findCurrentVersi(id);
        for(int i=1;i<=lastVersi;i++) {
             if(metode.kualifikasi.isPra()){
                 _summaryEvaluasiKualifikasi(id, i, out);
             }
           }
        out.print("</div>");
    }
    
    public static void _summaryEvaluasiPemilihan(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long id = (Long)args.get("arg");
        Kategori kategori = (Kategori) args.get("kategori");
        Metode metode = Lelang_seleksi.getMetodeLelang(id);
        out.print("<div class=\"card card-primary\"><div class=\"card-header\">Hasil Evaluasi</div>");
        Integer lastVersi = Evaluasi.findCurrentVersi(id);
        for(int i=1;i<=lastVersi;i++) {
             _summaryEvaluasiPenawaran(id, i, out);
           }
        out.print("</div>");
    }


    static void _summaryEvaluasiKualifikasi(Long id, Integer i, PrintWriter out) {
    	Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        List<NilaiEvaluasiView> list = null;
        Evaluasi kualifikasi = Evaluasi.findBy(id, Evaluasi.JenisEvaluasi.EVALUASI_KUALIFIKASI, i);
        if(kualifikasi != null) {
            out.print("<div class=\"card card-default\">");
            if(lelang.isPenawaranUlang() || lelang.isEvaluasiUlang()) {
            	out.print("<div class=\"card-header\">Evaluasi Kualifikasi ke-"+i+"</div>");
            }else {
            	out.print("<div class=\"card-header\">Evaluasi Kualifikasi</div>");
            }
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th width=\"300\">Nama Peserta</th>");
            out.print("<th width=\"40\">Hasil Evaluasi</th><th>Keterangan</th></tr></thead>");
            out.print("<tbody>");
            list = NilaiEvaluasiView.findAllByEvaluasi(kualifikasi.eva_id);
            for(NilaiEvaluasiView nilai:list) {
                out.print("<tr><td>"+JavaExtensions.raw(nilai.rkn_nama)+"</td>");
                out.print("<td>"+nilai.getStatus()+"</td>");
                out.print("<td>"+(StringUtils.isNotEmpty(nilai.nev_uraian)?JavaExtensions.raw(nilai.nev_uraian):"")+"</td>");
                out.print("</tr>");
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("</div>");
        }
        Evaluasi pembuktian = Evaluasi.findBy(id, Evaluasi.JenisEvaluasi.PEMBUKTIAN_KUALIFIKASI, i);
        if(pembuktian != null) {
            out.print("<div class=\"card card-default\">");
            if(lelang.isEvaluasiUlang() || lelang.isPenawaranUlang()) {
            	out.print("<div class=\"card-header\">Pembuktian Kualifikasi ke-"+i+"</div>");
            }
            else {
            	out.print("<div class=\"card-header\">Pembuktian Kualifikasi</div>");
            }
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th width=\"300\">Peserta</th>");
            out.print("<th width=\"40\">Lulus</th><th>Uraian</th></tr></thead>");
            out.print("<tbody>");
            list = NilaiEvaluasiView.findAllByEvaluasi(pembuktian.eva_id);
            for(NilaiEvaluasiView nilai:list) {
                out.print("<tr><td>"+JavaExtensions.raw(nilai.rkn_nama)+"</td>");
                out.print("<td>"+nilai.getStatus()+"</td>");
                out.print("<td>"+(StringUtils.isNotEmpty(nilai.nev_uraian)?JavaExtensions.raw(nilai.nev_uraian):"")+"</td>");
                out.print("</tr>");
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("</div>");
        }
    }

    static void _summaryEvaluasiPenawaran(Long id, Integer i, PrintWriter out) {
        List<NilaiEvaluasiView> list = null;
        Evaluasi administrasi = Evaluasi.findBy(id, Evaluasi.JenisEvaluasi.EVALUASI_ADMINISTRASI, i);
        if(administrasi != null) {
            out.print("<div class=\"card card-default\">");
            out.print("<div class=\"card-header\">Evaluasi Administrasi ke-"+i+"</div>");
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th width=\"300\">Peserta</th>");
            out.print("<th width=\"40\">Lulus</th><th>Uraian</th></tr></thead>");
            out.print("<tbody>");
            list = NilaiEvaluasiView.findAllByEvaluasi(administrasi.eva_id);
            for(NilaiEvaluasiView nilai:list) {
                out.print("<tr><td>"+JavaExtensions.raw(nilai.rkn_nama)+"</td>");
                out.print("<td>"+nilai.getStatus()+"</td>");
                out.print("<td>"+(StringUtils.isNotEmpty(nilai.nev_uraian)?JavaExtensions.raw(nilai.nev_uraian):"")+"</td>");
                out.print("</tr>");
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("</div>");
        }
        Evaluasi teknis = Evaluasi.findBy(id, Evaluasi.JenisEvaluasi.EVALUASI_TEKNIS, i);
        if(teknis != null) {
            out.print("<div class=\"card card-default\">");
            out.print("<div class=\"card-header\">Evaluasi Teknis ke-"+i+"</div>");
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th width=\"300\">Peserta</th>");
            out.print("<th width=\"40\">Lulus</th><th>Uraian</th></tr></thead>");
            out.print("<tbody>");
            list = NilaiEvaluasiView.findAllByEvaluasi(teknis.eva_id);
            for(NilaiEvaluasiView nilai:list) {
                out.print("<tr><td>"+JavaExtensions.raw(nilai.rkn_nama)+"</td>");
                out.print("<td>"+nilai.getStatus()+"</td>");
                out.print("<td>"+(StringUtils.isNotEmpty(nilai.nev_uraian)?JavaExtensions.raw(nilai.nev_uraian):"")+"</td>");
                out.print("</tr>");
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("</div>");
        }
        Evaluasi harga = Evaluasi.findBy(id, Evaluasi.JenisEvaluasi.EVALUASI_HARGA, i);
        if(harga != null) {
            out.print("<div class=\"card card-default\">");
            out.print("<div class=\"card-header\">Evaluasi Harga ke-"+i+"</div>");
            out.print("<table class=\"table table-sm table-bordered\">");
            out.print("<thead><tr><th width=\"300\">Peserta</th>");
            out.print("<th width=\"120\">Harga Penawaran</th><th width=\"120\">Harga Terkoreksi/Negosiasi</th>");
            out.print("<th width=\"40\">Lulus</th><th>Uraian</th></tr></thead>");
            out.print("<tbody>");
            list = NilaiEvaluasiView.findAllByEvaluasi(harga.eva_id);
            for(NilaiEvaluasiView nilai:list) {
                out.print("<tr><td>"+JavaExtensions.raw(nilai.rkn_nama)+"</td>");
                out.print("<td>"+StringFormat.rupiah(nilai.nev_harga)+"</td><td>"+StringFormat.rupiah(nilai.getHargaFinal())+"</td>");
                out.print("<td>"+nilai.getStatus()+"</td>");
                out.print("<td>"+(StringUtils.isNotEmpty(nilai.nev_uraian)?JavaExtensions.raw(nilai.nev_uraian):"")+"</td>");
                out.print("</tr>");
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("</div>");
        }
    }

}
