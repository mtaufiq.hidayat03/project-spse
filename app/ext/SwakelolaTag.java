package ext;

import groovy.lang.Closure;
import models.agency.contracts.AnggaranSwakelolaContract;
import models.agency.contracts.PaketAnggaranSwakelolaContract;
import models.agency.contracts.SirupContract;
import play.i18n.Messages;
import play.templates.FastTags;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author Abdul Wahid on 2/8/2019
 * class ini ada untuk mengurangi duplicated code antara paket-edit dengan paket-pl-edit,
 * terutama table rup dan table anggaran karena 2 table tersebut memiliki kesamaan,
 */
public class SwakelolaTag extends FastTags {

    public static void _rupTableSwa(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template,int fromLine) {
        List<? extends SirupContract> sirups = (List<? extends SirupContract>) args.get("sirups");
        String action = (String) args.get("action");
        Boolean isDraft = (Boolean) args.get("isDraft");
        if(isDraft == null)
            isDraft = Boolean.FALSE;
        Long paketId = (Long) args.get("paketId");
        StringBuilder content = new StringBuilder();
        content.append("<table class=\"table table-condensed\"><tr><th width=\"80\">").append(Messages.get("ct.kdrup")).append("</th>" +
                "<th>"+Messages.get("ct.np")+"</th><th width=\"200\">"+Messages.get("ct.sbd")+"</th><th></th></tr>");
        int index = 0;
        for (SirupContract sirup : sirups) {
            content.append("<tr>");
            content.append("<td>").append(sirup.getId()).append("</td>");
            content.append("<td>").append(sirup.getNama()).append("</td>");
            content.append("<td>").append(sirup.getSumberDanaString()).append("</td>");
            content.append("<td>");
            if (isDraft && index > 0) {
                content.append("<a href=\"").append(sirup.getDeleteUrl(paketId, action))
                        .append("\"><i class=\"fa fa-trash-o fa-lg\"></i></a>");
            }
            content.append("</td>");
            content.append("</tr>");
            index++;
        }
        content.append("</table>");
        out.print(content);
    }

    public static void _anggaranTableSwa(Map<?, ?> args,
                                      Closure body,
                                      PrintWriter out,
                                      GroovyTemplate.ExecutableTemplate template,
                                      int fromLine) {
        List<? extends PaketAnggaranSwakelolaContract> contracts = (List<? extends PaketAnggaranSwakelolaContract>) args.get("anggarans");
        Boolean isAllowEditPpk = (Boolean) args.get("allowEdit");
        String action = (String) args.get("action");
        StringBuilder content = new StringBuilder();
        content.append("<table id=\"tblSumberAnggaran\" class=\"table table-condensed\"><tr><th width=\"80\">").append(Messages.get("ct.tahun")).append("</th>" +
                "<th width=\"200\">"+Messages.get("ct.sbd")+"</th><th>"+Messages.get("ct.ka")+"</th><th width=\"150\">"+Messages.get("ct.nilai")+"</th>" +
                "<th width=\"150\">PPK</th></tr>");
        for (PaketAnggaranSwakelolaContract contract : contracts) {
            AnggaranSwakelolaContract anggaran = contract.getAnggaranContract();
            boolean isAnggaranExist = anggaran != null;
            content.append("<tr>");
            content.append("<td>").append(isAnggaranExist ? anggaran.getAngTahun() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist ? anggaran.getSbdLabel() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist
                    ? anggaran.getKodeRekening() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist
                    ? anggaran.getAngNilaiFormatted() : "").append("</td>");
            content.append("<td>").append(contract.getPpkNama());
            if (isAllowEditPpk) {
                content.append("<a class=\"btn btn-default btn-xs\" title=\""+Messages.get("ct.pppk")+"\" href=\"")
                        .append(contract.getUpdatePpkUrl(action))
                        .append("\">")
                        .append("<i class=\"fa fa-edit\"></i>&nbsp;")
                        .append(contract.optionLabel())
                        .append(" PPK")
                        .append("</a>");
            }
            content.append("</td>");
            content.append("</tr>");
        }
        content.append("</table>");
        out.print(content);
    }
}
