package ext;

import com.google.gson.Gson;
import controllers.BasicCtr;
import controllers.lelang.Lt17Ctr;
import controllers.security.KeyUtilities;
import groovy.lang.Closure;
import models.agency.*;
import models.common.*;
import models.handler.*;
import models.jcommon.blob.BlobTable;
import models.jcommon.mail.MailQueue;
import models.jcommon.util.CommonUtil;
import models.kontrak.BA_Pembayaran;
import models.kontrak.Pesanan;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Evaluasi.JenisEvaluasi;
import models.lelang.Persetujuan.JenisPersetujuan;
import models.lelang.Persetujuan.PersetujuanLelang;
import models.osd.Certificate;
import models.osd.JenisCertificate;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Http.Request;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;
import play.utils.HTML;
import utils.PenawaranUtil;
import utils.osd.OSDUtil;

import java.io.PrintWriter;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * componen tags terkait masalah workflow lelang
 * @author development
 *
 */
public class LelangTag extends FastTags {

	public static void _dokLelang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Dok_lelang dok_lelang =  (Dok_lelang)args.get("arg");
		boolean lelangV3 = (Boolean) args.get("lelangV3");
		if (dok_lelang != null) {
			StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(dok_lelang.dll_jenis.getLabel()).append("</div>");
			if(lelangV3 && dok_lelang != null) { // untuk handle dok_lelang dan dok_kualifikasi lelang versi 3.x
				content.append("<div class=\"list-group\">");
				List<BlobTable> list = dok_lelang.getDokumenList();
				if (!CommonUtil.isEmpty(list)) {
					for (BlobTable dok : list) {
						if (dok != null) {
							content.append("<a href=\"").append(dok.getDownloadUrl(DokLelangDownloadHandler.class))
									.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
									.append(dok.getFileName())
									.append(" <abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\">(")
									.append(FormatUtils.formatDateTimeInd(dok.blb_date_time)).append(")</abbr></a>");
						}
					}
				}
				content.append("</div>");
				// dokumen adendum v3
				JenisDokLelang jenis_adendum = dok_lelang.dll_jenis.isDokLelang() ? JenisDokLelang.DOKUMEN_ADENDUM:JenisDokLelang.DOKUMEN_ADENDUM_PRA;
				Dok_lelang dok_adendum = Dok_lelang.findBy(dok_lelang.lls_id, jenis_adendum);
				if(dok_adendum != null) {
					content.append("<div class=\"panel-heading\">").append(dok_adendum.dll_jenis.getLabel()).append("</div>");
					content.append("<div class=\"list-group\">");
					list = dok_lelang.getDokumenList();
					if (!CommonUtil.isEmpty(list)) {
						for (BlobTable dok : list) {
							if (dok != null) {
								content.append("<a href=\"").append(dok.getDownloadUrl(DokLelangDownloadHandler.class))
										.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
										.append(dok.getFileName())
										.append(" <abbr title=\"").append(Messages.get("ct.tgl_kirim")).append(" class=\"initialism\">(")
										.append(FormatUtils.formatDateTimeInd(dok.blb_date_time))
										.append(")</abbr></a>");
							}
						}
					}
					content.append("</div>");
				}
			} else {
				String preview = "lelang.DokumenCtr.preview";
				Map<String, Object> params = new HashMap<String, Object>(1);
				content.append("<div class=\"list-group\">");
				List<Dok_lelang_content> contentList = Dok_lelang_content.find("dll_id=? order by dll_versi", dok_lelang.dll_id).fetch();
				if (dok_lelang != null && dok_lelang.dll_id_attachment != null) {
					params.put("id", dok_lelang.dll_id);
					content.append("<a href=\"").append(Router.reverse(preview, params).url).append("\" class=\"list-group-item\" target=\"_blank\" title=\"Dokumen Tender\">").append(dok_lelang.dll_nama_dokumen).append("</a>");
				}
				if (!CommonUtil.isEmpty(contentList)) {
					for (Dok_lelang_content dok_content : contentList) {
						if (dok_content.dll_versi == 1 || dok_content.dll_modified) // versi 1 atau dll_modified=true tidak boleh muncul sebagai adendum
							continue;
						params.put("id", dok_content.dll_id);
						params.put("versi", dok_content.dll_versi);
						content.append("<a href=\"").append(Router.reverse(preview, params).url).append("\" class=\"list-group-item\" target=\"_blank\" title=\"Perubahan\">").append(Messages.get("lelang.perubahan")+" ").append(dok_content.dll_versi - 1).append("</a>");
					}
				}
				content.append("</div>");
			}
			content.append("</div>");
			out.print(content.toString());
		}
	}

	/**
	 * untuk penanganan flow berita acara
	 */
	public static void _beritaAcara(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,	int fromLine) {
		Lelang_seleksi lelang = (Lelang_seleksi) args.get("arg");
		TahapStarted tahapStarted = (TahapStarted) args.get("tahapStarted");
		List<Tahap> balist = lelang.isSatuFile() ? Berita_acara.BERITA_ACARA_SATU_FILE: Berita_acara.BERITA_ACARA_DUA_FILE;
		Active_user active_user = Active_user.current();
		boolean allow_upload_ba = lelang.lls_status.isAktif() && active_user.isPanitia();
		boolean allow_download_ba = lelang.isAllowDownloadBA();
		Berita_acara berita_acara = null;
		StringBuilder content = new StringBuilder();
		boolean allowBaNegosiasi = false;
		Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(lelang.lls_id);
		if(evaluasi != null && evaluasi.eva_status.isSelesai()) {
			List<Nilai_evaluasi> pemenangList = evaluasi.findPesertaList(lelang.getPemilihan().isLelangExpress());
			allowBaNegosiasi = pemenangList.size() < 3 || lelang.getPemilihan().isSeleksiUmum() || lelang.getPemilihan().isSeleksiSederhana();
			// yang dilakukan nego hanya jika jumlah penawar < 3 dan e-seleksi
		}
		if (balist.contains(Tahap.UPLOAD_BA_PENJELASAN) && !active_user.isRekanan() && tahapStarted.isPenjelasanPra() &&
				Diskusi_lelang.isEnd(lelang.lls_id, Tahap.PENJELASAN_PRA, controllers.BasicCtr.newDate())) {
			berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_PENJELASAN_PRA);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("ct.ba_pbpk"));
			if (allow_upload_ba && !lelang.isLelangV3()) {
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
						.append(" title=\"").append(Messages.get("ct.cbappk")).append("\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_PENJELASAN_PRA).append("\">").append(Messages.get("ct.cetak")).append("</a>");
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_PENJELASAN_PRA).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			if (berita_acara != null && !active_user.isRekanan()) {
				List<BlobTable> list = berita_acara.getDokumen();
				if(!CollectionUtils.isEmpty(list)) {
					content.append("<div class=\"list-group\">");
					for (BlobTable blob : list) {
						content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
								.append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
								.append(blob.getFileName()).append("<abbr title=\"").append(Messages.get("ct.tgl_kirim")).append("\" class=\"initialism\"> (")
								.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
					}
					content.append("</div>");
				}
			}
			content.append("</div>");
		}
		
		if (balist.contains(Tahap.UPLOAD_BA_PENJELASAN) && !active_user.isRekanan() && tahapStarted.isPenjelasan() && 
			Diskusi_lelang.isEnd(lelang.lls_id, Tahap.PENJELASAN, controllers.BasicCtr.newDate())) {
			String title = Messages.get("ct.ba_pp");
			if (tahapStarted.isPenjelasanPra()) title += Messages.get("ct.pemilihan");
			berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_PENJELASAN);
			content.append("<div class=\"card card-info\">");
			content.append("<div class=\"card-header\">").append(title);
			if (allow_upload_ba && !lelang.isLelangV3()) {
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
						.append(" title=\""+Messages.get("ct.cetak")+"").append(title)
						.append("\" value=\"").append(Tahap.UPLOAD_BA_PENJELASAN).append("\">").append(Messages.get("ct.cetak")).append("</a>");
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\""+Messages.get("ct.upload")+"").append(title)
						.append("\" value=\"").append(Tahap.UPLOAD_BA_PENJELASAN).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			if (berita_acara != null && !active_user.isRekanan()) {
				List<BlobTable> list = berita_acara.getDokumen();
				if(!CollectionUtils.isEmpty(list)) {
					content.append("<div class=\"list-group\">");
					for (BlobTable blob : list) {
						content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
								.append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
								.append(blob.getFileName()).append("<abbr title=\"").append(Messages.get("ct.tgl_kirim")).append("\" class=\"initialism\"> (")
								.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
					}
					content.append("</div>");
				}
			}
			content.append("</div>");
		}


		if (balist.contains(Tahap.UPLOAD_BA_EVALUASI_PENAWARAN) && !active_user.isRekanan() && tahapStarted.isPenetapanPemenangAkhir()) {
			berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_EVALUASI_PENAWARAN);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("ct.ba_ep"));
			if (allow_upload_ba && !lelang.isLelangV3()) {
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
						.append(" title=\""+Messages.get("ct.ctk_ba_evaluasi")+"\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_PENAWARAN).append("\">"+Messages.get("ct.cetak")+"</a>");
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\""+Messages.get("ct.ubaep")+"\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_PENAWARAN).append("\">"+Messages.get("ct.upload")+"</a>");
			}
			content.append("</div>");
			if (berita_acara != null && !active_user.isRekanan()) {
				List<BlobTable> list = berita_acara.getDokumen();
				if(!CollectionUtils.isEmpty(list)) {
					content.append("<div class=\"list-group\">");
					for (BlobTable blob : list) {
						content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
								.append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
								.append(blob.getFileName()).append("<abbr title=\""+Messages.get("ct.tgl_kirim")+"\" class=\"initialism\"> (")
								.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
					}
					content.append("</div>");
				}
			}

			content.append("</div>");
		}
		if (balist.contains(Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS) && !active_user.isRekanan() && tahapStarted.isPembukaanTeknis()) {
			berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_evaluasi_penawaran_admin_teknis")+"");
			if (allow_upload_ba && !lelang.isLelangV3()) {
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
						.append(" title=\""+Messages.get("lelang.cetak")+" "+Messages.get("lelang.berita_acara_evaluasi_penawaran_admin_teknis")+"\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS).append("\">"+Messages.get("lelang.cetak")+"</a>");
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\""+Messages.get("lelang.upload")+" "+Messages.get("lelang.berita_acara_evaluasi_penawaran_admin_teknis")+"\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_ADM_TEKNIS).append("\">"+Messages.get("lelang.upload")+"</a>");
			}
			content.append("</div>");
			if (berita_acara != null && !active_user.isRekanan()) {
				List<BlobTable> list = berita_acara.getDokumen();
				if(!CollectionUtils.isEmpty(list)) {
					content.append("<div class=\"list-group\">");
					for (BlobTable blob : list) {
						content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
								.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
								.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
								.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");

					}
					content.append("</div>");
				}
			}
			content.append("</div>");
		}
		if (balist.contains(Tahap.UPLOAD_BA_EVALUASI_HARGA) && !active_user.isRekanan() && tahapStarted.isPembukaanHarga()) {
			berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_EVALUASI_HARGA);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_evaluasi_penawaran_harga")+"");
			if (allow_upload_ba && !lelang.isLelangV3()) {
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
						.append(" title=\""+Messages.get("lelang.cetak")+" "+Messages.get("lelang.berita_acara_evaluasi_penawaran_harga")+"\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_HARGA).append("\">"+Messages.get("lelang.cetak")+"</a>");
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\""+Messages.get("lelang.upload")+" "+Messages.get("lelang.berita_acara_evaluasi_penawaran_harga")+"\"")
						.append(" value=\"").append(Tahap.UPLOAD_BA_EVALUASI_HARGA).append("\">"+Messages.get("lelang.upload")+"</a>");
			}
			content.append("</div>");
			if (berita_acara != null && !active_user.isRekanan()) {
				List<BlobTable> list = berita_acara.getDokumen();
				if(!CollectionUtils.isEmpty(list)) {
					content.append("<div class=\"list-group\">");
					for (BlobTable blob : list) {
						content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
								.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
								.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
								.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
					}
					content.append("</div>");
				}
			}
			content.append("</div>");
		}
		if (lelang.getPemilihan().isLelangExpress()) {
			if (balist.contains(Tahap.UPLOAD_BA_HASIL_LELANG)) {
				berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_HASIL_LELANG);
				content.append("<div class=\"panel panel-info\">");
				content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_pemilihan")+"");
				if (allow_upload_ba && !lelang.isLelangV3()) {
					content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
							.append(" title=\""+Messages.get("lelang.cetak")+Messages.get("lelang.berita_acara_pemilihan ")+"\"")
							.append(" value=\"").append(Tahap.UPLOAD_BA_HASIL_LELANG).append("\">"+Messages.get("lelang.cetak")+"</a>");
					content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
							.append(" title=\""+Messages.get("lelang.upload")+Messages.get("lelang.berita_acara_pemilihan ")+"\"")
							.append(" value=\"").append(Tahap.UPLOAD_BA_HASIL_LELANG).append("\">"+Messages.get("lelang.upload")+"</a>");
				}
				content.append("</div>");
				if (berita_acara != null && ((allow_download_ba && active_user.isRekanan()) || !active_user.isRekanan())) {
					List<BlobTable> list = berita_acara.getDokumen();
					if(!CollectionUtils.isEmpty(list)) {
						content.append("<div class=\"list-group\">");
						for (BlobTable blob : list) {
							content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
									.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
									.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
									.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
						}
						content.append("</div>");
					}
				}
				content.append("</div>");
			}

			if (balist.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR)) {
				berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.PENGUMUMAN_PEMENANG_AKHIR);
				content.append("<div class=\"panel panel-info\">");
					content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_reverse_auction")+"");
				if (allow_upload_ba && !lelang.isLelangV3()) {
						content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
								.append(" title=\""+Messages.get("lelang.cetak")+" "+Messages.get("lelang.berita_acara_reverse_auction")+"\"")
								.append(" value=\"").append(Tahap.PENGUMUMAN_PEMENANG_AKHIR).append("\">Cetak</a>");
						content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
								.append(" title=\""+Messages.get("lelang.upload")+" "+Messages.get("lelang.berita_acara_reverse_auction")+"\"")
								.append(" value=\"").append(Tahap.PENGUMUMAN_PEMENANG_AKHIR).append("\">Upload</a>");
				}
				content.append("</div>");

				if (berita_acara != null && !active_user.isRekanan()) {
					List<BlobTable> list = berita_acara.getDokumen();
					if(!CollectionUtils.isEmpty(list)) {
						content.append("<div class=\"list-group\">");
						for (BlobTable blob : list) {
							content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
									.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
									.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
									.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
						}
						content.append("</div>");
					}
				}
				content.append("</div>");

			}
		} else {
			if (balist.contains(Tahap.UPLOAD_BA_HASIL_LELANG)  && tahapStarted.isPengumumanPemenangAkhir()) {
				berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.UPLOAD_BA_HASIL_LELANG);
				content.append("<div class=\"panel panel-info\">");
				content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_pemilihan"));
				if (allow_upload_ba && !lelang.isLelangV3()) {
					content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
							.append(" title=\""+Messages.get("lelang.cetak")+" "+Messages.get("lelang.berita_acara_pemilihan")+"\"")
							.append(" value=\"").append(Tahap.UPLOAD_BA_HASIL_LELANG).append("\">"+Messages.get("lelang.cetak")+"</a>");
					content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
							.append(" title=\""+Messages.get("lelang.upload")+" "+Messages.get("lelang.berita_acara_pemilihan")+"\"")
							.append(" value=\"").append(Tahap.UPLOAD_BA_HASIL_LELANG).append("\">"+Messages.get("lelang.upload")+"</a>");
				}
				content.append("</div>");
				if (berita_acara != null && ((allow_download_ba && active_user.isRekanan()) || !active_user.isRekanan())) {
					List<BlobTable> list = berita_acara.getDokumen();
					if(!CollectionUtils.isEmpty(list)) {
						content.append("<div class=\"list-group\">");
						for (BlobTable blob : list) {
							content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
									.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
									.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
									.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
						}
						content.append("</div>");
					}
				}
				content.append("</div>");
			}

			if (balist.contains(Tahap.PENGUMUMAN_PEMENANG_AKHIR) && (tahapStarted.isPenetapanPemenangAkhir() && !active_user.isRekanan()) && allowBaNegosiasi) {
				berita_acara = Berita_acara.findBy(lelang.lls_id, Tahap.PENGUMUMAN_PEMENANG_AKHIR);
				content.append("<div class=\"card card-info\">");
				if(lelang.isAuction()) {
					content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_reverse_auction")+"");
				}else {
					content.append("<div class=\"panel-heading\">"+Messages.get("lelang.berita_acara_hasil_negosiasi")+"");
				}
				if (allow_upload_ba && !lelang.isLelangV3()) {
						if(lelang.isAuction()) {
							content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
									.append(" title=\""+Messages.get("lelang.cetak")+" "+Messages.get("lelang.berita_acara_reverse_auction")+"\"")
									.append(" value=\"").append(Tahap.PENGUMUMAN_PEMENANG_AKHIR).append("\">Cetak</a>");
							content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
									.append(" title=\""+Messages.get("lelang.upload")+" "+Messages.get("lelang.berita_acara_reverse_auction")+"\"")
									.append(" value=\"").append(Tahap.PENGUMUMAN_PEMENANG_AKHIR).append("\">Upload</a>");
						}else {
							content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"cetak\"")
								.append(" title=\""+Messages.get("lelang.cetak")+" "+Messages.get("lelang.berita_acara_hasil_negosiasi")+"\"")
								.append(" value=\"").append(Tahap.PENGUMUMAN_PEMENANG_AKHIR).append("\">Cetak</a>");
							content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
								.append(" title=\""+Messages.get("lelang.upload")+" "+Messages.get("lelang.berita_acara_hasil_negosiasi")+"\"")
								.append(" value=\"").append(Tahap.PENGUMUMAN_PEMENANG_AKHIR).append("\">Upload</a>");
					}
				}

				content.append("</div>");

				if (berita_acara != null && !active_user.isRekanan()) {
					List<BlobTable> list = berita_acara.getDokumen();
					if(!CollectionUtils.isEmpty(list)) {
						content.append("<div class=\"list-group\">");
						for (BlobTable blob : list) {
							content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDownloadHandler.class))
									.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
									.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
									.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
						}
						content.append("</div>");
					}
				}

				content.append("</div>");

			}
		}

		if (active_user.isRekanan())
			content.append("<div class=\"bs-callout bs-callout-info\">")
					.append(Berita_acara.getDownloadInfo(lelang.getKategori())).append("</div>");
		if (active_user.isPanitia() && !lelang.getPemilihan().isLelangExpress())
			content.append("<div class=\"bs-callout bs-callout-info\">"+Messages.get("lelang.pengiriman_berita_acara_dilakukan")+"</div>");
		out.print(content.toString());
	}

	/**
	 * Rincian HPS
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _rincianLelang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DaftarKuantitas dkh = (DaftarKuantitas)args.get("arg");
		StringBuilder content = new StringBuilder();
		if(dkh != null) {
			Locale indo = new Locale("id", "ID");
			NumberFormat defaultFormat = NumberFormat.getNumberInstance(indo);

            List<Rincian_hps> rincianList = dkh.items;
            content.append("<table class=\"table table-condensed table-bordered\" id=\"tblBayarUmum\" border=\"1\">\n");
            content.append("<tr><th width=\"200\">"+Messages.get("lelang.jenis_barang_jasa")+"</th><th width=\"100\">"+Messages.get("lelang.satuan_unit")+"</th><th width=\"100\">"+Messages.get("lelang.volume")+"</th>");
            content.append("<th width=\"150\">Keterangan</th></tr>");
            if (rincianList != null) {
				for (Rincian_hps rincian : rincianList) {
					content.append("<tr>");
					content.append("<td style=\"padding:5px;\" width=\"200\">").append(rincian.item != null ? HTML.htmlEscape(rincian.item) : "").append("</td>");
					content.append("<td style=\"padding:5px;\" width=\"100\">").append(rincian.unit != null ? HTML.htmlEscape(rincian.unit) : "").append("</td>");
					content.append("<td style=\"padding:5px;\" width=\"100\">").append(rincian.vol != null ? defaultFormat.format(rincian.vol) : "").append("</td>");
					content.append("<td style=\"padding:5px;\" width=\"150\">").append(rincian.keterangan != null ? HTML.htmlEscape(rincian.keterangan) : "").append("</td>");
					content.append("</tr>");
				}
			}
            content.append("</table>");
        }
        out.print(content.toString());
    }
    //SPPBJ lelang
    public static void _sppbjPenyedia(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Long lelangId = (Long)args.get("arg");
        Active_user active_user = Active_user.current();
        List<Sppbj> list = null;
        if(active_user.isRekanan())
        	list = Sppbj.find("lls_id=? and rkn_id=?", lelangId, active_user.rekananId).fetch();
        else if(active_user.isPpk())
        	list = Sppbj.find("lls_id=? and ppk_id=?", lelangId, active_user.ppkId).fetch();
        if(CommonUtil.isEmpty(list))
        	out.print("-");
        else {
        	BlobTable blob = null;
        	StringBuilder content = new StringBuilder();
        	content.append("<table class=\"table table-striped\"><thead><th>"+Messages.get("lelang.nama_file")+"</th><th>"+Messages.get("lelang.tanggal_kirim")+"</th></thead><tbody>");
        	for (Sppbj sppbj : list) {
        		blob = sppbj.getBlob();
        		if(blob != null) {
        			content.append("<tr><td><a href=\"").append(blob.getDownloadUrl(SPPBJDowloadHandler.class)).append("\"><i class=\"glyphicon glyphicon-download\"></i> ").append(blob.getFileName()).append("</a></td>");
	        		content.append("<td>"+FormatUtils.formatDateInd(sppbj.sppbj_tgl_kirim)+"</td>");
	        		content.append("</tr>");
        		}
			}
			content.append(" </tbody></table>");
			out.print(content.toString());
		}

	}

	// Kirim Penawaran dan Status kirim Penawaran Peserta
	public static void _kirimPenawaran(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Lelang_seleksi lelang = (Lelang_seleksi) args.get("arg");
		int jenisId = (Integer)args.get("jenis");
		JenisDokPenawaran jenis = JenisDokPenawaran.fromValue(jenisId);
		Peserta peserta = (Peserta) args.get("peserta");
		TahapNow tahapAktif = (TahapNow) args.get("tahapAktif");
		Dok_penawaran penawaran_teknis = peserta.getFileTeknis();
		Dok_penawaran penawaran_harga = peserta.getFileHarga();
		StringBuilder content = new StringBuilder();
		boolean isOSD = ConfigurationDao.isOSD(lelang.getPaket().pkt_tgl_buat);
		if(isOSD) {
			content.append("<div class=\"bs-callout bs-callout-danger\">"+Messages.get("lelang.kirim_dokumen_penawaran_spamkodok")+" ").append(Lt17Ctr.SPAMKODOK_VERSI).append("</div>");
		} else {
			content.append("<div class=\"bs-callout bs-callout-danger\">"+Messages.get("lelang.kirim_dokumen_penawaran_apendo")+"").append(Lt17Ctr.versiApendoBuildSistem).append(" "+Messages.get("lelang.token_dibawah")+"</div>");
		}
		content.append("<div class=\"bs-callout bs-callout-danger\">"+Messages.get("lelang.perhatikan_status_pengiriman_dokumen")+"</div>");
		content.append("<div class=\"bs-callout bs-callout-danger\">"+Messages.get("lelang.sesuaikan_upload_file_dokumen_penawaran")+"</div>");
		content.append("<div id=\"penawaran-info\">");
		content.append(PenawaranUtil.template(lelang, peserta, penawaran_harga, penawaran_teknis, jenis));
		content.append("</div>");
		boolean allow_kirim_penwaran = lelang.isAllowKirimPenawaran(peserta, tahapAktif, jenis) && lelang.lls_status.isAktif();
		if (allow_kirim_penwaran && !lelang.isLelangV3()) {
			String token = Lelang_token.findNGeneratePeserta(peserta.psr_id, lelang.lls_id, Request.current().host, jenis);
			String root_url = BasicCtr.getRequestHost() + Lt17Ctr.ROOT_URL_PENAWARAN;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("root_url", root_url);
			params.put("access_token", token);
			String json = new Gson().toJson(params);
			String token_url = root_url + '/' + Base64.encodeBase64String(json.getBytes());
			if(isOSD) {
				content.append("<div class=\"btn-group\" style=\"display:none\">");
				content.append("<a class=\"btn btn-secondary\" href=\"").append(token_url).append("\" title=\"Token\">").append(token).append("</a>");
			} else {
				content.append("<div class=\"btn-group\">");
				content.append("<a class=\"btn btn-secondary\" href=\"").append(token_url).append("\" title=\"Token Apendo\">").append(token).append("</a>");
			}
			content.append("<a class=\"btn btn-default\" href=\"").append(token_url).append("\">"+Messages.get("lelang.geret_token")+"</a>");
			content.append("</div>");

		} else if (tahapAktif.isPemasukanBerakhir() && (penawaran_teknis == null || penawaran_harga == null)) {
			// diberikan notifikasi ketika jadwal pemasukan penawaran telah berakhir
			content.append("<div class=\"alert alert-danger\">"+Messages.get("lelang.maaf_anda_tidak_diizinkan_mengirim")+"</div>");
		}
		out.print(content.toString());

	}

	// Peserta kirim kualifikasi
	public static void _kirimKualifikasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Lelang_seleksi lelang = (Lelang_seleksi)args.get("arg");
		Dok_penawaran dok_kualifikasi = (Dok_penawaran) args.get("dok_kualifikasi");
		Peserta peserta = (Peserta)args.get("peserta");
		TahapNow tahapAktif = (TahapNow)args.get("tahapAktif");
		boolean allow_kualifikasi = lelang.isPrakualifikasi()? tahapAktif.isPemasukanDokPra():tahapAktif.isPemasukanPenawaran();
		if(lelang.getPemilihan().isLelangExpress() || !lelang.lls_status.isAktif()) {
			allow_kualifikasi = false; // lelang express Penyedia tidak bisa kirim kualifikasi karena sudah ditangani oleh SIKaP
		}
		if(lelang.isPrakualifikasi() && lelang.is_kualifikasi_tambahan && allow_kualifikasi){
			allow_kualifikasi = false;
		}
		String ket_kualifikasi = "";
		if (dok_kualifikasi != null){
			ket_kualifikasi = Messages.get("lelang.sudah_dikirim_pada")+" "+ FormatUtils.formatDateTimeInd(dok_kualifikasi.dok_tgljam);
		} else {
			Jadwal jadwal = null;
			if(lelang.isPrakualifikasi()) {
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_DOK_PRA);
			}else {
				jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_PENAWARAN);
			}
			if (jadwal != null) {
				String tglAwal = "";
				String tglAkhir = "";
				if (jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
					tglAwal = FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal);
					tglAkhir = FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir);
				}
				ket_kualifikasi = Messages.get("lelang.belum_dikirim_jadwal")+" " + tglAwal + " "+Messages.get("lelang.s_d")+" "+ tglAkhir;
			}
		}
    	StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
    	content.append("<div class=\"panel-heading\">"+Messages.get("lelang.data_kualifikasi")+"</div>");
    	content.append("<ul class=\"list-group\">");
    	content.append("<li class=\"list-group-item\">"+Messages.get("lelang.persyaratan_kualifikasi")+" <span class=\"badge\">Status: ").append(ket_kualifikasi).append("</span>");
    	if(dok_kualifikasi != null)
    		content.append("<a class=\"badge\" target=\"_blank\" href=\"").append(Router.reverse("lelang.KualifikasiCtr.preview").add("id", peserta.psr_id).url).append("\" width=\"900\">"+Messages.get("lelang.lihat_data")+"</a>");
    	if(allow_kualifikasi)
    		content.append("<a class=\"badge\" href=\"").append(Router.reverse("lelang.KualifikasiCtr.kirim").add("id", peserta.psr_id).url).append("\">"+Messages.get("lelang.kirim_data")+"</a>");
    	content.append("</li>");

		if (lelang.isPrakualifikasi()) {
			Dok_penawaran dok_susulan = Dok_penawaran.findPenawaranPeserta(peserta.psr_id,JenisDokPenawaran.PENAWARAN_SUSULAN);
			String ket_susulan = Messages.get("lelang.belum_dikirim_kirim_ulang");
			if (dok_susulan != null && dok_susulan.dok_disclaim == 1) {
				ket_susulan = Messages.get("lelang.sudah_dikirim_pada")+" "+ FormatUtils.formatDateTimeInd(dok_susulan.auditupdate);
			}
			int jumlahEmail = MailQueueDao.countPesanKualifikasi(lelang.lls_id, Active_user.current().rekananId);
			boolean allow_susulan = jumlahEmail != 0 && peserta.is_dikirim_pesan && (lelang.is_kualifikasi_tambahan || lelang.isEvaluasiUlang()) && lelang.lls_status.isAktif();
			if (allow_susulan) {
				content.append("<li class=\"list-group-item\">"+Messages.get("lelang.persyaratan_kualifikasi_tambahan")+" <span class=\"badge\">Status : ").append(ket_susulan).append("</span>");
				if(dok_susulan != null && dok_susulan.dok_disclaim == 1)
					content.append("<a class=\"badge \" target=\"_blank\" href=\"").append(Router.reverse("lelang.KualifikasiCtr.susulan").add("id", peserta.psr_id).url).append("\">"+Messages.get("lelang.lihat_data")+"</a>");
				if(tahapAktif.isPemasukanDokPra()) {
					content.append("<a class=\"badge dialog\" title=\""+Messages.get("lelang.kirim_persyaratan_kualifikasi_tambahan")+"\" href=\"")
						.append(Router.reverse("lelang.KualifikasiCtr.kirimSusulan").add("id", peserta.psr_id).url);
					content.append(dok_susulan == null || dok_susulan.dok_disclaim == 0 ? "\">"+Messages.get("lelang.kirim_data")+"</a>" : "\">Update Data</a>") ;
				}
				content.append("</li>");
			}
		}
		content.append("</ul>");
		content.append("</div>");
		out.print(content.toString());
	}

    // informasi Evaluasi disisi panitia
    public static void _informasiEvaluasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
    	Lelang_seleksi lelang = (Lelang_seleksi)args.get("arg");
    	TahapStarted tahapStarted = (TahapStarted)args.get("tahapStarted");
    	Peserta peserta = (Peserta) args.get("peserta");
    	boolean hasilEvaKualifikasi = Evaluasi.isShowHasilEvaluasiKualifikasi(lelang.lls_id);
    	Map<String, Object> param = new HashMap<String, Object>(1);
    	param.put("id", lelang.lls_id);
    	StringBuilder content = new StringBuilder();
    	if(tahapStarted.isPengumuman()) {
    		content.append("<a href=\"").append(Router.reverse("lelang.EvaluasiCtr.hasil", param).url).append("\" target=\"_blank\" class=\"badge pull-right\" width=\"700\" height=\"600\">"+Messages.get("lelang.hasil_evaluasi")+"</a>");
    	}
    	if(hasilEvaKualifikasi) {
    		int jmlLulusKualifikasi = Nilai_evaluasi.countLulus(lelang.lls_id, JenisEvaluasi.EVALUASI_KUALIFIKASI);
    		content.append("<div><b>"+Messages.get("lelang.evaluasi_kualifikasi")+"</b><br />"+Messages.get("lelang.lulus")+" <span class=\"badge\">").append(jmlLulusKualifikasi).append("</span> "+Messages.get("lelang.penyedia_barang_jasa")+"</div>");
    	}
    	if(tahapStarted.isEvaluasiTeknis()) {
    		int jmlLulusAdmTeknis = Nilai_evaluasi.countLulus(lelang.lls_id, JenisEvaluasi.EVALUASI_TEKNIS);
    		content.append("<div><b>"+Messages.get("lelang.evaluasi_penawaran_administrasi_teknis")+"</b><br />"+Messages.get("lelang.lulus")+" <span class=\"badge\">").append(jmlLulusAdmTeknis).append(" "+Messages.get("lelang.penyedia_barang_jasa")+"</span></div>");
    	}
    	if(tahapStarted.isSanggahAdm()) {
    		int jmlSanggahanAdm = 0;
    		if(Active_user.current().isRekanan()) {
				Jadwal jadwal_sanggah = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.SANGGAH_ADM_TEKNIS);
				jmlSanggahanAdm = Sanggahan.countSanggahan(Tahap.SANGGAH_ADM_TEKNIS, peserta.psr_id, jadwal_sanggah);
			} else
    			jmlSanggahanAdm = Sanggahan.countSanggahan(lelang.lls_id, Tahap.SANGGAH_ADM_TEKNIS);
    		content.append("<div><b>"+Messages.get("lelang.sanggahan_administrasi_teknis")+"</b><a href=\"").append(Router.reverse("lelang.SanggahanCtr.admTeknis", param).url).append("\">"+Messages.get("lelang.sanggahan")+" <span class=\"badge\">").append(jmlSanggahanAdm).append("</span></a></div>	");
    	}
    	if(tahapStarted.isEvaluasiHarga()) {
    		int jmlLulusBiaya = Nilai_evaluasi.countLulus(lelang.lls_id, JenisEvaluasi.EVALUASI_HARGA);
    		content.append("<div><b>"+Messages.get("lelang.evaluasi_penawaran_biaya_harga")+"</b><br />"+Messages.get("lelang.lulus")+": <span class=\"badge\">").append(jmlLulusBiaya).append(" "+Messages.get("lelang.penyedia_barang_jasa")+"</span></div>");
    	}
		if (tahapStarted.isPengumuman()) {
			content.append("<div class=\"highlight\"><b>"+Messages.get("tag.pengumuman_pemenang")+"</b></div>");
			Evaluasi evaluasi = Evaluasi.findPenetapanPemenang(lelang.lls_id);
			if(evaluasi != null && evaluasi.eva_status.isSelesai()) {
				List<Nilai_evaluasi> pemenangList = evaluasi.findPemenangList();
				if (!CommonUtil.isEmpty(pemenangList)) {
					content.append("<table class=\"table table-sm\">");
					for (Nilai_evaluasi pemenang : pemenangList) {
						content.append("<tr><td>"+Messages.get("tag.urutan")+" ").append(pemenang.nev_urutan).append(' ');
						if (pemenang.isLulus()){
							if (lelang.getPemilihan().isLelangExpress() && pemenang.nev_urutan == 1)
								content.append("<i class=\"fa fa-star\" style=\"color:#f0ad4e\"></i>");
							if (!lelang.getPemilihan().isLelangExpress())
								content.append("<i class=\"fa fa-star\" style=\"color:#f0ad4e\"></i>");
						}
						if (lelang.getPemilihan().isLelangExpress() && pemenang.getPeserta().isPemenangVerif()) {
							content.append("<i class=\"fa fa-star\" style=\"color:#f0ad4e\"></i>");
						}
						Sppbj sppbj = Sppbj.find("lls_id=?", lelang.lls_id).first();

						if (sppbj != null && pemenang.getPeserta().getRekanan().rkn_id.equals(sppbj.getRekanan().rkn_id)) {
							content.append("<i class=\"fa fa-star\" style=\"color:#f0ad4e\"></i>");
						}
						if (pemenang.isLulus()){
							if (lelang.getPemilihan().isLelangExpress() && pemenang.nev_urutan == 1)
								content.append("<br>* "+Messages.get("lelang.penawaran_terendah")+"");
							if (!lelang.getPemilihan().isLelangExpress()) {
								Integer pemenangKlarifikasi =  pemenang.getPeserta().is_pemenang_klarifikasi;
								if( pemenangKlarifikasi!= null && pemenangKlarifikasi.equals(Integer.valueOf(1)))
									content.append("<br>* "+Messages.get("lelang.pemenang_hasil_klarifikasi_negosiasi")+"");
								else
									content.append("<br>* "+Messages.get("lelang.pemenang_hasil_evaluasi")+"");
							}

						}

						if (lelang.getPemilihan().isLelangExpress() && pemenang.getPeserta().isPemenangVerif())
							content.append("<br>* "+Messages.get("lelang.pemenang_verifikasi_pokja")+"");

						if (sppbj != null && pemenang.getPeserta().getRekanan().rkn_id.equals(sppbj.getRekanan().rkn_id))
							content.append("<br>* "+Messages.get("lelang.pemenang_berkontrak")+" ");
						content.append("</td><td>").append(pemenang.getPeserta().getNamaPeserta()).append("</td></tr>");
					}
					content.append("</table>");
				}
			}
			 else {
				content.append(""+Messages.get("lelang.belum_ada_pemenang "));
			}
		}
		out.print(content.toString());
	}

	//token pembukaan penawaran
	public static void _tokenPembukaan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Lelang_seleksi lelang = (Lelang_seleksi) args.get("arg");
		int jenisId = (Integer)args.get("jenis");
		TahapStarted tahapStarted = (TahapStarted) args.get("tahapStarted");
		Logger.debug(Lt17Ctr.ROOT_URL_PENAWARAN);
		JenisDokPenawaran jenis = JenisDokPenawaran.fromValue(jenisId);
		String token = Lelang_token.findNGeneratePegawai(Active_user.current().pegawaiId, lelang.lls_id, Request.current().host, jenis);
		String root_url = BasicCtr.getRequestHost()+Lt17Ctr.ROOT_URL_PENAWARAN;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("root_url",root_url);
		params.put("access_token", token);
		String json = Json.toJson(params);
		String token_url = root_url+ '/' +Base64.encodeBase64String(json.getBytes());
		StringBuilder content = new StringBuilder();
//    	content.append("<div class=\"card card-info\">");
//    	content.append("<div class=\"card-header\">Token Pembukaan Penawaran</div>");
//    	content.append("<div class=\"card-body\">");
		boolean allow_pembukaan = !lelang.isLelangV3() && (
				(tahapStarted.isPembukaan() && jenis.isAdmTeknisHarga())
						|| (tahapStarted.isPembukaanTeknis() && jenis.isAdmTeknis())
						|| (tahapStarted.isPembukaanHarga() && jenis.isHarga())
		);
		if(allow_pembukaan) {
			boolean isOSD = ConfigurationDao.isOSD(lelang.getPaket().pkt_tgl_buat);
			if(isOSD) {
				content.append("<div class=\"btn-group\" style=\"display:none\">");
			} else {
				content.append("<div><b>"+jenis.label+"</b></div>");
				content.append("<div class=\"btn-group\">");
			}
	    	content.append("<a class=\"btn btn-default disable\" href=\"").append(token_url).append("\" title=\""+Messages.get("lelang.token_pembukaan")+"\">").append(token).append("</a>");
			content.append("<a class=\"btn btn-default disable\" href=\"").append(token_url).append("\">"+Messages.get("lelang.lelang.geret_token")+"</a>");
			content.append("</div>");
			if(isOSD) {
				content.append("<div class=\"bs-callout bs-callout-info\">"+Messages.get("lelang.pembukaan_penawaran_amanda")+" ").append(Lt17Ctr.SPAMKODOK_VERSI).append("</div>");
			} else {
				content.append("<div class=\"bs-callout bs-callout-info\">"+Messages.get("lelang.pembukaan_penawaran_apendo")+"").append(Lt17Ctr.versiApendoBuildSistem).append(" "+Messages.get("lelang.token_diatas")+"</div>");
			}
		} else {
    		content.append("<div class=\"alert alert-danger\">"+Messages.get("lelang.pembukaan_penawaran_tidak_diperbolehkan")+" </div>");
    	}
	    out.print(content.toString());
    }

	public static void _dokumenKontrak(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Long lelangId = (Long)args.get("arg");
		Kontrak kontrakx = Kontrak.find("lls_id=? ", lelangId).first();
		StringBuilder content = new StringBuilder();
		if (kontrakx != null) {
			BlobTable blob = null;
			content.append("<table class=\"table table-striped\"><thead><tr><th>"+Messages.get("lelang.nama_doc")+"</th><th>"+Messages.get("lelang.dokumen")+"</th></tr>");
			content.append("<tbody>");
			if(kontrakx.kontrak_id_attacment2 != null) {
				blob = kontrakx.getBlob2();
				content.append("<tr><td>"+Messages.get("lelang.surat_perjanjian")+" </td>");
				content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakHandler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
				content.append("</tr>");
			}
			if(kontrakx.kontrak_sskk_attacment != null) {
				blob = kontrakx.getBlobSskk();
				content.append("<tr><td>SSKK </td>");
				content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakHandler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
				content.append("</tr>");
			}
			Pesanan pesananx = Pesanan.find("kontrak_id=?",kontrakx.kontrak_id).first();
			if(pesananx != null && pesananx.pes_attachment != null) {
				blob = pesananx.getBlob();
				content.append("<tr><td>SPMK / "+Messages.get("lelang.surat_pesanan")+"</td>");
				content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakHandler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
				content.append("</tr>");
			}
			List<BA_Pembayaran> listBap = BA_Pembayaran.find("kontrak_id=? order by bap_id asc", kontrakx.kontrak_id).fetch();
			if(!CommonUtil.isEmpty(listBap)) {
				int i = 1;
				for(BA_Pembayaran bapx : listBap) {
					if(bapx.cetak_bap_attachment != null) {
						blob = bapx.getBlobBap();
						if(blob != null) {
							content.append("<tr><td>Pembayaran ").append(i).append(" </td>");
							content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakHandler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
							content.append("</tr>");
						}
					}
					if(bapx.cetak_bast_attachment != null) {
						blob = bapx.getBlobBast();
						if(blob != null) {
							content.append("<tr><td>"+Messages.get("lelang.pembayaran")+" ").append(i).append(" </td>");
							content.append("<td><a href=\"").append(blob.getDownloadUrl(DokumenKontrakHandler.class)).append("\">").append(blob.getFileName()).append("</a></td>");
							content.append("</tr>");
						}
					}
					i++;
				}
			}
			content.append("</tbody></table>");
		}
		out.print(content.toString());
	}

	public static void _persetujuanList(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Long lelangId = (Long)args.get("arg");
		boolean pemenang = (Boolean)args.get("pemenang");
		String jenisKey = (String)args.get("jenisKey");

		Active_user active_user = Active_user.current();
		List<PersetujuanLelang> persetujuanList = null;
		JenisPersetujuan jenis = null;

		if(jenisKey != null){
			jenis = JenisPersetujuan.getByKey(jenisKey);
			persetujuanList = PersetujuanLelang.findList(lelangId, jenis);
		}else{
			if(pemenang)  {
				jenis = JenisPersetujuan.PEMENANG_LELANG;
			}else{
				jenis = JenisPersetujuan.PENGUMUMAN_LELANG;
			}
			persetujuanList = PersetujuanLelang.findList(lelangId, jenis);
		}

		Persetujuan persetujuan = Persetujuan.find("lls_id=? and peg_id=?", lelangId, active_user.pegawaiId).first();
		persetujuan = persetujuan != null ? persetujuan : new Persetujuan();

    	StringBuilder content = new StringBuilder();
    	content.append("<table class=\"table table-condensed\">");
		content.append("<tr><th width=\"350\">"+Messages.get("lelang.anggota_panitia_pokja")+"</th><th>Status</th><th>"+Messages.get("lelang.tanggal")+"</th><th>"+Messages.get("lelang.alasan_tidak_setuju")+"</th></tr>");
    	if(!CommonUtil.isEmpty(persetujuanList)) {
    		for(PersetujuanLelang item:persetujuanList) {
			    Map<String, Object> param = new HashMap<String, Object>(1);
			    param.put("pst_id", item.pst_id);
    			content.append("<tr><td>");
			    content.append("<a href=\"").append(Router.reverse("lelang.PersetujuanCtr.riwayatPersetujuan", param).url).append("\" class=\"dialog\" title=\""+Messages.get("ct.rp")+"\">");
			    content.append(persetujuan.pst_id == item.pst_id? "Anda" : item.peg_nama);
			    content.append("</a></td>");
    			if(item.pst_status.isSetuju())
    				content.append("<td>").append("<i class=\"fa fa-check\"></i>").append("</td>");
    			else if (item.pst_status.isTidakSetuju())
    				content.append("<td>").append("<i class=\"fa fa-times\"></i>").append("</td>");
    			else
    				content.append("<td></td>");
    			content.append("<td>").append(FormatUtils.formatDateTimeInd(item.pst_tgl_setuju)).append("</td>");
				content.append("<td>").append(JavaExtensions.raw(item.getAlasan())).append("</td>");
				content.append("</tr>");
			}
		}
		content.append("</table>");
		out.print(content.toString());
	}

	public static void _dokDukunganBank(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		BlobTable blob = (BlobTable)args.get("arg");
		if(blob == null)
			return;
		try {
			out.println(new StringBuilder("<a href=\"")
					.append(blob.getDownloadUrl(DokDukunganBankDownloadHandler.class))
					.append("\"><span class=\"fa fa-download\"></span> ").append(blob.blb_nama_file).append("</a> - ").append(FormatUtils.formatBytes(blob.getFile().length())));
		}catch (Exception e) {
			e.printStackTrace();
			Logger.error("downloadSecurityHandler wajib diisi");
		}
	}

	public static void _dokPenawaranLain(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		BlobTable blob = (BlobTable)args.get("arg");
		if(blob == null)
			return;
		try {
			out.println(new StringBuilder("<a href=\"")
					.append(blob.getDownloadUrl(DokPenawaranLainDownloadHandler.class))
					.append("\"><span class=\"fa fa-download\"></span> ").append(blob.blb_nama_file).append("</a> - ").append(FormatUtils.formatBytes(blob.getFile().length())));
		}catch (Exception e) {
			Logger.error(e, "downloadSecurityHandler wajib diisi");
		}
	}

	public static void _penawaranPeserta(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Lelang_seleksi lelang = (Lelang_seleksi) args.get("arg");
		boolean hide = (Boolean) args.get("hide");
		Active_user active_user = Active_user.current();
		out.print("<table class=\"table table-striped table-condensed\">");
		out.print("<tr><th width=\"20\">#</th><th>"+Messages.get("lelang.nama_penyedia_barang_jasa")+"</th><th width=\"120\">"+Messages.get("lelang.tgl_mendaftar")+"</th>");
		if(!lelang.getPemilihan().isLelangExpress())
			out.print("<th width=\"150\" class=\"text-center\">"+Messages.get("lelang.dokumen_kualifikasi")+"</th>");
		out.print("<th "+(!lelang.isLelangV3() ? "colspan=\"4\"":"")+" class=\"text-center\">"+Messages.get("lelang.dokumen_penawaran")+"</th>");
		if(!lelang.isLelangV3()) {
			out.print("<th "+(!lelang.getPemilihan().isLelangExpress() ? "colspan=\"2\"":"")+" class=\"text-center\">"+Messages.get("lelang.status_penawaran")+"</th>");
		}
		out.print("</tr>");
		if(!lelang.isLelangV3()) {
			out.print("<tr>");
			out.print("<th></th><th></th><th></th>");
			if(!lelang.getPemilihan().isLelangExpress()) {
				out.print("<th></th>");
			}
			out.print("<th width=\"100\" class=\"text-center\">"+Messages.get("lelang.status_penawaran")+"</th>");
			if(!lelang.getPemilihan().isLelangExpress()) {
				out.print("<th width=\"120\" class=\"text-center\">"+Messages.get("lelang.administrasi_teknis")+"</th>");
			}
			out.print("<th width=\"100\" class=\"text-center\">"+Messages.get("lelang.harga")+"</th>");
			out.print("<th width=\"100\" class=\"text-center\">"+Messages.get("lelang.masa_berlaku")+"</th>");
			if(!lelang.getPemilihan().isLelangExpress())
				out.print("<th width=\"100\" class=\"text-center\">"+Messages.get("lelang.administrasi_teknis")+"</th>");
			out.print("<th width=\"100\" class=\"text-center\">"+Messages.get("lelang.harga")+"</th>");
			out.print("<tr>");
		}
//		List<Peserta> list = lelang.getPesertaList();
		List<Dok_penawaran_peserta> list = Dok_penawaran_peserta.findByLelang(lelang.lls_id);
		if (!CollectionUtils.isEmpty(list)) {
			int idx = 1;
			boolean kualifikasiPermission = lelang.isKualifikasiPermission();
			Map<String, Object> param = new HashMap<String, Object>(1);
			TahapStarted tahapStarted = lelang.getTahapStarted();
			for (Dok_penawaran_peserta peserta : list) {
				param.put("id", peserta.psr_id);
				out.print("<tr>");
				out.print("<td align=\"right\">"+idx+"</td>");
				if(hide)
					out.print("<td>"+Messages.get("lelang.peserta")+" "+idx);
				else {
					out.print("<td>"+peserta.getNamaPeserta());
		    		if(active_user.isAuditor()) {
		    			out.print("<a href=\""+Router.reverse("AuditorCtr.audit_log", param).url+"\" class=\"btn btn-success jpopup\" width=\"600\" height=\"400\" style=\"margin-top:3px;\" title=\""+Messages.get("lelang.log_akses")+"\"><i class=\"fa fa-edit\"></i></a>");
		    		}
		    		out.print("</td>");
	    		}
				out.print("<td>"+StringFormat.formatDate(peserta.psr_tanggal)+"</td>");
				if(!lelang.getPemilihan().isLelangExpress()) {
					out.print("<td class=\"text-center\">");
					if(peserta.dok_kualifikasi.dok_id != null && !kualifikasiPermission)
						out.print("<i class=\"fa fa-check\"></i>");
					else if(dok_kualifikasi != null && dok_kualifikasi.isPermission() && kualifikasiPermission)
						out.print("<a href=\""+Router.reverse("lelang.KualifikasiCtr.preview", param).url+"\" class=\"badge\" target=\"_blank\" width=\"800\" height=\"600\"> "+Messages.get("lelang.kualifikasi")+"</a>");
					out.print("<br />");
					if(dok_tambahan != null && kualifikasiPermission)
						out.print("<a href=\""+dok_tambahan.getDokUrl()+"\">[ "+Messages.get("lelang.tambahan")+" <i class=\"fa fa-download\"></i> ]</a>");
					out.print("<br />");
					if(dok_susulan != null && kualifikasiPermission)
						out.print("<a href=\""+Router.reverse("lelang.KualifikasiCtr.susulan", param).url+"\" class=\"badge\" target=\"_blank\" width=\"800\" height=\"600\"> "+Messages.get("lelang.kualifikasi_tambahan")+" </a>");
					out.print("</td>");
				}
				if(lelang.isLelangV3()) {
					out.print("<td>");
					if(active_user.isPanitia() || active_user.isAuditor()) {
						boolean permission_penawaran = false;
						Lelang_key key=null, adm_key=null, harga_key=null;
						if(lelang.isSatuFile()) {
							key = Lelang_key.findBy(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA, lelang.lls_id);
							permission_penawaran = isFilePermission(lelang, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA);
						}else {
							boolean adm_teknis_permission = isFilePermission(lelang, JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI);
							boolean harga_permission = isFilePermission(lelang, JenisDokPenawaran.PENAWARAN_HARGA);
							permission_penawaran = adm_teknis_permission || harga_permission;
							adm_key = Lelang_key.findBy(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI, lelang.lls_id);
							harga_key = Lelang_key.findBy(JenisDokPenawaran.PENAWARAN_HARGA, lelang.lls_id);
						}

						for(Dok_penawaran penawaran:peserta.findPenawaranList()) {
							if(penawaran != null && penawaran.isAllowed()) {
								blob = penawaran.getDokumen();
								out.print("<div class=\"panel panel-info\">");
								out.print("<div class=\"panel-heading\">"+penawaran.dok_jenis.label+"</div>");
								out.print("<div class=\"panel-body\">");
								out.print("<dl class=\"dl-horizontal\">");
								out.print("<dt>File</dt><dd>");
								if(blob != null && permission_penawaran) {
									out.print("<a href=\""+blob.getDownloadUrl(DokLelangDownloadHandler.class)+"\" title=\""+FormatUtils.formatFileSize(blob.blb_ukuran)+"\"><i class=\"fa fa-download\"></i>"+blob.getFileName()+"</a>");
									out.print("<a href=\""+Router.reverse("AuditorCtr.audit_log_dokumen").add("id", blob.blb_id_content).add("versi", blob.blb_versi).url+"\" class=\"jpopup\" width=\"600\" height=\"400\" style=\"margin-top:3px;\" title=\""+Messages.get("lelang.detil_informasi_dokumen")+"\" >");
									out.print("<i class=\"fa fa-edit\"></i></a>");
								}
								out.print("</dd>");
								out.print("<dt>Hash</dt><dd><b>"+blob.blb_hash+"</b></dd>");
								out.print("<dt>"+Messages.get("lelang.tanggal_diterima_server")+"</dt><dd>"+penawaran.getTglFile()+"</dd>");
								out.print("</dl>");
								if(permission_penawaran) {
									out.print("<b> "+Messages.get("lelang.kunci_private_dokumen")+"</b>");
									if(penawaran.dok_jenis.isAdmTeknisHarga()) {
										String privateKey = Base64.encodeBase64String(KeyUtilities.decryptKey(key.private_key));
										out.print("<pre>"+StringFormat.splitToCol(privateKey, 60)+"</pre>");
									}
									if(penawaran.dok_jenis.isAdmTeknis()) {
										String privateKey = Base64.encodeBase64String(KeyUtilities.decryptKey(adm_key.private_key));
										out.print("<pre>"+StringFormat.splitToCol(privateKey, 60)+"</pre>");
									}
									if(penawaran.dok_jenis.isHarga()) {
										String privateKey = Base64.encodeBase64String(KeyUtilities.decryptKey(harga_key.private_key));
										out.print("<pre>"+StringFormat.splitToCol(privateKey, 60)+"</pre>");
									}
								}
								out.print("</div>");
								out.print("</div>");
							}
							out.print("</dd>");
							out.print("<dt>Hash</dt><dd><b>"+blob.blb_hash+"</b></dd>");
							out.print("<dt>Tanggal Diterima Server</dt><dd>"+peserta.dok_teknis.getTglFile()+"</dd>");
							out.print("</dl>");
							String privateKey = Base64.encodeBase64String(KeyUtilities.decryptKey(adm_key.private_key));
							out.print("<pre>"+StringFormat.splitToCol(privateKey, 60)+"</pre>");
							out.print("</div>");
							out.print("</div>");
						}
						if(peserta.dok_harga.dok_id != null && peserta.dok_harga.isAllowed()) {
							blob = peserta.dok_harga.getDokumen();
							out.print("<div class=\"card card-info\">");
							out.print("<div class=\"card-header\">"+peserta.dok_harga.dok_jenis.label+"</div>");
							out.print("<div class=\"card-body\">");
							out.print("<dl class=\"dl-horizontal\">");
							out.print("<dt>File</dt><dd>");
							if(blob != null && permission_penawaran) {
								out.print("<a href=\""+blob.getDownloadUrl(DokLelangDownloadHandler.class)+"\" title=\""+FormatUtils.formatFileSize(blob.blb_ukuran)+"\"><i class=\"fa fa-download\"></i>"+blob.getFileName()+"</a>");
								out.print("<a href=\""+Router.reverse("AuditorCtr.audit_log_dokumen").add("id", blob.blb_id_content).add("versi", blob.blb_versi).url+"\" class=\"jpopup\" width=\"600\" height=\"400\" style=\"margin-top:3px;\" title=\"detil informasi dokumen\" >");
								out.print("<i class=\"fa fa-edit\"></i></a>");
							}
							out.print("</dd>");
							out.print("<dt>Hash</dt><dd><b>"+blob.blb_hash+"</b></dd>");
							out.print("<dt>Tanggal Diterima Server</dt><dd>"+peserta.dok_harga.getTglFile()+"</dd>");
							out.print("</dl>");
							String privateKey = Base64.encodeBase64String(KeyUtilities.decryptKey(harga_key.private_key));
							out.print("<pre>"+StringFormat.splitToCol(privateKey, 60)+"</pre>");
							out.print("</div>");
							out.print("</div>");
						}
						if(peserta.dok_teknis_harga.dok_id != null && peserta.dok_teknis_harga.isAllowed()) {
							blob = peserta.dok_teknis_harga.getDokumen();
							out.print("<div class=\"card card-info\">");
							out.print("<div class=\"card-header\">"+peserta.dok_teknis_harga.dok_jenis.label+"</div>");
							out.print("<div class=\"card-body\">");
							out.print("<dl class=\"dl-horizontal\">");
							out.print("<dt>File</dt><dd>");
							if(blob != null && permission_penawaran) {
								out.print("<a href=\""+blob.getDownloadUrl(DokLelangDownloadHandler.class)+"\" title=\""+FormatUtils.formatFileSize(blob.blb_ukuran)+"\"><i class=\"fa fa-download\"></i>"+blob.getFileName()+"</a>");
								out.print("<a href=\""+Router.reverse("AuditorCtr.audit_log_dokumen").add("id", blob.blb_id_content).add("versi", blob.blb_versi).url+"\" class=\"jpopup\" width=\"600\" height=\"400\" style=\"margin-top:3px;\" title=\"detil informasi dokumen\" >");
								out.print("<i class=\"fa fa-edit\"></i></a>");
							}
							out.print("</dd>");
							out.print("<dt>Hash</dt><dd><b>"+blob.blb_hash+"</b></dd>");
							out.print("<dt>Tanggal Diterima Server</dt><dd>"+peserta.dok_teknis_harga.getTglFile()+"</dd>");
							out.print("</dl>");
							String privateKey = Base64.encodeBase64String(KeyUtilities.decryptKey(key.private_key));
							out.print("<pre>"+StringFormat.splitToCol(privateKey, 60)+"</pre>");
							out.print("</div>");
							out.print("</div>");

						}
					}
					out.print("</td>");
				}
				else if(tahapStarted.isPembukaan()){
					if(lelang.getPemilihan().isLelangExpress())
						out.print("<td class=\"text-center\">"+(dok_harga != null ? "<a href=\""+Router.reverse("lelang.PesertaCtr.cetakSuratPenawaran", param).url+"\" class=\"badge\">"+Messages.get("lelang.cetak")+"</a>":"")+"</td>");
					else
						out.print("<td class=\"text-center\">"+(dok_teknis != null ? "<a href=\""+Router.reverse("lelang.PesertaCtr.cetakSuratPenawaran", param).url+"\" class=\"badge\">"+Messages.get("lelang.cetak")+"</a>":"")+"</td>");

					if(!lelang.getPemilihan().isLelangExpress() &&  lelang.sudahPembukaanPenawaran())
						out.print("<td class=\"text-center\">"+(dok_teknis != null ? "<a href=\""+Router.reverse("lelang.PesertaCtr.rincian_adminteknis", param).url+"\" class=\"badge\" target=\"_blank\">"+Messages.get("lelang.detil")+"</a>":"")+"</td>");
					else
						out.print("<td class=\"text-center\"></td>");
					
					if(dok_harga != null && ((!lelang.getMetode().dokumen.isSatuFile() && peserta.isLulusTeknis() && tahapStarted.isPembukaanHarga())|| (tahapStarted.isPembukaan() && lelang.getMetode().dokumen.isSatuFile())))
						out.print("<td class=\"text-center\"><a href=\""+Router.reverse("lelang.PesertaCtr.rincian_penawaran", param).url+"\" class=\"badge\" target=\"_blank\">"+Messages.get("lelang.detil")+"</a></td>");
					else
						out.print("<td class=\"text-center\"></td>");

					out.print("<td class=\"text-center\">");
					if(lelang.getPemilihan().isLelangExpress() && dok_harga != null)
						out.print(dok_harga.dok_waktu);
					else if(dok_teknis != null)
						out.print(dok_teknis.dok_waktu+" "+Messages.get("lelang.hari")+"");
					out.print("</td>");

					if(!lelang.getPemilihan().isLelangExpress())
						out.print("<td class=\"text-center\">"+(peserta.dok_teknis.dok_id != null ? peserta.dok_teknis.getInfoKirim():"Belum dikirim")+"</td>");
					out.print("<td class=\"text-center\">"+(peserta.dok_harga.dok_id != null ? peserta.dok_harga.getInfoKirim():"Belum dikirim")+"</td>");
				} else {
					out.println("<td class=\"text-center\"></td><td class=\"text-center\"></td><td class=\"text-center\"></td><td class=\"text-center\"></td><td class=\"text-center\"></td><td class=\"text-center\"></td>");
				}
				out.print("</tr>");
				idx++;
			}
		}
		out.print("</table>");
	}

	// checking file permission tender v3
	static boolean isFilePermission(Lelang_seleksi lelang, JenisDokPenawaran jenisDokPenawaran) {
		boolean isPermission = false;
		String aktivitas="akt_id in (select akt_id from aktivitas where akt_jenis in ";
		if (lelang.isSatuFile()) {
			aktivitas += "('"+Tahap.PEMBUKAAN_PENAWARAN+"','"+Tahap.EVALUASI_PENAWARAN_ADM_TEKNIS+"','"+Tahap.EVALUASI_PENAWARAN_BIAYA+"')";
		} else if (jenisDokPenawaran == JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI) {
			aktivitas += "('"+Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS+"','"+Tahap.EVALUASI_PENAWARAN_ADM_TEKNIS+"')";
		} else if (jenisDokPenawaran == JenisDokPenawaran.PENAWARAN_HARGA) {
			// check jadwal PENETAPAN PEMENANG ADMIN-TEKNIS
			Jadwal jadwalPenetapanTeknis = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PENETAPAN_PEMENANG_ADM_TEKNIS);
			if(jadwalPenetapanTeknis != null){
				Evaluasi evaluasiTeknis = Evaluasi.findTeknis(lelang.lls_id);
				if(evaluasiTeknis != null)
					isPermission = evaluasiTeknis.eva_status.isSelesai(); // cek apakah sudah ditetapkan oleh ketua panitia atau belum
			}
			aktivitas += "('"+Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA+"','"+Tahap.EVALUASI_PENAWARAN_BIAYA+"')";
		}
		aktivitas += ")";
		Long count = Jadwal.count("lls_id=? and dtj_tglawal <= ? and  "+aktivitas, lelang.lls_id, controllers.BasicCtr.newDate());
		if(count > 0){
			long jumlahPenawarKualifikasi = lelang.jumlahPenawarKualifikasi();
			long jumlahPenawar = lelang.getJumlahPenawar();
			boolean enableDownload = ConfigurationDao.isEnableDownload(lelang.lls_id);
			if(lelang.isPrakualifikasi() && !lelang.getPemilihan().isLelangTerbatas())
				isPermission = (jumlahPenawarKualifikasi>=3 && jumlahPenawar>=1) && enableDownload;
			else
				isPermission = (jumlahPenawar >=1 && jumlahPenawarKualifikasi>=1) && enableDownload;
			return isPermission || lelang.isLelangUlang();
		}
		return false;
	}


	// table hasil evaluasi
	public static void _hasilEvaluasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Lelang_seleksi lelang = (Lelang_seleksi) args.get("arg");
		MetodeEvaluasi metodeEvaluasi = lelang.getEvaluasi();
		out.print("<table class=\"table table-sm table-hover\">");
		out.print("<tr>");
		out.print("<th>"+Messages.get("lelang.no")+"</th>");
		out.print("<th>"+Messages.get("lelang.nama_peserta")+"</th>");
		out.print("<th>"+Messages.get("lelang.harga_penawaran")+"</th>");
		out.print("<th>"+Messages.get("lelang.harga_terkoreksi")+"</th>");
		out.print("<th>"+(lelang.isAuction()  ? Messages.get("lelang.harga_reverse_auction") : Messages.get("lelang.harga_negosiasi"))+"</th>");
		out.print("<th width=\"20\"><span class=\"label label-info\">A</span></th>");
		out.print("<th width=\"20\"><span class=\"label label-danger\">K</span></th>");
		if(lelang.isPrakualifikasi() && (lelang.isKonsultansi() || lelang.isJkKonstruksi()))
			out.print("<th width=\"20\"><span class=\"badge  badge-danger\">SK</span></th>");
		out.print("<th width=\"20\"><span class=\"badge  badge-info\">T</span></th>");
		if(metodeEvaluasi.isNilai() || metodeEvaluasi.isPaguAnggaran() || metodeEvaluasi.isBiayaTerendah() || metodeEvaluasi.isKualitas())
			out.print("<th width=\"20\"><span class=\"label label-info\">"+Messages.get("lelang.skor")+"</span></th>");
		out.print("<th width=\"20\"><span class=\"label label-success\">H</span></th>");
		if(lelang.getEvaluasi().isNilai())
			out.print("<th width=\"20\"><span class=\"label label-success\">"+Messages.get("lelang.skor")+"</span></th>");
		if(!lelang.isLelangV3())
			out.print("<th width=\"20\"><span class=\"badge  badge-danger\">B</span></th>");
		out.print("<th width=\"20\"><span class=\"badge  badge-warning\">P</span></th>");

		TahapNow tahapNow = lelang.getTahapNow();
		if(lelang.isLelangV43() && tahapNow.isPembuktian()){
			out.print("<th class=\"text-center\" >"+Messages.get("lelang.verifikasi")+"</th>");
		}

		List<HasilEvaluasi> list = HasilEvaluasi.findByLelang(lelang.lls_id);
		if(!CollectionUtils.isEmpty(list)) {
			String currentTab = lelang.isPrakualifikasi() ? "#kualifikasi":"#administrasi"; // currentTab di detail evaluasi
			for(int indeks = 0; indeks <  list.size() ; indeks++) {
				HasilEvaluasi peserta = list.get(indeks);
				if(peserta == null)
					continue;
				out.print("<tr>");
				out.print("<td>"+(indeks + 1)+"</td>");
				out.print("<td><a href=\""+Router.reverse("lelang.EvaluasiCtr.detail").add("id", peserta.psr_id).url+currentTab+"\" value=\""+peserta.psr_id+"\">"+peserta.rkn_nama+"</a></td>");
				out.print("<td>" + (peserta.fileHarga != null ? peserta.getPenawaran() + " "+peserta.getNotifHargaPenawaran() : Messages.get("lelang.tidak_ada_penawaran")) + "</td>");
				out.print("<td>" + (peserta.fileHarga != null ? "<div>" + peserta.getPenawaranTerkoreksi() +" "+peserta.getNotifHargaPenawaranTerkoreksi()+"</div>" : Messages.get("lelang.tidak_ada_penawaran")) + "</td>");
				out.print("<td>" + (peserta.nev_harga_negosiasi != null ? "<div>" + peserta.getHargaNegosiasi() +" "+peserta.getNotifHargaNegosiasi()+"</div>" : "") + "</td>");
				out.print("<td class=\"text-center\">"+StringFormat.statuslulusIcon(peserta.administrasi)+"</td>");
				out.print("<td class=\"text-center\">"+StringFormat.statuslulusIcon(peserta.kualifikasi)+"</td>");
				if(lelang.isPrakualifikasi() && (lelang.isKonsultansi() || lelang.isJkKonstruksi()))
					out.print("<td>"+peserta.getSkorKualifikasi()+"</td>");
				out.print("<td class=\"text-center\">"+StringFormat.statuslulusIcon(peserta.teknis)+"</td>");
				if(metodeEvaluasi.isNilai() || metodeEvaluasi.isPaguAnggaran() || metodeEvaluasi.isBiayaTerendah() || metodeEvaluasi.isKualitas())
					out.print("<td>"+(peserta.skor_teknis != null? peserta.skor_teknis:0)+"</td>");
				out.print("<td class=\"text-center\">"+StringFormat.statuslulusIcon(peserta.harga)+"</td>");
				if(lelang.getEvaluasi().isNilai())
					out.print("<td>"+(peserta.skor_harga != null? peserta.skor_harga:0)+"</td>");
				if(!lelang.isLelangV3())
					out.print("<td class=\"text-center\">"+StringFormat.statuslulusIcon(peserta.pembuktian)+"</td>");
				out.print("<td class=\"text-center\">"+(peserta.isPemenang()?"<i class=\"fa fa-star\" style=\"color:#f0ad4e\"/>":"")+"</td>");
				if(lelang.isLelangV43() && tahapNow.isPembuktian()){
					out.print("<td class=\"text-center\">");
					String urlVerifikasi = Router.reverse("lelang.EvaluasiCtr.verifikasi_sikap").add("id", peserta.psr_id).url;
					out.print("<a href=\""+urlVerifikasi+"\" class=\"btn btn-warning btn-sm\" target=\"_blank\">Verifikasi</a>");
					out.print("<a href=\"https://zoom.us/signin\" class=\"btn btn-primary btn-sm\" target=\"_blank\"><i class=\"fa fa-video-camera\"></i> VC</a>");
					out.print("</td>");
				}
				out.print("</tr>");
			}
		}
		out.print("</table>");
	}

	public static void _cetakUndangan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Peserta peserta = (Peserta) args.get("arg");
		StringBuilder content = new StringBuilder();

		MailQueue mailPembuktianKualifikasi = MailQueueDao
				.getByJenisAndLelangAndRekanan(peserta.lls_id, peserta.rkn_id,JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);


		MailQueue mailKontrak =  MailQueueDao
				.getByJenisAndLelangAndRekanan(peserta.lls_id, peserta.rkn_id,JenisEmail.UNDANGAN_KONTRAK);

		Sppbj sppbj = Sppbj.findByLelang(peserta.lls_id);

		if (mailPembuktianKualifikasi != null){

			Map<String, Object> param = new HashMap<String, Object>(1);
			param.put("id", peserta.lls_id);
			param.put("rkn_id", peserta.rkn_id);
			param.put("jenis", JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI);

			content.append("<a href=\"").append(Router.reverse("admin.UtilityCtr.viewUdanganPdf", param).url).append("\" class=\"badge\">"+Messages.get("lelang.cetak_undangan_pembuktian_kualifikasi")+"</a>&nbsp;");

		}


		if (sppbj != null && mailKontrak != null){

			if (sppbj.rkn_id == peserta.rkn_id){

				Map<String, Object> param = new HashMap<String, Object>(1);
				param.put("id", peserta.lls_id);
				param.put("rkn_id", peserta.rkn_id);
				param.put("jenis", JenisEmail.UNDANGAN_KONTRAK);

				content.append("<a href=\"").append(Router.reverse("admin.UtilityCtr.viewUdanganPdf", param).url).append("\" class=\"badge\">"+Messages.get("lelang.cetak_undangan_kontrak")+"</a>&nbsp;");

			}

		}

		out.print(content.toString());
	}

	public static void _certificateInfo(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		boolean isOSD = ConfigurationDao.isOSD(BasicCtr.newDate());
		if(isOSD){
			Lelang_seleksi lelang = (Lelang_seleksi) args.get("arg");
			List<Anggota_panitia> anggota_panitia = Anggota_panitia.find("pnt_id=?", lelang.getPaket().pnt_id).fetch();
			List<String> infoPanitia = new ArrayList<>();
			boolean canCreate = true;
			LocalDateTime earliestExpired = null;
			for(Anggota_panitia anggota:anggota_panitia){
				boolean validCert = false;
				String info = Messages.get("flash.tidak_memiliki_sertifikat_aktif");
				Pegawai pegawai = Pegawai.findById(anggota.peg_id);
				if (pegawai.cer_id != null){
					Certificate certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id, JenisCertificate.CERT_PANITIA);

					if(certificate!=null && certificate.cer_sn!=null){
						if(certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
							if (!certificate.isCertExpired()) {
								LocalDateTime tmpTime =  LocalDateTime.ofInstant(certificate.getExpiredDate().toInstant(), ZoneId.systemDefault());
								if(earliestExpired == null || tmpTime.isBefore(earliestExpired)) {
									earliestExpired = tmpTime;
								}
								validCert = true;
							}
						}
					}
					if(certificate==null){
						info = Messages.get("flash.tidak_memiliki_sertifikat_aktif");
					}
				}
				canCreate = canCreate && validCert;
				if(!validCert) {
					infoPanitia.add(pegawai.peg_nama + " <strong>(" + info + ")</strong>");
				}
			}
			StringBuilder sb = new StringBuilder();
			if(canCreate && earliestExpired != null) {
				sb.append("<p>")
					.append(Messages.get("flash.batas_akhir_jadwal_pembukaan_penawaran")+" ").append("<strong>").append(earliestExpired.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm"))).append("</strong>")
					.append("</p>");
			} else {
				sb.append("<ul>");
				for (String info:infoPanitia) {
					sb.append("<li>").append(info).append("</li>");
				}
				sb.append("</ul>");
			}
			out.println(sb.toString());
		}
	}

	public static void _dokTenderPokja(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		List<BlobTable> list = (List<BlobTable>) args.get("dok");
		String label = (String) args.get("arg");
		if(StringUtils.isEmpty(label))
			return;
		StringBuilder content = new StringBuilder();
		content.append("<div class=\"card card-info\">");
		content.append("<div class=\"card-header\">").append(Messages.get(label)).append("</div>");
		if (!CollectionUtils.isEmpty(list)) {
			content.append("<div class=\"list-group\">");
			for (BlobTable blob : list) {
				content.append("<a href=\"").append(blob.getDownloadUrl(DokLelangDownloadHandler.class))
						.append("\" class=\"list-group-item\"><i class=\"fa fa-download\"></i> ")
						.append(blob.getFileName()).append("<abbr title=\"Tanggal Kirim\" class=\"initialism\"> (")
						.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
			}
			content.append("</div>");
		}
		content.append("</div>");
		out.print(content.toString());
	}


	/**
	 * Daftar Kuantitas dan Harga (dari Peserta maupun pokja)
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _daftarKuantitiasHarga(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DaftarKuantitas dkh = (DaftarKuantitas)args.get("arg");
		StringBuilder content = new StringBuilder();
		if(dkh != null) {
			Locale indo = new Locale("id", "ID");
			NumberFormat defaultFormat = NumberFormat.getNumberInstance(indo);
			content.append("<table class=\"table table-bordered\">");
			content.append("<thead><tr><th>"+Messages.get("lelang.jenis_barang_jasa")+"</th><th width=\"100\">"+Messages.get("lelang.satuan_unit")+"</th><th width=\"100\">"+Messages.get("lelang.volume")+"</th>");
			content.append("<th width=\"150\">"+Messages.get("lelang.harga_satuan")+"</th><th width=\"60\">"+Messages.get("lelang.pajak")+"</th><th width=\"60\">"+Messages.get("lelang.total_harga")+"</th><th width=\"150\">"+Messages.get("lelang.keterangan")+"</th></tr></thead><tbody>");
			if (CollectionUtils.isNotEmpty(dkh.items)) {
				for (Rincian_hps rincian : dkh.items) {
					content.append("<tr>");
					content.append("<td style=\"padding:5px;\">").append(rincian.item != null ? HTML.htmlEscape(rincian.item) : "").append("</td>");
					content.append("<td style=\"padding:5px;\">").append(rincian.unit != null ? HTML.htmlEscape(rincian.unit) : "").append("</td>");
					content.append("<td style=\"padding:5px;\">").append(rincian.vol != null ? defaultFormat.format(rincian.vol) : "").append("</td>");
					content.append("<td style=\"padding:5px;\">").append(FormatUtils.formatCurrencyRupiah(rincian.harga)).append("</td>");
					content.append("<td style=\"padding:5px;\">").append(rincian.pajak).append("</td>");
					content.append("<td style=\"padding:5px;\">").append(FormatUtils.formatCurrencyRupiah(rincian.total_harga)).append("</td>");
					content.append("<td style=\"padding:5px;\">").append(rincian.keterangan != null ? HTML.htmlEscape(rincian.keterangan) : "").append("</td>");
					content.append("</tr>");
				}
				content.append("<tr><th>Total</th><th colspan=\"6\">").append(FormatUtils.formatCurrencyRupiah(dkh.total)).append("</th></tr>");
			}
			content.append("</tbody></table>");
		}
		out.print(content.toString());
	}

	public static void _listUndangan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Peserta peserta = (Peserta)args.get("arg");
		Lelang_seleksi lelang = peserta.getLelang_seleksi();
		TahapNow tahapNow = lelang.getTahapNow();
		TahapStarted tahapStarted = lelang.getTahapStarted();
		Evaluasi kualifikasi = lelang.getEvaluasiKualifikasi();
		Nilai_evaluasi nilaiPembuktian = peserta.getNilaiPembuktian();

		boolean isTahapKirimUndangan = false;
		if(lelang.isPascaDuaFile()){
			if(lelang.getKategori().isConsultant() || lelang.getKategori().isJkKonstruksi()){
				isTahapKirimUndangan = tahapNow.isEvaluasiTeknis();
			}else{
				isTahapKirimUndangan = tahapNow.isEvaluasiHarga();
			}
		}else {
			isTahapKirimUndangan = tahapNow.isEvaluasiKualifikasi();
		}

		Boolean allowKirimUndangan =
				(!lelang.isSedangPersetujuanPemenang() || lelang.isPrakualifikasi())  &&
						(kualifikasi != null && kualifikasi.eva_status.isSedangEvaluasi()) &&
						Active_user.current().isPanitia() &&
						nilaiPembuktian != null && isTahapKirimUndangan;

		boolean showKolomPembuktian = tahapStarted.isEvaluasi() && nilaiPembuktian != null;

		Metode metode = lelang.getMetode();
		StringBuilder content = new StringBuilder();

		if(lelang.isPascakualifikasi()){
			if(metode.dokumen == MetodeDokumen.SATU_FILE){
				//Kolom Klarifikasi Administrasi, Kualifikasi, Teknis, dan Harga
				JenisEmail jenisEmail = JenisEmail.UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS_HARGA;
				content.append(renderKolomUndangan(lelang.lls_id,jenisEmail,peserta.psr_id,tahapNow.isEvaluasi()));
			}else if(metode.dokumen == MetodeDokumen.DUA_FILE){
				//Kolom Klarifikasi Administrasi, Kualifikasi, dan Teknis
				JenisEmail jenisEmail = JenisEmail.UNDANGAN_ADMIN_KUALIFIKASI_TEKNIS;
				content.append(renderKolomUndangan(lelang.lls_id,jenisEmail,peserta.psr_id,tahapNow.isEvaluasiTeknis()));

				//Kolom Klairifikasi Harga
				if (tahapStarted.isEvaluasiHarga() && !lelang.isSedangPersetujuanPemenang()){
					jenisEmail = JenisEmail.UNDANGAN_HARGA;
					content.append(renderKolomUndangan(lelang.lls_id,jenisEmail,peserta.psr_id,tahapNow.isEvaluasiHarga()));
				}
			}

			//Kolom Pembuktian Kualifikasi
			if(showKolomPembuktian){
				JenisEmail jenisEmail = JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI;
				content.append(renderKolomUndangan(lelang.lls_id, jenisEmail, peserta.psr_id, allowKirimUndangan));
			}

		}else{
			//Kolom Klarifikasi Kualifikasi
			JenisEmail mailKualifikasi = JenisEmail.UNDANGAN_KUALIFIKASI;
			content.append(renderKolomUndangan(lelang.lls_id,mailKualifikasi,peserta.psr_id,tahapNow.isEvaluasiKualifikasi()));

			//Kolom Klarifikasi Kualifikasi
			if(peserta.is_dikirim_pesan) {
				JenisEmail pesan_kualifikasi = JenisEmail.DOK_KUALIFIKASI;
				content.append(renderKolomUndangan(lelang.lls_id, pesan_kualifikasi, peserta.psr_id, tahapNow.isEvaluasiKualifikasi()));
			}

			//Kolom Pembuktian Kualifikasi
			if(showKolomPembuktian){
				JenisEmail jenisEmail = JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI;
				content.append(renderKolomUndangan(lelang.lls_id, jenisEmail, peserta.psr_id, allowKirimUndangan));
			}

			//Kolom Klarifikasi Administrasi dan Teknis
			if(tahapStarted.isEvaluasiTeknis()){
				JenisEmail adminTeknis = JenisEmail.UNDANGAN_ADMIN_TEKNIS;
				content.append(renderKolomUndangan(lelang.lls_id,adminTeknis,peserta.psr_id,tahapNow.isEvaluasiTeknis()));
			}

			//Kolom Klarifikasi Teknis (Revisi) dan Harga
			if(tahapStarted.isEvaluasiHarga()){
				JenisEmail harga = JenisEmail.UNDANGAN_HARGA;
				if(metode.dokumen == MetodeDokumen.DUA_TAHAP){
					harga = JenisEmail.UNDANGAN_TEKNISREVISI_HARGA;
				}

				content.append(renderKolomUndangan(lelang.lls_id,harga,peserta.psr_id,tahapNow.isEvaluasiHarga()));
			}

		}

		out.print(content.toString());
	}

	private static String renderKolomUndangan(Long lls_id, JenisEmail jenis, Long pesertaId, boolean showKirim){
		Peserta peserta = Peserta.findBy(pesertaId);
		MailQueue mail = MailQueueDao.getByJenisAndLelangAndRekanan(lls_id, peserta.rkn_id , jenis);

		StringBuilder content = new StringBuilder();

		content.append("<tr>");
		content.append("<th class=\"bgwarning\">"+jenis.label);
		if(jenis.equals(JenisEmail.UNDANGAN_PEMBUKTIAN_KUALIFIKASI)){
			content.append(" <span class='warning'>*</span>");
		}
		content.append("</th>");
		content.append("<td>");

		String labelKirim = "Kirim";
		if(mail != null){
			labelKirim = labelKirim + " Ulang";
			content.append("Sudah dikirim <i>("+FormatUtils.formatDateTimeInd(mail.enqueue_date)+")</i>");
			String urlView = Router.reverse("lelang.KlarifikasiCtr.view").add("id",pesertaId).add("jenis",jenis.id).url;
			content.append(" &nbsp<a href=\""+urlView+"\" class=\"badge  badge-secondary\" target=\"_blank\">Lihat</a>");
		}else{
			content.append("Belum dikirim");
		}

		String url = Router.reverse("lelang.KlarifikasiCtr.kirimPesan")
				.add("id",pesertaId)
				.add("jenis",jenis.id)
				.url;

		if(showKirim){
			content.append(" &nbsp<a href=\""+url+"\" class=\"badge  badge-secondary\">"+labelKirim+"</a>");
		}

		content.append("</tr>");

		return content.toString();
	}

	public static void _daftarKuantitiasHargaAuctionPeserta(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Long id = (Long) args.get("arg");
		List<ReverseAuctionDetil> penawaranList  = ReverseAuctionDetil.findByPeserta(id);
		if(CollectionUtils.isEmpty(penawaranList))
			return ;
		StringBuilder content = new StringBuilder();
		int size = penawaranList.size();
		for(ReverseAuctionDetil detil : penawaranList) {
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">"+Messages.get("lelang.penawaran")+" "+(size)+"</div>");
			content.append("<table class=\"table table-condensed table-bordered\"><tbody>");
			content.append("<tr><th width=\"15%\">"+Messages.get("lelang.harga_penawaran")+"</th><td>" + FormatUtils.formatCurrencyRupiah(detil.rad_nev_harga) + "</td></tr>");
			content.append("<tr><th>"+Messages.get("lelang.waktu_pengiriman")+"</th><td>" + FormatUtils.formatDateInd(detil.auditupdate) + "</td></tr>");
			content.append("<tr><td colspan=\"2\">");
			if (detil.dkh != null) {
				Locale indo = new Locale("id", "ID");
				NumberFormat defaultFormat = NumberFormat.getNumberInstance(indo);
				content.append("<table class=\"table table-bordered table-condensed\">");
				content.append("<thead><tr><th>"+Messages.get("lelang.jenis_barang_jasa")+"</th><th width=\"100\">"+Messages.get("lelang.satuan_unit")+"</th><th width=\"100\">"+Messages.get("lelang.volume")+"</th>");
				content.append("<th width=\"150\">"+Messages.get("lelang.harga_satuan")+"</th><th width=\"70\">"+Messages.get("lelang.pajak")+"</th><th width=\"160\">"+Messages.get("lelang.total_harga")+"</th><th width=\"150\">"+Messages.get("lelang.keterangan")+"</th></tr></thead><tbody>");
				if (CollectionUtils.isNotEmpty(detil.dkh.items)) {
					for (Rincian_hps rincian : detil.dkh.items) {
						content.append("<tr>");
						content.append("<td style=\"padding:5px;\">").append(rincian.item != null ? HTML.htmlEscape(rincian.item) : "").append("</td>");
						content.append("<td style=\"padding:5px;\">").append(rincian.unit != null ? HTML.htmlEscape(rincian.unit) : "").append("</td>");
						content.append("<td style=\"padding:5px;\">").append(rincian.vol != null ? defaultFormat.format(rincian.vol) : "").append("</td>");
						content.append("<td style=\"padding:5px;\">").append(FormatUtils.formatCurrencyRupiah(rincian.harga)).append("</td>");
						content.append("<td style=\"padding:5px;\">").append(rincian.pajak).append("</td>");
						content.append("<td style=\"padding:5px;\">").append(FormatUtils.formatCurrencyRupiah(rincian.total_harga)).append("</td>");
						content.append("<td style=\"padding:5px;\">").append(rincian.keterangan != null ? HTML.htmlEscape(rincian.keterangan) : "").append("</td>");
						content.append("</tr>");
					}
					content.append("<tr><th>Total</th><th colspan=\"6\">").append(FormatUtils.formatCurrencyRupiah(detil.dkh.total)).append("</th></tr>");
				}
				content.append("</tbody></table>");
				content.append("</td></tr></tbody></table>");
			}
			content.append("</div>");
			size--;
		}
		out.print(content.toString());
	}

	public static void _statusTender(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		StatusTender lelang = (StatusTender) args.get("arg");
		if(lelang == null)
			return;
		if(lelang.isItemized())
			out.print("<span class=\"badge  badge-info\">Itemized</span>");
		if(lelang.isLelangUlang())
			out.print((lelang.isKonsultansi() || lelang.isJkKonstruksi()) ? "<span class=\"badge  badge-warning\">Seleksi Ulang</span>":"<span class=\"badge  badge-warning\">Tender Ulang</span>");
		if(lelang.isDitutup())
			out.print((lelang.isKonsultansi() || lelang.isJkKonstruksi()) ? "<span class=\"badge  badge-info\">Seleksi Dibatalkan</span>":"<span class=\"badge  badge-info\">Tender Dibatalkan</span>");
		if(lelang.isUbahLelang())
			out.print((lelang.isKonsultansi() || lelang.isJkKonstruksi()) ? "<span class=\"badge  badge-info\">Seleksi Dibatalkan</span>" : "<span class=\"badge  badge-info\">Tender Diubah</span>");
		if(lelang.isPenawaranUlang())
			out.print(" <span class=\"label label-info\">"+Messages.get("lelang.pemasukan_penawaran_ulang")+"</span>");
		if(lelang.isEvaluasiUlang())
			out.print("<span class=\"label label-info\">"+Messages.get("lelang.evaluasi_ulang")+"</span>");
	}

}