package ext;

import groovy.lang.Closure;
import models.agency.Paket;
import models.agency.contracts.AnggaranContract;
import models.agency.contracts.PaketAnggaranContract;
import models.agency.contracts.SirupContract;
import models.common.Active_user;
import org.apache.commons.collections4.CollectionUtils;
import play.i18n.Messages;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 8/15/2018
 * class ini ada untuk mengurangi duplicated code antara paket-edit dengan paket-pl-edit,
 * terutama table rup dan table anggaran karena 2 table tersebut memiliki kesamaan,
 */
public class PaketTag extends FastTags {

    public static void _rupTable(Map<?, ?> args, Closure body,  PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        List<SirupContract> sirups = (List<SirupContract>) args.get("sirups");
        if(CollectionUtils.isEmpty(sirups))
            return;
        String action = (String) args.get("action");
        Boolean isDraft = (Boolean) args.get("isDraft");
        if(isDraft == null)
            isDraft = Boolean.FALSE;
        Long paketId = (Long) args.get("paketId");
        StringBuilder content = new StringBuilder();
        content.append("<table class=\"table table-condensed\"><tr><th width=\"80\">"+Messages.get("tag.kode_rup")+"</th>" +
                    "<th>"+Messages.get("tag.nama_paket")+"</th><th width=\"200\">"+Messages.get("tag.sumber_dana")+"</th>");
        if (isDraft) {
            content.append("<th></th>");
        }
        content.append("</tr>");
        int index = 0;
        for (SirupContract sirup : sirups) {
            content.append("<tr>");
            content.append("<td>").append(sirup.getId()).append("</td>");
            content.append("<td>").append(sirup.getNama()).append("</td>");
            content.append("<td>").append(sirup.getSumberDanaString()).append("</td>");
            content.append("<td>");
            if (isDraft && index > 0) {
                content.append("<a href=\"").append(sirup.getDeleteUrl(paketId, action))
                        .append("\"><i class=\"fa fa-trash-o fa-lg\"></i></a>");
            }
            content.append("</td>");
            content.append("</tr>");
            index++;
        }
        content.append("</table>");
        out.print(content);
    }

    public static void _anggaranTable(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        List<? extends PaketAnggaranContract> contracts = (List<? extends PaketAnggaranContract>) args.get("anggarans");
        Boolean isAllowEditPpk = (Boolean) args.get("allowEdit");
        String action = (String) args.get("action");
        StringBuilder content = new StringBuilder();
        content.append("<table id=\"tblSumberAnggaran\" class=\"table table-condensed\"><tr><th width=\"80\">"+Messages.get("ct.tahun")+"</th>" +
                "<th width=\"200\">"+Messages.get("ct.sbd")+"</th><th>"+Messages.get("ct.ka")+"</th><th width=\"150\">"+Messages.get("ct.nilai")+"</th>" +
                "<th width=\"150\">"+Messages.get("tag.ppk")+"</th></tr>");
        for (PaketAnggaranContract contract : contracts) {
            AnggaranContract anggaran = contract.getAnggaranContract();
            boolean isAnggaranExist = anggaran != null;
            content.append("<tr>");
            content.append("<td>").append(isAnggaranExist ? anggaran.getAngTahun() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist ? anggaran.getSbdLabel() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist
                    ? anggaran.getKodeRekening() : "").append("</td>");
            content.append("<td>").append(isAnggaranExist
                    ? anggaran.getAngNilaiFormatted() : "").append("</td>");
            content.append("<td>").append(contract.getPpkNama());
            if (isAllowEditPpk) {
                content.append("<a class=\"btn btn-default btn-xs\" title=\""+Messages.get("ct.pppk")+"\" href=\"")
                        .append(contract.getUpdatePpkUrl(action))
                        .append("\">")
                        .append("<i class=\"fa fa-edit\"></i>&nbsp;")
                        .append(contract.optionLabel())
                        .append(" PPK")
                        .append("</a>");
            }
            content.append("</td>");
            content.append("</tr>");
        }
        content.append("</table>");
        out.print(content);
    }

    public static void _konsolidasiList(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Long paketId = (Long) args.get("arg");
        Boolean editable = (Boolean) args.get("editable");
        Active_user active_user  = Active_user.current();
        List<Paket> list = Paket.findByKonsolidasiId(paketId);
        if(list == null || list.isEmpty())
            return;
        out.print("<table class=\"table table-sm\">");
        out.print("<tr>");
        out.print("<th>"+Messages.get("ct.kode")+"</th>");
        out.print("<th>"+Messages.get("ct.nama")+"</th>");
        if(active_user.isKuppbj())
            out.print("<th></th>");
        out.print("</tr>");
        Router.ActionDefinition action = Router.reverse("lelang.PaketCtr.hapus_konsolidasi");
        for(Paket paket : list) {
            out.print("<tr>");
            out.print("<td>"+paket.pkt_id+"</td>");
            out.print("<td>"+paket.pkt_nama+"</td>");
            if(active_user.isKuppbj() && editable)
                out.print("<td><a href=\""+action.add("id", paketId).add("paketId", paket.pkt_id).url+"\" value=\""+paket.pkt_id+"\"><i class=\" fa fa-trash-o fa-lg\"></i></a></td>");
            out.print("</tr>");
        }
        out.print("</table>");
    }
}
