package ext;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tika.Tika;
import play.Logger;
import play.data.validation.Validation;

import java.io.File;

public class PdfTypeCheck extends AbstractAnnotationCheck<PdfType> {

    static final String mes = "validation.dokumenType";

    //http://www.freeformatter.com/mime-types-list.html
    public static final String[] pattern = new String[]{"application/pdf"};

    public static String allowedExt = "pdf";

    static final Tika tika = new Tika();

    @Override
    public void configure(PdfType dokumen) {
        setMessage(dokumen.message());
    }

    @Override
    public boolean isSatisfied(Object validatedObject, Object value, OValContext context, Validator validator) throws OValException {
        value = Validation.willBeValidated(value);
        if (value == null || value.toString().length() == 0) {
            return true;
        }

        File file = (File) value;

        // use apache tika library
        // jika file ekstensi diubah, tetap dapat membaca content type aslinya
        String mimeType = null;
        try {
            mimeType = tika.detect(file);
        } catch (Exception e) {
            Logger.error(e, "PdfTypeCheck:%s", e.getMessage());
        }

        if(mimeType == null)
            return true;

        return ArrayUtils.contains(pattern, mimeType);
    }

}
