package ext;

import controllers.BasicCtr;
import controllers.PublikCtr;
import controllers.jcommon.blob.OwnerOnlyDownloadHandler;
import groovy.lang.Closure;
import models.agency.Paket;
import models.agency.Panitia;
import models.agency.Syarat_paket;
import models.bm.BroadcastMessage;
import models.common.*;
import models.entity.ItemDaftarAuditor;
import models.entity.ItemDataPenyedia;
import models.entity.Menu;
import models.jcommon.blob.BlobTable;
import models.jcommon.mail.MailQueue.BACA_STATUS;
import models.jcommon.util.CommonUtil;
import models.lelang.*;
import models.lelang.Dok_lelang.JenisDokLelang;
import models.nonlelang.Diskusi_lelangPl;
import models.nonlelang.PengumumanPl;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.common.TahapStartedPl;
import models.rekanan.Rekanan;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.Play.Mode;
import play.cache.Cache;
import play.i18n.Messages;
import play.mvc.Http.Request;
import play.mvc.Router;
import play.mvc.Scope.Flash;
import play.mvc.Scope.Params;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomTag extends FastTags {   

	public static final String SPSE3_URL = Play.configuration.getProperty("spse3.url", BasicCtr.getRequestHost()+"/eproc");
	/**
	 * Fungsi {@code _topmenu} merupakan custom tag Play!Framework yang digunakan untuk render menu setiap halaman
	 * pada SPSE, baik itu menu pada halaman publik maupun halaman user.
	 *
	 * @param args standar fast tag
	 * @param body standar fast tag
	 * @param out standar fast tag
	 * @param template standar fast tag
	 * @param fromLine standar fast tag
	 */
	@SuppressWarnings(value = {"unchecked"})
	public static void _topmenu(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
		Request request = Request.current();
		Active_user active_user = Active_user.current();
		StringBuilder content = new StringBuilder();
		Map<String, Object> enMap = new HashMap<>();
		enMap.put("lang", "en");
		Map<String, Object> idMap = new HashMap<>();
		idMap.put("lang", "id");
        if(active_user == null)
        {
        	//for improved speed, avoid using string concatenation
        	Menu[] menuLeft = Menu.MENU_PUBLIK;      
        	content.append("<nav class=\"topmenu navbar navbar-expand-lg\"><div class=\"container navbar-collapse\">");
        	content.append("<ul class=\"navbar-nav\" >");
        	for (Menu menu : menuLeft) {
				//untuk PL, karena di PL 1 halaman memiliki 2 tab dengan url yang berbeda jadi request action di hardcode
				//agar menu yg sedang aktif muncul
				String requestAction = request.action;
				if(requestAction.equals("nonlelang.PengadaanLCtr.index") || requestAction.equals("nonSpk.NonSpkCtr.index") || requestAction.equals("swakelola.SwakelolaCtr.index"))
					requestAction = "lelang.LelangCtr.index";
				String target="";
		        if (menu == Menu.PUBLIK_DAFTAR_HITAM || menu == Menu.PUBLIK_REGULASI) { //blacklist menggunakan external url, open new tab
			        target = "target=\"_blank\"";
		        }
				content.append("<li class=\"nav-item ").append(requestAction.equals(menu.action) ? "active":"").append("\" >");
        		content.append("<a class=\"nav-link\" href=\"").append(menu.page).append("\" ").append(target).append(">").append(menu.captionUpperCase).append("</a>");
        		content.append("</li>");
			}        
        	content.append("</ul>");
        	content.append("<ul class=\"navbar-nav ml-auto\">");
        	if(!request.action.contains("PublikCtr.loginAdmin") ^ request.action.contains("admin.Login2Ctr.loginAdmin") ^ request.action.contains("admin.Login2Ctr.loginPassword"))
        	{
				content.append("<li class=\"nav-item\" id=\"daftar\" ").append(request.action.equals("PublikCtr.mendaftarEmail")? "active":"");
				content.append("><a class=\"nav-link\" href=\"").append(Router.reverse("PublikCtr.mendaftarEmail")).append("\">PENDAFTARAN PENYEDIA</a></li>");
				content.append("<li class=\"nav-item dropdown\"><a id=\"login\" href=\"#\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\">LOGIN <b class=\"caret\"></b></a>");
				content.append("<div class=\"dropdown-menu dropdown-menu-right loginForm\" id=\"loginForm\">");
				content.append("<img src=\"").append(BasicCtr.CONTEXT_PATH).append("/public/images/imgng/loading.gif\">");
				content.append("</div></li></ul>");
        	}
			content.append("</ul>");
        	content.append("</div></nav>");
        }
        else { // Menu jika user sudah login
        	Menu[] menuLeft = Menu.findByGroup(active_user.group);          	
        	content.append("<div class=\"topmenu navbar navbar-expand-lg\"><div class=\"container navbar-collapse\">");
        	content.append("<ul class=\"navbar-nav\" >");
        	for (Menu menu : menuLeft) {
				//untuk PL, karena di PL 1 halaman memiliki 2 tab dengan url yang berbeda jadi request action di hardcode
				//agar menu yg sedang aktif muncul
				String requestAction = request.action;
				if(requestAction.equals("BerandaCtr.PPNonSpk")|| requestAction.equals("BerandaCtr.nonLelang")
						|| requestAction.equals("BerandaCtr.PpkNonTransaksional") || requestAction.equals("BerandaCtr.PpkSwakelola"))
					requestAction = "BerandaCtr.index";

				// MENU DAFTAR PAKET
				if(!requestAction.equals(menu.action)) {
					if(requestAction.equals("nonlelang.PaketPlCtr.index") || requestAction.equals("nonlelang.PaketPlCtr.indexNonSpk")
							|| requestAction.equals("swakelola.PaketSwakelolaCtr.index"))
						requestAction = "lelang.PaketCtr.index";
				}

				if(requestAction.equals("swakelola.PaketSwakelolaCtr.index"))
					requestAction = "nonlelang.PaketPlCtr.indexNonSpk";

        		content.append(menu.page).append("\">").append(menu.getCaptionUpperCase()).append("</a></li>");
        		content.append("<li ").append(requestAction.equals(menu.action) ? "class=\"active\"":"").append("><a href=\"");
			}        
        	content.append("</ul>");
        	content.append("<li><a id=\"logout\" href=\"#\"><i class=\"fa fa-power-off\"></i> "+Messages.get("beranda.keluar")+"</a></li>");
//        	content.append("<li class=\"label-info\" ><a href=\"").append(SPSE3_URL).append("\">SPSE 3.6</a></li>");
            content.append("<li><a href=\"").append(Router.reverse("PublikCtr.setLanguage", idMap).url).append("\"><span class=\"flag flag-id\"> </span></a></li>");
			content.append("<li><a href=\"").append(Router.reverse("PublikCtr.setLanguage", enMap).url).append("\"><span class=\"flag flag-us\"></span></a><li>");
        	content.append("<ul class=\"nav navbar-nav navbar-right\">");
        	content.append("</ul>");
        	content.append("</div></div>");
        	content.append("<div class=\"submenu\"><div class=\"container\">");
        	content.append("<div class=\"float-right\">").append(active_user.userlabel).append("</div>");
        	content.append("<div class=\"navBlock\">");
        	content.append("<a href=\"#\" id=\"showHideHeader\" title=\"Header show/hide...\" class=\"space\"><i class=\"fa fa-angle-double-up\"></i></a>");
        	if (active_user.isRekanan() || active_user.isPanitia()) 
        		content.append("<a href=\"").append(BasicCtr.getURLApendo()).append("\" target=\"_blank\" title=\"Aplikasi Apendo Penyedia\" class=\"space\"><span class=\"badge  badge-info\"> <i class=\"fa fa-download\"></i> Apendo/Spamkodok</span></a>");
        	
        	content.append("<a href=\"").append(BasicCtr.getURLAplikasiEprocLain()).append(" \" target=\"_blank\" class=\"space\"><span class=\"label label-info\">"+Messages.get("beranda.eprocurement_lainnya")+"</span></a>");
        	content.append("<a href=\"").append(BasicCtr.getURLUserManual()).append("\" target=\"_blank\" class=\"space\"><span class=\"label label-info\"> <i class=\"fa fa-book\"></i> "+Messages.get("beranda.petunjuk_penggunaan")+"</span></a>");
        	content.append("</div>");	        	
        	content.append("</div></div>");			
		}        
        out.print(content.toString());
	}

	public static void _topmenuPL(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Request request = Request.current();
		Active_user active_user = Active_user.current();
		StringBuilder content = new StringBuilder();
		if(active_user.isRekanan())
		{
			Menu[] menuLeft = Menu.MENU_REKANAN;
			content.append("<div class=\"topmenu\"><div class=\"container\">");
			content.append("<ul class=\"nav navbar-nav\" >");
			for (Menu menu : menuLeft) {
				content.append("<li class=\"nav-item ").append(request.action.equals(menu.action) ? "active":"").append("><a class=\"nav-link\" href=\"");
				content.append(menu.page).append("\">").append(menu.captionUpperCase).append("</a></li>");
			}
			content.append("</ul>");
			content.append("<ul class=\"nav navbar-nav ml-auto\">");
			content.append("<li><a id=\"logout\" href=\"#\"><i class=\"fa fa-power-off\"></i> "+Messages.get("beranda.keluar")+"</a></li>");
			content.append("</ul>");
			content.append("</div></div>");
			content.append("<div class=\"submenu\"><div class=\"container\">");
			content.append("<div class=\"float-right\">").append(active_user.userlabel).append("</div>");
			content.append("<div class=\"navBlock\">");
			content.append("<a href=\"#\" id=\"showHideHeader\" title=\"Header show/hide...\" class=\"space\"><i class=\"fa fa-angle-double-up\"></i></a>");
			if (active_user.isRekanan() || active_user.isPanitia())
				content.append("<a href=\"").append(BasicCtr.getURLApendo()).append("\" target=\"_blank\" title=\"Aplikasi Apendo Penyedia\" class=\"space\"><span class=\"badge  badge-info\"> <i class=\"fa fa-download\"></i> Apendo</span></a>");

			content.append("<a href=\"").append(BasicCtr.getURLUserManual()).append("\" target=\"_blank\" class=\"space\"><span class=\"badge  badge-info\"> <i class=\"fa fa-book\"></i>  Petunjuk Penggunaan</span></a>");
			content.append("<a href=\"").append(BasicCtr.getURLAplikasiEprocLain()).append(" \" target=\"_blank\" class=\"space\"><span class=\"badge  badge-info\">Aplikasi E-Procurement Lainnya</span></a>");
			content.append("</div>");
			content.append("</div></div>");
		}

		out.print(content.toString());
	}

	/**
	 * Fungsi {@code _title} merupakan custom tag Play!Framework yang digunakan set pesan system pada halaman SPSE.<br />	
	 * @param args standar fast tag
	 * @param body standar fast tag
	 * @param out standar fast tag
	 * @param template standar fast tag
	 * @param fromLine standar fast tag
	 */
	
	public static void _systemMessage(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String systemMsg = Cache.get("systemMsg", String.class);
		if (systemMsg == null) {
			systemMsg = Berita.getSystemMessage();
			Cache.set("systemMsg", systemMsg, "10mn"); // set 10 menit cache
		}
		if(!CommonUtil.isEmpty(systemMsg))
			out.print(systemMsg);
	}
	
	
	/**
	 * Fungsi {@code _title} merupakan custom tag Play!Framework yang digunakan set title halaman SPSE.<br />
	 * TODO: Perlu diperbaiki karena setiap halaman seharusnya punya title yang berbeda. Saat ini masih sama.	
	 * @param args standar fast tag
	 * @param body standar fast tag
	 * @param out standar fast tag
	 * @param template standar fast tag
	 * @param fromLine standar fast tag
	 */
	
	public static void _title(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String key="ext.CustomTag._title()";
		String value=Cache.get(key, String.class);
		if(value==null)
		{
			value = ConfigurationDao.getNamaLpse();
			if (value == null) {
				value= "LPSE";
			}
			Boolean isLatihan = Cache.get("isLatihan", Boolean.class) ;
			if (isLatihan != null && isLatihan) {
				value = value+ " [Latihan]";
			}
			Cache.set(key, value);
		}
		String content = JavaExtensions.toString(body);

		String title="<title>"+ value + ':' +content+"</title>";
		out.print(title);
	}
	
	public static void _alert(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		 StringBuilder content = new StringBuilder();
//        if(Validation.hasErrors()) { // error sebab oleh play.validation
//            StringBuilder msg_content = new StringBuilder();
//            List<play.data.validation.Error> list = Validation.errors();
//            list.remove(list.size() - 1);
//            for (play.data.validation.Error error:list)
//                if(StringUtils.isNotEmpty(error.message()))
//                    msg_content.append("<li>" + error.message() + "</li>");
//            if(StringUtils.isNotEmpty(msg_content.toString())) {
//                content.append("<div class=\"alert alert-danger alert-dismissable\">");
//                content.append("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>");
//                content.append("<ul class=\"list-unstyled\">");
//                content.append(msg_content.toString());
//                content.append("</ul></div>");
//            }
//        }
//        else {
            Flash flash = Flash.current();
            if(flash != null && flash.get("error") != null) {	// error sebab diset oleh SPSE
            	content.append("<div class=\"alert alert-danger\">").append(flash.get("error")).append("</div>");
            }
            if(flash != null && flash.get("success") != null) {
            	content.append("<div class=\"alert alert-success\">").append(flash.get("success")).append("</div>");
            }
//        }          
         out.print(content.toString());
	}
	
	public static void _pengumumanLelang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		out.print("<tr><th width=\"5%\">No</th><th>"+Messages.get("ct.np")+"</th><th class=center width=\"150\">"+Messages.get("hps.hps")+"</th><th class=center width=\"170\">"+Messages.get("ct.ah")+"</th></tr>");
		out.print("<table class=\"table table-condensed\"><thead>");
		out.print("</thead><tbody>");    
        Map<String, Object> map = new HashMap<String, Object>(1);
        for (Kategori kategori : Kategori.values()) {
        	List<PengumumanLelang> pengumumanList = PengumumanLelang.findByKategori(kategori);
        	String css = kategori.nama.replace(" ", "_");
        	out.print("<tr><td colspan=\"4\" class=\"bs-callout-info\"><a href=\"#\" onclick=\"$('."+css+"').toggle();\">"+kategori.getNama()+" </a><span class=\"badge pull-right\">"+pengumumanList.size()+"</span></td></tr>");
        	int i=1 ;        	
        	for (PengumumanLelang pengumumanLelang : pengumumanList) {
        		map.put("id", pengumumanLelang.lls_id);
        		out.print("<tr class=\""+css+"\" sytle=\"display:none\">");
        		out.print("<td><center>"+i+"</center></td>");
        		out.print("<td><a href=\""+Router.reverse("lelang.LelangCtr.pengumumanLelang", map).url+"\" target=\"_blank\">"+pengumumanLelang.pkt_nama+"</a>&nbsp;");

        		String flag = "spse 3";
        		String highlight = "badge-success";

        		if(pengumumanLelang.pkt_flag != 1){
        			flag = pengumumanLelang.pkt_flag == 2 ? "spse 4" : "spse 4.3";
        			highlight = pengumumanLelang.pkt_flag == 2 ? "badge-primary" : "badge-danger";
				}

				out.print("<span class=\"badge "+highlight+"\">"+flag+"</span></td>");
		        if (pengumumanLelang.isEnableViewHps()) {
			        out.print("<td class=\"table-hps\">" + FormatUtils.formatCurrenciesJutaRupiah(pengumumanLelang.pkt_hps) + "</td>");
		        } else {
			        out.print("<td class=\"table-hps\"><span class=\"label label-info\">"+Messages.get("hps.tidak_ditayangkan")+"</span></td>");
		        }
		        out.print("<td class=\"center\">"+FormatUtils.formatDateTimeInd(pengumumanLelang.tgl_akhir_daftar)+"</td>");
        		out.print("</tr>");
                i++;
			}        	
        }
        out.print("</tbody></table>");
	}

	public static void _pengumumanPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		out.print("<table class=\"table table-condensed pl\"><thead>");
		out.print("<tr><th width=\"5%\">No</th><th>"+Messages.get("ct.np")+"</th><th class=center width=\"150\">"+Messages.get("hps.hps")+"</th><th class=center width=\"170\">"+Messages.get("ct.ah")+"</th></tr>");
		out.print("</thead><tbody>");
		Map<String, Object> map = new HashMap<String, Object>(1);
		for (Kategori kategori : Kategori.values()) {
			List<PengumumanPl> pengumumanList = PengumumanPl.findByKategori(kategori);
			String css = kategori.nama.replace(" ", "_") + "_pl";
			out.print("<tr><td colspan=\"4\" class=\"bs-callout-danger\"><a href=\"#\" onclick=\"$('."+css+"').toggle();\">"+kategori.getNama()+" </a><span class=\"badge pull-right\">"+CollectionUtils.size(pengumumanList)+"</span></td></tr>");
			int i=1 ;
			for (PengumumanPl pengumumanLelang : pengumumanList) {
				MetodePemilihan metodePemilihan = MetodePemilihan.findById(pengumumanLelang.mtd_pemilihan);
				map.put("id", pengumumanLelang.lls_id);
				out.print("<tr class=\""+css+"\" sytle=\"display:none\">");
				out.print("<td><center>"+i+"</center></td>");
				out.print("<td><a href=\""+Router.reverse("nonlelang.PengadaanLCtr.pengumumanPl", map).url+"\" target=\"_blank\">"+pengumumanLelang.pkt_nama+"</a>&nbsp;");

				String flag = "spse 3";
				String highlight = "label-success";
				String mtdLabel = metodePemilihan.getLabel();

				if(pengumumanLelang.pkt_flag != 1){
					flag = pengumumanLelang.pkt_flag == 2 ? "spse 4" : "spse 4.3";
					highlight = pengumumanLelang.pkt_flag == 2 ? "badge badge-primary" : "badge badge-danger";
					if(pengumumanLelang.pkt_flag == 3) {
						MetodePemilihanPenyedia mtd = MetodePemilihanPenyedia.findById(pengumumanLelang.mtd_pemilihan);
						mtdLabel = mtd.label;
					}
				}

				out.print("<span class=\""+highlight+"\">"+flag+"</span>");
				out.print("&nbsp;<span class=\"badge  badge-info\">" + mtdLabel + "</span></td>");
				out.print("<td class=\"table-hps\">"+FormatUtils.formatCurrenciesJutaRupiah(pengumumanLelang.pkt_hps)+"</td>");
				out.print("<td class=\"center\">"+FormatUtils.formatDateTimeInd(pengumumanLelang.tgl_akhir_daftar)+"</td>");
				out.print("</tr>");
				i++;
			}
		}
		out.print("</tbody></table>");
	}
	
	
	/**Generate link untuk download sebuah file. Siapapun boleh download
	 * Untuk link di mana hanya owner yang boleh download, gunakan _downloadOwnerOnly
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _download(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		BlobTable blob = (BlobTable)args.get("arg");
		String cssClass = (String)args.get("class");
		Class handler = (Class)args.get("handler");
		if(blob == null)
			return;
		try {
			StringBuilder content = new StringBuilder("<a href=\""+blob.getDownloadUrl(handler)+"\"");
			if(!StringUtils.isEmpty(cssClass))
				content.append(" class=\""+cssClass+"\" ");
			content.append("><span class=\"fa fa-download\"></span> ").append(blob.blb_nama_file).append(" - ").append(FormatUtils.formatBytes(blob.blb_ukuran));
			content.append("</a>");
			out.println(content);
		}catch (Exception e) {
			Logger.error(e,"downloadSecurityHandler wajib diisi");
		}
	}
	
	public static void _downloadOwnerOnly(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		BlobTable blob = (BlobTable)args.get("arg");
		if(blob == null)
			return;
		try {
			out.println(new StringBuilder("<a href=\"").append(blob.getDownloadUrl(OwnerOnlyDownloadHandler.class)).append("\"><span class=\"fa fa-download\"></span> ").append(blob.blb_nama_file).append("</a> - ").append(FormatUtils.formatBytes(blob.getFile().length())));
		}catch (Exception e) {
			e.printStackTrace();
			Logger.error("downloadSecurityHandler wajib diisi");
		}
	}
	
	
	/**
	 * menampilkan menu data Penyedia di halaman Penyedia
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _menuDataPenyedia(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Request request = Request.current();
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\">");
		for(ItemDataPenyedia obj : ItemDataPenyedia.values()) {
			content.append("<li ").append(obj.action.contains(request.controller)? "class=\"active\"":"").append("><a href=\"").append(obj.page).append("\"><strong>").append(obj.getCaption()).append("</strong></a></li>");
		}
		content.append("</ul>");
		out.print(content.toString());
	}

	public static void _notifMigrasiSikap(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Request request = Request.current();
		StringBuilder content = new StringBuilder();
		// untuk tampilan status migrasi SIKaP
		if(!request.controller.toLowerCase().contains("migrasisikap")) {
			if(Active_user.isCurrentSedangMigrasi()) {
				content.append("<div class='alert alert-warning'>").append(Messages.get("sedang.migrasi.tidak.boleh.edit")).append("</div>");
			} else if(Active_user.isCurrentSelesaiMigrasi()) {
				content.append("<div class='alert alert-warning'>").append(Messages.get("mohon.gunakan.menu.integrasi")).append("</div>");
			}
		}
		out.print(content.toString());
	}
	public static void _menuDaftarAuditor(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Request request = Request.current();
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\">");
		for (ItemDaftarAuditor obj : ItemDaftarAuditor.values()) {
			content.append("<li ").append(obj.action.equals(request.action)? "class=\"active\"":"").append("><a href=\"").append(obj.page).append("\"><strong>").append(obj.getCaption()).append("</strong></a></li>");
		}
		content.append("</ul>");
		out.print(content.toString());
	}
	/**
	 * menampilkan syarat kualifikasi lelang
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _syaratKualifikasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Lelang_detil lelang = (Lelang_detil)args.get("arg");
		StringBuilder content = new StringBuilder();
		if(lelang.getPemilihan().isLelangExpress()) {
			String urlsikap_showkualifikasi = BasicCtr.SIKAP_URL+"/shortlist/showQualification?q="+lelang.lls_id;
			content.append("<a href=\"").append(urlsikap_showkualifikasi).append("\" target=\"_blank\">").append(Messages.get("menu.lihat_syarat_kualifikasi")).append("</a>");
		} else {
			if(lelang.pkt_flag != null && lelang.pkt_flag.intValue() > 1) { // paket dibuat dengan versi 4
				JenisDokLelang jenis = JenisDokLelang.DOKUMEN_LELANG;
				if(lelang.getMetode().kualifikasi.isPra())
					jenis = JenisDokLelang.DOKUMEN_LELANG_PRA;
				Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, jenis);
				if(dok_lelang == null)
					return;
				List<Checklist> ijinList = dok_lelang.getSyaratIjinUsaha();
				List<Checklist> syaratList = dok_lelang.getSyaratKualifikasi();
				List<Checklist> ijinBaruList = dok_lelang.getSyaratIjinUsahaBaru();
				List<Checklist> syaratAdminList = dok_lelang.getSyaratKualifikasiAdministrasi();
				List<Checklist> syaratTeknisList = dok_lelang.getSyaratKualifikasiTeknis();
				List<Checklist> syaratKeuanganList =  dok_lelang.getSyaratKualifikasiKeuangan();
				if (!CollectionUtils.isEmpty(ijinList) || !CollectionUtils.isEmpty(syaratList)) {
					content.append("<table class=\"table table-sm\">");
					if(!CollectionUtils.isEmpty(ijinList)){
						content.append("<tr><th>").append(Messages.get("menu.izin_usaha")).append("</th></tr>");
						content.append("<tr><td><table class=\"table table-condensed\">").append("<tr><td>").append(Messages.get("menu.jenis_ijin")).append("</td><td>").append(Messages.get("menu.klasifikasi")).append("</td></tr>");
						for(Checklist ijin : ijinList) {
							content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
						}
						content.append("</table></td></tr>");
					}
					for(Checklist syarat:syaratList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if(syarat.getJsonName() != null) {
							content.append("<b>").append(cm.ckm_nama).append("</b>");
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("<b>").append(syarat.chk_nama).append("</b>");					
						} else {
							content.append("<b>").append(cm.ckm_nama).append("</b>");
							if(!CommonUtil.isEmpty(syarat.chk_nama))
								content.append("<p>").append(syarat.chk_nama).append("</p>");
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
				if (!CollectionUtils.isEmpty(ijinBaruList) || !CollectionUtils.isEmpty(syaratAdminList)) {
					content.append("<h5>").append(Messages.get("menu.pkal")).append("</h5>");
					content.append("<table class=\"table table-condensed\">");
					if(!CollectionUtils.isEmpty(ijinBaruList)){
						content.append("<tr><th>").append(Messages.get("menu.izin_usaha")).append("</th></tr>");
						content.append("<tr><td><table class=\"table table-condensed\">").append("<tr><td>").append(Messages.get("menu.jenis_ijin")).append("</td><td>").append(Messages.get("menu.busbuksk")).append("</td></tr>");
						for(Checklist ijin : ijinBaruList) {
							content.append("<tr><td>").append(ijin.chk_nama).append("</td><td>").append(ijin.chk_klasifikasi).append("</td></tr>");
						}
						content.append("</table></td></tr>");
					}
					for(Checklist syarat:syaratAdminList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if(syarat.getJsonName() != null) {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("Syarat Kualifikasi Administrasi/Legalitas Lain<br/>");
							String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(syarat.chk_nama);
						} else {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);														
							if(!CommonUtil.isEmpty(syarat.chk_nama)) {
								String[] s = syarat.chk_nama.split("[\\r\\n]+");
								content.append("<i>");
								if (s != null) {
									for (String newline: s) {
										content.append(newline);
										content.append("<br/>");
									}
								} else 
									content.append(syarat.chk_nama);
								content.append("</i>");
							}
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
				if (!CollectionUtils.isEmpty(syaratTeknisList)) {
					content.append("<table class=\"table table-condensed\">");
					content.append("<h5>").append(Messages.get("menu.pkt")).append("</h5>");
					for(Checklist syarat:syaratTeknisList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if (cm.isInputNumber()) {
							int i = 0;
							for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
								if (newline.split("number") == null || newline.split("number").length < 2) {
									content.append(newline);
								} else {
									int j = 0;
									for (String number: newline.split("number")) {
										content.append(number);
										for(Map<String, String> val : syarat.getJsonName()) {
											for(String key : table.key) {
												if (key.equals("number"+i) && j == 0) 
													content.append(val.get(key));
											}
										}
										j++;
									}
								}
								content.append("<br/>");
								i++;
							}
						} else if (cm.isInputTextMulti()) {
							int i = 0;
							for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
								if (newline.split("text") == null || newline.split("text").length < 2) {
									content.append(newline);
								} else {
									int j = 0;
									for (String number: newline.split("text")) {
										content.append(number);
										for(Map<String, String> val : syarat.getJsonName()) {
											for(String key : table.key) {
												if (key.equals("text"+i) && j == 0) 
													content.append(val.get(key));
											}
										}
										j++;
									}
								}
								content.append("<br/>");
								i++;
							}
							
//							for (String newline: cm.ckm_nama.split("[\\r\\n]+")) {
//								if(lelang.getKualifikasi().isKecil()) {
//									String[] split= cm.ckm_nama.split("text");
//									content.append(split[0]);
//									for (Map<String, String> val : syarat.getJsonName()) {
//										for (String key : table.key) {
//											if (key.equals("text1"))
//												content.append(val.get(key));
//										}
//									}
//								} else {
//									int j = 1;
//									for (String text : newline.split("text")) {
//										content.append(text);
//										for (Map<String, String> val : syarat.getJsonName()) {
//											for (String key : table.key) {
//												if (key.equals("text" + j))
//													content.append(val.get(key));
//											}
//										}
//										j++;
//									}
//								}
//								content.append("<br/>");
//							}
						} else if (syarat.getJsonName() != null) {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							if(table != null) {
								content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
								content.append("<thead>");
								for (String label : table.label) {
									content.append("<th>").append(label).append("</th>");
								}
								content.append("</thead>");
								content.append("<tbody>");
								for (Map<String, String> val : syarat.getJsonName()) {
									content.append("<tr>");
									for (String key : table.key) {
										content.append("<td>").append(val.get(key)).append("</td>");
									}
									content.append("</tr>");
								}
								content.append("</tbody>");
								content.append("</table>");
							}
						} else if(cm.isSyaratLain()) {
							content.append("Syarat Kualifikasi Teknis Lain<br/>");
							String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(syarat.chk_nama);
						} else {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							if(!CommonUtil.isEmpty(syarat.chk_nama)) {
								String[] s = syarat.chk_nama.split("[\\r\\n]+");
								content.append("<i>");
								if (s != null) {
									for (String newline: s) {
										content.append(newline);
										content.append("<br/>");
									}
								} else 
									content.append(syarat.chk_nama);
								content.append("</i>");
							}
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
				if (!CollectionUtils.isEmpty(syaratKeuanganList)) {
					content.append("<h5>").append(Messages.get("menu,pkkk")).append("</h5>");
					content.append("<table class=\"table table-condensed\">");
					for(Checklist syarat:syaratKeuanganList) {
						Checklist_master cm = Checklist_master.findById(syarat.ckm_id);
						content.append("<tr><td>");
						KeyLabel table = cm.getTable();
						if(syarat.getJsonName() != null) {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							content.append("<table class=\"table table-sm table-bordered\" style=\"margin-top: 10px;\">");
							content.append("<thead>");
							for (String label : table.label) {
								content.append("<th>").append(label).append("</th>");
							}
							content.append("</thead>");
							content.append("<tbody>");
							for(Map<String, String> val : syarat.getJsonName()) {
								content.append("<tr>");
								for(String key : table.key) {
									content.append("<td>").append(val.get(key)).append("</td>");
								}
								content.append("</tr>");
							}
							content.append("</tbody>");
							content.append("</table>");
						} else if(cm.isSyaratLain()) {
							content.append("Syarat Kualifikasi Kemampuan Keuangan Lain<br/>");
							String[] newlines = syarat.chk_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(syarat.chk_nama);
						} else {
							String[] newlines = cm.ckm_nama.split("[\\r\\n]+");
							if (newlines != null) {
								for (String newline: newlines) {
									content.append(newline);
									content.append("<br/>");
								}
							} else 
								content.append(cm.ckm_nama);
							if(!CommonUtil.isEmpty(syarat.chk_nama)) {
								String[] s = syarat.chk_nama.split("[\\r\\n]+");
								content.append("<i>");
								if (s != null) {
									for (String newline: s) {
										content.append(newline);
										content.append("<br/>");
									}
								} else 
									content.append(syarat.chk_nama);
								content.append("</i>");
							}
						}					
						content.append("</td></tr>");
					}
					content.append("</table>");
				}
			}
			else { // paket dibuat dengan versi 3
				Dok_lelang dok_lelang = Dok_lelang.findBy(lelang.lls_id, JenisDokLelang.DOKUMEN_PRAKUALIFIKASI);
				if(dok_lelang == null)
					return;
				content.append("<table class=\"table table-sm\">");
				List<Checklist> syaratList = dok_lelang.getSyaratIjinUsaha();
				syaratList.addAll(dok_lelang.getSyaratKualifikasi());
				String namaSyarat = null;
				for (Checklist checklist : syaratList) {
					Checklist_master cm = Checklist_master.findById(checklist.ckm_id);
					if(cm != null)
						namaSyarat = cm.ckm_nama;
					content.append("<tr valign=\"top\">").append("<td>*</td><td>").append(namaSyarat);
					if(!CommonUtil.isEmpty(namaSyarat))
						content.append("<br />");	
					if(cm.isIjinUsaha())
					{
						content.append("<table class=\"table table-sm\">");
						content.append("<tr><th>Izin Usaha</th><th>Klasifikasi</th></tr>");
						List<Syarat_paket> list = Syarat_paket.findByPaket(lelang.pkt_id);
						for (Syarat_paket syarat : list) {
							if(CommonUtil.isEmpty(syarat.srt_nama))
								continue;
							content.append("<tr><td>").append(syarat.srt_nama).append("</td>");
							content.append("<td>").append(!CommonUtil.isEmpty(syarat.srt_klasifikasi)? syarat.srt_klasifikasi:"").append("</td></tr>");
						}
						content.append("</table>");
					}
					else
					{
						content.append(checklist.chk_nama);
					}
					content.append("</td></tr>");
				}		
				content.append("</table>");
			}			
		}
		out.print(content.toString());
	}
	
	public static void _rightContent(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
		Active_user active_user = Active_user.current();
		if(active_user== null || active_user.group == null)
			return;
		StringBuilder content = new StringBuilder("<div class=\"text-right\"><ul class=\"list-inline\">");
		if(active_user.group.isRekanan()) {
			Long rekananId = active_user.rekananId;
			long inbox = MailQueueDao.getJumlahInbox(rekananId, null);
			long inbox_sudah_dibaca = MailQueueDao.getJumlahInbox(rekananId, BACA_STATUS.READ);
			long inbox_belum_dibaca = MailQueueDao.getJumlahInbox(rekananId, BACA_STATUS.UNREAD);
			Map<String, Object> param = new HashMap<String, Object>(1);			
			content.append("<li><a href=\"").append(Router.reverse("rekanan.RekananCtr.inbox")).append("\"><b>").append(Messages.get("rightcontent.inbox")).append("<span class=\"badge\">").append(inbox).append("</span></b></a></li>");
			param.put("status", BACA_STATUS.UNREAD);
			content.append("<li><a href=\"").append(Router.reverse("rekanan.RekananCtr.inbox", param)).append("\"><b>").append(Messages.get("rightcontent.belumdibaca")).append("<span class=\"badge\">").append(inbox_belum_dibaca).append("</span></b></a></li>");
			param.put("status", BACA_STATUS.READ);
			content.append("<li><a href=\"").append(Router.reverse("rekanan.RekananCtr.inbox", param)).append("\"><b>").append(Messages.get("rightcontent.sudahdibaca")).append("<span class=\"badge\">").append(inbox_sudah_dibaca).append("</span></b></a></li>");
		}
		else if(active_user.group.isVerifikator()) {
			//value2 di bawah ini sebenarnya perlu di-cache juga, tapi karena jarang diakses; gak usah gpp
			Long baru = Rekanan.getJumlahRekananBaru();
			Long roaming = Rekanan.getJumlahRekananRoaming();
			Long tolak = Rekanan.getJumlahRekananDitolak();
			Long verified = Rekanan.getJumlahRekananTerverifikasi();
			content.append("<li><strong>Penyedia: </strong> </li>");
			content.append("<li><a href=\"").append(Router.reverse("VerifikatorCtr.rekananBaru")).append("\">").append(Messages.get("menu.baru")).append("<span class=\"badge\">").append(baru).append("</span></a></li>");
			content.append("<li><a href=\"").append(Router.reverse("VerifikatorCtr.rekananRoaming")).append("\">").append(Messages.get("menu.roaming")).append("<span class=\"badge\">").append(roaming).append("</span></a></li>");
			content.append("<li><a href=\"").append(Router.reverse("VerifikatorCtr.rekananDitolak")).append("\">").append(Messages.get("menu.ditolak")).append("<span class=\"badge\">").append(tolak).append("</span></a></li>");
			content.append("<li><a href=\"").append(Router.reverse("VerifikatorCtr.rekananTerverifikasi")).append("\">").append(Messages.get("menu.verif")).append("<span class=\"badge\">").append(verified).append("</span></a></li>");
		}	
		else if(active_user.group.isPanitia()) {
			//karena sering diakses maka masukkan ke dalam Cache
			Long panitiaId = Params.current().get("panitiaId", Long.class);
			if(panitiaId == null)
				return;
			String key="models.agency.dao.PanitiaDao.getJumlahPaketDraft(Long)" + panitiaId;
			Long[] value=Cache.get(key, Long[].class);
			if(value==null)
			{
				value=new Long[]
				{
					Panitia.getJumlahPaketDraft(panitiaId),
					Panitia.getJumlahPaketAktif(panitiaId),
					Panitia.getJumlahPaketDiulang(panitiaId)
				};
				Cache.add(key, value, "15s");
			}
			Map<String, Object> param = new HashMap<String, Object>(2);	
			Long draft = value[0];
			Long aktif = value[1];
			Long ulang = value[2];
			param.put("panitiaId", panitiaId);
			content.append("<li><a href=\"").append(Router.reverse("lelang.PaketCtr.index", param)).append("\">").append(Messages.get("menu.pkt")).append(" : </a></li>");
			param.put("status", Integer.valueOf(0));
			content.append("<li><a href=\"").append(Router.reverse("lelang.PaketCtr.index", param)).append("\">").append(Messages.get("men.draft")).append("<span class=\"badge\">").append(draft).append("</span></a></li>");
			param.put("status", Integer.valueOf(1));
			content.append("<li><a href=\"").append(Router.reverse("lelang.PaketCtr.index", param)).append("\">").append(Messages.get("menu.aktif")).append("<span class=\"badge\">").append(aktif).append("</span></a></li>");
			param.put("status", Integer.valueOf(3));
			content.append("<li><a href=\"").append(Router.reverse("lelang.PaketCtr.index", param)).append("\">").append(Messages.get("menu.diulang")).append("<span class=\"badge\">").append(ulang).append("</span></a></li>");
		}			
		content.append("</ul></div>");
		out.print(content.toString());
	}

	/**
	 * set background tatap muka aplikasi 
	 * tergantung config apakah production/latihan
	 * @param args
	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 */
	public static void _setBackground(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Boolean isLatihan = Cache.get("isLatihan", Boolean.class);
		if (isLatihan == null) {
			isLatihan = !ConfigurationDao.isProduction();
			Cache.set("isLatihan", isLatihan);
		}
		if(isLatihan)
			out.print("class=\"bg-body-latihan\"");
	}

	public static void _sesilatihan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if(!ConfigurationDao.isProduction())
		{
			SesiPelatihan sesi=BasicCtr.getSesiPelatihan();
			if(sesi != null) {
				String url = Router.reverse("UserCtr.sesiPelatihanPilih").url;
				out.print(" - <a href=\"" + url + "\" class=\"dialog\" title=\""+Messages.get("menu.msp")+"\">"+Messages.get("menu.sp")+": " + sesi.nama + "</a>");
			}
		}
	}
	
	@SuppressWarnings(value = {"unchecked"})
	public static void _tabInfoDetilLelang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
		Request request = Request.current();  
		TahapStarted tahapStarted = (TahapStarted)args.get("arg");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", tahapStarted.lelangId);
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\" role=\"tablist\">")
		.append("<li ").append(request.action.equals("lelang.LelangCtr.pengumumanLelang")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.LelangCtr.pengumumanLelang", param).url).append("\" >").append(Messages.get("menu.peserta")).append("</a></li>");
		param.remove("action");
		content.append("<li ").append(request.action.equals("lelang.PesertaCtr.index")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.PesertaCtr.index", param).url).append("\" >").append(Messages.get("menu.peserta")).append("</a></li>");
		param.remove("action");
		if(tahapStarted.isPengumuman()){
			content.append("<li ").append(request.action.equals("lelang.EvaluasiCtr.hasil")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.EvaluasiCtr.hasil", param).url).append("\">").append(Messages.get("menu.he")).append("</a></li>");
			param.remove("action");
			content.append("<li ").append(request.action.equals("lelang.EvaluasiCtr.pemenang")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.EvaluasiCtr.pemenang", param).url).append("\">").append(Messages.get("menu.pe")).append("</a></li>");
			param.remove("action");
			content.append("<li ").append(request.action.equals("lelang.EvaluasiCtr.pemenangBerkontrak")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.EvaluasiCtr.pemenangBerkontrak", param).url).append("\">").append(Messages.get("menu.pk")).append("</a></li>");
			param.remove("action");
		}				
		content.append("</ul>");
		out.print(content.toString());
	}

	public static void _tabInfoDetilPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Request request = Request.current();
		TahapStartedPl tahapStartedPl = (TahapStartedPl)args.get("arg");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", tahapStartedPl.plId);
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\" role=\"tablist\">")
				.append("<li ").append(request.action.equals("nonlelang.PengadaanLCtr.pengumumanPl")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.PengadaanLCtr.pengumumanPl", param).url).append("\" >").append(Messages.get("menu.pengumuman")).append("</a></li>");
		param.remove("action");
		content.append("<li ").append(request.action.equals("nonlelang.PesertaPlCtr.index")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.PesertaPlCtr.index", param).url).append("\" >").append(Messages.get("menu.peserta")).append("</a></li>");
		param.remove("action");
		if(tahapStartedPl.isPengumuman()){
			content.append("<li ").append(request.action.equals("nonlelang.EvaluasiPLCtr.hasil")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.EvaluasiPlCtr.hasil", param).url).append("\">").append(Messages.get("menu.he")).append("</a></li>");
			param.remove("action");
			content.append("<li ").append(request.action.equals("nonlelang.EvaluasiPLCtr.pemenang")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.EvaluasiPlCtr.pemenang", param).url).append("\">").append(Messages.get("menu.pe")).append("</a></li>");
			param.remove("action");
			content.append("<li ").append(request.action.equals("nonlelang.EvaluasiPLCtr.pemenangBerkontrak")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.EvaluasiPlCtr.pemenangBerkontrak", param).url).append("\">").append(Messages.get("menu.pk")).append("</a></li>");
			param.remove("action");
		}
		content.append("</ul>");
		out.print(content.toString());
	}
	
	@SuppressWarnings(value = {"unchecked"})
	public static void _tabmenuLelang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
		Request request = Request.current();
		TahapStarted tahapStarted = (TahapStarted)args.get("arg");
		Active_user active_user = Active_user.current();
        Long lelangId = tahapStarted.lelangId;
        Lelang_seleksi lelang = Lelang_seleksi.findById(lelangId);
		boolean show = true;
		if(lelang.isPrakualifikasi() && active_user.isRekanan()) {
			Peserta peserta = Peserta.findBy(lelangId, active_user.rekananId);
			if(peserta != null)
				show = peserta.isLulusPembuktian() && peserta.isLulusKualifikasi();
		}
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\" role=\"tablist\">")
		.append("<li ").append(request.action.equals("lelang.LelangCtr.view")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.LelangCtr.view").add("id", lelangId).url).append("\" >").append(Messages.get("menu.it")).append("</a></li>");
		if(tahapStarted.isPenjelasanPra()){			
			String ket = active_user.isPanitia() || active_user.isPpk()  ? "Pertanyaan":"Penjelasan";
			int count = active_user.isRekanan() ? Diskusi_lelang.countTanggapanPanitia(lelangId, Tahap.PENJELASAN_PRA):Diskusi_lelang.countTanggapanPeserta(lelangId, Tahap.PENJELASAN_PRA);
			content.append("<li ").append(request.action.equals("lelang.PenjelasanCtr.kualifikasi")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.PenjelasanCtr.kualifikasi").add("id", lelangId).url).append("\">").append(Messages.get("menu.pr")).append(" <span class=\"badge\" title=\"").append(ket).append("\">").append(count).append("</span></a></li>");
			String action = Router.reverse("lelang.PenjelasanCtr.kualifikasi").add("id", lelangId).url;
			boolean currentAction = request.action.equals("lelang.PenjelasanCtr.kualifikasi");
			int countPenjelasan = Diskusi_lelang.countTanggapanPanitia(lelangId, Tahap.PENJELASAN_PRA);
		}
		if(tahapStarted.isSanggahPra()){
			int count = Sanggahan.countSanggahan(lelangId, Tahap.SANGGAH_PRA);
			content.append("<li ").append(request.action.equals("lelang.SanggahanCtr.kualifikasi")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.SanggahanCtr.kualifikasi").add("id", lelangId).url).append("\">").append(Messages.get("menu.sgp")).append(" <span class=\"badge\">").append(count).append("</span></a></li>");
		}
		if (tahapStarted.isPenjelasan() && show) {
			boolean currentAction = request.action.equals("lelang.PenjelasanCtr.pengadaan");
			String action = Router.reverse("lelang.PenjelasanCtr.pengadaan").add("id", lelangId).url;
			int count = Diskusi_lelang.countTanggapanPeserta(lelangId, Tahap.PENJELASAN);
			int countPenjelasan = Diskusi_lelang.countTanggapanPanitia(lelangId, Tahap.PENJELASAN);
			content.append("<li ").append(currentAction ? "class=\"active\"" : "").append("><a href=\"").append(action).append("\">").append(Messages.get("menu.pty")).append("<span class=\"badge\" title=\""+Messages.get("menu.pty")+"\">").append(count).append("</span>").append(Messages.get("menu.dpj")).append("<span class=\"badge\" title=\""+Messages.get("menu.pj")+"\">").append(countPenjelasan).append("</span></a></li>");
		}
		if(!tahapStarted.isAuctionExpress() && (tahapStarted.isPembukaan() || tahapStarted.isEvaluasiPra()) && !active_user.isRekanan()){
			content.append("<li ").append(request.action.equals("lelang.PesertaCtr.penawaran")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.PesertaCtr.penawaran").add("id", lelangId).url).append("\">").append(Messages.get("menu.penawaran_peserta")).append("</a></li>");
		}
		if(tahapStarted.isAuction() && Paket.isFlag43(lelangId)) {
			content.append("<li ").append(request.action.equals("lelang.AuctionCtr.index") ? "class=\"active\"" : "").append("><a href=\"").append(Router.reverse("lelang.AuctionCtr.index").add("id", lelangId).url).append("\">").append(Messages.get("menu.ra")).append("</a></li>");
		}
		if((tahapStarted.isEvaluasi() || lelang.is_kualifikasi_tambahan) && !active_user.isRekanan()){
			content.append("<li ").append(request.controller.equals("lelang.EvaluasiCtr")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.EvaluasiCtr.index").add("id", lelangId).url).append("\">").append(Messages.get("menu.evaluasi")).append("</a></li>");
		}

		if(tahapStarted.isSanggahAdm() && show){
			int count = Sanggahan.countSanggahan(lelangId, Tahap.SANGGAH_ADM_TEKNIS);
			content.append("<li ").append(request.action.equals("lelang.SanggahanCtr.index")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.SanggahanCtr.admTeknis").add("id", lelangId).url).append("\">").append(Messages.get("menu.st")).append(" <span class=\"badge\">").append(count).append("</span></a></li>");
		}

		if(tahapStarted.isSanggah() && show){
			int count = Sanggahan.countSanggahan(lelangId, Tahap.SANGGAH);
			content.append("<li ").append(request.action.equals("lelang.SanggahanCtr.index")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("lelang.SanggahanCtr.index").add("id", lelangId).url).append("\">").append(Messages.get("menu.sanggahan")).append(" <span class=\"badge\">").append(count).append("</span></a></li>");
		}
		content.append("</ul>");
		out.print(content.toString());
	}
	
	@SuppressWarnings(value = {"unchecked"})
	public static void _tabmenuPL(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
		Request request = Request.current();
		TahapStartedPl tahapStartedPl = (TahapStartedPl)args.get("arg");
		Active_user active_user = Active_user.current();
        Long plId = tahapStartedPl.plId;
        Pl_seleksi pl = Pl_seleksi.findById(plId);
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", plId);
		boolean show = true;
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\" role=\"tablist\">")
		.append("<li ").append(request.action.equals("nonlelang.PengadaanLCtr.view")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.PengadaanLCtr.view", param).url).append("\" >").append(Messages.get("menu.ip")).append("</a></li>");
		
		if(pl.isPenunjukanLangsungNew()) {
			if(tahapStartedPl.isEvaluasiPra() && !active_user.isRekanan()){
				content.append("<li ").append(request.action.equals("nonlelang.PesertaPlCtr.penawaran")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.PesertaPlCtr.penawaran", param).url).append("\">").append(Messages.get("menu.penawaran_peserta")).append("</a></li>");
				content.append("<li ").append(request.controller.equals("nonlelang.EvaluasiPLCtr")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.EvaluasiPLCtr.indexpl", param).url).append("\">").append(Messages.get("menu.evaluasi")).append("</a></li>");
				param.remove("action");
			}
			if(tahapStartedPl.isPenjelasan()) {
				boolean currentAction = request.action.equals("nonlelang.PenjelasanPlCtr.pengadaan");
				String action = Router.reverse("nonlelang.PenjelasanPlCtr.pengadaan").add("id", plId).url;
				if(active_user.isPanitia() || active_user.isPP()){
					int count = Diskusi_lelangPl.countTanggapanPeserta(plId, Tahap.PENJELASAN);
					content.append("<li ").append(currentAction?"class=\"active\"":"").append("><a href=\"").append(action).append("\">").append(Messages.get("menu.pty")).append("<span class=\"badge\" title=\"").append(Messages.get("menu.pty")).append("\">").append(count).append("</span></a></li>");
				}else if(active_user.isRekanan() && show){
					int count = Diskusi_lelangPl.countTanggapanPp(plId, Tahap.PENJELASAN);
					content.append("<li ").append(currentAction?"class=\"active\"":"").append("><a href=\"").append(action).append("\">").append(Messages.get("menu.pj")).append(" <span class=\"badge\" title=\"").append(Messages.get("menu.pty")).append("\">").append(count).append("</span></a></li>");
				}else if(active_user.isPpk()){
					int count = Diskusi_lelangPl.countTanggapanPeserta(plId, Tahap.PENJELASAN);
					int countPenjelasan = Diskusi_lelangPl.countTanggapanPp(plId, Tahap.PENJELASAN);
					content.append("<li ").append(currentAction?"class=\"active\"":"").append("><a href=\"").append(action).append("\">").append(Messages.get("menu.pty")).append(" <span class=\"badge\" title=\"").append(Messages.get("menu.pty")).append("\">").append(count).append("</span></a></li>");
					content.append("<li ").append(currentAction?"class=\"active\"":"").append("><a href=\"").append(action).append("\">").append(Messages.get("menu.pj")).append(" <span class=\"badge\" title=\"").append(Messages.get("menu.pj")).append("\">").append(countPenjelasan).append("</span></a></li>");
				}
			}
		}
		else{
			if(tahapStartedPl.isPembukaan() && !active_user.isRekanan()){
				content.append("<li ").append(request.controller.equals("nonlelang.EvaluasiPLCtr")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.EvaluasiPLCtr.index", param).url).append("\">").append(Messages.get("menu.evaluasi")).append("</a></li>");
				content.append("<li ").append(request.action.equals("nonlelang.PesertaPlCtr.penawaran")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("nonlelang.PesertaPlCtr.penawaran", param).url).append("\">").append(Messages.get("menu.penawaran_peserta")).append("</a></li>");
				param.remove("action");
			}
		}
		
		content.append("</ul>");
		out.print(content.toString());
	}
    /**
     * banner di Halaman Index (Home)
     */
    public static void _banner(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
    	StringBuilder content = new StringBuilder();
    	/* Ini static link dari LKPP. Coding Sementara, ambil satu saja yang terakhir */
		BroadcastMessage staticLinkNasional = BroadcastMessageClient.findNewestStaticLink();
		if(staticLinkNasional != null) {
			content.append("<a href=\"").append(staticLinkNasional.url).append("\"><img src=\"data:image/png;base64,").append(staticLinkNasional.image_data).append("\" height=\"130px\" class=\"img-fluid\"></a>");
		} else {
			content.append("<a id=\"contact\" href=\"").append(Router.reverse("PublikCtr.kontakKami").url).append("\"><img src=\""+ConfigurationDao.CDN_URL+"/images/call-center.png\"  class=\"img-fluid\" /></a>");
		}
    	List<KontenMultimedia> list = KontenMultimedia.getLink();
    	if(!CommonUtil.isEmpty(list)) {
	    	content.append("<div id=\"banner\" class=\"carousel slide\" data-ride=\"carousel\">");
			content.append("<ol class=\"carousel-indicators\">");
			int i=0;
	    	for(KontenMultimedia banner:list) {
	    		if(!StringUtils.isEmpty(banner.path)) {
	    			content.append("<li data-target=\"#banner\" data-slide-to=\"").append(i).append("\" ").append(i == 0 ? "class=\"active\"" : "").append("></li>");
	    			i++;
	    		}
	    	}
	    	content.append("</ol>");
	    	content.append("<div class=\"carousel-inner\">");
	    	i=0;
	    	for(KontenMultimedia banner:list) {
	    		if(!StringUtils.isEmpty(banner.path)) {
	    			content.append("<div class=\"carousel-item ").append(i == 0 ? "active" : "").append("\">");
	    			content.append("<a href=\"").append(banner.description).append("\"><img src=\"").append(banner.getUrl()).append("\" alt=\"").append(banner.judul).append("\" class=\"text-center img-fluid\"> </a>");
	    			content.append("<div class=\"carousel-caption d-none d-md-block\">").append(banner.description).append("</div>");
	    			content.append("</div>");
	    			i++;
	    		}
	    	}
	    	content.append("</div>");
	    	content.append("<a class=\"carousel-control-left\" href=\"#banner\" data-slide=\"prev\"><span class=\"carousel-control-prev-icon\"></span>  </a>");
	    	content.append("<a class=\"carousel-control-right\" href=\"#banner\" data-slide=\"next\"><span class=\"carousel-control-next-icon\"></span></a>");
	    	content.append("</div>");
    	}
    	out.print(content.toString());
    }
    
    // banner slide di halaman Home (Index)
    public static void _slide(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
    	StringBuilder content = new StringBuilder();
    	List<KontenMultimedia> list = KontenMultimedia.getSlide();
    	content.append("<div id=\"carousel\" class=\"carousel slide\" data-ride=\"carousel\">");		
    	if(!CommonUtil.isEmpty(list)) {
    		content.append("<ol class=\"carousel-indicators\">");
			int i=0;
	    	for(KontenMultimedia banner:list) {
	    		if(!StringUtils.isEmpty(banner.path)) {
	    			content.append("<li data-target=\"#carousel\" data-slide-to=\"").append(i).append("\" ").append(i == 0 ? "class=\"active\"" : "").append("></li>");
	    			i++;
	    		}
	    	}
	    	content.append("</ol>");
	    	content.append("<div class=\"carousel-inner\">");
	    	i=0;
	    	for(KontenMultimedia banner:list) {
	    		if(!StringUtils.isEmpty(banner.path)) {
	    			content.append("<div class=\"carousel-item ").append(i == 0 ? "active" : "").append("\">");
	    			content.append("<img src=\"").append(banner.getUrl()).append("\" alt=\"").append(banner.judul).append("\" class=\"text-center img-fluid custom-slider\" />");
	    			content.append("<div class=\"carousel-caption d-none d-md-block\"><p>").append(banner.description).append("</p></div>");
	    			content.append("</div>");
	    			i++;
	    		}
	    	}
	    	content.append("</div>");
    	}
    	content.append("<a class=\"carousel-control-left\" href=\"#carousel\" data-slide=\"prev\"><span class=\"carousel-control-prev-icon\"></span>  </a>");
    	content.append("<a class=\"carousel-control-right\" href=\"#carousel\" data-slide=\"next\"><span class=\"carousel-control-next-icon\"></span></a>");
    	content.append("</div>");
    	out.print(content.toString());  	
    }
    
    public static void _broadcastMessage(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
    	StringBuilder content = new StringBuilder();
    	List<BroadcastMessage> nationalMsg = BroadcastMessageClient.findRunningText();
    	if(!CommonUtil.isEmpty(nationalMsg)) {
    		for(BroadcastMessage bm:nationalMsg) {
    			content.append("<li><a href=\"").append(bm.url).append("\" target=\"_blank\">[").append(FormatUtils.formatDateInd(bm.auditupdate)).append(Messages.get("menu.nasional")).append(bm.title).append(".</a></li>");
    		}
    	}
    	List<Berita> localMsg = Berita.findAllValidMarqueeMessage();
    	if(!CommonUtil.isEmpty(localMsg)) {
    		Map<String, Object> param = new HashMap<String, Object>(1);
    		for(Berita berita :localMsg) {
    			param.put("beritaId", berita.brt_id);
    			content.append("<li><a href=\"").append(Router.reverse("PublikCtr.detilPesanBerjalanPopup", param))
						.append("\" target=\"_blank\">[").append(FormatUtils.formatDateInd(berita.brt_tanggal)).append("] - ").append(berita.brt_judul).append(".</a></li>");
    		}
    	}
    	out.print(content.toString()); 
    }
    
    public static void _fileExist(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
    	BlobTable blob = (BlobTable)args.get("arg");
    	if(blob == null)
    		return;
    	if(!blob.getFile().exists()){
    		out.println("<div class=\"alert alert-danger\">"+Messages.get("menu.sftd")+"</div>");
    	}
    }

	public static void _listTenagaAhli(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
									   int fromLine){
		String[][] tenagaAhli = (String[][]) args.get("tenagaAhli");
		StringBuilder content = new StringBuilder();
		if (tenagaAhli != null){
			for (int i = 0; i < tenagaAhli.length; i++){

				content.append("<tr><td><div class=\"input-group\" style=\"width:300px;padding-top:5px;padding-bottom:5px;\">");
				content.append("<input type=\"text\" id=\"profesi[]\" name=\"tenagaAhli[").append(i).append("][0]\"class=\"form-control\" value=").append(tenagaAhli[i][0]).append(" /></div> </td>");
				content.append(" <td width=\"50px\"><div class=\"input-group\" style=\"padding-top:5px;\">");
				content.append(" <input type=\"text\" id=\"bobot[]\"name=\"tenagaAhli[").append(i).append("][1]\"class=\"form-control\" value=").append(tenagaAhli[i][1]).append(" style=\"width:80px;\"/>").append("<div class=\"input-group-append\"><span class=\"input-group-text\">%</span></div>");
				if (i != 0){
					content.append("</div></td><td><a href=\"javascript:void(0)\" class=\"removeBtn\">" +
							"<i class=\"fa fa-trash-o fa-lg\" style=\"padding: 15px 10px 10px 10px;\"></i></a></td></tr>");
				} else{
					content.append("</div></td><td></td></tr>");
				}
			}
		} else {
			content.append("<tr><td><div class=\"input-group\" style=\"width:300px;padding-top:5px;padding-bottom:5px;\">");
			content.append("<input type=\"text\" id=\"profesi[]\" name=\"tenagaAhli[0][0]\"class=\"form-control\" /></div> </td>");
			content.append(" <td width=\"50px\"><div class=\"input-group\" style=\"padding-top:5px;\">");
			content.append(" <input type=\"text\" id=\"bobot[]\"name=\"tenagaAhli[0][1]\"class=\"form-control\" style=\"width:80px;\"/>" +
					"<div class=\"input-group-append\"><span class=\"input-group-text\">%</span></div>");
			content.append("</div></td><td></td></tr>");
		}
		out.print(content.toString());
	}

	public static void _listTenagaAhliDoc(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template,
									   int fromLine){
		String[][] tenagaAhli = (String[][]) args.get("tenagaAhli");
		StringBuilder content = new StringBuilder();
		content.append("<ol type=\"1\">");
		for (int i = 0; i < tenagaAhli.length; i++){
			String profesi = tenagaAhli[i][0].substring(1);
			String bobot = tenagaAhli[i][1].substring(1);
			content.append("<li>").append(profesi.substring(0, profesi.length() - 1)).append(", diberi bobot = ").append(bobot.substring(0, bobot.length() - 1)).append("%</li>");
		}

		content.append("</ol>");
		out.print(content.toString());
	}
	
	/**
	 * Fungsi {@code _headerImage} merupakan custom tag Play!Framework yang digunakan untuk render header yang terdiri dari log dan nama LPSE.
	 * Tag ini dibutuhkan karena ada kebutuhan bahwa image disimpan di database untuk mode DEV
	 *
	 * @param args standar fast tag
	 * @param body standar fast tag
	 * @param out standar fast tag
	 * @param template standar fast tag
	 * @param fromLine standar fast tag
	 */
	@SuppressWarnings(value = {"unchecked"})
	public static void _headerImage(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {	
    	String jenis = args.get("jenis").toString();
    	KontenMultimedia.JenisKonten jenisKonten = KontenMultimedia.JenisKonten.valueOf(jenis);
    	KontenMultimedia konten = KontenMultimedia.find(jenisKonten);
		String imgSrc = BasicCtr.CONTEXT_PATH + konten.path;
//		String imgSrcNama = BasicCtr.CONTEXT_PATH + "/public/images/imgng/lpse-nama.png";
		if(Play.mode==Mode.DEV) {
			try {
				byte[] bytes = KontenMultimedia.getDevKonten(jenisKonten);
				if(bytes != null) {
					imgSrc = "data:image/png;base64,\n" + Base64.encodeBase64String(bytes);
//					is.close();
				}
			} catch (Exception e) {
				Logger.info(e, "DEV MODE ERROR: ");
			}
		}
		out.println(imgSrc);
	}

	// tag untuk informasi sesi
	public static void _sesiInfo(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		if (ConfigurationDao.isProduction())
			return;
		Integer sesi = (Integer) args.get("arg");
		if (sesi == null || sesi.intValue() == 0)
			out.println("<span class=\"label label-info\">"+Messages.get("menu.sd")+"</span>");
		else
			out.println("<span class=\"label label-info\">"+Messages.get("menu.session")+" " + sesi + "</span>");
	}

	public static void _printHtmlToText(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String content = (String)args.get("arg");
		if(null != content)
		out.print(content.toString());
	}
}