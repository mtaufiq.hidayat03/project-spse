package ext;

import org.apache.commons.lang3.StringUtils;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class DecimalBinder implements TypeBinder<Double> {

	@Override
	public Object bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
		 if(StringUtils.isEmpty(value))
			return null;
		value = value.replaceAll(",", ".");
		return Double.valueOf(value);
	}

}
