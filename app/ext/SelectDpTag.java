package ext;

import groovy.lang.Closure;
import models.LOVItem;
import models.agency.Paket_pl;
import models.agency.Paket_swakelola;
import models.agency.Pegawai;
import models.agency.Ukpbj;
import models.common.*;
import models.devpartner.LelangDp;
import models.devpartner.common.JenisKontrakDp;
import models.devpartner.common.KontrakTahunDp;
import models.devpartner.common.SumberDanaDp;
import models.devpartner.panel.Panel;
import models.devpartner.pojo.LOVItemDp;
import models.jcommon.util.DateUtil;
import models.lelang.Lelang_seleksi;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.TipeSwakelola;
import models.nonlelang.nonSpk.JenisRealisasiNonSpk;
import models.rekanan.TipePenyedia;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.i18n.Messages;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SelectDpTag extends FastTags {
	public static void _selectCopyJadwalDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");	
		LelangDp lelang = (LelangDp) args.get("lelang");
		List<LOVItem> lelangCopyList = LOVItemDp.getLelangSamaMetodeYangHanyaAdaDiJadwal(lelang.lls_id, lelang.getMetode());
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control selectpicker input-sm\" data-live-search=\"true\" data-width=\"80%\">");
		content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
		for(LOVItem obj:lelangCopyList) {
			if(obj.label.length() > 125) {
				int index = obj.label.indexOf(" ", 125);
				if(index >= 0){
					obj.label = obj.label.substring(0, index).concat("...");
				}
			}
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ?
					"selected=\"selected\"" : "").append('>').append(obj.id).append(" - ").append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectKontrakPembayaranDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		LelangDp lelang = (LelangDp) args.get("lelang");

		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append(" >");
		List<JenisKontrakDp> list = lelang.getMetode().jenisKontrakList;
		for (JenisKontrakDp item:list) {
			content.append("<option value=\"" ).append(item.id).append( "\" " ).append(value != null && item.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(Messages.get(item.label)).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectKontrakTahunDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");

		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append(" >");
		KontrakTahunDp[] list = KontrakTahunDp.values();
		for (KontrakTahunDp item:list) {
			content.append("<option value=\"" ).append(item.id).append( "\" " ).append(value != null && item.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(Messages.get(item.label)).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectSumberDanaDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		LelangDp lelang = (LelangDp) args.get("lelang");

		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append(" >");
		List<SumberDanaDp> list = lelang.getMetode().sumberDanaList;
		for (SumberDanaDp item:list) {
			content.append("<option value=\"" ).append(item.id).append( "\" " ).append(value != null && item.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(item.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectPanel(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");

		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append(" >");
		List<Panel> list = Panel.findAll();
		for (Panel item:list) {
			content.append("<option value=\"" ).append(item.pnl_id).append( "\" " ).append(value != null && item.pnl_id.equals(value) ? "selected=\"selected\"" : "").append('>').append(item.nama_panel).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
}
