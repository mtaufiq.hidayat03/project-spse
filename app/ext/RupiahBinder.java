package ext;

import org.apache.commons.lang3.StringUtils;
import play.data.binding.Global;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Mengubah params string menjadi double, string boleh dalam format angka saja
 * atau format rupiah misal: Rp 1.500.000,00
 *
 * @author arifk
 */
@Global
public class RupiahBinder implements TypeBinder<Double> {

	@Override
	public Object bind(String name, Annotation[] annotations, String value,  Class actualClass, Type genericType) throws Exception {
		if(StringUtils.isEmpty(value))
			return null;
		return FormatUtils.parseRupiah(value);
	}
}
