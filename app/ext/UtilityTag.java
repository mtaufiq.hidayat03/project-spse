package ext;

import groovy.lang.Closure;
import jobs.ServiceStatusJob;
import models.common.ConfigurationDao;
import models.common.ServisStatus;
import models.rekanan.Rekanan;
import models.sso.common.RekananSSO;
import org.apache.commons.collections4.CollectionUtils;
import play.i18n.Messages;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import utils.osd.CertificateUtil;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 8/23/2018
 */
public class UtilityTag extends FastTags {

    public static void _serviceStatus(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        StringBuilder content = new StringBuilder();
        content.append("<div style=\"margin-top: 5px;\">");
        content.append("<span class=\"").append(ServisStatus.adp.status ? "badge badge-success":"badge badge-danger").append("\" ")
                .append("style=\"margin-right: 5px;\">").append(ServisStatus.adp.jenis +" "+ServisStatus.adp.statusLabel).append("</span>");
        content.append("<span class=\"").append(ServisStatus.inaproc.status ? "badge badge-success":"badge badge-danger").append("\" ")
                .append("style=\"margin-right: 5px;\">").append(ServisStatus.inaproc.jenis +" "+ServisStatus.inaproc.statusLabel).append("</span>");
        content.append("<span class=\"").append(ServisStatus.sikap.status ? "badge badge-success":"badge badge-danger").append("\" ")
                .append("style=\"margin-right: 5px;\">").append(ServisStatus.sikap.jenis +" "+ServisStatus.sikap.statusLabel).append("</span>");
        content.append("<span class=\"").append(ServisStatus.sirup.status ? "badge badge-success":"badge badge-danger").append("\" ")
                .append("style=\"margin-right: 5px;\">").append(ServisStatus.sirup.jenis +" "+ServisStatus.sirup.statusLabel).append("</span>");
        if(CertificateUtil.enableCa) {
            content.append("<span class=\"").append(ServisStatus.ams.status ? "badge badge-success":"badge badge-danger").append("\" ")
                    .append("style=\"margin-right: 5px;\">").append(ServisStatus.ams.jenis +" "+ServisStatus.ams.statusLabel).append("</span>");
            content.append("<span class=\"").append(ServisStatus.kms.status ? "badge badge-success":"badge badge-danger").append("\" ")
                    .append("style=\"margin-right: 5px;\">").append(ServisStatus.kms.jenis +" "+ServisStatus.kms.statusLabel).append("</span>");
        }
        if(ServiceStatusJob.lastCheck != null) {
            content.append("<p style=\"margin-top: 5px;\">"+Messages.get("ct.ut")+" ")
                    .append(FormatUtils.format(ServiceStatusJob.lastCheck, "dd-MM-yyyy HH:mm"))
                    .append(", "+Messages.get("ct.stm")+"</p>");
        }
        content.append("</div>");
        out.print(content.toString());
    }

    public static void _chekDuplikasi(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
        Rekanan rekanan = (Rekanan) args.get("arg");
        if(rekanan == null)
            return;
        List<RekananSSO> list = Rekanan.getListDuplikasi(rekanan);
        if(!CollectionUtils.isEmpty(list)){
            out.print("<div class=\"alert alert-danger\">"+ Messages.get("penyedia.verifikasi.kesamaan.data")+"</div>");
            out.print("<table class=\"table table-condensed table-bordered\">");
            out.print("<thead><tr><th>LPSE</th><th>"+Messages.get("ct.user_id")+"</th><th>"+Messages.get("ct.nama_perusahaan")+"</th><th>"+Messages.get("ct.alamat")+"</th><th>NPWP</th><th>Email</th><th>"+Messages.get("ct.status")+"</th></tr></thead>");
            out.print("<tbody>");
            for(RekananSSO obj:list) {
                out.print("<tr>");
                out.print("<td>"+ ConfigurationDao.getNamaRepo(obj.repo_id)+"</td>");
                out.print("<td>"+obj.rkn_namauser+"</td>");
                out.print("<td>"+obj.rkn_nama+"["+obj.rkn_statcabang+"]</td>");
                out.print("<td>"+obj.rkn_alamat+"</td>");
                out.print("<td>"+obj.rkn_npwp+"</td>");
                out.print("<td>"+obj.rkn_email+"</td>");
                out.print("<td>"+obj.getStatus()+"</td>");
                out.print("</tr>");
            }
            out.print("</tbody>");
            out.print("</table>");
            out.print("<p>"+Messages.get("sistem.mengidentifikasi")+" <br/> "+Messages.get("harap.dikonfirmasikan.ke.penyedia")+"</p>");
        } else if(rekanan.isPenyediaBaru()){
            out.print("<div class=\"bs-callout bs-callout-warning\">"+Messages.get("ct.penyedia_sedang_verif")+"</div>");
        }
    }

}
