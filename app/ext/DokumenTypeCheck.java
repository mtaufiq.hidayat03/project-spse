package ext;

import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.tika.Tika;
import play.Logger;
import play.data.validation.Validation;

import java.io.File;

/**
 * tipe file check sebelum disimpan ke database
 * @author Arief Ardiyansah
 *
 */
public class DokumenTypeCheck extends AbstractAnnotationCheck<DokumenType> {

	static final String mes = "validation.dokumenType";
	
	//http://www.freeformatter.com/mime-types-list.html
	public static final String[] pattern = new String[]{"text/plain","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document",
								"application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/pdf",
								"image/jpeg", "image/x-citrix-jpeg","image/png", "image/x-citrix-png", "image/x-png","application/zip","application/x-rar-compressed"};
	
	public static String allowedExt = "*.doc, *.docx, *.xls, *.xlsx, *.pdf, *.jpg, *.jpeg, *.png, *.zip atau *.rar";
	static final Tika tika = new Tika();
			
	@Override
	public void configure(DokumenType dokumen) {
		setMessage(dokumen.message());
	}

	@Override
	public boolean isSatisfied(Object validatedObject, Object value, OValContext context, Validator validator) throws OValException {
		value = Validation.willBeValidated(value);
        if (value == null || value.toString().length() == 0) {
            return true;
        }

		File file = (File) value;

		// use apache tika library
		// jika file ekstensi diubah, tetap dapat membaca content type aslinya
		String mimeType = null;
		try {
			mimeType = tika.detect(file);
		} catch (Exception e) {
			Logger.error(e, "DokumenTypeCheck:%s", e.getMessage());
		}

		if(mimeType == null)
			return true;

        return ArrayUtils.contains(pattern, mimeType);
	}

}
