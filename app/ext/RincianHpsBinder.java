package ext;

import models.agency.Rincian_hps;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Class ini dibuat untuk kebutuhan konversi Penawaran Peserta dan Rincian item Pokja
 */

public class RincianHpsBinder implements TypeBinder<Rincian_hps[]> {

    @Override
    public Object bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        if(StringUtils.isEmpty(value))
            return null;
        return Rincian_hps.fromJson(value);
    }
}
