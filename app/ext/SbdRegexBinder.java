package ext;

import org.apache.commons.lang3.StringUtils;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Binder untuk Penanganan konten SBD
 * Created by Arief Ardiyansah on 3/5/16.
 * tab yang diperiksa untuk karakter yang tidak boleh adalah Lembar Data Kualifikasi, Lembar Data Pemilihan, dan SSKK
 * karakter yang diizinkan adalah huruf, angka, koma, titik, spasi, dan minus
 */
public class SbdRegexBinder implements TypeBinder<String> {

    private static final String REGEX = "[^a-zA-Z0-9.,\\-\\s ]";

    @Override
    public Object bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        if(StringUtils.isEmpty(value))
            return value;
        return value.replaceAll(REGEX, "");
    }
}
