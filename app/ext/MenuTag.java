package ext;

import groovy.lang.Closure;
import models.common.Active_user;
import models.common.ConfigurationDao;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.Map;

/**
 * @author HanusaCloud on 5/4/2018
 */
public class MenuTag extends FastTags {

    enum TabMenu {

        PENCATATAN_NONTENDER ("Pencatatan Non Tender","nonlelang.PaketPlCtr.indexNonSpk"),
        PENCATATAN_SWAKELOLA("Pencatatan Swakelola","swakelola.PaketSwakelolaCtr.index"),
        NON_TENDER("Non Tender", "nonlelang.PaketPlCtr.index"),
        NON_TENDER_PL("Non Tender", "nonlelang.PaketPlCtr.indexPenunjukanLangsung"),
        TENDER("Tender", "lelang.PaketCtr.index"),
        H_TENDER("Tender", "lelang.LelangCtr.index"),
        H_PENCATATAN_NONTENDER ("Pencatatan Non Tender","nonSpk.NonSpkCtr.index"),
        H_PENCATATAN_SWAKELOLA("Pencatatan Swakelola","swakelola.SwakelolaCtr.index"),
        H_NON_TENDER("Non Tender", "nonlelang.PengadaanLCtr.index");

        public final static TabMenu[] PPK_MENU = ConfigurationDao.isEnableDevPartner() ?
                new TabMenu[] {TENDER, NON_TENDER, PENCATATAN_NONTENDER, PENCATATAN_SWAKELOLA, DEV_PARTNER} :
                new TabMenu[] {TENDER, NON_TENDER, PENCATATAN_NONTENDER, PENCATATAN_SWAKELOLA};
        public final static TabMenu[] PP_MENU = new TabMenu[] {NON_TENDER};
        public final static TabMenu[] PANITIA_MENU = new TabMenu[] {TENDER, NON_TENDER_PL};
        public final static TabMenu[] DFAULT_MENU = new TabMenu[] {TENDER, NON_TENDER};
        public final static TabMenu[] CARI_MENU = new TabMenu[] {H_TENDER, H_NON_TENDER, H_PENCATATAN_NONTENDER, H_PENCATATAN_SWAKELOLA};

        public final String caption;
        public final String action;
        TabMenu(String caption, String action) {
            this.caption = caption;
            this.action = action;
        }

        public  String getCaption(){
            return Messages.get(caption);
        }
    }

    public static void _paketTabMenu(Map<?, ?> args,
                                     Closure body,
                                     PrintWriter out,
                                     GroovyTemplate.ExecutableTemplate template,
                                     int fromLine) {
        Http.Request request = Http.Request.current();

        StringBuilder content = new StringBuilder();
        Active_user active_user = Active_user.current();
        TabMenu[] menus = TabMenu.DFAULT_MENU;
        if (active_user != null && active_user.isPpk()) {
            menus = TabMenu.PPK_MENU;
        } else if (active_user != null && active_user.isPP()) {
            menus = TabMenu.PP_MENU;
        } else if (active_user != null && active_user.isPanitia()){
           menus = TabMenu.PANITIA_MENU;
        }
        content.append("<ul id=\"tabs\" class=\"nav nav-tabs\" role=\"tablist\">");
        for (TabMenu menu : menus) {
            content.append("<li class=\"nav-item\"><a class=\"nav-link ").append(menu.action.equals(request.action) ? "active":"")
                    .append("\" href=\"").append(Router.reverse(menu.action)).append("\">").append(menu.caption).append("</a>")
                    .append("</li>");
        }
        content.append("</ul>");
        out.print(content.toString());
    }

    public static void _cariTabMenu(Map<?, ?> args,
                                     Closure body,
                                     PrintWriter out,
                                     GroovyTemplate.ExecutableTemplate template,
                                     int fromLine) {
        Http.Request request = Http.Request.current();

        StringBuilder content = new StringBuilder();
        TabMenu[] menus = TabMenu.CARI_MENU;
        content.append("<ul id=\"tabs\" class=\"nav nav-tabs\" role=\"tablist\">");
        for (TabMenu menu : menus) {
            content.append("<li class=\"nav-item\"><a class=\"nav-link ").append(menu.action.equals(request.action) ? "active":"")
                    .append("\" href=\"").append(Router.reverse(menu.action)).append("\">").append(menu.caption).append("</a>")
                    .append("</li>");
        }
        content.append("</ul>");
        out.print(content.toString());
    }

}
