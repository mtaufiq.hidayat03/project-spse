package ext;

import groovy.lang.Closure;
import models.LOVItem;
import models.agency.Paket_pl;
import models.agency.Paket_swakelola;
import models.agency.Pegawai;
import models.agency.Ukpbj;
import models.common.*;
import models.jcommon.util.DateUtil;
import models.lelang.Lelang_seleksi;
import models.nonlelang.Pl_seleksi;
import models.nonlelang.TipeSwakelola;
import models.nonlelang.nonSpk.JenisRealisasiNonSpk;
import models.rekanan.TipePenyedia;
import models.secman.Group;
import org.apache.commons.lang3.StringUtils;
import play.i18n.Messages;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SelectTag extends FastTags {
	
	public static void _selectPropinsi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");		
		Boolean disabled = (Boolean) args.get("disabled");		
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"propinsi\" data-placeholder=\"Pilih Provinsi\" name=\"").append(name).append("\" class=\"form-control input-sm selectpicker propinsi\" data-live-search=\"true\" data-width=\"auto\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append("value = ''").append(value == null ? "selected":"").append(">"+Messages.get("tag.pilih_provinsi")+"</option>");
	    for (LOVItem propinsi:LOVItem.getPropinsi()) {
	    	content.append("<option value=\"" ).append( propinsi.id).append( "\" " ).append(value != null && propinsi.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(propinsi.label).append("</option>");      		
		}        	    
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectKabupaten(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Long propinsiId = (Long)args.get("propinsiId");		
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"kabupaten\" data-placeholder=\"Pilih Kabupaten\" name=\"").append(name).append("\" class=\"form-control input-sm selectpicker kabupaten\" data-live-search=\"true\" data-width=\"auto\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append("value = ''").append(value == null ? "selected":"").append(">"+Messages.get("tag.pilih_kabkota")+"</option>");
	    for (LOVItem kabupaten:LOVItem.getKabupaten(propinsiId)) {
	    	content.append("<option value=\"" ).append( kabupaten.id).append( "\" " ).append(value != null && kabupaten.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(kabupaten.label).append("</option>");      		
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectInstansi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		boolean isRequired = args.containsKey("required") && (Boolean) args.get("required");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"instansi\" name=\"")
				.append(name).append("\" class=\"form-control  selectpicker\" data-live-search=\"true\" data-width=\"auto\"")
				.append((disabled ? "disabled":"")).append((isRequired ? "required" : "")).append('>');
		content.append("<option value=\"\" ").append(value != null ? "selected":"").append(">"+Messages.get("tag.pilih_klpd")+"</option>");
	    for (LOVItem instansi:LOVItem.getInstansi()) {
	    	content.append("<option value=\"" ).append( instansi.id).append( "\" " ).append(value != null && instansi.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(instansi.label).append("</option>");      		
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectInstansiPpk(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"instansi\" name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append(value != null ? "selected":"").append(">"+Messages.get("tag.pilih_instansi")+"</option>");
		for (LOVItem instansi:LOVItem.getInstansi()) {
			content.append("<option value=\"" ).append( instansi.id).append( "\" " ).append(value != null && instansi.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(instansi.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectInstansiKabupaten(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");
		Long kbpId = (Long)args.get("kbpId");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"instansi\" name=\"").append(name).append("\" class=\"form-control input-sm selectpicker\" data-live-search=\"true\" data-width=\"auto\"").append((disabled ? "disabled":"")).append('>');
		content.append("<option value=\"\" ").append(value != null ? "selected":"").append(">"+Messages.get("tag.pilih_instansi")+"</option>");
		for (LOVItem instansi:LOVItem.getInstansiByKabupaten(kbpId)) {
			content.append("<option value=\"" ).append( instansi.id).append( "\" " ).append(value != null && instansi.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(instansi.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectPaketRup(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		String instansiId = (String) args.get("instansiId");
		Long satkerId = (Long) args.get("satkerId");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"paketRup\" data-placeholder=\"Pilih Paket RUP\" name=\"").append(name).append("\" class=\"form-control input-sm paketRup\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append(value == null ? "selected":"").append(">"+Messages.get("tag.pilih_paket_rup")+"</option>");
		 for (LOVItem paketRup:LOVItem.getPaketRupByKldiAndSatker(instansiId, satkerId)) {
	    	content.append("<option value=\"" ).append( paketRup.id).append( "\" " ).append(value != null && paketRup.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(paketRup.label).append("</option>");      		
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectKategori(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		String blankLabel = (String)args.get("blankLabel");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\"  style=\"width:250px;\" class=\"form-control  selectpicker\" data-width=\"auto\"	 title=\"Pilih Kategori\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		Kategori[] list = Kategori.all;
	    for (Kategori kategori:list) {
	    	content.append("<option value=\"" ).append(kategori.id).append('"').append(value != null && kategori.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kategori.getNama()).append("</option>");
		}        
	    if(!StringUtils.isEmpty(blankLabel))
	    	content.append("<option>").append(blankLabel).append("</option>");
	    content.append("</select>");
	    out.print(content.toString());
	}


	/* disesuaikan untuk metode pengadaan RUP */
	public static void _selectPemilihanPenyedia(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		StringBuilder content = new StringBuilder("<select id=\"metodepemilihan\" name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control input-sm\" data-live-search=\"true\" data-width=\"auto\" ").append('>');
		content.append("<option value=\"0\">"+Messages.get("tag.semua_metode_pemilihan")+"</option>");
		for (MetodePemilihanPenyedia obj:MetodePemilihanPenyedia.listMetodePemilihanTender) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.getLabel()).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectPemilihan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		Kategori kategori = (Kategori) args.get("kategori");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean)args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		MetodePemilihan[] list = MetodePemilihan.getByKategori(kategori);
	    for (MetodePemilihan obj:list) {
	    	content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectPemilihanPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		StringBuilder content = new StringBuilder("<select id=\"metodepemilihan\" name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control input-sm\" ").append('>');
		content.append("<option value=\"0\">"+Messages.get("tag.semua_metode_pemilihan")+"</option>");
	    for (MetodePemilihanPenyedia obj:MetodePemilihanPenyedia.listMetodePemilihanNonTender) {
	    	content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.getLabel()).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectPemilihanNonSpk(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		StringBuilder content = new StringBuilder("<select id=\"metodepemilihan\" name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control \" ").append('>');
		for (MetodePemilihanPenyedia obj:MetodePemilihanPenyedia.listMetodePemilihanPencatatan) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectPemilihanNonTender(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		Kategori kategori = (Kategori) args.get("kategori");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean)args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		MetodePemilihanPenyedia[] list = MetodePemilihanPenyedia.getNonByKategori(kategori);
		for (MetodePemilihanPenyedia obj:list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.getLabel()).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectPemilihanPlFilter(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean)args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control input-sm selectpicker\" data-width=\"auto\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		content.append("<option value=\"\" ").append((value == null ? "selected":"")).append(">"+Messages.get("tag.semua")+"</option>");
		MetodePemilihanPenyedia[] list = MetodePemilihanPenyedia.listMetodePemilihanNonTender;
		for (MetodePemilihanPenyedia obj:list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && value.equals(obj.id.longValue()) ? "selected=\"selected\"" : "").append('>').append(obj.getLabel()).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectPemilihanNonSpk(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		Kategori kategori = (Kategori) args.get("kategori");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean)args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control input-sm\" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		MetodePemilihanPenyedia[] list = MetodePemilihanPenyedia.getPencatatanByKategori(kategori);
		for (MetodePemilihanPenyedia obj:list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.getLabel()).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectBuktiPembayaran(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean)args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:200px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
//		Non_spk_seleksi.BuktiPembayaran[] list = Non_spk_seleksi.BuktiPembayaran.getBuktiPembayaran();
		JenisRealisasiNonSpk.BuktiPembayaran[] list = JenisRealisasiNonSpk.BuktiPembayaran.getBuktiPembayaran();
		for (JenisRealisasiNonSpk.BuktiPembayaran obj:list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectKontrakPembayaran(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		boolean isConsultant = args.containsKey("isConsultant") && (Boolean) args.get("isConsultant");
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append(" >");
		JenisKontrak[] list = JenisKontrak.getByCaraPembayaran(isConsultant);
	    for (JenisKontrak kontrak:list) {
	    	content.append("<option value=\"" ).append(kontrak.id).append( "\" " ).append(value != null && kontrak.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kontrak.label).append("</option>");
		}
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectKontrakTahunAnggaran(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((disabled ? "disabled":"")).append(" >");
		JenisKontrak[] list = JenisKontrak.getByBebanTahunAnggaran();
	    for (JenisKontrak kontrak:list) {
	    	content.append("<option value=\"" ).append(kontrak.id).append( "\" " ).append(value != null && kontrak.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kontrak.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectKontrakSumberDana(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((disabled ? "disabled":"")).append(" >");
		JenisKontrak[] list = JenisKontrak.getBySumberDana();
	    for (JenisKontrak kontrak:list) {
	    	content.append("<option value=\"" ).append(kontrak.id).append( "\" " ).append(value != null && kontrak.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kontrak.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectKontrakJenisPekerjaan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((disabled ? "disabled":"")).append(" >");
		JenisKontrak[] list = JenisKontrak.getByJenisPekerjaan();
	    for (JenisKontrak kontrak:list) {
	    	content.append("<option value=\"" ).append(kontrak.id).append( "\" " ).append(value != null && kontrak.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kontrak.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectKualifikasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String) args.get("value");
		Boolean disabled = (Boolean)args.get("disabled");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");		
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append("  ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append((value == null ? "selected":"")).append("></option>");
	    for (Kualifikasi kualifikasi:Kualifikasi.values()) {
	    	if(kualifikasi == Kualifikasi.KECIL_NON_KECIL || kualifikasi == Kualifikasi.MENENGAH || kualifikasi == Kualifikasi.BESAR)
	    		continue;
	    	content.append("<option value=\"" ).append(kualifikasi.id).append( "\" " ).append(value != null && kualifikasi.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kualifikasi.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectKualifikasiJk(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String) args.get("value");
		Boolean disabled = (Boolean)args.get("disabled");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");		
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append("  ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append((value == null ? "selected":"")).append("></option>");
	    for (Kualifikasi kualifikasi:Kualifikasi.values()) {
	    	if(kualifikasi == Kualifikasi.KECIL_NON_KECIL || kualifikasi == Kualifikasi.NON_KECIL)
	    		continue;
	    	content.append("<option value=\"" ).append(kualifikasi.id).append( "\" " ).append(value != null && kualifikasi.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(kualifikasi.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	/**
	 * 
	 * data satker dari rup (Pusat)
	 * hanya dipakai saat pembuatan paket
	 */
	public static void _selectSatkerPusat(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String instansiId= (String)args.get("instansiId");
		Integer tahun = (Integer)args.get("tahun");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		boolean isRequired = args.containsKey("required") && (Boolean) args.get("required");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"satker\" name=\"")
				.append(name).append("\" class=\"form-control  selectpicker\" data-live-search=\"true\" data-width=\"500px\" ")
				.append((disabled ? "disabled":""))
				.append((isRequired ? "required" : ""))
				.append('>');
		content.append("<option value=\"\" ").append(value != null ? "selected":"").append(">"+Messages.get("tag.pilih_workunit")+"</option>");
        if(instansiId != null) {
            for (LOVItem obj:LOVItem.getSatkerPusat(instansiId, tahun)) {
                content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(obj.label).append("</option>");
            }
        }
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectSatkerAll(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		StringBuilder content = new StringBuilder("<select id=\"satker\" data-placeholder=\"Pilih Satuan Kerja\" data-live-search=\"true\" data-width=\"300px\" name=\"").append(name).append("\" class=\"form-control \">");
		content.append("<option></option>");
		for (LOVItem obj:LOVItem.getSatkerAll()) {
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectSatker(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Long agencyid = Active_user.current().agencyId;		
		StringBuilder content = new StringBuilder("<select id=\"satker\" data-placeholder=\"Pilih Satuan Kerja\" name=\"").append(name).append("\" class=\"form-control \">");
		content.append("<option></option>");
		for (LOVItem obj:LOVItem.getSatkerAgency(agencyid)) {
	        content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(obj.label).append("</option>");
		}
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectSatkerAgency(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long agencyId= (Long)args.get("agencyId");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
	
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"satker\" name=\"").append(name).append("\" class=\"form-control \" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option></option>");
        if(agencyId != null) {
            for (LOVItem obj:LOVItem.getSatkerAgency(agencyId)) {            	
                content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(obj.label).append("</option>");           
            }
        }
	    content.append("</select>");
	    out.print(content.toString());
	} 
	
	public static void _selectPpk(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select style=\"width:400px;\" name=\"").append(name).append("\" class=\"form-control  ppk\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option>&nbsp;</option>");
	    for (LOVItem obj:LOVItem.getPegawaiPpk()) {
	    	content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	public static void _selectSumberDana(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");		
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select data-placeholder=\"Pilih Sumber Dana\" name=\"").append(name).append("\" class=\"form-control  sumber_dana\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
	    for (LOVItem obj:LOVItem.getSumberDana()) {
	    	content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

    public static void _selectMetodeNonEproc(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Object name = args.get("arg");
        String value = (String)args.get("value");
        Boolean disabled = (Boolean) args.get("disabled");
        if(disabled == null)
            disabled = Boolean.FALSE;
        StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control  sumber_dana\" ").append((disabled ? "disabled":"")).append('>');
        content.append("<option>&nbsp;</option>");
        for (LOVItem obj:LOVItem.getMetodeNonEproc()) {
            content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
        }
        content.append("</select>");
        out.print(content.toString());
    }

	public static void _selectTahun(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer)args.get("value");
		Integer start = (Integer)args.get("start");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append(value == null ? "selected":"").append(">&nbsp;</option>");
		int thnNow = DateUtil.getTahunSekarang();
		if(start == null)
			start = thnNow - 5;
		for (int i = start; i<=thnNow+2;i++) {			
			content.append("<option value=\"" ).append( i).append( "\" " ).append(value != null && Integer.valueOf(i).equals(value) ? "selected=\"selected\"" : "").append('>').append(i).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectTahunAnggaran(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		StringBuilder content = new StringBuilder("<select id=\"tahun\" name=\"").append(name).append("\" class=\"form-control  selectpicker\" data-live-search=\"true\" data-width=\"auto\" ").append((disabled!=null && disabled ? "disabled":"")).append('>');
		int tahunNow = DateUtil.getTahunSekarang();
		if(value == null){
			value = tahunNow;
		}
		// default mulai 2015 sampai tahun +1
		for (int i = 2015; i <= tahunNow + 1; i++) {	
			content.append("<option value=\"" ).append( i).append( "\" " ).append(Integer.valueOf(i).equals(value) ? "selected=\"selected\"" : "").append('>').append(i).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectJabatan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");
		String content = "<select name=\"" + name + "\" class=\"form-control \">" +
				"<option " + (value == null ? "selected" : "") + ">&nbsp;</option>" +
				"<option value=\"K\" " + (value.equals("K") ? "selected" : "") + ">"+Messages.get("tag.ketua")+"</option>" +
				"<option value=\"W\" " + (value.equals("W") ? "selected" : "") + ">"+Messages.get("tag.wakil")+"</option>" +
				"<option value=\"S\" " + (value.equals("S") ? "selected" : "") + ">"+Messages.get("tag.sekretaris")+"</option>" +
				"<option value=\"A\" " + (value.equals("A") ? "selected" : "") + ">"+Messages.get("tag.anggota")+"</option>" +
				"</select>";
		out.print(content);
	}
	
	public static void _selectGroup(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		boolean required = args.containsKey("required") && (Boolean) args.get("required");
		Group value=null;
		if(args!=null && args.get("value")!=null) {
			if (args.get("value").getClass().equals(String.class)){
				value = Group.valueOf((String)args.get("value"));
			}
			else{
				value = (Group)args.get("value");
			}
		}
		Active_user active_user = Active_user.current();
		if(active_user == null)
			return;
		Group[] groups = null;
		if(active_user.isAdminAgency())
			groups = Group.groupsAdminAgency;
		else if (active_user.isAdminPPE())
			groups = Group.groupsAdminPPE;
		StringBuilder content = new StringBuilder("<select data-placeholder=\"Pilih Sumber Dana\" name=\"")
				.append(name)
				.append("\" class=\"form-control  sumber_dana\"");
		if (required) {
			content.append("required=\"required\"");
		}
		content.append(">");
		content.append("<option value=\"\"></option>");
		for(Group group:groups) {
			if(ConfigurationDao.isProduction() && group.isTrainer()) // trainer tidak bisa dibuat, dalam mode production
				continue;
			content.append("<option value=\"").append(group.name()).append("\" ")
					.append((value == group ? "selected":"")).append('>')
					.append(Messages.get(group.getLabel())).append("</option>");
		}
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectAdminAgency(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Long agencyId = Active_user.current().agencyId;
		StringBuilder content = new StringBuilder("<select required name=\"").append(name).append("\" class=\"form-control  selectpicker adminAgencies\" data-live-search=\"true\" data-width=\"auto\">");
		content.append("<option ").append("value = \"\"").append(value == null ? "selected":"").append(">Pilih Agency</option>");
		for (LOVItem obj : LOVItem.getAdminAgency(agencyId, Active_user.current())) {
			content.append("<option value=\"" ).append( obj.id ).append( "\" "
					).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append( obj.label
					).append( "</option>");

		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectJenisLpse(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");			
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \">");
//		content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
		for(LOVItem obj:LOVItem.getJenisLpse()) {
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectJenisAgency(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");			
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \">");
		for(LOVItem obj:LOVItem.getJenisAgency()) {
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectAgency(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"agency\" name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append((value != null ? "selected":"")).append(">"+Messages.get("tag.pilih_instansi")+"</option>");
	    for (LOVItem agency:LOVItem.getAgency()) {
	    	content.append("<option value=\"" ).append( agency.id).append( "\" " ).append(value != null && agency.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(agency.label).append("</option>");      		
		}        
	    content.append("</select>");
	    out.print(content.toString());
	    
	}
	
	public static void _selectAgencySatker(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {		
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"agency\" name=\"").append(name).append("\" class=\"form-control input-sm\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option ").append((value != null ? "selected":"")).append(">"+Messages.get("tag.pilih_instansi")+"</option>");
	    for (LOVItem agency:LOVItem.getAgencySatker()) {
	    	content.append("<option value=\"" ).append( agency.id).append( "\" " ).append(value != null && agency.id.equals(value) ? "selected=\"selected\"" : "").append( " >").append(agency.label).append("</option>");      		
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}

	
	public static void _selectBentukUsaha(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");			
		StringBuilder content = new StringBuilder("<select id=\"bentukUsaha\" name=\"").append(name).append("\" class=\"form-control  \">");
		content.append("<option value=\"\" ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
		for(LOVItem obj:LOVItem.getBentukUsaha()) {
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectKepemilikan(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer)args.get("value");			
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \">");
		content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
		for(Status_kepemilikan obj:Status_kepemilikan.values()) {
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectCopyJadwal(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");	
		Lelang_seleksi lelang = (Lelang_seleksi)args.get("lelang");		
//		List<LOVItem> lelangCopyList = LOVItem.getLelangSamaMetode(lelang.lls_id, lelang.getMetode(), lelang.getPemilihan());
		List<LOVItem> lelangCopyList = LOVItem.getLelangSamaMetodeYangHanyaAdaDiJadwal(lelang.lls_id, lelang.getMetode(), lelang.getPemilihan());
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control selectpicker \" data-live-search=\"true\" data-width=\"80%\">");
		content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
		for(LOVItem obj:lelangCopyList) {
			if(obj.label.length() > 125) {
				int index = obj.label.indexOf(" ", 125);
				if(index >= 0){
					obj.label = obj.label.substring(0, index).concat("...");
				}
			}
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ?
					"selected=\"selected\"" : "").append('>').append(obj.id).append(" - ").append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectPphp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select style=\"width:400px;\" name=\"").append(name).append("\" class=\"form-control  pphp\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option>&nbsp;</option>");
	    for (LOVItem obj:LOVItem.getPegawaiPphp()) {
	    	content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}        
	    content.append("</select>");
	    out.print(content.toString());
	}
	
	public static void _selectTipePenyedia(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");			
		out.print("<select name=\""+name+"\"  class=\"form-control \">");
		out.print("<option "+(value == null ? "selected":"")+">&nbsp;</option>");
		for(TipePenyedia obj:TipePenyedia.values()) {
			out.print("<option value=\"" + obj.id+ "\" " + (value != null && obj.getTipePenyedia(value)!=null ? "selected=\"selected\"" : "") + '>' +obj.label+"</option>");
		}
		out.print("</select>");
	}

    public static void _selectBlnPajak(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        Object name = args.get("arg");
        Integer value = (Integer)args.get("value");
        StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control\">");
        content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
        int i =0;
        for(String obj: FormatUtils.month) {
            content.append("<option value=\"").append(i).append( "\" " ).append(value != null && i == value.intValue() ? "selected=\"selected\"" : "").append('>').append(obj).append("</option>");
            i++;
        }
        content.append("</select>");
        out.print(content.toString());
    }

	public static void _selectPanitia(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control  selectpicker\" data-live-search=\"true\" data-width=\"400\" ");
		content.append((autoSubmit!=null && autoSubmit) ? "onchange=\"javascript:this.form.submit();\"":"").append('>');
		content.append("<option value=\"\" ").append((value == null ? "selected":"")).append(">"+Messages.get("tag.semua")+"</option>");
		for(LOVItem obj:LOVItem.getPanitia(Active_user.current().pegawaiId)) {
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectJenisIjin(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {

		Object name = args.get("arg");
		String value = (String)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		String[] listValue = null;

		if (value != null) {

			String val = value.replaceAll("\\s","");

			listValue = val.split("\\,");

		}

		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select id=\"jenisIjin\" name=\"").append(name).append("\" class=\"selectpicker\" ")
				.append((disabled ? "disabled":"")).append(" multiple title=\"Pilih Izin Usaha\">");
		for (LOVItem jenisIjin:LOVItem.getJenisIjin()) {
			content.append("<option value=\"" ).append( jenisIjin.id).append( "\" " )
					.append(listValue != null && Arrays.asList(listValue).contains(jenisIjin.id.toString()) ? "selected=\"selected\"" : "")
					.append( " >").append(jenisIjin.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());

	}

	public static void _selectJenisDokumen(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		String content = "<select name=\"" + name + "\" class=\"form-control\">" +
				"<option value=\"" + 1 + "\" " + (value != null && value == 1 ? "selected=\"selected\"" : "") + '>' + "Bukti Pembelian" + "</option>" +
				"<option value=\"" + 2 + "\" " + (value != null && value == 2 ? "selected=\"selected\"" : "") + '>' + "Kuitansi" + "</option>" +
				"<option value=\"" + 3 + "\" " + (value != null && value == 3 ? "selected=\"selected\"" : "") + '>' + "Surat Perjanjian" + "</option>" +
				"<option value=\"" + 4 + "\" " + (value != null && value == 4 ? "selected=\"selected\"" : "") + '>' + "Surat Pesanan" + "</option>" +
				"<option value=\"" + 7 + "\" " + (value != null && value == 7 ? "selected=\"selected\"" : "") + '>' + "Dokumen Lainnya" + "</option>" +
				"</select>";

		out.print(content);
	}

	public static void _selectPenetapanPemenang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Lelang_seleksi.JenisLelang value = (Lelang_seleksi.JenisLelang)args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean)args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:250px;\" class=\"form-control \"");
		content.append((autoSubmit!=null && autoSubmit) ? "onchange=\"javascript:this.form.submit();\"":"").append((disabled ? " disabled" : "")).append('>');
		for(Lelang_seleksi.JenisLelang obj: Lelang_seleksi.JenisLelang.values()) {
			content.append("<option value=\"" ).append(obj).append( "\" " ).append(value != null && obj == value ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectMetodePemilihanNonSpk(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long) args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");

		String content = "<select id=\"metodePl\" name=\"" + name + "\" class=\"form-control  selectpicker\" data-width=\"auto\"" + (disabled != null && disabled ? "disabled" : "") + '>' +
				"<option value=''" + (value != null ? "selected" : "") + ">Pilih Metode Pemilihan</option>" +
				"<option value=\"" + 8 + "\" " + (value != null && value == 8 ? "selected=\"selected\"" : "") +
				'>' + "Pengadaan Langsung" + "</option>" +
				"<option value=\"" + 7 + "\" " + (value != null && value == 7 ? "selected=\"selected\"" : "") +
				'>' + "Penunjukan Langsung" + "</option>" +
				"</select>";

		out.print(content);
	}

	public static void _selectStatusNonTransaksional(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Paket_pl.StatusPaket value = (Paket_pl.StatusPaket) args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");

		String content = "<select id=\"statusNonTransaksional\" name=\"" + name + "\" class=\"form-control  selectpicker\" data-width=\"auto\"" + (disabled != null && disabled ? "disabled" : "") + '>' +
				"<option value=\"" + 1 + "\" " + (value != null && value.id == 1 ? "selected=\"selected\"" : "") +
				'>' + "Paket Sedang Berjalan" + "</option>" +
				"<option value=\"" + 2 + "\" " + (value != null && value.id == 2 ? "selected=\"selected\"" : "") +
				'>' + "Paket Sudah Selesai" + "</option>" +
				"</select>";

		out.print(content);
	}

	public static void _selectStatusSwakelola(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Paket_swakelola.StatusPaketSwakelola value = (Paket_swakelola.StatusPaketSwakelola) args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");

		String content = "<select id=\"statusNonTransaksional\" name=\"" + name + "\" class=\"form-control  selectpicker\" data-width=\"auto\"" + (disabled != null && disabled ? "disabled" : "") + '>' +
				"<option value=\"" + 1 + "\" " + (value != null && value.id == 1 ? "selected=\"selected\"" : "") +
				'>' + "Paket Sedang Berjalan" + "</option>" +
				"<option value=\"" + 2 + "\" " + (value != null && value.id == 2 ? "selected=\"selected\"" : "") +
				'>' + "Paket Sudah Selesai" + "</option>" +
				"</select>";

		out.print(content);
	}

	public static void _selectTipeSwakelola(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine){
		Object name = args.get("arg");
		Object value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		String blankLabel = (String)args.get("blankLabel");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\"  style=\"width:250px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		TipeSwakelola[] list = TipeSwakelola.all;
		for (TipeSwakelola tipeswakelola:list) {
			content.append("<option value=\"" ).append(tipeswakelola.id).append('"').append(value != null && tipeswakelola.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(tipeswakelola.label).append("</option>");
		}
		if(!StringUtils.isEmpty(blankLabel))
			content.append("<option>").append(blankLabel).append("</option>");
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectCopyJadwalPl(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long value = (Long)args.get("value");
		Pl_seleksi pl = (Pl_seleksi)args.get("pl");
		List<LOVItem> plCopyList = LOVItem.getPlSamaMetodeYangHanyaAdaDiJadwal(pl.lls_id, pl.getPemilihan());
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control selectpicker \" data-live-search=\"true\" data-width=\"80%\">");
		content.append("<option ").append((value == null ? "selected":"")).append(">&nbsp;</option>");
		for(LOVItem obj:plCopyList) {
			if(obj.label.length() > 125) {
				int index = obj.label.indexOf(" ", 125);
				if(index >= 0){
					obj.label = obj.label.substring(0, index).concat("...");
				}
			}
			content.append("<option value=\"" ).append( obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ?
					"selected=\"selected\"" : "").append('>').append(obj.id).append(" - ").append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectUkpbj(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long selected = (Long)args.get("selected");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control input-sm selectpicker\" data-live-search=\"true\" data-width=\"auto\" ").append((disabled ? "disabled":"")).append('>');
		content.append("<option value=\"\" ").append(selected != null ? "selected":"").append(">Pilih ").append(Group.UKPBJ.getLabel()).append("</option>");
		List<Ukpbj> list =  Ukpbj.order("ukpbj_id asc").fetch();
		for (Ukpbj ukpbj :list) {
			content.append("<option value=\"").append(ukpbj.ukpbj_id).append("\" ")
					.append(selected != null && ukpbj.ukpbj_id.equals(selected) ? "selected=\"selected\"" : "")
					.append(" >").append(ukpbj.nama).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectKUPPBJ(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Long selected = (Long)args.get("selected");
		Boolean disabled = (Boolean) args.get("disabled");
		boolean required = args.containsKey("required") && (Boolean) args.get("required");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"")
				.append(name)
				.append("\" class=\"form-control  selectpicker\" data-live-search=\"true\" data-width=\"auto\" ")
				.append((disabled ? "disabled":"")).append((required ? "required" : "")).append('>');
		content.append("<option value=\"\" ").append(selected != null ? "selected":"")
				.append(">Pilih ").append(Group.KUPPBJ.getLabel()).append("</option>");
		for (Pegawai pegawai : Pegawai.findKuppbjByAgency(Active_user.current().agencyId)) {
				content.append("<option value=\"").append(pegawai.peg_id).append("\" ")
						.append(selected != null && pegawai.peg_id.equals(selected) ? "selected=\"selected\"" : "")
						.append(" >").append(pegawai.peg_nama).append("-").append(pegawai.peg_nip).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectMetodeKualifikasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean) args.get("disabled");
		MetodeKualifikasi[] list = (MetodeKualifikasi[])args.get("list");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:330px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		// default option
		for (MetodeKualifikasi obj:list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectMetodeDokumen(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean) args.get("disabled");
		MetodeDokumen[] list = (MetodeDokumen[])args.get("list");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:330px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		for (MetodeDokumen obj: list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}

	public static void _selectMetodeEvaluasi(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		Integer value = (Integer) args.get("value");
		Boolean autoSubmit = (Boolean) args.get("autoSubmit");
		Boolean disabled = (Boolean) args.get("disabled");
		MetodeEvaluasi[] list = (MetodeEvaluasi[])args.get("list");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" style=\"width:330px;\" class=\"form-control \" ").append((autoSubmit!=null && autoSubmit ? "onchange=\"javascript:this.form.submit();\"":"")).append(' ').append((disabled ? "disabled":"")).append('>');
		// default option
		for (MetodeEvaluasi obj: list) {
			content.append("<option value=\"" ).append(obj.id).append( "\" " ).append(value != null && obj.id.equals(value) ? "selected=\"selected\"" : "").append('>').append(obj.label).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
	
	public static void _selectSPT(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String)args.get("value");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		if(value == null)
			value = "";
		String content = "<select name=\"" + name + "\" class=\"form-control \" " + (disabled ? "disabled":"") + ">" +
				"<option value=\"\" " + (value == null || value.equals("") ? "selected" : "") + ">Pilih Tahun</option>" +
				"<option value=\"1 tahun lalu\" " + (value.equals("1 tahun lalu") ? "selected" : "") + ">1 tahun lalu</option>" +
				"<option value=\"2 tahun lalu\" " + (value.equals("2 tahun lalu") ? "selected" : "") + ">2 tahun lalu</option>" +
				"</select>";
		out.print(content);
	}
	
	public static void _selectTahunPajak(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Object name = args.get("arg");
		String value = (String) args.get("value");
		Integer start = (Integer) args.get("start");
		Boolean disabled = (Boolean) args.get("disabled");
		if(disabled == null)
			disabled = Boolean.FALSE;
		StringBuilder content = new StringBuilder("<select name=\"").append(name).append("\" class=\"form-control \" ").append(' ').append((disabled ? "disabled":"")).append('>');
		content.append("<option value=\"\" ").append(value == null || value.equals("") ? "selected":"").append(">Pilih Tahun</option>");
		int thnNow = DateUtil.getTahunSekarang();
		if(start == null) start = thnNow;
		else start += 1900;
		for (int i = start - 1; i >= start - 2; i--) {			
			content.append("<option value=\"" ).append( i).append( "\" " ).append(value != null && !value.equals("") && Integer.valueOf(i).equals(Integer.valueOf(value)) ? "selected=\"selected\"" : "").append('>').append(i).append("</option>");
		}
		content.append("</select>");
		out.print(content.toString());
	}
}
