package ext;

import groovy.lang.Closure;
import org.apache.commons.collections4.CollectionUtils;
import play.mvc.Http;
import play.templates.FastTags;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 8/30/2018
 */
public class StepperTag extends FastTags {

    public static void _stepper(Map<?, ?> args,
                                Closure body,
                                PrintWriter out,
                                GroovyTemplate.ExecutableTemplate template,
                                int fromLine) {
        List<String> titles = (List<String>) args.get("arg");
        if (CollectionUtils.isEmpty(titles)) {
            out.print("");
            return;
        }
        Http.Request request = Http.Request.current();
        Integer currentStepper = request.params.get("step", Integer.class);
        if (currentStepper == null || currentStepper < 1) {
            currentStepper = 1;
        }
        if (currentStepper > titles.size()) {
            currentStepper = 1;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<div class=\"stepper\">");
        String glue = "";
        int index = 1;
        for (String title : titles) {
            sb.append(glue).append(stepperElement(index, title, currentStepper >= index));
            glue = stepperDivider();
            index++;
        }
        sb.append("</div>");
        out.print(sb.toString());
    }

    private static String stepperDivider() {
        return "<div class=\"stepper-divider\"></div>\n";
    }

    private static String stepperElement(int index, String title, boolean isComplete) {
        return "<div class=\"step " + (isComplete ? "stepper-complete" : "") + "\">\n" +
                "<div class=\"stepper-circle\">" + index + "</div>\n" +
                "<div class=\"stepper-title\">" + title + "</div>\n" +
                "</div>";
    }

}
