package ext;

import net.sf.oval.configuration.annotation.Constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * check tipe file yang diperbolehkan sesuai settingan di {@ext.DokumenCheck}
 * @author Arief Ardiyansah
 *
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(checkWith = DokumenTypeCheck.class)
public @interface DokumenType {

	 String message() default DokumenTypeCheck.mes;
}
