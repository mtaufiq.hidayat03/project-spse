package ext;


import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.data.binding.TypeBinder;
import play.mvc.Scope;
import utils.AesUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * Class untuk Melakukan Decrypt data yang di enkrip dengan utils.AesUtil
 *
 * @author Arief Ardiyansah
 */

public class AesDecryptBinder implements TypeBinder<String> {

    @Override
    public Object bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        if (StringUtils.isEmpty(value))
            return "";
        // decrypt AES message
        try {
            Logger.debug(value);
            AesUtil aesUtil = new AesUtil(256, 1000);
            return aesUtil.decrypt(Scope.Session.current().getId(), value);
        } catch (Exception e) {
            Logger.error(e, "Gagal Dekripsi");
            return "";
        }
    }
}
