package ext;

import groovy.lang.Closure;
import models.common.Active_user;
import models.common.Kategori;
import models.devpartner.*;
import models.devpartner.PersetujuanDp.JenisPersetujuan;
import models.devpartner.PersetujuanDp.PersetujuanLelang;
import models.devpartner.common.*;
import models.devpartner.contracts.StatusTenderDp;
import models.devpartner.handler.BeritaAcaraDpDownloadHandler;
import models.devpartner.pojo.HasilEvaluasiDp;
import models.devpartner.pojo.PengumumanLelangDp;
import models.jcommon.blob.BlobTable;
import models.jcommon.util.CommonUtil;
import org.apache.commons.collections4.CollectionUtils;
import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * componen tags terkait masalah workflow lelang
 * @author idoej
 *
 */
public class LelangDpTag extends FastTags {
	public static void _statusTenderDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		StatusTenderDp lelang = (StatusTenderDp) args.get("arg");
		if(lelang == null)
			return;
		if(lelang.isLelangUlang())
			out.print(lelang.isKonsultansi() ? "<span class=\"label label-warning\">Seleksi Ulang</span>":"<span class=\"label label-warning\">Tender Ulang</span>");
		if(lelang.isDitutup())
			out.print(lelang.isKonsultansi() ? "<span class=\"label label-info\">Seleksi Dibatalkan</span>":"<span class=\"label label-info\">Tender Dibatalkan</span>");
		if(lelang.isPenawaranUlang())
			out.print(" <span class=\"label label-info\">Pemasukan Penawaran Ulang</span>");
		if(lelang.isEvaluasiUlang())
			out.print("<span class=\"label label-info\">Evaluasi Ulang</span>");
	}

	public static void _persetujuanListDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Long lelangId = (Long)args.get("arg");
		boolean pemenang = (Boolean)args.get("pemenang");
		String jenisKey = (String)args.get("jenisKey");

		Active_user active_user = Active_user.current();
		List<PersetujuanLelang> persetujuanList = null;
		JenisPersetujuan jenis = null;

		if(jenisKey != null){
			jenis = JenisPersetujuan.getByKey(jenisKey);
			PersetujuanDp.checkPersetujuanBatalLelang(lelangId, jenis);
			persetujuanList = PersetujuanLelang.findBatalList(lelangId);
		}else{
			if(pemenang)  {
				PersetujuanDp.checkPersetujuanPemenang(lelangId);
				jenis = JenisPersetujuan.PEMENANG_LELANG;
			}else{
				PersetujuanDp.checkPersetujuanLelang(lelangId);
				jenis = JenisPersetujuan.PENGUMUMAN_LELANG;
			}
			persetujuanList = PersetujuanLelang.findList(lelangId, jenis);
		}

		PersetujuanDp persetujuan = PersetujuanDp.find("lls_id=? and peg_id=?", lelangId, active_user.pegawaiId).first();
		persetujuan = persetujuan != null ? persetujuan : new PersetujuanDp();

		StringBuilder content = new StringBuilder();
		content.append("<table class=\"table table-condensed\">");
		content.append("<tr><th width=\"350\">")
				.append(Messages.get("lelang.anggota_penyedia_pokja"))
				.append("</th><th>")
				.append(Messages.get("lelang.status")+"</th><th>")
				.append(Messages.get("evaluasi.tanggal")+"</th><th>")
				.append(Messages.get("lelang_evaluasi.alasan_tdk_setuju"))
				.append("</th></tr>");
		if(!CommonUtil.isEmpty(persetujuanList)) {
			for(PersetujuanLelang item:persetujuanList) {
				Map<String, Object> param = new HashMap<String, Object>(1);
				param.put("pst_id", item.pst_id);
				content.append("<tr><td>");
				content.append("<a href=\"").append(Router.reverse("devpartner.PersetujuanDpCtr.riwayatPersetujuan", param).url).append("\" class=\"dialog\" title=\""+Messages.get("ct.rp")+"\">");
				content.append(persetujuan.pst_id == item.pst_id? "Anda" : item.peg_nama);
				content.append("</a></td>");
				if(item.pst_status.isSetuju())
					content.append("<td>").append("<i class=\"fa fa-check\"></i>").append("</td>");
				else if (item.pst_status.isTidakSetuju())
					content.append("<td>").append("<i class=\"fa fa-times\"></i>").append("</td>");
				else
					content.append("<td></td>");
				content.append("<td>").append(FormatUtils.formatDateTimeInd(item.pst_tgl_setuju)).append("</td>");
				content.append("<td>").append(JavaExtensions.raw(item.getAlasan())).append("</td>");
				content.append("</tr>");
			}
		}
		content.append("</table>");
		out.print(content.toString());
	}

	@SuppressWarnings(value = {"unchecked"})
	public static void _tabmenuLelangDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Http.Request request = Http.Request.current();
		TahapDpStarted tahapStarted = (TahapDpStarted)args.get("arg");
		Active_user active_user = Active_user.current();
		Long lelangId = tahapStarted.lelangId;
		LelangDp lelang = LelangDp.findById(lelangId);
		boolean show = true;
		if(lelang.isPreQualification() && active_user.isRekanan()) {
			PesertaDp peserta = PesertaDp.findBy(lelangId, active_user.rekananId);
			if(peserta != null)
				show = peserta.isShortlisted();
		}
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\" role=\"tablist\">")
				.append("<li ").append(request.action.equals("devpartner.LelangDpCtr.view")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.LelangDpCtr.view").add("id", lelangId).url).append("\" >").append(Messages.get("menu.it")).append("</a></li>");
		if (tahapStarted.isPenjelasan() && show) {
			boolean currentAction = request.action.equals("devpartner.PenjelasanDpCtr.pengadaan");
			String action = Router.reverse("devpartner.PenjelasanDpCtr.pengadaan").add("id", lelangId).url;
			int count = DiskusiLelangDp.countTanggapanPeserta(lelangId, tahapStarted.getTahapPenjelasan());
			int countPenjelasan = DiskusiLelangDp.countTanggapanPanitia(lelangId, tahapStarted.getTahapPenjelasan());
			content.append("<li ").append(currentAction ? "class=\"active\"" : "").append("><a href=\"").append(action).append("\">").append(Messages.get("menu.pty")).append("<span class=\"badge\" title=\""+Messages.get("menu.pty")+"\">").append(count).append("</span>").append(Messages.get("menu.dpj")).append("<span class=\"badge\" title=\""+Messages.get("menu.pj")+"\">").append(countPenjelasan).append("</span></a></li>");
		}
		if((tahapStarted.isTampilkanPenawaran() || tahapStarted.isPenetapanShortlist()) && !active_user.isRekanan()){
			content.append("<li ").append(request.action.equals("devpartner.PesertaDpCtr.penawaran")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.PesertaDpCtr.penawaran").add("id", lelangId).url).append("\">").append(Messages.get("menu.penawaran_peserta")).append("</a></li>");
		}
		if((tahapStarted.isUploadEoi() || tahapStarted.isEvaluasi() || tahapStarted.isPenetapanShortlist() || tahapStarted.isPembukaan()) && !active_user.isRekanan()){
			content.append("<li ").append(request.controller.equals("devpartner.EvaluasiDpCtr")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.EvaluasiDpCtr.index").add("id", lelangId).url).append("\">").append(Messages.get("menu.evaluasi")).append("</a></li>");
		}

		content.append("</ul>");
		out.print(content.toString());
	}

	public static void _dokLelangDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		DokLelangDp dok_lelang =  (DokLelangDp)args.get("arg");
		String label = args.get("label").toString();
		if (dok_lelang != null) {
			StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get(label)).append("</div>");
			Map<String, Object> params = new HashMap<String, Object>(1);
			content.append("<div class=\"list-group\">");
			if (dok_lelang != null && dok_lelang.dll_id_attachment != null) {
				params.put("id", dok_lelang.dll_id);
				content.append("<a href=\"").append(dok_lelang.getDownloadUrl()).append("\" class=\"list-group-item\" target=\"_blank\" title=\"Dokumen Tender\">").append(Messages.get("doc.download")).append("</a>");
			}
			content.append("</div>");
			content.append("</div>");
			out.print(content.toString());
		}
	}

	public static void _kirimPenawaranDp(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
		LelangDp lelang = (LelangDp)args.get("arg");
		DokPenawaranDp.JenisDokPenawaran jenis = (DokPenawaranDp.JenisDokPenawaran)args.get("jenis");
		DokPenawaranDp dokPenawaran = (DokPenawaranDp) args.get("dok_penawaran");
		TahapDp tahapPenawaran = (TahapDp) args.get("tahapPenawaran");
		String dokLabel = (String) args.get("dokLabel");
		Map<String, Object> param = new HashMap<String, Object>(2);
		param.put("id", lelang.lls_id);
		param.put("jenis", jenis.id);

		String keterangan = "";
		if (dokPenawaran != null){
			keterangan = Messages.get("ct.sdp")+ " : " + FormatUtils.formatDateTimeInd(dokPenawaran.dok_tgljam);
		} else {
			JadwalDp jadwal = JadwalDp.findByLelangNTahap(lelang.lls_id, tahapPenawaran);
			if (jadwal != null) {
				String tglAwal = "";
				String tglAkhir = "";
				if (jadwal.dtj_tglawal != null && jadwal.dtj_tglakhir != null) {
					tglAwal = FormatUtils.formatDateTimeInd(jadwal.dtj_tglawal);
					tglAkhir = FormatUtils.formatDateTimeInd(jadwal.dtj_tglakhir);
				}
				keterangan = Messages.get("ct.bdk_jp")+" : " + tglAwal + " s.d. "+ tglAkhir;
			}
		}
		StringBuilder content = new StringBuilder("<div class=\"panel panel-info\">");
		content.append("<div class=\"panel-heading\">"+dokLabel+"</div>");
		content.append("<div class=\"list-group\">");
		content.append("<a href=\"")
				.append(Router.reverse("devpartner.DokumenDpCtr.viewKirimPenawaran", param).url)
				.append("\" class=\"list-group-item\" title=\"Dokumen Tender\">")
				.append(dokLabel+" <span class=\"badge\">Status : ").append(keterangan).append("</span>")
				.append("</a>");
		content.append("</div>");
		content.append("</div>");

		out.print(content.toString());
	}

	public static void _tabInfoDetilLelangDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		Http.Request request = Http.Request.current();
		TahapDpStarted tahapStarted = (TahapDpStarted)args.get("arg");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", tahapStarted.lelangId);
		StringBuilder content = new StringBuilder("<ul class=\"nav nav-tabs\" role=\"tablist\">")
				.append("<li ").append(request.action.equals("devpartner.LelangDpCtr.pengumumanLelang")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.LelangDpCtr.pengumumanLelang", param).url).append("\" >").append(Messages.get("menu.peserta")).append("</a></li>");
		param.remove("action");
		content.append("<li ").append(request.action.equals("devpartner.PesertaDpCtr.index")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.PesertaDpCtr.index", param).url).append("\" >").append(Messages.get("menu.peserta")).append("</a></li>");
		param.remove("action");
		if(tahapStarted.isPengumumanPemenangAkhir()){ // TODO
			content.append("<li ").append(request.action.equals("lelang.EvaluasiCtr.hasil")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.EvaluasiDpCtr.hasil", param).url).append("\">").append(Messages.get("menu.he")).append("</a></li>");
			param.remove("action");
			content.append("<li ").append(request.action.equals("lelang.EvaluasiCtr.pemenang")?"class=\"active\"":"").append("><a href=\"").append(Router.reverse("devpartner.EvaluasiDpCtr.pemenang", param).url).append("\">").append(Messages.get("menu.pe")).append("</a></li>");
			param.remove("action");
		}
		content.append("</ul>");
		out.print(content.toString());
	}

	public static void _penawaranPesertaDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		LelangDp lelang = (LelangDp) args.get("arg");
		TahapDpNow tahapAktif = lelang.getTahapNow();
		TahapDpStarted tahapStarted = lelang.getTahapStarted();
		TahapLelangDp tahapLelang = lelang.getTahapLelang();
		boolean hide = (Boolean) args.get("hide");
		Active_user active_user = Active_user.current();
		out.print("<table class=\"table table-striped table-condensed\">");
		out.print("<tr><th width=\"20\">#</th><th>"+Messages.get("lelang.nama_penyedia_barang_jasa")+"</th><th width=\"120\">"+Messages.get("lelang.tgl_mendaftar")+"</th>");
		if (lelang.isPreQualification())
			out.print("<th width=\"150\" class=\"text-center\">EOI</th>");
		if (lelang.getMetode().dokumen.isSatuFile()) {
			out.print("<th class=\"text-center\">"+ lelang.getPenawaranSatuFileLabel() +"</th>");
		} else {
			out.print("<th class=\"text-center\">"+ lelang.getPenawaranTeknisLabel() +"</th>");
			out.print("<th class=\"text-center\">"+ lelang.getPenawaranHargaLabel() +"</th>");
		}
		out.print("</tr>");

		List<PesertaDp> list = lelang.getPesertaList();
		if (!CollectionUtils.isEmpty(list)) {
			int idx = 1;
			boolean kualifikasiPermission = tahapStarted.isPenetapanShortlist();
			Map<String, Object> param = new HashMap<String, Object>(1);
			BlobTable blob = null;
			// special handling for cqs and qbs
			NilaiEvaluasiDp highestScore = null;
			if (lelang.getMetode().evaluasi.isCqs()) {
				EvaluasiDp shortlist = lelang.getShortlist();
				if (shortlist != null) {
					highestScore = shortlist.findSkorTertinggi();
				}
			} else if (lelang.getMetode().evaluasi.isQbs()) {
				EvaluasiDp teknis = lelang.getTeknis();
				if (teknis != null) {
					highestScore = teknis.findSkorTertinggi();
				}
			}
			for (PesertaDp peserta : list) {
				param.put("id", peserta.psr_id);
				out.print("<tr>");
				out.print("<td align=\"right\">"+idx+"</td>");
				if(hide)
					out.print("<td>Peserta "+idx);
				else {
					out.print("<td>"+peserta.getNamaPeserta());
					out.print("</td>");
				}
				out.print("<td>"+StringFormat.formatDate(peserta.psr_tanggal)+"</td>");
				List<DokPenawaranDp> dok_eoi_list = null, dok_teknis_list=null, dok_harga_list=null, dok_teknis_harga_list;
				if (lelang.isPreQualification()) {
					dok_eoi_list = DokPenawaranDp.findByPsrAndJenis(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_EOI.id);
					out.print("<td class=\"text-center\">");
					if(dok_eoi_list != null && !kualifikasiPermission)
						out.print("<i class=\"fa fa-check\"></i>");
					else if(!CollectionUtils.isEmpty(dok_eoi_list)) {
						for (DokPenawaranDp penawaran: dok_eoi_list) {
							out.print("<a href=\"" + penawaran.getDownloadUrl() + "\" class=\"badge\"> "+ penawaran.getDokumen().blb_nama_file +"</a> <br>");
						}
					}
					out.print("</td>");
				}
				if (lelang.isSatuFile()) {
					boolean allowDownload = tahapStarted.isTampilkanPenawaran() && (lelang.getMetode().isEmpanelment() || lelang.isSudahPembukaanPenawaran());
					if (allowDownload && lelang.getMetode().evaluasi.isCqs()) {
						allowDownload = highestScore != null && highestScore.psr_id == peserta.psr_id;
					}
					dok_teknis_harga_list = DokPenawaranDp.findByPsrAndJenis(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id);
					out.print("<td class=\"text-center\">");
					if(!CollectionUtils.isEmpty(dok_teknis_harga_list)) {
						for (DokPenawaranDp penawaran : dok_teknis_harga_list) {
							if(allowDownload){
								out.print("<a href=\"" + penawaran.getDownloadUrl() + "\" class=\"badge\"> "+ penawaran.getDokumen().blb_nama_file +"</a> <br>");
							} else {
								out.print("******");
							}
						}
					}
					out.print("</td>");
				} else {
					boolean allowDownloadHarga = tahapStarted.isPembukaanHarga() && lelang.isSudahPembukaanPenawaran();
					if (allowDownloadHarga && lelang.getMetode().evaluasi.isQbs()) {
						allowDownloadHarga = highestScore != null && highestScore.psr_id.equals(peserta.psr_id);
					}
					dok_teknis_list = DokPenawaranDp.findByPsrAndJenis(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id);
					dok_harga_list = DokPenawaranDp.findByPsrAndJenis(peserta.psr_id, DokPenawaranDp.JenisDokPenawaran.PENAWARAN_HARGA.id);
					out.print("<td class=\"text-center\">");
					if(!CollectionUtils.isEmpty(dok_teknis_list)) {
						for (DokPenawaranDp penawaran : dok_teknis_list) {
							if(tahapStarted.isTampilkanPenawaran()){
								out.print("<a href=\"" + penawaran.getDownloadUrl() + "\" class=\"badge\"> "+ penawaran.getDokumen().blb_nama_file +"</a> <br>");
							} else {
								out.print("******");
							}
						}
					}
					out.print("</td>");
					out.print("<td class=\"text-center\">");
					if(!CollectionUtils.isEmpty(dok_harga_list)) {
						for (DokPenawaranDp penawaran : dok_harga_list) {
							if(allowDownloadHarga){
								out.print("<a href=\"" + penawaran.getDownloadUrl() + "\" class=\"badge\"> "+ penawaran.getDokumen().blb_nama_file +"</a> <br>");
							} else {
								out.print("******");
							}
						}
					}
					out.print("</td>");
				}



				out.print("</tr>");
				idx++;
			}
		}
		out.print("</table>");
	}

	public static void _hasilEvaluasiDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		LelangDp lelang = (LelangDp) args.get("arg");
		MetodeEvaluasiDp metodeEvaluasi = lelang.getEvaluasi();
		out.print("<table class=\"table table-condensed table-hover\">");
		out.print("<tr>");
		out.print("<th>No.</th>");
		out.print("<th>"+ Messages.get("evaluasi.nama_peserta") +"</th>");
		if (!lelang.getMetode().isEmpanelment()) {
			out.print("<th>"+ Messages.get("evaluasi.harga_penawaran") +"</th>");
			out.print("<th>"+ Messages.get("evaluasi.harga_terkoreksi") +"</th>");
			out.print("<th>"+ Messages.get("evaluasi.harga_negosiasi") +"</th>");
			if (lelang.isPreQualification()) {
				out.print("<th width=\"20\"><span class=\"label label-danger\">S</span></th>");
				if (lelang.getMetode().isCqs()) {
					out.print("<th width=\"20\"><span class=\"label label-danger\">"+Messages.get("lelang_evaluasi.skor")+"</span></th>");
				}
			}
			out.print("<th width=\"20\"><span class=\"label label-info\">T</span></th>");
			if (metodeEvaluasi.isNilai())
				out.print("<th width=\"20\"><span class=\"label label-info\">"+Messages.get("lelang_evaluasi.skor")+"</span></th>");
			out.print("<th width=\"20\"><span class=\"label label-success\">H</span></th>");
			if (metodeEvaluasi.isNilaiHarga()) {
				out.print("<th width=\"20\"><span class=\"label label-success\">" + Messages.get("lelang_evaluasi.skor") + "</span></th>");
				out.print("<th width=\"20\"><span class=\"label label-warning\">" + Messages.get("lelang_evaluasi.skor") + "</span></th>");
			}
			if (!lelang.getMetode().evaluasi.isAutoCombinedEvaluation()) {
				out.print("<th width=\"20\"><span class=\"label label-warning\">R</span></th>");
			}
		}
		out.print("<th width=\"20\"><span class=\"label label-warning\">P</span></th>");

		List<HasilEvaluasiDp> list = HasilEvaluasiDp.findByLelang(lelang.lls_id);
		if(!CollectionUtils.isEmpty(list)) {
			String currentTab = lelang.isPreQualification() ? "#kualifikasi":( lelang.getMetode().isEmpanelment() ? "#empanelment" : "#teknis"); // currentTab di detail evaluasi
			for(int indeks = 0; indeks <  list.size() ; indeks++) {
				HasilEvaluasiDp peserta = list.get(indeks);
				if(peserta == null)
					continue;
				out.print("<tr"+(peserta.isGagalKontrak() ? " class=\"bg-danger\"" : "")+">");
				out.print("<td>"+(indeks + 1)+"</td>");
				out.print("<td><a href=\""+Router.reverse("devpartner.EvaluasiDpCtr.detail").add("id", peserta.psr_id).url+currentTab+"\" value=\""+peserta.psr_id+"\">"+peserta.rkn_nama+"</a></td>");
				if (!lelang.getMetode().isEmpanelment()) {
					out.print("<td>" + (peserta.psr_harga != null ? peserta.getPenawaran() + " "+peserta.getNotifHargaPenawaran() : Messages.get("lelang.tidak_ada_penawaran")) + "</td>");
					out.print("<td>" + (peserta.psr_harga != null ? "<div>" + peserta.getPenawaranTerkoreksi() +" "+peserta.getNotifHargaPenawaranTerkoreksi()+"</div>" : Messages.get("lelang.tidak_ada_penawaran")) + "</td>");
					out.print("<td>" + (peserta.nev_harga_negosiasi != null ? "<div>" + peserta.gethargaNegosiasi() +" "+peserta.getNotifHargaNegosiasi()+"</div>" : "") + "</td>");
					if(lelang.isPreQualification()) {
						out.print("<td class=\"text-center\">" + StringFormat.statuslulusIcon(peserta.kualifikasi) + "</td>");
						if (lelang.getMetode().isCqs()) {
							out.print("<td class=\"text-center\">" + (peserta.skor != null ? peserta.skor : 0) + "</td>");
						}
					}
					out.print("<td class=\"text-center\">" + StringFormat.statuslulusIcon(peserta.teknis) + "</td>");
					if (lelang.isNilai())
						out.print("<td>" + (peserta.skor_teknis != null ? peserta.skor_teknis : 0) + "</td>");
					out.print("<td class=\"text-center\">" + StringFormat.statuslulusIcon(peserta.harga) + "</td>");
					if (lelang.isNilaiHarga()) {
						out.print("<td>" + (peserta.skor_harga != null ? peserta.skor_harga : 0) + "</td>");
						out.print("<td>" + (peserta.skor_akhir != null ? peserta.skor_akhir : 0) + "</td>");
					}
					if (!lelang.getMetode().evaluasi.isAutoCombinedEvaluation()) {
						out.print("<td class=\"text-center\">"+(peserta.final_rank != null ? "<span style=\"font-weight:bold "+(peserta.isLulusRank() ? "" : ";color:#c60b0b")+"\">"+peserta.final_rank+"</span>" : "<i class=\"fa fa-minus\" />")+"</td>");
					}
				}
				out.print("<td class=\"text-center\">"+(peserta.isPemenang()?"<i class=\"fa fa-star\" style=\"color:#f0ad4e\"/>":"<i class=\"fa fa-close\" />")+"</td>");
				out.print("</tr>");
			}
		}
		out.print("</table>");
	}

    /**
     * untuk penanganan flow berita acara
     */
    public static void _beritaAcaraDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
        LelangDp lelang = (LelangDp) args.get("arg");
        TahapDpStarted tahapStarted = (TahapDpStarted) args.get("tahapStarted");
        Active_user active_user = Active_user.current();
        boolean allow_upload_ba = lelang.lls_status.isAktif() && active_user.isPanitia();
        Map<String, Object> params = new HashMap<String, Object>(1);
        params.put("id", lelang.lls_id);

        BeritaAcaraDp berita_acara = null;
        StringBuilder content = new StringBuilder();

		PesertaDp peserta = null;
		if (active_user.isRekanan()) {
        	peserta = PesertaDp.findBy(lelang.lls_id, active_user.rekananId);

		}

        boolean allowBaNegosiasi = false;
//        EvaluasiDp evaluasi = EvaluasiDp.findAkhir(lelang.lls_id);
//        if(evaluasi != null && evaluasi.eva_status.isSelesai()) {
//            List<Nilai_evaluasi> pemenangList = evaluasi.findPesertaList(lelang.getPemilihan().isLelangExpress());
//            allowBaNegosiasi = pemenangList.size() < 3 || lelang.getPemilihan().isSeleksiUmum() || lelang.getPemilihan().isSeleksiSederhana();
//            // yang dilakukan nego hanya jika jumlah penawar < 3 dan e-seleksi
//        }

		if (tahapStarted.isPenetapanShortlist()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.SHORTLIST_APPOINTMENT);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("badp.shortlist_appointment"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.SHORTLIST_APPOINTMENT);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.SHORTLIST_APPOINTMENT).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");

			if (!active_user.isRekanan() || peserta.isShortlisted()) {
				renderBaList(berita_acara, content);
			} else {
				content.append(Messages.get("ba.tidak_diijinkan"));
			}
			content.append("</div>");
		}

		if (tahapStarted.isPengumumanShortlist()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.SHORTLIST_ANNOUNCEMENT);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("badp.shortlist_announcement"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.SHORTLIST_ANNOUNCEMENT);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.SHORTLIST_ANNOUNCEMENT).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			if (!active_user.isRekanan() || peserta.isShortlisted()) {
				renderBaList(berita_acara, content);
			} else {
				content.append(Messages.get("ba.tidak_diijinkan"));
			}
			content.append("</div>");
		}

		if (tahapStarted.isPembukaan() && lelang.isSatuFile()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.BID_OPENING);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("badp.bid_opening"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.BID_OPENING);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.BID_OPENING).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			renderBaList(berita_acara, content);
			content.append("</div>");
		}

		if (tahapStarted.isEvaluasiTeknis() && !lelang.isSatuFile()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.TECHNICAL_EVALUATION);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(lelang.isNilai() ? Messages.get("lelang_evaluasi.skor_teknis") : Messages.get("badp.technical_evaluation"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.TECHNICAL_EVALUATION);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.TECHNICAL_EVALUATION).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			boolean allowDownload = !active_user.isRekanan() || peserta.isLulusTeknis();
			if (active_user.isRekanan() && lelang.isNilai() && allowDownload) {
				allowDownload = lelang.isSudahPembukaanPenawaran();
			}
			if (allowDownload) {
				renderBaList(berita_acara, content);
			} else {
				content.append(Messages.get("ba.tidak_diijinkan"));
			}
			content.append("</div>");
		}

		if (tahapStarted.isEvaluasiHarga() && !lelang.isSatuFile()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.FINANCIAL_EVALUATION);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("badp.financial_evaluation"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.FINANCIAL_EVALUATION);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.FINANCIAL_EVALUATION).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			boolean allowDownload = !active_user.isRekanan() || peserta.isLulusTeknis();
			if (active_user.isRekanan() && lelang.isNilaiHarga() && allowDownload) {
				allowDownload = lelang.isSudahPembukaanPenawaran();
			}
			if (allowDownload) {
				renderBaList(berita_acara, content);
			} else {
				content.append(Messages.get("ba.tidak_diijinkan"));
			}
			content.append("</div>");
		}

		if (tahapStarted.isEvaluasiAkhir() && !active_user.isRekanan()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.FINAL_EVALUATION);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("badp.final_ranking"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.FINAL_EVALUATION);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.FINAL_EVALUATION).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			renderBaList(berita_acara, content);
			content.append("</div>");
		}

		if (tahapStarted.isContractNegotiation() && !active_user.isRekanan()) {
			berita_acara = BeritaAcaraDp.findBy(lelang.lls_id, BeritaAcaraDp.JenisBA.CONTRACT_NEGOTIATION);
			content.append("<div class=\"panel panel-info\">");
			content.append("<div class=\"panel-heading\">").append(Messages.get("badp.contract_negotiation"));
			if (allow_upload_ba) {
				Map<String, Object> param = new HashMap<String, Object>(2);
				param.put("id", lelang.lls_id);
				param.put("jenis", BeritaAcaraDp.JenisBA.CONTRACT_NEGOTIATION);
				content.append("<a href=\"\" class=\"dialog pull-right badge btn-ba\" data-aksi=\"upload\"")
						.append(" title=\"").append(Messages.get("ct.ubappk")).append("\"")
						.append(" value=\"").append(BeritaAcaraDp.JenisBA.CONTRACT_NEGOTIATION).append("\">").append(Messages.get("ct.upload")).append("</a>");
			}
			content.append("</div>");
			if (berita_acara != null && !active_user.isRekanan()) {
				renderBaList(berita_acara, content);
			}
			content.append("</div>");
		}

        out.print(content.toString());
    }

    private static void renderBaList(BeritaAcaraDp berita_acara, StringBuilder content) {
    	if (berita_acara == null) {
			content.append(Messages.get("ba.belum_tersedia"));
    		return;
		}
		List<BlobTable> list = berita_acara.getDokumen();
		if (CollectionUtils.isEmpty(list)) {
			content.append(Messages.get("ba.belum_tersedia"));
			return;
		}

		content.append("<div class=\"list-group\">");
		for (BlobTable blob : list) {
			content.append("<a href=\"").append(blob.getDownloadUrl(BeritaAcaraDpDownloadHandler.class))
					.append("\" class=\"list-group-item\"><i class=\"glyphicon glyphicon-download\"></i> ")
					.append(blob.getFileName()).append("<abbr title=\"").append(Messages.get("ct.tgl_kirim")).append("\" class=\"initialism\"> (")
					.append(FormatUtils.formatDateTimeInd(blob.blb_date_time)).append(")</abbr></a>");
		}
		content.append("</div>");
	}

	public static void _statusPesertaLelang(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		LelangDp lelang = (LelangDp) args.get("arg");
		PesertaDp peserta = (PesertaDp) args.get("peserta");
		NilaiEvaluasiDp teknis = peserta.getTeknis();
		NilaiEvaluasiDp akhir = peserta.getAkhir();
		EvaluasiDp evaShortlist = lelang.getShortlist();
		EvaluasiDp evaTeknis = lelang.getTeknis();
		EvaluasiDp evaAkhir = lelang.getEvaluasiAkhir();
		if (lelang.getMetode().isEmpanelment()) {
			if (evaAkhir != null && evaAkhir.eva_status.isSelesai()) {
				out.print(akhir != null && akhir.isLulus() ? Messages.get("lelang_evaluasi.lulus") : Messages.get("lelang_evaluasi.ditolak"));
				return;
			}

			out.print(Messages.get("lelang_evaluasi.sedang_berlangsung"));
			return;
		}

		boolean adaPemenang = lelang.hasPemenang();
		boolean selesaiAkhir = evaAkhir != null && evaAkhir.eva_status.isSelesai();
		boolean selesaiTeknis = evaTeknis != null && evaTeknis.eva_status.isSelesai();
		boolean selesaiShortlist = evaShortlist != null && evaShortlist.eva_status.isSelesai();
		NilaiEvaluasiDp highestAkhir = evaAkhir != null ? evaAkhir.findRankTertinggi() : null;
		boolean shortlisted = peserta.isShortlisted();
		boolean lulusTeknis = teknis != null && teknis.isLulus();
		boolean negosiasi = !adaPemenang && selesaiAkhir && highestAkhir != null && highestAkhir.psr_id == peserta.psr_id;
		boolean menang = adaPemenang && peserta.is_pemenang == 1;

		if (menang) {
			out.print(Messages.get("lelang_evaluasi.menang"));
			return;
		}

		if (adaPemenang) {
			out.print(Messages.get("lelang_evaluasi.tidak_berhasil"));
			return;
		}

		if (negosiasi) {
			out.print(Messages.get("evaluasi.contract"));
			return;
		}

		if (lulusTeknis) {
			out.print(Messages.get("lelang_evaluasi.lulus_teknis"));
			return;
		}

		if (selesaiTeknis) {
			out.print(Messages.get("lelang_evaluasi.gagal_teknis"));
			return;
		}

		if (shortlisted) {
			out.print(lelang.isKonsultansi() ? Messages.get("lelang_evaluasi.lulus_shortlist") : Messages.get("lelang_evaluasi.lulus_pq"));
			return;
		}

		if (selesaiShortlist) {
			out.print(lelang.isKonsultansi() ? Messages.get("lelang_evaluasi.gagal_shortlist") : Messages.get("lelang_evaluasi.gagal_pq"));
			return;
		}

		out.print(Messages.get("lelang_evaluasi.sedang_berlangsung"));
	}

	public static void _pengumumanDp(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		out.print("<table class=\"table table-condensed\"><thead>");
		out.print("<tr><th width=\"5%\">No</th><th>"+Messages.get("ct.np")+"</th><th class=center width=\"150\">"+Messages.get("hps.hps")+"</th><th class=center width=\"170\">"+Messages.get("ct.ah")+"</th></tr>");
		out.print("</thead><tbody>");
		Map<String, Object> map = new HashMap<String, Object>(1);
		Map<Kategori, List<PengumumanLelangDp>>  kategoriMap = PengumumanLelangDp.findAsMap();
		for (Kategori kategori : KategoriDp.all) {
			List<PengumumanLelangDp> pengumumanList = kategoriMap.get(kategori);
			String css = kategori.nama.replace(" ", "_");
			out.print("<tr><td colspan=\"4\" class=\"bs-callout-info\"><a href=\"#\" onclick=\"$('."+css+"').toggle();\">"+kategori.getNama()+" </a><span class=\"badge pull-right\">"+pengumumanList.size()+"</span></td></tr>");
			int i=1 ;
			for (PengumumanLelangDp pengumumanLelang : pengumumanList) {
				map.put("id", pengumumanLelang.lls_id);
				out.print("<tr class=\""+css+"\" sytle=\"display:none\">");
				out.print("<td><center>"+i+"</center></td>");
				out.print("<td><a href=\""+Router.reverse("devpartner.LelangDpCtr.pengumumanLelang", map).url+"\" target=\"_blank\">"+pengumumanLelang.pkt_nama+"</a></td>");
				if (pengumumanLelang.isEnableViewHps()) {
					out.print("<td class=\"table-hps\">" + FormatUtils.formatCurrenciesJutaRupiah(pengumumanLelang.pkt_hps) + "</td>");
				} else {
					out.print("<td class=\"table-hps\"><span class=\"label label-info\">"+Messages.get("hps.tidak_ditayangkan")+"</span></td>");
				}
				out.print("<td class=\"center\">"+FormatUtils.formatDateTimeInd(pengumumanLelang.tgl_akhir_daftar)+"</td>");
				out.print("</tr>");
				i++;
			}
		}
		out.print("</tbody></table>");
		kategoriMap.clear();
	}
}