package ext;

import groovy.lang.Closure;
import org.apache.commons.lang3.StringUtils;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Router.ActionDefinition;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

import java.io.PrintWriter;
import java.util.Map;

/**
 * tag untuk button2 yang ada di aplikasi seperti (tambah, hapus, kembali)
 * agar mudah memaintainnya 
 * @author arief
 *
 */
public class ButtonTag extends FastTags {
	
	/**
	 * tag button Tambah
	 * default label='TAMBAH'
	 */
	public static void _addButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");		
		if (StringUtils.isEmpty(label)) {
			if(Lang.get().equals("id")){
				label = "Tambah";
			}else{
				label = "Add";
			}
		}
		out.print("<a class=\"btn btn-secondary  btn-tambah\" href=\"" + actionDef.url + "\" " + serialize(args, "action") + "> <i class=\"fa fa-plus-circle fa-lg\"></i> "+label+"</a>&nbsp;");
	}

	/**
	 * tag button simpan, digunakan untuk action simpan model
	 * default label='simpan'
	 * boleh diberi id
	 */
	public static void _saveButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {		
		String label = (String)args.get("title");		
		if (StringUtils.isEmpty(label)) {
			label = Messages.get("lelang.simpan");
		}
		String name = (String)args.get("name");
		if (StringUtils.isEmpty(name)) {
			name = "simpan";
		}
		String id=(String)args.get("id");
		if(!StringUtils.isEmpty(id))
			id=" id=\"" + id + "\" ";
		else id="";
		String confirm = (String)args.get("confirm");
		if(!StringUtils.isEmpty(confirm)) {
			confirm = "onClick=\"return confirm('"+confirm+"');\"";
		}
		String value = (String)args.get("value");
		if (!StringUtils.isEmpty(value))
			value = "value=\""+value+"\"";
		out.print("<button type=\"submit\" name=\""+name+"\" class=\"btn btn-success  btn-simpan\" "+ id + " "+confirm+" "+value+" > <i class=\"fa fa-save\"></i> "+label+"</button>&nbsp;");
	}
	
	/**
	 * tag button Kembali
	 * default label='Kembali'
	 */
	public static void _backButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");		
		if (StringUtils.isEmpty(label)) {
			if(Lang.get().equals("id")){
				label = "Kembali";
			}else{
				label = "Back";
			}
		}
		if(actionDef == null)
		{
			out.println("<button class=\"btn btn-secondary\" onclick=\"history.back()\"><i class=\"fa fa-arrow-circle-left fa-fw\"></i> " + label + "</button>&nbsp;");
		}
		else
			out.print("<a class=\"btn btn-secondary \" href=\"" + actionDef.url + "\" " + serialize(args, "href") + "><i class=\"fa fa-arrow-circle-left fa-fw\"></i> " + label + "</a>&nbsp;");
	}

	/**
	 * tag button Kembali
	 * default label='Kembali'
	 */
	public static void _nextButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if (StringUtils.isEmpty(label)) {
			label = "Kembali";
		}
		if(actionDef == null)
		{
			out.println("<button class=\"btn btn-secondary\" type=\"submit\">" + label + " <i class=\"fa fa-arrow-circle-right fa-fw\"></i></button>&nbsp;");
		}
		else
			out.print("<a class=\"btn btn-secondary \" href=\"" + actionDef.url + "\" " + serialize(args, "href") + ">" + label + " <i class=\"fa fa-arrow-circle-right fa-fw\"></i></a>&nbsp;");
	}

	public static void _resetButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if (StringUtils.isEmpty(label)) {
			label = "Reset";
		}
		if(actionDef == null)
		{
			out.println("<button class=\"btn btn-secondary\" onclick=\"history.back()\"><i class=\"fa fa-times\"></i> Kembali</button>&nbsp;");
		}
		else
			out.print("<a class=\"btn btn-secondary \" href=\"" + actionDef.url + "\" " + serialize(args, "href") + "><i class=\"fa fa-times\"></i> Reset</a>&nbsp;");
	}


	public static void _filterButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String label = (String)args.get("title");
		if (StringUtils.isEmpty(label)) {
			if(Lang.get().equals("id")){
				label = "Simpan";
			}else{
				label = "Save";
			}
		}
		String name = (String)args.get("name");
		if (StringUtils.isEmpty(name)) {
			if(Lang.get().equals("id")){
				label = "Simpan";
			}else{
				label = "Save";
			}
		}
		String id=(String)args.get("id");
		if(!StringUtils.isEmpty(id))
			id=" id=\"" + id + "\" ";
		else id="";
		out.print("<button type=\"submit\" name=\""+name+"\" class=\"btn btn-success \" "+ id +  "> <i class=\"fa fa-filter\"></i> "+label+"</button>&nbsp;");
	}
	
	
	/**
	 * tag button Hapus
	 * default label='Hapus'
	 */
	public static void _deleteButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");		
		if (StringUtils.isEmpty(label)) {

			if(Lang.get().equals("id")){
				label = "Hapus";
			}else{
				label = "Delete";
			}
		}
		String name = (String)args.get("name");
		if (StringUtils.isEmpty(name)) {
			if(Lang.get().equals("id")){
				label = "Hapus";
			}else{
				label = "Delete";
			}
		}
		String eventMsg = "onclick=\"return confirm('Anda yakin akan menghapus data ini?');\"";
		if(actionDef == null) // maka actionnya dilakukan didalam form 
			out.print("<button class=\"btn btn-danger  btn-hapus\" name=\""+name+"\" type=\"submit\" "+eventMsg+"><i class=\"fa fa-trash-o fa-lg\"></i> "+label+"</button>&nbsp;");
		else
			out.print("<a class=\"btn btn-danger btn-hapus\" href=\"" + actionDef.url + "\" " + serialize(args, "action") + ' ' +eventMsg+"><i class=\"fa fa-trash-o fa-lg\"></i> "+label+"</a>&nbsp;");
	}

	public static void _newDeleteButton(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");
		if (StringUtils.isEmpty(label)) {

			if(Lang.get().equals("id")){
				label = "Hapus";
			}else{
				label = "Delete";
			}
		}
		String name = (String)args.get("name");
		if (StringUtils.isEmpty(name)) {

			if(Lang.get().equals("id")){
				label = "Hapus";
			}else{
				label = "Delete";
			}
		}

		if(actionDef == null) // maka actionnya dilakukan didalam form
			out.print("<button class=\"btn btn-danger  btn-hapus\" name=\""+name+"\" type=\"submit\"><i class=\"fa fa-trash-o fa-lg\"></i> "+label+"</button>&nbsp;");
		else
			out.print("<a class=\"btn btn-danger btn-hapus\" href=\"" + actionDef.url + "\" " + serialize(args, "action") + "><i class=\"fa fa-trash-o fa-lg\"></i> "+label+"</a>&nbsp;");
	}

	/**
	 * tag generic button, digunakan untuk penampilan tombol-tombol sesuai kostumisasi
	 * default label='Button'
	 * boleh diberi id
	 */
	public static void _button(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		String label = (String)args.get("title");		
		if (StringUtils.isEmpty(label)) {
			label = "Button";
		}
		out.print("<button type=\"submit\" ");
		String name = (String)args.get("name");
		if (!StringUtils.isEmpty(name))
			out.print("name=\""+name+"\" ");
		String id=(String)args.get("id");
		if(!StringUtils.isEmpty(id))
			out.print(" id=\"" + id + "\" ");
		String cssClass = (String)args.get("cssClass");
		if (StringUtils.isEmpty(cssClass)) {
			cssClass = "btn btn-success ";
		}
		out.print("class=\""+cssClass+"\"");
		String confirm = (String)args.get("confirm");
		if(!StringUtils.isEmpty(confirm)) {
			out.print("onClick=\"return confirm('"+confirm+"');\"");
		}
		String value = (String)args.get("value");
		if (!StringUtils.isEmpty(value))
			out.print("value=\""+value+"\"");
		out.print(">");
		String iconClass = (String)args.get("iconClass");
		if (!StringUtils.isEmpty(iconClass)) {
			out.print("<i class=\"fa "+iconClass+"\"></i>");
		}
		out.print(label);
		out.print("</button>&nbsp;");
	}
	
	/**
	 * tag button Untuk link. Digunakan untuk generic link berbentuk tombol, dengan icon yang bisa disesuaikan
	 * default label='Klik'
	 */
	public static void _link(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) {
		ActionDefinition actionDef = (ActionDefinition) args.get("action");
		String label = (String)args.get("title");		
		if (StringUtils.isEmpty(label)) {
			if(Lang.get().equals("id")){
				label = "Klik";
			}else{
				label = "Click";
			}
		}
		String cssClass = (String)args.get("cssClass");
		if (StringUtils.isEmpty(cssClass)) {
			cssClass = "btn btn-secondary ";
		}
		String iconClass = (String)args.get("iconClass");
		if (StringUtils.isEmpty(iconClass)) {
			iconClass = "";
		}
		String newTab = (String)args.get("newTab");
		String target = "";
		if (!StringUtils.isEmpty(newTab)) {
			if(Boolean.parseBoolean(newTab)) {
				target = " target='_blank' ";
			}
		}
		String confirm = (String)args.get("confirm");
		if(!StringUtils.isEmpty(confirm)) {
			confirm = "onClick=\"return confirm('"+confirm+"');\"";
		}
		out.print("<a class=\""+cssClass+"\" href=\"" + actionDef.url + "\" " + serialize(args, "action") + target + ' ' + confirm +"> <i class=\"fa "+iconClass+"\"></i> "+label+"</a>&nbsp;");
	}
}
