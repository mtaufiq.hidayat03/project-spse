package ams.contracts;

import ams.models.Ams;
import ams.models.ServiceResult;
import ams.models.request.adp.AmsAdpStatusRequest;
import ams.models.response.adp.AmsAdpResponse;
import ams.models.response.ams.AmsCheckStatusResponse;
import ams.models.response.ams.AmsStatusResponse;
import ams.repositories.AmsAdpRepository;
import ams.repositories.AmsRepository;
import ams.utils.AmsUtil;
import ams.utils.restclient.model.Response;
import controllers.BasicCtr;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;
import play.i18n.Messages;
import utils.LogUtil;
import utils.osd.CertificateUtil;
import utils.osd.OSDUtil;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static ams.clients.AmsAdpClient.STATUS_VERIFIED_USER;
import static ams.models.request.adp.AmsAdpStatusRequest.PANITIA;
import static ams.models.request.adp.AmsAdpStatusRequest.REKANAN;
import static models.osd.JenisCertificate.*;

/**
 * @author HanusaCloud on 7/26/2018
 */
public interface ActiveUserAms extends ActiveUserContract {

    String TAG = "ActiveUserAms";

    AmsContract getAms();
    AmsUserContract getUser();
    void setAms(AmsContract model);

    default void setAmsRelatedData() {
        if(isRekanan()) // sementara AMS penyedia didisable
            return;
        LogUtil.debug(TAG, "set ams related data");
        if (!CertificateUtil.enableCa) {
            return;
        }
        setCertId(null);
        setAms(new Ams());
        AmsAdpStatusRequest statusRequest = new AmsAdpStatusRequest();
        AmsUserContract user = getUser();
        if (user != null) {
            statusRequest.setFromUser(user, getAmsDataType());
        }
        LogUtil.debug(TAG, statusRequest);
        Response<AmsAdpResponse> response = AmsAdpRepository.getStatus(statusRequest);
        if (response != null && response.isNetworkException()) {
            LogUtil.debug(TAG, "caught network exception!");
            setAms(null);
            setAms(new Ams(response));
            return;
        }
        if (response != null && response.body != null) {
            setAms(null);
            AmsContract ams = new Ams(response.body);
            setAms(ams);
            setStatus(ams);
            setCertificateStatus(response.body, getCerId());
            checkApprovalStatus(getCerId());
            updateUserData(user, response);
        }
    }

    default void updateUserData(AmsUserContract user, Response<AmsAdpResponse> response) {
        if (user != null && (isRekanan() || isPanitia()) && user.allowAttachAmsIdAndCerId()) {
            LogUtil.debug(TAG, "update cerId and amsId on partner or employee");
            user.updateCerOrAms(response.body);
            user.saveModel();
        }
    }

    default ServiceResult isCertificateValid() {
        if(isRekanan()) // sementara AMS penyedia didisable
            new ServiceResult("success");
        LogUtil.debug(TAG, "is certificate valid");
        AmsAdpStatusRequest statusRequest = new AmsAdpStatusRequest();
        AmsUserContract user = getUser();
        if (user != null) {
            statusRequest.setFromUser(user, getAmsDataType());
        }
        Response<AmsAdpResponse> response = AmsAdpRepository.getStatus(statusRequest);
        if (response.isNetworkException()) {
            return new ServiceResult(Messages.get("ams.gagal_terhubung_adp"));
        }
        AmsAdpResponse amsAdpResponse = response.body;
        X509Certificate certificate;
        if (amsAdpResponse == null
                || amsAdpResponse.certificate == null
                || (certificate = amsAdpResponse.certificate.getX509()) == null) {
            return new ServiceResult(Messages.get("ams.tdk_memiliki_sertifikat"));
        }
        AmsUtil.OcspResult result = AmsUtil.checkOCSPCertificate(certificate);
        LogUtil.debug(TAG, result);
        if (result.isForcedCreate()) {
            return new ServiceResult(result.getMessage());
        }
        if (result.isNetwork()) {
            return new ServiceResult(Messages.get("ams.tdtkso"));
        }
        return new ServiceResult(result.isValid(), result.isValid() ? Messages.get("ams.suskse") : Messages.get("ams.tidak_valid"));
    }

    default String getAmsDataType() {
        return isRekanan() ? REKANAN : PANITIA;
    }

    default void setAmsIdThroughAmsModel() {
        LogUtil.debug(TAG, "set ams_id through ams model.");
        AmsUserContract userAms = getUser();
        if (userAms != null) {
            userAms.setAmsId(getAms().getId());
            userAms.saveModel();
        }
    }

    default String getCertificationType() {
        if (isRekanan()) {
            return CERT_PENYEDIA;
        } else if (isPanitia()) {
            return CERT_PANITIA;
        }
        return "";
    }

    default void setCertificationType() {
        LogUtil.debug(TAG, "send ams data to adp");
        if (getAms().isCertificationTypeEmpty()) {
            getAms().setCertificationType(getCertificationType());
        }
    }

    default void setCsrExists() {
        if (isAmsExist()) {
            this.getAms().setNewStatus(isRekanan());
            save();
        }
    }

    default void setRenewStatus(String token) {
        if (isAmsExist()) {
            this.getAms().setReNewStatus(isRekanan(), token);
            save();
        }
    }

    default void generateAms() {
        setAmsRelatedData();
        save();
    }

    default boolean isAmsExist() {
        return getAms() != null;
    }

    default boolean mustRegisterAms() {
        return allowRegister();
    }

    default boolean allowRegister() {
        return isAmsExist() && getAms().allowToRegister();
    }

    default boolean cerIdExist() {
        return getCerId() != null && getCerId() != 0;
    }

    default boolean hasCertificateButNotRegistered() {
        return allowRegister() && cerIdExist();
    }

    default boolean mustSendEnrollmentDocument() {
        return isAmsExist()
                && getAms().isVerifiedUser()
                && getAms().isCertActive()
                && getAms().isAmsEnrollTokenEmpty();
    }

    default boolean mustSendEnrollmentAndCsr() {
        return isAmsExist()
                && getAms().isVerifiedUser()
                && getAms().isCsrRequested()
                && getAms().isAmsEnrollTokenEmpty();
    }

    default void setStatus(AmsContract ams) {
        if (!getAms().isVerifiedCertificate() && getAms().getId() != null) {
            LogUtil.debug(TAG, "get status from ams API.");
            Response<AmsStatusResponse> response = AmsRepository.getStatusResponse(ams);
            if (response != null && response.isNetworkException()) {
                getAms().forceSetExceptionExist();
                getAms().setMessage(response.formattedMessage("ams.network-exception", "AMS"));
                return;
            }
            if (response != null
                    && response.body != null
                    && response.body.isUserVerified()
                    && !getAms().isWaitingForPhoto()
                    && getAms().isAmsEnrollTokenEmpty()) {
                getAms().setAmsStatus(STATUS_VERIFIED_USER);
            }
        }
    }

    default void checkApprovalStatus(Long cerId) {
        LogUtil.debug(TAG, "checking approval status");
        if (!(getAms().isCsrRequested() || getAms().isCertificateRevocationRequested())) {
            LogUtil.debug(TAG, "no need to check approval status...");
            return;
        }
        if (getAms().isAmsEnrollTokenEmpty()) {
            LogUtil.debug(TAG, "enroll token is empty");
            return;
        }
        Response<AmsCheckStatusResponse> response = AmsRepository.checkStatus(getAms());
        if (response == null || response.isNetworkException()) {
            getAms().forceSetExceptionExist();
            getAms().setMessage(Messages.get("ams.network-exception", "AMS"));
            return;
        }

        AmsCheckStatusResponse model = response.body;
        if (model == null || !model.success) {
            LogUtil.debug(TAG, "check approval status failed");
            return;
        }
        if (!model.isRejected()) {
            LogUtil.debug(TAG, "approval not being rejected so pass it");
            return;
        }
        boolean resetToken = false;
        if (getAms().isCertificateRevocationRequested()) {
            LogUtil.debug(TAG, "revert back to certificate status active");
            getAms().setCerType(isRekanan() ? CERT_PENYEDIA : CERT_PANITIA);
        } else if (getAms().isCsrRequested()) {
            LogUtil.debug(TAG, "reject csr");
            getAms().setCerType(isRekanan() ? CSR_PENYEDIA_REJECTED : CSR_PANITIA_REJECTED);
            getAms().setAmsStatus(STATUS_VERIFIED_USER);
            resetToken = true;
        }
        LogUtil.debug(TAG, getAms());
        getAms().setApprovalMessage(model.message);
        AmsAdpRepository.changeCertStatus(getAms().getCerId() != null ? getAms().getCerId() : cerId,getAms(),resetToken);
    }

    default void requestRevoked() {
        this.getAms().setCerType(isRekanan() ? REVOKE_REQ_PENYEDIA : REVOKE_REQ_PANITIA);
    }

    default void setCertificateStatus(AmsAdpResponse response, Long certId) {
        if (response.certificate == null) {
            LogUtil.debug(TAG, "certificate null get from adp");
            response.certificate = OSDUtil.getLastCertificateFromInaproc(certId);
        }
        if (response.certificate == null) {
            LogUtil.debug(TAG, "certificate null");
            return;
        }
        LogUtil.debug(TAG, response.certificate);
        getAms().setCerType(response.certificate.cer_jenis);
        getAms().setCsrExists(response.certificate.isCsrExist());
        X509Certificate x509Certificate = response.certificate.getX509();
        if (x509Certificate == null) {
            //getAms().message = Messages.get("ams.fail-to-extract-x509");
            LogUtil.debug(TAG, "x509 null because decoding fail, check pem format!");
            return;
        }
        AmsUtil.OcspResult ocspResult = AmsUtil.checkOCSPCertificate(response.certificate.getX509());
        getAms().setOcspStatus(ocspResult.getOcspStatus());
        if (ocspResult.isNetwork() || ocspResult.isUnknown()) {
            getAms().setFromOcspResult(ocspResult);
        }
        revoking(response, ocspResult);
        expireDateValidation(x509Certificate, response);
    }

    default void expireDateValidation(X509Certificate x509Certificate, AmsAdpResponse response) {
        final Date endValid = x509Certificate.getNotAfter();
        final Date startValid = x509Certificate.getNotBefore();
        LogUtil.debug(TAG, endValid);
        LogUtil.debug(TAG, startValid);
        final Date now = ConfigurationDao.isLatihan() ? BasicCtr.newDate() : new Date();
        getAms().setCertificateExpired(startValid.after(now) && now.after(endValid));
        LogUtil.debug(TAG, "is expired: " + getAms().isCertificateExpired());
        getAms().setCerExists(response.certificate.isCerExist());
        //getAms().daysLeft = Days.daysBetween(new DateTime(endValid), new DateTime(now)).getDays();
        long diff = endValid.getTime() - now.getTime();
        getAms().setDaysLeft(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
    }

    default void revoking(AmsAdpResponse response, AmsUtil.OcspResult ocspResult) {
        getAms().setRevokeFromX509(ocspResult.isRevoked());
        if (ocspResult.isRevoked() || ocspResult.isForcedCreate()) {
            LogUtil.debug(TAG, "certificate is not in a GOOD mood or in blacklist, so force set revoked.");
            if (!getAms().isRevoked()) {
                response.certificate.forceRevokedCertificate(this.isRekanan());
                getAms().setCerType(response.certificate.cer_jenis);
                LogUtil.debug(TAG, "current cer type: " + response.certificate.cer_jenis);
                LogUtil.debug(TAG, "update certificate to adp because result from ocsp is REVOKED");
                getAms().setAmsStatus(STATUS_VERIFIED_USER);
                AmsAdpRepository.changeCertStatus(response.certificate.cer_id, getAms(), true);
            }
        }
    }

    default AmsNotification<String> generateAmsNotification(String requestAction) {
        LogUtil.debug(TAG, "execute ams notification");
        if (!CertificateUtil.enableCa
                || !isAmsExist()
                || !(isRekanan() || isPanitia())
                || requestAction.contains("AmsCtr")) {
            return null;
        }
        LogUtil.debug(TAG, getAms());
        if (getAms().exceptionExist()) {
            return new AmsNotification<>("amsException", true, getAms().getMessage());
        } else if (mustRegisterAms()) {
            LogUtil.debug(TAG, "show register page url");
            return new AmsNotification<>("amsRegister", true);
        } else if (getAms().isWaitingForPhoto()) {
            LogUtil.debug(TAG, "failed to upload photo ID!");
            return new AmsNotification<>("amsPhotoFailed", true);
        } else if (!getAms().isCsrExist()) {
            LogUtil.debug(TAG, "show cert passphrase");
            return new AmsNotification<>("certPassphrase", true);
        } else if (mustSendEnrollmentDocument()) {
            LogUtil.debug(TAG, "show enrollment page url");
            return new AmsNotification<>("amsSendEnrollment", true);
        } else if (mustSendEnrollmentAndCsr()) {
            LogUtil.debug(TAG, "show csr page url");
            return new AmsNotification<>("amsSendCsr", true);
        } else if (getAms().isWaitingForCert()) {
            LogUtil.debug(TAG, "show waiting for cert approval");
            return new AmsNotification<>("amsWaitingCert", true);
        } else if (getAms().isLessThan30Days()) {
            LogUtil.debug(TAG, "show renewal");
            return new AmsNotification<>("isRenewalNeeded", true, String.valueOf(getAms().getDaysLeft()));
        } else if (getAms().isCertificateRevocationRequested()) {
            LogUtil.debug(TAG, "certificate revocation is being requested");
            return new AmsNotification<>("revocationRequested", true);
        } else if (getAms().isNotAllowToBeUsed()) {
            LogUtil.debug(TAG, "show certificate expired or can not be used any longer");
            return new AmsNotification<>("allowTobeUsed", true, getAms().getStatusApprovalMessage());
		} else if (!CommonUtil.isEmpty(getAms().getMessage())) {
            return new AmsNotification<>("generalError", true, getAms().getMessage());
        }
        return null;
    }

    default long getRenewalDay() {
        return isAmsExist() && getAms().getDaysLeft() != null ? getAms().getDaysLeft() : 0;
    }

    class AmsNotification<T> {

        private final String key;
        private final boolean value;
        private final T payload;

        public AmsNotification(String key, boolean value) {
            this.key = key;
            this.value = value;
            this.payload = null;
        }

        public AmsNotification(String key, boolean value, T payload) {
            this.key = key;
            this.value = value;
            this.payload = payload;
        }

        public String getKey() {
            return key;
        }

        public boolean isValue() {
            return value;
        }

        public T getPayload() {
            return payload;
        }

    }

}
