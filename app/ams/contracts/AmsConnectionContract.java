package ams.contracts;

import ams.models.AmsAuth;
import ams.models.request.ams.AmsRegisterRequest;
import ams.models.request.ams.AmsRevokeRequest;
import ams.models.request.ams.DownloadCertificateRequest;
import ams.models.response.ams.*;
import ams.utils.restclient.annotations.*;
import ams.utils.restclient.contracts.Request;
import ams.utils.restclient.model.RequestBody;

/**
 * @author HanusaCloud on 5/8/2018
 */
public interface AmsConnectionContract {

    @Post("/oauth/token")
    Request<AmsTokenResponse> getToken(@Param AmsAuth model);

    @Post("/rest/v1/user")
    @Headers({
            @Header(key = "Content-Type", value = "application/x-www-form-urlencoded")
    })
    Request<AmsRegisterResponse> registerUser(@Headers ams.utils.restclient.model.Headers header,
                                              @Param AmsRegisterRequest form);

    @Post("/rest/v1/enroll/doc/{amsId}")
    Request<AmsEnrollTokenResponse> enrollmentDocument(@Path("amsId") String amsId,
                                                       @Param RequestBody request);

    @Post("/rest/v1/user/{amsId}")
    @Headers({
            @Header(key = "Content-Type", value = "multipart/form-data")
    })
    Request<AmsBaseResponse> uploadPhoto(@Path("amsId") String amsId,
                                         @Param RequestBody requestBody);

    @Post("/rest/v1/enroll/{enrollToken}")
    @Headers({
            @Header(key = "Content-Type", value = "text/plain")
    })
    Request<AmsBaseResponse> sendCsr(@Path("enrollToken") String enrollToken,
                                     @Param RequestBody requestBody);

    @Get("/rest/v1/user/{amsId}?produk={product}")
    Request<AmsStatusResponse> getStatus(@Headers ams.utils.restclient.model.Headers header,
                                         @Path("amsId") String amsId,
                                         @Path("product") String product);

    @Get("/rest/v1/user/crt/{amsId}")
    Request<AmsCertificateResponse> getCertificate(@Headers ams.utils.restclient.model.Headers header,
                                                   @Path("amsId") String amsId,
                                                   @Param DownloadCertificateRequest request);

    @Get("/rest/v1/enroll/{amsId}?produk={product}")
    Request<AmsSubjectResponse> getSubject(@Headers ams.utils.restclient.model.Headers headers,
                                        @Path("amsId") String amsId,
                                        @Path("product") String product);

    @Post("/rest/v1/revoke/{amsId}")
    @Headers({
            @Header(key = "Content-Type", value = "application/x-www-form-urlencoded")
    })
    Request<AmsRevokeResponse> revoke(@Headers ams.utils.restclient.model.Headers headers,
                                      @Path("amsId") String amsId,
                                      @Param AmsRevokeRequest request);

    @Post("/rest/v1/renewal/{amsId}?serial={oldSerial}")
    @Headers({
            @Header(key = "Content-Type", value = "text/plain")
    })
    Request<AmsRenewalResponse> renewal(@Path("amsId") String amsId,
                                        @Path("oldSerial") String serial,
                                        @Param RequestBody request);

    @Get("/rest/v1/user/req/status/{enrollToken}")
    Request<AmsCheckStatusResponse> checkStatus(@Headers ams.utils.restclient.model.Headers headers,
                                                @Path("enrollToken") String enrollToken);

}
