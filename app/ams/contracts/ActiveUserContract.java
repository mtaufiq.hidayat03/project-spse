package ams.contracts;

import models.secman.Group;

/**
 * @author HanusaCloud on 10/2/2018
 */
public interface ActiveUserContract {

    void setCertId(Long certId);
    void save();

    Group getGroup();
    Long getCerId();
    String getUserId();
    boolean isRekanan();
    boolean isPanitia();
    Long getRekananId();
    Long getPegawaiId();
    String getUniqueId(); // it can be rkn_npwp for rekanan or peg_nip for employee

}
