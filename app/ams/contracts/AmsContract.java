package ams.contracts;

import ams.utils.AmsUtil;

/**
 * @author HanusaCloud on 9/4/2018
 */
public interface AmsContract {

    Long getCerId();
    String getId();
    String getAmsStatus();
    String getAmsEnrollmentToken();
    String getCertificationType();
    String getOriginRepo();
    String getCerType();
    boolean isCertificateExpired();
    boolean isRevokeFromX509();
    String getOcspStatus();
    Long getDaysLeft();
    boolean isCsrExists();
    boolean isCerExists();
    String getMessage();
    String getApprovalMessage();
    boolean exceptionExist();
    boolean isWaitingForCertificate();
    boolean isVerifiedCertificate();
    boolean isVerifiedUser();
    boolean isAmsEnrollTokenEmpty();
    boolean isEnrollmentSent();
    boolean isEnrollmentProcessed();
    boolean isCertificateRevocationRequested();
    boolean isCerExist();
    boolean isCertActive();
    boolean isCsrExist();
    boolean allowToRegister();
    boolean isLessThan30Days();
    void forceSetCsrExists();
    void setNewStatus(boolean isRekanan);
    void setReNewStatus(boolean isRekanan, String token);
    void afterDownload(Long cerId);
    void forceSetCerExists();
    boolean isRevoked();
    boolean isNotAllowToBeUsed();
    boolean isCsrRequested();
    boolean isCsrRejected();
    boolean isWaitingForCert();
    boolean isWaitingForPhoto();
    String getStatusApprovalMessage();
    void forceSetExceptionExist();
    void setMessage(String message);
    void setCertificateExpired(boolean status);
    void setOcspStatus(String status);
    void setAmsStatus(String status);
    void setCerType(String cerType);
    void setCsrExists(boolean status);
    void setCerExists(boolean status);
    void setDaysLeft(Long daysLeft);
    void setApprovalMessage(String message);
    void setRevokeFromX509(boolean status);
    void setId(String amsId);
    void setAmsEnrollToken(String token);
    void setCerId(Long cerId);
    void setCertificationType(String type);
    boolean isCertificationTypeEmpty();

    void setFromOcspResult(AmsUtil.OcspResult result);

}
