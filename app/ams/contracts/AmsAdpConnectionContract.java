package ams.contracts;

import ams.models.request.adp.AmsAdpBaseRequest;
import ams.models.response.adp.AmsAdpBaseResponse;
import ams.models.response.adp.AmsAdpInitResponse;
import ams.models.response.adp.AmsAdpResponse;
import ams.models.response.adp.AmsAdpSaveCertificateResponse;
import ams.utils.restclient.annotations.Get;
import ams.utils.restclient.annotations.Param;
import ams.utils.restclient.annotations.Post;
import ams.utils.restclient.contracts.Request;

/**
 * @author HanusaCloud on 5/9/2018
 */
public interface AmsAdpConnectionContract {

    @Get("/ams/check-status")
    Request<AmsAdpResponse> getStatus(@Param AmsAdpBaseRequest request);

    @Get("/ams/init")
    Request<AmsAdpInitResponse> initAms(@Param AmsAdpBaseRequest request);

    @Post("/ams/save-certificate")
    Request<AmsAdpSaveCertificateResponse>  saveCertificate(@Param AmsAdpBaseRequest request);

    @Post("/ams/attach-enroll-token")
    Request<AmsAdpBaseResponse> attachEnrollToken(@Param AmsAdpBaseRequest request);

    @Post("/ams/change-certificate-status")
    Request<AmsAdpBaseResponse> changeCertificateStatus(@Param AmsAdpBaseRequest request);

    @Post("/ams/delete-certificate")
    Request<AmsAdpBaseResponse> deleteCertificate(@Param AmsAdpBaseRequest request);

}
