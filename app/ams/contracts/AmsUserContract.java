package ams.contracts;

import ams.models.response.adp.AmsAdpResponse;
import models.jcommon.util.CommonUtil;
import utils.LogUtil;

/**
 * @author HanusaCloud on 9/12/2018
 */
public interface AmsUserContract {

    String TAG = "AmsUserContract";

    Long getCerId();
    Long getUserId();
    String getAmsId();
    String getUniqueId();

    void setCerId(Long cerId);
    void setAmsId(String amsId);

    void saveModel();

    default boolean isAmsIdExist() {
        return !CommonUtil.isEmpty(getAmsId());
    }

    default boolean allowAttachAmsIdAndCerId() {
        return getCerId() == null || getCerId() == 0 || !isAmsIdExist();
    }

    default boolean isCerIdExist() {
        return getCerId() != null && getCerId() != 0;
    }

    default void updateCerOrAms(AmsAdpResponse response) {
        if (!isCerIdExist() && response.isCerIdExist()) {
            LogUtil.debug(TAG, "Update cerId");
            setCerId(response.certificate.cer_id);
        }
        if (!isAmsIdExist() && response.isAmsIdExist()) {
            LogUtil.debug(TAG, "Update amsId");
            setAmsId(response.id);
        }
    }

}
