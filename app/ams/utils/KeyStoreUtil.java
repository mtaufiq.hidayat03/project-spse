package ams.utils;

import io.netty.buffer.ByteBufAllocator;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import models.jcommon.util.CommonUtil;
import org.apache.commons.lang3.StringUtils;
import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.Dsl;
import play.Logger;
import play.Play;
import utils.LogUtil;

import javax.net.ssl.SSLEngine;
import javax.net.ssl.TrustManagerFactory;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

public class KeyStoreUtil {

    public static final String TAG = "KeyStoreUtil";
    public static KeyStore KEY_STORE;
    public static AsyncHttpClientConfig httpClientConfig;

    public static boolean isEnabledAmsKms() {
        return !StringUtils.isEmpty(Play.configuration.getProperty("spse_truststore"));
    }

    public static void generate() {
        LogUtil.debug(TAG, "generate keystore");
         KEY_STORE = generateKeyStore();
         httpClientConfig = getConfig();
    }

    private static KeyStore generateKeyStore() {
        try {
            final KeyStore keystore  = KeyStore.getInstance("jks");
            final char[] pass = Play.configuration.getProperty("spse_truststore_pass").toCharArray();
            InputStream keystoreStream = Play.classloader.getResourceAsStream(Play.configuration.getProperty("spse_truststore"));
            keystore.load(keystoreStream, pass);
            if (Play.mode.isDev()) {
                Enumeration<String> alias = keystore.aliases();
                while (alias.hasMoreElements()) {
                    LogUtil.debug(TAG, alias.nextElement());
                }
            }
            keystoreStream.close();
            LogUtil.debug(TAG, "Keystore's generated.");
            return keystore;
        } catch (Exception e) {
            LogUtil.error(TAG, e, "Failed to generate keystore!");
            return null;
        }
    }

    private static AsyncHttpClientConfig getConfig() {
        try {
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(
                    TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(KeyStoreUtil.KEY_STORE);

            SslContext context = SslContextBuilder.forClient().trustManager(tmf).build();
            SSLEngine engine = context.newEngine(ByteBufAllocator.DEFAULT);
            engine.setUseClientMode(true);
            return Dsl.config().setSslContext(context).setSslEngineFactory((asyncHttpClientConfig, s, i) -> {
                        final SSLEngine sslEngine = asyncHttpClientConfig.getSslContext()
                                .newEngine(ByteBufAllocator.DEFAULT);
                        sslEngine.setUseClientMode(true);
                        sslEngine.setNeedClientAuth(true);
                        return sslEngine;
                    })
                    .setMaxConnectionsPerHost(10).setMaxConnections(100).build();
        } catch (Exception e) {
            Logger.error(e, "Gagal create http client");
            return null;
        }
    }

    public static X509Certificate getFromKeyStore(final String key) {
        try {
            LogUtil.debug(TAG, "get cert by key: " + key);
            if (!CommonUtil.isEmpty(key) && KEY_STORE.containsAlias(key)) {
                X509Certificate cert = (X509Certificate) KEY_STORE.getCertificate(key);
                if (cert != null) {
                    LogUtil.debug(TAG, "dn: " + cert.getIssuerDN());
                }
                return cert;
            }
            return null;
        } catch (Exception e) {
            LogUtil.error(TAG, e, "get ca cert by key " + key + " from keystore failed!");
            return null;
        }
    }

}
