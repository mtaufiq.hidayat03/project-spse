package ams.utils;

import models.jcommon.util.CommonUtil;
import play.Play;
import play.mvc.Http;
import utils.LogUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author HanusaCloud on 8/23/2018
 */
public class UserAgent {

    public static final Pattern AMANDA_PATTERN = Pattern.compile("Spamkodok/(.*?)( )(.*)SPSE/(.*)");
    public static final Pattern APENDO_PATTERN = Pattern.compile("Apendo/(.*?)( )(.*)SPSE/(.*)");

    public static final String TAG = "UserAgentUtil";

    public static String extractUserAgent(Http.Request request) {
        return request != null && request.headers != null
                ? request.headers.get("user-agent").value() : null;
    }

    public static boolean isItFromAmanda(String userAgent) {
        LogUtil.debug(TAG, userAgent);
        if (CommonUtil.isEmpty(userAgent)) {
            return false;
        }
        if (Play.mode.isDev()) {
            final Matcher matcher = AMANDA_PATTERN.matcher(userAgent);
            while (matcher.find()) {
                LogUtil.debug(TAG, matcher.group());
            }
        }
        return AMANDA_PATTERN.matcher(userAgent).matches();
    }

    public static boolean allowUserAgent(Http.Request request) {
        LogUtil.debug(TAG, "checking user-agent");
        return isItFromAmanda(extractUserAgent(request));
    }

}
