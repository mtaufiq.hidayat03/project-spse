package ams.utils;

import org.bouncycastle.util.encoders.Base64;

/**
 * @author HanusaCloud on 10/29/2018
 */
public class Base64Util {

    public static String encode(byte[] bytes) {
        return Base64.toBase64String(bytes);
    }

}
