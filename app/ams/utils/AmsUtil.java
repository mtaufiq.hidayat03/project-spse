package ams.utils;

import models.jcommon.util.CommonUtil;
import play.Play;
import play.i18n.Messages;
import sun.security.provider.certpath.OCSP;
import utils.LogUtil;

import java.net.URI;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import static sun.security.provider.certpath.OCSP.RevocationStatus.CertStatus.GOOD;
import static sun.security.provider.certpath.OCSP.RevocationStatus.CertStatus.REVOKED;

/**
 * @author HanusaCloud on 7/17/2018
 */
public class AmsUtil {

    public static final String TAG = "AmsUtil";

    private static final String OCSP_URI;
    private static final X509Certificate CA_CERT;
    private static final String[] CN_BLACKLIST = new String[]{"OSD PSE"};

    static {
        Properties properties = Play.configuration;
        OCSP_URI = properties.getProperty("ams_ocsp");
        CA_CERT = KeyStoreUtil.getFromKeyStore(properties.getProperty("ams_ocsp_key"));
    }

    public static OcspResult checkOCSPCertificate(X509Certificate userCa) {
        try {
            LogUtil.debug(TAG, "check revocation using ocsp!");
            final String CN = getCnName(userCa);
            if (isCnInBlacklist(CN)) {
                return new OcspResult(
                        false,
                        Messages.get("errors.ctf"),
                        "FORCED_CREATE");
            }
            OCSP.RevocationStatus status = OCSP.check(userCa, CA_CERT, URI.create(OCSP_URI), CA_CERT, new Date());
            return new OcspResult(true, "success", statusConversion(status.getCertStatus()));
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            String message = e.getMessage();
            if (!CommonUtil.isEmpty(message) && message.contains("network error")) {
                return new OcspResult(false, "failed", "NETWORK");
            }
            return new OcspResult(false, "failed to validate");
        }
    }

    private static String getCnName(X509Certificate certificate) {
        try {
            LogUtil.debug(TAG, "IssuerDN: " + certificate.getIssuerDN().getName());
            return Arrays.stream(certificate.getIssuerDN().getName().split(","))
                    .filter(element -> element.contains("CN="))
                    .findAny()
                    .get()
                    .replace("CN=", "");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return null;
        }
    }

    private static boolean isCnInBlacklist(String cn) {
        LogUtil.debug(TAG, "check is this CN " + cn + " in blacklist");
        if (CommonUtil.isEmpty(cn)) {
            return false;
        }
        for (String blacklist : CN_BLACKLIST) {
            if (blacklist.equals(cn)) {
                return true;
            }
        }
        return false;
    }

    private static String statusConversion(OCSP.RevocationStatus.CertStatus ocspStatus) {
        LogUtil.debug(TAG, ocspStatus);
        if (ocspStatus == GOOD) {
            return "GOOD";
        } else if (ocspStatus == REVOKED) {
            return "REVOKED";
        } else {
            return "UNKNOWN";
        }
    }

    public static String oneLineFormat(String pem) {
        if (CommonUtil.isEmpty(pem)) {
            return "";
        }
        return pem.replaceAll("\\r\\n|\\r|\\n", "")
                .replace("-----BEGIN CERTIFICATE-----", "")
                .replace("-----END CERTIFICATE-----", "");
    }

    public static class OcspResult {

        private final boolean status;
        private final String message;
        private final String ocspStatus;

        public OcspResult(boolean status, String message) {
            this.status = status;
            this.message = message;
            this.ocspStatus = "UNKNOWN";
        }

        public OcspResult(boolean status, String message, String ocspStatus) {
            this.status = status;
            this.message = message;
            this.ocspStatus = ocspStatus;
        }

        public String getMessage() {
            return message;
        }

        public String getFormattedMessage() {
            if (isNetwork()) {
                return Messages.get("ams.check-ocsp-failed-network");
            } else if (isUnknown()) {
                return Messages.get("ams.check-ocsp-failed");
            }
            return getMessage();
        }

        public boolean isStatus() {
            return status;
        }

        public String getOcspStatus() {
            return ocspStatus;
        }

        public boolean isValid() {
            return ocspStatus.equals("GOOD");
        }

        public boolean isRevoked() {
            return ocspStatus.equals("REVOKED");
        }

        public boolean isUnknown() {
            return ocspStatus.equals("UNKNOWN");
        }

        public boolean isNetwork() {
            return ocspStatus.equals("NETWORK");
        }

        public boolean isForcedCreate() {
            return ocspStatus.equals("FORCED_CREATE");
        }

    }

}
