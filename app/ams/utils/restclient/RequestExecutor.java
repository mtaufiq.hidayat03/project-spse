package ams.utils.restclient;

import ams.utils.restclient.client.EncryptionContract;
import ams.utils.restclient.contracts.Request;
import ams.utils.restclient.contracts.RequestPayload;
import ams.utils.restclient.model.Response;
import models.jcommon.util.CommonUtil;
import org.asynchttpclient.AsyncHttpClient;
import utils.LogUtil;

/**
 * @author HanusaCloud on 5/8/2018
 */
public final class RequestExecutor<T> implements Request<T> {

    public static final String TAG = "RequestExecutor";

    private final AsyncHttpClient client;
    private final RequestPayload requestPayload;
    private EncryptionContract contract;

    public RequestExecutor(AsyncHttpClient client,
                           RequestPayload requestPayload,
                           EncryptionContract encryptionContract) {
        this.client = client;
        this.requestPayload = requestPayload;
        this.contract = encryptionContract;
    }

    @Override
    public Response<T> send() {
        LogUtil.debug(TAG, "Start Request\n");
        try {
            final org.asynchttpclient.Request request = requestPayload.getRequest();
            LogUtil.debug(TAG, request.getUrl());
            final org.asynchttpclient.Response response = client.prepareRequest(request)
                    .execute()
                    .get();
            LogUtil.debug(TAG, "Response Headers");
            LogUtil.debug(TAG, "");
            final String message = response.getStatusText();
            final int code = response.getStatusCode();
            LogUtil.debug(TAG, "response: " + code + " message: " + message);
            final String stringBody = response.getResponseBody();
            final String jsonResponse = stringBody != null
                    ? (contract != null
                        ? contract.dec(stringBody)
                        : stringBody)
                    : "";
            LogUtil.debug(TAG, "RAW: " + jsonResponse);
            LogUtil.debug(TAG, "End Request\n");
            return new Response<>(message,
                    code,
                    CommonUtil.isEmpty(jsonResponse) ? null : CommonUtil.fromJson(jsonResponse, requestPayload.responseType()));
        } catch (Exception ex) {
            LogUtil.error(TAG, ex);
            return new Response<>(ex);
        } finally {
            destroy();
        }
    }

    public void destroy() {
        if (contract != null) {
            contract = null;
        }
    }

}
