package ams.utils.restclient;

import ams.utils.restclient.annotations.Param;
import ams.utils.restclient.annotations.Path;
import ams.utils.restclient.client.EncryptionContract;
import ams.utils.restclient.contracts.FieldExtractor;
import ams.utils.restclient.contracts.FormUrlEncode;
import ams.utils.restclient.contracts.MethodPayload;
import ams.utils.restclient.contracts.RequestPayload;
import ams.utils.restclient.model.Header;
import ams.utils.restclient.model.Headers;
import ams.utils.restclient.model.RequestBody;
import com.google.gson.Gson;
import models.jcommon.util.CommonUtil;
import org.asynchttpclient.Request;
import org.asynchttpclient.RequestBuilder;
import org.asynchttpclient.request.body.multipart.Part;
import org.asynchttpclient.request.body.multipart.StringPart;
import play.Play;
import utils.LogUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 5/30/2018
 */
public final class Payload implements RequestPayload, FieldExtractor, FormUrlEncode {

    public static final String TAG = "Payload";

    private final MethodPayload methodPayload;
    private final Object[] args;
    private final Map<String, String> params;
    private final Headers headers;
    private final Headers.Builder headerBuilder;
    private final Map<String, Object> fragments;
    private RequestBody requestBody;
    private EncryptionContract encryptionContract;
    private final String url;

    public  Payload(MethodPayload methodPayload,
                   Object[] args,
                   EncryptionContract encryptionContract,
                   String baseUrl) {
        this.methodPayload = methodPayload;
        this.args = args;
        this.params = new HashMap<>();
        this.encryptionContract = encryptionContract;
        this.fragments = new HashMap<>();
        this.fragments.putAll(methodPayload.getFragments());
        this.headerBuilder = new Headers.Builder();
        this.headerBuilder.addAll(methodPayload.getHeaders());
        this.setParams(this.args, methodPayload.getParamAnnotations());
        this.headers = headerBuilder.build();
        this.url = this.getUrl(baseUrl, methodPayload.getPath(), this.fragments);
    }

    @Override
    public Type responseType() {
        return methodPayload.responseType();
    }

    @Override
    public Request getRequest() {
        LogUtil.debug(TAG, params);
        RequestBuilder builder = new RequestBuilder();
        for (Header entry : this.headers.getHeaders()) {
            builder.setHeader(entry.getKey(), entry.getValue());
        }
        LogUtil.debug(TAG, this.headers.getHeaders());
        builder.setMethod(methodPayload.getHttpMethod());
        if (requestBody != null) {
            LogUtil.debug(TAG, requestBody);
            LogUtil.debug(TAG, "request body is not null");
            builder.setUrl(url);
            if (requestBody.getHeaders() != null
                    && requestBody.getHeaders().getHeaders() != null
                    && requestBody.getHeaders().getHeaders().length > 0) {
                for (Header entry : requestBody.getHeaders().getHeaders()) {
                    builder.setHeader(entry.getKey(), entry.getValue());
                }
            }
            if (requestBody.getRawStringBody() != null) {
                LogUtil.debug(TAG, "set raw string body");
                builder.setBody(requestBody.getRawStringBody());
            } else if (requestBody.getRawFileBody() != null) {
                LogUtil.debug(TAG, "set raw file body");
                builder.setBody(requestBody.getRawFileBody());
            } else  {
                LogUtil.debug(TAG, "added body parts");
                for (Part part : requestBody.getParams()) {
                    builder.addBodyPart(part);
                }
            }
            return builder.build();
        }
        if (methodPayload.GET()) {
            final String path = encode(params);
            builder.setUrl(url + (!CommonUtil.isEmpty(path) ? "?" + path : ""));
        } else if (methodPayload.POST()) {
            LogUtil.debug(TAG, "generate POST payload");
            builder.setUrl(url);
            final Header contentType = getContentType();
            if (contentType != null && contentType.isMultipart()) {
                LogUtil.debug(TAG, "multipart");
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    builder.addBodyPart(
                            new StringPart(
                                    entry.getKey(),
                                    entry.getValue() == null ? "" : entry.getValue(),
                                    "UTF-8")
                    );
                }
            } else if (contentType != null && contentType.isJson()) {
                LogUtil.debug(TAG, "Json content type");
                builder.setBody(new Gson().toJson(params));
            } else {
                LogUtil.debug(TAG, "default url-form-encoded");
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    builder.addFormParam(entry.getKey(), entry.getValue());
                }
            }
        }
        destroy();
        final Request request = builder.build();
        logging(request, headers);
        return request;
    }

    private Header getContentType() {
        if (headers.getHeaders().length == 0) {
            return null;
        }
        for (Header header : headers.getHeaders()) {
            if (header.getKey().equalsIgnoreCase("content-type")) {
                return header;
            }
        }
        return null;
    }

    private void setParams(Object[] args, Annotation[][] paramAnnotations) {
        if (args != null && args.length > 0) {
            if (paramAnnotations.length > 0) {
                LogUtil.debug(TAG, "processing parameter annotations");
                for (int i = 0; i < paramAnnotations.length; i++) {
                    Annotation[] innerArray = paramAnnotations[i];
                    if (innerArray.length > 0) {
                        Annotation inner = innerArray[0];
                        if (inner instanceof Param) {
                            if (args[i] instanceof RequestBody) {
                                requestBody = (RequestBody) args[i];
                            } else {
                                Object param = args[i];
                                this.params.putAll(encryptionContract != null
                                        ? extract(param, encryptionContract)
                                        : extract(param));
                            }
                        } else if (inner instanceof Path) {
                            final String fragment = "{"+ ((Path) inner).value() + "}";
                            fragments.put(fragment, String.valueOf(args[i]));
                        } else if (inner instanceof ams.utils.restclient.annotations.Headers) {
                            this.headerBuilder.addAll((Headers) args[i]);
                        }
                    } else if (args[i] instanceof RequestBody) {
                        requestBody = (RequestBody) args[i];
                    } else if (args[i] instanceof Headers) {
                        this.headerBuilder.addAll((Headers) args[i]);
                    }
                }
            }
        }
    }

    private void destroy() {
        if (encryptionContract != null) {
            encryptionContract = null;
        }
    }

    private String getUrl(String baseUrl, String path, Map<String, Object> fragments) {
        if (!fragments.isEmpty()) {
            for (Map.Entry<String, Object> entry : fragments.entrySet()) {
                path = path.replace(entry.getKey(), entry.getValue().toString());
            }
        }
        return baseUrl + path;
    }

    private void logging(Request request, Headers headers) {
        if (Play.mode.isDev()) {
            final String TAG = "RequestLogging";
            LogUtil.debug(TAG, "(" + request.getMethod() + ") " + request.getUrl());
            LogUtil.debug(TAG, "Headers");
            LogUtil.debug(TAG, headers.toString());

            if (request.getByteData() != null) {
                LogUtil.multiline(TAG, new String(request.getByteData()));
            }
        }
    }

}
