package ams.utils.restclient.client;

/**
 * @author HanusaCloud on 5/8/2018
 */
public interface EncryptionContract {

    String enc(String content);

    String dec(String content);

}
