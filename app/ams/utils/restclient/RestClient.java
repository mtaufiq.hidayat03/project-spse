package ams.utils.restclient;

import ams.utils.restclient.client.EncryptionContract;
import ams.utils.restclient.contracts.MethodPayload;
import ams.utils.restclient.contracts.Request;
import ams.utils.restclient.contracts.RequestPayload;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.Response;
import utils.LogUtil;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author HanusaCloud on 5/8/2018
 */
public final class RestClient {

    public static final String TAG = "RestClient";

    private final Map<Method, MethodPayload> methodPayloads = new ConcurrentHashMap<>();
    private final String url;
    private final EncryptionContract encryptionContract;
    private final AsyncHttpClient client;

    public RestClient(String url, EncryptionContract encryptionContract, AsyncHttpClientConfig clientConfig) {
        this.url = url;
        this.encryptionContract = encryptionContract;
        this.client = Dsl.asyncHttpClient(clientConfig);
    }

    public <T> T create(final Class<T> service) {
        if (this.methodPayloads.isEmpty()) {
            LogUtil.debug(TAG, "methods empty, generating...");
            validate(service);
        }
        return (T) Proxy.newProxyInstance(service.getClassLoader(), new Class[]{service}, (proxy, method, args) -> {
            MethodPayload methodPayload = check(method);
            RequestPayload requestBuilder = new Payload(methodPayload, args, encryptionContract, url);
            Request<T> requestExecutor = new RequestExecutor(client, requestBuilder, encryptionContract);
            return requestExecutor;
        });
    }

    private void validate(final Class service) {
        Method[] var3 = service.getDeclaredMethods();
        int var4 = var3.length;
        for(int var5 = 0; var5 < var4; ++var5) {
            Method method = var3[var5];
            LogUtil.debug(TAG, "generating: " + method.getName());
            check(method);
        }
    }

    public void clean() {
        if (client != null && !client.isClosed()) {
            LogUtil.debug(TAG, "close http client");
            try {
                client.close();
            } catch (Exception e) {
                LogUtil.debug(TAG, e);
            }
        }
    }

    private MethodPayload check(Method method) {
        MethodPayload methodPayload = this.methodPayloads.get(method);
        if (methodPayload != null) {
            return methodPayload;
        }
        synchronized(this.methodPayloads) {
            methodPayload = this.methodPayloads.get(method);
            if (methodPayload == null) {
                methodPayload = new MethodProcessor(method);
                this.methodPayloads.put(method, methodPayload);
            }
            return methodPayload;
        }
    }

    public boolean ping() throws Exception {
        try {
            Response response = client.prepareGet(url).execute().get();
            LogUtil.debug(TAG, "Code: " + response.getStatusCode() + " Message: " + response.getStatusText());
            if (response.getStatusCode() >= 200 && response.getStatusCode() < 400) {
                return true;
            }
            return false;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

}
