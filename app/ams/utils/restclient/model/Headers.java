package ams.utils.restclient.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 5/31/2018
 */
public class Headers {

    private Header[] headers;

    public Headers(Builder builder) {
        this.headers = builder.getHeaders();
    }

    public Header[] getHeaders() {
        return headers;
    }

    public boolean containsJson() {
        if (headers.length > 0) {
            for (Header header : headers) {
                if (header.getValue().contains("application/json")) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (headers != null && headers.length > 0) {
            String glue = "";
            for (Header header : headers) {
                sb.append(glue).append(header.getKey()).append(": ").append(header.getValue());
                glue = "\n";
            }
        }
        return sb.toString();
    }

    public static class Builder {

        private final Map<String, String> headers = new HashMap<>();

        public Builder add(String key, String value) {
            if (allow(key)) {
                this.headers.put(key, value);
            }
            return this;
        }

        public Builder addAll(Map<String, String> headers) {
            if (headers != null && !headers.isEmpty()) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    add(entry.getKey(), entry.getValue());
                }
            }
            return this;
        }

        public Builder addAll(Headers headers) {
            if (headers != null) {
                for (Header header : headers.getHeaders()) {
                    add(header.getKey(), header.getValue());
                }
            }
            return this;
        }

        public Header[] getHeaders() {
            Header[] headers = new Header[this.headers.size()];
            int i = 0;
            for (Map.Entry<String, String> entry : this.headers.entrySet()) {
                headers[i] = new Header(entry.getKey(), entry.getValue());
                i++;
            }
            return headers;
        }

        private boolean allow(String key) {
            return !headers.containsKey(key);
        }

        public Headers build() {
            return new Headers(this);
        }

    }

}
