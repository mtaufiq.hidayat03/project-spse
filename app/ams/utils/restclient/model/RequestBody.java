package ams.utils.restclient.model;


import org.asynchttpclient.request.body.multipart.FilePart;
import org.asynchttpclient.request.body.multipart.Part;
import org.asynchttpclient.request.body.multipart.StringPart;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 5/31/2018
 * this class act as a bridge for Request.class,
 * because the library doesn't offer a way to modify the request body seperately.
 */
public class RequestBody {

    private final Part[] params;
    private final String rawStringBody; // set this variable if you want to send string in plain text
    private final File rawFileBody;
    private final Headers headers;

    private RequestBody(Builder builder) {
        this.params = builder.getParams();
        this.rawStringBody = builder.getStringRawBody();
        this.rawFileBody = builder.getRawFileBody();
        this.headers = builder.getHeaders();
    }

    public Part[] getParams() {
        return params;
    }

    public Headers getHeaders() {
        return headers;
    }

    public String getRawStringBody() {
        return rawStringBody;
    }

    public File getRawFileBody() {
        return rawFileBody;
    }

    public static class Builder {

        private final List<Part> params;
        private String rawStringBody;
        private File rawFileBody;
        private Headers headers;

        public Builder() {
            this.params = new ArrayList<>();
        }

        public Builder addBody(Part part) {
            this.params.add(part);
            return this;
        }

        public Builder addRawString(String value) {
            this.rawStringBody = value;
            return this;
        }

        public Builder addHeaders(Headers headers) {
            this.headers = headers;
            return this;
        }

        public Builder addRawFile(File file) {
            this.rawFileBody = file;
            return this;
        }

        public Builder addBody(String key, String value) {
            this.params.add(new StringPart(key, value));
            return this;
        }

        public Builder addBody(String key, File file) {
            this.params.add(new FilePart(key, file));
            return this;
        }

        public Headers getHeaders() {
            return this.headers;
        }

        public Part[] getParams() {
            return this.params.toArray(new Part[0]);
        }

        public String getStringRawBody() {
            return rawStringBody;
        }

        public File getRawFileBody() {
            return rawFileBody;
        }

        public RequestBody build() {
            return new RequestBody(this);
        }

    }

    public static RequestBody create(String contentType, String value) {
        return new Builder().addBody(new StringPart("", value, contentType)).build();
    }

}
