package ams.utils.restclient.model;

/**
 * @author HanusaCloud on 5/31/2018
 */
public class Header {

    private final String key;
    private final String value;

    public Header(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public boolean isJson() {
        return value.contains("application/json");
    }

    public boolean isMultipart() {
        return value.contains("multipart");
    }

}
