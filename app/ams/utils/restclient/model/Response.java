package ams.utils.restclient.model;

import play.i18n.Messages;

import java.net.ConnectException;

/**
 * @author HanusaCloud on 5/8/2018
 */
public class Response<T> {

    public static final int NETWORK_EXCEPTION = -2;

    public final String message;
    public final int code;
    public final T body;

    public Response() {
        this.message = "Opss.. something's not right!";
        this.code = -1;
        this.body = null;
    }

    public Response(String message, int code, T body) {
        this.message = message;
        this.code = code;
        this.body = body;
    }

    public Response(String message, int code) {
        this.message = message;
        this.code = code;
        this.body = null;
    }

    public Response(String message) {
        this.message = message;
        this.code = -1;
        this.body = null;
    }

    public Response(Throwable ex) {
        this.message = ex.getMessage();
        this.body = null;
        if (ex instanceof ConnectException || this.message.contains("ConnectException")) {
            this.code = NETWORK_EXCEPTION;
        } else {
            this.code = -1;
        }
    }

    public boolean isNetworkException() {
        return this.code == NETWORK_EXCEPTION;
    }

    public String formattedMessage(String key ,String append) {
        if (isNetworkException()) {
            return Messages.get(key, append);
        } else {
            return message;
        }
    }

}
