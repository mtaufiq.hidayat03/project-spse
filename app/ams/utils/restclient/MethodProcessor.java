package ams.utils.restclient;

import ams.utils.restclient.annotations.Get;
import ams.utils.restclient.annotations.Header;
import ams.utils.restclient.annotations.Post;
import ams.utils.restclient.contracts.FieldExtractor;
import ams.utils.restclient.contracts.FormUrlEncode;
import ams.utils.restclient.contracts.MethodPayload;
import ams.utils.restclient.model.Headers;
import utils.LogUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author HanusaCloud on 5/8/2018
 */
public final class MethodProcessor implements MethodPayload, FieldExtractor, FormUrlEncode {

    public static final String TAG = "MethodPayload";
    private final static Pattern URL_PATTERN = Pattern.compile("\\{([a-zA-Z][a-zA-Z0-9_-]*)\\}");

    private final String methodName;
    private final Type returnType;
    private final Annotation[] methodAnnotations;
    private final Annotation[][] paramAnnotations;
    private final Headers headers;
    private final String httpMethod;
    private final String path;
    private final Map<String, Object> fragments;

    public MethodProcessor(Method method) {
        this.methodName = method.getName();
        this.returnType = method.getGenericReturnType();
        this.methodAnnotations = method.getAnnotations();
        this.paramAnnotations = method.getParameterAnnotations();
        final String[] results = this.annotationProcessing(this.methodAnnotations);
        this.httpMethod = results[0];
        this.path = results[1];
        this.headers = extractHeaders(this.methodAnnotations);
        this.fragments = Collections.unmodifiableMap(this.extractFragments(this.path));
    }

    private String[] annotationProcessing(Annotation[] methodAnnotations) {
        String[] results = {"",""};
        LogUtil.debug(TAG, "processing method annotations");
        if (methodAnnotations != null && methodAnnotations.length > 0) {
            for (Annotation annotation : methodAnnotations) {
                if (annotation instanceof Get) {
                    results[0] = METHOD_GET;
                    results[1] = ((Get) annotation).value();
                } else if (annotation instanceof Post) {
                    results[0] = METHOD_POST;
                    results[1] = ((Post) annotation).value();
                }
            }
        } else {
            error("Method must have one annotation");
        }
        return results;
    }

    private Headers extractHeaders(Annotation[] methodAnnotations) {
        Headers.Builder builder = new Headers.Builder();
        LogUtil.debug(TAG, "extract headers from method annotations");
        if (methodAnnotations != null && methodAnnotations.length > 0) {
            for (Annotation annotation : methodAnnotations) {
                if (annotation instanceof ams.utils.restclient.annotations.Headers) {
                    for (Header header : ((ams.utils.restclient.annotations.Headers) annotation).value()) {
                        builder.add(header.key(), header.value());
                    }
                }
            }
        }
        return builder.build();
    }

    @Override
    public Type responseType() {
        return TypeUtil.getParameterUpperBound(0, (ParameterizedType) returnType);
    }

    @Override
    public String getPath() {
        return path;
    }

    private Map<String, Object> extractFragments(String path) {
        Map<String, Object> fragments = new HashMap<>();
        if (path.contains("{") && path.contains("}")) {
            final Matcher matcher = URL_PATTERN.matcher(path);
            while (matcher.find()) {
                fragments.put(matcher.group(), "");
            }
        }
        return fragments;
    }

    @Override
    public String getMethodName() {
        return methodName;
    }

    @Override
    public Headers getHeaders() {
        return headers;
    }

    @Override
    public Map<String, Object> getFragments() {
        return this.fragments;
    }

    @Override
    public Annotation[][] getParamAnnotations() {
        return paramAnnotations;
    }

    @Override
    public String getHttpMethod() {
        return this.httpMethod;
    }

    private RuntimeException error(String message) {
        throw new IllegalArgumentException(getMethodName() + " " + message, null);
    }

}
