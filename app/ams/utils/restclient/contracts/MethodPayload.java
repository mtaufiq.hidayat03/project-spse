package ams.utils.restclient.contracts;

import ams.utils.restclient.model.Headers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author HanusaCloud on 5/30/2018
 */
public interface MethodPayload {

    String METHOD_GET = "GET";
    String METHOD_POST = "POST";

    Type responseType();
    String getPath();
    String getMethodName();
    Headers getHeaders();
    Map<String, Object> getFragments();
    Annotation[][] getParamAnnotations();
    String getHttpMethod();

    default boolean GET() {
        return getHttpMethod().equals(METHOD_GET);
    }

    default boolean POST() {
        return getHttpMethod().equals(METHOD_POST);
    }

}
