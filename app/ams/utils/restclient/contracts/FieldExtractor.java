package ams.utils.restclient.contracts;

import ams.utils.restclient.annotations.CloakAble;
import ams.utils.restclient.annotations.Key;
import ams.utils.restclient.client.EncryptionContract;
import models.jcommon.util.CommonUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 5/8/2018
 */
public interface FieldExtractor {

    default Map<String, String> extract(Object param) {
        return extract(param, null);
    }

    default Map<String, String> extract(Object param, EncryptionContract contract) {
        Map<String, String> map = new HashMap<>();
        if (param == null) {
            return map;
        }
        try {
            Field[] fields = param.getClass().getFields();
            for (Field field : fields) {
                Annotation[] fieldAnnotations = field.getAnnotations();
                String fieldName = "";
                boolean containCloakAble = false;
                if (!CommonUtil.isEmpty(fieldAnnotations)) {
                    for (Annotation fieldAnnotation : fieldAnnotations) {
                        if (fieldAnnotation instanceof Key) {
                            fieldName = ((Key) fieldAnnotation).value();
                        } else if (fieldAnnotation instanceof CloakAble) {
                            fieldName = ((CloakAble) fieldAnnotation).value();
                            containCloakAble = true;
                        } else {
                            fieldName = field.getName();
                        }
                    }
                } else {
                    fieldName = field.getName();
                }
                final String fieldValue = (String) field.get(param);
                final String value = containCloakAble && contract != null ? contract.enc(fieldValue) : fieldValue;
                map.put(fieldName, value);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    default String getString(Field field) {
        return String.valueOf(field);
    }

}
