package ams.utils.restclient.contracts;

import java.net.FileNameMap;
import java.net.URLConnection;

/**
 * @author HanusaCloud on 5/9/2018
 */
public interface MimeTypeExtractor {

    default String getMimeType(String path) {
        try {
            FileNameMap fileNameMap = URLConnection.getFileNameMap();
            return fileNameMap.getContentTypeFor(path);
        } catch (Exception e) {
            return "image/jpeg";
        }
    }

}
