package ams.utils.restclient.contracts;

import ams.utils.restclient.annotations.Key;
import models.jcommon.util.CommonUtil;
import play.libs.URLs;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * @author HanusaCloud on 5/8/2018
 */
public interface FormUrlEncode {

    default String encodeModel(Object source, String delimiter) {
        StringBuilder sb = new StringBuilder();
        if (source == null) {
            return "";
        }
        try {
            Field[] fields = source.getClass().getFields();
            String glue = "";
            for (Field field : fields) {
                Annotation[] fieldAnnotations = field.getAnnotations();
                String fieldName = "";
                if (!CommonUtil.isEmpty(fieldAnnotations)) {
                    for (Annotation fieldAnnotation : fieldAnnotations) {
                        if (fieldAnnotation instanceof Key) {
                            fieldName = ((Key) fieldAnnotation).value();
                        } else {
                            fieldName = field.getName();
                        }
                    }
                } else {
                    fieldName = field.getName();
                }
                final String fieldValue = (String) field.get(source);
                sb.append(glue).append(fieldName).append("=").append(fieldValue);
                glue = delimiter;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    default String encode(Map<String, String> source) {
        StringBuilder sb = new StringBuilder();
        if (source != null && !source.isEmpty()) {
            String glue = "";
            for (Map.Entry<String, String> map : source.entrySet()) {
                sb.append(glue).append(map.getKey())
                        .append("=")
                        .append(URLs.encodePart(map.getValue() != null ? map.getValue() : ""));
                glue = "&";
            }
        }
        return sb.toString();
    }

}
