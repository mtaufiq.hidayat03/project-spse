package ams.utils.restclient.contracts;

import ams.utils.restclient.model.Response;

/**
 * @author HanusaCloud on 5/8/2018
 */
public interface Request<T> {

    Response<T> send();

}
