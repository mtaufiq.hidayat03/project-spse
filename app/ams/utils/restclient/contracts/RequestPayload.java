package ams.utils.restclient.contracts;

import org.asynchttpclient.Request;

import java.lang.reflect.Type;

/**
 * @author HanusaCloud on 5/8/2018
 */
public interface RequestPayload {

    Type responseType();
    Request getRequest();

}
