package ams.repositories;

import ams.clients.AmsClient;
import ams.contracts.AmsContract;
import ams.models.AmsAuth;
import ams.models.form.FormRegisterAms;
import ams.models.request.ams.AmsRegisterRequest;
import ams.models.request.ams.AmsRevokeRequest;
import ams.models.request.ams.DownloadCertificateRequest;
import ams.models.response.ams.*;
import ams.utils.restclient.contracts.Request;
import ams.utils.restclient.model.RequestBody;
import ams.utils.restclient.model.Response;
import play.Play;
import utils.LogUtil;

import java.io.File;

/**
 * @author HanusaCloud on 5/8/2018
 */
public class AmsRepository {

    public static final String TAG = "AmsRepository";

    public static String getClientId() {
        return Play.configuration.getProperty("ams_client_id", "76732347");
    }

    public static String getClientSecret() {
        return Play.configuration.getProperty("ams_client_secret", "v9h6-67vt-ft3l-adqf");
    }

    public static String getGrantType() {
        return Play.configuration.getProperty("ams_client_grant_type", "client_credentials");
    }

    public static String getDigitalSignature() {
        return Play.configuration.getProperty("ams_digital_signature", "Tanda Tangan Digital dan Enkripsi");
    }

    public static AmsTokenResponse getToken() {
        LogUtil.debug(TAG, "get token from ams server");
        Request<AmsTokenResponse> request = AmsClient.open()
                .getToken(new AmsAuth(getClientId(), getClientSecret(), getGrantType()));
        Response<AmsTokenResponse> response = request.send();
        LogUtil.debug(TAG, response);
        return response.body;
    }

    public static AmsRegisterResponse registerUser(FormRegisterAms data) {
        LogUtil.debug(TAG, "register user");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null.. early exits");
            return null;
        }
        LogUtil.debug(TAG, data);
        Request<AmsRegisterResponse> request = AmsClient.open()
                .registerUser(
                        token.getHeaders(),
                        new AmsRegisterRequest(data));
        Response<AmsRegisterResponse> response = request.send();
        LogUtil.debug(TAG, response);
        return response.body;
    }

    public static AmsBaseResponse uploadIdentityImage(AmsContract ams, File file) {
        LogUtil.debug(TAG, "upload image");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        RequestBody requestBody = new RequestBody.Builder()
                .addHeaders(token.getHeaders())
                .addBody("ktp", file)
                .build();
        Request<AmsBaseResponse> request = AmsClient.open()
                .uploadPhoto(
                        ams.getId(),
                        requestBody
                );
        Response<AmsBaseResponse> response = request.send();
        return response.body;
    }

    public static AmsEnrollTokenResponse enrollment(AmsContract ams, File file) {
        LogUtil.debug(TAG, "do enrollment");
        AmsTokenResponse token = getToken();
        if (token == null) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        RequestBody requestBody = new RequestBody.Builder()
                .addHeaders(token.getHeaders())
                .addBody("produk", getDigitalSignature())
                .addBody("jenis", "Individu")
                .addBody("surat_rekomendasi", file)
                .build();
        Request<AmsEnrollTokenResponse> request = AmsClient.open()
                .enrollmentDocument(
                        ams.getId(),
                        requestBody
                );
        Response<AmsEnrollTokenResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsBaseResponse sendCsr(AmsContract ams, String text) {
        LogUtil.debug(TAG, "send csr");
        if (ams.getAmsEnrollmentToken() == null) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        LogUtil.debug(TAG, "Enrollment token: " + ams.getAmsEnrollmentToken());
        AmsTokenResponse token = getToken();
        if (token == null) {
            LogUtil.debug(TAG, "access token null");
            return null;
        }
        RequestBody requestBody = new RequestBody.Builder()
                .addHeaders(token.getHeaders())
                .addRawString(text)
                .build();
        Request<AmsBaseResponse> request = AmsClient.open()
                .sendCsr(ams.getAmsEnrollmentToken(),
                        requestBody);
        Response<AmsBaseResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsCertificateResponse downloadCertificate(AmsContract ams) {
        LogUtil.debug(TAG, "download certificate");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        DownloadCertificateRequest payload = new DownloadCertificateRequest(
                ams.getAmsEnrollmentToken(),
                getDigitalSignature());
        Request<AmsCertificateResponse> request = AmsClient.open()
                .getCertificate(
                        token.getHeaders(),
                        ams.getId(),
                        payload);
        Response<AmsCertificateResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsStatusResponse getStatus(AmsContract user) {
        return getStatusResponse(user).body;
    }

    public static Response<AmsStatusResponse> getStatusResponse(AmsContract ams) {
        LogUtil.debug(TAG, "get status from AMS");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        if (ams.getId() == null) {
            LogUtil.debug(TAG, "amsId is null");
            return null;
        }
        Request<AmsStatusResponse> request = AmsClient.open()
                .getStatus(
                        token.getHeaders(),
                        ams.getId(),
                        getDigitalSignature()
                );
        Response<AmsStatusResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response;
    }

    public AmsSubjectResponse getSubject(AmsContract ams) {
        LogUtil.debug(TAG, "get subject");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        Request<AmsSubjectResponse> request = AmsClient.open()
                .getSubject(
                        token.getHeaders(),
                        ams.getId(),
                        getDigitalSignature()
                );
        Response<AmsSubjectResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsRevokeResponse revoke(AmsContract ams, String certSerial) {
        LogUtil.debug(TAG, "revoke action");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        Request<AmsRevokeResponse> request = AmsClient.open()
                .revoke(token.getHeaders(),
                        ams.getId(),
                        new AmsRevokeRequest(certSerial, null));
        Response<AmsRevokeResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsRenewalResponse renewal(String amsId, String oldSerial, String base64Csr) {
        LogUtil.debug(TAG, "renewal action");
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        RequestBody requestBody = new RequestBody.Builder()
                .addHeaders(token.getHeaders())
                .addRawString(base64Csr)
                .build();
        Request<AmsRenewalResponse> request = AmsClient.open()
                .renewal(amsId,
                        oldSerial,
                        requestBody
                );
        Response<AmsRenewalResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static Response<AmsCheckStatusResponse> checkStatus(AmsContract ams) {
        AmsTokenResponse token = getToken();
        if (token == null || !token.isTokenExist()) {
            LogUtil.debug(TAG, "token null");
            return null;
        }
        if (ams.isAmsEnrollTokenEmpty()) {
            return null;
        }
        Request<AmsCheckStatusResponse> request = AmsClient.open()
                .checkStatus(
                        token.getHeaders(),
                        ams.getAmsEnrollmentToken()
                );
        Response<AmsCheckStatusResponse> response = request.send();
        LogUtil.debug(TAG, response);
        return response;
    }

}
