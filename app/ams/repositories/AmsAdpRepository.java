package ams.repositories;

import ams.clients.AmsAdpClient;
import ams.contracts.ActiveUserAms;
import ams.contracts.AmsContract;
import ams.models.request.adp.*;
import ams.models.response.adp.AmsAdpBaseResponse;
import ams.models.response.adp.AmsAdpInitResponse;
import ams.models.response.adp.AmsAdpResponse;
import ams.models.response.adp.AmsAdpSaveCertificateResponse;
import ams.utils.restclient.contracts.Request;
import ams.utils.restclient.model.Response;
import com.google.gson.JsonObject;
import models.osd.Certificate;
import utils.LogUtil;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class  AmsAdpRepository  {

    public static final String TAG = "AmsAdpRepository";

    public static Response<AmsAdpResponse> getStatus(AmsAdpStatusRequest statusRequest) {
        return getstatusFromAdp(statusRequest);
    }

    public AmsAdpResponse getStatus(ActiveUserAms user) {
        return getstatusFromAdp(new AmsAdpStatusRequest(user)).body;
    }

    private static Response<AmsAdpResponse> getstatusFromAdp(AmsAdpStatusRequest statusRequest) {
        LogUtil.debug(TAG, "get status ams from adp");
        Request<AmsAdpResponse> request = AmsAdpClient.open()
                .getStatus(statusRequest.getRequest());
        Response<AmsAdpResponse> response = request.send();
        LogUtil.debug(TAG, response);
        return response;
    }

    public static AmsAdpInitResponse initAms(AmsAdpInputRequest model) {
        LogUtil.debug(TAG, "init ams");
        Request<AmsAdpInitResponse> request = AmsAdpClient.open()
                .initAms(model.getRequest());
        Response<AmsAdpInitResponse> response = request.send();
        LogUtil.debug(TAG, response);
        return response.body;
    }

    public static AmsAdpSaveCertificateResponse saveCertificate(Certificate certificate) {
        LogUtil.debug(TAG, "save certificate");
        Request<AmsAdpSaveCertificateResponse> request = AmsAdpClient.open()
                .saveCertificate(new AmsAdpBaseRequest(certificate.toJson()));
        Response<AmsAdpSaveCertificateResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsAdpBaseResponse attachEnrollToken(Long cerId, String enrollToken) {
        LogUtil.debug(TAG, "save certificate");
        AttachEnrollTokenRequest modelRequest = new AttachEnrollTokenRequest(cerId, enrollToken);
        LogUtil.debug(TAG, modelRequest);
        Request<AmsAdpBaseResponse> request = AmsAdpClient.open()
                .attachEnrollToken(new AmsAdpBaseRequest(modelRequest));
        Response<AmsAdpBaseResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsAdpBaseResponse changeCertStatus(Long cerId, AmsContract ams, boolean resetToken) {
        LogUtil.debug(TAG, "change certificate status");
        ChangeCertificateStatusRequest model = new ChangeCertificateStatusRequest(cerId, ams, resetToken);
        Request<AmsAdpBaseResponse> request = AmsAdpClient.open()
                .changeCertificateStatus(new AmsAdpBaseRequest(model));
        Response<AmsAdpBaseResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

    public static AmsAdpBaseResponse deleteCertificate(Long cerId, int version) {
        LogUtil.debug(TAG, "change certificate status");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cer_id", cerId);
        jsonObject.addProperty("version", version);
        Request<AmsAdpBaseResponse> request = AmsAdpClient.open()
                .deleteCertificate(new AmsAdpBaseRequest(jsonObject));
        Response<AmsAdpBaseResponse> response = request.send();
        LogUtil.debug(TAG, response.body);
        return response.body;
    }

}
