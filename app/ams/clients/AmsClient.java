package ams.clients;

import ams.contracts.AmsConnectionContract;
import ams.utils.KeyStoreUtil;
import ams.utils.restclient.RestClient;
import play.Play;
import utils.LogUtil;

/**
 * @author HanusaCloud on 5/7/2018
 */
public class AmsClient {

    public static final String TAG = "AmsClient";
    private static AmsConnectionContract amsConnectionContract;
    private static RestClient restClient;
    private static String AMS_URL;
    public static String AMS_OID;

    public static void generate() {
        if(KeyStoreUtil.httpClientConfig == null)
            return;
        AMS_URL = Play.configuration.getProperty("ams_url");
        AMS_OID = Play.configuration.getProperty("ams_oid", "2.16.360.1.2.1.70.106");
        restClient = new RestClient(AMS_URL, null, KeyStoreUtil.httpClientConfig);
    }

    public static AmsConnectionContract open() {
        if(amsConnectionContract == null) {
            amsConnectionContract = restClient.create(AmsConnectionContract.class);
        }
        return amsConnectionContract;
    }

    // check if service online
    public static boolean isOnline() {
        try {
            return restClient.ping();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return false;
        }
    }

    public static void clean() {
        LogUtil.debug(TAG, "clean resources");
        amsConnectionContract = null;
        if (restClient != null) {
            LogUtil.debug(TAG, "clean rest client");
            restClient.clean();
        }
    }

}
