package ams.clients;

import ams.contracts.AmsAdpConnectionContract;
import ams.utils.restclient.RestClient;
import ams.utils.restclient.client.EncryptionContract;
import models.common.ConfigurationDao;
import models.sso.common.adp.util.DceSecurityV2;
import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.Dsl;
import utils.LogUtil;

/**
 * @author HanusaCloud on 5/7/2018
 */
public class AmsAdpClient {

    public static final String TAG = "AmsAdpClient";

    public static final String STATUS_WAITING_FOR_ID = "WAITING_FOR_ID";
    public static final String WAITING_FOR_PHOTO_ID = "WAITING_FOR_PHOTO_ID";
    public static final String STATUS_WAITING_VERIFICATION = "WAITING_FOR_VERIFICATION";
    public static final String STATUS_ENROLLMENT_PROCESSED = "ENROLLMENT_PROCESSED";
    public static final String STATUS_WAITING_CERTIFICATE = "WAITING_FOR_CERTIFICATE";

    public static final String STATUS_VERIFIED = "VERIFIED";
    public static final String STATUS_VERIFIED_USER = "VERIFIED_USER";
    private static final AsyncHttpClientConfig config = Dsl.config().setMaxConnectionsPerHost(5).setMaxConnections(50).build();
    private static final RestClient restClient  = new RestClient(ConfigurationDao.getRestCentralService(), new Encryption(), config);

    private static AmsAdpConnectionContract amsAdpConnectionContract;

    private static class Encryption implements EncryptionContract {

        @Override
        public String enc(String content) {
            return DceSecurityV2.encrypt(content);
        }

        @Override
        public String dec(String content) {
            return DceSecurityV2.decrypt(content);
        }

    }

    public static AmsAdpConnectionContract open() {
        if(amsAdpConnectionContract == null)
            amsAdpConnectionContract = restClient.create(AmsAdpConnectionContract.class);
        return amsAdpConnectionContract;
    }

    public static void clean() {
        LogUtil.debug(TAG, "clean resources");
        if (restClient != null) {
            LogUtil.debug(TAG, "clean rest client");
            restClient.clean();
        }
    }

}
