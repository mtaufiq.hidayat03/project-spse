package ams.service;

import ams.contracts.ActiveUserAms;
import ams.contracts.AmsUserContract;
import ams.models.Ams;
import ams.models.RenewalRequest;
import ams.models.ServiceResult;
import ams.models.form.FormRegisterAms;
import ams.models.request.adp.AmsAdpInputRequest;
import ams.models.response.adp.AmsAdpBaseResponse;
import ams.models.response.adp.AmsAdpInitResponse;
import ams.models.response.adp.AmsAdpSaveCertificateResponse;
import ams.models.response.ams.*;
import ams.repositories.AmsAdpRepository;
import ams.repositories.AmsRepository;
import com.google.gson.JsonObject;
import controllers.BasicCtr;
import models.agency.Pegawai;
import models.jcommon.util.CommonUtil;
import models.osd.Certificate;
import models.osd.ErrorCodeOSD;
import models.osd.JenisCertificate;
import models.rekanan.Rekanan;
import models.secman.Group;
import play.i18n.Messages;
import play.mvc.Scope;
import utils.LogUtil;
import utils.osd.CertificateUtil;
import utils.osd.OSDUtil;

import static ams.clients.AmsAdpClient.*;
import static models.osd.ErrorCodeOSD.DATA_NOT_FOUND;
import static models.osd.ErrorCodeOSD.SUCCEEDED;
import static models.osd.JenisCertificate.*;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsService {

    public static final String TAG = "AmsService";

    public static ServiceResult register(ActiveUserAms user, FormRegisterAms form) {
        LogUtil.debug(TAG, "register user");
        AmsRegisterResponse registerResponse = AmsRepository.registerUser(form);
        if (registerResponse == null) {
            LogUtil.debug(TAG, "register user failed");
            return new ServiceResult(Messages.get("ams.register-failed"));
        }
        if (!registerResponse.success && registerResponse.isRegistered()) {
            LogUtil.debug(TAG, "user already been registered");
            initAms(user, registerResponse.id, STATUS_VERIFIED_USER);
            setAmsIdToUser(user);
            user.setAmsRelatedData();
            return new ServiceResult(true, registerResponse.message);
        }
        if (!registerResponse.success) {
            LogUtil.debug(TAG, "unknown error");
            return new ServiceResult(registerResponse.message);
        }

        initAms(user, registerResponse.id, STATUS_WAITING_FOR_ID);

        AmsAdpInitResponse initResponse = saveStatus(user);
        if (initResponse == null) {
            LogUtil.debug(TAG, "send to adp failed");
            user.save();
            return new ServiceResult(Messages.get("ams.adp-connection-error"));
        }

        //user.ams.cerId = initResponse.cerId;

        setAmsIdToUser(user);

        ServiceResult serviceResult = uploadIdCard(user, form);
        if (!serviceResult.isSuccess()) {
            user.save();
            return serviceResult;
        }
        user.save();
        return new ServiceResult(true, Messages.get("ams.register-success"));
    }

    private static void initAms(ActiveUserAms user, String amsId, String status) {
        if (user.getAms() == null) {
            user.setAms(new Ams());
        }
        LogUtil.debug(TAG, "set ams id and status");
        user.getAms().setId(amsId);
        user.getAms().setAmsStatus(status);
    }

    private  static void setAmsIdToUser(ActiveUserAms user) {
        LogUtil.debug(TAG, "set ams_id to employee or partner");
        LogUtil.debug(TAG, user.getAms());
        user.setAmsIdThroughAmsModel();
    }

    public static ServiceResult uploadIdCard(ActiveUserAms user, FormRegisterAms form) {
        LogUtil.debug(TAG, "upload id card to AMS");
        AmsBaseResponse response = AmsRepository.uploadIdentityImage(user.getAms(), form.file);
        if (response == null) {
            LogUtil.debug(TAG, "upload image failed");
            user.getAms().setAmsStatus(WAITING_FOR_PHOTO_ID);
            saveStatus(user);
            return new ServiceResult(Messages.get("ams.upload-photo-failed"));
        }
        if (!response.success) {
            return new ServiceResult(response.message);
        }
        user.getAms().setAmsStatus(STATUS_VERIFIED_USER);
        saveStatus(user);
        return new ServiceResult(true, Messages.get("ams.upload-photo-success"));
    }

    private static AmsAdpInitResponse saveStatus(ActiveUserAms user) {
        LogUtil.debug(TAG, "send ams data to adp");
        user.setCertificationType();
        return AmsAdpRepository.initAms(new AmsAdpInputRequest(user));
    }

    public static ServiceResult<String> submitEnrollmentDocument(ActiveUserAms user, FormRegisterAms form, boolean withCsr) {
        AmsEnrollTokenResponse response = AmsRepository.enrollment(user.getAms(), form.file);
        if (response == null) {
            return new ServiceResult<>(Messages.get("ams.enrolldocument-failed"));
        }
        if (!response.success) {
            return new ServiceResult<>(response.message);
        }
        user.getAms().setAmsEnrollToken(response.token);
        if ((user.getAms().getCerId() == null || user.getAms().getCerId() == 0)
                && (user.getCerId() != null && user.getCerId() != 0)) {
            user.getAms().setCerId(user.getCerId());
        }
        user.getAms().setAmsStatus(STATUS_VERIFIED);
        user.save();
        if (!withCsr) {
            saveStatus(user);
        }
        attachEnrollToken(user, response.token);
        return new ServiceResult<>(
                true,
                Messages.get("ams.enrolldocument-sent"),
                response.token);
    }

    public static ServiceResult sendEnrollAndCsr(ActiveUserAms user, FormRegisterAms model) {
        if (user.getAms().isAmsEnrollTokenEmpty()) {
            LogUtil.debug(TAG, "enrollment empty, send document...");
            ServiceResult<String> enrollToken = submitEnrollmentDocument(user, model, true);
            if (!enrollToken.isSuccess()) {
                return enrollToken;
            }
        }
        return sendCsr(user);
    }

    public static ServiceResult sendCsr(ActiveUserAms user) {
        LogUtil.debug(TAG, "send csr");
        Long cerId = user.getCerId();
        if (cerId == null || cerId == 0) {
            return new ServiceResult(Messages.get("ams.user-has-no-certificate"));
        }
        Certificate certificate = getLastCertificate(cerId);
        LogUtil.debug(TAG, certificate);
        if (certificate == null || CommonUtil.isEmpty(certificate.csr_pem)) {
            return new ServiceResult(Messages.get("ams.user-has-nocsr"));
        }

        AmsBaseResponse response = AmsRepository.sendCsr(user.getAms(), certificate.getBase64CSRPEM());
        if (response == null) {
            return new ServiceResult(Messages.get("ams.send-csr-failed"));
        }
        if (!response.success) {
            return new ServiceResult(response.message);
        }

        user.getAms().setAmsStatus(STATUS_WAITING_CERTIFICATE);
        saveStatus(user);
        return new ServiceResult(true, Messages.get("ams.send-csr-success"));
    }

    public static ServiceResult revokeCertificate(ActiveUserAms user, String currentSerial) {
        LogUtil.debug(TAG, "revoker certificate");
        Long cerId = user.getCerId();
        if (cerId == null || cerId == 0) {
            LogUtil.debug(TAG, "cert not found");
            return new ServiceResult(Messages.get("ams.certificate-not-found"));
        }
        Certificate certificate = getLastCertificate(cerId);
        if (certificate == null || CommonUtil.isEmpty(certificate.cer_sn)) {
            LogUtil.debug(TAG, "cert not found or cer_sn empty");
            return new ServiceResult(Messages.get("ams.certificate-not-found"));
        }
        if (!certificate.isActive()) {
            LogUtil.debug(TAG, "cert not active");
            return new ServiceResult(Messages.get("ams.no-active-certificate-found"));
        }
        if (!currentSerial.equals(certificate.cer_sn)) {
            LogUtil.debug(TAG, "miss match serial");
            return new ServiceResult(Messages.get("ams.miss-match-serial"));
        }
        AmsRevokeResponse response = AmsRepository.revoke(user.getAms(), certificate.cer_sn);
        if (response == null || !response.success) {
            LogUtil.debug(TAG, "response ams revoke empty");
            return new ServiceResult(Messages.get("ams.revoke-failed"));
        }
        final ErrorCodeOSD errorCodeOSD = OSDUtil.revokeCertificate(
                certificate,
                user.isRekanan() ? user.getRekananId() : user.getPegawaiId(),
                user.isRekanan(),
                BasicCtr.newDate(),
                user.getUserId()
        );
        LogUtil.debug(TAG, errorCodeOSD);
        if (errorCodeOSD == null || errorCodeOSD == ErrorCodeOSD.SUCCEEDED) {
            user.requestRevoked();
            user.save();
        }
        return new ServiceResult(true, response.message);
    }

    public static ServiceResult<JsonObject> downloadCertificate(ActiveUserAms user) {
        AmsCertificateResponse response = AmsRepository.downloadCertificate(user.getAms());
        if (response == null) {
            return new ServiceResult<>(Messages.get("ams.download-certificate-failed"));
        }
        if (!response.success) {
            LogUtil.debug(TAG, "download response failed");
            if (response.isRejected()) {
                LogUtil.debug(TAG, "certificate is rejected by admin.");
                rejectCertificate(user);
            }
            return new ServiceResult<>(response.message);
        }
        return setCSRStatus(user, response);
    }

    private static void rejectCertificate(ActiveUserAms user) {
        Certificate certificate = getLastCertificate(user.getCerId());
        if (certificate != null) {
            certificate.forceSetRejected(user);
            user.getAms().setCerType(certificate.cer_jenis);
            AmsAdpRepository.saveCertificate(certificate);
        }
    }

    /**
     * modified version of @{link controllers.osd.OSDServiceCtr#setCSRStatus()}.*/
    public static ServiceResult<JsonObject> setCSRStatus(ActiveUserAms user, AmsCertificateResponse response) {
        LogUtil.debug(TAG, "set csr status");
        if (!user.isPanitia() && !user.isRekanan()) {
            return new ServiceResult<>("User unauthorized!");
        }
        JsonObject result = new JsonObject();
        Certificate certificate;
        ServiceResult<Certificate> serviceResult;
        if (user.isRekanan()) {
            serviceResult = setPartner(user, response);
        } else {
            serviceResult = setEmployee(user, response);
        }
        if (!serviceResult.isSuccess()) {
            return new ServiceResult<>(serviceResult.getMessage());
        }

        certificate = serviceResult.getPayload();
        certificate.setEnrollToken(user.getAms().getAmsEnrollmentToken());

        AmsAdpSaveCertificateResponse resultAdp = AmsAdpRepository.saveCertificate(certificate);
        if (resultAdp == null || resultAdp.cerId == null || resultAdp.cerId == 0) {
            return new ServiceResult<>(ErrorCodeOSD.FAILED.getCode());
        }
        user.getAms().afterDownload(certificate.cer_id);
        user.save();
        saveStatus(user);
        result.addProperty("cer_pem", response.getCleanCertificate());
        result.addProperty("success", SUCCEEDED.getCode());
        result.addProperty("message", "download success");
        return new ServiceResult<>(true, "success", result);
    }

    private static ServiceResult<Certificate> setEmployee(ActiveUserAms user, AmsCertificateResponse response) {
        AmsUserContract pegawai = Pegawai.findByPegId(user.getPegawaiId());
        if (pegawai == null || pegawai.getCerId() == null) {
            return new ServiceResult<>(DATA_NOT_FOUND.getCode());
        }
        Certificate certificate = getLastCertificate(pegawai.getCerId());
        LogUtil.debug(TAG, certificate);
        if (certificate == null) {
            LogUtil.info(TAG, "Update CSRR Status failed, " +
                    "no CSRR OR CSR-Renew for panitia with ID : " + pegawai.getUserId());
            return new ServiceResult<>(DATA_NOT_FOUND.getCode());
        }
        if (!(certificate.cer_jenis.equals(CSRR_PANITIA)
                || certificate.cer_jenis.equals(CSRR_PANITIA_RENEW))) {
            LogUtil.info(TAG, "Update CSRR Status failed, " +
                    "no CSRR OR CSR-Renew for panitia with ID : " + pegawai.getUserId());
            return new ServiceResult<>(DATA_NOT_FOUND.getCode());
        }
        certificate.cer_jenis = JenisCertificate.CERT_PANITIA;
        certificate.cer_pem = response.getCleanCertificate();
        if (!certificate.setDnFromAmsCertificate(response)) {
            LogUtil.info(TAG, "Failed to set subject dn");
            return new ServiceResult<>("Gagal simpan data subject!");
        }
        return new ServiceResult<>(true, "success", certificate);
    }

    private static ServiceResult<Certificate> setPartner(ActiveUserAms user, AmsCertificateResponse response) {
        AmsUserContract rekanan = Rekanan.findById(user.getRekananId());
        if (rekanan == null || rekanan.getCerId() == null) {
            LogUtil.debug(TAG, "rekanan null or has no cerId");
            return new ServiceResult<>(DATA_NOT_FOUND.getCode());
        }
        Certificate certificate = getLastCertificate(rekanan.getCerId());
        LogUtil.debug(TAG, certificate);
        if (certificate == null) {
            LogUtil.info(TAG, "Update CSRR Status failed, " +
                    "no CSRR OR CSR-Renew for rekanan with ID : " + rekanan.getUserId());
            return new ServiceResult<>(DATA_NOT_FOUND.getCode());
        }
        if (!(certificate.cer_jenis.equals(CSRR_PENYEDIA)
                || certificate.cer_jenis.equals(CSRR_PENYEDIA_RENEW))) {
            LogUtil.info(TAG, "Update CSRR Status failed, " +
                    "no CSRR OR CSR-Renew for rekanan with ID : " + rekanan.getUserId());
            return new ServiceResult<>(DATA_NOT_FOUND.getCode());
        }
        certificate.cer_jenis = JenisCertificate.CERT_PENYEDIA;
        certificate.cer_pem = response.getCleanCertificate();
        if (!certificate.setDnFromAmsCertificate(response)) {
            LogUtil.info(TAG, "Failed to set subject dn");
            return new ServiceResult<>("Gagal simpan data Subject!");
        }
        return new ServiceResult<>(true, "success", certificate);
    }

    public static ServiceResult<String> renewCertificate(ActiveUserAms user, Scope.Params params) {
        LogUtil.debug(TAG, "renew certificate");
        ServiceResult<RenewalRequest> resultSetCsrRenew = renewCertificateToAdp(user, params);
        if (!resultSetCsrRenew.isSuccess()) {
            return new ServiceResult<>(resultSetCsrRenew.getPayload().response);
        }
        RenewalRequest payload = resultSetCsrRenew.getPayload();
        AmsRenewalResponse renewalResponse = AmsRepository.renewal(user.getAms().getId(), payload.oldSerial, payload.getBase64Csr());
        if (renewalResponse == null || !renewalResponse.success) {
            LogUtil.debug(TAG, "Something wrong.. rollback to the previous certificate");
            Certificate certificate = getLastCertificate(user.getCerId());
            if (certificate != null) {
                AmsAdpRepository.deleteCertificate(certificate.cer_id, certificate.cer_versi);
            }
            return new ServiceResult<>(OSDUtil.getReturn(ErrorCodeOSD.PARAMS_ERROR.getCode(), payload.key));
        }
        user.setRenewStatus(renewalResponse.token);
        attachEnrollToken(user, renewalResponse.token);
        saveStatus(user);
        return new ServiceResult<>(
                true,
                Messages.get("ams.certificate-renewal-in-progress"),
                payload.response);
    }

    private static ServiceResult<RenewalRequest> renewCertificateToAdp(ActiveUserAms user, Scope.Params params) {
        LogUtil.debug(TAG, "send new cert to adp");
        String random = params.get("random");
        byte[] key = CertificateUtil.RSADecryption(random);
        LogUtil.debug(TAG, params.get("params"));
        JsonObject jsonParam = OSDUtil.getParams(params.get("params"), key);
        LogUtil.debug(TAG, jsonParam);
        LogUtil.debug(TAG, random);
        RenewalRequest request = new RenewalRequest(jsonParam, key);
        if (jsonParam == null) {
            LogUtil.debug(TAG, "Error " + ErrorCodeOSD.PARAMS_ERROR.getLabel());
            request.response = OSDUtil.getReturn(ErrorCodeOSD.PARAMS_ERROR.getCode(), key);
            return new ServiceResult<>(false, "error", request);
        }
        Group group = user.getGroup();
        String respCode;
        if (group.isRekanan()) {
            Rekanan rekanan = Rekanan.findById(user.getRekananId());
            if (rekanan == null) {
                return new ServiceResult<>(Messages.get("ams.partner-not-found"));
            }
            respCode = OSDUtil.renewCSRRekanan(rekanan, request.oldSerial, request.subjectDN, request.newCsr, BasicCtr.newDate());
            if (respCode.equals(SUCCEEDED.getCode())) {
                try {
                    rekanan.saveToADPCentral();
                } catch (Exception e) {
                    LogUtil.error("cert id belum terupdate ke adp", e);
                } finally {
                    rekanan.save();
                }
            }
        } else {
            Pegawai pegawai = Pegawai.findBy(user.getUserId());
            if (pegawai == null) {
                return new ServiceResult<>(Messages.get("ams.employee-not-found"));
            }
            respCode = OSDUtil.renewCSRPegawai(pegawai, group, request.oldSerial, request.subjectDN, request.newCsr, BasicCtr.newDate());
        }
        request.responseCode = respCode;
        request.response = OSDUtil.getReturn(respCode, key);
        LogUtil.debug(TAG, request);
        final boolean isSucceed = respCode.equals(SUCCEEDED.getCode());
        return new ServiceResult<>(isSucceed, isSucceed ? "success" : "renew certificate failed", request);
    }

    private static void attachEnrollToken(ActiveUserAms user, String token) {
        AmsAdpBaseResponse attachResponse = AmsAdpRepository.attachEnrollToken(
                user.getAms().getCerId(),
                token);
        if (attachResponse == null || !attachResponse.isSuccess()) {
            LogUtil.debug(TAG, "attached enroll token failed");
        }
    }

    public static ServiceResult<String> sendRequestCertificate(ActiveUserAms user, Scope.Params params) {
        LogUtil.debug(TAG,"setCSRR");
        String random = params.get("random");
        String p = params.get("params");
        byte[] key = CertificateUtil.RSADecryption(random);
        JsonObject jsonParam = OSDUtil.getParams(p, key);
        String respCode = "";
        if (jsonParam == null) {
            LogUtil.info(TAG, ErrorCodeOSD.PARAMS_ERROR.getLabel());
            return new ServiceResult<>("Missing params");
        }
        String subjectDN = jsonParam.get("subjectDN").getAsString();
        String eeCSRR = jsonParam.get("csr").getAsString();
        String response = "";
        if (user.isRekanan()) {
            Rekanan rekanan = Rekanan.findById(user.getRekananId());
            respCode = OSDUtil.updateCSRRRekanan(rekanan, subjectDN, eeCSRR, BasicCtr.newDate());
            if (respCode.equals(ErrorCodeOSD.SUCCEEDED.getCode())) {
                try {
                    rekanan.saveToADPCentral();
                } catch (Exception e) {
                    LogUtil.error("cert id belum terupdate ke adp", e);
                } finally {
                    rekanan.save();
                }
            }
            response = OSDUtil.getReturn(respCode, key);
        } else {
            Pegawai pegawai = Pegawai.findBy(user.getUserId());
            response = OSDUtil.getReturn(
                    OSDUtil.updateCSRRPegawai(pegawai, user.getGroup(), subjectDN, eeCSRR, BasicCtr.newDate()), key);
        }
        user.setCsrExists();
        return new ServiceResult<>(true, "Berhasil kirim CSR", response);
    }

    private static Certificate getLastCertificate(Long cerId) {
        LogUtil.debug(TAG, "get latest certificate from adp");
        return OSDUtil.getLastCertificateFromInaproc(cerId);
    }

}
