package ams.models;

import ams.utils.Base64Util;
import com.google.gson.JsonObject;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 11/30/2018
 */
public class RenewalRequest {

    public String oldSerial;
    public String newCsr;
    public String subjectDN;
    public byte[] key;

    public String responseCode;
    public String response;

    public RenewalRequest() {}

    public RenewalRequest(JsonObject jsonParam, byte[] key) {
        if (jsonParam != null) {
            this.subjectDN = jsonParam.get("subjectDN").getAsString();
            this.newCsr = jsonParam.get("csr").getAsString();
            this.oldSerial = jsonParam.get("existingSN").getAsString();
            if (!CommonUtil.isEmpty(this.oldSerial)) {
                this.oldSerial = this.oldSerial.toUpperCase();
            }
        }
        this.key = key;
    }

    public String getBase64Csr() {
        return Base64Util.encode(newCsr.getBytes());
    }

}
