package ams.models;

import ams.utils.restclient.annotations.Key;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAuth {

    @Key("client_id")
    public String clientId;
    @Key("client_secret")
    public String clientSecret;
    @Key("grant_type")
    public String grantType;

    public AmsAuth(String clientId, String clientSecret, String grantType) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.grantType = grantType;
    }

}
