package ams.models.request.adp;

import ams.contracts.ActiveUserAms;
import ams.contracts.AmsContract;
import com.google.gson.annotations.SerializedName;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAdpInputRequest {

    @SerializedName("ams_id")
    public String amsId;
    @SerializedName("ams_status")
    public String amsStatus;
    @SerializedName("repo_id")
    public int repoId;
    @SerializedName("cer_id")
    public int cerId;
    @SerializedName("ams_enrollment_token")
    public String enrollmentToken;
    @SerializedName("certification_type")
    public String certificationType;
    @SerializedName("user_id")
    public Long userId;
    @SerializedName("user_type")
    public String userType;
    @SerializedName("unique_id")
    public String uniqueId;

    public AmsAdpInputRequest(ActiveUserAms activeUser) {
        AmsContract amsData = activeUser.getAms();
        if (activeUser.isRekanan()) {
            this.userId = activeUser.getRekananId();
            this.userType = "REKANAN";
        } else if (activeUser.isPanitia()) {
            this.userId = activeUser.getPegawaiId();
            this.userType = "PANITIA";
        }
        this.amsId = amsData.getId();
        this.amsStatus = amsData.getAmsStatus();
        this.repoId = ConfigurationDao.getRepoId();
        if (amsData.getCerId() != null) {
            this.cerId = amsData.getCerId().intValue();
        }
        if (!CommonUtil.isEmpty(amsData.getAmsEnrollmentToken())) {
            this.enrollmentToken = amsData.getAmsEnrollmentToken();
        }
        this.certificationType = amsData.getCertificationType();
        this.uniqueId = activeUser.getUniqueId();
    }

    public String getJson() {
        return CommonUtil.toJson(this);
    }

    public AmsAdpBaseRequest getRequest() {
        return new AmsAdpBaseRequest(getJson());
    }

}
