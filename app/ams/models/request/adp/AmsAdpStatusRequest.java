package ams.models.request.adp;

import ams.contracts.ActiveUserAms;
import ams.contracts.AmsUserContract;
import com.google.gson.annotations.SerializedName;
import models.common.ConfigurationDao;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAdpStatusRequest {

    public static final String REKANAN = "REKANAN";
    public static final String PANITIA = "PANITIA";

    @SerializedName("cer_id")
    public Long cerId;
    @SerializedName("repo_id")
    public Long repoId;
    @SerializedName("ams_id")
    public String amsId;
    @SerializedName("user_id")
    public Long userId;
    @SerializedName("user_type")
    public String userType;
    @SerializedName("unique_id")
    public String uniqueId; // it can be rkn_npwp partner or peg_nip for employee

    public AmsAdpStatusRequest() {
        this.repoId = (long) ConfigurationDao.getRepoId();
    }

    public AmsAdpStatusRequest(ActiveUserAms user) {
        this.amsId = user.getAms().getId();
        this.repoId = (long) ConfigurationDao.getRepoId();
        if (user.isRekanan()) {
            this.userId = user.getRekananId();
            this.userType = REKANAN;
        } else if (user.isPanitia()) {
            this.userId = user.getPegawaiId();
            this.userType = PANITIA;
        }
        this.uniqueId = user.getUniqueId();
        if (user.getAms().getCerId() != null) {
            this.cerId = user.getAms().getCerId();
        }
    }

    public void setFromUser(AmsUserContract user, String type) {
        this.amsId = user.getAmsId();
        this.userId = user.getUserId();
        this.userType = type;
        this.cerId = user.getCerId();
        this.uniqueId = user.getUniqueId();
    }

    public void setFromRekanan(AmsUserContract rekanan) {
        this.amsId = rekanan.getAmsId();
        this.userId = rekanan.getUserId();
        this.userType = REKANAN;
        this.cerId = rekanan.getCerId();
    }

    public void setFromPegawai(AmsUserContract pegawai) {
        this.amsId = pegawai.getAmsId();
        this.userId = pegawai.getUserId();
        this.userType = PANITIA;
        this.cerId = pegawai.getCerId();
    }

    public String getJson() {
        return CommonUtil.toJson(this);
    }

    public AmsAdpBaseRequest getRequest() {
        return new AmsAdpBaseRequest(getJson());
    }

}
