package ams.models.request.adp;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 8/15/2018
 */
public class AttachEnrollTokenRequest {

    @SerializedName("cer_id")
    public Long cerId;
    @SerializedName("enroll_token")
    public String enrollToken;

    public AttachEnrollTokenRequest(Long cerId, String enrollToken) {
        this.cerId = cerId;
        this.enrollToken = enrollToken;
    }

}
