package ams.models.request.adp;

import ams.clients.AmsAdpClient;
import ams.models.response.ams.AmsCertificateResponse;
import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;

import java.security.cert.X509Certificate;

/**
 * @author HanusaCloud on 5/14/2018
 */
public class AmsAdpCertificateRequest {

    @SerializedName("ams_id")
    public String amsId;
    @SerializedName("serial_number")
    public String serialNumber;
    @SerializedName("certificate")
    public String certificate; //better be in base64 encoded form
    @SerializedName("subject_dn")
    public String subjectDn;
    @SerializedName("ams_status")
    public String amsStatus = AmsAdpClient.STATUS_VERIFIED;
    @SerializedName("cer_version")
    public Integer cerVersion;
    @SerializedName("issuer_dn")
    public String issuerDn;

    public AmsAdpCertificateRequest(String amsId, AmsCertificateResponse response) {
        this.amsId = amsId;
        this.serialNumber = response.serialNumber;
        this.certificate = response.getBase64Certificate();
        final X509Certificate certificate = response.getAsCertificate();
        if (certificate != null) {
            this.subjectDn = certificate.getSubjectDN().getName();
            this.issuerDn = certificate.getIssuerDN().getName();
            this.cerVersion = certificate.getVersion();
        }
    }

    public String toJson() {
        return CommonUtil.toJson(this);
    }

}
