package ams.models.request.adp;

import ams.utils.restclient.annotations.CloakAble;
import com.google.gson.JsonObject;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAdpBaseRequest {

    @CloakAble("q")
    public String body;

    public AmsAdpBaseRequest(String body) {
        this.body = body;
    }

    public AmsAdpBaseRequest(JsonObject body) {
        this.body = body.toString();
    }

    public AmsAdpBaseRequest(Object model) {
        this.body = CommonUtil.toJson(model);
    }

}
