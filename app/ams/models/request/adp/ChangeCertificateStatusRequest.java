package ams.models.request.adp;

import ams.contracts.AmsContract;
import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 8/15/2018
 */
public class ChangeCertificateStatusRequest {

    @SerializedName("cer_id")
    public Long cerId;
    @SerializedName("status")
    public String status;
    @SerializedName("ams_id")
    public String amsId;
    @SerializedName("ams_status")
    public String amsStatus;
    @SerializedName("approval_message")
    public String approvalMessage;
    @SerializedName("reset_token")
    public boolean resetToken;

    public ChangeCertificateStatusRequest() {}

    public ChangeCertificateStatusRequest(Long cerId, AmsContract ams, boolean resetToken) {
        this.cerId = cerId;
        this.status = ams.getCerType();
        this.amsId = ams.getId();
        this.amsStatus = ams.getAmsStatus();
        this.approvalMessage = ams.getApprovalMessage();
        this.resetToken = resetToken;
    }

}
