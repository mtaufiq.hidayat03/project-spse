package ams.models.request.ams;

import ams.models.form.FormRegisterAms;
import ams.repositories.AmsRepository;
import ams.utils.restclient.annotations.Key;
import models.agency.Satuan_kerja;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsRegisterRequest {

    public String nik;
    public String nama;
    public String nip;
    public String email;
    @Key("nomor_telepon")
    public String telp;
    @Key("unit_kerja")
    public String unitKerja;
    public String kota;
    public String provinsi;
    @Key("oid_instansi")
    public String oidInstansi;
    public String produk = AmsRepository.getDigitalSignature();
    @Key("jabatan_organisasi")
    public String jabatan;
    public String npwp;

    public AmsRegisterRequest(FormRegisterAms ams) {
        this.nik = ams.nik;
        this.nama = ams.nama;
        this.nip = ams.nip;
        this.email = ams.email;
        this.telp = ams.nomor_telepon;
        final Satuan_kerja satuan_kerja = ams.getSatuanKerja();
        if (satuan_kerja != null) {
            this.unitKerja = satuan_kerja.stk_nama;
        } else {
            this.unitKerja = ams.unit_kerja;
        }
        this.kota = ams.kota;
        this.provinsi = ams.provinsi;
        this.oidInstansi = ams.getOid();
        this.jabatan = ams.jabatan;
        if (!CommonUtil.isEmpty(ams.npwp)) {
            this.npwp = ams.npwp;
        }
    }

}
