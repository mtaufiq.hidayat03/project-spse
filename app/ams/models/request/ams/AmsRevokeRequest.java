package ams.models.request.ams;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 7/12/2018
 */
public class AmsRevokeRequest {

    @SerializedName("serial")
    public String serial;
    @SerializedName("csr")
    public String csr;

    public AmsRevokeRequest(String serial, String csr) {
        this.serial = serial;
        this.csr = csr;
    }

}
