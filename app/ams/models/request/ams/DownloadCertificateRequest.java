package ams.models.request.ams;

import ams.utils.restclient.annotations.Key;

/**
 * @author HanusaCloud on 8/15/2018
 */
public class DownloadCertificateRequest {

    @Key("token")
    public String token;
    @Key("produk")
    public String produk;
    @Key("format")
    public String format;

    public DownloadCertificateRequest(String token, String produk) {
        this.token = token;
        this.produk = produk;
        this.format = "no-header";
    }

}
