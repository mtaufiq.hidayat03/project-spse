package ams.models.request.ams;

import ams.utils.restclient.annotations.Key;
import ams.utils.restclient.contracts.FieldExtractor;
import ams.utils.restclient.contracts.FormUrlEncode;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsEnrollmentRequest implements FieldExtractor,FormUrlEncode {

    @Key("produk")
    public String product;
    @Key("jenis")
    public String jenis;

    public AmsEnrollmentRequest(String product, String jenis) {
        this.product = product;
        this.jenis = jenis;
    }

    public String getEncoded() {
        return encode(extract(this));
    }

}
