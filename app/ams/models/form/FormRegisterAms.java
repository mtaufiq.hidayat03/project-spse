package ams.models.form;

import ams.clients.AmsClient;
import ams.contracts.ActiveUserAms;
import ams.repositories.AmsRepository;
import models.agency.Pegawai;
import models.agency.Satuan_kerja;
import models.common.CONFIG;
import models.common.ConfigurationDao;
import models.common.Propinsi_kabupaten;
import models.jcommon.config.Configuration;
import models.jcommon.util.CommonUtil;
import models.rekanan.Rekanan;
import models.sso.common.Kabupaten;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import play.Play;
import play.data.validation.Required;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FormRegisterAms {

    private static final List<String> IMAGE_EXTENSIONS;
    private static final List<String> DOCUMENT_EXTENSIONS;

    static {
        IMAGE_EXTENSIONS = Arrays.asList("jpeg", "jpg", "png");
        DOCUMENT_EXTENSIONS = Arrays.asList("doc", "docx", "pdf");
    }

    @Required
    public String nik;
    @Required
    public String nama;
    @Required
    public String nip;
    @Required
    public String email;
    public String nomor_telepon;
    public String unit_kerja;
    public String kota;
    public String provinsi;
    public String oid_instansi = AmsClient.AMS_OID;
    public String jabatan;
    public String produk = AmsRepository.getDigitalSignature();
    public Long kbpId;
    public String instansi;
    @Required
    public File file;
    public String npwp;

    public FormRegisterAms(ActiveUserAms user){
        if(!user.isPanitia()){
            Rekanan rekanan = Rekanan.findByNamaUser(user.getUserId());
            nama = rekanan.rkn_nama;
            email = rekanan.rkn_email;
            nomor_telepon = rekanan.rkn_telepon;
            Kabupaten kab = rekanan.getKabupaten();
            kota = Propinsi_kabupaten.transformKabupatenName(kab.kbp_nama);
            provinsi = rekanan.getKabupaten().getPropinsi().prp_nama;
            npwp = rekanan.rkn_npwp;
        }else{
            Pegawai pegawai = Pegawai.findById(user.getPegawaiId());
            nama = pegawai.peg_nama;
            nip = pegawai.peg_nip;
            email = pegawai.peg_email;
            nomor_telepon = pegawai.peg_telepon;

            if(pegawai.agc_id != null){
                Kabupaten kabupaten = pegawai.getAgency().getKabupaten();
                kbpId = kabupaten.kbp_id;
                kota = Propinsi_kabupaten.transformKabupatenName(kabupaten.kbp_nama);
                provinsi = pegawai.getAgency().getKabupaten().getPropinsi().prp_nama;
            }else{
                long kbp_id = Configuration.getLong(CONFIG.KABUPATEN.category);
                kbpId = kbp_id;
                Kabupaten kabupaten = Kabupaten.findById(kbp_id);
                kota = Propinsi_kabupaten.transformKabupatenName(kabupaten.kbp_nama);
                provinsi = kabupaten.getPropinsi().prp_nama;
            }

        }
    }

    /**
     * It must be sent with repo id at the end if it's production*/
    public String getOid() {
       if (ConfigurationDao.isLatihan() || Play.mode.isDev()) {
            return AmsClient.AMS_OID;
        }
        return AmsClient.AMS_OID + '.' + ConfigurationDao.getRepoId();
    }

    public String getUnitName() {
        return CommonUtil.isEmpty(unit_kerja) ? nama : unit_kerja;
    }

    public Satuan_kerja getSatuanKerja() {
        if (unit_kerja != null && StringUtils.isNumeric(unit_kerja)) {
            return Satuan_kerja.find("stk_id =?", Long.valueOf(unit_kerja)).first();
        }
        return null;
    }

    public boolean isValidFile() {
        return file != null && file.length() <= 1000000;
    }

    public boolean isValidPicture() {
        return isValidExtension(IMAGE_EXTENSIONS);
    }

    public boolean isValidDocument() {
        return isValidExtension(DOCUMENT_EXTENSIONS);
    }

    public String getPhoneNumber() {
        return !CommonUtil.isEmpty(nomor_telepon) ? nomor_telepon : "-";
    }

    public boolean isValidExtension(List<String> extensions) {
        final String extension = FilenameUtils.getExtension(file.getAbsolutePath());
        return extensions.stream()
                .anyMatch(e -> e.equals(extension));
    }

    public void rebind(FormRegisterAms model) {
        this.email = model.email;
        this.nama = model.nama;
        this.nomor_telepon = model.getPhoneNumber();
        this.kota = model.kota;
        this.provinsi = model.provinsi;
        this.nip = model.nip;
        if (!CommonUtil.isEmpty(model.npwp)) {
            this.npwp = model.npwp;
        }
    }

}
