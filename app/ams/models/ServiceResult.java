package ams.models;

import models.jcommon.util.CommonUtil;
import play.i18n.Messages;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class ServiceResult<T> {

    private final boolean status;
    private final String message;
    private final T payload;

    public ServiceResult(boolean status, String message) {
        this.status = status;
        this.message = message;
        this.payload = null;
    }

    public ServiceResult(String message) {
        this.status = false;
        this.message = message;
        this.payload = null;
    }

    public ServiceResult(boolean status, String message, T payload) {
        this.status = status;
        this.message = message;
        this.payload = payload;
    }

    public T getPayload() {
        return payload;
    }

    public String getMessage() {
        if (CommonUtil.isEmpty(message)) {
            return Messages.get("ams.oops");
    }
        return message;
    }

    public boolean isSuccess() {
        return status;
    }

    public String toJson() {
        return "{\"status\":"+status+",\"message\":\"" + message + "\"}";
    }

}
