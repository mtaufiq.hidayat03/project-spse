package ams.models;

import ams.contracts.AmsContract;
import ams.models.response.adp.AmsAdpResponse;
import ams.utils.AmsUtil;
import ams.utils.restclient.model.Response;
import models.jcommon.util.CommonUtil;
import models.osd.JenisCertificate;
import utils.LogUtil;

import static ams.clients.AmsAdpClient.*;
import static models.osd.JenisCertificate.*;

/**
 * @author HanusaCloud on 5/13/2018
 */
public class Ams implements AmsContract {

    public static final String TAG = "Ams";

    private Long cerId;
    private String id;
    private String amsStatus;
    private String amsEnrollToken;
    private String certificationType;
    private String origin_repo;
    private String cerType = "";
    private boolean isCertificateExpired = false;
    private boolean isRevokeFromX509 = false;
    private String ocspStatus;
    private Long daysLeft;
    private boolean csrExists = false;
    private boolean cerExists = false;

    private String message;
    private String approvalMessage;

    private boolean exceptionExist = false;

    public Ams() {}

    public Ams(AmsAdpResponse response) {
        if (this.cerId == null && response.isCerIdExist()) {
            this.cerId = response.certificate.cer_id;
        }
        this.id = response.id;
        this.amsStatus = response.amsStatus;
        this.amsEnrollToken = response.amsEnrollToken;
        this.origin_repo = response.origin_repo;
        this.approvalMessage = response.approvalMessage;
    }

    public Ams(Response<AmsAdpResponse> response) {
        this.forceSetExceptionExist();
        this.message = response.formattedMessage("ams.network-exception", "ADP");
    }

    @Override
    public Long getCerId() {
        return this.cerId;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getAmsStatus() {
        return this.amsStatus;
    }

    @Override
    public String getAmsEnrollmentToken() {
        return this.amsEnrollToken;
    }

    @Override
    public String getCertificationType() {
        return this.certificationType;
    }

    @Override
    public String getOriginRepo() {
        return this.origin_repo;
    }

    @Override
    public String getCerType() {
        return this.cerType;
    }

    @Override
    public boolean isCertificateExpired() {
        return this.isCertificateExpired;
    }

    @Override
    public boolean isRevokeFromX509() {
        return this.isRevokeFromX509;
    }

    @Override
    public String getOcspStatus() {
        return this.ocspStatus;
    }

    @Override
    public Long getDaysLeft() {
        return this.daysLeft;
    }

    @Override
    public boolean isCsrExists() {
        return this.csrExists;
    }

    @Override
    public boolean isCerExists() {
        return this.cerExists;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getApprovalMessage() {
        return this.approvalMessage;
    }

    @Override
    public boolean exceptionExist() {
        return this.exceptionExist;
    }

    @Override
    public boolean isWaitingForCertificate() {
        return !CommonUtil.isEmpty(amsStatus)
                && STATUS_WAITING_CERTIFICATE.equals(amsStatus);
    }

    @Override
    public boolean isVerifiedCertificate() {
        return id != null && !CommonUtil.isEmpty(amsStatus) && STATUS_VERIFIED.equals(amsStatus);
    }

    @Override
    public boolean isVerifiedUser() {
        return !CommonUtil.isEmpty(amsStatus) && STATUS_VERIFIED_USER.equals(amsStatus);
    }

    @Override
    public boolean isAmsEnrollTokenEmpty() {
        return CommonUtil.isEmpty(amsEnrollToken);
    }

    @Override
    public boolean isEnrollmentSent() {
        return !isAmsEnrollTokenEmpty()
                && isEnrollmentProcessed();
    }

    @Override
    public boolean isEnrollmentProcessed() {
        return !CommonUtil.isEmpty(amsStatus)
                && STATUS_ENROLLMENT_PROCESSED.equals(amsStatus);
    }

    @Override
    public boolean isCertificateRevocationRequested() {
        return !CommonUtil.isEmpty(cerType)
                && (cerType.equalsIgnoreCase(REVOKE_REQ_PANITIA)
                || cerType.equalsIgnoreCase(REVOKE_REQ_PENYEDIA));
    }

    @Override
    public boolean isCerExist() {
        LogUtil.debug(TAG, "cer exists: " + cerExists);
        return cerExists;
    }

    @Override
    public boolean isCertActive() {
        return !CommonUtil.isEmpty(cerType)
                && (cerType.equals(JenisCertificate.CERT_PANITIA)
                || cerType.equals(JenisCertificate.CERT_PENYEDIA));
    }

    @Override
    public boolean isCsrExist() {
        LogUtil.debug(TAG, "csr exists: " + csrExists);
        return csrExists;
    }

    @Override
    public boolean allowToRegister() {
        return id == null;
    }

    @Override
    public boolean isLessThan30Days() {
        return daysLeft != null && daysLeft < 30 && !(isRevoked() || isRevokeFromX509);
    }

    @Override
    public void forceSetCsrExists() {
        LogUtil.debug(TAG, "force set scr exists status to true");
        setCsrExists(true);
    }

    @Override
    public void setNewStatus(boolean isRekanan) {
        forceSetCsrExists();
        this.cerType = isRekanan ? CSRR_PENYEDIA : CSRR_PANITIA;
        this.amsStatus = STATUS_VERIFIED_USER;
        this.amsEnrollToken = null;
        this.isRevokeFromX509 = false;
        this.ocspStatus = "UNKNOWN";
        this.cerExists = false;
    }

    @Override
    public void setReNewStatus(boolean isRekanan, String token) {
        forceSetCsrExists();
        this.cerType = isRekanan ? CSRR_PENYEDIA_RENEW : CSRR_PANITIA_RENEW;
        this.amsStatus = STATUS_WAITING_CERTIFICATE;
        this.amsEnrollToken = token;
        this.isRevokeFromX509 = false;
        this.ocspStatus = "UNKNOWN";
        this.cerExists = false;
    }

    @Override
    public void afterDownload(Long cerId) {
        this.amsStatus = STATUS_VERIFIED;
        this.cerId = cerId;
        this.forceSetCerExists();
        this.message = null;
        this.daysLeft = null;
    }

    @Override
    public void forceSetCerExists() {
        //this.cerExists = true;
        setCerExists(true);
    }

    @Override
    public boolean isRevoked() {
        LogUtil.debug(TAG, "current cerType: " + this.cerType);
        return this.cerType.equals(REVOKED_PANITIA)
                || this.cerType.equals(REVOKED_PENYEDIA);
    }

    @Override
    public boolean isNotAllowToBeUsed() {
        return (isCertificateExpired
                || isRevoked()
                || isRevokeFromX509
                || isCsrRejected())
                && CommonUtil.isEmpty(message);
    }

    @Override
    public boolean isCsrRequested() {
        LogUtil.debug(TAG, "current cerType: " + this.cerType);
        return this.cerType.equals(CSRR_PANITIA)
                || this.cerType.equals(CSRR_PANITIA_RENEW)
                || this.cerType.equals(CSRR_PENYEDIA_RENEW)
                || this.cerType.equals(CSRR_PENYEDIA);
    }

    @Override
    public boolean isCsrRejected() {
        return this.cerType.equals(CSR_PENYEDIA_REJECTED)
                || this.cerType.equals(CSR_PANITIA_REJECTED);
    }

    @Override
    public boolean isWaitingForCert() {
        return isCsrExist()
                && !isCerExist()
                && !isCsrRejected();
    }

    @Override
    public boolean isWaitingForPhoto() {
        return amsStatus.equals(WAITING_FOR_PHOTO_ID);
    }

    @Override
    public String getStatusApprovalMessage() {
        if (isCsrRejected()) {
            return approvalMessage;
        }
        return null;
    }

    @Override
    public void forceSetExceptionExist() {
        this.exceptionExist = true;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void setCertificateExpired(boolean status) {
        this.isCertificateExpired = status;
    }

    @Override
    public void setOcspStatus(String status) {
        this.ocspStatus = status;
    }

    @Override
    public void setAmsStatus(String status) {
        this.amsStatus = status;
    }

    @Override
    public void setCerType(String cerType) {
        this.cerType = cerType;
    }

    @Override
    public void setCsrExists(boolean status) {
        this.csrExists = status;
    }

    @Override
    public void setCerExists(boolean status) {
        this.cerExists = status;
    }

    @Override
    public void setDaysLeft(Long daysLeft) {
        this.daysLeft = daysLeft;
    }

    @Override
    public void setApprovalMessage(String message) {
        this.approvalMessage = message;
    }

    @Override
    public void setRevokeFromX509(boolean status) {
        this.isRevokeFromX509 = status;
    }

    @Override
    public void setId(String amsId) {
        this.id = amsId;
    }

    @Override
    public void setAmsEnrollToken(String token) {
        this.amsEnrollToken = token;
    }

    @Override
    public void setCerId(Long cerId) {
        this.cerId = cerId;
    }

    @Override
    public void setCertificationType(String type) {
        this.certificationType = type;
    }

    @Override
    public boolean isCertificationTypeEmpty() {
        return CommonUtil.isEmpty(certificationType);
    }

    @Override
    public void setFromOcspResult(AmsUtil.OcspResult result) {
        this.message = result.getFormattedMessage();
        this.forceSetExceptionExist();
    }

}
