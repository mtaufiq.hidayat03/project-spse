package ams.models.response.adp;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAdpInitResponse extends AmsAdpBaseResponse {

    @SerializedName("cer_id")
    public Long cerId;

}
