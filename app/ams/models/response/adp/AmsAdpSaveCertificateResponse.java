package ams.models.response.adp;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/15/2018
 */
public class AmsAdpSaveCertificateResponse extends AmsAdpBaseResponse {

    @SerializedName("cer_id")
    public Long cerId;

}
