package ams.models.response.adp;

import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;
import models.osd.Certificate;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAdpResponse extends AmsAdpBaseResponse {

    @SerializedName("ams_id")
    public String id;
    @SerializedName("ams_status")
    public String amsStatus;
    @SerializedName("ams_enroll_token")
    public String amsEnrollToken;
    @SerializedName("origin_repo")
    public String origin_repo;
    @SerializedName("certificate")
    public Certificate certificate;
    @SerializedName("approval_message")
    public String approvalMessage;

    public AmsAdpResponse() {}

    public boolean isAmsIdExist() {
        return !CommonUtil.isEmpty(this.id);
    }

    public boolean isCerIdExist() {
        return certificate != null && certificate.cer_id != null && certificate.cer_id != 0;
    }

    public boolean isSuccess() {
        return "success".equals(status);
    }

}
