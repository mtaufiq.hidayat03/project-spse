package ams.models.response.adp;

import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/9/2018
 */
public class AmsAdpBaseResponse {

    public String status;
    public String message;

    public boolean isSuccess() {
        return !CommonUtil.isEmpty(status) && "success".equals(status);
    }

}
