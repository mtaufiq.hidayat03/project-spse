package ams.models.response.ams;

import ams.models.UserData;
import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/14/2018
 */
public class AmsSubjectResponse extends AmsBaseResponse {

    @SerializedName("user_data")
    public UserData userData;

}
