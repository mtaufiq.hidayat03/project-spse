package ams.models.response.ams;

import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 8/7/2018
 */
public class AmsCheckStatusResponse extends AmsBaseResponse {

    @SerializedName("request_status")
    public String requestStatus;

    public boolean isRejected() {
        return !CommonUtil.isEmpty(requestStatus) && requestStatus.contains("rejected");
    }

}
