package ams.models.response.ams;

import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 8/1/2018
 */
public class AmsRenewalResponse extends AmsBaseResponse {

    @SerializedName("token")
    public String token;

    public boolean isCertificateNotExist() {
        return !CommonUtil.isEmpty(this.message) && this.message.equalsIgnoreCase("Sertifikat tidak ditemukan");
    }
}
