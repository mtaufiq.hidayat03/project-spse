package ams.models.response.ams;

import ams.utils.AmsUtil;
import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;
import org.apache.commons.codec.binary.Base64;
import utils.LogUtil;
import utils.osd.CertificateUtil;

import java.security.cert.X509Certificate;

/**
 * @author HanusaCloud on 5/13/2018
 */
public class AmsCertificateResponse extends AmsBaseResponse {

    public static final String TAG = "AmsCertificateResponse";

    @SerializedName("certificate")
    public String certificate;
    @SerializedName("serial_number")
    public String serialNumber;
    public String rejectedReason;

    public String getCleanCertificate() {
        return AmsUtil.oneLineFormat(certificate);
    }

    public boolean isRejected() {
        return !CommonUtil.isEmpty(message)
                && (message.contains("reject")
                || message.contains("tolak"));
    }

    public String getBase64Certificate() {
        return Base64.encodeBase64String(certificate.getBytes());
    }

    public X509Certificate getAsCertificate() {
        try {
            X509Certificate certificate = (X509Certificate) CertificateUtil.getCertFromPEM(getCleanCertificate());
            LogUtil.debug(TAG, certificate.getIssuerDN().getName());
            LogUtil.debug(TAG, certificate.getSubjectDN().getName());
            LogUtil.debug(TAG, certificate.getSerialNumber());
            LogUtil.debug(TAG, certificate.getVersion());
            return certificate;
        } catch (Exception e) {
            LogUtil.error("AmsCertificateResponse", e);
            return null;
        }
    }

}
