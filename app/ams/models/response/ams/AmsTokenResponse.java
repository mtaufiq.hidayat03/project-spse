package ams.models.response.ams;

import ams.utils.restclient.model.Headers;
import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/7/2018
 */
public class AmsTokenResponse {

    @SerializedName("access_token")
    public String token;
    @SerializedName("token_type")
    public String tokenType;
    public String scope;
    @SerializedName("expires_in")
    public Long expiresIn;

    public boolean isTokenExist() {
        return token != null && !token.isEmpty();
    }

    public String getBearer() {
        return "Bearer " + token;
    }

    public Headers getHeaders() {
        return new Headers.Builder().add("Authorization", getBearer()).build();
    }

}
