package ams.models.response.ams;

import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/7/2018
 */
public class AmsRegisterResponse extends AmsBaseResponse {

    @SerializedName("id")
    public String id;

    public boolean isRegistered() {
        return !CommonUtil.isEmpty(message) && message.contains("Anda telah terdaftar sebelumnya");
    }

}
