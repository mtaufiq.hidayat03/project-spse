package ams.models.response.ams;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/7/2018
 */
public class AmsBaseResponse {

    @SerializedName("success")
    public boolean success;
    @SerializedName("message")
    public String message;

}
