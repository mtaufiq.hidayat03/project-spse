package ams.models.response.ams;

import com.google.gson.annotations.SerializedName;
import models.jcommon.util.CommonUtil;

/**
 * @author HanusaCloud on 5/10/2018
 */
public class AmsStatusResponse extends AmsBaseResponse {

    @SerializedName("id")
    public String id;
    @SerializedName("user_verified")
    public String userVerified;
    @SerializedName("user_registered")
    public String userRegistered;
    @SerializedName("user_certificate")
    public String userCertificate;

    public boolean isUserVerified() {
        return !CommonUtil.isEmpty(userVerified) && userVerified.equals("DONE");
    }

    public boolean isUserRegistered() {
        return !CommonUtil.isEmpty(userRegistered) && userRegistered.equals("DONE");
    }

    public boolean isCertificated() {
        return !CommonUtil.isEmpty(userCertificate) && userCertificate.equals("DONE");
    }

}
