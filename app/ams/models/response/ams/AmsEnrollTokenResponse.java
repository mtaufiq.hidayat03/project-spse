package ams.models.response.ams;

import ams.utils.restclient.model.Headers;
import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/10/2018
 */
public class AmsEnrollTokenResponse extends AmsBaseResponse {

    @SerializedName("token")
    public String token;

    public String getBearer() {
        return "Bearer " + token;
    }

    public Headers getHeaders() {
        return new Headers.Builder().add("Authorization", getBearer()).build();
    }

}
