package ams.models;

/**
 * @author HanusaCloud on 5/11/2018
 */
public class AmsCsrDataForm {

    public String enrollmentToken;
    public Long cerid;
    public String csr;
    public String email;

}
