package ams.models;

import com.google.gson.annotations.SerializedName;

/**
 * @author HanusaCloud on 5/14/2018
 */
public class UserData {

    @SerializedName("ST")
    public String st;
    public String description;
    @SerializedName("L")
    public String l;
    @SerializedName("emailAddress")
    public String email;
    @SerializedName("nip")
    public String nip;
    @SerializedName("OU")
    public String ou;
    @SerializedName("O")
    public String o;
    @SerializedName("SN")
    public String cn;

    public String getAsString() {
        return "ST=" + st +",L=" + l + ",email=" + email +
                ",NIP=" + nip + ",OU=" + ou + ",O=" + o +",CN=" + cn;
    }

}
