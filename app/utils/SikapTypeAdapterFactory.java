package utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapterFactory;
import models.rekanan.Ijin_usaha;
import models.rekanan.Pajak;
import models.rekanan.Pengalaman;
import models.rekanan.Staf_ahli;

/**
 * This class is a factory to create adapters to deserialize response from SIKaP on migration
 * @author idoej
 *
 */
public class SikapTypeAdapterFactory {
	
	public static TypeAdapterFactory createIjinUsahaAdapter() {
		return new CustomizedTypeAdapterFactory<Ijin_usaha>(Ijin_usaha.class) {
			@Override
			protected void afterRead(JsonElement deserialized) {
				// jni_nama
				JsonObject custom = deserialized.getAsJsonObject().get("jenisIjin").getAsJsonObject();
				deserialized.getAsJsonObject().add("jni_nama", custom.get("jni_nama"));
				// klasifikasi
				StringBuilder sb = new StringBuilder();
				JsonElement elm = deserialized.getAsJsonObject().get("listKlasifikasi");
				if(elm != null) {
					JsonArray listKlasifikasi = deserialized.getAsJsonObject().get("listKlasifikasi").getAsJsonArray();
					for(int i=0;i<listKlasifikasi.size();i++) {
						JsonObject klasifikasi = listKlasifikasi.get(i).getAsJsonObject();
						sb.append('|').append(klasifikasi.get("completeLabel").getAsString());
					}
					if(sb.length() > 0) {
						sb.delete(0, 1);
					}
				}
				deserialized.getAsJsonObject().addProperty("ius_klasifikasi", sb.toString());
			}
		};
	}
	
	public static TypeAdapterFactory createStafAhliAdapter() {
		return new CustomizedTypeAdapterFactory<Staf_ahli>(Staf_ahli.class) {
			@Override
			protected void afterRead(JsonElement deserialized) {
				// sta_kewarganegaraan --> sta_kewarganearaan
				JsonObject custom = deserialized.getAsJsonObject();
				deserialized.getAsJsonObject().add("sta_kewarganearaan", custom.get("sta_kewarganegaraan"));
			}
		};
	}
	
	public static TypeAdapterFactory createPajakAdapter() {
		return new CustomizedTypeAdapterFactory<Pajak>(Pajak.class) {
			@Override
			protected void afterRead(JsonElement deserialized) {
				// pjk_jenis
				JsonObject custom = deserialized.getAsJsonObject().get("pjk_jenis").getAsJsonObject();
				deserialized.getAsJsonObject().remove("pjk_jenis");
				deserialized.getAsJsonObject().add("pjk_jenis",custom.get("jnp_nama"));
				deserialized.getAsJsonObject().remove("pjk_periode");
				deserialized.getAsJsonObject().addProperty("pjk_periode",'Y');
			}
		};
	}
	
	public static TypeAdapterFactory createPengalamanAdapter() {
		return new CustomizedTypeAdapterFactory<Pengalaman>(Pengalaman.class) {
			@Override
			protected void afterRead(JsonElement deserialized) {
				// pjk_jenis
				JsonObject custom = deserialized.getAsJsonObject();
				deserialized.getAsJsonObject().add("pgn_id",custom.get("pgl_id"));
			}
		};
	}
}
