package utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import play.cache.Cache;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

// Implement 256-bit version like: http://securejava.wordpress.com/2012/10/25/aes-256/

public class AesUtil implements Serializable {

    private static final String KEY_CACHE = "aesutil_key_";
    private final int keySize;
    private final int iterationCount;
    private final Cipher cipher;

    public AesUtil(int keySize, int iterationCount) {
        this.keySize = keySize;
        this.iterationCount = iterationCount;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        }
        catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new IllegalStateException(e);
        }
    }

    public String encrypt(String session, String plaintext) {
        AesKey aesKey = Cache.get(KEY_CACHE+session, AesKey.class);
        if(aesKey == null)
            return "";
        try {
            SecretKey key = generateKey(aesKey.salt, aesKey.token);
            byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, aesKey.iv, plaintext.getBytes("UTF-8"));
            return base64(encrypted);
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    public String decrypt(String session, String ciphertext) {
        AesKey aesKey = Cache.get(KEY_CACHE+session, AesKey.class);
        if(aesKey == null)
            return "";
        try {
            SecretKey key = generateKey(aesKey.salt, aesKey.token);
            byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, aesKey.iv, base64(ciphertext));
            return new String(decrypted, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
        try {
            cipher.init(encryptMode, key, new IvParameterSpec(hex(iv)));
            return cipher.doFinal(bytes);
        }
        catch (InvalidKeyException
                | InvalidAlgorithmParameterException
                | IllegalBlockSizeException
                | BadPaddingException e) {
            throw new IllegalStateException(e);
        }
    }

    private SecretKey generateKey(String salt, String passphrase) {
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), hex(salt), iterationCount, keySize);
            SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
            return key;
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new IllegalStateException(e);
        }
    }

    public static String random(int length) {
        byte[] salt = new byte[length];
        new SecureRandom().nextBytes(salt);
        return hex(salt);
    }

    public static String base64(byte[] bytes) {
        return Base64.encodeBase64String(bytes);
    }

    public static byte[] base64(String str) {
        return Base64.decodeBase64(str);
    }

    public static String hex(byte[] bytes) {
        return Hex.encodeHexString(bytes);
    }

    public static byte[] hex(String str) {
        try {
            return Hex.decodeHex(str.toCharArray());
        }
        catch (DecoderException e) {
            throw new IllegalStateException(e);
        }
    }

    public AesKey getKey(String sessionid) {
        AesKey key = Cache.get(KEY_CACHE+sessionid, AesKey.class);
        if(key == null) {
            try {
                KeyGenerator keyGen = KeyGenerator.getInstance("AES");
                keyGen.init(256);
                SecretKey secretKey = keyGen.generateKey();
                byte[] keyBytes = secretKey.getEncoded();
                key = new AesKey();
                key.sessionid = sessionid;
                key.token = hex(keyBytes);
                key.salt = random(128/8);
                key.iv = random(128/8);
                Cache.set(KEY_CACHE+sessionid, key, "30mn");
            }catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return key;
    }

    public static class AesKey implements Serializable {
        public String token;
        public String iv;
        public String salt;
        public String sessionid;
    }
}