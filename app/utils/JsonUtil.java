package utils;

import com.google.gson.*;
import models.jcommon.util.DoubleConverter;

import java.lang.reflect.Type;

/**
 * Created by Lambang on 5/2/2016.
 */
public class JsonUtil {

	public static final Gson gson = new GsonBuilder().setDateFormat("MMM dd,yyyy")
			.registerTypeAdapter(Double.class, new ConvertDoubleJsonUtils()).create();
	public static final Gson gsonSikap = new GsonBuilder().setDateFormat("MMM dd,yyyy")
			.registerTypeAdapter(Double.class, new DoubleConverter())
			.registerTypeAdapterFactory(SikapTypeAdapterFactory.createIjinUsahaAdapter())
			.registerTypeAdapterFactory(SikapTypeAdapterFactory.createStafAhliAdapter())
			.registerTypeAdapterFactory(SikapTypeAdapterFactory.createPajakAdapter())
			.registerTypeAdapterFactory(SikapTypeAdapterFactory.createPengalamanAdapter())
			.create();

	public static JsonObject formatJson(String json) {
		json = json.replaceAll(":\\s+,", ":'0',");
		json = json.replaceAll(":,", ":'0',");
		json = json.replaceAll(":}", ":'0'}");
		json = json.replaceAll(":\\s+}", ":'0'}");
		json = json.replaceAll(":'-'}", ":'0'}");
		json = json.replaceAll(":\"-\"}", ":'0'}");
		JsonObject formattedJson = gson.fromJson(json, JsonObject.class);
		return formattedJson;
	}

	public static <T> T fromJson(final String json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}

	/**
	 * convert Json to Object
	 * @param json
	 * @param typeOfT
	 * @return
	 */
	public static <T> T fromJsonSikap(final String json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
		return gsonSikap.fromJson(json, typeOfT);
	}

	/**
	 * convert Json to Object
	 * @param json
	 * @param classOfT
	 * @return
	 */
	public static <T> T fromJsonSikap(final String json, Class<T> classOfT) throws JsonIOException, JsonSyntaxException {
		return gsonSikap.fromJson(json, classOfT);
	}
}
