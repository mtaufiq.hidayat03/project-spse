package utils.tus;

import models.tus.UuidStore;
import play.mvc.Http;
import utils.Helpers;
import utils.tus.exception.BadHeaderException;
import utils.tus.exception.DomainException;
import utils.tus.exception.RequestException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.*;

public class TusHandler {
	static int TIMEOUT = 30;

	static final String POST      = "POST";
	static final String HEAD      = "HEAD";
	static final String PATCH     = "PATCH";
	static final String OPTIONS   = "OPTIONS";
//	static final String GET       = "GET";

    String directory  = null;
    String path       = null;
    String host       = null;
    Http.Request request = null;
    Http.Response response = null;
    
    public boolean finishAll = false;
	public String uuid       = null;
	
	/**
	 * Constructor
	 * @param directory direktori tempat penyimpanan file yang di-upload
	 * @param path prefix yang digunakan dalam uuid untuk membedakan file ini berasal dari action apa sehingga mencegah cross-action takeover. Jika dalam request berikutnya beda path, akan ditolak. 
	 * @param request HTTP Request 
	 * @param response HTTP Response
	 */
    public TusHandler(String directory, String path, Http.Request request, Http.Response response) {
        this.setDirectory(directory);
        this.path = path;
        this.request = request;
        this.response = response;
    }

    /**
     * Proses request dari client
     * @param userid userid/username dari user yang melakukan request, digunakan untuk validasi sehingga hanya user yang memiliki yang bisa melakukan proses terhadap filenya
     * @param rkn_id id rekanan pemilik file
     * @throws BadHeaderException dilempar ketika request dari user tidak valid, bisa dari header yang salah atau file bukan miliknya
     * @throws Exception dilempar ketika terjadi error pemrosesan file di sisi server
     */
    public void process(String userid, Long rkn_id) throws BadHeaderException, Exception  {
    	String method = request.method;
        try {
            if (method.equals(OPTIONS)) {
                this.uuid = null;
            } else if (method.equals(POST)) {
                this.buildUuid();
            } else {
                this.getUserUuid();
            }
            switch (method) {
                case POST:
                    this.processPost(userid, rkn_id);
                    break;

                case HEAD:
                    this.processHead(userid);
                    break;

                case PATCH:
                    this.processPatch(userid);
                    break;

                case OPTIONS:
                    this.processOptions();
                    break;

//                case GET:
//                    this.processGet(send);
//                    break;

                default:
                    throw new RequestException("The requested method " + method + " is not allowed", 405);
            }

        } catch (BadHeaderException e) {
        	e.printStackTrace();
            response.status = 400;
//            response.message = e.getMessage();
//            this.addCommonHeader(method);
        } catch (RequestException e) {
        	e.printStackTrace();
            response.status = e.getResponseCode();
//            response.
//            this.addCommonHeader(method);
        } catch (Exception e) {
        	e.printStackTrace();
            this.response.status = 500;
//            this.addCommonHeader(method);
        }
        this.addCommonHeader(method);

//        this.response->sendHeaders();
    }


    /**
     * Build a new UUID (use in the POST request)
     * @throws DomainException dilempar ketika path == null
     */
    private void buildUuid() throws DomainException {
        if (this.path == null) {
            throw new DomainException("Path can't be null when calling buildUUid", 500);
        }
        
        this.uuid = this.path + UUID.randomUUID().toString();
    }


    /**
     * Get the UUID of the request (use for HEAD and PATCH request)
     * @return uuid from request
     * @throws Exception dilempar ketika path tidak ada dalam prefix uuid
     */
    private String getUserUuid() throws Exception {
        if (this.uuid == null) {
            String uuid = request.params.get("uuid");

            if (uuid.indexOf(this.path) != 0) {
                throw new Exception("The uuid and the path doesn't match : " + uuid + " - " + this.path);
            }

            this.uuid = uuid;
        }

        return this.uuid;
    }

    /**
     * Process the POST request
     * @param userid
     * @param rkn_id
     * @throws BadHeaderException
     * @throws Exception
     */
    private void processPost(String userid, Long rkn_id) throws BadHeaderException, Exception {
        if (UuidStore.isExistUuid(this.uuid)) {
            throw new BadHeaderException("The UUID already exists", 400);
        }

        Map headers = this.extractHeaders(Arrays.asList("Upload-Length", "Upload-Metadata"));

        if (!Helpers.isInteger(headers.get("Upload-Length")) || Helpers.getInt(headers.get("Upload-Length")) < 0) {
            throw new BadHeaderException("Upload-Length must be a positive integer", 400);
        }

        int finalLength = Helpers.getInt(headers.get("Upload-Length"));
        // based on tus-js implementation, filename is base64-encoded written in Upload-Metadata header. e.g "filename xxxxxx" 
        String filename = new String(Base64.getDecoder().decode(headers.get("Upload-Metadata").toString().replaceAll("filename ", "")));

        String directoryPath = this.directory + this.getFilename();
        File directoryToSave = new File(directoryPath);
        if(!directoryToSave.exists()) {
        	if(!directoryToSave.mkdir()) {
        		throw new Exception("Impossible to create directory "  + directoryPath);
        	}
        }
        
        String filePath = directoryPath + File.separator + filename;
        File file = new File(filePath);

        if (file.exists()) {
            throw new Exception("File already exists : " + filePath);
        }

        if (!file.createNewFile()) {
            throw new Exception("Impossible to create "  + filePath);
        }

        UuidStore uuidStore = new UuidStore();
        uuidStore.uuid = uuid;
        uuidStore.finalLength = finalLength;
        uuidStore.currentoffset = 0;
        uuidStore.filename = filename;
        uuidStore.fileowner = userid;
        uuidStore.rkn_id = rkn_id;
        uuidStore.save();
        response.status = 201;
        String location = request.getBase() + request.url;
        location += location.endsWith("/") ? "" : "/";
        response.setHeader("Location", location + this.uuid);
    }


    /**
     * Process the HEAD request
     * @param userid
     * @throws BadHeaderException if UUID doesn't exist
     * @throws RequestException if UUID does not belong to userid
     */
    private void processHead(String userid) throws BadHeaderException, RequestException {
    	UuidStore uuidStore = UuidStore.findById(this.uuid);
        if (uuidStore == null) {
        	throw new BadHeaderException("The UUID doesn't exists", 400);
        }
        if (!uuidStore.fileowner.equals(userid)) {
            throw new RequestException("The UUID does not belong to " + userid, 404);
        }
        response.status = 200;
        response.setHeader("Upload-Offset", String.valueOf(uuidStore.currentoffset));
    }
    
    /**
     * Process the PATCH request
     * @param userid
     * @throws BadHeaderException thrown when:
     * 				- If the Offset header isn't a positive integer
     * 				- If the Content-Type header isn't "application/offset+octet-stream"
     * 				- If the Offset header and Offset database are not equal
     * 				- If the final length is smaller than offset
     * @throws Exception
     */
    private void processPatch(String userid) throws BadHeaderException, Exception {
        // Check the uuid
    	UuidStore uuidStore = UuidStore.findById(this.uuid);
        if (uuidStore == null) {
            throw new Exception("The UUID doesn't exists");
        }
        if (!uuidStore.fileowner.equals(userid)) {
            throw new RequestException("The UUID does not belong to " + userid, 404);
        }

        // Check HTTP headers
        Map headers = this.extractHeaders(Arrays.asList("Upload-Offset", "Content-Length", "Content-Type"));
        
        if (!Helpers.isInteger(headers.get("Upload-Offset")) || Helpers.getInt(headers.get("Upload-Offset")) < 0) {
            throw new BadHeaderException("Offset must be a positive integer", 400);
        }

        if (!Helpers.isInteger(headers.get("Content-Length")) || Helpers.getInt(headers.get("Content-Length")) < 0) {
            throw new BadHeaderException("Content-Length must be a positive integer", 400);
        }

        if (headers.get("Content-Type") == null || !headers.get("Content-Type").equals("application/offset+octet-stream")) {
            throw new BadHeaderException("Content-Type must be \"application/offset+octet-stream\"", 400);
        }

        // Initialize vars
        int offsetHeader = Helpers.getInt(headers.get("Upload-Offset"));
        Integer offsetRedis = uuidStore.currentoffset;
        Integer maxLength = uuidStore.finalLength;
        int contentLength = Helpers.getInt(headers.get("Content-Length"));

        // Check consistency (user vars vs database vars)
        if (offsetRedis == null || offsetRedis != offsetHeader) {
            throw new BadHeaderException("Offset header isn't the same as in Redis", 400);
        }
        if (maxLength == null || offsetRedis > maxLength) {
            throw new BadHeaderException("Upload-Length is required and must be greather than Offset", 400);
        }

        // Check if the file isn't already entirely write
        if (offsetRedis == maxLength) {
            this.response.status = 200;
            finishAll = true;
            return;
        }
        
//        InputStream inputStream = request.body;

        String directoryPath = this.directory + this.getFilename();
        File directoryToSave = new File(directoryPath);
        if(!directoryToSave.exists()) {
        	if(!directoryToSave.mkdir()) {
        		throw new Exception("Impossible to create directory "  + directoryPath);
        	}
        }
        String filepath = directoryPath + File.separator + uuidStore.filename;
        File file = new File(filepath);
        FileOutputStream outputStream = new FileOutputStream(file, true);
        outputStream.getChannel().position(offsetRedis);

//        ignore_user_abort(true);

        int currentSize = offsetRedis;
        int totalWrite = 0;

        try (InputStream inputStream = request.body){
            while (true) {
//                set_time_limit(self::TIMEOUT);

                // Manage user abort
//                if(connection_status() != CONNECTION_NORMAL) {
//                    throw new Exception\Abort('User abort connexion');
//                }
            	int READ_SIZE = 1024;
            	byte[] data = new byte[READ_SIZE];
            	int sizeRead = inputStream.read(data, 0, READ_SIZE);

                // If user sent more datas than expected (by POST Upload-Length), abort
                if (sizeRead + currentSize > maxLength) {
                    throw new BadHeaderException("Size sent is greater than max length expected", 400);
                }


                // If user sent more datas than expected (by PATCH Content-Length), abort
                if (sizeRead + totalWrite > contentLength) {
                    throw new BadHeaderException("Size sent is greater than max length expected", 400);
                }

                // Write datas
                outputStream.write(data, 0, sizeRead);

                currentSize += sizeRead;
                totalWrite += sizeRead;
                uuidStore.currentoffset = currentSize;
                uuidStore.save();

                if (totalWrite == contentLength) {
                    inputStream.close();
                    outputStream.close();
                    break;
                }
            }
            this.response.status = 200;
            response.setHeader("Upload-Offset", String.valueOf(uuidStore.currentoffset));
            // finish writing all data
            if(maxLength == currentSize) {
                finishAll = true;
            }
        } catch (BadHeaderException e) {
        	e.printStackTrace();
            outputStream.close();
            this.response.status = 400;
        } catch (Exception e) {
        	e.printStackTrace();
            outputStream.close();
            this.response.status = 500;
        }
    }


    /**
     * Process the OPTIONS request
     *
     */
    private void processOptions() {
    	this.response.status = 200;
//        this.response.setHeader("Tus-Extension", "creation,termination,concatenation");
//        this.response.setHeader("Tus-Max-Size", "1073741824");
//        this.response.setHeader("Tus-Resumable", "1.0.0");
//        this.response.setHeader("Tus-Version", "1.0.0");
//        this.response.setHeader("Access-Control-Allow-Methods", "OPTIONS,GET,HEAD,POST,PATCH");
//        this.response.setHeader("Access-Control-Allow-Origin", "*");
//        this.response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Upload-Length, Upload-Offset, Tus-Resumable, Upload-Metadata");
//        this.response.setHeader("Access-Control-Expose-Headers", "Location, Range, Content-Disposition, Offset");
    }


    /**
     * Add the commons headers to the HTTP response
     *
     */
    private void addCommonHeader(String method) {
    	if(method.equals(OPTIONS)) {
	    	this.response.setHeader("Access-Control-Allow-Methods", "POST, HEAD, PATCH, OPTIONS");
			this.response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Upload-Length, Upload-Offset, Tus-Resumable, Upload-Metadata, Access-Control-Allow-Origin");
			this.response.setHeader("Access-Control-Max-Age", "86400");
			this.response.setHeader("Tus-Max-Size", "1073741824");
			this.response.setHeader("Tus-Version", "1.0.0");
			this.response.setHeader("Tus-Extension", "creation,termination,concatenation");
    	} else {
			this.response.setHeader("Access-Control-Expose-Headers", "Upload-Offset, Location, Upload-Length, Tus-Version, Tus-Resumable, Tus-Max-Size, Tus-Extension, Upload-Metadata");
    	}
		this.response.setHeader("Tus-Resumable", "1.0.0");
        this.response.setHeader("Access-Control-Allow-Origin", "*");
    }

    /**
     * Extract a list of headers in the HTTP headers
     * @param headers A list of header name to extract
     * @return A Map of header ([header name: header value])
     * @throws BadHeaderException If a header sought doesn't exist or are empty
     */
    private Map extractHeaders(List<String> headers) throws BadHeaderException {
    	Map<String, String> headersValues = new HashMap<String,String>();
        for (String header:headers) {
            String value = request.headers.get(header.toLowerCase()).value();

            if (value.trim().isEmpty()) {
                throw new BadHeaderException(header+" can't be empty", 400);
            }

            headersValues.put(header,value);
        }

        return headersValues;
    }

    /**
     * Set the directory where the file will be store
     * @param directory The directory where the file are stored
     */
    private void setDirectory(String directory) {
        this.directory = directory + (!directory.endsWith(File.separator) ? File.separator : "");
    }

    /**
     * Set the path to use in the URI
     * @param path The path to use in the URI
     */
    private void setPath(String path) {
        this.path = path;
    }

    /**
     * Get the filename to use when save the uploaded file
     * @return The filename to use
     * @throws Exception If the path isn't defined or If the uuid isn't defined
     */
    private String getFilename() throws Exception {
        if (this.path == null) {
            throw new Exception("Path can't be null when call getFilename");
        }

        if (this.uuid == null) {
            throw new Exception("Uuid can't be null when call getFilename");
        }

        return this.uuid.replaceAll(this.path, "");
    }
    
}
