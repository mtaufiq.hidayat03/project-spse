package utils.tus.exception;

public class DomainException extends Exception {
	int responseCode;
	
	public DomainException(String message, int responseCode) {
		super(message);
		this.responseCode = responseCode;
	}
	
	public int getResponseCode() {
		return this.responseCode;
	}
}
