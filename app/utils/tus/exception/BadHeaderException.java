package utils.tus.exception;

public class BadHeaderException extends Exception {
	int responseCode;
	
	public BadHeaderException(String message, int responseCode) {
		super(message);
		this.responseCode = responseCode;
	}
}
