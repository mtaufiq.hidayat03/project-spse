package utils.tus.exception;

public class RequestException extends Exception {
	int responseCode;
	
	public RequestException(String message, int responseCode) {
		super(message);
		this.responseCode = responseCode;
	}
	
	public int getResponseCode() {
		return this.responseCode;
	}
}
