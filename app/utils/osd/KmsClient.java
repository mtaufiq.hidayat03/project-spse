package utils.osd;

import ams.utils.KeyStoreUtil;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import models.jcommon.util.CommonUtil;
import models.osd.KmsData;
import org.apache.commons.codec.binary.Base64;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.Response;
import play.Logger;
import play.Play;
import play.mvc.Http.StatusCode;
import utils.LogUtil;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class KmsClient {

	public static final String TAG = "KmsClient";
    private static String SERVER_URL;
    private static  String CLIENT_ID;
    private static String CLIENT_SECRET;
    private static final String RESPONSE_SUCCESS = "success";
    private static AsyncHttpClient httpClient = null;

	public static void generate() {
		SERVER_URL = Play.configuration.getProperty("ttp.url");
		CLIENT_ID = Play.configuration.getProperty("ttp.client.id");
		CLIENT_SECRET = Play.configuration.getProperty("ttp.client.secret");
		httpClient = Dsl.asyncHttpClient(KeyStoreUtil.httpClientConfig);
    }
    
    @SuppressWarnings("unchecked")
	private static String getOAuthToken() throws Exception {
		// prepare auth header
		String key = CLIENT_ID + ':' + CLIENT_SECRET;
		key = Base64.encodeBase64String(key.getBytes());
		Response response = httpClient.preparePost(SERVER_URL + "/oauth/token")
				.addQueryParam("client_id", CLIENT_ID)
				.addQueryParam("grant_type", "client_credentials")
				.addHeader("Authorization", "Basic " + key)
				.addHeader("content-type", "application/json")
				.execute().get();
		// process response
		String token = null;
		if (response.getStatusCode() == StatusCode.UNAUTHORIZED) {
			throw new Exception("Anda tidak memiliki akses ke server KMS");
		} else {
			Logger.info("getOAuthToken: response body : %s", response.getResponseBody());
			JsonObject data = (JsonObject) new JsonParser().parse(response.getResponseBody());
			token = data.get("access_token").getAsString();
			// TODO: what to do with this?
			data.get("token_type").getAsString();
			data.get("scope").getAsString();
		}
		return token;

	}

    // check if service online
	public static boolean isOnline() {
		try {
			LogUtil.debug(TAG, SERVER_URL);
			Response response = httpClient.prepareGet(SERVER_URL).execute().get();
			LogUtil.debug(TAG, "Code: " + response.getStatusCode() + " Message: " + response.getStatusText());
			if (response.getStatusCode() >= 200 && response.getStatusCode() < 400) {
				return true;
			}
			return false;
		} catch (Exception e) {
			LogUtil.error(TAG, e);
			return false;
		}
	}
	
    @SuppressWarnings("unchecked")
	public static String getTicketId(KmsData kmsData) throws Exception{
    		Response response = httpClient.preparePost(SERVER_URL+"/api/lelang")
	    			.addHeader("Authorization",  "Bearer " + getOAuthToken())
					.addHeader("content-type","application/json")
	    			.setBody(CommonUtil.toJson(kmsData).getBytes())
	    			.execute().get();
	    	// process response
    		String idTicket = null;
    		Logger.info("KMSClient : getTicketId %s", response);
			if(response.getStatusCode() == StatusCode.UNAUTHORIZED){
				throw new Exception("Anda tidak memiliki akses ke server KMS");
			} else {
				Logger.info("KMSClient : getTicketId body : %s", response.getResponseBody());
		        JsonObject data = (JsonObject)new JsonParser().parse(response.getResponseBody());
		        String status = data.get(RESPONSE_SUCCESS).getAsString();
	            
		        if(status.equals("false")){
	        		throw new Exception(data.get("message").getAsString());
	        	} else {
	        		idTicket = data.get("id_ticket").getAsString();
	        	}
			}
			return idTicket;
    }
    
    
    @SuppressWarnings("unchecked")
	public static String getKPLX(String idTicket, Integer docId) throws Exception{
    		Response response = httpClient.prepareGet(SERVER_URL+"/api/lelang/kplx")
    				.addQueryParam("id_ticket", idTicket).addQueryParam("doc_id", String.valueOf(docId))
	    			.addHeader("Authorization",  "Bearer " + getOAuthToken()).addHeader("content-type","application/json")
	    			.execute().get();
	    	// process response
    		String kplx = null;
			if(response.getStatusCode() == StatusCode.UNAUTHORIZED){
				throw new Exception("Anda tidak memiliki akses ke server KMS");
			} else {			
		        JsonObject data = (JsonObject)new JsonParser().parse(response.getResponseBody());
		        String status = data.get(RESPONSE_SUCCESS).getAsString();		        
	        	if(status.equals("false")){
	        		throw new Exception(data.get("message").getAsString());
	        	} else {
	        		kplx = data.get("kplx").getAsString();
	        	}
			}
			return kplx;
    }        
    
    @SuppressWarnings("unchecked")
	public static String getKPLXd(Integer documentId, String idTicket) throws Exception{
    		Response response = httpClient.prepareGet(SERVER_URL+"/api/lelang/kplxd")
    				.addQueryParam("id_ticket", idTicket).addQueryParam("doc_id", String.valueOf(documentId))
	    			.addHeader("Authorization",  "Bearer " + getOAuthToken()).addHeader("content-type","application/json")
	    			.execute().get();
	    	// process response
    		String kplxd = null;
			if(response.getStatusCode() == StatusCode.UNAUTHORIZED){
				throw new Exception("Anda tidak memiliki akses ke server KMS");
			} else {			
		        JsonObject data = (JsonObject)new JsonParser().parse(response.getResponseBody());
		        String status = data.get(RESPONSE_SUCCESS).getAsString();
	        	if(status.equals("false")){
	        		throw new Exception(data.get("message").getAsString());
	        	} else {
	        		kplxd = data.get("kplxd").getAsString();
	        	}
			}
			return kplxd;
    }
    
    @SuppressWarnings("unchecked")
	public static boolean updateWaktuPembukaan(KmsData kmsData, String idTicket) throws Exception{
    		Response response = httpClient.preparePost(SERVER_URL+"/api/lelang/update/"+idTicket)
	    			.addHeader("Authorization",  "Bearer " + getOAuthToken()).addHeader("content-type","application/json")
	    			.setBody(CommonUtil.toJson(kmsData).getBytes()).execute().get();
	    	// process response
    		boolean hasil = false;
			if(response.getStatusCode() == StatusCode.UNAUTHORIZED){
				throw new Exception("Anda tidak memiliki akses ke server KMS");
			} else {			
		        JsonObject data = (JsonObject)new JsonParser().parse(response.getResponseBody());
		        String status = data.get(RESPONSE_SUCCESS).getAsString();
		        if(status.equals("false")){
	        		throw new Exception(data.get("message").getAsString());
	        	} else {
	        		hasil = true;
	        	}
			}
			return hasil;
    }
    
    @SuppressWarnings("unchecked")
	public static String getPrivateForAuditor(String idTicket, Integer docId) throws Exception{
    		Response response = httpClient.prepareGet(SERVER_URL+"/api/key")
    				.addQueryParam("id_ticket", idTicket).addQueryParam("doc_id", String.valueOf(docId))
	    			.addHeader("Authorization",  "Bearer " + getOAuthToken()).addHeader("content-type","application/json")
	    			.execute().get();
	    	// process response
    		String plainPrivate = null;
			if(response.getStatusCode() == StatusCode.UNAUTHORIZED){
				throw new Exception("Anda tidak memiliki akses ke server KMS");
			} else {			
		        JsonObject data = (JsonObject)new JsonParser().parse(response.getResponseBody());
		        String status = data.get(RESPONSE_SUCCESS).getAsString();
	        	if(status.equals("false")){
	        		throw new Exception(data.get("message").getAsString());
	        	} else {
	        		plainPrivate = data.get("private_key").getAsString();
	        	}
			}
			return plainPrivate;
    }
    
    public static boolean isValidIdPaket(String kplx, long pktId) {
    	String decodedKplx = new String(Base64.decodeBase64(kplx));
    	Pattern MY_PATTERN = Pattern.compile("<id_paket>(.*?)</id_paket>");
        Matcher m = MY_PATTERN.matcher(decodedKplx);
        if (m.find()) {
            String s = m.group(1);
            long sentPktId = Long.parseLong(s);
            return sentPktId == pktId;
        }
        return false;
    }


	public static void clean() {
		LogUtil.debug(TAG, "clean resources");
		if (httpClient != null) {
			LogUtil.debug(TAG, "clean rest client");
			try {
				httpClient.close();
				httpClient = null;
			}catch (IOException e) {

			}
		}
	}
}
