package utils.osd;

import models.osd.ErrorCodeOSD;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import play.Logger;
import play.Play;
import utils.Encrypt;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.*;
import java.security.spec.PKCS8EncodedKeySpec;

public class CertificateUtil {
    
	public static String keystorePath;
	
	public static String truststorePath;
	
    public static String truststorePass;
	
    public static String keystorePass;
    
    public static String serverAlias;
    
    public static String caAlias;
	
	public static boolean enableCa = false;
    
    static {
    	enableCa = Play.configuration.containsKey("keystore");
    	if(enableCa) {
    		keystorePath = Play.configuration.getProperty("keystore");
    		truststorePath = Play.configuration.getProperty("truststore");
    	    truststorePass = Play.configuration.getProperty("truststore.pass");
    	    keystorePass = Play.configuration.getProperty("keystore.pass");
    	    serverAlias = Play.configuration.getProperty("keystore.server.alias");
    	    caAlias = Play.configuration.getProperty("truststore.ca.alias");
    	}
    }


	private static PrivateKey getPrivateKey(){
    	PrivateKey privateKey = null;
    	try {
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(Play.classloader.getResourceAsStream(keystorePath), keystorePass.toCharArray());
	    	Key pvt = ks.getKey(serverAlias, keystorePass.toCharArray());
	    	KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(pvt.getEncoded());
            privateKey = keyFactory.generatePrivate(privateKeySpec);
		} catch (Exception e) {
			Logger.error(e, e.getMessage());
		}
		return privateKey;
    }
    
    public static String getCertificatePEM(){
    	String pem = null;
		try {
			Certificate cert = getCertificate();
			if(cert!=null)
				pem = Base64.encodeBase64String(cert.getEncoded());
		} catch (CertificateEncodingException e) {
			Logger.error(e, e.getMessage());
		}
		Logger.trace("\nCERT SERVER PEM:\n"+pem);
    	return pem;
    }
    
    public static byte[] getCertificateEncoded(){
    	try {
			return getCertificate().getEncoded();
		} catch (CertificateEncodingException e) {
			Logger.error(e, e.getMessage());
		}
		return null;
    }
    
    public static Certificate getCertificate(){
    	Certificate cert = null;
		try {
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(Play.classloader.getResourceAsStream(keystorePath), keystorePass.toCharArray());
	    	cert = ks.getCertificate(serverAlias);
		} catch (Exception e) {
			Logger.error(e, e.getMessage());
		}
		return cert;
    }
    
    public static Certificate getCACertificate(){
    	Certificate cert = null;
		try {
			Logger.debug("get ca certificate: " + caAlias);
			KeyStore ks = KeyStore.getInstance("JKS");
			ks.load(Play.classloader.getResourceAsStream(truststorePath), truststorePass.toCharArray());
	    	cert = ks.getCertificate(caAlias);
		} catch (Exception e) {
			Logger.error(e, e.getMessage());
		}
		return cert;
    }
    
    public static byte[] RSADecryption(String encrypted){
		try {
			byte[] encryptedByte = Base64.decodeBase64(encrypted);
			return Encrypt.decryptRSA(encryptedByte, getPrivateKey());
		} catch (Exception e) {
			Logger.error(e, e.getMessage());
		}
		return new byte[0];
	}
	
//	digunakan hanya untuk development
//	public String RSAEncryption(byte[] plain){
//		String ret = null;
//		try {
//			byte[] encrypted = RSAEncryption.encrypt(plain, getCertificate().getPublicKey());
//			ret = Base64.encode(encrypted);
//		} catch (InvalidKeyException e) {
//			logger.error(e);
//		} catch (NoSuchAlgorithmException e) {
//			logger.error(e);
//		} catch (NoSuchPaddingException e) {
//			logger.error(e);
//		} catch (IllegalBlockSizeException e) {
//			logger.error(e);
//		} catch (BadPaddingException e) {
//			logger.error(e);
//		} catch (NoSuchProviderException e) {
//			logger.error(e);
//		}
//		return ret;
//	}
    
    public static ErrorCodeOSD checkCertificate(X509Certificate certificate){
		try {
			certificate.checkValidity(controllers.BasicCtr.newDate());
			Logger.info("user certificate is valid : " + certificate.getSubjectDN().getName());
		} catch (CertificateExpiredException e) {
			Logger.debug(e, e.getMessage());
			return ErrorCodeOSD.CERT_EXPIRED;
		} catch (CertificateNotYetValidException e) {
			Logger.debug(e, e.getMessage());
			return ErrorCodeOSD.CERT_NOT_YET_VALID;
		}
		
		Certificate cacert = getCACertificate();
		if(cacert!=null){
			try {
				certificate.verify(cacert.getPublicKey());
				Logger.debug("user certificate verified : " + certificate.getSubjectDN().getName());
			} catch (InvalidKeyException e) {
				Logger.error(e, "certificate verification failed: InvalidKeyException");
			} catch (CertificateException e) {
				Logger.error(e, "certificate verification failed: CertificateException");
			} catch (NoSuchAlgorithmException e) {
				Logger.error(e, "certificate verification failed: NoSuchAlgorithmException");
			} catch (NoSuchProviderException e) {
				Logger.error(e,"certificate verification failed: NoSuchProviderException");
			} catch (SignatureException e) {
				Logger.debug(e, e.getMessage());
				return ErrorCodeOSD.CERT_UNTRUSTED;
			}
		} else {
			Logger.error("get CA certificate failed, cannot verifying user certificate : " + certificate.getSubjectDN().getName());
		}
		return null;
	}
    
    public static byte[] signing(File file){
		try {
			Signature rsa = Signature.getInstance("SHA256withRSA");
			rsa.initSign(getPrivateKey());
	    	FileInputStream fis = new FileInputStream(file);
	    	BufferedInputStream bufin = new BufferedInputStream(fis);
	    	byte[] buffer = new byte[1024];
	    	int len;
	    	while ((len = bufin.read(buffer)) >= 0) {
	    	    rsa.update(buffer, 0, len);
	    	};
	    	bufin.close();
	    	byte[] signature = rsa.sign();
	    	Logger.info("signing file completed");
    		return signature;
		} catch (NoSuchAlgorithmException e) {
			Logger.error(e, "no algorithm SHA256withRSA is available");
		} catch (InvalidKeyException e) {
			Logger.error(e,"private file invalid");
		} catch (FileNotFoundException e) {
			Logger.error(e, "no file to signed");
		} catch (SignatureException e) {
			Logger.error(e,"error on signing file");
		} catch (IOException e) {
			Logger.error(e, "error on signing file");

		} 
		Logger.error("signing file failed");
    	return null;
    }
    
    public static byte[] signing(String string){
		return signing(string.getBytes());
    }
    
    public static byte[] signing(byte[] bytes){
		try {
			Signature rsa = Signature.getInstance("SHA256withRSA");
			rsa.initSign(getPrivateKey());
    	    rsa.update(bytes);
	    	byte[] signature = rsa.sign();
	    	Logger.info("signing completed");
    		return signature;
		} catch (NoSuchAlgorithmException e) {
			Logger.error(e, "no algorithm SHA256withRSA is available");
		} catch (InvalidKeyException e) {
			Logger.error(e, "private key invalid");
		} catch (SignatureException e) {
			Logger.error(e, "error on signing");
		} 
		Logger.error("signing failed");
    	return null;
    }
    
//    buat ngetest verify signature
    public static boolean verify(byte[] data, byte[] sign){
    	boolean verified = false;
		try {
			Signature rsa = Signature.getInstance("SHA256withRSA");
			rsa.initVerify(getCertificate().getPublicKey());
    	    rsa.update(data);
	    	verified = rsa.verify(sign);
	    	Logger.info("signature verified: " + verified);
    		return verified;
		} catch (NoSuchAlgorithmException e) {
			Logger.error(e, "no algorithm SHA256withRSA is available");
		} catch (InvalidKeyException e) {
			Logger.error(e, "private key invalid");
		} catch (SignatureException e) {
			Logger.error(e, "error on signing");
		}
    	return verified;
    }
    
    public static byte[] formatSignature(byte[] signature){
    	// append signature with signer certificate
    	String cert = getCertificatePEM();
    	if(!cert.startsWith("-----BEGIN CERTIFICATE-----")) cert="-----BEGIN CERTIFICATE-----\n"+cert;
    	if(!cert.endsWith("-----END CERTIFICATE-----")) cert=cert+"\n-----END CERTIFICATE-----";
    	
		byte[] delimiter = new byte[]{35,20,20,4,35};
		byte[] serverCert = cert.getBytes();	// as pem byte[] 
		byte[] formated = new byte[signature.length+serverCert.length+delimiter.length];
//		ArrayUtil.arraycopy(signature, 0, formated, 0, signature.length);
//		ArrayUtil.arraycopy(delimiter, 0, formated, signature.length, delimiter.length);
//		ArrayUtil.arraycopy(serverCert, 0, formated, signature.length+delimiter.length, serverCert.length);
//		for(int i=0;i<formated.length;i++){
//			if(i<signature.length) formated[i]=signature[i];
//			if(i<delimiter.length) formated[i+signature.length]=delimiter[i];
//			if(i<serverCert.length) formated[i+signature.length+delimiter.length]=serverCert[i];
//		}
		boolean stop=false;
		int i=0;
		while(!stop){
			stop=true;
			if(i<signature.length){
				formated[i]=signature[i];
				stop=false;
			}
			if(i<delimiter.length){
				formated[i+signature.length]=delimiter[i];
				stop=false;
			}
			if(i<serverCert.length){
				formated[i+signature.length+delimiter.length]=serverCert[i];
				stop=false;
			}
			i++;
		}
        return formated;
    }
	/*
    public static void writeCertToFile(X509Certificate x509Certificate, String pemName) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PEMWriter pemWrt = new PEMWriter(new OutputStreamWriter(baos));
        pemWrt.writeObject(x509Certificate);
        pemWrt.close();
        baos.close();
        File file = new File(pemName);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(baos.toByteArray());
        fos.flush();
        fos.close();
    }*/

//  input certificate bisa berupa file CER/DER atau PEM
    public static Certificate getCertFromFile(String certificateFile) throws FileNotFoundException, CertificateException {
    	Security.addProvider(new BouncyCastleProvider());
        InputStream is = null;
        java.security.cert.Certificate retval = null;
        is = new FileInputStream(certificateFile);
        try {
        	CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
        	retval = cf.generateCertificate(is);
            is.close();
        } catch (Exception e) {
			Logger.error(e, e.getMessage());
        }
        return retval;
	}
    
	public static Certificate getCertFromPEM(String pem) throws CertificateException {
		return getCertfromDER(Base64.decodeBase64(pem));
	}
	
	public static Certificate getCertfromDER(byte[] cert) throws CertificateException {
		Security.addProvider(new BouncyCastleProvider());
		Certificate ret = null;
	    try {
	        CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC"); 
	        ret = cf.generateCertificate(new ByteArrayInputStream(cert));        	
	    } catch (Exception e) {
	    	Logger.error(e,"Certificate exception trying to read X509Certificate." + e);
		}
	    return ret;
	}
    
	public static String getPEMFromCert(Certificate certificate){
		String pem = null;
		try {
			pem = encodeSplitLine(certificate.getEncoded());
		} catch (CertificateEncodingException e) {
			Logger.error(e,"Can't convert java.security.cert.Certificate to String PEM" + e);
		}
		return pem;
	}
	

    public static String encodeSplitLine(byte[] data) {
		byte[] bytes = Base64.encodeBase64(data);

        // make sure we get limited lines...
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        for (int i = 0; i < bytes.length; i += 64) {
            if ((i + 64) < bytes.length) {
                os.write(bytes, i, 64);
                os.write('\n');
            } else {
                os.write(bytes, i, bytes.length - i);
            }
        }
        return new String(os.toByteArray());
    }
	
//	public static boolean isMatchSubjectDN(String subjectDN0, String subjectDN1){
//		String[] args0 = subjectDN0.split(",");
//		String[] args1 = subjectDN1.split(",");
//		if(args0.length==args1.length){
//			for(int i=0;i<args0.length;i++){
//				boolean found = false;
//				for(int j=0;j<args1.length;j++){
//					if(args0[i].trim().toUpperCase().equals(args1[j].trim().toUpperCase())){
//						found = true;
//						break;
//					}
//				}
//				if(!found)
//					return false;
//			}
//			return true;
//		}
//		return false;
//	}
}
