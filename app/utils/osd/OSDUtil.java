package utils.osd;

import ams.models.ServiceResult;
import ams.utils.AmsUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import ext.FormatUtils;
import models.agency.Anggota_panitia;
import models.agency.Paket;
import models.agency.Pegawai;
import models.common.ConfigurationDao;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import models.jcommon.util.DoubleConverter;
import models.lelang.Dok_penawaran.JenisDokPenawaran;
import models.lelang.Jadwal;
import models.lelang.Lelang_seleksi;
import models.osd.*;
import models.rekanan.Rekanan;
import models.secman.Group;
import models.secman.Usergroup;
import models.sso.common.adp.util.DceSecurityV2;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.util.encoders.Hex;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;
import utils.Encrypt;
import utils.JsonUtil;
import utils.LogUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import java.lang.reflect.Type;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OSDUtil {

	public static final String TAG = "OSDUtil";
	
	public enum X509_Cer_Options_enum_Gw {
		common_name_enum(0), country_enum(1), organization_enum(2), org_unit_enum(3), locality_enum(4), state_enum(5), serial_number_enum(6), email_enum(7), uri_enum(8), ip_enum(9), dns_enum(10), xmpp_enum(11), challenge_enum(12), start_enum(13), end_enum(14), is_CA_enum(15), path_limit_enum(16), constraints_enum(17), ex_constraints_enum(18);
		private int id;
		X509_Cer_Options_enum_Gw(int id){
			this.id = new Integer(id);
		}
		public Integer getId(){
			return id;
		}
	};
	
	
	public static JsonObject getParams(String params, byte[] key) {
		try {
//			byte[] key = CertificateUtil.RSADecryption(random);
			String plainParams = new String(Encrypt.decryptAES(Base64.decodeBase64(params), key));
			return JsonUtil.formatJson(plainParams);
		} catch (InvalidKeyException e) {
			Logger.error("error get params : " + e);
		} catch (NoSuchAlgorithmException e) {
			Logger.error("error get params : " + e);
		} catch (NoSuchProviderException e) {
			Logger.error("error get params : " + e);
		} catch (NoSuchPaddingException e) {
			Logger.error("error get params : " + e);
		} catch (ShortBufferException e) {
			Logger.error("error get params : " + e);
		} catch (IllegalBlockSizeException e) {
			Logger.error("error get params : " + e);
		} catch (BadPaddingException e) {
			Logger.error("error get params : " + e);
		}
		return null;
    }
	
	/**
	 * Mengembalikan return untuk response-response dalam bentuk JSON: {"status":value} yang dienkrip menggunakan <b>key</b>
	 * @param value return code yang ingin dikembalikan
	 * @param key key untuk enkrip JSON response
	 * @return
	 */
	public static String getReturn(String value, byte[] key){
    	String ret = null;
    	if(value==null)
    		value = ErrorCodeOSD.getDefault();
//    	byte[] key = CertificateUtil.RSADecryption(random);
    	try {
    		Map<String,Object> retval = new HashMap<>();
    		if(value.startsWith("ERR")) { // this is error
    			retval.put("status", value);
    		} else {
    			retval.put("value", value);
    		}
			ret = Base64.encodeBase64String(Encrypt.encryptAES(CommonUtil.toJson(retval), key));
		} catch (InvalidKeyException e) {
			Logger.error("error get return : " + e);
		} catch (NoSuchAlgorithmException e) {
			Logger.error("error get return : " + e);
		} catch (NoSuchProviderException e) {
			Logger.error("error get return : " + e);
		} catch (NoSuchPaddingException e) {
			Logger.error("error get return : " + e);
		} catch (ShortBufferException e) {
			Logger.error("error get return : " + e);
		} catch (IllegalBlockSizeException e) {
			Logger.error("error get return : " + e);
		} catch (BadPaddingException e) {
			Logger.error("error get return : " + e);
		}
		return ret;
    }
	
	/**
	 * Method yang membungkus proses aktivasi sehingga reuseable
	 * @param rekanan Rekanan yang ingin diaktivasi (jika group = rekanan)
	 * @param pegawai Pegawai yang ingin diaktivasi (jika group = pegawai)
	 * @param group group dari user
	 * @param currentDate tanggal saat ini 
	 * @return status aktivasi
	 */
	public static String certActivateWrapper(Rekanan rekanan, Pegawai pegawai, Group group, Date currentDate){
		Certificate certificate;
		if(rekanan!=null){
			if(rekanan!=null && rekanan.cer_id!=null){
//				certificate = Certificate.getLastCertificate(rekanan.cer_id);
				certificate = OSDUtil.getLastCertificateFromInaproc(rekanan.cer_id);
				if(certificate!=null){
					if(certificate.cer_jenis.equals(JenisCertificate.CSRR_PENYEDIA))
						return ErrorCodeOSD.CSRR_NOT_VERIFIED.getCode();
					else if(certificate.cer_jenis.equals(JenisCertificate.CSR_PENYEDIA_REJECTED))
						return ErrorCodeOSD.CSRR_REJECTED.getCode();
				} else
					return ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode();
			} else
				return ErrorCodeOSD.DATA_NOT_FOUND.getCode();
		} else {
			if(pegawai!=null && pegawai.cer_id!=null){
//				certificate = Certificate.getLastCertificate(pegawai.cer_id);
				certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id);
				if(certificate!=null && group!=null) {
					if(group.isAdminPPE()) {
						if(certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_PPE))
							return ErrorCodeOSD.CSRR_NOT_VERIFIED.getCode();
						else if(certificate.cer_jenis.equals(JenisCertificate.CSR_ADMIN_PPE_REJECTED))
							return ErrorCodeOSD.CSRR_REJECTED.getCode();
					} else if(group.isAdminAgency()) {
						if(certificate.cer_jenis.equals(JenisCertificate.CSRR_ADMIN_AGENCY))
							return ErrorCodeOSD.CSRR_NOT_VERIFIED.getCode();
						else if(certificate.cer_jenis.equals(JenisCertificate.CSR_ADMIN_AGENCY_REJECTED))
							return ErrorCodeOSD.CSRR_REJECTED.getCode();
					} else if(group.isVerifikator()) {
						if(certificate.cer_jenis.equals(JenisCertificate.CSRR_VERIFIKATOR))
							return ErrorCodeOSD.CSRR_NOT_VERIFIED.getCode();
						else if(certificate.cer_jenis.equals(JenisCertificate.CSR_VERIFIKATOR_REJECTED))
							return ErrorCodeOSD.CSRR_REJECTED.getCode();
					} else if(group.isPanitia()) {
						if(certificate.cer_jenis.equals(JenisCertificate.CSRR_PANITIA))
							return ErrorCodeOSD.CSRR_NOT_VERIFIED.getCode();
						else if(certificate.cer_jenis.equals(JenisCertificate.CSR_PANITIA_REJECTED))
							return ErrorCodeOSD.CSRR_REJECTED.getCode();
					} else {
							Logger.error("user group not found");
							return ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode();
					}
				} else
					return ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode();
			} else
				return ErrorCodeOSD.DATA_NOT_FOUND.getCode();
		}
		
		if(certificate.cer_jenis.equals(JenisCertificate.CERT_PENYEDIA)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_PANITIA)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_PPE)){
			try {
				X509Certificate x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
				if(x509Certificate!=null){
					certificate.cer_sn = new String(Hex.encode(x509Certificate.getSerialNumber().toByteArray())).toUpperCase();
					certificate.cer_issuer_dn = x509Certificate.getIssuerDN().getName();
					certificate.cer_subject_dn = x509Certificate.getSubjectDN().getName();
//					certificate.save();
					String pem = certificate.cer_pem;
					Long cer_id = OSDUtil.updateCertificateToInaproc(certificate, rekanan != null ? rekanan.rkn_namauser : pegawai.peg_namauser, currentDate);
					if(cer_id==null)
						return ErrorCodeOSD.FAILED.getCode();
					else
						return pem;
				}
			} catch (CertificateException e) {
				Logger.error(e, "can't get certificate info");
			}
		} else if(certificate.cer_jenis.equals(JenisCertificate.REVOKED_PENYEDIA)
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_PANITIA)
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_VERIFIKATOR)
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_AGENCY)
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_PPE)){
			return ErrorCodeOSD.CERT_EXPIRED.getCode();
		}
		return ErrorCodeOSD.FAILED.getCode();
	}
	
	public static String certActivateByOP(Long userId, boolean isRekanan, Date date){
		Rekanan rekanan = null;
		Pegawai pegawai = null;
		Group group = null;
		if(isRekanan)
			rekanan = Rekanan.findById(userId);
		else {
			pegawai = Pegawai.findById(userId);
			group= Usergroup.findGroupByUser(pegawai.peg_namauser);
		}
		return certActivateWrapper(rekanan, pegawai, group, date);
	}
	
	public static ServiceResult validCertPanitia(List<Anggota_panitia> anggotaPanitia, Lelang_seleksi lelangItem, List<Jadwal> jadwalList){
		int countCert = 0;
		for (Anggota_panitia anggota:anggotaPanitia) {
			Pegawai pegawai = Pegawai.findById(anggota.peg_id);
			if (pegawai.cer_id!=null) {
				Certificate certificate = OSDUtil.getLastCertificateFromInaproc(pegawai.cer_id, JenisCertificate.CERT_PANITIA);
	//			if(certificate==null){
	//				//jika sertifikat sedang proses renew, periksa versi sertifikat sebelumnya
	//				if(certificate.cer_jenis.equalsIgnoreCase(JenisCertificate.CSRR_PANITIA_RENEW))
	//					certificate = Certificate.getLastCertificateIfRenew(pegawai.cer_id, JenisCertificate.CERT_PANITIA);		
	//			}

				if (certificate == null
						|| certificate.cer_sn ==null) {
					LogUtil.debug(TAG, "no certificate found!");
					LogUtil.debug(TAG, pegawai.peg_nama + " no certificate found!");
					return new ServiceResult("Terdapat anggota panitia tidak memiliki sertifikat!");
				}

				if (!AmsUtil.checkOCSPCertificate(certificate.getX509()).isValid()) {
					LogUtil.debug(TAG, "gagal melakukan validasi sertifikat ke service!");
					return new ServiceResult("Gagal melakukan validasi sertifikat pokja ke service OCSP atau sertifikat telah di-revoke!");
				}

				if (certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
					if (!certificate.isCertExpired())
						countCert = countCert + certPanitiaValidDate(certificate, lelangItem, jadwalList);
				}
			}
		}
		final boolean status = countCert == anggotaPanitia.size();
		return new ServiceResult(status, status ? "success" : "gagal");
	}
	
	public static int certPanitiaValidDate(Certificate certificate, Lelang_seleksi lelangItem, List<Jadwal> jadwalList){
		X509Certificate x509Certificate;
		try {
			x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
			if(x509Certificate!=null){
				//periksa masa berlaku sertifikat bandingkan dengan batas akhir pembukaan penawaran
				Date endValidCert = x509Certificate.getNotAfter();
				if(jadwalList==null){
					if (lelangItem.isSatuFile()) {
						Tahap tahap1 = Tahap.PEMBUKAAN_PENAWARAN;
						Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
						Date dateBuka1 = jadwal1.dtj_tglakhir;
						if(jadwal1.isDtjAkhirExist() && dateBuka1.before(endValidCert)){
							return 1;
						}
					} else {	
						Tahap tahap1 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS;
						Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
						if(jadwal1 == null) {
							tahap1 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI;
							jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
						}
						Date dateBuka1 = jadwal1.dtj_tglakhir;
						if(jadwal1.isDtjAkhirExist() && dateBuka1.before(endValidCert)){
							Tahap tahap2 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA;
							Jadwal jadwal2 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap2);
							Date dateBuka2 = jadwal2.dtj_tglakhir;
							if(jadwal2.isDtjAkhirExist() && dateBuka2.before(endValidCert)){
								return 1;
							}
						}
					}
				} else {
					int retValue = 1;
					for (int i = 0; i < jadwalList.size(); i++) {
						Jadwal jadwal = jadwalList.get(i);
						if(jadwal.thp_id.equals(Tahap.PEMBUKAAN_PENAWARAN.id)
								|| jadwal.thp_id.equals(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS.id)
								|| jadwal.thp_id.equals(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA.id)
								|| jadwal.thp_id.equals(Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI.id)){
							//Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, jadwal.getTahap(), null, null);
							if(jadwal.isDtjAkhirExist() && jadwal.dtj_tglakhir.after(endValidCert)){
								retValue = 0;
							}
						}
					} 
					return retValue;
				}
			} else {
				return 0;
			}
		} catch (CertificateException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
//	metode untuk memeriksa apakah sertifikat OP masih aktif, sebelum pakai CRL atau OCSP
	public static boolean isOpAllowed(Pegawai op){
//		Certificate certOP = Certificate.getLastCertificate(op.cer_id);
		Certificate certOP = getLastCertificateFromInaproc(op.cer_id);
		if(certOP != null && (certOP.cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY) || certOP.cer_jenis.equals(JenisCertificate.CERT_ADMIN_PPE) || certOP.cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR)))
			return true;
		return false;
	}
	
	public static String getEjbcaProfileName(String profile) {
		return "LPSE-"+profile+"-PROFILE";
	}
	
	public static String getRangeCertificateValidDate(Certificate certificate){
		String rangeValidDate = "";
		X509Certificate x509Certificate;
		if(certificate.cer_pem!=null){
			try {
				x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
				if(x509Certificate!=null){
					//Date startValidCert = x509Certificate.getNotBefore();
					Date endValidCert = x509Certificate.getNotAfter();
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					rangeValidDate = dateFormat.format(endValidCert);
				}
			} catch (CertificateException e) {
				e.printStackTrace();
			}
		}
		return rangeValidDate;
	}
	
	public static String updateCSRRRekanan(Rekanan rekanan, String subjectDN, String eeCSRR, Date currentDate) {
		Certificate certificate = null;
		if (rekanan.cer_id != null && rekanan.cer_id != 0) {
			//periksa apakah pernah daftar dari lpse lain
			if(!rekanan.isCertRoaming()){
				certificate = getLastCertificateFromInaproc(rekanan.cer_id);
				if(certificate==null){
					Logger.info("Gagal meminta sertifikat dari Inaproc");
					return ErrorCodeOSD.FAILED.getCode();
				}
			} else {
				Logger.info("rekanan:" + rekanan.rkn_namauser + " pernah mendaftar sertifikat di lpse lain");
				return ErrorCodeOSD.INVALID_LPSE.getCode();
			}
		} else
			certificate = new Certificate();
		if(certificate.cer_id==null
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_PENYEDIA)
				|| certificate.cer_jenis.equals(JenisCertificate.CSR_PENYEDIA_REJECTED)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_PENYEDIA)){
			
			if(certificate.cer_id!=null){
				//Jika sudah punya sertifikat tapi sudah expired bisa buat sertifikat baru
				if(certificate.cer_jenis.equals(JenisCertificate.CERT_PENYEDIA)){
					//Jika sertifikat masih aktif tidak bisa request baru
					X509Certificate x509Certificate;
					try {
						x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
						if(x509Certificate!=null){
							//periksa masa berlaku sertifikat
							Date endValidCert = x509Certificate.getNotAfter();
							Date dateNow = new Date();
							if(dateNow.before(endValidCert)){
								Logger.info("set CSRR failed, user: "+rekanan.rkn_namauser+" already have active certificate");
								return ErrorCodeOSD.ALREADY_HAVE_CERT.getCode();
							}
						}
					} catch (CertificateException e) {
						e.printStackTrace();
					}
				}
			}
			
			certificate.cer_subject_dn = subjectDN;
			certificate.cer_sn = null;
			certificate.cer_issuer_dn = null;
			certificate.cer_jenis = JenisCertificate.CSRR_PENYEDIA;
			
			certificate.cer_pem = null;
			certificate.csr_pem = eeCSRR;
			
//			gunakan rkn_id milik AD sebagai username untuk certificate
			certificate.cer_username = "PN."+rekanan.rkn_id;
//			rekanan.cer_id = (Long) Certificate.insertCertificate(certificate);
			Long cer_id = insertCertificateToInaproc(certificate, rekanan.rkn_namauser, currentDate);
			if(cer_id == null){
				Logger.error("certificate gagal tersimpan ke ADP");
				return ErrorCodeOSD.FAILED.getCode();
			} else {
				rekanan.cer_id = cer_id;
				return ErrorCodeOSD.SUCCEEDED.getCode();
			}
		} else {
			Logger.info("set CSRR failed, user: "+rekanan.rkn_namauser+" already have request/active certificate");
			return ErrorCodeOSD.ALREADY_HAVE_CERT.getCode();
		}
	}
	
	public static Certificate getLastCertificateFromInaproc(Long cer_id, String cer_jenis) {
		if(cer_id == null)
			return null;
		Certificate userCert = new Certificate();
		userCert.cer_id = cer_id;
		if(cer_jenis != null) {
			userCert.cer_jenis = cer_jenis;
		}
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(userCert)));
		String url_service = ConfigurationDao.getRestCentralCertificateServiceLastCert(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(response.success() && !StringUtils.isEmpty(response.getString())) { // jika tidak ada error
				String res = DceSecurityV2.decrypt(response.getString());
				return CommonUtil.fromJson(res, Certificate.class);
			}
		} catch (Exception e) {
			Logger.error(e, "gagal ambil cert dari adp");
		}
		return null;
	}

	
	public static Certificate getLastCertificateFromInaproc(Long cer_id) {
		return getLastCertificateFromInaproc(cer_id, null);
	}
	
	public static List<Certificate> getCertificatesFromInaproc(Long cer_id) {
		if(cer_id == null)
			return null;
		Certificate userCert = new Certificate();
		userCert.cer_id = cer_id;
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(userCert)));
		String url_service = ConfigurationDao.getRestCentralCertificateServiceAllCert(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(response.success() && !StringUtils.isEmpty(response.getString())) { // jika tidak ada error
				Type collectionType = new TypeToken<List<Certificate>>(){}.getType();
				String res = DceSecurityV2.decrypt(response.getString());
				return (List<Certificate>) Json.fromJson(res, collectionType);
			}
		} catch (Exception e){
			return null;
		}
		return null;
	}
	
	public static String updateCSRRPegawai(Pegawai pegawai, Group group, String subjectDN, String eeCSRR, Date currentDate) {
		String usernamePrefix=null;
		Certificate certificate = null;
		if(group.isPanitia())
			usernamePrefix="PA.";
		else if(group.isVerifikator())
			usernamePrefix="VE.";
		if(group.isAdminAgency())
			usernamePrefix="AG.";
		
		if(pegawai.cer_id==null)
			certificate = new Certificate();
		else {
			certificate = getLastCertificateFromInaproc(pegawai.cer_id);
			if(certificate==null){
				Logger.info("Gagal meminta sertifikat dari Inaproc");
				return ErrorCodeOSD.FAILED.getCode();
			}
		}
		if(certificate.cer_id==null
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_PANITIA)
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_ADMIN_AGENCY)
				|| certificate.cer_jenis.equals(JenisCertificate.REVOKED_VERIFIKATOR)
				|| certificate.cer_jenis.equals(JenisCertificate.CSR_PANITIA_REJECTED)
				|| certificate.cer_jenis.equals(JenisCertificate.CSR_ADMIN_AGENCY_REJECTED)
				|| certificate.cer_jenis.equals(JenisCertificate.CSR_VERIFIKATOR_REJECTED)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_PANITIA)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR)){
			
			if(certificate.cer_id!=null){
				if(certificate.cer_jenis.equals(JenisCertificate.CERT_PANITIA)
						|| certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY)
						|| certificate.cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR)){
					//Jika sertifikat masih aktif tidak bisa request baru
					X509Certificate x509Certificate;
					try {
						x509Certificate = (X509Certificate) CertificateUtil.getCertFromPEM(certificate.cer_pem);
						if(x509Certificate!=null){
							//memeriksa masa berlaku sertifikat
							Date endValidCert = x509Certificate.getNotAfter();
							Date dateNow = new Date();
							if(dateNow.before(endValidCert)){
								Logger.info("set CSRR failed, user: "+pegawai.peg_namauser+" already have active certificate");
								return ErrorCodeOSD.FAILED.getCode();
							}
						}
					} catch (CertificateException e) {
						e.printStackTrace();
					}
				}
			}
			
			certificate.cer_subject_dn = subjectDN;
			certificate.cer_sn = null;
			certificate.cer_issuer_dn = null;
			if(group.isPanitia())
				certificate.cer_jenis = JenisCertificate.CSRR_PANITIA;
			else if(group.isAdminAgency())
				certificate.cer_jenis = JenisCertificate.CSRR_ADMIN_AGENCY;
			else if(group.isVerifikator())
				certificate.cer_jenis = JenisCertificate.CSRR_VERIFIKATOR;
			else {
				Logger.info(ErrorCodeOSD.NOT_AUTHORIZED_USER.getLabel());
				return ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode();
			}
			
			certificate.cer_pem = null;
			certificate.csr_pem = eeCSRR;
			
//				sementara gunakan nip + repo id sebagai username untuk certificate, sebelum ada ADNP untuk memastikan nip adalah unique
			certificate.cer_username = usernamePrefix+pegawai.peg_id + '.' + ConfigurationDao.getRepoId();
			Long cer_id = insertCertificateToInaproc(certificate, pegawai.peg_namauser, currentDate);
			if(cer_id == null){
				Logger.error("certificate gagal tersimpan ke ADP");
				return ErrorCodeOSD.FAILED.getCode();
			}
			
			pegawai.cer_id = cer_id;
			pegawai.save();
			return ErrorCodeOSD.SUCCEEDED.getCode();
		} else {
			Logger.info("set CSRR failed, user: "+pegawai.peg_namauser+" already have request/active certificate");
			return ErrorCodeOSD.ALREADY_HAVE_CERT.getCode();
		}
	}

	public static String renewCSRRekanan(Rekanan rekanan, String existingSN, String subjectDN, String eeCSRR, Date currentDate) {
		Certificate certificate = null;
		if(rekanan.cer_id!=null){
//			memeriksa apakah ada di repository local, kalau tidak ada berarti pernah daftar dari lpse lain
			if(rekanan.isCertRoaming()){
				Logger.info("rekanan:" + rekanan.rkn_namauser + " mendaftar sertifikat di lpse lain");
				return ErrorCodeOSD.INVALID_LPSE.getCode();
			}
//			certificate = Certificate.getLastCertificate(rekanan.cer_id);
			certificate = getLastCertificateFromInaproc(rekanan.cer_id);
			if(certificate==null){
				Logger.info("Gagal meminta sertifikat dari Inaproc");
				return ErrorCodeOSD.FAILED.getCode();
			} else {
				if(certificate.cer_jenis.equals(JenisCertificate.CERT_PENYEDIA)){
					LogUtil.debug("RenewCSRR", " existing: " + existingSN + " from adp: " + certificate.cer_sn);
					if (existingSN.equals(certificate.cer_sn)) {
						if(certificate!=null && certificate.cer_jenis.equals(JenisCertificate.CERT_PENYEDIA) && certificate.cer_sn!=null){
							if(certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
								if (certificate.isCertExpired()) {
									Logger.info("rekanan:" + rekanan.rkn_namauser + " masa berlaku sertifikat sudah habis");
									return ErrorCodeOSD.CERT_EXPIRED.getCode();
								}
							} else {
								Logger.info("rekanan:" + rekanan.rkn_namauser + " sudah pernah melakukan pembaruan sertifikat");
								return ErrorCodeOSD.CERT_HAVE_BEEN_RENEWED.getCode();
							}
						}
						certificate.cer_subject_dn = subjectDN;
						//certificate.setCer_sn(null);
						//certificate.setCer_issuer_dn(null);
						certificate.cer_jenis = JenisCertificate.CSRR_PENYEDIA_RENEW;
						
						certificate.cer_pem = null;
						certificate.csr_pem = eeCSRR;
						
//							gunakan rkn_id milik AD sebagai username untuk certificate
						if(certificate.cer_username==null)
							certificate.cer_username = "PN."+rekanan.rkn_id;
//						rekanan.cer_id = (Long) Certificate.insertCertificate(certificate);
						rekanan.cer_id = insertCertificateToInaproc(certificate, rekanan.rkn_namauser, currentDate);
						return ErrorCodeOSD.SUCCEEDED.getCode();
					} else {
						Logger.info("sertifikat rekanan:" + rekanan.rkn_namauser + " existing, tidak cocok dengan data di database");
						return ErrorCodeOSD.CERT_DOESNOT_MATCH.getCode();
					}
				} else {
					Logger.info("set CSR-Renew failed, user: "+rekanan.rkn_namauser+" tidak memiliki sertifikat");
					return ErrorCodeOSD.FAILED_REQUEST_RENEW.getCode();
				}
				
			}
		} else {
			Logger.info("rekanan:" + rekanan.rkn_namauser + " belum pernah mendaftar/memiliki sertifikat");
			return ErrorCodeOSD.INVALID_LPSE.getCode();
		}
	}

	public static String renewCSRPegawai(Pegawai pegawai, Group group, String existingSN, String subjectDN, String eeCSRR, Date currentDate) {
		String usernamePrefix=null;
		Certificate certificate;
		if(group.isPanitia())
			usernamePrefix="PA.";
		else if(group.isVerifikator())
			usernamePrefix="VE.";
		if(group.isAdminAgency())
			usernamePrefix="AG.";
		if(pegawai.cer_id==null){
			Logger.info("Pegawai:" + pegawai.peg_namauser + " belum pernah mendaftar/memiliki sertifikat");
			return ErrorCodeOSD.INVALID_LPSE.getCode();
		} else {
//			certificate = Certificate.getLastCertificate(pegawai.cer_id);
			certificate = getLastCertificateFromInaproc(pegawai.cer_id);
		}
		
		if(certificate==null){
			Logger.info("Gagal meminta sertifikat dari Inaproc");
			return ErrorCodeOSD.FAILED.getCode();
		}
			
		if(certificate.cer_jenis.equals(JenisCertificate.CERT_PANITIA)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY)
				|| certificate.cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR)){
			if(existingSN.equals(certificate.cer_sn)){
				String jenis;
				if(group.isPanitia())
					jenis = JenisCertificate.CERT_PANITIA;
				else if(group.isAdminAgency())
					jenis = JenisCertificate.CERT_ADMIN_AGENCY;
				else if(group.isVerifikator())
					jenis = JenisCertificate.CERT_VERIFIKATOR;
				else {
					Logger.info(ErrorCodeOSD.NOT_AUTHORIZED_USER.getLabel());
					return ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode();
				}
												
				if(certificate!=null && certificate.cer_jenis.equals(jenis) && certificate.cer_sn!=null){
					if(certificate.cer_sn.equalsIgnoreCase(certificate.getSerialNumberFromPEM())) {
						if (certificate.isCertExpired()) {
							Logger.info("pegawai:" + pegawai.peg_namauser + " masa berlaku sertifikat sudah habis");
							return ErrorCodeOSD.CERT_EXPIRED.getCode();
						}
					} else {
						Logger.info("pegawai:" + pegawai.peg_namauser + " sudah pernah melakukan perpanjangan sertifikat");
						return ErrorCodeOSD.CERT_HAVE_BEEN_RENEWED.getCode();
					}
				}
				
				certificate.cer_subject_dn = subjectDN;
				//certificate.setCer_sn(null);
				//certificate.setCer_issuer_dn(null);
				if(group.isPanitia())
					certificate.cer_jenis = JenisCertificate.CSRR_PANITIA_RENEW;
				else if(group.isAdminAgency())
					certificate.cer_jenis = JenisCertificate.CSRR_ADMIN_AGENCY_RENEW;
				else if(group.isVerifikator())
					certificate.cer_jenis = JenisCertificate.CSRR_VERIFIKATOR_RENEW;
				else {
					Logger.info(ErrorCodeOSD.NOT_AUTHORIZED_USER.getLabel());
					return ErrorCodeOSD.NOT_AUTHORIZED_USER.getCode();
				}
				
				certificate.cer_pem = null;
				certificate.csr_pem = eeCSRR;
				
//					sementara gunakan nip + repo id sebagai username untuk certificate, sebelum ada ADNP untuk memastikan nip adalah unique
				if(certificate.cer_username==null)
					certificate.cer_username = usernamePrefix+pegawai.peg_id + '.' + ConfigurationDao.getRepoId();
//				pegawai.cer_id = (Long) Certificate.insertCertificate(certificate);
				
				Long cer_id = insertCertificateToInaproc(certificate, pegawai.peg_namauser, currentDate);
				if(cer_id == null){
					Logger.error("certificate gagal tersimpan ke ADP");
					return ErrorCodeOSD.FAILED.getCode();
				}
				
				pegawai.cer_id = cer_id;
				pegawai.save();
				return ErrorCodeOSD.SUCCEEDED.getCode();
			} else {
				Logger.info("sertifikat pegawai:" + pegawai.peg_nama+ " existing, tidak cocok dengan data di database");
				return ErrorCodeOSD.CERT_DOESNOT_MATCH.getCode();
			}
			
		} else {
			Logger.info("set CSR-Renew failed, user: "+pegawai.peg_namauser+" tidak memiliki sertifikat");
			return ErrorCodeOSD.FAILED_REQUEST_RENEW.getCode();
		}
	}
	
//	mengambil data repository dari ADP
	public static JsonObject getServiceUrl(String repoId) {
		if(StringUtils.isNotEmpty(repoId)) {			
			String encRepoId = DceSecurityV2.encrypt(repoId);			
			String url = ConfigurationDao.getRestCentralService() + "/repository?q=" + URLs.encodePart(encRepoId);
			try {
				WSRequest request = WS.url(url);
				request.setHeader("authentication", Play.secretKey);
				request.setHeader("Accept", "application/xml, application/json")
						.setHeader("Accept-Encoding", "gzip, deflate");
				HttpResponse response = request.get();
				if (response.success() && !StringUtils.isEmpty(response.getString())) {
					String output = DceSecurityV2.decrypt(response.getString());
					JsonParser parser = new JsonParser();
					return parser.parse(output).getAsJsonObject();
				}
			}catch (Exception e) {
				LogUtil.error(TAG, e, "getServiceUrl");
			}
		} 
		
		return null;
	}
	
	public static Long insertCertificateToInaproc(Certificate obj, String userid, Date createdDate) {
		if (userid == null){
			obj.audituser = "UNKNOWN";
		}else{
			obj.audituser = userid;
		}
		obj.audittype = "C";
		obj.auditupdate = createdDate;	
		
		obj.repoId = new Long(ConfigurationDao.getRepoId());
		if(obj.cer_id != null)
			obj.cer_versi++;
		else {
			obj.cer_id = Certificate.nextSequence();
			obj.cer_versi = new Integer(0);
		}
		String jsonToSend = new GsonBuilder().registerTypeAdapter(Double.class, new DoubleConverter()).create().toJson(obj);
		String param = URLs.encodePart(DceSecurityV2.encrypt(jsonToSend));
		String url_service = ConfigurationDao.saveRestCentralCertificateService(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse httpResponse = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
				String response = DceSecurityV2.decrypt(httpResponse.getString());
				if (response.equalsIgnoreCase("0")) {
					return null;
				}
			}
		} catch (Exception e){
			return null;
		}
		return obj.cer_id;		
	}
	
	public static Long updateCertificatePemToInaproc(Certificate obj, String audituser, Date updatedDate) {
		Certificate cert = new Certificate();
		cert.cer_id = obj.cer_id;
		cert.cer_pem = obj.cer_pem;
		
		String param = DceSecurityV2.encrypt(new GsonBuilder().create().toJson(cert));
		String url_service = ConfigurationDao.savePemRestCentralCertificateService();
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse httpResponse = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).setParameter("q", param).post();
			if(httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
				String tmp = httpResponse.getString();
				String response = DceSecurityV2.decrypt(tmp);
				if (response.equalsIgnoreCase("0")) {
					return null;
				}
			}
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
		return updateCertificateToInaproc(obj, audituser, updatedDate);		
	}
	
	public static Long updateCertificateToInaproc(Certificate obj, String audituser, Date updatedDate) {
		if (audituser == null){
			obj.audituser = "UNKNOWN";
		}else{
			obj.audituser = audituser;
		}
		obj.audittype = "U";
		obj.auditupdate = updatedDate;
		
		obj.cer_pem = null;
		
		String param = URLs.encodePart(DceSecurityV2.encrypt(new GsonBuilder().create().toJson(obj)));
		String url_service = ConfigurationDao.saveRestCentralCertificateService(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse httpResponse = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
				String response = DceSecurityV2.decrypt(httpResponse.getString());
				if (response.equalsIgnoreCase("0")) {
					return null;
				}
			}
		} catch (Exception e){
			return null;
		}
		return obj.cer_id;		
	}
	
	public static String getCertIdFromInaproc(String cer_jenis) {
		if(cer_jenis == null)
			return null;
		Certificate userCert = new Certificate();
		userCert.cer_jenis = cer_jenis;
		userCert.repoId = new Long(ConfigurationDao.getRepoId());
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(userCert)));
		String url_service = ConfigurationDao.getRestCentralCertificateServiceCerIdList(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse httpResponse = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
				String response = DceSecurityV2.decrypt(httpResponse.getString());
				if(!StringUtils.isEmpty(response)) { // jika tidak ada error
					Type collectionType = new TypeToken<List<Long>>(){}.getType();
					List<Long> list = (List<Long>) new Gson().fromJson(response, collectionType);
					if (list.size()==0)
						return null;
					else {
						StringBuilder result = new StringBuilder();
						for (int i=0; i<list.size(); i++){
							if (i>0)
								result.append(',').append(list.get(i));
							else
								result.append(list.get(i));
						}
						return result.toString();
					}
				}
			}
		} catch (Exception e){
			return null;
		}
		return null;
	}
	
	public static ErrorCodeOSD revokeCertificate(Certificate certificate, Long userId, boolean rekanan, Date date, String audituser) {
		ErrorCodeOSD errorCode = null;
		if(certificate.cer_sn==null){
			String ret = certActivateByOP(userId, rekanan, date);
//			kalau berhasil, return-nya PEM certificate
			if(ret.length()<10){
				errorCode = ErrorCodeOSD.getErrorCode(ret);
			} else
				certificate=getLastCertificateFromInaproc(certificate.cer_id);
		}
		if(errorCode==null){
			if(certificate.cer_jenis.equals(JenisCertificate.CERT_PENYEDIA))
				certificate.cer_jenis = JenisCertificate.REVOKE_REQ_PENYEDIA;
			else if(certificate.cer_jenis.equals(JenisCertificate.CERT_PANITIA))
				certificate.cer_jenis = JenisCertificate.REVOKE_REQ_PANITIA;
			else if(certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_AGENCY))
				certificate.cer_jenis = JenisCertificate.REVOKE_REQ_ADMIN_AGENCY;
			else if(certificate.cer_jenis.equals(JenisCertificate.CERT_VERIFIKATOR))
				certificate.cer_jenis = JenisCertificate.REVOKE_REQ_VERIFIKATOR;
			else if(certificate.cer_jenis.equals(JenisCertificate.CERT_ADMIN_PPE))
				certificate.cer_jenis = JenisCertificate.REVOKE_REQ_ADMIN_PPE;
			Long cer_id = updateCertificateToInaproc(certificate, audituser, date);
			if(cer_id==null) {
				errorCode = ErrorCodeOSD.FAILED;
			}
		}
		return errorCode;
	}
	
	public static ErrorCodeOSD cancelRevokeCertificate(Certificate certificate) {
		ErrorCodeOSD errorCode = null;
		if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PENYEDIA))
			certificate.cer_jenis = JenisCertificate.CERT_PENYEDIA;
		else if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_PANITIA))
			certificate.cer_jenis = JenisCertificate.CERT_PANITIA;
		else if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_AGENCY))
			certificate.cer_jenis= JenisCertificate.CERT_ADMIN_AGENCY;
		else if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_VERIFIKATOR))
			certificate.cer_jenis = JenisCertificate.CERT_VERIFIKATOR;
		else if(certificate.cer_jenis.equals(JenisCertificate.REVOKE_REQ_ADMIN_PPE))
			certificate.cer_jenis = JenisCertificate.CERT_ADMIN_PPE;
		
		String param = URLs.encodePart(DceSecurityV2.encrypt(new GsonBuilder().create().toJson(certificate)));
		String url_service = ConfigurationDao.saveRestCentralCertificateService(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse httpResponse = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
				String response = DceSecurityV2.decrypt(httpResponse.getString());
				if (response.equalsIgnoreCase("1")) {
					errorCode = ErrorCodeOSD.FAILED;
				}
			}
		} catch (Exception e){
			errorCode = ErrorCodeOSD.UNKNOWN_ERROR;
		}
		return errorCode;
	}
	
	public static String deleteCertificateFromInaproc(Long cer_id) {
		if(cer_id == null)
			return null;
		Map paramMap = new HashMap<>();
		paramMap.put("cer_id", cer_id);
		String param = URLs.encodePart(DceSecurityV2.encrypt(new GsonBuilder().create().toJson(paramMap)));
		String url_service = ConfigurationDao.deleteRestCentralCertificateService(param);
		try {
			WSRequest request = WS.url(url_service);
			HttpResponse httpResponse = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
			if(httpResponse.success() && !StringUtils.isEmpty(httpResponse.getString())) {
				return DceSecurityV2.decrypt(httpResponse.getString());
			}
		} catch (Exception e){
			LogUtil.error(TAG, e);
		}
		return null;
	}
	
	public static String createKPL(Paket paket, List<Anggota_panitia> anggotaList, Lelang_seleksi lelangItem, String spseURL) {
		String ttp_ticket_id = null;
		if(paket!=null){
			StringBuffer sBuf = new StringBuffer();
			sBuf.append("<!DOCTYPE KPL_PAKET_XML>\n");
			sBuf.append("<kpl versi=\"1.0\">\n");
			sBuf.append("\t<lpse>\n");
			sBuf.append("\t\t<lpse_id>").append(ConfigurationDao.getRepoId()).append("</lpse_id>\n");
			sBuf.append("\t\t<lpse_nama>").append(ConfigurationDao.getNamaRepo()).append("</lpse_nama>\n");
			Logger.info("create KPL : %s", spseURL);
			if(!StringUtils.isEmpty(spseURL) && !"${server.url}".equals(spseURL)) {
				Logger.info("append lpse url propreties");
				sBuf.append("\t\t<lpse_url>").append(spseURL).append("</lpse_url>\n");
			}
			sBuf.append("\t</lpse>\n");
			sBuf.append("\t<paket>\n");
			sBuf.append("\t\t<id_paket>").append(paket.pkt_id.toString()).append("</id_paket>\n");
			sBuf.append("\t\t<nama_paket>").append(cleanNamaPaket(paket.pkt_nama)).append("</nama_paket>\n");
			sBuf.append("\t</paket>\n");
			sBuf.append("\t<panitias>\n");
			for(Anggota_panitia anggota:anggotaList){
				Pegawai pegawai = Pegawai.findById(anggota.peg_id);
				Certificate certificate = getLastCertificateFromInaproc(pegawai.cer_id, JenisCertificate.CERT_PANITIA);
				if(certificate!=null){
					sBuf.append("\t\t<panitia>\n");
					sBuf.append("\t\t\t<serial_number>").append(certificate.cer_sn).append("</serial_number>\n");
					sBuf.append("\t\t\t<certificate>\n").append(certificate.cer_pem).append("\n\t\t\t</certificate>\n");
					sBuf.append("\t\t</panitia>\n");
				}
			}
			sBuf.append("\t</panitias>\n");
			sBuf.append("</kpl>\n");

			Kpl kpl = Kpl.find("pkt_id=?", paket.pkt_id).first();
			if(kpl==null){
				kpl = new Kpl();
				kpl.pkt_id = paket.pkt_id;
			}
			String kplContent = sBuf.toString();
			Logger.debug("\nCONTENT KPL:\n"+kplContent);
			kpl.kpl_content = CertificateUtil.encodeSplitLine(kplContent.getBytes());
			//	signing
			Logger.debug("\nHASH KPL:\n"+DigestUtils.sha256Hex(kplContent));
			byte[] signature = CertificateUtil.signing(kplContent);
			if(Logger.isDebugEnabled()) {
				CertificateUtil.verify( kplContent.getBytes(),signature);
			}
			if(signature!=null){
				kpl.kpl_sign = CertificateUtil.encodeSplitLine(CertificateUtil.formatSignature(signature));
				Logger.debug("\nSIGN KPL:\n" + kpl.kpl_sign);
				if(kpl.kpl_id!=null){
					kpl.save();
					if(paket.ttp_ticket_id!=null){
						String keyKPL = Kpl.getKpl(paket.pkt_id);
						updateTTP(lelangItem, keyKPL, paket.ttp_ticket_id, paket);
					} else {
						String keyKPL = Kpl.getKpl(paket.pkt_id);
						ttp_ticket_id = insertTTP(lelangItem, keyKPL, paket);
						paket.ttp_ticket_id = ttp_ticket_id;
						paket.save();
					}
				}else {
					kpl.save();
					String keyKPL = Kpl.getKpl(paket.pkt_id);
					ttp_ticket_id = insertTTP(lelangItem, keyKPL, paket);
					paket.ttp_ticket_id = ttp_ticket_id;
					
					paket.save();
					if(Logger.isDebugEnabled())
						testKPL(kpl);
				}
			} else {
				Logger.error("signing kpl failed: " + paket.pkt_id.toString() + " - " + paket.pkt_nama);
			}
		}
		return ttp_ticket_id;
	}
	
	private static String insertTTP(Lelang_seleksi lelangItem, String keyKPL, Paket paket){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try {
			String timezone = FormatUtils.getZonaWaktu();
			KmsData kmsData = new KmsData();	
			Map<String,String> mapKmsData = new HashMap<String,String>();
			
			if (lelangItem.getMetode().dokumen.isSatuFile()) {
				if (lelangItem.getPemilihan().isLelangExpress()){
					Tahap tahap1 = Tahap.PEMASUKAN_PENAWARAN;
					Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
					String timeAwalText1 = dateFormat.format(jadwal1.dtj_tglakhir) + ' ' + timezone;
					mapKmsData.put(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id.toString(),timeAwalText1);
				} else {
					Tahap tahap1 = Tahap.PEMBUKAAN_PENAWARAN;
					Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
					String timeAwalText1 = dateFormat.format(jadwal1.dtj_tglawal) + ' ' + timezone;
					mapKmsData.put(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id.toString(),timeAwalText1);
				}
				
			} else {	
				Tahap tahap1 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS;
				Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
				if(jadwal1 == null) {
					tahap1 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI;
					jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
				}
				String timeAwalText1 = dateFormat.format(jadwal1.dtj_tglawal) + ' ' + timezone;
				
				Tahap tahap2 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA;
				Jadwal jadwal2 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap2);
	//			Date timeAwal2 = jadwal2.getDtj_tglawal();
				String timeAwalText2 = dateFormat.format(jadwal2.dtj_tglawal) + ' ' + timezone;
					
				// IDOEJ 30 agustus 2017, menghindari kemungkinan kunci tertukar antara teknis dan harga
				mapKmsData.put(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id.toString(),timeAwalText1);
				mapKmsData.put(JenisDokPenawaran.PENAWARAN_HARGA.id.toString(),timeAwalText2);
			}
			
			kmsData.idDocument = mapKmsData;
			kmsData.kpl = keyKPL;
			String idTicket = null;
			try {
				idTicket = KmsClient.getTicketId(kmsData);
				return idTicket;
			} catch (Exception e) {
				e.printStackTrace();
				Logger.error(e, "Failed request TTP, paket_id: " + paket.pkt_id);
				return idTicket;
			}	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static void updateTTP(Lelang_seleksi lelangItem, String keyKPL, String ttpTicketId, Paket paket){	
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try {
			String timezone = FormatUtils.getZonaWaktu();
			KmsData kmsData = new KmsData();	
			Map<String,String> mapTtpEprocData = new HashMap<String,String>();
			
			if (lelangItem.getMetode().dokumen.isSatuFile()) {
				Tahap tahap1 = Tahap.PEMBUKAAN_PENAWARAN;
				Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
				String timeAwalText1 = dateFormat.format(jadwal1.dtj_tglawal) + ' ' + timezone;
		
				mapTtpEprocData.put(String.valueOf(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id.toString()),timeAwalText1);
			} else {	
				Tahap tahap1 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS;
				Jadwal jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
				if(jadwal1 == null) {
					tahap1 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_ADM_TEKNIS_KUALIFIKASI;
					jadwal1 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap1);
				}
				String timeAwalText1 = dateFormat.format(jadwal1.dtj_tglawal) + ' ' + timezone;
				
				Tahap tahap2 = Tahap.PEMBUKAAN_DAN_EVALUASI_PENAWARAN_BIAYA;
				Jadwal jadwal2 = Jadwal.findByLelangNTahap(lelangItem.lls_id, tahap2);
				String timeAwalText2 = dateFormat.format(jadwal2.dtj_tglawal) + ' ' + timezone;
	
				// IDOEJ 30 agustus 2017, menghindari kemungkinan kunci tertukar antara teknis dan harga
				mapTtpEprocData.put(String.valueOf(JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id.toString()),timeAwalText1);
				mapTtpEprocData.put(String.valueOf(JenisDokPenawaran.PENAWARAN_HARGA.id.toString()),timeAwalText2);
			}
	
			kmsData.idDocument = mapTtpEprocData;
			kmsData.kpl = keyKPL;
			
			try {
				KmsClient.updateWaktuPembukaan(kmsData, ttpTicketId);
			} catch (Exception e) {
				Logger.error("Failed update TTP_id: " + ttpTicketId + ", paket_id: " + paket.pkt_id);
				e.printStackTrace();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void testKPL(Kpl kpl){
		byte[] content = Base64.decodeBase64(kpl.kpl_content);
		byte[] sign = Base64.decodeBase64(kpl.kpl_sign);
		byte[] delimiter = new byte[]{35,12,5,13,19,1,14,5,7,35};
		byte[] all = new byte[content.length+sign.length+delimiter.length];
		boolean stop=false;
		int i=0;
		while(!stop){
			stop=true;
			if(i<content.length){
				all[i]=content[i];
				stop=false;
			}
			if(i<delimiter.length){
				all[i+content.length]=delimiter[i];
				stop=false;
			}
			if(i<sign.length){
				all[i+content.length+delimiter.length]=sign[i];
				stop=false;
			}
			i++;
		}
		Logger.debug("\nKPL:\n"+CertificateUtil.encodeSplitLine(all));
	}
	
//	untuk filter invalid xml character & invalid windows/linux/mac folder name
	private static String cleanNamaPaket(String pkt_nama){
		return pkt_nama.replace("&", "")
				.replace("<", "")
				.replace(">", "")
				.replace(":", "")
				.replace("\"", "")
				.replace("\'", "")
				.replace("\\", "")
				.replace("/", "")
				.replace("|", "")
				.replace("*", "")
				.replace("?", "")
				.replace("[", "")
				.replace("]", "")
				.replace("=", "")
				.replace("%", "")
				.replace("$", "")
				.replace("+", "")
				.replace(",", "")
				.replace(";", "")
				.replace("~", "")
				.replace("#", "");
	}
}