package utils;

public class Helpers {

	public static boolean isInteger(Object val) {
		try {
			Integer.parseInt(val.toString());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static Integer getInt(Object val) {
		try {
			return Integer.parseInt(val.toString());
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Long getLong(Object val, Long nullValue) {
		try {
			return Long.parseLong(val.toString());
		} catch (Exception e) {
			return nullValue;
		}
	}
	
	// valid jika versiApendo >= versiSistem
	public static boolean validApendoVersion(String versiSistem, String versiApendo) {
		String[] vSistem = versiSistem.split("\\.");
		String[] vApendo = versiApendo.split("\\.");
		int[] vSistemInt = {Integer.parseInt(vSistem[0]),Integer.parseInt(vSistem[1]),Integer.parseInt(vSistem[2])};
		int[] vApendoInt = {Integer.parseInt(vApendo[0]),Integer.parseInt(vApendo[1]),Integer.parseInt(vApendo[2])};
		return vApendoInt[0] > vSistemInt[0] 
				|| (vApendoInt[0] == vSistemInt[0] && (vApendoInt[1] > vSistemInt[1] 
						|| (vApendoInt[1] == vSistemInt[1] && vApendoInt[2] >= vSistemInt[2])));
	}
}
