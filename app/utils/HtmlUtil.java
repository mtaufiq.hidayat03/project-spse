package utils;

import com.lowagie.text.DocumentException;
import ext.StringFormat;
import models.common.ConfigurationDao;
import models.jcommon.util.DateUtil;
import org.apache.commons.lang3.time.StopWatch;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Entities;
import org.jsoup.safety.Whitelist;
import org.xhtmlrenderer.pdf.ITextRenderer;
import play.Logger;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import ext.FormatUtils;


public class HtmlUtil {

	public static String textAreaView(String text, int cols) {
		StringBuilder view = new StringBuilder();
		if(text!=null && text.length()!=0){
			view.append(text.charAt(0));
			for(int i=1;i<text.length();i++){
				if(i%cols==0)
					view.append('\n');
				view.append(text.charAt(i));
			}
		}
		return view.toString();
	}
	
	
	/**
	 * Untuk string2 yg cukup panjang dan perlu dipotong gunakan fungsi ini.
	 * Fungsi ini memotong string dan menambahkan ... dan tooltip. Tambahkan
	 * parameter raw=true
	 */
	public static String trim(String str, int charWidth) {
		if (str != null) {
			boolean trimmed = str.length() > charWidth;
			if (trimmed) {
				StringBuffer buff = new StringBuffer("<span title=\"");

				String strTrimmed = str.substring(0, charWidth);
				str = str.replaceAll("\"", "&quot;");
				buff.append(str);
				buff.append("\">");
				buff.append(strTrimmed);
				buff.append("...");
				buff.append("</span>");
				return buff.toString();
			} else {
				return str;
			}
		} else {
			return "";
		}
	}
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
		   while ((line = reader.readLine()) != null) {
		      sb.append(line).append('\n');
		   }
		} catch (IOException e) {
		      e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public static InputStream generatePDF(String key, String content) {
		Template template = TemplateLoader.load(key,content, true);
		return generatePDF(template, null);
	}

	static final Pattern patternMsOp = Pattern.compile("<o:p>");
	static final Pattern patterMsOpClose = Pattern.compile("</o:p>");
	static final Pattern patternMsPcclass= Pattern.compile("<pclass=\"msonormal\">");
	static final Pattern patternMsPcclassClose= Pattern.compile("</pclass=\"msonormal\">");
	static final Pattern patternBr= Pattern.compile("<br>");
	static final Pattern patternAnd = Pattern.compile(" & ");
	

	public static InputStream generatePDF(Template template, Map<String, Object> params) {
		InputStream is = null;
		FormatUtils datezone = new FormatUtils();
		try {
			if(params == null)
				params = new HashMap<>();
			String informasi = "Dihasilkan oleh server pada: " + StringFormat.formatDateTime(DateUtil.newDate())+" "+ datezone.getZonaWaktu() + " - "+ ConfigurationDao.getNamaLpse();
			params.put("infoCetak", informasi);
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ITextRenderer renderer = new ITextRenderer();
			String content = template.render(params);
			// cleansing tag2 msword
			content = patternMsOp.matcher(content).replaceAll("");
			content = patterMsOpClose.matcher(content).replaceAll("");
			content = patternMsPcclass.matcher(content).replaceAll("");
			content = patternMsPcclassClose.matcher(content).replaceAll("");
			content = patternBr.matcher(content).replaceAll("<br/>");
			content = patternAnd.matcher(content).replaceAll(" &amp; ");
			Document document = Jsoup.parse(content);
			for(Element element : document.select("img")){
				element.remove();
			}
			document.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
			document.outputSettings().escapeMode(Entities.EscapeMode.xhtml);
			content = document.toString();
			LogUtil.debug("content",content);
			renderer.setDocumentFromString(content);
			renderer.layout();
			renderer.createPDF(os);
			renderer.finishPDF();
			is = new ByteArrayInputStream(os.toByteArray());
			os.flush();
			os.close();
			stopWatch.stop();
			Logger.debug("time to Cetak Pdf %s: %s", template.name, stopWatch.getTime());
		} catch (DocumentException e) {
			Logger.error(e, "Document Exception %s", e.getLocalizedMessage());
		} catch (Exception e) {
			Logger.error(e, "IO Exception %s", e.getLocalizedMessage());
		}
		return is;
	}
	
	public static String gzipToString(InputStream is) {		
		if(is == null)
			return null;
        try {
        	GZIPInputStream gzip = new GZIPInputStream(is);
    		ByteArrayOutputStream out = new ByteArrayOutputStream();
    		byte[] buffer = new byte[1024];
    		int len;
    		Logger.info("reading file gzip %s");
			while ((len = gzip.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
			return out.toString();
		} catch (IOException e) {
			Logger.error("Kesalahan convert gzip to String", e);
			return null;
		}
        
	}
}
