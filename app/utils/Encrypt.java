package utils;

import models.common.IEncryptedId;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import play.Logger;
import play.Play;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;

public class Encrypt implements IEncryptedId{
    private String id;
    private static final String DCEkey = "Y95YrM1551CpRb1w";

    public void setId(String myId) {
        id = myId;
    }
    
    /**Get encrypted ID from current ID */
    public String getId() {
        return this.encrypt(id);
    }
    
    public static String decrypt(String str) {

        if (str != null) {
            int len=str.length();
            StringBuffer buff=new StringBuffer(len/2);
            for(int i=0;i<len;i=i+2)
            {
                String hex=str.substring(i,i+2);
                char[] ascii =new char[1];
                ascii[0]=(char)Integer.parseInt(hex, 16);
                String st=String.valueOf(ascii);
                buff.append(st);
            }
            return buff.toString();
        }
        return str;
    }   
    
    /*
     * Simple Encrypt. Maybe need enhancement
     *  
     * 
     */
    public static String encrypt(String str) {

        String result="";
        if (str != null) {
            int len=str.length();
            StringBuffer buff=new StringBuffer(len*2);
            for(int i=0;i<len;i++)
            {
                String hex=Integer.toHexString(str.charAt(i));
                if(hex.length()==1)
                    buff.append('0');
                buff.append(hex);
            }
            result=buff.toString();
        }
        return result;
    }
    
    /**
     * 
     * @param 
     * @return
     */
    public static String[] encrypt(String[] ary) {
        if (ary != null) {
            int size = ary.length;
            String result[] = new String[size+1];
            int cummHashcode=0;
            int i=0;
            for (; i < size; i++) {
                result[i] = encrypt(ary[i]);
                cummHashcode+=result[i].hashCode();
            }
            result[i]=Integer.toString(cummHashcode);//add hash at last index 
            return result;
        }
        return ary;
    }
    
    /*
     *Meng-encrypt array dgn menambahkan jumlah dari hashcode tiap2 elemennya 
     * */

    public static String[] decrypt(String[] ary) {
        if (ary != null) {
            int size = ary.length-1;
            String result[] = new String[size];
            int cummHashcode=0;
            int i = 0;
            for (; i < size; i++) {
                cummHashcode+=ary[i].hashCode();
                result[i] = decrypt(ary[i]);
            }
            int hashCodeParam=Integer.parseInt(ary[i]);
            if(hashCodeParam!=cummHashcode)
                throw new RuntimeException("Service Parameter telah mengalami perubahan.");
            return result;
        };
        return ary;
    }   
    
    /**
     * Melakukan encrypt input string untuk login ke Inaproc (salah satunya Inaproc)
     * @param plaintext 
     * @param sKey Secret key dari enkripsi ini
     */
    public static String encryptInaprocURL(String plaintext, String sKey) {
        //String sKey="mysecretkey";
        byte[] ciphertext;

        try {
            SecretKeySpec key = new SecretKeySpec(sKey.getBytes(), "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            ciphertext = cipher.doFinal(plaintext.getBytes());
        } 
        catch (Exception e) { 
            return null; 
        }           
                  
        return Base64.encodeBase64URLSafeString(ciphertext);
    }
    
    /**
     * Melakukan decrypt input string yang digunakan untuk login ke Inaproc (salah satunya TTS)
     * @param ciphertext 
     * @param sKey Secret key dari enkripsi ini
     */
    public static String decryptInaprocURL(String ciphertext, String sKey){
         
        byte[] plaintext;        
        
        try {
            SecretKeySpec key = new SecretKeySpec(sKey.getBytes(), "Blowfish");
             Cipher plain = Cipher.getInstance("Blowfish");
             plain.init(Cipher.DECRYPT_MODE, key);
             plaintext = plain.doFinal(Base64.decodeBase64(ciphertext));
          } catch (Exception e) { 
              return null; 
          }         
          
          return new String(plaintext);      
      }
    
    public static byte[] apendoRSADecrypt(String encrypted){
    	try {
			byte[] encryptedByte = decodeApendo(encrypted);
			return decryptApendoKey(encryptedByte, getPrivateKeyApendo());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new byte[0];
    }
    
    private static final File apendoRSAFile = new File(Play.applicationPath + "/conf/apendo4_rsa.der");
    
    private static PrivateKey getPrivateKeyApendo(){
    	if (apendoRSAFile.exists()) {
            Logger.info("EXISTS!!!");
        }

        try(DataInputStream dis = new DataInputStream(new FileInputStream(apendoRSAFile))) {
	        byte[] keyBytes = new byte[(int) apendoRSAFile.length()];
	        dis.readFully(keyBytes);

	        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
	        KeyFactory kf = KeyFactory.getInstance("RSA");
	        return kf.generatePrivate(spec);
        }catch(Exception e) {
            Logger.info("PRIVATE KEY FAILED");
        	e.printStackTrace();
        	return null;
        }
    }
    
    public static byte[] decodeApendo(String text) {
    	byte[] bytes = text.getBytes();
        return org.bouncycastle.util.encoders.Base64.decode(bytes);
    }
    
    public static byte[] decryptApendoKey(byte[] encryptedBytes, PrivateKey key) {
    	try {
			Security.addProvider(new BouncyCastleProvider());
	//		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
			Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding", "BC");
			cipher.init(Cipher.DECRYPT_MODE, key);
			return cipher.doFinal(encryptedBytes);
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
    
    public static byte[] decryptApendoContent(byte[] cipherT, byte[] keyByte) {
    	try {
	    	Security.addProvider(new BouncyCastleProvider());
	    	SecretKeySpec key = new SecretKeySpec(keyByte,"AES");
	        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
	        cipher.init(Cipher.DECRYPT_MODE, key);
	        byte[] plainT = new byte[cipher.getOutputSize(cipherT.length)];
	        int ptLenght = cipher.update(cipherT, 0, cipherT.length, plainT, 0);
	        ptLenght += cipher.doFinal(plainT, ptLenght);
	        byte[] ret = new byte[ptLenght];
            System.arraycopy(plainT, 0, ret, 0, ptLenght);
	        return ret;
    	} catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
    }
    
    public static byte[] encryptRSA(byte[] textBytes, PublicKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException {
		Security.addProvider(new BouncyCastleProvider());
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding", "BC");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(textBytes);
	}


	public static byte[] decryptRSA(byte[] encryptedBytes, PrivateKey key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException {
		Security.addProvider(new BouncyCastleProvider());
//		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
		Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA1AndMGF1Padding", "BC");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(encryptedBytes);
	}
	
	public static byte[] encryptAES(String plainT, byte[] keyByte) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, ShortBufferException, IllegalBlockSizeException, BadPaddingException{
    	return encryptAES(plainT.getBytes(), keyByte);
    }
	
	public static byte[] encryptAES(byte[] plainT, byte[] keyByte) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, ShortBufferException, IllegalBlockSizeException, BadPaddingException{
    	Security.addProvider(new BouncyCastleProvider());
    	SecretKeySpec key = new SecretKeySpec(keyByte,"AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] cipherT = new byte[cipher.getOutputSize(plainT.length)];
        int ctLenght = cipher.update(plainT, 0, plainT.length, cipherT, 0);
        ctLenght += cipher.doFinal(cipherT, ctLenght);
        return cipherT;
    }

    public static byte[] decryptAES(byte[] cipherT, byte[] keyByte) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, ShortBufferException, IllegalBlockSizeException, BadPaddingException{
    	Security.addProvider(new BouncyCastleProvider());
    	SecretKeySpec key = new SecretKeySpec(keyByte,"AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] plainT = new byte[cipher.getOutputSize(cipherT.length)];
        int ptLenght = cipher.update(cipherT, 0, cipherT.length, plainT, 0);
        ptLenght += cipher.doFinal(plainT, ptLenght);
        byte[] ret = new byte[ptLenght];
        System.arraycopy(plainT, 0, ret, 0, ptLenght);
        return ret;
    }

	public static byte[] decryptDCEToByte(String str) {
		try {
			SecretKey key = new SecretKeySpec("Y95YrM1551CpRb1w".getBytes(), "AES");
			Cipher dcipher = Cipher.getInstance("AES");
			dcipher.init(Cipher.DECRYPT_MODE, key);
			byte[] dec = Base64.decodeBase64(str.getBytes("UTF-8"));
			byte[] utf8 = dcipher.doFinal(dec);
			return utf8;
		} catch (Exception e) {
			Logger.info("Failed in Decryption %s Error Stack :%s", str, e.toString());
		}
		return null;
	}

    public static String encryptSikapParams(byte[] bytes) {
        try {
            SecretKey key = new SecretKeySpec(DCEkey.getBytes(), "AES");
            Cipher ecipher = Cipher.getInstance("AES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] utf8 = bytes;
            byte[] enc = ecipher.doFinal(utf8);
            String result = Base64.encodeBase64String(enc);
            utf8 = null;
            enc = null;
            Logger.debug("Succeded in Encryption");
            return result;
        } catch (Exception e) {
            Logger.error(e, "Failed in Encryption");
        }
        return null;
    }

    public static String decryptSikapParams(String str) {
        try {
            SecretKey key = new SecretKeySpec(DCEkey.getBytes(), "AES");
            Cipher dcipher = Cipher.getInstance("AES");
            dcipher.init(Cipher.DECRYPT_MODE, key);
            byte[] dec = Base64.decodeBase64(str.getBytes("UTF-8"));
            byte[] utf8 = dcipher.doFinal(dec);
            String result = new String(utf8, "UTF-8");
            dec = null;
            utf8 = null;
            key = null;
            dcipher = null;
            Logger.debug("Succeded in Decryption from %s to %s", str, result);
            return result;
        } catch (Exception e) {
            Logger.error(e, "Failed in Decryption %s Error Stack :%s", str, e.toString());
        }
        return null;
    }
}
