package utils;

import java.util.List;

public class ModelValidationException extends Exception {
	public List<String> errors;
	public ModelValidationException(List<String> errors) {
		this.errors = errors;
	}
}
