package utils;

import models.jcommon.util.CommonUtil;
import play.Logger;

/**
 * @author HanusaCloud on 5/11/2018
 * this class is.. just the same as Logger.class basically,
 * but with abilities to print Object as json and giving TAG to each line for easy tracking.
 */
public class LogUtil {

    public static void info(String TAG, String message) {
        Logger.info(TAG + " - " + message);
    }

    public static void info(String TAG, Object object) {
        info(TAG, CommonUtil.toJson(object));
    }

    public static void debug(String TAG, String message) {
        Logger.debug(TAG + " - " + message);
    }

    public static void debug(String TAG, Object object) {
        debug(TAG, CommonUtil.toJson(object));
    }

    public static void error(String TAG, Throwable t, String message) {
        Logger.error(t, TAG + " - " + t.getMessage() + " - " + message);
    }

    public static void error(String TAG, Throwable t) {
        error(TAG, t, t.getMessage());
    }

    public static void multiline(String TAG, String message) {
        printLines(TAG, message);
    }

    public static void multiline(String TAG, Object obj) {
        final String message = CommonUtil.toJson(obj);
        printLines(TAG, message);
    }

    private static void printLines(String TAG, String message) {
        if (!CommonUtil.isEmpty(message)) {
            int maxLogSize = 500;
            for (int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                debug(TAG, "[" + i + "]" + message.substring(start, end));
                if (i == 50) {
                    debug(TAG, "[end]Too much to show.....");
                    break;
                }
            }
        }
    }

}
