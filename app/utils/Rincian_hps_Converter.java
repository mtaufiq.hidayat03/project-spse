package utils;

import com.google.gson.*;
import models.agency.Rincian_hps;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;

public class Rincian_hps_Converter implements JsonDeserializer<Rincian_hps> {

	@Override
	public Rincian_hps deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		Rincian_hps rincian = null;
		if(json.isJsonNull())
			return null;
		try {			
			 	JsonObject obj = json.getAsJsonObject();	
			 	JsonElement value = null;
				 rincian = new Rincian_hps();
				 rincian.item = obj.get("item").getAsString();
				 value = obj.get("keterangan");
				 if(value != null && !value.isJsonNull())
					 rincian.keterangan = value.getAsString();
				 rincian.unit = obj.get("unit").getAsString();
				 value = obj.get("unit2");
				 if(value != null && !value.isJsonNull())
					 rincian.unit2 = value.getAsString();
				 rincian.harga = toDouble(obj.get("harga").getAsString());
				 rincian.total_harga = toDouble(obj.get("total_harga").getAsString());
				 rincian.vol = toDouble(obj.get("vol").getAsString());
				 value = obj.get("vol2");
				 if(value != null && !value.isJsonNull())
					 rincian.vol2 = toDouble(value.getAsString());
				 value = obj.get("pajak");
				 if(value != null && !value.isJsonNull())
					 rincian.pajak = value.getAsDouble();
		} catch (Exception e) {
			throw new JsonParseException(e);
		}
		return rincian;
	}

	private Double toDouble(String data) {
		if(!StringUtils.isEmpty(data)) {
			data = StringUtils.replace(data,",","");
//			data = StringUtils.replace(data,",",".");
			return Double.parseDouble(data);
		}	
		return null;
	}
}
