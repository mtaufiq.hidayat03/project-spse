package utils;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Lambang on 5/2/2016.
 */
public class ConvertDoubleJsonUtils implements JsonDeserializer<Double> {
    @Override
    public Double deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        try {
            String value = json.getAsString();
            if(value == null || value.isEmpty())
                return 0d;
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            return Double.parseDouble(json.getAsString().replaceAll("\\s+",""));
        }
    }

}
