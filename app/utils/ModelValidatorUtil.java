package utils;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.data.validation.Validation.ValidationResult;
import play.i18n.Messages;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ModelValidatorUtil {
	/**
	 * 
	 * @param model
	 * @param i8nPrefix, prefix dari message yang ada di file messages. Misal di file messages ada entry ijin_usaha.ius_no, maka i8nPrefix = "ijin_usaha."
	 * @return
	 */
	public static List<String> validate(Object model, String i8nPrefix) throws IllegalAccessException {
		List<String> errors = new ArrayList<>();
		Field[] fields = model.getClass().getDeclaredFields();
	    for(Field field: fields){
	        if(field.isAnnotationPresent(Required.class)){
	            ValidationResult res = Validation.required(field.getName(), field.get(model));
	            if(!res.ok) {
	            	errors.add(Messages.get(i8nPrefix + field.getName()) + Messages.get("utility.ud") + Messages.get(model.getClass().getSimpleName().toLowerCase()) + Messages.get("utility.tbk"));
	            }
	        } 
	        if(field.isAnnotationPresent(MaxSize.class)){
	        	int size = field.getAnnotation(MaxSize.class).value();
	            ValidationResult res = Validation.maxSize(field.getName(), field.get(model), size);
	            if(!res.ok) {
	            	errors.add(Messages.get(i8nPrefix + field.getName()) + Messages.get("utility.ud") + Messages.get(model.getClass().getSimpleName().toLowerCase()) + Messages.get("utility.mum") + size);
	            }
	        }
	    }
	    return errors;
	}
	
	public static String printError(List<String> errors) {
		StringBuilder sb = new StringBuilder();
		for(String err:errors) {
			sb.append('[').append(err).append(']').append(',');
		}
		return sb.toString();
	}
}
