package utils;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import models.common.BlacklistCheckerHistory;
import models.common.BlacklistCheckerHistoryPl;
import models.common.ConfigurationDao;
import models.rekanan.Rekanan;
import play.Logger;
import play.Play;
import play.libs.WS;
import play.libs.ws.HttpResponse;
import play.libs.ws.WSRequest;

/**
 * Created by Lambang on 11/2/2016.
 */
public class BlacklistCheckerUtil {


    private static boolean getBlacklistStatus(String npwp) throws Exception {
        String url_service = BasicCtr.INAPROC_URL+"/api/blacklist/check/npwp?arg="+npwp;
        try {
            WSRequest request = WS.url(url_service);
            HttpResponse response = null;
            JsonObject result = null;
            if (Play.mode.isDev()) {
                response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).authenticate("eproc", "$#@3pr0c$#@")
                        .get();
            } else {
                response = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get();
            }
            if (response.success() && response.getJson() != null) {
                result = response.getJson().getAsJsonObject();
                return result.get("status").getAsBoolean();
            }
        } catch (Exception e) {
            throw e;
        }
        return false;
    }

    public static Integer checkBlacklistStatus(Rekanan rekanan, Long pegId, Long lelangId, Integer type){
        Integer status = null;
        try{
            Logger.info("[Blacklist-Check] Sedang Melakukan Check Blacklist...");
            status = getBlacklistStatus(rekanan.rkn_npwp)? 1 : 0;
            Logger.info("result: " + status);
        } catch (Exception e){
            Logger.error("[Blacklist-Check] Terjadi Kesalahan  %s", e.getLocalizedMessage());
            status = 2;
        }
        //simpan hasil pemeriksaan ke database
        BlacklistCheckerHistory.saveCheckHistory(BasicCtr.INAPROC_URL+"/api/blacklist/check/user?arg="+rekanan.rkn_nama, pegId, lelangId, type, status, rekanan.rkn_id);
        return status;

    }

    public static Integer checkBlacklistStatusPl(Rekanan rekanan, Long pegId, Long lelangId, Integer type) {
        Integer status = null;
        try {
            Logger.info("[Blacklist-Check] Sedang Melakukan Check Blacklist...");
            status = getBlacklistStatus(rekanan.rkn_npwp) ? 1 : 0;
        } catch (Exception e) {
            Logger.error("[Blacklist-Check] Terjadi Kesalahan  %s", e.getLocalizedMessage());
            status = 2;
        }
        //simpan hasil pemeriksaan ke database
        BlacklistCheckerHistoryPl.saveCheckHistoryPl(BasicCtr.INAPROC_URL + "/api/blacklist/check/user?arg=" + rekanan.rkn_nama, pegId, lelangId, type, status, rekanan.rkn_id);
        return status;

    }

}


