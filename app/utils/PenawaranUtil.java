package utils;

import models.common.Active_user;
import models.jcommon.util.CommonUtil;
import models.lelang.Dok_penawaran;
import models.lelang.Lelang_seleksi;
import models.lelang.Peserta;
import play.i18n.Messages;

import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 8/10/2018
 */
public class PenawaranUtil {

    public static Map<String, Object> getDataPenawaran(Active_user active_user, Long id, int jenis) {
        Map<String, Object> param = new HashMap<>();
        Lelang_seleksi lelang = Lelang_seleksi.findById(id);
        param.put("jenis", Dok_penawaran.JenisDokPenawaran.fromValue(jenis));
        param.put("lelang", lelang);
        param.put("tahapAktif", lelang.getTahapNow());
        boolean allowed = false;
        if (active_user.isRekanan()) { // untuk rekanan
            Peserta peserta = Peserta.find("lls_id=? and rkn_id=?", lelang.lls_id, active_user.rekananId).first();
            param.put("peserta", peserta);
            Dok_penawaran penawaran_kualifikasi = Dok_penawaran.findPenawaranPeserta(peserta.psr_id,
                    Dok_penawaran.JenisDokPenawaran.PENAWARAN_KUALIFIKASI);
            boolean kualifikasiUploaded = penawaran_kualifikasi != null || lelang.getPemilihan().isLelangExpress();

            allowed = active_user.isRekanan() && peserta != null && kualifikasiUploaded;
            if (lelang.isSatuFile()) {
                allowed = allowed && jenis == Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI_HARGA.id;
            } else {
                allowed = allowed && (jenis == Dok_penawaran.JenisDokPenawaran.PENAWARAN_TEKNIS_ADMINISTRASI.id
                        || jenis == Dok_penawaran.JenisDokPenawaran.PENAWARAN_HARGA.id);
            }
        }
        param.put("allowed", allowed);
        return param;
    }

    public static String template(Lelang_seleksi lelang,
                                  Peserta peserta,
                                  Dok_penawaran penawaran_harga,
                                  Dok_penawaran penawaran_teknis,
                                  Dok_penawaran.JenisDokPenawaran jenis) {
        StringBuilder content = new StringBuilder();
        if (lelang.isSatuFile()) { // 1 file
            if (lelang.getPemilihan().isLelangExpress()) {
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+ Messages.get("utility.doc_ph") +"</div>");
                content.append("<div class=\"panel-body\">");
                Dok_penawaran penawaran_all = peserta.getFileAdminTeknisHarga();
                if(penawaran_all != null)
                    content.append(penawaran_all.getInfo(lelang.isLelangV3()));
                else
                    content.append("Belum dikirim");
                content.append("</div>");
                content.append("</div>");
            } else if (lelang.isLelangV3()) {
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("utility.doc_pa_th")+"</div>");
                content.append("<div class=\"panel-body\">");
                if (penawaran_harga != null)
                    content.append(penawaran_harga.getInfo(lelang.isLelangV3()));
                else
                    content.append("Belum dikirim");
                content.append("</div>");
                content.append("</div>");
            } else {
                // adm teknis
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("utility.doc_pat")+"</div>");
                content.append("<div class=\"panel-body\">");
                if (penawaran_teknis != null)
                    content.append(penawaran_teknis.getInfo(lelang.isLelangV3()));
                else
                    content.append(Messages.get("utility.bd"));
                content.append("</div>");
                content.append("</div>");

                // harga
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("utility.doc_ph")+"</div>");
                content.append("<div class=\"panel-body\">");
                if (penawaran_harga != null)
                    content.append(penawaran_harga.getInfo(lelang.isLelangV3()));
                else
                    content.append(Messages.get("utility.bd"));
                content.append("</div>");
                content.append("</div>");
            }
        } else { // 2 file / 2 tahap
            if (jenis.isAdmTeknis()) {
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("utility.doc_pat")+"</div>");
                content.append("<div class=\"panel-body\">");
                if (penawaran_teknis != null)
                    content.append(penawaran_teknis.getInfo(lelang.isLelangV3()));
                else
                    content.append(Messages.get("utility.bd"));
                content.append("</div>");
                content.append("</div>");
            } else {
                content.append("<div class=\"panel panel-info\">");
                content.append("<div class=\"panel-heading\">"+Messages.get("utility.doc_ph")+"</div>");
                content.append("<div class=\"panel-body\">");
                if (penawaran_harga != null)
                    content.append(penawaran_harga.getInfo(lelang.isLelangV3()));
                else
                    content.append(Messages.get("utility.bd"));
                content.append("</div>");
                content.append("</div>");
            }
        }
        return content.toString();
    }

    public static String toJson(Active_user active_user,Long id, int jenis) {
        Map<String, Object> param = getDataPenawaran(active_user, id, jenis);
        return new PenawaranJsonResponse(true,
                template(
                        (Lelang_seleksi) param.get("lelang"),
                        (Peserta) param.get("peserta"),
                        (Dok_penawaran) param.get("penawaran_harga"),
                        (Dok_penawaran) param.get("penawaran_teknis"),
                        (Dok_penawaran.JenisDokPenawaran) param.get("jenis")))
                .toJson();
    }

    public static class PenawaranJsonResponse {

        public final String template;
        public final boolean status;

        public PenawaranJsonResponse(boolean status, String template) {
            this.template = template;
            this.status = status;
        }

        public String toJson() {
            return CommonUtil.toJson(this);
        }

    }

}