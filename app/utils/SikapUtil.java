package utils;

import com.google.gson.JsonObject;
import controllers.BasicCtr;
import models.common.ConfigurationDao;
import models.common.Tahap;
import models.jcommon.util.CommonUtil;
import models.lelang.Jadwal;
import models.lelang.Lelang_seleksi;
import models.sso.common.adp.util.DceSecurityV2;
import play.libs.URLs;
import play.libs.WS;
import play.libs.ws.WSRequest;

import java.util.concurrent.ExecutionException;

/**
 * Created by rayhanfrds on 4/19/17.
 * TODO : isi dengan methods terkait integrasi dengan SIKaP
 */
public class SikapUtil {

	public static final String URL_SIKAP_ISCRITERIA_CREATED = BasicCtr.SIKAP_URL + "/services/isCriteriaCreated?q=";

	/**
	 * @param lelang
	 * @return
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * digunakan untuk memeriksa shortlist ke SIKaP
	 */
	public static boolean isCriteriaCreated(Lelang_seleksi lelang) throws Exception {
		boolean hasCriteria = false;

		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("lls_id", lelang.lls_id);
		Jadwal jadwal = Jadwal.findByLelangNTahap(lelang.lls_id, Tahap.PEMASUKAN_PENAWARAN);
		if (jadwal != null) {
			if(jadwal.dtj_tglawal == null || jadwal.dtj_tglakhir == null){
				return false;
			}
			jsonObject.addProperty("dtj_tglawal", jadwal.dtj_tglawal.toString());
			jsonObject.addProperty("dtj_tglakhir", jadwal.dtj_tglakhir.toString());
		}
		String param = URLs.encodePart(DceSecurityV2.encrypt(CommonUtil.toJson(jsonObject)));
		try {
			WSRequest request = WS.url(URL_SIKAP_ISCRITERIA_CREATED + param);
			String content = request.timeout(ConfigurationDao.CONNECTION_TIMEOUT).get().getString();
			if (!CommonUtil.isEmpty(content)) {
				hasCriteria = Boolean.parseBoolean(DceSecurityV2.decrypt(content));
			}
		} catch (Exception e){
			throw e;
		}
		return hasCriteria;
	}
}
