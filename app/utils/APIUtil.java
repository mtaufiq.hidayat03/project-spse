package utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import models.common.JenisKontrak;
import models.common.Metode;
import models.common.MetodeEvaluasi;
import models.common.MetodePemilihan;
import org.sql2o.ResultSetHandler;
import play.db.jdbc.Query;
import play.db.jdbc.QueryBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Compact API agar penarikan data lebih efisien
 *
 * @author Mohamad Irvan Faradian
 */
public class APIUtil {
  private static final Gson gson = new Gson();
  private static final JsonParser jp = new JsonParser();

  private static class lelangLengkapType {
    Long id_lelang;
    Short jenis_pekerjaan;
    String nama_paket;
    Long nilai_pagu;
    Long nilai_hps;
    JsonArray id_rup;
    String id_instansi;
    JsonArray anggaran;
    JsonArray lokasi_pekerjaan;
    Short jumlah_peserta;
    JsonArray jadwal;
    String mtd_id;
    String mtd_pemilihan;
    String mtd_evaluasi;
    String lls_diulang_karena;
    String lls_ditutup_karena;
    String lls_kontrak_pembayaran;
    JsonArray evaluasi;
  }

  private static final ResultSetHandler<lelangLengkapType> lelangLengkapRS = new ResultSetHandler<lelangLengkapType>() {
    @Override
    public lelangLengkapType handle(ResultSet rs) throws SQLException {
      lelangLengkapType obj = new lelangLengkapType();
      obj.id_lelang = rs.getLong("id_lelang");
      obj.jenis_pekerjaan = rs.getShort("jenis_pekerjaan");
      obj.nama_paket = rs.getString("nama_paket");
      obj.nilai_pagu = rs.getLong("nilai_pagu");
      obj.nilai_hps = rs.getLong("nilai_hps");
      obj.id_rup = (rs.getString("id_rup") != null) ? jp.parse(rs.getString("id_rup")).getAsJsonArray() : null;
      obj.id_instansi = rs.getString("id_instansi");
      obj.anggaran = (rs.getString("anggaran") != null) ? jp.parse(rs.getString("anggaran")).getAsJsonArray() : null;
      obj.lokasi_pekerjaan = (rs.getString("lokasi_pekerjaan") != null) ? jp.parse(rs.getString("lokasi_pekerjaan")).getAsJsonArray() : null;
      obj.jumlah_peserta = rs.getShort("jumlah_peserta");
      obj.jadwal = (rs.getString("jadwal") != null) ? jp.parse(rs.getString("jadwal")).getAsJsonArray() : null;
      obj.mtd_pemilihan = MetodePemilihan.findById(rs.getInt("mtd_pemilihan")).toString();
      obj.mtd_id = Metode.findById(rs.getInt("mtd_id")).toString();
      obj.mtd_evaluasi = MetodeEvaluasi.findById(rs.getInt("mtd_evaluasi")).toString();
      obj.lls_diulang_karena = rs.getString("lls_diulang_karena");
      obj.lls_ditutup_karena = rs.getString("lls_ditutup_karena");
      obj.lls_kontrak_pembayaran = JenisKontrak.findById(rs.getInt("lls_kontrak_pembayaran")).toString();
      obj.evaluasi = (rs.getString("evaluasi") != null) ? jp.parse(rs.getString("evaluasi")).getAsJsonArray() : null;
      return obj;
    }
  };

  /**
   * @param tahun : Tahun Anggaran
   */
  public static String lelangLengkapJSON(Integer tahun) {
    final QueryBuilder sql = new QueryBuilder("SELECT * FROM (\n" +
      "  SELECT\n" +
      "    l.lls_id AS id_lelang,\n" +
      "    p.kgr_id AS jenis_pekerjaan,\n" +
      "    p.pkt_nama AS nama_paket,\n" +
      "    p.pkt_pagu AS nilai_pagu,\n" +
      "    p.pkt_hps AS nilai_hps,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        JSON_AGG(rup.id)\n" +
      "      FROM\n" +
      "        paket_sirup rup\n" +
      "        JOIN paket_satker ps ON rup.id = ps.rup_id\n" +
      "      WHERE\n" +
      "        ps.pkt_id = l.pkt_id\n" +
      "    ) AS id_rup,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        ARRAY_TO_STRING(ARRAY_AGG(distinct instansi_id),',')\n" +
      "      FROM\n" +
      "        paket_satker ps\n" +
      "        LEFT JOIN satuan_kerja sk ON ps.stk_id = sk.stk_id\n" +
      "      WHERE\n" +
      "        ps.pkt_id = p.pkt_id\n" +
      "    ) AS id_instansi,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        JSON_AGG(data)\n" +
      "      FROM (\n" +
      "        SELECT\n" +
      "          a.stk_id,\n" +
      "          a.ang_koderekening,\n" +
      "          a.ang_tahun,\n" +
      "          a.ang_nilai,\n" +
      "          a.sbd_id\n" +
      "        FROM\n" +
      "          paket_anggaran pa\n" +
      "          LEFT JOIN anggaran a ON pa.ang_id = a.ang_id\n" +
      "        WHERE\n" +
      "          a.ang_tahun = " + tahun + "\n" +
      "          AND pa.pkt_id = l.pkt_id\n" +
      "        ORDER BY\n" +
      "          a.stk_id ASC\n" +
      "      ) data\n" +
      "    ) AS anggaran,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        JSON_AGG(data)\n" +
      "      FROM (\n" +
      "        SELECT\n" +
      "          pl.kbp_id,\n" +
      "          pl.pkt_lokasi\n" +
      "        FROM\n" +
      "          paket_lokasi pl\n" +
      "        WHERE\n" +
      "          pl.pkt_id = l.pkt_id\n" +
      "      ) data\n" +
      "    ) AS lokasi_pekerjaan,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        COUNT(p.psr_id)\n" +
      "      FROM\n" +
      "        peserta p\n" +
      "      WHERE\n" +
      "        p.lls_id = l.lls_id\n" +
      "    ) as jumlah_peserta,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        JSON_AGG(data)\n" +
      "      FROM (\n" +
      "        SELECT\n" +
      "          j.dtj_id,\n" +
      "          a.akt_jenis,\n" +
      "          j.dtj_tglawal,\n" +
      "          j.dtj_tglakhir\n" +
      "        FROM\n" +
      "          jadwal j\n" +
      "          LEFT JOIN aktivitas a ON j.akt_id = a.akt_id\n" +
      "        WHERE\n" +
      "          j.lls_id = l.lls_id\n" +
      "        ORDER BY\n" +
      "          akt_urut ASC\n" +
      "      ) data\n" +
      "    ) AS jadwal,\n" +
      "    l.mtd_id,\n" +
      "    l.mtd_pemilihan,\n" +
      "    l.mtd_evaluasi,\n" +
      "    l.lls_diulang_karena,\n" +
      "    l.lls_ditutup_karena,\n" +
      "    l.lls_kontrak_pembayaran,\n" +
      "    (\n" +
      "      SELECT\n" +
      "        JSON_AGG(data)\n" +
      "      FROM (\n" +
      "        SELECT\n" +
      "          p.psr_id,\n" +
      "          r.rkn_id,\n" +
      "          r.rkn_nama,\n" +
      "          r.rkn_alamat,\n" +
      "          r.rkn_npwp,\n" +
      "          k.nev_lulus AS kualifikasi,\n" +
      "          k.nev_skor AS skor_kualifikasi,\n" +
      "          k.nev_uraian AS als_kualifikasi,\n" +
      "          a.nev_lulus AS administrasi,\n" +
      "          a.nev_uraian AS als_administrasi,\n" +
      "          t.nev_lulus AS teknis,\n" +
      "          t.nev_skor AS skor_teknis,\n" +
      "          t.nev_uraian AS als_teknis,\n" +
      "          h.nev_lulus AS harga,\n" +
      "          h.nev_skor AS skor_harga,\n" +
      "          h.nev_uraian AS als_harga,\n" +
      "          p.psr_harga,\n" +
      "          p.psr_harga_terkoreksi,\n" +
      "          s.nev_lulus AS pemenang,\n" +
      "          p.is_pemenang_verif AS pemenang_verif,\n" +
      "          q.nev_lulus AS pembuktian,\n" +
      "          q.nev_skor AS skor_pembuktian,\n" +
      "          q.nev_uraian AS als_pembuktian,\n" +
      "          s.nev_skor AS skor_akhir,\n" +
      "          psr_alasan_menang\n" +
      "        FROM\n" +
      "          peserta p\n" +
      "          LEFT JOIN rekanan r ON p.rkn_id = r.rkn_id\n" +
      "          LEFT JOIN dok_penawaran d ON p.psr_id = d.psr_id AND d.dok_jenis IN ( 2, 3 )\n" +
      "          LEFT JOIN (\n" +
      "            SELECT\n" +
      "              *\n" +
      "            FROM\n" +
      "              nilai_evaluasi n,\n" +
      "              evaluasi e\n" +
      "            WHERE\n" +
      "              n.eva_id = e.eva_id\n" +
      "              AND e.lls_id = l.lls_id\n" +
      "              AND eva_jenis = 5\n" +
      "              AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id = l.lls_id)\n" +
      "          ) q ON p.psr_id = q.psr_id\n" +
      "          LEFT JOIN (\n" +
      "            SELECT\n" +
      "              *\n" +
      "            FROM\n" +
      "              nilai_evaluasi n,\n" +
      "              evaluasi e\n" +
      "            WHERE\n" +
      "              n.eva_id = e.eva_id\n" +
      "              AND e.lls_id = l.lls_id\n" +
      "              AND eva_jenis = 0\n" +
      "              AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id = l.lls_id)\n" +
      "          ) k ON p.psr_id = k.psr_id\n" +
      "          LEFT JOIN (\n" +
      "            SELECT\n" +
      "              *\n" +
      "            FROM\n" +
      "              nilai_evaluasi n,\n" +
      "              evaluasi e\n" +
      "            WHERE\n" +
      "              n.eva_id = e.eva_id\n" +
      "              AND e.lls_id = l.lls_id\n" +
      "              AND eva_jenis = 1\n" +
      "              AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id = l.lls_id)\n" +
      "          ) a ON p.psr_id = a.psr_id\n" +
      "          LEFT JOIN (\n" +
      "            SELECT\n" +
      "              *\n" +
      "            FROM\n" +
      "              nilai_evaluasi n,\n" +
      "              evaluasi e\n" +
      "            WHERE\n" +
      "              n.eva_id = e.eva_id\n" +
      "              AND e.lls_id = l.lls_id\n" +
      "              AND eva_jenis = 2\n" +
      "              AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id = l.lls_id)\n" +
      "          ) t ON p.psr_id = t.psr_id\n" +
      "          LEFT JOIN (\n" +
      "            SELECT\n" +
      "              *\n" +
      "            FROM\n" +
      "              nilai_evaluasi n,\n" +
      "              evaluasi e\n" +
      "            WHERE\n" +
      "              n.eva_id = e.eva_id\n" +
      "              AND e.lls_id = l.lls_id\n" +
      "              AND eva_jenis = 3\n" +
      "              AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id = l.lls_id)\n" +
      "          ) h ON p.psr_id = h.psr_id\n" +
      "          LEFT JOIN (\n" +
      "            SELECT\n" +
      "              *\n" +
      "            FROM\n" +
      "              nilai_evaluasi n,\n" +
      "              evaluasi e\n" +
      "            WHERE\n" +
      "              n.eva_id = e.eva_id\n" +
      "              AND e.lls_id = l.lls_id\n" +
      "              AND eva_jenis = 4\n" +
      "              AND eva_versi = (SELECT MAX(eva_versi) FROM evaluasi WHERE lls_id = l.lls_id)\n" +
      "          ) s ON p.psr_id = s.psr_id\n" +
      "        WHERE\n" +
      "          p.lls_id = l.lls_id\n" +
      "        ORDER BY\n" +
      "          psr_harga ASC\n" +
      "      ) data\n" +
      "    ) AS evaluasi\n" +
      "  FROM\n" +
      "    lelang_seleksi l\n" +
      "    LEFT JOIN paket p ON l.pkt_id = p.pkt_id\n" +
      "  WHERE\n" +
      "    l.lls_status = 1\n" +
      ") d WHERE d.anggaran IS NOT NULL");

    List<String[]> data = Query.find(sql, lelangLengkapRS).fetch();
    return gson.toJson(data);
  }
}
